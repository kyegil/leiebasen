<?php return [
    'additional_configs' => [
    ],
    'routes' => [
        'offentlig' => [],
        'drift' => [],
        'flyko' => [],
        'oppfølging' => [],
        'sentral' => [],
        'min-søknad' => [],
        'mine-sider' => [],
    ],
    'class_preferences' => [
    ],
    'leiebasen' => [
        'db' => [
            'host' => 'localhost',
            'name' => 'leiebasen',
            'user' => 'root',
            'password' => 'root',
            'port' => null,
            'driver' => 'mysqli',
            'prefix' => '',
        ],
        'server' => [
            'installation_url' => 'http://localhost:8888',
            'root' => '~/public_html/leiebasen/',
            'file_storage' => '~/leiebasen_filarkiv',
            'production' => false,
            'db_backup' => '',
            'template_folders' => [
                '~/public_html/leiebasen/visningsfiler_extjs/',
                '~/public_html/leiebasen/visningsfiler/',
            ],
        ],
        'session' => [
            'name' => 'leiebasen',
            'timeout' => 3600
        ],
        'config' => [
            'default_url' => 'http://localhost:8888/',
            'post_logout_url' => 'http://localhost:8888/',
            'extjs_version' => 'ext-3.4.0',
            'automatisk_utskrift' => [
                'buntbegrensning' => 100
            ]
        ],
        'debug' => [
            'developer_email' => 'kyegil+999@gmail.com',
            'email_dev_copy' => [
                'beboerstøtte_innlegg',
                'beboerstøtte_sakstråd',
                'beboerstøtte_sakstråd_innlegg',
                'betalingskvittering',
                'betalingsplan_brutt_avtale',
                'betalingsplan_forslag',
                'betalingsplan_kommende_avdrag',
                'betalingsplan_manglende_avdrag',
                'betalingsplan_vedtak',
                'betalingsutsettelse_forespørsel',
                'betalingsutsettelse_vedtak',
                'brukerprofil_bekreftelse',
                'brukerprofil_engangsbillett',
                'cms_side_mottak',
                'depositum_utløpsvarsel',
                'drift_melding',
                'efakturaforsendelse_forkastet',
                'efakturaoppdrag_forkastet',
                'epostpurring',
                'epostregning',
                'forfallsvarsel',
                'kostnadsdeling_varsel',
                'leieforhold_utløpsvarsel',
                'leieregulering',
                'masseepost',
                'masseepost_oppsummering',
                'masseepost_tilfeldig_eksempel',
                'min_søknad_glemt_passord',
                'min_søknad_glemt_referanse',
                'søknadsskjema_fornyingspåminnelse',
                'søknadsskjema_kvittering',
                'søknadsskjema_notifikasjon',
                'umiddelbart_betalingsvarsel',
                'utskrift_forberedt',
            ],
        ],
        'google' => [
            'recaptcha3' => [
                'site_key' => '',
                'secret_key' => ''
            ]
        ],
        /**
         * @see \Monolog\Logger
         */
        'logger' => [
            'level' => 200,
        ],
        'nets' => [
            'host' => '185.96.138.21',
            'port' => 10026,
            'rsa_key' => '~/.ssh/id_rsa',
            'key_pw' => '',
            'user' => [
                'ocr' => '',
                'einvoice' => '',
            ],
            'efaktura_agreement_api' => [
                'test' => [
                    'endpoint_url' => 'https://mtf.payments.mastercard.no/efaktura/b2c-online-agreement/v3/',
                    'brukernavn' => '',
                    'passord' => '',
                    'certificate' => [
                        'location' => '',
                        'private_key' => '',
                        'passphrase' => '',
                    ],
                    'client_ip' => null,
                ],
                'produksjon' => [
                    'endpoint_url' => 'https://payments.mastercard.no/efaktura/b2c-online-agreement/v3/',
                    'brukernavn' => '',
                    'passord' => '',
                    'certificate' => [
                        'location' => '',
                        'private_key' => '',
                        'passphrase' => '',
                    ],
                    'client_ip' => null,
                ],
            ],
        ],
    ],
    'listeners' => [],
    'psr4_roots' => [],
    'pub_dir_links' => [],
];