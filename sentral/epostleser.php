#!/usr/bin/php
<?php


use Kyegil\Leiebasen\Leiebase;

const LEGAL = true;
    set_error_handler(function(int $errno, string $errstr, $errfile, $errline){
        $error_reporting = error_reporting();
        if ($error_reporting & $errno) {
            throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
        }
        return false;
    });
    set_exception_handler(function (Throwable $e):void {
    error_log(substr(addslashes($e), 0, 1024) . "\n", 3, '../var/log/Error.log');
        $developerEmail = \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['debug']['developer_email'] ?? null;
        if ($developerEmail) {
            error_log($e, 1, $developerEmail);
        }
    });

    require_once('../klasser/index.php');
    Leiebase::setConfig(include('../config.php'));

    $oppslagsbestyrer = new \Kyegil\Leiebasen\Oppslagsbestyrer(__FILE__);
    foreach($oppslagsbestyrer->hentInkluderinger() as $fil) {
        include_once($fil);
    }
    /** @var \Kyegil\Leiebasen\Oppslag\SentralKontroller $leiebase */
    $leiebase = $oppslagsbestyrer->hentOppslag();
    $mysqliConnection = $leiebase->mysqli;

    $fp = fopen( "php://stdin", "r" );

    $epost = "";

    while ( !feof( $fp ) )
    {
        $epost .= fread( $fp, 1024 );
    }

    if(!class_exists('MimeMessage')) {
        // Bounce
        die("Denne eposten kunne ikke leses av leiebasen. Logg inn for å legge inn melding.");
    }
    else {
        $epostinnhold = new MimeMessage("var", $epost);

        $body = $epostinnhold->extract_body(MAILPARSE_EXTRACT_RETURN);
        $headers = isset($epostinnhold->data['headers']) ? $epostinnhold->data['headers'] : [];

        $emne = isset($headers['subject']) ? $headers['subject'] : '';
        $avsender = isset($headers['from']) ? $headers['from'] : '';
        $avsenderadresse = mailparse_rfc822_parse_addresses($avsender);
        $avsenderadresse = isset($avsenderadresse[0]['address']) ? $avsenderadresse[0]['address'] : '';
        if(isset($headers['reply-to'])) {
            $svaradresse = mailparse_rfc822_parse_addresses($headers['reply-to']);
        }
        $svaradresse = isset($svaradresse[0]['address']) ? $svaradresse[0]['address'] : '';
        $spam = isset($headers['x-spam-flag']) && ($headers['x-spam-flag'] == 'YES') ? true : false;

        /** @var \Person $avsender */
        $avsender = $leiebase->hentPersonFraEpost($avsenderadresse) ?: $leiebase->hentPersonFraEpost($svaradresse);
        if(!$avsender) {
            die("Denne eposten er sendt fra en epostadresse som ikke er registrert. Forsøk på nytt fra en annen epost-adresse eller logg inn for å legge igjen melding.");
        }
        preg_match('/\[([^]]*)] *(.*)/', $emne, $elementer);
        $saksref = isset($elementer[1]) ? $elementer[1] : '';
        $emne = isset($elementer[2]) ? $elementer[2] : $emne;

        $privat = stripos($emne, 'privat') !== false ? true : false;
        $saksopplysning = stripos($emne, 'saksopplysning') !== false ? true : false;

        /** @var \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd $sak */
        $sak = $leiebase->hentBeboerstøtteSakFraRef($saksref);
        if(!$sak) {
            $saksref = explode('-', $saksref);
            $sakstype = $saksref[0];
            if($sakstype == 'skademelding') {
                // Opprett ny skademelding;
                die("Det er for øyeblikket ikke mulig å legge inn nye skademeldinger fra epost. Logg inn for å registrere skademeldinger.");
            }
            die("Denne eposten inneholder feil. Forsøk på nytt eller logg inn for å legge igjen melding");
        }
        else {
            $innlegg = $sak->leggTilInnlegg((object)[
                'avsender_id' => $avsender->hentId(),
                'innhold' => nl2br($body),
                'privat' => ($privat && $sak->privatPerson),
                'saksopplysning' => $saksopplysning
            ]);

            if(!$innlegg->erAvsenderAdmin()) {
                $sak->abonner($avsender->hentId());
            }
        }

        $privat = (bool)stripos($emne, 'privat');
        $saksopplysning = (bool)stripos($emne, 'saksopplysning');

        if(isset($elementer[1])) {

        }
    }

    fclose( $fp );

    // echo $message;


    // #!/usr/local/bin/php does not work
    // #!/usr/bin/php works


    $methods =     [
        0 => 'mimemessage',
        1 => 'get_child',
        2 => 'get_child_count',
        3 => 'get_parent',
        4 => 'extract_headers',
        5 => 'extract_body',
        6 => 'enum_uue',
        7 => 'extract_uue',
        8 => 'remove',
        9 => 'add_child',
    ];