#! /bin/bash

echo "Hva er url-nøkkel (underscore)?"
read underscore

echo "Hva er kontroller-fornavn (CamelCase)?"
read camelcase

echo "Hva er modellnavn?"
read modell

# _MalSkjema kontroller
cp oppslag/Drift/_MalSkjema.php oppslag/Drift/"$camelcase"Skjema.php
sed -i 's/_Mal/"$camelcase"/g' oppslag/Drift/"$camelcase"Skjema.php
sed -i 's/_mal/"$underscore"/g' oppslag/Drift/"$camelcase"Skjema.php
sed -i 's/_mal/"$underscore"/g' oppslag/Drift/"$camelcase"Skjema.php

# html visningsklasse
cp --recursive klasser/Visning/drift/html/_mal_skjema/ klasser/Visning/drift/html/"$underscore"_skjema
sed -i 's/_Mal/"$camelcase"/g' klasser/Visning/drift/html/"$underscore"_skjema/Index.php
sed -i 's/_mal/"$underscore"/g' klasser/Visning/drift/html/"$underscore"_skjema/Index.php

# html visningsfil
cp --recursive visningsfiler/drift/html/_mal_skjema/ visningsfiler/drift/html/"$underscore"_skjema
sed -i 's/_Mal/"$camelcase"/g' visningsfiler/drift/html/"$underscore"_skjema/Index.html.php
sed -i 's/_mal/"$underscore"/g' visningsfiler/drift/html/"$underscore"_skjema/Index.html.php

# js visningsklasse
cp --recursive klasser/Visning/drift/js/_mal_skjema/ klasser/Visning/drift/js/"$underscore"_skjema
sed -i 's/_Mal/"$camelcase"/g' klasser/Visning/drift/js/"$underscore"_skjema/Index.js.php
sed -i 's/_mal/"$underscore"/g' klasser/Visning/drift/js/"$underscore"_skjema/Index.js.php

# js visningsfil
cp --recursive visningsfiler/drift/js/_mal_skjema/ visningsfiler/drift/js/"$underscore"_skjema
sed -i 's/_Mal/"$camelcase"/g' visningsfiler/drift/js/"$underscore"_skjema/Index.js.php
sed -i 's/_mal/"$underscore"/g' visningsfiler/drift/js/"$underscore"_skjema/Index.js.php
