<?php



/**
 * @var string $saksref
 * @var string $innhold
 */

?>
<div>Det har kommet nye oppdateringer i sak [<?= $saksref;?>]. Du mottar denne eposten fordi du abonnerer på oppdateringer i saken.</div>
<div>Husk å beholde emnefeltet med saksnummeret dersom du svarer på denne eposten.</div>
<div><?= $innhold;?></div>