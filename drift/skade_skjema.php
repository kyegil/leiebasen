<?php


if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    public $ext_bibliotek = 'ext-3.4.0';

    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        if(!$id = (int)$_GET['id'] && $_GET['id'] != '*') {
            die("Ugyldig oppslag: ID ikke angitt for skademelding");
        }
        $this->hoveddata['sql'] = "SELECT id, CONCAT(bygning, IF(leieobjektnr, CONCAT('-', leieobjektnr), '')) AS leieobjektnr, registrerer, registrert, skade, beskrivelse, utført, sluttregistrerer, sluttrapport FROM skader WHERE id = '$id'";
    }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        $standardobjekt = "";
        if( isset($_GET['leieobjektnr'] ) ) {
            $standardobjekt = $this->mysqli->arrayData(array(
                'source'    => "leieobjekter",
                'where'        => "leieobjektnr = {$this->GET['leieobjektnr']}",
                'fields'    => "CONCAT(bygning, '-', leieobjektnr) AS standardobjekt"
            ));
            if( $standardobjekt->totalRows ) {
                $standardobjekt = $standardobjekt->data[0]->standardobjekt;
            }
            else {
                $standardobjekt = "";
            }
        }
?>

Ext.onReady(function() {
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';

    var leieobjektliste = new Ext.data.JsonStore({
        fields: [
            {name: 'id',        type: 'text', mapping: 'id'},
            {name: 'visning',    type: 'text', mapping: 'visning'}
        ],
        root: 'data',
        sortInfo: {
            field: 'id',
            direction: 'ASC'
        },
        url: "/drift/index.php?oppslag=skade_skjema&id=<?=$_GET['id']?>&oppdrag=hentdata&data=leieobjekter"
    });

<?php
        $kategorirad = array();
        $sql =    "SELECT kategorier.kategori, skadekategorier.kategori AS valgt\n"
            .    "FROM (\n"
            .    "SELECT kategori\n"
            .    "FROM skadekategorier\n"
            .    "GROUP BY kategori"
            .    ") AS kategorier LEFT JOIN skadekategorier ON skadekategorier.kategori = kategorier.kategori AND skadekategorier.skadeid = " . (int)$_GET['id'];
        $kategorier = $this->arrayData($sql);

        foreach($kategorier['data'] as $linje=>$kategori){
?>
    var kategori_<?=addslashes(str_replace(' ', '_', $kategori['kategori']))?> = new Ext.form.Checkbox({
        boxLabel: '<?=addslashes(ucfirst(str_replace(' ', '_', $kategori['kategori'])))?>',
        name: 'kategori_<?=addslashes(str_replace(' ', '_', $kategori['kategori']))?>',
        inputValue: 1,
        checked: <?= $kategori['valgt'] ? "true" : "false"?>
    });

<?php
            $kategorirad[] = "kategori_" . addslashes(str_replace(' ', '_', $kategori['kategori']));
        }
?>
    var leieobjektnr = new Ext.form.ComboBox({
        name: 'leieobjektnr',
        displayField: 'visning',
        hiddenName: 'leieobjektnr',
        valueField: 'id',
        allowBlank: true,
        fieldLabel: 'Bygning / bolig',
        forceSelection: true,
        maxHeight: 600,
        minChars: 1,
        mode: 'remote',
        listWidth: 500,
        selectOnFocus: true,
        store: leieobjektliste,
        triggerAction: 'all',
        typeAhead: true,
        width: 400
    });

    var skade = new Ext.form.TextField({
        fieldLabel: 'Skade',
        name: 'skade',
        width: 300
    });

    var nykategori = new Ext.form.TextField({
        fieldLabel: 'Ny kategori',
        stripCharsRe: / /,
        name: 'nykategori',
        width: 200
    });

    var beskrivelse = new Ext.form.HtmlEditor({
        fieldLabel: 'Beskrivelse',
        name: 'beskrivelse',
        height: 300,
        width: 700
    });

    var skjema = new Ext.FormPanel({
        autoScroll: true,
        bodyStyle:'padding:5px 5px 0',
        buttons: [],
        frame:true,
        height: 600,
        items: [
            leieobjektnr,
            skade,
            {    xtype: 'checkboxgroup',
                fieldLabel: 'Eksisterende kategori',
                columns: 6,
                itemCls: 'x-check-group-alt',
                // Put all controls in a single column with width 100%
                //        columns: 1,
                items: [<?=implode(", ", $kategorirad)?>]
            },
            nykategori,
            beskrivelse
        ],
        labelAlign: 'left', // evt right
        reader: new Ext.data.JsonReader({
            defaultType: 'textfield',
            fields: [
                leieobjektnr,
                skade,
                beskrivelse
        ],
            root: 'data'
        }),
        standardSubmit: false,
        title: 'Skaderegistrering'
    });

    skjema.addButton('Avbryt', function(){
        window.location = "<?=$this->returi->get();?>";
    });
    
    var lagreknapp = skjema.addButton({
        text: 'Lagre',
        disabled: true,
        handler: function(){
            skjema.form.submit({
                url:'/drift/index.php?oppslag=<?="{$_GET['oppslag']}&id={$_GET["id"]}";?>&oppdrag=taimotskjema',
                waitMsg:'Prøver å lagre...'
                });
        }
    });

    leieobjektliste.load({
        callback: function( records, options, success ) {
            if(<?= isset($_GET['leieobjektnr']) ? "true" : "false" ?>) {
                leieobjektnr.setValue('<?= $standardobjekt ?>');
            }
        }
    });

    skjema.render('panel');

<?php if($_GET['id'] != '*'):?>
    leieobjektliste.on({
        load: function() {
            skjema.getForm().load({
                url: '/drift/index.php?oppslag=<?="{$_GET['oppslag']}&id={$_GET["id"]}";?>&oppdrag=hentdata',
                waitMsg:'Henter opplysninger...'
            });
        }
    });
<?php else:?>
    lagreknapp.enable();
<?php endif;?>

    skjema.on({
        actioncomplete: function(form, action){
            if(action.type == 'load'){
                lagreknapp.enable();
            } 
            
            if(action.type == 'submit'){
                if(action.response.responseText == '') {
                    Ext.MessageBox.alert('Problem', 'Mottok ikke bekreftelsesmelding fra tjeneren  i JSON-format som forventet');
                } else {
                    window.location = "/drift/index.php?oppslag=skadeliste<?=isset($_GET['leieobjektnr']) ? "&id={$_GET['leieobjektnr']}" : ""?>";
                    Ext.MessageBox.alert('Suksess', 'Opplysningene er oppdatert');
                }
            }
        },
                            
        actionfailed: function(form,action){
            if(action.type == 'load') {
                if (action.failureType == "connect") { 
                    Ext.MessageBox.alert('Problem:', 'Klarte ikke laste data. Fikk ikke kontakt med tjeneren.');
                }
                else {
                    if (action.response.responseText == '') {
                        Ext.MessageBox.alert('Problem:', 'Skjemaet mottok ikke data i JSON-format som forventet');
                    }
                    else {
                        var result = Ext.decode(action.response.responseText);
                        if(result && result.msg) {            
                            Ext.MessageBox.alert('Mottatt tilbakemelding om feil:', result.msg);
                        }
                        else {
                            Ext.MessageBox.alert('Problem:', 'Innhenting av data mislyktes av ukjent grunn. (trolig manglende success-parameter i den returnerte datapakken). Action type='+action.type+', failure type='+action.failureType);
                        }
                    }
                }
            }
            if(action.type == 'submit') {
                if (action.failureType == "connect") {
                    Ext.MessageBox.alert('Problem:', 'Klarte ikke lagre data. Fikk ikke kontakt med tjeneren.');
                }
                else {    
                    var result = Ext.decode(action.response.responseText); 
                    if(result && result.msg) {            
                        Ext.MessageBox.alert('Mottatt tilbakemelding om feil:', result.msg);
                    }
                    else {
                        Ext.MessageBox.alert('Problem:', 'Lagring av data mislyktes av ukjent grunn. Action type='+action.type+', failure type='+action.failureType);
                    }
                }
            }
            
        } // end actionfailed listener
    }); // end skjema.on

});
<?php
        return ob_get_clean();
    }

    /**
     *
     */
    public function design() {
?>
<div id="panel" class="extjs-panel"></div>
<?php
    }

    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ""):string {
        switch ($data) {
            case "leieobjekter":
                $resultat['data'] = array();
                if( isset( $_POST['query'] ) ) {
                    $filter =    "WHERE leieobjekter.navn LIKE '%" . $_POST['query'] . "%'\n"
                    .    "OR bygninger.navn LIKE '%" . $_POST['query'] . "%'\n"
                    .    "OR gateadresse LIKE '%" . $_POST['query'] . "%'\n"
                    .    "OR leieobjektnr LIKE '" . (int)$_POST['query'] . "'\n";
                }
                else {
                    $filter = "";
                }

                $sql =    "SELECT\n"
                    .    "bygninger.id, concat(bygninger.navn, \" generelt\") AS visning\n"
                    .    "FROM bygninger LEFT JOIN (\n"
                    .    "leieobjekter LEFT JOIN utdelingsorden ON leieobjekter.leieobjektnr = utdelingsorden.leieobjekt AND utdelingsorden.rute = '{$this->hentValg('utdelingsrute')}'\n"
                    .    ") ON leieobjekter.bygning = bygninger.id\n"
                    .    $filter
                    .    "GROUP BY bygninger.id\n"
                    .    "ORDER BY MAX(utdelingsorden.plassering)\n";
                $bygninger = $this->arrayData($sql);

                foreach($bygninger['data'] as $linje => $d) {
                    $sql =    "SELECT\n"
                        .    "CONCAT(bygning, '-', leieobjektnr) AS id\n"
                        .    "FROM leieobjekter INNER JOIN bygninger ON leieobjekter.bygning = bygninger.id\n"
                        .    $filter
                        .    (isset($_POST['query']) ? "AND\n" : "WHERE\n") . "leieobjekter.bygning = '{$d['id']}'\n"
                        .    "ORDER BY leieobjektnr\n";
                    $leiligheter = $this->arrayData($sql);

                    $resultat['data'][] = $d;

                    foreach($leiligheter['data'] AS $leilighet) {
                        $leilighet['visning'] = " - - - " . $this->leieobjekt(substr(strstr($leilighet['id'], '-'), 1), true);
                        $resultat['data'][] = $leilighet;
                    }
                }

                $resultat['success'] = true;
                return json_encode($resultat);
            default:
                return json_encode($this->arrayData($this->hoveddata['sql']));
        }
    }

    /**
     * @param string $skjema
     * @return string
     */
    public function taIMot($skjema):string {
        $tp = $this->mysqli->table_prefix;
        $resultat = (object)[
            'success' => true,
            'msg' => ''
        ];
        $bygning = (int)$_POST['leieobjektnr'];
        $leieobjektnr = (substr(strstr($_POST['leieobjektnr'], '-'), 1));
        $oppdatering = !$insert = ($_GET['id'] =='*');

        try {
            $resultat = $this->mysqli->saveToDb([
                'insert' => $insert,
                'update' => $oppdatering,
                'table' => "{$tp}skader",
                'where' => ['id' => (int)$_GET['id']],
                'id' => (int)$_GET['id'],
                'fields' => [
                    'bygning' => $bygning,
                    'leieobjektnr' => $leieobjektnr,
                    'registrerer_id' => $this->bruker['id'],
                    'registrerer' => $this->bruker['navn'],
                    'skade' => $this->POST['skade'],
                    'beskrivelse' => $this->POST['beskrivelse']
                ]
            ]);
            $resultat->msg = "";

            // Lagre kategorier
            $sql = "SELECT kategori\n"
                . "FROM skadekategorier\n"
                . "GROUP BY kategori";
            $kategorier = $this->arrayData($sql);

            foreach ($kategorier['data'] as $kategori) {
                if (isset($_POST["kategori_{$kategori['kategori']}"])) {
                    $sql = "SELECT * FROM skadekategorier WHERE skadeid = {$resultat->id} AND kategori = '{$kategori['kategori']}'";
                    $alleredekategorisert = $this->arrayData($sql);
                    if (!count($alleredekategorisert['data'])) {
                        $this->mysqli->query("INSERT INTO skadekategorier SET skadeid = {$resultat->id}, kategori = '{$kategori['kategori']}'");
                    }
                } else {
                    $this->mysqli->query("DELETE FROM skadekategorier WHERE skadeid = {$resultat->id} AND kategori = '{$kategori['kategori']}'");
                }
            }
            if ($_POST['nykategori']) {
                $sql = "SELECT * FROM skadekategorier WHERE skadeid = {$resultat->id} AND kategori = '{$this->POST['nykategori']}'";
                $alleredekategorisert = $this->arrayData($sql);
                if (!count($alleredekategorisert['data'])) {
                    $this->mysqli->query("INSERT INTO skadekategorier SET skadeid = {$resultat->id}, kategori = '{$this->POST['nykategori']}'");
                }
            }

            if($oppdatering == false) {
                // Opprett støtte for nye skademeldinger
                /** @var \Kyegil\Leiebasen\Beboerstøtte\Sakstråd $sak */
                $sak = $this->opprett(\Kyegil\Leiebasen\Beboerstøtte\Sakstråd::class, [
                    'saksref' => 'skademelding-' . $resultat->id,
                    'tittel' => $this->POST['skade'],
                    'status' => 'åpen',
                    'område' => 'drift',
                    'skademelding_id' => $resultat->id
                ]);
                if($this->mysqli->saveToDb([
                    'id' => $resultat->id,
                    'update' => true,
                    'table' => $tp . 'skader',
                    'where' => [
                        'id' => $resultat->id
                    ],
                    'fields' => [
                        'beboerstøtte_id' => $sak->hentId()
                    ]
                ])->success) {
                    $resultat->beboerstøtte_id = $sak->hentId();
                }

                /** @var \Kyegil\Leiebasen\Beboerstøtte\Innlegg $innlegg */
                $innlegg = $sak->leggTilInnlegg((object)[
                    'saksopplysning' => false,
                    'privat' => false,
                    'avsluttet' => false,
                    'innhold' => $this->POST['beskrivelse'],
                    'avsender_id' => $this->bruker['id']
                ]);

                $administatorer
                    = $this->hentValg('beboerstøtte_skademeldinger_admin')
                    ? explode(',', $this->hentValg('beboerstøtte_skademeldinger_admin'))
                    : [];
                foreach ($administatorer as $administratorId) {
                    $sak->abonner($administratorId);
                }
                $sak->abonner($this->bruker['id']);
                $innlegg->sendEpost();
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = "KLarte ikke å lagre. Feilmelding:<br>" . $e->getMessage();
        }
        return json_encode($resultat);
    }
}