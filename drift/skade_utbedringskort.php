<?php


use Kyegil\Leiebasen\Leiebase;

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    public $tittel = 'Sluttrapport skade';
    public $ext_bibliotek = 'ext-4.2.1.883';

    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        if(!isset($_GET['id']) || !$_GET['id']) {
            header("Location: index.php");
        }

        $tp = $this->mysqli->table_prefix;
        $this->hoveddata['skade']    = $this->mysqli->arrayData([
            'source'    => "{$tp}skader AS skader",
            'where'     =>  ['skader.id' => $_GET['id']]
        ]);
        if($this->hoveddata['skade']->totalRows) {
            $this->hoveddata['skade'] = reset($this->hoveddata['skade']->data);
        }
        $this->tittel = "Utbedringsrapport skade {$this->hoveddata['skade']->id}: {$this->hoveddata['skade']->skade}";

    }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        $tp = $this->mysqli->table_prefix;
        if(@$_GET['returi'] == "default") {
            $this->returi->reset();
        }
        if(isset($this->hoveddata['skade']->totalRows)) {
            echo "\nwindow.location = '{$this->returi->get()}';\n";
        }

?>

    Ext.Loader.setConfig({
        enabled: true
    });
    Ext.Loader.setPath('Ext.ux', '<?php echo $this->http_host . '/pub/lib/' . $this->ext_bibliotek . "/examples/ux"?>');

    Ext.require([
        'Ext.data.*',
        'Ext.layout.container.Border',
        'Ext.grid.*',
        'Ext.grid.plugin.BufferedRenderer',
        'Ext.ux.RowExpander'
    ]);

    Ext.onReady(function() {

        Ext.tip.QuickTipManager.init();
        Ext.form.Field.prototype.msgTarget = 'side';
        Ext.Loader.setConfig({
            enabled: true
        });

        var panel = Ext.create('Ext.panel.Panel', {
            autoLoad: '/drift/index.php?oppslag=<?=$_GET['oppslag']?>&oppdrag=hentdata&id=<?php echo $this->hoveddata['skade']->id;?>',
            autoScroll: true,
            renderTo: 'panel',
            bodyStyle: 'padding:5px',
            title: '<?=addslashes($this->hoveddata['skade']->skade)?>',
            frame: true,
            height: 600,
            buttons: [{
                handler: function() {
                    window.location = '<?=$this->returi->get();?>';
                },
                text: 'Tilbake'
            }]
        });
    });
    <?php
        return ob_get_clean();
    }

    /**
     *
     */
    public function design() {
    ?>
    <div id="panel" class="extjs-panel"></div>
    <?php
    }


    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ""):string {
        switch ($data) {
            default: {
                ob_start();
                $tp = $this->mysqli->table_prefix;
                $kategorirad = array();
                $kategorier = $this->mysqli->arrayData([
                    'source' => "{$tp}skadekategorier as skadekategorier",
                    'fields' => ['kategori'],
                    'where' => ['skadeid' => $this->hoveddata['skade']->id],
                    'groupfields' => 'kategori'
                ]);
                foreach($kategorier->data as $kategori){
                    $kategorirad[] = $kategori->kategori;
                }

                ?>
                <h1><?php echo $this->hoveddata['skade']->skade;?></h1>Registrert av <?php echo $this->hoveddata['skade']->registrerer . " " . date('d.m.Y', strtotime($this->hoveddata['skade']->registrert));?>:<br>
                <p>
                <?php if($kategorirad):?>
                <?php echo count($kategorirad) > 1 ? "Kategorier:" : "Kategori:";?>
                <b>
                <?php echo Leiebase::liste($kategorirad);?>
                </b>
                <?php endif;?>
                </p>
                <div>
                <?php echo $this->hoveddata['skade']->sluttrapport;?>
                </div>

                <?php
                return ob_get_clean();
            }
        }
    }
}