<?php

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    /**
     * @var string
     */
    public $tittel = 'leiebasen';

    /**
     * @var string
     */
    public $ext_bibliotek = 'ext-4.2.1.883';
    

    /**
     * oppsett constructor.
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
    }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        if (isset($_GET['returi']) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }
        $tp = $this->mysqli->table_prefix;
?>
Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', '<?=$this->http_host . '/pub/lib/' . $this->ext_bibliotek . "/examples/ux"?>');

Ext.require([
     'Ext.data.*',
     'Ext.form.field.*',
    'Ext.layout.container.Border',
     'Ext.grid.*',
    'Ext.ux.RowExpander'
]);

Ext.onReady(function() {

    Ext.tip.QuickTipManager.init();
    Ext.form.Field.prototype.msgTarget = 'side';
    Ext.Loader.setConfig({enabled:true});
    
    Ext.define('Fbo', {
        extend: 'Ext.data.Model',
        idProperty: 'id',
        fields: [ // http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.Field
            {name: 'id', type: 'int'},
            {name: 'leieforhold', type: 'int'},
            {name: 'leieforholdbeskrivelse', type: 'string'},
            {name: 'type', type: 'int'},
            {name: 'typebeskrivelse', type: 'string'},
            {name: 'varsel', type: 'boolean'},
            {name: 'registrert', type: 'date', dateFormat: 'Y-m-d H:i:s'}
        ]
    });
    
    var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
        clicksToEdit: 1
    });
    
    var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
        autoCancel: false,
        listeners: {
            beforeedit: function (grid, e, eOpts) {
                return e.column.xtype !== 'actioncolumn';
            }
        }
    });


    var datasett = Ext.create('Ext.data.Store', {
        model: 'Fbo',
        pageSize: 200,
        remoteSort: true,
        proxy: {
            type: 'ajax',
            simpleSortMode: true,
            url: "/drift/index.php?oppslag=fboliste&oppdrag=hentdata",
            reader: {
                type: 'json',
                root: 'data',
                actionMethods: { // Feil plassering? actionMethods tilhører proxy..
                    read: 'POST'
                },
                totalProperty: 'totalRows'
            }
        },
        sorters: [{
            property: 'id',
            direction: 'DESC'
        }],
//        groupField: 'navn',
        autoLoad: true
    });
    


    var søkefelt = Ext.create('Ext.form.field.Text', {
        emptyText: 'Søk og klikk ↵',
        name: 'søkefelt',
        width: 200,
        listeners: {
            specialkey: function( felt, e, eOpts ) {
                datasett.getProxy().extraParams = {
                    søkefelt: søkefelt.getValue()
                };
                datasett.load({
                    params: {
                        start: 0,
                        limit: 300
                    }
                });
            }
        }
    });


    var rutenett = Ext.create('Ext.grid.Panel', {
        autoScroll: true,
        layout: 'border',
//        frame: false,
//         plugins: [{
//             ptype: 'rowexpander',
//             rowBodyTpl : ['{html}']
//         }],
        store: datasett,
        title: 'Registrerte faste betalingsoppdrag (AvtaleGiro-avtaler)',
        listeners: {
            cellclick: function( panel, td, cellIndex, record, tr, rowIndex, e, eOpts ) {
                window.location = "/drift/index.php?oppslag=leieforholdkort&id=" + record.get('leieforhold');
            }
        },
        tbar: [
            søkefelt
        ],
        columns: [
            {
                dataIndex: 'id',
                text: 'ID',
                hidden: true,
                width: 50,
                sortable: true
            },
            {
                dataIndex: 'leieforhold',
                text: 'Leieforhold',
                width: 200,
                renderer: function(value, metadata, record, rowIndex, colIndex, store){
                    return record.get('leieforholdbeskrivelse');
                },
                flex: 1,
                sortable: true
            },
            {
                dataIndex: 'type',
                text: 'Betalingstype',
                width: 80,
                renderer: function(value, metadata, record, rowIndex, colIndex, store){
                    return record.get('typebeskrivelse');
                },
                sortable: true
            },
            {
                dataIndex: 'varsel',
                text: 'Skal varsles',
                width: 90,
                align: 'center',
                renderer: Ext.util.Format.hake
            },
            {
                dataIndex: 'registrert',
                text: 'Registrert',
                width: 120,
                align: 'right',
                renderer: Ext.util.Format.dateRenderer('d.m.Y H:i:s')
            }
        ],
        renderTo: 'panel',
        height: 600,
        buttons: [{
            text: 'Tilbake',
            handler: function() {
                window.location = '<?=$this->returi->get();?>';
            }
        }, {
            text: 'Vis trekkoppdrag',
            handler: function() {
                window.location = '/drift/index.php?oppslag=fbo-kravliste';
            }
        }]
    });


});
<?php
        return ob_get_clean();
    }

    /**
     *
     */
    public function design() {
?>
<div id="panel" class="extjs-panel"></div>
<?php
    }

    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ""):string {
        $tp = $this->mysqli->table_prefix;
        switch ($data) {
            default:
                $søkefelt = @$this->GET['søkefelt'];
                $filter = "fbo.leieforhold LIKE '%{$søkefelt}%' OR kontraktpersoner.leietaker LIKE '%{$søkefelt}%' OR CONCAT(personer.fornavn, ' ', personer.etternavn) LIKE '%{$søkefelt}%'";

                $resultat = $this->mysqli->arrayData(array(
                    'distinct'    => true,
                    'returnQuery' => true,
                    'fields'    => "fbo.*",
                    'source' => "{$tp}fbo AS fbo
                                INNER JOIN {$tp}kontrakter AS kontrakter
                                ON fbo.leieforhold = kontrakter.leieforhold
                                LEFT JOIN {$tp}kontraktpersoner AS kontraktpersoner
                                ON kontrakter.kontraktnr = kontraktpersoner.kontrakt
                                LEFT JOIN {$tp}personer AS personer
                                ON kontraktpersoner.person = personer.personid
                                ",
                    'where'    => $filter,
                    'orderfields' => ($_GET['sort'] ? "fbo.{$this->GET['sort']} {$this->GET['dir']}, " : "") . "fbo.id DESC"
                ));
                foreach($resultat->data as $fbo) {
                    /** @var \Leieforhold|null $leieforhold */
                    $leieforhold = $this->hent(\Leieforhold::class, $fbo->leieforhold);
                    /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforholdModell */
                    $leieforholdModell = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold::class, $fbo->leieforhold);
                    $fbo->leieforholdbeskrivelse = "<a title=\"Åpne\" href=\"/drift/index.php?oppslag=leieforholdkort&id={$leieforholdModell}\">{$leieforholdModell}</a>: " . $leieforholdModell->hentBeskrivelse();
                    $fbo->typebeskrivelse = '';
                    if($fbo->type == 1) {
                        $fbo->typebeskrivelse = "Husleie";
                    }
                    if($fbo->type == 2) {
                        $fbo->typebeskrivelse = "Fellesstrøm";
                    }
                }
                return json_encode($resultat);
        }
    }

    /**
     * @param string $skjema
     * @return string
     */
    public function taIMot($skjema):string {
        switch ($skjema) {
            default:
                return json_encode(null);
        }
    }

    /**
     * @param $oppgave
     */
    public function oppgave($oppgave) {
        switch ($oppgave) {
            default:
                return json_encode(null);
        }
    }
}