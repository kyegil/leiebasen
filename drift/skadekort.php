<?php


use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd;
use Kyegil\Leiebasen\Modell\Leieobjekt\Skade;

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    /**
     * @var string
     */
    public $tittel = 'Rapportert skade';
    /**
     * @var string
     */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @var Skade  */
    public $hoveddata;

    /**
     * oppsett constructor.
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        if(!isset($_GET['id']) || !$_GET['id']) {
            header("Location: index.php");
        }

        $id = (int)$_GET['id'];
        /** @var Skade hoveddata */
        $this->hoveddata['skade'] = $this->hentModell(Skade::class, $id);
        $this->tittel = "Innmeldt skade {$this->hoveddata['skade']->hentId()}: {$this->hoveddata['skade']->skade}";
    }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        $tp = $this->mysqli->table_prefix;
        $id = $_GET['id'];

        if(@$_GET['returi'] == "default") {
            $this->returi->reset();
        }
        if(!$this->hoveddata['skade']->hentId()) {
            echo "\nwindow.location = '{$this->returi->get()}';\n";
        }

        /** @var \Kyegil\Leiebasen\Beboerstøtte\Innlegg[] $saksopplysninger */
        $saksopplysninger = [];
        if($this->hoveddata['skade']->beboerstøtte) {
            /** @var Sakstråd $sakstråd */
            $sakstråd = $this->hoveddata['skade']->beboerstøtte;
            $saksopplysninger = $sakstråd->hentSaksopplysninger();
        }

        $saksopplysningsposisjoner = [
            [3, -180],
            [263, -180],
            [523, -180],
            [3, -340],
            [263, -340],
            [523, -340]
        ];
    ?>

    Ext.Loader.setConfig({
        enabled: true
    });
    Ext.Loader.setPath('Ext.ux', '<?php echo $this->http_host . '/pub/lib/' . $this->ext_bibliotek . "/examples/ux"?>');

    Ext.require([
        'Ext.data.*',
        'Ext.layout.container.Border',
        'Ext.grid.*',
        'Ext.grid.plugin.BufferedRenderer',
        'Ext.ux.RowExpander'
    ]);

    Ext.onReady(function() {

        Ext.tip.QuickTipManager.init();
        Ext.form.Field.prototype.msgTarget = 'side';
        Ext.Loader.setConfig({
            enabled: true
        });

        Ext.define('BeboerstøtteInnlegg', {
            extend: 'Ext.data.Model',
            idProperty: 'id',
            fields: [ /* http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.Field */
                {name: 'id', type: 'int'},
                {name: 'sak_id', type: 'int'},
                {name: 'avsender_id', type: 'int'},
                {name: 'avsender', type: 'string'},
                {name: 'innhold', type: 'string'},
                {name: 'tidspunkt', type: 'date', dateFormat: 'Y-m-d H:i:s'},
                {name: 'vedlegg', type: 'string'}
            ]
        });

        var visSaksopplysninger = function() {
            <?php $nr = 0; $x = 3; $y = -180;?>
            <?php foreach($saksopplysninger as $saksopplysning):?>
            <?php if(isset($saksopplysningsposisjoner[$nr])):
                $x = $saksopplysningsposisjoner[$nr][0];
                $y = $saksopplysningsposisjoner[$nr][1]; endif;?>

            saksopplysning<?php echo $saksopplysning->id;?>.show().anchorTo(panel, 'bl', [<?php echo $x;?>, <?php echo $y;?>], true);
                <?php $nr++;?>
            <?php endforeach;?>
            <?php unset($nr, $x, $y);?>
        }

        var utbedreknapp = Ext.create('Ext.button.Button', {
            text: 'Meld skaden som utbedret',
            handler: function() {
                window.location = "/drift/index.php?oppslag=skade_utbedring_skjema&id=<?=(int)$_GET['id'];?>";
            },
            hidden: <?= $this->hoveddata['skade']->utført ? 'true' : 'false'?>
        });

        var visUtbedring = Ext.create('Ext.button.Button', {
            text: 'Vis utbedring',
            handler: function() {
                window.location = "/drift/index.php?oppslag=skade_utbedringskort&id=<?=(int)$_GET['id'];?>";
            },
            hidden: <?= $this->hoveddata['skade']->utført ? 'false' : 'true'?>
        });


        var støttetrådData = Ext.create('Ext.data.Store', {
            model: 'BeboerstøtteInnlegg',
            pageSize: 200,
            remoteSort: true,
            proxy: {
                type: 'ajax',
                simpleSortMode: true,
                actionMethods: {
                    read: 'GET'
                },
                url:'/drift/index.php?oppslag=<?=$_GET['oppslag']?>&oppdrag=hentdata&data=beboerstøtte<?=isset($_GET['id']) ? "&id={$_GET['id']}" : ""?>',
                reader: {
                    type: 'json',
                    root: 'data',
                    totalProperty: 'totalRows'
                }
            },
            sorters: [{
                property: 'id',
                direction: 'DESC'
            }],
//            groupField: 'navn',
            autoLoad: {
                start: 0,
                limit: 300
            }
        });


        <?php foreach($saksopplysninger as $saksopplysning):?>
            var saksopplysning<?php echo $saksopplysning->id;?> = Ext.create('Ext.window.Window', {
                title: 'Saksopplysning',
                headerPosition: 'top',
                collapsible: true,
                style: {
                    'background-color': '#FFBF00'
                },
                closable: false,
                header: {
                    style: {
                        'background-color': '#FFBF00'
                    }
                },
                height: 150,
                width: 250,
                border: false,
                layout: 'fit',
                items: [{
                    xtype: 'panel',
                    autoScroll: true,
                    border: false,
                    bodyStyle: {
                        'background-color': '#FFCF10'
                    },
                    html: <?php echo json_encode($saksopplysning->innhold);?>
                }]
            });
        <?php endforeach;?>



        var skaderapport = Ext.create('Ext.panel.Panel', {
            autoLoad: {
                url: '/drift/index.php?oppslag=skadekort&oppdrag=hentdata&id=<?php echo $this->hoveddata['skade']->id;?>',
                success: function(){
                    visSaksopplysninger();
                }
            },
            autoScroll: true,
            bodyStyle: 'padding:5px',
            frame: false,
            region: 'center'
        });

        var støttetrådPanel = Ext.create('Ext.grid.Panel', {
            store: støttetrådData,
            columns: [
                {
                    dataIndex: 'tidspunkt',
                    header: 'Tidspunkt',
                    renderer: Ext.util.Format.dateRenderer('d.m.Y H:i:s'),
                    sortable: false,
                    width: 120
                },
                {
                    dataIndex: 'avsender',
                    header: 'Avsender',
                    sortable: false,
                    width: 100
                }
            ],

            plugins: [
                {
                    ptype: 'rowexpander',
                    rowBodyTpl : new Ext.XTemplate(
                        '<div><b>{avsender} {tidspunkt:this.datoFormat}</b></div>',
                        '<div>{innhold}</p></div>',
                        {
                            datoFormat: function(v){
                                return Ext.util.Format.date(v,'d.m.Y H:i:s');
                            }
                        })
                }
            ],

            autoScroll: true,
            bodyStyle: 'padding:5px',
            frame: false,
            region: 'east',
            collapsible: true,
            collapsed: false,
            width: 350,
            title: 'Kommunikasjon'
        });

        var responsSkjema = Ext.create('Ext.form.Panel', {
            title: 'Skriv respons',
            region: 'south',
//            height: 200,
            autoScroll: true,
            bodyPadding: 5,
            collapsible: true,
            hidden: true,

            items: [{
                xtype: 'container',
                items: [
                    {
                        xtype: 'fieldset',
                        border: false,
                        layout:    {
                            type: 'hbox',
                            padding: '0',
                            defaultMargins: {
                                top: 0,
                                bottom: 10,
                                left: 0,
                                right: 0
                            }
                        },

                        fieldDefaults: {
                            width: 150,
                            hideLabel: true,
                            inputValue: 1,
                            uncheckedValue: 0
                        },

                        items: [
                            <?php if($this->hoveddata['skade']->beboerstøtte && $this->hoveddata['skade']->beboerstøtte->privatPerson):?>
                            {
                                xtype: 'checkbox',
                                name: 'privat',
                                boxLabel: 'Privat',
                                tabIndex: 1
                            },
                            <?php endif;?>
                            {
                                xtype: 'checkbox',
                                name: 'saksopplysning',
                                boxLabel: 'Saksopplysning',
                                tabIndex: 2
                            },
                            {
                                xtype: 'checkbox',
                                name: 'avsluttet',
                                boxLabel: 'Avslutt saken',
                                tabIndex: 4
                            },
                            {
                                xtype: 'filefield',
                                name: 'vedlegg',
                                tabIndex: 2,
                                buttonText: 'Vedlegg'
                            }
                        ]
                    },
                    {
                        xtype: 'htmleditor',
                        name: 'innhold',
                        fieldLabel: 'Melding',
                        hideLabel: true,
                        width: '100%',
                        height: 200,
                        tabIndex: 1

                    }
                ]
            }],

            buttons: [{
                text: 'Send',
                formBind: true,
                disabled: true,
                handler: function(){
                    var form = this.up('form').getForm();
                    if (form.isValid()) {
                        form.submit({
                            url:'/drift/index.php?oppslag=<?="{$_GET['oppslag']}&id={$_GET["id"]}";?>&oppdrag=taimotskjema&skjema=innlegg',
                            waitMsg:'Sender...',
                            success: function(form, action) {
                                Ext.Msg.alert('Meldinga er sendt', action.result.msg);
                                location.reload();
                            },
                            failure: function(form, action) {
                                Ext.Msg.alert('Det oppstod en feil under sending: ', action.result.msg);
                            }
                        });
                    }
                }
            }]

        });

        var panel = Ext.create('Ext.panel.Panel', {
            'layout': 'border',
            items: [
                skaderapport,
                støttetrådPanel,
                responsSkjema
            ],
            autoScroll: true,
            renderTo: 'panel',
            title: '<?=addslashes($this->hoveddata['skade']->skade);?>',
            frame: false,
            height: 600,
            buttons: [{
                handler: function() {
                    window.location = '<?=$this->returi->get();?>';
                },
                text: 'Tilbake'
            },
                {
                    handler: function() {
                        responsSkjema.show();
                        responsSkjema.expand();
                    },
                    text: 'Send respons / oppdatering'
                },
                {
                    handler: function() {
                        window.location = '/drift/index.php?oppslag=skade_skjema&id=<?=$_GET['id']?>';
                    },
                    text: 'Rediger'
                },
                utbedreknapp,
                visUtbedring
            ]
        });
    });
    <?php
        return ob_get_clean();
    }

    /**
     *
     */
    public function design() {
        echo '<div id="panel" class="extjs-panel"></div>';
    }


    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ""):string {
        $tp = $this->mysqli->table_prefix;
        switch ($data) {
            case 'beboerstøtte':
                $resultat = (object)[
                    'success' => true,
                    'data' => [],
                    'msg' => ''
                ];

                if($this->hoveddata['skade']->beboerstøtte) {
                    /** @var Sakstråd $sakstråd */
                    $sakstråd = $this->hoveddata['skade']->beboerstøtte;
                    foreach($sakstråd->innlegg as $innlegg) {
                        if(!$innlegg->saksopplysning) {
                            $resultat->data[] = [
                                'id' => $innlegg->id,
                                'avsender' => $innlegg->avsender->navn,
                                'innhold' => $innlegg->innhold,
                                'tidspunkt' => $innlegg->tidspunkt->format('Y-m-d H:i:s')
                            ];
                        }
                    }
                }

                return json_encode($resultat);

            default: {
                ob_start();
                $kategorirad = array();
                $kategorier = $this->mysqli->arrayData([
                    'source' => "{$tp}skadekategorier as skadekategorier",
                    'fields' => ['kategori'],
                    'where' => ['skadeid' => $this->hoveddata['skade']->id],
                    'groupfields' => 'kategori'
                ]);
                foreach($kategorier->data as $kategori){
                    $kategorirad[] = $kategori->kategori;
                }

    ?>
    <h1><?php echo $this->hoveddata['skade']->skade;?></h1>Registrert av <?php echo $this->hoveddata['skade']->registrerer->navn . " " . $this->hoveddata['skade']->registrert->format('d.m.Y');?>:<br>
    <p>
    <?php if($kategorirad):?>
            <?php echo count($kategorirad) > 1 ? "Kategorier:" : "Kategori:";?>
        <b>
            <?php echo Leiebase::liste($kategorirad);?>
        </b>
    <?php endif;?>
    </p>
    <div>
                <?php echo $this->hoveddata['skade']->beskrivelse;?>
    </div>

<?php
                return ob_get_clean();
            }
        }
    }

    /**
     * @param string $skjema
     * @return string
     */
    public function taIMot($skjema):string {
        $tp = $this->mysqli->table_prefix;
        $resultat = (object)[
            'success' => true,
            'msg' => ''
        ];

        switch ($skjema) {
            default:
                $saksopplysning = isset($_POST['saksopplysning']) ? (bool)$_POST['saksopplysning'] : false;
                $privat = isset($_POST['privat']) ? (bool)$_POST['privat'] : false;
                $avsluttet = isset($_POST['avsluttet']) ? (bool)$_POST['avsluttet'] : false;
                $innhold = isset($_POST['innhold']) ? $_POST['innhold'] : '';
                $vedlegg = (isset($_FILES['vedlegg']['size']) && $_FILES['vedlegg']['size']) ? $_FILES['vedlegg'] : '';

                $sti = "{$this->filarkiv}/beboerstøtte/drift/";
                if(!is_dir($sti)){
                    mkdir($sti, 0777, true);
                }

                if($innhold) {
                    try {
                        if($vedlegg) {
                            $filnavn = basename($vedlegg["name"]);
                            $finfo = finfo_open(FILEINFO_MIME_TYPE);
                            $mime = finfo_file($finfo, $vedlegg['tmp_name']);
                            finfo_close($finfo);

                            switch ($mime) {
                                case 'application/msword':
                                case 'application/pdf':
                                case 'application/vnd.oasis.opendocument.spreadsheet':
                                case 'application/vnd.oasis.opendocument.text':
                                case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                                case 'application/x-tar':
                                case 'application/zip':
                                case 'image/jpeg':
                                case 'image/png':
                                case 'text/plain':
                                    break;
                                default:
                                    throw new \Kyegil\Leiebasen\Exceptions\Feilmelding("Filer av typen {$mime} tillates desverre ikke. Vedlegget ble ikke lagret.");
                            }

                            if ($vedlegg["size"] > 1000000) {
                                throw new \Kyegil\Leiebasen\Exceptions\Feilmelding("Fila er for stor. Maks. filstørrelse er 1Mb.");
                            }
                        }

                        /** @var Sakstråd $sak */
                        $sak = $this->hoveddata['skade']->beboerstøtte;
                        if(!$sak || !$sak->hentId()) {
                            $sak = $this->nyModell(Sakstråd::class, (object)[
                                'saksref' => 'skademelding-' . $this->hoveddata['skade']->id,
                                'tittel' => $this->hoveddata['skade']->skade,
                                'status' => 'åpen',
                                'område' => 'drift',
                                'skademelding_id' => $this->hoveddata['skade']->id
                            ]);
                            $this->hoveddata['skade']->beboerstøtte_id = $this->mysqli->saveToDb([
                                'update' => true,
                                'table' => $tp . 'skader',
                                'where' => [
                                    'id' => $this->hoveddata['skade']->id
                                ],
                                'fields' => [
                                    'beboerstøtte_id' => $sak->hentId()
                                ]
                            ])->id;
                        }

                        /** @var \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd\Innlegg $innlegg */
                        $innlegg = $sak->leggTilInnlegg((object)[
                            'saksopplysning' => $saksopplysning,
                            'privat' => $privat,
                            'avsluttet' => $avsluttet,
                            'innhold' => $innhold,
                            'avsender' => $this->bruker['id']
                        ]);

                        if(!$innlegg->erAvsenderAdmin()) {
                            $sak->abonner($this->bruker['id']);
                        }


                        if($vedlegg) {
                            if (move_uploaded_file($vedlegg["tmp_name"], "{$sti}{$innlegg->hentId()}-{$filnavn}")) {
                                $innlegg->sett('vedlegg', $filnavn);
                            }
                            else {
                                $resultat->msg = "Fila kunne ikke lastes opp pga ukjent feil.";
                            }
                        }
                    } catch (Exception $e) {
                        $resultat->success = false;
                        $resultat->msg = "Det oppstod en feil under lagring: {$e->getMessage()}";
                    }

                    if($resultat->success) {
                        try {
                            $innlegg->sendEpost();
                        } catch (Exception $e) {
                            $resultat->msg = "Det oppstod en feil ved varsling per epost: {$e->getMessage()}";
                        }
                    }
                }
                else {
                    $resultat->success = false;
                    $resultat->msg = "Meldingen var tom";
                }
                break;
        }
        return json_encode($resultat);
    }
}