<?php

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    public $ext_bibliotek = 'ext-3.4.0';

    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        setlocale(LC_ALL, "nb_NO");

        $this->fra = $this->mysqli->real_escape_string($_POST['fra'] ?? ($_GET['fra'] ?? date('Y-m-d')));
        $this->til = $this->mysqli->real_escape_string($_POST['til'] ?? ($_GET['til'] ?? date('Y-m-d')));
    }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        if (isset($_GET['returi']) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }
?>
Ext.chart.Chart.CHART_URL = '/resources/charts.swf';

Ext.onReady(function() {
//     Ext.QuickTips.init();
//     Ext.form.Field.prototype.msgTarget = 'side';

    // oppretter datasettet
    var innbetalinger = new Ext.data.JsonStore({
        url:'/drift/index.php?oppslag=statistikk_innbetalinger&oppdrag=hentdata&data=innbetalinger',
        fields: [
            {name: 'måned'},
            {name: 'innbetalt', type: 'float'},
            {name: 'krav', type: 'float'}
        ],
        root: 'data'
    });

    var refresh = function(){
        innbetalinger.load({
            params: {
                fra: Ext.util.Format.date(fradato.getValue(), 'Y-m-d'),
                til: Ext.util.Format.date(tildato.getValue(), 'Y-m-d')
            }
        });
    }

    var fradato = new Ext.form.DateField({
        allowBlank: true,
        altFormats: "j.n.y|j.n.Y|j/n/y|j/n/Y|j-n-y|j-n-Y|j. M y|j. M -y|j. M. Y|j. F -y|j. F y|j. F Y|j/n|j-n|dm|dmy|dmY|d|Y-m-d",
        fieldLabel: 'Fradato',
        format: 'd.m.Y',
        name: 'fra',
        value: '<?=date('01.m.Y', $this->leggtilIntervall(time(), 'P-3Y'))?>',
        minValue: '01.07.2003',
        listeners: {
            change: refresh
        },
        width: 100
    });

    var tildato = new Ext.form.DateField({
        allowBlank: false,
        altFormats: "j.n.y|j.n.Y|j/n/y|j/n/Y|j-n-y|j-n-Y|j. M y|j. M -y|j. M. Y|j. F -y|j. F y|j. F Y|j/n|j-n|dm|dmy|dmY|d|Y-m-d",
        fieldLabel: 'Tildato',
        labelWidth: 100,
        format: 'd.m.Y',
        name: 'til',
        maxValue: '<?=date('t.m.Y', $this->leggtilIntervall(time(), 'P-1M'))?>',
        value: '<?=date('t.m.Y', $this->leggtilIntervall(time(), 'P-1M'))?>',
        listeners: {
            valid: refresh
        },
        width: 100
    });
    
    var bunnlinje = new Ext.Toolbar({
        items: [{html: '  Fra dato:  '}, fradato, {html: '  Til dato:  '}, tildato]
    });

    var panel = new Ext.Panel({
        height: 600,
        iconCls: 'chart',
        title: 'Innbetalinger (linje) mot krav (søyler)',
        frame: true,
        layout: 'fit',
        autoScroll: true,
        bbar: bunnlinje,
        items: [{
            xtype: 'linechart',
            store: innbetalinger,
            xField: 'måned',
            yField: 'innbetalt',
            yAxis: new Ext.chart.NumericAxis({
                displayName: 'Beløp',
                labelRenderer : Ext.util.Format.noMoney
            }),
            series: [{
                type: 'column',
                displayName: 'Krav',
                yField: 'krav',
                style: {
                    mode: 'stretch',
                    color:0xA40000
                }
            },{
                type:'line',
                displayName: 'Innbetalt',
                yField: 'innbetalt',
                style: {
                    color: 0x228B22
                }
            }]

        }
        ]
    });
    
    // Rutenettet rendres in i HTML-merket '<div id="panel">':
    panel.render('panel');    

});
<?php
        return ob_get_clean();
    }

    /**
     *
     */
    public function design() {
?>
<div id="panel" class="extjs-panel"></div>
<?php
    }

    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = "") {
        switch ($data) {
            case "innbetalinger":
                $sql =     $this->hoveddata['sql'] =
                    "SELECT MONTH(dato) AS måned, YEAR(dato) AS år, SUM(innbetalinger.beløp) AS innbetalt\n"
                .    "FROM innbetalinger\n"
                .    "WHERE konto != '0'\n"
                .    "AND dato >= '$this->fra'\n"
                .    "AND dato <= '$this->til'\n"
                .    "GROUP BY år, måned\n"
                .    "ORDER BY år, måned";
                $resultat = $this->arrayData($sql);
                foreach($resultat['data'] as $id=>$linje){
                    $sql =    "SELECT SUM(beløp) AS krav
                            FROM krav
                            WHERE kravdato >= '$this->fra'
                            AND kravdato <= '$this->til'
                            AND YEAR(kravdato) = '{$linje['år']}'
                            AND MONTH(kravdato) = '{$linje['måned']}'";
                    $krav = $this->arrayData($sql);
                    $resultat['data'][$id]['krav'] = $krav['data'][0]['krav'];
                    $resultat['data'][$id]['måned'] = \IntlDateFormatter::formatObject(new DateTime("{$linje['år']}-{$linje['måned']}-01"), 'MMMM y', 'nb_NO');
                }
                return json_encode($resultat);
        }
        return '';
    }
}