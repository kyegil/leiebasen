<?php /** @noinspection PhpMultipleClassDeclarationsInspection */

use Kyegil\Leiebasen\Modell\Ocr\Fil;
use Kyegil\Leiebasen\Modell\Ocr\Transaksjon;
use Kyegil\Leiebasen\Modell\Ocr\Transaksjonsett;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\Leiebasen\Oppslag\EksKontroller;
use Kyegil\Nets\Forsendelse;

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends EksKontroller {

    public $tittel = 'OCR innbetalingsoppdrag';
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * oppsett constructor.
     * @throws Exception|Throwable
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $filId = isset($_GET['id']) ? (int)$_GET['id'] : null;
        /** @var Fil $ocrFil */
        $ocrFil = $this->hentModell(Fil::class, $filId);
        $ocrFil = $ocrFil->hentId() ? $ocrFil : null;

        if(!$ocrFil) {
            header("Location: index.php?oppslag=ocr_liste");
        }
        $this->hoveddata['ocr_fil'] = $ocrFil;
    }


    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }
        /** @var Fil $ocrFil */
        $ocrFil = $this->hoveddata['ocr_fil'];

        $forsendelse = new Forsendelse( $ocrFil->ocr );

        if( !$forsendelse->valider() ){
            $html = "Klarte ikke lese fila pga. ukjent feil";
        }
        else {
            $html = addslashes("<table><tr><td><h1>OCR forsendelse nr. {$ocrFil->forsendelsesnummer} den {$forsendelse->dato->format('d.m.Y')}</h1></td>"
            .    "<td>Registrert av {$ocrFil->registrerer} den " . $ocrFil->registrert->format('d.m.Y H:i:s') . "<br>"
            .    "Antall oppdrag i forsendelsen: " . count($forsendelse->oppdrag)) . "</td></tr></table";
        }
?>

Ext.onReady(function() {
    var indrepanel = Ext.create('Ext.panel.Panel', {
        autoLoad: '/drift/index.php?oppslag=<?=$_GET["oppslag"]?>&oppdrag=hentdata&id=<?=$_GET["id"]?>',
        autoScroll: true,
        bodyStyle: 'padding: 5px;',
        title: '',
        frame: false,
        height: 400,
        plain: false
    });

    var panel = Ext.create('Ext.panel.Panel', {
        renderTo: 'panel',
        items: [
            {
                xtype: 'displayfield',
                value: '<?=$html?>'
            },
            indrepanel
        ],
        autoScroll: false,
        bodyStyle: 'padding: 5px;',
        title: '',
        frame: true,
        height: 600,
        plain: false,
        buttons: [{
            handler: function() {
                window.location = '<?=$this->returi->get();?>';                
            },
            text: 'Tilbake'
        }, {
            handler: function() {
                window.location = "/drift/index.php?oppslag=ocr_fil&id=<?=(int)$_GET['id']?>";
            },
            text: 'Last ned som OCR-fil'
        }]
    });


});
<?php
        return ob_get_clean();
    }

    public function design() {
?>
<div id="panel" class="extjs-panel"></div>
<?php
    }

    /**
     * @param string $data
     * @return string
     * @throws Exception
     */
    public function hentData($data = ""):string {
        ob_start();
        $kundeenhetID = $this->hentValg('nets_kundeenhetID');
        $avtaleId = $this->hentValg('nets_avtaleID_ocr');

        /** @var Fil $ocrFil */
        $ocrFil = $this->hoveddata['ocr_fil'];

        $forsendelse = new Forsendelse($ocrFil->ocr);

        if ($forsendelse->datamottaker != $kundeenhetID) {
            echo "Forsendelsen har feil kundeenhet-id nr. {$forsendelse->datamottaker}";
        } else {
            foreach ($forsendelse->oppdrag as $oppdrag) {
                if ($oppdrag->tjeneste != 9) {
                    echo "<div>Oppdrag {$oppdrag->oppdragsnr} tilhører tjeneste {$oppdrag->tjeneste}";
                    if ($oppdrag->tjeneste == 21) {
                        echo " (AvtaleGiro)";
                    }
                    if ($oppdrag->tjeneste == 42) {
                        echo " (eFaktura)";
                    }
                    echo "</div>";
                } else if ($oppdrag->avtaleId != $avtaleId) {
                    echo "<div>Forsendelsen har feil avtale-id: {$oppdrag->avtaleId}</div>";
                } else {
                    echo "Oppdrag nr. {$oppdrag->oppdragsnr} inneholder ";
                    echo "{$oppdrag->antallTransaksjoner} ocr-transaksjon" . ($oppdrag->antallTransaksjoner > 1 ? "er" : "") . " i Nets-avtale {$oppdrag->avtaleId} ({$this->kr($oppdrag->sumTransaksjoner)}):<br><br>";

                    echo "<table style=\"vertical-align:top; width:100%;\">";

                    /** @var Transaksjonsett $ocrTransaksjonsett */
                    $ocrTransaksjonsett = $ocrFil->hentTransaksjoner()
                        ->låsFiltre()->leggTilInnbetalingModell()
                    ;
                    $ocrTransaksjonsett
                        ->inkluder('innbetalinger')->inkluder('delbeløp');

                    /** @var Transaksjon $ocrTransaksjon */
                    foreach ($ocrTransaksjonsett as $ocrTransaksjon) {
                        $betalingModell = $ocrTransaksjon->hentInnbetaling();
                        $betalingsmåte = Transaksjon::TYPE_BESKRIVELSE[$ocrTransaksjon->transaksjonstype] ?? '';
                        $avstemtSum = 0;

                        echo "<tr style=\"vertical-align:top; padding: 5px;\">";

                        echo "<td>";
                        echo "<b>Transaksjon nr. {$ocrTransaksjon->transaksjonsnummer}</b><br>";
                        echo "Oppgjørsdato: {$ocrTransaksjon->oppgjørsdato->format('d.m.Y')}<br>";
                        echo "Delavregningsnummer: {$ocrTransaksjon->delavregningsnummer}<br>";
                        echo "Løpenummer: {$ocrTransaksjon->løpenummer}<br>";
                        echo "Beløp: {$this->kr($ocrTransaksjon->beløp)}<br>";
                        echo "Betalingsmåte: {$betalingsmåte}<br>";
                        echo "KID: <strong>{$ocrTransaksjon->kid}</strong><br>";
                        echo($ocrTransaksjon->debetkonto ? "Debetkonto: {$ocrTransaksjon->debetkonto}<br>" : "");
                        echo($ocrTransaksjon->fritekst ? "Fritekst: {$ocrTransaksjon->fritekst}<br>" : "");
                        echo($ocrTransaksjon->blankettnummer ? "Blankettnummer: {$ocrTransaksjon->blankettnummer}<br>" : "");
                        echo "Bankens arkivreferanse: {$ocrTransaksjon->arkivreferanse}<br>";
                        echo "<br>";
                        echo "</td>";

                        echo "<td>";
                        echo "<br>";
                        echo "<b>Betalingen gjelder ifølge KID {$ocrTransaksjon->kid}:</b><br>" . $this->tolkKid($ocrTransaksjon->kid) . "<br>";
                        echo "<br>";
                        echo "</td>";

                        echo "<td>";
                        echo "<br>";

                        if (!$betalingModell) {
                            echo "<div style=\"color:red;\"><strong>Denne transaksjonen har ikke blitt registrert som innbetaling i leiebasen!!</strong></div>";

                        } else {
                            echo "<strong>Faktisk behandling av betaling: </strong>"
                                . DriftKontroller::lenkeTilBetaling($betalingModell) . "<br>"
                                . "<table>";

                            foreach ($betalingModell->hentDelbeløp() as $delbeløp) {
                                $leieforhold = $delbeløp->leieforhold;
                                $krav = $delbeløp->krav;
                                echo "<tr><td>"
                                    . ($krav ? ($krav instanceof \Kyegil\Leiebasen\Modell\Leieforhold\Krav ? $krav->hentTekst() : "<i>tilbakebetalt</i>") : "<i>ikke avstemt</i>")
                                    . (
                                    $leieforhold && $krav instanceof \Kyegil\Leiebasen\Modell\Leieforhold\Krav
                                        ? (' ' . DriftKontroller::lenkeTilLeieforholdKort($leieforhold, $leieforhold->hentNavn()))
                                        : ''
                                    )
                                    . "</td><td style=\"text-align:right;\">{$this->kr($delbeløp->beløp)}</td></tr>";
                                $avstemtSum += $delbeløp->beløp;
                            }
                            echo "<tr><td><strong>Sum</strong></td><td><b" . (($avstemtSum != $ocrTransaksjon->beløp) ? " style=\"color:red;\" title=\"Det er {$this->kr(abs($avstemtSum - $ocrTransaksjon->beløp), false)} avvik i mellom transaksjonsbeløpet\nog summen av registrerte innbetalinger\"" : "") . ">{$this->kr($avstemtSum)}</b>";
                        }

                        echo "</td></tr></table>";
                        echo "</td></tr>";
                    }
                    echo "</table>";
                }
            }

        }
        return ob_get_clean();
    }
}