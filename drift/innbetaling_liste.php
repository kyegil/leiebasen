<?php

use Kyegil\Leiebasen\Leiebase;
if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    /**
     * @var string
     */
    public $tittel = 'Innbetalinger';

    /**
     * @var string
     */
    public $ext_bibliotek = 'ext-3.4.0';

    /**
     * oppsett constructor.
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
            parent::__construct($di, $config);
            $leieforhold
                = isset($_POST['leieforhold'])
                ? (int)$_POST['leieforhold']
                : false;
            $beløpsøkefelt
                = isset($_POST['beløpsøkefelt'])
                ? (float)str_replace(",", ".", $_POST['beløpsøkefelt'])
                : false;
            $søkefelt
                = isset($_POST['søkefelt'])
                ? $this->POST['søkefelt']
                : false;
            $dir
                = isset($_POST['dir'])
                ? $this->POST['dir']
                : "DESC";
            $sort
                = isset($_POST['sort'])
                ? "`{$this->POST['sort']}` {$dir}, `dato` DESC, `ocr_transaksjon`, `ref`, `betaler`\n"
                : "`dato` DESC, `ocr_transaksjon`, `ref`, `betaler`\n";

            $this->hoveddata['sql'] = "SELECT DISTINCT oppsummert.*\n"
                .    "FROM (\n"
                .    "SELECT `innbetaling` AS `id`, `dato`, SUM(`beløp`) AS `beløp`, `konto`, `betaler`, `ocr_transaksjon`, MIN(`registrert`) AS `registrert`, `ref` FROM `innbetalinger`\n"
                .    "GROUP BY `innbetaling`, `dato`, `konto`, `betaler`, `ocr_transaksjon`, `ref`\n"
                .    ") AS `oppsummert` INNER JOIN `innbetalinger`\n"
                .    "ON `oppsummert`.`dato` = `innbetalinger`.`dato` AND `oppsummert`.`konto` = `innbetalinger`.`konto` AND `oppsummert`.`betaler` = `innbetalinger`.`betaler` AND `oppsummert`.`ref` = `innbetalinger`.`ref`\n"
                .    "WHERE `innbetalinger`.`konto` != '0'\n"
                .    ($leieforhold ? ("AND `leieforhold` = '{$leieforhold}'\n") : "")
                .    (
                    $beløpsøkefelt
                    ? "AND (`oppsummert`.`beløp` = '{$beløpsøkefelt}' OR `innbetalinger`.`beløp` = '{$beløpsøkefelt}')\n"
                    : ""
                )
                .    (
                    $søkefelt
                    ? (
                        "AND (\n"
                            . (strtotime( $søkefelt )
                            ? "(
                                `oppsummert`.`dato` > date_sub('" . date('Y-m-d', strtotime($søkefelt)) . "', INTERVAL 4 DAY)
                                AND `oppsummert`.`dato` < date_add('" . date('Y-m-d', strtotime($søkefelt)) . "', INTERVAL 4 DAY)
                            )"
                            : "0"
                            )
                            . " OR `oppsummert`.`betaler` LIKE '%{$søkefelt}%'
                                OR `oppsummert`.`ref` LIKE '%{$søkefelt}%'
                                OR `innbetalinger`.`merknad` LIKE '%{$søkefelt}%'\n"
                        .")"
                    )
                    : ""
                )
                .    "\n"
                .    "ORDER BY {$sort}";
        }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        if (isset($_GET['returi']) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }
            ?>

        Ext.onReady(function() {
            Ext.QuickTips.init();
            Ext.form.Field.prototype.msgTarget = 'side';

            // oppretter datasettet
            var datasett = new Ext.data.JsonStore({
                url:'/drift/index.php?oppslag=innbetaling_liste&oppdrag=hentdata',
                fields: [
                    {name: 'id'},
                    {name: 'dato', type: 'date', dateFormat: 'Y-m-d'},
                    {name: 'beløp', type: 'float'},
                    {name: 'konto'},
                    {name: 'ocr_transaksjon'},
                    {name: 'ref'},
                    {name: 'merknad'},
                    {name: 'betaler'},
                    {name: 'registrert', type: 'date', dateFormat: 'Y-m-d H:i:s'},
                    {name: 'tekst'}
                ],
                totalProperty: 'totalRows',
                remoteSort: true,
                sortInfo: {
                    field: 'dato',
                    direction: 'DESC' // or 'ASC' (case sensitive for local sorting)
                },
                root: 'data'
            });


            function sendmelding(){
                Ext.Ajax.request({
                    waitMsg: 'Prøver å sende meldinger per epost...',
                    url: '/drift/index.php?oppslag=innbetaling_liste&oppdrag=oppgave&oppgave=sendmelding',
                    failure:function(){
                        Ext.MessageBox.alert('Mislykket...','Klarte ikke å sende meldinger om nye innbetalinger. Prøv igjen senere.');
                    },
                    success:function(response){
                        var tilbakemelding = Ext.util.JSON.decode(response.responseText);
                        if(tilbakemelding['success'] == true) {
                            Ext.MessageBox.alert('Utført', tilbakemelding.msg);
                        }
                        else {
                            Ext.MessageBox.alert('Hmm..',tilbakemelding['msg']);
                        }
                    }
                });
            }

            var leieforhold = new Ext.form.ComboBox({
                name: 'leieforhold',
                mode: 'remote',
                store: new Ext.data.JsonStore({
                    fields: [{name: 'value'},{name: 'text'}],
                    root: 'data',
                    url: '/drift/index.php?oppslag=data&oppdrag=hent_data&data=komboliste_leieforhold'
                }),
                emptyText: 'Søk på leieforhold',
                fieldLabel: 'Leieforhold',
                hideLabel: false,
                minChars: 0,
                queryDelay: 1000,
                allowBlank: true,
                displayField: 'text',
                editable: true,
                forceSelection: false,
                selectOnFocus: true,
                listWidth: 500,
                maxHeight: 600,
                typeAhead: false,
                listeners: {'select': function(){
                    datasett.baseParams = {leieforhold: leieforhold.getValue(), søkefelt: søkefelt.getValue(), beløpsøkefelt: beløpsøkefelt.getValue()};
                    datasett.load({params: {start: 0, limit: 300}});
                }},
                width: 140
            });


            var søkefelt = new Ext.form.TextField({
                fieldLabel: 'Frisøk',
                emptyText: 'Dato (dd.mm.åååå ± 3 dager), navn eller ref. Min 4 tegn',
                name: 'søkefelt',
                width: 300,
                listeners: {
                    valid: function() {
                        if( søkefelt.getValue().length > 3 ) {
                            datasett.baseParams = {
                                leieforhold: leieforhold.getValue(),
                                søkefelt: søkefelt.getValue(),
                                beløpsøkefelt: beløpsøkefelt.getValue()
                            };
                            datasett.load({
                                params: {
                                    start: 0,
                                    limit: 350
                                }
                            });
                        }
                    }
                }
            });

            var beløpsøkefelt = new Ext.form.TextField({
                fieldLabel: 'Beløp',
                emptyText: 'Søk på beløp',
                name: 'beløpsøkefelt',
                width: 100,
                listeners: {
                    'valid': function() {
                        datasett.baseParams = {
                            leieforhold: leieforhold.getValue(),
                            søkefelt: søkefelt.getValue(),
                            beløpsøkefelt: beløpsøkefelt.getValue()
                        };
                        datasett.load({params: {start: 0, limit: 300}});
                    }
                }
            });


            var søkeområde = new Ext.Panel({
                autoWidth: false,
                border: false,
                layout: 'column',
                height: 20,
                width: 1000,
                items: [{
                    columnWidth: 0.25,
                    labelAlign: 'right',
                    labelWidth: 60,
                    border: false,
                    layout: 'form',
                    items: [leieforhold]
                },{
                    columnWidth: 0.20,
                    labelAlign: 'right',
                    labelWidth: 50,
                    border: false,
                    layout: 'form',
                    items: [beløpsøkefelt]
                },{
                    columnWidth: 0.5,
                    labelAlign: 'right',
                    labelWidth: 60,
                    border: false,
                    layout: 'form',
                    items: [søkefelt]
                }]
            });


            var lastData = function(){
                datasett.load({params: {start: 0, limit: 300}});
            }

            lastData();


            var expander = new Ext.ux.grid.RowExpander({        tpl : new Ext.Template(
                    '{tekst}'
                )
            });

            var dato = {
                dataIndex: 'dato',
                header: 'Dato',
                sortable: true,
                renderer: Ext.util.Format.dateRenderer('d.m.Y'),
                width: 70
            };

            var registrert = {
                dataIndex: 'registrert',
                header: 'Registrert',
                sortable: true,
                renderer: Ext.util.Format.dateRenderer('d.m.Y'),
                width: 70
            };

            var beløp = {
                align: 'right',
                dataIndex: 'beløp',
                header: 'Beløp',
                renderer: Ext.util.Format.noMoney,
                sortable: true,
                width: 70
            };

            var konto = {
                dataIndex: 'konto',
                header: 'Konto',
                sortable: true,
                width: 90
            };

            var åpne = {
                header: '',
                sortable: false,
                renderer: function(value, metaData, record){
                    return '<a title="Klikk for å se detaljene i denne innbetalingen." href="/drift/index.php?oppslag=innbetalingskort&id=' + record.data.id + '">Åpne</a>';
                },
                width: 70
            };

            var ref = {
                dataIndex: 'ref',
                header: 'Ref',
                sortable: true,
                width: 100
            };

            var betaler = {
                dataIndex: 'betaler',
                header: 'Betalt av',
                sortable: true,
                width: 70
            };



            var bunnlinje = new Ext.PagingToolbar({
                pageSize: 300,
                store: datasett,
                displayInfo: true,
                displayMsg: 'Viser linje {0} - {1} av {2}',
                emptyMsg: "Ingen innbetalinger å vise",
                items:[
                    '-',
                    {
                        text: 'Send bekreftelser for nye innbetalinger',
                        handler: sendmelding
                    }
                ]
            });


            var rutenett = new Ext.grid.GridPanel({
                autoExpandColumn: 4,
                plugins: expander,
                autoScroll: true,
                border: false,
                stripeRows: true,
                store: datasett,
                tbar: [søkeområde],
                bbar: bunnlinje,
                columns: [
                    expander,
                    dato,
                    beløp,
                    konto,
                    betaler,
                    åpne,
                    ref,
                    registrert
                ],
                height: 600,
                viewConfig: {
                    enableRowBody: false,
                    showPreview: true,
                    getRowClass : function(record, rowIndex, p){
                        if(this.showPreview){
                            p.body = '' + record.data.tekst + '';
                            return 'x-grid3-row-expanded';
                        }
                    return 'x-grid3-row-collapsed';
                    }
                },
                title: 'Søk i innbetalinger'
            });

            // Rutenettet rendres in i HTML-merket '<div id="panel">':
            rutenett.render('panel');

        });
        <?php
        return ob_get_clean();
        }

    /**
     *
     */
    public function design() {
    ?>
    <div id="panel" class="extjs-panel"></div>
    <?php
    }

    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ""):string {
        switch ($data) {
            default:
                $start = $_POST['start'] ?? 0;
                $limit =$_POST['limit'] ?? 10;
                $resultat = $this->mysqli->arrayData(array(
                    'sql'            => $this->hoveddata['sql'],
                    'limit'            => "{$start}, {$limit}",
                    'returnQuery'    => true
                ));

                foreach( $resultat->data as $innbetaling ) {

                    $utlikninger = $this->mysqli->arrayData(array(
                        'fields'    => "innbetalinger.beløp, innbetalinger.merknad, innbetalinger.leieforhold, innbetalinger.krav, krav.tekst",
                        'source'    => "innbetalinger LEFT JOIN krav ON innbetalinger.krav = krav.id",
                        'where'        => "innbetalinger.innbetaling = '{$innbetaling->id}'"
                    ));

                    $innbetaling->tekst = "";

                    foreach($utlikninger->data as $utlikning) {
                        $innbetaling->tekst
                            .= "kr. " . number_format($utlikning->beløp, 2, ",", " ") . ": "
                            . (
                                $utlikning->krav > 0
                                ? "<a href=\"/drift/index.php?oppslag=krav_kort&id={$utlikning->krav}\">{$utlikning->tekst}</a>"
                                : ($utlikning->krav !== null ? "Tilbakebetaling" : "<i>ikke avstemt</i>")
                            ) . ", "
                            . Leiebase::liste($this->kontraktpersoner($this->sistekontrakt($utlikning->leieforhold)))
                            . " i " . $this->leieobjekt($this->kontraktobjekt($utlikning->leieforhold))
                            . (
                                $utlikning->merknad
                                ? " ({$utlikning->merknad})"
                                : ""
                            ) . "<br>";
                    }
                }
                return json_encode($resultat);
        }
    }

    /**
     * @param string $skjema
     * @return string
     */
    public function taIMot($skjema):string {
        switch ($skjema) {
            default:
                return json_encode(null);
        }
    }

    /**
     * @param $oppgave
     * @throws Exception
     */
    public function oppgave($oppgave) {
        switch($oppgave){
            default:
                return json_encode(null);
        }
    }
}