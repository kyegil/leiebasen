<?php

use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Innbetaling as InnbetalingModell;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Modell\Innbetalingsett;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan\Originalkrav;
use Kyegil\Leiebasen\Modell\Leieforhold\Depositum;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav as KravModell;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    public $tittel = 'leiebasen';
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * oppsett constructor.
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
    }



    /**	Pre HTML
     * Dersom leieforholdet ikke eksisterer vil du bli videresendt til oppslaget leieforhold_liste
     *
     * @return bool
     * @throws Exception
     */
    public function preHTML() {
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentModell(Leieforhold::class, $this->leieforhold( (int)!empty($_GET['id']) ? $_GET['id'] : null));
        if( !$leieforhold->hentId() ) {
            header("Location: index.php?oppslag=leieforhold_liste");
            return false;
        }

        else {
            $this->tittel = "Leieforhold $leieforhold: " . $leieforhold->hentNavn() . " i " . $leieforhold->leieobjekt->hentBeskrivelse() . " | Leiebasen";
            return parent::preHTML();
        }
    }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }

        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentModell(Leieforhold::class, $this->leieforhold( (int)!empty($_GET['id']) ? $_GET['id'] : null));

        $tab = $_GET['tab'] ?? null;
        switch($tab) {
            case "oppsummering":
            case "kontoforløp":
            case "krav":
            case "innbetalinger":
            case "kontooversikt":
            case "varsling":
            case "leieavtale":
            case "fellesstrøm":
            case "statistikk":
                break;
            default:
                $tab = '';
                break;
        }
    ?>

    Ext.Loader.setConfig({
        enabled: true
    });
    Ext.Loader.setPath('Ext.ux', '<?php echo $this->http_host . '/pub/lib/' . $this->ext_bibliotek . "/examples/ux"?>');

    Ext.require([
        'Ext.data.*',
        'Ext.form.field.*',
        'Ext.layout.container.Border',
        'Ext.grid.*',
        'Ext.grid.plugin.BufferedRenderer',
        'Ext.ux.RowExpander'
    ]);

    Ext.onReady(function() {

        Ext.tip.QuickTipManager.init();
        Ext.form.Field.prototype.msgTarget = 'side';
        Ext.Loader.setConfig({enabled:true});

        Ext.define('Transaksjon', {
            extend: 'Ext.data.Model',
            idProperty: 'id',
            fields: [ // http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.Field
                {name: 'id'},
                {name: 'dato',	type: 'date', dateFormat: 'Y-m-d'},
                {name: 'beløp',			type: 'float'},
                {name: 'husleie',		type: 'float'},
                {name: 'fellesstrøm',	type: 'float'},
                {name: 'annet',			type: 'float'},
                {name: 'innbetalt',	type: 'float'},
                {name: 'saldo',			type: 'float'},
                {name: 'tekst'}
            ]
        });

        Ext.define('Krav', {
            extend: 'Ext.data.Model',
            idProperty: 'id',
            fields: [ // http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.Field
                {name: 'id'},
                {name: 'dato',			type: 'date', dateFormat: 'Y-m-d'},
                {name: 'forfall',		type: 'date', dateFormat: 'Y-m-d', useNull: true},
                {name: 'giro',			type: 'int', useNull: true},
                {name: 'fom',			type: 'date', dateFormat: 'Y-m-d', useNull: true},
                {name: 'tom',			type: 'date', dateFormat: 'Y-m-d', useNull: true},
                {name: 'beløp',			type: 'float'},
                {name: 'utestående',	type: 'float'},
                {name: 'tekst'},
                {name: 'type'},
                {name: 'termin'},
                {name: 'html'}
            ]
        });

        Ext.define('Innbetaling', {
            extend: 'Ext.data.Model',
            idProperty: 'id',
            fields: [ // http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.Field
                {name: 'id'},
                {name: 'dato',				type: 'date', dateFormat: 'Y-m-d'},
                {name: 'beløp', 			type: 'float'},
                {name: 'betaler'},
                {name: 'konto'},
                {name: 'transaksjonstype',	type: 'int'},
                {name: 'transaksjonsbeskrivelse'},
                {name: 'ocr_transaksjon'},
                {name: 'ref'},
                {name: 'merknad'},
                {name: 'html'}
            ]
        });

        function velgKrav() {
            var antall = kravsett.getCount();
            var indeks = 0;
            var idag = new Date();
            while(indeks < antall && kravsett.getAt(indeks).get('dato') > idag) {
                indeks ++;
            }
            if(indeks) {
                krav.getView().bufferedRenderer.scrollTo(indeks, false);
            }
        }


        function velgTransaksjon() {
            var antall = transaksjoner.getCount();
            var indeks = 0;
            var idag = new Date();
            while(indeks < antall && transaksjoner.getAt(indeks).get('dato') > idag) {
                indeks ++;
            }
            if(indeks) {
                kontoforløp.getView().bufferedRenderer.scrollTo(indeks, false);
            }
        }


        function saldorenderer(value, metadata, record, rowIndex, colIndex, store) {
            resultat = Ext.util.Format.noMoney(value);
            nå = new Date();
            nestelinje = store.getAt(rowIndex+1);
            if(nestelinje && (nestelinje.get('dato') > nå)) {
                resultat = '<span style="font-weight: bold;">' + resultat + '</span>'
            }

            if(record.get('dato') > nå) {
                resultat = '';
            }
            if(value < 0){
                resultat = '<span style="color: red;">' + resultat + '</span>'
            }
            return resultat;
        }


        function restrenderer(value, metadata, record) {
            resultat = Ext.util.Format.noMoney(value);
            nå = new Date();
            if(value > 0 && record.get('dato') <= nå){
                resultat = '<span style="color: red;">' + resultat + '</span>'
            }
            if(record.get('dato') > nå){
                resultat = '<span style="color: grey;">' + resultat + '</span>'
            }
            if(0 == value){
                resultat = ''
            }
            return resultat;
        }

        gråfremtid = function(value, metadata, record) {
            nå = new Date();
            if(value instanceof Date){
                value = Ext.util.Format.date(value, 'd.m.Y');
            }
            if(value === null) {
                return '';
            }
            if(record.get('dato') > nå) {
                return '<span style="color: grey;">' + value + '</span>';
            }
            else
                return value;
        }


        gråfremtid.beløp = function(value, metadata, record) {
            nå = new Date();
            if(value) value = Ext.util.Format.noMoney(value);
            else value = "";
            if(record.get('dato') > nå) {
                return '<span style="color: grey;">' + value + '</span>';
            }
            else
                return value;
        }


        frysAvtale = function() {
            Ext.Ajax.request({
                waitMsg: 'Fryser...',
                url: "/drift/leieforhold_skjema/<?php echo $leieforhold;?>/oppgave&oppgave=frys",
                success : function(){
                    Ext.MessageBox.alert('Sånn', 'Leieavtalen er frosset');
                    oppsummering.getComponent('hovedoppsummering').getLoader().load();
                    varsling.getComponent('hovedvarsling').getLoader().load();
                    frysmeny.setHandler(function() {
                        tinAvtale();
                    });
                    frysmeny.setText('Tin leieforholdet');
                },
                failure : function(){
                    Ext.MessageBox.show('Arg!', 'Klarte ikke fryse leieavtalen');
                }
            });
        }


        tinAvtale = function() {
            Ext.Ajax.request({
                waitMsg: 'Gjenåpner...',
                url: "/drift/leieforhold_skjema/<?php echo $leieforhold;?>/oppgave&oppgave=tin",
                success : function(){
                    Ext.MessageBox.alert('Sånn', 'Leieavtalen er åpen');
                    oppsummering.getComponent('hovedoppsummering').getLoader().load();
                    varsling.getComponent('hovedvarsling').getLoader().load();
                    frysmeny.setHandler(function() {
                        frysAvtale();
                    });
                    frysmeny.setText('Frys leieforholdet');
                },
                failure : function(){
                    Ext.MessageBox.alert('Arg!', 'Klarte ikke gjenåpne leieavtalen');
                }
            });
        }


        var frysmeny = Ext.create('Ext.menu.Item', {
            text: '<?php echo $leieforhold->hentFrosset() ? "Tin leieforholdet" : "Frys leieforholdet";?>',
            handler: function(button, event) {
                <?php echo $leieforhold->hentFrosset() ? "tinAvtale();\n" : "frysAvtale();\n";?>
            }
        });


        var transaksjoner = Ext.create('Ext.data.Store', {
            model: 'Transaksjon',

            pageSize: 100,
            buffered: true,
            leadingBufferZone: 100,

            proxy: {
                type: 'ajax',
                simpleSortMode: true,
                url: "/drift/index.php?oppslag=leieforholdkort&id=<?php echo $leieforhold;?>&oppdrag=hentdata&data=transaksjoner",
                timeout: 300000,
                reader: {
                    type: 'json',
                    root: 'data',
                    actionMethods: { // Feil plassering? actionMethods tilhører proxy..
                        read: 'POST'
                    },
                    totalProperty: 'totalRows'
                }
            },

            remoteSort: true,
            sorters: [{
                property: 'dato',
                direction: 'DESC'
            }]
        });
        transaksjoner.on({
            load: velgTransaksjon
        });



        var kravsett = Ext.create('Ext.data.Store', {
            model: 'Krav',

            pageSize: 100,
            buffered: true,
            leadingBufferZone: 100,

            proxy: {
                type: 'ajax',
                simpleSortMode: true,
                url: "/drift/index.php?oppslag=leieforholdkort&id=<?php echo $leieforhold;?>&oppdrag=hentdata&data=krav",
                timeout: 300000,
                reader: {
                    type: 'json',
                    root: 'data',
                    actionMethods: { // Feil plassering? actionMethods tilhører proxy..
                        read: 'POST'
                    },
                    totalProperty: 'totalRows'
                }
            },

            remoteSort: true,
            sorters: [{
                property: 'dato',
                direction: 'DESC'
            }]
        });
        kravsett.on({
            load: velgKrav
        });



        var innbetalingssett = Ext.create('Ext.data.Store', {
            model: 'Innbetaling',

            pageSize: 100,
            buffered: true,
            leadingBufferZone: 100,

            proxy: {
                type: 'ajax',
                simpleSortMode: true,
                url: "/drift/index.php?oppslag=leieforholdkort&id=<?php echo $leieforhold;?>&oppdrag=hentdata&data=innbetalinger",
                timeout: 300000,
                reader: {
                    type: 'json',
                    root: 'data',
                    actionMethods: { // Feil plassering? actionMethods tilhører proxy..
                        read: 'POST'
                    },
                    totalProperty: 'totalRows'
                }
            },

            remoteSort: true,
            sorters: [{
                property: 'dato',
                direction: 'DESC'
            }]
        });



        var status = Ext.create('Ext.form.Panel', {
            itemId: 'status',
            autoScroll: true,
            bodyPadding: 5,
            frame: true,
            region: 'east',
            width: 350,
            autoLoad: '/drift/index.php?oppslag=leieforholdkort&id=<?php echo $leieforhold;?>&oppdrag=hentdata&data=balanse'
        });


        var oppsummering = Ext.create('Ext.panel.Panel', {
            itemId: 'oppsummering',
            title: 'Oppsummering',
            layout: 'border',
            items: [
                {
                    itemId: 'hovedoppsummering',
                    xtype: 'panel',
                    region: 'center',
                    autoScroll: true,
                    bodyPadding: 5,
                    border: false,

                    loader: {
                        url: '/drift/index.php?oppslag=leieforholdkort&oppdrag=hentdata&id=<?php echo $leieforhold;?>',
                        renderer: 'html',
                        autoLoad: true,
                        ajaxOptions: {
                            timeout: 900000
                        }
                    }

                },
                status
            ],
            buttons: [{
                scale: 'medium',
                text: 'Mer ...',
                menu: Ext.create('Ext.menu.Menu', {
                    items: [
                        {
                            text: 'Oppfølging av leieforhold <?php echo $leieforhold;?>',
                            handler: function() {
                                window.location = '/oppfølging/index.php?oppslag=oversikt&id=<?php echo $leieforhold;?>'
                            }
                        }
                    ]
                })
            },
                {
                    scale: 'medium',
                    text: 'Handlinger',
                    menu: Ext.create('Ext.menu.Menu', {
                        items: [
                            <?php if( $leieforhold->hentOppsigelse() ):?>
                            frysmeny
                            <?php else:?>
                            {
                                text: 'Endre leieforholdet',
                                handler: function() {
                                    window.location = '/drift/leieforhold_skjema/<?php echo $leieforhold;?>'
                                }
                            },
                            {
                                text: 'Registrer depositum',
                                handler: function() {
                                    window.location = '/drift/index.php?oppslag=depositum_skjema&leieforhold=<?php echo $leieforhold;?>&id=*'
                                }
                            },
                            <?php if( $leieforhold->hentTildato() ):?>
                            {
                                text: 'Forny leieavtalen',
                                handler: function() {
                                    window.location = '/drift/leieforhold_skjema/<?php echo $leieforhold;?>?fornyelse=1'
                                }
                            },
                            <?php endif;?>
                            {
                                text: 'Avslutt leieforholdet',
                                handler: function() {
                                    window.location = '/drift/index.php?oppslag=oppsigelsesskjema&id=<?php echo $leieforhold;?>'
                                }
                            }
                            <?php endif;?>
                        ]
                    })
                },
                {
                    scale: 'medium',
                    text: 'Tilbake',
                    handler: function() {
                        window.location = '<?php echo $this->returi->get();?>';
                    }
                }
            ]
        });



        var kontoforløp = Ext.create('Ext.grid.Panel', {
            itemId: 'kontoforløp',
            autoScroll: true,
            frame: false,
            features: [],

            plugins: [{
                ptype: 'bufferedrenderer',
                trailingBufferZone: 20,  // Keep 20 rows rendered in the table behind scroll
                leadingBufferZone: 50   // Keep 50 rows rendered in the table ahead of scroll
            }],

            store: transaksjoner,
            title: 'Kontoforløp',
            tbar: [
    //			søkefelt
            ],
            columns: [{
                dataIndex: 'id',
                align: 'right',
                header: 'Id',
                hidden: true,
                renderer: gråfremtid,
                sortable: false,
                width: 110
            },
                {
                    dataIndex: 'dato',
                    header: 'Dato',
                    renderer: gråfremtid,
                    sortable: true,
                    width: 70
                },
                {
                    dataIndex: 'tekst',
                    header: 'Beskrivelse',
                    flex: 1,
                    renderer: gråfremtid,
                    sortable: false
                },
                {
                    align: 'right',
                    dataIndex: 'husleie',
                    header: 'Husleie',
                    renderer: gråfremtid.beløp,
                    sortable: false,
                    width: 70
                },
                {
                    align: 'right',
                    dataIndex: 'fellesstrøm',
                    header: 'Fellesstrøm',
                    renderer: gråfremtid.beløp,
                    sortable: false,
                    width: 70
                },
                {
                    align: 'right',
                    dataIndex: 'annet',
                    header: 'Annet',
                    renderer: gråfremtid.beløp,
                    sortable: false,
                    width: 70
                },
                {
                    align: 'right',
                    style: {
                        color: 'green'
                    },
                    dataIndex: 'innbetalt',
                    header: 'Innbetalt',
                    renderer: gråfremtid.beløp,
                    sortable: false,
                    width: 80
                },
                {
                    align: 'right',
                    dataIndex: 'saldo',
                    header: 'Saldo',
                    renderer: saldorenderer,
                    sortable: false,
                    width: 80
                }
            ],

            listeners: {
                celldblclick: function(view, td, cellIndex, record) {
                    if(record.get('innbetalt')) {
                        window.location = "/drift/index.php?oppslag=innbetalingskort&id=" + record.get('id');
                    }
                    else {
                        window.location = "/drift/index.php?oppslag=krav_kort&id=" + record.get('id');
                    }
                }
            },

            buttons: []
        });
        kontoforløp.on({
            viewready: function() {
                transaksjoner.loadPage(1);
            }
        });


        var krav = Ext.create('Ext.grid.Panel', {
            itemId: 'krav',
            store: kravsett,
            title: 'Betalingskrav',
            autoScroll: true,
            frame: false,
            features: [],

            plugins: [{
                ptype: 'rowexpander',
                rowBodyTpl : ['{html}']
            }, {
                ptype: 'bufferedrenderer',
                trailingBufferZone: 100,  // Keep 20 rows rendered in the table behind scroll
                leadingBufferZone: 100   // Keep 50 rows rendered in the table ahead of scroll
            }],

            tbar: [
    //			søkefelt
            ],
            columns: [{
                align: 'right',
                dataIndex: 'id',
                header: 'Id',
                hidden: true,
                renderer: gråfremtid,
                sortable: true,
                width: 50
            },
                {
                    dataIndex: 'dato',
                    header: 'Dato',
                    renderer: gråfremtid,
                    sortable: true,
                    width: 70
                },
                {
                    dataIndex: 'tekst',
                    header: 'Krav',
                    id: 'tekst',
                    flex: 1,
                    renderer: gråfremtid,
                    sortable: true
                },
                {
                    dataIndex: 'type',
                    header: 'Kravtype',
                    renderer: gråfremtid,
                    sortable: true,
                    width: 70
                },
                {
                    dataIndex: 'fom',
                    header: 'Fra',
                    hidden: true,
                    renderer: gråfremtid,
                    sortable: true,
                    width: 120
                },
                {
                    dataIndex: 'tom',
                    header: 'Til',
                    hidden: true,
                    renderer: gråfremtid,
                    sortable: true,
                    width: 120
                },
                {
                    dataIndex: 'fom',
                    header: 'Termin',
                    renderer: function(value, metadata, record) {
                        return record.get('termin');
                    },
                    sortable: true,
                    width: 140
                },
                {
                    dataIndex: 'giro',
                    header: 'Regning',
                    renderer: gråfremtid,
                    sortable: true,
                    width: 70
                },
                {
                    dataIndex: 'forfall',
                    header: 'Forfall',
                    renderer: gråfremtid,
                    sortable: true,
                    width: 120
                },
                {
                    align: 'right',
                    dataIndex: 'beløp',
                    header: 'Beløp',
                    renderer: gråfremtid.beløp,
                    sortable: true,
                    width: 70
                },
                {
                    align: 'right',
                    dataIndex: 'utestående',
                    header: 'Ubetalt',
                    renderer: restrenderer,
                    sortable: true,
                    width: 70
                }
            ],

            listeners: {
                celldblclick: function(view, td, cellIndex, record) {
                    window.location = "/drift/index.php?oppslag=krav_kort&id=" + record.get('id');
                }
            },

            buttons: []
        });
        krav.on({
            viewready: function() {
                kravsett.loadPage(1);
            }
        });


        var innbetalinger = Ext.create('Ext.grid.Panel', {
            itemId: 'innbetalinger',
            autoScroll: true,
            frame: false,
            features: [],

            plugins: [{
                ptype: 'rowexpander',
                rowBodyTpl : ['{html}']
            }, {
                ptype: 'bufferedrenderer',
                trailingBufferZone: 20,  // Keep 20 rows rendered in the table behind scroll
                leadingBufferZone: 100   // Keep 50 rows rendered in the table ahead of scroll
            }],
            store: innbetalingssett,
            title: 'Innbetalinger',
            tbar: [
    //			søkefelt
            ],
            columns: [{
                dataIndex: 'id',
                header: 'Id',
                sortable: true,
                width: 110,
                hidden: true
            },
                {
                    dataIndex: 'dato',
                    header: 'Dato',
                    renderer: Ext.util.Format.dateRenderer('d.m.Y'),
                    sortable: true,
                    width: 70
                },
                {
                    dataIndex: 'ref',
                    header: 'Ref',
                    sortable: true,
                    width: 100
                },
                {
                    dataIndex: 'betaler',
                    header: 'Betalt av',
                    id: 'betaler',
                    flex: 1,
                    sortable: true,
                    width: 150
                },
                {
                    dataIndex: 'konto',
                    header: 'Konto',
                    sortable: true
                },
                {
                    dataIndex: 'transaksjonsbeskrivelse',
                    header: 'Betalingsmåte',
                    sortable: true,
                    width: 150
                },
                {
                    align: 'right',
                    dataIndex: 'beløp',
                    header: 'Beløp',
                    renderer: Ext.util.Format.noMoney,
                    sortable: true,
                    width: 80
                },
                {
                    dataIndex: 'merknad',
                    header: 'Merknad',
                    sortable: true,
                    flex: 1
                }
            ],

            listeners: {
                celldblclick: function(view, td, cellIndex, record) {
                    window.location = "/drift/index.php?oppslag=innbetalingskort&id=" + record.get('id');
                }
            },

            buttons: []
        });


        var kontooversikt = Ext.create('Ext.tab.Panel', {
            itemId: 'kontooversikt',
            title: 'Kontooversikt',
            items: [
                kontoforløp,
                krav,
                innbetalinger
            ],
            activeTab: '<?php echo $tab;?>',

            buttons: [{
                scale: 'medium',
                text: 'Mer ...',
                menu: Ext.create('Ext.menu.Menu', {
                    items: [
                        {
                            text: 'Oppfølging av leieforhold <?php echo $leieforhold;?>',
                            handler: function() {
                                window.location = '/oppfølging/index.php?oppslag=oversikt&id=<?php echo $leieforhold;?>'
                            }
                        }
                    ]
                })
            },
                {
                    scale: 'medium',
                    text: 'Handlinger',
                    menu: Ext.create('Ext.menu.Menu', {
                        items: [
                            {
                                text: 'Skriv ut kontoforløp',
                                handler: function() {
                                    window.open("/drift/index.php?oppslag=leieforhold_kontoforløp_utskrift&oppdrag=utskrift&id=<?php echo $leieforhold;?>");
                                }
                            },
                            {
                                text: 'Legg inn nytt krav/kreditt ...',
                                handler: function() {
                                    window.location = '/drift/index.php?oppslag=kravskjema&leieforhold=<?php echo $leieforhold;?>&id=*'
                                }
                            },
                            {
                                text: 'Send ny giro...',
                                handler: function() {
                                    window.location = '/drift/index.php?oppslag=utskriftsveiviser&ink_leieforhold=<?php echo $leieforhold;?>'
                                }
                            },
                            {
                                text: 'Purr...',
                                handler: function() {
                                    window.location = '/drift/index.php?oppslag=utskriftsveiviser_purringer&ink_leieforhold=<?php echo $leieforhold;?>'
                                }
                            },
                            {
                                text: 'Skriv ut samlegiro',
                                handler: function() {
                                    window.open('/drift/index.php?oppslag=leieforholdkort&id=<?php echo $leieforhold;?>&oppdrag=lagpdf&pdf=samlegiro')
                                }
                            },
                            {
                                text: 'Registrer betaling...',
                                handler: function() {
                                    window.location = '/drift/index.php?oppslag=betaling_skjema&id=*'
                                }
                            },
                            {
                                text: 'Oppfølging...',
                                handler: function() {
                                    window.location = '/oppfølging/index.php?oppslag=oversikt&id=<?php echo $leieforhold;?>'
                                }
                            }
                        ]
                    })
                },
                {
                    scale: 'medium',
                    text: 'Tilbake',
                    handler: function() {
                        window.location = '<?php echo $this->returi->get();?>';
                    }
                }
            ]
        });



        var varsling = Ext.create('Ext.panel.Panel', {
            itemId: 'varsling',
            title: 'Varsling',
            layout: 'border',
            items: [
                {
                    itemId: 'hovedvarsling',
                    xtype: 'panel',
                    autoScroll: true,
                    bodyPadding: 5,
                    border: false,
                    region: 'center',

                    loader: {
                        url: '/drift/index.php?oppslag=leieforholdkort&id=<?php echo $leieforhold;?>&oppdrag=hentdata&data=varsling',
                        renderer: 'html',
                        autoLoad: true,
                        ajaxOptions: {
                            timeout: 900000
                        }
                    }

                }
            ],

            buttons: [{
                scale: 'medium',
                text: 'Handlinger',
                menu: Ext.create('Ext.menu.Menu', {
                    items: [
                        <?php foreach( $leieforhold->hentLeietakere() as $leietaker ):?>
                        <?php if($leietaker->person):?>
                        {
                            text: 'Endre <?php echo addslashes($leietaker->hentNavn());?> sin adresse',
                            handler: function() {
                                window.location = '<?php echo DriftKontroller::url('person_adresse_skjema', $leietaker->person->id);?>'
                            }
                        },
                        <?php endif;?>
                        <?php endforeach;?>
                        {
                            text: 'Endre levering',
                            handler: function() {
                                window.location = '<?php echo $this::url('leieforhold_regningsadresse', $leieforhold);?>'
                            }
                        }
                    ]
                })
            },
                {
                    scale: 'medium',
                    text: 'Tilbake',
                    handler: function() {
                        window.location = '<?php echo $this->returi->get();?>';
                    }
                }
            ]
        });


        var leieavtale = Ext.create('Ext.panel.Panel', {
            itemId: 'leieavtale',
            title: 'Leieavtale',
            layout: 'border',
            items: [
                {
                    autoScroll: true,
                    bodyPadding: 5,
                    border: false,
                    xtype: 'panel',
                    region: 'center',

                    loader: {
                        url: '/drift/index.php?oppslag=leieforholdkort&id=<?php echo $leieforhold;?>&oppdrag=hentdata&data=leieavtale',
                        renderer: 'html',
                        autoLoad: true,
                        ajaxOptions: {
                            timeout: 900000
                        }
                    }

                }
            ],

            buttons: [{
                scale: 'medium',
                text: 'Handlinger',
                menu: Ext.create('Ext.menu.Menu', {
                    items: [
                        {
                            text: 'Skriv ut',
                            handler: function() {
                                window.open("<?php echo DriftKontroller::url('kontrakt_tekst_kort', $leieforhold->kontrakt->id, 'utlevering', ['data' => 'utskrift']);?>");
                            }
                        },
                        {
                            text: 'Last opp signert avtale...',
                            handler: function() {
                                window.location = '/drift/index.php?oppslag=kontrakt_opplasting_skjema&id=<?php echo $leieforhold->kontrakt->id;?>';
                            }
                        },
                        <?php if($leieforhold->kontrakt->hent('signert_fil')):?>
                        {
                            text: 'Last ned signert avtale',
                            handler: function() {
                                window.location = '/drift/index.php?oppslag=kontrakt_signert&id=<?php echo $leieforhold->kontrakt->id;?>';
                            }
                        },
                        <?php endif;?>
                        {
                            text: 'Endre avtaleteksten...',
                            handler: function() {
                                window.location = '/drift/index.php?oppslag=kontrakt_tekst_skjema&id=<?php echo $leieforhold->kontrakt->id;?>';
                            }
                        }
                    ]
                })
            },
                {
                    scale: 'medium',
                    text: 'Tilbake',
                    handler: function() {
                        window.location = '<?php echo $this->returi->get();?>';
                    }
                }
            ]
        });


        var fellesstrøm = Ext.create('Ext.panel.Panel', {
            itemId: 'fellesstrøm',
            title: 'Fellesstrøm',
            layout: 'border',
            items: [
                {
                    autoScroll: true,
                    bodyPadding: 5,
                    border: false,
                    xtype: 'panel',
                    region: 'center',

                    loader: {
                        url: '/drift/index.php?oppslag=leieforholdkort&id=<?php echo $leieforhold;?>&oppdrag=hentdata&data=fellesstrøm',
                        renderer: 'html',
                        autoLoad: true,
                        ajaxOptions: {
                            timeout: 900000
                        }
                    }

                }
            ],

            buttons: [{
                scale: 'medium',
                text: 'Handlinger',
                menu: Ext.create('Ext.menu.Menu', {
                    items: []
                })
            },
                {
                    scale: 'medium',
                    text: 'Tilbake',
                    handler: function() {
                        window.location = '<?php echo $this->returi->get();?>';
                    }
                }
            ]
        });


        var statistikk = Ext.create('Ext.panel.Panel', {
            itemId: 'statistikk',
            title: 'Statistikk',
            layout: 'border',
            items: [
                {
                    autoScroll: true,
                    bodyPadding: 5,
                    border: false,
                    xtype: 'panel',
                    region: 'center',

                    loader: {
                        url: '/drift/index.php?oppslag=leieforholdkort&id=<?php echo $leieforhold;?>&oppdrag=hentdata&data=statistikk',
                        renderer: 'html',
                        autoLoad: true,
                        ajaxOptions: {
                            timeout: 900000
                        }
                    }

                }
            ],

            buttons: [{
                scale: 'medium',
                text: 'Handlinger',
                menu: Ext.create('Ext.menu.Menu', {
                    items: []
                })
            },
                {
                    scale: 'medium',
                    text: 'Tilbake',
                    handler: function() {
                        window.location = '<?php echo $this->returi->get();?>';
                    }
                }
            ]
        });


        Ext.create('Ext.tab.Panel', {
            title: '<?php echo addslashes("Leieforhold $leieforhold: " . $leieforhold->hentNavn() . " i " . $leieforhold->leieobjekt->hentBeskrivelse());?>',
            autoScroll: true,
            bodyPadding: 5,
            frame: true,
            items: [
                oppsummering,
                kontooversikt,
                varsling,
                leieavtale,
                fellesstrøm,
                statistikk
            ],

            activeTab: '<?php echo addslashes( ($tab == 'kontoforløp' or $tab == 'krav' or $tab == 'innbetalinger') ? 'kontooversikt' : $tab);?>',

            renderTo: 'panel',
            height: 600
        });

        innbetalingssett.loadPage(1);

    });
    <?php
        return ob_get_clean();
    }

    /**
     *
     */
    public function design() {
    ?>
    <div id="panel" class="extjs-panel"></div>
    <?php
    }

    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ""):string {
        $tp = $this->mysqli->table_prefix;
        $sort		= $_GET['sort'] ?? '';
        $synkende	= ($_GET['dir'] ?? '') == 'DESC';
        $start		= (int)($_GET['start'] ?? 0);
        $limit		= $_GET['limit'] ?? null;

        $leieforholdId = intval(!empty($_GET['id']) ? $_GET['id'] : null);
        /** @var Leieforhold $leieforholdModell */
        $leieforholdModell = $this->hentModell(Leieforhold::class, $leieforholdId);

        if( !$leieforholdModell->hentId() ) {
            /** @var Leieforhold $leieforholdModell */
            $leieforholdModell = $this->hentModell(Leieforhold::class, $this->leieforhold($leieforholdId));
        }
        $leietakere		= $leieforholdModell->hentLeietakere();
        $leieobjekt		= $leieforholdModell->leieobjekt;
        $regningsobjekt	= $leieforholdModell->regningsobjekt;
        $regningsperson	= $leieforholdModell->regningsperson;
        $oppsigelse		= $leieforholdModell->hentOppsigelse();
        $depositum      = $leieforholdModell->hentDepositum();
        $efakturaIdentifier	= $leieforholdModell->regningsperson
            ? $leieforholdModell->regningsperson->hentEfakturaIdentifier()
            : null;
        $fbo			= $leieforholdModell->hentFbo();
        /** @var InnbetalingModell $sisteInnbetaling */
        $sisteInnbetaling = $leieforholdModell->hentSisteBetaling();
        $kontrakt		= $leieforholdModell->hentKontrakt();
        $betalingsplan = $leieforholdModell->hentBetalingsplan();


        switch ($data) {

            case 'balanse': {
                ob_start();
                /** @var \Kyegil\Leiebasen\Modell\Leieforhold\Kravsett $ubetalteKrav */
                $ubetalteKrav = $leieforholdModell->hentUteståendeKrav();
                if ($betalingsplan && $betalingsplan->hentAktiv()) {
                    $ubetalteKrav
                        ->leggTilLeftJoin(Originalkrav::hentTabell(),
                    '`' . KravModell::hentTabell() . '`.`' . KravModell::hentPrimærnøkkelfelt() . '` = `' . Originalkrav::hentTabell() . '`.`krav_id`'
                        )
                        ->leggTilFilter(['`' . Originalkrav::hentTabell() . '`.`' . Originalkrav::hentPrimærnøkkelfelt() . '`' => null])
                    ;

                }
                $framtidigeKrav = $leieforholdModell->hentFramtidigeKrav();
                $sum = 0;
                $antallFremtidige = 0;
    ?>
    <table class="dataload" style="font-size:9.5px;">
        <tr>
            <th>Krav</th>
            <th>Giro</th>
            <th>Forfall</th>
            <th>Å&nbsp;betale</th>
        </tr>

        <?php foreach($ubetalteKrav as $krav):?>
        <?php $regning = $krav->hentRegning();?>
        <tr>
            <td>
                <a title="Åpne" href="/drift/index.php?oppslag=krav_kort&id=<?php echo $krav->hentId();?>"><?php echo $krav->hentTekst();?></a>
            </td>
            <td><?php echo (($regning && $regning->hentUtskriftsdato()) ? "<a title=\"Klikk her for å åpne giroen i PDF-format\" target=\"_blank\" href=\"/drift/index.php?oppslag=giro&oppdrag=lagpdf&gironr={$regning->hentId()}\"><img alt=\"pdf\" style=\"height: 15px; width: 15px;\" src=\"/pub/media/bilder/felles/pdf.png\"></a>" : "&nbsp;" );?></td>
            <td><?php echo ($krav->hentForfall() ? $krav->hentForfall()->format('d.m.Y') : "&nbsp;" );?></td>
            <td class="value"><?php echo $this->kr($krav->hentUtestående());?></td>
        </tr>
        <?php $sum += $krav->hentUtestående();?>
        <?php endforeach;?>
        <?php if($sum != 0):?>
        <tr class="bold summary">
            <td colspan="3">Utestående</td>
            <td class="value"><?php echo $this->kr( $sum );?></td>
        </tr>
        <?php else:?>
        <tr>
            <td class="value" colspan="4"><i>Intet utestående <img alt="smiley" style="height: 15px; width: 15px;" src="/bilder/midkiffaries_Glossy_Emoticons.png"></i></td>
        </tr>
        <?php endif;?>

        <?php if($betalingsplan && $betalingsplan->aktiv):?>
        <tr>
            <td class="value" colspan="4">&nbsp;</td>
        </tr>
        <tr class="bold summary">
            <td colspan="3">Utestående inklusive betalingsplan</td>
            <td class="value"><?php echo $this->kr( $sum + $betalingsplan->hentUtestående());?></td>
        </tr>
        <?php endif;?>

        <tr>
            <td class="value" colspan="4">&nbsp;</td>
        </tr>
        <?php if($framtidigeKrav->hentAntall()):?>
        <tr class="bold summary">
            <th colspan="4">Kommende krav:</th>
        </tr>
        <?php endif;?>
        <?php foreach($framtidigeKrav as $krav):?>
        <?php $regning = $krav->hentRegning();?>
        <?php if(!$antallFremtidige or $krav->hentUtestående() != $krav->hentBeløp()):?>
        <tr>
            <td>
                <a title="Åpne" href="/drift/index.php?oppslag=krav_kort&id=<?php echo $krav->hentId();?>"><?php echo $krav->hentTekst();?></a>
            </td>
            <td><?php echo (($regning && $regning->hentUtskriftsdato()) ? "<a title=\"Klikk her for å åpne giroen i PDF-format\" target=\"_blank\" href=\"/drift/index.php?oppslag=giro&oppdrag=lagpdf&gironr={$regning->hentId()}\"><img alt=\"pdf\" style=\"height: 15px; width: 15px;\" src=\"/pub/media/bilder/felles/pdf.png\"></a>" : "&nbsp;" );?></td>
            <td><?php echo ($krav->hentForfall() ? $krav->hentForfall()->format('d.m.Y') : "&nbsp;" );?></td>
            <td class="value"><?php echo $this->kr($krav->hentUtestående());?></td>
        </tr>
        <?php $antallFremtidige++;?>
        <?php endif;?>
        <?php endforeach;?>
    </table>

    <?php if($sisteInnbetaling):?>
    <div style="font-size:9.5px;">Siste innbetaling: <?php echo $this->kr($sisteInnbetaling->hentBeløpTilLeieforhold($leieforholdModell));?> den <?php echo $sisteInnbetaling->dato->format('d.m.Y');?>.</div>
    <?php else:?>
    <div style="font-size:9.5px;">Det er ikke registrert noen innbetalinger til dette leieforholdet.</div>
    <?php endif;?>

    <?php if($betalingsplan && $betalingsplan->aktiv):?>
    <div style="font-size:9.5px;">Betalingsplan inngått for: <?php echo $this->kr($betalingsplan->hentStartgjeld());?><?php if($betalingsplan->hentSluttDato()):?> forventet nedbetalt  <?php echo $betalingsplan->hentSluttDato()->format('d.m.Y');?><?php endif;?>.</div>
    <?php else:?>
    <div style="font-size:9.5px;">Det er ikke inngått noen betalingsplan for dette leieforholdet.</div>
    <?php endif;?>

<?php
                return ob_get_clean();
        }

            case "varsling": {
                ob_start();
                $giroformat = $leieforholdModell->hentGiroformat() ?? '';
                $fbo = $leieforholdModell->hentFbo();
                ?>
                <div>
                    <strong>Adresse:</strong><br>
                    <?php if($leieforholdModell->hentRegningTilObjekt()):?>
                    Regninger og henvendelser leveres
                    <?php echo ($regningsobjekt && $regningsobjekt->id == $leieobjekt->hentId()) ? "direkte til leieobjektet." : "til {$regningsobjekt->hentType()} nr. {$regningsobjekt->hentId()}: {$regningsobjekt->hentBeskrivelse()}";?>
                    <?php elseif($regningsperson):?>
                    Regninger og henvendelser sendes i posten til <?php echo $regningsperson->hentNavn()?> sin adresse:<br><?php echo nl2br($regningsperson->hentPostadresse());?>
                    <?php else:?>
                    Regninger og henvendelser sendes i posten til:<br><?php echo nl2br($leieforholdModell->hentNavn() . "\n" . $this->vis(\Kyegil\Leiebasen\Visning\felles\html\leieforhold\Adressefelt::class, ['leieforhold' => $leieforholdModell]))?>
                    <?php endif;?>
                    <br>&nbsp;<br>
                </div>

                <?php if($leieforholdModell->hentFrosset()):?>
                    <div style="color:blue;"><strong>Leieforholdet er frosset, og ignoreres av leiebasen.</strong></div>
                <?php endif;?>

                <div>
                    Regningsformat: <strong><?php echo ( ucfirst($giroformat) ?: "Ønsker ikke giro tilsendt" );?></strong><br>
                    <?php if($efakturaIdentifier):?>eFaktura Identifier <?php echo $efakturaIdentifier;?><br><?php endif;?>
                    <?php if($fbo):?>AvtaleGiro registrert <?php echo $fbo->registrert->format('d.m.Y k\l. H:i');?>
                    <?php else:?>Ingen AvtaleGiro
                    <?php endif;?>
                    <br>&nbsp;<br>
                </div>

                <?php
                return ob_get_clean();
            }

            case "krav": {
                $kravsett = $leieforholdModell->hentKrav();
                $kravsett->inkluder('delkrav');
                /** @var InnbetalingModell\Delbeløpsett $delbeløpsett */
                $delbeløpsett = $kravsett->inkluder('innbetalinger');
                $delbeløpsett->leggTilInnbetalingModell();
                $resultat = (object)array(
                    'success'	=> true,
                    'data'		=> array()
                );
                /** @var Kyegil\Leiebasen\Modell\Leieforhold\Krav $krav */
                foreach( $kravsett as $krav ) {

                    $html = "";

                    /** @var KravModell\Delkravsett $delkravsett */
                    $delkravsett = $krav->hentDelkrav();
                    if( $delkravsett->hentAntall() ) {
                        $html .= "Inklusive:<br>";
                        /** @var KravModell\Delkrav $del */
                        foreach( $delkravsett as $delkrav ) {
                            $html .= "{$delkrav->delkravtype->navn}: {$this->kr($delkrav->beløp)}<br>";
                        }
                        $html .= "<br>";
                    }

                    /** @var InnbetalingModell\Delbeløpsett $betalinger */
                    $betalinger = $krav->hentInnbetalingsDelbeløp();
                    $html .= "Betalinger:<br>";
                    foreach( $betalinger as $delbeløp ) {
                        $html .= "<a href=\"/drift/index.php?oppslag=innbetalingskort&id={$delbeløp->innbetaling}\">{$delbeløp->innbetaling->hentDato()->format('d.m.Y')}: {$this->kr($delbeløp->beløp)}</a><br>";
                    }

                    $resultat->data[] = array(
                        'id'			=> (int)strval($krav),
                        'dato'			=> $krav->hentDato()->format('Y-m-d'),
                        'forfall'		=> ( ($forfall = $krav->hentForfall()) ? $forfall->format('Y-m-d') : null),
                        'tekst'			=> $krav->hentTekst(),
                        'giro'			=> ( ($giro = $krav->hentRegning()) ? (int)strval($giro) : null ),
                        'beløp'			=> (float)$krav->hentBeløp(),
                        'utestående'	=> (float)$krav->hentUtestående(),
                        'type'			=> $krav->hentType(),
                        'fom'			=> ($fom = $krav->hentFom()) ? $fom->format('Y-m-d') : null,
                        'tom'			=> ($tom = $krav->hentTom()) ? $tom->format('Y-m-d') : null,
                        'termin'		=> $krav->hentTermin(),
                        'html'			=> $html
                    );
                }
                $resultat->data =  $this->sorterObjekter($resultat->data, 'id', $synkende);
                $resultat->data =  $this->sorterObjekter($resultat->data, $sort, $synkende);
                $resultat->totalRows = count($resultat->data);
                $resultat->data = array_slice(  $resultat->data, $start, $limit);
                return json_encode( $resultat );
            }

            case "innbetalinger": {
                /** @var Innbetalingsett $innbetalinger */
                $innbetalinger = $leieforholdModell->hentInnbetalinger();
                $innbetalinger->leggTilFilter(['`' . InnbetalingModell::hentTabell() . '`.`konto` <> "0"']);
                /** @var InnbetalingModell\Delbeløpsett $delbeløpsett */
                $delbeløpsett = $innbetalinger->inkluder('delbeløp');
                $delbeløpsett->leggTilKravModell();
                $resultat = (object)array(
                    'success'	=> true,
                    'data'		=> array()
                );
                /** @var Delbeløp $delbeløp */
                foreach( $innbetalinger as $innbetaling ) {
                    $html = "";
                    foreach($innbetaling->hentDelbeløpTilLeieforhold($leieforholdModell) as $delbeløp) {
                        if( $delbeløp->krav ) {
                            if ($delbeløp->krav instanceof KravModell) {
                                $html .= "{$this->kr($delbeløp->beløp)}: {$delbeløp->krav->hentTekst()}<br>";
                            }
                            else if ($delbeløp->beløp < 0) {
                                $html .= "{$this->kr($delbeløp->beløp)}: Utbetaling av tilgodehavende<br>";
                            }
                            else if ($delbeløp->beløp < 0) {
                                $html .= "{$this->kr($delbeløp->beløp)}: <i>Refundert</i><br>";
                            }
                        }
                        else {
                            $html .= "{$this->kr($delbeløp->beløp)}: <i>Ikke avstemt</i><br>";
                        }

                    }

                    $resultat->data[] = array(
                        'id'						=> strval($innbetaling->hentId()),
                        'dato'						=> $innbetaling->hentDato()->format('Y-m-d'),
                        'ref'						=> $innbetaling->hentRef(),
                        'beløp'						=> (float)$innbetaling->hentBeløpTilLeieforhold($leieforholdModell),
                        'betaler'					=> $innbetaling->hentBetaler(),
                        'konto'						=> $innbetaling->hentKonto(),
                        'transaksjonstype'			=> ($ocr = $innbetaling->hentOcrTransaksjon()) ? (int)$ocr->transaksjonstype : null,
                        'transaksjonsbeskrivelse'	=> ($ocr) ? $ocr->hentTransaksjonsbeskrivelse() : null,
                        'merknad'					=> $innbetaling->hentMerknad(),
                        'html'						=> $html
                    );
                }
                $resultat->data =  $this->sorterObjekter($resultat->data, $sort, $synkende);
                $resultat->totalRows = count($resultat->data);
                $resultat->data = array_slice(  $resultat->data, $start, $limit);
                return json_encode($resultat);
            }

            case "transaksjoner": {
                $resultat = (object)array(
                    'success'	=> true,
                    'data'		=> [],
                    'totalRows'	=> 0
                );
                /** @var \Kyegil\Leiebasen\Samling $transaksjonsett */
                $transaksjonsett = $leieforholdModell->hentTransaksjoner(true);
                $saldo = 0;

                /** @var \Kyegil\Leiebasen\Modell\Leieforhold\Krav|InnbetalingModell $transaksjon */
                foreach( $transaksjonsett as $transaksjon ) {

                    if( $transaksjon instanceof \Kyegil\Leiebasen\Modell\Leieforhold\Krav ) {
                        $saldo -= $transaksjon->hentBeløp();
                        $resultat->data[] = (object)array(
                            'id'						=> (int)strval($transaksjon),
                            'dato'						=> $transaksjon->hentDato()->format('Y-m-d'),
                            'tekst'						=> $transaksjon->hentTekst(),
                            'beløp'						=> -$transaksjon->hentBeløp(),
                            'husleie'					=> ($transaksjon->hentType() == \Kyegil\Leiebasen\Modell\Leieforhold\Krav::TYPE_HUSLEIE) ? (float)$transaksjon->hentBeløp() : null,
                            'fellesstrøm'				=> ($transaksjon->hentType() == \Kyegil\Leiebasen\Modell\Leieforhold\Krav::TYPE_STRØM) ? (float)$transaksjon->hentBeløp() : null,
                            'annet'						=> ($transaksjon->hentType() != \Kyegil\Leiebasen\Modell\Leieforhold\Krav::TYPE_HUSLEIE && $transaksjon->hentType() != \Kyegil\Leiebasen\Modell\Leieforhold\Krav::TYPE_STRØM) ? (float)$transaksjon->hentBeløp() : null,
                            'innbetalt'					=> null,
                            'saldo'						=> $saldo
                        );
                    }
                    else {
                        $beløp = (float)$transaksjon->hentBeløpTilLeieforhold($leieforholdModell);
                        /** @var InnbetalingModell $transaksjon */
                        $saldo += $beløp;
                        $ocr = $transaksjon->hentOcrTransaksjon();
                        $betaler = $transaksjon->hentBetaler();

                        if ($beløp > 0) {
                            $retning = "innbetaling" . ($betaler ? " fra {$betaler}": "");
                        }
                        else {
                            $retning = "utbetaling" . ($betaler ? " til {$betaler}" : "");
                        }
                        $resultat->data[] = (object)array(
                            'id'						=> strval($transaksjon),
                            'dato'						=> $transaksjon->hentDato()->format('Y-m-d'),
                            'tekst'						=> ucfirst(($ocr ? "{$ocr->transaksjonsbeskrivelse} " : '') . "{$retning}"),
                            'beløp'						=> $beløp,
                            'husleie'					=> null,
                            'fellesstrøm'				=> null,
                            'annet'						=> null,
                            'innbetalt'					=> $beløp,
                            'saldo'						=> $saldo
                        );
                    }
                }

                if($synkende) {
                    $resultat->data = array_reverse( $resultat->data );
                }
                $resultat->totalRows = count($resultat->data);
                $resultat->data = array_slice(  $resultat->data, $start, $limit);
                return json_encode( $resultat );
            }

            case "leieavtale": {
                return $leieforholdModell->hentKontrakt()->hentTekst();
            }

            case "fellesstrøm": {
                ob_start();
                $fordelingsnøkler = $this->mysqli->select([
                    'source'	=> "{$tp}kostnadsdeling_fordelingsnøkler AS kostnadsdeling_fordelingsnøkler\n"
                        .	"LEFT JOIN {$tp}kostnadsdeling_tjenester AS kostnadsdeling_tjenester ON kostnadsdeling_fordelingsnøkler.tjeneste_id = kostnadsdeling_tjenester.id\n",
                    'fields'	=> "kostnadsdeling_fordelingsnøkler.fordelingsmåte,\n"
                        .	"kostnadsdeling_fordelingsnøkler.andeler,\n"
                        .	"kostnadsdeling_fordelingsnøkler.prosentsats,\n"
                        .	"kostnadsdeling_fordelingsnøkler.fastbeløp,\n"
                        .	"kostnadsdeling_fordelingsnøkler.følger_leieobjekt,\n"
                        .	"kostnadsdeling_tjenester.id,\n"
                        .	"kostnadsdeling_tjenester.navn,\n"
                        .	"kostnadsdeling_tjenester.beskrivelse\n",
                    'where'		=> "(!følger_leieobjekt AND kostnadsdeling_fordelingsnøkler.leieforhold = '{$leieforholdModell->hentId()}' )\n"
                        .	"OR ( følger_leieobjekt AND kostnadsdeling_fordelingsnøkler.leieobjekt = '{$leieobjekt}' )"
                ]);
                $fellesstrømDelkrav = $leieforholdModell->hentDelkravtype('strøm');
                ?>
                <h3>Deltakelse i strømdeling:</h3>

                <?php foreach($fordelingsnøkler->data as $fordelingsnøkkel):?>
                    <?php if($fordelingsnøkkel->fordelingsmåte == 'Andeler'):?>
                        <div><?php echo $fordelingsnøkkel->andeler > 1 ? "{$fordelingsnøkkel->andeler} deler" : "{$fordelingsnøkkel->andeler} del";?> av <?php echo "<a href=\"/drift/index.php?oppslag=fs_anlegg_kort&id={$fordelingsnøkkel->id}\">{$fordelingsnøkkel->navn}</a>, {$fordelingsnøkkel->beskrivelse}" . ($fordelingsnøkkel->følger_leieobjekt ? ", gjennom leieobjekt {$leieobjekt->hentId()}." : ".");?></div>
                    <?php elseif($fordelingsnøkkel->fordelingsmåte == 'Prosentvis'):?>
                        <div><?php echo $this->prosent($fordelingsnøkkel->prosentsats);?> av <?php echo "<a href=\"/drift/index.php?oppslag=fs_anlegg_kort&id={$fordelingsnøkkel->id}\">anlegg nr {$fordelingsnøkkel->navn}</a>, {$fordelingsnøkkel->beskrivelse}" . ($fordelingsnøkkel->følger_leieobjekt ? ", gjennom leieobjekt {$leieobjekt->hentId()}." : ".");?></div>
                    <?php else:?>
                        <div>Manuell beregning ved hver faktura for <?php echo "<a href=\"/drift/index.php?oppslag=fs_anlegg_kort&id={$fordelingsnøkkel->id}\">{$fordelingsnøkkel->navn}</a>, {$fordelingsnøkkel->beskrivelse}" . ($fordelingsnøkkel->følger_leieobjekt ? ", gjennom leieobjekt {$leieobjekt->hentId()}." : ".");?></div>
                    <?php endif;?>
                <?php endforeach;?>

                <?php if(!$fordelingsnøkler->totalRows):?>
                    Ingen deltakelse
                <?php endif;?>

                <?php if($fellesstrømDelkrav && $fellesstrømDelkrav->hentBeløp()):?>
                    <p>Fellesstrøm kreves inn i form av et fast tillegg til husleia på <?php echo $this->kr($fellesstrømDelkrav->beløp / $leieforholdModell->hentAntTerminer());?> per <?php echo Leiebase::periodeformat($leieforholdModell->hentTerminlengde(), false, true);?>.<br>
                    <?php if($fellesstrømDelkrav->hentPeriodiskAvstemming()):?>
                        Beløpet vil avregnes mot faktisk forbruk.<br>
                    <?php endif;?>
                    </p>
                <?php endif;?>
                <?php
                return ob_get_clean();
            }

            case "statistikk": {
                ob_start();
                $kravsett = $leieforholdModell->hentKrav();
                $kravsett->inkluder('innbetalinger');

                $idag = date_create_immutable();
                $fradato = $idag->sub(new DateInterval('P1Y'));
                $antallDager = 0;
                $antallKrav = 0;
                $løpende = false;

                foreach( $kravsett as $krav ) {
                    $forfall = $krav->hentForfall();
                    if( $forfall > $fradato and $forfall <= $idag ) {
                        $betalt = $krav->hentBetalt();
                        $løpende = $løpende || !$betalt;
                        if($betalt) {
                            $antallDager += $betalt->diff($forfall)->days;
                        $antallKrav++;
                        }
                    }
                }
                $antallDager = intval($antallDager/max(1, $antallKrav));

                ?>
                <div>Siste leiejustering: <?php echo $leieforholdModell->hentSisteLeiejustering()->dato->format('d.m.Y');?><br><br></div>

                <div><strong>Betalingspunktlighet:</strong>
                    <?php if($antallKrav):?>
                        Regninger som har forfalt siden <?php echo $fradato->format('d.m.Y');?> har gjennomsnittlig blitt betalt <?php echo ($antallDager ? ($antallDager < 0 ? (abs($antallDager) . " dager etter") : "{$antallDager} dager før") : "på");?> forfall.
                    <?php else:?>
                        <i>Ikke beregningsgrunnlag</i>
                    <?php endif;?>
                </div>

                <?php foreach ($this->hentPoengbestyrer()->hentAktiveProgram() as $poengprogram):?>
                    <div>
                        <div>
                            <strong><?php echo $poengprogram->navn;?></strong>:
                            <?php echo $this->hentPoengbestyrer()->hentPoengSum($poengprogram, $leieforholdModell);?>
                        </div>
                    </div>
                <?php endforeach;?>

                <?php
                return ob_get_clean();
            }

            default: {
                ob_start();
                $oppsigelsestid = Leiebase::periodeformat($leieforholdModell->hentOppsigelsestid());
                ?>
                <div>
                    Leieforhold nr. <?php echo $leieforholdModell->hentId();?>, påbegynt <?php echo $leieforholdModell->hentFradato()->format('d.m.Y');?><br>
                    Siste inngåtte leieavtale: #<?php echo $kontrakt->kontraktnr;?> gjelder fra <?php echo $kontrakt->dato->format('d.m.Y');?>.<br>

                    <?php if($depositum):?>
                        <a href="/drift/index.php?oppslag=depositum_kort&id=<?php echo $depositum->hentId();?>" title="Vis depositumsdetaljene">
                        <?php if($depositum->hentType() == Depositum::TYPE_GARANTI):?>
                        Depositumsgaranti
                            <?php echo $depositum->dato ? ('gitt ' . $depositum->dato->format('d.m.Y')) : '';?>
                            <?php echo $depositum->utløpsdato ? ('utløper ' . $depositum->utløpsdato->format('d.m.Y')) : '';?>
                        <?php else:?>
                            Depositum
                            <?php echo $depositum->dato ? ('betalt ' . $depositum->dato->format('d.m.Y')) : '';?>
                        <?php endif;?>
                        </a><br><br>
                    <?php endif;?>

                    <?php if( $oppsigelse ):?>
                        <strong style="color: red">Leieforholdet er oppsagt med virkning fra <?php echo $oppsigelse->fristillelsesdato->format('d.m.Y');?></strong><br>
                        Oppsigelsen ble levert <?php echo $oppsigelse->oppsigelsesdato->format('d.m.Y');?>. <br>

                        <?php if( $oppsigelse->oppsigelsestidSlutt > $oppsigelse->fristillelsesdato ):?>
                            Oppsigelsestiden utløpt: <?php echo $oppsigelse->oppsigelsestidSlutt->format('d.m.Y');?><br>
                        <?php endif;?>

                        <?php if($leieforholdModell->hentFrosset()):?>
                            <strong style="color: blue;">Leieavtalen er frosset, og ignoreres av leiebasen</strong><br>
                        <?php else:?>
                            <br>
                        <?php endif;?>

                    <?php else:?>
                        <?php if($leieforholdModell->hentTildato()):?>
                            Leieforholdet er tidsbegrenset til og med
                            <strong<?php if($leieforholdModell->hentTildato() < new DateTime()):?> style="color:red;"<?php endif;?>>
                            <?php echo $leieforholdModell->hentTildato()->format('d.m.Y');?>.
                            </strong><br>
                        <?php endif;?>

                        <?php if($leieforholdModell->hentTildato() and $leieforholdModell->hentTildato() < new DateTime()):?>
                            <strong style="color:red;">Leieavtalen har utløpt. Leieforholdet må avsluttes eller avtalen fornyes.</strong><br>
                        <?php endif;?>

                        <div>
                            <strong>Oppsigelsestid:</strong> <?php echo $oppsigelsestid ? $oppsigelsestid : "ingen avtalt oppsigelsestid";?>.<br>
                        </div>
                    <?php endif;?>

                    <?php if($leieforholdModell->hentAvventOppfølging()):?>
                        Videre oppfølging av utestående beløp e.l. bør avventes til etter <?php echo $leieforholdModell->hentAvventOppfølging()->format('d.m.Y');?><br>
                    <?php endif;?>

                    <?php if($leieforholdModell->hentStoppOppfølging()):?>
                        Utestående på leieforholdet skal ikke følges opp.<br>
                    <?php endif;?>
                    <br>
                    <strong><?php echo ($leietakere->hentAntall() > 1) ? "Leietakere:<br>" : "Leietaker: ";?></strong>

                    <?php foreach( $leietakere as $leietaker ):?>
                        <?php if($leietaker->person):?>
                            <a href="/drift/index.php?oppslag=personadresser_kort&id=<?php echo $leietaker->person->hentId();?>" title="Klikk på navnet for å åpne adressekortet"><?php echo $leietaker->hentNavn();?></a><br>
                        <?php else:?>
                            <?php echo $leietaker->hentNavn();?><br>
                        <?php endif;?>
                    <?php endforeach;?>

                    <?php if( $leieforholdModell->hentAndreBeboere() && $leieforholdModell->hentAndreBeboere()->hentAntall()):?>
                        <strong>Øvrige medlemmer av husstanden:<br></strong>
                        <?php foreach( $leieforholdModell->hentAndreBeboere() as $beboer ):?>
                            <a href="/drift/index.php?oppslag=personadresser_kort&id=<?php echo $beboer->hentId();?>" title="Klikk på navnet for å åpne adressekortet"><?php echo $beboer->hentNavn();?></a><br>
                        <?php endforeach;?>
                    <?php endif;?>

                    <div>&nbsp;</div>
                    <strong>Leieobjekt:</strong>
                    <a href="/drift/index.php?oppslag=leieobjekt_kort&id=<?php echo $leieobjekt->hentId();?>" title="Klikk for å åpne leieobjektkortet">
                        <?php echo ucfirst("{$leieobjekt->hentType()} nr. {$leieobjekt->hentId()}: {$leieobjekt->hentBeskrivelse()}");?>
                    </a><br>

                    <?php if(!$leieforholdModell->hentAndel()->compare('=', 1)):?>
                        Leieforholdet disponerer <?php echo $leieforholdModell->hentAndel()->asFraction();?> av leieobjektet i bofellesskap.<br>
                    <?php endif;?>
                </div>
                <br>

                <div>
                    <strong>Leie:</strong><br>
                    Årlig leie: <?php echo $this->kr( $leieforholdModell->hentLeiebeløp() * $leieforholdModell->hentAntTerminer());?><br>
                    Leia forfaller <?php echo $leieforholdModell->hentAntTerminer() > 1 ? "i {$leieforholdModell->hentAntTerminer()} årlige terminer" : "årlig";?>.<br>
                    Terminbeløp: <?php echo $this->kr( $leieforholdModell->hentLeiebeløp());?><br>
                </div>
                <br>

                <div>
                    Fast KID: <?php echo $leieforholdModell->hentKid();?><br>

                    <?php if($this->hentValg('efaktura') && $efakturaIdentifier):?>
                        eFaktura Identifier <?php echo $efakturaIdentifier;?><br>
                    <?php endif;?>

                    <?php if($this->hentValg('avtalegiro')):?>
                        Status AvtaleGiro: <?php echo $fbo ? "Avtale registrert {$fbo->registrert->format('d.m.Y H:i:s')}": "Ingen avtale registrert";?><br>
                    <?php endif;?>
                    <?php
                    return ob_get_clean();
                }
            }
        }


    /**
     * @param $pdf
     * @throws Exception
     */
    public function lagPDF( $pdf ) {
        switch ( $pdf ) {

            case "samlegiro":
            {
                $leieforhold = $this->hentModell(Leieforhold::class, !empty($_GET['id']) ? $_GET['id'] : null);

                $pdf = new \Fpdf\Fpdf();
                $this->vis(\Kyegil\Leiebasen\Visning\felles\fpdf\Pdf::class, [
                    'pdf' => $pdf,
                    'leieforholdsett' => [$leieforhold]
                ])->render();
                $pdf->Output('I', "Samlegiro Leieforhold {$leieforhold}.pdf", true);

                break;
            }
        }
    }


    /**
     * @param string $skjema
     * @return string
     */
    public function taIMot($skjema):string {
        /** @var \stdClass $resultat */
        $resultat = (object)[
            'success' => false
        ];

        return json_encode($resultat);
    }
}