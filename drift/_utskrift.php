<?php
if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}
?>
<!DOCTYPE html>
<html lang="no">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title><?php echo $this->tittel;?></title>
        <link rel="stylesheet" type="text/css" href="/pub/lib/<?=$this->ext_bibliotek?>/resources/css/ext-all.css">
        <link rel="stylesheet" type="text/css" href="/pub/css/leiebase.css">
    </head>

    <body class="dataload">
        <?php $this->utskrift();?>
    </body>
</html>