<?php

use Kyegil\Leiebasen\Leiebase;
if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    public $ext_bibliotek = 'ext-3.4.0';
    public $tittel = 'Oppsigelser';

    /**
     * @param array $di
     * @param array $config
     * @throws Throwable
     */
public function __construct(array $di = [], array $config = []) {
    parent::__construct($di, $config);
    $this->hoveddata['sql'] = "SELECT *\n"
        .    "FROM oppsigelser\n"
        .    "ORDER BY fristillelsesdato DESC, oppsigelsesdato DESC";
}

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        if (isset($_GET['returi']) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }
?>

Ext.onReady(function() {
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';

    // oppretter datasettet
    var datasett = new Ext.data.JsonStore({
        url:'/drift/index.php?oppdrag=hentdata&oppslag=oppsigelse_liste',
        fields: [
            {name: 'leieforhold', type: 'float'},
            {name: 'navn'},
            {name: 'oppsigelsesdato', type: 'date', dateFormat: 'Y-m-d'},
            {name: 'fristillelsesdato', type: 'date', dateFormat: 'Y-m-d'},
            {name: 'oppsigelsestid_slutt', type: 'date', dateFormat: 'Y-m-d'},
            {name: 'ref'},
            {name: 'merknad'},
            {name: 'oppsagt_av_utleier', type: 'float'}
        ],
        root: 'data'
    });
    datasett.load();

    bekreftSletting = function(id) {
        Ext.Msg.show({
            title: 'Bekreft',
            id: id,
            msg: 'Er du sikker på at du vil slette oppsigelsen av leieforhold ' + id + ' og gjenopprette leieterminene?',
            buttons: Ext.Msg.OKCANCEL,
            fn: function(buttonId, text, opt){
                if(buttonId === 'ok') {
                    utførSletting(opt.id);
                }
            },
            animEl: 'elId',
            icon: Ext.MessageBox.QUESTION
        });
    }


    utførSletting = function(id){
        Ext.Ajax.request({
            waitMsg: 'Sletter...',
            url: "/drift/index.php?oppslag=oppsigelse_liste&oppdrag=oppgave&oppgave=slettoppsigelse&id=" + id,
            success: function(response){
                var tilbakemelding = Ext.util.JSON.decode(response.responseText);
                if(tilbakemelding['success'] === true) {
                    Ext.MessageBox.alert('Utført', tilbakemelding.msg, function(){
                    datasett.load();
                    });
                }
                else {
                    Ext.MessageBox.alert('Hmm..',tilbakemelding['msg']);
                }
            }
        });
    }


    var leieforhold = {
        dataIndex: 'leieforhold',
        header: 'Leieforhold',
        renderer: function(value, metaData, record){
            return  "<a title=\"Klikk på et avtalenummer her for å gå til leieavtalen\" href=\"/drift/index.php?oppslag=leieforholdkort&id=" + value + "\">" + value + "</a>" + ": " + record.data.navn;
        },
        sortable: true,
        width: 400
    };

    var oppsigelsesdato = {
        dataIndex: 'oppsigelsesdato',
        header: 'Levert',
        sortable: true,
        renderer: Ext.util.Format.dateRenderer('d.m.Y'),
        width: 90
    };

    var fristillelsesdato = {
        dataIndex: 'fristillelsesdato',
        header: 'Ledig fra',
        sortable: true,
        renderer: Ext.util.Format.dateRenderer('d.m.Y'),
        width: 90
    };

    var oppsigelsestid_slutt = {
        dataIndex: 'oppsigelsestid_slutt',
        header: 'Oppsigelsestid til',
        sortable: true,
        renderer: Ext.util.Format.dateRenderer('d.m.Y'),
        width: 90
    };

    var ref = {
        dataIndex: 'ref',
        header: 'Ref',
        sortable: true,
        width: 50
    };

    var merknad = {
        dataIndex: 'merknad',
        header: 'Merknad',
        renderer: function(value){
            return  "<div title=\"" + value + "\">" + value + "</div>";
        },
        sortable: true,
        width: 50
    };

    var oppsagt_av_utleier = {
        dataIndex: 'oppsagt_av_utleier',
        header: 'Sagt opp av utleier',
        sortable: true,
        renderer: Ext.util.Format.hake,
        width: 60
    };


    var slett = {
        dataIndex: 'leieforhold',
        header: 'Slett',
        renderer: function(v){
            return "<a style=\"cursor: pointer\" title=\"Slett oppsigelsen av leieforhold " + v + "\" onClick=\"bekreftSletting(" + v + ")\"><img alt=\"Slett\" src=\"/pub/media/bilder/drift/slett.png\"></a>";
        },
        sortable: false,
        width: 30
    };

    var rutenett = new Ext.grid.GridPanel({
        autoExpandColumn: 0,
        store: datasett,
        columns: [
            leieforhold,
            oppsagt_av_utleier,
            oppsigelsesdato,
            fristillelsesdato,
            oppsigelsestid_slutt,
            ref,
            merknad,
            slett
        ],
        stripeRows: true,
        height: 600,
        title: 'Oppsigelser',
        buttons: [{
            handler: function() {
                window.location = "index.php";
            },
            text: 'Tilbake'
        }]
    });

    // Rutenettet rendres in i HTML-merket '<div id="panel">':
    rutenett.render('panel');

});
<?php
        return ob_get_clean();
}

    /**
     * @return void
     */
    public function design() {
?>
<div id="panel" class="extjs-panel"></div>
<?php
    }

    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ""):string {
        switch ($data) {
            default:
                $resultat = $this->arrayData($this->hoveddata['sql']);
                foreach($resultat['data'] as $linje=>$verdi){
                    $resultat['data'][$linje]['navn'] = Leiebase::liste($this->kontraktpersoner($verdi['leieforhold'])) . " i " . $this->leieobjekt($this->kontraktobjekt($verdi['leieforhold']), true);
                }
                return json_encode($resultat);
        }
    }

    public function oppgave($oppgave) {
        switch ($oppgave) {
            case "slettoppsigelse":
                $resultat = [
                    'success' => true,
                    'msg' => null
                ];
                /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
                $leieforhold = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold::class, !empty($_GET['id']) ? $_GET['id'] : null);
                try {
                    $leieforhold->slettOppsigelse();
                } catch (Exception $e) {
                    $resultat['success'] = false;
                    $resultat['msg'] = 'Klarte ikke slette:<br>' . $e->getMessage();
                }
                return json_encode($resultat);
        }
    }

    /**
     * @param string $skjema
     * @return string
     */
    public function taIMot($skjema):string {
        return json_encode(null);
    }
}