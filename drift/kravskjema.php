<?php

use Kyegil\Leiebasen\Modell\Leieforhold as LeieforholdModell;

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    /**
     * @var string
     */
    public $tittel = 'leiebasen';
    /**
     * @var string
     */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * oppsett constructor.
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        if(!$id = $this->GET['id']) die("Ugyldig oppslag: ID ikke angitt for kravet");
    }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        if(@$_GET['returi'] == "default") {
            $this->returi->reset();
        }
        $tp             = $this->mysqli->table_prefix;
        $id             = $_GET['id'];
        /** @var \Krav|null $krav */
        $krav           = $this->hent(\Krav::class, !empty($_GET['id']) ? $_GET['id'] : null);
        /** @var Kyegil\Leiebasen\Modell\Leieforhold\Krav $kravModell */
        $kravModell	    = $this->hentModell(Kyegil\Leiebasen\Modell\Leieforhold\Krav::class, !empty($_GET['id']) ? $_GET['id'] : null);

        $kreditt = false;

        if($kravModell->hentId()) {
            $leieforholdModell = $kravModell->hentLeieforhold();
            $kreditt = $kravModell->erKreditt();
        }
        else {
            /** @var Kyegil\Leiebasen\Modell\Leieforhold $leieforholdModell */
            $leieforholdModell	= $this->hentModell(Kyegil\Leiebasen\Modell\Leieforhold::class, $_GET['leieforhold'] ?? null);
        }
        if((!$kravModell->hentId() && $_GET['id'] != '*') || !$leieforholdModell->hentId()) {
            return "window.location = '{$this->returi->get()}'";
        }

        $delkravtyper = $this->hentDelkravtyper()
            ->leggTilFilter(['selvstendig_tillegg' => false])->låsFiltre();

        $låst = ($kravModell->hentId() && $kravModell->hentReellKravId());



?>
Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', '<?php echo $this->http_host . '/pub/lib/' . $this->ext_bibliotek . "/examples/ux";?>');

Ext.require([
    'Ext.data.*',
    'Ext.form.*'
]);

Ext.onReady(function() {

    Ext.tip.QuickTipManager.init();
    Ext.form.Field.prototype.msgTarget = 'title';
    Ext.Loader.setConfig({enabled:true});

    var aktiverFelter = function() {

        if(type.getValue() === "Husleie") {
            andel.enable();
            anleggsnr.disable();
        }
        else {
            andel.disable();
        }
        if(type.getValue() === "Fellesstrøm") {
            anleggsnr.enable();
        }
        else {
            anleggsnr.disable();
        }

        <?php foreach($delkravtyper as $delkrav):?>

        if(type.getValue() === "<?php echo addslashes($delkrav->kravtype);?>") {
            delkrav<?php echo $delkrav->id;?>.enable();
        }
        else {
            delkrav<?php echo $delkrav->id;?>.disable();
        }
        <?php endforeach;?>
    }

    var beløpsValidator = function( kravbeløp ) {
        var kravbeløp = beløp.getValue();
        var delkravsum = 0;
        <?php foreach($delkravtyper as $delkrav):?>

        if(type.getValue() === "<?php echo addslashes($delkrav->kravtype);?>") {
            delkravsum += delkrav<?php echo $delkrav->id;?>.getValue();
        }
        if( kravbeløp * delkrav<?php echo $delkrav->id;?>.getValue() < 0 ) {
            return 'Beløpet kan ikke være negativt.';
        }
        <?php endforeach;?>

        if( Math.abs(kravbeløp) < Math.abs(delkravsum) ) {
            return 'Det totale beløpet kan ikke være mindre enn delbeløpene.';
        }

        return true;
    }

    var slettKrav = function(svar) {
        if(svar === 'yes') {
            Ext.Ajax.request({
                waitMsg: 'Sletter...',
                url: '/drift/index.php?oppslag=kravskjema&oppdrag=oppgave&oppgave=slett&id=<?php echo $id;?>',
                success: function( response ) {
                    var tilbakemelding = Ext.JSON.decode( response.responseText );
                    if( tilbakemelding.success === true ) {
                        Ext.MessageBox.alert( 'Utført', '<?php echo $kreditt ? "Kreditten" : "Kravet";?> er slettet', function() {
                            window.location = tilbakemelding.url;
                        });
                    }
                    else {
                        Ext.MessageBox.alert( 'Hmm..', tilbakemelding.msg );
                    }
                }
            });
        }
    }

    var fortegn = Ext.create('Ext.form.RadioGroup', {
        fieldLabel: 'Krav eller kreditt',
        columns: 1,
        vertical: false,
        items: [
            {
                name:		'fortegn',
                boxLabel:	'Betalingskrav',
                inputValue:	'',
                readOnly: <?php echo $_GET['id'] == '*' ? "false" : "true"; ?>,
                checked: <?php echo $kreditt ? "false" : "true"; ?>
            },
            {
                name:		'fortegn',
                boxLabel:	'Kreditt',
                inputValue:	'-',
                readOnly: <?php echo $_GET['id'] == '*' ? "false" : "true"; ?>,
                checked: <?php echo $kreditt ? "true" : "false"; ?>
            }
        ]
    });

    var tekst = Ext.create('Ext.form.field.Text', {
        fieldLabel: 'Beskrivelse',
        labelWidth: 80,
        maxLength: 100,
        allowBlank: false,
        name: 'tekst',
        tabIndex: 1,
        readOnly: <?php echo $låst ? 'true' : 'false' ;?>,
        flex: 1
    });

    var beløp = Ext.create('Ext.form.field.Number', {
        allowBlank: false,
        allowDecimals: true,
        minValue: 0,
        validator: beløpsValidator,
        decimalSeparator: ',',
        decimalPrecision: 2,
        hideTrigger: true,
        fieldLabel: 'Beløp',
        labelAlign: 'right',
        labelWidth: 40,
        name: 'beløp',
        tabIndex: 2,
        readOnly: <?php echo $låst ? 'true' : 'false' ;?>,
        width: 140
    });

    var leieforhold = Ext.create('Ext.form.field.Hidden', {
        name: 'leieforhold',
        value: '<?php echo $leieforholdModell;?>'
    });

    var type = Ext.create('Ext.form.field.ComboBox', {
        allowBlank: false,
        fieldLabel: 'Type',
        labelAlign: 'left',
        forceSelection: true,
        name: 'type',
        queryMode: 'local',

        store: Ext.create('Ext.data.Store', {
            fields: ['text'],
            data : [
                {text: "Husleie"},
                {text: "Fellesstrøm"},
                {text: "Purregebyr"},
                {text: "Annet"}
            ]
        }),

        listeners: {
            change: aktiverFelter
        },

        flex: 1,
        readOnly: <?php echo $låst ? 'true' : 'false' ;?>,
        tabIndex: 3
    });

    var kravdato = Ext.create('Ext.form.field.Date', {
        allowBlank: false,
        fieldLabel: 'Kravdato (regnskapsdato)',
        labelAlign: 'left',
        labelWidth: 150,
        format: 'd.m.Y',
        name: 'kravdato',
        submitFormat: 'Y-m-d',
        flex: 1,
        tabIndex: 4,
        readOnly: <?php echo $låst ? 'true' : 'false' ;?>,
        value: new Date()
    });

    var forfall = Ext.create('Ext.form.field.Date', {
        allowBlank: true,
        fieldLabel: 'Forfall',
        labelAlign: 'left',
        labelWidth: 150,
        format: 'd.m.Y',
        submitFormat: 'Y-m-d',
        flex: 1,
        tabIndex: 5,
        name: 'forfall'
    });

    var andel = Ext.create('Ext.form.field.Text', {
        allowBlank: false,
        fieldLabel: 'Andel',
        labelWidth: 150,
        blankText: 'Du må oppgi hvor stor andel (som brøk, desimal eller i prosent) av leieobjektet som omfattes av leieavtalen dersom dette er i et bofellesskap, eller 1 dersom leieavtalen leier hele leieobjektet',
        maskRe: new RegExp("^[0-9/%,.]"),
        name: 'andel',
        tabIndex: 6,
        readOnly: <?php echo $låst ? 'true' : 'false' ;?>
    });

    var anleggsnr = Ext.create('Ext.form.field.ComboBox', {

        fieldLabel: 'Anleggsnr (for fellesstrøm)',
        labelAlign: 'top',
        labelWidth: 150,
        tabIndex: 7,
        readOnly: <?php echo $låst ? 'true' : 'false' ;?>,
        name: 'anleggsnr',

        matchFieldWidth: false,
        listConfig: {
            width: 500
        },

        store: Ext.create('Ext.data.JsonStore', {
            storeId: 'leieobjektliste',

            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: "/drift/index.php?oppslag=kravskjema&id=<?php echo $id;?>&oppdrag=hentdata&data=anleggsnummer",
                reader: {
                    type: 'json',
                    root: 'data',
                    idProperty: 'avtalereferanse'
                }
            },

            fields: [
                {name: 'avtalereferanse'},
                {name: 'anlegg'}
            ]
        }),
        queryMode: 'remote',
        displayField: 'anlegg',
        valueField: 'avtalereferanse',
        minChars: 0,
        queryDelay: 1000,

        allowBlank: false,
        typeAhead: true,
        editable: true,
        selectOnFocus: true,
        forceSelection: true
    });

    var fom = Ext.create('Ext.form.field.Date', {
        fieldLabel: 'Fra dato (inklusive)',
        labelWidth: 120,
        format: 'd.m.Y',
        submitFormat: 'Y-m-d',
        tabIndex: 8,
        readOnly: <?php echo $låst ? 'true' : 'false' ;?>,
        name: 'fom'
    });

    var tom = Ext.create('Ext.form.field.Date', {
        fieldLabel: 'Til dato (inklusive)',
        labelWidth: 120,
        format: 'd.m.Y',
        submitFormat: 'Y-m-d',
        tabIndex: 9,
        readOnly: <?php echo $låst ? 'true' : 'false' ;?>,
        name: 'tom'
    });

    var termin = Ext.create('Ext.form.field.Text', {
        fieldLabel: 'Termin',
        labelWidth: 50,
        tabIndex: 10,
        readOnly: <?php echo $låst ? 'true' : 'false' ;?>,
        name: 'termin'
    });

    <?php foreach($delkravtyper as $delkrav):?>

    var delkrav<?php echo $delkrav->id;?> = Ext.create('Ext.form.field.Number', {
        allowDecimals: <?php echo $delkrav->relativ ? "true" : "false";?>,
        minValue: 0,
        validator: beløpsValidator,
        allowBlank: false,
        disabled: true,
        decimalPrecision: 1,
        decimalSeparator: ',',
        hideTrigger: true,
        fieldLabel: '<?php echo addslashes($delkrav->navn);?>',
        labelWidth: 120,
        name: 'delkrav<?php echo $delkrav->id;?>',
        tabIndex: <?php echo 10 + $delkrav->id;?>,
        readOnly: <?php echo $låst ? 'true' : 'false' ;?>,
        width: 180
    });
    <?php endforeach;?>





    var lagreknapp = Ext.create('Ext.Button', {
        text: 'Lagre',
        disabled: true,
        handler: function() {
            skjema.form.submit({
                url:'/drift/index.php?oppslag=kravskjema&id=<?php echo $_GET["id"];?>&oppdrag=taimotskjema',
                waitMsg:'Prøver å lagre...',

                success: function(form, action) {
                    if( action.response.responseText === '' ) {
                        Ext.MessageBox.alert('Problem', 'Det kom en blank respons fra tjeneren.');
                    } else {
                        var tilbakemelding = Ext.JSON.decode( action.response.responseText );
                        Ext.MessageBox.alert( 'Lagret', tilbakemelding.msg );
                        window.location = tilbakemelding.url;
                    }
                }
            });
        }
    });

    var skjema = Ext.create('Ext.form.Panel', {
            autoScroll: true,
            bodyPadding: 5,
            renderTo: 'panel',
            height: 600,
            fieldDefaults: {
                labelAlign: 'left',
                width: 200
            },
            items: [
                <?php echo $låst ? "{xtype: 'displayfield', value: 'Det er bare mulig å endre forfallsdato på dette kravet. Ved feil kan du evt. kreditere (slette) det, for så å opprette et nytt.', width: 800}," : '' ;?>
                leieforhold,
                {
                    xtype: 'displayfield',
                    value: '<a href="/drift/index.php?oppslag=leieforholdkort&id=<?php echo $leieforholdModell;?>" title="Gå til leieforholdkortet"><?php echo addslashes( $leieforholdModell->hentBeskrivelse());?></a>',
                    width: 800
                },
                fortegn,
                {
                    xtype: 'container',
                    layout:	{
                        type: 'hbox',
                        padding: '0',
                        defaultMargins: {
                            top: 0,
                            bottom: 10,
                            left: 0,
                            right: 0
                        }
                    },
                    items: [
                        tekst,
                        beløp
                    ]
                },

                {
                    xtype: 'container',
                    layout:	{

                        type: 'column',
                        padding: '0',
                        defaultMargins: {
                            top: 0,
                            bottom: 10,
                            left: 0,
                            right: 0
                        }
                    },
                    defaults: {
                        columnWidth: 0.33,
                        xtype: 'container'
                    },
                    items: [
                        {
                            defaults: {
                                width: 250
                            },
                            items: [
                                type,
                                kravdato,
                                forfall
                            ]
                        },
                        {
                            defaults: {
                                width: 250
                            },
                            items: [
                                andel,
                                anleggsnr
                            ]
                        },
                        {
                            defaults: {
                                width: 220
                            },
                            items: [
                                fom,
                                tom,
                                termin
                            ]
                        }
                    ]
                },
                {
                    xtype: 'displayfield',
                    value: 'Delbeløp:'
                },

                <?php foreach($delkravtyper as $indeks => $delkrav):?>
                <?php if($indeks/3 == (int)($indeks/3)):
                //	dette er første av tre kolonner?>

                {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [
                        <?php endif;?>

                        {
                            xtype: 'container',
                            flex: 1,
                            padding: '0 5',
                            items: [
                                delkrav<?php echo $delkrav->id;?>

                            ]
                            <?php if((($indeks + 1)/3 == (int)(($indeks + 1)/3)) || ($indeks + 1 == $delkravtyper->hentAntall())):
                            //	dette er tredje av tre eller siste kolonne?>
                        }
                    ]
                },
            <?php else:?>

        },
        <?php endif;?>
        <?php endforeach;?>

        {}
],
    layout: 'anchor',
        frame: true,
        title: '<?php echo addslashes( $kravModell->hentId() ? $kravModell->hentTekst() : "Registrer nytt betalingskrav eller kreditt i leieforhold {$leieforholdModell} {$leieforholdModell->hentNavn()}");?>',
        buttons: [{
        text: 'Tilbake',
        handler: function() {
            window.location = '<?php echo $this->returi->get();?>';
        }
    },
        {
            text: 'Slett',
            disabled: <?php echo ((int)$id and ($kravModell->hentBeløp() > 0 || !$kravModell->hentRegning())) ? 'false' : 'true';?>,
            handler: function() {
                Ext.Msg.confirm("Bekreft", "Er du sikker på at <?php echo $kreditt ? "kreditten" : "kravet"; ?> skal slettes?", slettKrav);
            }
        },
        lagreknapp
    ]
});



    skjema.on({
        actioncomplete: function(form, action){
            if(action.type === 'load') {
                <?php echo $låst ? 'aktiverFelter();' : '' ;?>
                lagreknapp.enable();
            }
        },

        actionfailed: function(form,action) {
            if(action.type === 'load') {
                if (action.failureType === "connect") {
                    Ext.MessageBox.alert('Problem:', 'Klarte ikke laste data. Fikk ikke kontakt med tjeneren.');
                }
                else {
                    if (action.response.responseText === '') {
                        Ext.MessageBox.alert('Problem:', 'Skjemaet mottok ikke data i JSON-format som forventet');
                    }
                    else {
                        var result = Ext.JSON.decode(action.response.responseText);
                        if(result && result.msg) {
                            Ext.MessageBox.alert('Mottatt tilbakemelding om feil:', result.msg);
                        }
                        else {
                            Ext.MessageBox.alert('Problem:', 'Innhenting av data mislyktes av ukjent grunn.');
                        }
                    }
                }
            }
            if(action.type === 'submit') {
                if (action.failureType === "connect") {
                    Ext.MessageBox.alert('Problem:', 'Klarte ikke lagre data. Fikk ikke kontakt med tjeneren.');
                }
                else {
                    var result = Ext.JSON.decode(action.response.responseText);
                    if(result && result.msg) {
                        Ext.MessageBox.alert('Mottatt tilbakemelding om feil:', result.msg);
                    }
                    else {
                        Ext.MessageBox.alert('Problem:', 'Lagring av data mislyktes av ukjent grunn. Action type=' + action.type + ', failure type=' + action.failureType);
                    }
                }
            }

        }
    });


    skjema.getForm().load({
        url: '/drift/index.php?oppslag=kravskjema&id=<?php echo $id;?>&oppdrag=hentdata',
        waitMsg: 'Laster kravet...'
    });


});
<?php
        return ob_get_clean();
    }

    /**
     *
     */
    public function design() {
        echo '<div id="panel" class="extjs-panel"></div>';
    }

    /**
     * @param string $data
     * @return string
     * @throws Exception
     */
    public function hentData($data = ""):string {
        $tp = $this->mysqli->table_prefix;
        $id = $this->GET['id'];

        switch ($data) {

            case "anleggsnummer": {
                return json_encode( $this->mysqli->select([
                    'source'	=> "kostnadsdeling_tjenester",
                    'fields'	=> "avtalereferanse, CONCAT(avtalereferanse, ' (målernr: ', målernummer, '): ', beskrivelse) AS anlegg",
                    'where'		=>	isset( $_GET['query'] )
                        ? "CONCAT(avtalereferanse, ' (målernr: ', målernummer, '): ', beskrivelse) LIKE '%{$this->GET['query']}%'"
                        : null
                ]) );
            }

            default: {

                if($_GET['id'] == '*') {
                    $resultat = (object)array(
                        'success'	=> true,
                        'data'		=> array()
                    );
                }

                else {
                    /** @var \Krav|null $krav */
                    $krav = $this->hent(\Krav::class, $id);
                    $kravModell = $this->hentModell(LeieforholdModell\Krav::class, $id);

                    $delkravtyper = $this->mysqli->select([
                        'source'		=> "{$tp}delkravtyper AS delkravtyper",
                        'where'			=> "aktiv AND !selvstendig_tillegg",
                        'orderfields'	=> "orden, id"
                    ])->data;

                    if($kravModell->hentId())	{
                        $resultat = (object)array(
                            'success'	=> true,
                            'data'		=> array(
                                'beløp'		=> abs($kravModell->hentBeløp()),
                                'tekst'		=> $kravModell->hentTekst(),
                                'type'		=> $kravModell->hentType(),
                                'andel'		=> str_replace('-', '', $krav->hent('andel')),
                                'kravdato'	=> ($kravModell->hentKravdato() ? $kravModell->hentKravdato()->format('Y-m-d') : null),
                                'forfall'	=> ($kravModell->hentForfall() ? $kravModell->hentForfall()->format('Y-m-d') : null),
                                'fom'		=> ($kravModell->hentFom() ? $kravModell->hentFom()->format('Y-m-d') : null),
                                'tom'		=> ($kravModell->hentTom() ? $kravModell->hentTom()->format('Y-m-d') : null),
                                'termin'	=> $kravModell->hentTermin(),
                                'anleggsnr'	=> $kravModell->hentAnlegg() ? $kravModell->hentAnlegg()->hentAnleggsnummer() : ''
                            )
                        );

                        foreach( $delkravtyper as $delkrav ) {
                            $resultat->data["delkrav{$delkrav->id}"] = abs($krav->hentDel($delkrav->id));
                        }
                    }
                    else {
                        $resultat = (object)array(
                            'success'	=> false,
                            'msg'		=> "Kunne ikke laste dette kravet pga. en feil."
                        );
                    }
                }
                return json_encode($resultat);
            }
        }
    }

    /**
     * @param string $skjema
     * @return string
     */
    public function taIMot($skjema):string {
        $id			= (int)$_GET['id'];
        /** @var LeieforholdModell $leieforholdModell */
        $leieforholdModell = $this->hentModell(LeieforholdModell::class, $_POST['leieforhold'] ?? null);
        $fortegn	= $_POST['fortegn'] ?? '';
        $beløp		= str_replace(",", ".", $_POST['beløp']);
        $type		= isset($_POST['type']) && in_array($_POST['type'], LeieforholdModell\Krav::$typer)
            ? $_POST['type']
            : LeieforholdModell\Krav::TYPE_ANNET;
        $tekst		= $_POST['tekst'] ?? '';
        $termin		= $_POST['termin'] ?? '';
        $andel		= $_POST['andel'] ?? '';
        $andel		= $andel != 0 ? new \Kyegil\Fraction\Fraction($andel) : null;
        $kravdato	= date_create($_POST['kravdato'] ?? 'now');
        $forfall	= isset($_POST['forfall']) && $_POST['forfall']
            ? date_create_from_format('Y-m-d', $_POST['forfall'])
            : null;
        $fom		= isset($_POST['fom']) && $_POST['fom']
            ? date_create_from_format('Y-m-d H:i:s', ($_POST['fom'] . ' 00:00:00'))
            : null;
        $tom		= isset($_POST['tom']) && $_POST['tom']
            ? date_create_from_format('Y-m-d H:i:s', ($_POST['tom'] . ' 00:00:00'))
            : null;
        /** @var stdClass[] $delkrav */
        $delkrav	= [];


        try {
            $delkravtyper = $this->hentDelkravtyper()
                ->leggTilFilter(['selvstendig_tillegg' => false])->låsFiltre();
            if (!isset($_POST['fortegn'])
                || ($fortegn != '-' && $fortegn != '')) {
                throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Mottok ikke korrekt informasjon om beløpet gjelder krav eller kreditt.');
            }
            foreach ($delkravtyper as $delkravtype) {
                if (isset($_POST["delkrav{$delkravtype->id}"])) {
                    $delkrav[] = (object)array(
                        'type' => $delkravtype->id,
                        'beløp' => $fortegn . str_replace(",", ".", $_POST["delkrav{$delkravtype->id}"])
                    );
                }
            }
            /** @var LeieforholdModell\Krav $eksisterendeKrav */
            $eksisterendeKrav = $this->hentModell(LeieforholdModell\Krav::class, $id);
            if(!$eksisterendeKrav->hentId()) {
                $eksisterendeKrav = null;
            }
            $tidligstMuligeKravdato = $this->tidligstMuligeKravdato();

            if (!$leieforholdModell->hentId() && !$id) {
                throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Leieforhold eller krav-id mangler. Prøv på nytt ifra leieforholdkortet eller ifra kravkortet.');
            }
            if ($beløp == 0) {
                throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Beløpet mangler.');
            }
            if (!$type) {
                throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Kravtype er ikke angitt.');
            }
            if (
                in_array($type, [
                    \Kyegil\Leiebasen\Modell\Leieforhold\Krav::TYPE_HUSLEIE,
                    \Kyegil\Leiebasen\Modell\Leieforhold\Krav::TYPE_STRØM])
                && (!$fom || !$tom)
            ) {
                throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Fra- og til-dato er påkrevet for ' . $type);
            }
            if (!$kravdato) {
                throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Kravdato (regnskapsdato) er ikke angitt. Det skal normalt være dagens dato, eller første leiedato for husleiekrav.');
            }
            if (!$tekst) {
                throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Tekstbeskrivelse av kravet mangler.');
            }
            if ($eksisterendeKrav) {
                $leieforholdModell = $eksisterendeKrav->hentLeieforhold();
                $fortegn = $eksisterendeKrav->hentBeløp() < 0 ? '-' : '';
            }
            if (!$leieforholdModell->hentId()) {
                throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Leieforhold mangler for nytt krav.');
            }

            if($andel && $andel->compare('<', 0)) {
                $andel->multiply(-1, true);
            }

            if ($eksisterendeKrav && $eksisterendeKrav->hentRegning()) {
                if (
                    $type != $eksisterendeKrav->hentType()
                    || $beløp != abs($eksisterendeKrav->hentBeløp())
                    || $tekst != $eksisterendeKrav->hentTekst()
                    || $termin != $eksisterendeKrav->hentTermin()
                    || $andel != $eksisterendeKrav->hentAndel()
                    || $kravdato != $eksisterendeKrav->hentKravdato()
                    || $fom != $eksisterendeKrav->hentFom()
                    || $tom != $eksisterendeKrav->hentTom()
                ) {
                    throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Kravet er sendt ut, og kun forfallsdato kan endres.<br>Du kan evt. kreditere kravet (slette det), og så opprette et nytt.');
                }

                foreach ($delkrav as $del) {
                    if ($del->beløp != $eksisterendeKrav->hentDelkravBeløp($del->type)) {
                        throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Kravet er sendt ut, og kun forfallsdato kan endres.<br>Du kan evt. kreditere (slette) kravet, og så opprette et nytt.');
                    }
                }
            }

            // Nye krav kan ikke tilbakedateres
            if (!$eksisterendeKrav && $tidligstMuligeKravdato && ($kravdato < $tidligstMuligeKravdato)) {
                $kravdato = new DateTime;
            }

            if ($type == LeieforholdModell\Krav::TYPE_HUSLEIE) {
                if (
                    !$fom
                    || !$tom
                    || !$andel
                ) {
                    throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Fra- og tildato, og andel er påkrevet for husleiekrav');
                }

                if($andel->compare('>', $leieforholdModell->leieobjekt->hentLedighetForTidsrom($fom, $tom, $leieforholdModell))) {
                    throw new \Kyegil\Leiebasen\Exceptions\Feilmelding("Andelen er for høy i forhold til hva som er ledig i {$leieforholdModell->leieobjekt->hentType()} {$leieforholdModell->leieobjekt} i det angitte tidsrommet. Plass må evt frigjøres ved å slette andres leiekrav først.");
                }
            }

            //	Om kravet er nytt kan det opprettes
            if (!$eksisterendeKrav) {
                try {
                    $krav = $leieforholdModell->opprettKravOgDelkrav(
                        $type,
                        $tekst,
                        $fortegn . $beløp,
                        $kravdato,
                        $forfall,
                        new Kyegil\Fraction\Fraction($fortegn . $andel),
                        $termin,
                        $fom,
                        $tom
                    );

                    foreach ($delkrav as $del) {
                        $krav->settDelkravBeløp($del->type, $del->beløp);
                    }
                } catch (Exception $e) {
                    throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Kravet kunne ikke opprettes.<br>' . $e->getMessage());
                }

                return json_encode([
                    'success' => true,
                    'id' => strval($krav),
                    'msg' => "",
                    'url' => (string)$this->returi->get(1)
                ]);
            } //	Eksisterende krav forsøkes endret
            else {
                $resultat = (object)array(
                    'success' => true,
                    'id' => strval($eksisterendeKrav),
                    'msg' => "",
                    'url' => (string)$this->returi->get(1)
                );

                if ($eksisterendeKrav->hentRegning() === null) {
                    $eksisterendeKrav->type     = $type;
                    $eksisterendeKrav->kravdato = $kravdato;
                    $eksisterendeKrav->fom      = $fom;
                    $eksisterendeKrav->tom      = $tom;
                    $eksisterendeKrav->tekst    = $tekst;
                    $eksisterendeKrav->termin   = $termin;
                    $eksisterendeKrav->andel    = $andel;
                    $eksisterendeKrav->beløp    = "{$fortegn}{$beløp}";

                    foreach ($delkrav as $del) {
                        $eksisterendeKrav->settDelkravBeløp($del->type, $del->beløp);
                    }
                }

                $eksisterendeKrav->forfall    = $forfall;

                return json_encode($resultat);
            }
        } catch (Exception $e) {
            return json_encode(array(
                'success' => false,
                'msg' => $e->getMessage()
            ));
        }
    }


    /**
     * @param $oppgave
     */
    public function oppgave($oppgave) {
        switch ($oppgave) {

            case "slett": {
                $id = !empty($_GET['id']) ? $_GET['id'] : null;
                $resultat = (object)array(
                    'success'		=> true,
                    'msg'			=> '',
                    'url'			=> (string)$this->returi->get(1)->url
                );
                try {
                    /** @var \Kyegil\Leiebasen\Modell\Leieforhold\Krav $krav */
                    $krav = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold\Krav::class, $id);
                    $krav->slett();
                    if (strpos($resultat->url, 'oppslag=krav_kort') !== false
                        && strpos($resultat->url, 'id='. $id) !== false) {
                        $resultat->url = (string)$this->returi->get(2)->url;
                    }
                } catch (Exception $e) {
                    $resultat->success  = false;
                    $resultat->msg = $e->getMessage();
                }

                return json_encode( $resultat );
            }

            default: {
                return json_encode( null );
            }
        }
    }
}