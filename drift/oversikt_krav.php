<?php

use Kyegil\Leiebasen\Leiebase;
if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    public $ext_bibliotek = 'ext-3.4.0';

            /**
     * @param array $di
     * @param array $config
     * @throws Throwable
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $this->fra = $this->mysqli->real_escape_string($_GET['fra'] ?? '');
        $this->til = $this->mysqli->real_escape_string($_GET['til'] ?? '');
        if(!$this->fra)
            $this->fra = date('Y-m-01');
        if(!$this->til)
            $this->til = date('Y-m-d', $this->leggtilIntervall(strtotime($this->fra), 'P1M')-86400);
        $this->hoveddata['sql'] = "SELECT krav.*\n"
            .    "FROM krav\n"
            .    "WHERE kravdato >= '$this->fra'\n"
            .    "AND kravdato <= '$this->til'\n"
            .    (!empty($_GET['kravtype']) ? ("AND type = '" . $this->mysqli->real_escape_string($_GET['kravtype']) . "'\n") : "")
            .    "ORDER BY kravdato, kontraktnr, id";
    }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        if (isset($_GET['returi']) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }
?>

Ext.onReady(function() {
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';

    // oppretter datasettet
    var datasett = new Ext.data.JsonStore({
        url: "/drift/index.php?oppdrag=hentdata&oppslag=<?=$_GET['oppslag'];?>&fra=<?=$this->fra;?>&til=<?=$this->til;?><?=(!empty($_GET['kravtype'])) ? ("&kravtype=" . $_GET['kravtype']) : "";?>",
        fields: [
            {name: 'id', type: 'float'},
            {name: 'kravdato', type: 'date', dateFormat: 'Y-m-d'},
            {name: 'opprettet', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {name: 'oppretter'},
            {name: 'kontraktnr', type: 'float'},
            {name: 'kontraktpersoner'},
            {name: 'type'},
            {name: 'termin'},
            {name: 'tekst'},
            {name: 'gironr', type: 'float'},
            {name: 'beløp', type: 'float'},
            {name: 'utestående', type: 'float'}
        ],
        root: 'data'
    });
    datasett.load();

    var id = {
        align: 'right',
        dataIndex: 'id',
        header: 'ID',
        hidden: true,
        sortable: true,
        width: 50
    };

    var dato = {
        dataIndex: 'kravdato',
        header: 'Dato',
        hidden: false,
        renderer: Ext.util.Format.dateRenderer('d.m.Y'),
        sortable: true,
        width: 70
    };

    var kontraktnr = {
        dataIndex: 'kontraktnr',
        header: 'Leieavtale',
        renderer: function(value, metaData, record, rowIndex, colIndex, store){
            return "<a href=\"/drift/index.php?oppslag=leieforholdkort&id=" + value + "\">" + value + "</a> " + record.data.kontraktpersoner;
        },
        sortable: true,
        width: 160
    };

    var type = {
        dataIndex: 'type',
        header: 'Kravtype',
        sortable: true,
        width: 50
    };

    var termin = {
        dataIndex: 'termin',
        header: 'Termin',
        sortable: true,
        width: 130
    };

    var tekst = {
        dataIndex: 'tekst',
        header: 'Tekst',
        sortable: true,
        width: 50
    };

    var giro = {
        align: 'right',
        dataIndex: 'gironr',
        header: 'Giro',
        renderer: function(v){
            if(!v) return "";
            return "<a href=\"/drift/index.php?oppslag=giro&oppdrag=lagpdf&gironr=" + v + "\">" + v + "</a>";
        },
        sortable: true,
        width: 50
    };

    var beløp = {
        align: 'right',
        dataIndex: 'beløp',
        header: 'Beløp',
        renderer: Ext.util.Format.noMoney,
        sortable: true,
        width: 70
    };

    var utestående = {
        align: 'right',
        dataIndex: 'utestående',
        header: 'Utestående',
        renderer: Ext.util.Format.noMoney,
        sortable: true,
        width: 80
    };

    var opprettet = {
        dataIndex: 'opprettet',
        header: 'Opprettet',
        hidden: true,
        renderer: Ext.util.Format.dateRenderer('d.m.Y H:i'),
        sortable: true,
        width: 110
    };

    var oppretter = {
        dataIndex: 'oppretter',
        header: 'Registrert av',
        hidden: true,
        sortable: true,
        width: 120
    };


    var rutenett = new Ext.grid.GridPanel({
        buttons: [{
            text: '<< 1 mnd. <<',
            handler: function() {
                window.location = "/drift/index.php?oppslag=<?=$_GET['oppslag']?>&fra=<?=date('Y-m-01', $this->leggtilIntervall(strtotime($this->fra), 'P-1M'));?>&til=<?=date('Y-m-d', ($this->leggtilIntervall(strtotime(date('Y-m-01', $this->leggtilIntervall(strtotime($this->fra), 'P-1M'))), 'P1M')-86400));?>";
            }
        }, {
            text: 'Tilbake',
            handler: function() {
                window.location = "/drift/index.php?oppslag=oversikt_innbetalinger";
            }
        }, {
            text: '>> 1 mnd. >>',
            handler: function() {
                window.location = "/drift/index.php?oppslag=<?=$_GET['oppslag']?>&fra=<?=date('Y-m-d', $this->leggtilIntervall(strtotime($this->fra), 'P1M'));?>&til=<?=date('Y-m-d', ($this->leggtilIntervall(strtotime($this->fra), 'P2M')-86400));?>";
            }
        }, {
            text: 'Utskriftsversjon',
            handler: function() {
                window.open("/drift/index.php?oppdrag=utskrift&oppslag=oversikt_krav<?=(!empty($_GET['kravtype'])) ? ("&kravtype=" . $_GET['kravtype']) : "";?>&fra=<?=$this->fra?>&til=<?=$this->til?>");
            }
        }],
        store: datasett,
        columns: [
            id,
            dato, 
            kontraktnr,
            type,
            termin,
            tekst,
            giro,
            beløp,
            utestående,
            opprettet,
            oppretter
        ],
        autoExpandColumn: 5,
        stripeRows: true,
        height: 600,
        title: "<?=(!empty($_GET['kravtype']) ? $_GET['kravtype'] : 'Alle ') . 'krav ' . date('d.m.Y', strtotime($this->fra)) . ' - ' . date('d.m.Y', strtotime($this->til))?>"
    });

    // Rutenettet rendres in i HTML-merket '<div id="adresseliste">':
    rutenett.render('panel');

});
<?php
        return ob_get_clean();
    }

    /**
     *
     */
    public function design() {
?>
<div id="panel" class="extjs-panel"></div>
<?php
    }

    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ""):string {
        switch ($data) {
            default:
                $resultat = $this->arrayData($this->hoveddata['sql']);
                foreach($resultat['data'] as $linje=>$detaljer){
                    $resultat['data'][$linje]['kontraktpersoner'] = Leiebase::liste($this->kontraktpersoner($detaljer['kontraktnr']));
                }
                return json_encode($resultat);
        }
    }

    public function utskrift(){
        ob_start();
        $kravsett = $this->arrayData($this->hoveddata['sql']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="no" lang="no">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title><?=($_GET['kravtype'] ?? 'Alle ') . 'krav ' . date('d.m.Y', strtotime($this->fra)) . ' - ' . date('d.m.Y', strtotime($this->til))?></title>
    <link rel="stylesheet" type="text/css" href="/pub/css/leiebase.css">
    <link rel="stylesheet" type="text/css" href="/pub/lib/<?=$this->ext_bibliotek?>/resources/css/ext-all.css">
    <link rel="stylesheet" type="text/css" href="/pub/lib/<?=$this->ext_bibliotek?>/resources/css/xtheme-slate.css">
    <script language="JavaScript" type="text/javascript" src="/pub/lib/<?=$this->ext_bibliotek?>/adapter/ext/ext-base.js"></script>

    <script language="JavaScript" type="text/javascript" src="/pub/lib/<?=$this->ext_bibliotek?>/ext-all.js"></script>
    <script language="JavaScript" type="text/javascript" src="/pub/lib/<?=$this->ext_bibliotek?>/src/locale/ext-lang-no_NB.js"></script>
    <script language="JavaScript" type="text/javascript" src="/pub/js/fellesfunksjoner.js"></script>
    <style type="text/css">
    td, th, p {font-size: x-small; border-color: #909090;}
    </style>
</head>

<body>
<h1><?=($_GET['kravtype'] ?? 'Alle ') . 'krav ' . date('d.m.Y', strtotime($this->fra)) . ' - ' . date('d.m.Y', strtotime($this->til))?></h1>
<p></p>
<table style="text-align: left; width: 100%;" border="0" cellpadding="2" cellspacing="0">
<tbody>
<?php
        foreach($kravsett['data'] as $krav){
            $bakgrunn = !empty($bakgrunn) ? "" : "background-color: #E0E0E0;";
?>
    <tr style="vertical-align:top;">
        <td style="width: 60px; text-align: right;  border-top-style: solid; border-width: thin; <?=$bakgrunn?>"><?=date('d.m.Y', strtotime($krav['kravdato']))?></td>
        <td style="width: 40px; text-align: right; border-top-style: solid; border-width: thin; <?=$bakgrunn?>"><?=$krav['kontraktnr']?></td>
        <td style="width: 200px; text-indent: 2px; border-top-style: solid; border-width: thin; <?=$bakgrunn?>"><?=Leiebase::liste($this->kontraktpersoner($krav['kontraktnr']))?></td>
        <td style="border-top-style: solid; border-width: thin; <?=$bakgrunn?>"><?=$krav['type']?></td>
        <td style="border-top-style: solid; border-width: thin; <?=$bakgrunn?>"><?=$krav['termin']?></td>
        <td style="width: 40px; text-align: right; border-top-style: solid; border-width: thin; <?=$bakgrunn?>"><?=$krav['id']?></td>
        <td style="text-indent: 2px; border-top-style: solid; border-width: thin; <?=$bakgrunn?>"><?=$krav['tekst']?></td>
        <td style="width: 70px; text-align: right; border-top-style: solid; border-width: thin; font-weight: bold; <?=$bakgrunn?>">kr. <?=number_format($krav['beløp'], 2, ",", " ")?></td>
    </tr>
<?php
        }
?>
</tbody>
</table>
<script type="text/javascript">
    window.print();
</script>
</body>
</html>
<?php
    return ob_get_clean();
    }

    /**
     * @param string $skjema
     * @return string
     */
    public function taIMot($skjema):string {
        return json_encode(null);
    }
}