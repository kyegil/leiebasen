<?php

use Kyegil\Leiebasen\Leiebase;use Kyegil\Leiebasen\Modell\Leieforhold;use Kyegil\Leiebasen\Modell\Leieforhold\Krav as KravModell;use Kyegil\Leiebasen\Modell\Leieforhold\Regning;

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    /**
     * @var string
     */
    public $ext_bibliotek = 'ext-3.4.0';


    /**
     * oppsett constructor.
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
    }

    /**
     * @return string
     */
    public function skript():string {
                ob_start();
        $this->returi->reset();
?>

Ext.onReady(function() {

    var statistikk1 = new Ext.Panel({
        autoScroll: true,
        autoLoad: '/drift/index.php?oppslag=forsiden&oppdrag=hentdata&data=statistikk1',
        bodyStyle: 'padding: 2px',
        border: false,
        collapsible: true,
        collapsed: false,
        title: 'Oppsummert'
    });


    var statistikk2 = new Ext.Panel({
        autoScroll: true,
        autoLoad: '/drift/index.php?oppslag=forsiden&oppdrag=hentdata&data=statistikk2',
        bodyStyle: 'padding: 2px',
        border: false,
        collapsible: true,
        collapsed: false,
        title: 'Oppgjør'
    });

    var datasett = new Ext.data.JsonStore({
        model: 'Internmelding',
        url: '/drift/index.php?oppslag=forsiden&oppdrag=hentdata&data=internmeldinger',
        autoLoad: true,
        fields: [
            {name: 'id', type: 'float'},
            {name: 'tekst'},
            {name: 'intro'},
            {name: 'navn'},
            {name: 'avsender'},
            {name: 'tidspunkt', type: 'date', dateFormat: 'Y-m-d H:i:s'}
        ],
        root: 'data'
    });
    datasett.load();

    var hovedpanel = new Ext.Panel({
        layout:'border',
        defaults: {
            collapsible: true,
            split: true,
            bodyStyle: 'padding: 15px'
        },
        items: [{
            title: '',
            collapsible: false,
            region:'center',
            margins: '5 0 0 0',
            layout:'column',
            items: [{
                bodyStyle: 'padding: 3px',
                border: false,
                title: '',
                columnWidth: .5,
                items: [statistikk1]
            },{
                bodyStyle: 'padding: 3px',
                border: false,
                title: '',
                columnWidth: .5,
                items: [statistikk2]
            }]
        }],
        title: '',
        height: 600
    });

    hovedpanel.render('panel');
    
});
<?php
        return ob_get_clean();
    }

    /**
     *
     */
    public function design() {
?>
<div id="panel" class="extjs-panel"></div>
<?php
    }

    public function manipuler($data){
        return json_encode(null);
    }


    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ""):string {
        $tp = $this->mysqli->table_prefix;

        switch ($data) {
            case "statistikk1": ob_start();?>
        <b>Innbetalinger:</b>
        <table>
    <?php
                foreach( $this->mysqli->select([
                    'fields'        => [
                        'beløp'            => "SUM(beløp)",
                        'måned'            => "DATE_FORMAT(dato, '%Y-%m-01')"
                    ],
                    'source'        =>
                        "`{$tp}innbetalinger` AS `innbetalinger`\n"
                        . "LEFT JOIN `{$tp}eav_verdier` AS `eav_verdier`\n"
                        . "ON `innbetalinger`.`innbetalingsid` = `eav_verdier`.`objekt_id`\n"
                        . "AND `eav_verdier`.`modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Innbetaling\\\\Delbeløp'\n"
                        . "AND `eav_verdier`.`kode` = 'er_fremmed_beløp'\n",
                    'where'            => [
                        '`innbetalinger`.`konto` <>'        => '0',
                        '`eav_verdier`.`verdi`'        => null,

                    ],
                    'groupfields'    => "måned",
                    'orderfields'    => "måned DESC",
                    'limit'            => 4
                ])->data as $betalt ):
    ?>
            <tr>
                <td>
                    <a href="/drift/index.php?oppslag=oversikt_kontobevegelser&fra=<?php echo  $betalt->måned ?>&til=<?php echo  date('Y-m-t', strtotime($betalt->måned))?>"><?php echo  \IntlDateFormatter::formatObject(new DateTime($betalt->måned . "-01"), 'MMMM y', 'nb_NO') ?></a>:
                </td>
                <td style="text-align: right;">kr <?php echo  str_replace(" ", "&nbsp;", number_format($betalt->beløp, 2, ",", " ")) ?></td>
            </tr>
        <?php         endforeach;
    ?>
        </table>
        <a href="/drift/index.php?oppslag=oversikt_innbetalinger">se mer ...</a><br>
        
    <?php
        
                $resultat = $this->mysqli->select([
                    'source'    => "`{$tp}ocr_transaksjoner` AS ocr_transaksjoner",
                    'fields'    => [
                        'oppgjørsdato'    => "MAX(`oppgjørsdato`)"
                    ]
                ])->data;
                if( isset( $resultat[0] ) && $resultat[0]->oppgjørsdato):
    ?>
            Siste <a href="/drift/index.php?oppslag=ocr_liste">OCR-fil</a>: <?php echo date('d.m.Y', strtotime($resultat[0]->oppgjørsdato));?><br>
    <?php
        endif;
        
            $resultat = $this->mysqli->select([
                'source'        => "{$tp}innbetalinger AS innbetalinger",
                'fields'        => ["registrerer", "registrert"],
                'where'            => [
                    'konto <>'            => '0',
                    'ocr_transaksjon'    => false
                ],
                'orderfields'    => "registrert DESC",
                'limit'            => 1
            ])->data;
            if( isset( $resultat[0] ) && $resultat[0]->registrert):
    ?>
            Siste manuelle <a href="/drift/index.php?oppslag=innbetalinger">registrering av betaling</a>: <?php echo date('d.m.Y', strtotime($resultat[0]->registrert)) ?><br>
    <?php
                endif;
    ?>
        <br>Annet:<br>
    <?php
        
            $resultat = $this->mysqli->select([
                'source'        => "{$tp}giroer AS giroer",
                'fields'        => array(
                    'utskriftsdato'    => "MAX(utskriftsdato)"
                )
            ])->data;
            if( isset( $resultat[0] ) && $resultat[0]->utskriftsdato):
    ?>Siste giroutskrift: <?php echo  date('d.m.Y', strtotime($resultat[0]->utskriftsdato)) ?><br><?php
        endif;
        
            $resultat = $this->mysqli->select([
                'source'        => "{$tp}purringer AS purringer",
                'fields'        => [
                    'purredato'    => "MAX(purredato)"
                ]
            ])->data;
            if( isset( $resultat[0] ) && $resultat[0]->purredato):
    ?>Siste purring: <?php echo  date('d.m.Y', strtotime($resultat[0]->purredato)) ?><br>
    <?php
            endif;
            
            $resultat = $this->mysqli->select([
                'source'        => "`{$tp}kostnadsdeling_kostnader` AS `kostnadsdeling_kostnader`",
                'fields'        => ['termin', 'fradato', 'tildato', 'fordelt'],
                'orderfields'    => '`tildato` DESC, `fordelt` DESC',
                'limit'            => 1
            ])->data;
            if( isset( $resultat[0] ) ):
    ?>
            Siste <?php echo ($resultat[0]->fordelt ? "fordelte" : "registrerte") ?> <?php if($resultat[0]->fordelt):?><a href="/drift/index.php?oppslag=delte_kostnader_kostnad_arkiv"><?php else:?><a href="/drift/index.php?oppslag=delte_kostnader_kostnad_liste"><?php endif;?>fellesstrøm</a>:
            <span title="Fellesstrøm for perioden : <?php echo  date('d.m.Y', strtotime($resultat[0]->fradato)) ?> - <?php echo  date('d.m.Y', strtotime($resultat[0]->tildato)) ?>">termin <?php echo  $resultat[0]->termin ?></span>
            <?php if(!$resultat[0]->fordelt):?>
                <span title="Den foreslåtte fordelingen må bekreftes før den vil bli krevd inn."> (Fordelingen er ikke bekreftet.)</span>
            <?php endif;?>
            <br>
            <br>
    <?php
        endif;
        
                $resultat = $this->mysqli->select([
                    'source'        => "{$tp}skader AS skader INNER JOIN {$tp}bygninger AS bygninger ON skader.bygning=bygninger.id",
                    'fields'        => ["skader.skade", "skader.registrert", "bygninger.navn"],
                    'where'            => [
                        'utført'    => null
                    ],
                    'orderfields'    => "skader.registrert DESC",
                    'limit'            => 1
                ])->data;
                if( isset( $resultat[0] ) ):
    ?>
            Siste <a href="/drift/index.php?oppslag=skadeliste">skademelding</a>:<br>
            <?php echo  $resultat[0]->skade ?> i <?php echo  $resultat[0]->navn ?><br>meldt <?php echo  date( "d.m.Y", strtotime( $resultat[0]->registrert ) ) ?><br>
    <?php
                endif;
        
                return ob_get_clean();
        

            case "statistikk2":
                ob_start();
                $oppgjør = "(SELECT id AS kravid, utestående, !utestående AS oppgjort, IFNULL(krav.forfall, krav.kravdato) AS forfall, IF(!utestående, MAX(innbetalinger.dato), NOW()) AS oppgjørsdato, DATEDIFF(IFNULL(krav.forfall, krav.kravdato), IF(!utestående, MAX(innbetalinger.dato), NOW())) AS oppfyllelse\n"
                    .    "FROM {$tp}krav AS krav LEFT JOIN {$tp}innbetalinger AS innbetalinger ON krav.id = innbetalinger.krav\n"
                    .    "GROUP BY krav.id)\n"
                    .    "AS oppgjør";
                $resultat = $this->mysqli->select([
                    'source'    => "{$tp}krav AS krav INNER JOIN $oppgjør ON krav.id = oppgjør.kravid\n",
                    'fields'    => [
                        'totalt'    => "COUNT(oppgjør.kravid)",
                        'oppgjort'    => "SUM(oppgjør.oppgjort)"
                    ],
                    'where'            => [
                        'krav.type'            => "Husleie",
                        'oppgjør.forfall <= NOW()',
                        'oppgjør.forfall > DATE_SUB(NOW(), INTERVAL 1 MONTH)'
                    ]
                ])->data[0];
                $ant_leier = $resultat->totalt;
                echo "<span title=\"{$resultat->oppgjort} av totalt {$resultat->totalt}\">{$this->prosent($resultat->oppgjort/(max($ant_leier, 1)))}</span> av leier forfalt siste måned er betalt.<br>";
        
        
                $resultat = $this->mysqli->select([
                    'source'    => "{$tp}krav AS krav INNER JOIN $oppgjør ON krav.id = oppgjør.kravid",
                    'fields'    => [
                        'oppgjort'    => "COUNT(oppgjør.kravid)"
                    ],
                    'where'        => [
                        'krav.type'        => "Husleie",
                        'oppgjør.forfall <=NOW()',
                        'oppgjør.forfall > DATE_SUB(NOW(), INTERVAL 1 MONTH)',
                        'oppgjørsdato <= oppgjør.forfall'
                    ]
                ])->data[0];
        
                echo "<span title=\"{$resultat->oppgjort} av totalt $ant_leier\">{$this->prosent($resultat->oppgjort/max($ant_leier, 1))}</span> ble betalt innen forfall.<br>";
                
                echo "<br>";
        
        
                $resultat = $this->mysqli->select([
                    'source'    => "{$tp}krav AS krav",
                    'fields'    => [
                        'utestående'    => "SUM(utestående)",
                        'totalt'        => "SUM(beløp)"
                    ],
                    'where'        => [
                        'IFNULL(krav.forfall, krav.kravdato) <=NOW()',
                        'IFNULL(krav.forfall, krav.kravdato) > DATE_SUB(NOW(), INTERVAL 1 YEAR)'
                    ]
                ])->data[0];
        
                echo "Utestående siste 12 mnd: <span title=\"Av totalt {$this->kr($resultat->totalt, false)}"
                . ($resultat->totalt != 0 ? " (Dvs. {$this->prosent($resultat->utestående/$resultat->totalt, 1, false)})\">{$this->kr($resultat->utestående)}</span><br>" : '');
        
                
                $resultat = $this->mysqli->select([
                    'source'    => "{$tp}krav AS krav",
                    'fields'    => [
                        'utestående'    => "SUM(utestående)",
                        'totalt'        => "SUM(beløp)"
                    ],
                    'where'    => [
                        'IFNULL(krav.forfall, krav.kravdato) <= DATE_SUB(NOW(), INTERVAL 2 MONTH)',
                        'IFNULL(krav.forfall, krav.kravdato) > DATE_SUB(NOW(), INTERVAL 1 YEAR)'
                    ]
                ])->data[0];
        
                echo "- Minus siste 2 måneder: <span title=\"Av totalt {$this->kr($resultat->totalt, false)}"
                . ($resultat->totalt != 0 ? " (Dvs. {$this->prosent($resultat->utestående/$resultat->totalt, 1, false)})\">{$this->kr($resultat->utestående)}</span><br>" : '');
                
        }
                return ob_get_clean();
    }

    }