<?php

use Kyegil\Leiebasen\Modell\Leieforhold\AvtaleMal;

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    /** @var string */
    public $tittel = 'leiebasen';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * oppsett constructor.
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
    }

    /**
     * Pre HTML
     *
     * @return bool Sann for å skrive ut HTML-malen, usann for å stoppe den
     * @throws Exception
     */
    public function preHTML() {
        /** @var AvtaleMal $mal */
        $mal = $this->hentModell(AvtaleMal::class, $this->GET['id']);

        if($this->GET['id'] == '*') {
            $this->tittel = "Opprett ny mal | Leiebasen";
            return parent::preHTML();
        }

        else if( !$mal->hentId() ) {
            header("Location: index.php?oppslag=innstillinger");
            return false;
        }

        else {
            $this->tittel = $mal->hentMalnavn();
            return parent::preHTML();
        }
    }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        if (isset($_GET['returi']) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }

        /** @var AvtaleMal $mal */
        $mal = $this->hentModell(AvtaleMal::class, $this->GET['id']);

?>

    Ext.onReady(function() {

        Ext.tip.QuickTipManager.init();
        Ext.form.Field.prototype.msgTarget = 'side';
        Ext.Loader.setConfig({enabled:true});

        bekreftSletting = function() {
            Ext.Msg.show({
                title: 'Bekreft',
                msg: 'Er du sikker på at du vil slette malen?',
                buttons: Ext.Msg.OKCANCEL,
                fn: function(buttonId, text, opt){
                    if(buttonId == 'ok') {
                        utførSletting();
                    }
                },
                animEl: 'elId',
                icon: Ext.MessageBox.QUESTION
            });
        }


        utførSletting = function(){
            Ext.Ajax.request({
                waitMsg: 'Sletter...',
                url: "/drift/index.php?oppslag=<?=$_GET['oppslag']?>&oppdrag=oppgave&oppgave=slett&id=<?=$_GET['id']?>",
                success: function(response, options){
                    var tilbakemelding = Ext.JSON.decode(response.responseText);
                    if(tilbakemelding['success'] == true) {
                        Ext.MessageBox.alert('Utført', 'Malen er slettet', function(){
                            window.location = '<?=$this->returi->get();?>';
                        });
                    }
                    else {
                        Ext.MessageBox.alert('Hmm..',tilbakemelding['msg']);
                    }
                }
            });
        }

    var malnavn = Ext.create('Ext.form.field.Text', {
        fieldLabel: 'Mal',
        width: '100%',
        labelWidth: 120,
        name: 'malnavn',
        tabIndex: 1,
        value: <?php echo json_encode($mal->hentMalnavn());?>
    });

    var maltype = Ext.create('Ext.form.field.ComboBox', {
        fieldLabel: 'Type',
        name: 'type',
        width: '100%',
        labelWidth: 120,
        tabIndex: 2,
        allowBlank:    false,

        store: Ext.create('Ext.data.Store', {
            fields: ['type'],
            data : [{type: 'leieavtale'}, {type: 'framleieavtale'}, {type: 'betalingsplan'}]

        }),
        queryMode: 'local',
        displayField: 'type',
        valueField: 'type',
        forceSelection: true,
        value: <?php echo json_encode($mal->hentType());?>
    });

    var mal = Ext.create('Ext.form.field.HtmlEditor', {
        fieldLabel: 'Tekst',
        labelWidth: 120,
        height: 300,
        name: 'mal',
        tabIndex: 4,
        value: <?php echo json_encode($mal->hentMal());?>
    });

    var lagreknapp = Ext.create('Ext.Button', {
        text: 'Lagre',
        handler: function(){
            skjema.form.submit({
                url:'/drift/index.php?oppslag=<?="{$_GET['oppslag']}&id={$_GET["id"]}";?>&oppdrag=taimotskjema',
                waitMsg:'Prøver å lagre...'
                });
        }
    });

    var skjema = Ext.create('Ext.form.Panel', {
        autoScroll: true,
        bodyPadding: 5,
        items: [malnavn, maltype, mal],
        frame: true,
        title: '<?=(($_GET['id']== '*') ? "Ny mal" : $mal->hentMalnavn())?>',
        renderTo: 'panel',
        height: 600,
        buttons: [
            {
                text: 'Tilbake',
                handler: function() {
                    window.location = '<?=$this->returi->get();?>';
                }
            },
        lagreknapp,
        {
            text: 'Slett',
            handler: bekreftSletting
        }
        ]
    });

    skjema.on({
        actioncomplete: function(form, action){
            if(action.type == 'submit'){
                if(action.response.responseText == '') {
                    Ext.MessageBox.alert('Problem', 'Det kom en blank respons fra tjeneren.');
                } else {
                    var result = Ext.JSON.decode(action.response.responseText);
                    Ext.MessageBox.alert('Suksess', 'Opplysningene er oppdatert');
                    window.location = '/drift/index.php?oppslag=avtalemal_skjema&id=' + result.id;
                }
            }
        },

        actionfailed: function(form,action){
            if(action.type == 'submit') {
                if (action.failureType == "connect") {
                    Ext.MessageBox.alert('Problem:', 'Klarte ikke lagre data. Fikk ikke kontakt med tjeneren.');
                }
                else {
                    var result = Ext.JSON.decode(action.response.responseText);
                    if(result && result.msg) {
                        Ext.MessageBox.alert('Mottatt tilbakemelding om feil:', result.msg);
                    }
                    else {
                        Ext.MessageBox.alert('Problem:', 'Lagring av data mislyktes av ukjent grunn.');
                    }
                }
            }

        } // end actionfailed listener
    }); // end skjema.on
});
<?php
        return ob_get_clean();
    }

    /**
     *
     */
    public function design() {
?>
    <div id="panel" class="extjs-panel"></div>
<?php
    }

    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ""):string {
        $id = isset($_GET['id']) ? (int)$_GET['id'] : null;
        /** @var AvtaleMal $mal */
        $mal = $this->hentModell(AvtaleMal::class, $id);

        switch ($data) {

            default: {
                $resultat = (object)[
                    'success' => false,
                    'msg'  => ''
                ];
                return json_encode($resultat);
            }
        }
    }

    /**
     * @param string $skjema
     * @return string
     */
    public function taIMot($skjema):string {
        switch ($skjema) {
            default:
                $resultat = (object)[
                    'success' => true,
                    'msg'  => '',
                    'id'   => null
                ];

                $id = isset($_GET['id']) ? $_GET['id'] : null;
                try {
                    if(!isset($_POST['malnavn'])) {
                        throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Malnavn må oppgis');
                    }
                    if(!isset($_POST['mal'])) {
                        throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Maltekst må oppgis');
                    }
                    $malnavn = $_POST['malnavn'];
                    $maltekst = $_POST['mal'];
                    $maltype = $_POST['type'];

                    if ((int)$id) {
                        /** @var AvtaleMal $mal */
                        $mal = $this->hentModell(AvtaleMal::class, $id);
                    }
                    else if ($id == '*') {
                        $mal = $this->nyModell(AvtaleMal::class);
                    }
                    else {
                        throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Denne malen finnes ikke');
                    }
                    $mal->settMalnavn($malnavn)
                        ->settType($maltype)
                        ->settMal($maltekst)
                        ->settErFramleiemal($maltype == AvtaleMal::TYPE_FRAMLEIEAVTALE)
                    ;

                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                return json_encode($resultat);
        }
    }

    /**
     * @param $oppgave
     */
    public function oppgave($oppgave) {
        $resultat = (object)[
            'success' => true,
            'msg'  => '',
            'id'   => null
        ];

        $id = (int)$_GET['id'];

        try {
            switch ($oppgave) {
                case "slett":
                    /** @var AvtaleMal $mal */
                    $mal = $this->hentModell(AvtaleMal::class, $id);
                    $mal->slett();
                    break;
                default:
                    break;
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        return json_encode($resultat);
    }
}