<?php

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    public $tittel = 'Oversikt over anvendte gironummer';
    public $ext_bibliotek = 'ext-4.2.1.883';
    

    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);

        $tp = $this->mysqli->table_prefix;

        if( isset($_GET['oppdrag'] ) and $_GET['oppdrag'] == 'utskrift' ) {
            $this->mal = "_utskrift";
        }

        $fom = @$_GET['fom'] ?? null;
        $tom = @$_GET['tom'] ?? null;

        $order = "{$tp}giroer.gironr ASC";

        $filter = "1\n";
        $filter .= $fom ? "AND {$tp}giroer.gironr >= '$fom'\n" : "";
        $filter .= $tom ? "AND {$tp}.giroer.gironr <= '$tom'\n" : "";

        $this->hoveddata['query'] = array();

        $giroer = $this->mysqli->arrayData(array(
            'source'        => "{$tp}giroer LEFT JOIN {$tp}krav ON {$tp}giroer.gironr = {$tp}krav.gironr\n",
            'where'            => $filter,
            'groupfields'    => "giroer.gironr\n",
            'orderfields'    => $order,

            'returnQuery'    => true,
            'fields'        => "giroer.gironr,\n"
                            .    "giroer.sammensatt,\n"
                            .    "giroer.utskriftsdato,\n"
                            .    "giroer.format,\n"
                            .    "if(count(krav.id), 0, 1) AS forkastet\n"
        ))->data;

        foreach( $giroer as $giro ) {

            // Dersom gruppa eksisterer
            if(
                isset($gruppe)
                && $gruppe->sammensatt == $giro->sammensatt
                && $gruppe->utskriftsdato == $giro->utskriftsdato
                && $gruppe->format == $giro->format
                && $gruppe->forkastet == $giro->forkastet
                && $gruppe->til == ($giro->gironr - 1)
            ) {
                $gruppe->til = $giro->gironr;
            }

            // Dersom ny gruppe må opprettes
            else {
                unset( $gruppe );
                $gruppe = (object)array(
                    'fra'            => $giro->gironr,
                    'til'            => $giro->gironr,
                    'sammensatt'    => $giro->sammensatt,
                    'utskriftsdato'    => $giro->utskriftsdato,
                    'format'        => $giro->format,
                    'forkastet'        => $giro->forkastet
                );

                $this->hoveddata['query'][] = &$gruppe;
            }
        }
    }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }
        $tp = $this->mysqli->table_prefix;
?>
Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', '<?=$this->http_host . '/pub/lib/' . $this->ext_bibliotek . "/examples/ux"?>');

Ext.require([
     'Ext.data.*',
     'Ext.form.field.*',
    'Ext.layout.container.Border',
     'Ext.grid.*',
    'Ext.ux.RowExpander'
]);

Ext.onReady(function() {

    Ext.tip.QuickTipManager.init();
    Ext.form.Field.prototype.msgTarget = 'side';
    Ext.Loader.setConfig({enabled:true});
    
    Ext.define('Girosett', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'fra', type: 'int'},
            {name: 'til', type: 'int'},
            {name: 'forkastet', type: 'bool'},
             {name: 'sammensatt', type: 'date', dateFormat: 'Y-m-d H:i:s', useNull: true},
             {name: 'utskriftsdato', type: 'date', dateFormat: 'Y-m-d H:i:s', useNull: true},
            {name: 'format', type: 'string'}
        ]
    });
    

    var datasett = Ext.create('Ext.data.Store', {
        model: 'Girosett',
        pageSize: 200,
        remoteSort: true,
        proxy: {
            type: 'ajax',
            simpleSortMode: true,
            url: "/drift/index.php?oppslag=<?=$_GET['oppslag']?>&oppdrag=hentdata",
            reader: {
                type: 'json',
                root: 'data',
                totalProperty: 'totalRows'
            }
        },
        sorters: [{
            property: 'fra',
            direction: 'ASC'
        }],
        autoLoad: true
    });
    
    datasett.on('beforeload', function() {
        datasett.getProxy().extraParams.fom = fom.getValue();
        datasett.getProxy().extraParams.tom = tom.getValue();
    });


    var fom = Ext.create('Ext.form.field.Number', {
        fieldLabel: 'Start fra gironr',
        labelAlign: 'left',
        listeners: {
            blur: function() {
                datasett.getProxy().extraParams.fom = 0;
                datasett.load();
                pagingtb.moveFirst();
            }
        },
        width: 200
    });

    var tom = Ext.create('Ext.form.field.Number', {
        fieldLabel: 'Til og med gironr',
        labelAlign: 'left',
        listeners: {
            blur: function() {
                datasett.getProxy().extraParams.tom = 0;
                datasett.load();
                pagingtb.moveFirst();
            }
        },
        width: 200
    });

    var girofilter = Ext.create('Ext.form.FieldSet', {
        title: 'Vis gironummer',
        layout: 'hbox',
        items: [fom, tom]
    });
    
    var pagingtb = Ext.create('Ext.toolbar.Paging',{
        store: datasett, 
        dock: 'bottom',
        displayInfo: true
    });

    var rutenett = Ext.create('Ext.grid.Panel', {
        title: 'Oversikt over anvendte gironummer',
        autoScroll: true,
        features: [{
            ftype: 'summary'
        }],
        layout: 'border',
        store: datasett,
        title: '',
        dockedItems: [pagingtb],
        columns: [
            {
                dataIndex: 'fra',
                text: 'Nummerserie',
                width: 100,
                hidden: false,
                sortable: true,
                flex: 1,
                renderer: function(value, metadata, record, rowIndex, colIndex, store){
                    var til = record.data.til;
                    if( value != til ) {
                        return value + ' – ' + til ;
                    }
                    else {
                        return value;
                    }
                    
                }
            },
            {
                dataIndex: 'sammensatt',
                text: 'Tatt i bruk',
                width: 200,
                renderer: Ext.util.Format.dateRenderer('d.m.Y'),
                sortable: true
            },
            {
                dataIndex: 'utskriftsdato',
                text: 'Skrevet ut',
                width: 200,
                renderer: function(value, metadata, record, rowIndex, colIndex, store){
                    var forkastet = record.data.forkastet;
                    if( forkastet ) {
                        return 'Forkastet';
                    }
                    else {
                        return Ext.Date.format(value, 'd.m.Y H:i:s');
                    }
                    
                },
                sortable: true
            },
            {
                dataIndex: 'format',
                text: 'Format',
                sortable: true
            }
        ],
        renderTo: 'panel',
        height: 600,
        tbar: [
            girofilter
        ],
        buttons: [{
            text: 'Tilbake',
            handler: function() {
                window.location = '<?=$this->returi->get();?>';
            }
        }, {
            text: 'Skriv ut',
            handler: function() {
                window.open('/drift/index.php?oppslag=rapport-anvendte-gironummer&oppdrag=utskrift' + (fom.getValue() ? '&fom=' + fom.getValue() : '') + (tom.getValue() ? '&tom=' + tom.getValue() : '') );
            }
        }]
    });


});
<?php
        return ob_get_clean();
    }

    public function design() {
?>
<div id="panel" class="extjs-panel"></div>
<?php
    }

    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ""):string {
        $tp = $this->mysqli->table_prefix;
        $sort        = @$_GET['sort'];
        $synkende    = @$_GET['dir'] == "DESC" ? true : false;
        $start        = (int)@$_GET['start'];
        $limit        = @$_GET['limit'];

        switch ($data) {
            default: {
                $resultat = (object)array(
                    'success'    => true,
                    'data'        => $this->hoveddata['query']
                );

                $resultat->totalRows = count($resultat->data);
                $resultat->data = array_slice(  $resultat->data, $start, $limit);
                return json_encode($resultat);
            }
        }
    }


    /**
     * @param string $skjema
     * @return string
     */
    public function taIMot($skjema):string {
        switch ($skjema) {

        default:
            return json_encode(null);
        }
    }


    public function oppgave($oppgave) {
        switch ($oppgave) {
            default:
                return json_encode(null);
        }
    }


    public function utskrift() {
        ob_start();

        $fom = $_GET['fom'] ?? null;
        $tom = $_GET['tom'] ?? null;
        $header = "Oversikt over anvendte gironummer";
        if( $fom && $tom ) {
            $header .= " i serien $fom – $tom";
        }
        else if( $fom ) {
            $header .= " fra gironummer $fom";
        }
        else if( $tom ) {
            $header .= " til og med gironummer $tom}";
        }

        $resultat = (object)array(
            'success'    => true,
            'data'        => $this->hoveddata['query']
        );

        $resultat->totalRows = count($resultat->data);

?>
<h1><?php echo $header;?></h1>
<table>
    <tbody>
        <tr>
            <th>Nummerserie</th>
            <th>Tatt i bruk</th>
            <th>Skrevet ut</th>
            <th>Format</th>
        </tr>
        <?php foreach( $resultat->data as $girosett ):?>
        <tr>
            <td><?php echo ( $girosett->fra == $girosett->til ) ? "{$girosett->fra}" : "{$girosett->fra} – {$girosett->til}";?></td>
            <td><?php echo date('d.m.Y', strtotime( $girosett->sammensatt ) );?></td>
            <td><?php echo $girosett->forkastet ? "Forkastet" : (  $girosett->utskriftsdato ? date('d.m.Y H:i:s', strtotime( $girosett->utskriftsdato ) ) : '');?></td>
            <td><?php echo $girosett->format;?></td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
<script type="text/javascript">
    window.print();
</script>
<?php
        return ob_get_clean();
    }
}