<?php /** @noinspection PhpMultipleClassDeclarationsInspection */

use Kyegil\Leiebasen\EventManager;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Innbetalingsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Oppslag\EksKontroller;
use Kyegil\Leiebasen\Vedlikehold;

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends EksKontroller {

    /** @var string */
    public $tittel = 'leiebasen';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * Pre HTML
     * *
     * Dersom transaksjonen tilhører kreditt
     * vil du bli videresent til denne kreditten istedetfor å se transaksjonen
     *
     * @return bool Sann for å skrive ut HTML-malen, usann for å stoppe den
     * @throws Exception
     */
    public function preHTML(): bool
    {
        $id = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Innbetaling|null $innbetaling */
        $innbetaling = $this->hentModell(Innbetaling::class, $id);
        if(!$innbetaling->hentId()) {
            throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Finner ikke transaksjonen ' . $id);
        }
        if($innbetaling->hent('konto') == '0') {
            $this->respons = new \Kyegil\Leiebasen\Respons\Omdirigering($this::url('krav_kort', $innbetaling->hentKredittkrav()));
            return parent::preHTML();
        }

        else{
            $this->tittel = 'Betaling: '
                . $this->kr($innbetaling->hentBeløp(), false)
                . " den " . $innbetaling->hentDato()->format('d.m.Y')
                . " | Leiebasen";
            return parent::preHTML();
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function skript():string {
        ob_start();
        if (isset($_GET['returi']) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }
        $id = !empty($_GET['id']) ? $_GET['id'] : null;

        /** @var Innbetaling $innbetalingModell */
        $innbetalingModell = $this->hentModell(Innbetaling::class, $id);
        $ocr = $innbetalingModell->hentOcrTransaksjon();
        $utbetaling = $innbetalingModell->hentBeløp() < 0;

        $html = "<h1>" . ($utbetaling ? 'Ut' : 'Inn') . "betaling på {$this->kr(abs($innbetalingModell->hentBeløp()))}"
        . (
            $innbetalingModell->hentBetaler()
            ? (($utbetaling ? ' til' : ' fra') . " {$innbetalingModell->hentBetaler()}")
            : ""
        )
        . " den {$innbetalingModell->dato->format('d.m.Y')}</h1>"
        .	"Registrert av {$innbetalingModell->registrerer} den {$innbetalingModell->registrert->format('d.m.Y')}<br><br>"
        .	"Konto: {$innbetalingModell->konto} " . ($ocr ? "<a title=\"Klikk her for å gå til OCR-forsendelsen\" href=\"/drift/index.php?oppslag=ocr_kort&id={$ocr->hentFil()->hentId()}\">[Vis OCR-forsendelsen]</a>" : "") . "<br>"
        .	($ocr ? "Transaksjonstype: {$ocr->hentTransaksjonsbeskrivelse()}<br>" : "")
        .	($ocr ? "KID: {$ocr->kid}<br>" : "")
        .	"Referanse: {$innbetalingModell->ref}<br>"
        .	"Merknad: {$innbetalingModell->merknad}<br>";

?>

Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', '<?php echo $this->http_host . '/pub/lib/' . $this->ext_bibliotek . "/examples/ux"?>');

Ext.require([
    'Ext.data.*',
    'Ext.form.field.*',
    'Ext.layout.container.Border',
    'Ext.grid.*',
    'Ext.grid.plugin.BufferedRenderer',
    'Ext.ux.RowExpander'
]);

Ext.onReady(function() {

    Ext.tip.QuickTipManager.init();
    Ext.form.Field.prototype.msgTarget = 'side';
    Ext.Loader.setConfig({enabled:true});

    /**
    * @param {int} innbetalingsid
    * @param {String} innbetaling
    * @param {String} tidlLeieforhold
    * @param {boolean} erFremmedBeløp Om det eksisterende beløpet er markert som fremmed beløp
    */
	angiLeieforhold = function( innbetalingsid, innbetaling, tidlLeieforhold = '', erFremmedBeløp = false) {
		Ext.Ajax.request({
			waitMsg: 'Løsner...',
			url: "/drift/index.php?oppslag=innbetalingskort&oppdrag=manipuler&data=frakople&id=<?php echo $innbetalingModell;?>",
			params: {
				innbetalingsid: innbetalingsid
			},
			success : function() {
				leieforhold.getStore().baseParams = {innbetaling: innbetaling};
				leieforhold.clearValue();
                if( erFremmedBeløp ) {
                    leieforhold.disable();
                }
                else {
                    leieforhold.enable();
                }

                /**
                * @type {Ext.form.field.Checkbox}
                * @link https://docs.sencha.com/extjs/4.2.2/#!/api/Ext.form.field.Checkbox
                */
                let erFremmedBeløpFelt = Ext.create('Ext.form.field.Checkbox', {
                    name: 'er_fremmed_beløp',
                    fieldLabel: 'Fremmed beløp',
                    boxLabel: 'Beløpet skal betraktes som fremmed og ignoreres',
                    checked: erFremmedBeløp,
                    inputValue: 1,
                    uncheckedValue: 0,
                    listeners: {
                        change: function( checkbox, newValue) {
                            if( newValue ) {
                                leieforhold.disable();
                            }
                            else {
                                leieforhold.enable();
                            }
                        }
                    }
                });

                let leieforholdvindu = Ext.create('Ext.window.Window', {
					bodyStyle:'padding:5px 5px 0',
					closeAction: 'hide',
					hideBorders: true,
					width: 600,
					height: 200,
					autoScroll: false,
					items: [
						{
							xtype: 'displayfield',
							value: '<b>Angi hvilket leieforhold beløpet gjelder.</b><br><br>Finn ønsket leieforhold ved å skrive navn eller leieforholdnummer:'
						},
                        erFremmedBeløpFelt,
						leieforhold
					],
					buttons: [{
						text: 'Avbryt',
						handler: function() {
							leieforholdvindu.hide();
						}
					}, {
						text: 'Lagre',
						handler: function() {
							Ext.Ajax.request({
								waitMsg: 'Angir leieforhold...',
								url: "/drift/index.php?oppslag=innbetalingskort&oppdrag=oppgave&oppgave=endreleieforhold&innbetaling=" + innbetaling + "&tidl_leieforhold=" + tidlLeieforhold + "&leieforhold=" + leieforhold.getValue() + "&er_fremmed_beløp=" + (erFremmedBeløpFelt.getValue() ? 1 : 0),
								success: function(response) {
									var tilbakemelding = Ext.JSON.decode(response.responseText);
									if(tilbakemelding['success'] == true) {
										Ext.MessageBox.alert('Utført', tilbakemelding.msg);
										indrepanel.getLoader().load('/drift/index.php?oppslag=<?php echo $_GET['oppslag'];?>&oppdrag=hentdata&id=<?php echo $innbetalingModell;?>');
										leieforholdvindu.hide();
									}
									else {
										Ext.MessageBox.alert('Hmm..',tilbakemelding['msg']);
									}
								}
							});
						}
					}]
				});
				leieforholdvindu.show();
			}
		});
	}	
	
	
	kople = function(innbetalingsid, beløp, leieforhold) {
		var utlikning = 0;
		
		var oppdater = function() {
			utlikning = 0;
			for (index = 0; index < kravsett.length; ++index) {
				var krav = kravsett[index];
				utlikning += (komb[krav.id].getValue()) * (verdi[krav.id].value);
			}

			utlikningsvindu.setTitle('Valgt krav for kr. ' + Ext.util.Format.noMoney(utlikning) + ' --- Rest kr. ' + Ext.util.Format.noMoney(beløp - utlikning));
			return utlikning;
		}

		var utlikningsskjema = Ext.create('Ext.form.Panel', {
			layout: 'column',
			autoScroll: true,
			labelAlign: 'top', // evt right
			standardSubmit: false,
			buttons: [{
				text: 'Lagre',
				handler: function() {
					utlikningsskjema.getForm().submit({
						url: '/drift/index.php?oppslag=innbetalingskort&oppdrag=manipuler&data=kople&id=<?php echo $innbetalingModell;?>',
						params: {
							leieforhold: leieforhold
						},
						success: function() {
							indrepanel.getLoader().load('/drift/index.php?oppslag=<?php echo $_GET['oppslag'];?>&oppdrag=hentdata&id=<?php echo $innbetalingModell;?>');
							utlikningsvindu.close();
						}
					});
				}
			}]
		});
		
		var utlikningsvindu = Ext.create('Ext.window.Window', {
			layout: 'fit',
			modal: true,
			title: 'Avstemming av innbetaling',
			width: 600,
			height: 400,
			items: [utlikningsskjema]
		});
		
		var kravsett = [];
		var komb = [];
		var verdi = [];

		Ext.Ajax.request({
			url: "/drift/index.php?oppslag=innbetalingskort&oppdrag=hentdata&data=utlikningsmuligheter&id=<?php echo $innbetalingModell;?>",
			params: {
				innbetalingsid: innbetalingsid,
				leieforhold: leieforhold
			},
			success : function(response) {
				var result = Ext.JSON.decode( response.responseText );
				if( result.success ) {
					kravsett = result.data;
					for (index = 0; index < kravsett.length; ++index) {
						var krav = kravsett[index];

						komb[krav.id] = Ext.create('Ext.form.field.Checkbox', {
							boxLabel: krav.tekst + ( krav.id ? " (" + krav.id + ")" : ''),
							hideLabel: true,
							inputValue: 1,
							listeners: {
								change( checkbox, checked ) {
									var startverdi = verdi[checkbox.getName().slice(4)].startverdi;
									if(checked) {
										var rest = beløp - utlikning;
										var maksverdi = (Math.abs(startverdi) < Math.abs(rest) ? startverdi : rest );
										verdi[checkbox.getName().slice(4)].enable();
										verdi[checkbox.getName().slice(4)].setValue(maksverdi);
									}
									else {
										verdi[checkbox.getName().slice(4)].disable();
										verdi[checkbox.getName().slice(4)].setValue(startverdi);
									}
									oppdater();
									verdi[checkbox.getName().slice(4)].validate();
								}
							},
							name: 'komb' + krav.id,
							width: 500
						});
						utlikningsskjema.add(komb[krav.id]);
					
						verdi[krav.id] = Ext.create('Ext.form.field.Number', {
							decimalSeparator: ',',
							hideTrigger: true,
							disabled: true,
							listeners: {change: oppdater},
							minValue: Math.min(krav.beløp, 0),
							maxValue: Math.max(krav.beløp, 0),
							name: 'verdi' + krav.id,
							value: krav.beløp,
							validator: function(v){
								resultat = oppdater();
								if(Math.abs(resultat) > Math.abs(beløp)) {
									verdi[krav.id].setValue(
										Math.abs(krav.beløp) < Math.abs(v - (resultat - beløp))
										? krav.beløp
										: (v - (resultat - beløp))
									);
								}
								return true;
							},
							width: 50
						});
						verdi[krav.id].startverdi = krav.beløp
						
						utlikningsskjema.add(verdi[krav.id]);

					}
					utlikningsvindu.show();
				}
				else {
				}
			 }
		});
		
	}

	frakople = function(innbetalingsid) {
		Ext.Ajax.request({
			waitMsg: 'Løsner...',
			url: "/drift/index.php?oppslag=innbetalingskort&oppdrag=manipuler&data=frakople&id=<?php echo $innbetalingModell;?>",
			params: {
				innbetalingsid: innbetalingsid
			},
			success : function(){
			 	indrepanel.getLoader().load('/drift/index.php?oppslag=innbetalingskort&oppdrag=hentdata&id=<?php echo $innbetalingModell;?>');
			 }
		});
	}

	let leieforhold = Ext.create('Ext.form.field.ComboBox', {
		fieldLabel: 'Leieforhold',
		hideLabel: false,
		name: 'leieforhold',
		width: 570,
		matchFieldWidth: false,
		listConfig: {
			width: 700
		},

		store: Ext.create('Ext.data.JsonStore', {
			storeId: 'leieforholdliste',
		
			autoLoad: false,
			proxy: {
				type: 'ajax',
				url: "/drift/index.php?oppslag=innbetalingskort&oppdrag=hentdata&data=leieforholdforslag",
				reader: {
					type: 'json',
					root: 'data',
					idProperty: 'leieforhold'
				}
			},
			
			fields: [
				{name: 'leieforhold'},
				{name: 'visningsfelt'}
			]
		}),
		queryMode: 'remote',
		displayField: 'visningsfelt',
		valueField: 'leieforhold',
		minChars: 0,
		queryDelay: 1000,

		allowBlank: true,
		typeAhead: false,
		editable: true,
		selectOnFocus: true,
		forceSelection: true
	});

	let indrepanel = Ext.create('Ext.panel.Panel', {
		autoLoad: '/drift/index.php?oppslag=innbetalingskort&oppdrag=hentdata&id=<?php echo $innbetalingModell;?>',
		layout: 'anchor',
		frame: true,
        bodyStyle: 'padding:5px',
		title: '',
		border: false
	});

	let panel = Ext.create('Ext.panel.Panel', {
		renderTo: 'panel',
        autoScroll: true,
        layout: 'anchor',
		border: false,
		items: [
			{
				xtype: 'displayfield',
				value: '<?php echo addslashes($html);?>'
			},
			indrepanel
		],
        bodyStyle: 'padding:5px',
		title: '',
		frame: true,
		height: 600,
		plain: true,
		buttons: [{
			text: 'Endre',
			handler: function() {
				window.location = '/drift/index.php?oppslag=betaling_skjema&id=<?php echo $innbetalingModell;?>';
			}
		}, {
			text: 'Tilbake',
			handler: function() {
				window.location = '<?php echo $this->returi->get();?>';
			}
		}]
	});

});
<?php
        return ob_get_clean();
    }

    /**
     * @return void
     */
    public function design() {
?>
<div id="panel" class="extjs-panel"></div>
<?php
    }

    /**
     * @param string $data
     * @return string
     * @throws Exception
     */
    public function hentData( $data = "" ):string {
        switch ($data) {

        case "leieforholdforslag": {
            $tp = $this->mysqli->table_prefix;
            $query = $this->GET['query'];

            $resultat = (object)array(
                'success'	=> true,
                'data'		=> array()
            );

            /** @var Leieforholdsett $leieforholdsett */
            $leieforholdsett = $this->hentSamling(\Kyegil\Leiebasen\Modell\Leieforhold::class);
            $leieforholdsett->leggTilLeietakerNavnFilter($query);

            foreach( $leieforholdsett as $leieforhold ) {
                $resultat->data[] = array(
                    'leieforhold'	=> $leieforhold->hentId(),
                    'visningsfelt'	=> $leieforhold->hentId() . ': ' . $leieforhold->hentBeskrivelse()
                );
            }

            return json_encode($resultat);
        }

        case "utlikningsmuligheter": {
            $tp = $this->mysqli->table_prefix;
            $resultat = (object)array(
                'success'	=> false,
                'data'		=> array()
            );
            /** @var Innbetaling $innbetalingModell */
            $innbetalingModell = $this->hentModell(Innbetaling::class, !empty($_GET['id']) ? $_GET['id'] : null);
            if( !$innbetalingModell->hentId() ) {
                $resultat->msg = "Ugyldig betaling";
                return json_encode($resultat);
            }
            $delbeløp = $innbetalingModell->hentDelbeløp( $_POST['innbetalingsid'] ?? null );
            if( !$delbeløp ) {
                $resultat->msg = "Ugyldig delbeløp";
                return json_encode($resultat);
            }
            $leieforhold = $delbeløp->leieforhold;
            $utbetaling  = $innbetalingModell->hentBeløp() < 0;

            /** @var Innbetalingsett $motbetalinger */
            $motbetalinger = $this->hentSamling(Innbetaling::class);
            if ($utbetaling) {
                $motbetalinger->leggTilFilter(['`' . Innbetaling::hentTabell() . '`.`beløp` >' => 0]);
            }
            else {
                $motbetalinger->leggTilFilter(['`' . Innbetaling::hentTabell() . '`.`beløp` <' => 0]);
            }
            $motbetalinger
                ->leggTilFilter(['`' . Innbetaling::hentTabell() . '`.`krav`' => null])
                ->leggTilFilter(['`' . Innbetaling::hentTabell() . '`.`leieforhold`' => $leieforhold->hentId()])
                ->leggTilFilter(['`' . Innbetaling::hentTabell() . '`.`' . Innbetaling::hentPrimærnøkkelfelt() . '` !=' => $innbetalingModell->hentId()])
                ->låsFiltre()
            ;

            /** @var Kravsett $motkravsett */
            $motkravsett = $this->hentSamling(Krav::class);
            if ($utbetaling) {
                $motkravsett->leggTilFilter(['`' . Krav::hentTabell() . '`.`utestående` <' => 0]);
            }
            else {
                $motkravsett->leggTilFilter(['`' . Krav::hentTabell() . '`.`utestående` >' => 0]);
            }
            $motkravsett->leggTilFilter(['leieforhold' => $leieforhold->hentId()])->låsFiltre();
            $motkravsett->leggTilSortering('kravdato')->leggTilSortering('forfall', false, null, true);

            /** @var Innbetaling $betaling */
            foreach( $motbetalinger as $betaling ) {
                $resultat->data[] = array(
                    'id'	=>	0,
                    'tekst'	=>	$utbetaling ? 'Innbetalinger' : 'Utbetalinger',
                    'beløp'	=>	-$betaling->beløp
                );
            }

            /** @var Krav $krav */
            foreach( $motkravsett as $krav ) {
                $resultat->data[] = array(
                    'id'	=>	$krav->hentId(),
                    'tekst'	=>	$krav->hentTekst(),
                    'beløp'	=>	$krav->hentUtestående()
                );
            }

            $resultat->success = true;
            return json_encode($resultat);
        }

        default: {
            ob_start();
            /** @var Innbetaling $innbetalingsModell */
            $innbetalingsModell = $this->hentModell(Innbetaling::class, !empty($_GET['id']) ? $_GET['id'] : null);
            $utbetaling  = $innbetalingsModell->hentBeløp() < 0;

            /** @var Innbetaling\Delbeløpsett $avstemminger */
            $avstemminger = $innbetalingsModell->hentDelbeløp();
    ?>
        <div class="dataload">
            <!--suppress HtmlDeprecatedAttribute -->
            <table width="100%">
                <tr>
                    <th class="value">Beløp</th>
                    <th>Leieforhold</th>
                    <th>Avstemt mot</th>
                    <th>&nbsp;</th>
                </tr>
                <?php foreach($avstemminger as $avstemming):?>
                <tr>
                    <td class="value"><?php echo $this->kr(abs($avstemming->beløp));?></td>
                    <td>
                    <?php if($avstemming->leieforhold):?>
                        <a title="Klikk her for å åpne detaljene for leieforholdet." href="/drift/index.php?oppslag=leieforholdkort&id=<?php echo $avstemming->leieforhold;?>"><?php echo "{$avstemming->leieforhold}: {$avstemming->leieforhold->hentBeskrivelse()}";?></a>&nbsp;&nbsp;<a style="cursor: pointer;" onClick="angiLeieforhold(<?php echo $avstemming->hentId();?>, '<?php echo $innbetalingsModell;?>', <?php echo $avstemming->leieforhold;?>, false)">[Endre leieforhold]</a>
                    <?php elseif($avstemming->erFremmedBeløp):?>
                        <i>Ignoreres som fremmedbeløp</i>&nbsp;&nbsp;<a style="cursor: pointer;" onClick="angiLeieforhold(<?php echo $avstemming->hentId();?>, '<?php echo $innbetalingsModell;?>', null, true)">[Angi leieforhold]</a>
                    <?php else:?>
                        <i>ikke knyttet til leieforhold</i>&nbsp;&nbsp;<a style="cursor: pointer;" onClick="angiLeieforhold(<?php echo $avstemming->hentId();?>, '<?php echo $innbetalingsModell;?>', null, false)">[Angi leieforhold]</a>
                    <?php endif;?>
                    </td>
                    <td>
                    <?php if($avstemming->krav instanceof Krav):?>
                        <a href="/drift/index.php?oppslag=krav_kort&id=<?php echo $avstemming->krav;?>"><?php echo $avstemming->krav->hentTekst();?></a>
                    <?php elseif($avstemming->krav !== null):?>
                        Avstemt mot <?php echo $utbetaling ? 'inn' : 'ut';?>betalinger
                    <?php elseif($avstemming->erFremmedBeløp):?>
                        <i>ignoreres</i>
                    <?php else:?>
                        <i>ikke avstemt</i>
                    <?php endif;?>
                    </td>
                    <td>
                    <?php if($avstemming->krav !== null && !$avstemming->erFremmedBeløp):?>
                        <a style="cursor: pointer;" onClick="frakople(<?php echo $avstemming->hentId();?>)">[Løsne]</a>
                    <?php elseif($avstemming->leieforhold && !$avstemming->erFremmedBeløp):?>
                        <a style="cursor: pointer;" onClick="kople(<?php echo $avstemming->hentId();?>, <?php echo $avstemming->beløp;?>, <?php echo ($avstemming->leieforhold ? $avstemming->leieforhold : 'null');?>)">[Kople]</a>
                    <?php endif;?>
                    </td>
                </tr>
                <?php endforeach;?>
            </table>
        </div>
    <?php
            return ob_get_clean();
            }
        }
    }

    /**
     * @param $data
     * @return string
     * @throws \Throwable
     */
    public function manipuler($data = "") {
        $resultat = new stdClass;

        switch ($data) {
            case "frakople": {
                $delbeløpId = $this->POST['innbetalingsid'] ?? null;

                /** @var Innbetaling\Delbeløp $delbeløp */
                $delbeløp = $this->hentModell(Innbetaling\Delbeløp::class, $delbeløpId);
                if (!$delbeløp->hentId()) {
                    return json_encode(array(
                        'success'	=> false
                    ));
                }
                $innbetaling = $delbeløp->innbetaling;
                $delbeløp->fjernAvstemming();
                $innbetaling->samle();

                EventManager::getInstance()->triggerEvent(
                    Innbetaling::class, 'endret',
                    $innbetaling, ['ny' => false], $this->hentCoreModelImplementering()
                );

                Vedlikehold::getInstance($this)->sjekkAvstemming();
                $resultat->success = true;
                return (json_encode($resultat));
            }

            case "kople": {
                $innbetalingId = !empty($_GET['id']) ? $_GET['id'] : null;
                $leieforholdId = $_POST['leieforhold'] ?? null;
                $resultat = (object)array(
                    'success' => false,
                );

                /** @var Innbetaling $innbetaling */
                $innbetaling = $this->hentModell(Innbetaling::class, $innbetalingId);
                /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
                $leieforhold = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold::class, $leieforholdId);

                if( $innbetaling->hentId() ) {
                    foreach( $_POST as $parameter => $verdi ) {
                        if( substr($parameter, 0, 5) == 'verdi' ) {
                            $beløp = str_replace( ',', '.', $verdi );
                            if( substr($parameter, 5 ) ) {
                                /** @var Krav|null $motkrav */
                                $motkrav = $this->hentModell(Krav::class, substr($parameter, 5 ));
                                $motkrav = $motkrav->hentId() ? $motkrav : null;
                            }
                            else {
                                $motkrav = true;
                            }

                            $innbetaling->avstem($motkrav, $beløp, $leieforhold);

                            $betalingsplan = $leieforhold->hentBetalingsplan();
                            if($betalingsplan && $betalingsplan->hentAktiv()
                            ) {
                                $betalingsplan->oppdaterUteståendeAvdrag();
                                if($betalingsplan->hentUtestående() <= 0) {
                                    $betalingsplan->settAktiv(false);
                                }
                            }
                            $resultat->success = true;
                        }
                    }
                }

                EventManager::getInstance()->triggerEvent(
                    Innbetaling::class, 'endret',
                    $innbetaling, ['ny' => false], $this->hentCoreModelImplementering()
                );
                Vedlikehold::getInstance($this)->sjekkAvstemming();
                return (json_encode($resultat));
            }

            default:
                return json_encode(null);
        }
    }

    /**
     * @param $oppgave
     * @return string
     * @throws Throwable
     */
    public function oppgave($oppgave = ""): string {
        switch($oppgave) {

        case "endreleieforhold":
            try {
                $resultat = (object)[
                    'success' => true,
                    'msg' => '',
                    'data' => []
                ];

                $innbetalingId = $_GET['innbetaling'] ?? null;
                $tidlLeieforholdId = $_GET['tidl_leieforhold'] ?? null;
                $leieforholdId = $_GET['leieforhold'] ?? null;
                $erFremmedBeløp = isset($_GET['er_fremmed_beløp']) && $_GET['er_fremmed_beløp'];

                /** @var Innbetaling $innbetaling */
                $innbetaling = $this->hentModell(Innbetaling::class, $innbetalingId);
                /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
                $leieforhold = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold::class, $tidlLeieforholdId);
                $leieforhold = $leieforhold->hentId() ? $leieforhold : null;

                /** @var \Kyegil\Leiebasen\Modell\Leieforhold $nyttLeieforhold */
                $nyttLeieforhold = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold::class, $leieforholdId);

                if(!$erFremmedBeløp && !$nyttLeieforhold->hentId()) {
                    throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Ugyldig leieforhold');
                }

                if ($leieforhold) {
                    $innbetaling->fjernAvstemming(null, true, $leieforhold);
                }
                if ($erFremmedBeløp) {
                    $innbetaling->hentDelbeløp()->leggTilFilter(['leieforhold' => null])->låsFiltre()->forEach(function (Innbetaling\Delbeløp $delbeløp) {
                        $delbeløp->settErFremmedBeløp(true);
                    });
                } else {
                    $innbetaling->konter($nyttLeieforhold);
                }

                EventManager::getInstance()->triggerEvent(
                    Innbetaling::class, 'endret',
                    $innbetaling, ['ny' => false], $this->hentCoreModelImplementering()
                );
            }
            catch (Exception $e) {
                $resultat->success = false;
                $resultat->msg = $e->getMessage();
            }
            return json_encode($resultat);
        }
        return '';
    }
}