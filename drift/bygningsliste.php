<?php

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    /**
     * @var string
     */
    public $tittel = 'leiebasen';

    /**
     * @var string
     */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * oppsett constructor.
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
    }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        if (isset($_GET['returi']) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }
?>
Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', '<?=$this->http_host . '/pub/lib/' . $this->ext_bibliotek . "/examples/ux"?>');

Ext.require([
    'Ext.data.*',
    'Ext.form.field.*',
    'Ext.layout.container.Border',
    'Ext.grid.*',
    'Ext.ux.RowExpander'
]);

Ext.onReady(function() {

    Ext.tip.QuickTipManager.init();
    Ext.form.Field.prototype.msgTarget = 'side';
    Ext.Loader.setConfig({
        enabled:true
    });

    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';

    Ext.define('Ext.ux.BildeTrigger', {
        extend: 'Ext.form.field.Trigger',
        alias: 'widget.bildetrigger',

        // override onTriggerClick
        onTriggerClick: function(record) {
            Ext.Msg.alert('Status', 'You clicked my trigger!');
        }
    });

    Ext.define('Bygning', {
        extend: 'Ext.data.Model',
        idProperty: 'id',
        fields: [
            {name: 'id', type: 'float'},
            {name: 'kode', type: 'string'},
            {name: 'navn', type: 'string'},
            {name: 'bilde', type: 'string'},
            {name: 'html', type: 'string'}
        ]
    });

    var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
        clicksToEdit: 1
    });

    var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
        autoCancel: false,
        listeners: {
            beforeedit: function (grid, e, eOpts) {
                return e.column.xtype !== 'actioncolumn';
            }
        }
    });


    // oppretter datasettet
    var datasett = Ext.create('Ext.data.Store', {
        model: 'Bygning',
        pageSize: 200,
        remoteSort: true,
        proxy: {
            type: 'ajax',
            timeout: 900000,
            simpleSortMode: true,
            url: "/drift/index.php?oppslag=<?=$_GET['oppslag']?>&oppdrag=hentdata",
            reader: {
                type: 'json',
                root: 'data',
                actionMethods: { // Feil plassering? actionMethods tilhører proxy..
                    read: 'POST'
                },
                totalProperty: 'totalRows'
            }
        },
        sorters: [{
            property: 'id',
            direction: 'ASC'
        }],
        groupField: 'navn',
        autoLoad: true
    });


    var rutenett = Ext.create('Ext.grid.Panel', {
        autoScroll: true,
        autoExpandColumn: 2,
        layout: 'border',
//        frame: false,
        plugins: [{
            ptype: 'rowexpander',
            rowBodyTpl : ['{html}']
        }],
        store: datasett,
        title: 'Bygninger',
        columns: [
            {
                dataIndex: 'kode',
                text: 'Kode',
                width: 120,
                sortable: true
            },
            {
                dataIndex: 'navn',
                text: 'Navn',
                width: 150,
                flex: 1,
                sortable: true
            },
            {
                dataIndex: 'bilde',
                text: 'Bilde',
                width: 420,
                renderer: function(value, metadata, record, rowIndex, colIndex, store){
                    if(value) {
                        return '<img style= "max-width: 400px; max-height: 150px; height: 100%; width: auto;" src="' + value + '">';
                    }
                },
                sortable: true
            },
            {
                dataIndex: 'id',
                width: 40,
                align: 'right',
                renderer: function(value, metadata, record, rowIndex, colIndex, store) {
                    return '<a href="/drift/index.php?oppslag=bygning_skjema&id=' + value + '"><img src="/pub/media/bilder/drift/rediger.png" title="Klikk for å endre"></a>';
                }
            }
        ],
        renderTo: 'panel',
        height: 600,
        buttons: [{
            text: 'Tilbake',
            handler: function() {
                window.location = '<?=$this->returi->get();?>';
            }
        }, {
            text: 'Opprett ny bygning',
            handler: function() {
                window.location = '/drift/index.php?oppslag=bygning_skjema&id=*';
            }
        }]
    });

//  rutenett.on('edit', lagreEndringer);


});
<?php
        return ob_get_clean();
    }

    /**
     *
     */
    public function design() {
        echo '<div id="panel" class="extjs-panel"></div>';
    }

    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ""):string {
        switch ($data) {
            default:
                $resultat = $this->mysqli->arrayData(array(
                    'returnQuery' => true,
                    'source' => "bygninger",
                    'orderfields' => ($_GET['sort'] ? "{$this->GET['sort']} {$this->GET['dir']}, " : "") . "kode, id"
                ));
                foreach($resultat->data as $denne) {
                    $denne->html = "";
                    $leieobjekter = $this->mysqli->arrayData(array(
                        'source' => "leieobjekter",
                        'where' => "bygning = '{$denne->id}'",
                        'orderfields' => "etg DESC, leieobjektnr"
                    ));
                    foreach($leieobjekter->data as $leieobjekt) {
                        $denne->html .= $this->leieobjekt($leieobjekt->leieobjektnr, true) . "<br>";
                    }
                }
                return json_encode($resultat);
        }
    }

    /**
     * @param string $skjema
     * @return string
     */
    public function taIMot($skjema):string {
        switch ($skjema) {
            default:
                return json_encode(null);
        }
    }

    /**
     * @param $oppgave
     */
    public function oppgave($oppgave) {
        switch ($oppgave) {
            default:
                return json_encode(null);
        }
    }
}