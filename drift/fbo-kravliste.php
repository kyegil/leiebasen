<?php

use Kyegil\Leiebasen\Modell\Leieforhold\Regning;

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    /**
     * @var string
     */
    public $tittel = 'AvtaleGiro-trekkliste';

    /**
     * @var string
     */
    public $ext_bibliotek = 'ext-4.2.1.883';
    

    /**
     * oppsett constructor.
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
    }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        if (isset($_GET['returi']) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }
?>
Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', '<?=$this->http_host . '/pub/lib/' . $this->ext_bibliotek . "/examples/ux"?>');

Ext.require([
     'Ext.data.*',
     'Ext.form.field.*',
    'Ext.layout.container.Border',
     'Ext.grid.*',
    'Ext.ux.RowExpander'
]);

Ext.onReady(function() {

    Ext.tip.QuickTipManager.init();
    Ext.form.Field.prototype.msgTarget = 'side';
    Ext.Loader.setConfig({enabled:true});
    
    Ext.define('FboKrav', {
        extend: 'Ext.data.Model',
        idProperty: 'id',
        fields: [ // http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.Field
            {name: 'id', type: 'int'},
            {name: 'leieforhold', type: 'int'},
            {name: 'leieforholdbeskrivelse', type: 'string'},
            {name: 'gironr', type: 'int'},
            {name: 'kid', type: 'string'},
            {name: 'forsendelse', type: 'string'},
            {name: 'oppdrag', type: 'string'},
            {name: 'beløp', type: 'float'},
            {name: 'overføringsdato', type: 'date', dateFormat: 'Y-m-d'},
            {name: 'forfallsdato', type: 'date', dateFormat: 'Y-m-d'},
            {name: 'varslet', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {name: 'varslingsform', type: 'string'},
            {name: 'html', type: 'string'},
            {name: 'slettes', type: 'boolean'},
            {name: 'status', type: 'int'},
            {name: 'statusbeskrivelse', type: 'string'},
            {name: 'mottakskvittering', type: 'string'},
            {name: 'prosessert_kvittering', type: 'string'}
        ]
    });
    
    visKvittering = function( kvittering ) {
        var melding = Ext.create('Ext.window.Window', {
            title: 'Kvittering fra NETS for mottatt forsendelse',
            height: 400,
            width: 600,
            layout: 'fit',
            html: kvittering
        });
        melding.show();
    }
    
    
    var sendteTrekkrav = Ext.create('Ext.data.Store', {
        model: 'FboKrav',
        pageSize: 300,
        remoteSort: true,
        proxy: {
            type: 'ajax',
            simpleSortMode: true,
            url: "/drift/index.php?oppslag=fbo-kravliste&oppdrag=hentdata&data=trekkrav",
            reader: {
                type: 'json',
                root: 'data',
                actionMethods: { // Feil plassering? actionMethods tilhører proxy..
                    read: 'POST'
                },
                totalProperty: 'totalRows'
            }
        },
        sorters: [{
            property: 'forfallsdato',
            direction: 'ASC'
        }],
        autoLoad: true
    });
    

    var giroerPåVent = Ext.create('Ext.data.Store', {
        model: 'FboKrav',
        pageSize: 300,
        remoteSort: true,
        proxy: {
            type: 'ajax',
            simpleSortMode: true,
            url: "/drift/index.php?oppslag=fbo-kravliste&oppdrag=hentdata&data=venteliste",
            reader: {
                type: 'json',
                root: 'data',
                actionMethods: { // Feil plassering? actionMethods tilhører proxy..
                    read: 'POST'
                },
                totalProperty: 'totalRows'
            }
        },
        sorters: [{
            property: 'id',
            direction: 'ASC'
        }],
        autoLoad: {start: 0, limit: 300}
    });
    


    var søkefelt = Ext.create('Ext.form.field.Text', {
        emptyText: 'Søk og klikk ↵',
        name: 'søkefelt',
        width: 200,
        listeners: {
            specialkey: function() {
                sendteTrekkrav.getProxy().extraParams = {
                    søkefelt: søkefelt.getValue()
                };
                sendteTrekkrav.load({
                    params: {
                        start: 0,
                        limit: 300
                    }
                });
                
                giroerPåVent.getProxy().extraParams = {
                    søkefelt: søkefelt.getValue()
                };
                giroerPåVent.load({
                    params: {
                        start: 0,
                        limit: 300
                    }
                });
            }
        }
    });


    var rutenettSendteKrav = Ext.create('Ext.grid.Panel', {
        region: 'center',
        autoScroll: true,
        frame: false,

        plugins: [{
            ptype: 'rowexpander',
            rowBodyTpl : ['{html}']
        }],

        store: sendteTrekkrav,
        title: 'AvtaleGiro-trekk oversendt bank',
        listeners: {
            celldblclick: function( panel, td, cellIndex, record, tr, rowIndex, e, eOpts ) {
//                window.location = "/drift/index.php?oppslag=leieforholdkort&id=" + record.get('leieforhold');
            }
        },
        columns: [    // http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.Field
            {
                dataIndex: 'id',
                text: 'ID',
                hidden: true,
                width: 50,
                sortable: true
            },
            {
                dataIndex: 'forfallsdato',
                text: 'Forfall',
                width: 70,
                align: 'right',
                renderer: Ext.util.Format.dateRenderer('d.m.Y')
            },
            {
                dataIndex: 'leieforhold',
                text: 'Leieforhold',
                width: 200,
                renderer: function(value, metadata, record){
                    return record.get('leieforholdbeskrivelse');
                },
                flex: 1,
                sortable: true
            },
            {
                dataIndex: 'gironr',
                text: 'Giro',
                width: 50,
                align: 'right',
                sortable: true
            },
            {
                dataIndex: 'kid',
                text: 'KID',
                width: 90,
                align: 'right'
            },
            {
                dataIndex: 'beløp',
                text: 'Beløp',
                width: 70,
                align: 'right',
                renderer: Ext.util.Format.noMoney
            },
            {
                dataIndex: 'forsendelse',
                text: 'Forsendelse',
                width: 70,
                align: 'right',
                renderer: function(value, metaData, record) {
                    if (record.get('mottakskvittering')) {
                        return "<b onClick=\"visKvittering('" + record.get('prosessert_kvittering') + "')\">" + value + "</b>";
                    }
                    else {
                        return value;
                    }
                }

            },
            {
                dataIndex: 'oppdrag',
                text: 'Oppdrag',
                width: 70,
                align: 'right',
                renderer: function(value, metaData, record) {
                    if (record.get('prosessert_kvittering')) {
                        return "<b onClick=\"visKvittering('" + record.get('prosessert_kvittering') + "')\">" + value + "</b>";
                    }
                    else {
                        return value;
                    }
                }
            },
            {
                dataIndex: 'overføringsdato',
                text: 'Overført',
                width: 70,
                align: 'right',
                renderer: Ext.util.Format.dateRenderer('d.m.Y')
            },
            {
                dataIndex: 'varslet',
                text: 'Varslet',
                width: 70,
                align: 'right',
                renderer: Ext.util.Format.dateRenderer('d.m.Y')
            },
            {
                dataIndex: 'varslingsform',
                text: 'Varslingsform',
                width: 100,
                align: 'left'
            },
            {
                dataIndex: 'status',
                text: 'Status',
                width: 60,
                align: 'left',
                renderer: function(value, metadata, record){
                    return record.get('statusbeskrivelse');
                }
            }
        ]
    });


    var venteregister = Ext.create('Ext.grid.Panel', {
        region: 'south',
        autoScroll: true,
        split: true,
        collapsible: true,
        collapsed: true,

        plugins: [{
            ptype: 'rowexpander',
            rowBodyTpl : ['{html}']
        }],

        store: giroerPåVent,
        title: 'Oppdrag på venteliste for overføring til bank',
        listeners: {
            celldblclick: function( panel, td, cellIndex, record, tr, rowIndex, e, eOpts ) {
//                window.location = "/drift/index.php?oppslag=leieforholdkort&id=" + record.get('leieforhold');
            }
        },
        columns: [    // http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.Field
            {
                dataIndex: 'forfallsdato',
                text: 'Forfall',
                width: 70,
                align: 'right',
                renderer: Ext.util.Format.dateRenderer('d.m.Y')
            },
            {
                dataIndex: 'leieforhold',
                text: 'Leieforhold',
                width: 200,
                renderer: function(value, metadata, record){
                    return record.get('leieforholdbeskrivelse');
                },
                flex: 1,
                sortable: true
            },
            {
                dataIndex: 'gironr',
                text: 'Giro',
                width: 50,
                align: 'right',
                sortable: true
            },
            {
                dataIndex: 'kid',
                text: 'KID',
                width: 90,
                align: 'right'
            },
            {
                dataIndex: 'beløp',
                text: 'Beløp',
                width: 70,
                align: 'right',
                renderer: Ext.util.Format.noMoney
            },
            {
                dataIndex: 'varslet',
                text: 'Varslet',
                width: 70,
                align: 'right',
                renderer: Ext.util.Format.dateRenderer('d.m.Y')
            },
            {
                dataIndex: 'varslingsform',
                text: 'Varslingsform',
                width: 100,
                align: 'left'
            },
            {
                dataIndex: 'status',
                text: 'Status',
                width: 60,
                align: 'left',
                renderer: function(value, metadata, record){
                    return record.get('statusbeskrivelse');
                }
            }
        ],
        height: 240
    });


    var container = Ext.create('Ext.panel.Panel', {
//        autoScroll: true,
        layout: 'border',
//        frame: false,
        tbar: [
            søkefelt
        ],
        items: [
            rutenettSendteKrav,
            venteregister
        ],
        renderTo: 'panel',
        height: 600,
        buttons: [{
            text: 'Tilbake',
            handler: function() {
                window.location = '<?=$this->returi->get();?>';
            }
        }, {
            text: 'Faste betalingsoppdrag',
            handler: function() {
                window.location = '/drift/index.php?oppslag=fboliste';
            }
        }]
    });


    giroerPåVent.on({
        load: function( store, records ) {
            if( records.length ) {
                venteregister.expand();
            }
            else {
                venteregister.collapse();
            }
        }
    });


});
<?php
        return ob_get_clean();
    }

    /**
     *
     */
    public function design() {
?>
<div id="panel" class="extjs-panel"></div>
<?php
    }

    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ""):string {
        $tp = $this->mysqli->table_prefix;
        $resultat = null;

        switch ($data) {

            case "trekkrav": {
                $søkefelt = @$this->GET['søkefelt'];
                $sort        = @$_GET['sort'];
                $synkende    = @$_GET['dir'] == "DESC" ? true : false;
                $start        = (int)@$_GET['start'];
                $limit        = @$_GET['limit'];

                $filter = "AND (fbo_trekkrav.leieforhold LIKE '%{$søkefelt}%' OR CONCAT(personer.fornavn, ' ', personer.etternavn) LIKE '%{$søkefelt}%')";

                $resultat = $this->mysqli->arrayData(array(
                    'distinct'    =>true,
                    'fields'    => "fbo_trekkrav.*",
                    'returnQuery' => true,
                    'source' => "{$tp}fbo_trekkrav AS fbo_trekkrav
                                INNER JOIN {$tp}kontrakter AS kontrakter
                                ON fbo_trekkrav.leieforhold = kontrakter.leieforhold
                                LEFT JOIN {$tp}kontraktpersoner AS kontraktpersoner
                                ON kontrakter.kontraktnr = kontraktpersoner.kontrakt
                                LEFT JOIN {$tp}personer AS personer
                                ON kontraktpersoner.person = personer.personid
                                ",
                    'where'    => "fbo_trekkrav.forfallsdato >= CURDATE() {$filter}",
                    'orderfields' => "fbo_trekkrav.id DESC"
                ));

                foreach($resultat->data as $trekkrav) {
                    /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforholdModell */
                    $leieforholdModell = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold::class, $trekkrav->leieforhold);
                    $html = array();

                    /** @var \Giro|null $giro */
                    $giro = $this->hent(\Giro::class, $trekkrav->gironr);

                    $trekkrav->varslingsform = "";
                    $trekkrav->status = 0;
                    $trekkrav->statusbeskrivelse = "OK";
                    $ukedag = date('N');
                    $nesteForsendelse = $this->hentNetsProsessor()->nesteFboTrekkravForsendelse();
                    $forsendelsesdag = $nesteForsendelse->format('Y-m-d') == date('Y-m-d')
                                    ?    "i dag"
                                    :    (\IntlDateFormatter::formatObject($nesteForsendelse, 'EEEE', 'nb_NO'));

                    if( $giro->hentId() ) {
                        $forfall = $giro->hent('forfall');
                        $utestående = $giro->hent('utestående');
                        $girokrav = $giro->hent('krav');

                        foreach ($girokrav as $krav ) {
                            $html[] = "<a title=\"Åpne\" href=\"/drift/index.php?oppslag=krav_kort&id={$krav}\">{$krav->hent('tekst')}</a>";
                        }
                        $trekkrav->varslingsform = ucfirst(@$giro->hent('format'));

                        if(
                            !$forfall
                            or $forfall->format('Y-m-d') != $trekkrav->forfallsdato
                            or $utestående != $trekkrav->beløp
                        ) {
                            $trekkrav->endret = true;
                        }
                        else {
                            $trekkrav->endret = false;
                        }

                    }
                    else {
                        $trekkrav->endret = true;
                    }

                    if($trekkrav->endret) {
                        $trekkrav->status = 1;
                        $trekkrav->statusbeskrivelse = "<div title=\"Forfall eller utestående beløp er endret siden trekket ble oversendt bank. Slettemelding vil sendes NETS ved neste forsendelse {$forsendelsesdag} kl. {$nesteForsendelse->format('H:i')}.\">Endret</div>";

                        if( $trekkrav->forfallsdato <= $nesteForsendelse->format('Y-m-d') ) {
                            $trekkrav->status = 2;
                            $trekkrav->statusbeskrivelse = "<div style=\"color:red;\" title=\"Forfall eller utestående beløp er endret siden kravet ble oversendt NETS,\nmen en slettemelding nå vil trolig bli avvist av NETS pga for kort frist.\"><span style=\"font-size:larger;\">⚠</span></div>";
                        }
                        else if(
                            $giro->hentId()
                            and    ($giro->hent('utestående') != 0)
                            and    $this->hentNetsProsessor()->nesteFboTrekkravForsendelse() > $giro->fboOppdragsfrist()
                        ) {
                            $trekkrav->status = 2;
                            $trekkrav->statusbeskrivelse = "<div style=\"color:red;\" title=\"Forfall eller utestående beløp er endret siden trekket ble oversendt bankene,\nmen et korrigert trekk vil trolig bli avvist av NETS pga for kort frist.\nDersom trekket skal korrigeres må forfallsdato flyttes til et senere tidspunkt.\nBetaler kan evt selv slette trekket ifra sin nettbank\"><span style=\"font-size:larger;\">⚠</span></div>";
                        }
                    }

                    $trekkrav->leieforholdbeskrivelse = "<a title=\"Åpne\" href=\"/drift/index.php?oppslag=leieforholdkort&id={$leieforholdModell->hentId()}\">{$leieforholdModell->hentId()}</a>: " . $leieforholdModell->hentBeskrivelse();

                    $trekkrav->html = implode('<br>', $html);

                }

                $resultat->data =  $this->sorterObjekter($resultat->data, $sort, $synkende);
                $resultat->totalRows = count($resultat->data);
                $resultat->data = array_slice(  $resultat->data, $start, $limit);
                return json_encode( $resultat );
            }


            case "venteliste": {
                $søkefelt = @$this->GET['søkefelt'];
                $sort        = @$_GET['sort'];
                $synkende    = @$_GET['dir'] == "DESC" ? true : false;
                $start        = (int)@$_GET['start'];
                $limit        = @$_GET['limit'];

                $resultat = (object)array(
                    'success'    => true,
                    'msg'        => "",
                    'data'        => array()
                );

                $nesteForsendelse = $this->hentNetsProsessor()->nesteFboTrekkravForsendelse();
                $forsendelsesdag = $nesteForsendelse->format('Y-m-d') == date('Y-m-d')
                                ?    "i dag"
                                :    (\IntlDateFormatter::formatObject($nesteForsendelse, 'EEEE', 'nb_NO'));

                $filter = "kontrakter.leieforhold LIKE '%{$søkefelt}%' OR kontraktpersoner.leietaker LIKE '%{$søkefelt}%' OR CONCAT(personer.fornavn, ' ', personer.etternavn) LIKE '%{$søkefelt}%'";

                $giroer = $this->mysqli->arrayData(array(
                    'distinct'    =>true,
                    'class'        => \Giro::class,
                    'fields'    => "{$tp}giroer.gironr AS id",
                    'returnQuery' => true,
                    'source' => "{$tp}giroer AS giroer
                    
                                INNER JOIN {$tp}fbo AS fbo
                                ON giroer.leieforhold = fbo.leieforhold
                                AND (giroer.utskriftsdato IS NULL OR giroer.utskriftsdato >= fbo.registrert)
                                
                                INNER JOIN {$tp}kontrakter AS kontrakter
                                ON giroer.leieforhold = kontrakter.leieforhold
                                
                                INNER JOIN {$tp}krav AS krav
                                ON giroer.gironr = krav.gironr
                                
                                LEFT JOIN {$tp}fbo_trekkrav AS fbo_trekkrav
                                ON giroer.gironr = fbo_trekkrav.gironr
                                
                                INNER JOIN {$tp}kontraktpersoner AS kontraktpersoner
                                ON kontrakter.kontraktnr = kontraktpersoner.kontrakt
                                INNER JOIN {$tp}personer AS personer
                                ON kontraktpersoner.person = personer.personid
                                ",
                    'where'    => "krav.utestående > 0 AND !fbo_trekkrav.gironr IS NULL AND ({$filter})
                    ",
                    'orderfields' => "giroer.gironr DESC"
                ));

                /** @var \Giro $giro */
                foreach( $giroer->data as $giro ) {
                    /** @var Regning $regning */
                    $regning = $this->hentModell(Regning::class, $giro->id);
                    $utskriftsdato = $regning->hentUtskriftsdato();
                    $forfallsdato = $regning->hentForfall();
                    $fboOppdragsfrist = $regning->hentFboOppdragsfrist();
                    /** @var \Krav[] $girokrav */
                    $girokrav = $giro->hent('krav');
                    $html = array();
                    $status = 0;
                    $statusbeskrivelse = "<div title=\"Betalingskravet vil bli oversendt NETS\nved neste AvtaleGiro trekkoppdrag {$forsendelsesdag} kl. {$nesteForsendelse->format('H:i')}.\">OK</div>";

                    foreach ($girokrav as $krav ) {
                        $html[] = "<a title=\"Åpne\" href=\"/drift/index.php?oppslag=krav_kort&id={$krav}\">{$krav->hent('tekst')}</a>";
                    }

                    if( $fboOppdragsfrist and ($nesteForsendelse > $fboOppdragsfrist) ) {
                        $status = 2;
                        $statusbeskrivelse = "<div style=\"color:red;\" title=\"Det er for kort frist for å oversende dette beløpet til trekk via AvtaleGiro.\nForsøk å endre forfall til en senere dato.\">⌛</div>";
                    }

                    $resultat->data[] = (object)array(
                        'gironr'                => strval($regning),
                        'forfallsdato'            => ( $forfallsdato ? $forfallsdato->format('Y-m-d') : "" ),
                        'beløp'                    => $regning->hentUtestående(),
                        'kid'                    => $regning->hentKid(),
                        'leieforhold'            => strval($regning->leieforhold),
                        'leieforholdbeskrivelse'    => "<a title=\"Åpne\" href=\"/drift/index.php?oppslag=leieforholdkort&id={$regning->leieforhold}\">{$regning->leieforhold}</a>: {$regning->leieforhold->hentBeskrivelse()}",
                        'varslet'                => $utskriftsdato ? $utskriftsdato->format('Y-m-d H:i:s') : "",
                        'varslingsform'            => ucfirst($regning->hentFormat()),
                        'html'                    => implode('<br>', $html),
                        'status'                => $status,
                        'statusbeskrivelse'        => $statusbeskrivelse,
                    );
                }

                $resultat->data =  $this->sorterObjekter($resultat->data, $sort, $synkende);
                $resultat->totalRows = count($resultat->data);
                $resultat->data = array_slice(  $resultat->data, $start, $limit);
                return json_encode( $resultat );
            }


            default: {
                return json_encode($resultat);
            }
        }
    }

    /**
     * @param string $skjema
     * @return string
     */
    public function taIMot($skjema):string {
        switch ($skjema) {
            default:
                return json_encode(null);
        }
    }

    /**
     * @param $oppgave
     */
    public function oppgave($oppgave) {
        switch ($oppgave) {
            default:
                return json_encode(null);
        }
    }
}