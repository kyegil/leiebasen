<?php

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $this->mal = "_utskrift.php";
        if(!$id = $this->GET['id']) {
            die("Ugyldig oppslag: ID ikke angitt for kravet");
        }
    }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        if (isset($_GET['returi']) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }
        return ob_get_clean();
    }

    public function design() {
    }

    public function utskrift() {
        $id = @$_GET['id'];
        $tp = $this->mysqli->table_prefix;
        $fra = $this->GET['fra'];
        $til = $this->GET['til'];
        $beløp = 0;

        $delkravtype = @$this->mysqli->arrayData(array(
            'source'    => "{$tp}delkravtyper as delkravtyper",
            'where'        => "delkravtyper.id = '{$id}'"
        ))->data[0];

        if(!$delkravtype) {
            return;
        }

        /** @var Krav[] $kravsett */
        $kravsett = $this->mysqli->arrayData(array(
            'source'        => "{$tp}innbetalinger AS innbetalinger INNER JOIN {$tp}krav AS krav ON innbetalinger.krav = krav.id",
            'fields'        => "MAX(innbetalinger.dato) as betalt, krav.id\n",
            'where'            => "!utestående\n",
            'having'        => "betalt >= '{$fra}' AND betalt <= '{$til}'",
            'groupfields'    => "krav.id",
            'orderfields'    => "betalt, krav.id",
            'class'            => \Krav::class
        ))->data;

        /** @var \Krav $krav */
        foreach( $kravsett as $krav ) {
            $beløp += $krav->hentDel($id);
        }
        ob_start();
?>
<h1 style="text-align: center;">Innbetalinger til <?php echo $delkravtype->navn;?> i tidsrommet <?=date('d.m.Y', strtotime($_GET['fra'])) . " - " . date('d.m.Y', strtotime($_GET['til']))?></h1>
<p style="text-align: center; font-size: large; font-weight: bold;"><?php echo $this->kr($beløp);?></p>
<p style="text-align: center; font-size: medium;"><?php echo $delkravtype->beskrivelse;?></p>

<table>
<?php foreach( $kravsett as $krav ):?>
    <?php if($krav->hentDel($id) != 0):?>
        <tr style="font-size:0.8em;">
            <td><?php echo $krav->hent('betalt')->format('d.m.Y');?></td>
            <td><?php echo $krav->hent('leieforhold')->hent('beskrivelse');?></td>
            <td><?php echo $krav->hent('tekst');?></td>
            <td><?php echo $this->kr($krav->hentDel(1));?></td>
        </tr>
    <?php endif;?>
<?php endforeach;?>
</table>

<script type="text/javascript">
//    window.print();
</script>
<?php
        return ob_get_clean();
    }


    /**
     * @param string $skjema
     * @return string
     */
    public function taIMot($skjema):string {
        return json_encode(null);
    }

    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ""):string {
        switch ($data) {
            default:
                $resultat = $this->arrayData($this->hoveddata['sql']);
                return json_encode($resultat);
        }
    }
}