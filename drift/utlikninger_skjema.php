<?php /** @noinspection PhpMultipleClassDeclarationsInspection */

use Kyegil\Leiebasen\EventManager;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløpsett;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Oppslag\EksKontroller;

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends EksKontroller {

    /**
     * @var string
     */
    public $tittel = 'Skjema for avstemming av registrerte innbetalinger';
    /**
     * @var string
     */
    public $ext_bibliotek = 'ext-4.2.1.883';


    /**
     * @return string
     * @throws Exception
     */
    public function skript():string {
        ob_start();
        set_time_limit(300);
        if(isset($_GET['returi']) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }

        /** @var object[] $delbeløpBeholdere */
        $delbeløpBeholdere = [];

        /** @var Delbeløpsett $delbeløpsett */
        $delbeløpsett = $this->hentSamling(Delbeløp::class);
        $delbeløpsett->leggTilInnbetalingModell();
        $delbeløpsett->leggTilEavVerdiJoin('er_fremmed_beløp');
        $delbeløpsett
            ->leggTilFilter(['krav' => null])
            ->leggTilFilter(['`er_fremmed_beløp`.`verdi`' => null])
            ->låsFiltre();
        $delbeløpsett->leggTilSortering('dato');

        foreach($delbeløpsett as $delbeløp) {
            $innbetaling = $delbeløp->hentInnbetaling();
            $delbeløpBeholdere[] = (object)[
                'innbetaling'           => $innbetaling,
                'dato'                  => $innbetaling->hentDato(),
                'betaler'               => $innbetaling->hentBetaler(),
                'ref'                   => $innbetaling->hentRef(),
                'konto'                 => $innbetaling->hentKonto(),
                'kontonavn'             => $innbetaling->hentKonto() === '0' ? 'Kreditt' : $innbetaling->hentKonto(),
                'id'                    => $delbeløp->hentId(),
                'beløp'                 => $delbeløp->beløp,
                'leieforhold'           => $delbeløp->leieforhold,
                'mulige'                => [],
                'muligeLeieforhold'     => [],
                'leieforholdFeltsett'   => []
            ];
        }

        $transaksjonsfeltsett = [];

        foreach( $delbeløpBeholdere as $delbeløpObjekt ) {
            if($delbeløpObjekt->leieforhold) {

                // Dersom betalinga er ut fra konto kan den avstemmes mot innbetaling eller mot kreditt
                if( $delbeløpObjekt->beløp < 0 ) {
                    /** @var Delbeløpsett $muligeDelbeløp */
                    $muligeDelbeløp = $this->hentSamling(Delbeløp::class);
                    $muligeDelbeløp->leggTilFilter(['krav' => null])
                        ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`leieforhold`' => $delbeløpObjekt->leieforhold->hentId()])
                        ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`beløp` >' => 0])
                        ->låsFiltre();
                    $muligeDelbeløp->leggTilInnbetalingModell();

                    $delbeløpObjekt->mulige = $muligeDelbeløp->hentElementer();
                }

                // Dersom betalinga er positiv, dvs inn på konto kan den avstemmes mot ubetalte krav
                //    eller mot utbetalinger
                else {
                    // Dersom det er kreditt som skal avstemmes, så ser en først etter det
                    // krediterte kravet, for å avstemme mot dette.
                    if( $delbeløpObjekt->konto == '0' ) {
                        /** @var Krav|null $kreditertKrav */
                        $kreditertKrav = $delbeløpObjekt->innbetaling
                            ->hentKredittkrav()
                            ->hentKreditertKrav();
                        if( $kreditertKrav && $kreditertKrav->hentUtestående() > 0) {
                            $delbeløpObjekt->mulige[] = $kreditertKrav;
                        }
                    }

                    /** @var Kravsett $muligeKrav */
                    $muligeKrav = $delbeløpObjekt->leieforhold->hentKrav();
                    $muligeKrav->leggTilFilter(['`' . Krav::hentTabell() . '`.`utestående` >' => 0])
                        ->låsFiltre();
                    $muligeKrav->leggTilUttrykk('', 'match', '`' . Krav::hentTabell() . '`.`beløp` - `' . Krav::hentTabell() . '`.`utestående` = 0');
                    $muligeKrav->leggTilSortering('kravdato');
                    $muligeKrav->leggTilSortering('forfall');
                    $muligeKrav->leggTilSortering('match', true);

                    /** @var Delbeløpsett $muligeUtbetalinger */
                    $muligeUtbetalinger = $delbeløpObjekt->leieforhold->hentInnbetalingDelbeløp();
                    $muligeUtbetalinger->leggTilInnbetalingModell();
                    $muligeUtbetalinger->leggTilFilter(['krav' => null])
                        ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`beløp` <' => 0])
                        ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`beløp` <' => 0])
                        ->låsFiltre();

                    $delbeløpObjekt->mulige = array_merge(
                        $delbeløpObjekt->mulige,
                        $muligeKrav->hentElementer(),
                        $muligeUtbetalinger->hentElementer()
                    );
                    $delbeløpObjekt->mulige = array_unique( $delbeløpObjekt->mulige );
                }
            }

            // Grupper alle muligheter etter leieforhold
            /** @var Krav|Innbetaling $mulighet */
            foreach( $delbeløpObjekt->mulige as $mulighet ) {
                $leieforhold = strval($mulighet->hentLeieforhold());
                settype( $delbeløpObjekt->muligeLeieforhold[ $leieforhold ], 'array' );
                $delbeløpObjekt->muligeLeieforhold[ $leieforhold ][] = $mulighet;
            }

            foreach( $delbeløpObjekt->muligeLeieforhold as $leieforholdId => $muligheter ) {
                /** @var Leieforhold $leieforhold */
                $leieforhold = $this->hentModell(Leieforhold::class, $leieforholdId);
                $kravfelt = [];
                /** @var Krav|Innbetaling $mulighet */
                foreach( $muligheter as $mulighet ) {
                    $kravfelt[] = (
                        $mulighet instanceof Krav
                        ? "komb['{$delbeløpObjekt->innbetaling}_{$mulighet}'], verdi['{$delbeløpObjekt->innbetaling}_{$mulighet}']"
                        : "komb['{$delbeløpObjekt->innbetaling}_0_{$leieforhold}'], verdi['{$delbeløpObjekt->innbetaling}_0_{$leieforhold}']"
                    );
                }
                $delbeløpObjekt->leieforholdFeltsett[] = "{
                xtype: 'fieldset',
                collapsible: false,
                collapsed: " . ($delbeløpObjekt->leieforhold ? 'false' : 'true') . ",
                autoHeight: true,
                title: 'Leieforhold {$leieforhold}: {$leieforhold->hentNavn()}',
                layout: 'column',
                items: [" . implode(", ", $kravfelt) ."]
            }";
            }
        }
?>
Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', '<?php echo $this->http_host . '/pub/lib/' . $this->ext_bibliotek . "/examples/ux"?>');

Ext.require([
    'Ext.data.*',
    'Ext.form.*'
]);

Ext.onReady(function() {

    Ext.tip.QuickTipManager.init();
    Ext.form.Field.prototype.msgTarget = 'side';
    Ext.Loader.setConfig({enabled:true});
    
    var oppdater = [];
    var feltsett = [];
    var komb = [];
    var verdi = [];
    var avstemming = [];

    
    let leieforhold = Ext.create('Ext.form.field.ComboBox', {
        fieldLabel: 'Leieforhold',
        hideLabel: false,
        name: 'leieforhold',
        width: 570,
        matchFieldWidth: false,
        listConfig: {
            width: 700
        },

        store: Ext.create('Ext.data.JsonStore', {
            storeId: 'leieobjektliste',
        
            autoLoad: false,
            proxy: {
                type: 'ajax',
                url: "/drift/index.php?oppslag=data&oppdrag=hent_data&data=komboliste_leieforhold",
                reader: {
                    type: 'json',
                    root: 'data',
                    idProperty: 'leieforhold'
                }
            },
            
            fields: [
                {name: 'value'},
                {name: 'text'}
            ]
        }),
        queryMode: 'remote',
        displayField: 'text',
        valueField: 'value',
        minChars: 0,
        queryDelay: 1000,

        allowBlank: true,
        typeAhead: false,
        editable: true,
        selectOnFocus: true,
        forceSelection: true
    });


    /**
    * @param {int} innbetalingsid
    * @param {String} beskrivelse
    * @param {String} tidlLeieforhold
    */
    angiLeieforhold = function( innbetaling, beskrivelse, tidlLeieforhold = '') {
        leieforhold.getStore().baseParams = {innbetaling: innbetaling};
        leieforhold.clearValue();
        leieforhold.enable();

        /**
        * @type {Ext.form.field.Checkbox}
        * @link https://docs.sencha.com/extjs/4.2.2/#!/api/Ext.form.field.Checkbox
        */
        let erFremmedBeløpFelt = Ext.create('Ext.form.field.Checkbox', {
            name: 'er_fremmed_beløp',
            fieldLabel: 'Fremmed beløp',
            boxLabel: 'Beløpet skal betraktes som fremmed og ignoreres',
            checked: false,
            inputValue: 1,
            uncheckedValue: 0,
            listeners: {
                change: function( checkbox, newValue) {
                    if( newValue ) {
                        leieforhold.disable();
                    }
                    else {
                        leieforhold.enable();
                    }
                }
            }
        });

        let vindu = Ext.create('Ext.window.Window', {
            title: beskrivelse,
            bodyStyle:'padding:5px 5px 0',
            closeAction: 'hide',
            hideBorders: true,
            width: 600,
            height: 200,
            autoScroll: false,
            items: [
                {
                    xtype: 'displayfield',
                    value: '<b>Angi hvilket leieforhold beløpet gjelder.</b><br>Avstemminger som allerede er markert vil bli lagret, og skjemaet lastet på nytt.<br><br>Finn ønsket leieforhold ved å skrive navn eller leieforholdnummer:'
                },
                erFremmedBeløpFelt,
                leieforhold
            ],
            buttons: [{
                text: 'Avbryt',
                handler: function() {
                    vindu.hide();
                }
            }, {
                text: 'Lagre',
                handler: function() {
                    Ext.Ajax.request({
                        waitMsg: 'Angir leieforhold...',
                        params: {
                            innbetaling: innbetaling,
                            tidl_leieforhold: tidlLeieforhold,
                            leieforhold: leieforhold.getValue(),
                            er_fremmed_beløp: erFremmedBeløpFelt.getValue() ? 1 : 0
                        },
                        url: "/drift/index.php?oppslag=<?php echo $_GET['oppslag']?>&oppdrag=oppgave&oppgave=endreleieforhold",
                        success: function(response) {
                            var tilbakemelding = Ext.JSON.decode(response.responseText);
                            if(tilbakemelding['success'] == true) {
                                Ext.MessageBox.alert('Utført', tilbakemelding.msg);
                                skjema.form.submit({
                                    url: '/drift/index.php?oppslag=utlikninger_skjema&oppdrag=taimotskjema',
                                    waitMsg: 'Lagrer fordeling...'
                                });
                            }
                            else {
                                Ext.MessageBox.alert('Hmm..',tilbakemelding['msg']);
                            }
                        }
                    });
                }
            }]
        });
        vindu.show();
    }    
    
    
    <?php foreach($delbeløpBeholdere as $delbeløpBeholder):?>
    
    avstemming['<?php echo $delbeløpBeholder->innbetaling;?>'] = 0;
    
    oppdater['<?php echo $delbeløpBeholder->innbetaling;?>'] = function() {
        avstemming['<?php echo $delbeløpBeholder->innbetaling;?>'] = 0
    <?php foreach($delbeløpBeholder->mulige as $mulighet):?>
    + (komb['<?php echo ($mulighet instanceof Krav ? "{$delbeløpBeholder->innbetaling}_{$mulighet}" : "{$delbeløpBeholder->innbetaling}_0_{$delbeløpBeholder->leieforhold}");?>'].getValue()
            * verdi['<?php echo ($mulighet instanceof Krav ? "{$delbeløpBeholder->innbetaling}_{$mulighet}" : "{$delbeløpBeholder->innbetaling}_0_{$delbeløpBeholder->leieforhold}");?>'].getValue())
    <?php endforeach;?>    ;
        feltsett[<?php echo $delbeløpBeholder->id;?>].setTitle('Valgt krav for ' + Ext.util.Format.noMoney(avstemming['<?php echo $delbeløpBeholder->innbetaling;?>']));
        return avstemming['<?php echo $delbeløpBeholder->innbetaling;?>'];
    }

        <?php foreach($delbeløpBeholder->mulige as $mulighet):?>


            <?php if($mulighet instanceof Krav):?>
                <?php $komb = "{$delbeløpBeholder->innbetaling}_{$mulighet}"; $tekst = "Krav {$mulighet}: {$mulighet->hentTekst()} (mangler {$this->kr($mulighet->hentUtestående())} av {$this->kr($mulighet->hentBeløp())})"; $utestående = $mulighet->hentUtestående();?>
            <?php else:?>
                <?php /** @var Delbeløp $mulighet */ $komb = "{$delbeløpBeholder->innbetaling}_0_{$delbeløpBeholder->leieforhold}"; $tekst = ($mulighet->konto == '0' ? ('Kreditt:') : ($mulighet->beløp < 0 ? 'Ut' : 'Inn') . 'betalt:') . " {$this->kr(abs($mulighet->beløp))}"; $utestående = abs($mulighet->beløp);?>
            <?php endif;?>
            
    komb['<?php echo $komb?>'] = Ext.create('Ext.form.field.Checkbox', {
        boxLabel: '<?php echo addslashes($tekst);?>',
        hideLabel: true,
        disabled: <?php echo $delbeløpBeholder->leieforhold ? 'false' : 'true';?>,
        inputValue: 1,
        uncheckedValue: 0,
        listeners: {
            change: function( checkbox, newValue ) {
                oppdater['<?php echo $delbeløpBeholder->innbetaling;?>']();
                if( newValue ) {
                    verdi['<?php echo $komb;?>'].enable();
                }
                else {
                    verdi['<?php echo $komb;?>'].setValue('<?php echo $utestående;?>');
                    verdi['<?php echo $komb;?>'].disable();
                }
                verdi['<?php echo $komb;?>'].validate();
            }
        },
        name: 'komb<?php echo $komb;?>',
        width: 700
    });

    verdi['<?php echo $komb?>'] = Ext.create('Ext.form.field.Number', {
        allowNegative: false,
        decimalSeparator: ',',
        submitLocaleSeparator: false,
        hideTrigger: true,
        disabled: true,
        listeners: {
            change: oppdater['<?php echo $delbeløpBeholder->innbetaling;?>']

        },
        maxValue: <?php echo $utestående;?>,
        minValue: 0,
        name: 'verdi<?php echo $komb;?>',
        value: <?php echo $utestående;?>,
        validator: function(v) {
            v = v.replace(',', '.');
            let resultat = oppdater['<?php echo $delbeløpBeholder->innbetaling;?>']();
            if(resultat > <?php echo abs($delbeløpBeholder->beløp);?>) {
                verdi['<?php echo $komb;?>'].setValue(Math.min(<?php echo $utestående;?>, (v - (resultat - <?php echo abs($delbeløpBeholder->beløp);?>))));
            }
            return true;
        },
        width: 60
    });


        <?php endforeach;?>    

    feltsett[<?php echo $delbeløpBeholder->id;?>] = Ext.create('Ext.form.FieldSet', {
        collapsible: false,
        layout: 'anchor',
        items: [{
            xtype: 'displayfield',
            value: '<a title="Klikk for detaljer" href="/drift/index.php?oppslag=innbetalingskort&id=<?php echo $delbeløpBeholder->innbetaling;?>"><?php echo "{$delbeløpBeholder->kontonavn} {$this->kr(abs($delbeløpBeholder->beløp))}";?></a> <?php echo ($delbeløpBeholder->konto ? ( $delbeløpBeholder->beløp > 0 ? "betalt inn " : "betalt ut ") : "");?> <?php echo ($delbeløpBeholder->leieforhold ? (( $delbeløpBeholder->beløp > 0 ? "til " : "av ") . "leieforhold <a title=\"Klikk her for å gå til dette leieforholdet\" href=\"/drift/index.php?oppslag=leieforholdkort&id={$delbeløpBeholder->leieforhold}\">{$delbeløpBeholder->leieforhold}: " . addslashes($delbeløpBeholder->leieforhold->hent('beskrivelse')) . "</a><br>") : "") . ($delbeløpBeholder->konto != '0' ? (($delbeløpBeholder->beløp > 0 ? "av " : "til ") . ($delbeløpBeholder->betaler ?: "ukjent")) : "");?> den <?php echo $delbeløpBeholder->dato->format('d.m.Y');?>. Ref <?php echo $delbeløpBeholder->ref;?>'
        },{
            xtype: 'container',
            layout: 'form',
            columnWidth: 1,
            items:[
            <?php echo implode(", ", $delbeløpBeholder->leieforholdFeltsett)
            . (count($delbeløpBeholder->leieforholdFeltsett) ? ", " : "");?>

                {
                    xtype: 'displayfield',
                    hidden: <?php echo ( $delbeløpBeholder->konto == '0' ? 'true' : 'false' );?>,
                    value: '<a style="cursor: pointer;" title="Klikk her for å angi hvilket leieforhold beløpet skal <?php echo ($delbeløpBeholder->beløp > 0 ? "krediteres" : "trekkes fra");?>" onClick="angiLeieforhold(\'<?php echo $delbeløpBeholder->innbetaling;?>\', \'<?php echo $this->kr(abs($delbeløpBeholder->beløp)) . ($delbeløpBeholder->beløp > 0 ? ($delbeløpBeholder->betaler ? " innbetalt av {$delbeløpBeholder->betaler}" : " innbetalt") : ($delbeløpBeholder->betaler ? " utbetalt til {$delbeløpBeholder->betaler}" : " utbetalt")) . " den " . $delbeløpBeholder->dato->format('d.m.Y');?>\'<?php echo $delbeløpBeholder->leieforhold ? ", {$delbeløpBeholder->leieforhold}" : "";?>)"><?php echo ($delbeløpBeholder->leieforhold ? "Endre" : "Angi");?> leieforhold</a>'
                }
            ]
        }]
    });


        <?php $transaksjonsfeltsett[] = "feltsett[{$delbeløpBeholder->id}]";?>

    <?php endforeach;?>

    var lagreknapp = Ext.create('Ext.Button', {
        text: 'Lagre endringer',
        disabled: false,
        handler: function(){
            skjema.form.submit({
                url: '/drift/index.php?oppslag=<?php echo "{$_GET['oppslag']}";?>&oppdrag=taimotskjema',
                waitMsg:'Prøver å lagre...'
                });
        }
    });

    var skjema = Ext.create('Ext.form.Panel', {
        autoScroll: true,
        bodyPadding: 5,
        items: [<?php echo implode(", ", $transaksjonsfeltsett);?>],
        frame: true,
        standardSubmit: false,
        title: 'Forslag til avstemming av transaksjoner',
        renderTo: 'panel',
        height: 600,
        buttons: [
            {
                text: 'Tilbake',
                handler: function() {
                    window.location = '<?php echo $this->returi->get();?>';
                }
            },
            lagreknapp
        ]
    });


    skjema.on({
        actioncomplete: function(form, action){
            if(action.type === 'submit'){
                if(action.response.responseText === '') {
                    Ext.MessageBox.alert('Problem', 'Det kom en blank respons fra tjeneren.');
                } else {
                    Ext.MessageBox.alert('Suksess', 'Opplysningene er oppdatert');
                    window.location = "/drift/index.php?oppslag=utlikninger_skjema";
                }
            }
        },
                            
        actionfailed: function(form,action){
            if(action.type === 'submit') {
                if (action.failureType === "connect") {
                    Ext.MessageBox.alert('Problem:', 'Klarte ikke lagre data. Fikk ikke kontakt med tjeneren.');
                }
                else {    
                    var result = Ext.JSON.decode(action.response.responseText); 
                    if(result && result.msg) {            
                        Ext.MessageBox.alert('Mottatt tilbakemelding om feil:', result.msg);
                    }
                    else {
                        Ext.MessageBox.alert('Problem:', 'Lagring av data mislyktes av ukjent grunn.');
                    }
                }
            }
            
        }
    });
});
<?php
        return ob_get_clean();
    }

    /**
     *
     */
    public function design() {
?>
<div id="panel" class="extjs-panel"></div>
<?php
    }


    /**
     * @param string $data
     * @return string
     * @throws Exception
     */
    public function hentData($data = ''):string {
        return json_encode($this->arrayData($this->hoveddata['sql']));
    }


    /**
     * @param string $oppgave
     * @return string
     */
    public function oppgave(string $oppgave = ""): string
    {
        if ($oppgave == "endreleieforhold") {
            try {
                $resultat = (object)[
                    'success' => true,
                    'msg' => '',
                    'data' => []
                ];

                $innbetalingId = $_POST['innbetaling'] ?? null;
                $tidlLeieforholdId = $_POST['tidl_leieforhold'] ?? null;
                $leieforholdId = $_POST['leieforhold'] ?? null;
                $erFremmedBeløp = isset($_POST['er_fremmed_beløp']) && $_POST['er_fremmed_beløp'];

                /** @var Innbetaling $innbetaling */
                $innbetaling = $this->hentModell(Innbetaling::class, $innbetalingId);
                /** @var Leieforhold $leieforhold */
                $leieforhold = $this->hentModell(Leieforhold::class, $tidlLeieforholdId);
                $leieforhold = $leieforhold->hentId() ? $leieforhold : null;

                /** @var Leieforhold $nyttLeieforhold */
                $nyttLeieforhold = $this->hentModell(Leieforhold::class, $leieforholdId);

                if (!$erFremmedBeløp && !$nyttLeieforhold->hentId()) {
                    throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Ugyldig leieforhold');
                }

                if ($leieforhold) {
                    $innbetaling->fjernAvstemming(null, true, $leieforhold);
                }
                if ($erFremmedBeløp) {
                    $innbetaling->hentDelbeløp()->leggTilFilter(['leieforhold' => null])->låsFiltre()->forEach(function (Delbeløp $delbeløp) {
                        $delbeløp->settErFremmedBeløp(true);
                    });
                } else {
                    $innbetaling->konter($nyttLeieforhold);
                }

                EventManager::getInstance()->triggerEvent(
                    Innbetaling::class, 'endret',
                    $innbetaling, ['ny' => false], $this->hentCoreModelImplementering()
                );
            } catch (Exception $e) {
                $resultat->success = false;
                $resultat->msg = $e->getMessage();
            }
            return json_encode($resultat);
        }
        return '';
    }


    /**
     * @param string $skjema
     * @return string
     * @throws Throwable
     */
    public function taIMot($skjema):string {
        /** @var Leieforhold[] $leieforholdModeller */
        $leieforholdModeller  = [];

        // henter ut de innmeldte kombinasjonene av betalinger og krav
        $fordeling = [];
        foreach($_POST as $felt => $verdi) {
            if(substr($felt, 0, 4) == 'komb') {
                $id = substr($felt, 4);

                $felt = explode("_", $id);
                //    $felt[0]: Betalingsid for beløpet som skal fordeles
                //    $felt[1]: Krav-id eller 0 for betaling
                //    ($felt[2]: Leieforhold-id dersom $felt[1] er 0)


                settype( $fordeling[$felt[0]], 'array' );

                if( isset($_POST["verdi{$id}"])
                && is_numeric($_POST["verdi{$id}"]) ) {
                    $fordeling[$felt[0]][(int)$felt[1]] = array(
                        'beløp'            => $_POST["verdi{$id}"],
                        'leieforhold'    => intval($felt[2] ?? null)
                    );
                }
            }
        }


        foreach($fordeling as $innbetaling => $motparter) {
            /** @var Innbetaling $innbetalingsModell */
            $innbetalingsModell = $this->hentModell(Innbetaling::class, $innbetaling);

            if($innbetalingsModell->hentId()) {
                foreach($motparter as $kravid => $fordeling) {

                    // For utbetalinger må fordelingsbeløpet inverteres
                    if( $innbetalingsModell->hentBeløp() < 0 ) {
                        $fordeling['beløp'] *= (-1);
                    }

                    if( $kravid > 0 ) {
                        /** @var Krav $motkravModell */
                        $motkravModell = $this->hentModell(Krav::class, $kravid);
                        $leieforholdModell = null;
                    }
                    else {
                        /** @var Delbeløpsett $motkravDelbetalinger */
                        $motkravDelbetalinger = $this->hentSamling(Delbeløp::class);
                        $motkravDelbetalinger->leggTilFilter(['leieforhold' => strval($fordeling['leieforhold'])]);
                        $motkravDelbetalinger->leggTilFilter(['krav' => null]);
                        $motkravDelbetalinger->leggTilFilter([$innbetalingsModell->hentBeløp() < 0 ? 'beløp > 0' : 'beløp < 0'], true);
                        $motkravDelbetalinger->leggTilUttrykk('delbeløp_ekstra', 'eksakt_match', "IF(innbetalinger.beløp = '" . ($fordeling['beløp'] * (-1)) . "', 0, 1)");
                        $motkravDelbetalinger->leggTilUttrykk('delbeløp_ekstra', 'stort_nok', "IF(innbetalinger.beløp > '" . ($fordeling['beløp'] * (-1)) . "', 0, 1)");
                        $motkravDelbetalinger->leggTilSortering('delbeløp_ekstra.stort_nok');
                        $motkravDelbetalinger->leggTilSortering('delbeløp_ekstra.eksakt_match');

                        /** @var Innbetaling|null $motkravModell */
                        $motkravModell = $motkravDelbetalinger->hentFørste()
                            ? $motkravDelbetalinger->hentFørste()->hentInnbetaling()
                            : null
                        ;

                        if(!$motkravModell) {
                            /** @var Innbetaling $motkravModell */
                            $motkravModell = $this->hentModell(Innbetaling::class, null);
                        }
                        /** @var Leieforhold $leieforholdModell */
                        $leieforholdModell = $this->hentModell(Leieforhold::class, $fordeling['leieforhold']);
                    }

                    if( $fordeling['beløp'] != 0 ) {
                        $innbetalingsModell->avstem( $motkravModell, $fordeling['beløp'], $leieforholdModell );
                    }
                }
            }
        }

        foreach($leieforholdModeller as $leieforholdModell) {
            $betalingsplan = $leieforholdModell->hentBetalingsplan();
            if ($betalingsplan && $betalingsplan->hentAktiv()) {
                $betalingsplan->oppdaterUteståendeAvdrag();
                if($betalingsplan->hentUtestående() <= 0) {
                    $betalingsplan->settAktiv(false);
                }
            }
        }

        \Kyegil\Leiebasen\Vedlikehold::getInstance($this)->sjekkAvstemming();
        return json_encode(array(
            'success'    => true
        ));
    }
}