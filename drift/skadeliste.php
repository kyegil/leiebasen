<?php

use Kyegil\Leiebasen\Leiebase;

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    /**
     * @var string
     */
    public $tittel = 'leiebasen';

    /**
     * @var string
     */
    public $ext_bibliotek = 'ext-4.2.1.883';


    /**
     * oppsett constructor.
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
    }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        if (isset($_GET['returi']) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }
        $tp = $this->mysqli->table_prefix;
?>
Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', '<?=$this->http_host . '/pub/lib/' . $this->ext_bibliotek . "/examples/ux"?>');

Ext.require([
    'Ext.data.*',
    'Ext.form.field.*',
    'Ext.layout.container.Border',
    'Ext.grid.*',
    'Ext.ux.RowExpander'
]);

Ext.onReady(function() {

    Ext.tip.QuickTipManager.init();
    Ext.form.Field.prototype.msgTarget = 'side';
    Ext.Loader.setConfig({
        enabled:true
    });

    Ext.define('Skade', {
        extend: 'Ext.data.Model',
        idProperty: 'id',
        fields: [ // http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.Field
            {name: 'id', type: 'int'},
            {name: 'leieobjektnr', type: 'int'},
            {name: 'leieobjektbesk', type: 'string'},
            {name: 'navn', type: 'string'},
            {name: 'registrerer', type: 'string'},
            {name: 'registrert', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {name: 'skade', type: 'string'},
            {name: 'beskrivelse', type: 'string'},
            {name: 'utført', type: 'date', dateFormat: 'Y-m-d'},
            {name: 'sluttregistrerer', type: 'string'},
            {name: 'sluttrapport', type: 'string'},
            {name: 'html', type: 'string'}
        ]
    });

    var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
        clicksToEdit: 1
    });

    var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
        autoCancel: false,
        listeners: {
            beforeedit: function (grid, e, eOpts) {
                return e.column.xtype !== 'actioncolumn';
            }
        }
    });


    // oppretter datasettet
    var datasett = Ext.create('Ext.data.Store', {
        model: 'Skade',
        pageSize: 200,
        remoteSort: true,
        proxy: {
            type: 'ajax',
            simpleSortMode: true,
            actionMethods: {
                read: 'GET'
            },
            url:'/drift/index.php?oppslag=<?=$_GET['oppslag']?>&oppdrag=hentdata<?=isset($_GET['id']) ? "&id={$_GET['id']}" : ""?>',
            reader: {
                type: 'json',
                root: 'data',
                totalProperty: 'totalRows'
            }
        },
        sorters: [{
            property: 'id',
            direction: 'DESC'
        }],
        groupField: 'navn',
        autoLoad: {
            start: 0,
            limit: 300
        }
    });


    var kategorivelger = Ext.create('Ext.form.field.ComboBox', {
        fieldLabel: 'Kategori',
//        labelAlign: 'top',
//        labelWidth: 150,
//        tabIndex: 7,
        readOnly: false,
        name: 'kategori',

        matchFieldWidth: true,
        listConfig: {
            width: 500,
            maxHeight: 600
        },

        store: Ext.create('Ext.data.JsonStore', {
            storeId: 'skadekategoriliste',

            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: '/drift/index.php?oppslag=<?=$_GET['oppslag']?>&oppdrag=hentdata&data=kategori',
                reader: {
                    type: 'json',
                    root: 'data',
                    idProperty: 'kategori'
                }
            },

            fields: [
                {name: 'kategori'}
            ]
        }),
        queryMode: 'remote',
        displayField: 'kategori',
        valueField: 'kategori',
        hideLabel: false,
        minChars: 0,
        queryDelay: 1000,

        allowBlank: true,
        typeAhead: true,
        editable: true,
        selectOnFocus: false,
        forceSelection: false,
        typeAhead: false,
        listeners: {
            'change': function(){
                datasett.getProxy().extraParams = {kategori: kategorivelger.getValue()};
                datasett.load();
            },
            'select': function(){
                datasett.getProxy().extraParams = {kategori: kategorivelger.getValue()};
                datasett.load();
            }
        },
        width: 450
    });


    var id = {
        id: 'id',
        dataIndex: 'id',
        header: 'id',
        hidden: true,
        sortable: true,
        width: 50
    };

    var leieobjektnr = {
        dataIndex: 'leieobjektnr',
        header: 'Leil',
        renderer: function(value, metaData, record, rowIndex, colIndex, store){
            if(value) {
                return record.data.leieobjektbesk;
            }
            else {
                return record.data.navn;
            }
        },
        sortable: true,
        width: 200,
        flex: 1
    };

    var registrerer = {
        dataIndex: 'registrerer',
        header: 'Registrert av',
        sortable: true,
        width: 100
    };

    var registrert = {
        dataIndex: 'registrert',
        header: 'Registrert',
        renderer: Ext.util.Format.dateRenderer('d.m.Y'),
        sortable: true,
        width: 80
    };

    var beskrivelse = {
        dataIndex: 'beskrivelse',
        header: 'beskrivelse',
        sortable: true,
        width: 50
    };

    var utført = {
        dataIndex: 'utført',
        header: 'Utbedret',
        renderer: Ext.util.Format.hake,
        sortable: true,
        width: 40
    };

    var sluttregistrerer = {
        dataIndex: 'sluttregistrerer',
        header: 'sluttregistrerer',
        sortable: true,
        width: 50
    };

    var sluttrapport = {
        dataIndex: 'sluttrapport',
        header: 'sluttrapport',
        sortable: true,
        width: 50
    };

    var nyskadeknapp = Ext.create('Ext.button.Button', {
        text: 'Meld ny skade',
        scale: 'medium',
        handler: function() {
            window.location = "/drift/index.php?oppslag=skade_skjema<?=isset($_GET['id']) ? "&leieobjektnr=" . (int)$_GET['id'] : ""?>&id=*";
        }
    });

    var rutenett = Ext.create('Ext.grid.Panel', {
        title: 'Registrerte skader<?=isset($_GET['id']) ? (" i " . $this->leieobjekt((int)$_GET['id'], true)) : ""?>',
        autoScroll: true,
        layout: 'border',
        renderTo: 'panel',
        store: datasett,
        columns: [
            id,
            registrert,
            leieobjektnr,
            registrerer,
            utført
        ],
        autoExpandColumn: 2,
        viewConfig: {
            stripeRows: true
//            enableRowBody: true,
//            showPreview: true,
//            getRowClass : function(record, rowIndex, p, ds){
//                if(this.showPreview){
////                    p.body = '' + record.data.skade + '';
//                    return 'x-grid-row-expanded';
//                }
//                return 'x-grid-row-collapsed';
//            }
        },

        //plugins: [{
        //    ptype: 'rowexpander',
        //    rowBodyTpl : new Ext.XTemplate('{skade}')
        //}],

        features: [{
            ftype: 'rowbody',
            getAdditionalData: function (data, rowIndex, record, rowValues) {
                return {
                    rowBody: Ext.String.format('{0}', record.get("skade")),
                    rowBodyCls: this.rowBodyCls,
                    rowBodyColspan: this.view.headerCt.getColumnCount()
                };
            }
            //setupRowData: function(record, rowIndex, rowValues) {
            //    var headerCt = this.view.headerCt,
            //        colspan = headerCt.getColumnCount();
            //    // Usually you would style the my-body-class in CSS file
            //    return {
            //        rowBody: '<div style="padding: 1em">'+record.get("skade")+'</div>',
            //        rowBodyCls: "my-body-class",
            //        rowBodyColspan: colspan
            //    };
            //}
        }],
        tbar: [kategorivelger],
        dockedItems: [{
            xtype: 'pagingtoolbar',
            store: datasett,
            dock: 'bottom'
        }],

        buttons: [
            {
                text: 'Tilbake',
                scale: 'medium',
                handler: function() {
                    window.location = '<?=$this->returi->get();?>';
                }
            },
            nyskadeknapp
        ],
        height: 600,
        width: 500
    });

    var visknapp = Ext.create('Ext.button.Button', {
        text: 'Vis detaljer',
        scale: 'medium',
        handler: function() {
            window.location = "/drift/index.php?oppslag=skadekort<?=isset($_GET['id']) ? "&leieobjektnr=" . (int)$_GET['id'] : ""?>&id=" + rutenett.getSelectionModel().getLastSelected().data.id;
        },
        disabled: true
    });

    var visalleknapp = Ext.create('Ext.button.Button', {
        text: 'Vis alle registrerte skader',
        scale: 'medium',
        handler: function() {
            window.location = "/drift/index.php?oppslag=skadeliste";
        },
        hidden: <?php echo (isset($_GET['id']) and (int)$_GET['id']) ? 'false' : 'true';?>
    });

    // Oppretter detaljpanelet
    var detaljpanel = Ext.create('Ext.panel.Panel', {
        title: 'Detaljer',
        frame: false,
        height: 600,
        width: 400,
        items: [
            {
                autoScroll: true,
                id: 'detaljfelt',
                region: 'center',
                bodyStyle: {
                    background: '#ffffff',
                    padding: '7px'
                },
                html: 'Velg en skade i listen til venstre for å se flere detaljer.'
            }
        ],
        buttons: [visknapp, visalleknapp],
        layout: 'border',
        renderTo: 'detaljpanel'
    })



    // Hva skjer når du klikker på ei linje i rutenettet?:
    rutenett.on('select', function( rowModel, record, index, eOpts ) {
        var detaljfelt = Ext.getCmp('detaljfelt');

        // Format for detaljvisningen
        var detaljer = new Ext.Template([
            '{html}'
        ]);
        detaljer.overwrite(detaljfelt.body, record.data);
        visknapp.enable();
        if(!record.data.utført) {
            utbedreknapp.enable();
        }
        else{
            utbedreknapp.disable();
        }
    });

});
<?php
        return ob_get_clean();
    }

    /**
     *
     */
    public function design() {
?>
    <table style="text-align: left; width: 100%;" border="0" cellpadding="2" cellspacing="0">
        <tbody>
            <tr>
                <td style="vertical-align: top; width: 750px;"><div id="panel" class="extjs-panel"></div></td>
                <td style="vertical-align: top;"><div id="detaljpanel"></div></td>
            </tr>
        </tbody>
    </table>
<?php
    }

    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ""):string {
        $tp = $this->mysqli->table_prefix;
        switch ($data) {
            case "kategori":
                $filter = [];
                if(isset($_GET['query']) && $_GET['query']) {
                    $filter = ["kategori LIKE '%{$this->GET['query']}%'"];
                }
                $resultat = $this->mysqli->arrayData([
                    'source' => "{$tp}skadekategorier AS skadekategorier",
                    'fields' => ['kategori'],
                    'groupfields' => 'kategori',
                    'where' => $filter,
                    'returnQuery' => true
                ]);
                return json_encode($resultat);

            default:
                $filter = [];
                if(isset($_GET['kategori'])) {
                    $filter[] = ['kategori' => $_GET['kategori']];
                }
                if(isset($_GET['id'])) {
                    $bygning = $this->mysqli->arrayData([
                        'source' => "{$tp}leieobjekter as leieobjekter",
                        'fields' => 'bygning',
                        'where' => [
                            'leieobjektnr' => (int)$_GET['id']
                        ]
                    ]);
                    $bygning = $bygning->totalRows ? $bygning->data[0]->bygning : '';
                    $filter[] = [
                        'or' => [
                            'leieobjektnr' => $_GET['id'],
                            'and' => [
                                '!leieobjektnr',
                                'bygning' => $bygning
                            ]
                        ]
                    ];
                }

                $resultat = $this->mysqli->arrayData([
                    'source' => "{$tp}skader AS skader LEFT JOIN {$tp}skadekategorier as skadekategorier ON skader.id = skadekategorier.skadeid\n"
                            .   "LEFT JOIN {$tp}bygninger as bygninger ON skader.bygning = bygninger.id",
                    'fields' => [
                        'skader.id', 'skade', 'leieobjektnr', 'registrerer', 'registrert', 'beskrivelse', 'utført', 'sluttregistrerer', 'sluttrapport', 'bygninger.navn'
                    ],
                    'where' => $filter,
                    'groupfields' => 'skader.id',
                    'orderfields' => 'skader.id DESC',
                    'returnQuery' => true
                ]);
                foreach($resultat->data as $verdi){
                    $kategorirad = array();
                    $kategorier = $this->mysqli->arrayData([
                        'source' => "{$tp}skadekategorier as skadekategorier",
                        'fields' => ['kategori'],
                        'where' => ['skadeid' => $verdi->id],
                        'groupfields' => 'kategori'
                    ]);
                    foreach($kategorier->data as $kategori){
                        $kategorirad[] = $kategori->kategori;
                    }
                    $verdi->leieobjektbesk = $this->leieobjekt($verdi->leieobjektnr, true, false);
                    $html = ($verdi->leieobjektnr ? $this->leieobjekt($verdi->leieobjektnr, true) : $verdi->navn) . "<br>";
                    if($verdi->utført){
                        $html .= "<h1>Skaden er utbedret</h1>";
                        $html .= "{$verdi->skade}<br><br>";
                        $html .= "Meldt utbedret av {$verdi->sluttregistrerer} " . date('d.m.Y', strtotime($verdi->utført)) . ":<br>";
                        $html .= "{$verdi->sluttrapport}<br>";
                        $html .= "<b>Opprinnelig beskrivelse:</b><br>";
                        $html .= "{$verdi->beskrivelse}<br>";
                    }
                    else{
                        $html .= "<h1>{$verdi->skade}</h1>";
                        $html .= "Registrert av {$verdi->registrerer} " . date('d.m.Y', strtotime($verdi->registrert)) . ":<br>";
                        if($kategorirad){
                            $html .= count($kategorirad) > 1 ? "Kategorier: <b>" : "Kategori: <b>";
                            $html .= Leiebase::liste($kategorirad) . "</b><br><br>";
                        }
                        $html .= "{$verdi->beskrivelse}";
                    }

                    $verdi->html = $html;
                }
                return json_encode($resultat);
        }
    }

    /**
     * @param string $skjema
     * @return string
     */
    public function taIMot($skjema):string {
        return json_encode(null);
    }
}