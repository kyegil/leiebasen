<?php

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    public $ext_bibliotek = 'ext-3.4.0';

    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);

        $this->hoveddata['sql'] =    array();
        $this->tittel = "Adresseutskrift";
    //    $this->mal = "_utskrift.php";
    }

    public function lagPDF( $data ) {
        switch($data) {
            case "konvolutter":
                $leieforholdsett = explode(",", $_GET['leieforhold']);

                $pdf = new \Fpdf\Fpdf('L', 'mm', array(114, 229));
                $pdf->SetAutoPageBreak(false);

                foreach($leieforholdsett AS $leieforholdnr) {
                    /** @var \Leieforhold|null $leieforhold */
                    $leieforhold = $this->hent(\Leieforhold::class, $leieforholdnr );
                    $adresse = ($leieforhold->hent('navn') . "\n" . $leieforhold->hent('adressefelt'));

                    $adresse = utf8_decode($adresse);

                    $pdf->AddPage();
                    $pdf->SetFont('Arial','',9);
                    $pdf->setY($this->hentValg('konvolutt_marg_topp'));
                    $pdf->setX($this->hentValg('konvolutt_marg_venstre'));
                    $pdf->MultiCell(75, 3.5, $adresse, false, 'L');
                }

                $pdf->Output("konvoluttadresser.pdf", 'I');
                break;
            default:
                break;
        }
    }
}