<?php

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    public $ext_bibliotek = 'ext-3.4.0';
    public $tittel = 'OCR konteringsfiler';

            /**
     * @param array $di
     * @param array $config
     * @throws Throwable
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
    }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        if (isset($_GET['returi']) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }
?>

Ext.onReady(function() {
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';

    // oppretter datasettet
    var datasett = new Ext.data.JsonStore({
        url:'/drift/index.php?oppdrag=hentdata&oppslag=ocr_liste',
        fields: [
            {name: 'registrerer'},
            {name: 'registrert', type: 'date', dateFormat: 'Y-m-d H:i:s'},
            {name: 'id', type: 'float'},
            {name: 'fil_id', type: 'float'},
            {name: 'forsendelsesnummer', type: 'float'},
            {name: 'oppdragsnummer', type: 'float'},
            {name: 'oppdragskonto'},
            {name: 'avtaleid', type: 'float'},
            {name: 'transaksjonstype', type: 'float'},
            {name: 'transaksjonsnummer', type: 'float'},
            {name: 'oppgjørsdato', type: 'date', dateFormat: 'Y-m-d'},
            {name: 'bankdatasentral', type: 'float'},
            {name: 'delavregningsnummer', type: 'float'},
            {name: 'løpenummer', type: 'float'},
            {name: 'beløp', type: 'float'},
            {name: 'kid'},
            {name: 'blankettnummer', type: 'float'},
            {name: 'arkivreferanse'},
            {name: 'oppdragsdato', type: 'date', dateFormat: 'Y-m-d'},
            {name: 'debetkonto'},
            {name: 'fritekst'}
        ],
        totalProperty: 'totalRows',
        remoteSort: true,
        root: 'data'
    });

    var lastData = function() {
        datasett.baseParams = {
            søkefelt: ""
        };
        datasett.load({
            params: {
                start: 0,
                limit: 300
            }
        });
    }

    lastData();

    var forsendelsesnummer = {
        dataIndex: 'forsendelsesnummer',
        header: 'Forsendelse',
        align: 'right',
        renderer: function(value, metaData, record){
            return '<a href="/drift/index.php?oppslag=ocr_kort&id=' + record.data.fil_id + '\">' + value + '</a>';
        },
        sortable: true,
        width: 70
    };

    var registrerer = {
        dataIndex: 'registrerer',
        header: 'Registrert av',
        hidden: true,
        sortable: true,
        width: 100
    };

    var registrert = {
        dataIndex: 'registrert',
        header: 'Registrert dato',
        hidden: true,
        renderer: Ext.util.Format.dateRenderer('d.m.Y H:i:s'),
        sortable: true,
        width: 110
    };

    var oppdragsnummer = {
        dataIndex: 'oppdragsnummer',
        header: 'Oppdrag',
        hidden: true,
        align: 'right',
        sortable: true,
        width: 50
    };

    var oppdragskonto = {
        dataIndex: 'oppdragskonto',
        header: 'Oppdragskto',
        hidden: true,
        sortable: true,
        width: 70
    };

    var avtaleid = {
        dataIndex: 'avtaleid',
        header: 'AvtaleID',
        align: 'right',
        hidden: true,
        sortable: true,
        width: 50
    };

    var transaksjonstype = {
        dataIndex: 'transaksjonstype',
        header: 'Transaksjonstype',
        align: 'right',
        hidden: false,
        renderer: function(v){
            ttype = "";
            if(v == 10) ttype = 'Giro belastet konto';
            if(v == 11) ttype = 'Faste Oppdrag';
            if(v == 12) ttype = 'Direkte Remittering';
            if(v == 13) ttype = 'BTG (Bedrifts Terminal Giro)';
            if(v == 14) ttype = 'SkrankeGiro';
            if(v == 15) ttype = 'AvtaleGiro';
            if(v == 16) ttype = 'TeleGiro';
            if(v == 17) ttype = 'Giro betalt kontant';
            if(v == 18) ttype = 'Reversering med KID';
            if(v == 19) ttype = 'Kjøp med KID';
            if(v == 20) ttype = 'Reversering med fritekst';
            if(v == 21) ttype = 'Kjøp med fritekst';
            return v + ": " + ttype;
        },
        sortable: true,
        width: 100
    };

    var transaksjonsnummer = {
        dataIndex: 'transaksjonsnummer',
        header: 'Tr.nr',
        align: 'right',
        sortable: true,
        width: 30
    };

    var oppgjørsdato = {
        dataIndex: 'oppgjørsdato',
        header: 'Oppgjørsdato',
        renderer: Ext.util.Format.dateRenderer('d.m.Y'),
        sortable: true,
        width: 80
    };

    var bankdatasentral = {
        dataIndex: 'bankdatasentral',
        header: 'Bankdatasentral',
        align: 'right',
        hidden: true,
        sortable: true,
        width: 90
    };

    var delavregningsnummer = {
        dataIndex: 'delavregningsnummer',
        header: 'Delavregn',
        align: 'right',
        sortable: true,
        width: 70
    };

    var løpenummer = {
        dataIndex: 'løpenummer',
        header: 'Løpenr',
        align: 'right',
        sortable: true,
        width: 50
    };

    var beløp = {
        dataIndex: 'beløp',
        header: 'Beløp',
        align: 'right',
        renderer: Ext.util.Format.noMoney,
        sortable: true,
        width: 70
    };

    var kid = {
        dataIndex: 'kid',
        header: 'KID',
        sortable: true,
        width: 90
    };

    var blankettnummer = {
        dataIndex: 'blankettnummer',
        header: 'Blankettnr',
        align: 'right',
        hidden: true,
        sortable: true,
        width: 70
    };

    var arkivreferanse = {
        dataIndex: 'arkivreferanse',
        header: "Nets' Arkivref",
        align: 'right',
        sortable: true,
        width: 80
    };

    var oppdragsdato = {
        dataIndex: 'oppdragsdato',
        header: 'Oppdragsdato',
        renderer: Ext.util.Format.dateRenderer('d.m.Y'),
        sortable: true,
        width: 80
    };

    var debetkonto = {
        dataIndex: 'debetkonto',
        header: 'Debetkto',
        sortable: true,
        width: 80
    };

    var fritekst = {
        dataIndex: 'fritekst',
        header: 'Fritekst',
        sortable: true,
        width: 100
    };


    var søkefelt = new Ext.form.TextField({
        fieldLabel: 'Søk',
        name: 'søkefelt',
        width: 200,
        listeners: {
            'valid': function(){
                datasett.baseParams = {søkefelt: søkefelt.getValue()};
                datasett.load({params: {start: 0, limit: 300}});
            }
        }
    });

    var bunnlinje = new Ext.PagingToolbar({
        pageSize: 300,
        items: [
            søkefelt
        ],
        store: datasett,
        displayInfo: true,
        displayMsg: 'Viser linje {0} - {1} av {2}',
        emptyMsg: "Venter på resultat"
    });


    var rutenett = new Ext.grid.GridPanel({
        store: datasett,
        title: 'Registrerte OCR-oppdrag med innbetalinger',
        columns: [
            oppgjørsdato,
            forsendelsesnummer,
            løpenummer,
            transaksjonsnummer,
            registrerer,
            registrert,
            oppdragsnummer,
            oppdragskonto,
            avtaleid,
            transaksjonstype,
            bankdatasentral,
            delavregningsnummer,
            beløp,
            kid,
            blankettnummer,
            arkivreferanse,
            oppdragsdato,
            debetkonto,
            fritekst
        ],
        autoExpandColumn: 10,
        stripeRows: true,
        height: 600,
        bbar: bunnlinje

    });

    rutenett.on({
        rowdblclick: function(grid, rowIndex){
            window.location = "/drift/index.php?oppslag=ocr_kort&id=" + datasett.getAt(rowIndex).get('fil_id');
        },
        activate: function() {
            bevegelsesliste.getView().focusRow(passerte_datoer);
        }
    });

    // Rutenettet rendres in i HTML-merket '<div id="panel">':
    rutenett.render('panel');

});
<?php
        return ob_get_clean();
    }

    /**
     *
     */
    public function design() {
?>
<div id="panel" class="extjs-panel"></div>
<?php
    }

    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ""):string {
        switch ($data) {

            default: {
                $tp = $this->mysqli->table_prefix;
                $søkefelt = @$this->POST['søkefelt'];
                $sort = @$this->POST['sort'];
                $dir = @$this->POST['dir'];
                $limit = isset($_POST['start']) ? ((int)$_POST['start'] . ", " . (int)$_POST['limit']): "";

                $filter = (
                    $søkefelt
                    ? ("`ocr_filer`.`forsendelsesnummer` LIKE '%{$søkefelt}%'\nOR `ocr_filer`.`forsendelsesnummer` LIKE '%{$søkefelt}%'\nOR `ocr_filer`.`oppgjørsdato` LIKE '" . date('Y-m-d', strtotime($søkefelt)) . "'\nOR `ocr_transaksjoner`.`oppgjørsdato` LIKE '" . date('Y-m-d', strtotime($søkefelt)) . "'\nOR `ocr_transaksjoner`.`oppdragsdato` LIKE '" . date('Y-m-d', strtotime($søkefelt)) . "'\nOR `ocr_transaksjoner`.`løpenummer` LIKE '%{$søkefelt}%'\nOR `ocr_transaksjoner`.`beløp` LIKE '%" . str_replace(",", ".", $søkefelt) . "%'\nOR `ocr_transaksjoner`.`kid` LIKE '%{$søkefelt}%'\nOR `ocr_transaksjoner`.`blankettnummer` LIKE '%{$søkefelt}%'\nOR `ocr_transaksjoner`.`arkivreferanse` LIKE '%{$søkefelt}%'\nOR `ocr_transaksjoner`.`debetkonto` LIKE '%{$søkefelt}%'\nOR `ocr_transaksjoner`.`fritekst` LIKE '%{$søkefelt}%'")
                    : NULL
                );

                $resultat = $this->mysqli->arrayData(array(
                    'source'        => "`{$tp}ocr_filer` AS `ocr_filer` LEFT JOIN `{$tp}ocr_transaksjoner` AS `ocr_transaksjoner` ON `ocr_filer`.`fil_id` = `ocr_transaksjoner`.`fil_id`",

                    'fields'        => "`ocr_filer`.`registrerer`, `ocr_filer`.`registrert`, `ocr_transaksjoner`.*",

                    'orderfields'    => ($sort ? "{$sort} {$dir}\n" : "`ocr_transaksjoner`.`oppgjørsdato` DESC, `ocr_filer`.`forsendelsesnummer` DESC, `ocr_transaksjoner`.`transaksjonsnummer`\n"),

                    'limit'            => $limit,

                    'where'         => $filter,
                    'returnQuery'    => true
                ));
                return json_encode($resultat);
            }
        }
    }

    /**
     * @param string $skjema
     * @return string
     */
    public function taIMot($skjema):string {
        return json_encode(null);
    }
}