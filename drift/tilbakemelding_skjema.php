<?php

if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    public $ext_bibliotek = 'ext-3.4.0';

    public function __construct(array $di = [], array $config = []) {
    	parent::__construct($di, $config);
    }

    /**
     * @return string
     */
    public function skript():string {
        ob_start();
        $oppslag = $_GET['oppslag'] ?? '';
        $id = !empty($_GET['id']) ? $_GET['id'] : '';?>

    Ext.onReady(function() {

        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'side';

        let melding = new Ext.form.TextArea({
            allowBlank: false,
            fieldLabel: 'Melding',
            height: 360,
            name: 'melding',
            width: 800
        });

        let skjema = new Ext.FormPanel({
            autoScroll: true,
            bodyStyle:'padding:5px 5px 0',
            buttons: [],
            frame:true,
            height: 600,
            items: [melding],
            labelAlign: 'top', // evt right
            standardSubmit: false,
            title: 'Send feilmelding eller forbedringsforslag'
        });

        let lagreknapp = skjema.addButton({
            text: 'Send',
            disabled: false,
            handler: function(){
                skjema.form.submit({
                    url:'/drift/index.php?oppslag=<?="{$oppslag}&id={$id}";?>&oppdrag=taimotskjema',
                    waitMsg:'Meldingen sendes...'
                    });
            }
        });

    	skjema.render('panel');

        skjema.on({
            actioncomplete: function(form, action){
                if(action.type === 'submit'){
                    if(action.response.responseText === '') {
                        Ext.MessageBox.alert('Problem', 'Mottok ikke bekreftelsesmelding fra tjeneren  i JSON-format som forventet');
                    } else {
                        window.location = "index.php";
                        Ext.MessageBox.alert('Suksess', 'Takk for din tilbakemelding');
                    }
                }
            },

            actionfailed: function(form,action){
                if(action.type === 'submit') {
                    if (action.failureType === "connect") {
                        Ext.MessageBox.alert('Problem:', 'Klarte ikke sende. Fikk ikke kontakt med tjeneren.');
                    }
                    else {
                        var result = Ext.decode(action.response.responseText);
                        if(result && result.msg) {
                            Ext.MessageBox.alert('Mottatt tilbakemelding om feil:', result.msg);
                        }
                        else {
                            Ext.MessageBox.alert('Problem:', 'Lagring av data mislyktes av ukjent grunn. Action type='+action.type+', failure type='+action.failureType);
                        }
                    }
                }

            } // end actionfailed listener
        }); // end skjema.on
    });
    <?php
        return ob_get_clean();
    }

    /**
     *
     */
    public function design() {
        ?><div id="panel" class="extjs-panel"></div><?php
    }

    /**
     * @param string $skjema
     * @return string
     */
    public function taIMot($skjema):string {
        $developerEmail = \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['debug']['developer_email'] ?? null;
        $resultat['success'] = $this->sendMail((object)[
            'to' => $developerEmail,
            'subject' => "Melding via leiebasen",
            'html' => $this->POST['melding'],
            'from' => "{$this->bruker['navn']}<{$this->bruker['epost']}>"
        ]);
        return json_encode($resultat);
    }

    /**
     * @param string $data
     * @return string
     */
    public function hentData($data = ''):string {
        switch ($data) {
            default:
                $resultat = "";
                return json_encode($resultat);
        }
    }
}