<?php


if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    public $ext_bibliotek = 'ext-3.4.0';

    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $this->ext_bibliotek = 'ext-3.4.0';
        $this->hoveddata['sql'] =    "SELECT * FROM ocr_filer WHERE `fil_id` = '{$_GET['id']}'";
        $ocr = $this->arrayData($this->hoveddata['sql']);
        header('Content-type: text/plain; charset=utf-8');
        header('Content-Disposition: attachment; filename="OCR-' . $ocr['data'][0]['forsendelsesnummer'] . '.txt"');
        die($ocr['data'][0]['ocr']);
    }
}