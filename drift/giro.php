<?php


if(!defined('LEGAL')) {
    die('Ingen adgang - No access!<br>Sjekk at adressen du har oppgitt er riktig.');
}

/**
 * Class oppsett
 */
class oppsett extends \Kyegil\Leiebasen\Oppslag\EksKontroller {

    /**
     * @var string
     */
    public $ext_bibliotek = 'ext-3.4.0';


    /**
     * oppsett constructor.
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
    }

    public function lagPDF( $giro = null ) {
        $giro = $giro ?: ($_GET['gironr'] ?? null);

        if( $giro ) {
            try {
                /** @var \Kyegil\Leiebasen\Modell\Leieforhold\Regning $giro */
                $giro = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold\Regning::class, $giro);
                $pdf = $giro->hentPdf();
                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename="' . $giro->hentId() . '.pdf"');
                header('Content-Transfer-Encoding: binary');
                header('Content-Length: ' . strlen($pdf));
                header('Accept-Ranges: bytes');
                return $pdf;
            } catch (Exception $e) {
                $this->responder404();
            }
        }
    
        else {
            // Dette er ikke en enkeltgiro, men siste utskriftsbunke
            $fil = "{$this->filarkiv}/giroer/_utskriftsbunke.pdf";

            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="giroutskrift ' . date('Y-m-d H:i:s') . '.pdf"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: ' . filesize($fil));
            header('Accept-Ranges: bytes');

            @readfile( $fil );
        }
    }
}