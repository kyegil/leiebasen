<?php


use Kyegil\CoreModel\CoreModel;
use Kyegil\Leiebasen\CoreModelImplementering;
use Kyegil\Leiebasen\EventManager;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Nets\NetsException;
use Kyegil\Leiebasen\Nets\Oppkopling;
use Kyegil\Leiebasen\Nets\Prosessor as NetsProsessor;
use Kyegil\Nets\Forsendelse\Oppdrag\AvtaleGiro\BetalingskravOppdrag;
use Kyegil\Nets\Forsendelse\Oppdrag\AvtaleGiro\Sletteoppdrag;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

const LEGAL = true;
set_error_handler(function(int $errno, string $errstr, $errfile, $errline){
    $error_reporting = error_reporting();
    if ($error_reporting & $errno) {
        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
    }
    return false;
});
set_exception_handler(function (Throwable $e):void {
    error_log(substr(addslashes($e), 0, 8000) . "\n", 3, dirname(__FILE__) . '/var/log/Error.log');
    $developerEmail = \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['debug']['developer_email'] ?? null;
    if ($developerEmail) {
        error_log($e, 1, $developerEmail);
    }
});

require_once(dirname(__FILE__) . '/klasser/index.php');
Leiebase::setConfig(include(dirname(__FILE__) . '/config.php'));

CoreModel::$logger = new \Monolog\Logger('CoreModel');
if(!CoreModel::$logger->getHandlers()) {
    CoreModel::$logger->pushHandler(
        new StreamHandler(
            (Leiebase::$config['leiebasen']['server']['root'] ?? '') . 'var/log/CoreModel.log',
            Leiebase::$config['leiebasen']['logger']['level'] ?? Logger::INFO
        )
    );
}
set_time_limit(0);

$tid = time();

$leiebase = new \Kyegil\Leiebasen\CoreModelImplementering(
    \Kyegil\Leiebasen\Oppslagsbestyrer::forberedDependencies([]),
    []
);
$mysqliConnection = $leiebase->mysqli;
$cronTid = new DateTimeImmutable('now', new DateTimeZone('UTC'));
//$cronTid = new DateTimeImmutable('2023-06-05 11:00:00', new DateTimeZone('UTC'));


//    Ingen er logget inn, så det opprettes en egen profil for de automatiske prossessene
$leiebase->bruker = [
    'navn' => 'cron',
    'id' => '',
    'brukernavn' => 'crontab',
    'epost' => ''
];

$coreModelImplementering = $leiebase->hentCoreModelImplementering();
$coreModelImplementering->bruker = $leiebase->bruker;

/**
 * Kontrollrutiner
 */
EventManager::addListener(get_class($leiebase), 'cron', function(CoreModelImplementering $leiebase, array $argumenter = []) {
    try {
        \Kyegil\Leiebasen\Vedlikehold::cron($leiebase, $argumenter);
    } catch (Throwable $e) {
        $leiebase->logger->critical($e->getMessage(), $e->getTrace());
    }
    return $argumenter;
});

/**
 * Send varsel om kontrakter som er i ferd med å utløpe
 */
EventManager::addListener(get_class($leiebase), 'cron', function(CoreModelImplementering $leiebase, array $argumenter = []) {
    try {
        $leiebase->varsleFornying();
    } catch (Exception $e) {
        $leiebase->logger->critical($e->getMessage(), $e->getTrace());
    }
    return $argumenter;
});

/**
 * Send bekreftelse på alle nye innbetalinger som er mer en 2 timer gamle
 */
EventManager::addListener(get_class($leiebase), 'cron', function(CoreModelImplementering $leiebase, array $argumenter = []) {
    try {
        $leiebase->varsleNyeInnbetalinger(date_create()->sub(new DateInterval('PT2H')));
    } catch (Exception $e) {
        $leiebase->logger->critical($e->getMessage(), $e->getTrace());
    }
    return $argumenter;
});

/**
 * Send påminnelse om alle ubetalte krav som er i ferd med å forfalle
 */
EventManager::addListener(get_class($leiebase), 'cron', function(CoreModelImplementering $leiebase, array $argumenter = []) {
    try {
        $leiebase->varsleForfall();
    } catch (Exception $e) {
        $leiebase->logger->critical($e->getMessage(), $e->getTrace());
    }
    return $argumenter;
});

/**
 * Send påminnelse om alle nylig forfalte krav
 */
EventManager::addListener(get_class($leiebase), 'cron', function(CoreModelImplementering $leiebase, array $argumenter = []) {
    try {
        $leiebase->sendUmiddelbareBetalingsvarsler('epost');
    } catch (Exception $e) {
        $leiebase->logger->critical($e->getMessage(), $e->getTrace());
    }
    return $argumenter;
});

/**
 * Send påminnelse om kommende avdrag i betalingsplan
 */
EventManager::addListener(get_class($leiebase), 'cron', function(CoreModelImplementering $leiebase, array $argumenter = []) {
    try {
        $leiebase->varsleBetalingsplanKommendeAvdrag();
    } catch (Exception $e) {
        $leiebase->logger->critical($e->getMessage(), $e->getTrace());
    }
    return $argumenter;
});

/**
 * Send etterlysning etter manglende avdrag i betalingsplan
 */
EventManager::addListener(get_class($leiebase), 'cron', function(CoreModelImplementering $leiebase, array $argumenter = []) {
    try {
        $leiebase->varsleBetalingsplanManglendeAvdrag();
    } catch (Exception $e) {
        $leiebase->logger->critical($e->getMessage(), $e->getTrace());
    }
    return $argumenter;
});

/**
 * Send epost til utleier om brutt betalingsplan
 */
EventManager::addListener(get_class($leiebase), 'cron', function(CoreModelImplementering $leiebase, array $argumenter = []) {
    try {
        $leiebase->varsleBetalingsplanBruttAvtale();
    } catch (Exception $e) {
        $leiebase->logger->critical($e->getMessage(), $e->getTrace());
    }
    return $argumenter;
});

/**
 * Send automatiske regninger
 */
EventManager::addListener(get_class($leiebase), 'cron', [$leiebase->hentUtskriftsprosessor(), 'cron']);

/**
 * Opprett leiekrav i alle leieforhold hvor slike mangler
 */
EventManager::addListener(get_class($leiebase), 'cron', function(CoreModelImplementering $leiebase, array $argumenter = []) {
    try {
        $leiebase->opprettLeiekrav();
    } catch (Exception $e) {
        $leiebase->logger->critical($e->getMessage(), $e->getTrace());
    }
    return $argumenter;
});

/**
 * Hent NETS-forsendelser og lagre på lokal tjener
 */
EventManager::addListener(get_class($leiebase), 'cron', function(CoreModelImplementering $leiebase, array $argumenter = []) {
    try {
        if($leiebase->hentValg('ocr') || $leiebase->hentValg('avtalegiro')) {
            $cronTid = $argumenter[0] ?? date_create_immutable('now', new DateTimeZone('UTC'));
            $ocrOppkopling = $leiebase->hentNetsForbindelse(Oppkopling::TJENESTE_OCR);
            $ocrOppkopling->settOcrFeilmelding();
            $ocrOppkopling->opprettForbindelse();
            $filerForNedlasting = $ocrOppkopling->seEtterFilerForNedlasting();
            try {
                $ocrOppkopling->lastNedFraNets($filerForNedlasting);
            } catch (NetsException $e) {
                if(!$leiebase->live) {
                    $leiebase->logger->warning('NETS – ' . $e->getMessage());
                }
            }
            $ocrOppkopling->behandleInnkommenPost();

            /**
             * For testing er det mulig å sende et stdClass-objekt til denne metoden,
             * med full lokal filbane som egenskap eller med filinnholdet som egenskapens verdi.
             * Eksempel:
             *  $ocrOppkopling->behandleInnkommenPost((object)[\Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['file_storage'] . '/nets/inn/ocr/2020-08/OCR.D200820' => null]);
             */
            $ocrOppkopling->behandleInnkommenPost((object)[
                /** OCR konteringsdata */
//             \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['file_storage'] . '/nets/inn/ocr/2024-07/mandates-42021468082.txt' => null,
                /** FBO-avtale for leieforhold 79 */
//             \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['file_storage'] . '/nets/inn/ocr/2024-09/OCR.D020924' => null,
                /** Kvittering for AvtaleGiro-trekk (Avvist) */
//             \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['file_storage'] . '/nets/inn/AG-kvitteringer/2021-01/KV.AVVIST.D210112.T170647.K4636532.html' => null,
                /** Kvittering for AvtaleGiro-trekk (Godkjent) */
//             \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['file_storage'] . '/nets/inn/AG-kvitteringer/2021-01/KV.GODKJENT.F0112014.D210112.T190659.K4636584.html' => null,
            ]);

            if($leiebase->hentValg('avtalegiro')) {
                $leiebase->netsSlettUsendteAvtalegiroer()
                    ->forberedRegningsTrekkForSletting()
                    ->forberedBetalingsplanTrekkForSletting()
                ;

                /*
                 * AvtaleGiroer oversendes i hht \Kyegil\Leiebasen\Nets\Oppkopling::nesteFboTrekkravForsendelse
                 * siste kvarteret hver time mellom kl 10 og 14
                 */
                if($cronTid->format('N') < 6 && $cronTid->format('H') >= 10 && $cronTid->format('H') < 14 && $cronTid->format('i') >= 45) {
                    $leiebase->logger->debug('Forbereder overføring av eventuelle AvtaleGiroer');
                    /** @var Sletteoppdrag|null $sletteoppdrag */
                    $sletteoppdrag = $leiebase->fboLagSletteOppdrag();
                    if($sletteoppdrag) {
                        $ocrOppkopling->leggTilOppdragUt($sletteoppdrag);
                    }
                    /** @var BetalingskravOppdrag|null $trekkoppdrag */
                    $trekkoppdrag = $leiebase->fboLagBetalingskravOppdrag();
                    if($trekkoppdrag) {
                        $ocrOppkopling->leggTilOppdragUt($trekkoppdrag);
                    }
                    if($ocrOppkopling->hentOppdragUt()) {
                        $ocrOppkopling->lagAvtalegiroForsendelse();
                    }
                    $leiebase->logger->debug('Fullført overføring av AvtaleGiroer');
                }
            }

            $leiebase->hentNetsProsessor()->fboVarsle();

            /*
             * Ved behov er det mulig å sende AvtaleGiro trekkforsendelse til Nets manuelt.
             * Pass på at fila er i ISO-8859-1 tegnkoding (ikke UTF)
             */
//        $ocrOppkopling->leggTilUtgåendePost(\Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['file_storage'] . '/nets/ut/2022-11/Dirrem20221114-1114001.txt');

            $ocrOppkopling->sendUtgåendePost();
        }
    } catch (Exception $e) {
        $leiebase->logger->critical($e->getMessage(), $e->getTrace());
    }
    return $argumenter;
});

EventManager::addListener(get_class($leiebase), 'cron', function(CoreModelImplementering $leiebase, array $argumenter = []) {
    try {
        if($leiebase->hentValg('efaktura') && $leiebase->hentValg('efaktura_kommunikasjonsmåte') == 'sftp') {
            $efakturaOppkopling = $leiebase->hentNetsForbindelse(Oppkopling::TJENESTE_EFAKTURA);
            $efakturaOppkopling->opprettForbindelse(Leiebase::$config['leiebasen']['nets']['user']['einvoice'] ?? null);
            $filerForNedlasting = $efakturaOppkopling->seEtterFilerForNedlasting();
            try {
                $efakturaOppkopling->lastNedFraNets($filerForNedlasting);
            } catch (NetsException $e) {
                if(!$leiebase->live) {
                    $leiebase->logger->warning('NETS – ' . $e->getMessage());
                }
            }
            $efakturaOppkopling->behandleInnkommenPost();

            /**
             * For testing er det mulig å sende et stdClass-objekt til denne metoden,
             * med full lokal filbane som egenskap eller med filinnholdet som egenskapens verdi.
             * Eksempel:
             *  $efakturaOppkopling->behandleInnkommenPost((object)[\Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['file_storage'] . '/nets/inn/efaktura/2019-08/pSVABOL.0922801100002.bbs' => null]);
             */
            $efakturaOppkopling->behandleInnkommenPost((object)[
                /** Oppdragsnr 816001 Efaktura Avtale-forespørsel for leieforhold 1005 (avtalevalg/status CHANGE/NotActive) og 864 (avtalevalg/status ADD/PENDING) */
//            \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['file_storage'] . '/nets/inn/efaktura/2022-11/pSVABOL.0931401100001.bbs' => null,
                /** Mottakskvittering for efakturaforsendelse 0815001 */
//            \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['file_storage'] . '/nets/inn/efaktura/2019-08/pSVABOL.0922701100001.bbs' => null,
                /** Kvittering for ferdig prosessering (62 godkjente og 0 avviste efakturaer) fra efakturaoppdrag 815901 */
//            \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['file_storage'] . '/nets/inn/efaktura/2019-08/pSVABOL.0922701100002.bbs' => null,
                /** Oppdragsnr 815001 Efaktura Avtale-forespørsel for leieforhold 1515 (avtalevalg/status CHANGE/NotActive) og 1005 (avtalevalg/status ADD/PENDING) */
//            \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['file_storage'] . '/nets/inn/efaktura/2019-08/pSVABOL.0922701100003.bbs' => null
            ]);
            $efakturaOppkopling->behandleInnkommenPost((object)[
                /** Mottakskvittering for efakturaforsendelse 0815002 */
//            \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['file_storage'] . '/nets/inn/efaktura/2019-08/pSVABOL.0922701100004.bbs' => null,
                /** Kvittering for prosessert efakturaforsendelse 0815002 (kun på forsendelsesnivå: Ingen efakturaer er godkjent) */
//            \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['file_storage'] . '/nets/inn/efaktura/2019-08/pSVABOL.0922701100005.bbs' => null
            ]);
            $efakturaOppkopling->sendUtgåendePost();
        }
    } catch (Exception $e) {
        $leiebase->logger->critical("NETS – " . $e->getMessage(), $e->getTrace());
    }
    return $argumenter;
});

EventManager::addListener(get_class($leiebase), 'cron', [Leiebase::getClassPreference(NetsProsessor::class), 'cron']);

EventManager::addListener(get_class($leiebase), 'cron', [Leiebase::getClassPreference(\Kyegil\Leiebasen\Søknadprosessor::class), 'cron']);

EventManager::addListener(get_class($leiebase), 'cron', [Leiebase::getClassPreference(\Kyegil\Leiebasen\Leieregulerer::class), 'cron']);

EventManager::addListener(get_class($leiebase), 'cron', function(CoreModelImplementering $leiebase, array $argumenter = []) {
    try {
        $leiebase->hentEpostprosessor()->leverEpost();
    } catch (Exception $e) {
        $leiebase->logger->critical($e->getMessage(), $e->getTrace());
    }
    try {
        $leiebase->hentSmsProsessor()->leverSms();
    } catch (Exception $e) {
        $leiebase->logger->critical($e->getMessage(), $e->getTrace());
    }
    return $argumenter;
});

/**
 * Send påminnelse og varsle om evt depositumsgarantier som er i ferd med å utløpe
 */
EventManager::addListener(get_class($leiebase), 'cron', function(CoreModelImplementering $leiebase, array $argumenter = []) {
    try {
        $cronTid = $argumenter[0] ?? date_create_immutable('now', new DateTimeZone('UTC'));
        // Sjekk i løpet av 10 minutter etter kl 07.40, 12.40 og 20.40
        if(in_array($cronTid->format('H'), [7,12,20]) && intval($cronTid->format('i')/10) == 4) {
            $depositumUtløpsvarselTid = Leiebase::parseDateInterval($leiebase->hentValg('depositum_utløpsvarsel_tid'), false);
            $leiebase->sjekkOgVarsleUtløpendeDepositum($cronTid->add($depositumUtløpsvarselTid));
        }
    } catch (Exception $e) {
        $leiebase->logger->critical($e->getMessage(), $e->getTrace());
    }
    return $argumenter;
});

/**
 * Lås krav som har blitt helt eller delvis betalt
 */
EventManager::addListener(get_class($leiebase), 'cron', function(CoreModelImplementering $leiebase, array $argumenter = []) {
    try {
        $leiebase->låsKravMedBetalinger();
    } catch (Exception $e) {
        $leiebase->logger->critical($e->getMessage(), $e->getTrace());
    }
    return $argumenter;
});


/**
 * Forbered cron
 */
EventManager::getInstance()->triggerEvent(get_class($leiebase), 'preCron', $leiebase, [$cronTid], $leiebase->hentCoreModelImplementering());

/**
 * Utfør Cron-oppgaver
 */
EventManager::getInstance()->triggerEvent(get_class($leiebase), 'cron', $leiebase, [$cronTid], $leiebase->hentCoreModelImplementering());

/**
 * Rydd opp i cron-oppgaver
 */
try {
    $leiebase->settValg('cronsuksess', time());
    $leiebase->logger->debug('Cron fullført', [
        'memory_usage' => number_format(memory_get_usage()),
        'query_count' => CoreModel::$queryCount,
        'triggered_events' => \Kyegil\Leiebasen\EventManager::$triggerCount,
        'execution_time' => microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"] . 's',
    ]);
} catch (Exception $e) {
    $leiebase->logger->critical($e->getMessage(), $e->getTrace());
}

EventManager::getInstance()->triggerEvent(get_class($leiebase), 'postCron', $leiebase, [$cronTid], $leiebase->hentCoreModelImplementering());
