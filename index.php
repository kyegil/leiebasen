<?php


use Kyegil\CoreModel\CoreModel;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Oppslag\AbstraktKontroller;

const LEGAL = true;
set_error_handler(function(int $errno, string $errstr, $errfile, $errline){
    $error_reporting = error_reporting();
    if ($error_reporting & $errno) {
        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
    }
    return false;
});
set_exception_handler(function (Throwable $e):void {
    error_log(substr(addslashes($e), 0, 1024) . "\n", 3, './var/log/Error.log');
    $developerEmail = \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['debug']['developer_email'] ?? null;
    if ($developerEmail) {
        error_log($e, 1, $developerEmail);
    }
});

require_once('./klasser/index.php');
Leiebase::setConfig(include('./config.php'));

$oppslagsbestyrer = new \Kyegil\Leiebasen\Oppslagsbestyrer(__FILE__);
foreach($oppslagsbestyrer->hentInkluderinger() as $fil) {
    include_once($fil);
}
/** @var AbstraktKontroller $leiebase */
$leiebase = $oppslagsbestyrer->hentOppslag();
if(!$leiebase) {
    header("Location: " . (\Kyegil\leiebasen\Leiebase::$config['leiebasen']['config']['default_url'] ?? ''));
    die();
}
$mysqliConnection = $leiebase->mysqli;

if(!$leiebase->adgang($leiebase->område['område'])) {
    $leiebase->responder403();
}

if(!empty($_GET['oppdrag'])) {
    $leiebase->oppdrag($_GET['oppdrag']);
}
else {
    $leiebase->skrivHTML();
}
$leiebase->logger->debug('LEIEBASEN Fullført oppslag', [
    'get' => $_GET,
    'memory_usage' => number_format(memory_get_usage()),
    'query_count' => CoreModel::$queryCount,
    'triggered_events' => \Kyegil\Leiebasen\EventManager::$triggerCount,
    'execution_time' => microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"] . 's',
]);