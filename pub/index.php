<?php

use Kyegil\Leiebasen\Respons;

const LEGAL = true;
set_error_handler(function(int $errno, string $errstr, $errfile, $errline){
    $error_reporting = error_reporting();
    if ($error_reporting & $errno) {
        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
    }
    return false;
});
set_exception_handler(function (Throwable $e):void {
    error_log(substr(addslashes($e), 0, 1024) . "\n", 3, '../var/log/Error.log');
    $developerEmail = \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['debug']['developer_email'] ?? null;
    if ($developerEmail) {
        error_log($e, 1, $developerEmail);
    }
});

require_once('../klasser/index.php');
\Kyegil\Leiebasen\Leiebase::setConfig(include('../config.php'));

$url = $_GET['url'] ?? null;
if ($url) {
    foreach (\Kyegil\Leiebasen\Leiebase::$config['pub_dir_links'] ?? [] as $dir) {
        if(file_exists($dir . $url)) {
            $respons = new \Kyegil\Leiebasen\Respons\InlineFilRespons($dir . $url);
            die($respons);
        }
    }
}
$protocol = $_SERVER["SERVER_PROTOCOL"] ?? 'HTTP/1.1';
$respons = new Respons('');
$respons->header($protocol . ' 404 Not Found');
die($respons);