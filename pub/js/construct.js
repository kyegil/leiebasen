/*	-----------------------------------------------------------------------------------------------
	Namespace
--------------------------------------------------------------------------------------------------- */

var leiebasen = leiebasen || {kjerne: {}},
    $ = jQuery;


/*	-----------------------------------------------------------------------------------------------
	Globale variabler
--------------------------------------------------------------------------------------------------- */

var $doc = $( document ),
    $win = $( window ),
    winHeight = $win.height(),
    winWidth = $win.width();

var viewport = {};
	viewport.top = $win.scrollTop();
	viewport.bottom = viewport.top + $win.height();


/*	-----------------------------------------------------------------------------------------------
	Hjelpefunksjoner
--------------------------------------------------------------------------------------------------- */

/* Toggle an attribute ----------------------- */

function leiebasenVeksleAttributt( $element, attribute, trueVal, falseVal ) {

	if ( typeof trueVal === 'undefined' ) { trueVal = true; }
	if ( typeof falseVal === 'undefined' ) { falseVal = false; }

	if ( $element.attr( attribute ) !== trueVal ) {
		$element.attr( attribute, trueVal );
	} else {
		$element.attr( attribute, falseVal );
	}
}


/*	-----------------------------------------------------------------------------------------------
	Toggles
--------------------------------------------------------------------------------------------------- */

leiebasen.kjerne.toggles = {

	init: function() {

		// Do the toggle
		leiebasen.kjerne.toggles.toggle();

		// Check for toggle/untoggle on resize
		leiebasen.kjerne.toggles.resizeCheck();

		// Check for untoggle on escape key press
		leiebasen.kjerne.toggles.untoggleOnEscapeKeyPress();

	},

	// Do the toggle
	toggle: function() {

		$( '*[data-toggle-target]' ).on( 'click', function() {

			// Get our targets
			var $toggle = $( this ),
				targetString = $( this ).data( 'toggle-target' );

			if ( targetString === 'next' ) {
				var $target = $toggle.next();
			} else {
				var $target = $( targetString );
			}

			// Trigger events on the toggle targets before they are toggled
			if ( $target.is( '.active' ) ) {
				$target.trigger( 'toggle-target-before-active' );
			} else {
				$target.trigger( 'toggle-target-before-inactive' );
			}

			// Get the class to toggle, if specified
			var classToToggle = $toggle.data( 'class-to-toggle' ) ? $toggle.data( 'class-to-toggle' ) : 'active';

			// For cover modals, set a short timeout duration so the class animations have time to play out
			var timeOutTime = 0;

			if ( $target.hasClass( 'cover-modal' ) ) {
				var timeOutTime = 10;
			}

			setTimeout( function() {

				// Toggle the target of the clicked toggle
				if ( $toggle.data( 'toggle-type' ) == 'slidetoggle' ) {
					var duration = $toggle.data( 'toggle-duration' ) ? $toggle.data( 'toggle-duration' ) : 250;
					$target.slideToggle( duration );
				} else {
					$target.toggleClass( classToToggle );
				}

				// If the toggle target is 'next', only give the clicked toggle the active class
				if ( targetString === 'next' ) {
					$toggle.toggleClass( 'active' )

				// If not, toggle all toggles with this toggle target
				} else {
					$( '*[data-toggle-target="' + targetString + '"]' ).toggleClass( 'active' );
				}

				// Toggle aria-expanded on the target
				leiebasenVeksleAttributt( $target, 'aria-expanded', 'true', 'false' );

				// Toggle aria-pressed on the toggle
				leiebasenVeksleAttributt( $toggle, 'aria-pressed', 'true', 'false' );

				// Toggle body class
				if ( $toggle.data( 'toggle-body-class' ) ) {
					$( 'body' ).toggleClass( $toggle.data( 'toggle-body-class' ) );
				}

				// Check whether to lock the screen
				if ( $toggle.data( 'lock-screen' ) ) {
					leiebasen.kjerne.scrollLock.setTo( true );
				} else if ( $toggle.data( 'unlock-screen' ) ) {
					leiebasen.kjerne.scrollLock.setTo( false );
				} else if ( $toggle.data( 'toggle-screen-lock' ) ) {
					leiebasen.kjerne.scrollLock.setTo();
				}

				// Check whether to set focus
				if ( $toggle.data( 'set-focus' ) ) {
					var $focusElement = $( $toggle.data( 'set-focus' ) );
					if ( $focusElement.length ) {
						if ( $toggle.is( '.active' ) ) {
							$focusElement.focus();
						} else {
							$focusElement.blur();
						}
					}
				}

				// Trigger the toggled event on the toggle target
				$target.triggerHandler( 'toggled' );

				// Trigger events on the toggle targets after they are toggled
				if ( $target.is( '.active' ) ) {
					$target.trigger( 'toggle-target-after-active' );
				} else {
					$target.trigger( 'toggle-target-after-inactive' );
				}

			}, timeOutTime );

			return false;

		} );
	},

	// Check for toggle/untoggle on screen resize
	resizeCheck: function() {

		if ( $( '*[data-untoggle-above], *[data-untoggle-below], *[data-toggle-above], *[data-toggle-below]' ).length ) {

			$win.on( 'resize', function() {

				var winWidth = $win.width(),
					$toggles = $( '.toggle' );

				$toggles.each( function() {

					$toggle = $( this );

					var unToggleAbove = $toggle.data( 'untoggle-above' ),
						unToggleBelow = $toggle.data( 'untoggle-below' ),
						toggleAbove = $toggle.data( 'toggle-above' ),
						toggleBelow = $toggle.data( 'toggle-below' );

					// If no width comparison is set, continue
					if ( ! unToggleAbove && ! unToggleBelow && ! toggleAbove && ! toggleBelow ) {
						return;
					}

					// If the toggle width comparison is true, toggle the toggle
					if ( 
						( ( ( unToggleAbove && winWidth > unToggleAbove ) ||
						( unToggleBelow && winWidth < unToggleBelow ) ) &&
						$toggle.hasClass( 'active' ) )
						||
						( ( ( toggleAbove && winWidth > toggleAbove ) ||
						( toggleBelow && winWidth < toggleBelow ) ) &&
						! $toggle.hasClass( 'active' ) )
					) {
						$toggle.trigger( 'click' );
					}

				} );

			} );

		}

	},

	// Close toggle on escape key press
	untoggleOnEscapeKeyPress: function() {

		$doc.keyup( function( e ) {
			if ( e.key === "Escape" ) {

				$( '*[data-untoggle-on-escape].active' ).each( function() {
					if ( $( this ).hasClass( 'active' ) ) {
						$( this ).trigger( 'click' );
					}
				} );
					
			}
		} );

	},

} // leiebasen.kjerne.toggles


/*	-----------------------------------------------------------------------------------------------
	Cover Modals
--------------------------------------------------------------------------------------------------- */

leiebasen.kjerne.coverModals = {

	init: function () {

		if ( $( '.cover-modal' ).length ) {

			// Handle cover modals when they're toggled
			leiebasen.kjerne.coverModals.onToggle();

			// When toggled, untoggle if visitor clicks on the wrapping element of the modal
			leiebasen.kjerne.coverModals.outsideUntoggle();

			// Close on escape key press
			leiebasen.kjerne.coverModals.closeOnEscape();

			// Show a cover modal on load, if the query string says so
			leiebasen.kjerne.coverModals.showOnLoadandClick();

			// Hide and show modals before and after their animations have played out
			leiebasen.kjerne.coverModals.hideAndShowModals();

		}

	},

	// Handle cover modals when they're toggled
	onToggle: function() {

		$( '.cover-modal' ).on( 'toggled', function() {

			var $modal = $( this ),
				$body = $( 'body' );

			if ( $modal.hasClass( 'active' ) ) {
				$body.addClass( 'showing-modal' );
			} else {
				$body.removeClass( 'showing-modal' ).addClass( 'hiding-modal' );

				// Remove the hiding class after a delay, when animations have been run
				setTimeout ( function() {
					$body.removeClass( 'hiding-modal' );
				}, 500 );
			}
		} );

	},

	// Close modal on outside click
	outsideUntoggle: function() {

		$doc.on( 'click', function( e ) {

			var $target = $( e.target ),
				modal = '.cover-modal.active';

			if ( $target.is( modal ) ) {

				leiebasen.kjerne.coverModals.untoggleModal( $target );

			}

		} );

	},

	// Close modal on escape key press
	closeOnEscape: function() {

		$doc.keyup( function( e ) {
			if ( e.key === "Escape" ) {
				$( '.cover-modal.active' ).each( function() {
					leiebasen.kjerne.coverModals.untoggleModal( $( this ) );
				} );
			}
		} );

	},

	// Show modals on load
	showOnLoadandClick: function() {

		var key = 'modal';

		// Load based on query string
		if ( window.location.search.indexOf( key ) !== -1 ) {
				
			var modalTargetString = getQueryStringValue( key ),
				$modalTarget = $( '#' + modalTargetString + '-modal' );

			if ( modalTargetString && $modalTarget.length ) {
				setTimeout( function() {
					$modalTarget.addClass( 'active' ).triggerHandler( 'toggled' );
					leiebasen.kjerne.scrollLock.setTo( true );
				}, 250 );
			}
		}

		// Check for modal matching querystring when clicking a link
		// Format: www.url.com?modal=modal-id
		$( 'a' ).on( 'click', function() {

			// Load based on query string
			if ( $( this ).attr( 'href' ) && $( this ).attr( 'href' ).indexOf( key ) !== -1 ) {
					
				var modalTargetString = getQueryStringValue( key, $( this ).attr( 'href' ) ),
					$modalTarget = $( '#' + modalTargetString );

				if ( modalTargetString && $modalTarget.length ) {
					
					$modalTarget.addClass( 'active' ).triggerHandler( 'toggled' );
					leiebasen.kjerne.scrollLock.setTo( true );

					return false;

				}
			}

		} );

	},

	// Hide and show modals before and after their animations have played out
	hideAndShowModals: function() {

		var $modals = $( '.cover-modal' );

		// Show the modal
		$modals.on( 'toggle-target-before-inactive', function( e ) {
			if ( e.target != this ) return;
			
			$( this ).addClass( 'show-modal' );
		} );

		// Hide the modal after a delay, so animations have time to play out
		$modals.on( 'toggle-target-after-inactive', function( e ) {
			if ( e.target != this ) return;

			var $modal = $( this );
			setTimeout( function() {
				$modal.removeClass( 'show-modal' );
			}, 500 );
		} );

	},

	// Untoggle a modal
	untoggleModal: function( $modal ) {

		$modalToggle = false;

		// If the modal has specified the string (ID or class) used by toggles to target it, untoggle the toggles with that target string
		// The modal-target-string must match the string toggles use to target the modal
		if ( $modal.data( 'modal-target-string' ) ) {
			var modalTargetClass = $modal.data( 'modal-target-string' ),
				$modalToggle = $( '*[data-toggle-target="' + modalTargetClass + '"]' ).first();
		}

		// If a modal toggle exists, trigger it so all of the toggle options are included
		if ( $modalToggle && $modalToggle.length ) {
			$modalToggle.trigger( 'click' );

		// If one doesn't exist, just hide the modal
		} else {
			$modal.removeClass( 'active' );
		}

	}

} // leiebasen.kjerne.coverModals


/*	-----------------------------------------------------------------------------------------------
	Scroll Lock
--------------------------------------------------------------------------------------------------- */

leiebasen.kjerne.scrollLock = {

	init: function() {

		// Init variables
		window.scrollLocked = false,
		window.prevScroll = {
			scrollLeft : $win.scrollLeft(),
			scrollTop  : $win.scrollTop()
		},
		window.prevLockStyles = {};
		window.lockStyles = {
			'overflow-y' : 'scroll',
			'position'   : 'fixed',
			'width'      : '100%'
		};

		// Instantiate cache in case someone tries to unlock before locking
		leiebasen.kjerne.scrollLock.saveStyles();

	},

	// Save context's inline styles in cache
	saveStyles: function() {

		var styleAttr = $( 'html' ).attr( 'style' ),
			styleStrs = [],
			styleHash = {};

		if ( ! styleAttr ) {
			return;
		}

		styleStrs = styleAttr.split( /;\s/ );

		$.each( styleStrs, function serializeStyleProp( styleString ) {
			if ( ! styleString ) {
				return;
			}

			var keyValue = styleString.split( /\s:\s/ );

			if ( keyValue.length < 2 ) {
				return;
			}

			styleHash[ keyValue[ 0 ] ] = keyValue[ 1 ];
		} );

		$.extend( prevLockStyles, styleHash );
	},

	// Lock the scroll (do not call this directly)
	lock: function() {

		var appliedLock = {};

		if ( scrollLocked ) {
			return;
		}

		// Save scroll state and styles
		prevScroll = {
			scrollLeft : $win.scrollLeft(),
			scrollTop  : $win.scrollTop()
		};

		leiebasen.kjerne.scrollLock.saveStyles();

		// Compose our applied CSS, with scroll state as styles
		$.extend( appliedLock, lockStyles, {
			'left' : - prevScroll.scrollLeft + 'px',
			'top'  : - prevScroll.scrollTop + 'px'
		} );

		// Then lock styles and state
		$( 'html' ).css( appliedLock );
		$( 'html' ).addClass( 'scroll-locked' );
		$( 'html' ).attr( 'scroll-lock-top', prevScroll.scrollTop );
		$win.scrollLeft( 0 ).scrollTop( 0 );

		window.scrollLocked = true;
	},

	// Unlock the scroll (do not call this directly)
	unlock: function() {

		if ( ! window.scrollLocked ) {
			return;
		}

		// Revert styles and state
		$( 'html' ).attr( 'style', $( '<x>' ).css( prevLockStyles ).attr( 'style' ) || '' );
		$( 'html' ).removeClass( 'scroll-locked' );
		$( 'html' ).attr( 'scroll-lock-top', '' );
		$win.scrollLeft( prevScroll.scrollLeft ).scrollTop( prevScroll.scrollTop );

		window.scrollLocked = false;
	},

	// Call this to lock or unlock the scroll
	setTo: function( on ) {

		// If an argument is passed, lock or unlock accordingly
		if ( arguments.length ) {
			if ( on ) {
				leiebasen.kjerne.scrollLock.lock();
			} else {
				leiebasen.kjerne.scrollLock.unlock();
			}
			// If not, toggle to the inverse state
		} else {
			if ( window.scrollLocked ) {
				leiebasen.kjerne.scrollLock.unlock();
			} else {
				leiebasen.kjerne.scrollLock.lock();
			}
		}

	},

} // leiebasen.kjerne.scrollLock


/*	-----------------------------------------------------------------------------------------------
	Custom functions
--------------------------------------------------------------------------------------------------- */

/**
 * @param {Number} beløp
 * @param {boolean} html
 * @param {boolean} prefiks
 * @returns {string}
 */
leiebasen.kjerne.kr = function(beløp, html = true, prefiks = true) {
	let formatter = new Intl.NumberFormat('nb-NO', { style: 'currency', currency: 'NOK' });
	let resultat = formatter.format(beløp).replace(',00', ',–');
	resultat = resultat.replace('kr', '').trim();
	if(prefiks) {
		resultat = "kr " + resultat;
	}
	return html
		? '<span>' + resultat.replace(" ", '&nbsp;') + '</span>'
		: resultat;
}

/**
 * @param {Number} verdi
 * @param {Number} antallDesimaler
 * @param {boolean} html
 * @returns {string}
 */
leiebasen.kjerne.prosent = function(verdi, antallDesimaler = 1, html = true) {
	let formatter = new Intl.NumberFormat('nb-NO', { style: 'percent', maximumFractionDigits: antallDesimaler });
	let resultat = formatter.format(verdi);
	if (html) {
		return '<span>' + resultat.replace(" ", '&nbsp;') + '</span>';
	}
	return resultat;
}

/**
 * @param {Date|string} dato
 * @returns {string}
 */
leiebasen.kjerne.dato = function(dato) {
	if(dato instanceof Date) {
		dato = dato.toISOString();
	}
	return typeof dato === 'string'
		? dato.slice(8,10) + '.'
		+ dato.slice(5,7) + '.'
		+ dato.slice(0,4)
		+ dato.slice(10)
		: dato
		;
}

/**
 * @param {boolean|null} verdi
 * @returns {string}
 */
leiebasen.kjerne.boolVisning = function(verdi) {
	return verdi ? '✔︎' : '';
}

/**
 *
 * @param {string} url
 * @param {string} tekst
 * @param {string} title
 * @param {string} classes
 * @returns {string}
 */
leiebasen.kjerne.lenke = function(url, tekst, title, classes) {
	title = title ? title : tekst;
	return '<a href="'+ url + '"'
		+ (title ? (' title="' + title + '"') : '')
		+ (classes ? (' class="' + classes + '"') : '')
		+ '>'
		+ tekst + '</a>';
}

leiebasen.kjerne.multiFileFields = {
	init: function() {
		$(".file_uploads .file-upload-add").on('click', function(e) {
			const leggTilKnapp = $(e.target);
			const name = e.target.getAttribute('data-name');
			const feltsett = leggTilKnapp.parent();
			const nyeAntallFilerFelt = feltsett.find("input[name='nye_" + name + "_antall']");

			let nyFilFelt = $('<input type="file" name="' + name + '[]"/>');
			let nyFjernKnapp = $('<button>Fjern</button>');
			nyFjernKnapp.on('click', function(e) {
				const nyFeltBeholder = $(e.target).parent();
				nyeAntallFilerFelt.val(parseInt(nyeAntallFilerFelt.val(), 10) - 1);
				nyFeltBeholder.remove();
			});
			const nyFeltBeholder = $('<div>');
			nyFeltBeholder.append(nyFilFelt).append(nyFjernKnapp);
			nyFeltBeholder.insertBefore(leggTilKnapp);
			nyeAntallFilerFelt.val(parseInt(nyeAntallFilerFelt.val(), 10) + 1);
		});
	}
}


/*	-----------------------------------------------------------------------------------------------
	Function Calls
--------------------------------------------------------------------------------------------------- */

$doc.ready( function() {

	leiebasen.kjerne.toggles.init();						// Handle toggles
	leiebasen.kjerne.coverModals.init();					// Handle cover modals
	leiebasen.kjerne.scrollLock.init();						// Scroll Lock
	leiebasen.kjerne.multiFileFields.init()					// Multi file fields
} );