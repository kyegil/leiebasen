jQuery(document).ready(function() {
	
	/*
	    Sidebar
	*/
	$('.dismiss, .overlay').on('click', function() {
		$('.sidebar').removeClass('active');
		$('nav.brukermeny').removeClass('active');
        $('.overlay').removeClass('active');
    });

	$('.hovedmeny-toggle').on('click', function(e) {
		e.preventDefault();
		$('.sidebar').addClass('active');
		$('.overlay').addClass('active');
		// close opened sub-menus
		$('.collapse.show').toggleClass('show');
		$('a[aria-expanded=true]').attr('aria-expanded', 'false');
	});

	$('header .bruker').on('click', function(e) {
		e.preventDefault();
		let brukermeny = $('nav.brukermeny');
		let overlay = $('.overlay');
		if(brukermeny.hasClass('active')) {
			brukermeny.removeClass('active');
			overlay.removeClass('active');
		}
		else {
			brukermeny.addClass('active');
			overlay.addClass('active');
		}
	});

	$('header .varsler.utløst .varselmenyknapp').on('click', function(e) {
		e.preventDefault();
		let varselmeny = $('nav.varselmeny');
		let overlay = $('.overlay');
		if(varselmeny.hasClass('active')) {
			varselmeny.removeClass('active');
			overlay.removeClass('active');
		}
		else {
			varselmeny.addClass('active');
			overlay.addClass('active');
		}
	});

	/* change sidebar style */
	$('a.btn-customized-dark').on('click', function(e) {
		e.preventDefault();
		$('.sidebar').removeClass('light');
		$('nav.brukermeny').removeClass('light');
	});
	$('a.btn-customized-light').on('click', function(e) {
		e.preventDefault();
		$('.sidebar').addClass('light');
		$('nav.brukermeny').addClass('light');
	});
	/* replace the default browser scrollbar in the sidebar, in case the sidebar menu has a height that is bigger than the viewport */
	$('.sidebar, nav.brukermeny').mCustomScrollbar({
		theme: "minimal-dark"
	});
	
	$('.to-top a').on('click', function(e) {
		e.preventDefault();
		// noinspection EqualityComparisonWithCoercionJS
		if($(window).scrollTop() != 0) {
			$('html, body').stop().animate({scrollTop: 0}, 1000);
		}
	});
});
