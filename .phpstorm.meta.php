<?php
namespace PHPSTORM_META {

    use Kyegil\Leiebasen\ModellFabrikk;

    override(\Kyegil\Leiebasen\Leiebase::getClassPreference(), map([
    ]));
    override(\Kyegil\Leiebasen\Leiebase::hentModell(), map([
        '' => '@'
    ]));
    override(\Kyegil\Leiebasen\Leiebase::nyModell(), map([
        '' => '@'
    ]));
    override(\Kyegil\Leiebasen\Leiebase::hentSamling(), map([
        '' => '@sett'
    ]));
    override(ModellFabrikk::lag(), map([
        '' => '@'
    ]));
    override(\Kyegil\Leiebasen\Leiebase::hentVisning(), map([
        '' => '@'
    ]));
    override(\Kyegil\Leiebasen\Leiebase::vis(), map([
        '' => '@'
    ]));
}