# Leiebasen

_Leiebasen_ er et online administrasjonsprogram for forvaltning og utleie av boliger.
Programmet er først og fremst rettet mot kooperativer og mindre boligorganisasjoner, og søker å fremme beboermedvirkning og gjennomsiktighet i forvaltningen.

- Administrering av leieavtaler og leieobjekter, husleie og andre utgifter knyttet til leieforholdene, betalinger osv.
- Skaderegister holder styr på skader og utbedringer, og formidler oppdateringer mellom utleier og leietakere.
- 'Mine Sider' der leietaker selv kan holde oversikt over regninger og utestående, rapportere skader etc.
- 3.parts-tjenester som felllesstrøm etc. kan fordeles mellom beboerne etter angitte fordelingsnøkler.
- Mulighet for å publisere søknadsskjema, og håndtere innkomne søknader.
- Rapportering basert på leieobjekter, bygninger, eller geografiske eller administrative områder.
- Fleksibilitet i forhold til boligtyper (leiligheter, bofellesskap), ulike leieberegninger, kontraktmaler etc.

- Tilbakemeldinger og innspill fra små og store beboerorienterte boligorgansasjoner er velkomne.

## Bruk

Leiebasen er gratis å laste ned og bruke for eget formål. Den kan også brukes som utganspunkt for skreddersydde programmer for eget bruk.
Det følger ingen garantier med programmet; all bruk og videre utvikling foregår på eget ansvar. Jeg holder imidlertid kontinuerlig oppsyn med rapporterte bugs, og søker å utbedre slike så fort som mulig.

Se vedlagt GPL-lisens for detaljer.

## Installering

Leiebasen kan installeres på egen webserver.

Krav til servermiljø:

 * PHP versjon 7.4 eller nyere
 * Følgende PHP extensions:
     * ext-bcmath
     * ext-calendar
     * ext-curl
     * ext-fileinfo
     * ext-iconv
     * ext-intl
     * ext-json
     * ext-mailparse
     * ext-mbstring
     * ext-mysqli
     * ext-simplexml
     * ext-zip
 * MySQL eller MariaDB database. Du må ha brukernavn og passord for å kople til databasen.
 * git
 * composer
  
1. Last ned kildekoden fra [https://gitlab.com/kyegil/leiebasen](https://gitlab.com/kyegil/leiebasen) og pakk den ut på serveren der hvor du vil ha programmet installert.
2. Sett opp `config.php` i henhold til servermiljøet:
    1. Lokalisér `config.sample.php` der hvor du installerte kildekoden. Kopiér denne fila som `config.php`.
    2. Åpne `config.php` med en ren-tekstbehandler (f.eks TextEdit for Mac eller Notepad for Windows).
    3. Endre verdiene i de følgende feltene i `config.php` (behold anførselstegnene):
        * Under 'leiebasen' => 'db', her vil du finne database-instillingene 'host', 'name', 'user' og 'password':
            * For `name` oppgir du det faktiske navnet på databasen din istedetfor 'leiebasen'.
            * For `user` oppgir du brukernavn for databasen (istedet for 'root'). Denne brukeren må ha alle tillatelser på databasen.
            * For `password` oppgir du passordet for brukeren over (istedetfor 'root').
            * `host`: Dersom databasen befinner seg på en annen server enn den du har installert filene på, oppgir du navnet her. Ellers lar du 'localhost' være.
            * `prefix`: Dersom databasen deles med andre programmer, bør du oppgi en prefiks her for å skille tabellene fra annet innhold. Prefiksen bør være kort (2-4 tegn og etterfølges av understrek. F.eks `'lbs_'` som forkortelse for 'leiebasen'.
        * Under 'leiebasen' => 'server' bør du oppgi server- og domenenavn etc:
            * `installation_url`: Oppgi foretrukket url for installasjonen, (f.eks. https://leiebasen.min-org.no).
            * `root`: Oppgi komplett filbane på serveren, for der hvor du pakket ut filene (f.eks. '~/public_html/)
            * `file_storage`: Oppgi komplett filbane for et område utenfor offentlig tilgjengelighet, der du trygt kan lagre sensitive dokumenter (f.eks. '~/leiebasen_filer'
        * Dette er de viktigste innstillingene. [Jamfør dokumentasjonen for andre innstillinger](## config.php Konfigurasjonsreferanse).
        * Lagre config.php, og sørg for at den har filtillatelser satt til 0640.
    4. Oppdater delmoduler og dependencies. Dette trinnet krever at du har git og composer installert på serveren. Logg inn i serveren med ssh-terminal, og naviger fram til der du har installert leiebasen.
        1. Kjør kommandoen `git submodule update --init`
        2. Kjør kommandoen `composer install`
    5. Opprett database-innholdet. Bruk en nettleser, og besøk installasjonsadressen (den du har oppgitt som 'installation_url' i config.php over. Dersom du har gjort alle installerings-trinnene over, vil databasen nå installeres. Etter komplett installering vil du videresendes til administrator-innlogging.

## config.php Konfigurasjonsreferanse

Full referanse for oppsett av config.php:

* `additional_configs`: string[] Filbaner til evt. andre config-filer som skal leses og flettes sammmen med denne. Disse kan f.eks. brukes for å definere og aktivere plugins.
* `listeners`: (array) Evt. Listeners som plugges inn i leiebasen for tilpasset funksjonalitet.
* `routes`: assosiative arrays for der nøkkelen angir et lovlig område i leiebasen, og det for hvert område kan angis mapping til egne kontroller:

        'drift' => ['mitt_oppslag' => "Mitt\\Navneområde\\Controllers\\MittOppslag"]
        
* `class_preferences`: (object) Evt. overstyring av klasser i leiebasen. F.eks. kan en bestemt visning overstyres på denne måten:

        "Kyegil\\Leiebasen\\Visning\\sentral\\html\\shared\\Html": "MinSpesialtilpasning\\Visning\\sentral\\html\\shared\\Html"

    Den overstyrende klassen må være en utvidelse av den originale.

* `psr4_roots`: (objekt) Kartlegging av PSR4-rotmapper for Autoloading av egne klasser. F.eks for ovennevnte overstyring:

        "MinSpesialtilpasning\\Visning": "~/min_plugin/code/Visning"

* `pub_dir_links`: (objekt) Kartlegging av mapper som skal være tilgjengelige undermapper av `/pub/`. F.eks:

      'my_scripts' => ['my_scripts' => dirname(__FILE__) . '/pub/scripts'],

* `leiebasen` (innstillinger som tilhører leiebasen):
    * `db`:
        * `host`: Hostnavn for databasen
        * `name`: Databasens navn
        * `user`: Database-bruker
        * `password`: Passord for db-bruker
        * `port`: Port for database, dersom påkrevd
        * `driver`: Database-driver. Må være 'mysqli'
        * `prefix`: Evt prefiks for database-tabeller
    * `server`:
        * `installation_url`: URL for installasjonen
        * `root`: Filbane for installasjonen på server
        * `file_storage`: Filbane for data-/dokument-lager
        * `production`: TRUE for å sette leiebasen i live produksjonsmodus
        * `db_backup`: Filbane til en database-backup i SQL-format. Backup'en må lages regelmessig med f.eks mysqldump via cron, og lagres på angitt lokalisering.
        * `template_folders`: (array) Filbaner for visningsfil-mapper. Filbanene søkes gjennom i oppgitt rekkefølge (høyest prioritet først), og kan brukes for å tilpasse utseendet ved å overstyre standard visningsmaler.
    * `session`:
        * `name`: Navnet på informasjonskapselen som holder innloggingsøkten
        * `timeout`: Tiden i sekunder en inaktiv bruker holdes innlogget.
    * `config`:
        * `default_url`: Standardadressen du sendes til etter vellykket innlogging, dersom ingen bestemt adresse er oppgitt.
        * `post_logout_url`: Adressen du sendes til etter å ha logget ut av leiebasen.
        * `extjs_version`: Installert versjon av [ExtJs](https://www.sencha.com/products/extjs/communityedition/) (mappenavnet må tilsvare versjon-nummeret, og plasseres i `pub/lib/`.
    * `debug`:
        * `developer_email`: Epost til utvikler, for å motta test-eposter, og rapportering om problemer.
        * `debug_mail_sender`: Avsenderadresse for feilrapportering.
        * `email_dev_copy`: (array) Liste av eposter hvor utvikler skal blindkopieres inn
    * `google`:
        * `recaptcha3`:
            * `site_key`: Recaptcha3 Site Key dersom recaptcha brukes.
            * `secret_key`: Recaptcha3 Secret Key dersom recaptcha brukes.
    * `logger`:
        * `level`: PSR Log level. Normalt 200 for 'Informational' logging. Endre til 100 for debugging.
    * `nets` (innstillinger for NETS/Mastercard OCR, AvtaleGiro og eFaktura):
        * `host`: IP-adresse eller hostname for Mastercards server.
        * `port`: Port-nummer for Mastercards server.
        * `rsa_key`: Filbane for RSA-nøkkel for SFTP-oppkopling mot Mastercard
        * `user`:
            * `ocr`: Brukernavn for innlogging for OCR/AvtaleGiro.
            * `einvoice`: Brukernavn for innlogging for eFaktura.
        * `efaktura_api`:
            * `test`: Innstillinger for Mastercards testmiljø.
                * `endpoint_url`: Endepunkt for Mastercards API.
                * `brukernavn`: API brukernavn.
                * `passord`: API passord.
                * `certificate`: RSA serifikat tildelt av Mastercard.
                    * `location`: Filbane for sertifikatet.
                    * `private_key`: Filbane for private key for å åpne sertifikatet.
                    * `passphrase`: Passphrase for private key.
            * `produksjon`: Innstillinger for Mastercards produksjonsmiljø.
                * `endpoint_url`: Endepunkt for Mastercards API.
                * `brukernavn`: API brukernavn.
                * `passord`: API passord.
                * `certificate`: RSA serifikat tildelt av Mastercard.
                    * `location`: Filbane for sertifikatet.
                    * `private_key`: Filbane for private key for å åpne sertifikatet.
                    * `passphrase`: Passphrase for private key.

