<?php

//	Basisdefinisjon av alle objekter som er lagret i databasen
use Kyegil\Leiebasen\Leiebase;
use Kyegil\MysqliConnection\MysqliConnection;

/**
 * Class DatabaseObjekt
 *
 * @deprecated
 * @see \Kyegil\Leiebasen\Modell
 */
abstract class DatabaseObjekt {

    /**
     * @var
     */
    protected	$tabell;	// Hvilken tabell i databasen som inneholder primærnøkkelen for dette objektet

    /**
     * @var
     */
    protected	$idFelt;	// Hvilket felt i tabellen som lagrer primærnøkkelen for dette objektet

    /**
     * @var Leiebase
     */
    protected	$leiebase;	// Leiebaseobjektet

    /**
     * @var MysqliConnection
     */
    protected	$mysqli;	// Databasetilkoplingen

    /**
     * @var stdClass
     */
    protected	$data;		// DB-verdiene lagret som et objekt

    /**
     * @var array
     */
    protected	$gjengivelsesdata = [];	// assoc array med nøkler/verdier som brukes som variabler i visningsfilene

    /**
     * @var
     */
    protected	$gjengivelsesfil;	// streng med navnet på gjengivelsesfila

    /**
     * @var mixed
     */
    public		$id;		// Unikt id-heltall for dette objektet

    /**
     * @var array
     */
    public		$knagger = array();	// Ulike handlinger knyttet til framdriftsstadier

    /**
     * @return mixed
     */
    abstract protected function last();


    /**
     * Felles constructor for alle databaseobjekter
     *
     * DatabaseObjekt constructor.
     * @param null|stdClass| $param
     *      ->id integer objektidentifikator for databasen
     * @throws Exception
     */
    public function __construct($param = null ) {

        global $mysqliConnection;
        if( !is_a($mysqliConnection, 'Mysqli') ) {
            throw new Exception('Ingen tilgang på mysqli-tilkopling. \$mysqliConnection = ' . var_export($mysqliConnection, true));
        }
        $this->mysqli = $mysqliConnection;

        global $leiebase;
        if( !is_a($leiebase, Leiebase::class) ) {
            throw new Exception('Ingen tilgang på Leiebase-objektet. \$leiebase = ' . var_export($leiebase, true));
        }
        $this->leiebase = $leiebase;

        global $tillegg;
        $this->lastTillegg( $tillegg );

        if($param !== null) {
            if( is_object( $param ) ) {
                settype($param, 'array');
            }
            if ( !is_array($param) ) {
                $param = array('id'	=> $param);
            }

            $this->id = $param['id'];
        }

    }


    /**
     * Når en ikke eksisterende egenskap forsøkes hentet, forsøkes den først å hentes med hent().
     *
     * @param string $attributt
     * @return mixed
     * @throws Exception
     */
    public function __get($attributt) {
        return $this->hent( $this->fraCamelCase($attributt) );
    }


    /**
     * Når objektet behandles som streng vises id-verdien
     *
     * @return string id-heltallet for objektet
     */
    public function __toString() {
        return (string)$this->id;
    }


    /**
     * Gjengi
     *
     * Kravet gjengis for skjerm- utskrift- eller filgjengivelse.
     *
     * @param string $mal Gjengivelsesmalen som skal brukes. Må være ei fil som befinner seg i '_gjengivelser'
     * @param array|stdClass $param Eksterne parametere som skal brukes i gjengivelsen
     * @return string Gjengivelse av kravet
     * @deprecated
     * @see Leiebase::vis
     */
    public function gjengi($mal, $param = []) {
        settype( $param, 'array');
        $this->gjengivelsesdata = $param;

        switch($mal) {

            default:
            {
                break;
            }
        }

        $this->gjengivelsesfil = "{$mal}.php";
        return $this->_gjengi( (array)$param );
    }


    /**
     * Laster gjengivelsesfila og fyller variablene i denne med verdier
     *
     * @return string
     */
    protected function _gjengi() {
        extract($this->gjengivelsesdata, EXTR_SKIP);
        ob_start();
        include(__DIR__ . "/../_gjengivelser/{$this->gjengivelsesfil}");
        return ob_get_clean();
    }


    /**
     * Hent
     * @param string  $egenskap Egenskapen som skal hentes
     * @return mixed
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell::hent()
     */
    abstract public function hent($egenskap);


    /**
     * Opprett
     *
     * @param array $egenskaper Verdier som angis for det nye objektet
     * @return mixed
     * @deprecated
     * @see Modell::opprett()
     */
    abstract public function opprett($egenskaper);


    /**
     * Hent Id
     *
     * Forsøker å laste objektet, og returnerer primærnøkkel-verdien
     *
     * @return string|int
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell::hentId()
     */
    public function hentId() {
        return $this->hent($this->idFelt);
    }


    /**
     * Fra brøk
     *
     * Gjør om en brøkverdi til desimal
     *
     * @param string $brøk
     * @return float verdien som desimal
     * @deprecated
     * @see \Kyegil\Fraction\Fraction::asDecimal()
     */
    public function fraBrøk($brøk ) {
        $brøk = str_replace( array(",", "%"), array(".", "/100"), $brøk);

        $brøk = str_replace("%", "/100", $brøk);

        $brøk = explode("/", $brøk);

        if(!isset($brøk[1]) || $brøk[1] == 0) {
            $brøk[1] = 1;
        }

        return bcdiv($brøk[0], $brøk[1], 12);
    }


    /**
     * Last tillegg
     *
     * Laster knagger og metoder fra tilleggsmoduler
     *
     * @param array $tilleggsliste Listen over tillegg og konfigurasjoner
     * @throws Exception
     * @deprecated
     */
    public function lastTillegg($tilleggsliste ) {
        settype($tilleggsliste, 'array');

        foreach( $tilleggsliste as $tillegg ) {
            $klasse = get_class($this);

            if( isset( $tillegg->modeller->{$klasse} ) ) {
                foreach(  $tillegg->modeller->{$klasse}->knagger as $stadium => $handlinger ) {
                    $this->ved( $stadium, $handlinger );
                }
            }
        }
    }



    /**
     * Ved
     *
     * Knagger en handling til et stadium
     *
     * @param string $stadium
     * @param callable|array $handlinger
     * @throws Exception
     * @deprecated
     */
    public function ved($stadium, $handlinger ) {
        settype($handlinger, 'array');

        foreach( $handlinger as $handling ) {
            if( is_callable( $handling ) ) {
                $this->knagger[$stadium][] = $handling;
            }
            else {
                throw new Exception( "Kan ikke utføre " . print_r($handling, true) . "");
            }
        }

    }


    /**
     * @param string $camelCase
     * @return string mixed
     * @deprecated
     */
    public function fraCamelCase($camelCase)
    {
        if(!isset($this->leiebase->mellomlager->underscore[$camelCase])) {
            $understreket = mb_strtolower(mb_ereg_replace('(?<=\\w)([A-ZÆØÅ])', '_\\1', $camelCase));
            $this->leiebase->mellomlager->underscore[$camelCase] = $understreket;
            $this->leiebase->mellomlager->camelCase[$understreket] = $camelCase;
        }
        return $this->leiebase->mellomlager->underscore[$camelCase];
    }
}