<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:53
 */

namespace Kyegil\Leiebasen;


use Closure;
use Kyegil\ViewRenderer\View;

class Visning extends View
{
    /** @var CoreModelImplementering */
    protected $app;

    /**
     * @param $attributt
     * @param null $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        return parent::setData($attributt, $verdi);
    }

    /**
     * @param array|string $keyOrDataSet
     * @param null $value
     * @return $this
     */
    public function setData($keyOrDataSet, $value = null)
    {
        return $this->sett($keyOrDataSet, $value);
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function addData($key, $value)
    {
        return $this->leggTil($key, $value);
    }

    /**
     * @param $attributt
     * @param $verdi
     * @return $this
     */
    public function leggTil($attributt, $verdi)
    {
        return parent::addData($attributt, $verdi);
    }

    /**
     * @param string|null $attributt
     * @return array|mixed
     */
    public function hent(string $attributt = null)
    {
        return $this->getData($attributt);
    }

    /**
     * @param string $mal
     * @return $this
     */
    public function settMal(string $mal): View
    {
        return $this->setTemplate($mal);
    }

    /**
     * @return string
     */
    public function hentMal(): string
    {
        return $this->getTemplate();
    }

    /**
     * @param $ressurs
     * @param $verdi
     * @return $this
     */
    public function settRessurs($ressurs, $verdi) {
        return $this->setResource($ressurs, $verdi);
    }

    /**
     * @param string $ressurs
     * @return mixed|null
     */
    public function hentRessurs(string $ressurs)
    {
        return $this->getResource($ressurs);
    }

    /**
     * @return Visning
     */
    protected function prepareData()
    {
        return $this->forberedData();
    }

    /**
     * @return $this
     */
    protected function forberedData()
    {
        return parent::prepareData();
    }

    /**
     * @param string|array $keyOrDataSet
     * @param null $value
     * @return Visning
     */
    public function definerData($keyOrDataSet, $value = null)
    {
        return $this->setDataIfNotSet($keyOrDataSet, $value);
    }

    /**
     * Send et oppdrag i form av en funksjon til en bestemt overliggende klasse (forelder/ascendant).
     * Ascendanten identifiserer ved klasse-navn.
     * Den utførende ascendanten vil sendes som eneste argument til funksjonen.
     * Visningsklassen som sendte funksjonen er også tilgjengelig i variabelen $this.
     *
     * @param string $klasse
     * @param Closure $instruksjon
     * @return Visning
     */
    public function instruerAscendant(string $klasse, Closure $instruksjon)
    {
        return parent::tellAscendant($klasse, $instruksjon);
    }

    /**
     * @param string $class
     * @param Closure $instruction
     * @return Visning
     */
    public function tellAscendant(string $class, Closure $instruction)
    {
        return $this->instruerAscendant($class, $instruction);
    }

    /**
     * @param string $klasse
     * @param bool $tilbakevendende
     * @return Visning[]
     */
    public function hentElementerMedKlasse(string $klasse, $tilbakevendende = false): array
    {
        return parent::getDescendantsByClass($klasse, $tilbakevendende);
    }

    /**
     * @param string $klasse
     * @param string $mal
     * @param bool $tilbakevendende
     * @return $this
     */
    public function settMalForElementer(string $klasse, string $mal, $tilbakevendende = false): View
    {
        return parent::setTemplateOnChildClass($klasse, $mal, $tilbakevendende);
    }
}