<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 21/09/2022
 * Time: 22:41
 */

namespace Kyegil\Leiebasen\Autoriserer;

use DATABASE_CONFIG;
use Exception;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Person;
use Laminas\Authentication\AuthenticationService;
use Laminas\Authentication\Storage\Session;
use Laminas\Db\Adapter\Adapter as DbAdapter;
use Laminas\Db\ResultSet\ResultSet;
use Laminas\Session\SessionManager;
use function setcookie;

require_once(ROOT . "/app/Config/database.php");

/**
 * Class Kyegil\Leiebasen\Autoriserer\Cakephp2
 */
class Cakephp2 extends LaminasAuth
{

    /**
     * @link https://docs.laminas.dev/laminas-authentication/
     * @return AuthenticationService
     */
    public function hentAuthenticationService(): AuthenticationService
    {
        if (!isset($this->authenticationService)) {
            $this->authenticationService = new AuthenticationService();
            $sessionManager = new SessionManager();
            $sessionManager->setName(Leiebase::$config['leiebasen']['session']['name'] ?? 'leiebasen');
            $this->authenticationService->setStorage(new Session(
                'Auth',
                'User',
                $sessionManager
            ));
        }
        return $this->authenticationService;
    }

    /**
     * @return DbAdapter
     */
    public function hentDbAdapter(): DbAdapter
    {
        if (!isset($this->dbAdapter)) {
            $db = new DATABASE_CONFIG;
            $this->dbAdapter = new DbAdapter([
                'driver' => 'mysqli',
                'database' => $db->default['database'],
                'username' => $db->default['login'],
                'password' => $db->default['password'],
                'hostname' => $db->default['host'],
                'charset' => 'utf8'
            ]);
        }
        return $this->dbAdapter;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function erInnlogget(): bool
    {
        if ($this->hentBrukerprofil()
            && isset($_SESSION['Config']['time'])
            && $_SESSION['Config']['time'] > time()
        ) {
            $_SESSION['Config']['time'] = time() + Leiebase::$config['leiebasen']['session']['timeout'] ?? 3600;
            return true;
        } else {
            $this->hentAuthenticationService()->clearIdentity();
            return false;
        }
    }

    /**
     * @return void
     */
    public function loggUt()
    {
        parent::loggUt();
        unset($_COOKIE[Leiebase::$config['leiebasen']['session']['name'] ?? 'leiebasen']);
        setcookie(Leiebase::$config['leiebasen']['session']['name'] ?? 'leiebasen', '', time() - 3600);
    }

    /**
     * @param array|null $egenskaper
     * @return void
     * @throws Exception
     */
    public function krevIdentifisering(array $egenskaper = null)
    {
        if (!$this->erInnlogget()) {
            $redirect = Leiebase::url($_GET['oppslag'] ?? '', $_GET['id'] ?? null, $_GET['oppdrag'] ?? null, $_GET ?? [], $_GET['område'] ?? '');
            header("Location: " . (\Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['installation_url'] ?? '') . "/egeninnsats/uzantoj/ensalutu?url=" . rawurlencode($redirect));
            die();
        }
    }

    /**
     * @return Person\Brukerprofil|null
     * @throws Exception
     */
    public function hentBrukerprofil(): ?Person\Brukerprofil
    {
        if ($this->hentAuthenticationService()->hasIdentity()) {
            if (!isset($this->brukerprofil)) {
                $sessionBrukerData = $this->hentAuthenticationService()->getIdentity();

                $login = $sessionBrukerData['uzanto'] ?? null;
                if($login) {
                    /** @var Person\Brukerprofilsett $brukerprofilsett */
                    $brukerprofilsett = $this->app->hentSamling(Person\Brukerprofil::class);
                    /** @var Person\Brukerprofil $brukerprofil */
                    $brukerprofil = $brukerprofilsett->leggTilFilter(['login' => $login])
                        ->leggTilPersonModell()->hentFørste();
                    if (!$brukerprofil && ($sessionBrukerData['id'] ?? null)) {
                        $brukerprofil = $this->synkBrukerProfilFraCakephp2($sessionBrukerData['id']);
                    }
                    $this->brukerprofil = $brukerprofil;
                }
            }
        }
        else {
            $this->brukerprofil = null;
            $this->bruker = null;
        }
        return $this->brukerprofil;
    }

    /**
     * @param int $brukerId
     * @return Person\Brukerprofil
     * @throws Exception
     */
    public function synkBrukerProfilFraCakephp2(int $brukerId): Person\Brukerprofil
    {
        /** @var Person $bruker */
        $bruker = $this->app->hentModell(Person::class, $brukerId);
        if (!$bruker->hentId()) {
            throw new Exception('Brukeren er ikke registrert');
        }
        /** @var ResultSet $resultSet */
        $resultSet = $this->hentDbAdapter()->query(
            'SELECT * FROM uzantoj WHERE id = ?', [$brukerId]
        );
        $cakeProfile = $resultSet->current()->getArrayCopy();

        if(!$cakeProfile) {
            throw new Exception('Fant ikke brukerprofil');
        }

        $brukerprofilSamling = $this->app->hentSamling(Person\Brukerprofil::class);
        $brukerprofilSamling->leggTilFilter(['person_id' => $brukerId]);
        if ($brukerprofilSamling->hentAntall() > 1) {
            $brukerprofilSamling->leggTilFilter(['login' => $cakeProfile['uzanto']]);
        }
        /** @var Person\Brukerprofil $brukerprofil */
        $brukerprofil = $brukerprofilSamling->hentFørste();
        if ($brukerprofil) {
            $brukerprofil->login = $cakeProfile['uzanto'];
            $brukerprofil->passord = $cakeProfile['pasvorto'];
        }
        else {
            $brukerprofil = $this->app->nyModell(Person\Brukerprofil::class, (object)[
                'person' => $brukerId,
                'login' => $cakeProfile['uzanto'],
                'passord' => $cakeProfile['pasvorto']
            ]);
        }
        $epost = $cakeProfile['retpoŝto'];
        if ($epost) {
            $brukerprofil->person->epost = $epost;
        }
        return $brukerprofil;
    }

    /**
     * @param Person\Brukerprofil $brukerprofil
     * @return $this
     * @throws Exception
     */
    public function synkBrukerProfilTilCakephp2(Person\Brukerprofil $brukerprofil): Cakephp2
    {
        if (!$brukerprofil->hentId()) {
            return $this;
        }
        /** @var ResultSet $resultSet */
        $this->hentDbAdapter()->query(
            'REPLACE INTO `uzantoj` (`id`, `uzanto`, `pasvorto`, `retpoŝto`, `nomo`, `tempo_de_kreo`, `tempozono`) VALUES (?, ?, ?, ?, ?, NOW(), ?)',
            [
                $brukerprofil->person->id,
                $brukerprofil->login,
                $brukerprofil->passord,
                $brukerprofil->person->epost,
                $brukerprofil->person->hentNavn(),
                date_default_timezone_get()
            ]
        );
        return $this;
    }
}