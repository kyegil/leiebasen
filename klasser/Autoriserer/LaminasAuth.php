<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 21/09/2022
 * Time: 22:41
 */

namespace Kyegil\Leiebasen\Autoriserer;

use Exception;
use Kyegil\Leiebasen\Autoriserer;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Brukerprofil\Engangsbillett;
use Laminas\Authentication\Adapter\DbTable\CallbackCheckAdapter as AuthAdapter;
use Laminas\Authentication\AuthenticationService;
use Laminas\Authentication\Result;
use Laminas\Authentication\Storage\Session;
use Laminas\Db\Adapter\Adapter as DbAdapter;
use Laminas\Session\SessionManager;

/**
 * Class Kyegil\Leiebasen\Autoriserer\LaminasAuth
 */
class LaminasAuth extends Autoriserer
{
    protected DbAdapter $dbAdapter;

    private AuthAdapter $authAdapter;

    protected AuthenticationService $authenticationService;

    /**
     * @link https://docs.laminas.dev/laminas-authentication/
     * @return AuthenticationService
     */
    public function hentAuthenticationService(): AuthenticationService
    {
        if (!isset($this->authenticationService)) {
            $this->authenticationService = new AuthenticationService();
            $sessionManager = new SessionManager();
            $sessionManager->setName(Leiebase::$config['leiebasen']['session']['name'] ?? 'leiebasen');
            $this->authenticationService->setStorage(new Session(
                'Kyegil_Leiebasen',
                null,
                $sessionManager
            ));
        }
        return $this->authenticationService;
    }

    /**
     * @return DbAdapter
     */
    public function hentDbAdapter(): DbAdapter
    {
        if (!isset($this->dbAdapter)) {
            $this->dbAdapter = new DbAdapter([
                'driver' => Leiebase::$config['leiebasen']['db']['driver'] ?? 'mysqli',
                'database' => Leiebase::$config['leiebasen']['db']['name'] ?? '',
                'username' => Leiebase::$config['leiebasen']['db']['user'] ?? '',
                'password' => Leiebase::$config['leiebasen']['db']['password'] ?? '',
                'hostname' => Leiebase::$config['leiebasen']['db']['host'] ?? '',
                'charset' => Leiebase::$config['leiebasen']['db']['charset'] ?? 'utf8',
            ]);
        }
        return $this->dbAdapter;
    }

    /**
     * @return AuthAdapter
     */
    public function hentAuthAdapter(): AuthAdapter
    {
        if (!isset($this->authAdapter)) {
            $this->authAdapter = new AuthAdapter(
                $this->hentDbAdapter(),
                'brukerprofiler',
                'login',
                'passord',
                function ($hash, $passord) {
                    return password_verify($passord, $hash);
                }
            );
        }
        return $this->authAdapter;
    }

    /**
     * @param string $login
     * @param string $passord
     * @return bool
     * @throws Exception
     */
    public function loggInn(string $login = '', string $passord = ''): bool
    {
        $authAdapter = $this->hentAuthAdapter();
        $authAdapter->setIdentity($login)->setCredential($passord);
        $resultat = $this->hentAuthenticationService()->authenticate($authAdapter);
        if (!$resultat->isValid()) {
            switch ($resultat->getCode()) {
                case Result::FAILURE_CREDENTIAL_INVALID:
                    throw new Exception('Venligst sjekk passordet ditt, og prøv igjen.');
                case Result::FAILURE_IDENTITY_AMBIGUOUS:
                    throw new Exception('Tvetydig brukernavn. Innlogging mislyktes.');
                case Result::FAILURE_IDENTITY_NOT_FOUND:
                    throw new Exception('Ukjent brukernavn.');
                default:
                    throw new Exception('Innlogging mislyktes:<br>' . implode('<br>', $resultat->getMessages()));
            }
        }
        /** @var Person $bruker */
        $bruker = $this->hentBruker();
        if (!$bruker || !$bruker->hentId()) {
            $this->loggUt();
            throw new Exception('Brukeren er ikke registrert');
        }
        return true;
    }

    /**
     * @return bool
     */
    public function loggUt()
    {
        $this->hentAuthenticationService()->clearIdentity();
        $this->brukerprofil = null;
        $this->bruker = null;
        return true;
    }

    /**
     * @param array|null $egenskaper
     * @return void
     */
    public function krevIdentifisering(array $egenskaper = null)
    {
        if (!$this->erInnlogget()) {
            $redirect = Leiebase::url($_GET['oppslag'] ?? '', $_GET['id'] ?? null, $_GET['oppdrag'] ?? null, $_GET ?? [], $_GET['område'] ?? '');
            header("Location: " . (\Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['installation_url'] ?? '') . "/offentlig/index.php?oppslag=innlogging&url=" . rawurlencode($redirect));
            die();
        }
    }

    /**
     * @return Person\Brukerprofil|null
     * @throws Exception
     */
    public function hentBrukerprofil(): ?Person\Brukerprofil
    {
        if ($this->hentAuthenticationService()->hasIdentity()) {
            if (!isset($this->brukerprofil)) {
                $login = $this->hentAuthenticationService()->getIdentity();

                /** @var Person\Brukerprofilsett $brukerprofilsett */
                $brukerprofilsett = $this->app->hentSamling(Person\Brukerprofil::class);
                /** @var Person\Brukerprofil $brukerprofil */
                $brukerprofil = $brukerprofilsett->leggTilFilter(['login' => $login])
                    ->leggTilPersonModell()->hentFørste();
                $this->brukerprofil = $brukerprofil;
            }
        }
        else {
            $this->brukerprofil = null;
            $this->bruker = null;
        }
        return $this->brukerprofil;
    }

    /**
     * @param string $engangsbillett
     * @return string Redirect url
     * @throws Exception
     */
    public function loggInnMedEngangsbillett(string $engangsbillett): string {
        /** @var Engangsbillett|null $engangsbillett */
        $engangsbillett = $this->app->hentSamling(Engangsbillett::class)
            ->leggTilFilter(['billett' => $engangsbillett])
            ->leggTilFilter(['utløper >= NOW()'])
            ->hentFørste();
        if (!$engangsbillett || !$engangsbillett->brukerprofil) {
            throw new Exception('Denne billetten er ugyldig, eller den har allerede blitt brukt');
        }
        $brukerprofil = $engangsbillett->hentBrukerprofil();
        $login = $brukerprofil ? $brukerprofil->login : null;
        $url = $engangsbillett->url;
        if (!$login) {
            throw new Exception('Finner ingen bruker for denne engangsbilletten');
        }
        $this->hentAuthenticationService()->getStorage()->write($login);
        $engangsbillett->slett();
        return $url;
    }

}