<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 21/09/2022
 * Time: 22:41
 */

namespace Kyegil\Leiebasen\Autoriserer;

use Exception;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Søknad;
use Laminas\Authentication\Adapter\DbTable\CallbackCheckAdapter as AuthAdapter;
use Laminas\Authentication\AuthenticationService;
use Laminas\Authentication\Result;
use Laminas\Authentication\Storage\Session;
use Laminas\Session\SessionManager;

/**
 *
 */
class MinSøknad extends LaminasAuth
{
    private ?Søknad\Adgang $søknadAdgang;

    private ?Søknad $søknad;

    /**
     * @link https://docs.laminas.dev/laminas-authentication/
     * @return AuthenticationService
     */
    public function hentAuthenticationService(): AuthenticationService
    {
        if (!isset($this->authenticationService)) {
            $this->authenticationService = new AuthenticationService();
            $sessionManager = new SessionManager();
            $sessionManager->setName(Leiebase::$config['leiebasen']['session']['name'] ?? 'leiebasen');
            $this->authenticationService->setStorage(new Session(
                'Kyegil_Leiebasen',
                'min-søknad',
                $sessionManager
            ));
        }
        return $this->authenticationService;
    }

    /**
     * @return AuthAdapter
     */
    public function hentAuthAdapter(): AuthAdapter
    {
        if (!isset($this->authAdapter)) {
            $this->authAdapter = new AuthAdapter(
                $this->hentDbAdapter(),
                'søknad_adganger',
                'referanse',
                'pw',
                function ($hash, $passord) {
                    return password_verify($passord, $hash);
                }
            );
        }
        return $this->authAdapter;
    }

    /**
     * @param string $login
     * @param string $passord
     * @return bool
     * @throws Exception
     */
    public function loggInn(string $login = '', string $passord = ''): bool
    {
        $authAdapter = $this->hentAuthAdapter();
        $authAdapter->setIdentity($login)->setCredential($passord);
        $resultat = $this->hentAuthenticationService()->authenticate($authAdapter);
        if (!$resultat->isValid()) {
            switch ($resultat->getCode()) {
                case Result::FAILURE_CREDENTIAL_INVALID:
                    throw new Exception('Venligst sjekk passordet ditt, og prøv igjen.');
                case Result::FAILURE_IDENTITY_AMBIGUOUS:
                    throw new Exception('Tvetydig brukernavn. Innlogging mislyktes.');
                case Result::FAILURE_IDENTITY_NOT_FOUND:
                    throw new Exception('Ukjent brukernavn.');
                default:
                    throw new Exception('Innlogging mislyktes:<br>' . implode('<br>', $resultat->getMessages()));
            }
        }
        /** @var Søknad $søknad */
        $søknad = $this->hentSøknad();
        if (!$søknad || !$søknad->hentId()) {
            $this->loggUt();
            throw new Exception('Finner ikke søknad med denne referansen');
        }
        return true;
    }

    /**
     * @return null
     */
    public function hentId(): ?int
    {
        return null;
    }

    /**
     * @return string|null
     */
    public function hentBrukernavn(): ?string
    {
        return null;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function hentEpost(): ?string
    {
        $søknadAdgang = $this->hentSøknadAdgang();
        return $søknadAdgang ? $søknadAdgang->epost : null;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function hentNavn(): ?string
    {
        $søknad = $this->hentSøknad();
        if ($søknad) {
            $navn = $søknad->hent('kontaktperson_navn');
            if (!$navn) {
                $navn = trim($søknad->hent('kontaktperson_fornavn') . ' ' . $søknad->hent('kontaktperson_etternavn'));
            }
            return $navn ?: null;
        }
        return null;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function erInnlogget(): bool
    {
        return (bool)$this->hentSøknad();
    }

    /**
     * @return Søknad\Adgang|null
     * @throws Exception
     */
    public function hentSøknadAdgang(): ?Søknad\Adgang
    {
        if ($this->hentAuthenticationService()->hasIdentity()) {
            if (!isset($this->søknadAdgang)) {
                $referanse = $this->hentAuthenticationService()->getIdentity();

                /** @var Søknad\Adgangsett $søknadAdgangsett */
                $søknadAdgangsett = $this->app->hentSamling(Søknad\Adgang::class);
                /** @var Søknad\Adgang $søknadAdgang */
                $søknadAdgang = $søknadAdgangsett->leggTilFilter(['referanse' => $referanse])->hentFørste();
                $this->søknadAdgang = $søknadAdgang;
            }
        }
        else {
            $this->søknadAdgang = null;
            $this->søknad = null;
        }
        return $this->søknadAdgang;
    }

    /**
     * @return Søknad|null
     * @throws Exception
     */
    public function hentSøknad(): ?Søknad
    {
        $søknadAdgang = $this->hentSøknadAdgang();
        $this->søknad = $søknadAdgang ? $søknadAdgang->søknad : null;
        return $this->søknad;
    }
}