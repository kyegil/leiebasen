<?php

namespace Kyegil\Leiebasen\Sms;

use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Melding;
use Kyegil\Leiebasen\Modell\Meldingsett;

abstract class AbstraktProsessor
{
    /** @var Leiebase */
    protected $app;

    /**
     * @param Leiebase $leiebase
     * @return AbstraktProsessor
     */
    abstract public static function getInstance(Leiebase $leiebase):AbstraktProsessor;

    /**
     * @param string $smsNummer
     * @param string $melding
     * @return Melding
     */
    abstract public function forberedMelding(string $smsNummer, string $smsAvsender, string $melding): Melding;

    /**
     * @return int|null
     */
    abstract public function hentPartiBegrensning():?int;

    /**
     * @param Melding $melding
     * @return AbstraktProsessor
     */
    abstract public function send(Melding $melding):AbstraktProsessor;

    /**
     * Sender en batch e-poster ifra køen
     *
     * @return AbstraktProsessor
     * @throws \Exception
     */
    public function leverSms(): AbstraktProsessor
    {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        /** @var Meldingsett $meldingsett */
        $meldingsett = $this->app->hentSamling(Melding::class);
        $meldingsett->leggTilFilter(['medium' => \Kyegil\Leiebasen\Modell\Melding::MEDIUM_SMS])->låsFiltre();
        $meldingsett->leggTilSortering('id')->leggTilSortering('prioritet', true);
        $meldingsett->begrens($this->hentPartiBegrensning());

        foreach($meldingsett as $melding) {
            if($this->hentPartiBegrensning() > 0) {
                try {
                    $this->send($melding);
                } catch (\Throwable $e) {
                    $this->app->logger->critical($e);
                }
            }
        }
        return $this->app->post($this, __FUNCTION__, $this, $args);
    }
}