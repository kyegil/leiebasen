<?php

namespace Kyegil\Leiebasen\Sms;

use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Melding;

class VoidSms extends AbstraktProsessor
{
    /** @var VoidSms */
    private static VoidSms $instance;
    /** @var Leiebase */
    protected $app;

    /**
     * @param Leiebase $leiebase
     * @return $this
     */
    public static function getInstance(Leiebase $leiebase): AbstraktProsessor
    {
        if ( !isset( self::$instance ) )
        {
            self::$instance = new self($leiebase);
        }
        return self::$instance;
    }

    /**
     * @param Leiebase $app
     */
    private function __construct(Leiebase $app)
    {
        $this->app = $app;
    }

    /**
     * @return int|null
     */
    public function hentPartiBegrensning(): ?int
    {
        return null;
    }

    /**
     * @param string $smsNummer
     * @param string $melding
     * @return Melding
     */
    public function forberedMelding(string $smsNummer, string $smsAvsender, string $melding): Melding
    {
        /** @var Melding $melding */
        $melding = $this->app->nyModell(Melding::class, (object)[
            'medium' => Melding::MEDIUM_SMS,
            'til' => $smsNummer,
            'headers' => (object)['sender' => $smsAvsender],
            'innhold' => $melding,
        ]);
        return $melding;
    }

    /**
     * @param Melding $melding
     * @return AbstraktProsessor
     */
    public function send(Melding $melding): AbstraktProsessor
    {
        return $this;
    }
}