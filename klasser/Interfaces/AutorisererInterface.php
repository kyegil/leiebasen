<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 21/09/2022
 * Time: 20:28
 */

namespace Kyegil\Leiebasen\Interfaces;

interface AutorisererInterface
{
    public function hentId():?int;
    public function hentNavn():?string;
    public function hentEpost():?string;
    public function hentBrukernavn():?string;
    public function erInnlogget(): bool;
    public function erLoginTilgjengelig(string $login, int $personId): bool;
    public function loggInn(string $login = '', string $passord = ''): bool;
    public function loggUt();
    public function krevIdentifisering(array $egenskaper = null);
    public function finnBrukernavnForPersonId(int $personId): ?string;
    public function loggInnMedEngangsbillett(string $engangsbillett): string;
}