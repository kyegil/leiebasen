<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 30/09/2022
 * Time: 20:01
 */

namespace Kyegil\Leiebasen\Interfaces;

interface EventManagerInterface
{
    public static function addListener(string $class, string $event, callable $action, ?bool $priority = false);
    public static function getListeners(string $class, string $event): array;
    public static function clearListeners(string $class, ?string $event = null);
    public function triggerEvent(string $class, string $event, ?object $subject, array $arguments = [], ?object $app = null):?array ;
}