<?php

namespace Kyegil\Leiebasen\Visning\flyko\js\adgang_liste\body\main;


use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Skript-fil i flyko
 *
 *  Ressurser:
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\flyko\js\adgang_liste\body\main
 */
class Index extends Flyko
{
    /** @var string */
    protected $template = 'flyko/js/adgang_liste/body/main/Index.js';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        return parent::forberedData();
    }
}