<?php

namespace Kyegil\Leiebasen\Visning\flyko\js\søknad_kort\body\main;


use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Skript-fil i flyko
 *
 *  Ressurser:
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\flyko\js\søknad_kort\body\main
 */
class Index extends Flyko
{
    protected $template = 'flyko/js/søknad_kort/body/main/Index.js';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        return parent::forberedData();
    }
}