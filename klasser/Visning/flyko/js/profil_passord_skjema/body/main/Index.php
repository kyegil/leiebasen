<?php

namespace Kyegil\Leiebasen\Visning\flyko\js\profil_passord_skjema\body\main;


use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Skript-fil i flyko
 *
 *  Ressurser:
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\flyko\js\profil_passord_skjema\body\main
 */
class Index extends Flyko
{
    protected $template = 'flyko/js/profil_passord_skjema/body/main/Index.js';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        return parent::forberedData();
    }
}