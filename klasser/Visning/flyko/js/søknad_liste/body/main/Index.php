<?php

namespace Kyegil\Leiebasen\Visning\flyko\js\søknad_liste\body\main;


use Kyegil\Leiebasen\Modell\Søknad\Type;
use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Skript-fil i flyko
 *
 *  Ressurser:
 *      søknad_type \Kyegil\Leiebasen\Modell\Søknad\Type
 *  Mulige variabler:
 *      $ekstraJs
 * @package Kyegil\Leiebasen\Visning\flyko\js\søknad_liste\body\main
 */
class Index extends Flyko
{
    /** @var string */
    protected $template = 'flyko/js/søknad_liste/body/main/Index.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return Index
     */
    public function sett($attributt, $verdi = null)
    {
        if ($attributt == 'søknad_type') {
            return $this->settSøknadType($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Type $type
     * @return Index
     */
    private function settSøknadType(Type $type): Index
    {
        return $this->settRessurs('søknad_type', $type);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Type|null $søknadstype */
        $søknadstype = $this->hentRessurs('søknad_type');

        $datasett = ['ekstraJs' => ''];
        if ($søknadstype) {
            $datasett['ekstraJs'] = $søknadstype->hentKonfigurering('javaScript');
        }
        $this->definerData($datasett);
        return parent::forberedData();
    }
}