<?php

namespace Kyegil\Leiebasen\Visning\flyko\js\søknad_skjema\body\main;


use Exception;
use Kyegil\Leiebasen\Modell\Søknad\Type;
use Kyegil\Leiebasen\Visning\Offentlig;

/**
 * Skript-fil for flyko's interne visning av offentlig søknadsskjema
 *
 *  Ressurser:
 *      søknadSkjema \Kyegil\Leiebasen\Modell\Søknad\Type
 *  Mulige variabler:
 *      $språkKode
 *      $script
 * @package Kyegil\Leiebasen\Visning\flyko\js\søknad_skjema\body\main
 */
class Index extends Offentlig
{
    /** @var string */
    protected $template = 'flyko/js/søknad_skjema/body/main/Index.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['søknadSkjema'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Type|null $skjema */
        $skjema = $this->hentRessurs('søknadSkjema');
        $javaScript = '';
        if ($skjema && $skjema->konfigurering) {
            $konfigurering = $skjema->konfigurering;
            $javaScript = $konfigurering->javaScript ?? '';
        }
        $this->definerData(['script' => $javaScript]);
        return parent::forberedData();
    }
}