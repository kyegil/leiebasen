<?php

namespace Kyegil\Leiebasen\Visning\flyko\js\søknad_arkiv\body\main;


use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Skript-fil i flyko
 *
 *  Ressurser:
 *      søknad_type \Kyegil\Leiebasen\Modell\Søknad\Type
 *  Mulige variabler:
 *      $ekstraJs
 * @package Kyegil\Leiebasen\Visning\flyko\js\søknad_arkiv\body\main
 */
class Index extends Flyko
{
    /** @var string */
    protected $template = 'flyko/js/søknad_arkiv/body/main/Index.js';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $datasett = ['ekstraJs' => ''];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}