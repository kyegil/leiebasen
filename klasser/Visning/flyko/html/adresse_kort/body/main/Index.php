<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\adresse_kort\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\felles\html\person\Adressefelt;
use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Visning for adressekort i flyko
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $kort
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\flyko\html\adresse_kort\body\main
 */
class Index extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/adresse_kort/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['adressekort'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $kortData = [];
        /** @var Person|null $adressekort */
        $adressekort = $this->hentRessurs('adressekort');
        if ($adressekort) {
            if ($adressekort->erOrg) {
                $kortData[] = new HtmlElement('dt', ['class' => 'label'], 'Organisasjonsnr');
                $kortData[] = new HtmlElement('dd', [], $adressekort->orgNr);
            }
            else {
                $kortData[] = new HtmlElement('dt', ['class' => 'label'], 'Fødselsdato');
                $kortData[] = new HtmlElement('dd', [], ($adressekort->fødselsdato ? $adressekort->fødselsdato->format('d.m.Y') : ''));
            }
            $kortData[] = new HtmlElement('dt', ['class' => 'label'], 'E-post');
            if ($adressekort->epost) {
                $kortData[] = new HtmlElement(
                    'dd',
                    [],
                    new HtmlElement(
                        'a',
                        ['href' => 'mailto:' . $adressekort->epost, 'title' => 'Send epost'],
                        $adressekort->epost
                    )
                );
            }
            else {
                $kortData[] = new HtmlElement('dd');
            }
            $kortData[] = new HtmlElement('dt', ['class' => 'label'], 'Telefon');
            if ($adressekort->telefon) {
                $kortData[] = new HtmlElement(
                    'dd',
                    [],
                    new HtmlElement(
                        'a',
                        ['href' => 'tel:' . str_replace(' ', '', $adressekort->telefon), 'title' => 'Ring'],
                        $adressekort->telefon
                    )
                );
            }
            else {
                $kortData[] = new HtmlElement('dd');
            }
            $kortData[] = new HtmlElement('dt', ['class' => 'label'], 'Mobiltelefon');
            if ($adressekort->mobil) {
                $kortData[] = new HtmlElement(
                    'dd',
                    [],
                    new HtmlElement(
                        'a',
                        ['href' => 'tel:' . str_replace(' ', '', $adressekort->mobil), 'title' => 'Ring'],
                        $adressekort->mobil
                    )
                );
            }
            else {
                $kortData[] = new HtmlElement('dd');
            }
            $kortData[] = new HtmlElement('dt', ['class' => 'label'], 'Adresse');
            $kortData[] = new HtmlElement('dd', [], $this->app->vis(Adressefelt::class, ['person' => $adressekort]));
        }
        $kortData = new HtmlElement('dl', [], $kortData);
        $datasett = [
            'kort' => $kortData,
            'knapper' => [],
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}