<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\søknad_skjema_skjema\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Søknad\Type;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\HtmlEditor;
use Kyegil\Leiebasen\Visning\felles\html\form\TextArea;
use Kyegil\Leiebasen\Visning\Flyko;
use stdClass;

/**
 * Visning for søknadskjema-oppsett i mine sider
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *      søknadsskjema \Kyegil\Leiebasen\Modell\Søknad\Type
 *  Mulige variabler:
 *      $skjema
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\flyko\html\søknad_skjema_skjema\body\main
 */
class Index extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/søknad_skjema_skjema/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['søknadsskjema'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Type|null $søknadSkjema */
        $søknadSkjema = $this->hentRessurs('søknadsskjema');
        $råVerdier = $søknadSkjema ? $søknadSkjema->getRawData() : new stdClass();
        /** @var string $konfigurering */
        $konfigurering = property_exists($råVerdier, 'søknadstyper') && property_exists($råVerdier->søknadstyper, 'konfigurering') ? $råVerdier->søknadstyper->konfigurering : null;
        $konfigurering = $søknadSkjema ? $søknadSkjema->konfigurering : new \stdClass();
        $jsKonfig = $konfigurering->javaScript ?? '';
        unset($konfigurering->javaScript);

        /** @var Field $kodeFelt */
        $kodeFelt = $this->app->vis(Field::class, [
            'name' => 'kode',
            'label' => 'Kode',
            'required' => true,
            'value' => $søknadSkjema ? $søknadSkjema->kode : null
        ]);

        /** @var Field $kodeFelt */
        $navnFelt = $this->app->vis(Field::class, [
            'name' => 'navn',
            'label' => 'Navn',
            'required' => true,
            'value' => $søknadSkjema ? $søknadSkjema->navn : null
        ]);

        /** @var HtmlEditor $detaljFelt */
        $beskrivelseFelt = $this->app->vis(HtmlEditor::class, [
            'name' => 'beskrivelse',
            'label' => 'Beskrivelse',
            'value' => $søknadSkjema ? $søknadSkjema->hentBeskrivelse() : null
        ]);

        $konfigFelt = $this->app->vis(TextArea::class, [
            'id' => 'søknadsskjema-konfigurering',
            'type' => 'hidden',
            'name' => 'konfigurering',
            'label' => 'Konfigurering',
            'value' => $this->gjørJsonLesbar($konfigurering)
        ]);

        $jsFelt = $this->app->vis(TextArea::class, [
            'id' => 'søknadsskjema-javascript',
            'type' => 'hidden',
            'name' => 'js',
            'label' => 'Evt. skripts i skjemaet',
            'value' => $søknadSkjema ? $jsKonfig : ''
        ]);

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/flyko/index.php?oppslag=søknad_skjema_skjema&oppdrag=ta_i_mot_skjema&skjema=søknadsskjema&id=' . ($søknadSkjema ? $søknadSkjema->hentId() : '*'),
                'formId' => 'søknadsskjema',
                'buttonText' => 'Lagre',
                'fields' => [
                    new HtmlElement('div', [
                        'class' => 'postnrsted',
                        'style' => 'width: 100%; display: inline-flex;'
                    ], [
                        new HtmlElement('div', ['style' => 'margin: 0 10px;'], $kodeFelt),
                        new HtmlElement('div', ['style' => 'margin: 0 10px; width:100%'], $navnFelt)
                    ]),
                    $beskrivelseFelt,
                    $konfigFelt,
                    $jsFelt
                ]
            ]),
            'knapper' => []
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @param $objekt
     * @return false|string
     */
    private function gjørJsonLesbar($objekt)
    {
        return json_encode($objekt, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
    }
}