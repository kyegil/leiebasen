<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\framleie_liste\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleie;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleiesett;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Visning for framleieliste i flyko
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $tabell
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\flyko\html\framleie_liste\body\main
 */
class Index extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/framleie_liste/body/main/Index.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /**
         * Kolonne-objekt for DataTables
         *
         * @link https://datatables.net/reference/option/columns
         */
        $kolonner = [
            (object)[
                'data' => 'framleieid',
                'name' => 'framleieid',
                'type' => 'num',
                'title' => 'Id',
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
//                'width' => '40px'
            ],
            (object)[
                'data' => 'fradato',
                'name' => 'fradato',
                'type' => 'date',
                'title' => 'Fra dato',
                'render' => new JsFunction('return type === \'display\' || type === \'filter\' ? row.fradatoFormatert : data;', ['data', 'type', 'row', 'meta']),
                'responsivePriority' => 4, // Høyere tall = lavere prioritet
            ],
            (object)[
                'data' => 'tildato',
                'name' => 'tildato',
                'title' => 'Til dato',
                'responsivePriority' => 3, // Høyere tall = lavere prioritet
                'render' => new JsFunction('return type === \'display\' || type === \'filter\' ? row.tildatoFormatert : data;', ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'leieforholdId',
                'name' => 'leieforholdId',
                'title' => 'Leieforhold',
                'responsivePriority' => 1, // Høyere tall = lavere prioritet
                'render' => new JsFunction('return type === \'display\' ? row.leieforhold : data;', ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'framleiere',
                'name' => 'framleiere',
                'title' => 'Framleid til',
                'responsivePriority' => 1, // Høyere tall = lavere prioritet
            ],
        ];

        $data = [];
        /** @var Framleiesett $framleiesett */
        $framleiesett = $this->app->hentSamling(Framleie::class);
        $framleiesett->leggTilLeieforholdModell();
        foreach($framleiesett as $framleieavtale) {
            $leieforhold = $framleieavtale->hentLeieforhold();
            $fraDato = $framleieavtale->hentFradato();
            $tilDato = $framleieavtale->hentTildato();
            $framleiepersoner = [];
            foreach ($framleieavtale->hentFramleiepersoner() as $person) {
                $framleiepersoner[] = $person->hentNavn();
            }
            $data[] = (object)[
                'framleieid' => $framleieavtale->hentId(),
                'leieforholdId' => $leieforhold ? $leieforhold->hentId() : null,
                'leieforhold' => $leieforhold ? ($leieforhold->hentId() . ' ' . $leieforhold->hentBeskrivelse()) : null,
                'fradato' => $fraDato ? $fraDato->format('Y-m-d') : '',
                'fradatoFormatert' => $fraDato ? $fraDato->format('d.m.Y') : '',
                'tildato' => $tilDato ? $tilDato->format('Y-m-d') : '',
                'tildatoFormatert' => $tilDato ? $tilDato->format('d.m.Y') : '',
                'framleiere' => implode(', ', $framleiepersoner)
            ];
        }

        /**
         * Konfig-objekt for DataTables
         * * pageLength initiert antall per side
         * * lengthMenu
         *
         * @link https://datatables.net/reference/option/
         */
        $dataTablesConfig = (object)[
            'order' => [[0, 'desc']],
            'columns' => $kolonner,
            'pageLength' => 25,
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'data' => $data,
        ];

        $datasett = [
            'tabell' => $this->app->vis(DataTable::class, [
                'tableId' => 'framleie',
                'caption' => 'Oversikt over framleie',
                'kolonner' => $kolonner,
                'data' => $data,
                'dataTablesConfig' => $dataTablesConfig
            ]),
            'knapper' => [],
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}