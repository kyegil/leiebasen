<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\leieobjekter_ledige\body\main\index;


use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Leieobjekt-linje i liste over ledige leieobjekter i flyko
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $dato
 *      $leieobjektnr
 *      $leieobjektbeskrivelse
 *      $endring
 * @package Kyegil\Leiebasen\Visning\flyko\html\leieobjekter_ledige\body\main\index
 */
class KommendeLedighetLinje extends Flyko
{
    protected $template = 'flyko/html/leieobjekter_ledige/body/main/index/KommendeLedighetLinje.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        return parent::forberedData();
    }
}