<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\leieobjekter_ledige\body\main\index;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Leieobjekt-linje i liste over ledige leieobjekter i flyko
 *
 *  Ressurser:
 *      leieobjekt \Kyegil\Leiebasen\Modell\Leieobjekt
 *      sisteLeieForhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $leieobjektnr
 *      $leieobjektbeskrivelse
 *      $sisteLeietaker
 *      $ledighetsTekst
 *      $leietakerliste
 * @package Kyegil\Leiebasen\Visning\flyko\html\leieobjekter_ledige\body\main\index
 */
class LeieobjektLinje extends Flyko
{
    protected $template = 'flyko/html/leieobjekter_ledige/body/main/index/LeieobjektLinje.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['leieobjekt', 'sisteLeieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Leieobjekt $leieobjekt */
        $leieobjekt = $this->hentRessurs('leieobjekt');
        if($leieobjekt) {
            /** @var Leieforhold|null $sisteLeieForhold */
            $sisteLeieForhold = $this->hentRessurs('sisteLeieforhold') ?: $leieobjekt->hentSisteLeieforhold();
            $ledighet = $leieobjekt->hentLedighet();

            $datasett = [
                'leieobjektnr' => $leieobjekt->hentType() . ' ' . $leieobjekt->hentId(),
                'leieobjektbeskrivelse' => new HtmlElement('a', ['href' => '/flyko/index.php?oppslag=leieobjekt&id=' . $leieobjekt->hentId()], $leieobjekt->hentBeskrivelse()),
                'sisteLeietaker' => $sisteLeieForhold ? $sisteLeieForhold->hentNavn() : '',
                'ledighetsTekst' => $ledighet->asDecimal() <> 1 ? $ledighet->asMixedNumber() . ' er ledig.' : ''
            ];
            $this->definerData($datasett);
        }

        return parent::forberedData();
    }
}