<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\søknad_skjema_liste\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Visning for søknadsskjema-liste i flyko
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $tabell
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\flyko\html\søknad_skjema_liste\body\main
 */
class Index extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/søknad_skjema_liste/body/main/Index.html';

    public function sett($attributt, $verdi = null)
    {
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /**
         * Kolonne-objekt for DataTables
         *
         * @link https://datatables.net/reference/option/columns
         */
        $kolonner = [
            (object)[
                'data' => 'id',
                'name' => 'id',
                'title' => 'Id',
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return type== 'display' ? '<a href=\"/flyko/index.php?oppslag=søknad_skjema_skjema&id=' + row.id + '\">' + data + '</a>' : data;", ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'kode',
                'name' => 'kode',
                'title' => 'Kode',
                'responsivePriority' => 1, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return type== 'display' ? '<a href=\"/flyko/index.php?oppslag=søknad_skjema_skjema&id=' + row.id + '\">' + data + '</a>' : data;", ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'navn',
                'name' => 'navn',
                'title' => 'Navn',
                'responsivePriority' => 1, // Høyere tall = lavere prioritet
            ],
            (object)[
                'data' => 'id',
                'title' => '',
                'responsivePriority' => 1, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return type== 'display' ? '<a href=\"/flyko/index.php?oppslag=søknad_liste&søknad_type=' + row.id + '\">Se innkomne søknader</a>' : data;", ['data', 'type', 'row', 'meta']),
            ],
        ];

        $data = [];
        /** @var Søknad\Typesett $søknadTypesett */
        $søknadTypesett = $this->app->hentSamling(Søknad\Type::class);

        foreach($søknadTypesett as $søknadType) {
            $data[] = (object)[
                'id' => $søknadType->hentId(),
                'kode' => $søknadType->hentKode(),
                'navn' => $søknadType->hentNavn(),
            ];
        }

        /**
         * Konfig-objekt for DataTables
         * * pageLength initiert antall per side
         * * lengthMenu
         *
         * @link https://datatables.net/reference/option/
         */
        $dataTablesConfig = (object)[
            'order' => [[0, 'asc']],
            'columns' => $kolonner,
            'pageLength' => 25,
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'data' => $data,
        ];

        $datasett = [
            'tabell' => $this->app->vis(DataTable::class, [
                'tableId' => 'søknadstyper',
                'kolonner' => $kolonner,
                'data' => $data,
                'dataTablesConfig' => $dataTablesConfig
            ]),
            'knapper' => [$this->hentLinkKnapp("/flyko/index.php?oppslag=søknad_skjema_skjema&id=*", 'Opprett nytt søknadsskjema', 'Lag et helt nytt søknadskjema.')],
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}