<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\leieobjekter_liste\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Visning for skadeliste i flyko
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $leieobjekttabell
 * @package Kyegil\Leiebasen\Visning\flyko\html\leieobjekter_liste\body\main
 */
class Index extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/leieobjekter_liste/body/main/Index.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $kolonner = [
            (object)[
                'data' => 'posisjon',
                'name' => 'posisjon',
                'title' => 'Posisjon',
                'width' => '40px'
            ],
            (object)[
                'data' => 'leieobjektnr',
                'name' => 'leieobjektnr',
                'title' => 'Nr',
                'responsivePriority' => 2,
                'width' => '40px'
            ],
            (object)[
                'data' => 'type',
                'name' => 'type',
                'title' => 'Type',
                'width' => '50px'
            ],
            (object)[
                'data' => 'beskrivelse',
                'name' => 'beskrivelse',
                'title' => 'Beskrivelse',
                'responsivePriority' => 1,
                'render' => new JsFunction("return '<a href=\"/flyko/index.php?oppslag=leieobjekt&id=' + row.leieobjektnr + '\">' + data + '</a>';", ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'bygning_id',
                'name' => 'bygning_id',
                'render' => new JsFunction("return row.bygning_navn;", ['data', 'type', 'row', 'meta']),
                'title' => 'Bygning',
            ],
            (object)[
                'data' => 'etg',
                'name' => 'etg',
                'title' => 'Etg',
                'width' => '50px'
            ],
            (object)[
                'data' => 'areal',
                'name' => 'areal',
                'title' => 'Areal',
                'render' => new JsFunction("return data + '&nbsp;m²';", ['data', 'type', 'row', 'meta']),
                'width' => '50px'
            ],
        ];

        $data = [];
        $leieobjektsett = $this->app->hentAktiveLeieobjekter()->leggTilBygningModell();
        foreach($leieobjektsett as $leieobjekt) {
            $posisjon = $leieobjekt->hentUtskriftsposisjon();
            $data[] = (object)[
                'posisjon' => $posisjon ?: '',
                'leieobjektnr' => $leieobjekt->hentId(),
                'type' => $leieobjekt->hentType(),
                'beskrivelse' => $leieobjekt->beskrivelse,
                'bygning_id' => strval($leieobjekt->bygning),
                'bygning_navn' => $leieobjekt->bygning ? $leieobjekt->bygning->navn : '',
                'etg' => $leieobjekt->etg,
                'areal' => $leieobjekt->areal,
                'ant_rom' => $leieobjekt->antRom
            ];
        }
        $dataTablesConfig = (object)[
            'order' => [[0, 'asc']],
            'columns' => $kolonner,
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'data' => $data,
        ];

        $datasett = [
            'leieobjekttabell' => $this->app->vis(DataTable::class, [
                'tableId' => 'leieobjekter',
                'caption' => 'Oversikt over leieobjekter',
                'kolonner' => $kolonner,
                'data' => $data,
                'dataTablesConfig' => $dataTablesConfig
            ]),
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}