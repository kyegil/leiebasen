<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\søknad_kort\body\main\notater;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Visning for søknadsnotat i flyko
 *
 *  Ressurser:
 *      notat \Kyegil\Leiebasen\Modell\Søknad\Notat
 *  Mulige variabler:
 *      $notat_id
 *      $søknad_id
 *      $bruker_id
 *      $søknad
 *      $forfatter
 *      $tidspunkt
 *      $tekst
 *      $sletteKnapp
 * @package Kyegil\Leiebasen\Visning\flyko\html\søknad_kort\body\main\notater
 */
class Notat extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/søknad_kort/body/main/notater/Notat.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['notat'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $datasett = [
            'notat_id' => '',
            'søknad_id' => '',
            'bruker_id' => '',
            'søknad' => '',
            'forfatter' => '',
            'tidspunkt' => '',
            'tekst' => '',
            'sletteKnapp' => '',
        ];

        /** @var Søknad\Notat|null $notat */
        $notat = $this->hentRessurs('notat');

        if($notat) {
            $sletteKnapp = $notat->hentSystemgenerert() ? '' : new HtmlElement('a', [
                'class' => 'slett button',
                'onclick' => new JsCustom('leiebasen.flyko.søknadKort.slettNotat(' . $notat->hentId() . ')')
            ], 'Slett notatet');
            $datasett = [
                'notat_id' => $notat->hentId(),
                'søknad_id' => $notat->søknad->hentId(),
                'bruker_id' => $notat->forfatter->hentId(),
                'søknad' => '',
                'forfatter' => $notat->forfatter->hentNavn(),
                'tidspunkt' => $notat->tidspunkt->format('d.m.Y H:i'),
                'tekst' => $notat->tekst,
                'sletteKnapp' => $sletteKnapp,
            ];
        }

        $this->definerData($datasett);
        return parent::forberedData();
    }
}