<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\søknad_kort\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Søknadprosessor;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\AutoComplete;
use Kyegil\Leiebasen\Visning\Flyko;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

/**
 * Visning for søknad i flyko
 *
 *  Ressurser:
 *      søknad \Kyegil\Leiebasen\Modell\Søknad
 *  Mulige variabler:
 *      $kort
 *      $notater
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\flyko\html\søknad_kort\body\main
 */
class Index extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/søknad_kort/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['søknad'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $kort = new ViewArray();
        $kort->addItem(new HtmlElement('div', ['class' => 'utdyping'],
            ['Gå til <a href="#kategorier">bunnen av siden</a> for å se eller endre kategoriseringen av denne søknaden, eller for å legge til administrative tilleggsopplysninger.',]
        ));


        $metaData = [];
        $kortData = [];
        /** @var Søknad|null $søknad */
        $søknad = $this->hentRessurs('søknad');
        $søknadsfelter = $søknad->type->hentFeltKonfigurasjoner();

        $gyldighetstid = Søknadprosessor::getInstance($this->app)->hentGyldighet($søknad->type);
        $oppdatert = $søknad->hentOppdatert() ?? $søknad->hentRegistrert();
        $utløpt = $gyldighetstid && ($oppdatert < date_create_immutable()->sub($gyldighetstid));

        $metaData[] = new HtmlElement('dt', ['class' => 'label'], 'Aktiv søknad');
        $metaData[] = new HtmlElement('dd', [], $søknad->aktiv ? 'Ja' : 'Nei');
        $metaData[] = new HtmlElement('dt', ['class' => 'label'], 'Registrert');
        $metaData[] = new HtmlElement('dd', [], $søknad->registrert->format('d.m.Y'));
        $metaData[] = new HtmlElement('dt', ['class' => 'label'], 'Oppdatert');
        $metaData[] = new HtmlElement('dd', ($utløpt ? ['class' => 'utløpt'] : []), $søknad->oppdatert ? $søknad->oppdatert->format('d.m.Y') : '');

        $søknadstekstInkludert = false;
        $søknadstekstPosisjon = array_search('søknadstekst', $søknad->type->hentFelter());
        /** @var Egenskap $søknadsfelt */
        foreach($søknadsfelter as $søknadsfelt) {
            $feltKode = 'felt-' . $søknadsfelt->hentKode();
            $etikett = $this->hentEtikett($søknadsfelt->hentKode(), $søknad);
            $verdi = $this->hentVerdi($søknadsfelt, $søknad);

            if (!$søknadstekstInkludert && array_search($søknadsfelt->kode, $søknad->type->hentFelter()) > $søknadstekstPosisjon) {
                $kortData[] = new HtmlElement('dt', ['class' =>  'felt-søknadstekst label'], 'Søknad');
                $kortData[] = new HtmlElement('dd', ['class' => 'felt-søknadstekst'], $søknad->søknadstekst);
                $søknadstekstInkludert = true;
            }
            if (isset($verdi) && $verdi !== '') {
                $kortData[] = new HtmlElement('dt', ['class' => $feltKode . ' label'], $etikett);
                $kortData[] = new HtmlElement('dd', ['class' => $feltKode], $verdi);
            }
        }

        $kort->addItem(new HtmlElement('dl', ['class' => 'metadata'], $metaData));
        $kort->addItem(new HtmlElement('dl', ['class' => 'søknadsinnhold'], $kortData));
        $kort->addItem(
            new ViewArray([
                new HtmlElement('div', ['style' => 'float: right;'],
                    $this->hentLinkKnapp(
                        '/flyko/index.php?oppslag=søknad_skjema&id=' . $søknad->hentId(),
                        'Rediger/oppdater originalsøknaden ...'
                    ),
                ),
                new HtmlElement('div', ['class' => 'utdyping'],
                    [
                        'Det er mulig å endre innholdet i selve søknaden, for å rette evt feil ved registrering osv. Evt endringer blir notert.<br>',
                        'Endringene vil også være synlig for søker.<br>',
                        'Som et alternativ kan du i stedet oppdatere <a href="#kategorier">kategoriene</a> eller legge til et <a href="#søknad-notater">notat</a>.',
                    ]
                ),
            ]),
        );
        $kort->addItem(new HtmlElement('hr',));

        $kategoriskjema = $this->app->vis(AjaxForm::class, [
            'action' => '/flyko/index.php?oppslag=søknad_kort&oppdrag=ta_i_mot_skjema&skjema=kategorier&id=' . ($søknad ? $søknad->hentId() : '*'),
            'formId' => 'kategorier',
            'buttonText' => 'Lagre kategoriene',
            'fields' => [
                new HtmlElement('h3', [], 'Kategorisering av søknaden'),
                new HtmlElement('div', ['class' => 'utdyping'],
                    [
                        'Kategoriene brukes for å organisere søknadene internt, og er ikke tilgjengelig for søker. Velg aktuelle kategorier ifra nedtrekkslisten, eller skriv inn en ny kategori som fritekst.<br>',
                        'Unngå unødige eller tvetydige kategorier, og duplikater som skyldes feilstaving. Hold kategoriene på et oversiktlig nivå. For å slette en kategori helt må den fjernes fra alle søknadene som bruker den.',
                    ]
                ),
                $this->app->vis(AutoComplete::class, [
                    'name' => 'kategorier',
                    'options' => $søknad->type ? $søknad->type->hentKategorier() : [],
                    'class' => 'filter',
                    'multiple' => true,
                    'forceSelection' => false,
                    'tokenSeparators' => [','],
                    'value' => $søknad->hentKategorier()
                ]),
            ],
        ]);

        $datasett = [
            'kort' => [
                new HtmlElement('div', [
                    'class' => $søknad->aktiv ? 'søknadskort aktiv' : 'søknadskort inaktiv'
                ], $kort),
                $kategoriskjema
            ],
            'notater' => $this->app->vis(Notater::class, ['søknad' => $søknad]),
            'knapper' => [],
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @param string $kode
     * @param Søknad $søknad
     * @return object|null
     * @throws Exception
     */
    public static function hentLayoutElement(string $kode, Søknad $søknad): ?object {
        $layout = $søknad->hentTypekonfigurering('layout');
        foreach ($layout as $felt) {
            if (isset($felt->id) && $felt->id == ($kode . '_felt')
                && !empty($felt->label)) {
                return $felt;
            }
        }
        return null;
    }

    /**
     * @param string $kode
     * @param Søknad|null $søknad
     * @return string
     * @throws Exception
     */
    private static function hentEtikett(string $kode, ?Søknad $søknad = null): string
    {
        if($søknad) {
            $layoutElement = self::hentLayoutElement($kode, $søknad);
            if ($layoutElement && !empty($layoutElement->label)) {
                return $layoutElement->label;
            }
        }
        return Modell::ucfirst(str_replace('_', ' ', $kode));
    }

    /**
     * @param Egenskap $egenskap
     * @param Søknad $søknad
     * @return string|null
     * @throws Exception
     */
    private function hentVerdi(Egenskap $egenskap, Søknad $søknad): ?string
    {
        $verdi = $søknad->hent($egenskap->kode);
        switch ($egenskap->type) {
            case 'bool':
            case 'boolean':
                return $verdi ? 'Ja' : 'Nei';
            case 'json':
                return self::arrayToHtmlDatalistRecursive($verdi, $egenskap->kode, $søknad);
        }
        return isset($verdi) ? strval($verdi) : null;
    }

    /**
     * Recursive array to HTML Datalist
     *
     * @param mixed $verdi
     * @param string|null $egenskap
     * @param Søknad|null $søknad
     * @return ViewInterface|string
     * @throws Exception
     */
    private static function arrayToHtmlDatalistRecursive($verdi, ?string $egenskap = null, ?Søknad $søknad = null) {
        if(is_object($verdi)) {
            $resultat = new ViewArray();
            foreach ($verdi as $egenskap => $egenskapverdi) {
                if (isset($egenskapverdi) && $egenskapverdi !== '') {
                    $dt = new HtmlElement('dt', ['class' => 'felt- ' . $egenskap . ' label'], self::hentEtikett($egenskap, $søknad));
                    $dd = new HtmlElement('dd', ['class' => 'felt- ' . $egenskap], self::arrayToHtmlDatalistRecursive($egenskapverdi, $egenskap, $søknad));
                    $resultat->addItem($dt)->addItem($dd);
                }
            }
            return new HtmlElement('dl', [],$resultat);
        }
        else if(is_array($verdi)) {
            $resultat = new ViewArray();
            $resultat->setGlue('');
            foreach ($verdi as $element) {
                $resultat->addItem(self::arrayToHtmlDatalistRecursive($element, null, $søknad));
            }
            return $resultat;
        }
        else if (is_scalar($verdi)){
            $layoutElement = $egenskap ? self::hentLayoutElement($egenskap, $søknad) : null;
            if ($layoutElement) {
                if(isset($layoutElement->element) && in_array($layoutElement->element, ['checkbox'])) {
                    return $verdi ? 'Ja' : 'Nei';
                }
            }
            return strval($verdi);
        }

        return '';
    }
}