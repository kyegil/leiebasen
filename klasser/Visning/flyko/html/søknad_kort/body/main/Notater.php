<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\søknad_kort\body\main;


use Exception;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Visning\Flyko;
use Kyegil\Leiebasen\Visning\flyko\html\søknad_kort\body\main\notater\Notat;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for søknad i flyko
 *
 *  Ressurser:
 *      søknad \Kyegil\Leiebasen\Modell\Søknad
 *  Mulige variabler:
 *      $notatListe
 *      $notatSkjema
 * @package Kyegil\Leiebasen\Visning\flyko\html\søknad_kort\body\main
 */
class Notater extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/søknad_kort/body/main/Notater.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['søknad'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $notatListe = new ViewArray();
        $datasett = [
            'notatListe' => $notatListe,
            'notatSkjema' => '',
        ];
        /** @var Søknad|null $søknad */
        $søknad = $this->hentRessurs('søknad');

        if ($søknad) {
            $datasett['notatSkjema'] = $this->app->vis(NotatSkjema::class, ['søknad' => $søknad]);
            foreach ($søknad->hentNotater() as $notat) {
                $notatListe->addItem($this->app->vis(Notat::class, ['notat' => $notat]));
            }
        }

        $this->definerData($datasett);
        return parent::forberedData();
    }
}