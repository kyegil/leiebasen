<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\søknad_kort\body\main;


use Exception;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\HtmlEditor;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for søknadskjema-oppsett i mine sider
 *
 *  Ressurser:
 *      søknad \Kyegil\Leiebasen\Modell\Søknad
 *  Mulige variabler:
 *      $skjema
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\flyko\html\søknad_kort\body\main
 */
class NotatSkjema extends Visning\Flyko
{
    /** @var string */
    protected $template = 'flyko/html/søknad_kort/body/main/NotatSkjema.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['søknad'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Søknad|null $søknad */
        $søknad = $this->hentRessurs('søknad');

        $fields = [];
        $fields[] = $this->app->vis(HtmlEditor::class, [
            'name' => 'tekst',
            'label' => 'Legg til notat'
        ]);

        $datasett = [
            'skjema' => '',
            'knapper' => new ViewArray()
        ];
        if ($søknad) {
            $datasett['skjema'] = $this->app->vis(AjaxForm::class, [
                'action' => '/flyko/index.php?oppslag=søknad_kort&oppdrag=ta_i_mot_skjema&skjema=notat&søknad=' . $søknad->hentId(),
                'formId' => 'søknad-notat',
                'buttonText' => 'Lagre',
                'fields' => $fields,
            ]);
        }
        $this->definerData($datasett);
        return parent::forberedData();
    }
}