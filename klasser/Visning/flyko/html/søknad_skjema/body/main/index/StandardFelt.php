<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\søknad_skjema_skjema\body\main\index;


use Exception;
use Kyegil\Leiebasen\Modell\Søknad\Type;
use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *  Mulige variabler:
 *  $innhold
 * @package Kyegil\Leiebasen\Visning\flyko\html\søknad_skjema_skjema\body\main\index
 */
class StandardFelt extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/søknad_skjema_skjema/body/main/index/StandardFelt.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['søknadsskjema'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Type|null $søknadSkjema */
        $søknadSkjema = $this->hentRessurs('søknadsskjema');

        $datasett = [];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}