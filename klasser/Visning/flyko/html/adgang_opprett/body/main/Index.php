<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\adgang_opprett\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Sett;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\AutoComplete;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\EmailField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\Flyko;
use Kyegil\ViewRenderer\ViewArray;
use stdClass;

/**
 * Visning for skjema for å tildele adganger til leieforholdet
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\flyko\html\adgang_opprett\body\main
 */
class Index extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/adgang_opprett/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['bruker'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $options = $this->hentOptions();
        $felter = new ViewArray([
            $this->app->vis(AutoComplete::class, [
                'id' => 'navnesøk-felt',
                'name' => 'navn',
                'options' => $options,
                'forceSelection' => false,
                'label' => 'Gi adgang til',
                'required' => true,
                'select2Configs' => (object)[
                    'allowClear' => true,
                    'placeholder' => 'Søk fram den som skal ha adgang til flyko-sidene, eller skriv inn navnet om vedkommende ikke er registrert',
                ]
            ]),
            $this->app->vis(Checkbox::class, [
                'id' => 'nyoppføring-checkbox',
                'label' => 'Jeg finner ikke vedkommende i listen over',
                'checked' => false,
            ]),
            $this->app->vis(FieldSet::class, [
                'id' => 'nyoppføring-felter',
                'label' => 'Oppgi navn og epostadresse for å registrere ny person',
                'contents' => [
                    new HtmlElement('div', [], 'Før du registrerer en ny person må du forsikre deg om at vedkommende ikke allerede er registrert. Bruk søkefeltet over, og prøv alle varianter av navnet.'),
                    $this->app->vis(Checkbox::class, [
                        'id' => 'nyoppføring-er-org',
                        'label' => 'Organisasjonsnavn',
                        'name' => 'er_organisasjon',
                        'checked' => false,
                        'onclick' => 'leiebasen.flyko.adgangOpprett.veksleMellomPersonOgOrg();'
                    ]),
                    $this->app->vis(Field::class, [
                        'id' => 'nyoppføring-fornavn-felt',
                        'label' => 'Fornavn',
                        'name' => 'fornavn',
                        'required' => true,
                    ]),
                    $this->app->vis(Field::class, [
                        'id' => 'nyoppføring-etternavn-felt',
                        'label' => 'Etternavn',
                        'name' => 'etternavn',
                        'required' => true,
                    ]),
                    $this->app->vis(Field::class, [
                        'id' => 'nyoppføring-navn-felt',
                        'label' => 'Navn',
                        'name' => 'organisasjonsnavn',
                        'required' => true,
                    ]),
                    $this->app->vis(EmailField::class, [
                        'id' => 'nyoppføring-epost-felt',
                        'label' => 'E-post',
                        'name' => 'epost',
                        'required' => true,
                    ]),
                    $this->app->vis(Field::class, [
                        'id' => 'nyoppføring-mobil-felt',
                        'label' => 'Mobiltelefon',
                        'name' => 'mobil',
                        'required' => true,
                    ]),
                    $this->app->vis(DateField::class, [
                        'id' => 'nyoppføring-fødselsdato-felt',
                        'name' => 'fødselsdato',
                        'label' => 'Fødselsdato',
                    ]),
                    $this->app->vis(Field::class, [
                        'id' => 'nyoppføring-orgnr-felt',
                        'name' => 'orgnr',
                        'label' => 'Org.nr',
                    ]),
                    $this->app->vis(Field::class, [
                        'id' => 'nyoppføring-adresse1-felt',
                        'name' => 'adresse1',
                        'label' => 'Adresse',
                    ]),
                    $this->app->vis(Field::class, [
                        'id' => 'nyoppføring-adresse2-felt',
                        'name' => 'adresse2',
                    ]),
                    $this->app->vis(Field::class, [
                        'name' => 'postnr',
                        'label' => 'Postnr',
                    ]),
                    $this->app->vis(Field::class, [
                        'name' => 'poststed',
                        'label' => 'Poststed',
                    ])
                ]
            ]),
        ]);

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/flyko/index.php?oppslag=adgang_opprett&oppdrag=ta_i_mot_skjema&skjema=navn',
                'formId' => 'adgangsnavn',
                'buttonText' => 'Registrer adgangen',
                'fields' => [
                    $felter
                ]
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    private function hentOptions(): stdClass
    {
        $result = (object)['' => ''];
        /** @var Sett $personsett */
        $personsett = $this->app->hentSamling(Person::class)
            ->leggTilSortering('fornavn')
            ->leggTilSortering('etternavn');
        /** @var Person $person */
        foreach ($personsett as $person) {
            $result->{$person->hentId()} = $person->hentNavn();
            if ($person->hentFødselsdato()) {
                $result->{$person->hentId()} .= ' f. ' . $person->hentFødselsdato()->format('d.m.Y');
            }
        }
        return $result;
    }

    /**
     * @return string[]
     * @throws Exception
     */
    private function hentPersonliste(): array
    {
        $result = [['id' => '', 'text' => '']];
        /** @var Sett $personsett */
        $personsett = $this->app->hentSamling(Person::class)
            ->leggTilSortering('fornavn')
            ->leggTilSortering('etternavn');
        /** @var Person $person */
        foreach ($personsett as $person) {
            $result[] = [
                'id' => $person->hentId(),
                'text' => $person->hentNavn()
            ];
        }
        return $result;
    }
}