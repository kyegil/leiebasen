<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\oversikt\body\main;

use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Visning for grunnleggende html-struktur i flyko
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *  Mulige variabler:
 *      $head HTML head
 *      $header
 *      $hovedmeny
 *      $main
 *      $footer
 *      $scripts Script satt inn til slutt før </body>
 * @package Kyegil\Leiebasen\Visning\flyko\html\oversikt\body\main
 */
class Index extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/oversikt/body/main/Index.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(is_string($attributt) && in_array($attributt, ['bruker'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     */
    protected function forberedData()
    {
        /** @var Person $bruker */
        $bruker = $this->hentRessurs('bruker');
        $this->definerData([
        ]);
        return parent::forberedData();
    }
}