<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\søknad_arkiv\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Visning for skadeliste i flyko
 *
 *  Ressurser:
 *      søknad_type \Kyegil\Leiebasen\Modell\Søknad\Type
 *  Mulige variabler:
 *      $filtre
 *      $tabell
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\flyko\html\søknad_arkiv\body\main
 */
class Index extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/søknad_arkiv/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return Index
     */
    public function sett($attributt, $verdi = null)
    {
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /**
         * Kolonne-objekt for DataTables
         *
         * @link https://datatables.net/reference/option/columns
         */
        $kolonner = [
            (object)[
                'data' => 'id',
                'name' => 'id',
                'title' => 'Id',
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return type=='display' ? '<a href=\"/flyko/index.php?oppslag=søknad_kort&id=' + row.id + '\">' + data + '</a>' : data;", ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'registrert',
                'name' => 'registrert',
                'title' => 'Registrert',
                'responsivePriority' => 1, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return leiebasen.flyko.søknadArkiv.renderDate(data, type, row);", ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'oppdatert',
                'name' => 'oppdatert',
                'title' => 'Oppdatert',
                'responsivePriority' => 2, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return leiebasen.flyko.søknadArkiv.renderOppdatert(data, type, row);", ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'beskrivelse',
                'name' => 'beskrivelse',
                'title' => '',
                'responsivePriority' => 3, // Høyere tall = lavere prioritet
            ],
            (object)[
                'data' => 'kategorier',
                'name' => 'kategorier',
                'title' => 'Kategorier',
                'responsivePriority' => 4, // Høyere tall = lavere prioritet
            ],
            (object)[
                'data' => 'stikkord',
                'name' => 'stikkord',
                'title' => 'Stikkord',
                'visible' => false,
            ],
        ];

        /**
         * Konfig-objekt for DataTables
         * * pageLength initiert antall per side
         * * lengthMenu
         *
         * @link https://datatables.net/reference/option/
         */
        $dataTablesConfig = (object)[
            'ajax' => (object)[
                'url' => '/flyko/index.php?oppslag=søknad_arkiv&oppdrag=hent_data&data=søknader',
            ],
            'columns' => $kolonner,
            'dataSrc' => 'data',
            'deferRender' => true,
            'order' => [[2, 'desc'],[1, 'desc']],
            'language' => (object)[
                'search' => 'Søk på navn eller stikkord',
                'zeroRecords' => 'Ingen søknader funnet'
            ],
            'pageLength' => 25,
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
        ];

        $datasett = [
            'tabell' => $this->app->vis(DataTable::class, [
                'tableId' => 'søknader',
                'caption' => 'Oversikt over arkiverte (inaktive) søknader',
                'kolonner' => $kolonner,
                'dataTablesConfig' => $dataTablesConfig
            ]),
            'knapper' => [],
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}