<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\søknad_liste\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Søknad\Type;
use Kyegil\Leiebasen\Visning\felles\html\form\AutoComplete;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\Leiebasen\Visning\Flyko;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for skadeliste i flyko
 *
 *  Ressurser:
 *      søknad_type \Kyegil\Leiebasen\Modell\Søknad\Type
 *  Mulige variabler:
 *      $filtre
 *      $tabell
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\flyko\html\søknad_liste\body\main
 */
class Index extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/søknad_liste/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return Index
     */
    public function sett($attributt, $verdi = null)
    {
        if ($attributt == 'søknad_type') {
            return $this->settSøknadType($verdi);
        }
        if (in_array($attributt, ['kategorier'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Type $type
     * @return $this
     */
    private function settSøknadType(Type $type): Index
    {
        return $this->settRessurs('søknad_type', $type);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Type|null $søknadstype */
        $søknadstype = $this->hentRessurs('søknad_type');
        /** @var array $kategorier */
        $kategorier = $this->hentRessurs('kategorier') ?? [];
        $filtre = new ViewArray();

        $filtre->addItem($this->app->vis(AutoComplete::class, [
            'id' => 'kategori-filter',
            'options' => $søknadstype ? $søknadstype->hentKategorier() : [],
            'class' => 'filter',
            'label' => 'Velg kategori (velg flere for å sile søknadene ytterligere)',
            'multiple' => true,
            'forceSelection' => true,
            'value' => $kategorier,
            'style' => 'height: 15px;',
        ]));

        $filtre->addItem(new HtmlElement('div', ['class' => 'utdyping'],
        ['Kategorier kan angis for hver enkelt søknad. På nye søknader blir enkelte kategorier påstemplet automatisk på grunnlag av valg gjort av søker, men de bør også gjennomgåes og verifiseres manuelt.']
        ));

        /**
         * Kolonne-objekt for DataTables
         *
         * @link https://datatables.net/reference/option/columns
         */
        $kolonner = [
            (object)[
                'data' => 'id',
                'name' => 'id',
                'title' => 'Id',
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return type=='display' ? '<a href=\"/flyko/index.php?oppslag=søknad_kort&id=' + row.id + '\">' + data + '</a>' : data;", ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'registrert',
                'name' => 'registrert',
                'title' => 'Registrert',
                'responsivePriority' => 1, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return leiebasen.flyko.søknadListe.renderDate(data, type, row);", ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'oppdatert',
                'name' => 'oppdatert',
                'title' => 'Oppdatert',
                'responsivePriority' => 2, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return leiebasen.flyko.søknadListe.renderOppdatert(data, type, row);", ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'beskrivelse',
                'name' => 'beskrivelse',
                'title' => '',
                'responsivePriority' => 3, // Høyere tall = lavere prioritet
            ],
            (object)[
                'data' => 'kategorier',
                'name' => 'kategorier',
                'title' => 'Kategorier',
                'responsivePriority' => 4, // Høyere tall = lavere prioritet
            ],
            (object)[
                'data' => 'stikkord',
                'name' => 'stikkord',
                'title' => 'Stikkord',
                'visible' => false,
            ],
        ];

        if ($søknadstype) {
            $typeKolonner = $søknadstype->hentKonfigurering('kolonner') ?? [];
            foreach($typeKolonner as $typeKolonne) {
                if (isset($typeKolonne->render)) {
                    $typeKolonne->render = new JsFunction("return {$typeKolonne->render}(data, type, row, meta);", ['data', 'type', 'row', 'meta']);
                }
                $kolonner[] = $typeKolonne;
            }
        }

        /**
         * Konfig-objekt for DataTables
         * * pageLength initiert antall per side
         * * lengthMenu
         *
         * @link https://datatables.net/reference/option/
         */
        $dataTablesConfig = (object)[
            'ajax' => (object)[
                'url' => '/flyko/index.php?oppslag=søknad_liste&oppdrag=hent_data&data=søknader' . ($søknadstype ? '&id=' . $søknadstype->hentId() : ''),
                'data' => new JsFunction('data.kategorier = $("#kategori-filter").val();', ['data'])
            ],
            'dataSrc' => 'data',
            'deferRender' => true,
            'order' => [[2, 'desc'],[1, 'desc']],
            'columns' => $kolonner,
            'language' => (object)[
                'search' => 'Søk på navn eller stikkord',
                'zeroRecords' => 'Ingen søknader funnet'
            ],
            'pageLength' => 25,
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
        ];

        $datasett = [
            'filtre' => $filtre,
            'tabell' => $this->app->vis(DataTable::class, [
                'tableId' => 'søknader',
                'caption' => 'Oversikt over registrerte søknader',
                'kolonner' => $kolonner,
                'dataTablesConfig' => $dataTablesConfig
            ]),
            'knapper' => [$this->hentLinkKnapp('/flyko/index.php?oppslag=søknad_skjema' . ($søknadstype ? ('&skjema=' . $søknadstype->hentId()) : '') . '&id=*', 'Registrer ny søknad', 'Registrer en innkommen søknad.')],
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}