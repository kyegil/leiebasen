<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\adgang_liste\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Modell\Person\Adgangsett;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Visning for skadeliste i flyko
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $tabell
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\flyko\html\adgang_liste\body\main
 */
class Index extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/adgang_liste/body/main/Index.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /**
         * Kolonne-objekt for DataTables
         *
         * @link https://datatables.net/reference/option/columns
         */
        $kolonner = [
            (object)[
                'data' => 'personid',
                'name' => 'personid',
                'title' => 'Id',
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
//                'width' => '40px'
            ],
            (object)[
                'data' => 'navn',
                'name' => 'navn',
                'title' => 'Navn',
                'responsivePriority' => 1, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return '<a href=\"/flyko/index.php?oppslag=adresse_kort&id=' + row.personid + '\">' + data + '</a>';", ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'epost',
                'name' => 'epost',
                'title' => 'E-post',
                'responsivePriority' => 2, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return '<a href=\"mailto:' + data + '\">' + data + '</a>';", ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'telefon',
                'name' => 'telefon',
                'title' => 'Telefon',
                'responsivePriority' => 3, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return '<a href=\"tel:' + data.replace(/\s/g, '') + '\">' + data + '</a>';", ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'personid',
                'name' => 'slett',
                'title' => 'Fjern adgang',
                'responsivePriority' => 4, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return '<button onclick=\"leiebasen.flyko.adgangListe.slettAdgang(' + data + ',\'' + row.navn + '\');\">Slett</button>';", ['data', 'type', 'row', 'meta']),
            ],
        ];

        $data = [];
        /** @var Adgangsett $adgangsett */
        $adgangsett = $this->app->hentSamling(Adgang::class)
            ->leggTilFilter(['adgang' => Adgang::ADGANG_FLYKO]);
        foreach($adgangsett as $adgang) {
            $person = $adgang->person;
            if ($person) {
                $data[] = (object)[
                    'personid' => $person->hentId(),
                    'navn' => $person->hentNavn(),
                    'epost' => $person->hentEpost(),
                    'telefon' => $person->hentMobil() ? $person->hentMobil() : $person->hentTelefon(),
                ];
            }
        }

        /**
         * Konfig-objekt for DataTables
         * * pageLength initiert antall per side
         * * lengthMenu
         *
         * @link https://datatables.net/reference/option/
         */
        $dataTablesConfig = (object)[
            'order' => [[0, 'asc']],
            'columns' => $kolonner,
            'pageLength' => 25,
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'data' => $data,
        ];

        $datasett = [
            'tabell' => $this->app->vis(DataTable::class, [
                'tableId' => 'adganger',
                'caption' => 'Oversikt over personer med adgang til Flyko',
                'kolonner' => $kolonner,
                'data' => $data,
                'dataTablesConfig' => $dataTablesConfig
            ]),
            'knapper' => [$this->hentLinkKnapp("/flyko/index.php?oppslag=adgang_opprett", 'Legg til medlem', 'Opprett adgang til flyko-området for en ny person.')],
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}