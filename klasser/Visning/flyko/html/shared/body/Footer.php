<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\shared\body;


use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Visning for footer i flyko
 *
 *  Mulige variabler:
 *      $head
 *      $menu
 * @package Kyegil\Leiebasen\Visning\flyko\html\shared
 */
class Footer extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/shared/body/Footer.html';

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $this->definerData([
            'head' => '',
            'menu' => ''
        ]);
        return parent::forberedData();
    }

}