<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\shared\body;


use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Visning for brukermeny
 *
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\flyko\html\shared
 */
class Brukermeny extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/shared/body/Brukermeny.html';

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $this->definerData([
        ]);
        return parent::forberedData();
    }

}