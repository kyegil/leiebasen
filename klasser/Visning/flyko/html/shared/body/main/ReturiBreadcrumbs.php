<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\shared\body\main;


use Kyegil\Leiebasen\Visning\Flyko;

class ReturiBreadcrumbs extends Flyko
{
    protected $template = 'flyko/html/shared/body/main/ReturiBreadcrumbs.html';
}