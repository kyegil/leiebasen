<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\shared\body;


/**
 * Visning for main content
 *
 *  Mulige variabler:
 *      $tittel
 *      $innhold
 *      $breadcrumbs
 *      $varsler
 *      $tilbakeknapp
 * @package Kyegil\Leiebasen\Visning\flyko\html\shared
 */
class Main extends \Kyegil\Leiebasen\Visning\Flyko
{
    /** @var string */
    protected $template = 'flyko/html/shared/body/Main.html';

    /**
     * @return Main
     */
    protected function forberedData()
    {
        $this->definerData([
            'tittel' => '',
            'innhold' => '',
            'breadcrumbs' => '',
            'varsler' => '',
            'tilbakeknapp' => ''
        ]);
        return parent::forberedData();
    }

}