<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\shared\body;


use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Visning for hovedmeny
 *
 *  Mulige variabler:
 *      $utleier
 * @package Kyegil\Leiebasen\Visning\flyko\html\shared
 */
class Hovedmeny extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/shared/body/Hovedmeny.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'bruker') {
            return $this->settRessurs('bruker', $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     */
    protected function forberedData()
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        $this->definerData([
            'utleier' => $this->app->hentValg('utleier')
        ]);
        return parent::forberedData();
    }

}