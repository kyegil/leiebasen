<?php

namespace Kyegil\Leiebasen\Visning\flyko\html\shared\body;


use Kyegil\Leiebasen\Visning\Flyko;

/**
 * Visning for header i Flyko-sidene
 *
 *  Mulige variabler:
 *      $språkvelger
 *      $logo
 *      $logoWidth
 *      $logoHeight
 *      $navn
 * @package Kyegil\Leiebasen\Visning\flyko\html\shared
 */
class Header extends Flyko
{
    /** @var string */
    protected $template = 'flyko/html/shared/body/Header.html';

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $this->definerData([
            'språkvelger' => '',
            'logo' => '/pub/media/bilder/offentlig/blank.png',
            'logoWidth' => 1,
            'logoHeight' => 1,
            'navn' => $this->app->hentValg('utleier')
        ]);
        return parent::forberedData();
    }

}