<?php

    namespace Kyegil\Leiebasen\Visning\flyko\html\shared;


    use Kyegil\Leiebasen\Visning\felles\html\shared\HeadInterface as HtmlHead;
    use Kyegil\Leiebasen\Visning\Flyko;

    /**
     * Visning for html head i Flyko
     *
     *  Mulige variabler:
     *      $title
     *      $extraMetaTags
     *      $links
     *      $scripts Scripts inserted to HTML Head
     * @package Kyegil\Leiebasen\Visning\flyko\html\shared
     */
    class Head extends Flyko implements HtmlHead
    {
        /** @var string */
        protected $template = 'flyko/html/shared/Head.html';

        /**
         * @return Head
         */
        protected function forberedData()
        {
            $scripts = [];

            $this->definerData([
                'title' => '',
                'extraMetaTags' => '',
                'links' => '',
                'scripts' => ''
            ]);
            return parent::forberedData();
        }

    }