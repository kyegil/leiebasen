<?php

    namespace Kyegil\Leiebasen\Visning\flyko\html\shared;


    use Kyegil\Leiebasen\Visning\felles\html\shared\HtmlInterface;
    use Kyegil\Leiebasen\Visning\Flyko;

    /**
     * Visning for grunnleggende html-struktur i Flyko
     *
     *  Mulige variabler:
     *      $head HTML head
     *      $header
     *      $hovedmeny
     *      $main
     *      $footer
     *      $scripts Script satt inn til slutt før </body>
     * @package Kyegil\Leiebasen\Visning\flyko\html\shared
     */
    class Html extends Flyko implements HtmlInterface
    {
        /** @var string */
        protected $template = 'flyko/html/shared/Html.html';

        /**
         * @return Html
         */
        protected function forberedData()
        {
            $this->definerData([
                'head' => '',
                'header' => '',
                'hovedmeny' => '',
                'main' => '',
                'footer' => '',
                'scripts' => ''
            ]);
            return parent::forberedData();
        }

    }