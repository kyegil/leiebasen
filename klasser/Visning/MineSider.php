<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 14:44
 */

namespace Kyegil\Leiebasen\Visning;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;

class MineSider extends Common
{
    /**
     * Hent link-knapp
     *
     * @param string $url Lenken
     * @param string $tekst Knapp-teksten
     * @param null $title hover-tekst
     * @param string $target Målramme
     * @return HtmlElement
     */
    public function hentLinkKnapp($url, $tekst, $title = null, $target = '_self')
    {
        return new HtmlElement(
            'a',
            [
                'href' => $url,
                'target' => $target,
                'class' => 'button',
                'title' => $title,
            ],
            $tekst
        );
    }
}