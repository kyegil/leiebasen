<?php

namespace Kyegil\Leiebasen\Visning\oppfølging\html\profil_passord_skjema;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\DisplayField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\Oppfølging;

/**
 * Visning for brukerprofil-skjema i oppfølging
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *  Mulige variabler:
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\oppfølging\html\profil_passord_skjema
 */
class Index extends Oppfølging
{
    /** @var string */
    protected $template = 'oppfølging/html/profil_passord_skjema/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['bruker'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Person $bruker */
        $bruker = $this->hentRessurs('bruker');
        $brukerProfil = $bruker ? $bruker->hentBrukerProfil() : null;

        /** @var Field $brukerIdFelt */
        $brukerIdFelt = $this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\form\Field::class, [
            'name' => 'id',
            'type' => 'hidden',
            'required' => true,
            'value' => $bruker->hentId()
        ]);

        /** @var Field $brukernavnFelt */
        $brukernavnFelt = $this->app->vis(DisplayField::class, [
            'readonly' => true,
            'name' => 'login',
            'label' => 'Brukernavn (brukes ved innlogging)',
            'value' => $brukerProfil->hentBrukernavn()
        ]);

        $endreBrukernavnLenke = new HtmlElement('a', [
            'href' => '/oppfølging/index.php?oppslag=profil_skjema',
            'title' => 'Endre brukernavnet i din profil'
        ],'Endre brukernavnet');

        /** @var Field $passord1Felt */
        $passord1Felt = $this->app->vis(Field::class, [
            'name' => 'pw1',
            'id' => 'pw1',
            'type' => 'password',
            'minlength' => Person\Brukerprofil::$passordLengde,
            'pattern' => Person\Brukerprofil::$passordRegex,
            'placeholder' => Person\Brukerprofil::$passordHint,
            'label' => 'Ønsket passord',
            'required' => true,
            'value' => null
        ]);

        /** @var Field $passord2Felt */
        $passord2Felt = $this->app->vis(Field::class, [
            'name' => 'pw2',
            'id' => 'pw2',
            'type' => 'password',
            'label' => 'Gjenta ønsket passord',
            'required' => true,
            'value' => null
        ]);

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/oppfølging/index.php?oppslag=profil_skjema&oppdrag=ta_i_mot_skjema&skjema=passord',
                'formId' => 'passord',
                'buttonText' => 'Lagre',
                'fields' => [
                    $brukerIdFelt,
                    $brukernavnFelt,
                    $endreBrukernavnLenke,
                    $passord1Felt,
                    $passord2Felt
                ]
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}