<?php

namespace Kyegil\Leiebasen\Visning\oppfølging\html;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\AutoComplete;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\CollapsibleSection;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\DisplayField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FileField;
use Kyegil\Leiebasen\Visning\felles\html\form\HtmlEditor;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\Leiebasen\Visning\felles\html\form\TextArea;
use Kyegil\Leiebasen\Visning\oppfølging\html\shared\body\main\Content;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

/** @inheritdoc  */
class NotatSkjema extends Content
{
    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null): NotatSkjema
    {
        if($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        if($attributt == 'notat') {
            return $this->settNotat($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Notat|null $notat
     * @return $this
     */
    protected function settNotat(?Notat $notat): NotatSkjema
    {
        $this->settRessurs('notat', $notat);
        return $this;
    }

    /**
     * @param Leieforhold|null $leieforhold
     * @return $this
     */
    protected function settLeieforhold(?Leieforhold $leieforhold): NotatSkjema
    {
        $this->settRessurs('leieforhold', $leieforhold);
        return $this;
    }


    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): NotatSkjema
    {
        /** @var Notat|null $notat */
        $notat = $this->hentRessurs('notat');
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        $felter = new ViewArray();

        /** @var Select $kategoriFelt */
        $kategoriFelt = $this->app->vis(Select::class, [
            'label' => 'Type',
            'name' => 'kategori',
            'id' => 'kategori_felt',
            'options' => Notat::hentKategorier(),
            'value' => $notat ? $notat->hentKategori() : null
        ]);

        /** @var TextArea $notatFelt */
        $notatFelt = $this->app->vis(TextArea::class, [
            'label' => 'Notat',
            'name' => 'notat',
            'value' => $notat ? $notat->hentNotat() : null
        ]);

        /** @var Field $datoFelt */
        $datoFelt = $this->app->vis(Field::class, [
            'name' => 'dato',
            'label' => 'Dato',
            'type' => 'date',
            'value' => $notat ? $notat->hentDato()->format('Y-m-d') : null

        ]);

        /** @var HtmlEditor $brevFelt */
        $brevFelt = $this->app->vis(HtmlEditor::class, [
            'name' => 'brevtekst',
            'label' => 'Innhold',
            'value' => $notat ? $notat->hentBrevtekst() : null
        ]);

        /** @var Select $avsenderFelt */
        $avsenderFelt = $this->app->vis(Select::class, [
            'label' => 'Gjelder hendvendelse fra',
            'name' => 'henvendelse_fra',
            'options' => (object)[
                '' => 'Ikke relevant',
                Notat::FRA_UTLEIER => $this->app->hentValg('utleier'),
                Notat::FRA_LEIETAKER => $leieforhold ? $leieforhold->hentNavn() : 'Leietaker',
                Notat::FRA_FRAMLEIER => 'Framleier',
                Notat::FRA_ANDRE => 'Andre',
            ],
            'value' => $notat ? $notat->hentHenvendelseFra() : ''
        ]);

        /** @var Checkbox $skjulForLeietakerFelt */
        $skjulForLeietakerFelt = $this->app->vis(Checkbox::class, [
            'label' => "Dette er et internt notat, som ikke skal være synlig for leietaker via 'mine sider'",
            'name' => 'skjul_for_leietaker',
            'checked' => $notat && $notat->hentSkjulForLeietaker()
        ]);

        /** @var Field $dokumentreferanseFelt */
        $dokumentreferanseFelt = $this->app->vis(Field::class, [
            'label' => 'Referanse til eksternt dokument, saksnr e.l.',
            'name' => 'dokumentreferanse',
            'value' => $notat ? $notat->hentDokumentreferanse() : null
        ]);

        /** @var Field $dokumenttypeFelt */
        $dokumenttypeFelt = $this->app->vis(AutoComplete::class, [
            'label' => 'Det eksterne dokumentet er av type (velg fra listen eller skriv inn ny type)',
            'name' => 'dokumenttype',
            'multiple' => false,
            'options' => $this->hentDokumentTyper(),
            'value' => $notat ? \Kyegil\Leiebasen\Leiebase::ucfirst($notat->hentDokumenttype()) : null
        ]);

        /** @var Field $avventOppfølgingFelt */
        $avventOppfølgingFelt = $this->app->vis(DateField::class, [
            'name' => 'avvent_oppfølging',
            'id' => 'avvent_oppfølging_felt',
            'autocomplete' => false,
            'label' => 'Vent med videre oppfølging inntil dato',
            'value' => $leieforhold && $leieforhold->hentAvventOppfølging() ? $leieforhold->hentAvventOppfølging()->format('Y-m-d') : '',
        ]);

        /** @var Checkbox $stoppOppfølgingFelt */
        $stoppOppfølgingFelt = $this->app->vis(Checkbox::class, [
            'label' => 'Stopp all oppfølging av dette leieforholdet for uoverskuelig fremtid',
            'name' => 'stopp_oppfølging',
            'id' => 'stopp_oppfølging_felt',
            'checked' => $leieforhold ? $leieforhold->hentStoppOppfølging() : false
        ]);

        /** @var Checkbox $frysLeieforholdFelt */
        $frysLeieforholdFelt = $this->app->vis(Checkbox::class, [
            'label' => 'Frys leieforholdet',
            'name' => 'frys_leieforholdet',
            'id' => 'frys_leieforholdet_felt',
            'checked' => $leieforhold ? $leieforhold->hentFrosset() : false
        ]);

        /** @var CollapsibleSection $oppfølgingsFeltsett */
        $oppfølgingsFeltsett = $this->app->vis(CollapsibleSection::class, [
            'label' => 'Valg for videre oppfølging',
            'contents' => [
                new HtmlElement(
                    'div', [],
                    "Disse feltene er ikke synlig for leietaker via 'Mine sider'.<br>"
                    . 'Feltene fungerer som en intern påminnelse om å holde tilbake videre oppfølging.<br>'
                ),
                $avventOppfølgingFelt,
                $stoppOppfølgingFelt,
                $frysLeieforholdFelt,
                new HtmlElement('div', [], 'Frysing av leieforholdet stopper all behandling fra leiebasen. Frys leieforholdet hvis det er regnskapsmessig avskrevet.'),
            ],
            'collapsed' => !$leieforhold || (!$leieforhold->stoppOppfølging && !$leieforhold->avventOppfølging && !$leieforhold->frosset)
        ]);

        $vedleggFeltsettFelter = new ViewArray();

        if ($notat && $notat->hentVedlegg()) {
            $vedleggFeltsettFelter->addItem($this->app->vis(DisplayField::class, [
                'label' => 'Vedlegg',
                'class' => 'eksisterende-vedlegg',
                'value' => $notat->hentVedleggsnavn(),
            ]));
            $vedleggFeltsettFelter->addItem(new HtmlElement('div', [
                'class' => 'button',
                'onclick' => 'leiebasen.oppfølging.notatSkjema.lastNedVedlegg()'
            ], 'Last ned'));
            $vedleggFeltsettFelter->addItem(new HtmlElement('div', [
                'class' => 'button',
                'onclick' => 'leiebasen.oppfølging.notatSkjema.slettVedlegg()'
            ], 'Slett eksisterende vedlegg'));
            $vedleggFeltsettFelter->addItem($this->app->vis(FileField::class, [
                'label' => 'Last opp vedlegg på nytt (erstatter eksisterende vedlegg)',
                'name' => 'vedlegg',
                'multiple' => false
            ]));
        }
        else {
            $vedleggFeltsettFelter->addItem($this->app->vis(FileField::class, [
                'label' => 'Last opp vedlegg',
                'name' => 'vedlegg',
                'multiple' => false
            ]));
        }

        /** @var CollapsibleSection $oppfølgingsFeltsett */
        $vedleggFeltsett = $this->app->vis(CollapsibleSection::class, [
            'label' => 'Vedlegg / innhold',
            'contents' => [
                new HtmlElement(
                    'div', [],
                    "Last opp korrespondansen som fil, eller lim innholdet inn i tekstbehandleren under.<br>"
                ),
                $vedleggFeltsettFelter,
                $brevFelt,
            ]
        ]);

        if($leieforhold) {
            $felter->addItem(new HtmlElement('div', [], 'Leieforhold ' . $leieforhold->hentId() . ' ' . $leieforhold->hentNavn()));
            $felter->addItem($this->app->vis(Field::class, [
                'name' => 'leieforhold',
                'type' => 'hidden',
                'value' => $leieforhold->hentId()
            ]));
        }
        else {
            $felter->addItem($this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\form\auto_complete\Leieforhold::class, [
                'name' => 'leieforhold',
                'required' => true,
                'forceSelection' => true,
                'multiple' => false
            ]));
        }
        $felter->addItem($skjulForLeietakerFelt);
        $felter->addItem($kategoriFelt);
        $felter->addItem($datoFelt);
        $felter->addItem($avsenderFelt);
        $felter->addItem($notatFelt);
        $felter->addItem($dokumentreferanseFelt);
        $felter->addItem($dokumenttypeFelt);
        $felter->addItem($vedleggFeltsett);
        $felter->addItem($oppfølgingsFeltsett);

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [];
        $buttons[] = new HtmlElement('button', ['role'=> 'button', 'type' => 'submit', 'form'=> 'notatskjema'], 'Lagre');

        $datasett = [
            'mainBodyContents' => $this->app->vis(AjaxForm::class, [
                'action' => $this->app::url('notat_skjema', ($notat ? $notat->hentId() : '*'), 'mottak', ['skjema' => 'notat']),
                'formId' => 'notatskjema',
                'fields' => $felter,
                'buttons' => ''
            ]),
            'buttons' => $buttons,
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }

    /**
     * @return array
     * @throws Exception
     */
    private function hentDokumentTyper(): array
    {
        $dokumentTyper = [''];
        $tp = $this->app->mysqli->table_prefix;
        foreach($this->app->mysqli->arrayData([
            'distinct' => true,
            'flat' => true,
            'source' => $tp . Notat::hentTabell(),
            'fields' => ['dokumenttype']
        ])->data as $type) {
            if (trim($type)) {
                $dokumentTyper[] = \Kyegil\Leiebasen\Leiebase::ucfirst(trim($type));
            }
        }
        return $dokumentTyper;
    }
}