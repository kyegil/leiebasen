<?php

namespace Kyegil\Leiebasen\Visning\oppfølging\html;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Modell\Person\Adgangsett;
use Kyegil\Leiebasen\Oppslag\OppfølgingKontroller;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\Leiebasen\Visning\Oppfølging;
use Kyegil\Leiebasen\Visning\oppfølging\html\shared\body\main\Content;
use Kyegil\ViewRenderer\ViewArray;

/**
 * @inheritdoc
 */
class AdgangListe extends Content
{
    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): AdgangListe
    {
        /**
         * Kolonne-objekt for DataTables
         *
         * @link https://datatables.net/reference/option/columns
         */
        $kolonner = [
            (object)[
                'data' => 'personid',
                'name' => 'personid',
                'title' => 'Id',
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
//                'width' => '40px'
            ],
            (object)[
                'data' => 'navn',
                'name' => 'navn',
                'title' => 'Navn',
                'responsivePriority' => 1, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return '<a href=\"/drift/index.php?oppslag=personadresser_kort&id=' + row.personid + '\">' + data + '</a>';", ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'epost',
                'name' => 'epost',
                'title' => 'E-post',
                'responsivePriority' => 2, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return '<a href=\"mailto:' + data + '\">' + data + '</a>';", ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'telefon',
                'name' => 'telefon',
                'title' => 'Telefon',
                'responsivePriority' => 3, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return '<a href=\"tel:' + data.replace(/\s/g, '') + '\">' + data + '</a>';", ['data', 'type', 'row', 'meta']),
            ],
            (object)[
                'data' => 'personid',
                'name' => 'slett',
                'title' => 'Fjern adgang',
                'responsivePriority' => 4, // Høyere tall = lavere prioritet
                'render' => new JsFunction("return '<button onclick=\"leiebasen.oppfølging.adgangListe.slettAdgang(' + data + ',\'' + row.navn + '\');\">Slett</button>';", ['data', 'type', 'row', 'meta']),
            ],
        ];

        $data = [];
        /** @var Adgangsett $adgangsett */
        $adgangsett = $this->app->hentSamling(Adgang::class)
            ->leggTilFilter(['adgang' => Adgang::ADGANG_OPPFØLGING]);
        foreach($adgangsett as $adgang) {
            $person = $adgang->person;
            if ($person) {
                $data[] = (object)[
                    'personid' => $person->hentId(),
                    'navn' => $person->hentNavn(),
                    'epost' => $person->hentEpost(),
                    'telefon' => $person->hentMobil() ? $person->hentMobil() : $person->hentTelefon(),
                ];
            }
        }

        $knapper = new ViewArray([$this->hentLinkKnapp(OppfølgingKontroller::url('adgang_opprett'), 'Tildel adgang for andre', 'Opprett adgang til oppfølging-området for en ny person.')]);

        /**
         * Konfig-objekt for DataTables
         * * pageLength initiert antall per side
         * * lengthMenu
         *
         * @link https://datatables.net/reference/option/
         */
        $dataTablesConfig = (object)[
            'fixedHeader' => (object)[
                'footer' => true,
            ],
            'layout' => (object)[
                'topStart' => null,
                'topEnd' => null,
                'bottomStart' => null,
                'bottomEnd' => null,
                'top' => [
                    'pageLength',
                    (object)['div' => (object)['html' => '<h3>' . $this->app->tittel . '</h3>']],
                    'search'
                ],
                'bottom' => [
                    'info',
                    'paging',
                    (object)['div' => (object)['html' => (string)$knapper]],

                ]
            ],
            'order' => [[0, 'asc']],
            'columns' => $kolonner,
            'pageLength' => 300,
            'pagingType'=> 'simple_numbers',
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'scrollCollapse' => true, // Collapse when no content
            'scrollY' => 'calc(100vh - 320px)', // Table height
            'data' => $data,

        ];

        $datasett = [
            'heading' => '',
            'mainBodyContents' => $this->app->vis(DataTable::class, [
                'tableId' => 'adganger',
                'caption' => 'Oversikt over personer med adgang til Oppfølging',
                'kolonner' => $kolonner,
                'data' => $data,
                'dataTablesConfig' => $dataTablesConfig
            ]),
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }
}