<?php

namespace Kyegil\Leiebasen\Visning\oppfølging\html;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\html\form\TextArea;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Adressefelt;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\Leiebasen\Visning\oppfølging\html\shared\body\main\Content;
use Kyegil\ViewRenderer\ViewArray;

/**
 * @inheritdoc
 */
class NotatListe extends Content
{
    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Leieforhold $leieforhold
     * @return $this
     * @throws \Exception
     */
    protected function settLeieforhold($leieforhold): NotatListe
    {
        $this->settRessurs('leieforhold', $leieforhold);
        return $this;
    }


    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): NotatListe
    {
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        /**
         * Kolonne-objekt for DataTables
         *
         * @link https://datatables.net/reference/option/columns
         */
        $kolonner = [
            (object)[
                'data' => 'dato',
                'name' => 'dato',
                'title' => 'Dato',
                'responsivePriority' => 1,
                'render' => new JsFunction(
                    "if (type == 'display') {return data.split('-').reverse().join('.');} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
                'width' => '40px'
            ],
            (object)[
                'data' => 'kategori',
                'name' => 'kategori',
                'title' => '',
                'responsivePriority' => 2,
                'render' => new JsFunction(
                    "if (type == 'display') {return '<a title=\"Detaljer\" href=\"/oppfølging/index.php?oppslag=notat&id=' + row.id + '\">' + data + '</a>';} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
                'width' => '40px'
            ],
        ];
        if (!$leieforhold) {
            $kolonner[] =             (object)[
                'data' => 'leieforhold',
                'name' => 'leieforhold',
                'title' => 'Leieforhold',
                'responsivePriority' => 2,
                'render' => new JsFunction(
                    "if (type == 'display') {return '<a title=\"Filtrer på leieforholdet\" href=\"/oppfølging/index.php?oppslag=oversikt&id=' + data + '\">' + data + '</a>' + ': ' + row.leieforhold_beskrivelse;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ];
        }
        $kolonner[] = (object)[
            'data' => 'notat',
            'name' => 'notat',
            'title' => 'Notat',
        ];

        $leieforholdOppsummering = null;
        $leieforholdVelger = null;
        if (!$leieforhold) {
            $leieforholdVelger = $this->app->hentVisning(\Kyegil\Leiebasen\Visning\felles\html\form\auto_complete\Leieforhold::class, [
                'id' => 'leieforhold-velger',
                'forceSelection' => true,
                'multiple' => false,
            ]);
        }
        else {
            $leieforholdKolonne1 = new ViewArray();
            $leieforholdKolonne1->addItem(new HtmlElement('p', [], 'Leieforholdet påbegynt ' . $leieforhold->fradato->format('d.m.Y')));
            if ($leieforhold->hentOppsigelse()) {
                $leieforholdKolonne1->addItem(new HtmlElement('p', [], 'Avsluttet ' . $leieforhold->hentOppsigelse()->fristillelsesdato->format('d.m.Y')));
            }
            $leieforholdKolonne1->addItem(new HtmlElement('p', [], 'Utestående: ' . $this->app->kr($leieforhold->hentUtestående())));

            if ($leieforhold->stoppOppfølging) {
                $leieforholdKolonne1->addItem(
                    new HtmlElement('p', [],
                        new HtmlElement('strong', [],
                            'Videre oppfølging av leieforholdet har opphørt'
                        )
                    )
                );
            }
            else if ($leieforhold->avventOppfølging) {
                $leieforholdKolonne1->addItem(
                    new HtmlElement('p', [],
                        'Videre oppfølging av leieforholdet avventes inntil ' . new HtmlElement('strong', [],
                            $leieforhold->hentAvventOppfølging()->format('d.m.Y')
                        )
                    )
                );
            }
            if ($leieforhold->frosset) {
                $leieforholdKolonne1->addItem(
                    new HtmlElement('p', [],
                        new HtmlElement('strong', [],
                            'Leieforholdet er frosset'
                        )
                    )
                );
            }

            foreach($this->app->hentPoengbestyrer()->hentAktiveProgram() as $poengprogram) {
                $leieforholdKolonne1->addItem(new HtmlElement('div', [], [
                    new HtmlElement('strong', [], $poengprogram->navn),
                    ': ' . $this->app->hentPoengbestyrer()->hentPoengSum($poengprogram, $leieforhold)
                ]));
            }

            $leieforholdKolonne2 = new ViewArray();
            if ($leieforhold->hentRegningTilObjekt()) {
                $leieforholdKolonne2->addItem(new HtmlElement('p', [], 'Purringer og henvendelser leveres på døra til ' . $leieforhold->regningsobjekt->hentBeskrivelse()));
            }
            else {
                $leieforholdKolonne2->addItem(new HtmlElement('p', [], 'Purringer og henvendelser sendes til ' . $this->app->hentVisning(Adressefelt::class, ['leieforhold' => $leieforhold])));
            }

            $leieforholdLenke = new HtmlElement('a', [
                'title' => 'Klikk her for å vise leieforholdet i Drift',
                'href' => '/drift/index.php?oppslag=leieforholdkort&id=' . $leieforhold->hentId()
            ], $leieforhold->hentBeskrivelse());
            $leieforholdOppsummering = $this->app->vis(FieldSet::class, [
                'class' => $leieforhold && $leieforhold->frosset ? 'frosset' : null,
                'label' => $leieforholdLenke,
                'contents' => [$leieforholdKolonne1, $leieforholdKolonne2]
            ]);

        }

        $knapper = new ViewArray([
            $this->hentLinkKnapp(
                '/oppfølging/index.php?oppslag=notat_skjema' . ($leieforhold ? '&leieforhold=' . $leieforhold->hentId() : '') . '&id=*',
                'Registrer nytt notat',
                'Registrer notat om en ny henvendelse eller avtale.')
        ]);

        if ($leieforhold) {
            $knapper->addItem($this->hentLinkKnapp(
                '/drift/index.php?oppslag=leieforholdkort&id=' . $leieforhold->hentId() ,
                'Vis Leieforholdet i Drift',
                'Vis Leieforhold ' . $leieforhold->hentId() . ' i Drift'));
        }

        /**
         * Konfig-objekt for DataTables
         * * pageLength initiert antall per side
         * * lengthMenu
         *
         * @link https://datatables.net/reference/option/
         */
        $dataTablesConfig = (object)[
            'fixedHeader' => (object)[
                'footer' => true,
            ],
            'layout' => (object)[
                'topStart' => null,
                'topEnd' => null,
                'bottomStart' => null,
                'bottomEnd' => null,
                'top' => [
                    'pageLength',
                    (object)['div' => (object)['html' => '<h3>' . $this->app->tittel . '</h3>']],
                    'search'
                ],
                'bottom' => [
                    'info',
                    'paging',
                    (object)['div' => (object)['html' => (string)$knapper]],

                ]
            ],
            'order' => [[0, 'desc']],
            'columns' => $kolonner,
            'pageLength' => 300,
            'pagingType'=> 'simple_numbers',
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'scrollCollapse' => true, // Collapse when no content
            'scrollY' => 'calc(100vh - 320px)', // Table height
            'createdRow' => new JsFunction('console.log(data.status);if(data.status) {$(row).addClass(data.status);} if(data.frosset) {$(row).addClass(\'frosset\');}', ['row', 'data', 'dataIndex']),
            'search' => (object)[
                'return' => true,
            ],
            'searchDelay' => 1000,
            'serverSide' => true,
            'processing' => true,
            'ajax' => (object)[
                'url' => '/oppfølging/index.php?oppslag=oversikt&oppdrag=hent_data&data=notater' . ($leieforhold ? ('&id=' . $leieforhold->id) : ''),
            ]
        ];

        $mainBodyContents = new ViewArray([
            $this->hentVarsler($leieforhold),
            $leieforholdOppsummering,
            $leieforholdVelger,
            $this->app->vis(DataTable::class, [
                'tableId' => 'notater_liste',
                'kolonner' => $kolonner,
                'dataTablesConfig' => $dataTablesConfig
            ]),
        ]);

        $datasett = [
            'heading' => '',
            'mainBodyContents' => $mainBodyContents,
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }

    /**
     *
     */
    private function hentVarsler(Leieforhold $filterLeieforhold = null)
    {
        $varsler = new ViewArray();
        $this->hentVarslerOmGjenopptaking($varsler, $filterLeieforhold)
            ->hentVarslerOmBetalingsplanforslag($varsler, $filterLeieforhold)
            ->hentVarslerOmUtsettelsesforespørsler($varsler, $filterLeieforhold);

        return $varsler;
    }

    /**
     * Hent varsler for leieforhold hvor oppfølging kan gjenopptaes
     *
     * @param ViewArray $varsler
     * @param Leieforhold|null $filterLeieforhold
     * @return NotatListe
     * @throws Exception
     */
    private function hentVarslerOmGjenopptaking(ViewArray $varsler, ?Leieforhold $filterLeieforhold): NotatListe
    {
        /** @var Leieforholdsett $gjenopptakinger */
        $gjenopptakinger = $this->app->hentSamling(Leieforhold::class)
            ->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`avvent_oppfølging` IS NOT NULL'])
            ->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`avvent_oppfølging` < NOW()'])
            ->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`stopp_oppfølging`' => 0])
            ->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`frosset`' => 0])
            ->leggTilSortering('avvent_oppfølging');
        if ($filterLeieforhold) {
            $gjenopptakinger->leggTilFilter([Leieforhold::hentPrimærnøkkelfelt() => $filterLeieforhold->hentId()]);
        }

        foreach($gjenopptakinger as $leieforhold) {
            $tekst = new HtmlElement('div', [], 'Oppfølging av ' . new HtmlElement(
                    'a',
                    [
                        'href' => '/oppfølging/index.php?oppslag=oversikt&id=' . $leieforhold->hentId(),
                        'title' => 'Se notater for leieforhold ' . $leieforhold->hentId()
                    ],
                    Leiebase::genitivApostrof($leieforhold->hentNavn()) . ' leieforhold nr. ' . $leieforhold->hentId())
                . ' kan gjenopptas');

            $knapp = new HtmlElement(
                'div',
                [
                    'class' => 'button',
                    'onclick' => 'leiebasen.oppfølging.notatListe.nullstillAvventing(' . $leieforhold->hentId() . ');'
                ],
                'Oppfattet'
            );

            $varsel = new HtmlElement(
                'div', [
                    'class' => 'alert alert-info gjenopptaking gjenopptaking-' . $leieforhold->hentId()
            ],
            [$tekst, $knapp]
            );
            $varsler->addItem($varsel);
        }
        return $this;
    }

    /**
     * Hent varsler om forslag til betalingsplan som krever behandling
     *
     * @param ViewArray $varsler
     * @param Leieforhold|null $filterLeieforhold
     * @return NotatListe
     * @throws Exception
     */
    private function hentVarslerOmBetalingsplanforslag(ViewArray $varsler, ?Leieforhold $filterLeieforhold): NotatListe
    {
        /** @var Leieforholdsett $leieforholdsett */
        $leieforholdsett = $this->app->hentSamling(Leieforhold::class);
        $leieforholdsett->leggTilEavVerdiJoin('betalingsplan_forslag', true);

        if ($filterLeieforhold) {
            $leieforholdsett->leggTilFilter([Leieforhold::hentPrimærnøkkelfelt() => $filterLeieforhold->hentId()]);
        }

        foreach ($leieforholdsett as $leieforhold) {
            /** @var \stdClass $betalingsplanForslag */
            $betalingsplanForslag = $leieforhold->hentBetalingsplanForslag();
            $tekst = new HtmlElement('div', [], 'Det foreligger et ubehandlet ' . new HtmlElement(
                    'a',
                    [
                        'href' => '/oppfølging/index.php?oppslag=notat&id=' . $betalingsplanForslag->notat_id,
                        'title' => 'Vis foreslått betalingsplan ' . $betalingsplanForslag->notat_id
                    ],
                    'forslag til betalingsplan')
                . ' for leieforhold ' . $leieforhold->hentId() . '. Planen må godkjennes for å aktiveres, eller avvises.');

            $knapp = new HtmlElement(
                'a',
                [
                    'class' => 'button',
                    'href' => '/oppfølging/index.php?oppslag=notat&id=' . $betalingsplanForslag->notat_id,
                    'title' => 'Vis foreslått betalingsplan ' . $betalingsplanForslag->notat_id
                ],
                'Vis betalingsplanen'
            );

            $varsel = new HtmlElement(
                'div', [
                'class' => 'varsel varsel-nivå2 betalingsplan-forslag betalingsplan-forslag-' . $leieforhold->hentId()
            ],
                [$tekst, $knapp]
            );
            $varsler->addItem($varsel);
        }
        return $this;
    }

    /**
     * Hent varsler om forespørsler om betalingsutsettelse
     *
     * @param ViewArray $varsler
     * @param Leieforhold|null $filterLeieforhold
     * @return NotatListe
     * @throws Exception
     */
    private function hentVarslerOmUtsettelsesforespørsler(
        ViewArray $varsler,
        ?Leieforhold $filterLeieforhold
    ): NotatListe
    {
        /** @var Leieforhold\Regningsett $regningsett */
        $regningsett = $this->app->hentSamling(Leieforhold\Regning::class);
        $regningsett->leggTilEavVerdiJoin('utsettelse_forespørsel', true);

        if ($filterLeieforhold) {
            $regningsett->leggTilFilter(['leieforhold' => $filterLeieforhold->hentId()]);
        }

        foreach ($regningsett as $regning) {
            /** @var \stdClass $utsettelseForespørsel */
            $utsettelseForespørsel = $regning->hentUtsettelseForespørsel();
            $ønsketForfall = new \DateTimeImmutable($utsettelseForespørsel->ønsket_forfall);
            $tekst = new HtmlElement('div', [], "{$regning->leieforhold->hentNavn()} har " . new HtmlElement(
                    'a',
                    [
                        'href' => '/oppfølging/index.php?oppslag=notat&id=' . $utsettelseForespørsel->notat_id,
                        'title' => 'Vis henvendelse ' . $utsettelseForespørsel->notat_id
                    ],
                    'bedt om betalingsutsettelse')
                . ' for regning ' . $regning->hentId() . ' til ' . $ønsketForfall->format('d.m.Y'));


            $avvisKnapp = new HtmlElement(
                'button',
                [
                    'name' => 'beslutning',
                    'type' => 'submit',
                    'value' => 'avvist'
                ],
                'Avvis'
            );

            $godkjennKnapp = new HtmlElement(
                'button',
                [
                    'name' => 'beslutning',
                    'type' => 'submit',
                    'value' => 'godkjent'
                ],
                'Godkjenn'
            );

            $skjema = $this->app->vis(AjaxForm::class, [
                'formId' => 'godkjenn-betalingsutsettelse-' . $regning->hentId(),
                'action' => '/oppfølging/index.php?oppslag=notat_liste&oppdrag=oppgave&oppgave=behandle_betalingsutsettelse',
                'fields' => [
                    new HtmlElement('input', [
                        'type' => 'hidden', 'name' => 'regning', 'value' => $regning->hentId()
                    ]),
                    $this->app->vis(TextArea::class, [
                        'label' => 'Beslutning',
                        'name' => 'melding',
                    ]),
                ],
                'buttons' => [
                    $avvisKnapp,
                    $godkjennKnapp
                ]
            ]);

            $godkjennSkjema = $this->app->vis(AjaxForm::class, [
                'formId' => 'godkjenn-betalingsutsettelse-' . $regning->hentId(),
                'action' => '/oppfølging/index.php?oppslag=notat_liste&oppdrag=oppgave&oppgave=godkjennBetalingsutsettelse',
                'items' => [new HtmlElement('input', [
                    'type' => 'hidden', 'name' => 'regning', 'value' => $regning->hentId()
                ])],
                'saveButtonText' => 'Godkjenn'
            ]);

            $varsel = new HtmlElement(
                'div', [
                'class' => 'varsel varsel-nivå2 betalingsutsettelse-forespørsel betalingsutsettelse-forespørsel-' . $regning->leieforhold->hentId()
            ],
                [$tekst, $skjema]
            );
            $varsler->addItem($varsel);
        }
        return $this;
    }
}