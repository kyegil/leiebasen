<?php

namespace Kyegil\Leiebasen\Visning\oppfølging\html\adgang_opprett;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Sett;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\AutoComplete;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\EmailField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\Oppfølging;
use Kyegil\ViewRenderer\ViewArray;
use stdClass;

/**
 * Visning for skjema for å tildele adganger til leieforholdet
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\oppfølging\html\adgang_opprett
 */
class Index extends Oppfølging
{
    /** @var string */
    protected $template = 'oppfølging/html/adgang_opprett/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['bruker'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $options = $this->hentOptions();
        $felter = new ViewArray([
            $this->app->vis(AutoComplete::class, [
                'id' => 'navnesøk-felt',
                'name' => 'navn',
                'options' => $options,
                'forceSelection' => false,
                'label' => 'Gi adgang til (vedkommende må allered ha adgang til drift-sidene)',
                'required' => true,
                'select2Configs' => (object)[
                    'allowClear' => true,
                    'placeholder' => 'Søk fram den som skal ha adgang til oppfølging-sidene.',
                ]
            ]),
        ]);

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/oppfølging/index.php?oppslag=adgang_opprett&oppdrag=ta_i_mot_skjema&skjema=navn',
                'formId' => 'adgangsnavn',
                'buttonText' => 'Registrer adgangen',
                'fields' => [
                    $felter
                ]
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    private function hentOptions(): stdClass
    {
        $result = (object)['' => ''];
        /** @var Sett $personsett */
        $personsett = $this->app->hentSamling(Person::class)
            ->leggTilInnerJoin(
                Person\Adgang::hentTabell(),
                '`' . Person::hentTabell() . '`.`' . Person::hentPrimærnøkkelfelt()
                . '` = `' . Person\Adgang::hentTabell() . '`.`personid`'
            )
            ->leggTilFilter(['`' . Person\Adgang::hentTabell() . '`.`adgang`' => Person\Adgang::ADGANG_DRIFT])
            ->leggTilSortering('fornavn')
            ->leggTilSortering('etternavn');
        /** @var Person $person */
        foreach ($personsett as $person) {
            $result->{$person->hentId()} = $person->hentNavn();
            if ($person->hentFødselsdato()) {
                $result->{$person->hentId()} .= ' f. ' . $person->hentFødselsdato()->format('d.m.Y');
            }
        }
        return $result;
    }
}