<?php

namespace Kyegil\Leiebasen\Visning\oppfølging\html\betalingsplan_kort;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Visning\felles\html\form\CollapsibleSection;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

/**
 * Visning for betalingsplan i oppfølging
 *
 * Ressurser:
 * *   betalingsplan \Kyegil\Leiebasen\Modell\Leieforhold\Notat
 * *   notat \Kyegil\Leiebasen\Modell\Leieforhold\Notat
 *
 * Tilgjengelige variabler:
 * * $notatId
 * * $aktivitet
 * * $dato
 * * $kravoversikt
 * * $avdragsoversikt
 * * $notatTekst
 * * $registrert
 * * $skjulForLeietaker
 * * $avtaleTekst
 * * $eksterntDokument
 * * $vedlegg
 * * $knapper
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\betalingsplan_kort
 */
class Index extends \Kyegil\Leiebasen\Visning\oppfølging\html\notat\Index
{
    /** @var string */
    protected $template = 'oppfølging/html/betalingsplan_kort/Index.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'betalingsplan') {
            return $this->settBetalingsplan($verdi);
        }
        if($attributt == 'notat') {
            return $this->settNotat($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Betalingsplan|null $betalingsplan
     * @return $this
     */
    protected function settBetalingsplan(?Betalingsplan $betalingsplan): Index
    {
        $this->settRessurs('betalingsplan', $betalingsplan);
        return $this;
    }

    /**
     * @param Notat|null $notat
     * @return $this
     */
    protected function settNotat(?Notat $notat): Index
    {
        $this->settRessurs('notat', $notat);
        return $this;
    }

    /**
     * @param Leieforhold|null $leieforhold
     * @return $this
     */
    protected function settLeieforhold(?Leieforhold $leieforhold): Index
    {
        $this->settRessurs('leieforhold', $leieforhold);
        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Betalingsplan|null $betalingsplan */
        $betalingsplan = $this->hentRessurs('betalingsplan');
        /** @var Notat|null $notat */
        $notat = $betalingsplan ? $betalingsplan->hentNotat() : $this->hentRessurs('notat');
        if(
            $notat && !$betalingsplan
            && $notat->leieforhold->hentBetalingsplan()
            && $notat->leieforhold->hentBetalingsplan()->hentNotat()
            && $notat->leieforhold->hentBetalingsplan()->hentNotat()->hentId() == $notat->hentId()
        ) {
            $this->settBetalingsplan($notat->leieforhold->hentBetalingsplan());
        }
        $knapper = [];

        if ($notat) {
            $knapper[] = $this->hentLinkKnapp('/oppfølging/index.php?oppslag=betalingsplan_skjema&id=' . $notat->hentId(), 'Redigér avtalen', 'Endre');
            $this->definerData([
                'avdragsoversikt' => $this->hentAvdragsoversikt(),
                'kravoversikt' => $this->hentKravoversikt(),
                'avtaleTekst' => $this->app->vis(CollapsibleSection::class, [
                    'label' => 'Betalingsavtale',
                    'contents' => $this->hentAvtaletekst()
                ]),
            ]);
        }

        $this->definerData([
            'kravoversikt' => '',
            'avtaleTekst' => '',
            'avdragsoversikt' => '',
        ]);
        parent::forberedData();
        $knapper = $this->hent('knapper');
        if ($knapper instanceof ViewArray) {
            foreach ($knapper->getItems() as &$knapp) {
                if($knapp instanceof HtmlElement) {
                    $knapptekst = $knapp->getContents()[0] ?? '';
                    if ($knapptekst == 'Skriv ut brevet') {
                        $knapp->setContents(['Skriv ut avtalen']);
                    }
                }
            }
        }
        return $this;
    }

    private function hentAvtaletekst()
    {
        /** @var Notat $notat */
        $notat = $this->hentRessurs('notat');
        $avtaletekst = $notat->hentBrevtekst();
        $leieforhold = $notat->leieforhold;
        $plan = $leieforhold->hentBetalingsplan();
        if ($plan) {
            return $plan->visUtfyltAvtaletekst($avtaletekst);
        }

        return $avtaletekst;
    }

    /**
     * @return ViewInterface|string
     * @throws Exception
     */
    private function hentAvdragsoversikt()
    {
        /** @var Betalingsplan|null $betalingsplan */
        $betalingsplan = $this->hentRessurs('betalingsplan');
        /** @var Notat|null $notat */
        $notat = $this->hentRessurs('notat');
        $dataTablesConfig = (object)[
            'searching' => false,
            'info' => false,
            'ordering' => false,
            'scrollY' => '200px',
            'paging' => false,
        ];

        if($betalingsplan) {
            $dataTablesConfig->columns = [
                (object)[
                    'data' => 'dato',
                    'name' => 'dato',
                    'title' => 'Dato',
                    'responsivePriority' => 1,
                    'width' => '60px'
                ],
                (object)[
                    'data' => 'beløp',
                    'name' => 'beløp',
                    'title' => 'Beløp',
                    'footer' => 'Avtalt nedbetaling: ' . $this->app->kr($betalingsplan->startgjeld),
                    'className' => 'dt-right',
                    'responsivePriority' => 1, // Høyere tall = lavere prioritet
                ],
                (object)[
                    'data' => 'utestående',
                    'name' => 'beløp',
                    'title' => 'Ubetalt',
                    'footer' => 'Resterende beløp: ' . $this->app->kr($betalingsplan->hentUtestående()),
                    'className' => 'dt-right',
                    'responsivePriority' => 2, // Høyere tall = lavere prioritet
                ],
                (object)[
                    'data' => 'etterlyst',
                    'name' => 'etterlyst',
                    'title' => '',
                    'responsivePriority' => 3,
                    'width' => '130px'
                ],
                (object)[
                    'data' => 'avtalegiro',
                    'name' => 'avtalegiro',
                    'title' => '',
                    'responsivePriority' => 3,
                    'width' => '130px'
                ],
            ];

            $avdragsett = $betalingsplan->hentAvdragsett();
            $tabellData = [];
            if($avdragsett->hentAntall()) {
                foreach ($avdragsett as $avdrag) {
                    $tabellData[] = (object)[
                        'dato' => $avdrag->hentForfallsdato()->format('d.m.Y'),
                        'beløp' => $this->app->kr($avdrag->hentBeløp()),
                        'utestående' => $this->app->kr($avdrag->hentUtestående()),
                        'etterlyst' => $avdrag->hentEtterlysningSendt() ? 'Etterlyst ' . $avdrag->hentEtterlysningSendt()->format('d.m.Y') : null,
                        'avtalegiro' => $avdrag->hentAvtalegiroTrekk() ? 'AvtaleGiro' : '',
                    ];
                }
            }
            $tabell = $this->app->vis(DataTable::class, [
                'dataTablesConfig' => $dataTablesConfig,
                'caption' => 'Avtalte avdrag',
                'data' => $tabellData,
            ]);
            return $tabell;
        }
        else if ($notat
            && ($betalingsplanForslag = $notat->leieforhold->hentBetalingsplanForslag())
            && $betalingsplanForslag->notat_id == $notat->hentId()
        ) {
            $dataTablesConfig->columns = [
                (object)[
                    'data' => 'dato',
                    'name' => 'dato',
                    'title' => 'Dato',
                    'responsivePriority' => 1,
                    'width' => '130px'
                ],
                (object)[
                    'data' => 'beløp',
                    'name' => 'beløp',
                    'title' => 'Beløp',
                    'className' => 'dt-right',
                    'responsivePriority' => 1, // Høyere tall = lavere prioritet
                ],
             ];

            $tabellData = [];
            foreach ($betalingsplanForslag->avdrag ?? [] as $dato => $beløp) {
                $dato = date_create_immutable($dato);
                $tabellData[] = (object)[
                    'dato' => $dato->format('d.m.Y'),
                    'beløp' => $this->app->kr($beløp),
                ];
            }
            $tabell = $this->app->vis(DataTable::class, [
                'dataTablesConfig' => $dataTablesConfig,
                'caption' => 'Foreslåtte avdrag',
                'data' => $tabellData,
            ]);
            return $tabell;
        }
        return '';
    }

    /**
     * @return ViewInterface|string
     * @throws Exception
     */
    private function hentKravoversikt()
    {
        /** @var Betalingsplan|null $betalingsplan */
        $betalingsplan = $this->hentRessurs('betalingsplan');
        /** @var Notat|null $notat */
        $notat = $this->hentRessurs('notat');
        $dataTablesConfig = (object)[
            'searching' => false,
            'info' => false,
            'ordering' => false,
            'scrollY' => '200px',
            'paging' => false,
        ];

        $dataTablesConfig->columns = [
            (object)[
                'data' => 'dato',
                'name' => 'dato',
                'title' => 'Forfalt',
                'responsivePriority' => 1,
                'width' => '130px'                ],
            (object)[
                'data' => 'tekst',
                'name' => 'tekst',
                'title' => 'Hva',
                'className' => 'dt-right',
                'responsivePriority' => 2, // Høyere tall = lavere prioritet
            ],
            (object)[
                'data' => 'beløp',
                'name' => 'beløp',
                'title' => 'Opprinnelig beløp',
                'className' => 'dt-right',
                'responsivePriority' => 1, // Høyere tall = lavere prioritet
            ],
            (object)[
                'data' => 'utestående',
                'name' => 'utestående',
                'title' => 'Utestående',
                'className' => 'dt-right',
                'responsivePriority' => 1, // Høyere tall = lavere prioritet
            ],
        ];

        if($betalingsplan) {
            $originalKravsett = $betalingsplan->hentOriginalkravsett()->leggTilKravModell();
            $tabellData = [];
            $sumBeløp = 0;
            $sumUtestående = 0;
            foreach ($originalKravsett as $originalKrav) {
                $tabellData[] = (object)[
                    'dato' => $originalKrav->krav->hentForfall() ? $originalKrav->krav->hentForfall()->format('d.m.Y') : '',
                    'tekst' => $originalKrav->krav->hentTekst(),
                    'beløp' => $this->app->kr($originalKrav->hentUteståendeGrunnlag()),
                    'utestående' => $this->app->kr($originalKrav->krav->hentUtestående()),
                ];
                $sumBeløp += $originalKrav->hentUteståendeGrunnlag();
                $sumUtestående += $originalKrav->krav->hentUtestående();
            }
            $dataTablesConfig->columns[2]->footer = $this->app->kr($sumBeløp);
            $dataTablesConfig->columns[3]->footer = $this->app->kr($sumUtestående);
            $tabell = $this->app->vis(DataTable::class, [
                'dataTablesConfig' => $dataTablesConfig,
                'caption' => 'Gjenværende gjeldsgrunnlag',
                'data' => $tabellData,
            ]);
            return $tabell;
        }
        else if ($notat
            && ($betalingsplanForslag = $notat->leieforhold->hentBetalingsplanForslag())
            && $betalingsplanForslag->notat_id == $notat->hentId()
        ) {
            $tabellData = [];
            $kravsett = $notat->leieforhold->hentKrav()
                ->filtrerEtterIdNumre($betalingsplanForslag->originalkrav ?? [])
                ->låsFiltre();
            $sumUtestående = 0;
            foreach ($kravsett as $krav) {
                $tabellData[] = (object)[
                    'dato' => $krav->hentForfall() ? $krav->hentForfall()->format('d.m.Y') : '',
                    'tekst' => $krav->hentTekst(),
                    'beløp' => $this->app->kr($krav->hentBeløp()),
                    'utestående' => $this->app->kr($krav->hentUtestående()),
                ];
                $sumUtestående += $krav->hentUtestående();
            }
            $dataTablesConfig->columns[3]->footer = $this->app->kr($sumUtestående);
            $tabell = $this->app->vis(DataTable::class, [
                'caption' => 'Gjeldsgrunnlag',
                'dataTablesConfig' => $dataTablesConfig,
                'data' => $tabellData,
            ]);
            return $tabell;
        }
        return '';
    }
}