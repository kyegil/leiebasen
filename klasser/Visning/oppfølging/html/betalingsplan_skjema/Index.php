<?php

namespace Kyegil\Leiebasen\Visning\oppfølging\html\betalingsplan_skjema;


use Exception;
use Kyegil\CoreModel\CoreModel;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\AutoComplete;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\CheckboxGroup;
use Kyegil\Leiebasen\Visning\felles\html\form\CollapsibleSection;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\DisplayField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\html\form\FileField;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\Leiebasen\Visning\felles\html\form\TextArea;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\Leiebasen\Visning\Oppfølging;
use Kyegil\ViewRenderer\ViewArray;
use stdClass;

/**
 * Visning for betalingsplan i oppfølging
 *
 *  Ressurser:
 *      notat \Kyegil\Leiebasen\Modell\Leieforhold\Notat
 *  Mulige variabler:
 *      $notatId
 *      $leieforholdVelger
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\oppfølging\html\betalingsplan_skjema
 */
class Index extends Oppfølging
{
    /** @var string */
    protected $template = 'oppfølging/html/betalingsplan_skjema/Index.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        if($attributt == 'betalingsplan') {
            return $this->settBetalingsplan($verdi);
        }
        if($attributt == 'notat') {
            return $this->settNotat($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Notat|null $notat
     * @return $this
     */
    protected function settNotat(?Notat $notat): Index
    {
        $this->settRessurs('notat', $notat);
        return $this;
    }

    /**
     * @param Leieforhold|null $leieforhold
     * @return $this
     */
    protected function settLeieforhold(?Leieforhold $leieforhold): Index
    {
        $this->settRessurs('leieforhold', $leieforhold);
        return $this;
    }

    /**
     * @param Betalingsplan|null $betalingsplan
     * @return $this
     */
    protected function settBetalingsplan(?Betalingsplan $betalingsplan): Index
    {
        $this->settRessurs('betalingsplan', $betalingsplan);
        return $this;
    }


    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Notat|null $notat */
        $notat = $this->hentRessurs('notat');
        /** @var Betalingsplan|null $betalingsplan */
        $betalingsplan = $this->hentRessurs('betalingsplan');
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $notat
            ? $notat->hentLeieforhold()
            : ($betalingsplan ? $betalingsplan->hentLeieforhold() : $this->hentRessurs('leieforhold'));

        $felter = new ViewArray();

        /** @var TextArea $notatFelt */
        $notatFelt = $this->app->vis(TextArea::class, [
            'label' => 'Notat',
            'name' => 'notat',
            'value' => $notat ? $notat->hentNotat() : null
        ]);

        $aktivPlanFelt = $this->app->vis(Checkbox::class, [
            'name' => 'aktiv',
            'label' => 'Planen er aktiv',
            'value' => 1,
            'checked' => $betalingsplan ? $betalingsplan->aktiv : true
        ]);

        /** @var Field $datoFelt */
        $datoFelt = $this->app->vis(Field::class, [
            'name' => 'dato',
            'required' => true,
            'label' => 'Dato',
            'type' => 'date',
            'value' => $notat ? $notat->hentDato()->format('Y-m-d') : date_create()->format('Y-m-d')

        ]);

        /** @var CheckboxGroup $originalkravVelger */
        $originalkravVelger = $this->app->vis(CheckboxGroup::class, [
            'id' => 'originalkravVelger',
            'name' => 'originalkrav',
            'class' => 'noborder',
            'required' => true,
            'forceSelection' => true,
            'multiple' => true,
            'options' => $this->hentOriginaleKravOptions($notat ? $betalingsplan : null),
            'value' => $this->hentOriginalKravIdNumre($leieforhold, $notat ? $betalingsplan : null)
        ]);
        /** @var HtmlElement $sumFelt */
        $sumFelt = new HtmlElement('div', ['class' => 'sum gjeld'], '<strong>Sum:</strong> ');
        $uteståendeFeltsett = $this->app->vis(FieldSet::class, [
            'label' => 'Utestående',
            'id' => 'utestående_feltsett',
            'contents' => [
                $originalkravVelger,
                $sumFelt,
            ]
        ]);

        /**
         * Utestående foreslåes til å betales ned over 6 mnd,
         * men minimum 1000 per måned dersom gjelden er under 6000
         * og maksimum 2500 per mnd
         */
        $foreslåttAvdragsbeløp = min(2500,
            $leieforhold->hentForfalt() > 6000
                ? 100 * (1 + round($leieforhold->hentForfalt() / 600))
                : 1000
        );

        /** @var Field $terminBeløp */
        $terminBeløp = $this->app->vis(Field::class, [
            'label' => 'Normalt avdragsbeløp',
            'name' => 'terminbeløp',
            'type' => 'number',
            'step' => '0.01',
            'min' => 100,
            'value' => $betalingsplan
                ? $betalingsplan->hentAvdragsbeløp()
                : $foreslåttAvdragsbeløp,
            'required' => true
        ]);

        /** @var Field $terminBeløp */
        $terminlengde = $this->app->vis(Select::class, [
            'label' => 'Hver',
            'name' => 'terminlengde',
            'options' => $this->hentTerminlengder(),
            'value' => $betalingsplan
                ? $betalingsplan->getStringValue('terminlengde')
                : CoreModel::prepareIso8601IntervalSpecification($leieforhold->hentTerminlengde()),
            'required' => true
        ]);

        $avtalegiroFelt = '';
        if($this->app->hentValg('avtalegiro') && $leieforhold->hentFbo()) {
            $avtalegiroFelt = new ViewArray([
                $this->app->vis(Checkbox::class, [
                    'name' => 'avtalegiro',
                    'label' => 'Kan trekkes med AvtaleGiro',
                    'value' => 1,
                    'checked' => $betalingsplan ? !$betalingsplan->utenomAvtalegiro : true
                ]),
                'NB! Ved inngåelse eller endring av avtale kan det ta opptil 5 dager før trekk med AvtaleGiro blir oppdatert'
            ]);
        }

        $forfallspåminnelseFelt = '';
        if($leieforhold->hentEpost()) {
            $forfallspåminnelseFelt = $this->app->vis(Field::class, [
                'name' => 'forfallspåminnelse_ant_dager',
                'label' => 'Send epost-påminnelse x dager før hvert avdrag (hold tomt for ingen påminnelser)',
                'type' => 'number',
                'min' => 1,
                'value' => $betalingsplan ? $betalingsplan->forfallspåminnelseAntDager : 3,
            ]);
        }

        /** @var Checkbox $følgerLeiebetalinger */
        $følgerLeiebetalinger = $this->app->vis(Checkbox::class, [
            'name' => 'følger_leiebetalinger',
            'label' => 'Betales samtidig med husleie',
            'value' => 1,
            'checked' => $notat && $betalingsplan ? $betalingsplan->følgerLeiebetalinger : false
        ]);

        /** @var Field $terminBeløp */
        $førsteForfall = $this->app->vis(DateField::class, [
            'name' => 'startdato',
            'label' => 'Dato for første avdrag',
            'value' => $notat && $betalingsplan && $betalingsplan->hentStartDato() ? $betalingsplan->hentStartDato()->format('Y-m-d') : $leieforhold->nyForfallsdato()->format('Y-m-d'),
            'required' => true
        ]);

        $planTabell = $this->app->vis(DataTable::class, [
            'tableId' => 'plantabell',
            'dataTablesConfig' => (object)[
                'paging' => false,
                'searching' => false,
                'processing' => true,
                'info' => false,
                'ordering' => false,
                'data' => [],
                'language' => (object)[
                    'emptyTable' => 'Angi ønsket avdragsbeløp over for å generere avdrag'
                ],
                'columns' => [
                    (object)[
                        'data' => 'dato',
                        'name' => 'dato',
                        'title' => 'Dato',
                        'className' => 'dato',
                    ],
                    (object)[
                        'data' => 'avdrag',
                        'name' => 'avdrag',
                        'title' => 'Beløp (kan tilpasses)',
                        'className' => 'beløp text-right',
                        'width' => '30px',
                    ],
                ]
            ],
        ]);

        if ($notat || $betalingsplan) {
            /** @var CollapsibleSection $avtaleFeltsett */
            $avtaleFeltsett = $this->app->vis(CollapsibleSection::class, [
                'label' => 'Avtaletekst',
                'collapsed' => true,
                'contents' => [
                    $this->app->vis(AvtaleRedigerer::class, [
                        'id' => 'avtaletekstFelt',
                        'name' => 'avtaletekst',
                        'label' => 'Innhold',
                        'value' => $notat
                            ? $notat->hentBrevtekst()
                            : str_replace(
                                '{avdragsoversikt}', '<div class="avdragsoversikt"></div>',
                                Betalingsplan::gjengiAvtalemal($betalingsplan->hentAvtale(), $this->app, $betalingsplan->leieforhold)
                        )
                    ])
                ]
            ]);

        }
        else {
            $avtaleFeltsett = $this->app->vis(Checkbox::class, [
                'name' => 'avtaletekst-generer',
                'label' => 'Generer avtale for utskrift basert på opplysningene som er gitt over',
                'checked' => true
            ]);
        }

        /** @var Select $avsenderFelt */
        $avsenderFelt = $this->app->vis(Select::class, [
            'label' => 'Gjelder hendvendelse fra',
            'name' => 'henvendelse_fra',
            'options' => (object)[
                '' => 'Ikke relevant',
                Notat::FRA_UTLEIER => $this->app->hentValg('utleier'),
                Notat::FRA_LEIETAKER => $leieforhold ? $leieforhold->hentNavn() : 'Leietaker',
                Notat::FRA_FRAMLEIER => 'Framleier',
                Notat::FRA_ANDRE => 'Andre',
            ],
            'value' => $notat ? $notat->hentHenvendelseFra() : ''
        ]);

        /** @var Field $dokumentreferanseFelt */
        $dokumentreferanseFelt = $this->app->vis(Field::class, [
            'label' => 'Referanse til papiravtale, arkivert henvendelse, saksnr e.l.',
            'name' => 'dokumentreferanse',
            'value' => $notat ? $notat->hentDokumentreferanse() : null
        ]);

        /** @var Field $dokumenttypeFelt */
        $dokumenttypeFelt = $this->app->vis(AutoComplete::class, [
            'label' => 'Det eksterne dokumentet er av type (velg fra listen eller skriv inn ny type)',
            'name' => 'dokumenttype',
            'multiple' => false,
            'options' => $this->hentDokumentTyper(),
            'value' => $notat ? \Kyegil\Leiebasen\Leiebase::ucfirst($notat->hentDokumenttype()) : null
        ]);

        $vedleggFeltsettFelter = new ViewArray();

        if ($notat && $notat->hentVedlegg()) {
            $vedleggFeltsettFelter->addItem($this->app->vis(DisplayField::class, [
                'label' => 'Vedlegg',
                'class' => 'eksisterende-vedlegg',
                'value' => $notat->hentVedleggsnavn(),
            ]));
            $vedleggFeltsettFelter->addItem(new HtmlElement('div', [
                'class' => 'button',
                'onclick' => 'leiebasen.oppfølging.betalingsplanSkjema.lastNedVedlegg()'
            ], 'Last ned'));
            $vedleggFeltsettFelter->addItem(new HtmlElement('div', [
                'class' => 'button',
                'onclick' => 'leiebasen.oppfølging.betalingsplanSkjema.slettVedlegg()'
            ], 'Slett eksisterende vedlegg'));
            $vedleggFeltsettFelter->addItem($this->app->vis(FileField::class, [
                'label' => 'Last opp vedlegg på nytt (erstatter eksisterende vedlegg)',
                'name' => 'vedlegg',
                'multiple' => false
            ]));
        }
        else {
            $vedleggFeltsettFelter->addItem($this->app->vis(FileField::class, [
                'label' => 'Last opp avtalen fra fil',
                'name' => 'vedlegg',
                'multiple' => false
            ]));
        }

        /** @var CollapsibleSection $oppfølgingsFeltsett */
        $vedleggFeltsett = $this->app->vis(CollapsibleSection::class, [
            'label' => 'Betalingsavtale',
            'collapsed' => false,
            'contents' => [
                $avtaleFeltsett,
                $vedleggFeltsettFelter,
                $dokumentreferanseFelt,
                $dokumenttypeFelt,
            ]
        ]);

        $felter->addItem(new HtmlElement('div', [], 'Leieforhold ' . $leieforhold->hentId() . ' ' . $leieforhold->hentBeskrivelse()));
        $felter->addItem($this->app->vis(Field::class, [
            'name' => 'leieforhold',
            'type' => 'hidden',
            'value' => $leieforhold->hentId()
        ]));

        $felter->addItem($datoFelt);
        $felter->addItem($avsenderFelt);
        $felter->addItem($notatFelt);
        $felter->addItem($aktivPlanFelt);
        $felter->addItem($uteståendeFeltsett);
        $felter->addItem($this->app->vis(FieldSet::class, [
            'id' => 'frekvens_feltsett',
            'label' => 'Nedbetaling',
            'contents' => [
                $følgerLeiebetalinger,
                new HtmlElement('div', [], [new ViewArray([
                    $førsteForfall,
                    $terminBeløp,
                    $terminlengde,
                ])]),
                new HtmlElement('div', ['class' => 'totalbeløp'], ),
                'Unntak fra normal-avdragene kan angis i avdragsoversikten nedenfor.<br>',
                $forfallspåminnelseFelt,
                $avtalegiroFelt,
            ],
        ]));

        $felter->addItem($planTabell);
        $felter->addItem($vedleggFeltsett);

        $datasett = [
            'notatId' => $notat ? $notat->id : '*',
            'skjema' => [$this->app->vis(AjaxForm::class, [
                'action' => '/oppfølging/index.php?oppslag=betalingsplan_skjema&oppdrag=ta_i_mot_skjema&skjema=notat&id=' . ($notat ? $notat->hentId() : '*'),
                'formId' => 'betalingsplanskjema',
                'buttonText' => 'Lagre',
                'fields' => $felter
            ])],
            'knapper' => [
            ],
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return array
     * @throws Exception
     */
    private function hentDokumentTyper(): array
    {
        $dokumentTyper = [''];
        $tp = $this->app->mysqli->table_prefix;
        foreach($this->app->mysqli->arrayData([
            'distinct' => true,
            'flat' => true,
            'source' => $tp . Notat::hentTabell(),
            'fields' => ['dokumenttype']
        ])->data as $type) {
            if (trim($type)) {
                $dokumentTyper[] = \Kyegil\Leiebasen\Leiebase::ucfirst(trim($type));
            }
        }
        return $dokumentTyper;
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    private function hentOriginaleKravOptions(?Betalingsplan $betalingsplan): stdClass
    {
        $ubetalteKrav = new stdClass();
        if ($betalingsplan && $betalingsplan->hentId()) {
            foreach($betalingsplan->hentKravsett() as $krav) {
                $ubetalteKrav->{$krav->hentId()} = $krav->tekst
                    . ' ('
                    . $this->app->kr($krav->utestående, false)
                    . ($krav->forfall ? (' forfall: ' . $krav->forfall->format('d.m.Y')) : '')
                    . ')'
                ;
            }
        }

        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        foreach($leieforhold->hentUteståendeKrav() as $krav) {
            $ubetalteKrav->{$krav->hentId()} = $krav->tekst
                . ' ('
                . $this->app->kr($krav->utestående, false)
                    . ($krav->forfall ? (' forfall: ' . $krav->forfall->format('d.m.Y')) : '')
                . ')'
            ;
        }
        return $ubetalteKrav;
    }

    /**
     * @param Leieforhold $leieforhold
     * @param Betalingsplan|null $betalingsplan
     * @return int[]
     * @throws Exception
     */
    private function hentOriginalKravIdNumre(Leieforhold $leieforhold, ?Betalingsplan $betalingsplan): array
    {
        if ($betalingsplan && $betalingsplan->hentId()) {
            return $betalingsplan->hentKravsett()->hentIdNumre();
        }
        return $leieforhold->hentUteståendeKrav()->hentIdNumre();
    }

    /**
     * @return object
     */
    private function hentTerminlengder()
    {
        return (object)[
            'P7D' => 'uke',
            'P14D' => '2. uke',
            'P28D' => '4. uke',
            'P1M' => 'måned',
            'P2M' => '2. måned',
            'P3M' => '3. måned',
            'P6M' => '6. måned',
        ];
    }
}