<?php

namespace Kyegil\Leiebasen\Visning\oppfølging\html\notat;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for notat i oppfølging
 *
 *  Ressurser:
 *      notat \Kyegil\Leiebasen\Modell\Leieforhold\Notat
 *  Mulige variabler:
 *      $notatId
 *      $aktivitet
 *      $notatTekst
 *      $registrert
 *      $skjulForLeietaker
 *      $brevTekst
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\oppfølging\html\notat
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'oppfølging/html/notat/Index.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'notat') {
            return $this->settNotat($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Notat|null $notat
     * @return $this
     */
    protected function settNotat(?Notat $notat): Index
    {
        $this->settRessurs('notat', $notat);
        return $this;
    }

    /**
     * @param Leieforhold|null $leieforhold
     * @return $this
     */
    protected function settLeieforhold(?Leieforhold $leieforhold): Index
    {
        $this->settRessurs('leieforhold', $leieforhold);
        return $this;
    }


    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Notat|null $notat */
        $notat = $this->hentRessurs('notat');
        $leieforhold = $notat->leieforhold;
        /** @var \stdClass|null $betalingsplanForslag */
        $betalingsplanForslag = $leieforhold->hentBetalingsplanForslag()
        && $leieforhold->hentBetalingsplanForslag()->notat_id
        && $leieforhold->hentBetalingsplanForslag()->notat_id == $notat->hentId()
            ? $leieforhold->hentBetalingsplanForslag()
            : null;
        $knapper = [];

        if ($notat) {
            if ($betalingsplanForslag) {
                $knapper[] = new HtmlElement('button', [
                    'title' => 'Godkjenn og aktivér betalingsplanen',
                    'onclick' => 'leiebasen.oppfølging.notat.godkjennBetalingsplan()',
                ], 'Godkjenn betalingsplanen');
                $knapper[] = new HtmlElement('button', [
                    'title' => 'Avvis betalingsplanen',
                    'onclick' => 'leiebasen.oppfølging.notat.avvisBetalingsplan()',
                ], 'Avvis betalingsplanen');
            }
            $notatTekst = $this->hentNotatTekst($notat);
            if($notat->hentBrevtekst()) {
                $knapper[] = new HtmlElement('button', [
                    'title' => 'Skriv ut brevet',
                    'onclick' => 'leiebasen.oppfølging.notat.visBrev()'
                ], 'Skriv ut brevet');
            }
            if($notat->hentVedlegg()) {
                $knapper[] = new HtmlElement('button', [
                    'title' => 'Last ned vedlegg',
                    'onclick' => 'leiebasen.oppfølging.notat.lastVedlegg()'
                ], 'Last ned vedlegg');
            }

            $knapper[] = $this->hentLinkKnapp('/oppfølging/index.php?oppslag=notat_skjema&id=' . $notat->hentId(), 'Endre notatet', 'Endre');

            $datasett = [
                'notatId' => $notat->id,
                'leieforholdBeskrivelse' => $notat->leieforhold->hentBeskrivelse(),
                'frosset' => $notat->leieforhold->frosset,
                'stoppOppfølging' => $notat->leieforhold->stoppOppfølging,
                'avventOppfølging' => $notat->leieforhold->avventOppfølging > date_create() ? $notat->leieforhold->avventOppfølging->format('d.m.Y') : '',
                'dato' => $notat->dato->format('d.m.Y'),
                'aktivitet' => $this->hentAktivitetsBeskrivelse(),
                'notatTekst' => $notatTekst,
                'registrert' => $notat->registrert->format('d.m.Y H:i:s') . ' av ' . $notat->registrerer,
                'skjulForLeietaker' => $notat->skjulForLeietaker,
                'brevTekst' => $notat->brevtekst,
                'eksterntDokument' => $this->hentEksterntDokument(),
                'vedlegg' => $notat->hentVedleggsnavn(),
                'knapper' => $knapper,
            ];
        }
        else {
            $datasett = [
                'notatId' => '',
                'leieforholdBeskrivelse' => '',
                'dato' => '',
                'aktivitet' => '',
                'notatTekst' => '',
                'registrert' => '',
                'skjulForLeietaker' => '',
                'brevTekst' => '',
                'eksterntDokument' => '',
                'vedlegg' => '',
                'knapper' => [],
            ];
        }
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return string
     */
    private function hentAktivitetsBeskrivelse(): string
    {
        /** @var Notat|null $notat */
        $notat = $this->hentRessurs('notat');

        switch ($notat->hentKategori()) {
            case '':
                $beskrivelse = '';
                if ($notat->hentHenvendelseFra()) {
                    if ($notat->hentHenvendelseFra() == 'fra utleier') {
                        $beskrivelse = 'Henvendelse fra ' . $this->app->hentValg('utleier');
                    }
                    else {
                        $beskrivelse = 'Henvendelse mottatt ' . $notat->hentHenvendelseFra();
                    }
                }
                break;
            case 'brev':
                $kategorier = Notat::hentKategorier();
                $beskrivelse = \Kyegil\Leiebasen\Leiebase::ucfirst(property_exists($kategorier, $notat->hentKategori())
                    ? $kategorier->{$notat->hentKategori()}
                    : $notat->hentKategori()
                );
                if ($notat->hentHenvendelseFra()) {
                    if ($notat->hentHenvendelseFra() == 'fra utleier') {
                        $beskrivelse .= ' fra ' . $this->app->hentValg('utleier');
                    }
                    else {
                        $beskrivelse .= ' mottatt ' . $notat->hentHenvendelseFra();
                    }
                }
                break;
            default:
                $kategorier = Notat::hentKategorier();
                $beskrivelse = \Kyegil\Leiebasen\Leiebase::ucfirst(property_exists($kategorier, $notat->hentKategori())
                    ? $kategorier->{$notat->hentKategori()}
                    : $notat->hentKategori()
                );
                break;
        }
        return $beskrivelse;
    }

    /**
     * @return string
     */
    private function hentEksterntDokument()
    {
        /** @var Notat|null $notat */
        $notat = $this->hentRessurs('notat');

        $beskrivelse = [];
        if($notat->hentDokumenttype()) {
            $beskrivelse[] = \Kyegil\Leiebasen\Leiebase::ucfirst($notat->hentDokumenttype());
        }
        if($notat->hentDokumentreferanse()) {
            $beskrivelse[] = 'ref: ' . $notat->hentDokumentreferanse();
        }
        return implode(', ', $beskrivelse);
    }

    /**
     * @param Notat|null $notat
     * @return string|null
     */
    private function hentNotatTekst(?Notat $notat): ?string
    {
        if(!$notat) {
            return '';
        }
        switch($notat->kategori) {
            case $notat::KATEGORI_BETALINGSPLAN:
                $leieforhold = $notat->leieforhold;
                $betalingsplan = $leieforhold->hentBetalingsplan();
                /** @var \stdClass|null $betalingsplanForslag */
                $betalingsplanForslag = $leieforhold->hentBetalingsplanForslag()
                && $leieforhold->hentBetalingsplanForslag()->notat_id
                && $leieforhold->hentBetalingsplanForslag()->notat_id == $notat->hentId()
                ? $leieforhold->hentBetalingsplanForslag()
                : null;

                if (
                    $betalingsplanForslag
                    && isset($betalingsplanForslag->notat_id)
                    && $betalingsplanForslag->notat_id == $notat->hentId()
                ) {
                    $melding = ['Dette er et forslag til betalingsplan som ennå ikke er godkjent.<br>'];
                    $melding[] = 'Forslaget må godkjennes for å aktiveres, eller avvises.<br>';
                    $notatTekst = new ViewArray([new HtmlElement('div', ['class' => 'obs'], $melding)]);
                    $notatTekst->addItem($notat->notat . '<br>');
                    $notatTekst->addItems([new HtmlElement('button', [
                        'class' => 'button',
                        'onclick' => 'leiebasen.oppfølging.notat.godkjennBetalingsplan()',
                        'title' => 'Godkjenn og aktivér betalingsplanen'
                    ], 'Godkjenn og aktivér betalingsplanen'),
                        new HtmlElement('button', [
                            'class' => 'button',
                            'onclick' => 'leiebasen.oppfølging.notat.avvisBetalingsplan()',
                            'title' => 'Avvis betalingsplanen'
                        ], 'Avvis betalingsplanen')
                        ]);
                    return $notatTekst;
                }

                if(!$betalingsplan
                    || !$betalingsplan->hentNotat()
                    || $betalingsplan->hentNotat()->hentId() != $notat->hentId()
                ) {
                    $melding = ['Dette notatet gjelder en <i>inaktiv</i> betalingsplan.<br>'];
                    if ($betalingsplan && $betalingsplan->hentNotat() && $betalingsplan->hentNotat()->hentId()) {
                        $melding[] = 'Se ' . new HtmlElement('a', [
                                'href' => '/oppfølging/index.php?oppslag=notat&id=' . $betalingsplan->hentNotat()->hentId(),
                                'title' => 'Gå til gjeldende betalingsplan'
                            ], 'gjeldende betalingsplan') . '. ';
                    }
                    $melding[] = 'Klikk ' . new HtmlElement('a', [
                            'href' => '/oppfølging/index.php?oppslag=notat_skjema&id=*',
                            'title' => 'Inngå ny betalingsplan'
                        ], 'her') .' for å inngå en ny betalingsplan.<br>';
                    $notatTekst = new ViewArray([new HtmlElement('div', ['class' => 'obs'], $melding)]);
                    $notatTekst->addItem($notat->notat);
                    return $notatTekst;
                }

                if(!$betalingsplan->aktiv
                ) {
                    $melding = ['OBS! Betalingsplanen er ikke aktiv.'];
                    $notatTekst = new ViewArray([new HtmlElement('div', ['class' => 'obs'], $melding)]);
                    $notatTekst->addItem($notat->notat);
                    return $notatTekst;
                }
                return '';
            default:
                return $notat->notat;
        }
    }
}