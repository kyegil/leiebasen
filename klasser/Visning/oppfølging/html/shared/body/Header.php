<?php

namespace Kyegil\Leiebasen\Visning\oppfølging\html\shared\body;


use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\felles\html\shared\body\Brukermeny;
use Kyegil\Leiebasen\Visning\felles\html\shared\body\Hovedmeny;
use Kyegil\Leiebasen\Visning\felles\html\shared\body\Varsler;
use Kyegil\Leiebasen\Visning\Oppfølging;

/**
 * Visning for header i Oppfølging-sidene
 *
 *  Mulige variabler:
 *      $språkvelger
 *      $logo
 *      $logoWidth
 *      $logoHeight
 *      $navn
 *      $hovedmeny
 *      $brukermeny
 * @package Kyegil\Leiebasen\Visning\oppfølging\html\shared\body
 */
class Header extends Oppfølging
{
    /** @var string */
    protected $template = 'oppfølging/html/shared/body/Header.html';

    /**
     * @return $this
     */
    protected function forberedData()
    {
        /** @var Person $bruker */
        $bruker = $this->hentRessurs('bruker');
        $hovedmeny = $this->hent('hovedmeny') ?? $this->app->vis(Hovedmeny::class, [
            'bruker' => $bruker ?? ($this->app->hoveddata['bruker'] ?? null)
        ]);
        $brukermeny = $this->hent('brukermeny') ?? $this->app->vis(Brukermeny::class, [
            'bruker' => $bruker ?? ($this->app->hoveddata['bruker'] ?? null)
        ]);
        $varsler = $this->hent('varsler') ?? $this->app->vis(Varsler::class);
        $this->definerData([
            'språkvelger' => '',
            'logo' => '/pub/media/bilder/offentlig/blank.png',
            'logoWidth' => 1,
            'logoHeight' => 1,
            'navn' => $this->app->hentValg('utleier'),
            'hovedmeny' => $hovedmeny,
            'brukermeny' => $brukermeny,
            'varsler' => $varsler,
        ]);
        return parent::forberedData();
    }

}