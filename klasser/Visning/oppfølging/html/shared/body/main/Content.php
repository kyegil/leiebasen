<?php

namespace Kyegil\Leiebasen\Visning\oppfølging\html\shared\body\main;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Visning\felles\html\shared\body\main\ContentInterface;

/**
 * Visning for variabelt innhold i main content
 *
 * Tilgjengelige variabler:
 * * $heading
 * * $mainBodyContents
 * * $footerContents
 * * $buttons
 *
 * @package Kyegil\Leiebasen\Visning\oppfølging\html\shared\body\main
 */
class Content extends \Kyegil\Leiebasen\Visning\Oppfølging implements ContentInterface
{
    /** @var string */
    protected $template = 'oppfølging/html/shared/body/main/Content.html';

    /**
     * @return Content
     */
    protected function forberedData(): Content
    {
        $buttons = $this->hent('buttons') ?? '';
        $this->sett('buttons', null);
        $this->definerData([
            'heading' => $this->app->tittel,
            'mainBodyContents' => '',
            'footerContents' => trim($buttons) ? new HtmlElement('div', ['class' => 'nav-buttons d-grid gap-2 d-md-flex justify-content-md-end'], $buttons) : '',
            'buttons' => '',
        ]);
        return parent::forberedData();
    }
}