<?php

namespace Kyegil\Leiebasen\Visning\oppfølging\html\shared\body;


/**
 * Visning for main content
 *
 *  Mulige variabler:
 *      $tittel
 *      $innhold
 *      $breadcrumbs
 *      $varsler
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\oppfølging\html\shared\body
 */
class Main extends \Kyegil\Leiebasen\Visning\Oppfølging
{
    /** @var string */
    protected $template = 'oppfølging/html/shared/body/Main.html';

    /**
     * @return Main
     */
    protected function forberedData()
    {
        $this->definerData([
            'tittel' => '',
            'innhold' => '',
            'breadcrumbs' => '',
            'varsler' => '',
            'knapper' => ''
        ]);
        return parent::forberedData();
    }

}