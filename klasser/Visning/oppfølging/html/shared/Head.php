<?php

    namespace Kyegil\Leiebasen\Visning\oppfølging\html\shared;


    use Kyegil\Leiebasen\Visning\felles\html\shared\HeadInterface as HtmlHead;
    use Kyegil\Leiebasen\Visning\Oppfølging;

    /**
     * Visning for html head i Oppfølging
     *
     *  Mulige variabler:
     *      $title
     *      $extraMetaTags
     *      $links
     *      $scripts Scripts inserted to HTML Head
     * @package Kyegil\Leiebasen\Visning\oppfølging\html\shared
     */
    class Head extends Oppfølging implements HtmlHead
    {
        /** @var string */
        protected $template = 'oppfølging/html/shared/Head.html';

        /**
         * @return Head
         */
        protected function forberedData(): Head
        {
            $this->definerData([
                'title' => '',
                'extraMetaTags' => '',
                'links' => '',
                'scripts' => ''
            ]);
            return parent::forberedData();
        }
    }