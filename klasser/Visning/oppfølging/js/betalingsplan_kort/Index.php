<?php

namespace Kyegil\Leiebasen\Visning\oppfølging\js\betalingsplan_kort;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Skript-fil i oppfølging
 *
 *  Ressurser:
 *      notat \Kyegil\Leiebasen\Modell\Leieforhold\Notat
 *  Mulige variabler:
 *      $notatId
 * @package Kyegil\Leiebasen\Visning\oppfølging\html\betalingsplan_kort
 */
class Index extends \Kyegil\Leiebasen\Visning\oppfølging\js\notat\Index
{
}