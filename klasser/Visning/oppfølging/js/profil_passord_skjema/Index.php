<?php

namespace Kyegil\Leiebasen\Visning\oppfølging\js\profil_passord_skjema;


use Kyegil\Leiebasen\Visning\Oppfølging;

/**
 * Skript-fil i oppfølging
 *
 *  Ressurser:
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\oppfølging\js\profil_passord_skjema
 */
class Index extends Oppfølging
{
    protected $template = 'oppfølging/js/profil_passord_skjema/Index.js';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        return parent::forberedData();
    }
}