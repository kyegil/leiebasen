<?php

namespace Kyegil\Leiebasen\Visning\oppfølging\js\betalingsplan_skjema;


use Exception;
use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Skript-fil i oppfølging
 *
 *  Ressurser:
 *      notat \Kyegil\Leiebasen\Modell\Leieforhold\Notat
 *      betalingsplan \Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $eksisterendePlan
 *      $leieforholdId
 *      $helligdager
 *      $eksisterendePlanJson
 *      $leiebetalingerJson
 * @package Kyegil\Leiebasen\Visning\mine_sider\js\betalingsplan_skjema
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'oppfølging/js/betalingsplan_skjema/Index.js';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        if($attributt == 'betalingsplan') {
            return $this->settBetalingsplan($verdi);
        }
        if($attributt == 'notat') {
            return $this->settNotat($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Notat|null $notat
     * @return $this
     */
    protected function settNotat(?Notat $notat): Index
    {
        $this->settRessurs('notat', $notat);
        return $this;
    }

    /**
     * @param Leieforhold|null $leieforhold
     * @return $this
     */
    protected function settLeieforhold(?Leieforhold $leieforhold): Index
    {
        $this->settRessurs('leieforhold', $leieforhold);
        return $this;
    }

    /**
     * @param Betalingsplan|null $betalingsplan
     * @return $this
     */
    protected function settBetalingsplan(?Betalingsplan $betalingsplan): Index
    {
        $this->settRessurs('betalingsplan', $betalingsplan);
        return $this;
    }


    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Betalingsplan|null $betalingsplan */
        $betalingsplan = $this->hentRessurs('betalingsplan');
        /** @var Notat|null $notat */
        $notat = $betalingsplan ? $betalingsplan->hentNotat() : $this->hentRessurs('notat');
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        $datasett = [
            'notatId' => $notat ? $notat->hentId() : '',
            'leieforholdId' => $leieforhold ? $leieforhold->hentId() : '',
            'eksisterendePlan' => boolval($leieforhold->hentBetalingsplanForslag()),
            'helligdager' => $this->hentHelligdager(intval(date('Y')) +10),
            'eksisterendePlanJson' => json_encode($this->hentEksisterendePlanAvdrag()),
            'leiebetalingerJson' => json_encode($this->hentLeieinnbetalingerDetaljer($leieforhold)),
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @param int $inntilÅr
     * @return string
     * @throws Exception
     */
    private function hentHelligdager(int $inntilÅr): string
    {
        $helligdager = [];
        $år = intval(date('Y'));
        while ($år < $inntilÅr) {
            $åretsHelligdager = $this->app->bankfridager($år);
            array_walk($åretsHelligdager, function (&$dato) use ($år) {
                $dato = $år . '-' . $dato;
            });
            $helligdager = array_merge($helligdager, $åretsHelligdager);
            $år++;
        }
        sort($helligdager);
        return json_encode($helligdager);
    }

    /**
     * @return object[]
     * @throws Exception
     */
    private function hentEksisterendePlanAvdrag(): array
    {
        $plan = [];
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        $eksisterendeForslag = $leieforhold ? $leieforhold->hentBetalingsplanForslag() : null;
        if ($eksisterendeForslag) {
            foreach($eksisterendeForslag->avdrag as $dato => $beløp) {
                $plan[] = (object)[
                    'dato' => $dato,
                    'avdrag' => $beløp
                ];
            }
        }
        return $plan;
    }

    private function hentLeieinnbetalingerDetaljer(?Leieforhold $leieforhold): object
    {
        $detaljer = (object)[
            'tilgjengelig' => false,
        ];
        if($leieforhold) {
            /** @var Krav|null $førsteLeie */
            $førsteLeie = $leieforhold->hentKrav()
                ->leggTilFilter(['type' => Krav::TYPE_HUSLEIE])
                ->leggTilFilter(['fom > NOW()'])
                ->leggTilSortering('kravdato')
                ->låsFiltre()
                ->hentFørste()
            ;
            if($førsteLeie) {
                $terminlengde = $leieforhold->hentTerminlengde();
                $detaljer->terminlengde = (object)[
                    'y' => $terminlengde->y,
                    'm' => $terminlengde->m,
                    'd' => $terminlengde->d,
                ];
                $terminBetalingsfrist = $leieforhold->hentTerminBetalingsfrist();
                $detaljer->terminBetalingsfrist = (object)[
                    'y' => $terminBetalingsfrist->y,
                    'm' => $terminBetalingsfrist->m,
                    'd' => $terminBetalingsfrist->d,
                ];

                $detaljer->betalingsElementer = (object)[
                    'Husleie' => $leieforhold->hentLeiebeløp(),
                ];
                $leieforhold->hentDelkravtyper()
                    ->leggTilFilter(['`' . Delkravtype::hentTabell() . '`.`kravtype`' => Krav::TYPE_HUSLEIE])
                    ->leggTilFilter(['selvstendig_tillegg' => true])->låsFiltre()
                    ->forEach(function(Leieforhold\Krav\LeieforholdDelkravtype $tillegg) use ($detaljer) {
                        $detaljer->betalingsElementer->{$tillegg->hentNavn()} = $tillegg->hentTerminbeløp();
                    } );

                /**
                 * Fast dato per måned kan være 1-27,
                 * eller 't' for siste dag hver måned
                 */
                $forfallFastDagIMåneden = $leieforhold->hentForfallFastDagIMåneden();
                $forfallFastDagIMåneden = ($forfallFastDagIMåneden === 't' || (int)$forfallFastDagIMåneden > 27) ? 't' : max(intval($forfallFastDagIMåneden), 0);
                $detaljer->forfallFastDagIMåneden = $forfallFastDagIMåneden;
                $detaljer->forfallFastUkedag = $leieforhold->hentForfallFastUkedag();
                $detaljer->forfallFastDato = $leieforhold->hentForfallFastDato();


                $detaljer->førsteLeieTermin = $førsteLeie->fom->format('Y-m-d');
                $detaljer->tilgjengelig = true;
            }
        }
        return $detaljer;
    }
}