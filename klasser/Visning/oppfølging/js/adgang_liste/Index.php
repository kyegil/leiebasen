<?php

namespace Kyegil\Leiebasen\Visning\oppfølging\js\adgang_liste;


use Kyegil\Leiebasen\Visning\Oppfølging;

/**
 * Skript-fil i oppfølging
 *
 *  Ressurser:
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\oppfølging\js\adgang_liste
 */
class Index extends Oppfølging
{
    /** @var string */
    protected $template = 'oppfølging/js/adgang_liste/Index.js';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        return parent::forberedData();
    }
}