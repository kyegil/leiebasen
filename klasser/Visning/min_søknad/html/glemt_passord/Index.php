<?php

namespace Kyegil\Leiebasen\Visning\min_søknad\html\glemt_passord;


use Exception;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for innlogging til min søknad
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $språkKode
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\min_søknad\html\glemt_passord
 */
class Index extends Visning\MinSøknad
{
    /** @var string */
    protected $template = 'min_søknad/html/glemt_passord/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var string|null $språk */
        $språk = $this->hent('språkKode');
        if ($språk == 'en') {
            $this->template = 'min_søknad/html/glemt_passord/IndexEn.html';
        }

        $skjemaElementer = new ViewArray();

        $referanseFelt = $this->app->vis(Field::class, [
            'name' => 'referanse',
            'label' => $språk == 'en' ? 'Login reference for the application' : 'Innloggingsreferanse for søknaden',
            'required' => true,
        ]);
        $skjemaElementer->addItem($referanseFelt);

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/min-søknad/index.php?oppslag=login&oppdrag=ta_i_mot_skjema&skjema=glemt_passord&l=' . $språk,
                'formId' => 'glemt_passord',
                'buttonText' => $språk == 'en' ? 'Submit' : 'Send',
                'fields' => $skjemaElementer,
                'reCaptchaSiteKey' => (\Kyegil\leiebasen\Leiebase::$config['leiebasen']['google']['recaptcha3']['site_key'] ?? null)
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}