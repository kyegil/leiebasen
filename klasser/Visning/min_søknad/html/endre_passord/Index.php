<?php

namespace Kyegil\Leiebasen\Visning\min_søknad\html\endre_passord;


use Exception;
use Kyegil\Leiebasen\Modell\Søknad\Adgang;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\MinSøknad;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for passord-endringsskjema i min søknad
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $språkKode
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\min_søknad\html\endre_passord
 */
class Index extends MinSøknad
{
    /** @var string */
    protected $template = 'min_søknad/html/endre_passord/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var string|null $språk */
        $språk = $this->hent('språkKode');
        /** @var Adgang|null $adgang */
        $adgang = $this->hent('adgang');

        $skjemaElementer = new ViewArray();

        $skjemaElementer->addItem(
            $this->app->vis(Field::class, [
                'name' => 'adgang_id',
                'required' => true,
                'type' => 'hidden',
                'value' => strval($adgang)
            ])
        );

        $skjemaElementer->addItem(
            $this->app->vis(Field::class, [
                'name' => 'passord1',
                'type' => 'password',
                'label' => $språk == 'en' ? 'Preferred password' : 'Ønsket passord',
                'required' => true,
            ])
        );
        $skjemaElementer->addItem(
                $this->app->vis(Field::class, [
                'name' => 'passord2',
                'type' => 'password',
                'label' => $språk == 'en' ? 'Repeat the password' : 'Gjenta passordet',
                'required' => true,
            ])
        );

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/min-søknad/index.php?oppslag=endre_passord&oppdrag=ta_i_mot_skjema&skjema=endre_passord&l=' . $språk,
                'formId' => 'endre_passord',
                'buttonText' => $språk == 'en' ? 'Submit' : 'Send',
                'fields' => $skjemaElementer,
                'reCaptchaSiteKey' => (\Kyegil\leiebasen\Leiebase::$config['leiebasen']['google']['recaptcha3']['site_key'] ?? null)
            ]),
            'språkKode' => $språk
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}