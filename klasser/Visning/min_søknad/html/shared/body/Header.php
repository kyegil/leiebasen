<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 28/07/2020
 * Time: 12:46
 */

namespace Kyegil\Leiebasen\Visning\min_søknad\html\shared\body;


use Kyegil\Leiebasen\Visning\Offentlig;

/**
 * Visning for header i min søknad
 *
 *  Mulige variabler:
 *      $språkvelger
 *      $logo
 *      $logoWidth
 *      $logoHeight
 *      $navn
 * @package Kyegil\Leiebasen\Visning\min_søknad\html\shared
 */
class Header extends Offentlig
{
    /** @var string */
    protected $template = 'min_søknad/html/shared/body/Header.html';

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $this->definerData([
            'språkvelger' => '',
            'logo' => '/pub/media/bilder/offentlig/blank.png',
            'logoWidth' => 1,
            'logoHeight' => 1,
            'navn' => $this->app->hentValg('utleier')
        ]);
        return parent::forberedData();
    }

}