<?php

namespace Kyegil\Leiebasen\Visning\min_søknad\html\shared\body;


use Kyegil\Leiebasen\Visning\MinSøknad;

/**
 * Visning for hovedmeny
 *
 *  Mulige variabler:
 *      $språkKode
 * @package Kyegil\Leiebasen\Visning\min_søknad\html\shared\body
 */
class Hovedmeny extends MinSøknad
{
    /** @var string */
    protected $template = 'min_søknad/html/shared/body/Hovedmeny.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     */
    protected function forberedData()
    {
        /** @var string|null $språk */
        $språk = $this->hent('språkKode');
        if ($språk == 'en') {
            $this->template = 'min_søknad/html/shared/body/HovedmenyEn.html';
        }

        return parent::forberedData();
    }

}