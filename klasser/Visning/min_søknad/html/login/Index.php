<?php

namespace Kyegil\Leiebasen\Visning\min_søknad\html\login;


use Exception;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for innlogging til min søknad
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $språkKode
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\min_søknad\html\login
 */
class Index extends Visning\MinSøknad
{
    /** @var string */
    protected $template = 'min_søknad/html/login/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var string|null $språk */
        $språk = $this->hent('språkKode');
        if ($språk == 'en') {
            $this->template = 'min_søknad/html/login/IndexEn.html';
        }

        $skjemaElementer = new ViewArray();

        $loginFelt = $this->app->vis(Field::class, [
            'name' => 'login',
            'label' => $språk == 'en' ? 'Email' : 'E-post',
            'required' => true,
        ]);
//        $skjemaElementer->addItem($loginFelt);

        $referanseFelt = $this->app->vis(Field::class, [
            'name' => 'referanse',
            'label' => $språk == 'en' ? 'Reference' : 'Referanse',
            'required' => true,
        ]);
        $skjemaElementer->addItem($referanseFelt);

        $passordFelt = $this->app->vis(Field::class, [
            'name' => 'passord',
            'type' => 'password',
            'label' => $språk == 'en' ? 'Password' : 'Passord',
            'required' => true,
        ]);
        $skjemaElementer->addItem($passordFelt);

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/min-søknad/index.php?oppslag=login&oppdrag=ta_i_mot_skjema&skjema=login&l=' . $språk,
                'formId' => 'login',
                'buttonText' => $språk == 'en' ? 'Log in' : 'Logg inn',
                'fields' => $skjemaElementer,
                'reCaptchaSiteKey' => (\Kyegil\leiebasen\Leiebase::$config['leiebasen']['google']['recaptcha3']['site_key'] ?? null)
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}