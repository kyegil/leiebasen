<?php

namespace Kyegil\Leiebasen\Visning\min_søknad\epost;


use Exception;
use Kyegil\Leiebasen\Visning;

/**
 * Visning for epost ReferanseOversikt
 *
 *  Ressurser:
 *      adgangsett \Kyegil\Leiebasen\Modell\Søknad\Adgangsett
 *  Mulige variabler:
 *      $lenke
 *      $språk
 * @package Kyegil\Leiebasen\Visning\min_søknad\epost
 */
class Passordlenke extends Visning\MinSøknad
{
    /** @var string */
    protected $template = 'min_søknad/epost/Passordlenke.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return Passordlenke
     */
    public function sett($attributt, $verdi = null)
    {
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return Passordlenke
     * @throws Exception
     */
    protected function forberedData(): Passordlenke
    {
        /** @var string|null $språk */
        $språk = $this->hent('språk');

        switch($språk) {
            case 'en':
                $this->template = 'min_søknad/epost/PassordlenkeEn.html';
        }

        $this->definerData([
            'lenke' => '',
            'utleier' => $this->app->hentValg('utleier'),
            'språk' => '',
        ]);
        return parent::forberedData();
    }
}