<?php

namespace Kyegil\Leiebasen\Visning\min_søknad\epost;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Søknad\Adgang;
use Kyegil\Leiebasen\Modell\Søknad\Adgangsett;
use Kyegil\Leiebasen\Visning;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for epost ReferanseOversikt
 *
 *  Ressurser:
 *      adgangsett \Kyegil\Leiebasen\Modell\Søknad\Adgangsett
 *  Mulige variabler:
 *      $epost
 *      $språk
 *      $utleier
 *      $referanser
 * @package Kyegil\Leiebasen\Visning\felles\epost\søknad
 */
class ReferanseOversikt extends Visning\MinSøknad
{
    /** @var string */
    protected $template = 'min_søknad/epost/ReferanseOversikt.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return ReferanseOversikt
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'adgangsett') {
            return $this->settAdgangsett($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Adgangsett $adgangsett
     * @return ReferanseOversikt
     */
    public function settAdgangsett(Adgangsett $adgangsett): ReferanseOversikt
    {
        return $this->settRessurs('adgangsett', $adgangsett);
    }

    /**
     * @return ReferanseOversikt
     * @throws Exception
     */
    protected function forberedData(): ReferanseOversikt
    {
        /** @var Adgangsett|null $adgangsett */
        $adgangsett = $this->hentRessurs('adgangsett');
        /** @var string|null $språk */
        $språk = $this->hent('språk');

        switch($språk) {
            case 'en':
                $this->template = 'min_søknad/epost/ReferanseOversiktEn.html';
        }

        if($adgangsett) {
            $søknadListe = new ViewArray();
            /** @var Adgang $adgang */
            foreach($adgangsett as $adgang) {
                $søknad = $adgang->søknad;
                $linje = new HtmlElement('tr', [], new ViewArray([
                    new HtmlElement('td', ['style' => 'text-align:left; padding: 0 5px;'], $søknad->type->navn),
                    new HtmlElement('td', ['style' => 'text-align:left; padding: 0 5px;'], $søknad->hentRegistrert()->format('d.m.Y')),
                    new HtmlElement('td', ['style' => 'text-align:left; padding: 0 5px;'], $adgang->hentReferanse())
                ]));
                $søknadListe->addItem($linje);
            }
            $epost = $adgangsett->hentFørste()->epost;
            $datasett = [
                'epost' => $epost,
                'språk' => $språk,
                'referanser' => new HtmlElement('table', [], new ViewArray([
                    new HtmlElement('thead', [], new HtmlElement('tr', [], new ViewArray([
                        new HtmlElement('th', ['style' => 'text-align:left; padding: 0 5px;'], $språk == 'en' ? 'Application' : 'Søknad'),
                        new HtmlElement('th', ['style' => 'text-align:left; padding: 0 5px;'], $språk == 'en' ? 'Date' : 'Dato'),
                        new HtmlElement('th', ['style' => 'text-align:left; padding: 0 5px;'], $språk == 'en' ? 'Login Reference' : 'Innloggingsreferanse')
                    ]))),
                    new HtmlElement('tbody', [], $søknadListe),
                ])),
            ];
            $this->definerData($datasett);
        }
        $this->definerData([
            'epost' => '',
            'språk' => '',
            'utleier' => $this->app->hentValg('utleier'),
            'referanser' => '',
        ]);
        return parent::forberedData();
    }
}