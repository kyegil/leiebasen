<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\poeng_status;


use Exception;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\poeng_program\DetaljTabell;

/**
 * Visning for Poeng-program-detaljer i drift
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $varsler
 *      $innhold
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\drift\html\poeng_status
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/poeng_status/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return Index
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['leieforhold', 'program'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return Index
     * @throws Exception
     */
    protected function forberedData()
    {
        $knapper = [
        ];

        $datasett = [
            'varsler' => '',
            'innhold' => $this->app->vis(DetaljTabell::class, [
                'program' => $this->hentRessurs('program'),
                'leieforhold' => $this->hentRessurs('leieforhold'),
            ]),
            'knapper' => $knapper,
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}