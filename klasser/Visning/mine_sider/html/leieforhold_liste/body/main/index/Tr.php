<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_liste\body\main\index;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Visning for skjema for å velge Hoved-leieforhold i mine sider
 *
 *  Ressurser:
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $leieforholdId
 *      $leieforholdBeskrivelse
 *      $velg
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_liste\body\main\index
 */
class Tr extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/leieforhold_liste/body/main/index/Tr.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        if($leieforhold) {
            $datasett = [
                'leieforholdId' => $leieforhold->hentId(),
                'leieforholdBeskrivelse' => $leieforhold->hentBeskrivelse()
            ];
            $this->definerData($datasett);
        }
        return parent::forberedData();
    }
}