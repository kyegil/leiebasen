<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_liste\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for skjema for å velge Hoved-leieforhold i mine sider
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *  Mulige variabler:
 *      $brukerNavn
 *      $form
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_liste\body\main
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/leieforhold_liste/body/main/Index.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['bruker'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Person $bruker */
        $bruker = $this->hentRessurs('bruker');
        if($bruker) {
            /** @var Leieforhold $hovedLeieforhold */
            $hovedLeieforhold = $bruker->hentHovedLeieforhold();
            /** @var Leieforhold[] $andreLeieforhold */
            $andreLeieforhold = $this->hentAndreLeieforhold($bruker);
            $formFields = new ViewArray();
            $formFields->addItem($this->app->vis(Visning\mine_sider\html\leieforhold_liste\body\main\index\Tr::class, [
                'leieforhold' => $hovedLeieforhold,
                'velg' => 'Hovedleieforhold'
            ]));
            foreach($andreLeieforhold as $leieforhold) {
                $formFields->addItem($this->app->vis(Visning\mine_sider\html\leieforhold_liste\body\main\index\Tr::class, [
                    'leieforhold' => $leieforhold,
                    'velg' => $this->app->vis(Visning\felles\html\form\RadioButton::class, [
                        'name' => 'hovedleieforhold-velger',
                        'label' => 'Angi som hovedleieforhold',
                        'value' => $leieforhold->hentId()
                        ])
                ]));
            }
            $table = new HtmlElement('table', [], new HtmlElement('tbody', [], $formFields));

            $datasett = [
                'brukerNavn' => $bruker->hentNavn(),
                'form' => $this->app->vis(AjaxForm::class, [
                    'action' => '/mine-sider/index.php?oppslag=leieforhold_liste&oppdrag=ta_i_mot_skjema&skjema=hovedleieforhold',
                    'formId' => 'hovedleieforholdskjema',
                    'buttonText' => 'Lagre Hovedleieforhold',
                    'fields' => $table
                ])
            ];
            $this->definerData($datasett);
        }
        return parent::forberedData();
    }

    /**
     * @param Person $bruker
     * @return Leieforhold[]
     * @throws Exception
     */
    protected function hentAndreLeieforhold(Person $bruker): array
    {
        $adganger = clone $bruker->hentAdganger();
        $adganger->leggTilFilter(['adgang' => 'mine-sider']);
        /** @var Leieforhold[] $andreLeieforhold */
        $andreLeieforhold = [];
        /** @var Leieforhold $hovedLeieforhold */
        $hovedLeieforhold = $bruker->hentHovedLeieforhold();
        /** @var Person\Adgang $adgang */
        foreach($adganger as $adgang) {
            if(strval($adgang->leieforhold) != strval($hovedLeieforhold)) {
                $andreLeieforhold[] = $adgang->leieforhold;
            }
        }
        return $andreLeieforhold;
    }
}