<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\fs_regning\body\main;


use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Visning\mine_sider\html\fs_regning\body\main\index\Andel;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Class Index
 *
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      strømanlegg Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg
 *      faktura Kyegil\Leiebasen\Samling|Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad[]
 *  Mulige variabler:
 *      $id
 *      $strømanlegg
 *      $anleggsnummer
 *      $anleggsbeskrivelse
 *      $fordeling
 *      $fordeltBeløp
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\fs_regning\body\main
 */
class Index extends MineSider
{
    protected $template = 'mine_sider/html/fs_regning/body/main/Index.html';

    protected $data = [
        'id' => null,
        'fakturanummer' => null,
        'beløp' => null,
        'anleggsnummer' => null,
        'fraDato' => null,
        'tilDato' => null,
        'termin' => null,
        'forbruk' => null,
        'beregnet' => null,
        'fordelt' => null,
        'strømanlegg' => null,
        'anleggsbeskrivelse' => null,
        'fordeling' => null,
        'fordeltBeløp' => null
    ];
    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['strømanlegg', 'faktura'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad $faktura */
        $faktura = $this->hentRessurs('faktura');
        /** @var Strømanlegg $strømanlegg */
        $strømanlegg = $this->hentRessurs('strømanlegg');

        if($faktura && !$strømanlegg) {
            $strømanlegg = $faktura->hentAnlegg();
        }

        if($faktura) {
            $fordeling = new ViewArray();
            $fordeltBeløp = 0;
            $andeler = $faktura->hentAndeler();
            /** @var Regning\Andel $andel */
            foreach($andeler as $andel) {
                $fordeling->addItem($this->app->vis(Andel::class, [
                    'andel' => $andel
                ]));
                $fordeltBeløp += $andel->hentBeløp();
            }
            $this->definerData($datasett = [
                'id' => $faktura->hentId(),
                'fakturanummer' => $faktura->hentFakturanummer(),
                'beløp' => $this->app->kr($faktura->hentFakturabeløp()),
                'anleggsnummer' => strval($faktura->hentAnlegg()),
                'fraDato' => $faktura->hentFradato() ? $faktura->hentFradato()->format('d.m.Y') : '',
                'tilDato' => $faktura->hentTildato() ? $faktura->hentTildato()->format('d.m.Y') : '',
                'termin' => $faktura->hentTermin(),
                'forbruk' => $faktura->hentForbruk() ? "{$faktura->hentForbruk()} kWh" : '',
                'beregnet' => $faktura->hentBeregnet(),
                'fordelt' => $faktura->hentFordelt(),
                'fordeling' => $fordeling,
                'fordeltBeløp' => $this->app->kr($fordeltBeløp)
            ]);
        }

        if($strømanlegg) {
            $this->definerData($datasett = [
                'strømanlegg' => $strømanlegg->hentId(),
                'anleggsnummer' => $strømanlegg->hentAnleggsnummer(),
                'anleggsbeskrivelse' => $strømanlegg->hentFormål(),

            ]);
        }
        return parent::forberedData();
    }
}