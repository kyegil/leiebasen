<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\fs_regning\body\main\index;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andel as KostnadAndel;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Class Andel
 *
 * Visning
 *
 *  Ressurser:
 *      andel Kyegil\Leiebasen\Samling|Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andel
 *  Mulige variabler:
 *      $id
 *      $beløp
 *      $beskrivelse
 *      $leieforhold
 *      $leieforholdBeskrivelse
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\fs_regning\body\main\index
 */
class Andel extends MineSider
{
    protected $template = 'mine_sider/html/fs_regning/body/main/index/Andel.html';

    protected $data = [
    ];
    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['andel', 'faktura', 'leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var KostnadAndel $andel */
        $andel = $this->hentRessurs('andel');
        /** @var Kostnad $faktura */
        $faktura = $this->hentRessurs('faktura');
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        if($andel && !$faktura) {
            $faktura = $andel->hentRegning();
        }

        if($andel) {
            $beløp = $andel->hentBeløp();
            $leieforhold = $andel->hentLeieforhold();
            $this->definerData($datasett = [
                'id' => $andel->hentId(),
                'beløp' => $this->app->kr($beløp),
                'beskrivelse' => $andel->hentTekst(),
            ]);
        }
        if($leieforhold) {
            $leieforholdBeskrivelse = $leieforhold->hentBeskrivelse();
            if($this->app->adgang('mine-sider', $leieforhold)) {
                $leieforholdBeskrivelse = new HtmlElement('a', [
                    'href' => '/mine-sider/index.php?oppslag=leieforhold&id=' . $leieforhold->hentId(),
                    'title' => 'Vis leieforholdet'
                    ], $leieforholdBeskrivelse);
            }
            $this->definerData($datasett = [
                'leieforhold' => $leieforhold->hentId(),
                'leieforholdBeskrivelse' => $leieforholdBeskrivelse
            ]);
        }

        return parent::forberedData();
    }
}