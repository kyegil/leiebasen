<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\betalingsplan_skjema;


use DateTimeImmutable;
use Exception;
use Kyegil\CoreModel\CoreModel;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\AvtaleMal;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\AutoComplete;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\CheckboxGroup;
use Kyegil\Leiebasen\Visning\felles\html\form\CollapsibleSection;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\Leiebasen\Visning\felles\html\form\TextArea;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;
use stdClass;

/**
 * Visning for betalingsplan i mine_sider
 *
 *  Ressurser:
 *      notat \Kyegil\Leiebasen\Modell\Leieforhold\Notat
 *  Mulige variabler:
 *      $leieforholdVelger
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\betalingsplan_skjema
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/betalingsplan_skjema/Index.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        if($attributt == 'betalingsplan') {
            return $this->settBetalingsplan($verdi);
        }
        if($attributt == 'notat') {
            return $this->settNotat($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Notat|null $notat
     * @return $this
     */
    protected function settNotat(?Notat $notat): Index
    {
        $this->settRessurs('notat', $notat);
        return $this;
    }

    /**
     * @param Leieforhold|null $leieforhold
     * @return $this
     */
    protected function settLeieforhold(?Leieforhold $leieforhold): Index
    {
        $this->settRessurs('leieforhold', $leieforhold);
        return $this;
    }

    /**
     * @param Betalingsplan|null $betalingsplan
     * @return $this
     */
    protected function settBetalingsplan(?Betalingsplan $betalingsplan): Index
    {
        $this->settRessurs('betalingsplan', $betalingsplan);
        return $this;
    }


    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Notat|null $notat */
        $notat = $this->hentRessurs('notat');
        /** @var Betalingsplan|null $betalingsplan */
        $betalingsplan = $this->hentRessurs('betalingsplan');
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $notat
            ? $notat->hentLeieforhold()
            : ($betalingsplan ? $betalingsplan->hentLeieforhold() : $this->hentRessurs('leieforhold'));

        $felter = new ViewArray();
        $advarsel = '';
        if ($leieforhold->hentBetalingsplan() && $leieforhold->hentBetalingsplan()->hentAktiv()) {
            $advarsel = new HtmlElement('div', ['class' => 'varsel'], [
                'Det eksisterer allerede en aktiv betalingsplan for dette leieforholdet.<br>',
                'En ny plan vil, om den blir godkjent, erstatte den eksisterende avtalen.',
                new HtmlElement('a', [
                    'title' => 'Vis aktiv betalingsplan',
                    'href' => '/mine-sider/index.php?oppslag=notat&id=' . $leieforhold->hentBetalingsplan()->hentNotat()->hentId()
                ],
                    'Se aktiv betalinsplan her.',
                ),
                ]
            );
        }
        /** @var stdClass $eksisterendeForslag */
        $eksisterendeForslag = $leieforhold->hentBetalingsplanForslag() ?? new stdClass();

        /** @var TextArea $notatFelt */
        $notatFelt = $this->app->vis(TextArea::class, [
            'label' => 'Notat til ' . $this->app->hentValg('utleier'),
            'name' => 'notat',
            'value' => null
        ]);

        /** @var CheckboxGroup $originalkravVelger */
        $originalkravVelger = $this->app->vis(CheckboxGroup::class, [
            'id' => 'originalkravVelger',
            'name' => 'originalkrav',
            'class' => 'noborder',
            'required' => true,
            'forceSelection' => true,
            'multiple' => true,
            'options' => $this->hentOriginaleKravOptions(),
            'value' => $eksisterendeForslag->originalkrav ?? $this->hentForfalteKravIdNumre(),
            'readonly' => true,
        ]);
        /** @var HtmlElement $sumFelt */
        $sumFelt = new HtmlElement('div', ['class' => 'sum gjeld'], '<strong>Sum:</strong> ');
        $uteståendeFeltsett = $this->app->vis(FieldSet::class, [
            'label' => 'Utestående',
            'id' => 'utestående_feltsett',
            'contents' => [
                $originalkravVelger,
                $sumFelt,
            ]
        ]);

        /**
         * Utestående foreslåes til å betales ned over 6 mnd,
         * men minimum 1000 per måned dersom gjelden er under 6000
         * og maksimum 2500 per mnd
         */
        $foreslåttAvdragsbeløp = min(2500,
            $leieforhold->hentForfalt() > 6000
                ? 100 * (1 + round($leieforhold->hentForfalt() / 600))
                : 1000
        );

        /** @var Field $terminBeløp */
        $terminBeløp = $this->app->vis(Field::class, [
            'label' => 'Normalt avdragsbeløp',
            'name' => 'terminbeløp',
            'type' => 'number',
            'step' => '0.01',
            'min' => 100,
            'value' => $eksisterendeForslag->avdragsbeløp ?? $foreslåttAvdragsbeløp,
            'required' => true
        ]);

        /** @var Field $terminBeløp */
        $terminlengde = $this->app->vis(Select::class, [
            'label' => 'Gjentas hver',
            'name' => 'terminlengde',
            'options' => $this->hentTerminlengder(),
            'value' => $eksisterendeForslag->terminlengde
                ?? CoreModel::prepareIso8601IntervalSpecification($leieforhold->hentTerminlengde()),
            'required' => true
        ]);

        $avtalegiroFelt = '';
        if($this->app->hentValg('avtalegiro') && $leieforhold->hentFbo()) {
            $avtalegiroFelt = new ViewArray([
                $this->app->vis(Checkbox::class, [
                    'name' => 'avtalegiro',
                    'label' => 'Jeg ønsker at avdragene trekkes med AvtaleGiro',
                    'value' => 1,
                    'checked' => isset($eksisterendeForslag->utenom_avtalegiro) ? !$eksisterendeForslag->utenom_avtalegiro : true
                ]),
                'NB! Ved inngåelse eller endring av avtale kan det ta opptil 5 dager før AvtaleGiro blir oppdatert'
            ]);
        }

        $forfallspåminnelseFelt = '';
        if($leieforhold->hentEpost()) {
            $forfallspåminnelseFelt = $this->app->vis(Field::class, [
                'name' => 'forfallspåminnelse_ant_dager',
                'label' => 'Motta epost-påminnelse x dager før hvert avdrag (hold tomt for ingen påminnelser)',
                'type' => 'number',
                'min' => 1,
                'value' => $eksisterendeForslag->forfallspåminnelse_ant_dager ?? 3,
            ]);
        }

        /** @var Checkbox $følgerLeiebetalinger */
        $følgerLeiebetalinger = $this->app->vis(Checkbox::class, [
            'name' => 'følger_leiebetalinger',
            'label' => 'Betales samtidig med husleie',
            'value' => 1,
            'checked' => $eksisterendeForslag->følger_leiebetalinger ?? false
        ]);

        /** @var Field $terminBeløp */
        $førsteForfall = $this->app->vis(DateField::class, [
            'name' => 'startdato',
            'label' => 'Dato for første avdrag',
            'value' => $eksisterendeForslag->start_dato ?? null, // $leieforhold->nyForfallsdato()->format('Y-m-d'),
            'required' => true
        ]);

        $planTabell = $this->app->vis(DataTable::class, [
            'tableId' => 'plantabell',
            'dataTablesConfig' => (object)[
                'paging' => false,
                'searching' => false,
                'processing' => true,
                'info' => false,
                'ordering' => false,
                'data' => [],
                'language' => (object)[
                    'emptyTable' => 'Angi ønsket avdragsbeløp over for å generere avdrag'
                ],
                'columns' => [
                    (object)[
                        'data' => 'dato',
                        'name' => 'dato',
                        'title' => 'Dato',
                        'className' => 'dato',
                    ],
                    (object)[
                        'data' => 'avdrag',
                        'name' => 'avdrag',
                        'title' => 'Beløp (kan tilpasses)',
                        'className' => 'beløp text-right',
                        'width' => '30px',
                    ],
                ]
            ],
        ]);

        /** @var AvtaleRedigerer $avtaleFelt */
        $avtaleFelt = $this->app->vis(AvtaleRedigerer::class, [
            'id' => 'avtaletekstFelt',
            'name' => 'avtaletekst',
            'label' => 'Variabler i krøllklammer {} vil erstattes i hht til planen over. Avdragsoversikten må beholdes i teksten.',
            'value' => str_replace(
                '{avdragsoversikt}', '<div class="avdragsoversikt"></div>',
                $this->forberedAvtaletekst($leieforhold)
            ),
        ]);

        /** @var CollapsibleSection $avtaleFeltsett */
        $avtaleFeltsett = $this->app->vis(CollapsibleSection::class, [
            'label' => 'Forslag til avtale',
            'collapsed' => false,
            'contents' => [
                $avtaleFelt
            ]
        ]);

        $felter->addItem(new HtmlElement('div', [], 'Leieforhold ' . $leieforhold->hentId() . ' ' . $leieforhold->hentBeskrivelse()));
        $felter->addItem($this->app->vis(Field::class, [
            'name' => 'leieforhold',
            'type' => 'hidden',
            'value' => $leieforhold->hentId()
        ]));

        $felter->addItem($uteståendeFeltsett);
        $felter->addItem($this->app->vis(FieldSet::class, [
            'id' => 'frekvens_feltsett',
            'label' => 'Nedbetaling',
            'contents' => [
                $følgerLeiebetalinger,
                new HtmlElement('div', [], [new ViewArray([
                    $førsteForfall,
                    $terminBeløp,
                    $terminlengde,
                ])]),
                new HtmlElement('div', ['class' => 'totalbeløp'], ),
                'Unntak fra normal-avdragene kan angis i avdragsoversikten nedenfor.<br>',
                $forfallspåminnelseFelt,
                $avtalegiroFelt,
            ],
        ]));

        $felter->addItem($planTabell);
        $felter->addItem($avtaleFeltsett);
        $felter->addItem($notatFelt);

        $datasett = [
            'notatId' => '*',
            'skjema' => [
                $advarsel,
                $this->app->vis(AjaxForm::class, [
                    'action' => '/mine-sider/index.php?oppslag=betalingsplan_skjema&oppdrag=ta_i_mot_skjema&skjema=betalingsplan&leieforhold=' . $leieforhold->hentId(),
                    'formId' => 'betalingsplanskjema',
                    'buttonText' => 'Send forslaget til godkjenning',
                    'fields' => $felter,
                ])
            ],
            'knapper' => [
            ],
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    private function hentOriginaleKravOptions(): stdClass
    {
        $ubetalteKrav = new stdClass();
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        foreach($leieforhold->hentUteståendeKrav() as $krav) {
            $ubetalteKrav->{$krav->hentId()}
                = $krav->tekst
                . ' ('
                    . $this->app->kr($krav->utestående, false)
                    . ($krav->forfall ? (' forfall: ' . $krav->forfall->format('d.m.Y')) : '')
                . ')'
            ;
        }
        return $ubetalteKrav;
    }

    /**
     * @param Leieforhold $leieforhold
     * @return int[]
     * @throws Exception
     */
    private function hentOriginalKravIdNumre(): array
    {
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        return $leieforhold->hentUteståendeKrav()->hentIdNumre();
    }

    /**
     * @param Leieforhold $leieforhold
     * @return int[]
     * @throws Exception
     */
    private function hentForfalteKravIdNumre(): array
    {
        $resultat = [];
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        foreach($leieforhold->hentUteståendeKrav() as $krav) {
            if($krav->hentForfall() && $krav->hentForfall() < new DateTimeImmutable()) {
                $resultat[] = $krav->hentId();
            }
        }
        return $resultat;
    }

    /**
     * @return object
     */
    private function hentTerminlengder()
    {
        return (object)[
            'P7D' => 'uke',
            'P14D' => '2. uke',
            'P28D' => '4. uke',
            'P1M' => 'måned',
            'P2M' => '2. måned',
            'P3M' => '3. måned',
            'P6M' => '6. måned',
        ];
    }

    /**
     * @param Leieforhold $leieforhold
     * @return array|string|string[]
     * @throws Exception
     */
    private function forberedAvtaletekst(Leieforhold $leieforhold)
    {
        /** @var AvtaleMal|null $avtaleMal */
        $avtaleMal = $this->app->hentSamling(AvtaleMal::class)
            ->leggTilFilter(['type' => AvtaleMal::TYPE_BETALINGSPLAN])
            ->hentFørste();
        if ($avtaleMal) {
            return Betalingsplan::gjengiAvtalemal($avtaleMal->mal, $this->app, $leieforhold);
        }
        return '';

    }
}