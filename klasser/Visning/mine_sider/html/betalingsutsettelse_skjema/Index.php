<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\betalingsutsettelse_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\Leiebasen\Visning\felles\html\form\TextArea;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewInterface;

/**
 * Visning for betalingsutsettelse-skjema i mine sider
 *
 *  Ressurser:
 *      krav \Kyegil\Leiebasen\Modell\Leieforhold\Krav|null
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $leieforholdId
 *      $kravId
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\betalingsutsettelse_skjema
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/betalingsutsettelse_skjema/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return Index
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['krav', 'leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return Index
     * @throws Exception
     */
    protected function forberedData()
    {
        $iDag = date_create_immutable();
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        /** @var Krav|null $krav */
        $krav = $this->hentRessurs('krav');

        $betalingsutsettelseAktivert = (bool)$this->app->hentValg('betalingsutsettelse_aktivert');
        $spillerom = $leieforhold->hentBetalingUtsettelseSpillerom();
        $antallDager = $spillerom
            ? $iDag->add($spillerom)->diff($iDag)->days
            : null;

        if($betalingsutsettelseAktivert) {
            if ($antallDager === 0) {
                $forklaring = 'Du har ikke spillerom for å endre forfall selv.<br>';
                $forklaring .= 'Dette kan være fordi du har utestående som ikke omfattes av en betalingsplan, eller fordi du allerede har brukt opp dit spillerom for betalingsutsettelser.';
                $forklaring .= "Rytterligere betalingsutsettelser må derfor godkjennes av {$this->app->hentValg('utleier')}.";
            }
            else {
                $forklaring = 'Du kan selv utsette beløp som ennå ikke har forfalt til betaling.<br>';
                if(isset($antallDager)) {
                    $forklaring .= " Du har et spillerom for utsettelse på totalt {$antallDager} dager. Disse kan fordeles mellom flere regninger." ;
                }
            }
        }
        else {
            $forklaring = "Betalingsutsettelsen må godkjennes for å tre i kraft.";
        }

        /** @var Select $kravFelt */
        $kravFelt = $this->app->vis(Select::class, [
            'label' => 'Velg hva du ønsker nytt forfall på',
            'name' => 'krav',
            'required' => true,
            'value' => $krav ? $krav->hentId() : null
        ]);

        /** @var Select $kravFelt */
        $forfallsdatoFelt = $this->app->vis(DateField::class, [
            'label' => 'Oppgi ønsket forfallsdato',
            'name' => 'forfallsdato',
            'required' => true,
            'value' => $krav && $krav->hentForfall() ? $krav->hentForfall()->format('Y-m-d') : null
        ]);

        /** @var TextArea $notatFelt */
        $notatFelt = $this->app->vis(TextArea::class, [
            'label' => 'Melding/begrunnelse for søknad om utsettelse',
            'name' => 'notat',
            'required' => false,
            'value' => ''
        ]);

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [];
        $buttons[] = new HtmlElement('button', ['type' => 'submit'], 'Lagre ny forfallsdato');

        $datasett = [
            'skjema' => [
                new HtmlElement('div', [], $forklaring),
                $this->app->vis(AjaxForm::class, [
                'action' => '/mine-sider/index.php?oppslag=betalingsutsettelse_skjema&oppdrag=ta_i_mot_skjema&skjema=betalingsutsettelse&leieforhold=' . $leieforhold->hentId(),
                'formId' => 'betalingsutsettelse_skjema',
                'buttonText' => 'Lagre',
                'fields' => [
                    $kravFelt,
                    $forfallsdatoFelt,
                    $notatFelt
                ],
                'buttons' => $buttons
                ])
            ],
            'knapper' => [
            ],
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}