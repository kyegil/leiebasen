<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\leieobjekter_ledige\body\main\index;


use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Leieobjekt-linje i liste over ledige leieobjekter i mine sider
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $dato
 *      $leieobjektnr
 *      $leieobjektbeskrivelse
 *      $endring
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\leieobjekter_ledige\body\main\index
 */
class KommendeLedighetLinje extends MineSider
{
    protected $template = 'mine_sider/html/leieobjekter_ledige/body/main/index/KommendeLedighetLinje.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        return parent::forberedData();
    }
}