<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\leieobjekter_ledige\body\main;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Visning\mine_sider\html\leieobjekter_ledige\body\main\index\KommendeLedighetLinje;
use Kyegil\Leiebasen\Visning\mine_sider\html\leieobjekter_ledige\body\main\index\LeieobjektLinje;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for liste over ledige leieobjekter i mine sider
 *
 *  Mulige variabler:
 *      $dato
 *      $leieobjekter
 *      $kommendeLedigheter
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\leieobjekter_ledige\body\main
 */
class Index extends MineSider
{
    protected $template = 'mine_sider/html/leieobjekter_ledige/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $iDag = new DateTime();
        $leieobjekter = new ViewArray();
        $leieobjektSamling = $this->app->hentSamling(Leieobjekt::class)
            ->leggTilFilter(['ikke_for_utleie' => false]);
        /** @var Leieobjekt $leieobjekt */
        foreach ($leieobjektSamling as $leieobjekt) {
            $ledighet = $leieobjekt->hentLedighet($iDag);
            $leietakerliste = new ViewArray();
            /** @var Leieforhold $leieforhold */
            foreach($leieobjekt->hentLeieforhold($iDag, $iDag) as $leieforhold) {
                $andel = $leieforhold->hentAndel();
                $leietakerliste->addItem(($andel->asDecimal() == 1 ? 'Leies av ' : $andel->asMixedNumber(true)  . ' leies av ') . $leieforhold->hentNavn() . '<br>');
            }
            if($ledighet->asDecimal() > 0) {
                $leieobjekter->addItem($this->app->vis(LeieobjektLinje::class, [
                    'leieobjekt' => $leieobjekt,
                    'sisteLeieforhold' => $leieobjekt->hentSisteLeieforhold($iDag),
                    'leietakerliste' => $leietakerliste
                ]));
            }
        }

        $datasett = [
            'dato' => date_create()->format('d.m.Y'),
            'leieobjekter' => $leieobjekter,
            'kommendeLedigheter' => $this->hentKommendeLedigheter()
        ];

        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @param DateTime|null $dato
     * @return ViewArray
     * @throws Exception
     */
    protected function hentKommendeLedigheter(DateTime $dato = null)
    {
        if(!$dato) {
            $dato = new DateTime();
        }
        $endringer = [];
        $kommendeOppsigelser = $this->app->hentSamling(Leieforhold\Oppsigelse::class)
            ->leggTilFilter(['`' . Leieforhold\Oppsigelse::hentTabell() . '`.`fristillelsesdato` >' => $dato->format('Y-m-d')])
            ->leggTilSortering('fristillelsesdato', false, Leieforhold\Oppsigelse::hentTabell());
        /** @var Leieforhold\Oppsigelse $oppsigelse */
        foreach($kommendeOppsigelser as $oppsigelse) {
            $endringer[$oppsigelse->fristillelsesdato->format('Y-m-d')][$oppsigelse->leieforhold->leieobjekt->id][] = $oppsigelse;
        }
        $kommendeInnflyttinger = $this->app->hentSamling(Leieforhold::class)
            ->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`fradato` >' => $dato->format('Y-m-d')])
            ->leggTilSortering('fradato', false, Leieforhold::hentTabell());
        /** @var Leieforhold $leieforhold */
        foreach($kommendeInnflyttinger as $leieforhold) {
            $endringer[$leieforhold->fradato->format('Y-m-d')][$leieforhold->leieobjekt->id][] = $leieforhold;
        }

        $kommendeLedigheter = new ViewArray();
        foreach ($endringer as $datostreng => $endring) {
            $endringsdato = new DateTime($datostreng);
            foreach($endring as $leieobjektId => $endringsobjekter) {
                /** @var Leieobjekt $leieobjekt */
                $leieobjekt = $this->app->hentModell(Leieobjekt::class, $leieobjektId);
                $ledighet = $leieobjekt->hentLedighet($endringsdato);
                $endringstekst = [];
                foreach($endringsobjekter as $index => $endringsobjekt) {
                    if($endringsobjekt instanceof Leieforhold\Oppsigelse) {
                        $endringstekst[] = "{$endringsobjekt->leieforhold->navn} flytter ut.";
                    }
                    elseif ($endringsobjekt instanceof Leieforhold) {
                        $endringstekst[] = "{$endringsobjekt->navn} flytter inn.";
                    }
                }
                if($ledighet->asDecimal() == 1) {
                    $endringstekst[] = ucfirst($leieobjekt->hentType()) . " {$leieobjekt->hentId()} blir da ledig.";
                }
                else if($ledighet->asDecimal() != 0) {
                    $endringstekst[] = $ledighet->asMixedNumber() . ' av ' . ($leieobjekt->boenhet ? 'boligen' : 'lokalet') . ' blir da ledig.';
                }
                /** @var Leieforhold $leieforhold */
                foreach($leieobjekt->hentLeieforhold($endringsdato, $endringsdato) as $leieforhold) {
                    $leieforholdAndel = $leieforhold->hentAndel();
                    if($leieforholdAndel->asDecimal() != 1) {
                        $endringstekst[] = $leieforholdAndel->asMixedNumber() . " leies av {$leieforhold->hentNavn()}";
                    }
                }

                $kommendeLedigheter->addItem(
                    $this->app->vis(KommendeLedighetLinje::class, [
                        'dato' => $endringsdato->format('d.m.Y'),
                        'leieobjektnr' => $leieobjekt->hentType() . ' ' . $leieobjekt->hentId(),
                        'leieobjektbeskrivelse' => $leieobjekt->hentBeskrivelse(),
                        'endring' => implode("<br>\n", $endringstekst)
                    ])
                );
            }
        }

        return $kommendeLedigheter;
    }
}