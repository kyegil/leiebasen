<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold\body\main;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Leietakerfelt;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $leieforholdnavn
 *      $fradato
 *      $sistFornyet
 *      $oppsigelsesdato
 *      $fristillelsesdato
 *      $leietakere
 *      $leieobjektId
 *      $leieobjekt
 *      $utestående
 *      $forfalt
 *      $leiebeløp
 *      $terminlengde
 *      $fastKid
 *      $efakturaAktivert
 *      $avtaleGiroAktivert
 *      $efakturaAktiver
 *      $efakturaavtale
 *      $fboRegistrert
 *      $avtaletekstKnapp
 *      $uteståendeKnapp
 *      $lagBetalingsplanKnapp
 *      $transaksjonerKnapp
 *      $preferanserKnapp
 *      $merknaderKnapp
 *      $visLeieforholdVelger
 *      $poengProgram
 *      $harAktivBetalingsplan (bool)
 *      $aktivBetalingsplanTilDato
 *      $visBetalingsplanKnapp
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold\body\main
 */
class Index extends MineSider
{
    protected $template = 'mine_sider/html/leieforhold/body/main/Index.html';

    /**
     * @param string $nøkkel
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($nøkkel, $verdi = null)
    {
        if(in_array($nøkkel, ['bruker', 'leieforhold'])) {
            return $this->settRessurs($nøkkel, $verdi);
        }
        return parent::sett($nøkkel, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        $leieobjekt = $leieforhold->hentLeieobjekt();
        $id = $leieforhold->hentId();
        /** @var Person $bruker */
        $bruker = $this->hentRessurs('bruker');
        $harFlereLeieforhold = false;
        if($bruker instanceof Person) {
            $adganger = $this->hentAdganger($bruker);
            /** @var Person\Adgang $adgang */
            foreach($adganger as $adgang) {
                if($adgang->leieforhold->hentId() != $id) {
                    $harFlereLeieforhold = true;
                    break;
                }
            }
        }
        $avtaletekstKnapp = [$this->hentLinkKnapp("/mine-sider/index.php?oppslag=leieforhold_avtaletekst&id={$id}", 'Vis avtaleteksten', 'Vis avtaleteksten')];
        if ($leieforhold && $leieforhold->kontrakt->hent('signert_fil')) {
            $avtaletekstKnapp[] = $this->hentLinkKnapp("/mine-sider/index.php?oppslag=kontrakt_signert&id={$leieforhold->kontrakt->id}", 'Last ned signert leieavtale', 'Last ned den signert leieavtalen');
        }
        $harAktivBetalingsplan = false;
        $aktivBetalingsplanTilDato = '';
        $visBetalingsplanKnapp = '';
        $lagBetalingsplanKnapp = '';
        if ($leieforhold && $leieforhold->hentUtestående() > 0) {
            $harAktivBetalingsplan = (bool)$leieforhold->hentAktivBetalingsplan();
            if ($harAktivBetalingsplan) {
                $aktivBetalingsplanTilDato = $leieforhold->hentAktivBetalingsplan()->hentSluttDato();
                $notat = $leieforhold->hentAktivBetalingsplan()->hentNotat();
                $aktivBetalingsplanTilDato = $aktivBetalingsplanTilDato ? $aktivBetalingsplanTilDato->format('d.m.Y') : '';
                if ($notat) {
                    $visBetalingsplanKnapp = $this->hentLinkKnapp('/mine-sider/index.php?oppslag=notat&id=' . $notat->hentId(), 'Vis nedbetalingsplanen', 'Vis den nåværende nedbetalingsplanen');
                }
            }
            else if($leieforhold->hentBetalingsplanForslag()) {
                $lagBetalingsplanKnapp = $this->hentLinkKnapp("/mine-sider/index.php?oppslag=betalingsplan_skjema&leieforhold={$id}&id=*", 'Vis/endre foreslått nedbetalingsplan ...', 'Vis eller endre det foreliggende forslaget til betalingsplan');
            }
           else {
                $lagBetalingsplanKnapp = $this->hentLinkKnapp("/mine-sider/index.php?oppslag=betalingsplan_skjema&leieforhold={$id}&id=*", 'Opprett nedbetalingsplan ...', 'Foreslå avtale om nedbetaling av utestående');
            }
        }

        $poengProgram = new ViewArray();

        foreach($this->app->hentPoengbestyrer()->hentAktiveProgram() as $poengprogram) {
            $poengProgram->addItem(new HtmlElement('div', [], [
                new HtmlElement('a', [
                    'href' => "/mine-sider/index.php?oppslag=poeng_status&program={$poengprogram}&id={$leieforhold}"
                ], $poengprogram->navn), ': ',
                $this->app->hentPoengbestyrer()->hentPoengSum($poengprogram, $leieforhold)
            ]));
        }


        $this->definerData([
            'leieforholdnavn' => htmlspecialchars($leieforhold->beskrivelse),
            'fradato' => $leieforhold->fradato->format('d.m.Y'),
            'sistFornyet' => $this->hentSistFornyet($leieforhold)->format('d.m.Y'),
            'oppsigelsesdato' => $leieforhold->oppsigelse ? $leieforhold->oppsigelse->oppsigelsesdato->format('d.m.Y') : '',
            'fristillelsesdato' => $leieforhold->oppsigelse ? $leieforhold->oppsigelse->fristillelsesdato->format('d.m.Y') : '',
            'leietakere' => $this->app->vis(Leietakerfelt::class, ['leieforhold' => $leieforhold]),
            'leieobjektId' => $leieforhold->leieobjekt->id,
            'leieobjekt' => htmlspecialchars("{$leieobjekt->hentType()} nr. {$leieobjekt->hentId()}: {$leieobjekt->hentBeskrivelse()}"),
            'utestående' => $leieforhold->utestående ? $this->app->kr($leieforhold->utestående) : '',
            'forfalt' => $leieforhold->forfalt ? $this->app->kr($leieforhold->forfalt) : '',
            'leiebeløp' => $this->app->kr($leieforhold->leiebeløp),
            'terminlengde' => htmlspecialchars($this->app->periodeformat( $leieforhold->terminlengde, false, true)),
            'fastKid' => $leieforhold->kid,
            'efakturaAktivert' => (bool)$this->app->hentValg('efaktura'),
            'avtaleGiroAktivert' => (bool)$this->app->hentValg('avtalegiro'),
            'efakturaavtale' => $leieforhold->hentEfakturaIdentifier() ?: '',
            'fboRegistrert' => $leieforhold->fbo ? $leieforhold->fbo->registrert->format('d.m.Y k\l. H:i') : '',
            'avtaletekstKnapp' => $avtaletekstKnapp,
            'uteståendeKnapp' => $this->hentLinkKnapp("/mine-sider/index.php?oppslag=leieforhold_utestående&id={$id}", 'Se detaljer', 'Vis utestående på leieforholdet'),
            'lagBetalingsplanKnapp' => $lagBetalingsplanKnapp,
            'transaksjonerKnapp' => $this->hentLinkKnapp("/mine-sider/index.php?oppslag=leieforhold_transaksjoner&id={$id}", 'Vis transaksjoner', 'Vis krav og betalinger'),
            'preferanserKnapp' => $this->hentLinkKnapp("/mine-sider/index.php?oppslag=leieforhold_preferanser&id={$id}", 'Mine varslingspreferanser', 'Vis  preferanser for regningsformat etc.'),
            'merknaderKnapp' => $this->hentLinkKnapp("/mine-sider/index.php?oppslag=leieforhold_merknader&id={$id}", 'Vis merknader', 'Vis merknader gjort på leieforholdet.'),
            'visLeieforholdVelger' => $harFlereLeieforhold,
            'poengProgram' => $poengProgram,
            'harAktivBetalingsplan' => $harAktivBetalingsplan,
            'aktivBetalingsplanTilDato' => $aktivBetalingsplanTilDato,
            'visBetalingsplanKnapp' => $visBetalingsplanKnapp,
        ]);
        return parent::forberedData();

    }

    /**
     * @param \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold
     * @return \DateTime
     */
    protected function hentSistFornyet(Leieforhold $leieforhold)
    {
        return $leieforhold->kontrakt->dato;
    }

    /**
     * @param Person $bruker
     * @return \Kyegil\Leiebasen\Samling
     * @throws \Exception
     */
    protected function hentAdganger(Person $bruker)
    {
        $adganger = clone $bruker->hentAdganger();
        $adganger->leggTilFilter(['adgang' => 'mine-sider']);
        return $adganger;
    }
}