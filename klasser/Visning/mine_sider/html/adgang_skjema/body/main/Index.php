<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\adgang_skjema\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Adgangsett;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\CheckboxGroup;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;
use stdClass;

/**
 * Visning for liste over leieforhold du har adgang til via mine sider
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *  Mulige variabler:
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\adgang_skjema\body\main
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/adgang_skjema/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['bruker'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Person $bruker */
        $bruker = $this->hentRessurs('bruker');
        $brukerProfil = $bruker ? $bruker->hentBrukerProfil() : null;
        /** @var Adgangsett $adganger */
        $adganger = $bruker->hentAdganger();
        $egneLeieforhold = $bruker->hentLeieforhold();
        $leieforholdIder = [];
        $leieforholdFelter = new ViewArray();

        foreach ($egneLeieforhold as $leieforhold) {
            $leieforholdIder[] = $leieforhold->hentId();
            $andreSomHarAdgang = new stdClass();
            $andreSomIkkeKanFjernes = [];
            /** @var Person $annen */
            foreach ($this->hentAndreSomHarAdgangTilLeieforhold($leieforhold, $bruker) as $annen) {
                $andreSomHarAdgang->{$annen->hentId()} = $annen->hentNavn();
                if ($annen->erLeietakerI($leieforhold)) {
                    $andreSomIkkeKanFjernes[] = $annen->hentId();
                }
            }
            $andreFelt = $this->app->vis(CheckboxGroup::class, [
                'label' => 'Andre som også har adgang til leieforhold ' . $leieforhold->hentId(),
                'name' => 'andres_adganger[' . $leieforhold->hentId() . ']',
                'options' => $andreSomHarAdgang,
                'disabled' => $andreSomIkkeKanFjernes,
                'value' => array_keys((array)$andreSomHarAdgang)
            ]);
            $andreFelt->hent('optionFields')->addItem(new HtmlElement('a', ['class' => 'button', 'href' => '/mine-sider/index.php?oppslag=adgang_opprett&id=' . $leieforhold->hentId()], 'Gi andre adgang til å administrere leieforholdet'));
            $leieforholdFelter->addItem($this->app->vis(FieldSet::class, [
                'label' => 'Leieforhold ' . $leieforhold->hentId() . ' ' . $leieforhold->hentBeskrivelse(),
                'contents' => [
                    $this->app->vis(Checkbox::class, [
                        'name' => 'adgang[mine-sider][]',
                        'value' => $leieforhold->hentId(),
                        'label' => 'Leieforholdet er tilgjengelig i Mine Sider',
                        'checked' => $bruker->harAdgangTil('mine-sider', $leieforhold)
                    ]),
                    $andreFelt
                ]
            ]));
        }

        foreach($adganger as $adgang) {
            if ($adgang->adgang == 'mine-sider') {
                $leieforhold = $adgang->leieforhold;
                if (!in_array($leieforhold->hentId(), $leieforholdIder)) {
                    $leieforholdFelter->addItem($this->app->vis(FieldSet::class, [
                        'label' => 'Leieforhold ' . $leieforhold->hentId() . ' ' . $leieforhold->hentBeskrivelse(),
                        'contents' => [
                            $this->app->vis(Checkbox::class, [
                                'name' => 'adgang[mine-sider][]',
                                'value' => $leieforhold->hentId(),
                                'label' => 'Leieforholdet er tilgjengelig i Mine Sider',
                                'checked' => $bruker->harAdgangTil('mine-sider', $leieforhold)
                            ]),
                            new HtmlElement('div', ['class' => 'checkboxnote'], 'Dersom du frasier deg denne adgangen kan den ikke gjenopprettes av deg selv.')
                        ]
                    ]));
                }
            }

            else {
                switch ($adgang->adgang) {
                    case 'oppfølging':
                        $label = 'oppfølging';
                        break;
                    case 'drift':
                        $label = 'drift-sidene';
                        break;
                    default:
                        $label = $adgang->adgang;
                        break;
                }
                $leieforholdFelter->addItem($this->app->vis(FieldSet::class, [
                    'label' => ucfirst($label),
                    'contents' => [
                        $this->app->vis(Checkbox::class, [
                            'name' => 'adgang[' . $adgang->adgang . ']',
                            'label' => 'Jeg skal fortsatt ha adgang til ' . $label,
                            'checked' => true
                        ]),
                        new HtmlElement('div', ['class' => 'checkboxnote'], 'Dersom du frasier deg denne adgangen kan den ikke gjenopprettes av deg selv.')
                    ]
                ]));
            }
        }

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/mine-sider/index.php?oppslag=adgang_skjema&oppdrag=ta_i_mot_skjema&skjema=adgangskjema',
                'formId' => 'brukerprofil',
                'buttonText' => 'Lagre endringer',
                'fields' => [
                    $leieforholdFelter
                ]
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @param Leieforhold $leieforhold
     * @param Person $andreEnn
     * @return Person[]
     * @throws Exception
     */
    protected function hentAndreSomHarAdgangTilLeieforhold(Leieforhold $leieforhold, Person $andreEnn): array
    {
        $resultat = [];
        foreach ($leieforhold->hentAdganger() as $adgang) {
            if ($adgang->person->hentId() != $andreEnn->hentId()) {
                $resultat[] = $adgang->person;
            }
        }
        return $resultat;
    }
}