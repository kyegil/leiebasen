<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\skademelding_utbedring_skjema\body\main;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieobjekt\Skade;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\HtmlEditor;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *      skade \Kyegil\Leiebasen\Modell\Leieobjekt\Skade
 *  Mulige variabler:
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\skademelding_utbedring_skjema\body\main
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/skademelding_utbedring_skjema/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['bruker', 'skade'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Skade $skade */
        $skade = $this->hentRessurs('skade');

        /** @var DateField $utbedretDatoFelt */
        $utbedretDatoFelt = $this->app->vis(DateField::class, [
            'id' => 'skademelding-utført-dato',
            'name' => 'utført',
            'label' => 'Utbedret dato',
            'required' => true,
            'value' => date('Y-m-d')
        ]);

        /** @var HtmlEditor $innholdsFelt */
        $sluttrapportFelt = $this->app->vis(HtmlEditor::class, [
            'id' => 'skademelding-sluttrapport-tekst',
            'name' => 'sluttrapport',
            'label' => 'Beskrivelse av utførte tiltak, evt. sluttrapport',
            'value' => null,
        ]);

        $fields = [
            new HtmlElement('div', [], $skade->hentSkade()),
            $utbedretDatoFelt,
            $sluttrapportFelt
        ];

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/mine-sider/index.php?oppslag=skademelding_skjema&oppdrag=ta_i_mot_skjema&skjema=utbedring&id=' . $skade,
                'formId' => 'skademelding_utbedring',
                'buttonText' => 'Lagre',
                'fields' => $fields
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}