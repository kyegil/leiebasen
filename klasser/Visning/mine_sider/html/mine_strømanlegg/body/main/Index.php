<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\mine_strømanlegg\body\main;


use DateTimeImmutable;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Samling;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\mine_sider\html\mine_strømanlegg\body\main\index\AktiveAnlegg;
use Kyegil\Leiebasen\Visning\mine_sider\html\mine_strømanlegg\body\main\index\PassiveAnlegg;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\mine_strømanlegg\body\main
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/mine_strømanlegg/body/main/Index.html';

    protected $aktiveAnlegg;

    protected $passiveAnlegg;

    /**
     * @param string $nøkkel
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($nøkkel, $verdi = null)
    {
        if($nøkkel == 'bruker') {
            return $this->settRessurs('bruker', $verdi);
        }
        return parent::sett($nøkkel, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Person $bruker */
        $bruker = $this->hentRessurs('bruker');
        $aktiveAnlegg = new ViewArray();
        /** @var Strømanlegg $anlegg */
        foreach($this->hentAktiveAnlegg($bruker) as $anlegg) {
            $aktiveAnlegg->addItem($this->app->vis(Visning\mine_sider\html\mine_strømanlegg\body\main\index\Anleggslinje::class, [
                'anlegg' => $anlegg
            ]));
        }
        $passiveAnlegg = new ViewArray();
        foreach($this->hentPassiveAnlegg($bruker) as $anlegg) {
            $passiveAnlegg->addItem($this->app->vis(Visning\mine_sider\html\mine_strømanlegg\body\main\index\Anleggslinje::class, [
                'anlegg' => $anlegg
            ]));
        }
        $this->definerData([
            'aktiveAnlegg' => $this->app->vis(AktiveAnlegg::class, [
                'anlegg' => $aktiveAnlegg
            ]),
            'passiveAnlegg' => $this->app->vis(PassiveAnlegg::class, [
                'anlegg' => $passiveAnlegg
            ])
        ]);
        return parent::forberedData();
    }

    /**
     * @param Person $bruker
     * @return Samling
     * @throws \Exception
     */
    private function hentAktiveAnlegg(Person $bruker)
    {
        if(!isset($this->aktiveAnlegg)) {
            /** @var int[] $leieforholdIder */
            $leieforholdIder = [];
            /** @var int[] $leieobjektIder */
            $leieobjektIder = [];
            foreach($bruker->hentLeieforhold(new DateTimeImmutable()) as $leieforhold) {
                if($leieforhold->hentId()) {
                    $leieforholdIder[] = $leieforhold->hentId();
                    $leieobjekt = $leieforhold->hentLeieobjekt();
                    if($leieobjekt) {
                        $leieobjektIder[] = $leieobjekt->hentId();
                    }
                }
            }
            $this->aktiveAnlegg = $this->app->hentSamling(\Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg::class)
                ->leggTilInnerJoin(Fordelingsnøkkel\Element::getDbTable(),
                    '`' . Tjeneste::hentTabell() . '`.`' . Tjeneste::hentPrimærnøkkelfelt()
                    . '` = `' . Fordelingsnøkkel\Element::getDbTable() . '`.`tjeneste_id`')
                ->leggTilFilter([
                    'or' => [
                        '!følger_leieobjekt AND leieforhold' => $leieforholdIder,
                        'følger_leieobjekt AND leieobjekt' => $leieobjektIder
                    ]
                ]);
        }
        return $this->aktiveAnlegg;
    }

    /**
     * @param Person $bruker
     * @return Samling
     * @throws \Exception
     */
    private function hentPassiveAnlegg(Person $bruker)
    {
        if(!isset($this->passiveAnlegg)) {
            /** @var Samling $adganger */
            $adganger = $bruker->hentAdganger()
                ->leggTilFilter([
                    'adgang' => 'mine-sider'
                ]);
            $leieforholdIder = [];
            $leieobjektIder = [];
            /** @var Person\Adgang $adgang */
            foreach($adganger as $adgang) {
                $leieforhold = $adgang->hentLeieforhold();
                if($leieforhold->hentId()) {
                    $leieforholdIder[] = $leieforhold->hentId();
                    $leieobjekt = $leieforhold->hentLeieobjekt();
                    if($leieobjekt) {
                        $leieobjektIder[] = $leieobjekt->hentId();
                    }
                }
            }
            $aktiveAnleggTjenesteIder = $this->hentAktiveAnlegg($bruker)->hentIdNumre();
            $this->passiveAnlegg = $this->app->hentSamling(\Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg::class)
                ->leggTilInnerJoin(Fordelingsnøkkel\Element::getDbTable(),
                    '`' . Tjeneste::hentTabell() . '`.`' . Tjeneste::hentPrimærnøkkelfelt()
                    . '` = `' . Fordelingsnøkkel\Element::getDbTable() . '`.`tjeneste_id`')
                ->leggTilFilter([
                    'or' => [
                        '!følger_leieobjekt AND leieforhold' => $leieforholdIder,
                        'følger_leieobjekt AND leieobjekt' => $leieobjektIder
                    ],
                ])
                ->leggTilFilter([
                    '`' . \Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg::hentTabell() . '`.`' . \Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg::hentPrimærnøkkelfelt() . '` NOT IN' => $aktiveAnleggTjenesteIder
                ]);
        }
        return $this->passiveAnlegg;
    }
}