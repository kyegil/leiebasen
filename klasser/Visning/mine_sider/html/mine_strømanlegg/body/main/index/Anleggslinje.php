<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 03/11/2020
 * Time: 11:25
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\mine_strømanlegg\body\main\index;


use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      anlegg \Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\mine_strømanlegg\body\main\index
 */
class Anleggslinje extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/mine_strømanlegg/body/main/index/Anleggslinje.html';

    /**
     * @param array|string $attributt
     * @param mixed|null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
{
        if($attributt == 'anlegg') {
            return $this->settRessurs('anlegg', $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Strømanlegg $anlegg */
        $anlegg = $this->hentRessurs('anlegg');
        $this->definerData([
            'anleggsnummer' => $anlegg->hentId(),
            'beskrivelse' => $anlegg->hentFormål()
        ]);
        return parent::forberedData();
    }
}