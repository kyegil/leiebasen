<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 03/11/2020
 * Time: 11:25
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\mine_strømanlegg\body\main\index;


use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $anlegg \Kyegil\Leiebasen\Visning|string
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\mine_strømanlegg\body\main\index
 */
class AktiveAnlegg extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/mine_strømanlegg/body/main/index/AktiveAnlegg.html';
}