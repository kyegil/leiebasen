<?php

    namespace Kyegil\Leiebasen\Visning\mine_sider\html\profil_kommunikasjon_preferanser\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Adgangsett;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for liste over kontraktpreferanser for ulike leieforhold
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *  Mulige variabler:
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\adgang_skjema\body\main
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/profil_kommunikasjon_preferanser/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Index
    {
        if(in_array($attributt,['bruker'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): Index
    {
        /** @var Person $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Adgangsett $adganger */
        $adganger = $bruker->hentAdganger();
        $leieforholdIder = [];
        $leieforholdFelter = new ViewArray();

        foreach($adganger as $adgang) {
            if ($adgang->adgang == 'mine-sider') {
                $adgangId = $adgang->hentId();
                $leieforhold = $adgang->leieforhold;
                if (!in_array($leieforhold->hentId(), $leieforholdIder)) {
                    $leieforholdFelter->addItem($this->app->vis(FieldSet::class, [
                        'label' => 'Leieforhold ' . $leieforhold->hentId() . ' ' . $leieforhold->hentBeskrivelse(),
                        'contents' => [
                            $this->app->vis(Checkbox::class, [
                                'id' => 'epostvarsling-' . $adgangId,
                                'name' => 'adgang[' . $adgangId . '][epostvarsling]',
                                'value' => 1,
                                'label' => 'Jeg ønsker å motta automatiske epost-varsler for dette leieforholdet.',
                                'checked' => $adgang->hentEpostvarsling(),
                                'onclick' => 'leiebasen.mineSider.oppdaterForbindelse(' . $adgangId . ', true);'
                            ]),
                            $this->app->vis(Checkbox::class, [
                                'id' => 'innbetalingsbekreftelse-' . $adgangId,
                                'name' => 'adgang[' . $adgangId . '][innbetalingsbekreftelse]',
                                'value' => 1,
                                'label' => 'Jeg ønsker å få tilsendt kvittering for registrerte betalinger.',
                                'checked' => $adgang->hentInnbetalingsbekreftelse()
                            ]),
                            $this->app->vis(Checkbox::class, [
                                'id' => 'forfallsvarsel-' . $adgangId,
                                'name' => 'adgang[' . $adgangId . '][forfallsvarsel]',
                                'value' => 1,
                                'label' => 'Jeg ønsker å motta påminnelse om regninger som er i ferd med å forfalle.',
                                'checked' => $adgang->hentForfallsvarsel()
                            ]),
                            $this->app->vis(Checkbox::class, [
                                'id' => 'umiddelbart_betalingsvarsel_epost-' . $adgangId,
                                'name' => 'adgang[' . $adgangId . '][umiddelbart_betalingsvarsel_epost]',
                                'value' => 1,
                                'label' => 'Jeg ønsker å motta påminnelse umiddelbart ved manglende betaling.',
                                'checked' => $adgang->hentUmiddelbartBetalingsvarselEpost()
                            ]),
                        ]
                    ]));
                }
            }
        }

        $smsReservasjonFelt = $this->app->vis(Field::class, [
            'name' => 'reservasjon_mot_masse_sms',
            'type' => 'hidden',
//            'label' => 'Jeg ønsker å reservere meg mot tekstmeldinger sendt fra ' . $this->app->hentValg('utleier'),
            'value' => $bruker->hent('reservasjon_mot_masse_sms')
        ]);

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/mine-sider/index.php?oppslag=profil_kommunikasjon_preferanser&oppdrag=ta_i_mot_skjema&skjema=preferanser',
                'formId' => 'kommunikasjonspreferanser',
                'buttonText' => 'Lagre preferanser',
                'fields' => [
                    $leieforholdFelter,
                    $this->app->vis(FieldSet::class, [
                        'label' => 'Manuelt utsendt epost',
                        'contents' => [
                            new HtmlElement('div', [], 'I tillegg til automatiske epostvarsler kan det iblant sendes informasjon til grupper av beboere eller til alle beboere.'),
                            new HtmlElement('div', [], 'Det er mulig å reservere seg fra disse epostene, men du risikerer å gå glipp av viktige kunngjøringer.'),
                            $this->app->vis(Checkbox::class, [
                                'name' => 'reservasjon_mot_masseepost',
                                'value' => 1,
                                'label' => 'Jeg ønsker å reservere meg mot informasjons-eposter sendt fra ' . $this->app->hentValg('utleier'),
                                'checked' => $bruker->hent('reservasjon_mot_masseepost')
                            ])
                        ]
                    ]),
                    $smsReservasjonFelt,
                ]
            ])
        ];

        $this->definerData($datasett);
        return parent::forberedData();
    }
}