<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\skademelding_kort\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd\Innlegg;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\html\table\HtmlTable;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *      skade \Kyegil\Leiebasen\Modell\Leieobjekt\Skade
 *  Mulige variabler:
 *      $registrerer
 *      $registrert
 *      $kategorier
 *      $skadebeskrivelse
 *      $bygning
 *      $leieobjektbeskrivelse
 *      $utbedretDato
 *      $sluttregistrerer
 *      $sluttrapport
 *      $saksopplysninger
 *      $kommunikasjonstråd
 *      $endreSkademeldingKnapp
 *      $leggTilSaksopplysningKnapp
 *      $leggTilInnleggKnapp
 *      $utbedreKnapp
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\skademelding_kort\body\main
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/skademelding_kort/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['bruker', 'skade'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Leieobjekt\Skade $skade */
        $skade = $this->hentRessurs('skade');
        if($skade) {
            $bygning = $skade->hentBygning();
            $leieobjekt = $skade->hentLeieobjekt();

            $kategorierLi = new ViewArray();
            /** @var Leieobjekt\Skade\Kategori $kategori */
            foreach($skade->hentKategorier() as $kategori) {
                $kategorierLi->addItem(new HtmlElement('li', [], strtolower($kategori->hentKategori())));
            }

            $datasett = [
                'registrerer' => $skade->hentRegistrererNavn(),
                'registrert' => $skade->hentRegistrert()->format('d.m.Y H:i:s'),
                'kategorier' => new HtmlElement('ul', ['class' => 'tags'], $kategorierLi),
                'skadebeskrivelse' => $skade->hentBeskrivelse(),
                'bygning' => $bygning ? $bygning->hentNavn() : '',
                'leieobjektbeskrivelse' => $leieobjekt ? $leieobjekt->hentBeskrivelse() : '',
                'sluttregistrerer' => $skade->sluttregistrererNavn,
                'utbedretDato' => $skade->utført ? $skade->utført->format('d.m.Y') : '',
                'sluttrapport' => $skade->hentSluttrapport(),
                'saksopplysninger' => $this->hentSaksOpplysninger($skade),
                'kommunikasjonstråd' => $this->hentKommunikasjonstråd($skade),
                'endreSkademeldingKnapp' => $this->hentEndreSkademeldingKnapp($skade),
                'leggTilSaksopplysningKnapp' => $this->hentLeggTilSaksopplysningKnapp($skade),
                'leggTilInnleggKnapp' => $this->hentLeggTilInnleggKnapp($skade),
                'utbedreKnapp' => $this->hentUtbedreKnapp($skade),
                'abonnementStatus' => ''
            ];
            $this->definerData($datasett);
        }
        return parent::forberedData();
    }

    /**
     * @param Leieobjekt\Skade $skade
     * @return Visning|string
     * @throws Exception
     */
    public function hentSaksOpplysninger(Leieobjekt\Skade $skade)
    {
        $rutenett = '';
        /** @var Person $bruker */
        $bruker = $this->hentRessurs('bruker');
        $beboerstøtteSak = $skade->hentBeboerstøtte();
        if($beboerstøtteSak) {
            $privatPerson = $beboerstøtteSak->hentPrivatPerson();
            $tabellLinjer = [];

            /** @var Innlegg $innlegg */
            foreach($beboerstøtteSak->hentInnlegg() as $innlegg) {
                if(!$innlegg->privat || strval($bruker) == strval($privatPerson)) {
                    if($innlegg->saksopplysning) {
                        $tabellLinjer[] = (object)[
                            'tidspunkt' => $innlegg->hentTidspunkt()->format('d.m.Y'),
                            'avsender' => $innlegg->hentAvsender()->hentNavn(),
                            'innhold' => $innlegg->hentInnhold(),
                            'vedlegg' => $innlegg->vedlegg ? new HtmlElement('a', [
                                'href' => '/mine-sider/index.php?oppslag=skademelding_kort&id=' . $skade->hentId() . '&oppdrag=hent_data&data=vedlegg&innlegg=' . $innlegg->hentId(),
                                'title' => 'Last ned'
                            ], new HtmlElement('img', [
                                'src' => '/pub/media/bilder/mine-sider/nedlast-trn.svg',
                                'height' => '35px',
                                'style' => 'height: 35px; background-color: currentColor;'
                            ])) : ''
                        ];
                    }
                }
            }

            if($tabellLinjer) {
                /** @var HtmlTable $rutenett */
                $rutenett = $this->app->hentVisning(HtmlTable::class, [
                    'caption' => 'Saksopplysninger',
                    'kolonner' => [
                        (object)[
                            'title' => 'Tidspunkt'
                        ],
                        (object)[
                            'title' => 'Avsender'
                        ],
                        (object)[
                            'title' => 'Saksopplysning'
                        ],
                        (object)[
                            'title' => 'Vedlegg'
                        ]
                    ],
                    'data' => $tabellLinjer
                ]);
            }
        }

        return $rutenett;
    }

    /**
     * @param Leieobjekt\Skade $skade
     * @return Visning|string
     * @throws Exception
     */
    public function hentKommunikasjonstråd(Leieobjekt\Skade $skade)
    {
        $rutenett = '';
        /** @var Person $bruker */
        $bruker = $this->hentRessurs('bruker');
        $beboerstøtteSak = $skade->hentBeboerstøtte();
        if($beboerstøtteSak) {
            $privatPerson = $beboerstøtteSak->hentPrivatPerson();
            $tabellLinjer = [];

            /** @var Innlegg $innlegg */
            foreach($beboerstøtteSak->hentInnlegg() as $innlegg) {
                if(!$innlegg->privat || strval($bruker) == strval($privatPerson)) {
                    if(!$innlegg->saksopplysning) {
                        $tabellLinjer[] = (object)[
                            'tidspunkt' => $innlegg->hentTidspunkt()->format('d.m.Y'),
                            'avsender' => $innlegg->hentAvsender()->hentNavn(),
                            'innhold' => $innlegg->hentInnhold(),
                            'vedlegg' => $innlegg->vedlegg ? new HtmlElement('a', [
                                'href' => '/mine-sider/index.php?oppslag=skademelding_kort&id=' . $skade->hentId() . '&oppdrag=hent_data&data=vedlegg&innlegg=' . $innlegg->hentId(),
                                'title' => 'Last ned'
                            ], new HtmlElement('img', [
                                'src' => '/pub/media/bilder/mine-sider/nedlast-trn.svg',
                                'height' => '35px',
                                'style' => 'height: 35px; background-color: currentColor;'
                            ])) : ''
                        ];
                    }
                }
            }

            if($tabellLinjer) {
                /** @var HtmlTable $rutenett */
                $rutenett = $this->app->hentVisning(HtmlTable::class, [
                    'caption' => 'Annen kommunikasjon',
                    'kolonner' => [
                        (object)[
                            'title' => 'Tidspunkt'
                        ],
                        (object)[
                            'title' => 'Avsender'
                        ],
                        (object)[
                            'title' => 'Innhold'
                        ],
                        (object)[
                            'title' => 'Vedlegg'
                        ]
                    ],
                    'data' => $tabellLinjer
                ]);
            }
        }

        return $rutenett;
    }

    /**
     * @param Leieobjekt\Skade $skade
     * @return Visning|string
     * @throws Exception
     */
    public function hentEndreSkademeldingKnapp(Leieobjekt\Skade $skade)
    {
        if($skade->utført) {
            return '';
        }
        return new HtmlElement(
            'a',
            [
                'href' => "/mine-sider/index.php?oppslag=skademelding_skjema&id={$skade}",
                'class' => 'button',
                'title' => 'Endre skademeldingen',
            ],
            'Endre skademeldingen'
        );
    }

    /**
     * @param Leieobjekt\Skade $skade
     * @return Visning|string
     * @throws Exception
     */
    public function hentLeggTilSaksopplysningKnapp(Leieobjekt\Skade $skade)
    {
        if($skade->utført) {
            return '';
        }
        return new HtmlElement(
            'a',
            [
                'href' => "/mine-sider/index.php?oppslag=skademelding_saksopplysning_skjema&id={$skade}",
                'class' => 'button',
                'title' => 'Legg til saksopplysning',
            ],
            'Legg til saksopplysning'
        );
    }

    /**
     * @param Leieobjekt\Skade $skade
     * @return Visning|string
     * @throws Exception
     */
    public function hentLeggTilInnleggKnapp(Leieobjekt\Skade $skade)
    {
        if($skade->utført) {
            return '';
        }
        return new HtmlElement(
            'a',
            [
                'href' => "/mine-sider/index.php?oppslag=skademelding_oppdatering_skjema&id={$skade}",
                'class' => 'button',
                'title' => 'Legg til oppdatering',
            ],
            'Legg til oppdatering'
        );
    }

    /**
     * @param Leieobjekt\Skade $skade
     * @return Visning|string
     * @throws Exception
     */
    public function hentUtbedreKnapp(Leieobjekt\Skade $skade)
    {
        if($skade->utført) {
            return '';
        }
        return new HtmlElement(
            'a',
            [
                'href' => "/mine-sider/index.php?oppslag=skademelding_utbedring_skjema&id={$skade}",
                'class' => 'button',
                'title' => 'Rapportér skaden som utbedret',
            ],
            'Rapportér skaden som utbedret'
        );
    }
}