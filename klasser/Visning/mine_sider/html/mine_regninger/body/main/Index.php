<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\mine_regninger\body\main;


use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\mine_sider\html\mine_regninger\body\main\index\Utestående;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *  Mulige variabler:
 *      utestående \Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_preferanser\body\main\index\Utestående
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_preferanser\body\main
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/mine_regninger/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'bruker') {
            return $this->settRessurs('bruker', $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Person $bruker */
        $bruker = $this->hentRessurs('bruker');
        $this->definerData([
            'utestående' => $this->app->vis(Utestående::class, [
                'bruker' => $bruker
            ])
        ]);
        return parent::forberedData();
    }
}