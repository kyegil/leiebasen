<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\mine_regninger\body\main\index\utestående\leieforhold;


use Exception;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      krav \Kyegil\Leiebasen\Modell\Leieforhold\Krav
 *  Mulige variabler:
 *      $tekst
 *      $regningsnr
 *      $forfallsdato
 *      $utestående
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_preferanser\body\main\index\utestående\leieforhold
 */
class Krav extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/mine_regninger/body/main/index/utestående/leieforhold/Krav.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'krav') {
            return $this->settRessurs('krav', $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold\Krav $krav */
        $krav = $this->hentRessurs('krav');

        $datasett = [
            'tekst' => $krav->tekst,
            'forfallsdato' => $krav->forfall ? $krav->forfall->format('d.m.Y') : '',
            'regningsnr' => strval($krav->regning),
            'utestående' => $this->app->kr($krav->utestående)
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}