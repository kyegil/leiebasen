<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\mine_regninger\body\main\index;


use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_preferanser\body\main\index
 */
class Utestående extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/mine_regninger/body/main/index/Utestående.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return Utestående
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'bruker') {
            return $this->settRessurs('bruker', $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return Utestående
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Person $bruker */
        $bruker = $this->hentRessurs('bruker');
        $adganger = $bruker->hentAdganger()->leggTilFilter([
            'adgang' => "mine-sider"
        ]);
        $uteståendeLeieforhold = [];
        /** @var Person\Adgang $adgang */
        foreach($adganger as $adgang) {
            $leieforhold = $adgang->leieforhold;
            if($leieforhold && $leieforhold->hentUtestående()) {
                $uteståendeLeieforhold[] = $this->app->vis(utestående\Leieforhold::class,[
                    'leieforhold' => $leieforhold
                ]);
            }
        }

        $datasett = [
            'uteståendeLeieforhold' => $uteståendeLeieforhold ? new ViewArray($uteståendeLeieforhold) : ''
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}