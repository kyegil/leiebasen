<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\mine_regninger\body\main\index\utestående;


use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $leieforholdId
 *      $leieforholdBeskrivelse
 *      $uteståendeRegninger
 *      $sumUtestående
 *      $visBetalingsplanKnapp
 *      $lagBetalingsplanKnapp
 *      $betalingutsettelseKnapp
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_preferanser\body\main\index\utestående
 */
class Leieforhold extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/mine_regninger/body/main/index/utestående/Leieforhold.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return Leieforhold
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'leieforhold') {
            return $this->settRessurs('leieforhold', $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return Leieforhold
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        $uteståendeRegninger = [];
        $sumUtestående = 0;

        $uteståendeKrav = $leieforhold->hentKrav();
        $uteståendeKrav->leggTilLeftJoinForRegning();
        $uteståendeKrav->leggTilFilter([
            'krav.utestående >' => 0,
            'or' => [
                'krav.kravdato <= NOW()',
                '`' . Regning::hentTabell() . '`.`utskriftsdato`',
                ]
        ]);

        /** @var Krav $krav */
        foreach($uteståendeKrav as $krav) {
            if ($krav->hentRegning()) {
                $uteståendeRegninger [] = $this->app->vis(leieforhold\Krav::class, [
                    'krav' => $krav
                ]);
                $sumUtestående += $krav->hentUtestående();
            }
        }
        $visBetalingsplanKnapp = '';
        $lagBetalingsplanKnapp = '';
        if($sumUtestående > 0) {
            if($leieforhold->hentAktivBetalingsplan()) {
                $aktivBetalingsplanNotat = $leieforhold->hentAktivBetalingsplan()->hentNotat();
                if($leieforhold->hentAktivBetalingsplan()) {
                    $visBetalingsplanKnapp = $this->hentLinkKnapp('/mine-sider/index.php?oppslag=notat&id=' . $aktivBetalingsplanNotat->hentId(), 'Vis nedbetalingsavtale', 'Vis aktiv betalingsavtale');
                }
            }
            else {
                $lagBetalingsplanKnapp = $this->hentLinkKnapp("/mine-sider/index.php?oppslag=betalingsplan_skjema&leieforhold={$leieforhold->hentId()}&id=*", 'Opprett nedbetalingsplan ...', 'Opprett avtale om nedbetaling av utestående');
            }
        }
        $betalingutsettelseKnapp = $this->hentLinkKnapp("/mine-sider/index.php?oppslag=betalingsutsettelse_skjema&leieforhold={$leieforhold->hentId()}&id=*", 'Betalingsutsettelse ...', 'Du har anleding til å utsette betaling på én eller flere regninger');

        $datasett = [
            'leieforholdId' => $leieforhold->hentId(),
            'leieforholdBeskrivelse' => $leieforhold->hentBeskrivelse(),
            'uteståendeRegninger' => new ViewArray($uteståendeRegninger),
            'sumUtestående' => $this->app->kr($sumUtestående),
            'visBetalingsplanKnapp' => $visBetalingsplanKnapp,
            'lagBetalingsplanKnapp' => $lagBetalingsplanKnapp,
            'betalingutsettelseKnapp' => $betalingutsettelseKnapp,
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}