<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\skade_leieobjekt_abonnement_skjema\body\main;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\auto_complete\Leieobjekter as LeieobjekterAutoComplete;
use Kyegil\Leiebasen\Visning\felles\html\form\auto_complete\Bygninger as BygningerAutoComplete;
use Kyegil\Leiebasen\Visning\felles\html\form\auto_complete\Områder as OmråderAutoComplete;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Visning for brukerprofil-skjema i mine sider
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *  Mulige variabler:
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\skade_leieobjekt_abonnement_skjema\body\main
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/skade_leieobjekt_abonnement_skjema/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['bruker'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Person $bruker */
        $bruker = $this->hentRessurs('bruker');

        $skadeAutoAbonnement = $bruker->hentBrukerProfilPreferanse('skade_auto_abonnement') ?? (object)[
            'leieobjekter' => [],
            'bygninger' => [],
            'områder' => [],
        ];
        $leieobjektIder = $skadeAutoAbonnement->leieobjekter ?? [];
        $bygningsIder = $skadeAutoAbonnement->bygninger ?? [];
        $områdeIder = $skadeAutoAbonnement->områder ?? [];

        $leieobjekterFelt = $this->app->vis(LeieobjekterAutoComplete::class, [
            'value' => $leieobjektIder
        ]);
        $bygningerFelt = $this->app->vis(BygningerAutoComplete::class, [
            'value' => $bygningsIder
        ]);
        $områderFelt = $this->app->vis(OmråderAutoComplete::class, [
            'value' => $områdeIder,
            'label' => 'Motta skademeldinger for hele områder (Inkluderer alle bygninger og leieobjekter i hvert område)'
        ]);

        $tekst = new HtmlElement('div', [], 'Du vil motta epost dersom det meldes inn skader i noen av leieobjektene eller bygningene under.');

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/mine-sider/index.php?oppslag=skade_leieobjekt_abonnement_skjema&oppdrag=ta_i_mot_skjema&skjema=abonnement',
                'formId' => 'skade_leieobjekt_abonnement_skjema',
                'buttonText' => 'Lagre endringer',
                'fields' => [
                    $tekst,
                    $leieobjekterFelt,
                    $bygningerFelt,
                    $områderFelt,
                ]
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}