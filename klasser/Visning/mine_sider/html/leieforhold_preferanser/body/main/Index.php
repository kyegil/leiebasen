<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_preferanser\body\main;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\html\form\RadioButtonGroup;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\Leiebasen\Visning\felles\html\person\Adressefelt;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $leieforholdId
 *      $form
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_preferanser\body\main
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/leieforhold_preferanser/body/main/Index.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return Index
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null): Index
    {
        if($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Leieforhold $leieforhold
     * @return $this
     * @throws \Exception
     */
    protected function settLeieforhold($leieforhold)
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $this->settRessurs('leieforhold', $leieforhold);
        return $this;
    }


    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        /** @var RadioButtonGroup $giroformatFelt */
        $giroformatFelt = $this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\form\RadioButtonGroup::class, [
            'id' => 'giroformat',
            'name' => 'giroformat',
            'label' => 'Regningsformat',
            'value' => $leieforhold->hentGiroformat() ?? '',
            'options' => (object)['epost' => 'E-post', 'papir' => 'Papir']
        ]);
        /** @var Checkbox $regningTilObjektFelt */
        $regningTilObjektFelt = $this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\form\Checkbox::class, [
            'id' => 'regningTilObjekt-velger',
            'name' => 'regning_til_objekt',
            'label' => 'Regninger deles ut lokalt',
            'checked' => $leieforhold->hentRegningTilObjekt()
        ]);
        /** @var Select $regningsobjektFelt */
        $regningsobjektFelt = $this->app->vis(Select::class, [
            'id' => 'regningsobjekt-velger',
            'name' => 'regningsobjekt',
            'label' => 'Intern levering av giroer etc til',
            'value' => $leieforhold->hentRegningsobjekt(),
            'options' => $this->hentLeieobjektMuligheter()
        ]);
        $regningspersonFelt = $this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\form\RadioButtonGroup::class, [
            'id' => 'regningsperson-velger',
            'name' => 'regningsperson',
            'label' => 'Regningsadresse',
            'value' => $leieforhold->hentRegningsperson() ? (int)$leieforhold->hentRegningsperson()->hentId() : 0,
            'options' => $this->hentRegningspersonMuligheter($leieforhold)
        ]);

        $regningsadresseFelt = $this->app->vis(FieldSet::class, [
            'id' => 'regningsadresse-felter',
            'label' => 'Send til egen regningsadresse',
            'contents' => [
                $this->app->vis(Field::class, [
                    'name' => 'regningsadresse1',
                    'label' => 'Adresse',
                    'value' => $leieforhold->hentRegningsadresse1()
                ]),
                $this->app->vis(Field::class, [
                    'name' => 'regningsadresse2',
                    'value' => $leieforhold->hentRegningsadresse2()
                ]),
                $this->app->vis(Field::class, [
                    'name' => 'postnr',
                    'label' => 'Postnr',
                    'value' => $leieforhold->hentPostnr()
                ]),
                $this->app->vis(Field::class, [
                    'name' => 'poststed',
                    'label' => 'Sted',
                    'value' => $leieforhold->hentPoststed()
                ]),
                $this->app->vis(Field::class, [
                    'name' => 'land',
                    'label' => 'Land',
                    'value' => $leieforhold->hentLand()
                ])
            ]
        ]);

        $datasett = [
            'leieforholdId' => $leieforhold->hentId(),
            'form' => $this->app->vis(AjaxForm::class, [
                'action' => '/mine-sider/index.php?oppslag=leieforhold_preferanser&oppdrag=ta_i_mot_skjema&skjema=preferanseskjema&id=' . $leieforhold->hentId(),
                'formId' => 'preferanseskjema',
                'buttonText' => 'Lagre valgene',
                'fields' => [
                    $giroformatFelt,
                    $regningTilObjektFelt,
                    $regningsobjektFelt,
                    $regningspersonFelt,
                    $regningsadresseFelt
                ]
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return \stdClass
     * @throws \Exception
     */
    private function hentLeieobjektMuligheter()
    {
        $resultat = new \stdClass();
        /** @var Leieobjekt $leieobjekt */
        foreach($this->app->hentSamling(Leieobjekt::class)->leggTilSortering('navn') as $leieobjekt) {
            $resultat->{$leieobjekt->hentId()} = $leieobjekt->hentBeskrivelse();
        }
        return $resultat;
    }

    /**
     * @param \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold
     * @return \stdClass
     */
    private function hentRegningspersonMuligheter($leieforhold)
    {
        $resultat = new \stdClass();
        /** @var Leieforhold\Leietaker $leietaker */
        foreach($leieforhold->hentLeietakere() as $leietaker) {
            /** @var \Kyegil\Leiebasen\Modell\Person|null $person */
            $person = $leietaker->hentPerson();
            if(!$leietaker->slettet && $person) {
                $adresse = $this->app->vis(Adressefelt::class, [
                    'navn' => new HtmlElement('em', [], Leiebase::genitivApostrof($person->hentNavn()) . ' adresse:'),
                    'person' => $person
                ])->settMal('felles/html/person/Adressefelt-Inline.html');
                $resultat->{$person->hentId()} = $adresse;
            }
        }
        $resultat->{0} = "Oppgi en uavhengig regningsadresse for leieforholdet";
        return $resultat;
    }
}