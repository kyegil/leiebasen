<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 28/07/2020
 * Time: 12:46
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\oversikt\body\main;

use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *      leieobjekt \Kyegil\Leiebasen\Modell\Leieobjekt
 *  Mulige variabler:
 *      $head HTML head
 *      $header
 *      $hovedmeny
 *      $main
 *      $footer
 *      $scripts Script satt inn til slutt før </body>
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\oversikt\body\main
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/oversikt/body/main/Index.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(is_string($attributt) && in_array($attributt, ['bruker', 'leieforhold', 'leieobjekt'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     */
    protected function forberedData()
    {
        /** @var Person $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        /** @var Leieobjekt|null $leieobjekt */
        $leieobjekt = $this->hentRessurs('leieobjekt');

        $leieforholdId = $leieforhold ? $leieforhold->hentId() : '';
        $leieobjektId = $leieobjekt ? $leieobjekt->hentId() : '';
        $this->definerData([
            'leieforholdId' => $leieforholdId,
            'leieobjektId' => $leieobjektId
        ]);
        return parent::forberedData();
    }
}