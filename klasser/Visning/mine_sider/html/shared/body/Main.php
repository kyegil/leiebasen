<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 28/07/2020
 * Time: 12:46
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\shared\body;


use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Visning for main content
 *
 *  Mulige variabler:
 *      $tittel
 *      $innhold
 *      $breadcrumbs
 *      $varsler
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\shared
 */
class Main extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/shared/body/Main.html';

    /**
     * @return Main
     */
    protected function forberedData()
    {
        $this->definerData([
            'tittel' => '',
            'innhold' => '',
            'breadcrumbs' => '',
            'varsler' => '',
            'knapper' => ''
        ]);
        return parent::forberedData();
    }

}