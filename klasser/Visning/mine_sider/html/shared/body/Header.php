<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 28/07/2020
 * Time: 12:46
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\shared\body;


use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Visning for header i mine sider
 *
 *  Mulige variabler:
 *      $språkvelger
 *      $logo
 *      $logoWidth
 *      $logoHeight
 *      $navn
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\shared
 */
class Header extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/shared/body/Header.html';

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $this->definerData([
            'språkvelger' => '',
            'logo' => '/pub/media/bilder/offentlig/blank.png',
            'logoWidth' => 1,
            'logoHeight' => 1,
            'navn' => $this->app->hentValg('utleier')
        ]);
        return parent::forberedData();
    }

}