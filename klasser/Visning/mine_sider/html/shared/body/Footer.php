<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 28/07/2020
 * Time: 12:46
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\shared\body;


use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\AppInterface;

/**
 * Visning for footer i mine sider
 *
 *  Mulige variabler:
 *      $head
 *      $menu
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\shared
 */
class Footer extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/shared/body/Footer.html';

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $this->definerData([
            'head' => '',
            'menu' => ''
        ]);
        return parent::forberedData();
    }

}