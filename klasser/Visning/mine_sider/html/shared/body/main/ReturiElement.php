<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 28/07/2020
 * Time: 12:46
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\shared\body\main;


use Kyegil\Leiebasen\Visning\MineSider;

class ReturiElement extends MineSider
{
    protected $template = 'mine_sider/html/shared/body/main/ReturiElement.html';
}