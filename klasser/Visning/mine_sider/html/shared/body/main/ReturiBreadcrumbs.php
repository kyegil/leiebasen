<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 28/07/2020
 * Time: 12:46
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\shared\body\main;


use Kyegil\Leiebasen\Visning\MineSider;

class ReturiBreadcrumbs extends MineSider
{
    protected $template = 'mine_sider/html/shared/body/main/ReturiBreadcrumbs.html';
}