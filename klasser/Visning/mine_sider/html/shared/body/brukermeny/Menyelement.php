<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\shared\body\brukermeny;


use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for hovedmeny
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *      nodeObjekt object
 *  Mulige variabler:
 *      $brukernavn
 *      $menyNivå
 *      $id
 *      $text
 *      $url
 *      $active
 *      $nåværende
 *      $symbol
 *      $xtraClasses
 *      $items
 *      $nyGruppe
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\shared\body\brukermeny
 */
class Menyelement extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/shared/body/brukermeny/Menyelement.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null): Menyelement
    {
        if(in_array($attributt, ['bruker', 'nodeObjekt'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     */
    protected function forberedData(): Menyelement
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        $nodeObjekt = $this->hentRessurs('nodeObjekt');
        $menyNivå = $this->hent('menyNivå') ?? 0;
        $brukernavn = $bruker ? $bruker->hentNavn() : '';
        $nyGruppe = false;

        $items = new ViewArray();
        foreach($nodeObjekt->items ?? [] as $underNode) {
            if(is_object($underNode)) {
                $item = $this->app->vis(Menyelement::class, [
                    'bruker' => $bruker,
                    'nodeObjekt' => $underNode,
                    'menyNivå' => $menyNivå + 1,
                    'nyGruppe' => $nyGruppe,
                ]);
                $nyGruppe = false;
            }
            elseif ($underNode === '-') {
                $nyGruppe = true;
                continue;
            }
            $items->addItem($item);
        }

        $this->definerData([
            'brukernavn' => $brukernavn,
            'menyNivå' => $menyNivå,
            'id' => $nodeObjekt && isset($nodeObjekt->id) ? $nodeObjekt->id : '',
            'text' => $nodeObjekt && isset($nodeObjekt->text) ? $nodeObjekt->text : '',
            'url' => $nodeObjekt && isset($nodeObjekt->url) ? $nodeObjekt->url : '',
            'active' => $nodeObjekt->active ?? false,
            'nåværende' => $nodeObjekt->nåværende ?? false,
            'symbol' => $nodeObjekt && isset($nodeObjekt->symbol) ? $nodeObjekt->symbol : '',
            'extraClasses' => $nodeObjekt && isset($nodeObjekt->extraClasses) ? implode(' ', $nodeObjekt->extraClasses) : '',
            'items' => $items,
            'nyGruppe' => false,
        ]);
        return parent::forberedData();
    }

}