<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\shared\body;


use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\body\brukermeny\Menyelement;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for brukermeny
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *      menyStruktur object[]
 *  Mulige variabler:
 *      $brukernavn
 *      $items
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\shared
 */
class Brukermeny extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/shared/body/Brukermeny.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['bruker', 'menyStruktur'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     */
    protected function forberedData(): Brukermeny
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker') ?? ($this->app->hoveddata['bruker'] ?? null);
        $menyStruktur = $this->hentRessurs('menyStruktur');
        $brukernavn = $bruker ? $bruker->hentNavn() : '';
        $menyStruktur = $menyStruktur ?? $this->app->hentMenyStruktur('brukermeny', $bruker);

        $items = new ViewArray();
        foreach($menyStruktur as $nodeObjekt) {
            $items->addItem($this->app->vis( Menyelement::class, ([
                'brukernavn' => $brukernavn,
                'menyNivå' => 0,
                'nodeObjekt' => $nodeObjekt
            ])));
        }

        $this->definerData([
            'brukernavn' => $brukernavn,
            'items' => $items,
        ]);
        return parent::forberedData();
    }
}