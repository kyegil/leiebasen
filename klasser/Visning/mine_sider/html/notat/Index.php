<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\notat;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for notat i mine_sider
 *
 * Ressurser:
 * *   notat \Kyegil\Leiebasen\Modell\Leieforhold\Notat
 *
 * Tilgjengelige variabler:
 * * $notatId
 * * $aktivitet
 * * $notatTekst
 * * $registrert
 * * $skjulForLeietaker
 * * $brevTekst
 * * $knapper
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\notat
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/notat/Index.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'notat') {
            return $this->settNotat($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Notat|null $notat
     * @return $this
     */
    protected function settNotat(?Notat $notat): Index
    {
        $this->settRessurs('notat', $notat);
        return $this;
    }

    /**
     * @param Leieforhold|null $leieforhold
     * @return $this
     */
    protected function settLeieforhold(?Leieforhold $leieforhold): Index
    {
        $this->settRessurs('leieforhold', $leieforhold);
        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Notat|null $notat */
        $notat = $this->hentRessurs('notat');
        $knapper = [];

        if ($notat) {
            $notatTekst = $this->hentNotatTekst($notat);
            if($notat->hentBrevtekst()) {
                $knapper[] = new HtmlElement('button', [
                    'title' => 'Skriv ut brevet',
                    'onclick' => 'leiebasen.mineSider.notat.visBrev()'
                ], 'Skriv ut brevet');
            }
            if($notat->hentVedlegg()) {
                $knapper[] = new HtmlElement('button', [
                    'title' => 'Last ned vedlegg',
                    'onclick' => 'leiebasen.mineSider.notat.lastVedlegg()'
                ], 'Last ned vedlegg');
            }

//            $knapper[] = $this->hentLinkKnapp('/mine-sider/index.php?oppslag=notat_skjema&id=' . $notat->hentId(), 'Endre notatet', 'Endre');

            $datasett = [
                'notatId' => $notat->id,
                'leieforholdBeskrivelse' => $notat->leieforhold->hentBeskrivelse(),
                'frosset' => $notat->leieforhold->frosset,
                'stoppOppfølging' => $notat->leieforhold->stoppOppfølging,
                'avventOppfølging' => $notat->leieforhold->avventOppfølging > date_create() ? $notat->leieforhold->avventOppfølging->format('d.m.Y') : '',
                'dato' => $notat->dato->format('d.m.Y'),
                'aktivitet' => $this->hentAktivitetsBeskrivelse(),
                'notatTekst' => $notatTekst,
                'registrert' => $notat->registrert->format('d.m.Y H:i:s') . ' av ' . $notat->registrerer,
                'skjulForLeietaker' => $notat->skjulForLeietaker,
                'brevTekst' => $notat->brevtekst,
                'eksterntDokument' => $this->hentEksterntDokument(),
                'vedlegg' => $notat->hentVedleggsnavn(),
                'knapper' => $knapper,
            ];
        }
        else {
            $datasett = [
                'notatId' => '',
                'leieforholdBeskrivelse' => '',
                'dato' => '',
                'aktivitet' => '',
                'notatTekst' => '',
                'registrert' => '',
                'skjulForLeietaker' => '',
                'brevTekst' => '',
                'eksterntDokument' => '',
                'vedlegg' => '',
                'knapper' => [],
            ];
        }
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return string
     */
    private function hentAktivitetsBeskrivelse()
    {
        /** @var Notat|null $notat */
        $notat = $this->hentRessurs('notat');

        switch ($notat->hentKategori()) {
            case '':
                if ($notat->hentHenvendelseFra()) {
                    if ($notat->hentHenvendelseFra() == 'fra utleier') {
                        $beskrivelse = 'Henvendelse fra ' . $this->app->hentValg('utleier');
                    }
                    else {
                        $beskrivelse = 'Henvendelse mottatt ' . $notat->hentHenvendelseFra();
                    }
                }
                break;
            case 'brev':
                $kategorier = Notat::hentKategorier();
                $beskrivelse = ucfirst(property_exists($kategorier, $notat->hentKategori())
                    ? $kategorier->{$notat->hentKategori()}
                    : $notat->hentKategori()
                );
                if ($notat->hentHenvendelseFra()) {
                    if ($notat->hentHenvendelseFra() == 'fra utleier') {
                        $beskrivelse .= ' fra ' . $this->app->hentValg('utleier');
                    }
                    else {
                        $beskrivelse .= ' mottatt ' . $notat->hentHenvendelseFra();
                    }
                }
                break;
            default:
                $kategorier = Notat::hentKategorier();
                $beskrivelse = \Kyegil\Leiebasen\Leiebase::ucfirst(property_exists($kategorier, $notat->hentKategori())
                    ? $kategorier->{$notat->hentKategori()}
                    : $notat->hentKategori()
                );

                break;
        }
        return $beskrivelse;
    }

    /**
     * @return string
     */
    private function hentEksterntDokument()
    {
        /** @var Notat|null $notat */
        $notat = $this->hentRessurs('notat');

        $beskrivelse = [];
        if($notat->hentDokumenttype()) {
            $beskrivelse[] = \Kyegil\Leiebasen\Leiebase::ucfirst($notat->hentDokumenttype());
        }
        if($notat->hentDokumentreferanse()) {
            $beskrivelse[] = 'ref: ' . $notat->hentDokumentreferanse();
        }
        return implode(', ', $beskrivelse);
    }

    /**
     * @param Notat|null $notat
     * @return string|null
     */
    private function hentNotatTekst(?Notat $notat): ?string
    {
        if(!$notat) {
            return '';
        }
        switch($notat->kategori) {
            case $notat::KATEGORI_BETALINGSPLAN:
                $leieforhold = $notat->leieforhold;
                $betalingsplanForslag = $leieforhold->hentBetalingsplanForslag();
                $betalingsplan = $leieforhold->hentBetalingsplan();
                if(!$betalingsplan
                    || !$betalingsplan->hentNotat()
                    || $betalingsplan->hentNotat()->hentId() != $notat->hentId()
                ) {
                    if($betalingsplanForslag && isset($betalingsplanForslag->notat_id) && $betalingsplanForslag->notat_id == $notat->hentId()) {
                        $melding = [
                            'Dette er en <i>foreslått</i> betalingsplan. Planen har så langt ikke blitt godkjent.<br>',
                            'Klikk ' . new HtmlElement('a', [
                                'href' => '/mine-sider/index.php?oppslag=betalingsplan_skjema&leieforhold=' . $leieforhold->hentId() . '&id=*',
                                'title' => 'Endre forslaget'
                            ], 'her') .' for å sende et oppdatert forslag til betalingsplan.<br>'
                        ];
                    }
                    else {
                        $melding = [
                            'Dette notatet gjelder en <i>inaktiv</i> betalingsplan.<br>',
                            'Klikk ' . new HtmlElement('a', [
                                'href' => '/mine-sider/index.php?oppslag=betalingsplan_skjema&leieforhold=' . $leieforhold->hentId() . '&id=*',
                                'title' => 'Foreslå en ny betalingsplan'
                            ], 'her') .' for å foreslå en ny betalingsplan.<br>'
                        ];
                    }

                    if ($betalingsplan && $betalingsplan->hentNotat() && $betalingsplan->hentNotat()->hentId()) {
                        $melding[] = 'Se ' . new HtmlElement('a', [
                                'href' => '/mine-sider/index.php?oppslag=notat&id=' . $betalingsplan->hentNotat()->hentId(),
                                'title' => 'Gå til gjeldende betalingsplan'
                            ], 'gjeldende betalingsplan') . '. ';
                    }

                    $notatTekst = new ViewArray([new HtmlElement('div', ['class' => 'obs'], $melding)]);
                    $notatTekst->addItem($notat->notat);
                    return $notatTekst;
                }
                if(!$betalingsplan->aktiv
                ) {
                    $melding = ['OBS! Betalingsplanen er ikke aktiv.'];
                    $notatTekst = new ViewArray([new HtmlElement('div', ['class' => 'obs'], $melding)]);
                    $notatTekst->addItem($notat->notat);
                    return $notatTekst;
                }
                return '';
            default:
                return $notat->notat;
        }
    }
}