<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\skademelding_skjema\body\main;


use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\AutoComplete;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\HtmlEditor;
use Kyegil\Leiebasen\Visning\MineSider;
use stdClass;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      skade \Kyegil\Leiebasen\Modell\Leieobjekt\Skade
 *      leieobjekt \Kyegil\Leiebasen\Modell\Leieobjekt
 *      bygning \Kyegil\Leiebasen\Modell\Leieobjekt\Bygning
 *  Mulige variabler:
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\skademelding_skjema\body\main
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/skademelding_skjema/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['skade', 'leieobjekt', 'bygning'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Leieobjekt\Skade $skade */
        $skade = $this->hentRessurs('skade');
        /** @var Leieobjekt $leieobjekt */
        $leieobjekt = $this->hentRessurs('leieobjekt');
        /** @var Bygning $bygning */
        $bygning = $this->hentRessurs('bygning');
        $kategorier = [];
        if($skade) {
            /** @var Leieobjekt\Skade\Kategori $kategori */
            foreach($skade->hentKategorier() as $kategori) {
                $kategorier[] = strtolower($kategori->hentKategori());
            }
        }

        /** @var AutoComplete $leieobjektFelt */
        $leieobjektFelt = $this->app->vis(AutoComplete::class, [
            'id' => 'leieobjekt-velger',
            'name' => 'leieobjekt',
            'label' => 'Leieobjekt',
            'value' => $leieobjekt,
            'options' => $this->hentLeieobjektMuligheter(),
            'required' => true,
            'hidden' => !$leieobjekt && $bygning,
            'disabled' => !$leieobjekt && $bygning
        ]);

        /** @var AutoComplete $bygningsFelt */
        $bygningsFelt = $this->app->vis(AutoComplete::class, [
            'id' => 'bygning-velger',
            'name' => 'bygning',
            'label' => 'Bygning',
            'value' => $bygning,
            'options' => $this->hentBygningsMuligheter(),
            'required' => true,
            'hidden' => $leieobjekt || !$bygning,
            'disabled' => $leieobjekt || !$bygning
        ]);

        /** @var Checkbox $regningTilObjektFelt */
        $gjelderBygningFelt = $this->app->vis(Checkbox::class, [
            'id' => 'gjelderBygning-velger',
            'name' => 'gjelder_bygning',
            'label' => 'Skaden er ikke begrenset til ett enkelt leieobjekt, men omfatter bygningen.',
            'checked' => !$leieobjekt && $bygning
        ]);

        /** @var Field $skadeFelt */
        $skadeFelt = $this->app->vis(Field::class, [
            'name' => 'skade',
            'label' => 'Overskrift / Oppsummerende navn på skade',
            'required' => true,
            'value' => $skade ? $skade->skade : null
        ]);

        /** @var AutoComplete $kategoriFelt */
        $kategoriFelt = $this->app->vis(AutoComplete::class, [
            'id' => 'kategori-velger',
            'name' => 'kategorier',
            'label' => 'Kategorier (Du kan skrive inn nye kategorier om det trengs)',
            'value' => $kategorier,
            'multiple' => true,
            'forceSelection' => false,
            'tokenSeparators' => [',', ' '],
            'options' => $this->hentEksisterendeKategorier()
        ]);

        /** @var HtmlEditor $detaljFelt */
        $detaljFelt = $this->app->vis(HtmlEditor::class, [
            'id' => 'skademelding-beskrivelse',
            'name' => 'beskrivelse',
            'label' => 'Detaljer',
            'value' => $skade ? $skade->hentBeskrivelse() : null
        ]);

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/mine-sider/index.php?oppslag=skademelding_skjema&oppdrag=ta_i_mot_skjema&skjema=skademelding&id=' . ($skade ? $skade->hentId() : '*'),
                'formId' => 'skademelding',
                'buttonText' => $skade ?  'Lagre' : 'Registrer skademeldingen',
                'fields' => [
                    $leieobjektFelt,
                    $bygningsFelt,
                    $gjelderBygningFelt,
                    $skadeFelt,
                    $kategoriFelt,
                    $detaljFelt
                ]
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    private function hentLeieobjektMuligheter()
    {
        $resultat = new stdClass();
        /** @var Leieobjekt $leieobjekt */
        foreach($this->app->hentSamling(Leieobjekt::class)
                    ->leggTilLeftJoin('utdelingsorden', 'utdelingsorden.leieobjekt = leieobjekter.leieobjektnr AND utdelingsorden.rute = 1')
                    ->leggTilSortering('utdelingsorden.plassering')
                as $leieobjekt) {
            $resultat->{$leieobjekt->hentId()} = $leieobjekt->hentId() . ': ' . $leieobjekt->hentBeskrivelse();
        }
        return $resultat;
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    private function hentBygningsMuligheter()
    {
        $resultat = new stdClass();
        /** @var Bygning $bygning */
        foreach($this->app->hentSamling(Bygning::class)
                    ->leggTilSortering('kode')
                as $bygning) {
            $resultat->{$bygning->hentId()} = $bygning->hentNavn();
        }
        return $resultat;
    }

    /**
     * Henter alle eksisterende skadekategorier
     *
     * @return string[]
     * @throws Exception
     */
    private function hentEksisterendeKategorier()
    {
        return $this->app->mysqli->arrayData([
            'source' => Leieobjekt\Skade\Kategori::hentTabell(),
            'fields' => ['kategori' => 'LOWER(kategori)'],
            'orderfields' => 'kategori',
            'distinct' => true,
            'flat' => true
        ])->data;
    }
}