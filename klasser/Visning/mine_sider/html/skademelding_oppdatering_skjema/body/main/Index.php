<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\skademelding_oppdatering_skjema\body\main;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieobjekt\Skade;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FileField;
use Kyegil\Leiebasen\Visning\felles\html\form\HtmlEditor;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *      skade \Kyegil\Leiebasen\Modell\Leieobjekt\Skade
 *  Mulige variabler:
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\skademelding_oppdatering_skjema\body\main
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/skademelding_oppdatering_skjema/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['bruker', 'skade'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Skade $skade */
        $skade = $this->hentRessurs('skade');

        /** @var Field $innholdsFelt */
        $innholdsFelt = $this->app->vis(HtmlEditor::class, [
            'id' => 'skademelding-oppdatering-tekst',
            'name' => 'tekst',
            'label' => 'Oppdatering',
            'value' => null,
            'required' => true
        ]);

        $vedleggsFelt = $this->app->vis(FileField::class, [
            'id' => 'skademelding-oppdatering-vedlegg',
            'name' => 'vedlegg',
            'label' => 'Vedlegg',
            'accept' => implode(', ', ['application/msword', 'application/pdf',
                'application/vnd.oasis.opendocument.spreadsheet', 'application/vnd.oasis.opendocument.text',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'application/x-tar', 'application/zip',
                'image/jpeg', 'image/png', 'text/plain'])
        ]);

        $fields = [
            new HtmlElement('div', [], $skade->hentSkade()),
            $innholdsFelt,
            $vedleggsFelt
        ];
        if($this->erPrivatObjekt($skade)) {
            $privatFelt = $this->app->vis(Checkbox::class, [
                'id' => 'skademelding-oppdatering-privat',
                'name' => 'privat',
                'label' => 'Hold oppdateringen privat',
            ]);

            $fields[] = $privatFelt;
            $fields[] = new HtmlElement('div', [], 'Private opplysninger deles med driftsgruppa og boligstiftelsens representanter, men ikke med øvrige beboere.');
        }

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/mine-sider/index.php?oppslag=skademelding_skjema&oppdrag=ta_i_mot_skjema&skjema=oppdatering&id=' . $skade,
                'formId' => 'skademelding_oppdatering',
                'buttonText' => $skade ?  'Lagre' : 'Registrér oppdateringen',
                'fields' => $fields
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @param Skade $skade
     * @return bool
     * @throws \Exception
     */
    protected function erPrivatObjekt(Skade $skade): bool
    {
        $privatObjekt = false;
        $bruker = $this->hentRessurs('bruker');
        $beboerstøtte = $skade->hentBeboerstøtte();
        if($bruker) {
            if($beboerstøtte) {
                if(strval($bruker) == strval($beboerstøtte->privatPerson)) {
                    $privatObjekt = true;
                }
            }
            else {
                if(
                    strval($bruker) == strval($skade->registrerer)
                    && !$skade->registrerer->harAdgangTil('drift') ) {
                    $privatObjekt = true;
                }
            }

        }
        return $privatObjekt;
    }
}