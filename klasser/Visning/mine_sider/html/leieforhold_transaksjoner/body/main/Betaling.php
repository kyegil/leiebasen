<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_transaksjoner\body\main;


use Kyegil\Leiebasen\Visning\MineSider;

class Betaling extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/leieforhold_transaksjoner/body/main/Betaling.html';

    /**
     * @return Betaling
     */
    protected function forberedData()
    {
        $this->definerData([
            'dato' => '',
            'tekst' => '',
            'beløp' => '',
            'utestående' => ''
        ]);
        return parent::forberedData();
    }
}