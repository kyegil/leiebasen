<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_transaksjoner\body\main;


use Kyegil\Leiebasen\Visning\MineSider;

class Krav extends MineSider
{
    /** @var string  */
    protected $template = 'mine_sider/html/leieforhold_transaksjoner/body/main/Krav.html';

    /**
     * @return Krav
     */
    protected function forberedData()
    {
        $this->definerData([
            'dato' => '',
            'tekst' => '',
            'regning' => '',
            'forfall' => '',
            'beløp' => '',
            'utestående' => ''
        ]);
        return parent::forberedData();
    }
}