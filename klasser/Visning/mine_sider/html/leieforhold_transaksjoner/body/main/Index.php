<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_transaksjoner\body\main;


use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;

class Index extends MineSider
{
    protected $template = 'mine_sider/html/leieforhold_transaksjoner/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Leieforhold $leieforhold
     * @return $this
     * @throws \Exception
     */
    protected function settLeieforhold($leieforhold)
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $this->settRessurs('leieforhold', $leieforhold);
        return $this;
    }


    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        $transaksjoner = clone $leieforhold->hentTransaksjoner();
        $transaksjoner->snu()->beskjær(0, 10);
        $utestående = $leieforhold->hentUtestående();
        $datasett = [
            'transaksjoner' => new ViewArray()
        ];

        /** @var Leieforhold\Krav|\Kyegil\Leiebasen\Modell\Innbetaling $transaksjon */
        foreach ($transaksjoner as $transaksjon) {
            if($transaksjon instanceof Leieforhold\Krav) {
                $beløp = $transaksjon->hentBeløp();
                $datasett['transaksjoner']->addItem($this->app->vis(Krav::class, [
                    'dato' => $transaksjon->hentKravdato() ? $transaksjon->hentKravdato()->format('d.m.Y') : '',
                    'tekst' => $transaksjon->hentTekst(),
                    'regning' => strval($transaksjon->hentRegning()),
                    'forfall' => $transaksjon->hentForfall() ? $transaksjon->hentForfall()->format('d.m.Y') : '',
                    'beløp' => $this->app->kr($beløp),
                    'utestående' => $this->app->kr($utestående)
                ]));
                $utestående -= $beløp;
            }
            else {
                $beløp = $transaksjon->hentBeløpTilLeieforhold($leieforhold);
                $datasett['transaksjoner']->addItem($this->app->vis(Betaling::class, [
                    'dato' => $transaksjon->hentDato() ? $transaksjon->hentDato()->format('d.m.Y') : '',
                    'tekst' => $transaksjon->hentBeløp() > 0 ? "Betaling fra {$transaksjon->hentBetaler()}" : 'Tilbakebetaling',
                    'beløp' => $this->app->kr(-$beløp),
                    'utestående' => $this->app->kr($utestående)
                ]));
                $utestående += $beløp;
            }
        }

        $datasett['tidligereUtestående'] = $this->app->kr($utestående);
        $datasett['nedlastingsknapp'] = $this->hentLinkKnapp(
            "/mine-sider/index.php?oppslag=leieforhold_transaksjoner_csv&id={$leieforhold->hentId()}",
            'Fullstendig historikk',
            'Last ned fullstendig historikk i CSV-format'
        );
        $this->definerData($datasett);
        return parent::forberedData();
    }
}