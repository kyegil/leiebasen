<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\drift_status\body\main;


use DateTime;
use Exception;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjektsett;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Visning for skadeliste i mine sider
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $skadetabell
 *      $meldSkadeKnapp
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\skadeliste\body\main
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/drift_status/body/main/Index.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Leieforhold|null $sisteLeieforhold */
        $sisteLeieforhold = $this->app->hentSamling(Leieforhold::class)
            ->leggTilSortering('fradato', true)->begrens(1)->hentFørste();

        /** @var Leieobjekt\Skade|null $sisteSkademelding */
        $sisteSkademelding = $this->app->hentSamling(Leieobjekt\Skade::class)
            ->leggTilSortering('registrert', true)->begrens(1)->hentFørste();

        /** @var Leieforhold\Regning|null $sisteGiro */
        $sisteGiro = $this->app->hentSamling(Leieforhold\Regning::class)
            ->fjernTilleggsfelter()
            ->leggTilFilter('`' . Leieforhold\Regning::hentTabell() . '`.`utskriftsdato` IS NOT NULL')
            ->leggTilSortering('utskriftsdato', true)->begrens(1)->hentFørste();

        /** @var Innbetaling|null $sisteInnbetaling */
        $sisteInnbetaling = $this->app->hentSamling(Innbetaling::class)
            ->leggTilFilter(['`' . Innbetaling::hentTabell() . '`.`beløp` > 0'])
            ->leggTilSortering('dato', true)->begrens(1)->hentFørste();

        $datasett = [
            'utleier' => $this->app->hentValg('utleier'),

            'sisteLeieforholdDato' => $sisteLeieforhold ? $sisteLeieforhold->hentFradato()->format('d.m.Y') : '',
            'sisteLeieforholdNavn' => $sisteLeieforhold ? $sisteLeieforhold->hentBeskrivelse() : '',

            'antallLedigeLeieobjekter' => $this->hentAntallLedigeLeieobjekter(),

            'sisteSkademeldingId' => $sisteSkademelding ? $sisteSkademelding->hentId() : '',
            'sisteSkademeldingDato' => $sisteSkademelding ? $sisteSkademelding->hentRegistrert()->format('d.m.Y') : '',
            'sisteSkademeldingBeskrivelse' => $sisteSkademelding ? $sisteSkademelding->hentSkade() : '',

            'sisteGiroutskriftDato' => $sisteGiro ? $sisteGiro->hentUtskriftsdato()->format('d.m.Y') : '',

            'sisteInnbetalingDato' => $sisteInnbetaling ? $sisteInnbetaling->hentDato()->format('d.m.Y') : '',

            'oppgjør' => $this->app->vis(Oppgjør::class),
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return int
     * @throws Exception
     */
    private function hentAntallLedigeLeieobjekter(): int
    {
        $iDag = new DateTime();
        $antallLedigeLeieobjekter = 0;
        /** @var Leieobjektsett $leieobjektSett */
        $leieobjektSett = $this->app->hentSamling(Leieobjekt::class)
            ->leggTilFilter(['ikke_for_utleie' => false]);
        foreach ($leieobjektSett as $leieobjekt) {
            /** @var Fraction $ledighet */
            $ledighet = $leieobjekt->hentLedighet($iDag);
            if($ledighet->compare('>', 0)) {
                $antallLedigeLeieobjekter++;
            }
        }
        return $antallLedigeLeieobjekter;
    }
}