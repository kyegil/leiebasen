<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\drift_status\body\main;


use DateInterval;
use DateTimeImmutable;
use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\MineSider;
use stdClass;

/**
 * Visning for skadeliste i mine sider
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $skadetabell
 *      $meldSkadeKnapp
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\skadeliste\body\main
 */
class Oppgjør extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/drift_status/body/main/Oppgjør.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $tp = $this->app->mysqli->table_prefix;
        $idag = new DateTimeImmutable();

        $sisteMånedStart = $idag->sub(new DateInterval('P1M'));
        $sisteMånedSlutt = $idag->sub(new DateInterval('P1D'));

        /** @var stdClass $oppgjør */
        $oppgjør = $this->app->mysqli->arrayData([
            'source' => '`' . $tp . 'krav` AS `krav`',
            'where' => [
                '`krav`.`type`' => Leieforhold\Krav::TYPE_HUSLEIE,
                '`krav`.`beløp` >' => 0,
                '`krav`.`forfall` >=' => $sisteMånedStart->format('Y-m-d'),
                '`krav`.`forfall` <=' => $sisteMånedSlutt->format('Y-m-d'),
            ],
            'fields' => [
                'ant_leier' => 'COUNT(`krav`.`id`)',
                'ant_betalt' => 'SUM(IF(`krav`.`utestående` = 0, 1, 0))',
                'ant_betalt_innen_forfall' => 'SUM(IF((SELECT SUM(`beløp`) FROM `' . $tp .'innbetalinger` AS `innbetalinger` WHERE `innbetalinger`.`krav` = `krav`.`id` AND `innbetalinger`.`dato` <= `krav`.`forfall`) = `krav`.`beløp`, 1, 0))',
            ]
        ])->data[0];

        $betalingsgradSisteMnd = 100 * $oppgjør->ant_betalt / $oppgjør->ant_leier;
        $betaltInnenForfallSisteMnd = 100 * $oppgjør->ant_betalt_innen_forfall / $oppgjør->ant_leier;

        $ubetalt1FraDato = $idag->sub(new DateInterval('P1Y'));
        $ubetalt2FraDato = $idag->sub(new DateInterval('P1Y'));
        $ubetalt1TilDato = $idag->sub(new DateInterval('P1D'));
        $ubetalt2TilDato = $idag->sub(new DateInterval('P2M1D'));

        /** @var stdClass $utestående1 */
        $utestående1 = $this->app->mysqli->arrayData([
            'source' => '`' . $tp . 'krav` AS `krav`',
            'fields' => ['SUM(`krav`.`beløp`) AS `beløp`', 'SUM(`krav`.`utestående`) AS `utestående`'],
            'where' => [
                '`krav`.`beløp` >' => 0,
                '`krav`.`kravdato` >=' => $ubetalt1FraDato->format('Y-m-d'),
                '`krav`.`kravdato` <=' => $ubetalt1TilDato->format('Y-m-d'),
            ]
        ])->data[0];
        /** @var stdClass $utestående2 */
        $utestående2 = $this->app->mysqli->arrayData([
            'source' => '`' . $this->app->mysqli->table_prefix . 'krav` AS `krav`',
            'fields' => ['SUM(`krav`.`beløp`) AS `beløp`', 'SUM(`krav`.`utestående`) AS `utestående`'],
            'where' => [
                '`krav`.`beløp` >' => 0,
                '`krav`.`kravdato` >=' => $ubetalt2FraDato->format('Y-m-d'),
                '`krav`.`kravdato` <=' => $ubetalt2TilDato->format('Y-m-d'),
            ]
        ])->data[0];

        $datasett = [
            'betalingsgradSisteMnd' => number_format($betalingsgradSisteMnd, 1, ',', ' ') . '%',
            'betaltInnenForfallSisteMnd' => number_format($betaltInnenForfallSisteMnd, 1, ',', ' ') . '%',
            'ubetalt1FraDato' => $ubetalt1FraDato->format('d.m.Y'),
            'ubetalt1TilDato' => $ubetalt1TilDato->format('d.m.Y'),
            'ubetalt1Beløp' => $this->app->kr($utestående1->utestående),
            'ubetalt2FraDato' => $ubetalt2FraDato->format('d.m.Y'),
            'ubetalt2TilDato' => $ubetalt2TilDato->format('d.m.Y'),
            'ubetalt2Beløp' => $this->app->kr($utestående2->utestående),
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}