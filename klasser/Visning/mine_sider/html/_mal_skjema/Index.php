<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\_mal_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewInterface;

/**
 * Visning for leieberegningsskjema i mine sider
 *
 *  Ressurser:
 *      modell \Kyegil\Leiebasen\Modell
 *  Mulige variabler:
 *      $modellId
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\_mal_skjema
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/_mal_skjema/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return Index
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['modell'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return Index
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Modell|null $modell */
        $modell = $this->hentRessurs('modell');

        /** @var Field $modellIdFelt */
        $modellIdFelt = $this->app->vis(Field::class, [
            'name' => 'id',
            'type' => 'hidden',
            'required' => true,
            'value' => $modell ? $modell->hentId() : '*'
        ]);

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [];
        if ($modell) {
            $buttons[] = new HtmlElement('a',
                [
                    'class' => 'button',
                    'onclick' => 'leiebasen.mine_sider._malSkjema.slett()'
                ],
                'Slett'
            );
        }
        $buttons[] = new HtmlElement('button', ['type' => 'submit'], 'Lagre');

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/mine-sider/index.php?oppslag=_mal_skjema&oppdrag=ta_i_mot_skjema&skjema=_mal_skjema&id=' . ($modell ? $modell->hentId() : '*'),
                'formId' => '_mal_skjema',
                'buttonText' => 'Lagre',
                'fields' => [
                    $modellIdFelt,
                ],
                'buttons' => $buttons
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}