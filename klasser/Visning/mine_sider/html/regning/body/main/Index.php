<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\regning\body\main;


use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Class Index
 *
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      regning Kyegil\Leiebasen\Modell\Leieforhold\Regning
 *  Mulige variabler:
 *      $regningsmal
 *      $nedlastingsknapp
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\regning\body\main
 */
class Index extends MineSider
{
    protected $template = 'mine_sider/html/regning/body/main/Index.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'regning') {
            return $this->settRegning($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Leieforhold\Regning $regning
     * @return $this
     * @throws \Exception
     */
    protected function settRegning($regning)
    {
        /** @var Leieforhold\Regning $regning */
        $this->settRessurs('regning', $regning);
        return $this;
    }


    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Leieforhold\Regning $regning */
        $regning = $this->hentRessurs('regning');

        $this->definerData([
            'regningsmal' => $this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\regning\Regning::class, [
                'regning' => $regning
            ]),
            'nedlastingsknapp' => $this->hentLinkKnapp(
                "/mine-sider/index.php?oppslag=regning_pdf&id={$regning->hentId()}",
                'Last ned',
                'Last ned som PDF'
            )
        ]);
        return parent::forberedData();
    }
}