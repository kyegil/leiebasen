<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\skadeliste\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Skade;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Visning for skadeliste i mine sider
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $skadetabell
 *      $meldSkadeKnapp
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\skadeliste\body\main
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/skadeliste/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['leieobjekt'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Leieobjekt|null $leieobjekt */
        $leieobjekt = $this->hentRessurs('leieobjekt');
        $bygning = $leieobjekt ? $leieobjekt->hentBygning() : null;
        $kolonner = [
//            (object)[
//                'name' => 'merInfo',
////                'title' => 'Registrert',
//                'className' => 'mer-info',
//                'render' => new JsFunction("return '➥';", ['data', 'type', 'row', 'meta']),
////                'width' => '100px'
//            ],
            (object)[
                'data' => 'registrert',
                'name' => 'registrert',
                'title' => 'Registrert',
                'className' => 'dato',
                'render' => new JsFunction("return type === 'sort' || type === 'type' ? data : row.registrert_formatert;", ['data', 'type', 'row', 'meta']),
                'width' => '100px'
            ],
            (object)[
                'data' => 'leieobjekt',
                'name' => 'leieobjekt',
                'title' => 'Leieobjekt',
                'render' => new JsFunction("if(data) return row.leieobjektbeskrivelse; else return row.navn;", ['data', 'type', 'row', 'meta']),
                'visible' => !$leieobjekt,
                'classes' => ['min-tablet-p']
            ],
            (object)[
                'data' => 'skade',
                'name' => 'skade',
                'render' => new JsFunction("return '<a title=\"Skademelding ' + row.id + '\" href=\"/mine-sider/index.php?oppslag=skademelding_kort&id=' + row.id + '\">' + data + '</a>';", ['data', 'type', 'row', 'meta']),
                'title' => 'Skade',
                'classes' => ['all']
            ],
            (object)[
                'data' => 'kategorier',
                'name' => 'kategorier',
                'title' => 'Kategorier',
                'render' => new JsFunction("return data.length ? (Array.isArray(data) ? '<ul class=\"tags\"><li>' + data.join('</li><li>') + '</li></ul>' : data) : '';", ['data', 'type', 'row', 'meta']),
                'classes' => ['none']
            ],
            (object)[
                'data' => 'beskrivelse',
                'name' => 'beskrivelse',
                'title' => 'Beskrivelse',
                'classes' => ['none']
            ],
            (object)[
                'data' => 'registrerer',
                'name' => 'registrerer',
                'title' => 'Registrert av',
                'classes' => ['min-tablet-l']
            ],
            (object)[
                'data' => 'utført',
                'name' => 'utført',
                'title' => 'Utbedret',
                'className' => 'sentrert',
                'render' => new JsFunction("if(data) return row.utført_dato_formatert; else return 'Ikke utbedret';", ['data', 'type', 'row', 'meta']),
                'classes' => ['min-tablet-l'],
                'width' => '80px'
            ],
            (object)[
                'data' => 'sluttrapport',
                'name' => 'sluttrapport',
                'title' => 'Tiltak utført',
                'render' => new JsFunction("return data ? data : 'Ikke utbedret';", ['data', 'type', 'row', 'meta']),
                'classes' => ['none'],
            ],
        ];

        $data = [];
        $skadeSamling = $this->app->hentSamling(Skade::class)
            ->leggTilSortering('registrert', true);
        $leieobjektfilter = [];
        if($leieobjekt) {
            $leieobjektfilter['or']['leieobjektnr'] = $leieobjekt->hentId();
        }
        if($bygning) {
            $leieobjektfilter['or']['and'] = [
                ['!leieobjektnr'],
                'bygning' => $bygning->hentId()
            ];
        }
        if($leieobjekt || $bygning) {
            $skadeSamling->leggTilFilter($leieobjektfilter);
        }

        /** @var Skade $skade */
        foreach($skadeSamling as $skade) {
            $bygning = $skade->bygning;
            $kategorier = [];
            $kategoriliste = '<ul class="tags">';
            /** @var Skade\Kategori $kategori */
            foreach($skade->hentKategorier() as $kategori) {
                $kategorier[] = $kategori->hentKategori();
                $kategoriliste .= '<li>' . strtolower($kategori->hentKategori()) . ' </li>';
            }
            $kategoriliste .= '</ul>';
            $data[] = (object)[
                'id' => $skade->hentId(),
                'skade' => $skade->skade,
                'registrert' => $skade->registrert->format('Y-m-d H:i:s'),
                'registrert_formatert' => $skade->registrert->format('d.m.Y'),
                'registrerer' => $skade->registrerer ? $skade->registrerer->hentNavn() : null,
                'leieobjekt' => (int)strval($bygning) . '-' . ($skade->leieobjekt ? $skade->leieobjekt->hentId() : ''),
                'leieobjektbeskrivelse' => strval($skade->leieobjekt) ? $skade->leieobjekt->hentBeskrivelse() : ($bygning ? $bygning->hentNavn() : ''),
                'beskrivelse' => $skade->beskrivelse,
                'utført' => (bool)$skade->utført,
                'utført_dato_formatert' => $skade->utført ? $skade->utført->format('d.m.Y') : '',
                'sluttregistrerer' => $skade->sluttregistrerer ? $skade->sluttregistrerer->hentNavn() : null,
                'sluttrapport' => $skade->sluttrapport,
                'navn' => $bygning ? $bygning->navn : null,
                'kategorier' => $kategorier,
                'kategoriliste' => $kategoriliste
            ];
        }
        $dataTablesConfig = (object)[
//            'ajax' => (object)[
//                'dataSrc' => 'data',
//                'data' => (object)[
//                    'query' => 'test'
//                ],
//                'url' => "/mine-sider/index.php?oppslag=skadeliste&id={$leieobjekt}&oppdrag=hent_data&data=skadeliste"
//            ],
            'order' => [[6, 'desc'],[0, 'desc']],
            'columns' => $kolonner,
            'responsive' => (object)[
                'details' => (object)[
//                    'renderer' => new JsFunction("", ['api', 'rowIdx', 'columns'])
//                    'display' => new JsCustom('$.fn.dataTable.Responsive.display.childRowImmediate')
                ],
            ],
            'data' => $data
        ];

        $datasett = [
            'skadetabell' => $this->app->vis(DataTable::class, [
                'tableId' => 'Skadetabell',
                'caption' => 'Skader' . ($leieobjekt ? ' på ' . $leieobjekt->hentNavn() : ''),
                'kolonner' => $kolonner,
//                'data' => $initData,
                'dataTablesConfig' => $dataTablesConfig
            ]),
            'meldSkadeKnapp' => $this->hentLinkKnapp("/mine-sider/index.php?oppslag=skademelding_skjema&leieobjekt={$leieobjekt}&id=*", 'Rapporter en skade', 'Registrer en ny skade.')
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}