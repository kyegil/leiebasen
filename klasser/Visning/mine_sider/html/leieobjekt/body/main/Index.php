<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\leieobjekt\body\main;


use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Visning\felles\html\leieobjekt\leieberegning\Leieberegningsformel;
use Kyegil\Leiebasen\Visning\felles\html\leieobjekt\Leieforholdfelt;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      leieobjekt \Kyegil\Leiebasen\Modell\Leieobjekt
 *  Mulige variabler:
 *      $leieobjektId
 *      $ikkeForUtleie
 *      $navn
 *      $gateadresse
 *      $beskrivelse
 *      $postnummer
 *      $poststed
 *      $bilde
 *      $bygningsId
 *      $bygningsnavn
 *      $areal
 *      $antRom
 *      $bad
 *      $toalett
 *      $toalettKategori
 *      $merknader
 *      $leieberegningsmetode
 *      $leieberegningsformel
 *      $leieforholdfelt
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold\body\main
 */
class Index extends MineSider
{
    protected $template = 'mine_sider/html/leieobjekt/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'leieobjekt') {
            return $this->settRessurs('leieobjekt', $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Leieobjekt $leieobjekt */
        $leieobjekt = $this->hentRessurs('leieobjekt');
        /** @var Leieobjekt\Bygning $bygning */
        $bygning = $leieobjekt->hentBygning();
        /** @var Leieobjekt\Leieberegning $leieberegning */
        $leieberegning = $leieobjekt->hentLeieberegning();
        /** @var Fraction[] $vanligsteLeieandeler */
        $vanligsteLeieandeler = $leieobjekt->hentTypiskLeieandel();

        $this->definerData([
            'leieobjektId' => $leieobjekt->hentId(),
            'ikkeForUtleie' => $leieobjekt->hent('ikke_for_utleie'),
            'navn' => $leieobjekt->hent('navn'),
            'gateadresse' => $leieobjekt->hent('gateadresse'),
            'etasje' => $leieobjekt->hent('etg'),
            'beskrivelse' => $leieobjekt->hent('beskrivelse'),
            'postnummer' => $leieobjekt->hent('postnr'),
            'poststed' => $leieobjekt->hent('poststed'),
            'bilde' => $leieobjekt->hent('bilde'),
            'bygningsId' => strval($bygning),
            'bygningsnavn' => $bygning ? $bygning->hentNavn() : '',
            'areal' => $leieobjekt->hent('areal'),
            'antRom' => $leieobjekt->hent('ant_rom'),
            'bad' => $leieobjekt->hent('bad'),
            'toalett' => $leieobjekt->hent('toalett'),
            'toalettKategori' => $leieobjekt->hent('toalett_kategori'),
            'merknader' => nl2br($leieobjekt->hent('merknader')),
            'leieberegningsmetode' => $leieberegning->hentNavn(),
            'leieberegningsformel' => $this->app->vis(Leieberegningsformel::class, [
                'leieberegning' => $leieberegning,
                'leieobjekt' => $leieobjekt
            ]),
            'leieforholdfelt' => $this->app->vis(Leieforholdfelt::class, ['leieobjekt' => $leieobjekt])
        ]);
        return parent::forberedData();
    }
}