<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\oversikt_inntekter\body\main;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Delkravtypesett;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;
use stdClass;

class Index extends MineSider
{
    protected $template = 'mine_sider/html/oversikt_inntekter/body/main/Index.html';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $tp = $this->app->mysqli->table_prefix;
        $kravTyper = [
            Krav::TYPE_HUSLEIE,
            Krav::TYPE_STRØM
        ];
        /** @var stdClass[] $inntekterKolonneKonfigurasjoner */
        $inntekterKolonneKonfigurasjoner = [
            'periode' => (object)[
                'overskrift' => 'Tidsrom',
                'class' => null
            ],
            'totalt' => (object)[
                'overskrift' => 'Totalt',
                'class' => null
            ],
        ];
        /** @var Delkravtypesett $tilleggstyper */
        $tilleggstyper = $this->app->hentDelkravtyper()->leggTilFilter(['selvstendig_tillegg' => true]);

        $groupfields = 'IF(YEAR(`krav`.`kravdato`) = YEAR(NOW()), CONCAT(YEAR(`krav`.`kravdato`), \'-\', DATE_FORMAT(`krav`.`kravdato`, \'%m\')), YEAR(`krav`.`kravdato`))';

        $dataFelter = [
            'periode' => $groupfields,
            'totalt' => 'SUM(`krav`.`beløp`)'
        ];
        foreach ($kravTyper as $kravType) {
            $dataFelter[strtolower($kravType)] = "SUM(IF(`krav`.`type` = '{$kravType}', `krav`.`beløp`, 0))";
            $inntekterKolonneKonfigurasjoner[strtolower($kravType)] = (object)[
                'overskrift' => $kravType,
                'class' => 'min-width450'
            ];
        }

        foreach ($tilleggstyper as $tilleggstype) {
            $dataFelter[strtolower($tilleggstype->kode)] = "SUM(IF(`krav`.`type` = '{$tilleggstype->kode}', `krav`.`beløp`, 0))";
            $inntekterKolonneKonfigurasjoner[strtolower($tilleggstype->kode)] = (object)[
                'overskrift' => $tilleggstype->navn . " (tillegg til {$tilleggstype->kravtype})",
                'class' => 'min-width700'
            ];
        }

        /** @var stdClass[] $inntekter */
        $inntekter = $this->app->mysqli->arrayData([
            'source' => "`{$tp}krav` AS `krav`",
            'groupfields' => $groupfields,
            'where' => [
                '`krav`.`kravdato` < NOW()',
                '`krav`.`kravdato` >=' => date_create()->sub(new \DateInterval('P10Y'))->format('Y-01-01')
            ],
            'orderfields' => 'periode DESC',
            'fields' => $dataFelter
        ])->data;

        /** @var ViewArray $inntekterKolonneOverskrifter */
        $inntekterKolonneOverskrifter = $this->kolonneOverskrifter($inntekterKolonneKonfigurasjoner);

        $inntekterTabellLinjer = new ViewArray();
        foreach ($inntekter as $linje) {
            $inntekterTabellLinjer->addItem($this->hentLinjeHtml($inntekterKolonneKonfigurasjoner, $linje));
        }

        $this->definerData([
            'inntekterKolonneOverskrifter' => $inntekterKolonneOverskrifter,
            'inntekterTabellLinjer' => $inntekterTabellLinjer,
        ]);
        return parent::forberedData();
    }

    /**
     * @param stdClass[] $kolonneKonfigurasjoner
     * @return ViewArray
     */
    private function kolonneOverskrifter(array $kolonneKonfigurasjoner): ViewInterface
    {
        $kolonneOverskrifter = new ViewArray();
        foreach ($kolonneKonfigurasjoner as $kolonne => $konfigurasjon) {
            $argumenter = $konfigurasjon->class ? ['class' => $konfigurasjon->class] : [];
            $kolonneOverskrifter->addItem(new HtmlElement('th', $argumenter, $konfigurasjon->overskrift));
        }
        return $kolonneOverskrifter;
    }

    /**
     * @param stdClass[] $kolonneKonfigurasjoner
     * @param stdClass $linje
     * @return HtmlElement TR-element
     */
    private function hentLinjeHtml(array $kolonneKonfigurasjoner, stdClass $linje): ViewInterface
    {
        $celleHtml = new ViewArray();
        foreach($kolonneKonfigurasjoner as $kolonne => $konfigurasjon) {
            $class = ($kolonne == 'periode' ? '' : 'beløp ')
                . ($konfigurasjon->class ?: '');
            $verdi = $linje->$kolonne ?? 0;
            if ($kolonne == 'periode') {
                if (strlen($verdi) > 4) {
                    $verdi = \IntlDateFormatter::formatObject(new \DateTime($verdi . '-01'), 'MMM yy', 'nb_NO');
                }
            }
            else {
                $verdi = $this->app->kr($verdi);
            }
            $kolonnelabel = new HtmlElement('span', ['class' => 'kolonnelabel'], $konfigurasjon->overskrift);
            $argumenter = $class ? ['class' => $class] : [];
            $celleHtml->addItem(new HtmlElement('td', $argumenter, [$kolonnelabel, $verdi]));
        }
        return new HtmlElement('tr', [], $celleHtml);
    }
}