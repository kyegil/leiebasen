<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_merknader\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\Leiebasen\Visning\MineSider;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for leieforhold-merkander i mine sider
 *
 *  Ressurser:
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $oppsummeringsfelt
 *      $filtre
 *      $notatTabell
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_merknader\body\main
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/leieforhold_merknader/body/main/Index.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Leieforhold $leieforhold
     * @return $this
     * @throws \Exception
     */
    protected function settLeieforhold($leieforhold): Index
    {
        $this->settRessurs('leieforhold', $leieforhold);
        return $this;
    }


    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $kolonner = [
            (object)[
                'data' => 'dato',
                'name' => 'dato',
                'title' => 'Dato',
                'responsivePriority' => 2,
                'render' => new JsFunction("if (data.length > 0 && type == 'display') {return data.split('-').reverse().join('.');} return data;", ['data', 'type', 'row', 'meta']),
                'width' => '40px'
            ],
            (object)[
                'data' => 'kategori',
                'name' => 'kategori',
                'title' => '',
                'responsivePriority' => 2,
                'render' => new JsFunction(
                    "if (type == 'display') {return '<a title=\"Detaljer\" href=\"/mine-sider/index.php?oppslag=notat&id=' + row.id + '\">' + data + '</a>';} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
                'width' => '40px'
            ],
            (object)[
                'data' => 'notat',
                'name' => 'notat',
                'title' => 'Notat',
            ],
            (object)[
                'data' => 'brev',
                'name' => 'brev',
                'title' => 'Brev',
                'render' => new JsFunction("return type != 'display' ?  data : (data ? '<a style=\"cursor: pointer; text-decoration: none;\" onClick=\"leiebasen.mineSider.visBrev(' + row.id + ')\" title=\"Vis brev\">🖨</a>' : '');", ['data', 'type', 'row', 'meta']),
                'responsivePriority' => 3,
                'width' => '20px'
            ],
            (object)[
                'data' => 'vedlegg',
                'name' => 'vedlegg',
                'title' => 'Vedlegg',
                'render' => new JsFunction("return data.length == 0 || type != 'display' ?  data : '<a style=\"cursor: pointer; text-decoration: none;\" onClick=\"leiebasen.mineSider.lastVedlegg(' + row.id + ')\" title=\"Last ned\">📎</a>';", ['data', 'type', 'row', 'meta']),
                'responsivePriority' => 3,
                'width' => '20px'
            ],
        ];

        $data = [];
        $knapper = [];
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        if ($leieforhold) {
            $notater = $leieforhold->hentBetalingsNotater()
                ->leggTilFilter(['skjul_for_leietaker' => false])
                ->låsFiltre()
            ;
            foreach($notater as $notat) {
                $data[] = (object)[
                    'id' => $notat->hentId(),
                    'dato' => $notat->hentDato() ? $notat->hentDato()->format('Y-m-d') : '',
                    'kategori' => $notat->hentKategori(),
                    'brev' => boolval(trim($notat->hentBrevtekst())),
                    'notat' => $notat->hentNotat(),
                    'vedlegg' => $notat->hentVedlegg(),
                ];
            }

            if($leieforhold->hentUtestående() > 0) {
                if($leieforhold->hentBetalingsplan()) {
                    $knapper[] = $this->hentLinkKnapp(
                        '/mine-sider/index.php?oppslag=betalingsplan_skjema&leieforhold=' . $leieforhold->hentId() . '&id=*',
                        'Foreslå ny betalingsplan',
                        'Lag nytt betalingsplan-forslag for leieforhold ' . $leieforhold->hentId(),
                    );
                }
                else {
                    $knapper[] = $this->hentLinkKnapp(
                        '/mine-sider/index.php?oppslag=betalingsplan_skjema&leieforhold=' . $leieforhold->hentId() . '&id=*',
                        'Foreslå betalingsplan',
                        'Lag forslag til betalingsplan for leieforhold ' . $leieforhold->hentId(),
                    );
                }
            }
        }

        $dataTablesConfig = (object)[
            'order' => [[0, 'desc']],
            'columns' => $kolonner,
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'data' => $data,
        ];

        $oppsummeringsfelt = new ViewArray();

        $datasett = [
            'oppsummeringsfelt' => $oppsummeringsfelt,
            'filtre' => '',
            'notatTabell' => $this->app->vis(DataTable::class, [
                'tableId' => 'notater_liste',
                'caption' => 'Betalingsmerknader og kommunikasjon',
                'kolonner' => $kolonner,
                'data' => $data,
                'dataTablesConfig' => $dataTablesConfig
            ]),
            'knapper' => $knapper,
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}