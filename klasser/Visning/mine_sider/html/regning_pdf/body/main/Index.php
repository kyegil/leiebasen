<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 *
 * Selv om denne kontrolleren normalt skal respondere med ei fil,
 * er den beholdt i html-området fordi evt problemer skal vises som html
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\regning_pdf\body\main;


use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\MineSider;

class Index extends MineSider
{
    protected $template = 'mine_sider/html/regning_pdf/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'regning') {
            return $this->settRegning($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Leieforhold\Regning $regning
     * @return $this
     * @throws \Exception
     */
    protected function settRegning($regning)
    {
        /** @var Leieforhold\Regning $regning */
        $this->settRessurs('regning', $regning);
        return $this;
    }


    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Leieforhold\Regning $regning */
        $regning = $this->hentRessurs('regning');

        $this->definerData([
            'regningsmal' => $this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\regning\Regning::class, [
                'regning' => $regning
            ]),
            'nedlastingsknapp' => $this->hentLinkKnapp(
                "/mine-sider/index.php?oppslag=regning&oppdrag=hent_data&data=pdf&id={$regning->hentId()}",
                'Last ned',
                'Last ned som PDF'
            )
        ]);
        return parent::forberedData();
    }
}