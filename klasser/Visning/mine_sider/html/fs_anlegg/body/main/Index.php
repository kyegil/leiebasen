<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\html\fs_anlegg\body\main;


use DateInterval;
use DateTime;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling\Fakturaliste;
use Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling\Fordelingsnøkkel;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Class Index
 *
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      strømanlegg Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg
 *      strømregninger Kyegil\Leiebasen\Samling|Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad[]
 *  Mulige variabler:
 *      $anleggsnummer
 *      $målernummer
 *      $beskrivelse
 *      $målerplassering
 *      $fordeling
 *      $forbruksstatistikk
 *      $regningsliste
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\fs_anlegg\body\main
 */
class Index extends MineSider
{
    protected $template = 'mine_sider/html/fs_anlegg/body/main/Index.html';

    /**
     * @var array
     */
    protected $data = [
        'anleggsnummer' => null,
        'målernummer' => null,
        'beskrivelse' => null,
        'målerplassering' => null,
        'fordeling' => null,
        'forbruksstatistikk' => null
    ];

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['strømanlegg', 'strømregninger'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Strømanlegg $strømanlegg */
        $strømanlegg = $this->hentRessurs('strømanlegg');
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnadsett|\Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad[] $strømregninger */
        $strømregninger = $this->hentRessurs('strømregninger');
        if($strømanlegg && !$strømregninger) {
            $strømregninger = $strømanlegg->hentRegninger();
            $strømregninger->sorterLastede(function($a,$b){
                /**
                 * @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad $a
                 * @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad $b
                 */
                $fraA = $a->hentFradato();
                $fraB = $b->hentFradato();
                if($fraA == $fraB) {
                    return 0;
                }
                return $fraA > $fraB ? -1 : 1;
            });
            $strømregninger->beskjær(0, 6);
        }
        $fraDato = new DateTime();
        $fraDato->sub(new DateInterval('P1Y'))->add(new DateInterval('P1D'));
        $tilDato = new DateTime();

        if($strømanlegg) {
            $this->definerData([
                'anleggsnummer' => htmlspecialchars($strømanlegg->hentAnleggsnummer()),
                'målernummer' => htmlspecialchars($strømanlegg->hentMålernummer()),
                'beskrivelse' => htmlspecialchars($strømanlegg->hentFormål()),
                'målerplassering' => htmlspecialchars($strømanlegg->hentPlassering()),
                'fordeling' => $this->app->vis(Fordelingsnøkkel::class, [
                    'fordelingsnøkkel' => $strømanlegg->hentFordelingsnøkkel()
                ]),
                'forbruksstatistikk' => new HtmlElement('div', ['class'=>'extjs-container extjs4'],
                    new HtmlElement('div', ['id'=>'forbruksstatistikk'])
                ),
                'forbruksstatistikkFraDato' => $this->app->vis(DateField::class, [
                    'id' => 'forbruksstatistikk-fradato',
                    'value' => $fraDato->format('Y-m-d'),
                    'label' => 'Fra dato'
                ]),
                'forbruksstatistikkTilDato' => $this->app->vis(DateField::class, [
                    'id' => 'forbruksstatistikk-tildato',
                    'value' => $tilDato->format('Y-m-d'),
                    'label' => 'Til dato'
                ])
            ]);
        }

        if($strømregninger) {
            $regningsliste = $this->app->vis(Fakturaliste::class, [
                'kostnader' => $strømregninger
            ]);
            $this->definerData([
                'regningsliste' => $regningsliste
            ]);
        }

        return parent::forberedData();
    }
}