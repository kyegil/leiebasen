<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_avtaletekst\body\main;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *      kontrakt \Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt
 *  Mulige variabler:
 *      $leieforholdId
 *      $kontraktId
 *      $tekst
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_preferanser\body\main
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/html/leieforhold_avtaletekst/body/main/Index';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Index
    {
        if($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        if($attributt == 'kontrakt') {
            return $this->settKontrakt($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Leieforhold $leieforhold
     * @return $this
     * @throws Exception
     */
    protected function settLeieforhold(Leieforhold $leieforhold): Index
    {
        $this->settRessurs('leieforhold', $leieforhold);
        return $this;
    }

    /**
     * @param Kontrakt $kontrakt
     * @return $this
     * @throws Exception
     */
    protected function settKontrakt(Kontrakt $kontrakt): Index
    {
        $this->settRessurs('kontrakt', $kontrakt);
        return $this;
    }

    /**
     * @throws Exception
     */
    protected function forberedData(): Index
    {
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        /** @var Kontrakt|null $kontrakt */
        $kontrakt = $this->hentRessurs('kontrakt');
        if (!$kontrakt && $leieforhold) {
            $kontrakt = $leieforhold->kontrakt;
        }

        if ($kontrakt) {
            $datasett = [
                'leieforholdId' => $kontrakt->leieforhold->hentId(),
                'kontraktId' => $kontrakt->hentId(),
                'tekst' => $kontrakt->hentTekst()
            ];
        }
        else {
            $datasett = [
                'leieforholdId' => '',
                'kontraktId' => '',
                'tekst' => ''
            ];
        }
        $this->definerData($datasett);
        return parent::forberedData();
    }
}