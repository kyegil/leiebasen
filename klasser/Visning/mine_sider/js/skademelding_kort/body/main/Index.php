<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\js\skademelding_kort\body\main;


use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt\Skade;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Skript-fil i mine sider
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *      skade \Kyegil\Leiebasen\Modell\Leieobjekt\Skade
 *  Mulige variabler:
 *      $skadeId
 * @package Kyegil\Leiebasen\Visning\mine_sider\js\skademelding_kort\body\main
 */
class Index extends MineSider
{
    protected $template = 'mine_sider/js/skademelding_kort/body/main/Index.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['bruker', 'skade'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Skade $skade */
        $skade = $this->hentRessurs('skade');

        $datasett = [
            'skadeId' => $skade ? $skade->hentId() : null
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}