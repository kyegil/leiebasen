<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\js\leieforhold_merknader\body\main;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Skript-fil i mine sider
 *
 *  Ressurser:
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $leieforholdId
 * @package Kyegil\Leiebasen\Visning\mine_sider\js\leieforhold_merknader\body\main
 */
class Index extends MineSider
{
    protected $template = 'mine_sider/js/leieforhold_merknader/body/main/Index.js';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Leieforhold $leieforhold
     * @return $this
     * @throws \Exception
     */
    protected function settLeieforhold($leieforhold): Index
    {
        $this->settRessurs('leieforhold', $leieforhold);
        return $this;
    }


    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        $datasett = [
            'leieforholdId' => $leieforhold ? $leieforhold->hentId() : null
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}