<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\js\profil_skjema\body\main;


use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Skript-fil i mine sider
 *
 *  Ressurser:
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\mine_sider\js\profil_skjema\body\main
 */
class Index extends MineSider
{
    protected $template = 'mine_sider/js/profil_skjema/body/main/Index.js';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        return parent::forberedData();
    }
}