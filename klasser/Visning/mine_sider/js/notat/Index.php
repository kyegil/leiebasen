<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\js\notat;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Skript-fil i mine_sider
 *
 *  Ressurser:
 *      notat \Kyegil\Leiebasen\Modell\Leieforhold\Notat
 *  Mulige variabler:
 *      $notatId
 * @package Kyegil\Leiebasen\Visning\mine_sider\js\notat
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/js/notat/Index.js';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'notat') {
            return $this->settNotat($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Notat|null $notat
     * @return $this
     */
    protected function settNotat(?Notat $notat): Index
    {
        $this->settRessurs('notat', $notat);
        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Notat|null $notat */
        $notat = $this->hentRessurs('notat');
        $datasett = [
            'notatId' => $notat ? $notat->hentId() : null
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}