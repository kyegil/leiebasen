<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\js\leieforhold_preferanser\body\main;


use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\MineSider;

class Index extends MineSider
{
    protected $template = 'mine_sider/js/leieforhold_preferanser/body/main/Index.js';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Leieforhold $leieforhold
     * @return $this
     * @throws \Exception
     */
    protected function settLeieforhold(Leieforhold $leieforhold)
    {
        $this->settRessurs('leieforhold', $leieforhold);
        return $this;
    }


    /**
     * @return Index
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        /** @var string[] $datasett */
        $datasett = [
            'leieforholdId' => $leieforhold->hentId()
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}