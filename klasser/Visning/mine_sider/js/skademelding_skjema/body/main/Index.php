<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\js\skademelding_skjema\body\main;


use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Skript-fil i mine sider
 *
 *  Ressurser:
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\mine_sider\js\skademelding_skjema\body\main
 */
class Index extends MineSider
{
    protected $template = 'mine_sider/js/skademelding_skjema/body/main/Index.js';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        return parent::forberedData();
    }
}