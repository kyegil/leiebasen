<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\js\profil_kommunikasjon_preferanser;


use Exception;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Skript-fil i mine sider
 *
 *  Ressurser:
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\mine_sider\js\adgang_opprett\body\main
 */
class Script extends MineSider
{
    protected $template = 'mine_sider/js/profil_kommunikasjon_preferanser/Script.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        return parent::forberedData();
    }
}