<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\mine_sider\js\fs_anlegg\body\main;


use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Skript-fil i mine sider
 *
 *  Ressurser:
 *      strømanlegg Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg
 *  Mulige variabler:
 *      $forbruksdiagramModellFelter
 * @package Kyegil\Leiebasen\Visning\mine_sider\js\skademelding_skjema\body\main
 */
class Forbruksstatistikk extends MineSider
{
    protected $template = "mine_sider/js/fs_anlegg/body/main/Forbruksstatistikk.js";

    protected $data = [
        'forbruksdiagramModellFelter' => null
    ];

    /**
     * @param array|string $attributt
     * @param mixed|null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'strømanlegg') {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Strømanlegg $strømanlegg */
        $strømanlegg = $this->hentRessurs('strømanlegg');
        if($strømanlegg) {
            $forbruksdiagramModellFelter = $this->hentForbruksdiagramModellFelter($strømanlegg);
            $forbruksdiagramAkseFelter = $this->hentForbruksdiagramAkseFelter($strømanlegg);

            $this->definerData([
                'forbruksdiagramAkseFelter' => json_encode($forbruksdiagramAkseFelter),
                'forbruksdiagramModellFelter' => json_encode($forbruksdiagramModellFelter),
                'store' => $this->app->vis(Ext\data\JsonStore::class, [
                    'model' => 'ForbruksdiagramModell',
                    'url' => "/mine-sider/index.php?oppslag=fs_anlegg&id={$strømanlegg->hentId()}&oppdrag=hent_data&data=forbruksstatistikk"
                ])
            ]);
        }

        return parent::forberedData();
    }

    /**
     * @param Strømanlegg $strømanlegg
     * @return string[]
     * @throws \Exception
     */
    protected function hentForbruksdiagramAkseFelter(Strømanlegg $strømanlegg): array
    {
        $resultat = [];
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad $regning */
        foreach($strømanlegg->hentRegninger() as $regning) {
            $resultat[] = strval($regning->hentFakturanummer());
        }
        return $resultat;
    }

    /**
     * @param Strømanlegg $strømanlegg
     * @return \stdClass[]
     * @throws \Exception
     */
    protected function hentForbruksdiagramModellFelter(Strømanlegg $strømanlegg): array
    {
        $resultat = [];
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad $regning */
        foreach($strømanlegg->hentRegninger() as $regning) {
            $resultat[] = (object)[
                'name' => strval($regning->hentFakturanummer()),
                'type' => 'float'
            ];
        }
        $resultat[] = (object)[
            'name' => 'dato',
            'type' => 'date',
            'dateFormat' => 'Y-m-d'
        ];
        return $resultat;
    }
}