<?php

namespace Kyegil\Leiebasen\Visning\mine_sider\js\betalingsutsettelse_skjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\MineSider;

/**
 * Skript-fil i mine_sider
 *
 *  Ressurser:
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $leieforholdId
 *      $kravId
 *      $helligdager
 *      $framtidigeKravJson
 * @package Kyegil\Leiebasen\Visning\mine_sider\js\betalingsutsettelse_skjema
 */
class Index extends MineSider
{
    /** @var string */
    protected $template = 'mine_sider/js/betalingsutsettelse_skjema/Index.js';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Leieforhold|null $leieforhold
     * @return $this
     */
    protected function settLeieforhold(?Leieforhold $leieforhold): Index
    {
        $this->settRessurs('leieforhold', $leieforhold);
        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        $datasett = [
            'leieforholdId' => $leieforhold->hentId(),
            'helligdager' => $this->hentHelligdager(intval(date('Y')) +10),
            'framtidigeKravJson' => json_encode($this->hentForeståendeKrav())
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @param int $inntilÅr
     * @return string
     * @throws Exception
     */
    private function hentHelligdager(int $inntilÅr): string
    {
        $helligdager = [];
        $år = intval(date('Y'));
        while ($år < $inntilÅr) {
            $åretsHelligdager = $this->app->bankfridager($år);
            array_walk($åretsHelligdager, function (&$dato) use ($år) {
                $dato = $år . '-' . $dato;
            });
            $helligdager = array_merge($helligdager, $åretsHelligdager);
            $år++;
        }
        sort($helligdager);
        return json_encode($helligdager);
    }

    /**
     * @return object
     * @throws Exception
     */
    private function hentForeståendeKrav(): object
    {
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        $framtidigeKrav = new \stdClass();
        foreach($leieforhold->hentUteståendeKrav() as $krav) {
            if($krav->forfall && $krav->utestående > 0 && !$krav->hentBetalingsplanLink()) {
                $framtidigeKrav->{$krav->hentId()} = $this->hentKravdata($krav);
            }
        }
        foreach($leieforhold->hentFramtidigeKrav() as $krav) {
            if($krav->forfall && $krav->utestående > 0 && !$krav->hentBetalingsplanLink()) {
                $framtidigeKrav->{$krav->hentId()} = $this->hentKravdata($krav);
            }
        }
        return $framtidigeKrav;
    }

    private function hentKravdata(Leieforhold\Krav $krav): object
    {
        $forfall = clone $krav->forfall;
        if($forfall instanceof \DateTime) {
            $forfall = \DateTimeImmutable::createFromMutable($forfall);
        }
        $regning = $krav->regning;
        $maxForfallsdato = null;
        $originalForfallsdato = $regning ? $regning->hent('original_forfallsdato') : null;
        if($originalForfallsdato instanceof \DateTime) {
            $originalForfallsdato = \DateTimeImmutable::createFromMutable($originalForfallsdato);
        }
        $originalForfallsdato = $originalForfallsdato ?? $forfall;
        $spillerom = $krav->leieforhold->hentBetalingUtsettelseSpillerom();
        if ($spillerom !== null) {
            $maxForfallsdato = $forfall->add($spillerom);
        }

        $beskrivelse = $forfall->format('d.m.Y');
        if ($regning && $regning->hent('original_forfallsdato')) {
            $beskrivelse .= ' (endret fra ' . $originalForfallsdato->format('d.m.Y') . ')';
        }
        $beskrivelse .= ': ';
        $beskrivelse .= $krav->tekst . ' ' . $this->app->kr($krav->utestående, false);

        return (object)[
            'id' => $krav->hentId(),
            'forfall' => $forfall->format('Y-m-d'),
            'originalForfallsdato' => $originalForfallsdato ? $originalForfallsdato->format('Y-m-d') : null,
            'maxForfallsdato' => $maxForfallsdato ? $maxForfallsdato->format('Y-m-d') : null,
            'beskrivelse' => $beskrivelse
        ];
    }
}