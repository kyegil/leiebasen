<?php

namespace Kyegil\Leiebasen\Visning\drift\html;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Oppslag\Drift\OversiktGjeld\Utlevering;
use Kyegil\Leiebasen\Poengbestyrer;
use Kyegil\Leiebasen\Visning\drift\html\shared\body\main\Content;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\ViewRenderer\ViewArray;

/**
 * @inheritdoc
 */
class OversiktGjeld extends Content
{
    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): OversiktGjeld
    {
        /** @var Person $bruker */
        $bruker = $this->app->hentModell(Person::class, $this->app->bruker['id']);

        $leieforholdFilter = $this->hentLeieforholdFilter();
        
        /**
         * Kolonne-objekt for DataTables
         *
         * @link https://datatables.net/reference/option/columns
         */
        $kolonner = [
            (object)[
                'data' => 'leieforhold',
                'name' => 'leieforhold',
                'title' => 'Leieforhold',
                'responsivePriority' => 2, // Høyere tall = lavere prioritet
                'className' => 'dt-head-left dt-body-left noColvis',
                'render' => new JsFunction(
                    "if (type == 'display') {return row.leieforholdbeskrivelse;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
                'width' => '100%',
            ],
            (object)[
                'data' => 'utestående',
                'name' => 'utestående',
                'title' => 'Utestående',
                'orderable' => true,
                'responsivePriority' => 2, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return leiebasen.kjerne.kr(data);} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
                'className' => 'dt-right',
                'type' => 'num',
            ],
            (object)[
                'data' => 'forfalt',
                'name' => 'forfalt',
                'title' => 'Forfalt',
                'orderable' => true,
                'responsivePriority' => 4, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return leiebasen.kjerne.kr(data);} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
                'visible' =>false,
                'className' => 'dt-right',
                'type' => 'num',
            ],
            (object)[
                'data' => 'siste_innbetaling',
                'name' => 'siste_innbetaling',
                'title' => 'Siste innbetaling',
                'orderable' => true,
                'responsivePriority' => 7, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return leiebasen.kjerne.dato(data);} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
                'visible' =>false,
            ],
            (object)[
                'data' => 'leiebeløp',
                'name' => 'leiebeløp',
                'title' => 'Leiebeløp',
                'responsivePriority' => 7, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return leiebasen.kjerne.kr(data);} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
                'className' => 'dt-right',
                'type' => 'num',
                'visible' =>false,
            ],
            (object)[
                'data' => 'tilsv_ant_leier',
                'name' => 'tilsv_ant_leier',
                'title' => 'Leier',
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
                'className' => 'dt-right',
                'type' => 'num',
                'render' => new JsFunction(
                    "if (type == 'display') {return (data < 1 ? '<1' : ('>=' + Math.floor(data)));}return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
            (object)[
                'data' => 'avsluttet',
                'name' => 'avsluttet',
                'title' => 'Avsluttet',
                'responsivePriority' => 6, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return data ? leiebasen.kjerne.dato(data) : '';} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
            (object)[
                'width' => 40,
                'data' => 'frosset',
                'name' => 'frosset',
                'title' => 'Frosset',
                'className' => 'dt-center',
                'render' => new JsFunction(
                    "if (type == 'display') {return leiebasen.kjerne.boolVisning(data);} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
                'responsivePriority' => 6, // Høyere tall = lavere prioritet
            ],
            (object)[
                'width' => '250px',
                'data' => 'siste_betalingsplan',
                'name' => 'siste_betalingsplan',
                'title' => 'Bet. plan',
                'responsivePriority' => 7, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return data ? leiebasen.kjerne.dato(data) : '';} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
        ];

        foreach (Poengbestyrer::getInstance($this->app)->hentAktiveProgram() as $program) {
            $programAlias = Utlevering::POENG_PROGRAM_PREFIX . $program->kode;
            $programKolonne = (object)[
                'data' => $programAlias,
                'name' => $programAlias,
                'title' => $program->navn,
                'orderable' => true,
                'responsivePriority' => 8, // Høyere tall = lavere prioritet
                'visible' =>false,
                'className' => 'dt-right',
                'type' => 'num',
                'render' => new JsFunction(
                    "if (type == 'display') {return leiebasen.drift.oversiktGjeld.formaterPoeng(data, '{$program->kode}', row.leieforhold);} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ];
            $kolonner[] = $programKolonne;
        }

        if ($bruker->harAdgangTil(Adgang::ADGANG_OPPFØLGING)) {
            $kolonner[] = (object)[
                'width' => '250px',
                'data' => 'oppfølging',
                'name' => 'oppfølging',
                'title' => 'Oppfølging',
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return row.oppfølging_formatert;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ];
        }

        /**
         * Konfig-objekt for DataTables
         * * pageLength initiert antall per side
         * * lengthMenu
         *
         * @link https://datatables.net/reference/option/
         */
        $dataTablesConfig = (object)[
            'fixedHeader' => (object)[
                'footer' => true,
            ],
            'layout' => (object)[
                'topStart' => null,
                'topEnd' => null,
                'bottomStart' => null,
                'bottomEnd' => null,
                'top' => [
                    'pageLength',
                    new JsFunction('let resultat = document.createElement("div");resultat.innerHTML = `' . (string)$leieforholdFilter . '`; return resultat;'),
                    (object)[
                        'buttons' => [
                            (object)[
                                'extend' => 'colvis',
                                'text' => 'Kolonner',
                                'columns' => ':not(.noColvis)'
                            ]
                        ]
                    ],
                    'search'
                ],
                'bottom' => ['info', 'paging', (object)['buttons' => ['copy','pdf', 'csv', 'print']]]
            ],
            'order' => [
                [$this->kolonneIndeks('oppfølging', $kolonner), 'asc'],
                [$this->kolonneIndeks('tilsv_ant_leier', $kolonner), 'desc']
            ],
            'columns' => $kolonner,
            'pageLength' => 100,
            'pagingType'=> 'simple_numbers',
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'scrollCollapse' => false, // Collapse when no content
            'scrollY' => 'calc(100vh - 240px)', // Table height
            'search' => (object)[
                'return' => true,
            ],
            'searchDelay' => 1000,
            'serverSide' => true,
            'processing' => true,
            'ajax' => (object)[
                'url' => '/drift/index.php?oppslag=oversikt_gjeld&oppdrag=hent_data',
                'data' => new JsFunction('data.leieforholdfilter = $("#leieforholdfilter").find(":selected").val();',
                    ['data']
                ),
            ],
        ];

        $knapper = [];

        if($knapper) {
            $dataTablesConfig->scrollY = 'calc(100vh - 285px)';
        }

        $datasett = [
            'heading' => '',
            'mainBodyContents' => $this->app->vis(DataTable::class, [
                'classes' => ['display', 'compact',
                    'full-page-dt' // Custom class for correct scrolling behavior on full page DataTables
                ],
                'tableId' => 'gjeldsoversikt',
                'kolonner' => $kolonner,
                'dataTablesConfig' => $dataTablesConfig
            ]),
            'buttons' => $knapper,
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }

    /**
     * @return HtmlElement
     * @throws Exception
     */
    private function hentLeieforholdFilter()
    {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $options = new ViewArray();
        $options->addItem(new HtmlElement('option', ['value' => 0], 'All gjeld'));
        $options->addItem(new HtmlElement('option', ['value' => 1, 'selected' => true], 'Kun nåværende leieforhold'));
        $options->addItem(new HtmlElement('option', ['value' => 2], 'Kun avsluttede leieforhold'));
        $options->addItem(new HtmlElement('option', ['value' => 5], 'Kun avsluttede ikke-frosne leieforhold'));
        $options->addItem(new HtmlElement('option', ['value' => 10], 'Kun frosne leieforhold'));
        $leieforholdFilter = new HtmlElement('select', [
            'id' => 'leieforholdfilter',
            'class' => 'dt-input',
            'onchange' => new JsCustom('leiebasen.drift.oversiktGjeld.oppdaterTabell()'),
        ], $options);
        return $this->app->post($this, __FUNCTION__, $leieforholdFilter, $args);
    }

    private function kolonneIndeks(string $name, array $kolonner)
    {
        foreach ($kolonner as $indeks => $kolonne) {
            if(($kolonne->name ?? '') === $name) {
                return $indeks;
            }
        }
        return null;
    }
}