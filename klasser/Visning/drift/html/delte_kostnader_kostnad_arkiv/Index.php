<?php

namespace Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_kostnad_arkiv;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;

/**
 * Visning for Regninger på delte kostnader i drift
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $tabell
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_kostnad_arkiv
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/delte_kostnader_kostnad_arkiv/Index.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /**
         * Kolonne-objekt for DataTables
         *
         * @link https://datatables.net/reference/option/columns
         */
        $kolonner = [
            (object)[
                'data' => 'id',
                'name' => 'id',
                'title' => 'Id',
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
//                'width' => '40px'
            ],
            (object)[
                'data' => 'fakturanummer',
                'name' => 'fakturanummer',
                'title' => 'Fakturanummer',
                'responsivePriority' => 2, // Høyere tall = lavere prioritet
            ],
            (object)[
                'data' => 'tjeneste_id',
                'name' => 'tjeneste_id',
                'title' => 'Tjeneste',
                'responsivePriority' => 1, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return row.tjeneste;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
            (object)[
                'data' => 'termin',
                'name' => 'termin',
                'title' => 'Termin',
                'responsivePriority' => 4, // Høyere tall = lavere prioritet
            ],
            (object)[
                'data' => 'fradato',
                'name' => 'fradato',
                'title' => 'Fra dato',
                'responsivePriority' => 3, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return row.fradato_formatert;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
            (object)[
                'data' => 'tildato',
                'name' => 'tildato',
                'title' => 'Til dato',
                'responsivePriority' => 3, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return row.tildato_formatert;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
            (object)[
                'data' => 'forbruk',
                'name' => 'forbruk',
                'title' => 'Forbruk',
                'className' => 'dt-right',
                'responsivePriority' => 3, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return row.forbruk_formatert;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
            (object)[
                'data' => 'beløp',
                'name' => 'beløp',
                'title' => 'Beløp',
                'className' => 'dt-right',
                'responsivePriority' => 1, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return row.beløp_formatert;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
            (object)[
                'data' => 'html',
                'orderable' => false,
                'hidden' => true, // Høyere tall = lavere prioritet
            ],
        ];

        /**
         * Konfig-objekt for DataTables
         * * pageLength initiert antall per side
         * * lengthMenu
         *
         * @link https://datatables.net/reference/option/
         */
        $dataTablesConfig = (object)[
//            'fixedHeader' => true,
            'fixedHeader' => (object)[
                'footer' => true,
            ],
            'order' => [[0, 'desc']],
            'columns' => $kolonner,
            'pageLength' => 25,
            'pagingType'=> 'simple_numbers',
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'scrollCollapse' => false, // Collapse when no content
            'scrollY' => 'calc(100vh - 310px)', // Table height
            'createdRow' => new JsFunction('console.log(data.status);if(data.status) {$(row).addClass(data.status);} if(data.frosset) {$(row).addClass(\'frosset\');}', ['row', 'data', 'dataIndex']),
            'search' => (object)[
                'return' => true,
            ],
            'searchDelay' => 1000,
            'serverSide' => true,
            'processing' => true,
            'ajax' => (object)[
                'url' => '/drift/index.php?oppslag=delte_kostnader_kostnad_arkiv&oppdrag=hent_data&data=fordelte_kostnader',
            ]
        ];

        $knapper = [
            $this->hentLinkKnapp(
                '/drift/index.php?oppslag=delte_kostnader_kostnad_skjema&id=*',
                'Registrer kostnad',
                'Registrer ny regning eller kostnad for fordeling.')
        ];

        $datasett = [
            'varsler' => '',
            'tabell' => $this->app->vis(DataTable::class, [
                'tableId' => 'fordelingsarkiv',
                'kolonner' => $kolonner,
                'dataTablesConfig' => $dataTablesConfig
            ]),
            'knapper' => $knapper,
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}