<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\html\krav_kort\extjs4\panel\krav;


use Kyegil\Leiebasen\Modell\Leieforhold\Purring as PurringModell;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Panel-innhold for krav
 *
 *  Ressurser:
 *      $purring \Kyegil\Leiebasen\Modell\Leieforhold\Purring
 *  Mulige variabler:
 *      $dato
 *      $purremåte
 * @package Kyegil\Leiebasen\Visning\drift\html\krav_kort\extjs4\panel\krav
 */
class Purring extends Drift
{
    protected $template = 'drift/html/krav_kort/extjs4/panel/krav/Purring.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['purring'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var PurringModell $purring */
        $purring = $this->hentRessurs('purring');

        if($purring) {
            $this->definerData([
                'purremåte' => $purring->hentPurremåte(),
                'dato' => $purring->hentPurredato() ? $purring->hentPurredato()->format('d.m.Y') : ''
            ]);
        }
        return parent::forberedData();
    }
}