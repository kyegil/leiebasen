<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\html\krav_kort\extjs4\panel\krav;


use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Panel-innhold for krav
 *
 *  Ressurser:
 *      $delbeløp \Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp
 *  Mulige variabler:
 *      $delbeløpId
 *      $transaksjonsId
 *      $betaler
 *      $dato
 *      $konto
 *      $beløp
 *      $referanse
 * @package Kyegil\Leiebasen\Visning\drift\html\krav_kort\extjs4\panel\krav
 */
class Innbetaling extends Drift
{
    protected $template = 'drift/html/krav_kort/extjs4/panel/krav/Innbetaling.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['delbeløp'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Delbeløp $delbeløp */
        $delbeløp = $this->hentRessurs('delbeløp');

        if($delbeløp) {
            $this->definerData([
                'delbeløpId' => $delbeløp->hentId(),
                'transaksjonsId' => $delbeløp->hentInnbetaling()->hentId(),
                'betaler' => $delbeløp->hentBetaler(),
                'dato' => $delbeløp->hentDato() ? $delbeløp->hentDato()->format('d.m.Y') : '',
                'konto' => $delbeløp->hentKonto(),
                'beløp' => $this->app->kr($delbeløp->hentBeløp()),
                'referanse' => $delbeløp->hentRef()
            ]);
        }
        return parent::forberedData();
    }
}