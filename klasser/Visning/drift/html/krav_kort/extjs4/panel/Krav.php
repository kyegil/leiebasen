<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\html\krav_kort\extjs4\panel;


use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav as KravModell;
use Kyegil\Leiebasen\Modell\Leieforhold\Purring as PurringModell;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\html\krav_kort\extjs4\panel\krav\Innbetaling;
use Kyegil\Leiebasen\Visning\drift\html\krav_kort\extjs4\panel\krav\Purring;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Panel-innhold for krav
 *
 *  Ressurser:
 *      $krav \Kyegil\Leiebasen\Modell\Leieforhold\Krav
 *      $leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $kravId
 *      $reellkravId
 *      $kravTekst
 *      $kravDato
 *      $kravBeløp
 *      $utestående
 *      $kravtype
 *      $leieobjektId
 *      $leieobjektBeskrivelse
 *      $leieforholdId
 *      $leieforholdBeskrivelse
 *      $strømanleggId
 *      $andel
 *      $termin
 *      $fom
 *      $tom
 *      $regningsnummer
 *      $utskriftsdato
 *      $forfall
 *      $kid
 *      $fastKid
 *      $delkrav
 *      $innbetalinger
 *      $purringer
 *      $regningsformat
 *      $efakturaId
 *      $efakturaForsendelse
 *      $efakturaOppdrag
 *      $efakturaForsendelsesdato
 *      $efakturaKvittertDato
 *      $efakturaStatus
 *      $fboTrekkId
 *      $fboForsendelse
 *      $fboOppdrag
 *      $fboBeløp
 *      $fboOverføringsdato
 *      $fboForfallsdato
 *      $fboVarslet
 *      $fboVarselformat
 * @package Kyegil\Leiebasen\Visning\drift\html\krav_kort\extjs4\panel
 */
class Krav extends Drift
{
    protected $template = 'drift/html/krav_kort/extjs4/panel/Krav.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['bruker', 'krav', 'leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var KravModell|null $krav */
        $krav = $this->hentRessurs('krav');
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }
        if($krav) {
            $kravDato = $krav->hentDato();
            /** @var Leieforhold $leieforhold */
            $leieforhold = $leieforhold ?: $krav->hentLeieforhold();
            /** @var Leieobjekt $leieobjekt */
            $leieobjekt = $krav->hentLeieobjekt();
            /** @var Strømanlegg $strømanlegg */
            $strømanlegg = $krav->hentAnlegg();
            /** @var Fraction|null $andel */
            $andel = $krav->hentAndel();
            $andelsbeskrivelse = '';
            if ($andel) {
                $andelsbeskrivelse = $andel->asDecimal() != 1 ? $andel->asMixedNumber() : 'Hele leieobjektet';
            }

            $termin = $krav->hentTermin();
            if(!$termin && $krav->hentFom() && $krav->hentTom()) {
                $termin = "{$krav->hentFom()->format('d.m.Y')} – {$krav->hentTom()->format('d.m.Y')}";
            }

            $delkrav = new ViewArray();
            /** @var KravModell\Delkrav $del */
            foreach($krav->hentDelkrav() as $del) {
                $delkrav->addItem($this->app->vis(Delkrav::class, [
                    'delkrav' => $del
                ]));
            }

            $innbetalinger = new ViewArray();
            /** @var Delbeløp $del */
            foreach($krav->hentInnbetalingsDelbeløp() as $delbeløp) {
                $innbetalinger->addItem($this->app->vis(Innbetaling::class, [
                    'delbeløp' => $delbeløp
                ]));
            }

            $purringer = new ViewArray();
            /** @var PurringModell $purring */
            foreach($krav->hentPurringer() as $purring) {
                $purringer->addItem($this->app->vis(Purring::class, [
                    'purring' => $purring
                ]));
            }

            /** @var Regning $regning */
            $regning = $krav->hentRegning();
            $forfall = $krav->hentForfall();

            $efaktura = $regning ? $regning->hentEfaktura() : null;
            $efakturaForsendelsesDato = $efaktura ? $efaktura->hentForsendelsesdato() : null;
            $efakturaKvittertDato = $efaktura ? $efaktura->hentKvittertDato() : null;

            $fboTrekk = $regning ? $regning->hentFboTrekk() : null;
            $fboOverføringsdato = $fboTrekk ? $fboTrekk->hentOverføringsdato() : null;
            $fboForfallsdato = $fboTrekk ? $fboTrekk->hentForfallsdato() : null;
            $fboVarslet = $fboTrekk ? $fboTrekk->hentVarslet() : null;
            $fboVarselformat = $regning ? ($regning->hentFormat() == 'papir' ? 'giro' : $regning->hentFormat()) : '';

            $this->definerData([
                'kravId' => $krav->hentId(),
                'reellKravId' => $krav->hentReellKravId() ?? 'Ikke fastsatt',
                'kravTekst' => $krav->hentTekst(),
                'kravDato' => $kravDato ? $kravDato->format('d.m.Y') : '',
                'kravBeløp' => $this->app->kr($krav->hentBeløp()),
                'utestående' => $this->app->kr($krav->hentUtestående()),
                'kravtype' => $krav->hentType(),
                'leieforholdId' => $leieforhold->hentId(),
                'leieforholdBeskrivelse' => $leieforhold->hentBeskrivelse(),
                'leieobjektId' => $leieobjekt ? $leieobjekt->hentId() : null,
                'leieobjektBeskrivelse' => $leieobjekt ? $leieobjekt->hentBeskrivelse() : null,
                'strømanleggId' => $strømanlegg ? $strømanlegg->hentAnleggsnummer() : null,
                'andel' => $andelsbeskrivelse,
                'termin' => $termin,
                'fom' => $krav->hentFom() ? $krav->hentFom()->format('d.m.Y') : '',
                'tom' => $krav->hentTom() ? $krav->hentTom()->format('d.m.Y') : '',
                'delkrav' => $delkrav,
                'innbetalinger' => $innbetalinger,
                'purringer' => $purringer,

                'regningsnummer' => $regning ? $regning->hentId() : '',
                'utskriftsdato' => $regning && $regning->hentUtskriftsdato() ? $regning->hentUtskriftsdato()->format('d.m.Y') : null,
                'forfall' => $forfall ? $forfall->format('d.m.Y') : null,
                'kid' => $regning ? $regning->hentKid() : '',
                'fastKid' => $leieforhold->hentKid(),
                'regningsformat' => $regning ? $regning->hentFormat() : '',

                'efakturaId' => $efaktura ? $efaktura->hentId() : '',
                'efakturaForsendelse' => $efaktura ? $efaktura->hentForsendelse() : '',
                'efakturaOppdrag' => $efaktura ? $efaktura->hentOppdrag() : '',
                'efakturaForsendelsesdato' => $efakturaForsendelsesDato ? $efakturaForsendelsesDato->format('d.m.Y') : '',
                'efakturaKvittertDato' => $efakturaKvittertDato ? $efakturaKvittertDato->format('d.m.Y') : '',
                'efakturaStatus' => $efaktura ? $efaktura->hentStatus() : '',

                'fboTrekkId' => $fboTrekk ? $fboTrekk->hentId() : '',
                'fboForsendelse' => $fboTrekk ? $fboTrekk->hentForsendelse() : '',
                'fboOppdrag' => $fboTrekk ? $fboTrekk->hentOppdrag() : '',
                'fboBeløp' => $fboTrekk ? $this->app->kr($fboTrekk->hentBeløp()) : '',
                'fboOverføringsdato' => $fboOverføringsdato ? $fboOverføringsdato->format('d.m.Y') : '',
                'fboForfallsdato' => $fboForfallsdato ? $fboForfallsdato->format('d.m.Y') : '',
                'fboVarslet' => $fboVarslet ? $fboVarslet->format('d.m.Y H:i:s') : '',
                'fboVarselformat' => $fboVarselformat
            ]);
        }
        return parent::forberedData();
    }
}