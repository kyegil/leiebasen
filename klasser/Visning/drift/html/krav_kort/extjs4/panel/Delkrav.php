<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\html\krav_kort\extjs4\panel;


use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Panel-innhold for krav
 *
 *  Ressurser:
 *      $delkrav \Kyegil\Leiebasen\Modell\Leieforhold\Krav\Delkrav
 *      $leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $kode
 *      $navn
 *      $beløp
 * @package Kyegil\Leiebasen\Visning\drift\html\krav_kort\extjs4
 */
class Delkrav extends Drift
{
    protected $template = 'drift/html/krav_kort/extjs4/panel/Delkrav.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['delkrav'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Krav\Delkrav $delkrav */
        $delkrav = $this->hentRessurs('delkrav');
        /** @var Delkravtype $delkravtype */
        $delkravtype = $delkrav->hentDelkravtype();

        if($delkrav) {
            $this->definerData([
                'kode' => $delkravtype->hentKode(),
                'navn' => $delkravtype->hentNavn(),
                'beløp' => $this->app->kr($delkrav->hentBeløp())
            ]);
        }
        return parent::forberedData();
    }
}