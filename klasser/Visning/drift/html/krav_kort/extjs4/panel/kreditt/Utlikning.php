<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\html\krav_kort\extjs4\panel\kreditt;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Panel-innhold for krav
 *
 *  Ressurser:
 *      $utlikning \Kyegil\Leiebasen\Modell\Leieforhold\Purring
 *  Mulige variabler:
 *      $utlikningsId
 *      $avstemt
 *      $leieforholdId
 *      $utlikningsbeskrivelse
 *      $beløp
 *      $beløpFormatert
 * @package Kyegil\Leiebasen\Visning\drift\html\krav_kort\extjs4\panel\krav
 */
class Utlikning extends Drift
{
    protected $template = 'drift/html/krav_kort/extjs4/panel/kreditt/Utlikning.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['utlikning'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Delbeløp $utlikning */
        $utlikning = $this->hentRessurs('utlikning');

        if($utlikning) {
            $avstemt = false;
            $utlikningskrav = $utlikning->hentKrav();
            $utlikningsbeskrivelse = new HtmlElement('i',[], "ikke avstemt");
            if($utlikningskrav instanceof Krav) {
                $avstemt = true;
                $utlikningsbeskrivelse = [
                    'avstemt mot ',
                    new HtmlElement('a', [
                        'href' => "/drift/index.php?oppslag=krav_kort&id={$utlikningskrav}"
                    ], $utlikningskrav->hentTekst())
                ];
            }
            elseif(isset($utlikningskrav)) {
                $avstemt = true;
                $utlikningsbeskrivelse = 'utbetalt';
            }

            $this->definerData([
                'utlikningsId' => $utlikning->hentId(),
                'avstemt' => $avstemt,
                'leieforholdId' => $utlikning->hentLeieforhold() ? $utlikning->hentLeieforhold() : null,
                'beløp' => abs($utlikning->hentBeløp()),
                'beløpFormatert' => $this->app->kr(abs($utlikning->hentBeløp())),
                'utlikningsbeskrivelse' => $utlikningsbeskrivelse
            ]);
        }
        return parent::forberedData();
    }
}