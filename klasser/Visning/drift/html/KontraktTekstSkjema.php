<?php

namespace Kyegil\Leiebasen\Visning\drift\html;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\AvtaleMal;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Visning\drift\html\shared\body\main\Content;
use Kyegil\Leiebasen\Visning\felles\html\bootstrap\Modal;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\HtmlEditor;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\ViewRenderer\ViewInterface;
use stdClass;

/** @inheritdoc  */
class KontraktTekstSkjema extends Content
{
    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): KontraktTekstSkjema
    {
        if(in_array($attributt,['kontrakt'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): KontraktTekstSkjema
    {
        /** @var Kontrakt|null $kontrakt */
        $kontrakt = $this->hentRessurs('kontrakt');

        $malVelger = $this->app->vis(Select::class, [
            'id' => 'mal-velger',
            'class' => 'col',
            'options' => $this->hentMalOptions($kontrakt ? $kontrakt->leieforhold : null),
            'value' => '0'
        ]);

        $forhåndsVisningKnapp = new HtmlElement('div', [
            'role' => 'button',
            'tabindex' => '0',
            'class' => 'btn btn-secondary forhåndsvis',
        ], 'Forhåndsvis');

        $forhåndsvisning = $this->app->vis(Modal::class, [
            'id' => 'forhåndsvisning',
            'title' => 'Leieavtale ' . $kontrakt . ' i leieforhold ' . $kontrakt->leieforhold,
            'body' => '&nbsp;',
            'size' => 'xl',
        ]);

        $tekstRedigerer = $this->app->vis(HtmlEditor::class, [
            'id' => 'avtale-redigerer',
            'name' => 'avtaletekstmal',
        ]);

        $felter = new HtmlElement('div', ['class' => 'container'], [
            $forhåndsvisning,
            new HtmlElement('div', ['class' => 'row align-items-start'], [$malVelger, $forhåndsVisningKnapp]),
            $tekstRedigerer,
        ]);

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [$forhåndsVisningKnapp];
        $buttons[] = new HtmlElement('div', [
                'role' => 'button',
                'class' => 'btn btn-secondary utskrift',
            ], 'Skriv ut'
        );
        $buttons[] = new HtmlElement('button', [
            'role'=> 'button',
            'type' => 'submit',
            'form' => 'avtaletekst'
            ],
            'Lagre avtaleteksten'
        );
        $buttons[] = new HtmlElement('a', [
            'role' => 'button',
            'href' => $this->app->returi->get()->url,
            'class' => 'btn btn-secondary fortsett',
        ], 'Fortsett'
        );

        $datasett = [
            'mainBodyContents' => $this->app->vis(AjaxForm::class, [
                'useModal' => true,
                'action' => $this->app::url('kontrakt_tekst_skjema', $kontrakt, 'mottak', ['skjema' => 'avtaletekst']),
                'formId' => 'avtaletekst',
                'fields' => $felter,
                'buttons' => ''
            ]),
            'buttons' => $buttons,
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }

    /**
     * @param Leieforhold|null $leieforhold
     * @return object|mixed|null
     */
    private function hentMalOptions(?Leieforhold $leieforhold): object
    {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $options = new stdClass();

        if($leieforhold && $leieforhold->avtaletekstmal) {
            $options->{'0'} = 'Bruk eksisterende avtaletekst';
        }

        /** @var Leieforhold\AvtaleMalsett $malsett */
        $malsett = $this->app->hentSamling(AvtaleMal::class);
        $malsett->leggTilFilter(['type' => AvtaleMal::TYPE_LEIEAVTALE]);
        foreach ($malsett as $mal) {
            $options->{$mal->hentId()} = $mal->malnavn;
        }

        return $this->app->post($this, __FUNCTION__, $options, $args);
    }
}