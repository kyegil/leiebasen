<?php

namespace Kyegil\Leiebasen\Visning\drift\html\leieregulering_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Leieregulerer;
use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\CollapsibleSection;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\HtmlEditor;
use Kyegil\Leiebasen\Visning\felles\html\form\NumberField;
use Kyegil\Leiebasen\Visning\felles\html\table\HtmlTable;
use Kyegil\Leiebasen\Visning\felles\html\table\TableBody;
use Kyegil\Leiebasen\Visning\felles\html\table\TableFoot;
use Kyegil\Leiebasen\Visning\felles\html\table\Td;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

/**
 * Visning for leieberegningsskjema i drift
 *
 *  Ressurser:
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $leieforholdId
 * @package Kyegil\Leiebasen\Visning\drift\html\leieregulering_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/leieregulering_skjema/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return Index
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return Index
     * @throws Exception
     */
    protected function forberedData()
    {
        $leieregulerer = Leieregulerer::getInstance($this->app);
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        $leieOppdatert = $leieforhold->hent('leie_oppdatert') ?? $leieforhold->hentFradato();
        $varslingsmetode = $leieregulerer->hentVarseltypeForLeieforhold($leieforhold);

        /** @var \DateTimeImmutable $justeringsdato */
        $justeringsdato = $leieregulerer->foreslåJusteringsdato($leieforhold);
        $foreslåttBasisleie = $leieregulerer->foreslåJustertBasisLeie($leieforhold, $justeringsdato);
        $utløpt = $leieforhold->tildato && $leieforhold->tildato <= $justeringsdato;

        $leieforholdDetaljer = [
            'Leieforhold ' . $leieforhold->hentId() . '<br>',
            $leieforhold->hentBeskrivelse() . '<br>',
            'Påbegynt ' . $leieforhold->hentFradato()->format('d.m.Y') . '<br>',
            'Sist justert leie: ' . $leieOppdatert->format('d.m.Y') . '<br>',
            'Nåværende leie: ' . $this->app->kr($leieforhold->hentLeiebeløp()) . ' per '
            . $this->app->periodeformat($leieforhold->hentTerminlengde(), false, true) . '<br>',
            'Varslingsmåte: ' . $varslingsmetode . '<br>'
        ];
        if($utløpt) {
            $leieforholdDetaljer [] = new HtmlElement('div', ['style' => 'color:red'],
                'Leieforholdet '
                . ($leieforhold->tildato <= date_create_immutable() ? 'utløp ' : 'utløper ')
            . $leieforhold->tildato->format('d.m.Y')
                . ', og må fornyes før leia kan reguleres.<br>'
            );
        }
        else if ($leieforhold->tildato) {
            $leieforholdDetaljer [] = 'Leieforholdet utløper: ' . $leieforhold->tildato->format('d.m.Y') . '<br>';
        }
        $leieforholdDetaljer = new HtmlElement('div', ['class' => 'leieregulering leieforholdbeskrivelse'], $leieforholdDetaljer);
        $leieOppbyggingTabell = $this->visLeieOppbyggingTabell($leieforhold, $justeringsdato);
        $mal = $this->app->hentValg('leiejustering_brevmal') ?? '';
        $varsel = $leieregulerer->fyllJusteringsVarselForLeieforhold($leieforhold, $mal, $justeringsdato, $foreslåttBasisleie);

        /** @var HtmlEditor $varselFelt */
        $varselFelt = $this->app->vis(HtmlEditor::class, [
            'ckEditorConfig' => (object)[
                'htmlSupport' => (object)[
                    'allow' => [(object)[
                        'name' => 'span',
                        'attributes' => true,
                    ]]
                ],
            ],
            'id' => 'justeringsvarsel',
            'name' => 'justeringsvarsel',
            'value' => $varsel
        ]);

        $varselFeltCollapsible = $this->app->vis(CollapsibleSection::class, [
            'label' => 'Varsel om regulering',
            'contents' => $varselFelt
        ]);

        /** @var Field $leieforholdIdFelt */
        $leieforholdIdFelt = $this->app->vis(Field::class, [
            'name' => 'id',
            'type' => 'hidden',
            'required' => true,
            'value' => $leieforhold->hentId()
        ]);

        $justeringsdatoFelt = $this->app->vis(DateField::class, [
            'name' => 'justeringsdato',
            'label' => 'Dato for ny leie',
            'style' => 'flex: 1; margin: 0 10px;',
            'required' => true,
            'min' => $justeringsdato->format('Y-m-d'),
            'value' => $justeringsdato->format('Y-m-d'),
        ]);

        $basisLeiebeløpFelt = $this->app->vis(NumberField::class, [
            'name' => 'basis_leiebeløp',
            'label' => 'Grunnbeløp per år',
            'style' => 'flex: 1; margin: 0 10px;',
            'required' => true,
            'value' => $foreslåttBasisleie,
        ]);

        $justeringsFelter = new HtmlElement('div', ['style' => 'display: flex;'],
            [$justeringsdatoFelt, $basisLeiebeløpFelt]
        );

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [];
        if (!$utløpt) {
            $buttons[] = new HtmlElement('a',
                [
                    'id' => 'utskriftsknapp',
                    'class' => 'button',
                    'onclick' => 'leiebasen.drift.leiereguleringSkjema.skrivUt()'
                ],
                'Skriv ut varsel'
            );
            $buttons[] = new HtmlElement('button',
                ['type' => 'submit'],
                'Gjennomfør leiereguleringen'
            );
        }

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/drift/index.php?oppslag=leieregulering_skjema&oppdrag=ta_i_mot_skjema&skjema=leieregulering&id=' . ($leieforhold ? $leieforhold->hentId() : ''),
                'formId' => 'leieregulering_skjema',
                'fields' => [
                    $leieforholdDetaljer,
                    $leieforholdIdFelt,
                    $justeringsFelter,
                    $leieOppbyggingTabell,
                    $varselFeltCollapsible
                ],
                'buttons' => $buttons
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * Vis HTML-tabell med leieoppbyggingen
     *
     * @param Leieforhold $leieforhold
     * @return HtmlTable
     * @throws Exception
     */
    private function visLeieOppbyggingTabell(Leieforhold $leieforhold, \DateTimeImmutable $justeringsdato): HtmlTable
    {
        $leieregulerer = Leieregulerer::getInstance($this->app);
        $justeringsfaktor = $leieregulerer->hentJusteringsfaktorForLeieforhold($justeringsdato, $leieforhold);
        $basisLeie = round($leieregulerer->foreslåJustertBasisLeie($leieforhold, $justeringsdato));
        $årsleie = $basisLeie;
        $leieforholdDelkravtypesett = $leieforhold->hentDelkravtyper();
        $leieforholdDelkravtypesett->leggTilDelkravTypeModell();
        $leieforholdDelkravtypesett->leggTilFilter(['`' . Delkravtype::hentTabell() . '`.`kravtype`' => Leieforhold\Krav::TYPE_HUSLEIE]);
        $leieforholdDelkravtypesett->leggTilFilter(['selvstendig_tillegg' => false]);
        $tabellInnhold = new ViewArray();

        $tr = new HtmlElement('tr', [
            'data-leie-element' => 'basisleie',
        ], [
            $this->app->vis(Td::class, [
                'content' => 'Årlig basisleie'
            ]),
            $this->app->vis(Td::class, [
                'class' => 'beløp',
                'content' => ($basisLeie)
            ]),
        ]);
        $tabellInnhold->addItem($tr);

        foreach ($leieforholdDelkravtypesett as $leieforholdDelkravtype) {
            if($leieforholdDelkravtype->relativ) {
                $beskrivelse = $leieforholdDelkravtype->hentNavn() . ' (' . $this->app->prosent($leieforholdDelkravtype->delkravtype->hentSats()/100) . ')';
                $sats = $leieforholdDelkravtype->delkravtype->hentSats() / 100 * $årsleie;
            }
            else if ($leieforholdDelkravtype->delkravtype->valgfritt
                && $leieforholdDelkravtype->delkravtype->indeksreguleres
            ) {
                $beskrivelse = $leieforholdDelkravtype->hentNavn();
                $sats = round($justeringsfaktor->multiply($leieforholdDelkravtype->delkravtype->hentSats())->asDecimal());
            }
            else {
                $beskrivelse = $leieforholdDelkravtype->hentNavn();
                $sats = $leieforholdDelkravtype->delkravtype->hentSats();
            }
            if($leieforholdDelkravtype->delkravtype->valgfritt) {
                $satsCelle = $this->app->vis(Td::class, [
                    'class' => 'beløp',
                    'content' => new HtmlElement('input', [
                        'type' => 'number',
                        'min' => 0,
                        'step' => 1,
                        'pattern' => "d+",
                        'name' => 'delkrav[' . $leieforholdDelkravtype->hentKode() . ']',
                        'value' => $leieforholdDelkravtype->relativ ? $leieforholdDelkravtype->delkravtype->hentSats() : round($sats),
                    ]),
                ]);
            }
            else {
                $satsCelle = $this->app->vis(Td::class, [
                    'class' => 'beløp',
                    'content' => ($sats),
                ]);
            }
            $tr = new HtmlElement('tr', [
                'class' => $leieforholdDelkravtype->relativ ? 'relativ' : null,
                'data-leie-element' => $leieforholdDelkravtype->hentKode()
            ], [
                $this->app->vis(Td::class, [
                    'content' => $beskrivelse
                ]),
                $satsCelle,
            ]);
            $tabellInnhold->addItem($tr);
            $årsleie += $sats;
        }

        $tr = new HtmlElement('tr', [
            'data-leie-element' => 'bruttoleie',
        ], [
            $this->app->vis(Td::class, [
                'content' => 'Totalt per år'
            ]),
            $this->app->vis(Td::class, [
                'class' => 'skjult beløp',
                'content' => round($årsleie)
            ]),
        ]);
        $tabellInnhold->addItem($tr);

        $tableBody = $this->app->vis(TableBody::class, [
            'rows' => $tabellInnhold
        ]);
        $tr = new HtmlElement('tr', [
            'data-leie-element' => 'terminleie',
        ], [
            $this->app->vis(Td::class, [
                'content' => 'Å betale per '
                    . $this->app->periodeformat($leieforhold->hentTerminlengde(), false, true)
            ]),
            $this->app->vis(Td::class, [
                'class' => 'beløp',
                'content' => (round($årsleie / $leieforhold->antTerminer)),
            ]),
        ]);
        $tableFoot = $this->app->vis(TableFoot::class, ['rows' => $tr]);

        /** @var HtmlTable $tabell */
        $tabell = $this->app->vis(HtmlTable::class, [
            'tableId' => 'leieOppbyggingTabell',
            'tableBody' => $tableBody,
            'tableFoot' => $tableFoot
        ]);
        return $tabell;
    }
}