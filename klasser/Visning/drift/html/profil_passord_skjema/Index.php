<?php

namespace Kyegil\Leiebasen\Visning\drift\html\profil_passord_skjema;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\DisplayField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;

/**
 * Visning for brukerprofil-passord-skjema i drift
 *
 *  Ressurser:
 *      brukerprofil \Kyegil\Leiebasen\Modell\Person\Brukerprofil
 *  Mulige variabler:
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\drift\html\profil_passord_skjema\body\main
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/profil_passord_skjema/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['brukerprofil'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Person\Brukerprofil $brukerProfil */
        $brukerProfil = $this->hentRessurs('brukerprofil');

        if($brukerProfil) {
            /** @var Field $brukerIdFelt */
            $brukerIdFelt = $this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\form\Field::class, [
                'name' => 'id',
                'type' => 'hidden',
                'required' => true,
                'value' => $brukerProfil->hentId()
            ]);

            /** @var Field $brukernavnFelt */
            $brukernavnFelt = $this->app->vis(DisplayField::class, [
                'readonly' => true,
                'name' => 'login',
                'label' => 'Brukernavn (brukes ved innlogging)',
                'value' => $brukerProfil->hentBrukernavn()
            ]);

            $endreBrukernavnLenke = new HtmlElement('a', [
                'href' => '/drift/index.php?oppslag=profil_skjema&id=' . $brukerProfil->person->hentId(),
                'title' => 'Endre brukernavnet'
            ],'Endre brukernavnet');
        }

        /** @var Field $passord1Felt */
        $passord1Felt = $this->app->vis(Field::class, [
            'name' => 'pw1',
            'id' => 'pw1',
            'type' => 'password',
            'minlength' => Person\Brukerprofil::$passordLengde,
            'pattern' => Person\Brukerprofil::$passordRegex,
            'placeholder' => Person\Brukerprofil::$passordHint,
            'label' => 'Ønsket passord',
            'required' => true,
            'autocomplete' => 'off',
            'value' => ''
        ]);

        /** @var Field $passord2Felt */
        $passord2Felt = $this->app->vis(Field::class, [
            'name' => 'pw2',
            'id' => 'pw2',
            'type' => 'password',
            'label' => 'Gjenta ønsket passord',
            'required' => true,
            'autocomplete' => 'off',
            'value' => ''
        ]);

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/drift/index.php?oppslag=profil_skjema&oppdrag=ta_i_mot_skjema&skjema=passord',
                'formId' => 'passord',
                'buttonText' => 'Lagre',
                'fields' => [
                    $brukerIdFelt,
                    $brukernavnFelt,
                    $endreBrukernavnLenke,
                    $passord1Felt,
                    $passord2Felt
                ]
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}