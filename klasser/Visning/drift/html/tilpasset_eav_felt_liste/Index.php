<?php

namespace Kyegil\Leiebasen\Visning\drift\html\tilpasset_eav_felt_liste;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;

/**
 * Visning for liste over tilpassede EAV-felter i leiebasen
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $tabell
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\drift\html\tilpasset_eav_felt_liste
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/tilpasset_eav_felt_liste/Index.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /**
         * Kolonne-objekt for DataTables
         *
         * @link https://datatables.net/reference/option/columns
         */
        $kolonner = [
            (object)[
                'data' => 'id',
                'name' => 'id',
                'title' => 'Id',
                'orderable' => true,
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
//                'width' => '40px'
            ],
            (object)[
                'data' => 'kode',
                'name' => 'kode',
                'title' => 'Kode',
                'responsivePriority' => 1, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display' && row.er_egendefinert) {return leiebasen.kjerne.lenke('/drift/index.php?oppslag=tilpasset_eav_felt_skjema&id=' + row.id, data);} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
            (object)[
                'data' => 'modell',
                'name' => 'modell',
                'title' => 'Modell',
                'responsivePriority' => 2, // Høyere tall = lavere prioritet
            ],
            (object)[
                'data' => 'er_egendefinert',
                'name' => 'er_egendefinert',
                'title' => 'Egendefinert',
                'className' => 'dt-right',
                'responsivePriority' => 4, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return data ? '✔' : '';} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
            (object)[
                'data' => 'type',
                'name' => 'type',
                'title' => 'Datatype',
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
            ],
            (object)[
                'data' => 'rekkefølge',
                'name' => 'rekkefølge',
                'title' => 'Orden',
                'className' => 'dt-right',
                'responsivePriority' => 3, // Høyere tall = lavere prioritet
            ],
            (object)[
                'data' => 'beskrivelse',
                'orderable' => false,
                'hidden' => true,
            ],
        ];

        /**
         * Konfig-objekt for DataTables
         * * pageLength initiert antall per side
         * * lengthMenu
         *
         * @link https://datatables.net/reference/option/
         */
        $dataTablesConfig = (object)[
            'fixedHeader' => (object)[
                'footer' => true,
            ],
            'order' => [[0, 'desc']],
            'columns' => $kolonner,
            'pageLength' => 25,
            'pagingType'=> 'simple_numbers',
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'scrollCollapse' => false, // Collapse when no content
            'scrollY' => 'calc(100vh - 310px)', // Table height
            'createdRow' => new JsFunction('console.log(data.status);if(data.status) {$(row).addClass(data.status);} if(data.frosset) {$(row).addClass(\'frosset\');}', ['row', 'data', 'dataIndex']),
            'search' => (object)[
                'return' => true,
            ],
            'searchDelay' => 1000,
            'serverSide' => true,
            'processing' => true,
            'ajax' => (object)[
                'url' => '/drift/index.php?oppslag=tilpasset_eav_felt_liste&oppdrag=hent_data&data=egenskaper',
            ]
        ];

        $knapper = [
            $this->app->visTilbakeknapp(),
            $this->hentLinkKnapp(
                '/drift/index.php?oppslag=tilpasset_eav_felt_skjema&id=*',
                'Ny egenskap'),
        ];

        $datasett = [
            'varsler' => '',
            'tabell' => $this->app->vis(DataTable::class, [
                'tableId' => 'tilpasset_eav_felt_liste',
                'kolonner' => $kolonner,
                'dataTablesConfig' => $dataTablesConfig
            ]),
            'knapper' => $knapper,
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}