<?php

namespace Kyegil\Leiebasen\Visning\drift\html;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Visning\drift\html\shared\body\main\Content;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\ViewRenderer\ViewInterface;

class _MalSkjema extends Content
{
    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['modell'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData():_MalSkjema
    {
        /** @var Modell|null $modell */
        $modell = $this->hentRessurs('modell');

        /** @var Field $modellIdFelt */
        $modellIdFelt = $this->app->vis(Field::class, [
            'name' => 'id',
            'type' => 'hidden',
            'required' => true,
            'value' => $modell ? $modell->hentId() : '*'
        ]);

        /** @var string[]|ViewInterface[] $buttons */
        $knapper = [];
        $knapper[] = $this->app->visTilbakeknapp();
        if ($modell) {
            $knapper[] = new HtmlElement('a',
                [
                    'class' => 'button',
                    'onclick' => 'leiebasen.drift._malSkjema.slett()'
                ],
                'Slett'
            );
        }
        $knapper[] = new HtmlElement('button', ['type' => 'submit'], 'Lagre');

        $datasett = [
            'heading' => $this->app->tittel,
            'mainBodyContents' => $this->app->vis(AjaxForm::class, [
                'action' => $this->app::url('_mal_skjema', ($modell ? $modell->hentId() : '*'), 'mottak', ['skjema' => '_mal_skjema']),
                'formId' => '_mal_skjema',
                'buttonText' => 'Lagre',
                'fields' => [
                    $modellIdFelt,
                ],
                'useModal' => true,
            ]),
            'buttons' => $knapper,
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }
}