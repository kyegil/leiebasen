<?php

namespace Kyegil\Leiebasen\Visning\drift\html\leieberegning_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\CollapsibleSection;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\NumberField;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\Leiebasen\Visning\felles\html\form\TextArea;

/**
 * Visning for spesialregel i leieberegningsskjema i drift
 *
 *  Mulige variabler:
 *      $mal
 * @package Kyegil\Leiebasen\Visning\drift\html\leieberegning_skjema
 */
class RegelMal extends Drift
{
    /** @var string */
    protected $template = 'drift/html/leieberegning_skjema/RegelMal.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Field $spesialregelForklaringFelt */
        $spesialregelForklaringFelt = $this->app->vis(Field::class, [
            'name' => 'spesialregel[{#}][forklaring]',
            'label' => 'Forklaring av regel',
            'required' => true,
        ]);

        /** @var NumberField $spesialregelSatsFelt */
        $spesialregelSorteringsordenFelt = $this->app->vis(Field::class, [
            'type' => 'hidden',
            'name' => 'spesialregel[{#}][sorteringsorden]',
        ]);

        /** @var NumberField $spesialregelSatsFelt */
        $spesialregelSatsFelt = $this->app->vis(NumberField::class, [
            'name' => 'spesialregel[{#}][sats]',
            'min' => 0,
            'step' => 'any',
            'label' => 'Sats',
            'required' => true,
        ]);

        /** @var Checkbox $spesialregelUnntattFraLeiejusteringFelt */
        $spesialregelUnntattFraLeiejusteringFelt = $this->app->vis(Checkbox::class, [
            'id' => 'leieberegning-spesialregler-mal-felt-unntatt-fra-leiejustering',
            'name' => 'spesialregel[{#}][unntatt_fra_leiejustering]',
            'label' => 'Unntatt fra leiejustering',
        ]);

        /** @var Field $spesialregelProssessorFelt */
        $spesialregelProsessorFelt = $this->app->vis(Select::class, [
            'id' => 'leieberegning-spesialregler-mal-felt-prosessor',
            'name' => 'spesialregel[{#}][prosessor]',
            'label' => 'Prosessor',
            'required' => true,
            'options' => $this->hentProsessorOptions()
        ]);

        /** @var TextArea $spesialregelParamFelt */
        $spesialregelParamFelt = $this->app->vis(TextArea::class, [
            'name' => 'spesialregel[{#}][param]',
            'label' => 'Parametere for prosessering',
        ]);

        $spesialregelSletteKnapp = new HtmlElement('a',
            ['class' => 'button forkast-regel'],
            new HtmlElement('img', [
                'src' => '/pub/media/bilder/felles/forkast.svg',
                'title' => 'Forkast denne regelen',
                'onclick' => 'leiebasen.drift.leieberegningSkjema.forkastRegel({#});'
            ])
        );
        $spesialregelMal = $this->app->vis(CollapsibleSection::class, [
            'id' => 'leieberegning-spesialregler-mal-collapsible',
            'class' => 'leieberegning spesialregler mal',
            'collapsed' => false,
            'draggable' => true,
            'ondragstart' => 'leiebasen.drift.leieberegningSkjema.spesialregelOnDragStart(event)',
            'ondragend' => 'leiebasen.drift.leieberegningSkjema.spesialregelOnDragEnd(event)',

            'ondragover' => 'leiebasen.drift.leieberegningSkjema.spesialregelOnDragOver(event)',
            'contents' => [
                $spesialregelForklaringFelt,
                $spesialregelSorteringsordenFelt,
                $spesialregelSatsFelt,
                $spesialregelUnntattFraLeiejusteringFelt,
                $spesialregelProsessorFelt,
                $spesialregelParamFelt,
                $spesialregelSletteKnapp,
            ]
        ]);

        $datasett = [
            'mal' => $spesialregelMal
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return object
     */
    private function hentProsessorOptions(): object
    {
        $options = (object)[Leieberegning::class . '::formelProsessor' => 'formelProsessor'];
        return $this->app->after($this, __FUNCTION__, $options);
    }
}