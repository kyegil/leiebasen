<?php

namespace Kyegil\Leiebasen\Visning\drift\html\leieberegning_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\NumberField;
use Kyegil\Leiebasen\Visning\felles\html\form\TextArea;
use Kyegil\ViewRenderer\ViewInterface;

/**
 * Visning for leieberegningsskjema i drift
 *
 *  Ressurser:
 *      leieberegning \Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning
 *  Mulige variabler:
 *      $leieberegningId
 * @package Kyegil\Leiebasen\Visning\drift\html\leieberegning_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/leieberegning_skjema/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['leieberegning'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Leieberegning|null $leieberegning */
        $leieberegning = $this->hentRessurs('leieberegning');

        /** @var Field $brukernavnFelt */
        $leieberegningIdFelt = $this->app->vis(Field::class, [
            'name' => 'id',
            'type' => 'hidden',
            'required' => true,
            'value' => $leieberegning ? $leieberegning->hentId() : '*'
        ]);

        /** @var Field $navnFelt */
        $navnFelt = $this->app->vis(Field::class, [
            'name' => 'navn',
            'label' => 'Navn på beregningsmetode',
            'required' => true,
            'value' => $leieberegning ? $leieberegning->hentNavn() : null
        ]);

        /** @var TextArea $beskrivelseFelt */
        $beskrivelseFelt = $this->app->vis(TextArea::class, [
            'name' => 'beskrivelse',
            'label' => 'Beskrivelse',
            'required' => true,
            'value' => $leieberegning ? $leieberegning->hentBeskrivelse() : null
        ]);

        /** @var NumberField $leiePerObjektFelt */
        $leiePerObjektFelt = $this->app->vis(NumberField::class, [
            'name' => 'leie_per_objekt',
            'min' => 0,
            'step' => 'any',
            'label' => 'Grunnbeløp for hele leieobjektet (for bofellesskap deles beløpet mellom leieforholdene) i kr per år',
            'required' => false,
            'value' => $leieberegning ? $leieberegning->hentLeiePerObjekt() : null
        ]);

        /** @var NumberField $leiePerKontraktFelt */
        $leiePerKontraktFelt = $this->app->vis(NumberField::class, [
            'name' => 'leie_per_kontrakt',
            'min' => 0,
            'step' => 'any',
            'label' => 'Grunnbeløp per leieavtale i leieobjektet (i kr per år)',
            'required' => false,
            'value' => $leieberegning ? $leieberegning->hentLeiePerKontrakt() : null
        ]);

        /** @var NumberField $leiePerPerKvadratmeterFelt */
        $leiePerPerKvadratmeterFelt = $this->app->vis(NumberField::class, [
            'name' => 'leie_per_kvadratmeter',
            'min' => 0,
            'label' => 'Beløp per kvadratmeter (i kr per år)',
            'required' => false,
            'value' => $leieberegning ? $leieberegning->hentLeiePerKvadratmeter() : null
        ]);

        $spesialregelBeholder = new HtmlElement('div',
            ['class' => 'leieberegning spesialregler beholder'],
            new HtmlElement('div',
                ['class' => 'leieberegning spesialregler ny button', 'onclick' => 'leiebasen.drift.leieberegningSkjema.leggTilSpesialRegel()'],
                'Legg til spesialregel'
            )
        );

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [];
        if ($leieberegning) {
            $buttons[] = new HtmlElement('a',
                [
                    'class' => 'button',
                    'onclick' => 'leiebasen.drift.leieberegningSkjema.slettLeieberegning()'
                ],
                'Slett'
            );
        }
        $buttons[] = new HtmlElement('button', ['type' => 'submit'], 'Lagre');

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/drift/index.php?oppslag=leieberegning_skjema&oppdrag=ta_i_mot_skjema&skjema=leieberegningsskjema&id=' . ($leieberegning ? $leieberegning->hentId() : '*'),
                'formId' => 'leieberegningsskjema',
                'buttonText' => 'Lagre',
                'fields' => [
                    $leieberegningIdFelt,
                    $navnFelt,
                    $beskrivelseFelt,
                    $leiePerObjektFelt,
                    $leiePerKontraktFelt,
                    $leiePerPerKvadratmeterFelt,
                    $spesialregelBeholder
                ],
                'buttons' => $buttons
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}