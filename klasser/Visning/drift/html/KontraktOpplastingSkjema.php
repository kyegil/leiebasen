<?php

namespace Kyegil\Leiebasen\Visning\drift\html;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\Leiebasen\Visning\drift\html\shared\body\main\Content;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\FileField;
use Kyegil\ViewRenderer\ViewArray;

/** @inheritdoc  */
class KontraktOpplastingSkjema extends Content
{
    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): KontraktOpplastingSkjema
    {
        if(in_array($attributt,['kontrakt'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): KontraktOpplastingSkjema
    {
        /** @var Kontrakt|null $kontrakt */
        $kontrakt = $this->hentRessurs('kontrakt');
        $alleredeOpplastet = (bool)$this->hent('alleredeOpplastet');

        $felter = new ViewArray();

        if($alleredeOpplastet) {
            $advarsel = new HtmlElement('div', ['class' => 'alert alert-warning'],
                'Denne leieavtalen har allerede blitt lastet opp.<br>Dersom du laster opp på nytt vil den eksisterende bli erstattet'
            );
            $nedlastingsKnapp = new HtmlElement('a', [
                    'class' => 'btn btn-secondary',
                    'href' => DriftKontroller::url('kontrakt_signert', $kontrakt->hentId()),
                    'target' => '_blank',
                ], 'Last ned den eksisterende signerte avtalen'
            );
            $felter->addItem($advarsel);
            $felter->addItem($nedlastingsKnapp);
        }

        $filFelt = $this->app->vis(FileField::class, [
            'name' => 'signert_avtale',
            'label' => 'Signert avtale',
            'required' => true,
        ]);
        $felter->addItem($filFelt);

        $buttons[] = new HtmlElement('a', [
            'role' => 'button',
            'href' => $this->app->returi->get()->url,
            'class' => 'btn btn-secondary fortsett',
        ], 'Fortsett uten å laste opp');
        $buttons[] = new HtmlElement('button', [
            'role' => 'button',
            'type' => 'submit',
            'class' => 'button btn btn-primary',
            'form' => 'kontrakt_opplasting_skjema',
        ], 'Lagre opplastet fil');

        $datasett = [
            'mainBodyContents' => $this->app->vis(AjaxForm::class, [
                'useModal' => true,
                'action' => $this->app::url('kontrakt_opplasting_skjema', $kontrakt, 'mottak', ['skjema' => 'kontrakt_opplasting_skjema']),
                'formId' => 'kontrakt_opplasting_skjema',
                'fields' => $felter,
                'buttons' => ''
            ]),
            'buttons' => $buttons,
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }
}