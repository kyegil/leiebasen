<?php

namespace Kyegil\Leiebasen\Visning\drift\html;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\drift\html\shared\body\main\Content;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\auto_complete\Leieobjekter;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\html\form\RadioButton;
use Kyegil\Leiebasen\Visning\felles\html\form\RadioButtonGroup;
use Kyegil\Leiebasen\Visning\felles\html\person\Adressefelt;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

/** @inheritdoc  */
class LeieforholdRegningsadresseSkjema extends Content
{
    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): LeieforholdRegningsadresseSkjema
    {
        if(in_array($attributt,['leieforhold', 'kontraktpersoner', 'husholdningsmedlemmer'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): LeieforholdRegningsadresseSkjema
    {
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        /** @var ViewArray $regningspersonFelter */
        $regningspersonFelter = $this->hentregningspersonFelter($leieforhold);

        $regningTilObjektFelt = $this->app->vis(Checkbox::class, [
            'name' => 'regning_til_objekt',
            'label' => 'Lokal utlevering av regninger etc',
            'checked' => $leieforhold->regningTilObjekt,
        ]);

        $regningsobjektFelt = $this->app->vis(Leieobjekter::class, [
            'name' => 'regningsobjekt',
            'multiple' => false,
            'label' => 'Regninger etc utdeles lokalt til',
            'value' => (string)$leieforhold->regningsobjekt,
        ]);

        $efakturaFelt = null;
        if($this->app->hentValg('efaktura')) {
            $efakturaFelt = $this->app->vis(Checkbox::class, [
                'disabled' => true,
                'label' => 'eFaktura (administreres ifra nettbank)',
                'checked' => $leieforhold->giroformat == 'eFaktura',
            ]);
        }

        $regningsFormatFelt = $this->app->vis(RadioButtonGroup::class, [
            'name' => 'giroformat',
            'label' => 'Regningsformat',
            'value' => $leieforhold->hent('giroformat'),
            'options' => $this->hentRegningsformatOptions(),
        ]);
        /** @var ViewArray $optionFields */
        $optionFields = $regningsFormatFelt->hent('optionFields') ?? new ViewArray();
        $optionFields->addItem($efakturaFelt);

        $felter = new HtmlElement('div', ['class' => 'container'], [
            $regningTilObjektFelt,
            $regningsobjektFelt,
            $regningspersonFelter,
            $regningsFormatFelt,
        ]);

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [];
        $buttons[] = $this->app->visTilbakeknapp();
        $buttons[] = new HtmlElement('button', ['role'=> 'button', 'type' => 'submit', 'form' => 'regningsadresse_skjema'], 'Lagre');

        $datasett = [
            'mainBodyContents' => $this->app->vis(AjaxForm::class, [
                'useModal' => true,
                'action' => $this->app::url('leieforhold_regningsadresse', $leieforhold, 'mottak', ['skjema' => 'regningsadresse']),
                'formId' => 'regningsadresse_skjema',
                'fields' => $felter,
                'buttons' => ''
            ]),
            'buttons' => $buttons,
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }

    /**
     * @param Leieforhold|null $leieforhold
     * @return mixed|null
     * @throws Exception
     */
    private function hentregningspersonFelter(?Leieforhold $leieforhold)
    {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $felter = new ViewArray();
        $eFakturaAktivert = (bool)$this->app->hentValg('efaktura');

        foreach($leieforhold->hentLeietakere() as $leietaker) {
            $person = $leietaker->person;
            $harEfaktura = $eFakturaAktivert && $person->hentEfakturaIdentifier();
            if($person) {
                $radio = $this->app->vis(RadioButton::class, [
                    'name' => 'regningsperson',
                    'label' => $person->hentNavn(),
                    'value' => (string)$person,
                    'checked' => $leieforhold && $leieforhold->regningsperson->hentId() == $person->hentId()
                ]);
                $efakturaLabel = new HtmlElement(
                    'div', ['hidden' => true, 'class' => 'efaktura-label'],
                    $person->hentNavn() . ($harEfaktura ? ' (har eFaktura):' : ':')
                );
                $adresseLabel = new HtmlElement(
                    'div', ['hidden' => true, 'class' => 'adresse-label'],
                    "{$person->hentNavn()}'s adresse" . ($harEfaktura ? ' (har eFaktura):' : ':')
                );
                $tekst = new HtmlElement(
                    'div', ['class' => 'adressefelt'],
                    [$this->app->vis(Adressefelt::class, ['person' => $person, 'navn' => ''])]
                );
                $felter->addItem(new HtmlElement('div', ['class' => 'col-md'], [$radio, $tekst, $efakturaLabel, $adresseLabel]));
            }
        }

        $uavhengigAdresseRadio = $this->app->vis(RadioButton::class, [
            'name' => 'regningsperson',
            'label' => 'Oppgi en uavhengig regningsadresse for leieforholdet' . ($eFakturaAktivert ? ' (E-faktura ikke tilgjengelig):' : ':'),
            'value' => '',
            'checked' => !$leieforhold->regningsperson
        ]);


        $adresse1Felt = $this->app->vis(Field::class, [
            'name' => 'regningsadresse1',
            'required' => true,
            'label' => 'Adresse',
            'value' => $leieforhold->regningsadresse1
        ]);

        $adresse2Felt = $this->app->vis(Field::class, [
            'name' => 'regningsadresse2',
            'value' => $leieforhold->regningsadresse2
        ]);

        $postnrFelt = $this->app->vis(Field::class, [
            'name' => 'postnr',
            'class' => 'col',
            'required' => true,
            'label' => 'Postnummer',
            'value' => $leieforhold->postnr
        ]);

        $poststedFelt = $this->app->vis(Field::class, [
            'name' => 'poststed',
            'class' => 'col',
            'required' => true,
            'label' => 'Sted',
            'value' => $leieforhold->poststed
        ]);

        $landFelt = $this->app->vis(Field::class, [
            'name' => 'land',
            'label' => 'Land',
            'value' => $leieforhold->land
        ]);

        $uavhengigAdresseFelter = new HtmlElement('div',
            ['class' => 'uavhengigAdresseFeltsett collapse'],
            [
                $adresse1Felt,
                $adresse2Felt,
                new HtmlElement('div', ['class' => 'row'], [$postnrFelt, $poststedFelt]),
                $landFelt,
            ]
        );

        $feltsett = $this->app->vis(FieldSet::class, [
            'class' => 'regningsperson-feltsett collapse',
            'label' => 'Efaktura-mottaker og adresse for regninger',
            'contents' => [
                new HtmlElement('div', ['class' => 'row justify-content-around'], $felter),
                $uavhengigAdresseRadio,
                $uavhengigAdresseFelter,
            ],
        ]);

        return $this->app->post($this, __FUNCTION__, $feltsett, $args);
    }

    /**
     * @return object
     */
    private function hentRegningsformatOptions(): object
    {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $options = (object)[
            '' => 'Ønsker ikke regninger tilsendt (holder egen oversikt)',
            'papir' => 'Papir-regninger',
            'epost' => 'Regninger sendt med e-post',
        ];
        return $this->app->post($this, __FUNCTION__, $options, $args);
    }
}