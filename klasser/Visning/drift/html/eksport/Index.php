<?php

namespace Kyegil\Leiebasen\Visning\drift\html\eksport;


use DateInterval;
use DateTimeImmutable;
use Exception;
use IntlDateFormatter;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Eksportprofil;
use Kyegil\Leiebasen\Modell\Eksportprofilsett;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\html\form\RadioButtonGroup;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use stdClass;

/**
 * Visning for eksportskjema i drift
 *
 *  Ressurser:
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\drift\html\eksport
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/eksport/Index.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var RadioButtonGroup $typeFelt */
        $typeFelt = $this->app->vis(RadioButtonGroup::class, [
            'label' => 'Eksport-type',
            'name' => 'type',
            'options' => (object)[
                'standard' => 'Standard-eksport',
                'tilpasset' => 'Spesiatilpasset eksport'
            ],
            'value' => 'standard'
        ]);

        /** @var Select $standardEksportFelt */
        $standardEksportFelt = $this->app->vis(Select::class, [
            'name' => 'profil[standard]',
            'class' => 'standard-eksport',
            'label' => 'Standard-eksport',
            'required' => true,
            'options' => $this->hentStandardEksporter(),
            'value' => null
        ]);

        /** @var Select $tilpassetEksportFelt */
        $tilpassetEksportFelt = $this->app->vis(Select::class, [
            'name' => 'profil[tilpasset]',
            'class' => 'tilpasset-eksport',
            'label' => 'Spesiatilpasset eksport',
            'required' => true,
            'options' => $this->hentTilpassedeEksporter(),
            'value' => null,
        ]);

        /** @var Select $kontoFelt */
        $periodeFelt = $this->app->vis(Select::class, [
            'name' => 'periode',
            'label' => 'Periode',
            'required' => true,
            'options' => $this->hentPerioder(),
            'value' => date_create_immutable()->sub(new DateInterval('P1M'))->format('Y-m'),
        ]);

        /** @var DateField $fradatoFelt */
        $fradatoFelt = $this->app->vis(DateField::class, [
            'name' => 'fradato',
            'label' => 'Fra dato',
            'required' => true,
        ]);

        /** @var DateField $tildatoFelt */
        $tildatoFelt = $this->app->vis(DateField::class, [
            'name' => 'tildato',
            'label' => 'Til dato',
            'required' => true,
        ]);

        /** @var Select $datoformatFelt */
        $datoformatFelt = $this->app->vis(Select::class, [
            'name' => 'datoformat',
            'label' => 'Datoformat',
            'required' => true,
            'options' => $this->hentDatoformater(),
            'value' => 'Y-m-d',
        ]);

        /** @var Select $skilletegnFelt */
        $skilletegnFelt = $this->app->vis(Select::class, [
            'name' => 'skilletegn',
            'label' => 'CSV Skilletegn (delimiter)',
            'required' => true,
            'options' => (object)[
                htmlspecialchars(',') => 'komma (,)',
                htmlspecialchars(';') => 'semikolon (;)',
                htmlspecialchars("\t") => 'tabulator-tegn (\\t)',
            ],
            'value' => htmlspecialchars(','),
        ]);

        /** @var Select $innpakningFelt */
        $innpakningFelt = $this->app->vis(Select::class, [
            'name' => 'innpakning',
            'label' => 'Tekstkvalifikator',
            'required' => true,
            'options' => (object)[
                htmlspecialchars("'") => ("Enkle gåseøyne (')"),
                htmlspecialchars('"') => ('Doble gåseøyne (")')
            ],
            'value' => htmlspecialchars('"'),
        ]);

        /** @var FieldSet $eksportTypeFeltsett */
        $eksportTypeFeltsett = $this->app->vis(FieldSet::class, [
            'label' => 'Eksport',
            'contents' => [
                $typeFelt,
                $standardEksportFelt,
                $tilpassetEksportFelt,
            ]
        ]);

        /** @var FieldSet $periodeFeltsett */
        $periodeFeltsett = $this->app->vis(FieldSet::class, [
            'label' => 'Tidsrom',
            'contents' => [
                $periodeFelt,
                $fradatoFelt,
                $tildatoFelt,
            ]
        ]);

        /** @var FieldSet $formatFeltsett */
        $formatFeltsett = $this->app->vis(FieldSet::class, [
            'label' => 'Eksportformat',
            'contents' => [
                $skilletegnFelt,
                $innpakningFelt,
                $datoformatFelt,
            ]
        ]);

        /** @var HtmlElement $eksportKnapp */
        $eksportKnapp = new HtmlElement('button', ['type' => 'submit'], 'Eksportér');

        $datasett = [
            'skjema' => new HtmlElement('form', [
                'id' => 'eksportvalg',
                'method' => 'GET',
            ], [
                $this->app->vis(Field::class, ['type' => 'hidden', 'name' => 'oppslag', 'value' => 'eksport']),
                $this->app->vis(Field::class, ['type' => 'hidden', 'name' => 'oppdrag', 'value' => 'hent_data']),
                $this->app->vis(Field::class, ['type' => 'hidden', 'name' => 'data', 'value' => 'eksport']),
                $eksportTypeFeltsett,
                $periodeFeltsett,
                $formatFeltsett,
                $eksportKnapp,
            ]),
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return object
     */
    private function hentStandardEksporter(): object
    {
        $resultat = (object)[
            'leieforhold' => 'Leieforhold påbegynt i tidsrommet',
            'krav' => 'Krav',
            'utestående' => 'Alt utestående per dato',
            'betalinger' => 'Betalinger',
            'bygningsregnskap' => 'Inntekter per bygning',
            'regninger_arkiv' => 'Alle regninger som filarkiv',
        ];
        /** @var stdClass $resultat */
        $resultat = $this->app->after($this, __FUNCTION__, $resultat);
        return $resultat;
    }

    /**
     * @return object
     * @throws Exception
     */
    private function hentTilpassedeEksporter(): object
    {
        $resultat = new stdClass();
        /** @var Eksportprofilsett $eksportprofiler */
        $eksportprofiler = $this->app->hentSamling(Eksportprofil::class);
        foreach($eksportprofiler as $eksportprofil) {
            $resultat->{$eksportprofil->hentId()} = $eksportprofil->hentNavn();
        }
        /** @var stdClass $resultat */
        $resultat = $this->app->after($this, __FUNCTION__, $resultat);
        return $resultat;
    }

    /**
     * @return object
     */
    private function hentPerioder(): object
    {
        $resultat = new stdClass();
        $periode = new DateTimeImmutable();
        for($i=0;$i<7;$i++) {
            $periode = $periode->sub(new DateInterval('P1M'));
            $resultat->{$periode->format('Y-m')}
                = \Kyegil\Leiebasen\Leiebase::ucfirst(IntlDateFormatter::create(
                    'nb_NO',
                null, null, null, null,
                'MMMM yyyy')
                ->format($periode));
        }
        $resultat->{''} = 'Angi fra- og til-dato';
        /** @var stdClass $resultat */
        $resultat = $this->app->after($this, __FUNCTION__, $resultat);
        return $resultat;
    }

    /**
     * @return object
     */
    private function hentDatoformater():object
    {
        $resultat = (object)[
            'Y-m-d' => 'Y-m-d (1999-12-31)',
            'Ymd'   => 'Ymd (19991231)',
            'd.m.Y' => 'd.m.Y (31.12.1999)',
            'd.m.y' => 'd.m.y (31.12.99)',
            'd/m/Y' => 'd/m/Y (31/12/1999)',
            'd/m/y' => 'd/m/y (31/12/99)',
            'dmY'   => 'dmY (31121999)',
            'm/d/Y' => 'm/d/Y (12/31/1999)',
            'm/d/y' => 'm/d/y (12/31/99)'
        ];
        /** @var stdClass $resultat */
        $resultat = $this->app->after($this, __FUNCTION__, $resultat);
        return $resultat;
    }
}