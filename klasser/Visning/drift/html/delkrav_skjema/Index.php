<?php

namespace Kyegil\Leiebasen\Visning\drift\html\delkrav_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\DisplayField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\NumberField;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\Leiebasen\Visning\felles\html\form\TextArea;
use Kyegil\ViewRenderer\ViewInterface;

/**
 * Visning for konfigurering av delkrav i drift
 *
 *  Ressurser:
 *      delkravtype \Kyegil\Leiebasen\Modell\Delkravtype
 *  Mulige variabler:
 *      $delkravtypeId
 * @package Kyegil\Leiebasen\Visning\drift\html\delkrav_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/delkrav_skjema/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return Index
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Index
    {
        if(in_array($attributt,['delkravtype'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return Index
     * @throws Exception
     */
    protected function forberedData(): Index
    {
        /** @var Delkravtype|null $delkravtype */
        $delkravtype = $this->hentRessurs('delkravtype');

        /** @var Field $brukernavnFelt */
        $delkravtypeIdFelt = $this->app->vis(Field::class, [
            'name' => 'id',
            'type' => 'hidden',
            'required' => true,
            'value' => $delkravtype ? $delkravtype->hentId() : '*'
        ]);

        /** @var Checkbox $aktivFelt */
        $aktivFelt = $this->app->vis(Checkbox::class, [
            'name' => 'aktiv',
            'label' => 'Aktivert',
            'checked' => $delkravtype && $delkravtype->hentAktiv()
        ]);

        if($delkravtype) {
            /** @var DisplayField $kravTypeFelt */
            $kravTypeFelt = $this->app->vis(DisplayField::class, [
                'label' => 'Delkrav som følger krav av type',
                'required' => true,
                'value' => $delkravtype->hentKravtype()
            ]);
        }
        else {
            /** @var Select $kravTypeFelt */
            $kravTypeFelt = $this->app->vis(Select::class, [
                'name' => 'kravtype',
                'label' => 'Delkrav som følger krav av type',
                'required' => true,
                'options' => $this->hentKravtypeOptions()
            ]);
        }

        /** @var Checkbox $indeksreguleresFelt */
        $tilleggFelt = $this->app->vis(Checkbox::class, [
            'name' => 'selvstendig_tillegg',
            'label' => 'Selvstendig tillegg (Beløpet utgjør ikke en del av hovedbeløpet, men blir krevd inn som et selvstendig krav)',
            'checked' => $delkravtype && $delkravtype->hentSelvstendigTillegg()
        ]);

        /** @var Field $kodeFelt */
        $kodeFelt = $this->app->vis(Field::class, [
            'name' => 'kode',
            'label' => 'Kode',
            'pattern' => '^[A-Za-zÀ-ÖØ-öø-ÿ0-9]+$',
            'required' => true,
            'value' => $delkravtype ? $delkravtype->hentKode() : null
        ]);

        /** @var Field $navnFelt */
        $navnFelt = $this->app->vis(Field::class, [
            'name' => 'navn',
            'label' => 'Navn',
            'required' => true,
            'value' => $delkravtype ? $delkravtype->hentNavn() : null
        ]);

        /** @var TextArea $beskrivelseFelt */
        $beskrivelseFelt = $this->app->vis(TextArea::class, [
            'name' => 'beskrivelse',
            'label' => 'Beskrivelse',
            'required' => true,
            'value' => $delkravtype ? $delkravtype->hentBeskrivelse() : null
        ]);

        /** @var Checkbox $valgfrittFelt */
        $valgfrittFelt = $this->app->vis(Checkbox::class, [
            'name' => 'valgfritt',
            'label' => 'Valgfritt (Beløpet kan avtales ved kontraktinngåelse)',
            'checked' => $delkravtype && $delkravtype->hentValgfritt()
        ]);

        /** @var Checkbox $relativFelt */
        $relativFelt = $this->app->vis(Checkbox::class, [
            'name' => 'relativ',
            'label' => 'Relativt beløp (oppgis som en prosentdel av hovedkravet)',
            'checked' => $delkravtype && $delkravtype->hentRelativ()
        ]);

        /** @var Checkbox $indeksreguleresFelt */
        $indeksreguleresFelt = $this->app->vis(Checkbox::class, [
            'name' => 'indeksreguleres',
            'label' => 'Beløpet indeksreguleres automatisk',
            'checked' => $delkravtype && $delkravtype->hent('indeksreguleres')
        ]);

        /** @var NumberField $satsFelt */
        $satsFelt = $this->app->vis(NumberField::class, [
            'name' => 'sats',
            'label' => 'Sats',
            'step' => 'any',
            'required' => true,
            'value' => $delkravtype ? $delkravtype->hentSats() : null
        ]);

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [];
        $buttons[] = new HtmlElement('button', ['type' => 'submit'], 'Lagre');

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/drift/index.php?oppslag=delkrav_skjema&oppdrag=ta_i_mot_skjema&skjema=delkravtypeskjema&id=' . ($delkravtype ? $delkravtype->hentId() : '*'),
                'formId' => 'delkravtypeskjema',
                'fields' => [
                    $delkravtypeIdFelt,
                    $kravTypeFelt,
                    $tilleggFelt,
                    $aktivFelt,
                    $navnFelt,
                    $kodeFelt,
                    $beskrivelseFelt,
                    $valgfrittFelt,
                    $relativFelt,
                    $satsFelt,
                    $indeksreguleresFelt,
                ],
                'buttons' => $buttons
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    private function hentKravtypeOptions(): array
    {
        $this->app->before($this, __FUNCTION__, []);
        $options = Krav::$typer;
        return $this->app->after($this, __FUNCTION__, $options);
    }
}