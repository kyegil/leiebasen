<?php

namespace Kyegil\Leiebasen\Visning\drift\html\betaling_skjema;


use Exception;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Konto;
use Kyegil\Leiebasen\Modell\Kontosett;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\DecimalField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use stdClass;

/**
 * Visning for betalingsskjema i drift
 *
 *  Ressurser:
 *      transaksjon \Kyegil\Leiebasen\Modell\Innbetaling
 *  Mulige variabler:
 *      $transaksjonId
 * @package Kyegil\Leiebasen\Visning\drift\html\betaling_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/betaling_skjema/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['transaksjon'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Innbetaling|null $transaksjon */
        $transaksjon = $this->hentRessurs('transaksjon');

        /** @var Field $brukernavnFelt */
        $transaksjonIdFelt = $this->app->vis(Field::class, [
            'name' => 'id',
            'type' => 'hidden',
            'required' => true,
            'value' => $transaksjon ? $transaksjon->hentId() : '*'
        ]);

        /** @var Field $datoFelt */
        $datoFelt = $this->app->vis(DateField::class, [
            'name' => 'dato',
            'label' => 'Transaksjonsdato',
            'required' => true,
            'value' => $transaksjon ? $transaksjon->hentDato()->format('Y-m-d') : null
        ]);

        /** @var Field $kontoFelt */
        $kontoFelt = $this->app->vis(Select::class, [
            'name' => 'konto',
            'label' => 'Konto',
            'required' => true,
            'options' => $this->hentKontoer(),
            'value' => $transaksjon ? $transaksjon->hentKonto() : null
        ]);

        /** @var Field $beløpFelt */
        $beløpFelt = $this->app->vis(DecimalField::class, [
            'name' => 'beløp',
            'label' => 'Innbetalt beløp<br>(Positivt beløp for betaling inn til konto, negativt beløp for betaling ut av konto)',
            'required' => true,
            'value' => $transaksjon ? $transaksjon->hentBeløp() : null
        ]);

        /** @var Field $betalerFelt */
        $betalerFelt = $this->app->vis(Field::class, [
            'name' => 'betaler',
            'label' => 'Avsender (mottaker ved betaling ut av konto), som oppgitt på transaksjonsbilag, kontoutskrift e.l.',
            'required' => true,
            'value' => $transaksjon ? $transaksjon->hentBetaler() : null
        ]);

        /** @var Field $referanseFelt */
        $referanseFelt = $this->app->vis(Field::class, [
            'name' => 'referanse',
            'label' => 'Unik (per dato) referanse til bilag/kontotransaksjon e.l.',
            'required' => true,
            'value' => $transaksjon ? $transaksjon->hentRef() : null
        ]);

        /** @var Field $merknadFelt */
        $merknadFelt = $this->app->vis(Field::class, [
            'name' => 'merknad',
            'label' => 'Evt. merknader',
            'value' => $transaksjon ? $transaksjon->hentMerknad() : null
        ]);

        $transaksjonFeltsett = $this->app->vis(FieldSet::class, [
            'label' => 'Transaksjon',
            'contents' => [
                $transaksjonIdFelt,
                $datoFelt,
                $kontoFelt,
                $beløpFelt,
                $betalerFelt,
                $referanseFelt,
                $merknadFelt
            ]
        ]);

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/drift/index.php?oppslag=betaling_skjema&oppdrag=ta_i_mot_skjema&skjema=betalingsskjema&id=' . ($transaksjon ?? '*'),
                'formId' => 'betalingsskjema',
                'buttonText' => 'Lagre',
                'fields' => [
                    $transaksjonFeltsett,
                ],
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    private function hentKontoer(): stdClass
    {
        $resultat = new stdClass();
        /** @var Kontosett $kontosett */
        $kontosett = $this->app->hentSamling(Konto::class);
        $kontosett->leggTilFilter(['`' . Konto::hentTabell(). '`.`' . Konto::hentPrimærnøkkelfelt(). '` > 0']);
        foreach ($kontosett as $konto) {
            $resultat->{$konto->hentNavn()} = $konto->hentNavn();
        }
        /** @var stdClass $resultat */
        $resultat = $this->app->after($this, __FUNCTION__, $resultat);
        return $resultat;
    }
}