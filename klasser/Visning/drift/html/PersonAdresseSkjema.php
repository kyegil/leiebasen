<?php

namespace Kyegil\Leiebasen\Visning\drift\html;


use Exception;
use Kyegil\Leiebasen\EavVerdiVisningRenderer;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Egenskapsett;
use Kyegil\Leiebasen\Modell\Eav\Verdi;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\drift\html\shared\body\main\Content;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\CollapsibleSection;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

/** @inheritdoc  */
class PersonAdresseSkjema extends Content
{
    /**
     * @inheritdoc
     */
    public function sett($attributt, $verdi = null): PersonAdresseSkjema
    {
        if($attributt == 'person') {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): PersonAdresseSkjema
    {
        /** @var Person|null $person */
        $person = $this->hentRessurs('person');

        $felter = new ViewArray();

        $felter->addItem(
            new HtmlElement('div', [], $person ? $person->hentId() : null),
        );

        $felter->addItem($this->app->vis(Field::class, [
            'name' => 'id',
            'type' => 'hidden',
            'value' => $person ? $person->hentId() : null,
        ]));

        $felter->addItem($this->app->vis(Checkbox::class, [
            'name' => 'er_org',
            'label' => 'Firma/organisasjon',
            'checked' => $person ? $person->erOrg : null,
        ]));

        $felter->addItem($this->app->vis(Field::class, [
            'name' => 'fornavn',
            'label' => 'Fornavn',
            'value' => $person ? $person->fornavn : null,
            'required' => true,
        ]));

        $felter->addItem($this->app->vis(Field::class, [
            'name' => 'etternavn',
            'label' => 'Etternavn',
            'value' => $person ? $person->etternavn : null,
            'required' => true,
        ]));

        $felter->addItem($this->app->vis(DateField::class, [
            'name' => 'fødselsdato',
            'label' => 'Fødselsdato',
            'value' => ($person && $person->fødselsdato) ? $person->fødselsdato->format('Y-m-d') : null,
        ]));

        $felter->addItem($this->app->vis(Field::class, [
            'name' => 'personnr',
            'label' => 'Personnummer (5 eller 11 sifre)',
            'pattern' => '[0-9]{5}',
            'value' => $person ? $person->personnr : null,
        ]));

        $adresse1 = $this->app->vis(Field::class, [
            'name' => 'adresse1',
            'label' => 'Gateadresse',
            'value' => $person ? $person->adresse1 : null,
        ]);

        $adresse2 = $this->app->vis(Field::class, [
            'name' => 'adresse2',
            'value' => $person ? $person->adresse2 : null,
        ]);

        $postnr = $this->app->vis(Field::class, [
            'name' => 'postnr',
            'label' => 'Postnummer',
            'value' => $person ? $person->postnr : null,
        ]);

        $poststed = $this->app->vis(Field::class, [
            'name' => 'poststed',
            'label' => 'Poststed',
            'value' => $person ? $person->poststed : null,
        ]);

        $land = $this->app->vis(Field::class, [
            'name' => 'land',
            'label' => 'Land',
            'value' => $person && $person->land ? $person->land : 'Norge',
        ]);

        $felter->addItem($this->app->vis(FieldSet::class, [
            'label' => 'Postadresse',
            'contents' => [
                $adresse1, $adresse2, $postnr, $poststed, $land,
            ],
        ]));

        $felter->addItem($this->app->vis(Field::class, [
            'name' => 'epost',
            'type' => 'email',
            'label' => 'E-post',
            'value' => $person ? $person->epost : null,
        ]));

        $felter->addItem($this->app->vis(Field::class, [
            'name' => 'mobil',
            'type' => 'tel',
            'pattern' => '\+?[0-9]+',
            'label' => 'Mobil/SMS telefonnummer',
            'value' => $person ? $person->mobil : null,
        ]));

        $felter->addItem($this->app->vis(Field::class, [
            'name' => 'telefon',
            'type' => 'tel',
            'label' => 'Annet telefonnummer',
            'value' => $person ? $person->telefon : null,
        ]));

        $this->forberedPersonEgendefinerteFelter($person, $felter);
        $this->forberedLeietakerEgendefinerteFelter($person, $felter);

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [];
        $buttons[] = $this->app->visTilbakeknapp();
        $buttons[] = new HtmlElement('div', [
            'id' => 'sletteknapp',
            'role' => 'button',
            'class' => 'btn btn-secondary fortsett',
            'onclick' => 'leiebasen.drift.personAdresseSkjema.bekreftOgSlettAdressekort()',
        ], 'Slett adressekortet'
        );
        $buttons[] = new HtmlElement('button', ['role'=> 'button', 'type' => 'submit', 'form'=> 'adressekort'], 'Lagre');

        $datasett = [
//            'heading' => 'Page Title',
            'mainBodyContents' => $this->app->vis(AjaxForm::class, [
                'useModal' => true,
                'action' => $this->app::url('person_adresse_skjema', $person ?? '*', 'mottak', ['skjema' => 'adressekort']),
                'formId' => 'adressekort',
                'fields' => $felter,
                'buttons' => ''
            ]),
            'buttons' => $buttons,
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }

    /**
     * @param Person|null $person
     * @param ViewArray|null $felter
     * @return ViewArray
     * @throws Exception
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    private function forberedPersonEgendefinerteFelter(?Person $person, ?ViewArray $felter = null): ViewArray
    {
        $felter = $felter ?? new ViewArray();

        /** @var Egenskapsett $eavEgenskaper */
        $eavEgenskaper = $this->app->hentSamling(Egenskap::class);
        $eavEgenskaper->leggTilEavVerdiJoin('felt_type', true);
        $eavEgenskaper->leggTilSortering('rekkefølge');
        $eavEgenskaper->leggTilFilter(['modell' => Person::class])->låsFiltre();

        foreach($eavEgenskaper as $egenskap) {
            /** @var Verdi|null $verdiObjekt */
            $verdiObjekt = $person
                ? $egenskap->hentVerdier()
                    ->leggTilFilter(['objekt_id' => $person->hentId()])
                    ->låsFiltre()->hentFørste()
                : null;
            $felter->addItem($egenskap->visVerdi($verdiObjekt, 'felt_type'));
        }
        return $felter;
    }

    /**
     * @param Person|null $person
     * @param ViewArray|null $felter
     * @return ViewArray
     * @throws Exception
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    private function forberedLeietakerEgendefinerteFelter(?Person $person, ?ViewArray $felter = null): ViewArray
    {
        $felter = $felter ?? new ViewArray();

        /** @var Egenskapsett $leietakerEgenskaper */
        $leietakerEgenskaper = $this->app->hentSamling(Egenskap::class);
        $leietakerEgenskaper->leggTilEavVerdiJoin('felt_type', true);
        $leietakerEgenskaper->leggTilSortering('rekkefølge');
        $leietakerEgenskaper->leggTilFilter(['modell' => Leietaker::class])->låsFiltre();

        if($leietakerEgenskaper->hentAntall()) {
            foreach($person->hentKontraktLinker() as $leietaker) {
                $leietakerfelter = new ViewArray();
                foreach($leietakerEgenskaper as $egenskap) {
                    /** @var Verdi|null $verdiObjekt */
                    $verdiObjekt = $leietaker
                        ? $egenskap->hentVerdier()
                            ->leggTilFilter(['objekt_id' => $leietaker->hentId()])
                            ->låsFiltre()->hentFørste()
                        : null;

                    /** @var object|null $feltType */
                    $feltType = $egenskap->hentFeltType();
                    /** @var string|null $visningType */
                    $visningType = $feltType ? ($feltType->type ?? null) : null;
                    /** @var class-string<EavVerdiVisningRenderer> $renderKlasse */
                    $renderKlasse = Egenskap::$renderProsessorMapping['felt_type'][$visningType] ?? null;
                    if(is_a($renderKlasse, EavVerdiVisningRenderer::class, true)) {
                        $felt = $renderKlasse::vis(
                            $verdiObjekt,
                            (object)['name' => 'leietaker-egenskaper[' . $leietaker->hentId() . '][' . $egenskap->kode . ']'],
                            $egenskap,
                            $visningType,
                            'felt_type'
                        );
                        $leietakerfelter->addItem($felt);
                    }
                }
                $felter->addItem($this->app->vis(CollapsibleSection::class, [
                    'label' => 'Leieforhold ' . $leietaker->leieforhold . ': ' . $leietaker->leieforhold->hentNavn(),
                    'contents' => $leietakerfelter,
                ]));
            }
        }

        return $felter;
    }
}