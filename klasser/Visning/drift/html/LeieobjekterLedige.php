<?php

namespace Kyegil\Leiebasen\Visning\drift\html;


use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Visning\drift\html\leieobjekter_ledige\KommendeLedighetLinje;
use Kyegil\Leiebasen\Visning\drift\html\leieobjekter_ledige\LeieobjektLinje;
use Kyegil\Leiebasen\Visning\drift\html\shared\body\main\Content;
use Kyegil\Leiebasen\Visning\felles\html\bootstrap\Card;
use Kyegil\Leiebasen\Visning\felles\html\shared\body\main\Kort;
use Kyegil\Leiebasen\Visning\felles\html\table\HtmlTable;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

/**
 * @inheritdoc
 */
class LeieobjekterLedige extends Content
{
    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): LeieobjekterLedige
    {
        $iDag = new DateTimeImmutable();

        /** @var string[]|ViewInterface[] $knapper */
        $knapper = [];
        $knapper[] = $this->app->visTilbakeknapp();
        $datasett = [
            'heading' => $this->app->tittel,
            'mainBodyContents' => [
                $this->app->vis(Card::class, [
                    'header' => 'Ledige leieobjekter og lokaler per ' . $iDag->format('d.m.Y'),
                    'innhold' => $this->hentLedigeLeieobjekter($iDag),
                ]),
                $this->app->vis(Card::class, [
                    'header' => 'Kommende inn- og utflyttinger',
                    'innhold' => $this->hentKommendeLedigheter($iDag),
                ]),
            ],
            'buttons' => $knapper,
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }

    /**
     * @param DateTimeInterface|null $dato
     * @return HtmlTable
     * @throws Exception
     */
    private function hentLedigeLeieobjekter(?DateTimeInterface $dato = null): HtmlTable
    {
        if(!$dato) {
            $dato = new DateTimeImmutable();
        }
        $kolonner = [
            (object)['title' => 'Leieobjekt nr.', 'classes' => ['d-none', 'd-sm-block']],
            (object)['title' => 'Beskrivelse'],
            (object)['title' => 'Forrige Leietaker'],
        ];
        $ledigLeieobjektLinjer = new ViewArray();
        $leieobjektsett = $this->app->hentAktiveLeieobjekter();
        /** @var Leieobjekt $leieobjekt */
        foreach ($leieobjektsett as $leieobjekt) {
            $ledighet = $leieobjekt->hentLedighet($dato);
            $leietakerliste = new ViewArray();
            /** @var Leieforhold $leieforhold */
            foreach($leieobjekt->hentLeieforhold($dato, $dato) as $leieforhold) {
                $andel = $leieforhold->hentAndel();
                $leietakerliste->addItem(($andel->asDecimal() == 1 ? 'Leies av ' : $andel->asMixedNumber(true)  . ' leies av ') . $leieforhold->hentNavn() . '<br>');
            }
            if($ledighet->asDecimal() > 0) {
                $sisteAvsluttedeAndelsperiode = $leieobjekt->hentSisteAvsluttedeAndelsperiode($dato);
                $ledigLeieobjektLinjer->addItem($this->app->vis(LeieobjektLinje::class, [
                    'leieobjekt' => $leieobjekt,
                    'sisteLeieforhold' => $sisteAvsluttedeAndelsperiode ? $sisteAvsluttedeAndelsperiode->leieforhold : null,
                    'leietakerliste' => $leietakerliste
                ]));
            }
        }

        return $this->app->vis(HtmlTable::class, [
            'classes' => ['table'],
            'kolonner' => $kolonner,
            'tableBody' => $ledigLeieobjektLinjer,
        ]);
    }

    /**
     * @param DateTimeInterface|null $dato
     * @return HtmlTable
     * @throws Exception
     */
    protected function hentKommendeLedigheter(DateTimeInterface $dato = null): HtmlTable
    {
        if(!$dato) {
            $dato = new DateTimeImmutable();
        }
        $endringer = [];
        $kommendeOppsigelser = $this->app->hentSamling(Leieforhold\Oppsigelse::class)
            ->leggTilFilter(['`' . Leieforhold\Oppsigelse::hentTabell() . '`.`fristillelsesdato` >' => $dato->format('Y-m-d')])
            ->leggTilSortering('fristillelsesdato', false, Leieforhold\Oppsigelse::hentTabell());
        /** @var Leieforhold\Oppsigelse $oppsigelse */
        foreach($kommendeOppsigelser as $oppsigelse) {
            $endringer[$oppsigelse->fristillelsesdato->format('Y-m-d')][$oppsigelse->leieforhold->leieobjekt->id][] = $oppsigelse;
        }
        $kommendeInnflyttinger = $this->app->hentSamling(Leieforhold::class)
            ->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`fradato` >' => $dato->format('Y-m-d')])
            ->leggTilSortering('fradato', false, Leieforhold::hentTabell());
        /** @var Leieforhold $leieforhold */
        foreach($kommendeInnflyttinger as $leieforhold) {
            $endringer[$leieforhold->fradato->format('Y-m-d')][$leieforhold->leieobjekt->id][] = $leieforhold;
        }

        $kommendeLedigheter = new ViewArray();
        foreach ($endringer as $datostreng => $endring) {
            $endringsdato = new DateTimeImmutable($datostreng);
            foreach($endring as $leieobjektId => $endringsobjekter) {
        /** @var Leieobjekt $leieobjekt */
                $leieobjekt = $this->app->hentModell(Leieobjekt::class, $leieobjektId);
                $ledighet = $leieobjekt->hentLedighet($endringsdato);
                $endringstekst = [];
                foreach($endringsobjekter as $endringsobjekt) {
                    if($endringsobjekt instanceof Leieforhold\Oppsigelse) {
                        $endringstekst[] = "{$endringsobjekt->leieforhold->navn} flytter ut.";
                    }
                    elseif ($endringsobjekt instanceof Leieforhold) {
                        $endringstekst[] = "{$endringsobjekt->navn} flytter inn.";
                    }
                }
                if($ledighet->asDecimal() == 1) {
                    $endringstekst[] = ucfirst($leieobjekt->hentType()) . " {$leieobjekt->hentId()} blir da ledig.";
                }
                else if($ledighet->asDecimal() != 0) {
                    $endringstekst[] = $ledighet->asMixedNumber() . ' av ' . ($leieobjekt->boenhet ? 'boligen' : 'lokalet') . ' blir da ledig.';
                }
                /** @var Leieforhold $leieforhold */
                foreach($leieobjekt->hentLeieforhold($endringsdato, $endringsdato) as $leieforhold) {
                    $leieforholdAndel = $leieforhold->hentAndel();
                    if($leieforholdAndel->asDecimal() != 1) {
                        $endringstekst[] = $leieforholdAndel->asMixedNumber() . " leies av {$leieforhold->hentNavn()}";
                    }
                }

                $kommendeLedigheter->addItem(
                    $this->app->vis(KommendeLedighetLinje::class, [
                        'dato' => $endringsdato->format('d.m.Y'),
                        'leieobjektnr' => $leieobjekt->hentType() . ' ' . $leieobjekt->hentId(),
                        'leieobjektbeskrivelse' => $leieobjekt->hentBeskrivelse(),
                        'endring' => implode("<br>\n", $endringstekst)
                    ])
                );
            }
        }

        return $this->app->vis(HtmlTable::class, [
            'classes' => ['table'],
            'kolonner' => [
                (object)['title' => 'Dato'],
                (object)['title' => 'Leieobjekt'],
            ],
            'tableBody' => $kommendeLedigheter,
        ]);
    }
}