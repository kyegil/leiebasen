<?php

namespace Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_kostnad_liste;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;

/**
 * Visning for Regninger på delte kostnader i drift
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $tabell
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_kostnad_liste
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/delte_kostnader_kostnad_liste/Index.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /**
         * Kolonne-objekt for DataTables
         *
         * @link https://datatables.net/reference/option/columns
         */
        $kolonner = [
            (object)[
                'data' => 'id',
                'name' => 'id',
                'title' => 'Id',
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
//                'width' => '40px'
            ],
            (object)[
                'data' => 'tjeneste_id',
                'name' => 'tjeneste_id',
                'title' => 'Tjeneste',
                'responsivePriority' => 1, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return row.tjeneste;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
            (object)[
                'data' => 'fakturanummer',
                'name' => 'fakturanummer',
                'title' => 'Fakturanummer',
                'responsivePriority' => 2, // Høyere tall = lavere prioritet
            ],
            (object)[
                'data' => 'beløp',
                'name' => 'beløp',
                'title' => 'Beløp',
                'responsivePriority' => 3, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return row.beløp_formatert;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
            (object)[
                'data' => 'fradato',
                'name' => 'fradato',
                'title' => 'Fra dato',
                'responsivePriority' => 3, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return row.fradato_formatert;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
            (object)[
                'data' => 'tildato',
                'name' => 'tildato',
                'title' => 'Til dato',
                'responsivePriority' => 3, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return row.tildato_formatert;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
        ];

//        $data = [];
//        /** @var Kostnadsett $regningsett */
//        $regningsett = $this->app->hentSamling(Kostnad::class)
//            ->leggTilFilter(['fordelt' => false]);
//        foreach($regningsett as $regning) {
//            $data[] = (object)[
//                'id' => $regning->hentId(),
//                'tjeneste_id' => $regning->tjeneste->hentId(),
//                'tjeneste' => $regning->tjeneste->navn,
//                'fakturanummer' => $regning->fakturanummer,
//                'beløp' => $regning->fakturabeløp,
//                'beløp_formatert' => $this->app->kr($regning->fakturabeløp),
//                'fradato' => $regning->fradato ? $regning->fradato->format('Y-m-d') : null,
//                'fradato_formatert' => $regning->fradato ? $regning->fradato->format('d.m.Y') : null,
//                'tildato' => $regning->tildato ? $regning->tildato->format('Y-m-d') : null,
//                'tildato_formatert' => $regning->tildato ? $regning->tildato->format('d.m.Y') : null,
//                'forbruk' => $regning->forbruk,
//                'forbruk_formatert' => $regning->forbruk . $regning->forbruksenhet,
//            ];
//        }

        /**
         * Konfig-objekt for DataTables
         * * pageLength initiert antall per side
         * * lengthMenu
         *
         * @link https://datatables.net/reference/option/
         */
        $dataTablesConfig = (object)[
            'order' => [[0, 'desc']],
            'columns' => $kolonner,
            'pageLength' => 25,
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'createdRow' => new JsFunction('console.log(data.status);if(data.status) {$(row).addClass(data.status);} if(data.frosset) {$(row).addClass(\'frosset\');}', ['row', 'data', 'dataIndex']),
            'search' => (object)[
                'return' => true,
            ],
            'searchDelay' => 1000,
            'serverSide' => true,
            'processing' => true,
            'ajax' => (object)[
                'url' => '/drift/index.php?oppslag=delte_kostnader_kostnad_liste&oppdrag=hent_data&data=nye_fakturaer',
            ]
        ];

        $knapper = [
            $this->hentLinkKnapp(
                '/drift/index.php?oppslag=delte_kostnader_kostnad_skjema&id=*',
                'Registrer kostnad',
                'Registrer ny regning eller kostnad for fordeling.')
        ];

        $datasett = [
            'varsler' => '',
            'tabell' => $this->app->vis(DataTable::class, [
                'tableId' => 'fordelingsregninger',
                'caption' => 'Kostnader for fordeling blant beboere',
                'kolonner' => $kolonner,
                'dataTablesConfig' => $dataTablesConfig
            ]),
            'knapper' => $knapper,
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}