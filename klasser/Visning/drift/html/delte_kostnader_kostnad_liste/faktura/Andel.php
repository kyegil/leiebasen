<?php
/**
 * Part of boligstiftelsen.svartlamon.org
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_kostnad_liste\faktura;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Class Andel
 *
 * Visning
 *
 *  Ressurser:
 *      andel Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andel
 *  Mulige variabler:
 *      $id
 *      $beløp
 *      $beskrivelse
 *      $leieforhold
 *      $leieforholdBeskrivelse
 *      $kravId
 * @package Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_kostnad_liste\faktura
 */
class Andel extends Drift
{
    protected $template = 'drift/html/delte_kostnader_kostnad_liste/faktura/Andel.html';

    protected $data = [
    ];
    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['andel', 'faktura', 'leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andel $andel */
        $andel = $this->hentRessurs('andel');
        /** @var Kostnad $faktura */
        $faktura = $this->hentRessurs('faktura');
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        if($andel) {
            $beløp = $andel->hentBeløp();
            $leieforhold = $andel->hentLeieforhold();
            $this->definerData([
                'id' => $andel->hentId(),
                'beløp' => $this->app->kr($beløp),
                'beskrivelse' => $andel->hentTekst(),
                'kravId' => strval($andel->krav)
            ]);
        }
        if($leieforhold) {
            $leieforholdBeskrivelse = $leieforhold->hentBeskrivelse();
            $leieforholdBeskrivelse = new HtmlElement('a', [
                'href' => '/drift/index.php?oppslag=leieforholdkort&id=' . $leieforhold->hentId(),
                'title' => 'Vis leieforholdet'
            ], $leieforholdBeskrivelse);
        }
        $this->definerData([
            'leieforhold' => $leieforhold ? $leieforhold->hentId() : '',
            'leieforholdBeskrivelse' => $leieforhold ? $leieforholdBeskrivelse : ''
        ]);

        return parent::forberedData();
    }
}