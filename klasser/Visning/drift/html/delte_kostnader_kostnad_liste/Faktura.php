<?php

namespace Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_kostnad_liste;


use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_kostnad_liste\faktura\Andel;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Class Faktura
 *
 * Visning
 *
 *  Ressurser:
 *      tjeneste Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste
 *      faktura Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad
 *  Mulige variabler:
 *      $id
 *      $fakturaId
 *      $fakturanummer
 *      $beløp
 *      $tjenesteId
 *      $tjenesteNavn
 *      $tjenesteBeskrivelse
 *      $fraDato
 *      $tilDato
 *      $termin
 *      $forbruk
 *      $beregnet
 *      $fordelt
 *      $strømanlegg
 *      $anleggsbeskrivelse
 *      $fordeling
 *      $fordeltBeløp
 * @package Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_kostnad_liste
 */
class Faktura extends Drift
{
    protected $template = 'drift/html/delte_kostnader_kostnad_liste/Faktura.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['tjeneste', 'faktura'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad $faktura */
        $faktura = $this->hentRessurs('faktura');
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste|null $tjeneste */
        $tjeneste = $this->hentRessurs('tjeneste');

        if($faktura && !$tjeneste) {
            $tjeneste = $faktura->tjeneste ? $faktura->tjeneste->hentTypeModell() : null;
        }

        if($faktura) {
            $fordeling = new ViewArray();
            $fordeltBeløp = 0;
            $andeler = $faktura->hentAndeler();
            foreach($andeler as $andel) {
                $fordeling->addItem($this->app->vis(Andel::class, [
                    'andel' => $andel
                ]));
                $fordeltBeløp += $andel->hentBeløp();
            }
            $this->definerData([
                'fakturaId' => $faktura->hentId(),
                'fakturanummer' => $faktura->hentFakturanummer(),
                'beløp' => $this->app->kr($faktura->hentFakturabeløp()),
                'tjenesteId' => $faktura->hentTjeneste() ? $faktura->tjeneste->id : '',
                'tjenesteNavn' => $faktura->hentTjeneste() ? $faktura->tjeneste->navn : '',
                'tjenesteBeskrivelse' => $faktura->hentTjeneste() ? $faktura->tjeneste->beskrivelse : '',
                'fraDato' => $faktura->hentFradato() ? $faktura->hentFradato()->format('d.m.Y') : '',
                'tilDato' => $faktura->hentTildato() ? $faktura->hentTildato()->format('d.m.Y') : '',
                'termin' => $faktura->hentTermin(),
                'forbruk' => $faktura->hentForbruk() ? (str_replace('.', ',', $faktura->hentForbruk()) . '&nbsp;' . $faktura->hentForbruksenhet()) : '',
                'beregnet' => $faktura->hentBeregnet(),
                'fordelt' => $faktura->hentFordelt(),
                'fordeling' => $fordeling,
                'fordeltBeløp' => $this->app->kr($fordeltBeløp)
            ]);
        }

        if($tjeneste) {
            $this->definerData([
                'tjenesteId' => $tjeneste->hentId(),
                'tjenesteNavn' => $tjeneste->hentNavn(),
                'tjenesteBeskrivelse' => $tjeneste->hentBeskrivelse(),

            ]);
        }
        return parent::forberedData();
    }
}