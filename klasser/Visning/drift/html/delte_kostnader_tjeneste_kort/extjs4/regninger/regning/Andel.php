<?php
/**
 * Part of boligstiftelsen.svartlamon.org
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_tjeneste_kort\extjs4\regninger\regning;


use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andel as AndelModell;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Panel-innhold for krav
 *
 *  Ressurser:
 *      andel \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andel
 *  Mulige variabler:
 *      $andelId
 *      $kravId
 *      $beløp
 *      $andelbeskrivelse
 *      $leieforholdId
 *      $leieforholdBeskrivelse
 * @package Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_tjeneste_kort\extjs4\regninger\regning
 */
class Andel extends Drift
{
    protected $template = 'drift/html/delte_kostnader_tjeneste_kort/extjs4/regninger/regning/Andel.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['andel'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var AndelModell|null $andel */
        $andel = $this->hentRessurs('andel');

        if ($andel) {
            $this->definerData([
                'andelId' => $andel->hentId(),
                'kravId' => $andel->krav ? $andel->krav->hentId() : '',
                'beløp' => $this->app->kr($andel->beløp),
                'andelbeskrivelse' => $andel->tekst,
                'leieforholdId' => $andel->leieforhold ? $andel->leieforhold->hentId() : '',
                'leieforholdBeskrivelse' => $andel->leieforhold ? $andel->leieforhold->hentBeskrivelse() : '',
            ]);
        }

        $this->definerData([
            'andelId' => '',
            'kravId' => '',
            'beløp' => '',
            'andel' => '',
            'leieforholdId' => '',
            'leieforholdBeskrivelse' => '',
        ]);
        return parent::forberedData();
    }
}