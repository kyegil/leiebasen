<?php
/**
 * Part of boligstiftelsen.svartlamon.org
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_tjeneste_kort\extjs4\regninger;


use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad as RegningModell;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_tjeneste_kort\extjs4\regninger\regning\Andel;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Panel-innhold for krav
 *
 *  Ressurser:
 *      regning \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad
 *  Mulige variabler:
 *      $regningId
 *      $andeler
 * @package Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_tjeneste_kort\extjs4\regninger
 */
class Regning extends Drift
{
    protected $template = 'drift/html/delte_kostnader_tjeneste_kort/extjs4/regninger/Regning.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['regning'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var RegningModell|null $regning */
        $regning = $this->hentRessurs('regning');

        $andeler = new ViewArray();
        if ($regning) {
            foreach($regning->hentAndeler() as $andel) {
                $andeler->addItem($this->app->hentVisning(Andel::class, ['andel' => $andel]));
            }
        }

        $this->definerData([
            'regningId' => strval($regning),
            'andeler' => $andeler
        ]);
        return parent::forberedData();
    }
}