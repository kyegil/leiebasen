<?php

namespace Kyegil\Leiebasen\Visning\drift\html\depositum_kort;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Depositum;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Visning for depositum i drift
 *
 * Ressurser:
 * * depositum \Kyegil\Leiebasen\Modell\Leieforhold\Depositum
 *
 * Tilgjengelige variabler:
 * * $depositumId
 * * $leieforholdId
 * * $leieforholdBeskrivelse
 * * $type
 * * $dato
 * * $beløp
 * * $betaler
 * * $notat
 * * $konto
 * * $fil
 * * $filtype
 * * $utløpsdato
 * * $påminnelser_sendt
 * @package Kyegil\Leiebasen\Visning\drift\html\depositum_kort
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/depositum_kort/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['depositum'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $datasett = [
            'depositumId' => '',
            'leieforholdId' => '',
            'leieforholdBeskrivelse' => '',
            'type' => '',
            'dato' => '',
            'beløp' => '',
            'betaler' => '',
            'notat' => '',
            'konto' => '',
            'fil' => '',
            'filtype' => '',
            'utløpsdato' => '',
            'påminnelser_sendt' => '',
        ];
        /** @var Depositum|null $depositum */
        $depositum = $this->hentRessurs('depositum');

        if($depositum) {
            $datasett['depositumId'] = $depositum->hentId();
            $datasett['leieforholdId'] = $depositum->leieforhold->hentId();
            $datasett['leieforholdBeskrivelse'] = $depositum->leieforhold->hentBeskrivelse();
            $datasett['type'] = $depositum->hentType();
            $datasett['dato'] = $depositum->hentDato() ? $depositum->hentDato()->format('d.m.Y') : '';
            $datasett['beløp'] = $depositum->hentBeløp();
            $datasett['betaler'] = $depositum->hentBetaler();
            $datasett['notat'] = $depositum->hentNotat();
            
            if($depositum->hentType() == Depositum::TYPE_GARANTI) {
                $garantiFil = $depositum->hentFil();

                $utløpspåminnelser = $depositum->hentUtløpspåminnelser() ?? [];
                array_walk($utløpspåminnelser, function (\DateTimeInterface &$utløpspåminnelse) {
                    $utløpspåminnelse = $utløpspåminnelse->format('d.m.Y');
                });

                $filendelse = pathinfo($garantiFil, PATHINFO_EXTENSION);

                $datasett['fil'] = '/drift/index.php?oppslag=depositum_kort&id=' . $depositum->hentId() . '&oppdrag=hent_data&data=garanti';

                $datasett['filtype'] = $filendelse;
                $datasett['utløpsdato'] = $depositum->hentUtløpsdato() ? $depositum->hentUtløpsdato()->format('d.m.Y') : '';
                $datasett['påminnelser_sendt'] = implode(', ', $utløpspåminnelser);
            }
            else {
                $datasett['konto'] = $depositum->hentKonto();
            }
        }
        
        $this->definerData($datasett);
        return parent::forberedData();
    }
}