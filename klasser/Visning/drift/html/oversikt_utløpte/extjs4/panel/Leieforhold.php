<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\html\oversikt_utløpte\extjs4\panel;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold as LeieforholdModell;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Panel-innhold for leieobjekt
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *      leieobjekt \Kyegil\Leiebasen\Modell\Leieobjekt
 *  Mulige variabler:
 *      $leieobjektId
 *      $leieforholdBeskrivelse
 *      $fraDato
 *      $tilDato
 *      $symbol
 * @package Kyegil\Leiebasen\Visning\drift\html\oversikt_utløpte\extjs4\panel
 */
class Leieforhold extends Drift
{
    protected $template = 'drift/html/oversikt_utløpte/extjs4/panel/Leieforhold.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var LeieforholdModell|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        $idag = new \DateTime();
        $enMånedFram = date_create()->add(new \DateInterval('P1M'));

        if($leieforhold) {
            $symbol = '';
            if ($leieforhold->tildato < $idag) {
                $symbol = new HtmlElement('img', [
                    'alt' => 'Advarsel',
                    'src' => '/pub/media/bilder/drift/advarsel.png',
                    'style' => 'float: left; margin: 4px; height: 50px;'
                ]);
            }
            else if ($leieforhold->tildato < $enMånedFram) {
                $symbol = new HtmlElement('img', [
                    'alt' => 'Påminnelse',
                    'src' => '/pub/media/bilder/drift/tegnestift.png',
                    'style' => 'float: left; margin: 4px; height: 35px;'
                ]);
            }
            $this->definerData([
                'leieforholdId' => $leieforhold->hentId(),
                'leieforholdBeskrivelse' => $leieforhold->hentBeskrivelse(),
                'fraDato' => $leieforhold->fradato->format('d.m.Y'),
                'tilDato' => $leieforhold->tildato ? $leieforhold->tildato->format('d.m.Y') : '',
                'symbol' => $symbol,
            ]);
        }
        $this->definerData([
            'leieforholdId' => '',
            'leieforholdBeskrivelse' => '',
            'fraDato' => '',
            'tilDato' => '',
            'symbol' => '',
        ]);
        return parent::forberedData();
    }
}