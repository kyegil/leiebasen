<?php

namespace Kyegil\Leiebasen\Visning\drift\html\poeng_liste;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Poengprogram;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;

/**
 * Poengliste i drift
 *
 *  Ressurser:
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *      program \Kyegil\Leiebasen\Modell\Poengprogram
 *  Mulige variabler:
 *      $poengId
 *      $leieforholdBeskrivelse
 *      $programId
 *      $programKode
 *      $programNavn
 *      $programBeskrivelse
 *      $varsler
 *      $tabell
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\drift\html\poeng_liste
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/poeng_liste/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return Index
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['program', 'leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return Index
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Poengprogram $program */
        $program = $this->hentRessurs('program');

        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        /**
         * Kolonne-objekt for DataTables
         *
         * @link https://datatables.net/reference/option/columns
         */
        $kolonner = [
            (object)[
                'data' => 'id',
                'name' => 'id',
                'title' => 'Id',
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
            ],
            (object)[
                'data' => 'tid',
                'name' => 'tid',
                'title' => 'Dato',
                'responsivePriority' => 2, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return row.tid_formatert;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
                'className' => 'dato',
            ],
            (object)[
                'data' => 'type',
                'name' => 'type',
                'title' => 'Type',
                'responsivePriority' => 3, // Høyere tall = lavere prioritet
            ],
            (object)[
                'data' => 'verdi',
                'name' => 'verdi',
                'title' => 'Verdi',
                'responsivePriority' => 2, // Høyere tall = lavere prioritet
                'className' => 'dt-right',
            ],
            (object)[
                'data' => 'tekst',
                'name' => 'tekst',
                'title' => 'Hva',
                'responsivePriority' => 4, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return '<a href=\"/drift/index.php?oppslag=poeng_skjema&id=' + row.id + '\">' + data + '</a>';} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
        ];

        if(!$leieforhold) {
            array_splice($kolonner, 2, 0, [
                (object)[
                    'data' => 'leieforhold_id',
                    'name' => 'leieforhold_id',
                    'title' => 'Leieforhold',
                    'responsivePriority' => 2, // Høyere tall = lavere prioritet
                    'render' => new JsFunction(
                        "if (type == 'display') {return row.leieforhold;} return data;",
                        ['data', 'type', 'row', 'meta']
                    ),
                ],
            ] );
        }

        /**
         * Konfig-objekt for DataTables
         * * pageLength initiert antall per side
         * * lengthMenu
         *
         * @link https://datatables.net/reference/option/
         */
        $dataTablesConfig = (object)[
//            'fixedHeader' => true,
            'fixedHeader' => (object)[
                'footer' => true,
            ],
            'order' => [[0, 'desc']],
            'columns' => $kolonner,
            'pageLength' => 25,
            'pagingType'=> 'simple_numbers',
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'scrollCollapse' => false, // Collapse when no content
            'scrollY' => 'calc(100vh - 310px)', // Table height
            'search' => (object)[
                'return' => true,
            ],
            'searchDelay' => 1000,
            'serverSide' => true,
            'processing' => true,
            'ajax' => (object)[
                'url' => "/drift/index.php?oppslag=poeng_liste&oppdrag=hent_data&data=poeng&kode={$program->hentKode()}" . ($leieforhold ? "&id={$leieforhold->hentId()}" : ''),
            ]
        ];

        $knapper = [
            $this->hentLinkKnapp(
                "/drift/index.php?oppslag=poeng_skjema&kode={$program->hentKode()}&id=*" . ($leieforhold ? "&leieforhold={$leieforhold->hentId()}" : ''),
                'Registrer nytt',
                'Registrer nytt')
        ];

        $datasett = [
            'leieforholdId' => $leieforhold ? $leieforhold->hentId() : '',
            'leieforholdBeskrivelse' => $leieforhold ? $leieforhold->hentBeskrivelse() : '',
            'programId' => $program->hentId(),
            'programKode' => $program->hentKode(),
            'programNavn' => $program->hentNavn(),
            'programBeskrivelse' => $program->hentBeskrivelse(),
            'varsler' => '',
            'tabell' => $this->app->vis(DataTable::class, [
                'tableId' => 'mal',
                'caption' => $leieforhold ? DriftKontroller::lenkeTilLeieforholdKort($leieforhold, $leieforhold->hentBeskrivelse()) : '',
                'kolonner' => $kolonner,
                'dataTablesConfig' => $dataTablesConfig
            ]),
            'knapper' => $knapper,
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}