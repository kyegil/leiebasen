<?php

namespace Kyegil\Leiebasen\Visning\drift\html\leieobjekt_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Egenskapsett;
use Kyegil\Leiebasen\Modell\Eav\Verdi;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Sett;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\auto_complete\Områder;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FileField;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\Leiebasen\Visning\felles\html\form\TextArea;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;
use stdClass;

/**
 * Visning for leieobjekt-skjema i drift
 *
 *  Ressurser:
 *      leieobjekt \Kyegil\Leiebasen\Modell\Leieobjekt
 *  Mulige variabler:
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\drift\html\leieobjekt_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/leieobjekt_skjema/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Index
    {
        if(in_array($attributt,['leieobjekt'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): Index
    {
        /** @var Leieobjekt|null $leieobjekt */
        $leieobjekt = $this->hentRessurs('leieobjekt');

        $egendefinerteFelter = $this->hentEgendefinerteFelter($leieobjekt);

        /** @var Field $leieobjektIdFelt */
        $leieobjektIdFelt = $this->app->vis(Field::class, [
            'name' => 'id',
            'type' => 'hidden',
            'required' => true,
            'value' => $leieobjekt ? $leieobjekt->hentId() : '*'
        ]);

        $aktivertFelt = $this->app->vis(Checkbox::class, [
            'name' => 'aktivert',
            'label' => 'Leieobjektet kan leies ut',
            'checked' => !$leieobjekt || !$leieobjekt->hentIkkeForUtleie(),
        ]);

        $navnFelt = $this->app->vis(Field::class, [
            'name' => 'navn',
            'required' => false,
            'label' => 'Navn på leieobjekt',
            'value' => $leieobjekt ? $leieobjekt->navn : null,
        ]);

        $gateadresseFelt = $this->app->vis(Field::class, [
            'name' => 'gateadresse',
            'label' => 'Gateadresse',
            'value' => $leieobjekt ? $leieobjekt->gateadresse : null,
        ]);

        $bygningFelt = $this->app->vis(Select::class, [
            'name' => 'bygning',
            'label' => 'Bygning',
            'options' => $this->hentBygninger(),
            'value' => $leieobjekt ? strval($leieobjekt->bygning) : null,
        ]);

        $områderFelt = $this->app->vis(Områder::class, [
            'value' => $leieobjekt ? $this->hentOmråder($leieobjekt) : null,
        ]);

        $etasjeFelt = $this->app->vis(Select::class, [
            'name' => 'etasje',
            'label' => 'Etasje',
            'options' => $this->hentEtasjevalg(),
            'value' => $leieobjekt ? $leieobjekt->hentEtg() : null,
        ]);

        $beskrivelseFelt = $this->app->vis(Field::class, [
            'name' => 'beskrivelse',
            'label' => 'Evt. annen beskrivelse',
            'value' => $leieobjekt ? $leieobjekt->hent('beskrivelse') : null,
        ]);

        $postnummerFelt = $this->app->vis(Field::class, [
            'name' => 'postnr',
            'label' => 'Postnummer',
            'value' => $leieobjekt ? $leieobjekt->hentPostnr() : null,
        ]);

        $poststedFelt = $this->app->vis(Field::class, [
            'name' => 'poststed',
            'label' => 'Poststed',
            'value' => $leieobjekt ? $leieobjekt->hentPoststed() : null,
        ]);

        $boenhetFelt = $this->app->vis(Select::class, [
            'name' => 'boenhet',
            'label' => 'Leies ut som',
            'options' => (object)[0 => 'Nærings- eller annet lokale', 1 => 'Bolig'],
            'value' => $leieobjekt ? ($leieobjekt->hentBoenhet() ? '1' : '0') : '1',
        ]);

        $arealFelt = $this->app->vis(Field::class, [
            'name' => 'areal',
            'pattern' => '[0-9]*',
            'label' => 'Areal i ㎡',
            'value' => $leieobjekt && $leieobjekt->hentAreal() ? $leieobjekt->hentAreal() : null,
        ]);

        $antallRomFelt = $this->app->vis(Field::class, [
            'name' => 'ant_rom',
            'pattern' => '[0-9]*',
            'label' => 'Antall rom',
            'value' => $leieobjekt && $leieobjekt->hentAntRom() ? $leieobjekt->hentAntRom() : null,
        ]);

        $badFelt = $this->app->vis(Checkbox::class, [
            'name' => 'bad',
            'label' => 'Tilgang på bad eller dusj',
            'checked' => $leieobjekt && $leieobjekt->hentBad(),
        ]);

        $toalettKategoriFelt = $this->app->vis(Select::class, [
            'name' => 'toalett_kategori',
            'label' => 'Toalettforhold',
            'required' => true,
            'options' => (object)[
                0 => 'Leieobjektet har ikke tilgang til eget toalett, eller har utedo',
                1 => 'Det er tilgang til felles toalett i samme bygning/oppgang',
                2 => 'Leieobjektet har eget toalett',
            ],
            'value' => $leieobjekt ? strval($leieobjekt->hentToalettKategori()) : null,
        ]);

        $toalettFelt = $this->app->vis(Field::class, [
            'name' => 'toalett',
            'label' => 'Spesifisering av toalettforhold',
            'value' => $leieobjekt ? $leieobjekt->hentToalett() : null,
        ]);

        $leieberegningFelt = $this->app->vis(Select::class, [
            'name' => 'leieberegning',
            'label' => 'Leieberegning',
            'required' => true,
            'options' => $this->hentLeieberegningsmetoder(),
            'value' => $leieobjekt ? strval($leieobjekt->leieberegning) : null,
        ]);

        $merknaderFelt = $this->app->vis(TextArea::class, [
            'name' => 'merknader',
            'label' => 'Merknader',
            'value' => $leieobjekt ? $leieobjekt->hentMerknader() : null,
        ]);

        $bildeFelt = $this->app->vis(FileField::class, [
            'name' => 'bildefelt',
            'label' => 'Last opp bilde',
        ]);

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [];
        $buttons[] = $this->app->visTilbakeknapp();
        $buttons[] = new HtmlElement('button', ['type' => 'submit'], 'Lagre');

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/drift/index.php?oppslag=leieobjekt_skjema&oppdrag=ta_i_mot_skjema&skjema=leieobjekt_skjema&id=' . ($leieobjekt ? $leieobjekt->hentId() : '*'),
                'formId' => 'leieobjekt_skjema',
                'buttonText' => 'Lagre',
                'fields' => [
                    $leieobjektIdFelt,
                    $aktivertFelt,
                    $navnFelt,
                    $gateadresseFelt,
                    $bygningFelt,
                    $områderFelt,
                    $etasjeFelt,
                    $beskrivelseFelt,
                    $postnummerFelt,
                    $poststedFelt,
                    $boenhetFelt,
                    $arealFelt,
                    $antallRomFelt,
                    $badFelt,
                    $toalettKategoriFelt,
                    $toalettFelt,
                    $leieberegningFelt,
                    $egendefinerteFelter,
                    $merknaderFelt,
                    $bildeFelt,
                ],
                'buttons' => $buttons
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    protected function hentBygninger(): stdClass
    {
        $resultat = (object)['' => ''];
        /** @var Bygning $bygning */
        foreach ($this->app->hentSamling(Bygning::class) as $bygning) {
            $resultat->{$bygning->hentId()} = "{$bygning->hentKode()}: {$bygning->hentNavn()}";
        }

        return $resultat;
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    protected function hentOmråder(?Leieobjekt $leieobjekt = null): array
    {
        $resultat = [];

        if ($leieobjekt) {
            /** @var Område[] $områder */
            $områder = $leieobjekt->hentOmråder();
        }
        else {
            /** @var Sett $områder */
            $områder = $this->app->hentSamling(Område::class);
        }
        /** @var Område $område */
        foreach ($områder as $område) {
            $resultat[] = $område->hentId();
        }

        return $resultat;
    }

    /**
     * @return stdClass|array
     * @throws Exception
     */
    protected function hentEtasjevalg(): array
    {
        return ['', 'Loft', 'Kjeller', 'Sokkel', '1. etg.', '2. etg.', '3. etg.', '4. etg.', '5. etg.'];
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    protected function hentLeieberegningsmetoder(): stdClass
    {
        $resultat = new stdClass();
        /** @var Leieberegning $leieberegning */
        foreach ($this->app->hentSamling(Leieberegning::class) as $leieberegning) {
            $resultat->{$leieberegning->hentId()} = "{$leieberegning->hentNavn()}";
        }

        return $resultat;
    }

    /**
     * @param Leieobjekt $leieobjekt
     * @return ViewArray
     * @throws \Throwable
     */
    private function hentEgendefinerteFelter(?Leieobjekt $leieobjekt): ViewArray
    {
        $resultat = new ViewArray();
        /** @var Egenskapsett $eavEgenskaper */
        $eavEgenskaper = $this->app->hentSamling(Egenskap::class);
        $eavEgenskaper->leggTilEavVerdiJoin('felt_type', true);
        $eavEgenskaper->leggTilSortering('rekkefølge');
        $eavEgenskaper->leggTilFilter(['modell' => Leieobjekt::class])->låsFiltre();

        foreach($eavEgenskaper as $egenskap) {
            /** @var Verdi|null $verdiObjekt */
            $verdiObjekt = $leieobjekt
                ? $egenskap->hentVerdier()
                ->leggTilFilter(['objekt_id' => $leieobjekt->hentId()])
                ->låsFiltre()->hentFørste()
            : null;
            $resultat->addItem($egenskap->visVerdi($verdiObjekt, 'felt_type'));
        }
        return $resultat;
    }
}