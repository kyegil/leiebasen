<?php

namespace Kyegil\Leiebasen\Visning\drift\html;


use Exception;
use Kyegil\Leiebasen\EavVerdiVisningRenderer;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Egenskapsett;
use Kyegil\Leiebasen\Modell\Eav\Verdi;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Personsett;
use Kyegil\Leiebasen\Visning\drift\html\shared\body\main\Content;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

/** @inheritdoc  */
class LeieforholdPersonerSkjema extends Content
{
    /**
     * @inheritdoc
     */
    public function sett($attributt, $verdi = null): LeieforholdPersonerSkjema
    {
        if(in_array($attributt,['leieforhold', 'kontraktpersoner'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): LeieforholdPersonerSkjema
    {
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        /** @var Personsett|null $kontraktpersoner */
        $kontraktpersoner = $this->hentRessurs('kontraktpersoner')
            ?? $this->hentKontraktPersonsett($leieforhold);

        /** @var Personsett|null $kontraktpersoner */
        $husholdningsmedlemmer = $this->hentRessurs('husholdningsmedlemmer')
            ?? $leieforhold->hentAndreBeboere();


        $kontraktpersonerFelter = new ViewArray();
        foreach ($kontraktpersoner as $person) {
            $kontraktpersonerFelter->addItem($this->hentPersonFelter($person));
        }

        $husholdningsmedlemmerFelter = new ViewArray();
        foreach ($husholdningsmedlemmer ?? [] as $person) {
            $husholdningsmedlemmerFelter->addItem($this->hentPersonFelter($person));
        }

        $leieobjektAdresseFelter = $this->hentLeieobjektAdresseFelter($leieforhold->hentLeieobjekt());

        /** @var FieldSet $kontraktpersonerFeltsett */
        $kontraktpersonerFeltsett = $this->app->vis(FieldSet::class, [
            'label' => 'Leietakere',
            'class' => 'row',
            'contents' => $kontraktpersonerFelter,
        ]);

        /** @var FieldSet|null $husholdningsmedlemmerFeltsett */
        $husholdningsmedlemmerFeltsett = null;
        if($husholdningsmedlemmer) {
            $husholdningsmedlemmerFeltsett = $this->app->vis(FieldSet::class, [
                'label' => 'Øvrige husstand-medlemmer',
                'class' => 'row',
                'contents' => $husholdningsmedlemmerFelter,
            ]);
        }

        $autofyllAdresserKnapp = new HtmlElement('div', [
            'role' => 'button',
            'class' => 'btn btn-secondary autofyll-adresser',
        ],
            'Oppdatér samtlige adresser til leieobjektets adresse'
        );

        $felter = new HtmlElement('div', ['class' => 'container'], [
            $autofyllAdresserKnapp,
            $leieobjektAdresseFelter,
            $kontraktpersonerFeltsett,
            $husholdningsmedlemmerFeltsett
        ]);

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [];
        $buttons[] = new HtmlElement('a', [
            'role' => 'button',
            'href' => $this->app->returi->get()->url,
            'class' => 'btn btn-secondary fortsett',
        ], 'Fortsett uten å lagre'
        );
        $buttons[] = new HtmlElement('button', ['role'=> 'button', 'type' => 'submit', 'form'=> 'adressekort'], 'Lagre kontaktopplysningene');

        $datasett = [
//            'heading' => 'Page Title',
            'mainBodyContents' => $this->app->vis(AjaxForm::class, [
                'useModal' => true,
                'action' => $this->app::url('leieforhold_personer_skjema', $leieforhold, 'mottak', ['skjema' => 'adressekort']),
                'formId' => 'adressekort',
                'fields' => $felter,
                'buttons' => ''
            ]),
            'buttons' => $buttons,
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }

    /**
     * @param Leieforhold $leieforhold
     * @return Personsett
     * @throws Exception
     */
    private function hentKontraktPersonsett(Leieforhold $leieforhold): Personsett
    {
        /** @var Person[] $personer */
        $personer = [];
        /** @var Personsett $personsett */
        $personsett = $this->app->hentSamling(Person::class);
        foreach ($leieforhold->hentLeietakere() as $leietaker) {
            $personer[] = $leietaker->person;
        }
        return $personsett->lastFra($personer);
    }

    /**
     * @param Person $person
     * @return ViewInterface
     * @throws Exception
     */
    private function hentPersonFelter(Person $person): ViewInterface
    {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $personFelter = new ViewArray();

        $personFelter->addItem(
            new HtmlElement('div', [], $person->hentId())
        );

        $personFelter->addItem($this->app->vis(Field::class, [
            'name' => "personer[$person][id]",
            'type' => 'hidden',
            'value' => $person->hentId(),
        ]));

        $personFelter->addItem($this->app->vis(Checkbox::class, [
            'name' => "personer[$person][er_org]",
            'label' => 'Firma/organisasjon',
            'checked' => $person->erOrg,
        ]));

        $personFelter->addItem($this->app->vis(Field::class, [
            'name' => "personer[$person][fornavn]",
            'label' => 'Fornavn',
            'value' => $person->fornavn,
            'required' => true,
        ]));

        $personFelter->addItem($this->app->vis(Field::class, [
            'name' => "personer[$person][etternavn]",
            'label' => 'Etternavn',
            'value' => $person->etternavn,
            'required' => true,
        ]));

        $personFelter->addItem($this->app->vis(DateField::class, [
            'name' => "personer[$person][fødselsdato]",
            'label' => 'Fødselsdato',
            'value' => $person->fødselsdato ? $person->fødselsdato->format('Y-m-d') : null,
        ]));

        $personFelter->addItem($this->app->vis(Field::class, [
            'name' => "personer[$person][personnr]",
            'label' => 'Personnummer (5 eller 11 sifre)',
            'pattern' => '[0-9]{5}',
            'value' => $person->personnr,
        ]));

        $adresse1 = $this->app->vis(Field::class, [
            'name' => "personer[$person][adresse1]",
            'label' => 'Gateadresse',
            'value' => $person->adresse1,
        ]);

        $adresse2 = $this->app->vis(Field::class, [
            'name' => "personer[$person][adresse2]",
            'value' => $person->adresse2,
        ]);

        $postnr = $this->app->vis(Field::class, [
            'name' => "personer[$person][postnr]",
            'label' => 'Postnummer',
            'value' => $person->postnr,
        ]);

        $poststed = $this->app->vis(Field::class, [
            'name' => "personer[$person][poststed]",
            'label' => 'Poststed',
            'value' => $person->poststed,
        ]);

        $land = $this->app->vis(Field::class, [
            'name' => "personer[$person][land]",
            'label' => 'Land',
            'value' => $person->land ?: 'Norge',
        ]);

        $autofyllAdresseKnapp = new HtmlElement('div', [
            'role' => 'button',
            'class' => 'btn btn-secondary autofyll-adresse',
        ],
            'Bruk leieobjektets adresse'
        );

        $personFelter->addItem($this->app->vis(FieldSet::class, [
            'label' => 'Postadresse',
            'contents' => [
                $adresse1, $adresse2, $postnr, $poststed, $land, $autofyllAdresseKnapp,
            ],
        ]));

        $personFelter->addItem($this->app->vis(Field::class, [
            'name' => "personer[$person][epost]",
            'type' => 'email',
            'label' => 'E-post',
            'value' => $person->epost,
        ]));

        $personFelter->addItem($this->app->vis(Field::class, [
            'name' => "personer[$person][mobil]",
            'type' => 'tel',
            'pattern' => '\+?[0-9]+',
            'label' => 'Mobil/SMS telefonnummer',
            'value' => $person->mobil,
        ]));

        $personFelter->addItem($this->app->vis(Field::class, [
            'name' => "personer[$person][telefon]",
            'type' => 'tel',
            'label' => 'Annet telefonnummer',
            'value' => $person->telefon,
        ]));

        $personFelter = $this->forberedPersonEgendefinerteFelter($person, $personFelter);

        $resultat = new HtmlElement('div', ['class' => 'col'], $personFelter);
        return $this->app->post($this, __FUNCTION__, $resultat, $args);
    }

    /**
     * @param Leieobjekt|null $leieobjekt
     * @return ViewArray
     */
    private function hentLeieobjektAdresseFelter(?Leieobjekt $leieobjekt ): ViewArray
    {
        $leieobjektAdresseFelter = new ViewArray();

        $leieobjektAdresseFelter->addItem($this->app->vis(Field::class, [
            'class' => 'leieobjekt-adresse adresse1',
            'type' => 'hidden',
            'value' => $leieobjekt ? $leieobjekt->hentGateadresse() : null,
        ]));

        $leieobjektAdresseFelter->addItem($this->app->vis(Field::class, [
            'class' => 'leieobjekt-adresse adresse2',
            'type' => 'hidden'
        ]));

        $leieobjektAdresseFelter->addItem($this->app->vis(Field::class, [
            'class' => 'leieobjekt-adresse postnr',
            'type' => 'hidden',
            'value' => $leieobjekt ? $leieobjekt->hentPostnr() : null,
        ]));

        $leieobjektAdresseFelter->addItem($this->app->vis(Field::class, [
            'class' => 'leieobjekt-adresse poststed',
            'type' => 'hidden',
            'value' => $leieobjekt ? $leieobjekt->hentPoststed() : null,
        ]));

        $leieobjektAdresseFelter->addItem($this->app->vis(Field::class, [
            'class' => 'leieobjekt-adresse land',
            'type' => 'hidden',
            'value' => 'Norge',
        ]));

        return $leieobjektAdresseFelter;
    }

    /**
     * @param Person $person
     * @param ViewArray|null $felter
     * @return ViewArray
     * @throws Exception
     */
    private function forberedPersonEgendefinerteFelter(Person $person, ?ViewArray $felter = null): ViewArray
    {
        $felter = $felter ?? new ViewArray();

        /** @var Egenskapsett $personEgenskaper */
        $personEgenskaper = $this->app->hentSamling(Egenskap::class);
        $personEgenskaper->leggTilEavVerdiJoin('felt_type', true);
        $personEgenskaper->leggTilSortering('rekkefølge');
        $personEgenskaper->leggTilFilter(['modell' => Person::class])->låsFiltre();

        foreach($personEgenskaper as $egenskap) {
            /** @var Verdi|null $verdiObjekt */
            $verdiObjekt = $person
                ? $egenskap->hentVerdier()
                    ->leggTilFilter(['objekt_id' => $person->hentId()])
                    ->låsFiltre()->hentFørste()
                : null;

            /** @var object|null $feltType */
            $feltType = $egenskap->hentFeltType();
            /** @var string|null $visningType */
            $visningType = $feltType ? ($feltType->type ?? null) : null;
            /** @var class-string<EavVerdiVisningRenderer> $renderKlasse */
            $renderKlasse = Egenskap::$renderProsessorMapping['felt_type'][$visningType] ?? null;
            if(is_a($renderKlasse, EavVerdiVisningRenderer::class, true)) {
                $felt = $renderKlasse::vis(
                    $verdiObjekt,
                    (object)['name' => 'personer[' . $person->hentId() . '][' . $egenskap->kode . ']'],
                    $egenskap,
                    $visningType,
                    'felt_type'
                );
                $felter->addItem($felt);
            }
        }

        $leietakerEgenskaper = $this->forberedLeietakerEgendefinerteFelter($person);
        if($leietakerEgenskaper->getItems()) {
            $felter->addItem($this->app->vis(FieldSet::class, [
                'label' => 'I dette leieforholdet',
                'class' => 'leietaker-egenskaper',
                'contents' => $leietakerEgenskaper,
            ]));
        }

        return $felter;
    }

    /**
     * @param Person $person
     * @param ViewArray|null $felter
     * @return ViewArray
     * @throws Exception
     */
    private function forberedLeietakerEgendefinerteFelter(Person $person, ?ViewArray $felter = null): ViewArray
    {
        $felter = $felter ?? new ViewArray();

        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        foreach($leieforhold->hentLeietakere() as $leietaker) {
            if(strval($leietaker->person) == strval($person)) {
                /** @var Egenskapsett $leietakerEgenskaper */
                $leietakerEgenskaper = $this->app->hentSamling(Egenskap::class);
                $leietakerEgenskaper->leggTilEavVerdiJoin('felt_type', true);
                $leietakerEgenskaper->leggTilSortering('rekkefølge');
                $leietakerEgenskaper->leggTilFilter(['modell' => Leietaker::class])->låsFiltre();

                foreach($leietakerEgenskaper as $egenskap) {
                    /** @var Verdi|null $verdiObjekt */
                    $verdiObjekt = $leietaker
                        ? $egenskap->hentVerdier()
                            ->leggTilFilter(['objekt_id' => $leietaker->hentId()])
                            ->låsFiltre()->hentFørste()
                        : null;

                    /** @var object|null $feltType */
                    $feltType = $egenskap->hentFeltType();
                    /** @var string|null $visningType */
                    $visningType = $feltType ? ($feltType->type ?? null) : null;
                    /** @var class-string<EavVerdiVisningRenderer> $renderKlasse */
                    $renderKlasse = Egenskap::$renderProsessorMapping['felt_type'][$visningType] ?? null;
                    if(is_a($renderKlasse, EavVerdiVisningRenderer::class, true)) {
                        $felt = $renderKlasse::vis(
                            $verdiObjekt,
                            (object)['name' => 'leietaker-egenskaper[' . $leietaker->hentId() . '][' . $egenskap->kode . ']'],
                            $egenskap,
                            $visningType,
                            'felt_type'
                        );
                        $felter->addItem($felt);
                    }
                }
            }
        }

        return $felter;
    }
}