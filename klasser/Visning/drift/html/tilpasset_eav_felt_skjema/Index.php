<?php

namespace Kyegil\Leiebasen\Visning\drift\html\tilpasset_eav_felt_skjema;


use Exception;
use Kyegil\Leiebasen\EavVerdiVisningRenderer;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\html\form\IntegerField;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\Leiebasen\Visning\felles\html\form\TextArea;
use Kyegil\ViewRenderer\ViewInterface;
use stdClass;

/**
 * Visning for eav-felt-skjema i drift
 *
 *  Ressurser:
 *      eavEgenskap \Kyegil\Leiebasen\Modell\Eav\Egenskap
 *  Mulige variabler:
 *      $eavEgenskapId
 * @package Kyegil\Leiebasen\Visning\drift\html\tilpasset_eav_felt_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/tilpasset_eav_felt_skjema/Index.html';

    protected $modeller;

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['eavEgenskap'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Egenskap|null $eavEgenskap */
        $eavEgenskap = $this->hentRessurs('eavEgenskap');
        $erEgendefinert = !$eavEgenskap || $eavEgenskap->erEgendefinert;

        $feltType = $eavEgenskap ? $eavEgenskap->hentFeltType() : null;
        $visning = $eavEgenskap ? $eavEgenskap->hentVisning() : null;
        $feltTypeType = null;
        $feltTypeOppsett = null;
        $visningType = null;
        $visningOppsett = null;

        if($feltType && isset($feltType->type)) {
            $feltTypeType = $feltType->type ?: null;
            $feltTypeOppsett = $feltType->oppsett ?? null;
        }
        if($visning && isset($visning->type)) {
            $visningType = $visning->type ?: null;
            $visningOppsett = $visning->oppsett ?? null;
        }

        /** @var Field $eavEgenskapIdFelt */
        $eavEgenskapIdFelt = $this->app->vis(Field::class, [
            'name' => 'id',
            'type' => 'hidden',
            'required' => true,
            'value' => $eavEgenskap ? $eavEgenskap->hentId() : '*'
        ]);

        /** @var Select $modellFelt */
        $modellFelt = $this->app->vis(Select::class, [
            'name' => 'modell',
            'label' => 'Modell',
            'required' => true,
            'disabled' => (bool)$eavEgenskap,
            'options' => $this->hentModeller(),
            'value' => $eavEgenskap ? $eavEgenskap->hentModell() : ''
        ]);

        /** @var Field $kodeFelt */
        $kodeFelt = $this->app->vis(Field::class, [
            'name' => 'kode',
            'label' => 'Kode',
            'required' => true,
            'disabled' => !$erEgendefinert,
            'value' => $eavEgenskap ? $eavEgenskap->hentKode() : ''
        ]);

        /** @var TextArea $typeFelt */
        $beskrivelseFelt = $this->app->vis(TextArea::class, [
            'name' => 'beskrivelse',
            'label' => 'Beskrivelse',
            'disabled' => !$erEgendefinert,
            'value' => $eavEgenskap ? $eavEgenskap->hentBeskrivelse() : null
        ]);

        /** @var Select $typeFelt */
        $typeFelt = $this->app->vis(Select::class, [
            'name' => 'type',
            'label' => 'Datatype',
            'required' => true,
            'disabled' => !$erEgendefinert,
            'options' => $this->hentTyper(),
            'value' => $eavEgenskap ? $eavEgenskap->hentType() : 'string'
        ]);

        /** @var Field $rekkefølgeFelt */
        $rekkefølgeFelt = $this->app->vis(IntegerField::class, [
            'name' => 'rekkefølge',
            'label' => 'Sorteringsrekkefølge',
            'value' => $eavEgenskap ? $eavEgenskap->hentRekkefølge() : null
        ]);

        /** @var TextArea $typeFelt */
        $konfigureringFelt = $this->app->vis(TextArea::class, [
            'name' => 'konfigurering',
            'label' => 'Konfigurering (JSON)',
            'disabled' => !$erEgendefinert,
            'value' => $eavEgenskap && $eavEgenskap->hentKonfigurering() ? json_encode($eavEgenskap->hentKonfigurering(), JSON_PRETTY_PRINT) : null
        ]);

        /** @var Select $feltTypeTypeFelt */
        $feltTypeTypeFelt = $this->app->vis(Select::class, [
            'name' => 'felt_type[type]',
            'label' => 'Felt-type for utfylling',
            'disabled' => !$erEgendefinert,
            'options' => $this->hentFeltTypeTyper(),
            'value' => $feltTypeType
        ]);

        /** @var TextArea $feltTypeOppsettFelt */
        $feltTypeOppsettFelt = $this->app->vis(TextArea::class, [
            'name' => 'felt_type[oppsett]',
            'label' => 'Felttype-konfigurering for skjema (JSON)',
            'disabled' => !$erEgendefinert,
            'value' => $feltTypeOppsett ? json_encode($feltTypeOppsett, JSON_PRETTY_PRINT) : null
        ]);

        /** @var Select $visningTypeFelt */
        $visningTypeFelt = $this->app->vis(Select::class, [
            'name' => 'visning[type]',
            'label' => 'Visningstype',
            'disabled' => !$erEgendefinert,
            'options' => $this->hentVisningTyper(),
            'value' => $visningType
        ]);

        /** @var TextArea $visningOppsettFelt */
        $visningOppsettFelt = $this->app->vis(TextArea::class, [
            'name' => 'visning[oppsett]',
            'label' => 'Visning-konfigurering (JSON)',
            'disabled' => !$erEgendefinert,
            'value' => $visningOppsett ? json_encode($visningOppsett, JSON_PRETTY_PRINT) : null
        ]);

        $eavEgenskapFeltsett = $this->app->vis(FieldSet::class, [
            'label' => 'Eav-egenskap',
            'contents' => [
                $eavEgenskapIdFelt,
                $modellFelt,
                $kodeFelt,
                $beskrivelseFelt,
                $rekkefølgeFelt,
                $typeFelt,
                $konfigureringFelt,
                $feltTypeTypeFelt,
                $feltTypeOppsettFelt,
                $visningTypeFelt,
                $visningOppsettFelt,
            ]
        ]);

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [];
        $buttons[] = $this->app->visTilbakeknapp();
        $buttons[] = new HtmlElement('button', ['type' => 'submit'], 'Lagre');

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/drift/index.php?oppslag=tilpasset_eav_felt_skjema&oppdrag=ta_i_mot_skjema&skjema=feltskjema&id=' . ($eavEgenskap ?? '*'),
                'formId' => 'betalingsskjema',
                'buttonText' => 'Lagre',
                'fields' => [
                    $eavEgenskapFeltsett,
                ],
                'buttons' => $buttons
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    private function hentTyper(): stdClass
    {
        $resultat = (object)[
            'string' => 'streng',
            'int' => 'heltall',
            'boolean' => 'på/av',
            'date' => 'dato',
            'time' => 'klokkeslett',
            'datetime' => 'dato og klokkeslett',
            'interval' => 'tidsintervall',
            'json' => 'json-objekt',
        ];
        $resultat = $this->app->after($this, __FUNCTION__, $resultat);
        return $resultat;
    }

    /**
     * @param stdClass $modeller
     * @return $this
     * @throws Exception
     */
    public function settModeller(stdClass $modeller): Index
    {
        list('modeller' => $modeller) = $this->app->pre($this, __FUNCTION__, ['modeller' => $modeller]);
        $this->modeller = $modeller;
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    private function hentModeller(): stdClass
    {
        $this->app->pre($this, __FUNCTION__, []);
        $this->modeller = $this->modeller ?? (object)[
            Leieforhold::class => 'Leieforhold',
            Leieobjekt::class => 'Leieobjekt',
            Person::class => 'Person',
            Leieforhold\Leietaker::class => 'Leietaker (    begrenset til hvert leieforhold)',
            Person\Adgang::class => 'Adgang',
            Person\Brukerprofil::class => 'Brukerprofil',
        ];
        return $this->app->post($this, __FUNCTION__, $this->modeller);
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    private function hentFeltTypeTyper(): stdClass
    {
        $this->app->pre($this, __FUNCTION__, []);
        $resultat = (object)[
            '' => 'Kan ikke settes manuelt',
        ];
        /**
         * @var string $kode
         * @var class-string<EavVerdiVisningRenderer> $klasse
         */
        foreach(Egenskap::$renderProsessorMapping['felt_type'] as $kode => $klasse) {
            $resultat->$kode = $klasse::hentNavn();
        }
        return $this->app->post($this, __FUNCTION__, $resultat);
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    private function hentVisningTyper(): stdClass
    {
        $this->app->pre($this, __FUNCTION__, []);
        $resultat = (object)[
            '' => 'Vises ikke',
        ];
        /**
         * @var string $kode
         * @var class-string<EavVerdiVisningRenderer> $klasse
         */
        foreach(Egenskap::$renderProsessorMapping['visning'] as $kode => $klasse) {
            $resultat->$kode = $klasse::hentNavn();
        }
        return $this->app->post($this, __FUNCTION__, $resultat);
    }
}