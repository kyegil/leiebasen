<?php

namespace Kyegil\Leiebasen\Visning\drift\html;


use DateInterval;
use DateTimeImmutable;
use Exception;
use IntlDateFormatter;
use Kyegil\CoreModel\CoreModelException;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Leieobjektsett;
use Kyegil\Leiebasen\Visning\drift\html\shared\body\main\Content;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class OversiktUtleietabell extends Content
{
    /** @var int */
    const LEIEOBJEKTHØYDE = 120;
    /** @var int */
    const PIXLER_PER_DAG = 3;
    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): OversiktUtleietabell
    {
        if(in_array($attributt,['fra', 'til', 'leieobjekter'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): OversiktUtleietabell
    {
        set_time_limit(300);
        /** @var DateTimeImmutable $fra */
        $fra = $this->hentRessurs('fra') ?? new DateTimeImmutable();
        /** @var DateTimeImmutable $til */
        $til = $this->hentRessurs('til') ?? new DateTimeImmutable();
        /** @var Leieobjektsett|null $leieobjekter */
        $leieobjekter = $this->hentRessurs('leieobjekter');

        $tidligereKnapp = new HtmlElement('a', [
            'role' => 'button',
            'class' => 'btn btn-secondary',
            'href' => $this->app->url('oversikt-utleietabell', null, null, [
                'fra' => $fra->sub(new DateInterval('P6M'))->format('Y-m-01'),
                'til' => $til->sub(new DateInterval('P6M'))->format('Y-m-t'),
            ])
        ],
            '<<< 6 mnd. <<<');

        $senereKnapp = new HtmlElement('a', [
            'role' => 'button',
            'class' => 'btn btn-secondary',
            'href' => $this->app->url('oversikt-utleietabell', null, null, [
                'fra' => $fra->add(new DateInterval('P6M'))->format('Y-m-01'),
                'til' => $til->add(new DateInterval('P6M'))->format('Y-m-t'),
            ])
        ],
            '>>> 6 mnd. >>>'
        );

        /** @var string[]|ViewInterface[] $knapper */
        $knapper = [
            $tidligereKnapp,
            $senereKnapp,
        ];
        $knapper[] = $this->app->visTilbakeknapp();
        $styleOverrides = new HtmlElement('style', [], '@media only screen and (min-width: 1380px) {.container {max-width:1320px;} }}');
        $datasett = [
            'heading' => $this->app->tittel,
            'mainBodyContents' => [$styleOverrides, $this->hentUtleietabell($fra, $til, $leieobjekter)],
            'buttons' => $knapper,
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }

    /**
     * @param DateTimeImmutable $fra
     * @param DateTimeImmutable $til
     * @param Leieobjektsett|null $leieobjekter
     * @return HtmlElement
     * @throws CoreModelException
     */
    public function hentUtleietabell(
        DateTimeImmutable $fra,
        DateTimeImmutable $til,
         ?Leieobjektsett $leieobjekter = null
    ): ViewInterface
    {
        $husleieKravsett = $this->hentHusleieKravsett($fra, $til, $leieobjekter);

        $datasett = $this->trekkUtDatasett($husleieKravsett);

        $overskriftsrad = $this->lagOverskriftsrad($fra, $til);
        $bygningslinjer = new ViewArray();

        /**
         * @var int $bygningID
         * @var object[] $leieobjektBeholdere
         */
        foreach ($datasett as $bygningId => $leieobjektBeholdere) {
            $bygningslinjer->addItem($this->hentBygningLinje($leieobjektBeholdere, $bygningId));
        }


        return new HtmlElement('div',
            [
                'id' => 'utleietabell',
                'style' => 'width: ' . ($til->diff($fra)->days * self::PIXLER_PER_DAG + 183) . 'px;'
            ],
            [$overskriftsrad, $bygningslinjer]
        );
    }

    /**
     * @param DateTimeImmutable $fra
     * @param DateTimeImmutable $til
     * @param Leieobjektsett|null $leieobjekter
     * @return Kravsett
     * @throws CoreModelException
     */
    private function hentHusleieKravsett(
        DateTimeImmutable $fra,
        DateTimeImmutable $til,
        ?Leieobjektsett $leieobjekter = null
    ): Kravsett
    {
        /** @var Kravsett $husleieSett */
        $husleieSett = $this->app->hentSamling(Krav::class);
        $husleieSett->leggTilLeieforholdModell(['navn', 'beskrivelse']);
        $husleieSett->leggTilLeieobjektModell(['navn', 'etg', 'beskrivelse', 'gateadresse', 'bygning']);
        $husleieSett->leggTilLeftJoin(Bygning::hentTabell(),
            '`' . Leieobjekt::hentTabell() . '`.`bygning` = `' . Bygning::hentTabell() . '`.`' . Bygning::hentPrimærnøkkelfelt() . '`'
        );
        $husleieSett->leggTilLeftJoin('utdelingsorden', '`' . Leieforhold::hentTabell() . '`.`regningsobjekt` = `utdelingsorden`.`leieobjekt` AND `utdelingsorden`.`rute` = ' . $this->app->hentValg('utdelingsrute'));
        $husleieSett->leggTilEavVerdiJoin('annullering');

        $husleieSett->settHovedFelter(['leieforhold', 'leieobjekt', 'tekst', 'beløp', 'andel', 'termin', 'fom', 'tom', 'utestående', 'forfall']);
        $husleieSett->leggTilModell(Bygning::class, 'leieobjekter.bygninger', null, []);

        $husleieSett->leggTilFilter(['`' . Krav::hentTabell() . '`.`type`' => Krav::TYPE_HUSLEIE]);
        $husleieSett->leggTilFilter(['`' . Krav::hentTabell() . '`.`beløp` >=' => 0]);
        $husleieSett->leggTilFilter(['`' . Krav::hentTabell() . '`.`tom` >' => $fra->format('Y-m-d')]);
        $husleieSett->leggTilFilter(['`' . Krav::hentTabell() . '`.`fom` <' => $til->format('Y-m-d')]);
        $husleieSett->leggTilFilter(['`annullering`.`verdi` IS NULL']);
        if ($leieobjekter) {
            $husleieSett->leggTilFilter(['`' . Leieobjekt::hentTabell() . '`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '`' => $leieobjekter->hentIdNumre()]);
        }
        $husleieSett->låsFiltre();

        $husleieSett->leggTilSortering('fom');
        $husleieSett->leggTilSortering('leieforhold');
        $husleieSett->leggTilSortering('plassering', false, 'utdelingsorden');

        $husleieSett->inkluder('purringlinker');
        return $husleieSett;
    }

    /**
     * @param Kravsett $husleieKravsett
     * @return array
     * @throws Exception
     */
    private function trekkUtDatasett(Kravsett $husleieKravsett): array
    {
        $datasett = [];

        /** @var DateTimeImmutable $fra */
        $fra = $this->hentRessurs('fra') ?? new DateTimeImmutable();
        /** @var DateTimeImmutable $til */
        $til = $this->hentRessurs('til') ?? new DateTimeImmutable();
        $visUtleiegrad = $this->hentRessurs('visUtleiegrad') ?? true;

        /** @var Krav $husleie */
        foreach ($husleieKravsett as $husleie) {
            $rawData = $husleie->getRawData();
            $leieobjektId = $rawData->{Leieobjekt::hentTabell()}->{Leieobjekt::hentPrimærnøkkelfelt()} ?? 0;
            $bygningId = $rawData->{Leieobjekt::hentTabell()}->{'bygning'} ?? 0;

            $leieforhold = $husleie->hentLeieforhold();
            $leieforholdId = $leieforhold->hentId();

            if(!isset($datasett[$bygningId][$leieobjektId])) {
                settype($datasett[$bygningId][$leieobjektId], 'object');
                $leieobjekt = $husleie->hentLeieobjekt();
                $datasett[$bygningId][$leieobjektId]->leieobjekt = $leieobjekt;
                settype($datasett[$bygningId][$leieobjektId]->perioder, 'array');
                settype($datasett[$bygningId][$leieobjektId]->leieforhold, 'array');
                $leieforhold = $husleie->hentLeieforhold();

                if( $visUtleiegrad ) {
                    $perioder = &$datasett[$bygningId][$leieobjektId]->perioder;
                    $tidslinje = $leieobjekt->hentTidslinje($fra, $til);
                    array_pop($tidslinje);

                    /**
                     * @var int $indeks
                     * @var DateTimeImmutable $dato
                     */
                    foreach ($tidslinje as $indeks => $dato) {
                        $periode = (object)['start' => $dato, 'utleie' => $leieobjekt->hentUtleiegrad($dato)];
                        if(isset($tidslinje[$indeks+1])) {
                            $periode->slutt = $tidslinje[$indeks+1]->sub(new DateInterval('P1D'));
                        }
                        else {
                            $periode->slutt = clone $til;
                        }
                        $perioder[] = $periode;
                    }
                }
            }

            settype($datasett[$bygningId][$leieobjektId]->leieforhold[$leieforholdId], 'object');
            $datasett[$bygningId][$leieobjektId]->leieforhold[$leieforholdId]->leieforhold = $leieforhold;

            settype(
                $datasett[$bygningId][$leieobjektId]->leieforhold[$leieforholdId]->krav,
                'array'
            );
            $datasett[$bygningId][$leieobjektId]->leieforhold[$leieforholdId]->krav[] = $husleie;
        }
        return $datasett;
    }

    /**
     * @param DateTimeImmutable $fra
     * @param DateTimeImmutable $til
     * @return HtmlElement
     */
    private function lagOverskriftsrad(DateTimeImmutable $fra, DateTimeImmutable $til): ViewInterface
    {
        /** @var DateTimeImmutable[] $måneder */
        $måneder = [];
        $måned = $fra;
        while( $måned < $til ) {
            $måneder[] = clone $måned;
            $måned = $måned->add(new DateInterval('P1M'));
        }

        sort($måneder);
        $kolonner = new ViewArray();
        $start = reset($måneder);

        foreach ($måneder as $måned) {
            // $dager er antall dager perioden består av
            $dager = $måned->format('t');

            // $w er bredden i antall pixler
            //    på perioden som skal vises
            $w = $dager * self::PIXLER_PER_DAG;

            // $x er horisontal plassering av perioden
            //    oppgitt i antall pixler fra venstre
            $x = 180 + $måned->diff($start)->days * self::PIXLER_PER_DAG;

            $kolonner->addItem(
                new HtmlElement('div',
                    ['style' => 'left: ' . $x . 'px; width: ' . $w . 'px;',],
                    IntlDateFormatter::formatObject($måned, 'MMMM y', 'nb_NO')
                )
            );
        }
        return new HtmlElement('div', ['class' => 'overskriftsrad'], $kolonner);
    }

    /**
     * @param array $leieobjektBeholdere
     * @param string $bygningsId
     * @return HtmlElement
     * @throws Exception
     */
    private function hentBygningLinje(array $leieobjektBeholdere, string $bygningsId): ViewInterface
    {
        $leieobjektLinjer = new ViewArray();
        /** @var object $leieobjektBeholder */
        foreach ($leieobjektBeholdere as $leieobjektBeholder) {
            $leieobjektLinjer->addItem($this->hentLeieobjektLinje($leieobjektBeholder));
        }
        return new HtmlElement('div', ['class' => 'bygning bygning-' . $bygningsId], $leieobjektLinjer);
    }

    /**
     * @param object $leieobjektBeholder
     * @return HtmlElement
     * @throws Exception
     */
    private function hentLeieobjektLinje(object $leieobjektBeholder): ViewInterface
    {
        /** @var bool $visBetalinger */
        $visBetalinger = $this->hentRessurs('visBetalinger') ?? true;

        /** @var Leieobjekt $leieobjekt */
        $leieobjekt = $leieobjektBeholder->leieobjekt;
        $leieforholdLinjer = new ViewArray();
        $leieobjektBeskrivelseFelt = new HtmlElement('div', [
            'class' => 'beskrivelse',
            'title' => $leieobjekt->hentBeskrivelse(),
        ],
            '<div>' . $this->app::lenkeTilLeieobjektKort($leieobjekt, "#{$leieobjekt}: {$leieobjekt->hentBeskrivelse()}") . '</div>'
        );

        $utleiegradLinje = $this->hentUtleiegradLinje($leieobjektBeholder);

        $husleieLinje = null;
        if( $visBetalinger ) {
            /** @var object $leieforholdBeholder */
            foreach ($leieobjektBeholder->leieforhold as $leieforholdBeholder) {
                $leieforholdLinjer->addItem($this->hentLeieforholdLinje($leieforholdBeholder));
            }
            $husleieLinje = new HtmlElement('div',
                ['class' => 'husleie', 'style' => 'min-height: ' . self::LEIEOBJEKTHØYDE . 'px;'],
                $leieforholdLinjer
            );
        }
        return new HtmlElement('div',
            ['class' => 'leieobjekt leieobjekt-' . $leieobjekt->hentId()],
            [
                $leieobjektBeskrivelseFelt,
                $husleieLinje,
                $utleiegradLinje,
            ]
        );
    }

    /**
     * @param object $leieforholdBeholder
     * @return ViewInterface
     * @throws Exception
     */
    private function hentLeieforholdLinje(object $leieforholdBeholder): ViewInterface
    {
        /** @var DateTimeImmutable $fra */
        $fra = $this->hentRessurs('fra') ?? new DateTimeImmutable();
        /** @var Leieforhold $leieforhold */
        $leieforhold = $leieforholdBeholder->leieforhold;
        $andel = isset($leieforholdBeholder->krav[0])
            ? $leieforholdBeholder->krav[0]->hentAndel()
            : $leieforhold->hentAndel($fra);
        $linjeHøyde = $andel->multiply(self::LEIEOBJEKTHØYDE);

        $beskrivelseFelt = new HtmlElement('div', [
            'class' => 'beskrivelse',
            'title' => $leieforhold->hentBeskrivelse(),
        ],
            '<div>' . $this->app::lenkeTilLeieforholdKort($leieforhold, "{$leieforhold}: {$leieforhold->hentNavn()}") . '</div>'
        );
        $husleieFelter = new ViewArray();
        foreach ($leieforholdBeholder->krav as $husleie) {
            $husleieFelter->addItem($this->hentHusleieFelt($husleie));
        }

        return new HtmlElement('div', [
            'class' => 'leieforhold leieforhold-' . $leieforhold->hentId(),
            'style' => 'height: ' . $linjeHøyde->asDecimal() . 'px;',
            ], [
                $beskrivelseFelt,
                $husleieFelter
            ]
        );
    }

    /**
     * @throws Exception
     */
    private function hentHusleieFelt(Krav $termin): ViewInterface
    {
        /** @var DateTimeImmutable $fra */
        $fra = $this->hentRessurs('fra') ?? new DateTimeImmutable();
        /** @var DateTimeImmutable $til */
        $til = $this->hentRessurs('til') ?? new DateTimeImmutable();
        $iDag = new DateTimeImmutable();

        $fom                = $termin->hentFom();
        $tom                = $termin->hentTom();
        $beløp              = $termin->hentBeløp();
        $utestående         = $termin->hentUtestående();
        $forfall            = $termin->hentForfall();
        $antallPurringer    = $termin->hentAntallPurringer();


        $andel = $termin->hentAndel() ?? $termin->leieforhold->hentAndel($termin->fom);
        $linjeHøyde = $andel->multiply(self::LEIEOBJEKTHØYDE);
        $h = round($linjeHøyde->asDecimal());

        // $start og $slutt er første og siste dato
        //    som kommer innenfor visningsspekteret i tabellen
        $start    = max($fom, $fra);
        $slutt    = min($tom, $til);

        // $dager er antall dager leieterminen består av
        $dager    = $slutt->diff( $start )->days + 1;

        // $w er bredden i antall pixler
        //    på (delen av) leieterminen som skal vises
        $w        = $dager * self::PIXLER_PER_DAG;

        // $x er horisontal plassering av leieterminen
        //    oppgitt i antall pixler fra venstre
        $x = 90 + $start->diff($fra)->days * self::PIXLER_PER_DAG;

        $class = ['termin'];
        $stil = array(
            "height: {$h}px;",
            "left: {$x}px;",
            "width: {$w}px;",
        );

        if( $fom == $fra ) {
            $stil[] = "border-left: 1px solid gray;";
        }
        if( $h < 25 ) {
            $stil[] = "white-space: nowrap;";
        }
        if( $tom <= $til ) {
            $stil[] = "border-right: 1px solid gray;";
        }

        if( $utestående == 0 ) {
            $class[] = 'betalt';
        }
        else if( $forfall <= $iDag ) {
            if( $beløp == $utestående && $antallPurringer > 2 ) {
                $class[] = 'forfalt';
            }
            else if( $beløp == $utestående and $antallPurringer ) {
                $class[] = 'purret';
            }
            else if( $beløp == $utestående ) {
                $class[] = 'ubetalt';
            }
            else {
                $class[] = 'delvis-betalt';
            }
        }
        else if ( $beløp != $utestående ) {
            $class[] = 'delvis-betalt';
        }

        $tekst = $termin->hentTermin() . '<br>' . $this->app->kr($beløp);
        $title = "{$termin->hentTermin()}\n{$this->app->kr($beløp, false)}";
        if($utestående > 0) {
            if($utestående != $beløp) {
                $title .= "\nRest: {$this->app->kr($utestående, false)}\nAnt. purringer: {$antallPurringer}";
            }
            if($antallPurringer) {
                $title .= "\nPurret {$antallPurringer} ganger";
            }
        }

        return new HtmlElement('div', [
            'title' => $title,
            'class' => implode(' ', $class),
            'style' => implode(' ', $stil),
        ], [
            '<div>'
            . $this->app::lenkeTilKravkort($termin, $tekst)
            . '</div>',
        ]);
    }

    private function hentUtleiegradLinje(object $leieobjektBeholder): ViewInterface
    {
        /** @var bool $visUtleiegrad */
        $visUtleiegrad = $this->hentRessurs('visUtleiegrad') ?? true;
        $utleiegradFelter = new ViewArray();
        $utleiegradLinje = new ViewArray();
        if($visUtleiegrad) {
            /** @var DateTimeImmutable $fra */
            $fra = $this->hentRessurs('fra') ?? new DateTimeImmutable();
            /** @var DateTimeImmutable $til */
            $til = $this->hentRessurs('til') ?? new DateTimeImmutable();

            foreach($leieobjektBeholder->perioder as $periode) {
                // $start og $slutt er første og siste dato
                //    som kommer innenfor visningsspekteret i tabellen
                $start    = max($periode->start, $fra);
                $slutt    = min($periode->slutt, $til);
                /** @var Fraction $utleie */
                $utleie = $periode->utleie;

                // $dager er antall dager perioden består av
                $dager    = $slutt->diff( $start )->days + 1;

                // $w er bredden i antall pixler
                //    på perioden som skal vises
                $w        = $dager * self::PIXLER_PER_DAG;

                // $x er horisontal plassering av perioden
                //    oppgitt i antall pixler fra venstre
                $x = 90 + $start->diff($fra)->days * self::PIXLER_PER_DAG;

                $tidsrom = $start->format('d.m.Y') . '–' . $slutt->format('d.m.Y');
                $forklaring = "{$utleie->asFraction()} utleid {$tidsrom}";
                $class = [];
                $stil = [
                    "left: {$x}px;",
                    "width: {$w}px;",
                ];

                if( $utleie->compare('>', 1)) {
                    $class[] = 'overleid';
                }
                else if( $utleie->compare('=', 1) ) {
                    $class[] = 'utleid';
                    $forklaring = 'Utleid ' . $tidsrom;
                }
                else if( $utleie->compare('<=', 0)) {
                    $class[] = 'ikke-utleid';
                    $forklaring = 'Ikke utleid ' . $tidsrom;
                }
                else {
                    $class[] = 'delvis-utleid';
                }

                $utleiegradFelter->addItem(new HtmlElement('div', [
                        'title' => $forklaring,
                        'class' => implode(' ', $class),
                        'style' => implode(' ', $stil),
                    ],
                    $utleie->compare('<=', 0) || $utleie->compare('=', 1)
                        ? $tidsrom
                        : $forklaring
                    )
                );
            }
            $utleiegradLinje = new HtmlElement('div',
                ['class' => 'leiegrad'],
                [
                    new HtmlElement('div', [], 'Utleid:'),
                    $utleiegradFelter
                ]
            );

        }
        return $utleiegradLinje;
    }
}