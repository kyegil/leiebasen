<?php

namespace Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler;


use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Delkravtypesett;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Innstillinger for utleiemaler og leieberegning
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $panel
 * @package Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler
 */
class Utleie extends Drift
{
    /** @var string */
    protected $template = 'drift/html/innstillinger/paneler/Utleie.html';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $panel = new ViewArray();
        $innstillinger = $this->app->hentValg();

        $panel->addItem($this->app->vis(AvtalemalListe::class));
        $panel->addItem($this->app->vis(LeieberegningsmodellListe::class));
        $panel->addItem($this->app->vis(DelkravtypeListe::class));
        $panel->addItem($this->app->vis(Select::class, [
            'label' => 'Delkravtype for fellesstrøm',
            'name' => 'delkravtype_fellesstrøm',
            'value' => $innstillinger['delkravtype_fellesstrøm'],
            'options' => $this->hentDelkratypeFellesstrømOptions(),
        ]));

        $this->definerData('panel', $panel);
        return parent::forberedData();
    }

    /**
     * @return object
     * @throws \Exception
     */
    private function hentDelkratypeFellesstrømOptions(): object
    {
        $options = new \stdClass();
        /** @var Delkravtypesett $delkravtypesett */
        $delkravtypesett = $this->app->hentSamling(Delkravtype::class);
        foreach($delkravtypesett as $delkravtype) {
            $options->{$delkravtype->hentId()} = $delkravtype->hentNavn();
        }
        return $options;
    }
}