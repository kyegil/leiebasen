<?php

namespace Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegningsett;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;

/**
 * Visning for brukerprofil-passord-skjema i drift
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $tabell
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler
 */
class LeieberegningsmodellListe extends Drift
{
    /** @var string */
    protected $template = 'drift/html/innstillinger/paneler/LeieberegningsmodellListe.html';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $innstillinger = $this->app->hentValg();
        $kolonner = [
            (object)[
                'data' => 'id',
                'name' => 'id',
                'title' => '',
                'responsivePriority' => 2,
                'width' => '40px'
            ],
            (object)[
                'data' => 'navn',
                'name' => 'navn',
                'title' => 'Beregningsmodell',
                'width' => '100%',
            ],
        ];

        $data = [];
        /** @var Leieberegningsett $leieberegningsett */
        $leieberegningsett = $this->app->hentSamling(Leieberegning::class);
        foreach($leieberegningsett as $leieberegning) {
            $data[] = (object)[
                'id' => $leieberegning->hentId(),
                'navn' => (string)new HtmlElement('a', [
                    'href' => '/drift/index.php?oppslag=leieberegning_skjema&id=' . $leieberegning->hentId(),
                    'title' => 'Vis leieberegningen',
                ], $leieberegning->hentNavn()),
            ];
        }

        $dataTablesConfig = (object)[
            'searching' => false,
            'paging' => false,
            'info' => false,
            'autoWidth' => false,
            'scrollY' => '200px',
            'order' => [[0, 'asc']],
            'columns' => $kolonner,
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'data' => $data,
        ];

        $this->definerData('tabell', $this->app->vis(DataTable::class, [
            'tableId' => 'leieberegningsmodeller',
            'caption' => 'Modeller for beregning av leiebeløp',
            'kolonner' => $kolonner,
            'data' => $data,
            'dataTablesConfig' => $dataTablesConfig
        ]));
        $this->definerData('knapper', [
            new HtmlElement('a', [
                'href' => '/drift/index.php?oppslag=leieberegning_skjema&id=*',
                'title' => 'Legg til ny leieberegningsmodell'
            ], 'Ny beregningsmodell')
        ]);
        return parent::forberedData();
    }
}