<?php

namespace Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler;


use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Innstillinger for utleiemaler og leieberegning
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $panel
 * @package Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler
 */
class Purringer extends Drift
{
    /** @var string */
    protected $template = 'drift/html/innstillinger/paneler/Purringer.html';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $panel = new ViewArray();
        $innstillinger = $this->app->hentValg();

        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Betalingsfrist på purringer (i formatet "P" + antall + "D" (dager) eller "M" (måneder)',
            'name' => 'purreforfallsfrist',
            'pattern' => 'P[0-9]+[D|M]',
            'value' => $innstillinger['purreforfallsfrist'],
        ]));

        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Minimumsintervall mellom hver purring (i formatet "P" + antall + "D" (dager) eller "M" (måneder)',
            'name' => 'purreintervall',
            'pattern' => 'P[0-9]+[D|M]',
            'value' => $innstillinger['purreintervall'],
        ]));

        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Purregebyr (0 for ikke å kreve gebyr)',
            'name' => 'purregebyr',
            'pattern' => '[0-9]+',
            'value' => $innstillinger['purregebyr'],
        ]));

        $panel->addItem($this->app->vis(Checkbox::class, [
            'label' => 'Tillat gebyrbelagte betalingsvarsler sendt med epost',
            'name' => 'epost_purregebyr_aktivert',
            'checked' => $innstillinger['epost_purregebyr_aktivert'],
        ]));

        $this->definerData('panel', $panel);
        return parent::forberedData();
    }
}