<?php

namespace Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler;


use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\Leiebasen\Visning\felles\html\form\TextArea;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Innstillinger for Nets-kommunikasjon
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $panel
 * @package Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler
 */
class Nets extends Drift
{
    /** @var string */
    protected $template = 'drift/html/innstillinger/paneler/Nets.html';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $panel = new ViewArray();
        $innstillinger = $this->app->hentValg();
        $efakturaKommunikasjonsmåter = $this->app->hentNetsProsessor()->hentEfakturaKommunikasjonsmåter();

        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Kundeenhet-ID NETS',
            'name' => 'nets_kundeenhetID',
            'value' => $innstillinger['nets_kundeenhetID'],
        ]));
        $panel->addItem($this->app->vis(FieldSet::class, [
            'label' => 'OCR konteringsdata',
            'contents' => [
                $this->app->vis(Checkbox::class, [
                    'label' => 'Aktivert',
                    'name' => 'ocr',
                    'checked' => (bool)$innstillinger['ocr'],
                ]),
                $this->app->vis(Field::class, [
                    'label' => 'Avtale-ID',
                    'name' => 'nets_avtaleID_ocr',
                    'value' => $innstillinger['nets_avtaleID_ocr'],
                ]),
            ],
        ]));
        $panel->addItem($this->app->vis(FieldSet::class, [
            'label' => 'Faste betalingsoppdrag (AvtaleGiro)',
            'contents' => [
                $this->app->vis(Checkbox::class, [
                    'label' => 'Aktivert',
                    'name' => 'avtalegiro',
                    'checked' => (bool)$innstillinger['avtalegiro'],
                ]),
                $this->app->vis(Field::class, [
                    'label' => 'Avtale-ID',
                    'name' => 'nets_avtaleID_fbo',
                    'value' => $innstillinger['nets_avtaleID_fbo'],
                ]),
            ],
        ]));
        $panel->addItem($this->app->vis(FieldSet::class, [
            'label' => 'eFaktura',
            'contents' => [
                $this->app->vis(Checkbox::class, [
                    'label' => 'Aktivert',
                    'name' => 'efaktura',
                    'checked' => (bool)$innstillinger['efaktura'],
                ]),
                $this->app->vis(Select::class, [
                    'label' => 'eFaktura kommunikasjonsmåte',
                    'name' => 'efaktura_kommunikasjonsmåte',
                    'value' => $innstillinger['efaktura_kommunikasjonsmåte'],
                    'options' => $efakturaKommunikasjonsmåter,
                ]),
                $this->app->vis(Field::class, [
                    'label' => 'eFaktura referansenr.',
                    'name' => 'efaktura_referansenummer',
                    'value' => $innstillinger['efaktura_referansenummer'],
                ]),
                $this->app->vis(TextArea::class, [
                    'label' => 'Tekst på eFaktura før fakturadetaljer (maks 480 tegn)',
                    'name' => 'efaktura_tekst1',
                    'maxlength' => 480,
                    'value' => $innstillinger['efaktura_tekst1'],
                ]),
                $this->app->vis(TextArea::class, [
                    'label' => 'Tekst på eFaktura etter fakturadetaljer (maks 480 tegn)',
                    'name' => 'efaktura_tekst2',
                    'maxlength' => 480,
                    'value' => $innstillinger['efaktura_tekst2'],
                ]),
            ],
        ]));

        $this->definerData('panel', $panel);
        return parent::forberedData();
    }
}