<?php

namespace Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler;


use Exception;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\CollapsibleSection;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\html\form\HtmlEditor;
use Kyegil\Leiebasen\Visning\felles\html\form\IntegerField;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Innstillinger for betalingsplaner og betalingsutsettelser
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $panel
 * @package Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler
 */
class Betalingsplaner extends Drift
{
    /** @var string */
    protected $template = 'drift/html/innstillinger/paneler/Betalingsplaner.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $panel = new ViewArray();
        $innstillinger = $this->app->hentValg();

        $panel->addItem($this->app->vis(FieldSet::class, [
            'label' => 'Betalingsplaner',
            'contents' => [
                $this->app->vis(HtmlEditor::class, [
                    'label' => 'Mal for påminnelse om forestående avdrag i betalingsplan',
                    'name' => 'betalingsplan_påminnelse_mal',
                    'value' => $innstillinger['betalingsplan_påminnelse_mal'],
                ]),
                $this->app->vis(HtmlEditor::class, [
                    'label' => 'Mal for etterlysnings av manglende avdrag i betalingsplan',
                    'name' => 'betalingsplan_etterlysning_mal',
                    'value' => $innstillinger['betalingsplan_etterlysning_mal'],
                ]),
                $this->app->vis(IntegerField::class, [
                    'label' => 'Antall dager etter manglende avdrag betalingen skal etterlyses',
                    'name' => 'betalingsplan_etterlysning_ant_dager',
                    'value' => $innstillinger['betalingsplan_etterlysning_ant_dager'],
                ]),
                $this->app->vis(HtmlEditor::class, [
                    'label' => 'Mal for varsel til utleier om brutt betalingsplan',
                    'name' => 'betalingsplan_bruddvarsel_mal',
                    'value' => $innstillinger['betalingsplan_bruddvarsel_mal'],
                ]),
                $this->app->vis(IntegerField::class, [
                    'label' => 'Antall dager etter manglende avdrag betalingsplanen skal betraktes som brutt og deaktiveres',
                    'name' => 'betalingsplan_bruddvarsel_ant_dager',
                    'value' => $innstillinger['betalingsplan_bruddvarsel_ant_dager'],
                ]),
            ],
        ]));

        $panel->addItem($this->app->vis(CollapsibleSection::class, [
            'label' => 'Aktivér selvstyrt betalingsutsettelse',
            'name' => 'betalingsutsettelse_aktivert',
            'collapsed' => !$innstillinger['betalingsutsettelse_aktivert'],
            'contents' => [
                $this->app->vis(IntegerField::class, [
                    'label' => 'Antall dager utsettelse som kan fordeles på ubetalte regninger i et leieforhold uten utestående regninger (blank = ubegrenset)',
                    'name' => 'betalingsutsettelse_spillerom_standard',
                    'value' => $innstillinger['betalingsutsettelse_spillerom_standard'],
                ]),
                $this->app->vis(IntegerField::class, [
                    'label' => 'Antall dager utsettelse som kan fordeles på ubetalte regninger i et leieforhold med nylig forfalte regninger (blank = ubegrenset)',
                    'name' => 'betalingsutsettelse_spillerom_kortsiktig_gjeld',
                    'value' => $innstillinger['betalingsutsettelse_spillerom_kortsiktig_gjeld'],
                ]),
                $this->app->vis(IntegerField::class, [
                    'label' => 'Muligheten for automatisk betalingsutsettelse opphører for leieforhold med forfalte regninger eldre enn antall dager (blank = ubegrenset)',
                    'name' => 'betalingsutsettelse_spillerom_kortsiktighet',
                    'value' => $innstillinger['betalingsutsettelse_spillerom_kortsiktighet'],
                ]),
            ],
        ]));

        $this->definerData('panel', $panel);
        return parent::forberedData();
    }
}