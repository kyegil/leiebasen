<?php

namespace Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler;


use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Personsett;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\AutoComplete;
use Kyegil\Leiebasen\Visning\felles\html\form\EmailField;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Innstillinger for utleiemaler og leieberegning
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $panel
 * @package Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler
 */
class Beboerstøtte extends Drift
{
    /** @var string */
    protected $template = 'drift/html/innstillinger/paneler/Beboerstøtte.html';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $panel = new ViewArray();
        $innstillinger = $this->app->hentValg();

        $panel->addItem($this->app->vis(EmailField::class, [
            'label' => 'Epostadresse for beboerstøtte',
            'name' => 'beboerstøtte_epost',
            'value' => $innstillinger['beboerstøtte_epost'],
        ]));
        /** @var AutoComplete $beboerstøtteSkademeldingerAdmin */
        $beboerstøtteSkademeldingerAdmin = $this->app->vis(AutoComplete::class, [
            'label' => 'Administratorer for skademeldinger',
            'name' => 'beboerstøtte_skademeldinger_admin',
            'value' => $innstillinger['beboerstøtte_skademeldinger_admin'],
            'options' => $this->hentSkademeldingerAdminOptions((int)$innstillinger['beboerstøtte_skademeldinger_admin']),
        ]);
        $panel->addItem($beboerstøtteSkademeldingerAdmin);

        $this->definerData('panel', $panel);
        return parent::forberedData();
    }

    /**
     * @param int $valgt
     * @return object
     * @throws \Exception
     */
    private function hentSkademeldingerAdminOptions(int $valgt): object
    {
        $options = new \stdClass();
        /** @var Personsett $personsett */
        $personsett = $this->app->hentSamling(Person::class)
            ->leggTilLeftJoin('adganger', '`' . Person::hentTabell() . '`.`' . Person::hentPrimærnøkkelfelt() . '` = `adganger`.`personid`')
            ->leggTilFilter([
                'or' => [
                    'adgang' => 'drift',
                    Person::hentPrimærnøkkelfelt() => $valgt
                ]
            ])
            ->leggTilSortering('fornavn')
            ->leggTilSortering('etternavn')
        ;
        foreach($personsett as $person) {
            $options->{$person->hentId()} = $person->hentNavn();
        }
        return $options;
    }
}