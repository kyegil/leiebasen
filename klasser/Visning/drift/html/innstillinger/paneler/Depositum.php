<?php

namespace Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler;


use Exception;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\HtmlEditor;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Innstillinger for depositum
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $panel
 * @package Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler
 */
class Depositum extends Drift
{
    /** @var string */
    protected $template = 'drift/html/innstillinger/paneler/Depositum.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $panel = new ViewArray();
        $innstillinger = $this->app->hentValg();

        $panel->addItem($this->app->vis(HtmlEditor::class, [
            'label' => 'Påminnelse til leietaker om depositumsgarantier som er i ferd med å utløpe',
            'description' => 'Sendes som e-post',
            'name' => 'depositum_utløpsvarsel_tekst',
            'value' => $innstillinger['depositum_utløpsvarsel_tekst'],
        ]));

        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Frist for å sende påminnelse til leietaker om depositumsgarantier som er i ferd med å utløpe<br>oppgis som "P" + antall + D (dager),M (måneder) eller Y (år)',
            'name' => 'depositum_utløpsvarsel_tid',
            'required' => true,
            'pattern' => 'P[0-9]+(D|M|Y)',
            'value' => $innstillinger['depositum_utløpsvarsel_tid'],
        ]));
        $this->definerData('panel', $panel);
        return parent::forberedData();
    }
}