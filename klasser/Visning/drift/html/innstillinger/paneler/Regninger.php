<?php

namespace Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\AutoComplete;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\CollapsibleSection;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\html\form\IntegerField;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\Leiebasen\Visning\felles\html\form\TextArea;
use Kyegil\ViewRenderer\ViewArray;
use stdClass;

/**
 * Innstillinger for regninger
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $panel
 * @package Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler
 */
class Regninger extends Drift
{
    /** @var string */
    protected $template = 'drift/html/innstillinger/paneler/Regninger.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $panel = new ViewArray();
        $innstillinger = $this->app->hentValg();

        $panel->addItem($this->app->vis(Select::class, [
            'label' => 'Sperredato for etterregistrering av krav for foregående måned',
            'name' => 'sperredato_for_etterregistrering_av_krav',
            'value' => $innstillinger['sperredato_for_etterregistrering_av_krav'],
            'options' => $this->hentDatoerOptions(),
        ]));

        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Vanlig betalingsfrist for leie og faste krav (oppgis som "P" + ant dager + "D")',
            'name' => 'betalingsfrist_faste_krav',
            'required' => true,
            'pattern' => 'P[0-9]+(D|M)',
            'value' => $innstillinger['betalingsfrist_faste_krav'],
        ]));

        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Minimum tid regninger sendes før forfall (oppgis som "P" + ant dager + "D")',
            'name' => 'min_frist_regninger',
            'required' => true,
            'pattern' => 'P[0-9]+D',
            'value' => $innstillinger['min_frist_regninger'],
        ]));

        $panel->addItem($this->app->vis(TextArea::class, [
            'label' => 'Tekst på giroer',
            'name' => 'girotekst',
            'value' => $innstillinger['girotekst'],
        ]));

        $panel->addItem($this->app->vis(Select::class, [
            'label' => 'Utdelingsrute (Utskriftsrekkefølge)',
            'name' => 'utdelingsrute',
            'value' => $innstillinger['utdelingsrute'],
            'options' => $this->hentUtdelingsruteOptions(),
        ]));

        $panel->addItem($this->app->vis(Checkbox::class, [
            'label' => 'Aktiver Epostregninger',
            'name' => 'epost_faktura_aktivert',
            'checked' => $innstillinger['epost_faktura_aktivert'],
        ]));

        $panel->addItem($this->app->vis(Checkbox::class, [
            'label' => 'Aktiver kreditnota med epost',
            'name' => 'epost_kreditnota_aktivert',
            'checked' => $innstillinger['epost_kreditnota_aktivert'],
        ]));

        $panel->addItem($this->app->vis(FieldSet::class, [
            'label' => 'Adressefelt på konvolutter',
            'contents' => [
                $this->app->vis(IntegerField::class, [
                    'label' => 'Avstand i mm til adressefelt fra konvoluttenes toppkant',
                    'name' => 'konvolutt_marg_topp',
                    'value' => $innstillinger['konvolutt_marg_topp'],
                ]),
                $this->app->vis(IntegerField::class, [
                    'label' => 'Avstand i mm til adressefelt fra konvoluttenes venstrekant',
                    'name' => 'konvolutt_marg_venstre',
                    'value' => $innstillinger['konvolutt_marg_venstre'],
                ]),
            ],
        ]));

        $panel->addItem($this->app->vis(FieldSet::class, [
            'label' => 'Send regninger automatisk',
            'contents' => [
                $this->app->vis(CollapsibleSection::class, [
                    'label' => 'Automatisk utsending av regning/faktura',
                    'name' => 'automatisk_faktura',
                    'collapsed' => !$innstillinger['automatisk_faktura'],
                    'contents' => [
                        $this->app->vis(Field::class, [
                            'label' => 'Forbered manuell utskrift på følgende datoer<br>(separeres med komma, * = alle dager, negative datoer angir antall dager før månedsskifte)',
                            'name' => 'automatisk_faktura_datoer',
                            'pattern' => '^[*0-9, -]+$',
                            'value' => $innstillinger['automatisk_faktura_datoer'],
                        ]),
                        $this->app->vis(AutoComplete::class, [
                            'label' => 'Kun på følgende ukedager',
                            'name' => 'automatisk_faktura_ukedager',
                            'multiple' => true,
                            'options' => $this->hentUkedagOptions(),
                            'forceSelection' => true,
                            'value' => explode(',', $innstillinger['automatisk_faktura_ukedager']),
                        ]),
                        $this->app->vis(Field::class, [
                            'label' => 'Inkluder krav med forfall fram til (oppgis som "P" + antall + "D" (dager) eller "M" (måneder))',
                            'name' => 'automatisk_faktura_tidsrom',
                            'pattern' => 'P[0-9]+D|P[0-9]+M([0-9]+D)?',
                            'value' => $innstillinger['automatisk_faktura_tidsrom'],
                        ]),
                        $this->app->vis(AutoComplete::class, [
                            'label' => 'Ekskluder kravtyper',
                            'multiple' => true,
                            'name' => 'automatisk_faktura_ekskluder_kravtyper',
                            'options' => $this->hentKravtypeOptions(),
                            'forceSelection' => true,
                            'value' => $innstillinger['automatisk_faktura_ekskluder_kravtyper'],
                        ]),
                        $this->app->vis(Checkbox::class, [
                            'label' => 'Separate regninger for hver hovedkravtype (tillegg følger hovedkrav)',
                            'name' => 'automatisk_faktura_adskilt',
                            'checked' => $innstillinger['automatisk_faktura_adskilt'],
                        ]),
                        $this->app->vis(Checkbox::class, [
                            'label' => 'Purr ved utskrift',
                            'name' => 'automatisk_purring',
                            'checked' => $innstillinger['automatisk_purring'],
                        ]),
                    ],
                ]),
            ],
        ]));

        $this->definerData('panel', $panel);
        return parent::forberedData();
    }

    /**
     * @return int[]
     */
    private function hentDatoerOptions(): array
    {
        return [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
    }

    /**
     * @return stdClass
     */
    private function hentUtdelingsruteOptions(): object
    {
        $this->app->pre($this, __FUNCTION__, []);
        $options = (object)[
            1 => 1
        ];
        return $this->app->post($this, __FUNCTION__, $options);
    }

    /**
     * Henter ukedag-options i hht ISO-8601 (Mandag–søndag 1–7)
     *
     * @return object
     */
    private function hentUkedagOptions(): object
    {
        return (object)[
            1 => 'Mandag',
            2 => 'Tirsdag',
            3 => 'Onsdag',
            4 => 'Torsdag',
            5 => 'Fredag',
            6 => 'Lørdag',
            7 => 'Søndag',
        ];
    }

    /**
     * @return string[]
     */
    private function hentKravtypeOptions(): array
    {
        return Krav::$typer;
    }
}