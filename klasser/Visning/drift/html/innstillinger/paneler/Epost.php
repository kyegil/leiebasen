<?php

namespace Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler;


use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\HtmlEditor;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\Leiebasen\Visning\felles\html\form\TextArea;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Innstillinger for utleiemaler og leieberegning
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $panel
 * @package Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler
 */
class Epost extends Drift
{
    /** @var string */
    protected $template = 'drift/html/innstillinger/paneler/Epost.html';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $panel = new ViewArray();
        $innstillinger = $this->app->hentValg();

        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Avsenderadresse for e-post',
            'name' => 'autoavsender',
            'value' => $innstillinger['autoavsender'],
        ]));
        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Svaradresser for masseepost',
            'name' => 'epost_svaradresser',
            'value' => $innstillinger['epost_svaradresser'],
        ]));
        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Send e-postpåminnelse om forfall x dager før (angis som "P" + ant dager + "D"',
            'pattern' => 'P[0-9]+D',
            'name' => 'forfallsvarsel_innen',
            'value' => $innstillinger['forfallsvarsel_innen'],
        ]));
        $panel->addItem($this->app->vis(Select::class, [
            'label' => 'Umiddelbart e-post betalingsvarsel',
            'name' => 'umiddelbart_betalingsvarsel_ant_dager',
            'value' => $innstillinger['umiddelbart_betalingsvarsel_ant_dager'],
            'options' => (object)['' => 'Ikke send umiddelbart betalingsvarsel', '0' => 'På forfallsdato', '1' => 'Dagen etter forfall', '2' => '2 dager etter forfall', '3' => '3 dager etter forfall', '4' => '4 dager etter forfall', '5' => '5 dager etter forfall']
        ]));
        $panel->addItem($this->app->vis(HtmlEditor::class, [
            'label' => 'Tekst for umiddelbart betalingsvarsel',
            'name' => 'umiddelbart_betalingsvarsel_tekst_epost',
            'value' => $innstillinger['umiddelbart_betalingsvarsel_tekst_epost'],
        ]));
        $panel->addItem($this->app->vis(HtmlEditor::class, [
            'label' => 'Bunntekst på alle eposter',
            'name' => 'eposttekst',
            'value' => $innstillinger['eposttekst'],
        ]));
        $panel->addItem($this->app->vis(HtmlEditor::class, [
            'label' => 'Automatisk påminnelse om at leieavtalen må fornyes',
            'name' => 'utløpsvarseltekst',
            'value' => $innstillinger['utløpsvarseltekst'],
        ]));
        $panel->addItem($this->app->vis(TextArea::class, [
            'label' => 'Tekst på meldinger om fordeling av fellesstrøm',
            'name' => 'strømfordelingstekst',
            'value' => $innstillinger['strømfordelingstekst'],
        ]));
        $panel->addItem($this->app->vis(Select::class, [
            'label' => 'E-post-transport',
            'name' => 'epost_transport',
            'value' => $innstillinger['epost_transport'] ?? 'php',
            'options' => $this->hentEpostTransportOptions(),
        ]));
        $panel->addItem($this->app->vis(Select::class, [
            'label' => 'SMS-transport',
            'name' => 'sms_transport',
            'value' => $innstillinger['sms_transport'] ?? 'php',
            'options' => $this->hentSmsTransportOptions(),
        ]));
        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Mulige SMS-avsendere (separert med komma)',
            'name' => 'sms_avsendere',
            'value' => $innstillinger['sms_avsendere'],
        ]));

        $this->definerData('panel', $panel);
        return parent::forberedData();
    }

    /**
     * @return object
     */
    private function hentEpostTransportOptions(): object
    {
        $this->app->before($this, __FUNCTION__, []);
        $options = (object)[
            Leiebase::EPOST_TRANSPORT_TEST_MAIL => 'Test mail (kun utvikler)',
            Leiebase::EPOST_TRANSPORT_PHP => 'PHP mail()',
            Leiebase::EPOST_TRANSPORT_PHPMAILER => 'PHPMailer'
        ];
        return $this->app->after($this, __FUNCTION__, $options);
    }

    /**
     * @return object
     */
    private function hentSmsTransportOptions(): object
    {
        $this->app->before($this, __FUNCTION__, []);
        $options = (object)[
            '' => 'Av',
        ];
        return $this->app->after($this, __FUNCTION__, $options);
    }
}