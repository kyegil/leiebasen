<?php

namespace Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Delkravtypesett;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;

/**
 * Visning for brukerprofil-passord-skjema i drift
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $tabell
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler
 */
class DelkravtypeListe extends Drift
{
    /** @var string */
    protected $template = 'drift/html/innstillinger/paneler/DelkravtypeListe.html';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $innstillinger = $this->app->hentValg();
        $kolonner = [
            (object)[
                'data' => 'id',
                'name' => 'id',
                'title' => '',
                'responsivePriority' => 2,
                'width' => '40px'
            ],
            (object)[
                'data' => 'navn',
                'name' => 'navn',
                'title' => 'Delkrav',
                'width' => '100%',
            ],
            (object)[
                'data' => 'aktiv',
                'name' => 'aktiv',
                'title' => 'Aktiv',
                'responsivePriority' => 2,
                'render' => new JsFunction("return type != 'display' ?  data : (data ? '✔︎' : '');", ['data', 'type', 'row', 'meta']),
                'width' => '40px',
            ],
        ];

        $data = [];
        /** @var Delkravtypesett $delkravtypesett */
        $delkravtypesett = $this->app->hentSamling(Delkravtype::class);
        foreach($delkravtypesett as $delkravtype) {
            $data[] = (object)[
                'id' => $delkravtype->hentId(),
                'navn' => (string)new HtmlElement('a', [
                    'href' => '/drift/index.php?oppslag=delkrav_skjema&id=' . $delkravtype->hentId(),
                    'title' => 'Detaljer',
                ], $delkravtype->hentNavn()),
                'aktiv' => $delkravtype->hentAktiv(),
            ];
        }

        $dataTablesConfig = (object)[
            'searching' => false,
            'paging' => false,
            'info' => false,
            'autoWidth' => false,
            'scrollY' => '200px',
            'columns' => $kolonner,
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'data' => $data,
        ];

        $this->definerData('tabell', $this->app->vis(DataTable::class, [
            'tableId' => 'delkravtyper',
            'caption' => 'Delkrav (beløp bakt inn i eller i tillegg til husleie og andre krav)',
            'kolonner' => $kolonner,
            'data' => $data,
            'dataTablesConfig' => $dataTablesConfig
        ]));
        $this->definerData('knapper', [
            new HtmlElement('a', [
                'href' => '/drift/index.php?oppslag=delkrav_skjema&id=*',
                'title' => 'Opprett ny type delkrav eller tillegg'
            ], 'Ny type tillegg/delkrav')
        ]);
        return parent::forberedData();
    }
}