<?php

namespace Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler;


use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\CollapsibleSection;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\html\form\HtmlEditor;
use Kyegil\Leiebasen\Visning\felles\html\form\NumberField;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Innstillinger for leieregulering
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $panel
 * @package Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler
 */
class Leieregulering extends Drift
{
    /** @var string */
    protected $template = 'drift/html/innstillinger/paneler/Leieregulering.html';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $panel = new ViewArray();
        $innstillinger = $this->app->hentValg();

        $panel->addItem(                $this->app->vis(Select::class, [
            'label' => 'Minimumsintervall mellom hver gang leien endres',
            'name' => 'leiejustering_intervall',
            'value' => $innstillinger['leiejustering_intervall'],
            'options' => (object)[
                'P12M' => '1 år',
                'P15M' => '15 måneder',
                'P18M' => '18 måneder',
                'P2Y' => '2 år',
                'P3Y' => '3 år',
                'P5Y' => '5 år',
            ],
        ])
        );
        $panel->addItem($this->app->vis(Select::class, [
            'label' => 'Minimumsfrist for varsel om prisjustering',
            'name' => 'leiejustering_varselfrist',
            'value' => $innstillinger['leiejustering_varselfrist'],
            'options' => (object)[
                'P1M' => '1 måned',
                'P42D' => '6 uker',
                'P2M' => '2 måneder',
                'P3M' => '3 måneder',
            ],
        ]));
        $panel->addItem($this->app->vis(CollapsibleSection::class, [
            'label' => 'Automatisk leieregulering',
            'name' => 'leiejustering_automatisk',
            'collapsed' => !$innstillinger['leiejustering_automatisk'],
            'contents' => [
                $this->app->vis(Checkbox::class, [
                    'label' => 'Oppdater justeringssatsen automatisk jfr gjeldende konsumprisindeks',
                    'name' => 'leiejustering_auto_kpi',
                    'checked' => (bool)$innstillinger['leiejustering_auto_kpi'],
                ]),
                $this->app->vis(NumberField::class, [
                    'label' => 'Årlig justeringssats',
                    'step' => '0.1',
                    'name' => 'leiejustering_gjeldende_indeks',
                    'value' => $innstillinger['leiejustering_gjeldende_indeks'],
                ]),
                $this->app->vis(FieldSet::class, [
                    'label' => 'Forankring til leieobjektets leieberegning',
                    'contents' => [
                        'Dersom forslaget til justert leie avviker <i>litt</i> fra leieobjektets foreslåtte leie for nye leieobjekter,<br>',
                        'kan det justeres opp (positiv forankring) eller ned (negativ forankring) for å matche nye leieforhold.<br>',
                        'Oppgi maksimalt avvik (i prosent) som kan korrigeres. ',
                        'Avvik større enn dette vil ikke korrigeres.',
                        $this->app->vis(NumberField::class, [
                            'label' => 'Positiv forankring',
                            'min' => 0,
                            'max' => 5,
                            'step' => '0.1',
                            'name' => 'leiejustering_pos_forankring',
                            'value' => (float)$innstillinger['leiejustering_pos_forankring'],
                        ]),
                        $this->app->vis(NumberField::class, [
                            'label' => 'Negativ forankring',
                            'min' => 0,
                            'max' => 5,
                            'step' => '0.1',
                            'name' => 'leiejustering_neg_forankring',
                            'value' => (float)$innstillinger['leiejustering_neg_forankring'],
                        ]),
                    ],
                ]),
                $this->app->vis(Select::class, [
                    'label' => 'Automatisk godkjenning av justeringsforslag før fristen for varsling utløper',
                    'name' => 'leiejustering_auto_godkjenning_tid_før_frist',
                    'value' => $innstillinger['leiejustering_auto_godkjenning_tid_før_frist'],
                    'options' => (object)[
                        '' => 'Ingen automatisk godkjenning',
                        'P1D' => 'Dagen før fristen utløper',
                        'P2D' => '2 dager før fristen utløper',
                        'P3D' => '3 dager før fristen utløper',
                        'P4D' => '4 dager før fristen utløper',
                        'P5D' => '5 dager før fristen utløper',
                        'P7D' => '1 uke før fristen utløper',
                        'P10D' => '10 dager før fristen utløper',
                        'P14D' => '2 uker før fristen utløper',
                        'P21D' => '3 uker før fristen utløper',
                        'P28D' => '4 uker før fristen utløper',
                        'P1M' => '1 måned før fristen utløper',
                    ],
                ]),
            ],
        ]));

        $panel->addItem($this->app->vis(Select::class, [
            'label' => 'Vis forslag til framtidig leiejustering',
            'name' => 'leiejustering_forslagsfrist',
            'value' => $innstillinger['leiejustering_forslagsfrist'],
            'options' => (object)[
                'P12D' => '2 uker fram i tid',
                'P1M' => '1 måned fram i tid',
                'P42D' => '6 uker fram i tid',
                'P2M' => '2 måneder fram i tid',
                'P3M' => '3 måneder fram i tid',
            ],
        ]));

        $panel->addItem($this->app->vis(CollapsibleSection::class, [
            'label' => 'Mal for varsel om justering av leie',
            'collapsed' => true,
            'contents' => [
                $this->app->vis(HtmlEditor::class, [
                    'label' => 'Mal for brev og e-post',
                    'name' => 'leiejustering_brevmal',
                    'value' => $innstillinger['leiejustering_brevmal'],
                ]),
            ],
        ]));

        $this->definerData('panel', $panel);
        return parent::forberedData();
    }
}