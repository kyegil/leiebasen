<?php

namespace Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold\AvtaleMal;
use Kyegil\Leiebasen\Modell\Leieforhold\AvtaleMalsett;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;

/**
 * Visning for avtalemal-liste i drift
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $tabell
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler
 */
class AvtalemalListe extends Drift
{
    /** @var string */
    protected $template = 'drift/html/innstillinger/paneler/AvtalemalListe.html';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $innstillinger = $this->app->hentValg();
        $kolonner = [
            (object)[
                'data' => 'id',
                'name' => 'id',
                'title' => '',
                'responsivePriority' => 2,
                'width' => '40px'
            ],
            (object)[
                'data' => 'navn',
                'name' => 'navn',
                'title' => 'Mal',
                'width' => '100%',
            ],
            (object)[
                'data' => 'type',
                'name' => 'type',
                'title' => 'Type',
                'responsivePriority' => 2,
                'width' => '40px',
            ],
        ];

        $data = [];
        /** @var AvtaleMalsett $avtaleMalsett */
        $avtaleMalsett = $this->app->hentSamling(AvtaleMal::class);
        foreach($avtaleMalsett as $avtaleMal) {
            $data[] = (object)[
                'id' => $avtaleMal->hentId(),
                'navn' => (string)new HtmlElement('a', [
                    'href' => '/drift/index.php?oppslag=avtalemal_skjema&id=' . $avtaleMal->hentId(),
                    'title' => 'Vis malen',
                ], $avtaleMal->hentMalnavn()),
                'type' => $avtaleMal->hentType()
            ];
        }

        $dataTablesConfig = (object)[
            'searching' => false,
            'paging' => false,
            'info' => false,
            'autoWidth' => false,
            'scrollY' => '200px',
            'order' => [[0, 'asc']],
            'columns' => $kolonner,
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'data' => $data,
        ];

        $this->definerData('tabell', $this->app->vis(DataTable::class, [
            'tableId' => 'avtalemaler',
            'caption' => 'Leieavtalemaler',
            'kolonner' => $kolonner,
            'data' => $data,
            'dataTablesConfig' => $dataTablesConfig
        ]));
        $this->definerData('knapper', [
            new HtmlElement('a', [
                'href' => '/drift/index.php?oppslag=avtalemal_skjema&id=*',
                'title' => 'Registrer en ny mal'
            ], 'Lag ny mal')
        ]);
        return parent::forberedData();
    }
}