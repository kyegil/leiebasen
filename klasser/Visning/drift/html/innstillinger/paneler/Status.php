<?php

namespace Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler;


use DateTimeImmutable;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\DisplayField;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for brukerprofil-passord-skjema i drift
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $panel
 * @package Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler
 */
class Status extends Drift
{
    /** @var string */
    protected $template = 'drift/html/innstillinger/paneler/Status.html';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $panel = new ViewArray();
        $innstillinger = $this->app->hentValg();

        $varselstempelInnbetalinger = new DateTimeImmutable('@' . $innstillinger['varselstempel_innbetalinger'] ?? 0);
        $varselstempelKontraktUtløp = new DateTimeImmutable('@' . $innstillinger['varselstempel_kontraktutløp'] ?? 0);
        $varselstempelForfall = new DateTimeImmutable('@' . $innstillinger['varselstempel_forfall'] ?? 0);
        $varselstempelUmiddelbartBetalingsvarsel = new DateTimeImmutable($innstillinger['varselstempel_umiddelbart_betalingsvarsel'] ?? 'now');

        $backupSisteNedlastet = new DateTimeImmutable('@' . $innstillinger['backup_siste_nedlastet'] ?? 0);
        $backupSisteTilgjengelige = $this->hentSisteTilgjengeligeBackup();

        $panel->addItem($this->app->vis(DisplayField::class, [
            'label' => 'Betalinger kvittert inntil',
            'value' => $varselstempelInnbetalinger->format('d.m.Y H:i:s')
        ]));
        $panel->addItem($this->app->vis(DisplayField::class, [
            'label' => 'Varsler om fornying av kontrakter sendt inntil',
            'value' => $varselstempelKontraktUtløp->format('d.m.Y H:i:s')
        ]));
        $panel->addItem($this->app->vis(DisplayField::class, [
            'label' => 'Varsler om forestående forfall sendt inntil',
            'value' => $varselstempelForfall->format('d.m.Y H:i:s')
        ]));
        $panel->addItem($this->app->vis(DisplayField::class, [
            'label' => 'Umiddelbart betalingsvarsel sist sendt for forfall',
            'value' => $varselstempelUmiddelbartBetalingsvarsel->format('d.m.Y H:i:s')
        ]));
        $panel->addItem($this->app->vis(FieldSet::class, [
            'label' => 'Sikkerhetskopi av database',
            'contents' => [
                $this->app->vis(DisplayField::class, [
                    'label' => 'Siste databasekopi som har blitt lastet ned var fra',
                    'value' => $backupSisteNedlastet ? $backupSisteNedlastet->format('d.m.Y H:i:s') : null
                ]),
                $this->app->vis(DisplayField::class, [
                    'label' => 'Du kan nå laste ned backup ifra ',
                    'value' => $backupSisteTilgjengelige ? $backupSisteTilgjengelige->format('d.m.Y H:i:s') : null
                ]),
                new HtmlElement('a', ['class' => 'button', 'target' => '_blank', 'href' => '/drift/index.php?oppslag=eksport&oppdrag=hentdata&data=backup'], 'Last ned tilgjengelig sikkerhetskopi')
            ]
        ]));


        $this->definerData('panel', $panel);
        return parent::forberedData();
    }

    private function hentSisteTilgjengeligeBackup(): ?DateTimeImmutable
    {
        $backupSisteTilgjengelige = null;
        try {
            if (file_exists(\Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['db_backup'])) {
                $backupSisteTilgjengelige = new DateTimeImmutable('@' . filectime(\Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['db_backup']));
            }
        } catch (\Exception $e) {
        }
        return $backupSisteTilgjengelige;
    }
}