<?php

namespace Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler;


use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for brukerprofil-passord-skjema i drift
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $panel
 * @package Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler
 */
class Organisasjon extends Drift
{
    /** @var string */
    protected $template = 'drift/html/innstillinger/paneler/Organisasjon.html';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $panel = new ViewArray();
        $innstillinger = $this->app->hentValg();

        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Utleier',
            'name' => 'utleier',
            'value' => $innstillinger['utleier'],
            'autocomplete' => 'off',
        ]));
        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Organisasjonsnummer',
            'name' => 'orgnr',
            'value' => $innstillinger['orgnr'],
            'autocomplete' => 'off',
        ]));
        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Adresse',
            'name' => 'adresse',
            'value' => $innstillinger['adresse'],
            'autocomplete' => 'off',
        ]));
        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Postnr/sted',
            'name' => 'postnr',
            'value' => $innstillinger['postnr'],
            'autocomplete' => 'off',
        ]));
        $panel->addItem($this->app->vis(Field::class, [
            'name' => 'poststed',
            'value' => $innstillinger['poststed'],
            'autocomplete' => 'off',
        ]));
        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Telefon',
            'name' => 'telefon',
            'type' => 'tel',
            'value' => $innstillinger['telefon'],
            'autocomplete' => 'off',
        ]));
        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Mobilnummer',
            'name' => 'mobil',
            'type' => 'tel',
            'value' => $innstillinger['mobil'],
            'autocomplete' => 'off',
        ]));
        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'E-postadresse',
            'name' => 'epost',
            'type' => 'email',
            'value' => $innstillinger['epost'],
            'autocomplete' => 'off',
        ]));
        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Hjemmeside',
            'name' => 'hjemmeside',
            'type' => 'url',
            'value' => $innstillinger['hjemmeside'],
            'autocomplete' => 'off',
        ]));
        $panel->addItem($this->app->vis(Field::class, [
            'label' => 'Bankkonto for betalinger',
            'name' => 'bankkonto',
            'value' => $innstillinger['bankkonto'],
            'autocomplete' => 'off',
        ]));

        $this->definerData('panel', $panel);
        return parent::forberedData();
    }
}