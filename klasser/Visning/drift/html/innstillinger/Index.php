<?php

namespace Kyegil\Leiebasen\Visning\drift\html\innstillinger;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler\Beboerstøtte;
use Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler\Betalingsplaner;
use Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler\Depositum;
use Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler\Epost;
use Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler\Leieregulering;
use Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler\Nets;
use Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler\Organisasjon;
use Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler\Purringer;
use Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler\Regninger;
use Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler\Status;
use Kyegil\Leiebasen\Visning\drift\html\innstillinger\paneler\Utleie;
use Kyegil\Leiebasen\Visning\drift\html\shared\Html;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\CollapsibleSection;

/**
 * Visning for brukerprofil-passord-skjema i drift
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\drift\html\innstillinger
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/innstillinger/Index.html';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $this->instruerAscendant(Html::class, function($html) {
            $htmlHead = $html->getData('head');
            $scripts = $htmlHead->getData('scripts');
            if(stripos($scripts, 'select2') === false) {
                $htmlHead
                    /**
                     * Importer Select2 for autocomplete
                     * @link https://select2.org/
                     */
                    ->leggTil('links', new HtmlElement('link', [
                        'rel' => "stylesheet",
                        'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
                    ]))
                    ->leggTil('scripts', new HtmlElement('script', [
                        'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
                    ]));
            }
        });
        $paneler = $this->lagPaneler();
        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/drift/index.php?oppslag=innstillinger&oppdrag=ta_i_mot_skjema&skjema=innstillinger',
                'formId' => 'passord',
                'buttonText' => 'Lagre',
                'fields' => $paneler
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return array
     */
    private function lagPaneler(): array
    {
        $this->app->pre($this, __FUNCTION__, []);

        $status = $this->app->vis(CollapsibleSection::class, [
            'label' => 'Driftstatus',
            'contents' => $this->app->vis(Status::class)
        ]);
        $organisasjon = $this->app->vis(CollapsibleSection::class, [
            'label' => 'Organisasjon',
            'contents' => $this->app->vis(Organisasjon::class)
        ]);
        $utleie = $this->app->vis(CollapsibleSection::class, [
            'label' => 'Utleiemaler og leieberegning',
            'contents' => $this->app->vis(Utleie::class)
        ]);
        $regninger = $this->app->vis(CollapsibleSection::class, [
            'label' => 'Regninger',
            'contents' => $this->app->vis(Regninger::class)
        ]);
        $betalingsplaner = $this->app->vis(CollapsibleSection::class, [
            'label' => 'Betalingsplaner og betalingsutsettelse',
            'contents' => $this->app->vis(Betalingsplaner::class)
        ]);
        $purringer = $this->app->vis(CollapsibleSection::class, [
            'label' => 'Purringer',
            'contents' => $this->app->vis(Purringer::class)
        ]);
        $epost = $this->app->vis(CollapsibleSection::class, [
            'label' => 'E-post og SMS',
            'contents' => $this->app->vis(Epost::class)
        ]);
        $leieregulering = $this->app->vis(CollapsibleSection::class, [
            'label' => 'Leieregulering',
            'contents' => $this->app->vis(Leieregulering::class)
        ]);
        $depositum = $this->app->vis(CollapsibleSection::class, [
            'label' => 'Depositum',
            'contents' => $this->app->vis(Depositum::class)
        ]);
        $nets = $this->app->vis(CollapsibleSection::class, [
            'label' => 'Nets',
            'contents' => $this->app->vis(Nets::class)
        ]);
        $beboerstøtte = $this->app->vis(CollapsibleSection::class, [
            'label' => 'Beboerstøtte',
            'contents' => $this->app->vis(Beboerstøtte::class)
        ]);
        $paneler = [
            $status,
            $organisasjon,
            $utleie,
            $leieregulering,
            $regninger,
            $purringer,
            $betalingsplaner,
            $epost,
            $depositum,
            $nets,
            $beboerstøtte,
        ];
        return $this->app->post($this, __FUNCTION__, $paneler);
    }
}