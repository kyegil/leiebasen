<?php

namespace Kyegil\Leiebasen\Visning\drift\html;


use Exception;
use Kyegil\Leiebasen\Visning\drift\html\shared\body\main\Content;
use Kyegil\Leiebasen\Visning\felles\html\shared\body\main\Kort;
use Kyegil\ViewRenderer\ViewInterface;

class _MalKort extends Content
{
    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): _MalKort
    {
        if(in_array($attributt,[])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): _MalKort
    {
        /** @var string[]|ViewInterface[] $knapper */
        $knapper = [];
        $knapper[] = $this->app->visTilbakeknapp();
        $datasett = [
            'heading' => $this->app->tittel,
            'mainBodyContents' => $this->app->vis(Kort::class, [
                'innhold' => ''
            ]),
            'buttons' => $knapper,
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }
}