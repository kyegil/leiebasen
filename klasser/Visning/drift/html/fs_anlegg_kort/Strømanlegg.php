<?php
/**
 * Part of boligstiftelsen.svartlamon.org
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\html\fs_anlegg_kort;


use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg as StrømanleggModell;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Panel-innhold for krav
 *
 *  Ressurser:
 *      $strømanlegg \Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg
 *  Mulige variabler:
 *      $tjenesteId
 *      $navn
 *      $formål
 *      $leverandør
 *      $anleggsnummer
 *      $målernummer
 *      $plassering
 * @package Kyegil\Leiebasen\Visning\drift\html\fs_anlegg_kort\extjs4\panel
 */
class Strømanlegg extends Drift
{
    protected $template = 'drift/html/fs_anlegg_kort/Strømanlegg.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['strømanlegg'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var StrømanleggModell $strømanlegg */
        $strømanlegg = $this->hentRessurs('strømanlegg');

        $this->definerData([
            'tjenesteId' => strval($strømanlegg),
            'navn' => $strømanlegg ? $strømanlegg->hentNavn() : '',
            'formål' => $strømanlegg ? $strømanlegg->hentFormål() : '',
            'leverandør' => $strømanlegg ? $strømanlegg->hentLeverandør() : '',
            'anleggsnummer' => $strømanlegg ? $strømanlegg->hentAnleggsnummer() : '',
            'målernummer' => $strømanlegg ? $strømanlegg->hentMålernummer() : '',
            'plassering' => $strømanlegg ? $strømanlegg->hentPlassering() : '',
        ]);
        return parent::forberedData();
    }
}