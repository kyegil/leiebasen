<?php

namespace Kyegil\Leiebasen\Visning\drift\html\leieobjekt_kort;


use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Egenskapsett;
use Kyegil\Leiebasen\Modell\Eav\Verdi;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\leieobjekt\leieberegning\Leieberegningsformel;
use Kyegil\Leiebasen\Visning\felles\html\leieobjekt\Leieforholdfelt;
use Kyegil\Leiebasen\Visning\felles\html\leieobjekt\Toalettbeskrivelse;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for leieobjekt-kort i drift
 *
 * Ressurser:
 * * leieobjekt \Kyegil\Leiebasen\Modell\Leieobjekt
 *
 * Tilgjengelige variabler:
 * * $leieobjektId
 * * $aktivert
 * * $navn
 * * $gateadresse
 * * $etasje
 * * $beskrivelse
 * * $postnummer
 * * $poststed
 * * $bilde
 * * $erBolig
 * * $areal
 * * $antallRom
 * * $bad
 * * $toalett
 * * $toalettKategori
 * * $merknader
 * * $leieberegningsId
 * * $leieberegningsNavn
 * * $leieberegningsformel
 * * $bygningsId
 * * $bygningsKode
 * * $bygningsNavn
 * * $leieforholdfelt
 * * $leietakere
 * * $områder
 * * $egendefinerteFelter
 * @package Kyegil\Leiebasen\Visning\drift\html\leieobjekt_kort
 */
class Index extends Drift
{
    protected $template = 'drift/html/leieobjekt_kort/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'leieobjekt') {
            return $this->settRessurs('leieobjekt', $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Leieobjekt $leieobjekt */
        $leieobjekt = $this->hentRessurs('leieobjekt');
        /** @var Leieobjekt\Bygning $bygning */
        $bygning = $leieobjekt->hentBygning();
        /** @var Leieobjekt\Leieberegning $leieberegning */
        $leieberegning = $leieobjekt->hentLeieberegning();
        /** @var Fraction[] $vanligsteLeieandeler */
        $vanligsteLeieandeler = $leieobjekt->hentTypiskLeieandel();
        $egendefinerteFelter = $this->hentEgendefinerteFelter($leieobjekt);

        if($bygning) {
            $this->definerData([
                'bygningsId' => $bygning->hentId(),
                'bygningsKode' => $bygning->hentKode(),
                'bygningsNavn' => $bygning->hentNavn()
            ]);
        }
        if($leieberegning) {
            $this->definerData([
                'leieberegningsId' => $leieberegning->hentId(),
                'leieberegningsNavn' => $leieberegning->hentNavn(),
                'leieberegningsFormel' => $this->app->vis(Leieberegningsformel::class, [
                    'leieberegning' => $leieberegning,
                    'leieobjekt' => $leieobjekt
                ]),
            ]);
        }

        /** @var HtmlElement[] $leietakere */
        $leietakere = [];
        /** @var Leieforholdsett $leieforholdsett */
        $leieforholdsett = $leieobjekt->hentLeieforhold();
        /** @var Leieforhold $leieforhold */
        foreach ($leieforholdsett as $leieforhold) {
            $leietakere[] = new HtmlElement('li', [], new HtmlElement('a', [
                'title' => 'Gå til leieforhold ' . $leieforhold->hentId(),
                'href' => '/drift/index.php?oppslag=leieforholdkort&id=' . $leieforhold->hentId()
            ], $leieforhold->hentNavn()));
        }
        /** @var HtmlElement $leietakere */
        $leietakere = new HtmlElement('ul', [], $leietakere);

        /** @var string[] $områder */
        $områder = [];
        /** @var Område $område */
        foreach ($leieobjekt->hentOmråder() as $område) {
            $områder[] = $område->hentNavn();
        }

        $this->definerData([
            'leieobjektId' => $leieobjekt->hentId(),
            'aktivert' => !$leieobjekt->hentIkkeForUtleie(),
            'navn' => $leieobjekt->hentNavn(),
            'gateadresse' => $leieobjekt->hentGateadresse(),
            'etasje' => $leieobjekt->hentEtg(),
            'beskrivelse' => $leieobjekt->hent('beskrivelse'),
            'postnummer' => $leieobjekt->hentPostnr(),
            'poststed' => $leieobjekt->hentPoststed(),
            'bilde' => $leieobjekt->hentBilde(),
            'bygningsId' => null,
            'bygningsKode' => null,
            'bygningsNavn' => null,
            'erBolig' => $leieobjekt->hentBoenhet(),
            'areal' => $leieobjekt->hentAreal(),
            'antallRom' => $leieobjekt->hentAntRom(),
            'bad' => $leieobjekt->hentBad(),
            'toalett' => $this->app->vis(Toalettbeskrivelse::class, [
                'leieobjekt' => $leieobjekt
            ]),
            'toalettKategori' => $leieobjekt->hentToalettKategori(),
            'merknader' => nl2br($leieobjekt->hentMerknader()),
            'leieberegningsId' => null,
            'leieberegningsNavn' => null,
            'leieberegningsFormel' => null,
            'leieforholdfelt' => $this->app->vis(Leieforholdfelt::class, ['leieobjekt' => $leieobjekt]),
            'leietakere' => $leietakere,
            'områder' => $this->app->liste($områder),
            'egendefinerteFelter' => $egendefinerteFelter,
        ]);
        return parent::forberedData();
    }

    /**
     * @param Leieobjekt $leieobjekt
     * @return ViewArray
     * @throws \Throwable
     */
    private function hentEgendefinerteFelter(?Leieobjekt $leieobjekt): ViewArray
    {
        $resultat = new ViewArray();
        /** @var Egenskapsett $eavEgenskaper */
        $eavEgenskaper = $this->app->hentSamling(Egenskap::class);
        $eavEgenskaper->leggTilEavVerdiJoin('visning', true);
        $eavEgenskaper->leggTilSortering('rekkefølge');
        $eavEgenskaper->leggTilFilter(['modell' => Leieobjekt::class])->låsFiltre();

        foreach($eavEgenskaper as $egenskap) {
            /** @var Verdi|null $verdiObjekt */
            $verdiObjekt = $leieobjekt
                ? $egenskap->hentVerdier()
                    ->leggTilFilter(['objekt_id' => $leieobjekt->hentId()])
                    ->låsFiltre()->hentFørste()
                : null;
            $beskrivelse = $egenskap->hentBeskrivelse();
            $resultat->addItem(new HtmlElement('dt', [], $beskrivelse));
            $resultat->addItem(new HtmlElement('dd', [], $egenskap->visVerdi($verdiObjekt, 'visning')));
        }
        return $resultat;
    }
}