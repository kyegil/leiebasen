<?php

    namespace Kyegil\Leiebasen\Visning\drift\html\shared;


    use Kyegil\Leiebasen\Visning\Drift;
    use Kyegil\Leiebasen\Visning\felles\html\shared\HeadInterface as HtmlHead;

    /**
     * Visning for html head i drift
     *
     *  Mulige variabler:
     *      $title
     *      $extPath
     *      $extraMetaTags
     *      $links
     *      $scripts Scripts inserted to HTML Head
     * @package Kyegil\Leiebasen\Visning\drift\html\shared
     */
    class Head extends Drift implements HtmlHead
    {
        /** @var string */
        protected $template = 'drift/html/shared/Head.html';

        /**
         * @return Head
         */
        protected function forberedData(): Head
        {
            $this->definerData([
                'title' => '',
                'extPath' => '',
                'extraMetaTags' => '',
                'links' => '',
                'scripts' => ''
            ]);
            return parent::forberedData();
        }
    }