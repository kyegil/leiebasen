<?php

namespace Kyegil\Leiebasen\Visning\drift\html\shared\body;


/**
 * Visning for main content
 *
 *  Mulige variabler:
 *      $tittel
 *      $innhold
 *      $breadcrumbs
 *      $varsler
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\drift\html\shared\body
 */
class Main extends \Kyegil\Leiebasen\Visning\Drift
{
    /** @var string */
    protected $template = 'drift/html/shared/body/Main.html';

    /**
     * @return Main
     */
    protected function forberedData()
    {
        $this->definerData([
            'tittel' => '',
            'innhold' => '',
            'breadcrumbs' => '',
            'varsler' => '',
            'knapper' => ''
        ]);
        return parent::forberedData();
    }

}