<?php

namespace Kyegil\Leiebasen\Visning\drift\html\shared\body;


use Kyegil\Leiebasen\Visning\Drift;

/**
 * Visning for footer i drift
 *
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\drift\html\shared\body
 */
class Footer extends Drift
{
    /** @var string */
    protected $template = 'drift/html/shared/body/Footer.html';

    /**
     * @return $this
     */
    protected function forberedData()
    {
        return parent::forberedData();
    }

}