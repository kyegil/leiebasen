<?php

    namespace Kyegil\Leiebasen\Visning\drift\html\shared;


    use Kyegil\Leiebasen\Modell\Person;
    use Kyegil\Leiebasen\Visning\Drift;
    use Kyegil\Leiebasen\Visning\felles\html\bootstrap\Modal;
    use Kyegil\Leiebasen\Visning\felles\html\shared\body\Brukermeny;
    use Kyegil\Leiebasen\Visning\drift\html\shared\body\Header;
    use Kyegil\Leiebasen\Visning\felles\html\shared\body\Hovedmeny;
    use Kyegil\Leiebasen\Visning\felles\html\shared\body\Varsler;
    use Kyegil\Leiebasen\Visning\felles\html\shared\HtmlInterface;

    /**
     * Visning for grunnleggende html-struktur i Drift
     *
     *  Mulige variabler:
     *      $head HTML head
     *      $header
     *      $hovedmeny
     *      $brukermeny
     *      $varsler
     *      $brukernavn
     *      $main
     *      $footer
     *      $scripts Script satt inn til slutt før </body>
     * @package Kyegil\Leiebasen\Visning\drift\html\shared
     */
    class Html extends Drift implements HtmlInterface
    {
        /** @var string */
        protected $template = 'drift/html/shared/Html.html';

        /**
         * @return Html
         */
        protected function forberedData()
        {
            $head = $this->hent('head') ?? $this->app->vis(Head::class, [
                'extPath' => '/pub/lib/' . $this->app->ext_bibliotek,
                'title' => $this->app->tittel,
            ]);
            $header = $this->hent('header') ?? $this->app->vis(Header::class);

            /** @var Person $bruker */
            $bruker = $this->hentRessurs('bruker');
            $hovedmeny = $this->hent('hovedmeny') ?? $this->app->vis(Hovedmeny::class, [
                'bruker' => $bruker ?? ($this->app->hoveddata['bruker'] ?? null)
            ]);
            $brukermeny = $this->hent('hovedmeny') ?? $this->app->vis(Brukermeny::class, [
                'bruker' => $bruker ?? ($this->app->hoveddata['bruker'] ?? null)
            ]);
            $modal = $this->app->vis(Modal::class, ['id' => 'main-modal']);
            $varsler = $this->hent('varsler') ?? $this->app->vis(Varsler::class);
            $this->definerData([
                'head' => $head,
                'header' => $header,
                'hovedmeny' => $hovedmeny,
                'brukermeny' => $brukermeny,
                'modal' => $modal,
                'varsler' => $varsler,
                'brukernavn' => $bruker ? $bruker->hentNavn() : '',
                'main' => '',
                'footer' => '',
                'scripts' => ''
            ]);
            return parent::forberedData();
        }

    }