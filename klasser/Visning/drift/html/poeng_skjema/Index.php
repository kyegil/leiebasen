<?php

namespace Kyegil\Leiebasen\Visning\drift\html\poeng_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold as LeieforholdModell;
use Kyegil\Leiebasen\Modell\Poengprogram;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\auto_complete\Leieforhold;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\ViewRenderer\ViewInterface;

/**
 * Visning for poengskjema i drift
 *
 *  Ressurser:
 *      poeng \Kyegil\Leiebasen\Modell\Poengprogram\Poeng
 *      program \Kyegil\Leiebasen\Modell\Poengprogram
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\drift\html\poeng_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/poeng_skjema/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return Index
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['program', 'poeng', 'leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return Index
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Poengprogram\Poeng $poeng */
        $poeng = $this->hentRessurs('poeng');

        /** @var Poengprogram $program */
        $program = $poeng ? $poeng->program : $this->hentRessurs('program');

        /** @var LeieforholdModell $leieforhold */
        $leieforhold = $poeng ? $poeng->leieforhold : $this->hentRessurs('leieforhold');

        /** @var Field $poengIdFelt */
        $poengIdFelt = $this->app->vis(Field::class, [
            'name' => 'id',
            'type' => 'hidden',
            'required' => true,
            'value' => $poeng ? $poeng->hentId() : '*'
        ]);

        /** @var Field $programFelt */
        $programFelt = $this->app->vis(Field::class, [
            'name' => 'program',
            'type' => 'hidden',
            'required' => true,
            'value' => $program->hentId()
        ]);

        /** @var Field $datoFelt */
        $datoFelt = $this->app->vis(Field::class, [
            'name' => 'tid',
            'label' => 'Dato',
            'type' => 'datetime-local',
            'required' => true,
            'value' => $poeng ? $poeng->hentTid()->format('Y-m-d H:i') : date('Y-m-d H:i')
        ]);

        /** @var Field $tekstFelt */
        $tekstFelt = $this->app->vis(Field::class, [
            'name' => 'tekst',
            'label' => 'Beskrivelse',
            'required' => true,
            'value' => $poeng ? $poeng->hentTekst() : null
        ]);

        /** @var Select $typeFelt */
        $typeFelt = $this->app->vis(Select::class, [
            'name' => 'type',
            'label' => 'Type',
            'required' => true,
            'value' => $poeng ? $poeng->hentType() : null,
            'options' => $program->hentTyper()
        ]);

        /** @var Field $verdiFelt */
        $verdiFelt = $this->app->vis(Field::class, [
            'name' => 'verdi',
            'type' => 'number',
            'label' => 'Poengverdi',
            'required' => true,
            'value' => $poeng ? $poeng->hentVerdi() : 1
        ]);

        if($leieforhold) {
            /** @var Field $leieforholdFelt */
            $leieforholdFelt = $this->app->vis(Field::class, [
                'name' => 'leieforhold',
                'type' => 'hidden',
                'required' => true,
                'value' => $leieforhold->hentId()
            ]);
        }
        else {
            /** @var Leieforhold $leieforholdFelt */
            $leieforholdFelt = $this->app->vis(Leieforhold::class, [
                'name' => 'leieforhold',
                'label' => 'Leieforhold',
                'required' => true,
            ]);
        }

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [];
        if ($poeng) {
            $buttons[] = new HtmlElement('a',
                [
                    'class' => 'button',
                    'onclick' => 'leiebasen.drift.poengSkjema.slett()'
                ],
                'Slett'
            );
        }
        $buttons[] = new HtmlElement('button', ['type' => 'submit'], 'Lagre');

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/drift/index.php?oppslag=poeng_skjema&oppdrag=ta_i_mot_skjema&skjema=poeng&id=' . ($poeng ? $poeng->hentId() : '*&kode=' . $program->kode),
                'formId' => 'poeng_skjema',
                'buttonText' => 'Lagre',
                'fields' => [
                    $leieforhold ?
                        DriftKontroller::lenkeTilLeieforholdKort($leieforhold) . ' ' . $leieforhold->hentBeskrivelse()
                        : '',

                    $poengIdFelt,
                    $programFelt,
                    $leieforholdFelt,
                    $datoFelt,
                    $typeFelt,
                    $tekstFelt,
                    $verdiFelt,
                ],
                'buttons' => $buttons
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}