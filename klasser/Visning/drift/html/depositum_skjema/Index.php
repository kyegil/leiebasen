<?php

namespace Kyegil\Leiebasen\Visning\drift\html\depositum_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Depositum;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\auto_complete\Leieforhold as LeieforholdVelger;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\DisplayField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FileField;
use Kyegil\Leiebasen\Visning\felles\html\form\HtmlEditor;
use Kyegil\Leiebasen\Visning\felles\html\form\RadioButtonGroup;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

/**
 * Visning for depositum-skjema i drift
 *
 * Ressurser:
 * * leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 * * depositum \Kyegil\Leiebasen\Modell\Leieforhold\Depositum
 *
 * Tilgjengelige variabler:
 * * $leieforholdId
 * * $depositumId
 * * $skjema
 * @package Kyegil\Leiebasen\Visning\drift\html\depositum_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/depositum_skjema/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Index
    {
        if(in_array($attributt,['leieforhold', 'depositum'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Depositum|null $depositum */
        $depositum = $this->hentRessurs('depositum');
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        $dato = $depositum ? $depositum->hentDato() : null;
        $utløpsdato = $depositum ? $depositum->hentUtløpsdato() : null;

        /** @var Field $depositumIdFelt */
        $depositumIdFelt = $this->app->vis(Field::class, [
            'name' => 'id',
            'type' => 'hidden',
            'required' => true,
            'value' => $depositum ? $depositum->hentId() : '*'
        ]);

        if($leieforhold) {
            /** @var ViewArray $leieforholdFelt */
            $leieforholdFelt = new ViewArray([
                $this->app->vis(DisplayField::class, [
                    'label' => 'Leieforhold',
                    'value' => $leieforhold->hentBeskrivelse(),
                ]),
                $this->app->vis(Field::class, [
                    'name' => 'leieforhold',
                    'type' => 'hidden',
                    'value' => $leieforhold->hentId()
                ])
            ]);
        }
        else {
            /** @var LeieforholdVelger $leieforholdFelt */
            $leieforholdFelt = $this->app->vis(LeieforholdVelger::class, [
                'name' => 'leieforhold',
                'label' => 'Leieforhold',
                'disabled' => $leieforhold,
                'required' => true,
                'multiple' => false,
                'value' => $leieforhold ? $leieforhold->hentId() : null
            ]);
        }

        /** @var RadioButtonGroup $typeFelt */
        $typeFelt = $this->app->vis(RadioButtonGroup::class, [
            'name' => 'type',
            'label' => 'Type',
            'options' => (object)['depositum' => 'Depositum', 'garanti' => 'Depositumsgaranti'],
            'required' => false,
            'value' => $depositum ? $depositum->hentType() : 'depositum',
        ]);

        /** @var Field $datoFelt */
        $datoFelt = $this->app->vis(DateField::class, [
            'name' => 'dato',
            'label' => 'Dato',
            'required' => true,
            'value' => $dato ? $dato->format('Y-m-d') : null,
        ]);

        /** @var Field $betalerFelt */
        $beløpFelt = $this->app->vis(Field::class, [
            'name' => 'beløp',
            'label' => 'Beløp',
            'required' => true,
            'value' => $depositum ? $depositum->hentBeløp() : null,
        ]);

        /** @var Field $betalerFelt */
        $betalerFelt = $this->app->vis(Field::class, [
            'name' => 'betaler',
            'label' => 'Navn på betaler/garantist',
            'required' => false,
            'value' => $depositum ? $depositum->hentBetaler() : null,
        ]);

        /** @var HtmlEditor $notatFelt */
        $notatFelt = $this->app->vis(HtmlEditor::class, [
            'name' => 'notat',
            'label' => 'Notat/detaljer',
            'value' => $depositum ? $depositum->hentNotat() : null,
        ]);

        /** @var Field $kontoFelt */
        $kontoFelt = $this->app->vis(Field::class, [
            'name' => 'konto',
            'label' => 'Depositumskonto',
            'required' => false,
            'value' => $depositum ? $depositum->hentKonto() : null,
        ]);

        /** @var FileField $garantiFelt */
        $garantiFelt = $this->app->vis(FileField::class, [
            'name' => 'garanti',
            'label' => 'Garantidokument',
            'required' => false,
            'value' => $depositum ? $depositum->hentFil() : null,
            'accept' => implode(', ', ['application/pdf', 'image/jpeg', 'image/png',])
        ]);

        /** @var DateField $utløpsdatoFelt */
        $utløpsdatoFelt = $this->app->vis(DateField::class, [
            'name' => 'utløpsdato',
            'label' => 'Utløpsdato for depositumsgaranti',
            'required' => false,
            'value' => $utløpsdato ? $utløpsdato->format('Y-m-d') : null,
        ]);

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [];
        $buttons[] = $this->app->visTilbakeknapp();
        if ($depositum) {
            $buttons[] = new HtmlElement('a',
                [
                    'class' => 'button',
                    'onclick' => 'leiebasen.drift.depositumSkjema.slett()'
                ],
                'Slett'
            );
        }
        $buttons[] = new HtmlElement('button', ['type' => 'submit'], 'Lagre');

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/drift/index.php?oppslag=depositum_skjema&oppdrag=ta_i_mot_skjema&skjema=depositum_skjema&id=' . ($depositum ? $depositum->hentId() : '*'),
                'formId' => 'depositum_skjema',
                'buttonText' => 'Lagre',
                'fields' => [
                    $depositumIdFelt,
                    $leieforholdFelt,
                    $typeFelt,
                    $datoFelt,
                    $beløpFelt,
                    $betalerFelt,
                    $notatFelt,
                    $kontoFelt,
                    $garantiFelt,
                    $utløpsdatoFelt,
                ],
                'buttons' => $buttons
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}