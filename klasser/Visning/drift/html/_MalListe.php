<?php

namespace Kyegil\Leiebasen\Visning\drift\html;


use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Samling;
use Kyegil\Leiebasen\Visning\drift\html\shared\body\main\Content;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;

class _MalListe extends Content
{
    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): _MalListe
    {
        /** @var Person $bruker */
        $bruker = $this->app->hentModell(Person::class, $this->app->bruker['id']);

        /**
         * Kolonne-objekt for DataTables
         *
         * @link https://datatables.net/reference/option/columns
         */
        $kolonner = [
            (object)[
                'data' => 'id',
                'name' => 'id',
                'title' => 'Id',
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
            ],
        ];

        $data = $this->hentTabellData();

        /**
         * Konfig-objekt for DataTables
         * * pageLength initiert antall per side
         * * lengthMenu
         *
         * @link https://datatables.net/reference/option/
         */
        $dataTablesConfig = (object)[
            'fixedHeader' => (object)[
                'footer' => true,
            ],
            'layout' => (object)[
                'top' => [
                    'search',
                    (object)['div' => (object)['html' => '<h3>' . $this->app->tittel . '</h3>']],
                    (object)['buttons' => ['copy','pdf', 'print']],
                ],
                'topStart' => null,
                'topEnd' => null,
                'bottomStart' => null,
                'bottomEnd' => null,
            ],
            'order' => [[0, 'desc']],
            'columns' => $kolonner,
            'pageLength' => 300,
            'pagingType'=> 'simple_numbers',
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'scrollCollapse' => false, // Collapse when no content
            'scrollY' => 'calc(100vh - 240px)', // Table height
            'search' => (object)[
                'return' => true,
            ],

            // Direkte data
            'data' => $data,

            // eller ajax-data
            'searchDelay' => 1000,
            'serverSide' => true,
            'processing' => true,
            'ajax' => (object)[
                'url' => '/drift/index.php?oppslag=_mal_liste&oppdrag=hent_data&data=',
            ]
        ];

        $knapper = [];

        $knapper = [
            $this->hentLinkKnapp(
                '/drift/index.php?oppslag=_mal_skjema&id=*',
                'Registrer ny',
                'Registrer ny')
        ];

        if($knapper) {
            $dataTablesConfig->scrollY = 'calc(100vh - 285px)';
        }
        $datasett = [
            'heading' => $this->app->tittel,
            'mainBodyContents' => $this->app->vis(DataTable::class, [
                'tableId' => 'mal',
                'caption' => '',
                'classes' => ['display', 'compact',
                    'full-page-dt' // Custom class for correct scrolling behavior on full page DataTables
                ],
                'kolonner' => $kolonner,
                'data' => $data,
                'dataTablesConfig' => $dataTablesConfig
            ]),
            'buttons' => $knapper,
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }

    private function hentTabelldata(): array
    {
        $data = [];
        /** @var Samling $modellsett */
        $modellsett = $this->app->hentSamling(Modell::class);

        foreach($modellsett as $modell) {
            $data[] = (object)[
                'id' => $modell->hentId(),
            ];
        }
        return $data;
    }
}