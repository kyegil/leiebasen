<?php

namespace Kyegil\Leiebasen\Visning\drift\html;


use Exception;
use Kyegil\Leiebasen\EavVerdiVisningRenderer;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Egenskapsett;
use Kyegil\Leiebasen\Modell\Eav\Verdi;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Modell\Person\Brukerprofil;
use Kyegil\Leiebasen\Modell\Personsett;
use Kyegil\Leiebasen\Visning\drift\html\shared\body\main\Content;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\CollapsibleSection;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

/** @inheritdoc  */
class LeieforholdBrukerprofilerSkjema extends Content
{
    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): LeieforholdBrukerprofilerSkjema
    {
        if(in_array($attributt,['leieforhold', 'kontraktpersoner'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): LeieforholdBrukerprofilerSkjema
    {
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        /** @var Personsett|null $kontraktpersoner */
        $kontraktpersoner = $this->hentRessurs('kontraktpersoner')
            ?? $this->hentKontraktPersonSett($leieforhold);

        $brukerprofilerFelter = new ViewArray();
        foreach ($kontraktpersoner as $person) {
            $brukerprofilerFelter->addItem($this->hentBrukerprofilFelter($leieforhold, $person));
        }

        $felter = new HtmlElement('div', ['class' => 'container'], [
            new HtmlElement('div', ['class' => 'alert alert-info'],
                'Fyll inn ønsket brukernavn for å opprette eller endre nettprofil for leiebasens beboersider.<br>'
                . 'La brukernavnet være blankt for å ikke opprette en nettprofil nå.<br>'
                . 'Ønsket passord kan enten fylles inn her, eller det kan opprettes av leietakeren selv ved første gangs pålogging.'
            ),
            new HtmlElement('div', ['class' => 'row'], $brukerprofilerFelter),
        ]);

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [];
        $buttons[] = new HtmlElement('a', [
            'role' => 'button',
            'href' => $this->app->returi->get()->url,
            'class' => 'btn btn-secondary fortsett',
        ], 'Fortsett uten å lagre'
        );
        $buttons[] = new HtmlElement('button', ['role'=> 'button', 'type' => 'submit', 'form' => 'brukerprofilskjema'], 'Lagre');

        $datasett = [
            'mainBodyContents' => $this->app->vis(AjaxForm::class, [
                'useModal' => true,
                'action' => $this->app::url('leieforhold_brukerprofiler_skjema', $leieforhold, 'mottak', ['skjema' => 'brukerprofilskjema']),
                'formId' => 'brukerprofilskjema',
                'fields' => $felter,
                'buttons' => ''
            ]),
            'buttons' => $buttons,
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }

    /**
     * @param Leieforhold $leieforhold
     * @return Personsett
     * @throws Exception
     */
    private function hentKontraktPersonSett(Leieforhold $leieforhold): Personsett
    {
        /** @var Person[] $personer */
        $personer = [];
        /** @var Personsett $personsett */
        $personsett = $this->app->hentSamling(Person::class);

        foreach ($leieforhold->hentLeietakere() as $leietaker) {
            $personer[] = $leietaker->person;
        }
        return $personsett->lastFra($personer);
    }

    /**
     * @param Leieforhold $leieforhold
     * @param Person $person
     * @return ViewInterface
     * @throws Exception
     */
    private function hentBrukerprofilFelter(Leieforhold $leieforhold, Person $person): ViewInterface
    {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $personFelter = new ViewArray();
        $brukerprofilFelter = new ViewArray();
        $brukerprofil = $person->hentBrukerprofil();
        $adgang = $person->hentAdgangTil(Person\Adgang::ADGANG_MINESIDER, $leieforhold);

        $personFelter->addItem($this->app->vis(Field::class, [
            'name' => "personer[$person][id]",
            'type' => 'hidden',
            'value' => $person->hentId(),
        ]));

        $personFelter->addItem($this->app->vis(Field::class, [
            'name' => "personer[$person][brukerprofil][login]",
            'label' => 'Brukernavn for innlogging',
            'value' => $brukerprofil ? $brukerprofil->login : null,
        ]));

        $personFelter->addItem($this->app->vis(Field::class, [
            'name' => "personer[$person][epost]",
            'type' => 'email',
            'label' => 'E-post',
            'value' => $person->epost,
        ]));

        $personFelter->addItem($this->app->vis(Checkbox::class, [
            'name' => "personer[$person][adgang][mine-sider][$leieforhold][epostvarsling]",
            'label' => 'Ønsker epostvarsling',
            'checked' => $adgang ? $adgang->epostvarsling : true,
        ]));

        /** @var Field $passord1Felt */
        $passord1Felt = $this->app->vis(Field::class, [
            'name' => "personer[$person][brukerprofil][pw1]",
            'type' => 'password',
            'minlength' => Person\Brukerprofil::$passordLengde,
            'pattern' => Person\Brukerprofil::$passordRegex,
            'placeholder' => Person\Brukerprofil::$passordHint,
            'label' => 'Ønsket passord',
            'required' => true,
            'autocomplete' => 'off',
            'value' => ''
        ]);

        /** @var Field $passord2Felt */
        $passord2Felt = $this->app->vis(Field::class, [
            'name' => "personer[$person][brukerprofil][pw2]",
            'type' => 'password',
            'label' => 'Gjenta ønsket passord',
            'required' => true,
            'autocomplete' => 'off',
            'value' => ''
        ]);

        $personFelter->addItem($this->app->vis(CollapsibleSection::class, [
            'name' => "personer[$person][brukerprofil][sett_passord]",
            'contents' => [$passord1Felt, $passord2Felt],
            'label' => 'Angi et passord for profilen',
        ]));

        $brukerprofilFelter->addItem($this->app->vis(Checkbox::class, [
            'name' => "personer[$person][brukerprofil_preferanser][reservasjon_mot_masseepost]",
            'label' => 'Ønsker å reservere seg mot e-post massemeldinger',
            'checked' => $person->hentBrukerProfilPreferanse('reservasjon_mot_masseepost'),
        ]));

        if($this->app->hentValg('sms_transport')) {
            $brukerprofilFelter->addItem($this->app->vis(Checkbox::class, [
                'name' => "personer[$person][brukerprofil_preferanser][reservasjon_mot_massesms]",
                'label' => 'Ønsker å reservere seg mot SMS massemeldinger',
                'checked' => $person->hentBrukerProfilPreferanse('reservasjon_mot_massesms'),
            ]));
        }

        $skadeAbonnement = $person->hentBrukerProfilPreferanse('skade_auto_abonnement');
        $skadeAbonnementLeieobjekter = $skadeAbonnement && isset($skadeAbonnement->leieobjekter) ? $skadeAbonnement->leieobjekter : [];
        $skadeAbonnementBygninger = $skadeAbonnement && isset($skadeAbonnement->bygninger) ? $skadeAbonnement->bygninger : [];

        $skadeAbonnementFelter = new ViewArray();
        if($leieforhold->leieobjekt) {
            $skadeAbonnementFelter->addItem($this->app->vis(Checkbox::class, [
                'name' => "personer[$person][brukerprofil_preferanser][skade_auto_abonnement][leieobjekter]",
                'label' => 'Ønsker å motta skademeldinger innrapportert på leieobjektet.',
                'checked' => in_array($leieforhold->leieobjekt->hentId(), $skadeAbonnementLeieobjekter),
            ]));
        }

        if($leieforhold->leieobjekt && $leieforhold->leieobjekt->hentBygning()) {
            $skadeAbonnementFelter->addItem($this->app->vis(Checkbox::class, [
                'name' => "personer[$person][brukerprofil_preferanser][skade_auto_abonnement][bygninger]",
                'label' => 'Ønsker å motta skademeldinger innrapportert på bygningen.',
                'checked' => in_array($leieforhold->leieobjekt->bygning->hentId(), $skadeAbonnementBygninger),
            ]));
        }

        if($skadeAbonnementFelter->getItems()) {
            $brukerprofilFelter->addItem($this->app->vis(FieldSet::class, [
                'label' => 'Skademeldinger',
                'contents' => $skadeAbonnementFelter,
            ]));
        }

        $this->forberedBrukerprofilEgendefinerteFelter($person, $brukerprofil, $brukerprofilFelter);
        $this->forberedAdgangEgendefinerteFelter($person, $leieforhold, $adgang, $brukerprofilFelter);

        $personFelter->addItem($this->app->vis(FieldSet::class, [
            'label' => 'Brukerprofil-preferanser',
            'contents' => [$brukerprofilFelter],
        ]));

        $personKort = $this->app->vis(FieldSet::class, [
            'class' => 'card',
            'label' => $person->hentNavn(),
            'contents' => $personFelter,
        ]);

        $resultat = new HtmlElement('div', ['class' => 'col'], $personKort);
        return $this->app->post($this, __FUNCTION__, $resultat, $args);
    }

    /**
     * @param Person $person
     * @param Brukerprofil|null $brukerprofil
     * @param ViewArray|null $felter
     * @return ViewArray
     * @throws Exception
     */
    private function forberedBrukerprofilEgendefinerteFelter(Person $person, ?Person\Brukerprofil $brukerprofil, ?ViewArray $felter = null): ViewArray
    {
        $felter = $felter ?? new ViewArray();

        /** @var Egenskapsett $egenskapsett */
        $egenskapsett = $this->app->hentSamling(Egenskap::class);
        $egenskapsett->leggTilEavVerdiJoin('felt_type', true);
        $egenskapsett->leggTilSortering('rekkefølge');
        $egenskapsett->leggTilFilter(['modell' => Person\Brukerprofil::class])->låsFiltre();

        foreach($egenskapsett as $egenskap) {
            /** @var Verdi|null $verdiObjekt */
            $verdiObjekt = $brukerprofil
                ? $egenskap->hentVerdier()
                    ->leggTilFilter(['objekt_id' => $brukerprofil->hentId()])
                    ->låsFiltre()->hentFørste()
                : null;

            /** @var object|null $feltType */
            $feltType = $egenskap->hentFeltType();
            /** @var string|null $visningType */
            $visningType = $feltType ? ($feltType->type ?? null) : null;
            /** @var class-string<EavVerdiVisningRenderer> $renderKlasse */
            $renderKlasse = Egenskap::$renderProsessorMapping['felt_type'][$visningType] ?? null;
            if(is_a($renderKlasse, EavVerdiVisningRenderer::class, true)) {
                $felt = $renderKlasse::vis(
                    $verdiObjekt,
                    (object)['name' => 'personer[' . $person->hentId() . '][brukerprofil][' . $egenskap->kode . ']'],
                    $egenskap,
                    $visningType,
                    'felt_type'
                );
                $felter->addItem($felt);
            }
        }

        return $felter;
    }

    /**
     * @param Person $person
     * @param Leieforhold $leieforhold
     * @param Adgang|null $adgang
     * @param ViewArray|null $felter
     * @return ViewArray
     * @throws Exception
     */
    private function forberedAdgangEgendefinerteFelter(Person $person, Leieforhold $leieforhold, ?Adgang $adgang, ?ViewArray $felter = null)
    {
        $felter = $felter ?? new ViewArray();

        /** @var Egenskapsett $egenskapsett */
        $egenskapsett = $this->app->hentSamling(Egenskap::class);
        $egenskapsett->leggTilEavVerdiJoin('felt_type', true);
        $egenskapsett->leggTilSortering('rekkefølge');
        $egenskapsett->leggTilFilter(['modell' => Person\Brukerprofil::class])->låsFiltre();

        foreach($egenskapsett as $egenskap) {
            /** @var Verdi|null $verdiObjekt */
            $verdiObjekt = $adgang
                ? $egenskap->hentVerdier()
                    ->leggTilFilter(['objekt_id' => $adgang->hentId()])
                    ->låsFiltre()->hentFørste()
                : null;

            /** @var object|null $feltType */
            $feltType = $egenskap->hentFeltType();
            /** @var string|null $visningType */
            $visningType = $feltType ? ($feltType->type ?? null) : null;
            /** @var class-string<EavVerdiVisningRenderer> $renderKlasse */
            $renderKlasse = Egenskap::$renderProsessorMapping['felt_type'][$visningType] ?? null;
            if(is_a($renderKlasse, EavVerdiVisningRenderer::class, true)) {
                $felt = $renderKlasse::vis(
                    $verdiObjekt,
                    (object)['name' => "personer[$person][adgang][mine-sider][$leieforhold][{$egenskap->kode}]"],
                    $egenskap,
                    $visningType,
                    'felt_type'
                );
                $felter->addItem($felt);
            }
        }

        return $felter;
    }
}