<?php

namespace Kyegil\Leiebasen\Visning\drift\html\massemelding_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Melding;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\bootstrap\Modal;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\HtmlTemplateEditor;
use Kyegil\Leiebasen\Visning\felles\html\form\MultiFileFields;
use Kyegil\Leiebasen\Visning\felles\html\form\RadioButtonGroup;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\Leiebasen\Visning\felles\html\form\TextArea;
use Kyegil\ViewRenderer\ViewInterface;

/**
 * Visning for _mal-skjema i drift
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\drift\html\massemelding_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/massemelding_skjema/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['bruker', 'mottakerLeieforholdIder', 'medium', 'emne', 'innhold', 'kopiOppsummering', 'kopiEksempel', 'svaradresse', 'sms_avsender', 'vedlegg'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        $mottakerLeieforholdIder = $this->hentRessurs('mottakerLeieforholdIder');
        $medium = $this->hentRessurs('medium') ?? Melding::MEDIUM_EPOST;
        $emne = $this->hentRessurs('emne');
        $innhold = $this->hentRessurs('innhold');
        $kopiOppsummering = $this->hentRessurs('kopiOppsummering');
        $kopiEksempel = $this->hentRessurs('kopiEksempel');
        $svaradresse = $this->hentRessurs('svaradresse');
        $smsAvsender = $this->hentRessurs('sms_avsender');

        $medieOptions = $this->hentMedieOptions();
        if(!property_exists($medieOptions, $medium)) {
            $medium = Melding::MEDIUM_EPOST;
        }

        /** @var string[] $vedlegg */
        $vedlegg = array_filter($this->hentRessurs('vedlegg') ?? [], 'file_exists');
        $vedlegg = array_map(function($fil) {
            return basename($fil);
        }, $vedlegg);

        /** @var Leieforholdsett $leieforholdsett */
        $leieforholdsett = $this->app->hentSamling(Leieforhold::class);
        $leieforholdsett->filtrerEtterIdNumre($mottakerLeieforholdIder ?? [])->låsFiltre();

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }

        $forhåndsvisning = $this->app->vis(Modal::class, [
            'id' => 'forhåndsvisning',
            'title' => 'Forhåndsvisning',
            'body' => '&nbsp;',
        ]);

        if(count((array)$medieOptions) > 1) {
            /** @var RadioButtonGroup $medieVelger */
            $medieVelger = $this->app->vis(RadioButtonGroup::class, [
                'label' => 'Velg medium',
                'name' => 'medium',
                'value' => $medium,
                'options' => $medieOptions
            ]);
        }
        else {
            /** @var RadioButtonGroup $medieVelger */
            $medieVelger = $this->app->vis(Field::class, [
                'type' => 'hidden',
                'name' => 'medium',
                'value' => $medium
            ]);
        }

        /** @var Field $mottakerLeieforholdIderFelt */
        $mottakerLeieforholdIderFelt = $this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\form\auto_complete\Leieforhold::class, [
            'name' => 'mottakere',
            'class' => 'massemelding-mottakere',
            'value' => $mottakerLeieforholdIder,
            'select2Configs' => (object)[
                'placeholder' => 'For avansert søk, bruk knappen nederst på siden',
            ],
        ]);

        /** @var Select $epostSvaradresseFelt */
        $epostSvaradresseFelt = $this->app->vis(Select::class, [
            'label' => 'Svaradresse',
            'name' => 'svaradresse',
            'options' => explode(',', $this->app->hentValg('epost_svaradresser')),
            'value' => $svaradresse
        ]);

        /** @var Select $epostSvaradresseFelt */
        $smsAvsenderFelt = $this->app->vis(Select::class, [
            'label' => 'SMS-avsender',
            'name' => 'sms_avsender',
            'options' => array_filter(explode(',', $this->app->hentValg('sms_avsendere')), 'trim'),
            'value' => $smsAvsender
        ]);

        $vedleggsFelt = $this->app->vis(MultiFileFields::class, [
            'label' => 'Vedlegg',
            'name' => 'vedlegg',
            'value' => $vedlegg
        ]);

        $kopiOppsummeringsFelt = $this->app->vis(Checkbox::class, [
            'name' => 'kopi[oppsummering]',
            'label' => 'Send oppsummering som epost til avsender',
            'value' => $kopiOppsummering
        ]);

        $kopiEksempelFelt = $this->app->vis(RadioButtonGroup::class, [
            'label' => 'Send kopi til avsender',
            'name' => 'kopi[eksempel]',
            'value' => $kopiEksempel,
            'options' => (object)['' => 'Nei', 'tilfeldig' => 'Send kopi av et tilfeldig valgt eksempel', 'alle' => 'Send kopi av alle eposter']
        ]);

        $emneFelt = $this->app->vis(Field::class, [
            'name' => 'emne',
            'label' => 'Emne',
            'value' => $emne
        ]);

        $renTekstInnholdFelt = $this->app->vis(TextArea::class, [
            'id' => 'tekst-innhold',
            'name' => 'innhold',
            'height' =>300,
            'label' => "Tekst",
            'value' => $innhold,
        ]);

        $htmlInnholdFelt = $this->app->vis(HtmlTemplateEditor::class, [
            'id' => 'html-innhold',
            'name' => 'innhold',
            'height' =>300,
            'label' => "Tekst",
            'value' => $innhold,
            'variabelOptions' => $this->hentVariabelOptions(),
        ]);

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [];
        $buttons[] = $this->app->visTilbakeknapp();
        $buttons[] = new HtmlElement('button', [
            'type' => 'submit',
            'name' => 'send',
            'value' => '0',
        ], 'Velg mottakere');

        $buttons[] = new HtmlElement('a', [
            'class' => 'button',
            'onclick' => 'leiebasen.drift.massemeldingSkjema.forhåndsvis()'
        ], 'Forhåndsvis');

        $buttons[] = new HtmlElement('button', [
            'class' => 'button',
            'type' => 'submit',
            'name' => 'send',
            'value' => '1',
        ], 'Send');

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/drift/index.php?oppslag=massemelding_skjema&oppdrag=ta_i_mot_skjema&skjema=massemelding_skjema',
                'formId' => 'massemelding_skjema',
                'fields' => [
                    $forhåndsvisning,
                    $medieVelger,
                    $mottakerLeieforholdIderFelt,
                    $epostSvaradresseFelt,
                    $smsAvsenderFelt,
                    $vedleggsFelt,
                    $kopiOppsummeringsFelt,
                    $kopiEksempelFelt,
                    $emneFelt,
                    $renTekstInnholdFelt,
                    $htmlInnholdFelt,
                ],
                'buttons' => $buttons
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return object
     * @throws Exception
     */
    private function hentMedieOptions(): object
    {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $options = (object)['epost' => 'E-post'];
        if($this->app->hentValg('sms_transport')) {
            $options->sms = 'SMS';
        }
        return $this->app->post($this, __FUNCTION__, $options, $args);
    }

    /**
     * @return object|mixed|null
     * @throws Exception
     */
    private function hentVariabelOptions(): object
    {
        $this->app->pre($this, __FUNCTION__, []);
        $variabelOptions = (object)[
            '{leieforhold.id}' => '{leieforhold.id}',
            '{leieforhold.kode}' => '{leieforhold.kode}',
            '{leieforhold.navn}' => '{leieforhold.navn}',
            '{leieforhold.beskrivelse}' => '{leieforhold.beskrivelse}',
            '{leieforhold.adressefelt}' => '{leieforhold.adressefelt}',
            '{leieforhold.fradato}' => '{leieforhold.fradato}',
            '{leieforhold.tildato}' => '{leieforhold.tildato}',
            '{leieforhold.kontraktnr}' => '{leieforhold.kontraktnr}',
            '{leieforhold.andel}' => '{leieforhold.andel}',
            '{leieforhold.antTerminer}' => '{leieforhold.antTerminer}',
            '{leieforhold.årligBasisleie}' => '{leieforhold.årligBasisleie}',
            '{leieforhold.giroformat}' => '{leieforhold.giroformat}',
            '{leieforhold.kid}' => '{leieforhold.kid}',
            '{leieforhold.leiebeløp}' => '{leieforhold.leiebeløp}',
            '{leieforhold.leietakerfelt}' => '{leieforhold.leietakerfelt}',
            '{leieforhold.oppsigelsestid}' => '{leieforhold.oppsigelsestid}',
            '{leieforhold.terminlengde}' => '{leieforhold.terminlengde}',
            '{leieforhold.utestående}' => '{leieforhold.utestående}',
            '{leieforhold.avtaletekst}' => '{leieforhold.avtaletekst}',
            '{leieobjekt.id}' => '{leieobjekt.id}',
            '{leieobjekt.navn}' => '{leieobjekt.navn}',
            '{leieobjekt.beskrivelse}' => '{leieobjekt.beskrivelse}',
            '{leieobjekt.adresse}' => '{leieobjekt.adresse}',
            '{leieobjekt.antRom}' => '{leieobjekt.antRom}',
            '{leieobjekt.areal}' => '{leieobjekt.areal}',
            '{leieobjekt.etg}' => '{leieobjekt.etg}',
            '{leieobjekt.gateadresse}' => '{leieobjekt.gateadresse}',
            '{leieobjekt.postnr}' => '{leieobjekt.postnr}',
            '{leieobjekt.poststed}' => '{leieobjekt.poststed}',
            '{leieobjekt.type}' => '{leieobjekt.type}',
            '{for hver kontrakt}{/for hver kontrakt}' => '{for hver kontrakt}{/for hver kontrakt}',
            '{kontrakt.kontraktnr}' => '- {kontrakt.kontraktnr}',
            '{kontrakt.tildato}' => '- {kontrakt.tildato}',
            '{for hver leietaker}{/for hver leietaker}' => '{for hver leietaker}{/for hver leietaker}',
            '{leietaker.id}' => '- {leietaker.id}',
            '{leietaker.navn}' => '- {leietaker.navn}',
            '{leietaker.fornavn}' => '- {leietaker.fornavn}',
            '{leietaker.etternavn}' => '- {leietaker.etternavn}',
            '{leietaker.adresse}' => '- {leietaker.adresse}',
            '{leietaker.epostadresse}' => '- {leietaker.epostadresse}',
            '{leietaker.orgnr}' => '- {leietaker.orgnr}',
            '{leietaker.fødselsdato}' => '- {leietaker.fødselsdato}',
            '{leietaker.fødselsnummer}' => '- {leietaker.fødselsnummer}',
            '{leietaker.telefon}' => '- {leietaker.telefon}',
            '{leietaker.mobil}' => '- {leietaker.mobil}',
            '{oppsigelse.oppsigelsesdato}' => '{oppsigelse.oppsigelsesdato}',
            '{oppsigelse.fristillelsesdato}' => '{oppsigelse.fristillelsesdato}',
            '{oppsigelse.oppsigelsestidSlutt}' => '{oppsigelse.oppsigelsestidSlutt}',
            '{for hvert delkrav}{/for hvert delkrav}' => '{for hvert delkrav}{/for hvert delkrav}',
            '{delkrav.kode}' => '- {delkrav.kode}',
            '{delkrav.navn}' => '- {delkrav.navn}',
            '{delkrav.beskrivelse}' => '- {delkrav.beskrivelse}',
            '{for hvert tillegg}{/for hvert tillegg}' => '{for hvert tillegg}{/for hvert tillegg}',
            '{tillegg.kode}' => '- {tillegg.kode}',
            '{tillegg.navn}' => '- {tillegg.navn}',
            '{tillegg.beskrivelse}' => '- {tillegg.beskrivelse}',
            '{utleier.navn}' => '{utleier.navn}',
            '{utleier.adresse}' => '{utleier.adresse}',
            '{utleier.postnr}' => '{utleier.postnr}',
            '{utleier.poststed}' => '{utleier.poststed}',
            '{utleier.orgnr}' => '{utleier.orgnr}',
            '{utleier.telefon}' => '{utleier.telefon}',
            '{utleier.mobil}' => '{utleier.mobil}',
            '{utleier.epost}' => '{utleier.epost}',
            '{utleier.hjemmeside}' => '{utleier.hjemmeside}',
        ];
        return $this->app->post($this, __FUNCTION__, $variabelOptions);
    }
}