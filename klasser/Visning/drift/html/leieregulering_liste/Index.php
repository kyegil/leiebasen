<?php

namespace Kyegil\Leiebasen\Visning\drift\html\leieregulering_liste;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;

/**
 * Visning for Leiereguleringsforslag i drift
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $varsler
 *      $tabell
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\drift\html\leieregulering_liste
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/leieregulering_liste/Index.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /**
         * Kolonne-objekt for DataTables
         *
         * @link https://datatables.net/reference/option/columns
         */
        $kolonner = [
//            (object)[
//                'orderable' => false,
//                'className' => 'select-checkbox',
//            ],
            (object)[
                'data' => 'id',
                'name' => 'id',
                'title' => 'Leieforhold',
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return row.navn;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
            (object)[
                'data' => 'justeringsdato',
                'name' => 'justeringsdato',
                'title' => 'Dato',
                'width' => 75,
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return row.utløpt ? 'Utløpt' : row.justeringsdato_formatert;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
            (object)[
                'data' => 'eksisterende_leie',
                'name' => 'eksisterende_leie',
                'title' => 'Leie nå',
                'width' => 85,
                'responsivePriority' => 20, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return row.eksisterende_leie_formatert;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
                'className' => 'beløp',
            ],
            (object)[
                'data' => 'sist_endret',
                'name' => 'sist_endret',
                'title' => 'Sist endret',
                'width' => 75,
                'responsivePriority' => 20, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return row.sist_endret_formatert;} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
            ],
            (object)[
                'data' => 'ny_leie',
                'name' => 'ny_leie',
                'title' => 'Ny leie',
                'width' => 85,
                'responsivePriority' => 10, // Høyere tall = lavere prioritet
                'render' => new JsFunction(
                    "if (type == 'display') {return '<a href=\"/drift/index.php?oppslag=leieregulering_skjema&id=' + row.id + '\">' + row.ny_leie_formatert + '</a>';} return data;",
                    ['data', 'type', 'row', 'meta']
                ),
                'className' => 'beløp',
            ],
            (object)[
                'data' => 'varsling',
                'name' => 'varsling',
                'title' => 'Varsling',
                'responsivePriority' => 5, // Høyere tall = lavere prioritet
                'className' => 'varsling',
            ],
        ];

        /**
         * Konfig-objekt for DataTables
         * * pageLength initiert antall per side
         * * lengthMenu
         *
         * @link https://datatables.net/reference/option/
         */
        $dataTablesConfig = (object)[
            'buttons' => [
                (object)[
                    'extend' => 'selectNone',
                    'text' => 'Fjern markeringer',
                ],
                (object)[
                    'extend' => 'selectAll',
                    'text' => 'Marker alle',
                ],
                (object)[
                    'text' => 'Marker brev-varsler',
                    'action' => new JsFunction("dt.rows( '.brev' ).select();", ['e', 'dt', 'node', 'config'])
                ],
                (object)[
                    'extend' => 'selected',
                    'text' => 'Godkjenn markerte forslag',
                    'action' => new JsFunction("leiebasen.drift.leiereguleringListe.godkjennMarkerte(dt);", ['e', 'dt', 'node', 'config'])
                ],
                (object)[
                    'extend' => 'selectedSingle',
                    'text' => 'Vis detaljer',
                    'action' => new JsFunction("window.location.href='/drift/index.php?oppslag=leieregulering_skjema&id=' + dt.rows( { selected: true } ).data()[0].id;", ['e', 'dt', 'node', 'config'])
                ],
            ],
            'dom' => '<lfB>rtip',
            'order' => [[1, 'asc']],
            'columns' => $kolonner,
            'createdRow' => new JsFunction("if(data.klasser) $(row).addClass(data.klasser);", ['row', 'data', 'dataIndex']),
            'fixedHeader' => true,
            'pageLength' => 25,
            'responsive' => (object)[
                'details' => (object)[
                ],
            ],
            'rowId' => 'id',
            'search' => (object)[
                'return' => true,
            ],
            'searchDelay' => 1000,
            'select' => (object)[
                'style' => 'os',
                'info' => false,
            ],
            'serverSide' => true,
            'processing' => true,
            'ajax' => (object)[
                'url' => '/drift/index.php?oppslag=leieregulering_liste&oppdrag=hent_data&data=reguleringsforslag',
            ],
        ];

        $datasett = [
            'varsler' => '',
            'tabell' => $this->app->vis(DataTable::class, [
                'tableId' => 'reguleringsforslag',
                'caption' => 'Forslag til leieregulering',
                'kolonner' => $kolonner,
                'dataTablesConfig' => $dataTablesConfig
            ]),
            'knapper' => [],
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}