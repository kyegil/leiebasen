<?php

namespace Kyegil\Leiebasen\Visning\drift\html\leieobjekter_ledige;


use Kyegil\Leiebasen\Visning\Drift;

/**
 * Leieobjekt-linje i liste over ledige leieobjekter i flyko
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $dato
 *      $leieobjektnr
 *      $leieobjektbeskrivelse
 *      $endring
 * @package Kyegil\Leiebasen\Visning\drift\html\leieobjekter_ledige
 */
class KommendeLedighetLinje extends Drift
{
    protected $template = 'drift/html/leieobjekter_ledige/KommendeLedighetLinje.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        return parent::forberedData();
    }
}