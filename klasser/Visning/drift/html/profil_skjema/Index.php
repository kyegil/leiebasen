<?php

namespace Kyegil\Leiebasen\Visning\drift\html\profil_skjema;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;

/**
 * Visning for brukerprofil-skjema i drift
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *  Mulige variabler:
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\drift\html\profil_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/html/profil_skjema/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['brukerprofil_person'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Person $brukerprofilPerson */
        $brukerprofilPerson = $this->hentRessurs('brukerprofil_person');
        $brukerProfil = $brukerprofilPerson ? $brukerprofilPerson->hentBrukerProfil() : null;

        $adganger = [];

        foreach($brukerprofilPerson->hentAdganger() as $adgang) {
            $beskrivelse = $this->app::ucfirst($adgang->adgang);
            $beskrivelse = [new HtmlElement('a', [
                'href' => '/drift/index.php?oppslag=adgang_skjema&id=' . $adgang->hentId(),
                'title' => 'Vis innstillinger',
            ], $beskrivelse)];
            if ($adgang->leieforhold) {
                $beskrivelse[] = ' Leieforhold ' . $adgang->leieforhold->hentId() . ' ' . $adgang->leieforhold->hentBeskrivelse();
            }
            $adganger[] = $this->app->vis(Checkbox::class, [
                'name' => 'adganger[]',
                'label' => $beskrivelse,
                'checked' => true,
                'value' => $adgang->hentId()
            ]);
        }

        $adganger[] = new HtmlElement('a', [
            'class' => 'button',
            'href' => '/drift/index.php?oppslag=adgang_skjema&id=*&personid=' . $brukerprofilPerson->hentId(),
            'title' => 'Tildel ny adgang',
        ], 'Tildel ny adgang');

        /** @var Field $brukernavnFelt */
        $brukerprofilIdFelt = $this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\form\Field::class, [
            'name' => 'id',
            'type' => 'hidden',
            'required' => true,
            'value' => $brukerProfil ? $brukerProfil->hentId() : '*'
        ]);

        /** @var Field $brukernavnFelt */
        $brukernavnFelt = $this->app->vis(Field::class, [
            'name' => 'login',
            'label' => 'Brukernavn',
            'required' => true,
            'value' => $brukerProfil ? $brukerProfil->hentBrukernavn() : null
        ]);

        if($brukerProfil) {
            $endrePassordLenke = new HtmlElement('a', [
                'href' => '/drift/index.php?oppslag=profil_passord_skjema&id=' . $brukerProfil->hentId(),
                'title' => 'Endre passordet som hører til dette brukernavnet'
            ],'Endre passordet');
        }
        else {
            $endrePassordLenke = '';
        }

        /** @var Field $brukernavnFelt */
        $epostAdresseFelt = $this->app->vis(Field::class, [
            'name' => 'epost',
            'label' => 'E-postadresse',
            'required' => false,
            'value' => $brukerprofilPerson->hentEpost()
        ]);

        $brukerprofilFeltsett = $this->app->vis(FieldSet::class, [
            'name' => 'epost',
            'label' => 'Brukerprofil',
            'contents' => [
                $brukerprofilIdFelt,
                $brukernavnFelt,
                $endrePassordLenke,
                $epostAdresseFelt
            ]
        ]);

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/drift/index.php?oppslag=profil_skjema&oppdrag=ta_i_mot_skjema&skjema=brukerprofil&id=' . $brukerprofilPerson->hentId(),
                'formId' => 'brukerprofil',
                'buttonText' => 'Lagre endringer',
                'fields' => [
                    $brukerprofilFeltsett,
                    $this->app->vis(FieldSet::class, [
                        'name' => 'epost',
                        'label' => 'Adganger',
                        'contents' => $adganger
                    ])
                ]
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}