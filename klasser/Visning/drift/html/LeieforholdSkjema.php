<?php

namespace Kyegil\Leiebasen\Visning\drift\html;


use Collator;
use DateInterval;
use DateTimeInterface;
use Exception;
use Kyegil\CoreModel\CoreModel;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Delkravtypesett;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Egenskapsett;
use Kyegil\Leiebasen\Modell\Eav\Verdi;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Personsett;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\drift\html\shared\body\main\Content;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\CollapsibleSection;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\html\form\NumberField;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

/**
 * {@inheritdoc}
 *
 * Ressurser:
 * * \Kyegil\Leiebasen\Modell\Leieforhold leieforhold
 * * \DateTimeInterface fraDato
 * * \DateTimeInterface tilDato
 * * \DateInterval oppsigelsestid
 * * \Kyegil\Fraction\Fraction andel
 * * int leieobjektId
 * * bool fornyelse
 */
class LeieforholdSkjema extends Content
{
    private static array $forfallsdatoer = [
        '' => 'ingen fast dato',
        '1' => '1. hver måned',
        '2' => '2. hver måned',
        '3' => '3. hver måned',
        '4' => '4. hver måned',
        '5' => '5. hver måned',
        '6' => '6. hver måned',
        '7' => '7. hver måned',
        '8' => '8. hver måned',
        '9' => '9. hver måned',
        '10' => '10. hver måned',
        '11' => '11. hver måned',
        '12' => '12. hver måned',
        '13' => '13. hver måned',
        '14' => '14. hver måned',
        '15' => '15. hver måned',
        '16' => '16. hver måned',
        '17' => '17. hver måned',
        '18' => '18. hver måned',
        '19' => '19. hver måned',
        '20' => '20. hver måned',
        '21' => '21. hver måned',
        '22' => '22. hver måned',
        '23' => '23. hver måned',
        '24' => '24. hver måned',
        '25' => '25. hver måned',
        '26' => '26. hver måned',
        '27' => '27. hver måned',
        't' => 'Siste dagen hver måned'
];
    private static array $forfallsdager = [
        '' => 'ingen fast ukedag',
        '1' => 'mandag',
        '2' => 'tirsdag',
        '3' => 'onsdag',
        '4' => 'torsdag',
        '5' => 'fredag',
        '6' => 'lørdag',
        '7' => 'søndag',
    ];
    private static array $betalingsfrister = [
        'P0D' => 'forskuddsvis betaling',
        'P1D' => '1 dag',
        'P2D' => '2 dager',
        'P3D' => '3 dager',
        'P4D' => '4 dager',
        'P5D' => '5 dager',
        'P6D' => '6 dager',
        'P7D' => '7 dager',
        'P8D' => '8 dager',
        'P9D' => '9 dager',
        'P10D' => '10 dager',
        'P11D' => '11 dager',
        'P12D' => '12 dager',
        'P13D' => '13 dager',
        'P14D' => '14 dager',
        'P15D' => '15 dager',
        'P16D' => '16 dager',
        'P17D' => '17 dager',
        'P18D' => '18 dager',
        'P19D' => '19 dager',
        'P20D' => '20 dager',
        'P21D' => '21 dager',
        'P22D' => '22 dager',
        'P23D' => '23 dager',
        'P24D' => '24 dager',
        'P25D' => '25 dager',
        'P26D' => '26 dager',
        'P27D' => '27 dager',
        'P28D' => '28 dager',
        'P29D' => '29 dager',
        'P30D' => '30 dager',
        'P31D' => '31 dager',
        'P1M' => '1 måned',
        'P2M' => '2 måneder',
        'P3M' => '3 måneder',
    ];

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): LeieforholdSkjema
    {
        if(in_array($attributt,['leieforhold', 'fraDato', 'tilDato', 'oppsigelsestid', 'andel',])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): LeieforholdSkjema
    {
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        if($leieforhold) {
            $this->sett('leieforholdId', $leieforhold->hentId());
        }

        /** @var int|null $leieforholdId */
        $leieforholdId = $this->hent('leieforholdId');

        $felter = new ViewArray();
        $felter->addItem($this->hentNavnData());
        $felter->addItem($this->hentNavnData(true));

        /**
         * Felt for leieforhold-id
         *
         * @var Field $leieforholdIdFelt
         */
        $leieforholdIdFelt = $this->app->vis(Field::class, [
            'name' => 'id',
            'type' => 'hidden',
            'value' => $leieforhold ? $leieforhold->hentId() : $leieforholdId
        ]);
        $felter->addItem($leieforholdIdFelt);

        if($leieforhold) {
            $felter->addItem(new HtmlElement('div', [], $leieforhold->hentBeskrivelse()));
        }

        if($leieforhold && !$this->hent('fornyelse')) {
            $notat = new HtmlElement('div', ['class' => 'alert alert-info'],
                'Ved endringer i eksisterende leieforhold, bør det vurderes å skrive ut og signere leieavtalen på nytt.'
            );
            $felter->addItem($notat);
        }

        $felter->addItem($this->app->vis(CollapsibleSection::class, [
            'label' => 'Leieforholdets omfang',
            'contents' => $this->forberedOmfangFelter(),
            'collapsed' => false,
        ]));

        $felter->addItem($this->app->vis(CollapsibleSection::class, [
            'label' => 'Leietakere',
            'contents' => $this->forberedLeietakerFelter(),
            'collapsed' => false,
        ]));

        $this->forberedLeieberegningFelter($felter);

        $felter->addItem($this->app->vis(CollapsibleSection::class, [
            'label' => 'Betalingsterminer og forfall',
            'contents' => $this->forberedTerminFelter(),
            'collapsed' => false,
        ]));

        /** @var ViewArray $leieforholdSpesifikasjoner */
        $leieforholdSpesifikasjoner = $this->forberedLeieforholdSpesifikasjoner($leieforhold);
        if($leieforholdSpesifikasjoner->getItems()) {
            $felter->addItem($this->app->vis(CollapsibleSection::class, [
                'label' => 'Andre spesifikasjoner',
                'contents' => $leieforholdSpesifikasjoner,
                'collapsed' => false,
            ]));
        }

        /** @var string[]|ViewInterface[] $buttons */
        $buttons = [];
        $buttons[] = $this->app->visTilbakeknapp();
        $buttons[] = new HtmlElement('button', ['role'=> 'button', 'type' => 'submit', 'form' => 'leieforhold_skjema'], ($leieforhold ? 'Lagre' : 'Opprett'));

        $datasett = [
            'mainBodyContents' => $this->app->vis(AjaxForm::class, [
                'action' => $this->app::url('leieforhold_skjema', ($leieforhold ?: '*'), 'mottak', ['skjema' => 'leieforhold_skjema']),
                'formId' => 'leieforhold_skjema',
                'fields' => $felter,
                'buttons' => '',
                'useModal' => true,
            ]),
            'buttons' => $buttons,
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }

    /**
     * @param DateInterval|null $oppsigelsestid
     * @return mixed|null
     */
    private function hentOppsigelsestidOptions(?DateInterval $oppsigelsestid)
    {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $options = (object)[
            'P12M' => $this->app::periodeformat('P12M'),
            'P6M' => $this->app::periodeformat('P6M'),
            'P4M' => $this->app::periodeformat('P4M'),
            'P3M' => $this->app::periodeformat('P3M'),
            'P2M' => $this->app::periodeformat('P2M'),
            'P1M' => $this->app::periodeformat('P1M'),
            'P28D' => $this->app::periodeformat('P28D'),
            'P21D' => $this->app::periodeformat('P21D'),
            'P14D' => $this->app::periodeformat('P14D'),
            'P7D' => $this->app::periodeformat('P7D'),
            'P0M' => 'Ingen oppsigelsestid',
        ];
        if($oppsigelsestid) {
            $options->{$this->app::periodeformat($oppsigelsestid, true)}
                = $this->app::periodeformat($oppsigelsestid);
        }
        return $this->app->post($this, __FUNCTION__, $options, $args);
    }

    /**
     * @return ViewArray
     * @throws Exception
     */
    private function forberedOmfangFelter(): ViewArray
    {
        $omfangFelter = new ViewArray();
        $bofellesskapFelter = new ViewArray();

        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        if($leieforhold) {
            $this->sett('leieforholdId', $leieforhold->hentId());
        }

        /** @var int|null $leieobjektId */
        $leieobjektId = $leieforhold && $leieforhold->hentLeieobjekt() ? $leieforhold->leieobjekt->id : $this->hent('leieobjektId');

        /** @var DateTimeInterface|null $fraDato */
        $fraDato = $leieforhold ? $leieforhold->hentFradato() : $this->hentRessurs('fraDato');
        $virkeDato = $leieforhold ? $this->hentRessurs('fraDato') : null;

        /** @var DateTimeInterface|null $fraDato */
        $tilDato = $leieforhold ? $leieforhold->hentTildato() : $this->hentRessurs('tilDato');

        /** @var DateInterval|null $oppsigelsestid */
        $oppsigelsestid = $leieforhold ? $leieforhold->hentOppsigelsestid() : $this->hentRessurs('oppsigelsestid');

        /** @var Fraction|null $andel */
        $andel = $leieforhold ? $leieforhold->hentAndel() : $this->hentRessurs('andel');
        $andel->simplify();

        /**
         * Leieobjekt-feltet
         *
         * @var Select $leieobjektFelt
         */
        $leieobjektFelt = $this->app->vis(Select::class, [
            'id' => 'leieobjekt-velger',
            'name' => 'leieobjekt',
            'label' => 'Leieobjekt',
            'required' => true,
            'hidden' => (bool)$leieforhold,
            'value' => $leieobjektId,
            'options' => $leieobjektId ? [$leieobjektId] : null,
        ]);

        /**
         * Felt for fra-dato
         *
         * @var DateField $fradatoFelt
         */
        $fradatoFelt = $this->app->vis(DateField::class, [
            'name' => 'fradato',
            'required' => true,
            'disabled' => (bool)$leieforhold,
            'label' => 'Fra dato',
            'value' => $fraDato ? $fraDato->format('Y-m-d') : null
        ]);

        /**
         * Dersom bofellesskap, er dette delt i likeverdige leieforhold?
         *
         * @var Checkbox $tidsbegrensetFelt
         */
        $tidsbegrensetFelt = $this->app->vis(Checkbox::class, [
            'id' => 'tidsbegrenset-leieforhold',
            'label' => 'Leieforholdet er tidsbestemt',
            'checked' => (bool)$tilDato,
        ]);

        /**
         * Felt for fra-dato
         *
         * @var DateField $tildatoFelt
         */
        $tildatoFelt = $this->app->vis(DateField::class, [
            'name' => 'tildato',
            'required' => true,
            'label' => 'til og med dato',
            'value' => $tilDato ? $tilDato->format('Y-m-d') : null
        ]);

        $fornyFelt = null;
        $virkeDatoFelt = null;
        if($leieforhold) {
            $fornyFelt = $this->app->vis(Checkbox::class, [
                'name' => 'fornyelse',
                'label' => 'Forny leieavtalen',
                'checked' => (bool)$this->hent('fornyelse'),
            ]);
            $virkeDatoFelt = $this->app->vis(DateField::class, [
                'name' => 'virkedato',
                'required' => true,
                'label' => 'Ikrafttreden for ny kontrakt og evt endringer',
                'min' => $leieforhold->hentKontrakt()->hentDato()->format('Y-m-d'),
                'value' => $virkeDato ? $virkeDato->format('Y-m-d') : null
            ]);
        }

        /**
         * Felt for fra-dato
         *
         * @var DateField $tildatoFelt
         */
        $oppsigelsestidFelt = $this->app->vis(Select::class, [
            'name' => 'oppsigelsestid',
            'label' => 'Oppsigelsestid',
            'value' => $oppsigelsestid ? $this->app::periodeformat($oppsigelsestid, true) : 'P3M',
            'options' => $this->hentOppsigelsestidOptions($oppsigelsestid),
        ]);

        /**
         * Dersom bofellesskap, er dette delt i likeverdige leieforhold?
         *
         * @var Checkbox $bofellesskapLikeDelerFelt
         */
        $bofellesskapLikeDelerFelt = $this->app->vis(Checkbox::class, [
            'id' => 'bofellesskap-like-deler',
            'label' => 'Leieobjektet er delt mellom flere likeverdige leieforhold',
            'pattern' => '[0-9]+',
            'checked' => !$andel || $andel->getNumerator() == 1,
        ]);

        /**
         * Total antall deler som leieobjektet er delt opp i ved bofellesskap
         *
         * @var Field $bofellesskapNevnerFelt
         */
        $bofellesskapNevnerFelt = $this->app->vis(NumberField::class, [
            'name' => 'andel[nevner]',
            'inputmode' => 'numeric',
            'label' => 'Totalt antall deler som leieobjektet er delt i',
            'value' => $andel ? $andel->getDenominator() : 1,
            'min' => 1,
        ]);

        /**
         * Antall deler som dette leieforholdet utfyller ved bofellesskap
         *
         * @var Field $bofellesskapTellerFelt
         */
        $bofellesskapTellerFelt = $this->app->vis(NumberField::class, [
            'name' => 'andel[teller]',
            'inputmode' => 'numeric',
            'label' => 'Antall deler som dette leieforholdet legger beslag på',
            'value' => $andel ? $andel->getNumerator() : 1,
            'min' => 1,
        ]);

        $bofellesskapFelter->addItem($bofellesskapLikeDelerFelt);
        $bofellesskapFelter->addItem($bofellesskapNevnerFelt);
        $bofellesskapFelter->addItem($bofellesskapTellerFelt);
        $bofellesskapFeltsett = $this->app->vis(CollapsibleSection::class, [
            'name' => 'bofellesskap',
            'label' => 'Det er flere leieforhold i leieobjektet (bofellesskap)',
            'contents' => $bofellesskapFelter,
            'collapsed' => !$andel || $andel->compare('=', 1),
        ]);

        $omfangFelter->addItem($leieobjektFelt);
        $omfangFelter->addItem($fradatoFelt);
        if($virkeDatoFelt) {
            $omfangFelter->addItem($fornyFelt);
            $omfangFelter->addItem($virkeDatoFelt);
        }
        $omfangFelter->addItem($tidsbegrensetFelt);
        $omfangFelter->addItem($tildatoFelt);
        $omfangFelter->addItem($oppsigelsestidFelt);
        $omfangFelter->addItem($bofellesskapFeltsett);
        return $omfangFelter;
    }

    /**
     * @return ViewArray
     * @throws Exception
     */
    private function forberedLeietakerFelter(): ViewArray
    {
        $leietakerFelter = new ViewArray();
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        if($leieforhold) {
            $nr = 0;
            foreach($leieforhold->hentLeietakere()->leggTilPersonModell() as $leietaker) {
                $leietakerFelter->addItem($this->hentLeietakerFeltsett($leietaker->person, $nr));
                $nr++;
            }

            /** @var Personsett|null $husstandMedlemmer */
            if($husstandMedlemmer = $leieforhold->hentAndreBeboere()) {
                foreach ($husstandMedlemmer as $husstandMedlem) {
                    $leietakerFelter->addItem($this->hentLeietakerFeltsett($husstandMedlem, $nr, true));
                    $nr++;
                }
            }
        }
        else {
            $leietakerFelter->addItem($this->hentLeietakerFeltsett());
        }

        $adressekortModal = $this->hentAdressekortModal();
        $leietakerFelter->addItem($adressekortModal);

        $nyLeietakerKnapp = new HtmlElement('div', ['role' => 'button', 'class' => 'btn btn-secondary ny-leietaker-knapp'], 'Legg til flere leietakere');
        $leietakerFelter->addItem(new HtmlElement('div', ['class' => 'row align-items-end'], $nyLeietakerKnapp));

        return $leietakerFelter;
    }

    /**
     * @param ViewArray $felter
     * @return void
     * @throws Exception
     */
    private function forberedLeieberegningFelter(ViewArray $felter): void
    {
        $delbeløpFelter = new ViewArray();
        $tilleggFelter = new ViewArray();

        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        /** @var Delkravtypesett $delkravtyper */
        $delkravtyper = $this->app->hentDelkravtyper()
            ->leggTilFilter(['kravtype' => Leieforhold\Krav::TYPE_HUSLEIE])->låsFiltre();

        foreach($delkravtyper as $delkravtype) {
            $contents = [
                $delkravtype->hentBeskrivelse(),
                $this->app->vis(Field::class, [
                    'type' => 'hidden',
                    'name' => 'delkrav[' . $delkravtype . '][id]',
                    'value' => $delkravtype->hentId(),
                ]),
                $this->app->vis(Field::class, [
                    'type' => 'hidden',
                    'name' => 'delkrav[' . $delkravtype . '][relativ]',
                    'value' => $delkravtype->relativ ? '1' : '',
                ]),
                $this->app->vis(NumberField::class, [
                    'step' => '1',
                    'min' => '0',
                    'inputmode' => 'decimal',
                    'readonly' => !$delkravtype->hentValgfritt(),
                    'label' => $delkravtype->relativ ? 'Beløp oppgitt i prosent av husleien' : 'Beløp i kr per år',
                    'name' => 'delkrav[' . $delkravtype . '][sats]',
                    'value' => $leieforhold
                        ?   round($leieforhold->hentDelkravtype($delkravtype->kode) ? $leieforhold->hentDelkravtype($delkravtype->kode)->hentSats() : 0)
                        :   round($delkravtype->hentSats()),
                ]),
                $this->app->vis(Checkbox::class, [
                    'label' => 'Beløpet skal avstemmes periodisk',
                    'name' => 'delkrav[' . $delkravtype . '][periodisk_avstemming]',
                    'checked' => $leieforhold && $leieforhold->hentDelkravtype($delkravtype->kode)
                        && $leieforhold->hentDelkravtype($delkravtype->kode)->hentPeriodiskAvstemming(),
                    'disabled' => $delkravtype->hentId() != $this->app->hentValg('delkravtype_fellesstrøm')
                ]),
            ];
            if(!$delkravtype->selvstendigTillegg) {
                $delbeløpFelter->addItem($this->app->vis(FieldSet::class, [
                    'label' => $delkravtype->hentNavn(),
                    'class' => 'col-sm',
                    'contents' => $contents,
                ]));
            }
            else {
                $tilleggFelter->addItem($this->app->vis(FieldSet::class, [
                    'label' => $delkravtype->hentNavn(),
                    'class' => 'col-sm border-0',
                    'contents' => $contents,
                ]));
            }
        }

        $foreslåttBeløpFelt = $this->app->vis(Field::class, [
            'readonly' => true,
            'name' => 'foreslått_leiebeløp',
            'label' => 'Foreslått årlig leiebeløp',
            'class' => 'col-sm',
        ]);

        $nåværendeLeiebeløp = $leieforhold
            ? round($leieforhold->hentLeiebeløp() * $leieforhold->hentAntTerminer())
            : null;
        $nåværendeBeløpFelt = null;
        if($leieforhold) {
            $nåværendeBeløpFelt = $this->app->vis(Field::class, [
                'readonly' => true,
                'label' => 'Nåværende årlig leiebeløp',
                'name' => 'nåværende_leiebeløp',
                'value' => $nåværendeLeiebeløp,
                'class' => 'col-sm',
            ]);
        }

        $leiebeløpFelt = $this->app->vis(NumberField::class, [
            'required' => true,
            'step' => '1',
            'min' => '0',
            'inputmode' => 'decimal',
            'name' => 'leiebeløp',
            'label' => 'Årlig leiebeløp',
            'value' => $leieforhold && !$this->hent('fornyelse') ? $nåværendeLeiebeløp : null,
        ]);

        $brukNåværendeLeiebeløpKnapp = null;
        if($leieforhold && $this->hent('fornyelse')) {
            $brukNåværendeLeiebeløpKnapp =  new HtmlElement('div', [
                'role' => 'button',
                'tabindex' => '0',
                'class' => 'btn btn-secondary bruk-nåværende-leiebeløp disabled',
            ], 'Behold gjeldende leie');
        }

        $brukForeslåttLeiebeløpKnapp =  new HtmlElement('div', [
            'role' => 'button',
            'tabindex' => '0',
            'class' => 'btn btn-secondary bruk-foreslått-leiebeløp disabled',
        ], 'Bruk foreslått leie');

        $felter->addItem($this->app->vis(CollapsibleSection::class, [
            'label' => 'Leieberegning',
            'collapsed' => false,
            'contents' => new HtmlElement('div',
                ['class' => 'container'],
                [
                    new HtmlElement('div',
                        ['class' => 'delbeløp row justify-content-between'],
                        [$delbeløpFelter]
                    ),
                    new HtmlElement('div',
                        ['class' => 'row justify-content-between align-items-start'],
                        [
                            $foreslåttBeløpFelt,
                            $nåværendeBeløpFelt,
                            new HtmlElement('div',
                                ['class' => 'col-sm'],
                                [
                                    $leiebeløpFelt,
                                    new HtmlElement('div', ['class' => 'row'], [
                                        $brukForeslåttLeiebeløpKnapp,
//                                            $brukNåværendeLeiebeløpKnapp
                                        ]
                                    )
                                ]
                            ),
                        ]
                    ),
                ]
            )
        ]));
        $felter->addItem($this->app->vis(CollapsibleSection::class, [
            'label' => 'Tillegg til husleia',
            'collapsed' => false,
            'contents' => new HtmlElement('div',
                ['class' => 'tillegg container'],
                [
                    new HtmlElement('div',
                        ['class' => 'row justify-content-between'],
                        [$tilleggFelter]
                    )
                ]
            )
        ]));
    }

    /**
     * @return ViewArray
     * @throws Exception
     */
    private function forberedTerminFelter(): ViewArray
    {
        $terminFelter = new ViewArray();

        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        $antallTerminerFelt = $this->app->vis(Select::class, [
            'label' => 'Antall årlige leieterminer',
            'class' => 'col-sm',
            'name' => 'ant_terminer',
            'value' => $leieforhold ? $leieforhold->antTerminer : 12,
            'options' => (object)[
                1 => '1 (årlig)',
                2 => '2 (hver 6. måned)',
                3 => '3 (hver 4. måned)',
                4 => '4 (kvartalsvis)',
                6 => '6 (hver 2. måned)',
                12 => '12 (månedlig)',
                13 => '13 (hver 4. uke)',
                26 => '26 (hver 2. uke)',
                52 => '52 (hver uke)',
            ]
        ]);

        $leiePerTermin = $this->app->vis(Field::class, [
            'readonly' => true,
            'class' => 'col-sm leie-per-termin',
            'label' => 'Leiebeløp per termin',
        ]);

        $totaltPerTermin = $this->app->vis(Field::class, [
            'readonly' => true,
            'class' => 'col-sm totalt-per-termin',
            'label' => 'Totalt å betale per termin inkludert tillegg',
        ]);

        $fastÅrligDatoFelt = $this->app->vis(DateField::class, [
            'label' => 'Påbegynn ny leietermin årlig på denne datoen',
            'class' => 'col-sm',
            'name' => 'fast_dato',
            'value' => $leieforhold ? $leieforhold->forfallFastDato : null,
        ]);

        $fastMånedsDatoFelt = $this->app->vis(Select::class, [
            'label' => 'Påbegynn nye leieterminer på denne dagen i måneden',
            'class' => 'col-sm',
            'name' => 'dag_i_måneden',
            'value' => $leieforhold ? $leieforhold->forfallFastDagIMåneden : '1',
            'options' => (object)self::$forfallsdatoer,
        ]);

        $fastUkesDatoFelt = $this->app->vis(Select::class, [
            'label' => 'Påbegynn nye leieterminer på en',
            'name' => 'ukedag',
            'class' => 'col-sm',
            'value' => $leieforhold ? $leieforhold->forfallFastUkedag : '',
            'options' => (object)self::$forfallsdager,
        ]);

        $betalingsfristFelt = $this->app->vis(Select::class, [
            'label' => 'Betalingsfrist for leie og faste krav, ifra påbegynt leietermin',
            'name' => 'termin_betalingsfrist',
            'class' => 'col-sm',
            'value' => $leieforhold
                ? ($leieforhold->terminBetalingsfrist instanceof DateInterval
                    ? $this->app->periodeformat($leieforhold->terminBetalingsfrist, true)
                    : '')
                : $this->app->hentValg('betalingsfrist_faste_krav'),
            'options' => (object)self::$betalingsfrister,
        ]);

        $terminFelter->addItem(new HtmlElement('div',
            ['class' => 'row justify-content-between align-items-end'],
            [$antallTerminerFelt, $leiePerTermin, $totaltPerTermin]
        ));
        $terminFelter->addItem(new HtmlElement('div',
            ['class' => 'row justify-content-between align-items-end'],
            [$fastÅrligDatoFelt, $fastMånedsDatoFelt, $fastUkesDatoFelt, $betalingsfristFelt]
        ));

        return $terminFelter;
    }

    /**
     * @param Leieforhold|null $leieforhold
     * @return ViewArray
     * @throws Exception
     */
    private function forberedLeieforholdSpesifikasjoner(?Leieforhold $leieforhold): ViewArray
    {
        $spesifikasjoner = new ViewArray();

        /** @var Egenskapsett $eavEgenskaper */
        $eavEgenskaper = $this->app->hentSamling(Egenskap::class);
        $eavEgenskaper->leggTilEavVerdiJoin('felt_type', true);
        $eavEgenskaper->leggTilSortering('rekkefølge');
        $eavEgenskaper->leggTilFilter(['modell' => Leieforhold::class])->låsFiltre();

        foreach($eavEgenskaper as $egenskap) {
            /** @var Verdi|null $verdiObjekt */
            $verdiObjekt = $leieforhold
                ? $egenskap->hentVerdier()
                    ->leggTilFilter(['objekt_id' => $leieforhold->hentId()])
                    ->låsFiltre()->hentFørste()
                : null;
            $spesifikasjoner->addItem($egenskap->visVerdi($verdiObjekt, 'felt_type'));
        }

        return $spesifikasjoner;
    }

    /**
     * @param Person|null $person
     * @param int $nr
     * @param bool $husstandMedlem
     * @return Visning
     */
    private function hentLeietakerFeltsett(
        ?Person $person = null,
        int $nr = 0,
        bool $husstandMedlem = false
    ): Visning
    {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $leietakerFelter = new ViewArray();

        $erOrgFelt = $this->app->vis(Checkbox::class, [
            'id' => '',
            'label' => 'Firma/organisasjon/gruppe',
            'name' => 'leietakere[' . $nr . '][er_org]',
            'checked' => $person && $person->erOrg,
        ]);
        $leietakerFelter->addItem($erOrgFelt);

        $husstandmedlemFelt = $this->app->vis(Checkbox::class, [
            'id' => '',
            'label' => 'Registreres som <i>ikke avtaleansvarlig</i> husstandmedlem',
            'name' => 'leietakere[' . $nr . '][husstandmedlem]',
            'checked' => $husstandMedlem,
        ]);

        $fornavnFelt = $this->app->vis(Field::class, [
            'id' => '',
            'placeholder' => 'Fornavn',
            'class' => 'col-3',
            'required' => true,
            'autocomplete' => 'off',
            'name' => 'leietakere[' . $nr . '][fornavn]',
            'value' => $person ? $person->fornavn : '',
            'list' => 'fornavn-data',
        ]);

        $etternavnFelt = $this->app->vis(Field::class, [
            'id' => '',
            'placeholder' => 'Etternavn',
            'class' => 'col-3',
            'required' => true,
            'autocomplete' => 'off',
            'name' => 'leietakere[' . $nr . '][etternavn]',
            'value' => $person ? $person->etternavn : '',
            'list' => 'etternavn-data',
        ]);

        $adressekortFelt = $this->app->vis(Field::class, [
            'id' => '',
            'tabindex' => -1,
            'required' => true,
            'class' => 'form-control',
            'name' => 'leietakere[' . $nr . '][id]',
            'value' => $person ? $person->hentId() : null,
        ]);

        $adresseSøkKnapp = new HtmlElement('div', [
            'role' => 'button',
            'tabindex' => '0',
            'class' => 'btn btn-outline-secondary adresse-søk-knapp disabled',
        ], 'Finn adressekort');

        $slettKnapp = new HtmlElement('div', ['role' => 'button', 'class' => 'fas fa-trash btn btn-secondary p-2 fjern-leietaker-knapp'], 'Slett');

        $leietakerFelter->addItem(new HtmlElement('div', ['class' => 'row justify-content-between'], [
            $fornavnFelt,
            $etternavnFelt,
            new HtmlElement('div', ['class' => 'col-sm input-group mb-3'], [
                $adressekortFelt,
                new HtmlElement('div', ['class' => 'input-group-append'], [
                    $adresseSøkKnapp,
                ])
            ]),
            new HtmlElement('div', ['class' => 'col-sm',], [$slettKnapp,])
        ]));
        $leietakerFelter->addItem($husstandmedlemFelt);

        $leietakerFeltsett = $this->app->vis(FieldSet::class, [
            'id' => '',
            'label' => $person ? $person->hentNavn() : null,
            'class' => 'leietaker-feltsett container my-2 py-2',
            'contents' => $leietakerFelter,
        ]);

        return $this->app->post($this, __FUNCTION__, $leietakerFeltsett, $args);
    }

    private function hentAdressekortModal()
    {
        $adresseSøkFelt = $this->app->vis(FieldSet::class, [
            'id' => 'adressekort-velger',
            'label' => 'Velg adressekort',
        ]);
        return $this->app->vis(Visning\felles\html\bootstrap\Modal::class, [
            'id' => 'adressekort-modal',
            'class' => 'modal-adressekort',
            'title' => '',
            'body' => new HtmlElement('div', [], [
                'Finn eksisterende adressekort dersom dette finnes.<br>',
                'Registrer som ny kontakt <i>kun dersom du er sikker på</i> at vedkommende ikke allerede er registrert.',
                $adresseSøkFelt
            ]),
            'verticallyCentered' => true,
            'size' => '',
        ]);
    }

    /**
     * @param bool $etternavn
     * @return array
     * @throws Exception
     */
    private function hentNavneforslag(bool $etternavn = false): array
    {
        $tp = CoreModel::$tablePrefix;
        /** @var string[] $navneforslag */
        $select = $this->app->mysqli->select([
            'flat' => true,
            'distinct' => true,
            'source' => "`{$tp}personer` AS `personer`",
            'fields' => [$etternavn ? 'etternavn' : 'fornavn'],
        ])->data;
        $navneforslag = [''];
        foreach ($select as $fornavn) {
            $navneforslag = array_merge($navneforslag, explode(' ', trim($fornavn)));
        }
        $navneforslag = array_unique(array_filter($navneforslag, function ($fornavn) {
            return strpos($fornavn, '.') === false;
        }));
        array_walk($navneforslag, function (&$fornavn) {
            $fornavn = ucfirst($fornavn);
        });
        Collator::create('no_NB')->sort($navneforslag);
        return $navneforslag;
    }

    /**
     * @param bool $etternavn
     * @return HtmlElement
     * @throws Exception
     */
    private function hentNavnData(bool $etternavn = false): HtmlElement
    {
        $options = [];
        foreach ($this->hentNavneforslag($etternavn) as $navneforslag) {
            $options[] = new HtmlElement('option', ['value' => $navneforslag]);
        }
        return new HtmlElement('datalist', ['id' => $etternavn ? 'etternavn-data' : 'fornavn-data'], $options);
    }
}