<?php


namespace Kyegil\Leiebasen\Visning\drift\extjs4\Ext\form\flexi_checkbox_groups;

use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning as BygningsModell;
use Kyegil\Leiebasen\Samling;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FlexiCheckboxGroup;
use stdClass;

/**
 * Flexi Checkbox Bygningvelger for ExtJs4
 *
 *  Resources:
 *      config              object  The config object with which the ComboBox element will be initialised
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly    boolean if not set to false only the config is returned as a JS object
 *      itemId              string The itemId will be assigned to the checkbox group
 *      name                string The HTML name of the checkbox group
 *      remote              boolean The data should be loaded over AJAX
 *      url                 string The remote data url
 *      dataValues          object|array The local Data values,
 *                          given either as array or as a value=>text object
 *      value               array The initial value(s),
 *
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.FieldSet
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form
 */
class Bygninger extends FlexiCheckboxGroup
{
    /**
     * @var array
     */
    protected $resources = [
        'remote' => true,
        'url' => '/drift/index.php?oppslag=data&oppdrag=hent_data&data=komboliste_bygninger',
    ];

    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        if ($this->getResource('remote') === false) {
            $this->setResource('dataValues', $this->getDataValues());
        }
        return parent::prepareData();
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    private function getDataValues(): stdClass
    {
        $values = new stdClass();
        /** @var Samling $bygningsamling */
        $bygningsamling = $this->app->hentSamling(BygningsModell::class);
        /** @var BygningsModell $bygning */
        foreach ($bygningsamling as $bygning) {
            $values->{$bygning->hentId()} = $bygning->hentKode() . ' ' . $bygning->hentNavn();
        }
        return $values;
    }
}