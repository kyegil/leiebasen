<?php


namespace Kyegil\Leiebasen\Visning\drift\extjs4\Ext\form\field\combo_box;

use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt as LeieobjektModell;
use Kyegil\Leiebasen\Modell\Leieobjektsett;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\ComboBox;
use stdClass;

/**
 * Combo Box Leieobjektvelger for ExtJs4
 *
 *  Resources:
 *      config              object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly    boolean if not set to false only the config is returned as a JS object
 *      remote              boolean The data should be loaded over AJAX
 *      url                 string The remote data url
 *      dataValues          object|array The local Data values,
 *                          given either as array or as a value=>text object
 *      value               The initial value,
 *
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.ComboBox
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\combo_box
 */
class Leieobjekt extends ComboBox
{
    /**
     * @var array
     */
    protected $resources = [
        'remote' => true,
        'url' => '/drift/index.php?oppslag=data&oppdrag=hent_data&data=komboliste_leieobjekter',
    ];

    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        if ($this->getResource('remote') === false) {
            $this->setResource('dataValues', $this->getDataValues());
        }
        $this->setConfigIfNotSet([
            'forceSelection' => true,
        ]);
        return parent::prepareData();
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    private function getDataValues(): stdClass
    {
        $values = new stdClass();
        /** @var Leieobjektsett $leieobjektsamling */
        $leieobjektsamling = $this->app->hentSamling(LeieobjektModell::class);
        /** @var LeieobjektModell $leieobjekt */
        foreach ($leieobjektsamling as $leieobjekt) {
            $values->{$leieobjekt->hentId()} = $leieobjekt->hentId() . ' ' . $leieobjekt->hentBeskrivelse();
        }
        return $values;
    }
}