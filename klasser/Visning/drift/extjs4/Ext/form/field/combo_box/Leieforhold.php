<?php


namespace Kyegil\Leiebasen\Visning\drift\extjs4\Ext\form\field\combo_box;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold as LeieforholdModell;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\ComboBox;
use stdClass;

/**
 * Combo Box Leieforholdvelger for ExtJs4
 *
 *  Resources:
 *      config              object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly    boolean if not set to false only the config is returned as a JS object
 *      remote              boolean The data should be loaded over AJAX
 *      url                 string The remote data url
 *      dataValues          object|array The local Data values,
 *                          given either as array or as a value=>text object
 *      listWidth           string|int The drop down list width
 *
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.ComboBox
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\combo_box
 */
class Leieforhold extends ComboBox
{
    /**
     * @var array
     */
    protected $resources = [
        'remote' => true,
        'url' => '/drift/index.php?oppslag=data&oppdrag=hent_data&data=komboliste_leieforhold',
    ];

    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        if ($this->getResource('remote') === false) {
            $this->setResource('dataValues', $this->getDataValues());
        }
        $this->setConfigIfNotSet([
            'forceSelection' => true,
            'hideTrigger' => true,
        ]);
        return parent::prepareData();
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    private function getDataValues(): stdClass
    {
        $values = new stdClass();
        /** @var Leieforholdsett $leieforholdsamling */
        $leieforholdsamling = $this->app->hentSamling(LeieforholdModell::class);
        foreach ($leieforholdsamling as $leieforhold) {
            $values->{$leieforhold->hentId()} = $leieforhold->hentId() . ' ' . $leieforhold->hentBeskrivelse();
        }
        return $values;
    }
}