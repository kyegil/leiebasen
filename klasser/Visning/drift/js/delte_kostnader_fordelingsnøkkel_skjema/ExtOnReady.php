<?php

namespace Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_fordelingsnøkkel_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\extjs4\Ext\form\field\combo_box\Leieforhold;
use Kyegil\Leiebasen\Visning\drift\extjs4\Ext\form\field\combo_box\Leieobjekt;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\button\Button;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Checkbox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Hidden;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Number;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Text;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FieldContainer;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel as FormPanel;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\RadioGroup;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $tjeneste \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste|null
 *      $fordelingsnøkkel \Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel|null
 *      $leiebasenJsObjektEgenskaper stdClass
 *  Mulige variabler:
 *      $tjenesteId
 *      $meny
 *      $initLeiebasenJsObjektEgenskaper
 *      $skjema
 *      $formActionUrl
 * @package Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_fordelingsnøkkel_skjema
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/delte_kostnader_fordelingsnøkkel_skjema/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['tjeneste', 'fordelingsnøkkel'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        if ($attributt == 'leiebasenJsObjektEgenskaper') {
            return $this->settLeiebasenJsObjektEgenskaper($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param stdClass $leiebasenJsObjektEgenskaper
     * @return $this
     */
    public function settLeiebasenJsObjektEgenskaper(stdClass $leiebasenJsObjektEgenskaper): Drift
    {
        return $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var stdClass $leiebasenJsObjektEgenskaper */
        $leiebasenJsObjektEgenskaper = $this->hentRessurs('leiebasenJsObjektEgenskaper');
        if (!$leiebasenJsObjektEgenskaper) {
            $leiebasenJsObjektEgenskaper = new stdClass();
            $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
        }
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Tjeneste|null $tjeneste */
        $tjeneste = $this->hentRessurs('tjeneste');
        /** @var Fordelingsnøkkel|null $fordelingsnøkkel */
        $fordelingsnøkkel = $tjeneste ? $tjeneste->hentFordelingsnøkkel() : $this->hentRessurs('fordelingsnøkkel');
        $tjenesteId = $tjeneste->hentId();

        $leiebasenJsObjektEgenskaper->skjema = (object)[
            'felter' => (object)[]
        ];

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }

        $elementer = [];
        /** @var FieldContainer $elementBeholder */
        $elementBeholder = $this->app->vis(FieldContainer::class, [
            'itemId' => 'elementbeholder'
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->elementer = $elementBeholder;
        settype($leiebasenJsObjektEgenskaper->drift, 'object');
        $leiebasenJsObjektEgenskaper->drift->delteKostnaderFordelingsnøkkelSkjema = (object)[
            'feltMaler' => []
        ];

        if ($fordelingsnøkkel) {
            foreach ($fordelingsnøkkel->hentElementer() as $fordelingselement) {
                $elementFeltsett = $this->hentElementFeltsett($fordelingselement);
                $elementer[] = $elementFeltsett;
            }
        }

        for($i = 0; $i < 20; $i++) {
            $leiebasenJsObjektEgenskaper->drift->delteKostnaderFordelingsnøkkelSkjema->feltMaler[]
            = $this->hentElementFeltsett();
        }
        $elementBeholder->setData('items', $elementer);

        $this->definerData([
            'tjenesteId' => $tjenesteId,
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'initLeiebasenJsObjektEgenskaper' => $this->gjengiLeiebasenJsObjektEgenskaper(),
            'skjema' => $this->app->vis(FormPanel::class, [
                'id' => 'skjema',
                'configObjectOnly' => false,
                'title' => 'fordelingsnøkkel for ' . $tjeneste->hentNavn(),
                'enabledSaveButton' => true,
                'formActionUrl' => '/drift/index.php?oppslag=delte_kostnader_fordelingsnøkkel_skjema&id=' . $tjenesteId . '&oppdrag=ta_imot_skjema',
                'returnUrl' => $this->app->returi->get(),
                'formSourceUrl' => '/drift/index.php?oppslag=delte_kostnader_fordelingsnøkkel_skjema&id=' . $tjenesteId . '&oppdrag=hent_data',
                'items' => [
                    new JsCustom('leiebasen.skjema.felter.elementer'),
                    $this->app->hentVisning(Button::class, [
                        'text' => 'Legg til nytt element',
                        'scale' => 'medium',
                        'handler' => new JsFunction('button.up().getComponent("elementbeholder").add(leiebasen.drift.delteKostnaderFordelingsnøkkelSkjema.feltMaler.pop());button.setVisible(leiebasen.drift.delteKostnaderFordelingsnøkkelSkjema.feltMaler.length)', ['button', 'event'])
                    ])
                ]
            ]),
            'formActionUrl' => '/drift/index.php?oppslag=delte_kostnader_fordelingsnøkkel_skjema&id=' . $tjenesteId . '&oppdrag=ta_i_mot_skjema'
        ]);

        return parent::forberedData();
    }

    /**
     * @param Fordelingsnøkkel\Element|null $fordelingselement
     * @return FieldSet
     * @throws Exception
     */
    private function hentElementFeltsett(?Fordelingsnøkkel\Element $fordelingselement = null): FieldSet
    {
        $elementFeltsettElementer = [];
        $elementId = rand();

        $elementFeltsettElementer[] = $this->app->vis(Hidden::class, [
            'name' => 'element[' . $elementId . '][id]',
            'itemId' => $fordelingselement ? ('id-' . $fordelingselement->hentId()) : null,
            'value' => $fordelingselement ? $fordelingselement->hentId() : '*',
        ]);

        $elementFeltsettElementer[] = $this->app->vis(RadioGroup::class, [
            'fieldLabel' => 'Fordelingsmåte',
            'labelWidth' => 150,
            'allowBlank' => false,
            'blankText' => 'Fordelingsmåte må oppgis',
            'name' => 'element[' . $elementId . '][fordelingsmåte]',
            'itemId' => $fordelingselement ? ('fordelingsmåte-' . $fordelingselement->hentId()) : null,
            'value' => $fordelingselement ? $fordelingselement->hentFordelingsmåte() : null,
            'dataValues' => (object)[
                Fordelingsnøkkel\Element::FORDELINGSMÅTE_FAST => 'Manuell beregning eller fast beløp (f.eks. kr. 100)',
                Fordelingsnøkkel\Element::FORDELINGSMÅTE_PROSENT => 'Fast prosentsats (f.eks. 50%)',
                Fordelingsnøkkel\Element::FORDELINGSMÅTE_ANDELER => 'Fordeling i like deler (f.eks. 1 del hver)',
            ],
            'listeners' => (object)[
                'change' => new JsFunction(
                    "
                        let fastbeløp = radiogroup.getChecked()[0].getSubmitValue() === 'Fastbeløp';
                        let prosentvis = radiogroup.getChecked()[0].getSubmitValue() === 'Prosentvis';
                        let andeler = radiogroup.getChecked()[0].getSubmitValue() === 'Andeler';
                        radiogroup.up().getComponent('fastbeløp').setVisible(fastbeløp).setDisabled(!fastbeløp);
                        radiogroup.up().getComponent('prosentsats').setVisible(prosentvis).setDisabled(!prosentvis);
                        radiogroup.up().getComponent('andeler').setVisible(andeler).setDisabled(!andeler);
                    ",
                    ['radiogroup', 'newValue', 'oldValue', 'eOpts']
                )
            ]
        ]);

        $elementFeltsettElementer[] = $this->app->vis(Text::class, [
            'name' => 'element[' . $elementId . '][forklaring]',
            'itemId' => 'forklaring',
            'value' => $fordelingselement ? $fordelingselement->hentForklaring() : '',
            'fieldLabel' => 'Merknad/forklaring',
            'labelWidth' => 150,
        ]);

        $elementFeltsettElementer[] = $this->app->vis(Checkbox::class, [
            'name' => 'element[' . $elementId . '][følger_leieobjekt]',
            'itemId' => 'følger_leieobjekt',
            'value' => !$fordelingselement || $fordelingselement->hentFølgerLeieobjekt(),
            'fieldLabel' => 'Følger leieobjektet',
            'labelWidth' => 150,
            'boxLabel' => 'Andelen følger leieobjektet, og overføres automatisk til nye beboere',
            'listeners' => (object)[
                'change' => new JsFunction(
                    "
                    checkbox.up().getComponent('leieobjekt').setVisible(newValue).allowBlank = !newValue;
                    checkbox.up().getComponent('leieforhold').setVisible(!newValue).allowBlank = newValue;
                    checkbox.up().getComponent(newValue ? 'leieobjekt' : 'leieforhold').focus();
                    ",
                    ['checkbox', 'newValue', 'oldValue', 'eOpts']
                )
            ]
        ]);

        /** @var Leieobjekt $leieobjektFelt */
        $leieobjektFelt = $this->app->vis(Leieobjekt::class, [
            'name' => 'element[' . $elementId . '][leieobjekt]',
            'itemId' => 'leieobjekt',
            'value' => $fordelingselement && $fordelingselement->leieobjekt ? $fordelingselement->leieobjekt->id : (
            $fordelingselement && !$fordelingselement->følgerLeieobjekt && $fordelingselement->leieforhold && $fordelingselement->leieforhold->leieobjekt ? $fordelingselement->leieforhold->leieobjekt->id : null
            ),
            'fieldLabel' => 'Leieobjekt',
            'labelWidth' => 150,
        ]);
        if ($fordelingselement && !$fordelingselement->hentFølgerLeieobjekt()) {
            $leieobjektFelt->setConfig('hidden', true);
        }
        if ($fordelingselement && !$fordelingselement->leieobjekt) {
            $leieobjektFelt->getConfig('store')->setConfig('autoLoad', false);
        }
        $elementFeltsettElementer[] = $leieobjektFelt;

        /** @var Leieforhold $leieforholdFelt */
        $leieforholdFelt = $this->app->vis(Leieforhold::class, [
            'name' => 'element[' . $elementId . '][leieforhold]',
            'itemId' => 'leieforhold',
            'value' => $fordelingselement && $fordelingselement->leieforhold ? $fordelingselement->leieforhold->id : null,
            'fieldLabel' => 'Gjelder kun leieforhold',
            'labelWidth' => 150,
            'boxLabel' => 'Angi leieforhold',
        ]);
        if (!$fordelingselement || $fordelingselement->hentFølgerLeieobjekt()) {
            $leieforholdFelt->setConfig('hidden', true);
        }
        if (!$fordelingselement || !$fordelingselement->leieforhold) {
            $leieforholdFelt->getConfig('store')->setConfig('autoLoad', false);
        }
        $elementFeltsettElementer[] = $leieforholdFelt;


        $elementFeltsettElementer[] = $this->app->vis(Number::class, [
            'name' => 'element[' . $elementId . '][fastbeløp]',
            'itemId' => 'fastbeløp',
            'value' => $fordelingselement ? $fordelingselement->hentFastbeløp() : null,
            'fieldLabel' => 'Angi beløp',
            'labelWidth' => 150,
            'allowBlank' => true,
            'decimalPrecision' => 0,
            'minValue' => 0,
            'disabled' => !$fordelingselement || $fordelingselement->hentFordelingsmåte() != Fordelingsnøkkel\Element::FORDELINGSMÅTE_FAST,
            'hidden' => !$fordelingselement || $fordelingselement->hentFordelingsmåte() != Fordelingsnøkkel\Element::FORDELINGSMÅTE_FAST,
        ]);

        $elementFeltsettElementer[] = $this->app->vis(Number::class, [
            'name' => 'element[' . $elementId . '][prosentsats]',
            'itemId' => 'prosentsats',
            'value' => $fordelingselement ? ($fordelingselement->hentProsentsats() * 100) : null,
            'fieldLabel' => 'Angi sats i prosent',
            'labelWidth' => 150,
            'decimalPrecision' => 1,
            'disabled' => !$fordelingselement || $fordelingselement->hentFordelingsmåte() != Fordelingsnøkkel\Element::FORDELINGSMÅTE_PROSENT,
            'hidden' => !$fordelingselement || $fordelingselement->hentFordelingsmåte() != Fordelingsnøkkel\Element::FORDELINGSMÅTE_PROSENT,
        ]);

        $elementFeltsettElementer[] = $this->app->vis(Number::class, [
            'name' => 'element[' . $elementId . '][andeler]',
            'itemId' => 'andeler',
            'value' => $fordelingselement && $fordelingselement->hentAndeler() ? $fordelingselement->hentAndeler() : 1,
            'fieldLabel' => 'Antall deler',
            'labelWidth' => 150,
            'allowDecimals' => false,
            'disabled' => !$fordelingselement || $fordelingselement->hentFordelingsmåte() != Fordelingsnøkkel\Element::FORDELINGSMÅTE_ANDELER,
            'hidden' => !$fordelingselement || $fordelingselement->hentFordelingsmåte() != Fordelingsnøkkel\Element::FORDELINGSMÅTE_ANDELER,
        ]);

        $elementFeltsettElementer[] = $this->app->vis(Button::class, [
            'text' => 'Slett dette elementet',
            'handler' => new JsFunction('let fieldset = button.up();fieldset.up().remove(fieldset);', ['button', 'event'])
        ]);

        /** @var FieldSet $elementFeltsett */
        $elementFeltsett = $this->app->vis(FieldSet::class, [
            'title' => $fordelingselement ? 'Eksisterende element' : 'Nytt element',
            'layout' => 'form',
            'defaults' => (object)[
                'labelWidth' => '500px'
            ],
            'items' => $elementFeltsettElementer
        ]);
        return $elementFeltsett;
    }
}