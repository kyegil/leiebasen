<?php

namespace Kyegil\Leiebasen\Visning\drift\js;


use Exception;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Skript-fil i drift
 *
 * Ressurser:
 *
 * Tilgjengelige variabler:
 *
 * @package Kyegil\Leiebasen\Visning\drift\js
 */
class _MalListe extends Drift
{
    protected $template = 'drift/js/_MalListe.js';

    /**
     * @return _MalListe
     * @throws Exception
     */
    protected function forberedData()
    {
        $this->definerData([
            'skript' => '',
        ]);

        return parent::forberedData();
    }
}