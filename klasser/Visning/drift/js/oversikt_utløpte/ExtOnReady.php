<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\js\oversikt_utløpte;


use Kyegil\Leiebasen\Visning\Drift;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $bruker \Kyegil\Leiebasen\Modell\Person
 *  Mulige variabler:
 *      $brukerId
 *      $tilbakeKnapp
 * @package Kyegil\Leiebasen\Visning\drift\js\shared\body\main
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/oversikt_utløpte/ExtOnReady.js';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'bruker') {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $bruker = $this->hentRessurs('bruker');
        $brukerId = strval($bruker);

        $this->definerData([
            'meny' => '',
            'tilbakeUrl' => $this->app->returi->get()->url
        ]);
        return parent::forberedData();
    }
}