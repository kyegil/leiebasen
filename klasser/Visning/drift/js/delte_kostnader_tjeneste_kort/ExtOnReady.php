<?php

namespace Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_tjeneste_kort;


use Exception;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\JsonStore;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $tjeneste \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste
 *      $leiebasenJsObjektEgenskaper stdClass
 *  Mulige variabler:
 *      $tjenesteId
 *      $tjenesteBeskrivelse
 *      $tab
 *      $forbruksdiagramAkseFelter
 *      $forbruksdiagramModellFelter
 *      $forbruksdiagramStore
 *      $tilbakeKnapp
 * @package Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_tjeneste_kort
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/delte_kostnader_tjeneste_kort/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['tjeneste'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        if ($attributt == 'leiebasenJsObjektEgenskaper') {
            return $this->settLeiebasenJsObjektEgenskaper($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param stdClass $leiebasenJsObjektEgenskaper
     * @return $this
     */
    public function settLeiebasenJsObjektEgenskaper(stdClass $leiebasenJsObjektEgenskaper): Drift
    {
        return $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var stdClass $leiebasenJsObjektEgenskaper */
        $leiebasenJsObjektEgenskaper = $this->hentRessurs('leiebasenJsObjektEgenskaper');
        if (!$leiebasenJsObjektEgenskaper) {
            $leiebasenJsObjektEgenskaper = new stdClass();
            $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
        }
        /** @var Tjeneste|null $tjeneste */
        $tjeneste = $this->hentRessurs('tjeneste');

        $forbruksdiagramModellFelter = $this->hentForbruksdiagramModellFelter($tjeneste);
        $forbruksdiagramAkseFelter = $this->hentForbruksdiagramAkseFelter($tjeneste);

        $this->definerData([
            'meny' => $this->app->vis(Meny::class, []),
            'tjenesteId' => strval($tjeneste),
            'tjenesteBeskrivelse' => $tjeneste->hentBeskrivelse(),
            'tab' => '',
            'forbruksdiagramAkseFelter' => json_encode($forbruksdiagramAkseFelter),
            'forbruksdiagramModellFelter' => json_encode($forbruksdiagramModellFelter),
            'forbruksdiagramStore' => $this->app->vis(JsonStore::class, [
                'model' => 'leiebasen.ForbruksdiagramModell',
                'url' => "/drift/index.php?oppslag=delte_kostnader_tjeneste_kort&id={$tjeneste->hentId()}&oppdrag=hent_data&data=forbruksstatistikk"
            ]),
            'forbruksdiagrammodellFields' => $this->hentForbruksdiagrammodellFields($tjeneste),
            'tilbakeUrl' => $this->app->returi->get()->url,
        ]);
        return parent::forberedData();
    }

    /**
     * @param Tjeneste|null $tjeneste
     * @return string
     * @throws Exception
     */
    private function hentForbruksdiagrammodellFields(?Tjeneste $tjeneste): string
    {
        $fields = [(object)[
            'name' => 'dato',
            'type' => 'date',
            'dateFormat' => 'Y-m-d',
        ]];
        if ($tjeneste) {
            foreach($tjeneste->hentRegninger() as $regning) {
                if ($regning->hentFakturanummer()) {
                    $fields[] = (object)[
                        'name' => $regning->hentFakturanummer(),
                        'type' => 'float',
                    ];
                }
            }
        }
        return json_encode($fields);
    }

    /**
     * @param Tjeneste|null $tjeneste
     * @return string[]
     * @throws Exception
     */
    protected function hentForbruksdiagramAkseFelter(?Tjeneste $tjeneste): array
    {
        $resultat = [];
        if ($tjeneste) {
            /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad $regning */
            foreach($tjeneste->hentRegninger() as $regning) {
                $resultat[] = strval($regning->hentFakturanummer());
            }
        }
        return $resultat;
    }

    /**
     * @param Tjeneste|null $tjeneste
     * @return \stdClass[]
     * @throws Exception
     */
    protected function hentForbruksdiagramModellFelter(?Tjeneste $tjeneste): array
    {
        $resultat = [];
        if ($tjeneste) {
            /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad $regning */
            foreach($tjeneste->hentRegninger() as $regning) {
                $resultat[] = (object)[
                    'name' => strval($regning->hentFakturanummer()),
                    'type' => 'float'
                ];
            }
        }
        $resultat[] = (object)[
            'name' => 'dato',
            'type' => 'date',
            'dateFormat' => 'Y-m-d'
        ];
        return $resultat;
    }
}