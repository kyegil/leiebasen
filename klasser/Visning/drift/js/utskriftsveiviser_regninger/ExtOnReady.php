<?php

namespace Kyegil\Leiebasen\Visning\drift\js\utskriftsveiviser_regninger;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\Store;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $bruker \Kyegil\Leiebasen\Modell\Person
 *      $kravsett \Kyegil\Leiebasen\Modell\Leieforhold\Kravsett
 *  Mulige variabler:
 *      $brukerId
 *      $datasett json-formatert datasett
 *      $adskilt bool
 *      $tilbakeKnapp
 *      $inkLeieforhold json-array med leieforhold-nummer
 *      $eksLeieforhold json-array med leieforhold-nummer
 *      $utskriftAlleredeIgangsatt bool
 * @package Kyegil\Leiebasen\Visning\drift\js\utskriftsveiviser_regninger
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/utskriftsveiviser_regninger/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): ExtOnReady
    {
        if ($attributt == 'kravsett') {
            return $this->settKravsett($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Kravsett $kravsett
     * @return $this
     */
    private function settKravsett(Kravsett $kravsett): ExtOnReady
    {
        return $this->settRessurs('kravsett', $kravsett);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): ExtOnReady
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Kravsett|null $kravsett */
        $kravsett = $this->hentRessurs('kravsett');

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }

        $this->definerData([
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'adskilt' => false,
            'datasett' => $this->app->vis(Store::class, [
                'model' => 'leiebasen.Krav',
                'data' => $this->hentKravsettData($kravsett)
            ]),
            'tilbakeUrl' => $this->app->returi->get()->url,
            'inkLeieforhold' => '[]',
            'eksLeieforhold' => '[]',
            'utskriftAlleredeIgangsatt' => (bool)$this->app->hentValg('utskriftsforsøk'),
        ]);

        return parent::forberedData();
    }

    /**
     * @param Kravsett|null $kravsett
     * @return array
     * @throws Exception
     */
    private function hentKravsettData(?Kravsett $kravsett): array
    {
        $resultat = [];
        if ($kravsett) {
            foreach ($kravsett as $krav) {
                $leieforhold = $krav->leieforhold;
                $resultat[] = (object)[
                    'id' => $krav->hentId(),
                    'tekst' => $krav->hentTekst(),
                    'type' => $krav->hentType(),
                    'dato' => $krav->hentDato()->format('Y-m-d'),
                    'leieforhold_id' => $leieforhold->hentId(),
                    'leieforhold_beskrivelse' => $leieforhold->hentNavn(),
                    'regningsformat' => $leieforhold->hentGiroformat() ?? '',
                    'forsinket_efaktura' => $leieforhold->hentFboOgEfaktura()
                ];
            }
        }
        return $resultat;
    }
}