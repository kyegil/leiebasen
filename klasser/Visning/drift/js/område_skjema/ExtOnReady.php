<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\js\område_skjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\HtmlEditor;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Text;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FlexiCheckboxGroup;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel as FormPanel;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $område \Kyegil\Leiebasen\Modell\Område
 *  Mulige variabler:
 *      $områdeId
 *      $meny
 *      $skjema
 *      $formActionUrl
 * @package Kyegil\Leiebasen\Visning\drift\js\område_skjema
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/område_skjema/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['område'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Område|null $område */
        $område = $this->hentRessurs('område');

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }
        $this->definerData([
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'skjema' => $this->app->vis(FormPanel::class, [
                'id' => 'skjema',
                'configObjectOnly' => false,
                'title' => $område ? $område->navn : 'Opprett nytt område',
                'enabledSaveButton' => true,
                'formActionUrl' => '/drift/index.php?oppslag=område_skjema&id=' . ($område && $område->hentId() ? $område->hentId() : '*') . '&oppdrag=taimotskjema',
                'returnUrl' => $this->app->returi->get(),
                'formSourceUrl' => '/drift/index.php?oppslag=område_skjema&id=' . ($område && $område->hentId() ? $område->hentId() : '*') . '&oppdrag=hentdata',
                'items' => [
                    $this->app->vis(Text::class, [
                        'name' => 'navn',
                        'fieldLabel' => 'Navn',
                        'value' => $område ? $område->navn : null
                    ]),
                    $this->app->vis(HtmlEditor::class, [
                        'name' => 'beskrivelse',
                        'fieldLabel' => 'Beskrivelse',
                        'height' => 100,
                        'value' => $område ? $område->beskrivelse : null
                    ]),
                    $this->app->vis(FlexiCheckboxGroup::class, [
                        'name' => 'bygninger',
                        'fieldLabel' => 'Bygninger som er med i området',
                        'remote' => true,
                        'url' => '/drift/index.php?oppslag=område_skjema&id=' . ($område && $område->hentId() ? $område->hentId() : '*') . '&oppdrag=hent_data&data=bygninger',
                        'value' => $this->hentBygninger($område)
                    ]),
                    $this->app->vis(FlexiCheckboxGroup::class, [
                        'name' => 'leieobjekter',
                        'fieldLabel' => 'Andre individuelle leieobjekter som er med',
                        'remote' => true,
                        'url' => '/drift/index.php?oppslag=område_skjema&id=' . ($område && $område->hentId() ? $område->hentId() : '*') . '&oppdrag=hent_data&data=leieobjekter',
                        'value' => $this->hentLeieobjekter($område)
                    ]),
                ]
            ]),
            'formActionUrl' => '/drift/index.php?oppslag=område_skjema&id=' . ($område && $område->hentId() ? $område->hentId() : '*') . '&oppdrag=ta_i_mot_skjema'

        ]);

        return parent::forberedData();
    }

    /**
     * Returnerer et stdClass objekt,
     * med bygningens id som egenskapsnavn og beskrivelse som verdi
     *
     * @param Område|null $område
     * @return stdClass|null
     * @throws Exception
     */
    protected function hentBygninger(Område $område = null): ?stdClass
    {
        $resultat = null;
        if ($område) {
            $resultat = new stdClass();
            /** @var Bygning $bygning */
            foreach ($område->hentBygninger() as $bygning) {
                $resultat->{$bygning->hentId()} = "{$bygning->hentNavn()} ({$bygning->hentKode()})";
            }
        }
        return $resultat;
    }

    /**
     * Returnerer et stdClass objekt,
     * med bygningens id som egenskapsnavn og beskrivelse som verdi
     *
     * @param Område|null $område
     * @return stdClass
     * @throws Exception
     */
    protected function hentLeieobjekter(Område $område = null): ?stdClass
    {
        $resultat = null;
        if ($område) {
            $resultat = new stdClass();
            /** @var Leieobjekt $leieobjekt */
            foreach ($område->hentLeieobjekter() as $leieobjekt) {
                $resultat->{$leieobjekt->hentId()} = $leieobjekt->hentId() . ' | ' . $leieobjekt->hentBeskrivelse();
            }
        }
        return $resultat;
    }
}