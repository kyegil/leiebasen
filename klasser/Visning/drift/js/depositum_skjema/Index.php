<?php

namespace Kyegil\Leiebasen\Visning\drift\js\depositum_skjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Depositum;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Skript-fil i drift
 *
 * Ressurser:
 * * leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 * * depositum \Kyegil\Leiebasen\Modell\Leieforhold\Depositum
 *
 * Tilgjengelige variabler:
 * * $leieforholdId
 * * $depositumId
 * @package Kyegil\Leiebasen\Visning\drift\js\depositum_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/js/depositum_skjema/Index.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Index
    {
        if(in_array($attributt,['leieforhold', 'depositum'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Depositum|null $depositum */
        $depositum = $this->hentRessurs('depositum');
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        $datasett = [
            'depositumId' => $depositum ? $depositum->hentId() : '',
            'leieforholdId' => $leieforhold ? $leieforhold->hentId() : '',
            'skript' => '',
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}