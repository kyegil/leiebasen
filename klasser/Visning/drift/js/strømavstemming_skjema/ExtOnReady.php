<?php

namespace Kyegil\Leiebasen\Visning\drift\js\strømavstemming_skjema;


use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Date;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Number;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $bruker \Kyegil\Leiebasen\Modell\Person
 *  Mulige variabler:
 *      $brukerId
 *      $tilbakeKnapp
 *      $tittel
 *      $tilEditor
 *      $nySatsEditor
 *      $avregningEditor
 * @package Kyegil\Leiebasen\Visning\drift\js\strømavstemming_skjema
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/strømavstemming_skjema/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'bruker') {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }
        $this->definerData([
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'tilbakeUrl' => $this->app->returi->get()->url,
            'tittel' => 'Avstemming av faste fellesstrøm-krav',
            'tilEditor' => $this->app->vis(Date::class),
            'nySatsEditor' => $this->app->vis(Number::class),
            'avregningEditor' => $this->app->vis(Number::class)
        ]);
        return parent::forberedData();
    }
}