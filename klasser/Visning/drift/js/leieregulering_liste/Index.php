<?php

namespace Kyegil\Leiebasen\Visning\drift\js\leieregulering_liste;


use Exception;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Skript-fil i drift
 *
 *  Ressurser:
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\drift\js\leieregulering_liste
 */
class Index extends Drift
{
    protected $template = 'drift/js/leieregulering_liste/Index.js';

    /**
     * @return Index
     * @throws Exception
     */
    protected function forberedData()
    {
        $this->definerData([
            'skript' => '',
        ]);

        return parent::forberedData();
    }
}