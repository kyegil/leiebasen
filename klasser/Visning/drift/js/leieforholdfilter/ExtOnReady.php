<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\js\leieforholdfilter;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\extjs4\Ext\form\flexi_checkbox_groups\Bygninger as BygningsVelger;
use Kyegil\Leiebasen\Visning\drift\extjs4\Ext\form\flexi_checkbox_groups\Leieforhold as LeieforholdVelger;
use Kyegil\Leiebasen\Visning\drift\extjs4\Ext\form\flexi_checkbox_groups\Leieobjekter as LeieobjektVelger;
use Kyegil\Leiebasen\Visning\drift\extjs4\Ext\form\flexi_checkbox_groups\Områder as OmrådeVelger;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\CheckboxGroup;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Checkbox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Date;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Hidden;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Number;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel as FormPanel;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $bruker \Kyegil\Leiebasen\Modell\Person
 *  Mulige variabler:
 *      $brukerId
 *      $inkludertFilter
 *      $ekskludertFilter
 *      $leieobjektFilter
 *      $bygningsFilter
 *      $områdeFilter
 *      $leiebeløpMinFilter
 *      $fradatoMinFilter
 *      $fradatoMaksFilter
 *      $sistFornyetMinFilter
 *      $sistFornyetMaksFilter
 *      $tildatoMinFilter
 *      $tildatoMaksFilter
 *      $tilbakeKnapp
 *      $sisteLeiejusteringMinFilter
 *      $sisteLeiejusteringMinFilter
 *      $oppsigelighetsFilter
 *      $kunAktiveFilter
 *      $resultatSkjema
 * @package Kyegil\Leiebasen\Visning\drift\js\leieforholdfilter
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/leieforholdfilter/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'bruker') {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): ExtOnReady
    {
        $bruker = $this->hentRessurs('bruker');
        $brukerId = strval($bruker);

        $this->definerData([
            'meny' => '',
            'inkludertFilter' => $this->app->vis(LeieforholdVelger::class, [
                'submitAsArray' => false,
                'configObjectOnly' => false,
                'itemId' => 'inkludertFilter',
                'emptyText' => 'Velg leieforhold å inkludere',
                'fieldLabel' => 'Inkluderte leieforhold',
                'name' => 'filter[inkludert]'
            ]),
            'ekskludertFilter' => $this->app->vis(LeieforholdVelger::class, [
                'submitAsArray' => false,
                'configObjectOnly' => false,
                'itemId' => 'ekskludertFilter',
                'emptyText' => 'Velg leieforhold å ekskludere',
                'fieldLabel' => 'Ekskluderte leieforhold',
                'name' => 'filter[ekskludert]'
            ]),
            'leieobjektFilter' => $this->app->vis(LeieobjektVelger::class, [
                'configObjectOnly' => false,
                'itemId' => 'leieobjektFilter',
                'emptyText' => 'Velg leieobjekt',
                'fieldLabel' => 'Leieobjekter',
                'name' => 'filter[leieobjekt]'
            ]),
            'bygningsFilter' => $this->app->vis(BygningsVelger::class, [
                'configObjectOnly' => false,
                'itemId' => 'bygningsFilter',
                'emptyText' => 'Velg bygning',
                'fieldLabel' => 'Bygning',
                'name' => 'filter[bygning]'
            ]),
            'områdeFilter' => $this->app->vis(OmrådeVelger::class, [
                'configObjectOnly' => false,
                'itemId' => 'områdeFilter',
                'emptyText' => 'Velg område',
                'fieldLabel' => 'Område',
                'name' => 'filter[område]'
            ]),
            'leiebeløpMinFilter' => $this->app->vis(Number::class, [
                'configObjectOnly' => false,
                'labelWidth' => 150,
                'fieldLabel' => 'Leiebeløp per måned fra',
                'name' => 'filter[beløpMin]',
                'minValue' => 0,
                'width' => null
            ]),
            'leiebeløpMaksFilter' => $this->app->vis(Number::class, [
                'configObjectOnly' => false,
                'fieldLabel' => 'til',
                'name' => 'filter[beløpMaks]',
                'minValue' => 0,
                'width' => null
            ]),
            'fradatoMinFilter' => $this->app->vis(Date::class, [
                'configObjectOnly' => false,
                'labelWidth' => 150,
                'fieldLabel' => 'Påbegynt tidligst',
                'name' => 'filter[fradatoMin]'
            ]),
            'fradatoMaksFilter' => $this->app->vis(Date::class, [
                'configObjectOnly' => false,
                'fieldLabel' => 'senest',
                'name' => 'filter[fradatoMaks]'
            ]),
            'sistFornyetMinFilter' => $this->app->vis(Date::class, [
                'configObjectOnly' => false,
                'labelWidth' => 150,
                'fieldLabel' => 'Fornyet etter',
                'name' => 'filter[fornyetMin]'
            ]),
            'sistFornyetMaksFilter' => $this->app->vis(Date::class, [
                'configObjectOnly' => false,
                'fieldLabel' => 'før',
                'name' => 'filter[fornyetMaks]'
            ]),
            'tildatoMinFilter' => $this->app->vis(Date::class, [
                'configObjectOnly' => false,
                'labelWidth' => 150,
                'fieldLabel' => 'Utløper tidligst',
                'name' => 'filter[tildatoMin]'
            ]),
            'tildatoMaksFilter' => $this->app->vis(Date::class, [
                'configObjectOnly' => false,
                'fieldLabel' => 'senest',
                'name' => 'filter[tildatoMaks]'
            ]),
            'sisteLeiejusteringMinFilter' => $this->app->vis(Date::class, [
                'configObjectOnly' => false,
                'labelWidth' => 150,
                'fieldLabel' => 'Siste leiejustering etter',
                'name' => 'filter[leiejusteringMin]'
            ]),
            'sisteLeiejusteringMaksFilter' => $this->app->vis(Date::class, [
                'configObjectOnly' => false,
                'fieldLabel' => 'før',
                'name' => 'filter[leiejusteringMaks]'
            ]),
            'oppsigelighetsFilter' => $this->app->vis(CheckboxGroup::class, [
                'configObjectOnly' => false,
                'name' => 'filter[tidsbegrensning]',
                'fieldLabel' => 'Midlertidige avtaler',
                'dataValues' => (object)['tidsbegrenset' => 'Tidsbegrenset', 'oppsigelig' => 'Ikke tidsbegrenset']
            ]),
            'kunAktiveFilter' => $this->app->vis(Checkbox::class, [
                'configObjectOnly' => false,
                'hideLabel' => true,
                'value' => true,
                'name' => 'filter[aktive]',
                'boxLabel' => 'Kun aktive leieforhold',
                'listeners' => (object)['change' => new JsCustom('leiebasen.filtrerOgLast')]
            ]),
            'resultatSkjema' => $this->app->vis(FormPanel::class, [
                'configObjectOnly' => false,
                'renderTo' => null,
                'standardSubmit' => true,
                'url' => (string)$this->app->returi->get(),
                'items' => [
                    $this->app->vis(Hidden::class, [
                        'itemId' => 'leieforhold',
                        'name' => 'leieforhold'
                    ])
                ]
            ]),
            'tilbakeUrl' => $this->app->returi->get()->url
        ]);
        return parent::forberedData();
    }
}