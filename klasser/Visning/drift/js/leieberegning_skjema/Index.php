<?php

namespace Kyegil\Leiebasen\Visning\drift\js\leieberegning_skjema;


use Exception;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\html\leieberegning_skjema\RegelMal;

/**
 * Skript-fil i drift
 *
 *  Ressurser:
 * * leieberegning \Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning
 * Tilgjengelige variabler:
 * * $leieberegningId
 * * $spesialregelMal
 * * $spesialregler
 *
 * @package Kyegil\Leiebasen\Visning\drift\js\leieberegning_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/js/leieberegning_skjema/Index.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['leieberegning'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning|null $leieberegning */
        $leieberegning = $this->hentRessurs('leieberegning');

        $datasett = [
            'leieberegningId' => $leieberegning ? $leieberegning->hentId() : '',
            'spesialregelMal' => json_encode((string)$this->app->vis(RegelMal::class)),
            'spesialregler' => json_encode($leieberegning ? $leieberegning->hentSpesialregler() : [])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}