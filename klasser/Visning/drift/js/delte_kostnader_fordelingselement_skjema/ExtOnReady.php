<?php

namespace Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_fordelingselement_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel\Element;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\container\Container;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\ComboBox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Display;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Hidden;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Number;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\TextArea;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel as FormPanel;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\RadioGroup;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $element \Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel\Element|null
 *      $fordelingsnøkkel \Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel|null
 *      $leiebasenJsObjektEgenskaper stdClass
 *  Mulige variabler:
 *      $elementId
 *      $fordelingsnøkkelId
 *      $meny
 *      $initLeiebasenJsObjektEgenskaper
 *      $skjema
 *      $formActionUrl
 * @package Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_fordelingselement_skjema
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/delte_kostnader_fordelingselement_skjema/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['element','fordelingsnøkkel'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        if ($attributt == 'leiebasenJsObjektEgenskaper') {
            return $this->settLeiebasenJsObjektEgenskaper($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param stdClass $leiebasenJsObjektEgenskaper
     * @return $this
     */
    public function settLeiebasenJsObjektEgenskaper(stdClass $leiebasenJsObjektEgenskaper): Drift
    {
        return $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var stdClass $leiebasenJsObjektEgenskaper */
        $leiebasenJsObjektEgenskaper = $this->hentRessurs('leiebasenJsObjektEgenskaper');
        if (!$leiebasenJsObjektEgenskaper) {
            $leiebasenJsObjektEgenskaper = new stdClass();
            $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
        }
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Element|null $element */
        $element = $this->hentRessurs('element');
        /** @var Fordelingsnøkkel|null $fordelingsnøkkel */
        $fordelingsnøkkel
            = $element && $element->fordelingsnøkkel
            ? $element->fordelingsnøkkel
            : $this->hentRessurs('fordelingsnøkkel'
        );
        $elementId = $element && $element->hentId() ? $element->hentId() : '*';
        $fordelingsnøkkelId = $fordelingsnøkkel ? $fordelingsnøkkel->hentId() : null;
        /** @var Tjeneste|null $tjeneste */
        $tjeneste = $fordelingsnøkkel ? $fordelingsnøkkel->hentTjeneste() : null;

        $leiebasenJsObjektEgenskaper->skjema = (object)[
            'felter' => (object)[]
        ];

        $leiebasenJsObjektEgenskaper->skjema->felter->element
        = $this->app->vis(Display::class, [
            'name' => 'element',
            'fieldLabel' => 'Element nr.',
            'value' => $elementId
        ]);

        $leiebasenJsObjektEgenskaper->skjema->felter->tjeneste
        = $this->app->vis(Display::class, [
            'fieldLabel' => 'Fordeling av',
            'value' => $tjeneste ? ($tjeneste->hentAvtalereferanse() . ' | ' . $tjeneste->hentNavn()) : ''
        ]);

        $leiebasenJsObjektEgenskaper->skjema->felter->fordelingsnøkkel
        = $this->app->vis(Hidden::class, [
            'name' => 'fordelingsnøkkel',
            'value' => $fordelingsnøkkelId
        ]);

        $leiebasenJsObjektEgenskaper->skjema->felter->fordelingsmåte
            = $this->app->vis(RadioGroup::class, [
            'name' => 'fordelingsmåte',
            'dataValues' => (object)[
                'Fastbeløp' => 'Beløpet beregnes manuelt og kreves inn for hver enkelt faktura',
                'Prosentvis' => 'Prosentandel beregnes av hver faktura (Beregnes etter fradrag av manuelle beløp)',
                'Andeler' => 'Brøk-andeler av hver faktura, f.eks. 1 av x like store deler (Evt. andre beregninger trekkes fra først)'
            ],
            'value' => $element ? $element->fordelingsmåte : 'Andeler'
        ]);

        $leiebasenJsObjektEgenskaper->skjema->felter->følgerLeieobjekt
            = $this->app->vis(RadioGroup::class, [
            'name' => 'følger_leieobjekt',
            'dataValues' => (object)[
                '1' => 'Dette elementet følger leieobjektet og overføres automatisk til nye leietakere',
                '0' => 'Dette elementet gjelder kun angitt leieforhold',
            ],
            'value' => !$element || !$element->følgerLeieobjekt ? '0': '1'
        ]);

        /** @var ComboBox $leieobjektFelt */
        $leieobjektFelt = $this->app->vis(ComboBox::class, [
            'name' => 'leieobjekt',
            'fieldLabel' => 'Leieobjekt',
            'forceSelection' => true,
            'allowBlank' => false,
            'remote'=> true,
            'flex' => 1,
            'ignoredConfigs' => ['width'],
            'autoLoad' => false,
            'url' => '/drift/index.php?oppslag=delte_kostnader_fordelingselement_skjema&fordelingsnøkkel=' . $fordelingsnøkkelId . '&id=' . $elementId . '&oppdrag=hent_data&data=leieobjekter',
            'value' => $element && $element->leieobjekt ? strval($element->leieobjekt->id) : null
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->leieobjekt = $leieobjektFelt;

        /** @var ComboBox $leieforholdFelt */
        $leieforholdFelt = $this->app->vis(ComboBox::class, [
            'name' => 'leieforhold',
            'fieldLabel' => 'Leieforhold',
            'forceSelection' => true,
            'allowBlank' => false,
            'remote'=> true,
            'flex' => 1,
            'ignoredConfigs' => ['width'],
            'url' => '/drift/index.php?oppslag=delte_kostnader_fordelingselement_skjema&fordelingsnøkkel=' . $fordelingsnøkkelId . '&id=' . $elementId . '&oppdrag=hent_data&data=leieforhold',
            'value' => $element && $element->leieforhold ? strval($element->leieforhold->id) : null
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->leieforhold = $leieforholdFelt;

        $leiebasenJsObjektEgenskaper->skjema->felter->fastbeløp
            = $this->app->vis(Number::class, [
            'name' => 'fastbeløp',
            'fieldLabel' => 'Foreslått beløp for neste fordeling',
            'allowDecimals' => true,
            'decimalPrecision' => 1,
            'flex' => 1,
            'ignoredConfigs' => ['width'],
            'value' => $element ? $element->hentFastbeløp() : null
        ]);

        $leiebasenJsObjektEgenskaper->skjema->felter->prosentsats
            = $this->app->vis(Number::class, [
            'name' => 'prosentsats',
            'fieldLabel' => 'Prosentsats',
            'allowDecimals' => true,
            'decimalPrecision' => 1,
            'flex' => 1,
            'ignoredConfigs' => ['width'],
            'value' => $element ? $element->hentProsentsats() * 100 : null
        ]);

        $leiebasenJsObjektEgenskaper->skjema->felter->andeler
            = $this->app->vis(Number::class, [
            'name' => 'andeler',
            'fieldLabel' => 'Andeler',
            'allowDecimals' => false,
            'flex' => 1,
            'ignoredConfigs' => ['width'],
            'value' => $element ? $element->hentAndeler() : null
        ]);

        $leiebasenJsObjektEgenskaper->skjema->felter->forklaring
            = $this->app->vis(TextArea::class, [
            'name' => 'forklaring',
            'fieldLabel' => 'Forklaring/utdyping av dette fordelingsnøkkel-elementet',
            'value' => $element ? $element->hentForklaring() : null
        ]);

        $this->definerData([
            'elementId' => $elementId,
            'fordelingsnøkkelId' => $fordelingsnøkkelId,
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'initLeiebasenJsObjektEgenskaper' => $this->gjengiLeiebasenJsObjektEgenskaper(),
            'skjema' => $this->app->vis(FormPanel::class, [
                'id' => 'skjema',
                'configObjectOnly' => false,
                'title' => 'Fordelingsnøkkel-element',
                'enabledSaveButton' => true,
                'formActionUrl' => '/drift/index.php?oppslag=delte_kostnader_fordelingselement_skjema&fordelingsnøkkel=' . $fordelingsnøkkelId . '&id=' . $elementId . '&oppdrag=ta_imot_skjema',
                'returnUrl' => $this->app->returi->get(),
                'formSourceUrl' => '/drift/index.php?oppslag=delte_kostnader_fordelingselement_skjema&fordelingsnøkkel=' . $fordelingsnøkkelId . '&id=' . $elementId . '&oppdrag=hent_data',
                'items' => [
                    new JsCustom('leiebasen.skjema.felter.element'),
                    new JsCustom('leiebasen.skjema.felter.tjeneste'),
                    new JsCustom('leiebasen.skjema.felter.fordelingsnøkkel'),
                    new JsCustom('leiebasen.skjema.felter.fordelingsmåte'),
                    $this->app->vis(Container::class, [
                        'layout' => (object)[
                            'type' => 'hbox',
                            'defaultMargins' => (object)[
                                'left' => 5,
                                'right' => 5,
                            ]
                        ],
                        'items' => [
                            new JsCustom('leiebasen.skjema.felter.fastbeløp'),
                            new JsCustom('leiebasen.skjema.felter.prosentsats'),
                            new JsCustom('leiebasen.skjema.felter.andeler'),
                        ]
                    ]),
                    new JsCustom('leiebasen.skjema.felter.følgerLeieobjekt'),
                    $this->app->vis(Container::class, [
                        'layout' => (object)[
                            'type' => 'hbox',
                            'defaultMargins' => (object)[
                                'left' => 5,
                                'right' => 5,
                            ]
                        ],
                        'items' => [
                            new JsCustom('leiebasen.skjema.felter.leieobjekt'),
                            new JsCustom('leiebasen.skjema.felter.leieforhold'),
                        ]
                    ]),
                    new JsCustom('leiebasen.skjema.felter.forklaring'),
                ]
            ]),
            'formActionUrl' => '/drift/index.php?oppslag=delte_kostnader_fordelingselement_skjema&fordelingsnøkkel=' . $fordelingsnøkkelId . '&id=' . $elementId . '&oppdrag=ta_imot_skjema',
        ]);

        return parent::forberedData();
    }
}