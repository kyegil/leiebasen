<?php

namespace Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_kostnad_kort;


use Exception;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $kostnad \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad
 *      $tjeneste \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste
 *      $leiebasenJsObjektEgenskaper stdClass
 *  Mulige variabler:
 *      $tjenesteId
 *      $tjenesteBeskrivelse
 *      $kostnadId
 *      $tjenesteBeskrivelse
 *      $tilbakeKnapp
 *      $fordelt (boolen)
 * @package Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_tjeneste_kort
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/delte_kostnader_kostnad_kort/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['tjeneste','kostnad'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        if ($attributt == 'leiebasenJsObjektEgenskaper') {
            return $this->settLeiebasenJsObjektEgenskaper($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param stdClass $leiebasenJsObjektEgenskaper
     * @return $this
     */
    public function settLeiebasenJsObjektEgenskaper(stdClass $leiebasenJsObjektEgenskaper): Drift
    {
        return $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var stdClass $leiebasenJsObjektEgenskaper */
        $leiebasenJsObjektEgenskaper = $this->hentRessurs('leiebasenJsObjektEgenskaper');
        if (!$leiebasenJsObjektEgenskaper) {
            $leiebasenJsObjektEgenskaper = new stdClass();
            $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
        }
        /** @var Tjeneste\Kostnad|null $kostnad */
        $kostnad = $this->hentRessurs('kostnad');
        /** @var Tjeneste|null $tjeneste */
        $tjeneste = $kostnad && $kostnad->tjeneste ? $kostnad->tjeneste : $this->hentRessurs('tjeneste');

        $this->definerData([
            'meny' => $this->app->vis(Meny::class, []),
            'tjenesteId' => strval($tjeneste),
            'tjenesteBeskrivelse' => $tjeneste ? $tjeneste->hentBeskrivelse() : '',
            'kostnadId' => strval($kostnad),
            'kostnadBeskrivelse' => $kostnad->hentFakturanummer(),
            'tilbakeUrl' => $this->app->returi->get()->url,
            'fordelt' => $kostnad->hentFordelt()
        ]);
        return parent::forberedData();
    }
}