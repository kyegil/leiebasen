<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\js\regnskapsprosjekt_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Regnskapsprosjekt;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\container\Container;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\TreeStore;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\ComboBox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\HtmlEditor;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Text;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FlexiCheckboxGroup;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel as FormPanel;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\tree\Panel as TreePanel;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $prosjekt \Kyegil\Leiebasen\Modell\Regnskapsprosjekt
 *  Mulige variabler:
 *      $prosjektId
 *      $meny
 *      $skjema
 *      $formActionUrl
 * @package Kyegil\Leiebasen\Visning\drift\js\regnskapsprosjekt_skjema
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/regnskapsprosjekt_skjema/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['prosjekt'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Regnskapsprosjekt|null $prosjekt */
        $prosjekt = $this->hentRessurs('prosjekt');

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }
        $this->definerData([
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'skjema' => $this->app->vis(FormPanel::class, [
                'id' => 'skjema',
                'configObjectOnly' => false,
                'title' => $prosjekt ? $prosjekt->navn : 'Opprett nytt prosjektregnskap',
                'enabledSaveButton' => true,
                'formActionUrl' => '/drift/index.php?oppslag=regnskapsprosjekt_skjema&id=' . ($prosjekt && $prosjekt->hentId() ? $prosjekt->hentId() : '*') . '&oppdrag=taimotskjema',
                'returnUrl' => $this->app->returi->get(),
                'formSourceUrl' => '/drift/index.php?oppslag=regnskapsprosjekt_skjema&id=' . ($prosjekt && $prosjekt->hentId() ? $prosjekt->hentId() : '*') . '&oppdrag=hentdata',
                'items' => [
                    $this->app->vis(FieldSet::class, [
                        'title' => 'Prosjektbeskrivelse',
                        'collapsible' => true,
                        'items' => [
                            $this->app->vis(Container::class, [
                                'layout' => 'column',
                                'defaults' => (object)[
                                    'columnWidth' => 0.5,
                                    'margin' => 5
                                ],
                                'items' => [
                                    $this->app->vis(Text::class, [
                                        'name' => 'kode',
                                        'fieldLabel' => 'Kode',
                                        'value' => $prosjekt ? $prosjekt->kode : null
                                    ]),
                                    $this->app->vis(Text::class, [
                                        'name' => 'navn',
                                        'fieldLabel' => 'Navn',
                                        'value' => $prosjekt ? $prosjekt->navn : null
                                    ])
                                ]
                            ]),
                            $this->app->vis(HtmlEditor::class, [
                                'name' => 'beskrivelse',
                                'fieldLabel' => 'Beskrivelse',
                                'height' => 100,
                                'value' => $prosjekt ? $prosjekt->beskrivelse : null
                            ])
                        ]
                    ]),

                    $this->app->vis(Container::class, [
                        'layout' => 'column',
                        'defaults' => (object)[
                            'columnWidth' => 0.5,
                            'margin' => 5
                        ],
                        'items' => [
                            $this->app->vis(ComboBox::class, [
                                'name' => 'summerings_enhet',
                                'fieldLabel' => 'Summerings-enhet',
                                'dataValues' => $this->hentSummeringsEnhetData(),
                                'value' => $prosjekt ? $prosjekt->konfigurering->summeringsEnhet : 'kravtype.alle'
                            ]),

                            $this->app->vis(ComboBox::class, [
                                'name' => 'segmentering',
                                'fieldLabel' => 'Segmentering',
                                'allowBlank' => true,
                                'forceSelection' => true,
                                'dataValues' => (object)[
                                    '' => 'Ingen segmentering',
                                    'måned' => 'Per måned',
                                    'uke' => 'Per uke',
                                    'bygning' => 'Per bygning',
                                    'leieobjekt' => 'Per leieobjekt',
                                    'kravtype' => 'Per kravtype',
                                    'strømanlegg' => 'Per strømanlegg'
                                ],
                                'value' => $prosjekt ? $prosjekt->konfigurering->segmentering : ''
                            ])
                        ]
                    ]),

                    $this->app->vis(Container::class, [
                        'layout' => 'column',
                        'defaults' => (object)[
                            'columnWidth' => 0.5,
                            'margin' => 5
                        ],
                        'items' => [
                            $this->app->vis(FieldSet::class, [
                                'title' => 'Inkluderte leieforhold',
                                'itemId' => 'inkludert',
                                'collapsible' => true,
                                'items' => [
                                    $this->app->vis(FlexiCheckboxGroup::class, [
                                        'configObjectOnly' => false,
                                        'emptyText' => 'Inkluderte områder',
                                        'name' => 'inkluderte_områder',
                                        'forceSelection' => true,
                                        'remote' => true,
                                        'url' => '/drift/index.php?oppslag=regnskapsprosjekt_skjema&id=' . ($prosjekt && $prosjekt->hentId() ?: '*') . '&oppdrag=hentdata&data=områder',
                                        'value' => $prosjekt ? $this->områderSomObjekt($prosjekt->konfigurering->inkludert->områder ?? []) : []
                                    ]),
                                    $this->app->vis(TreePanel::class, [
                                        'id' => 'inkluderte_leieobjekter',
                                        'itemId' => 'inkluderte_leieobjekter',
                                        'height' => 100,
                                        'store' => $this->app->vis(TreeStore::class, [
                                            'root' => new JsCustom(json_encode($this->hentLeieobjekterForInkludering()))
                                        ]),
                                        'margin' => '0 0 5 0'
                                    ]),
                                    $this->app->vis(FlexiCheckboxGroup::class, [
                                        'configObjectOnly' => false,
                                        'emptyText' => 'Inkluder spesifikke leieforhold',
                                        'name' => 'inkluderte_leieforhold',
                                        'forceSelection' => true,
                                        'remote' => true,
                                        'url' => '/drift/index.php?oppslag=regnskapsprosjekt_skjema&id=' . ($prosjekt && $prosjekt->hentId() ?: '*') . '&oppdrag=hentdata&data=leieforhold',
                                        'value' => $prosjekt ? $this->leieforholdSomObjekt($prosjekt->konfigurering->inkludert->leieforhold ?? []) : []
                                    ])
                                ]
                            ]),

                            $this->app->vis(FieldSet::class, [
                                'title' => 'Ekskluderte leieforhold',
                                'itemId' => 'ekskludert',
                                'collapsible' => true,
                                'items' => [
                                    $this->app->vis(FlexiCheckboxGroup::class, [
                                        'configObjectOnly' => false,
                                        'emptyText' => 'Ekskluderte områder',
                                        'name' => 'ekskluderte_områder',
                                        'forceSelection' => true,
                                        'remote' => true,
                                        'url' => '/drift/index.php?oppslag=regnskapsprosjekt_skjema&id=' . ($prosjekt && $prosjekt->hentId() ?: '*') . '&oppdrag=hentdata&data=områder',
                                        'value' => $prosjekt ? $this->områderSomObjekt($prosjekt->konfigurering->ekskludert->områder ?? []) : []
                                    ]),
                                    $this->app->vis(TreePanel::class, [
                                        'id' => 'ekskluderte_leieobjekter',
                                        'itemId' => 'ekskluderte_leieobjekter',
                                        'rootVisible' => false,
                                        'height' => 100,
                                        'store' => $this->app->vis(TreeStore::class, [
                                            'root' => new JsCustom(json_encode($this->hentLeieobjekterForEkskludering()))
                                        ]),
                                        'margin' => '0 0 5 0'
                                    ]),
                                    $this->app->vis(FlexiCheckboxGroup::class, [
                                        'configObjectOnly' => false,
                                        'emptyText' => 'Ekskluder spesifikke leieforhold',
                                        'name' => 'ekskluderte_leieforhold',
                                        'forceSelection' => true,
                                        'remote' => true,
                                        'url' => '/drift/index.php?oppslag=regnskapsprosjekt_skjema&id=' . ($prosjekt && $prosjekt->hentId() ?: '*') . '&oppdrag=hentdata&data=leieforhold',
                                        'value' => $prosjekt ? $this->leieforholdSomObjekt($prosjekt->konfigurering->ekskludert->leieforhold ?? []) : []
                                    ])
                                ]
                            ]),
                        ],
                    ]),
                ]
            ]),
            'formActionUrl' => '/drift/index.php?oppslag=regnskapsprosjekt_skjema&id=' . ($prosjekt && $prosjekt->hentId() ? $prosjekt->hentId() : '*') . '&oppdrag=ta_i_mot_skjema'

        ]);

        return parent::forberedData();
    }

    /**
     * @return stdClass
     * @throws Exception
     * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.NodeInterface
     */
    public function hentLeieobjekterForInkludering(): stdClass {
        /** @var Regnskapsprosjekt $prosjekt */
        $prosjekt = $this->hentRessurs('prosjekt');
        /** @var object $inkludert */
        $inkludert = $prosjekt ? $prosjekt->konfigurering->inkludert : (object)['bygninger' => null, 'leieobjekter' => null, 'leieforhold' => null];

        $root = (object)[
            'id' => '*',
            'text' => 'Alle leieforhold',
            'expanded' => isset($inkludert->bygninger) || isset($inkludert->leieobjekter),
            'checked' => $inkludert->bygninger === null && $inkludert->leieobjekter === null,
            'children' => []
        ];

        /** @var Bygning $bygning */
        foreach ($this->app->hentSamling(Bygning::class) as $bygning) {
            $root->children[$bygning->hentId()] = (object)[
                'id' => $bygning . '-0',
                'text' => 'Alle leieforhold i ' . $bygning->hentNavn(),
                'expanded' => false,
                'checked' => is_array($inkludert->bygninger) && in_array($bygning->hentId(), $inkludert->bygninger),
                'leaf' => false,
                'children' => []
            ];

        }
        /** @var Leieobjekt $leieobjekt */
        foreach ($this->app->hentSamling(Leieobjekt::class)->leggTilBygningModell() as $leieobjekt) {
            $bygning = $leieobjekt->bygning;
            $bygningsId = $bygning ? $bygning->hentId() : 0;
            $checked = is_array($inkludert->leieobjekter) && in_array($leieobjekt->hentId(), $inkludert->leieobjekter);

            if (!$bygningsId) {
                settype($root->children[0], 'object');
                settype($root->children[0]->children, 'array');
                $root->children[0]->expanded = $checked;
            }
            $root->children[intval($bygningsId)]->children[] = (object)[
                'id' => $bygningsId . '-' . $leieobjekt,
                'text' => 'Alle leieforhold i ' . $leieobjekt->hentBeskrivelse(),
                'checked' => $checked,
                'leaf' => true
            ];
        }
        if (isset($root->children[0])) {
            $root->children[0]->text = 'Andre leieobjekter';
            $root->children[0]->expanded = false;
            $root->children[0]->checked = is_array($inkludert->bygninger) && in_array(0, $inkludert->bygninger);
            $root->children[0]->leaf = false;
        }
        $root->children = array_values($root->children);
        return $root;
    }

    /**
     * @return stdClass
     * @throws Exception
     * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.NodeInterface
     */
    public function hentLeieobjekterForEkskludering(): stdClass {
        /** @var Regnskapsprosjekt $prosjekt */
        $prosjekt = $this->hentRessurs('prosjekt');
        /** @var object $ekskludert */
        $ekskludert = $prosjekt ? $prosjekt->konfigurering->ekskludert : (object)['bygninger' => [], 'leieobjekter' => [], 'leieforhold' => []];

        $root = (object)[
            'id' => '*',
            'text' => 'Alle leieforhold',
            'expanded' => isset($ekskludert->bygninger) || isset($ekskludert->leieobjekter),
            'checked' => $ekskludert->bygninger === null && $ekskludert->leieobjekter === null,
            'children' => []
        ];

        /** @var Bygning $bygning */
        foreach ($this->app->hentSamling(Bygning::class) as $bygning) {
            $root->children[$bygning->hentId()] = (object)[
                'id' => $bygning . '-0',
                'text' => 'Alle leieforhold i ' . $bygning->hentNavn(),
                'expanded' => false,
                'checked' => is_array($ekskludert->bygninger) && in_array($bygning->hentId(), $ekskludert->bygninger),
                'leaf' => false,
                'children' => []
            ];

        }
        /** @var Leieobjekt $leieobjekt */
        foreach ($this->app->hentSamling(Leieobjekt::class)->leggTilBygningModell() as $leieobjekt) {
            $bygning = $leieobjekt->bygning;
            $bygningsId = $bygning ? $bygning->hentId() : 0;
            $checked = is_array($ekskludert->leieobjekter) && in_array($leieobjekt->hentId(), $ekskludert->leieobjekter);

            if (!$bygningsId) {
                settype($root->children[0], 'object');
                settype($root->children[0]->children, 'array');
            }
            settype($root->children[intval($bygningsId)]->expanded, 'boolean');
            $root->children[intval($bygningsId)]->expanded |= $checked;
            $root->children[intval($bygningsId)]->children[] = (object)[
                'id' => $bygningsId . '-' . $leieobjekt,
                'text' => 'Alle leieforhold i ' . $leieobjekt->hentBeskrivelse(),
                'checked' => $checked,
                'leaf' => true
            ];
        }
        if (isset($root->children[0])) {
            $root->children[0]->text = 'Andre leieobjekter';
            $root->children[0]->checked = is_array($ekskludert->bygninger) && in_array(0, $ekskludert->bygninger);
            $root->children[0]->leaf = false;
        }
        $root->children = array_values($root->children);
        return $root;
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    protected function hentSummeringsEnhetData(): stdClass
    {
        $data = (object)[
            'kravtype.alle' => 'Alle krav',
            'kravtype.husleie' => Leieforhold\Krav::TYPE_HUSLEIE,
            'kravtype.fellesstrøm' => 'Fellesstrøm',
            'kravtype.purregebyr' => 'Purregebyr',
            'kravtype.annet' => 'Annet',
        ];

        /** @var Delkravtype $delkravtype */
        foreach ($this->app->hentDelkravtyper() as $delkravtype) {
            if ($delkravtype->selvstendigTillegg) {
                $data->{'kravtype.' . $delkravtype->kode} = $delkravtype->navn . ' (tillegg til ' . strtolower($delkravtype->kravtype) . ')';
            }
            else {
                $data->{'delkrav.' . $delkravtype->kode} = $delkravtype->navn . ' (del av ' . strtolower($delkravtype->kravtype) . ')';
            }
        }

        return $data;
    }

    /**
     * Returnerer et stdClass objekt,
     * med leieforholdets id som egenskapsnavn og beskrivelse som verdi
     *
     * @param int[] $leieforholdIder
     * @return stdClass
     * @throws Exception
     */
    protected function leieforholdSomObjekt(array $leieforholdIder): stdClass
    {
        $resultat = new stdClass();
        foreach ($leieforholdIder as $leieforholdId) {
            /** @var Leieforhold $leieforhold */
            $leieforhold = $this->app->hentModell(Leieforhold::class, (int)$leieforholdId);
            $resultat->$leieforholdId = $leieforhold->hentBeskrivelse();
        }
        return $resultat;
    }

    /**
     * Returnerer et stdClass objekt,
     * med områdets id som egenskapsnavn, og navn som verdi
     *
     * @param int[] $områdeIder
     * @return stdClass
     * @throws Exception
     */
    protected function områderSomObjekt(array $områdeIder): stdClass
    {
        $resultat = new stdClass();
        foreach ($områdeIder as $områdeId) {
            /** @var Område $område */
            $område = $this->app->hentModell(Område::class, (int)$områdeId);
            $resultat->$områdeId = $område->hentNavn();
        }
        return $resultat;
    }

}