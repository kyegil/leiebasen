<?php

namespace Kyegil\Leiebasen\Visning\drift\js\innstillinger\skript;


use Kyegil\Leiebasen\Visning\Drift;

/**
 * Skript-fil i drift
 *
 *  Ressurser:

 *  Mulige variabler:
 *      $skript
 * @package Kyegil\Leiebasen\Visning\drift\js\innstillinger
 */
class Leieregulering extends Drift
{
    protected $template = 'drift/js/innstillinger/skript/Leieregulering.js';
}