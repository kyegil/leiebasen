<?php

namespace Kyegil\Leiebasen\Visning\drift\js\innstillinger;


use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Skript-fil i drift
 *
 *  Ressurser:
 *      $leiebasenJsObjektEgenskaper stdClass
 *  Mulige variabler:
 *      $meny
 *      $initLeiebasenJsObjektEgenskaper
 *      $skjema
 *      $formActionUrl
 * @package Kyegil\Leiebasen\Visning\drift\js\innstillinger
 */
class Index extends Drift
{
    protected $template = 'drift/js/innstillinger/Index.js';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $this->definerData([
            'skript' => new ViewArray([
                $this->app->vis(Visning\drift\js\innstillinger\skript\Leieregulering::class)
            ])
        ]);

        return parent::forberedData();
    }
}