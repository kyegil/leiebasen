<?php

namespace Kyegil\Leiebasen\Visning\drift\js\_mal_skjema;


use Exception;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Skript-fil i drift
 *
 * Ressurser:
 * * modell \Kyegil\Leiebasen\Modell
 *
 * Tilgjengelige variabler:
 * * $modellId
 *
 * @package Kyegil\Leiebasen\Visning\drift\js\_mal_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/js/_mal_skjema/Index.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,[])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell|null $modell */
        $modell = $this->hentRessurs('leieberegning');

        $datasett = [
            'modellId' => $modell ? $modell->hentId() : '',
            'skript' => '',
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}