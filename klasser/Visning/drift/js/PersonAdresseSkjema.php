<?php

namespace Kyegil\Leiebasen\Visning\drift\js;


use Exception;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Skript-fil i drift
 *
 * Ressurser:
 * * leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *
 * Tilgjengelige variabler:
 * * $leieforholdId
 * * $skript
 *
 * @package Kyegil\Leiebasen\Visning\drift\js
 */
class PersonAdresseSkjema extends Drift
{
    /** @var string */
    protected $template = 'drift/js/PersonAdresseSkjema.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['person'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Person|null $person */
        $person = $this->hentRessurs('person');

        $datasett = [
            'personId' => $person ? $person->hentId() : '*',
            'skript' => '',
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}