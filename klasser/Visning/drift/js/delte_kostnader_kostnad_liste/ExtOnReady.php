<?php

namespace Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_kostnad_liste;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\ComboBox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Date;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Number;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Text;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\grid\column\Action;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\grid\column\Column;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $bruker \Kyegil\Leiebasen\Modell\Person
 *      $leiebasenJsObjektEgenskaper object
 *  Mulige variabler:
 *      $brukerId
 *      $initLeiebasenJsObjektEgenskaper
 *      $tilbakeKnapp
 * @package Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_kostnad_liste
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/delte_kostnader_kostnad_liste/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'bruker') {
            return $this->settRessurs($attributt, $verdi);
        }
        if ($attributt == 'leiebasenJsObjektEgenskaper') {
            return $this->settLeiebasenJsObjektEgenskaper($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param stdClass $leiebasenJsObjektEgenskaper
     * @return $this
     */
    public function settLeiebasenJsObjektEgenskaper(stdClass $leiebasenJsObjektEgenskaper): Drift
    {
        return $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var stdClass $leiebasenJsObjektEgenskaper */
        $leiebasenJsObjektEgenskaper = $this->hentRessurs('leiebasenJsObjektEgenskaper');
        if (!$leiebasenJsObjektEgenskaper) {
            $leiebasenJsObjektEgenskaper = new stdClass();
            $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
        }
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        $brukerId = strval($bruker);

        $leiebasenJsObjektEgenskaper->drift = (object)[
            'kolonner' => (object)[
                'id' => $this->app->vis(Column::class, [
                    'dataIndex' => 'id',
                    'align' => 'right',
                    'text' => 'Id',
                    'sortable' =>	true,
                    'hidden' => true,
                    'width' => 40
                ]),
                'fakturanummer' => $this->app->vis(Column::class, [
                    'dataIndex' => 'fakturanummer',
                    'text' => 'Fakturanr',
                    'sortable' => true,
                    'width' => 100,
                    'editor' => $this->app->vis(Text::class, [
                        'allowBlank' => true,
                        'selectOnFocus' => true,
                    ])
                ]),
                'tjeneste_id' => $this->app->vis(Column::class, [
                    'dataIndex' => 'tjeneste_id',
                    'text' => 'Tjeneste',
                    'sortable' => true,
                    'width' => 100,
                    'flex' => 1,
                    'editor' => $this->app->vis(ComboBox::class, [
                        'allowBlank' => true,
                        'remote' => false,
                        'forceSelection' => true,
                        'matchFieldWidth' => false,
                        'width' => 800,
                        'dataValues' => $this->hentTjenester(),
                    ]),
                    'renderer' => new JsFunction("return record.get('tjeneste');", ['value', 'metaData', 'record', 'rowIndex', 'colIndex', 'store'])

                ]),
                'beløp' => $this->app->vis(Column::class, [
                    'dataIndex' => 'beløp',
                    'text' => 'Beløp',
                    'align' => 'right',
                    'sortable' => true,
                    'width' => 100,
                    'editor' => $this->app->vis(Number::class, [
                        'allowBlank' => false,
                        'allowDecimals' => true,
                        'minValue' => 0,
                        'blankText' => 'Du må angi et beløp',
                        'decimalPrecision' => 2,
                        'keyNavEnabled' => false,
                        'mouseWheelEnabled' => false,
                        'selectOnFocus' => true,
                    ]),
                    'renderer' => new JsFunction("return record.get('beløp_formatert');", ['value', 'metaData', 'record', 'rowIndex', 'colIndex', 'store'])
                ]),
                'fradato' => $this->app->vis(Column::class, [
                    'dataIndex' => 'fradato',
                    'text' => 'Fra dato',
                    'align' => 'right',
                    'sortable' => true,
                    'width' => 80,
                    'renderer' => new JsCustom("Ext.util.Format.dateRenderer('d.m.Y')"),
                    'editor' => $this->app->vis(Date::class, [
                        'hideTrigger' => true,
                        'format' => 'd.m.Y',
                        'selectOnFocus' => true,
                    ]),
                ]),
                'tildato' => $this->app->vis(Column::class, [
                    'dataIndex' => 'tildato',
                    'text' => 'Til dato',
                    'align' => 'right',
                    'sortable' => true,
                    'width' => 80,
                    'renderer' => new JsCustom("Ext.util.Format.dateRenderer('d.m.Y')"),
                    'editor' => $this->app->vis(Date::class, [
                        'hideTrigger' => true,
                        'format' => 'd.m.Y',
                        'selectOnFocus' => true,
                    ]),
                ]),
                'termin' => $this->app->vis(Column::class, [
                    'dataIndex' => 'termin',
                    'text' => 'Termin',
                    'flex' => 1,
                    'width' => 50,
                    'editor' => $this->app->vis(Text::class, []),
                ]),
                'forbruk' => $this->app->vis(Column::class, [
                    'dataIndex' => 'forbruk',
                    'text' => 'Forbruk',
                    'align' => 'right',
                    'sortable' => true,
                    'width' => 100,
                    'editor' => $this->app->vis(Number::class, [
                        'allowBlank' => true,
                        'allowDecimals' => true,
                        'minValue' => 0,
                        'decimalPrecision' => 2,
                        'keyNavEnabled' => false,
                        'mouseWheelEnabled' => false,
                        'selectOnFocus' => true,
                    ]),
                    'renderer' => new JsFunction("return record.get('forbruk_formatert');", ['value', 'metaData', 'record', 'rowIndex', 'colIndex', 'store'])
                ]),
                'varslet' => $this->app->vis(Column::class, [
                    'dataIndex' => 'varslet',
                    'text' => 'Varslet',
                    'sortable' => false,
                    'width' => 50,
                    'renderer' => new JsFunction("return value ? '✔' : '';", ['value']),
                ]),
                'varsle' => $this->app->vis(Action::class, [
                    'dataIndex' => 'id',
                    'text' => 'Varsle',
                    'icon' => '/pub/media/bilder/felles/epost.png',
                    'align' => 'center',
                    'tooltip' => 'Varsle deltakerne om den foreslåtte fordelingen av denne kostnaden',
                    'width' => 50,
                    'isDisabled' => new JsFunction("return !record.get('kan_varsles') || record.get('varslet');", ['view', 'rowIndex', 'colIndex', 'item', 'record']),
                    'handler' => new JsFunction(
                        "leiebasen.drift.varsleFordeling(record);",
                        ['grid', 'rowIndex', 'colIndex', 'item', 'e', 'record']
                    )
                ]),
                'bekreft' => $this->app->vis(Action::class, [
                    'dataIndex' => 'id',
                    'text' => 'Bekreft',
                    'icon' => '/pub/media/bilder/felles/padlock-locked.svg',
                    'align' => 'center',
                    'tooltip' => 'Bekreft og krev inn i hht foreslått fordeling',
                    'width' => 50,
                    'isDisabled' => new JsFunction("return !record.get('kan_låses');", ['view', 'rowIndex', 'colIndex', 'item', 'record']),
                    'handler' => new JsFunction(
                        "leiebasen.drift.bekreftFordeling(record);",
                        ['grid', 'rowIndex', 'colIndex', 'item', 'e', 'record']
                    )
                ]),
                'slett' => $this->app->vis(Action::class, [
                    'dataIndex' => 'id',
                    'text' => 'Slett',
                    'icon' => '/pub/media/bilder/drift/slett.png',
                    'tooltip' => 'Slett',
                    'width' => 50,
                    'handler' => new JsFunction(
                        "leiebasen.drift.slettFaktura(record);",
                        ['grid', 'rowIndex', 'colIndex', 'item', 'e', 'record']
                    )
                ]),
            ]
        ];

        $this->definerData([
            'meny' => '',
            'initLeiebasenJsObjektEgenskaper' => $this->gjengiLeiebasenJsObjektEgenskaper(),
            'tilbakeUrl' => $this->app->returi->get()->url,
        ]);
        return parent::forberedData();
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    private function hentTjenester(): stdClass
    {
        $resultat = new stdClass();

        $tjenester = $this->app->hentSamling(Tjeneste::class);
        /** @var Tjeneste $tjeneste */
        foreach ($tjenester as $tjeneste) {
            $resultat->{$tjeneste->hentId()} = "{$tjeneste->hentId()}: {$tjeneste->hentNavn()}";
        }

        return $resultat;
    }
}