<?php

namespace Kyegil\Leiebasen\Visning\drift\js\oversikt_gjeld;


use Exception;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Skript-fil i gjeldsoversikten i drift
 *
 *  Ressurser:
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\drift\js\oversikt_gjeld
 */
class Index extends Drift
{
    protected $template = 'drift/js/oversikt_gjeld/Index.js';

    /**
     * @return Index
     * @throws Exception
     */
    protected function forberedData()
    {
        $this->definerData([
            'skript' => '',
        ]);

        return parent::forberedData();
    }
}