<?php

namespace Kyegil\Leiebasen\Visning\drift\js\eksport;


use Kyegil\Leiebasen\Modell\Eksportprofil;
use Kyegil\Leiebasen\Modell\Eksportprofilsett;
use Kyegil\Leiebasen\Visning\Drift;
use stdClass;

/**
 * Skript-fil i drift
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $skript
 * @package Kyegil\Leiebasen\Visning\drift\js\eksport
 */
class Index extends Drift
{
    protected $template = 'drift/js/eksport/Index.js';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $this->definerData('eksportFormat', json_encode($this->hentEksportOppsett(), JSON_FORCE_OBJECT));
        return parent::forberedData();
    }

    private function hentEksportOppsett():object
    {
        $resultat = new stdClass();
        /** @var Eksportprofilsett $eksportprofiler */
        $eksportprofiler = $this->app->hentSamling(Eksportprofil::class);
        foreach($eksportprofiler as $eksportprofil) {
            $oppsett = $eksportprofil->hentOppsett();
            $resultat->{$eksportprofil->hentId()} = (object)[
                'csvSkilletegn' => $oppsett->csvSkilletegn ?? null,
                'datoformat' => $oppsett->datoformat ?? null,
                'innpakning' => $oppsett->innpakning ?? null,
            ];
        }
        /** @var stdClass $resultat */
        $resultat = $this->app->after($this, __FUNCTION__, $resultat);
        return $resultat;
    }
}