<?php

namespace Kyegil\Leiebasen\Visning\drift\js\adgang_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\button\Button;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\container\Container;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Checkbox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\ComboBox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Display;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Password;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Text;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel as FormPanel;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $adgang \Kyegil\Leiebasen\Modell\Person\Adgang|null
 *      $person \Kyegil\Leiebasen\Modell\Person|null
 *      $leieforhold \Kyegil\Leiebasen\Modell\Leieforhold|null
 *      $leiebasenJsObjektEgenskaper stdClass
 *  Mulige variabler:
 *      $adgangId
 *      $område
 *      $meny
 *      $initLeiebasenJsObjektEgenskaper
 *      $skjema
 *      $formActionUrl
 * @package Kyegil\Leiebasen\Visning\drift\js\adgang_skjema
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/adgang_skjema/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['adgang','person','område','leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        if ($attributt == 'leiebasenJsObjektEgenskaper') {
            return $this->settLeiebasenJsObjektEgenskaper($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param stdClass $leiebasenJsObjektEgenskaper
     * @return $this
     */
    public function settLeiebasenJsObjektEgenskaper(stdClass $leiebasenJsObjektEgenskaper): Drift
    {
        return $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var stdClass $leiebasenJsObjektEgenskaper */
        $leiebasenJsObjektEgenskaper = $this->hentRessurs('leiebasenJsObjektEgenskaper');
        if (!$leiebasenJsObjektEgenskaper) {
            $leiebasenJsObjektEgenskaper = new stdClass();
            $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
        }
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Person\Adgang|null $adgang */
        $adgang = $this->hentRessurs('adgang');
        /** @var Person|null $person */
        $person = $this->hentRessurs('person');
        /** @var string|null $område */
        $område = $this->hentRessurs('område');
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        $adgangId = $adgang && $adgang->hentId() ? $adgang->hentId() : '*';
        $brukerProfil = $person ? $person->hentBrukerProfil() : null;

        $leiebasenJsObjektEgenskaper->skjema = (object)[
            'felter' => (object)[]
        ];

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }

        if ($person) {
            $lenke = strval(new HtmlElement('a', [
                'href' => '/drift/index.php?oppslag=personadresser_kort&id=' . $person->hentId(),
                'title' => 'Gå til adressekortet',
            ], $person->hentNavn() . ' (' . $person->hentId() . ')'
            ));
            /** @var Display $personFelt */
            $personFelt = $this->app->vis(Display::class, [
                'name' => 'person',
                'fieldLabel' => 'Bruker',
                'submitValue' => true,
                'value' => $person->hentId(),
                'renderer' => new JsFunction("return " . json_encode($lenke) . ";", []),
            ]);
        }
        else {
            /** @var ComboBox $personFelt */
            $personFelt = $this->app->vis(ComboBox::class, [
                'name' => 'person',
                'fieldLabel' => 'Bruker',
                'forceSelection' => true,
                'allowBlank' => false,
                'remote'=> true,
                'flex' => 1,
                'ignoredConfigs' => ['width'],
                'url' => '/drift/index.php?oppslag=adgang_skjema&id=' . $adgangId . '&oppdrag=hent_data&data=personer',
                'listeners' => (object)[
                    'select' => new JsFunction('leiebasen.skjema.oppdaterBruker();'),
                ]
            ]);
        }
        $leiebasenJsObjektEgenskaper->skjema->felter->person = $personFelt;

        if ($leieforhold) {
            $leieforholdFelt = $this->app->vis(Display::class, [
                'name' => 'leieforhold',
                'fieldLabel' => 'Leieforhold',
                'value' => strval(new HtmlElement('a', [
                    'href' => '/drift/index.php?oppslag=leieforholdkort&id=' . $leieforhold->hentId(),
                    'title' => 'Gå til adressekortet',
                ], $leieforhold->hentId() . ' – ' . $leieforhold->hentNavn()
                ))
            ]);
        }
        else {
            /** @var ComboBox $leieforholdFelt */
            $leieforholdFelt = $this->app->vis(ComboBox::class, [
                'name' => 'leieforhold',
                'fieldLabel' => 'Leieforhold',
                'forceSelection' => true,
                'allowBlank' => false,
                'remote'=> true,
                'url' => '/drift/index.php?oppslag=adgang_skjema&id=' . $adgangId . '&oppdrag=hent_data&data=leieforhold',
            ]);
        }
        $leiebasenJsObjektEgenskaper->skjema->felter->leieforhold = $leieforholdFelt;

        if ($område) {
            $områdeFelt = $this->app->vis(Display::class, [
                'name' => 'område',
                'fieldLabel' => 'Adgangsområde',
                'value' => str_replace(['mine-sider','drift','flyko','oppfølging'], ['Mine Sider', 'Drift', 'FlyKo', 'Oppfølging'], $område)
            ]);
        }
        else if ($leieforhold) {
            /** @var ComboBox $områdeFelt */
            $områdeFelt = $this->app->vis(ComboBox::class, [
                'name' => 'område',
                'fieldLabel' => 'Adgangsområde',
                'remote'=> false,
                'forceSelection' => true,
                'allowBlank' => false,
                'dataValues' => (object)[
                    'mine-sider' => 'Mine Sider'
                ],
                'value' => 'mine-sider'
            ]);
        }
        else {
            $områdeFelt = $this->app->vis(ComboBox::class, [
                'name' => 'område',
                'fieldLabel' => 'Adgangsområde',
                'remote'=> false,
                'forceSelection' => true,
                'dataValues' => $this->hentOmrådeOptions(),
                'listeners' => (object)[
                    'select' => new JsFunction('leiebasen.skjema.oppdaterLeieforholdFelt();', [])
                ]
            ]);
        }
        $leiebasenJsObjektEgenskaper->skjema->felter->område = $områdeFelt;

        /** @var Text $brukernavnFelt */
        $brukernavnFelt = $this->app->vis(Text::class, [
            'name' => 'brukernavn',
            'fieldLabel' => 'Brukernavn',
            'labelAlign' => 'top',
            'value' => $brukerProfil ? $brukerProfil->hentBrukernavn() : null
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->brukernavn = $brukernavnFelt;

        /** @var Text $epostFelt */
        $epostFelt = $this->app->vis(Text::class, [
            'name' => 'epost',
            'fieldLabel' => 'E-postadresse',
            'labelAlign' => 'top',
            'value' => $person ? $person->epost : null
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->epost = $epostFelt;

        $this->definerData([
            'adgangId' => $adgangId,
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'initLeiebasenJsObjektEgenskaper' => $this->gjengiLeiebasenJsObjektEgenskaper(),
            'skjema' => $this->app->vis(FormPanel::class, [
                'id' => 'skjema',
                'configObjectOnly' => false,
                'title' => $person ? ('Nett-adgang for ' . $person->hentNavn()) : 'Tildel adgang for nett-område',
                'enabledSaveButton' => true,
                'formActionUrl' => '/drift/index.php?oppslag=adgang_skjema&id=' . ($adgang && $adgang->hentId() ? $adgang->hentId() : '*') . '&oppdrag=ta_imot_skjema',
                'returnUrl' => $this->app->returi->get(),
                'formSourceUrl' => '/drift/index.php?oppslag=adgang_skjema&id=' . ($adgang && $adgang->hentId() ? $adgang->hentId() : '*') . '&oppdrag=hent_data',
                'items' => [
                    $this->app->vis(Container::class, [
                        'layout' => 'hbox',
                        'items' => [
                            new JsCustom('leiebasen.skjema.felter.person'),
                            $this->app->vis(Button::class, [
                                'text' => 'Registrer ny person',
                                'handler' => new JsFunction('window.location.href="/drift/person_adresse_skjema/*";'),
                                'margin' => '0 0 0 5',
                                'hidden' => (bool)$person
                            ]),
                        ]
                    ]),
                    new JsCustom('leiebasen.skjema.felter.område'),
                    new JsCustom('leiebasen.skjema.felter.leieforhold'),
                    $this->app->vis(FieldSet::class, [
                        'title' => 'Epost-varsling',
                        'layout' => 'vbox',
                        'items' => [
                            $this->app->vis(Checkbox::class, [
                                'name' => 'epostvarsling',
                                'labelAlign' => 'top',
                                'boxLabel'=> 'Brukeren mottar e-post fra leiebasen',
                                'value' => $adgang ? $adgang->epostvarsling : true
                            ]),
                            $this->app->vis(Checkbox::class, [
                                'name' => 'innbetalingsbekreftelse',
                                'fieldLabel' => 'Innbetalingsbekreftelse',
                                'labelAlign' => 'top',
                                'boxLabel'=> 'Send også kvittering for registrerte betalinger',
                                'value' => $adgang ? $adgang->innbetalingsbekreftelse : true
                            ]),
                            $this->app->vis(Checkbox::class, [
                                'name' => 'umiddelbart_betalingsvarsel_epost',
                                'fieldLabel' => 'Umiddelbart betalingsvarsel med e-post',
                                'labelAlign' => 'top',
                                'boxLabel'=> 'Send påminnelse umiddelbart ved manglende betaling',
                                'value' => $adgang ? $adgang->umiddelbartBetalingsvarselEpost : true
                            ]),
                            $this->app->vis(Checkbox::class, [
                                'name' => 'forfallsvarsel',
                                'fieldLabel' => 'Forfallsvarsel',
                                'labelAlign' => 'top',
                                'boxLabel'=> 'Send også påminnelse om krav som er i ferd med å forfalle til betaling',
                                'value' => $adgang ? $adgang->forfallsvarsel : false
                            ]),
                        ]
                    ]),
                    $this->app->vis(FieldSet::class, [
                        'title' => 'Brukerprofil (Endringer her vil påvirke alle adganger for denne brukeren)',
                        'layout' => 'hbox',
                        'items' => [
                            $this->app->vis(Container::class, [
                                'layout' => 'vbox',
                                'flex' => 1,
                                'padding' => '0 5 0 0',
                                'items' => [
                                    new JsCustom('leiebasen.skjema.felter.brukernavn'),
                                    new JsCustom('leiebasen.skjema.felter.epost'),
                                ]
                            ]),
                            $this->app->vis(FieldSet::class, [
                                'title' => 'Angi nytt passord (fylles kun inn ved endring)',
                                'checkboxToggle' => true,
                                'checkboxName' => 'endre_passord',
                                'collapsed' => false,
                                'layout' => 'vbox',
                                'flex' => 1,
                                'items' => [
                                    $this->app->vis(Password::class, [
                                        'name' => 'passord1',
                                        'fieldLabel' => 'Nytt passord',
                                        'labelAlign' => 'top',
                                    ]),
                                    $this->app->vis(Password::class, [
                                        'name' => 'passord2',
                                        'fieldLabel' => 'Gjenta det nye passordet for bekreftelse',
                                        'labelAlign' => 'top',
                                    ]),
                                ]
                            ]),
                        ]
                    ]),
                ]
            ]),
            'formActionUrl' => '/drift/index.php?oppslag=adgang_skjema&id=' . $adgangId . '&oppdrag=ta_i_mot_skjema'
        ]);

        return parent::forberedData();
    }

    /**
     * @return object
     * @throws \Throwable
     */
    private function hentOmrådeOptions()
    {
        $options = (object)[
            Person\Adgang::ADGANG_MINESIDER => 'Mine Sider',
            Person\Adgang::ADGANG_DRIFT => 'Drift',
        ];
        return $this->app->post($this, __FUNCTION__, $options);
    }
}