<?php

namespace Kyegil\Leiebasen\Visning\drift\js\leieobjekt_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Sett;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Checkbox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\ComboBox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\File;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Text;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\TextArea;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FlexiCheckboxGroup;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel as FormPanel;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $område \Kyegil\Leiebasen\Modell\Område
 *  Mulige variabler:
 *      $områdeId
 *      $meny
 *      $skjema
 *      $formActionUrl
 * @package Kyegil\Leiebasen\Visning\drift\js\område_skjema
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/leieobjekt_skjema/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['leieobjekt'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Leieobjekt|null $leieobjekt */
        $leieobjekt = $this->hentRessurs('leieobjekt');

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }

        $this->definerData([
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'skjema' => $this->app->vis(FormPanel::class, [
                'id' => 'skjema',
                'configObjectOnly' => false,
                'title' => $leieobjekt ? $leieobjekt->beskrivelse : 'Registrer nytt leieobjekt',
                'enabledSaveButton' => true,
                'formActionUrl' => '/drift/index.php?oppslag=leieobjekt_skjema&id=' . ($leieobjekt && $leieobjekt->hentId() ? $leieobjekt->hentId() : '*') . '&oppdrag=ta_imot_skjema',
                'returnUrl' => $this->app->returi->get(),
                'formSourceUrl' => '/drift/index.php?oppslag=leieobjekt_skjema&id=' . ($leieobjekt && $leieobjekt->hentId() ? $leieobjekt->hentId() : '*') . '&oppdrag=hent_data',
                'items' => [
                    $this->app->vis(Checkbox::class, [
                        'name' => 'aktivert',
                        'fieldLabel' => 'Aktivert',
                        'boxLabel' => 'Leieobjektet kan leies ut',
                        'checked' => !$leieobjekt || !$leieobjekt->hentIkkeForUtleie()
                    ]),
                    $this->app->vis(Text::class, [
                        'name' => 'navn',
                        'allowBlank' => false,
                        'fieldLabel' => 'Navn på leieobjekt',
                        'value' => $leieobjekt ? $leieobjekt->navn : null
                    ]),
                    $this->app->vis(Text::class, [
                        'name' => 'gateadresse',
                        'fieldLabel' => 'Gateadresse',
                        'value' => $leieobjekt ? $leieobjekt->gateadresse : null
                    ]),
                    $this->app->vis(ComboBox::class, [
                        'name' => 'bygning',
                        'fieldLabel' => 'Bygning',
                        'value' => $leieobjekt ? strval($leieobjekt->bygning) : null,
                        'remote' => false,
                        'forceSelection' => true,
                        'dataValues' => $this->hentBygninger()
                    ]),
                    $this->app->vis(FlexiCheckboxGroup::class, [
                        'name' => 'områder',
                        'fieldLabel' => 'Områder',
                        'value' => $leieobjekt ? $this->hentOmråder($leieobjekt) : null,
                        'remote' => false,
                        'dataValues' => $this->hentOmråder()
                    ]),
                    $this->app->vis(ComboBox::class, [
                        'name' => 'etasje',
                        'fieldLabel' => 'Etasje',
                        'value' => $leieobjekt ? $leieobjekt->hentEtg() : null,
                        'remote' => false,
                        'forceSelection' => true,
                        'dataValues' => $this->hentEtasjevalg()
                    ]),
                    $this->app->vis(Text::class, [
                        'name' => 'beskrivelse',
                        'fieldLabel' => 'Evt. annen beskrivelse',
                        'value' => $leieobjekt ? $leieobjekt->hent('beskrivelse') : null
                    ]),
                    $this->app->vis(Text::class, [
                        'name' => 'postnr',
                        'fieldLabel' => 'Postnummer',
                        'value' => $leieobjekt ? $leieobjekt->hentPostnr() : null
                    ]),
                    $this->app->vis(Text::class, [
                        'name' => 'poststed',
                        'fieldLabel' => 'Poststed',
                        'value' => $leieobjekt ? $leieobjekt->hentPoststed() : null
                    ]),
                    $this->app->vis(ComboBox::class, [
                        'name' => 'boenhet',
                        'fieldLabel' => 'Leies ut som',
                        'forceSelection' => true,
                        'value' => $leieobjekt ? ($leieobjekt->hentBoenhet() ? '1' : '0') : '1',
                        'remote' => false,
                        'dataValues' => (object)[0 => 'Nærings- eller annet lokale', 1 => 'Bolig']
                    ]),
                    $this->app->vis(Text::class, [
                        'name' => 'areal',
                        'fieldLabel' => 'Areal i ㎡',
                        'regex' => new JsCustom('/^[0-9]*$/'),
                        'value' => $leieobjekt && $leieobjekt->hentAreal() ? $leieobjekt->hentAreal() : null
                    ]),
                    $this->app->vis(Text::class, [
                        'name' => 'ant_rom',
                        'fieldLabel' => 'Antall rom',
                        'regex' => new JsCustom('/^[0-9]*$/'),
                        'value' => $leieobjekt && $leieobjekt->hentAntRom() ? $leieobjekt->hentAntRom() : null
                    ]),
                    $this->app->vis(Checkbox::class, [
                        'name' => 'bad',
                        'fieldLabel' => 'Bad',
                        'boxLabel' => 'Tilgang på bad eller dusj',
                        'checked' => $leieobjekt && $leieobjekt->hentBad()
                    ]),
                    $this->app->vis(ComboBox::class, [
                        'name' => 'toalett_kategori',
                        'fieldLabel' => 'Toalettforhold',
                        'value' => $leieobjekt ? strval($leieobjekt->hentToalettKategori()) : null,
                        'remote' => false,
                        'allowBlank' => false,
                        'forceSelection' => true,
                        'dataValues' => (object)[
                            0 => 'Leieobjektet har ikke tilgang til eget toalett, eller har utedo',
                            1 => 'Det er tilgang til felles toalett i samme bygning/oppgang',
                            2 => 'Leieobjektet har eget toalett',
                        ]
                    ]),
                    $this->app->vis(Text::class, [
                        'name' => 'toalett',
                        'fieldLabel' => 'Spesifisering av toalettforhold',
                        'value' => $leieobjekt ? $leieobjekt->hentToalett() : null
                    ]),
                    $this->app->vis(ComboBox::class, [
                        'name' => 'leieberegning',
                        'fieldLabel' => 'Leieberegning',
                        'value' => $leieobjekt ? strval($leieobjekt->leieberegning) : null,
                        'remote' => false,
                        'allowBlank' => false,
                        'forceSelection' => true,
                        'dataValues' => $this->hentLeieberegningsmetoder()
                    ]),
                    $this->app->vis(TextArea::class, [
                        'name' => 'merknader',
                        'fieldLabel' => 'Merknader',
                        'value' => $leieobjekt ? $leieobjekt->hentMerknader() : null
                    ]),
                    $this->app->vis(File::class, [
                        'name' => 'bildefelt',
                        'fieldLabel' => 'Last opp bilde',
                        'tabIndex' => 3
                    ]),
                ]
            ]),
            'formActionUrl' => '/drift/index.php?oppslag=leieobjekt_skjema&id=' . ($leieobjekt && $leieobjekt->hentId() ? $leieobjekt->hentId() : '*') . '&oppdrag=ta_i_mot_skjema'
        ]);

        return parent::forberedData();
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    protected function hentBygninger(): stdClass
    {
        $resultat = (object)['' => ''];
        /** @var Bygning $bygning */
        foreach ($this->app->hentSamling(Bygning::class) as $bygning) {
            $resultat->{$bygning->hentId()} = "{$bygning->hentKode()}: {$bygning->hentNavn()}";
        }

        return $resultat;
    }

    /**
     * @return stdClass|array
     * @throws Exception
     */
    protected function hentEtasjevalg(): array
    {
        return ['', 'Loft', 'Kjeller', 'Sokkel', '1. etg.', '2. etg.', '3. etg.', '4. etg.', '5. etg.'];
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    protected function hentLeieberegningsmetoder(): stdClass
    {
        $resultat = new stdClass();
        /** @var Leieberegning $leieberegning */
        foreach ($this->app->hentSamling(Leieberegning::class) as $leieberegning) {
            $resultat->{$leieberegning->hentId()} = "{$leieberegning->hentNavn()}";
        }

        return $resultat;
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    protected function hentOmråder(?Leieobjekt $leieobjekt = null): stdClass
    {
        $resultat = new stdClass();

        if ($leieobjekt) {
            /** @var Område[] $områder */
            $områder = $leieobjekt->hentOmråder();
        }
        else {
            /** @var Sett $områder */
            $områder = $this->app->hentSamling(Område::class);
        }
        /** @var Område $område */
        foreach ($områder as $område) {
            $resultat->{$område->hentId()} = "{$område->hentNavn()}";
        }

        return $resultat;
    }
}