<?php

namespace Kyegil\Leiebasen\Visning\drift\js\poeng_skjema;


use Exception;
use Kyegil\Leiebasen\Modell\Poengprogram;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Skript-fil i drift
 *
 *  Ressurser:
 *      poeng \Kyegil\Leiebasen\Modell\Poengprogram\Poeng
 *      program \Kyegil\Leiebasen\Modell\Poengprogram
 *  Mulige variabler:
 *      $programKode
 *      $poengId
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\drift\js\poeng_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/js/poeng_skjema/Index.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return Index
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['program', 'poeng'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return Index
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Poengprogram\Poeng $poeng */
        $poeng = $this->hentRessurs('poeng');

        /** @var Poengprogram $program */
        $program = $poeng ? $poeng->program : $this->hentRessurs('program');

        $datasett = [
            'programKode' => $program ? $program->hentKode() : '',
            'poengId' => $poeng ? $poeng->hentId() : '',
            'skript' => '',
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}