<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\js\framleie_nye_personer_skjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleie;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Sett;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\html\person\Adressefelt;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $framleieforhold \Kyegil\Leiebasen\Modell\Leieforhold\Framleie
 *  Mulige variabler:
 *      $meny
 *      $tilbakeUrl
 *      $framleieforholdId
 *      $fornavnListe
 *      $etternavnListe
 *      $personListe
 *      $engangspolett
 *      $erOrg boolean
 *      $fornavn
 *      $etternavn
 *      $treffJsonArray
 *      $adressekortId
 * @package Kyegil\Leiebasen\Visning\drift\js\framleie_skjema
 */
class Trinn1ExtOnReady extends Drift
{
    protected $template = 'drift/js/framleie_nye_personer_skjema/Trinn1ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['framleieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Framleie|null $framleieforhold */
        $framleieforhold = $this->hentRessurs('framleieforhold');
        $framleieforholdId = $framleieforhold ? $framleieforhold->hentId() : '*';

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }

        $fornavnListe = $this->app->mysqli->arrayData([
            'distinct' => true,
            'source' => $this->app->mysqli->table_prefix . Person::hentTabell() . ' as ' . Person::hentTabell(),
            'fields' => ['navn' => 'fornavn']
        ])->data;

        $etternavnListe = $this->app->mysqli->arrayData([
            'distinct' => true,
            'source' => $this->app->mysqli->table_prefix . Person::hentTabell() . ' as ' . Person::hentTabell(),
            'fields' => ['navn' => 'etternavn']
        ])->data;

        $personListe = $this->app->mysqli->arrayData([
            'source' => $this->app->mysqli->table_prefix . Person::hentTabell() . ' as ' . Person::hentTabell(),
            'fields' => [
                'id' => 'personid',
                'navn' => 'TRIM(CONCAT(fornavn, " ", etternavn, " (", personid, ")"))'
            ],
            'orderfields' => 'etternavn, fornavn'
        ])->data;

        $this->definerData([
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'framleieforholdId' => $framleieforholdId,
            'tilbakeUrl' => $this->app->returi->get()->url,
            'fornavnListe' => json_encode($fornavnListe),
            'etternavnListe' => json_encode($etternavnListe),
            'personListe' => json_encode($personListe),
            'engangspolett' => $this->app->opprettPolett(),
            'treffJsonArray' => json_encode($this->finnAdressekortTreff()),
            'erOrg' => null,
            'fornavn' => null,
            'etternavn' => null
        ]);
        return parent::forberedData();
    }

    /**
     * Søker fram eksakte og mulige treff på adressekort
     * @return array
     * @throws Exception
     */
    protected function finnAdressekortTreff(): array
    {
        $fornavn = $this->hent('fornavn');
        $etternavn = $this->hent('etternavn');
        $erOrg = $this->hent('erOrg');
        $resultat = [];
        if($etternavn) {
            /** @var Sett $eksaktSøk */
            $eksaktSøk = $this->app->hentSamling(Person::class)->leggTilFilter([
                'er_org' => $erOrg,
                'etternavn' => $etternavn
            ]);
            if (!$erOrg) {
                $eksaktSøk->leggTilFilter(['fornavn' => $fornavn]);
            }
            /** @var Person|null $eksaktTreff */
            $eksaktTreff = $eksaktSøk->hentFørste();
            if ($eksaktTreff) {
                $resultat[] = (object)[
                    'prioritert' => true,
                    'personId' => $eksaktTreff->hentId(),
                    'erOrg' => $eksaktTreff->erOrg,
                    'navn' => $eksaktTreff->hentNavn(),
                    'personnr' => $eksaktTreff->personnr,
                    'fødselsdato' => $eksaktTreff->fødselsdato ? $eksaktTreff->fødselsdato->format('d.m.Y') : null,
                    'adresse' => (string)$this->app->vis(Adressefelt::class, ['person' => $eksaktTreff])
                ];
            }

            $muligheter = $this->app->hentSamling(Person::class);
            $etternavnArray = array_filter(explode(' ', str_replace('-', ' ', $etternavn)), function($del) {
                return strlen($del) > 2;
            });
            $etternavnFilter = ['or' => []];
            foreach($etternavnArray as $delEtternavn) {
                $etternavnFilter['or'][] = [
                    'etternavn LIKE' => "%{$delEtternavn}%"
                ];
            }
            $filter = $etternavnFilter;

            if(!$erOrg) {
                $fornavnArray = array_filter(explode(' ', str_replace('-', ' ', $fornavn)), function($del) {
                    return strlen($del) > 2;
                });
                $fornavnFilter = ['or' => []];
                foreach($fornavnArray as $delFornavn) {
                    $fornavnFilter['or'][] = [
                        'fornavn LIKE' => "%{$delFornavn}%"
                    ];
                }
                $filter = ['and' => [$filter, $fornavnFilter]];
            }
            $filter = ['or' => [$filter, [
                'CONCAT(fornavn, \' \', etternavn) LIKE' => trim("{$fornavn} {$etternavn}")
            ]]];
            $muligheter->leggTilFilter($filter);
            if ($eksaktTreff) {
                $muligheter->leggTilFilter(['personid !=' => $eksaktTreff->hentId()]);
            }

            foreach($muligheter as $mulighet) {
                $resultat[] = (object)[
                    'prioritert' => false,
                    'personId' => $mulighet->hentId(),
                    'erOrg' => $mulighet->erOrg,
                    'navn' => $mulighet->hentNavn(),
                    'personnr' => $mulighet->personnr,
                    'fødselsdato' => $mulighet->fødselsdato ? $mulighet->fødselsdato->format('d.m.Y') : null,
                    'adresse' => (string)$this->app->vis(Adressefelt::class, ['person' => $mulighet])
                ];
            }
        }
        return $resultat;
    }
}