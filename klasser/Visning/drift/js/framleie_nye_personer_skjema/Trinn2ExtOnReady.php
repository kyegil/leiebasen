<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\js\framleie_nye_personer_skjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleie;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $framleieforhold \Kyegil\Leiebasen\Modell\Leieforhold\Framleie
 *  Mulige variabler:
 *      $meny
 *      $tilbakeUrl
 *      $framleieforholdId
 *      $fornavnListe
 *      $etternavnListe
 *      $engangspolett
 * @package Kyegil\Leiebasen\Visning\drift\js\framleie_skjema
 */
class Trinn2ExtOnReady extends Drift
{
    protected $template = 'drift/js/framleie_nye_personer_skjema/Trinn2ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['framleieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Framleie|null $framleieforhold */
        $framleieforhold = $this->hentRessurs('framleieforhold');
        $framleieforholdId = $framleieforhold ? $framleieforhold->hentId() : '*';

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }

        $fornavnListe = $this->app->mysqli->arrayData([
            'distinct' => true,
            'source' => $this->app->mysqli->table_prefix . Person::hentTabell() . ' as ' . Person::hentTabell(),
            'fields' => ['navn' => 'fornavn']
        ])->data;

        $etternavnListe = $this->app->mysqli->arrayData([
            'distinct' => true,
            'source' => $this->app->mysqli->table_prefix . Person::hentTabell() . ' as ' . Person::hentTabell(),
            'fields' => ['navn' => 'etternavn']
        ])->data;

        $this->definerData([
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'framleieforholdId' => $framleieforholdId,
            'tilbakeUrl' => $this->app->returi->get()->url,
            'fornavnListe' => json_encode($fornavnListe),
            'etternavnListe' => json_encode($etternavnListe),
            'engangspolett' => $this->app->opprettPolett()
        ]);
        return parent::forberedData();
    }
}