<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\js\regnskapsprosjekt_kort;


use DateInterval;
use DateTime;
use Exception;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Regnskapsprosjekt;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\JsonStore;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\Store;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\ComboBox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Date;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Display;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\grid\feature\GroupingSummary;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\grid\Panel as GridPanel;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\panel\Panel;
use Kyegil\ViewRenderer\JsArray;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $prosjekt \Kyegil\Leiebasen\Modell\Regnskapsprosjekt
 *  Mulige variabler:
 *      $prosjektId
 *      $meny
 *      $skjema
 *      $formActionUrl
 * @package Kyegil\Leiebasen\Visning\drift\js\regnskapsprosjekt_skjema
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/regnskapsprosjekt_kort/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['prosjekt'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Regnskapsprosjekt|null $prosjekt */
        $prosjekt = $this->hentRessurs('prosjekt');

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }

        /**
         * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.Field
         */
        $dataFields = [
            (object)['name' => 'delkrav', 'type' => 'int', 'useNull' => true],
            (object)['name' => 'krav', 'type' => 'int'],
            (object)['name' => 'dato', 'type' => 'date', 'useNull' => true, 'dateFormat' => 'Y-m-d'],
            (object)['name' => 'beskrivelse'],
            (object)['name' => 'beløp', 'type' => 'float'],
            (object)['name' => 'leieforhold', 'type' => 'int'],
            (object)['name' => 'leieforhold_beskrivelse'],
            (object)['name' => 'leieobjekt', 'type' => 'int'],
            (object)['name' => 'leieobjekt_beskrivelse'],
            (object)['name' => 'bygning', 'type' => 'int'],
            (object)['name' => 'bygning_beskrivelse'],
            (object)['name' => 'strømanlegg'],
            (object)['name' => 'segment'],
            (object)['name' => 'kravtype'],
            (object)['name' => 'måned'],
            (object)['name' => 'måned_formatert'],
            (object)['name' => 'uke'],
            (object)['name' => 'uke_formatert'],
        ];
        /**
         * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
         */
        $columns = [
            (object)['dataIndex' => 'dato', 'text' => 'Dato', 'width' => 80, 'align' => 'right', 'renderer' => new JsCustom("Ext.util.Format.dateRenderer('d.m.Y')")],
            (object)['dataIndex' => 'leieforhold', 'width' => 320, 'text' => 'Leieforhold', 'renderer' => new JsFunction("return record.get('leieforhold_beskrivelse');", ['value', 'metaData', 'record'])],
            (object)['dataIndex' => 'beskrivelse', 'width' => 350, 'text' => 'Hva'],
            (object)['dataIndex' => 'beløp', 'text' => 'Beløp', 'width' => 100, 'align' => 'right', 'summaryType' => 'sum', 'renderer' => new JsCustom("Ext.util.Format.noMoney")],
        ];

        /** @var JsonStore $resultatStore */
        $resultatStore = $this->app->vis(JsonStore::class, [
            'url' => "/drift/index.php?oppslag=regnskapsprosjekt_kort&id={$prosjekt->hentId()}&oppdrag=hent_data&data=rapport_json",
            'configObjectOnly' => false,
            'id' => 'resultatStore',
            'fields' => $dataFields,
            'groupField' => $this->hentGroupField($prosjekt)
        ]);

        /** @var GridPanel $resultatPanel */
        $resultatPanel = $this->app->vis(GridPanel::class, [
            'configObjectOnly' => false,
            'renderTo' => null,
            'width' => '100%',
            'columns' => $columns,
            'sortableColumns' => false,
            'store' => $resultatStore,
            'features' => [
                $this->app->vis(GroupingSummary::class, [
                    'startCollapsed' => true,
                    'enableGroupingMenu' => false,
                ])
            ]
        ]);


        $this->definerData([
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'prosjektId' => $prosjekt->hentId(),
            'tidsromVelger' => $this->app->vis(ComboBox::class, [
                'configObjectOnly' => false,
                'name' => 'tidsrom',
                'emptyText' => 'Velg tidsrom',
                'width' => 200,
                'store' => $this->app->vis(Store::class, [
                    'fields' => [
                        'value',
                        'text',
                        /** @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.Field */
                        (object)['name' => 'fom', 'type' => 'date', 'dateFormat' => 'Y-m-d', 'useNull' => true],
                        (object)['name' => 'tom', 'type' => 'date', 'dateFormat' => 'Y-m-d', 'useNull' => true]
                    ],
                    'data' => $this->hentTidsrom()
                ])
            ]),
            'fomFelt' => $this->app->vis(Date::class, [
                'configObjectOnly' => false,
                'name' => 'fom',
                'emptyText' => 'Fra og med',
                'allowBlank' => false,
                'width' => 100
            ]),
            'tomFelt' => $this->app->vis(Date::class, [
                'configObjectOnly' => false,
                'name' => 'tom',
                'emptyText' => 'til og med',
                'allowBlank' => false,
                'width' => 100
            ]),
            'resultatPanel' => $resultatPanel,
            'kort' => $this->app->vis(Panel::class, [
                'id' => 'regnskapsprosjekt_kort',
                'configObjectOnly' => false,
                'title' => $prosjekt->navn,
                'returnUrl' => (string)$this->app->returi->get(),
                'tbar' => new JsArray(['tidsromVelger', 'fomFelt', 'tomFelt']),
                'items' => [
                    $this->app->vis(Display::class, [
                        'value' => $prosjekt->beskrivelse
                    ]),
                    new JsCustom('resultatPanel')
                ]
            ])
        ]);

        return parent::forberedData();
    }

    /**
     * Henter Data for tidsrom-velgeren
     *
     * @return stdClass[]
     */
    protected function hentTidsrom(): array
    {
        $resultat = [];
        $iDag = new DateTime();

        // Siste 7 dager
        $fom = new DateTime();
        $tom = new DateTime();
        $fom->sub(new DateInterval('P7D'));
        $resultat[] = (object)[
            'value' => 'siste_7_dager',
            'text' => 'Siste 7 dager',
            'fom' => $fom->format('Y-m-d'),
            'tom' => $tom->format('Y-m-d')
        ];

        // Inneværende måned
        $resultat[] = (object)[
            'value' => 'inneværende_mnd',
            'text' => \IntlDateFormatter::formatObject($iDag, 'MMMM y', 'nb_NO'),
            'fom' => $iDag->format('Y-m-01'),
            'tom' => $iDag->format('Y-m-t')
        ];

        // Forrige måned
        $fom = new DateTime();
        $fom->sub(new DateInterval('P1M'));

        $resultat[] = (object)[
            'value' => 'siste_1_mnd',
            'text' => \IntlDateFormatter::formatObject($fom, 'MMMM y', 'nb_NO'),
            'fom' => $fom->format('Y-m-01'),
            'tom' => $fom->format('Y-m-t')
        ];

        // Forrige år
        $fom = new DateTime();
        $fom->sub(new DateInterval('P1Y'));

        $resultat[] = (object)[
            'value' => 'siste_1_år',
            'text' => $fom->format('Y'),
            'fom' => $fom->format('Y-01-01'),
            'tom' => $fom->format('Y-12-31')
        ];

        return $resultat;
    }

    /**
     * @param $felt
     * @return stdClass
     */
    protected function hentDataFeltKonfig($felt): stdClass
    {
        $dataField = (object)[
            'name' => $felt->navn,
        ];
        if ($felt->type == 'heltall') {
            $dataField->type = 'int';
        }
        if ($felt->type == 'desimal') {
            $dataField->type = 'float';
        }
        if ($felt->type == 'dato') {
            $dataField->type = 'date';
            $dataField->dateFormat = 'Y-m-d';
            $dataField->useNull = true;
        }
        return $dataField;
    }

    /**
     * @param Regnskapsprosjekt $prosjekt
     * @return string|null
     * @throws Exception
     */
    private function hentGroupField(Regnskapsprosjekt $prosjekt): ?string
    {
        $segmentering = $prosjekt->hentSegmentering();
        switch ($segmentering) {
            case '':
                return null;
            default:
                return $segmentering;
        }
    }
}