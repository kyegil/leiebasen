<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\js\krav_kort;


use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $krav \Kyegil\Leiebasen\Modell\Leieforhold\Krav
 *      $kredittkopling
 *  Mulige variabler:
 *      $kravId
 *      $kredittkoplingId
 *      $tilbakeKnapp
 * @package Kyegil\Leiebasen\Visning\drift\js\krav_kort
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/krav_kort/ExtOnReady.js';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['bruker', 'krav', 'kredittkopling'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Krav|null $krav */
        $krav = $this->hentRessurs('krav');

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }
        if($krav) {
            /** @var Innbetaling|null $kredittkopling */
            $kredittkopling = $this->hentRessurs('kredittkopling') ?: $krav->hentKredittkopling();
            $this->definerData([
                'kravId' => $krav->hentId(),
                'kredittkoplingId' => strval($kredittkopling)
            ]);
        }
        if($kredittkopling) {
            $this->definerData([
                'kredittkoplingId' => $kredittkopling->hentId()
            ]);
        }

        $this->definerData([
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'tilbakeUrl' => $this->app->returi->get()->url,
        ]);
        return parent::forberedData();
    }
}