<?php

namespace Kyegil\Leiebasen\Visning\drift\js\fs_anlegg_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Checkbox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\ComboBox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Hidden;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Text;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\TextArea;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel as FormPanel;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $strømanlegg \Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg|null
 *      $leiebasenJsObjektEgenskaper stdClass
 *  Mulige variabler:
 *      $tjenesteId
 *      $meny
 *      $initLeiebasenJsObjektEgenskaper
 *      $skjema
 *      $formActionUrl
 * @package Kyegil\Leiebasen\Visning\drift\js\fs_anlegg_skjema
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/fs_anlegg_skjema/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['strømanlegg'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        if ($attributt == 'leiebasenJsObjektEgenskaper') {
            return $this->settLeiebasenJsObjektEgenskaper($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param stdClass $leiebasenJsObjektEgenskaper
     * @return $this
     */
    public function settLeiebasenJsObjektEgenskaper(stdClass $leiebasenJsObjektEgenskaper): Drift
    {
        return $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var stdClass $leiebasenJsObjektEgenskaper */
        $leiebasenJsObjektEgenskaper = $this->hentRessurs('leiebasenJsObjektEgenskaper');
        if (!$leiebasenJsObjektEgenskaper) {
            $leiebasenJsObjektEgenskaper = new stdClass();
            $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
        }
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Strømanlegg|null $strømanlegg */
        $strømanlegg = $this->hentRessurs('strømanlegg');
        $tjenesteId = $strømanlegg && $strømanlegg->hentId() ? $strømanlegg->hentId() : '*';

        $leiebasenJsObjektEgenskaper->skjema = (object)[
            'felter' => (object)[]
        ];

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }

        /** @var ComboBox $leieforholdFelt */
        $kravtypeFelt = $this->app->vis(Hidden::class, [
            'name' => 'type',
            'value' => Krav::TYPE_STRØM
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->type = $kravtypeFelt;

        $navnFelt = $this->app->vis(Text::class, [
            'name' => 'navn',
            'fieldLabel' => 'Navn',
            'allowBlank' => false,
            'value' => $strømanlegg ? $strømanlegg->hentNavn() : null
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->navn = $navnFelt;

        $leverandørFelt = $this->app->vis(Text::class, [
            'name' => 'leverandør',
            'fieldLabel' => 'Strømleverandør',
            'value' => $strømanlegg ? $strømanlegg->hentLeverandør() : null
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->leverandør = $leverandørFelt;

        $anleggsnummerFelt = $this->app->vis(Text::class, [
            'name' => 'avtalereferanse',
            'fieldLabel' => 'Anleggsnummer',
            'allowBlank' => false,
            'value' => $strømanlegg ? $strømanlegg->hentAnleggsnummer() : null
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->anleggsnummer = $anleggsnummerFelt;

        $målernummerFelt = $this->app->vis(Text::class, [
            'name' => 'målernummer',
            'fieldLabel' => 'Målernummer',
            'value' => $strømanlegg ? $strømanlegg->hentMålernummer() : null
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->målernummer = $målernummerFelt;

        $målerplasseringFelt = $this->app->vis(Text::class, [
            'name' => 'plassering',
            'fieldLabel' => 'Måler befinner seg',
            'value' => $strømanlegg ? $strømanlegg->hentPlassering() : null
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->plassering = $målerplasseringFelt;

        $formålFelt = $this->app->vis(TextArea::class, [
            'name' => 'beskrivelse',
            'fieldLabel' => 'Brukes til',
            'value' => $strømanlegg ? $strømanlegg->hentFormål() : null,
            'height' => 200
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->formål = $formålFelt;

        $forbruksregistreringsFelt = $this->app->vis(Checkbox::class, [
            'name' => 'registrer_forbruk',
            'fieldLabel' => 'Registrer forbruk',
            'boxLabel' => 'Registrer strømforbruk dersom forbruket vises på faktura',
            'value' => !$strømanlegg || $strømanlegg->hentRegistrerForbruk()
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->registrerForbruk = $forbruksregistreringsFelt;

        $forbruksenhetFelt = $this->app->vis(Text::class, [
            'name' => 'forbruksenhet',
            'fieldLabel' => 'Forbruksenhet',
            'value' => $strømanlegg ? $strømanlegg->hentForbruksenhet() : 'kWh'
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->forbruksenhet = $forbruksenhetFelt;

        $this->definerData([
            'tjenesteId' => $tjenesteId,
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'initLeiebasenJsObjektEgenskaper' => $this->gjengiLeiebasenJsObjektEgenskaper(),
            'skjema' => $this->app->vis(FormPanel::class, [
                'id' => 'skjema',
                'configObjectOnly' => false,
                'title' => $strømanlegg ? $strømanlegg->hentNavn() : 'Registrer nytt strømanlegg',
                'enabledSaveButton' => true,
                'formActionUrl' => '/drift/index.php?oppslag=delte_kostnader_tjeneste_skjema&id=' . ($strømanlegg && $strømanlegg->hentId() ? $strømanlegg->hentId() : '*') . '&oppdrag=ta_imot_skjema',
                'returnUrl' => $this->app->returi->get(),
                'formSourceUrl' => '/drift/index.php?oppslag=delte_kostnader_tjeneste_skjema&id=' . ($strømanlegg && $strømanlegg->hentId() ? $strømanlegg->hentId() : '*') . '&oppdrag=hent_data',
                'items' => [
                    new JsCustom('leiebasen.skjema.felter.type'),
                    new JsCustom('leiebasen.skjema.felter.navn'),
                    new JsCustom('leiebasen.skjema.felter.leverandør'),
                    new JsCustom('leiebasen.skjema.felter.anleggsnummer'),
                    new JsCustom('leiebasen.skjema.felter.målernummer'),
                    new JsCustom('leiebasen.skjema.felter.plassering'),
                    new JsCustom('leiebasen.skjema.felter.formål'),
                    new JsCustom('leiebasen.skjema.felter.registrerForbruk'),
                    new JsCustom('leiebasen.skjema.felter.forbruksenhet'),
                    new JsCustom('leiebasen.skjema.felter.person'),
                ]
            ]),
            'formActionUrl' => '/drift/index.php?oppslag=delte_kostnader_tjeneste_skjema&id=' . $tjenesteId . '&oppdrag=ta_i_mot_skjema'
        ]);

        return parent::forberedData();
    }
}