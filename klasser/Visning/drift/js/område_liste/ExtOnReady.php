<?php

namespace Kyegil\Leiebasen\Visning\drift\js\område_liste;


use Exception;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $bruker \Kyegil\Leiebasen\Modell\Person
 *  Mulige variabler:
 *      $brukerId
 *      $tilbakeKnapp
 * @package Kyegil\Leiebasen\Visning\drift\js\områder_liste
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/område_liste/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'bruker') {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $bruker = $this->hentRessurs('bruker');
        $brukerId = strval($bruker);

        $this->definerData([
            'meny' => '',
            'tilbakeUrl' => $this->app->returi->get()->url
        ]);
        return parent::forberedData();
    }
}