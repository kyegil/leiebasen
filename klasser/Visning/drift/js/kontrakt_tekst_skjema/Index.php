<?php

namespace Kyegil\Leiebasen\Visning\drift\js\kontrakt_tekst_skjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\AvtaleMal;
use Kyegil\Leiebasen\Modell\Leieforhold\AvtaleMalsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Visning\Drift;
use stdClass;

/**
 * Skript-fil i drift
 *
 * Ressurser:
 * * kontrakt \Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt
 *
 * Tilgjengelige variabler:
 * * $kontraktId
 * * $leieforholdId
 * * $avtalemaler
 * * $skript
 *
 * @package Kyegil\Leiebasen\Visning\drift\js\kontrakt_tekst_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/js/kontrakt_tekst_skjema/Index.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Index
    {
        if(in_array($attributt,['kontrakt'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Kontrakt|null $kontrakt */
        $kontrakt = $this->hentRessurs('kontrakt');

        $avtaleMaler = $this->hentAvtaleMaler($kontrakt ? $kontrakt->leieforhold : null);

        $datasett = [
            'kontraktId' => $kontrakt ? $kontrakt->hentId() : '',
            'leieforholdId' => $kontrakt && $kontrakt->leieforhold ? $kontrakt->leieforhold->hentId() : '',
            'avtalemaler' => json_encode($avtaleMaler),
            'skript' => '',
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    private function hentAvtaleMaler(?Leieforhold $leieforhold)
    {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $maler = new stdClass();

        if($leieforhold && $leieforhold->avtaletekstmal) {
            $maler->{'0'} = $leieforhold->avtaletekstmal;
        }

        /** @var AvtaleMalsett $malsett */
        $malsett = $this->app->hentSamling(AvtaleMal::class);
        $malsett->leggTilFilter(['type' => AvtaleMal::TYPE_LEIEAVTALE]);
        foreach ($malsett as $mal) {
            $maler->{$mal->hentId()} = $mal->mal;
        }

        return $this->app->post($this, __FUNCTION__, $maler, $args);
    }
}