<?php

namespace Kyegil\Leiebasen\Visning\drift\js\massemelding_skjema;


use Exception;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Skript-fil i drift
 *
 *  Ressurser:
 *
 *  Mulige variabler:
 *
 * @package Kyegil\Leiebasen\Visning\drift\js\massemelding_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/js/massemelding_skjema/Index.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,[])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $datasett = [
            'skript' => '',
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}