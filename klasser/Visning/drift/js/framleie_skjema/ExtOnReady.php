<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\js\framleie_skjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleie;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Adressefelt as LeieforholdAdressefelt;
use Kyegil\Leiebasen\Visning\felles\html\leieobjekt\Adressefelt as LeieobjektAdressefelt;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $framleieforhold \Kyegil\Leiebasen\Modell\Leieforhold\Framleie
 *      $leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $framleieforholdId
 *      $leieforholdId
 *      $leieforholdBeskrivelse
 *      $leieforholdStartdato
 *      $leieforholdTildato
 *      $leieforholdJsonData
 *      $tilbakeKnapp
 * @package Kyegil\Leiebasen\Visning\drift\js\framleie_skjema
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/framleie_skjema/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['framleieforhold', 'leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Framleie|null $framleieforhold */
        $framleieforhold = $this->hentRessurs('framleieforhold');
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $framleieforhold ? $framleieforhold->leieforhold : $this->hentRessurs('leieforhold');
        $framleieforholdId = $framleieforhold ? $framleieforhold->hentId() : '*';
        $leietakere = [];
        $leieforholdEpostadresser = [];
        $leieforholdTelefonnumre = [];

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }
        if ($leieforhold) {
            $leietakersett = $leieforhold->hentLeietakere();
            $antallLeietakere = $leietakersett->hentAntall();
            /** @var Leieforhold\Leietaker $leietaker */
            foreach ($leietakersett as $leietaker) {
                $fødselsdato = $leietaker->person && $leietaker->person->fødselsdato ? $leietaker->person->fødselsdato->format('d.m.Y') : '';
                $leietakere[] = $leietaker->hentNavn() . ($fødselsdato ? " f. {$fødselsdato}" : '');
                if ($leietaker->person) {
                    $telefonnummer = $leietaker->person->telefon ?: $leietaker->person->mobil;
                    if ($telefonnummer) {
                        $leieforholdTelefonnumre[] = $antallLeietakere > 1
                            ? "{$leietaker->person->fornavn}: {$telefonnummer}"
                            : $telefonnummer;
                    }
                    if ($leietaker->person->epost) {
                        $leieforholdEpostadresser[] = $antallLeietakere > 1
                        ? "{$leietaker->person->fornavn}: {$leietaker->person->epost}"
                        : $leietaker->person->epost;
                    }
                }
            }
        }

        $this->definerData([
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'framleieforholdId' => $framleieforholdId,
            'tilbakeUrl' => $this->app->returi->get()->url,
            'leieforholdId' => $leieforhold ? $leieforhold->hentId() : '',
            'leieforholdBeskrivelse' => $leieforhold ? $leieforhold->hentBeskrivelse() : '',
            'leieforholdStartdato' => $leieforhold ? $leieforhold->fradato->format('d.m.Y') : '',
            'leieforholdTildato' => $leieforhold && $leieforhold->tildato ? $leieforhold->tildato->format('d.m.Y') : '',
            'leieforholdJsonData' => json_encode([
                'leieforhold' => $leieforhold ? $leieforhold->hentId() : '',
                'visningsfelt' => $leieforhold ? $leieforhold->hentBeskrivelse() : '',
                'startdato' => $leieforhold ? $leieforhold->fradato->format('Y-m-d') : '',
                'tildato' => $leieforhold && $leieforhold->tildato ? $leieforhold->tildato->format('Y-m-d') : '',
                'leieobjektnr' => $leieforhold ? $leieforhold->leieobjekt->id : '',
                'kid' => $leieforhold ? $leieforhold->hentKid() : '',
                'er_bofellesskap' => $leieforhold ? $leieforhold->erBofellesskap() : false,
                'husleie' => $leieforhold ? $leieforhold->leiebeløp : '...',
                'kontaktadresse' => $leieforhold ? (string)$this->app->vis(LeieforholdAdressefelt::class, ['leieforhold' => $leieforhold]) : '',
                'boligadresse' => $leieforhold ? (string)$this->app->vis(LeieobjektAdressefelt::class, ['leieobjekt' => $leieforhold->leieobjekt]) : '',
                'leietakere' => implode('<br>', $leietakere),
                'telefon' => implode(', ', $leieforholdTelefonnumre),
                'epost' => implode(', ', $leieforholdEpostadresser)
            ])
        ]);
        return parent::forberedData();
    }
}