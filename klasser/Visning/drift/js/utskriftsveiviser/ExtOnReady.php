<?php

namespace Kyegil\Leiebasen\Visning\drift\js\utskriftsveiviser;


use DateInterval;
use DateTime;
use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\extjs4\Ext\form\flexi_checkbox_groups\Leieforhold as LeieforholdCheckboxGroup;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\button\Button;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\CheckboxGroup;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Checkbox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Date;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\TextArea;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel as FormPanel;
use Kyegil\ViewRenderer\JsArray;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      inkLeieforhold int[]
 *      eksLeieforhold int[]
 *      forfallFramTil DateTime
 *      kravtyper string[]
 *      adskilt bool
 *  Mulige variabler:
 *      $meny
 *      $skjema
 *      $formActionUrl
 *      $utskriftAlleredeIgangsatt bool
 * @package Kyegil\Leiebasen\Visning\drift\js\utskriftsveiviser
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/utskriftsveiviser/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): ExtOnReady
    {
        if(in_array($attributt, ['inkLeieforhold','eksLeieforhold','forfallFramTil','kravtyper','adskilt'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        try {
            $fakturaTidsrom = new DateInterval(strval($this->app->hentValg('automatisk_faktura_tidsrom')));
        } catch (Exception $e) {
            $fakturaTidsrom = new DateInterval('P1M');
        }
        /** @var Person|null $bruker */
        $bruker         = $this->hentRessurs('bruker');
        /** @var int[] $inkLeieforhold */
        $inkLeieforhold = $this->hentRessurs('inkLeieforhold') ?? [];
        /** @var int[] $eksLeieforhold */
        $eksLeieforhold = $this->hentRessurs('eksLeieforhold') ?? [];
        $adskilt = (bool)$this->hentRessurs('adskilt');
        /** @var DateTime $forfallFramTil */
        $forfallFramTil = $this->hentRessurs('forfallFramTil');
        if (!$forfallFramTil) {
            $forfallFramTil = new DateTime();
            $forfallFramTil->add($fakturaTidsrom);
        }
        $kravTyperDataValues = $this->hentKravTyperDataValues();
        /** @var string[] $kravtyper */
        $kravtyper = $this->hentRessurs('kravtyper') ?: array_keys(get_object_vars($kravTyperDataValues));
        /** @var string $girotekst */
        $girotekst = $this->app->hentValg('girotekst') ?? '';

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }

        $inklLeieforholdVelger = $this->app->vis(LeieforholdCheckboxGroup::class, [
            'submitAsArray' => true,
            'itemId' => 'ink_leieforhold',
            'name' => 'ink_leieforhold',
            'fieldLabel' => 'Inkluder leieforhold',
            'labelWidth' => 200,
            'emptyText' => 'Søk opp leieforhold (minst 4 tegn) å begrense utskriften til, eller hold blank for å inkludere alle leieforhold',
            'value' => $this->hentLeieforholdDataValues($inkLeieforhold)
        ]);

        $eksLeieforholdVelger = $this->app->vis(LeieforholdCheckboxGroup::class, [
            'submitAsArray' => true,
            'itemId' => 'eks_leieforhold',
            'name' => 'eks_leieforhold',
            'fieldLabel' => 'Utelat leieforhold',
            'labelWidth' => 200,
            'emptyText' => 'Søk opp evt leieforhold (minst 4 tegn) som skal utelates fra utskriften',
            'value' => $this->hentLeieforholdDataValues($eksLeieforhold)
        ]);

        $tildatoFelt = $this->app->vis(Date::class, [
            'itemId' => 'tildato',
            'name' => 'tildato',
            'fieldLabel' => 'Inkluder krav med forfall fram til',
            'labelWidth' => 200,
            'format' => 'd.m.Y',
            'value' => $forfallFramTil,
        ]);

        $kravtyperFelt = $this->app->vis(CheckboxGroup::class, [
            'itemId' => 'kravtyper',
            'name' => 'kravtyper',
            'fieldLabel' => 'Kravtyper',
            'labelWidth' => 200,
            'dataValues' => $kravTyperDataValues,
            'value' => $kravtyper,
            'submitAsArray' => true
        ]);

        $adskiltFelt = $this->app->vis(Checkbox::class, [
            'itemId' => 'adskilt',
            'name' => 'adskilt',
            'fieldLabel' => 'Adskilte kravtyper',
            'labelWidth' => 200,
            'boxLabel' => 'Separate regninger for de ulike kravtypene',
            'value' => $adskilt,
        ]);

        $girotekstFelt = $this->app->vis(TextArea::class, [
            'itemId' => 'girotekst',
            'name' => 'girotekst',
            'fieldLabel' => 'Tekst på regninger',
            'labelWidth' => 200,
            'value' => $girotekst,
        ]);

        $formActionUrl = '/drift/index.php?oppslag=utskriftsveiviser_regninger';
        $this->definerData([
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'skjema' => $this->app->vis(FormPanel::class, [
                'configObjectOnly' => false,
                'id' => 'utskriftskjema',
                'standardSubmit' => true,
                'defaults' => (object)[
                    'width' => 600,
                ],
                'title' => 'Velg filtrering for hvilke krav som skal inkluderes i utskriften',
                'enabledSaveButton' => true,
                'formActionUrl' => $formActionUrl,
                'returnUrl' => $this->app->returi->get(),
                'items' => new JsArray([
                    $inklLeieforholdVelger,
                    $eksLeieforholdVelger,
                    $tildatoFelt,
                    $kravtyperFelt,
                    $adskiltFelt,
                    $girotekstFelt
                ]),
                'cancelButton' => $this->app->vis(Button::class, [
                    'text' => 'Avbryt',
                    'handler' => new JsFunction("window.location = '{$this->app->returi->get()}'")
                ]),
                'saveButton' => $this->app->vis(Button::class, [
                    'text' => 'Fortsett',
                    'handler' => new JsFunction("leiebasen.fortsett();")
                ]),
            ]),
            'formActionUrl' => $formActionUrl,
            'utskriftAlleredeIgangsatt' => (bool)$this->app->hentValg('utskriftsforsøk'),
        ]);

        return parent::forberedData();
    }

    /**
     * @return object
     * @throws Exception
     */
    private function hentKravTyperDataValues(): object
    {
        $resultat = (object)[
            mb_strtolower(Krav::TYPE_HUSLEIE) => Krav::TYPE_HUSLEIE,
            mb_strtolower(Krav::TYPE_STRØM) => Krav::TYPE_STRØM,
        ];
        $delkravTyper = $this->app->hentDelkravtyper();
        foreach ($delkravTyper as $delkravtype) {
            if ($delkravtype->selvstendigTillegg) {
                $resultat->{$delkravtype->kode} = $delkravtype->navn . '<br>(Tillegg til ' . $delkravtype->kravtype . ')';
            }
        }
        $resultat->{mb_strtolower(Krav::TYPE_ANNET)} = Krav::TYPE_ANNET;
        return $resultat;
    }

    /**
     * @param int[] $leieforholdIder
     * @return stdClass
     * @throws Exception
     */
    private function hentLeieforholdDataValues(array $leieforholdIder): stdClass
    {
        $resultatObjekt = new stdClass();
        /** @var Leieforholdsett $leieforholdsett */
        $leieforholdsett = $this->app->hentSamling(Leieforhold::class)->filtrerEtterIdNumre($leieforholdIder);
        foreach ($leieforholdsett as $leieforhold) {
            $resultatObjekt->{$leieforhold->hentId()} = $leieforhold->hentId() . ' ' . $leieforhold->hentBeskrivelse();
        }
        return $resultatObjekt;
    }
}