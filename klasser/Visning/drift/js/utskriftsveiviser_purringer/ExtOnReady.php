<?php

namespace Kyegil\Leiebasen\Visning\drift\js\utskriftsveiviser_purringer;


use DateInterval;
use DateTime;
use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Purring;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\button\Button;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Checkbox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Display;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Hidden;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FieldContainer;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel as FormPanel;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\RadioGroup;
use Kyegil\ViewRenderer\JsArray;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $bruker \Kyegil\Leiebasen\Modell\Person
 *      $kravsett \Kyegil\Leiebasen\Modell\Leieforhold\Kravsett
 *      $ubetalteKrav \Kyegil\Leiebasen\Modell\Leieforhold\Kravsett
 *  Mulige variabler:
 *      $brukerId
 *      $skjema \Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel
 *      $adskilt bool
 *      $tilbakeKnapp
 *      $utskriftAlleredeIgangsatt bool
 * @package Kyegil\Leiebasen\Visning\drift\js\utskriftsveiviser_purringer
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/utskriftsveiviser_purringer/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): ExtOnReady
    {
        if ($attributt == 'kravsett') {
            return $this->settKravsett($verdi);
        }
        if ($attributt == 'ubetalteKrav') {
            return $this->settUbetalteKrav($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Kravsett $kravsett
     * @return $this
     */
    private function settKravsett(Kravsett $kravsett): ExtOnReady
    {
        return $this->settRessurs('kravsett', $kravsett);
    }

    /**
     * @param Kravsett $ubetalteKrav
     * @return $this
     */
    private function settUbetalteKrav(Kravsett $ubetalteKrav): ExtOnReady
    {
        return $this->settRessurs('ubetalteKrav', $ubetalteKrav);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): ExtOnReady
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Kravsett|null $kravsett */
        $kravsett = $this->hentRessurs('kravsett');
        /** @var Kravsett|null $ubetalteKrav */
        $ubetalteKrav = $this->hentRessurs('ubetalteKrav');

        $skjemaFelter = new JsArray();

        $ubetalteKravBunter = $this->buntPurringer($ubetalteKrav);

        if ($kravsett) {
            foreach ($kravsett as $krav) {
                $kravId = $krav->hentId();
                $skjemaFelter->addItem($this->app->vis(Hidden::class, [
                    'name' => 'krav[]',
                    'itemId' => 'krav-' . $kravId,
                    'value' => $kravId
                ]));
            }
        }

        /**
         * Velg felles eller separate purringer
         */
        $skjemaFelter->addItem($this->app->vis(Checkbox::class, [
            'boxLabel' => 'Send én felles purring for hvert leieforhold.',
            'checked' => true,
            'fieldLabel' => 'Kombinerte purringer',
            'name' => 'kombipurring',
            'inputValue' => 1
        ]));

        /** @var stdClass $leieforholdHolder */
        foreach($ubetalteKravBunter as $leieforholdHolder) {
            $skjemaFelter->addItem($this->hentLeieforholdFeltsett(
                $leieforholdHolder->leieforhold,
                $leieforholdHolder->oppfølgingsstopp,
                $leieforholdHolder->purres,
                $leieforholdHolder->purreformat,
                $leieforholdHolder->formatvalg,
                $leieforholdHolder->oppfølgingsfrist,
                $leieforholdHolder->regninger,
            ));
        }


        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }

        $cancelButton = $this->app->vis(Button::class, [
            'text' => 'Avbryt',
            'handler' => new JsFunction("window.location = '" . addslashes($this->app->returi->get()) . "';")
        ]);
        $saveButton = $this->app->vis(Button::class, [
            'text' => 'Lag utskrift for valgte regninger og purringer',
            'handler' => new JsFunction('leiebasen.skrivUt();')
        ]);

        $this->definerData([
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'formActionUrl' => '/drift/index.php?oppslag=utskriftsveiviser&oppdrag=oppgave&oppgave=opprett_utskrift',
            'skjema' => $this->app->vis(FormPanel::class, [
                'id' => 'skjema',
                'configObjectOnly' => false,
                'title' => 'Velg hvilke regninger/krav som skal purres',
                'enabledSaveButton' => true,
                'formActionUrl' => '/drift/index.php?oppslag=utskriftsveiviser&oppdrag=oppgave&oppgave=opprett_utskrift',
                'returnUrl' => $this->app->returi->get(),
                'items' => $skjemaFelter,
                'cancelButton' => $cancelButton,
                'saveButton' => $saveButton,
            ]),
            'tilbakeUrl' => $this->app->returi->get()->url,
            'utskriftAlleredeIgangsatt' => (bool)$this->app->hentValg('utskriftsforsøk'),
        ]);

        return parent::forberedData();
    }

    /**
     * Bunter kravene sammen i regninger og leieforhold, og trekker ut relevant data.
     *
     * Returnerer array av objekter med følgende egenskaper:
     * * Leieforhold leieforhold
     * * boolean purres Leieforholdet foreslåes purret dersom minst én av regningene er moden for det
     * * string purreformat Leieforholdets foretrukne regningsformat
     * * string formatvalg Anbefalt purreformat. Dette kan avvike fra leieforholdets foretrukne regningsformat pga innstillinger
     * * boolean oppfølgingsstopp Leieforholdet har uendelig eller midlertidig oppfølgingsstopp
     * * \DateTime|null oppfølgingsfrist Oppfølgingsstoppen avsluttes (dersom midlertidig)
     * * stdClass[] regninger
     * > * Regning regning Regningsmodellen
     * > * string regningstekst Oppsummering av kravene i regningen
     *
     * @param Kravsett $ubetalteKrav
     * @return stdClass[]
     * @throws Exception
     */
    private function buntPurringer(Kravsett $ubetalteKrav): array
    {
        /** @var object[] $bunter */
        $bunter = [];
        $regningsBeskrivelser = $this->hentRegningsbeskrivelser($ubetalteKrav);
        foreach ($ubetalteKrav as $krav) {
            $leieforhold = $krav->leieforhold;
            $leieforholdId = $leieforhold->hentId();
            $regning = $krav->regning;

            settype($bunter[$leieforholdId], 'object');
            settype($bunter[$leieforholdId]->purres, 'boolean');
            settype($bunter[$leieforholdId]->regninger, 'array');
            settype($bunter[$leieforholdId]->oppfølgingsstopp, 'boolean');

            $bunter[$leieforholdId]->oppfølgingsfrist = $leieforhold->avventOppfølging > date_create() ? $leieforhold->avventOppfølging : null;
            $bunter[$leieforholdId]->oppfølgingsstopp = $bunter[$leieforholdId]->oppfølgingsstopp || ($leieforhold->stoppOppfølging ||$leieforhold->avventOppfølging > date_create());
            $bunter[$leieforholdId]->leieforhold = $krav->leieforhold;
            $bunter[$leieforholdId]->purreformat = $bunter[$leieforholdId]->purreformat ?? Purring::PURREMÅTE_GIRO;
            $bunter[$leieforholdId]->formatvalg = $bunter[$leieforholdId]->formatvalg ?? Purring::PURREMÅTE_GIRO;

            /**
             * Leieforholdet foreslåes purret dersom minst én av regningene
             * er moden for det
             */
            if (!$bunter[$leieforholdId]->purres) {
                $sisteForfall = $regning->hentSisteForfall();
                if ($sisteForfall) {
                    $sisteForfall = clone $sisteForfall;
                }
                if( $sisteForfall->add(new DateInterval($this->app->hentValg('purreintervall'))) < date_create() ) {
                    $bunter[$leieforholdId]->purres = !$krav->hentBetalingsplan();
                }
            }

            if($this->app->hentValg('epost_faktura_aktivert') && $krav->leieforhold->hentGiroformat() == Purring::PURREMÅTE_EPOST) {
                $bunter[$leieforholdId]->purreformat = Purring::PURREMÅTE_EPOST;
                if(
                    $this->app->hentValg('purregebyr') == 0
                    || $this->app->hentValg('epost_purregebyr_aktivert')
                ) {
                    $bunter[$leieforholdId]->formatvalg = Purring::PURREMÅTE_EPOST;
                }
            }

            $bunter[$leieforholdId]->regninger[$regning->hentId()] = (object)[
                'regning' => $regning,
                'regningstekst' => implode('<br>', $regningsBeskrivelser->{$regning->hentId()})
            ];
        }
        return $bunter;
    }

    /**
     * Returnerer et objekt med regningsnr som egenskapsnavn, og kravtekstene som verdi
     *
     * @param Kravsett $kravsett
     * @return stdClass
     * @throws Exception
     */
    private function hentRegningsbeskrivelser(Kravsett $kravsett): stdClass
    {
        $regningsbeskrivelser = (object)[];
        foreach ($kravsett as $krav) {
            $regningsbeskrivelser->{$krav->hentRegning()->hentId()}[] = $krav->hentTekst();
        }
        return $regningsbeskrivelser;
    }

    /**
     * Hent container med alle regningfeltene
     *
     * @param Regning $regning
     * @param string $regningstekst
     * @param bool $leieforholdetKanPurres
     * @return FieldContainer
     * @throws Exception
     */
    private function hentRegningFelter(
        Regning $regning,
        string $regningstekst,
        bool $leieforholdetKanPurres
    ): FieldContainer
    {
        $regningsFelter = new JsArray();
        $sisteForfall = clone $regning->hentSisteForfall();
        $sistePurring = $regning->hentSistePurring();

        $regningenForeslåsPurret = false;
        if($leieforholdetKanPurres) {
            $regningenForeslåsPurret = $sisteForfall->add(new DateInterval($this->app->hentValg('purreintervall'))) < date_create();
        }

        $purretekst = 'Forfalt: '
            . '<strong>'
            . $regning->hentForfall()->format('d.m.Y')
            . '</strong>. '
        ;
        if ($sistePurring) {
            $purretekst
                .= 'Sist purret: '
                . '<strong>'
                . $sistePurring->hentPurredato()->format('d.m.Y')
                . '</strong>.'
            ;
        }
        else {
            $purretekst .= 'Ikke tidligere purret';
        }

        /**
         * Inkluderings-checkbox per regning
         */
        $regningsFelter->addItem($this->app->vis(Checkbox::class, [
            'fieldLabel' => 'Regning ' . $regning->hentId(),
            'boxLabel' => $regningstekst,
            'name' => 'purreregninger[]',
            'checked' => $regningenForeslåsPurret,
            'inputValue' => $regning->hentId()
        ]));

        /**
         * Purre-informasjon per regning
         */
        $regningsFelter->addItem($this->app->vis(Display::class, [
            'fieldLabel' => ' ',
            'labelSeparator' => '',
            'value' => $purretekst
        ]));

        /** @var FieldContainer $kontainer */
        $kontainer = $this->app->vis(FieldContainer::class, [
            'items' => $regningsFelter
        ]);
        return $kontainer;
    }

    /**
     * Hent container med alle felter for leieforholdet
     *
     * @param stdClass $leieforholdHolder
     * @return FieldSet
     * @throws Exception
     */


    /**
     * Hent feltsett med alle felter for leieforholdet
     *
     * @param Leieforhold $leieforhold
     * @param bool $oppfølgingsstopp
     * @param bool $purres
     * @param string $purreformat
     * @param string $formatvalg
     * @param DateTime|null $oppfølgingsfrist
     * @param stdClass[] $regningsHoldere
     * * regning
     * * regningstekst
     * @return FieldSet
     * @throws Exception
     */
    private function hentLeieforholdFeltsett(
        Leieforhold $leieforhold,
        bool $oppfølgingsstopp,
        bool $purres,
        string $purreformat = Purring::PURREMÅTE_GIRO,
        string $formatvalg = Purring::PURREMÅTE_GIRO,
        ?DateTime $oppfølgingsfrist = null,
        array $regningsHoldere = []
    ): FieldSet
    {
        $leieforholdFeltsettInnhold = new JsArray();

        $leieforholdetKanPurres = !$oppfølgingsstopp;
        $leieforholdetHarRegningerSomBørPurres = $purres;

        /** @var string $tittel */
        if ($oppfølgingsstopp) {
            $tittel = new HtmlElement('span', [], 'Leieforhold ' . $leieforhold->hentId() . ': ' . $leieforhold->navn)
                . ' (oppfølgingsstopp'
                . ($oppfølgingsfrist ? (' til ' . $oppfølgingsfrist->format('d.m.Y')) : ')');
        }
        else {
            $tittel = $leieforhold->hentId() . ': ' . $leieforhold->navn;
        }

        /**
         * Purreformat for leieforholdet
         */
        $leieforholdFeltsettInnhold->addItem($this->app->vis(RadioGroup::class, [
            'fieldLabel' => 'Purreformat',
            'columns' => 2,
            'vertical' => true,
            'name' => 'purreformat[' . $leieforhold->hentId() . ']',
            'dataValues' =>
                $purreformat == Purring::PURREMÅTE_EPOST
                    ? (object)[
                    Purring::PURREMÅTE_GIRO => 'Giroblankett',
                    Purring::PURREMÅTE_EPOST => 'E-post'
                ]
                    : (object)[Purring::PURREMÅTE_GIRO => 'Giroblankett'],
            'value' => $formatvalg
        ]));

        /**
         * Purregebyr-velger for leieforholdet
         */
        if($this->app->hentValg('purregebyr') > 0) {
            $leieforholdFeltsettInnhold->addItem($this->app->vis(Checkbox::class, [
                'fieldLabel' => '',
                'checked' => !$oppfølgingsstopp,
                'boxLabel' => 'Krev gebyr for de purringene som tilfredstiller kravene til det:',
                'name' => 'purregebyr[]',
                'inputValue' => $leieforhold->hentId()
            ]));
        }

        /**
         * Vis regningene
         *
         * @var stdClass $regningsHolder
         */
        foreach($regningsHoldere as $regningsHolder) {
            /** @var Regning $regning */
            $regning = $regningsHolder->regning;
            /** @var string $regningstekst */
            $regningstekst = $regningsHolder->regningstekst;

            $leieforholdFeltsettInnhold->addItem(
                $this->hentRegningFelter($regning, $regningstekst, $leieforholdetKanPurres)
            );
        }

        /**
         * Checkbox for statusoversikt
         */
        $leieforholdFeltsettInnhold->addItem($this->app->vis(Checkbox::class, [
            'fieldLabel' => 'Send oversikt',
            'boxLabel' => 'Send gebyrfri oversikt over alt utestående',
            'name' => 'statusoversikter[]',
            'inputValue' => $leieforhold->hentId(),
            'checked' => $oppfølgingsstopp
        ]));

        /**
         * Fieldset for leieforholdet
         *
         * @var FieldSet $leieforholdFeltsett
         */
        $leieforholdFeltsett = $this->app->vis(FieldSet::class, [
            'collapsible' => true,
            'collapsed' => !$leieforholdetHarRegningerSomBørPurres,
            'title' => strval(new HtmlElement('strong', [], $tittel)),
            'items' => $leieforholdFeltsettInnhold
        ]);
        return $leieforholdFeltsett;
    }
}