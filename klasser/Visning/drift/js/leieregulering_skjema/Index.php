<?php

namespace Kyegil\Leiebasen\Visning\drift\js\leieregulering_skjema;


use Exception;
use Kyegil\Leiebasen\Leieregulerer;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Skript-fil i drift
 *
 *  Ressurser:
 *      modell \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $leieforholdId
 *      $eksisterendeBasisLeie
 *      $antallTerminer
 *      $varselType
 *      $skript
 * @package Kyegil\Leiebasen\Visning\drift\js\leieregulering_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/js/leieregulering_skjema/Index.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');

        $datasett = [
            'leieforholdId' => $leieforhold ? $leieforhold->hentId() : '',
            'skript' => '',
            'eksisterendeBasisLeie' => $leieforhold->hentÅrligBasisleie(),
            'antallTerminer' => $leieforhold->antTerminer,
            'varselType' => $leieforhold && $leieforhold->hentEpost()
                ? Leieregulerer::VARSELTYPE_EPOST
                : Leieregulerer::VARSELTYPE_BREV,
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}