<?php

namespace Kyegil\Leiebasen\Visning\drift\js\leieforhold_regningsadresse_skjema;


use Exception;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Skript-fil i drift
 *
 * Ressurser:
 * * leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *
 * Tilgjengelige variabler:
 * * $leieforholdId
 * * $efakturaAktiv bool
 * * $skript
 *
 * @package Kyegil\Leiebasen\Visning\drift\js\leieforhold_regningsadresse_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/js/leieforhold_regningsadresse_skjema/Index.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold|null $modell */
        $leieforhold = $this->hentRessurs('leieforhold');

        $datasett = [
            'leieforholdId' => $leieforhold ? $leieforhold->hentId() : '*',
            'efakturaAktiv' => (bool)$this->app->hentValg('efaktura'),
            'skript' => '',
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}