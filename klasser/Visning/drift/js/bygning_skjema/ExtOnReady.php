<?php

namespace Kyegil\Leiebasen\Visning\drift\js\bygning_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\File;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Hidden;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Text;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FlexiCheckboxGroup;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel as FormPanel;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\Img;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $bygning \Kyegil\Leiebasen\Modell\Leieobjekt\Bygning
 *  Mulige variabler:
 *      $bygningsId
 *      $meny
 *      $skjema
 *      $formActionUrl
 * @package Kyegil\Leiebasen\Visning\drift\js\bygning_skjema
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/bygning_skjema/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['bygning'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Bygning|null $bygning */
        $bygning = $this->hentRessurs('bygning');

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }
        $this->definerData([
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'bygningsId' => $bygning ? $bygning->hentId() : '*',
            'skjema' => $this->app->vis(FormPanel::class, [
                'id' => 'bygningsskjema',
                'configObjectOnly' => false,
                'title' => $bygning ? $bygning->navn : 'Registrer ny bygning',
                'enabledSaveButton' => true,
                'formActionUrl' => '/drift/index.php?oppslag=bygning_skjema&id=' . ($bygning && $bygning->hentId() ? $bygning->hentId() : '*') . '&oppdrag=ta_imot_skjema',
                'returnUrl' => $this->app->returi->get(),
                'formSourceUrl' => '/drift/index.php?oppslag=bygning_skjema&id=' . ($bygning && $bygning->hentId() ? $bygning->hentId() : '*') . '&oppdrag=hent_data',
                'items' => [
                    $this->app->vis(Text::class, [
                        'name' => 'kode',
                        'fieldLabel' => 'Kode',
                        'value' => $bygning ? $bygning->kode : null,
                        'tabIndex' => 1
                    ]),
                    $this->app->vis(Text::class, [
                        'name' => 'navn',
                        'fieldLabel' => 'Navn/beskrivelse',
                        'value' => $bygning ? $bygning->navn : null,
                        'tabIndex' => 2
                    ]),
                    $this->app->vis(FlexiCheckboxGroup::class, [
                        'name' => 'områder',
                        'fieldLabel' => 'Områder',
                        'remote' => true,
                        'url' => '/drift/index.php?oppslag=bygning_skjema&id=' . ($bygning && $bygning->hentId() ? $bygning->hentId() : '*') . '&oppdrag=hent_data&data=områder',
                        'value' => $this->hentOmråder($bygning)
                    ]),
                    $this->app->vis(File::class, [
                        'name' => 'bildefelt',
                        'fieldLabel' => 'Last opp bilde',
                        'tabIndex' => 3
                    ]),
                    $this->app->vis(Hidden::class, [
                        'name' => 'bilde'
                    ]),
                    $this->app->vis(Img::class, [
                        'configObjectOnly' => false,
                        'height' => 350,
                        'maxWidth' => 350,
                        'src' => $bygning ? $bygning->bilde : null
                    ]),
                    new JsCustom('bildeverdi'),
                    new JsCustom('bildevisning'),
                ]
            ]),
            'formActionUrl' => '/drift/index.php?oppslag=bygning_skjema&id=' . ($bygning && $bygning->hentId() ? $bygning->hentId() : '*') . '&oppdrag=ta_i_mot_skjema'
        ]);

        return parent::forberedData();
    }

    /**
     * @param Bygning|null $bygning
     * @return stdClass|null
     * @throws Exception
     */
    protected function hentOmråder(?Bygning $bygning)
    {
        $resultat = null;
        if ($bygning) {
            $resultat = new stdClass();
            /** @var Område $område */
            foreach ($bygning->hentOmråder() as $område) {
                $resultat->{$område->hentId()} = $område->hentNavn();
            }
        }
        return $resultat;
    }
}