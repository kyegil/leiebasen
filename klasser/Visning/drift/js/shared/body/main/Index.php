<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\js\shared\body\main;


use Kyegil\Leiebasen\Visning\Drift;

/**
 * Visning for grunnleggende JS i drift
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $loaderPath
 *      $extRequire
 *      $extOnReady
 * @package Kyegil\Leiebasen\Visning\drift\js\shared\body\main
 */
class Index extends Drift
{
    protected $template = 'drift/js/shared/body/main/Index.js';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $this->definerData([
            'loaderPath' => $this->app->http_host . '/' . $this->app->ext_bibliotek . "/examples/ux",
            'extRequire' => json_encode([
                'Ext.data.*',
                'Ext.form.field.*',
                'Ext.layout.container.Border',
                'Ext.grid.*',
                'Ext.ux.RowExpander'
            ]),
            'extOnReady' => ''
        ]);
        return parent::forberedData();
    }
}