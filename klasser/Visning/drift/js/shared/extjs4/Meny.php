<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\drift\js\shared\extjs4;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\ViewRenderer\JsArray;

/**
 * Visning for Ext-meny i drift
 *
 *  Ressurser:
 *      bruker Kyegil\Leiebasen\Modell\Person
 *  Mulige variabler:
 *      $brukerId
 *      $menyElementer
 * @package Kyegil\Leiebasen\Visning\drift\js\shared\body\main
 */
class Meny extends Drift
{
    protected $template = 'drift/js/shared/extjs4/Meny.js';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'bruker') {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        $brukerId = strval($bruker);

        $menyStruktur = $this->app->hentMenyStruktur('', $bruker);

        $this->definerData([
            'brukerId' => $brukerId,
            'menyElementer' => JsCustom::JsonImproved($this->visHovedElementer($menyStruktur))
        ]);
        return parent::forberedData();
    }

    /**
     * @param array $elementer
     * @param Person|null $bruker
     * @return JsArray
     */
    public function visHovedElementer(array $elementer, ?Person $bruker = null): JsArray
    {
        list('elementer' => $elementer, 'bruker' => $bruker) = $this->app->pre($this, __FUNCTION__, ['elementer' => $elementer, 'bruker' => $bruker]);
        $resultat = new JsArray();
        foreach ($elementer as $element) {
            if($resultat->getItems()) {
                $resultat->addItem(\Kyegil\Leiebasen\Jshtml\JsCustom::JsonImproved('-'));
            }
            if (is_object($element)) {
                $visning = (object)[
                    'text' => strval(new HtmlElement('span', [
                        'style' => 'font-weight:bold;'
                    ], mb_strtoupper($element->text ?? ''))),
                    'hideOnClick' => false,
                    'menu' => (object)[
                        'xtype' => 'menu'
                    ],
                ];
                if (isset($element->id)) {
                    $visning->menu->id = $element->id;
                }
                if (isset($element->items)) {
                    $visning->menu->items = $this->visUnderElementer($element->items);
                }
            }
            $resultat->addItem(\Kyegil\Leiebasen\Jshtml\JsCustom::JsonImproved($visning));
        }
        return $this->app->post($this, __FUNCTION__, $resultat);
    }

    /**
     * @param array $elementer
     * @param Person|null $bruker
     * @return JsArray
     */
    public function visUnderElementer(array $elementer, ?Person $bruker = null): JsArray
    {
        list('elementer' => $elementer, 'bruker' => $bruker) = $this->app->pre($this, __FUNCTION__, ['elementer' => $elementer, 'bruker' => $bruker]);
        $resultat = new JsArray();
        foreach ($elementer as $element) {
            $visning = '-';
            if (is_object($element)) {
                $visning = new \stdClass();
                if (isset($element->id)) {
                    $visning->id = $element->id;
                }
                if (isset($element->text)) {
                    $visning->text = $element->text;
                }
                if (isset($element->url)) {
                    $visning->handler = new JsFunction('window.location.href = "' . $element->url . '"');
                }
                if (isset($element->items)) {
                    $visning->items = $this->visUnderElementer($element->items);
                    $visning->menu = ((object)['items' => $this->visUnderElementer($element->items)]);
                }
            }
            $resultat->addItem(\Kyegil\Leiebasen\Jshtml\JsCustom::JsonImproved($visning));
        }
        return $this->app->post($this, __FUNCTION__, $resultat);
    }
}