<?php

namespace Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_tjeneste_skjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Checkbox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\ComboBox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Text;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\TextArea;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel as FormPanel;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $tjeneste \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste|null
 *      $leiebasenJsObjektEgenskaper stdClass
 *  Mulige variabler:
 *      $tjenesteId
 *      $meny
 *      $initLeiebasenJsObjektEgenskaper
 *      $skjema
 *      $formActionUrl
 * @package Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_tjeneste_skjema
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/delte_kostnader_tjeneste_skjema/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['tjeneste'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        if ($attributt == 'leiebasenJsObjektEgenskaper') {
            return $this->settLeiebasenJsObjektEgenskaper($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param stdClass $leiebasenJsObjektEgenskaper
     * @return $this
     */
    public function settLeiebasenJsObjektEgenskaper(stdClass $leiebasenJsObjektEgenskaper): Drift
    {
        return $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var stdClass $leiebasenJsObjektEgenskaper */
        $leiebasenJsObjektEgenskaper = $this->hentRessurs('leiebasenJsObjektEgenskaper');
        if (!$leiebasenJsObjektEgenskaper) {
            $leiebasenJsObjektEgenskaper = new stdClass();
            $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
        }
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        /** @var Tjeneste|null $tjeneste */
        $tjeneste = $this->hentRessurs('tjeneste');
        $tjenesteId = $tjeneste && $tjeneste->hentId() ? $tjeneste->hentId() : '*';

        $leiebasenJsObjektEgenskaper->skjema = (object)[
            'felter' => (object)[]
        ];

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }

        /** @var ComboBox $leieforholdFelt */
        $kravtypeFelt = $this->app->vis(ComboBox::class, [
            'name' => 'type',
            'fieldLabel' => 'Kravtype',
            'forceSelection' => true,
            'allowBlank' => false,
            'remote'=> false,
            'dataValues' => (object)[
                Krav::TYPE_ANNET => Krav::TYPE_ANNET,

            ],
            'value' => $tjeneste ? $tjeneste->hentType() : Krav::TYPE_ANNET
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->type = $kravtypeFelt;

        $navnFelt = $this->app->vis(Text::class, [
            'name' => 'navn',
            'fieldLabel' => 'Navn',
            'allowBlank' => false,
            'value' => $tjeneste ? $tjeneste->hentNavn() : null
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->navn = $navnFelt;

        $leverandørFelt = $this->app->vis(Text::class, [
            'name' => 'leverandør',
            'fieldLabel' => 'Leverandør',
            'value' => $tjeneste ? $tjeneste->hentLeverandør() : null
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->leverandør = $leverandørFelt;

        $avtalereferanseFelt = $this->app->vis(Text::class, [
            'name' => 'avtalereferanse',
            'fieldLabel' => 'Avtalenr el. avtale-referanse',
            'allowBlank' => false,
            'value' => $tjeneste ? $tjeneste->hentAvtalereferanse() : null
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->avtalereferanse = $avtalereferanseFelt;

        $beskrivelseFelt = $this->app->vis(TextArea::class, [
            'name' => 'beskrivelse',
            'fieldLabel' => 'Beskrivelse',
            'value' => $tjeneste ? $tjeneste->hentBeskrivelse() : null,
            'height' => 200
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->beskrivelse = $beskrivelseFelt;

        $forbruksregistreringsFelt = $this->app->vis(Checkbox::class, [
            'name' => 'registrer_forbruk',
            'fieldLabel' => 'Registrer forbruk',
            'boxLabel' => 'Forbruk kan registreres for statistikk dersom tjenesten er forbruksbasert og forbruk vises på faktura',
            'value' => $tjeneste && $tjeneste->hentRegistrerForbruk()
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->registrerForbruk = $forbruksregistreringsFelt;

        $forbruksenhetFelt = $this->app->vis(Text::class, [
            'name' => 'forbruksenhet',
            'fieldLabel' => 'Forbruk oppgis i (enhet)',
            'value' => $tjeneste ? $tjeneste->hentForbruksenhet() : null
        ]);
        $leiebasenJsObjektEgenskaper->skjema->felter->forbruksenhet = $forbruksenhetFelt;

        $this->definerData([
            'tjenesteId' => $tjenesteId,
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'initLeiebasenJsObjektEgenskaper' => $this->gjengiLeiebasenJsObjektEgenskaper(),
            'skjema' => $this->app->vis(FormPanel::class, [
                'id' => 'skjema',
                'configObjectOnly' => false,
                'title' => $tjeneste ? $tjeneste->hentNavn() : 'Registrer ny tjeneste',
                'enabledSaveButton' => true,
                'formActionUrl' => '/drift/index.php?oppslag=delte_kostnader_tjeneste_skjema&id=' . ($tjeneste && $tjeneste->hentId() ? $tjeneste->hentId() : '*') . '&oppdrag=ta_imot_skjema',
                'returnUrl' => $this->app->returi->get(),
                'formSourceUrl' => '/drift/index.php?oppslag=delte_kostnader_tjeneste_skjema&id=' . ($tjeneste && $tjeneste->hentId() ? $tjeneste->hentId() : '*') . '&oppdrag=hent_data',
                'items' => [
                    new JsCustom('leiebasen.skjema.felter.type'),
                    new JsCustom('leiebasen.skjema.felter.navn'),
                    new JsCustom('leiebasen.skjema.felter.leverandør'),
                    new JsCustom('leiebasen.skjema.felter.avtalereferanse'),
                    new JsCustom('leiebasen.skjema.felter.registrerForbruk'),
                    new JsCustom('leiebasen.skjema.felter.forbruksenhet'),
                    new JsCustom('leiebasen.skjema.felter.beskrivelse'),
                    new JsCustom('leiebasen.skjema.felter.person'),
                ]
            ]),
            'formActionUrl' => '/drift/index.php?oppslag=delte_kostnader_tjeneste_skjema&id=' . $tjenesteId . '&oppdrag=ta_i_mot_skjema'
        ]);

        return parent::forberedData();
    }
}