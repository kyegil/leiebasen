<?php

namespace Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_kostnad_import;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\button\Button;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\File;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel as FormPanel;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $leiebasenJsObjektEgenskaper stdClass
 *  Mulige variabler:
 *      $meny
 *      $initLeiebasenJsObjektEgenskaper
 *      $skjema
 *      $formActionUrl
 * @package Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_kostnad_import
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/delte_kostnader_kostnad_import/ExtOnReady.js';


    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['bruker'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        if ($attributt == 'leiebasenJsObjektEgenskaper') {
            return $this->settLeiebasenJsObjektEgenskaper($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param stdClass $leiebasenJsObjektEgenskaper
     * @return $this
     */
    public function settLeiebasenJsObjektEgenskaper(stdClass $leiebasenJsObjektEgenskaper): Drift
    {
        return $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var stdClass $leiebasenJsObjektEgenskaper */
        $leiebasenJsObjektEgenskaper = $this->hentRessurs('leiebasenJsObjektEgenskaper');
        if (!$leiebasenJsObjektEgenskaper) {
            $leiebasenJsObjektEgenskaper = new stdClass();
            $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
        }
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');

        $leiebasenJsObjektEgenskaper->skjema = (object)[
            'felter' => (object)[]
        ];

        if($bruker) {
            $this->definerData([
                'brukerId' => $bruker->hentId()
            ]);
        }

        /** @var File $importfilfelt */
        $importfilfelt = $this->app->vis(File::class, [
            'name' => 'importfil',
            'fieldLabel' => 'Importfil',
            'allowBlank' => false,
            'buttonText' => 'Last opp fil med kostnader for fordeling'
        ]);

        /** @var Button $avbrytKnapp */
        $avbrytKnapp = $this->viewFactory->createView(Button::class, [
            'configObjectOnly' => true,
            'id' => 'cancel-button',
            'itemId' => 'cancel-button',
            'text' => 'Avbryt',
            'handler' => new JsFunction('window.location.href = ' . json_encode($this->app->returi->get()->url) . ';')
        ]);

        /** @var Button $valideringsKnapp */
        $eksempelKnapp = $this->viewFactory->createView(Button::class, [
            'configObjectOnly' => true,
            'id' => 'example-button',
            'itemId' => 'example-button',
            'text' => 'Last ned eksempelfil',
            'disabled' => false,
            'handler' => new JsFunction("window.location.href = '/drift/index.php?oppslag=delte_kostnader_kostnad_import&oppdrag=hent_data&data=eksempelfil';"),
        ]);

        /** @var Button $valideringsKnapp */
        $valideringsKnapp = $this->viewFactory->createView(Button::class, [
            'configObjectOnly' => true,
            'id' => 'validate-button',
            'itemId' => 'validate-button',
            'text' => 'Validér importfila',
            'disabled' => false,
            'handler' => new JsFunction(
                'button.up().up().getForm().submit({waitMsg: "Validérer...", url: ' . json_encode('/drift/index.php?oppslag=delte_kostnader_kostnad_import&oppdrag=ta_imot_skjema&skjema=validering') . '});',
                ['button', 'eventObject']
            )
        ]);

        /** @var Button $importKnapp */
        $importKnapp = $this->viewFactory->createView(Button::class, [
            'configObjectOnly' => true,
            'id' => 'save-button',
            'itemId' => 'save-button',
            'text' => 'Importér',
            'disabled' => false,
            'handler' => new JsFunction(
                'button.up().up().getForm().submit({waitMsg: "Importerer...", url: ' . json_encode('/drift/index.php?oppslag=delte_kostnader_kostnad_import&oppdrag=ta_imot_skjema&skjema=import') . '});',
                ['button', 'eventObject']
            )
        ]);

        $leiebasenJsObjektEgenskaper->skjema->felter->importfil = $importfilfelt;

        $this->definerData([
            'meny' => $this->app->vis(Meny::class, [
                'bruker' => $bruker
            ]),
            'initLeiebasenJsObjektEgenskaper' => $this->gjengiLeiebasenJsObjektEgenskaper(),
            'skjema' => $this->app->vis(FormPanel::class, [
                'id' => 'skjema',
                'configObjectOnly' => false,
                'title' => 'Import av nye kostnader eller fakturaer for fordeling',
                'enabledSaveButton' => false,
                'items' => [
                    new JsCustom('leiebasen.skjema.felter.importfil'),
                ],
                'buttons' => [
                    $avbrytKnapp,
                    $eksempelKnapp,
                    $valideringsKnapp,
                    $importKnapp
                ]
            ]),
            'formActionUrl' => '/drift/index.php?oppslag=delte_kostnader_kostnad_import&oppdrag=ta_imot_skjema&skjema=import'
        ]);

        return parent::forberedData();
    }
}