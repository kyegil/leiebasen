<?php

namespace Kyegil\Leiebasen\Visning\drift\js\profil_passord_skjema;


use Kyegil\Leiebasen\Visning\Drift;

/**
 * Skript-fil i mine sider
 *
 *  Ressurser:
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\drift\js\profil_passord_skjema
 */
class Index extends Drift
{
    protected $template = 'drift/js/profil_passord_skjema/Index.js';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        return parent::forberedData();
    }
}