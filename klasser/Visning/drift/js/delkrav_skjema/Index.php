<?php

namespace Kyegil\Leiebasen\Visning\drift\js\delkrav_skjema;


use Exception;
use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Visning\Drift;

/**
 * Skript-fil i drift
 *
 *  Ressurser:
 *      delkravtype \Kyegil\Leiebasen\Modell\Delkravtype
 *  Mulige variabler:
 *      $delkravtypeId
 * @package Kyegil\Leiebasen\Visning\drift\js\leieberegning_skjema
 */
class Index extends Drift
{
    /** @var string */
    protected $template = 'drift/js/delkrav_skjema/Index.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return Index
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Index
    {
        if(in_array($attributt,['delkravtype'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return Index
     * @throws Exception
     */
    protected function forberedData(): Index
    {
        /** @var Delkravtype|null $delkravtype */
        $delkravtype = $this->hentRessurs('delkravtype');

        $datasett = [
            'delkravtypeId' => $delkravtype ? $delkravtype->hentId() : '',
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}