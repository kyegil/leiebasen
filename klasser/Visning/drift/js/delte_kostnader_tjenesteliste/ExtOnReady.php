<?php

namespace Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_tjenesteliste;


use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Drift;
use stdClass;

/**
 * ExtOnReady-innhold
 *
 *  Ressurser:
 *      $bruker \Kyegil\Leiebasen\Modell\Person
 *      $leiebasenJsObjektEgenskaper object
 *  Mulige variabler:
 *      $brukerId
 *      $initLeiebasenJsObjektEgenskaper
 *      $tilbakeKnapp
 * @package Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_tjenesteliste
 */
class ExtOnReady extends Drift
{
    protected $template = 'drift/js/delte_kostnader_tjenesteliste/ExtOnReady.js';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'bruker') {
            return $this->settRessurs($attributt, $verdi);
        }
        if ($attributt == 'leiebasenJsObjektEgenskaper') {
            return $this->settLeiebasenJsObjektEgenskaper($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param stdClass $leiebasenJsObjektEgenskaper
     * @return $this
     */
    public function settLeiebasenJsObjektEgenskaper(stdClass $leiebasenJsObjektEgenskaper): Drift
    {
        return $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var stdClass $leiebasenJsObjektEgenskaper */
        $leiebasenJsObjektEgenskaper = $this->hentRessurs('leiebasenJsObjektEgenskaper');
        if (!$leiebasenJsObjektEgenskaper) {
            $leiebasenJsObjektEgenskaper = new stdClass();
            $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
        }
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        $brukerId = strval($bruker);

        $this->definerData([
            'meny' => '',
            'initLeiebasenJsObjektEgenskaper' => $this->gjengiLeiebasenJsObjektEgenskaper(),
            'tilbakeUrl' => $this->app->returi->get()->url,
        ]);
        return parent::forberedData();
    }
}