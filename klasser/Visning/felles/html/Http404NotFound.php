<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/08/2020
 * Time: 10:37
 */

namespace Kyegil\Leiebasen\Visning\felles\html;


use Kyegil\Leiebasen\Visning;

/**
 * Class Http403Forbidden
 *
 * Visning for http-respons 404 Not Found
 *
 *  Mulige variabler:
 *      $h1 Overskrift
 *      $tekst
 * @package Kyegil\Leiebasen\Visning\felles\html
 */
class Http404NotFound extends Visning
{
    /** @var string */
    protected $template = 'felles/html/Http404NotFound.html';

    protected function forberedData()
    {
        $this->definerData([
            'h1' => 'Siden finnes ikke',
            'tekst' => 'Dette oppslaget finnes ikke.'
        ]);
        return parent::forberedData();
    }
}