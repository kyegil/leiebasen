<?php

namespace Kyegil\Leiebasen\Visning\felles\html\shared\body\main;


use Kyegil\Leiebasen\Visning\Common;

class ReturiBreadcrumbs extends Common
{
    protected $template = 'felles/html/shared/body/main/ReturiBreadcrumbs.html';
}