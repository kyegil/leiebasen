<?php

namespace Kyegil\Leiebasen\Visning\felles\html\shared\body\main;


use Kyegil\Leiebasen\Visning\Common;

/**
 * Visning for kort med innhold
 *
 * Tilgjengelige variabler:
 * * $id kortets html id
 * * $header
 * * $innhold
 * * $knapper
 * @package Kyegil\Leiebasen\Visning\felles\html\shared\body\main
 */
class Kort extends Common
{
    protected $template = 'felles/html/shared/body/main/Kort.html';

    protected function forberedData()
    {
        $kortId = 'kort_' . md5(rand());
        $this->definerData([
            'id' => $kortId,
            'header' => '',
            'innhold' => '',
            'knapper' => '',
        ]);
        return parent::forberedData();
    }
}