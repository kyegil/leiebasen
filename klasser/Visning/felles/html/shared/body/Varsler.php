<?php

namespace Kyegil\Leiebasen\Visning\felles\html\shared\body;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for varsler
 *
 *  Ressurser:
 *      bruker Person[]
 *  Mulige variabler:
 *      $items
 * @package Kyegil\Leiebasen\Visning\felles\html\shared\body
 */
class Varsler extends \Kyegil\Leiebasen\Visning
{
    /** @var string */
    protected $template = 'felles/html/shared/body/Varsler.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['bruker'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     */
    protected function forberedData(): Varsler
    {
        $items = $this->hent('items');
        if(!isset($items)) {
            $items = new ViewArray();
            foreach($this->app->hentAdvarsler(\Kyegil\Leiebasen\Leiebase::VARSELNIVÅ_ALARM) as $advarsel) {
                $oppsummering = new HtmlElement('div', ['class' => 'oppsummering'], $advarsel['oppsummering']);
                $detaljer = new HtmlElement('div', ['class' => 'detaljer'], $advarsel['tekst']);
                $item = new HtmlElement('li', ['class' => 'alarm'], [$oppsummering, $detaljer]);
                $items->addItem($item);
            }
            foreach($this->app->hentAdvarsler(\Kyegil\Leiebasen\Leiebase::VARSELNIVÅ_ADVARSEL) as $advarsel) {
                $oppsummering = new HtmlElement('div', ['class' => 'oppsummering'], $advarsel['oppsummering']);
                $detaljer = new HtmlElement('div', ['class' => 'detaljer'], $advarsel['tekst']);
                $item = new HtmlElement('li', ['class' => 'advarsel'], [$oppsummering, $detaljer]);
                $items->addItem($item);
            }
            foreach($this->app->hentAdvarsler(\Kyegil\Leiebasen\Leiebase::VARSELNIVÅ_ORIENTERING) as $advarsel) {
                $oppsummering = new HtmlElement('div', ['class' => 'oppsummering'], $advarsel['oppsummering']);
                $detaljer = new HtmlElement('div', ['class' => 'detaljer'], $advarsel['tekst']);
                $item = new HtmlElement('li', ['class' => 'orientering'], [$oppsummering, $detaljer]);
                $items->addItem($item);
            }
        }

        $this->definerData([
            'items' => $items,
        ]);
        return parent::forberedData();
    }
}