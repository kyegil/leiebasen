<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/05/2021
 * Time: 17:37
 */

namespace Kyegil\Leiebasen\Visning\felles\html\shared;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Visning\Common;

/**
 * Utskriftsmal
 *
 * Tilgjengelige variabler:
 * * $tittel
 * * $innhold
 * * $scriptTag
 * @package Kyegil\Leiebasen\Visning\felles\html\shared
 */
class Utskrift extends Common
{
    protected $template = 'felles/html/shared/Utskrift.html';

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $this->definerData([
            'tittel' => '',
            'innhold' => '',
            'scriptTag' => new HtmlElement('script', [], 'window.print();')
        ]);
        return parent::forberedData();
    }
}