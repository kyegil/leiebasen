<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/08/2020
 * Time: 18:09
 */

namespace Kyegil\Leiebasen\Visning\felles\html\leieobjekt;


use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Visning;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      leieobjekt \Kyegil\Leiebasen\Modell\Leieobjekt
 *  Mulige variabler:
 *      $navn
 *      $gateadresse
 *      $postnr
 *      $poststed
 * @package Kyegil\Leiebasen\Visning\felles\html\leieforhold
 */
class Adressefelt extends Visning
{
    protected $template = 'felles/html/leieobjekt/Adressefelt.html';

    /**
     * @param string $nøkkel
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function setData($nøkkel, $verdi = null)
    {
        if(in_array($nøkkel, ['leieobjekt'])) {
            return $this->settRessurs($nøkkel, $verdi);
        }
        return parent::setData($nøkkel, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        parent::forberedData();
        /** @var Leieobjekt $leieobjekt */
        $leieobjekt = $this->hentRessurs('leieobjekt');
        if ($leieobjekt) {
            $adresse = [
                'navn' => $leieobjekt->navn,
                'gateadresse' => $leieobjekt->gateadresse,
                'postnr' => $leieobjekt->postnr,
                'poststed' => $leieobjekt->poststed
            ];
            $data = array_merge($adresse, $this->hent());
            $this->sett($data);
        }
        return $this;
    }
}