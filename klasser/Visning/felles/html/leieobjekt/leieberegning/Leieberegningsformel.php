<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/08/2020
 * Time: 10:37
 */

namespace Kyegil\Leiebasen\Visning\felles\html\leieobjekt\leieberegning;


use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      leieberegning \Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning
 *  Mulige variabler:
 *      $navn
 *      $beskrivelse
 *      $leiePerObjekt
 *      $leiePerKontrakt
 *      $leiePerKvadratmeter
 *      $kvadratmeterLeie
 *      $leieForBad
 *      $leieForEgenDo
 *      $leieForFellesDo
 * @package Kyegil\Leiebasen\Visning\felles\html\leieobjekt\leieberegning
 */
class Leieberegningsformel extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/leieobjekt/leieberegning/Leieberegningsformel.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['leieberegning', 'leieobjekt'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Leieberegning $leieberegning */
        $leieberegning = $this->hentRessurs('leieberegning');
        /** @var Leieobjekt $leieobjekt */
        $leieobjekt = $this->hentRessurs('leieobjekt');

        if($leieobjekt) {
            $this->definerData([
                'navn' => $leieberegning->hentNavn(),
                'beskrivelse'  => $leieberegning->hentBeskrivelse(),
                'leiePerObjekt'  => $leieberegning->hentLeiePerObjekt() ? $this->app->kr($leieberegning->hentLeiePerObjekt()) : '',
                'leiePerKontrakt'  => $leieberegning->hentLeiePerKontrakt() ? $this->app->kr($leieberegning->hentLeiePerKontrakt()) : '',
                'leiePerKvadratmeter'  => $leieberegning->hentLeiePerKvadratmeter() ? $this->app->kr($leieberegning->hentLeiePerKvadratmeter()) : $this->app->kr(0),
                'kvadratmeterLeie'  => $leieobjekt->hentAreal() && $leieberegning->hentLeiePerKvadratmeter() ? $this->app->kr($leieobjekt->hentAreal() * $leieberegning->hentLeiePerKvadratmeter()) : '',
                'leieForBad'  => $leieobjekt->hentBad() && $leieberegning->hentLeieForBad() ? $this->app->kr($leieberegning->hentLeieForBad()) : '',
                'leieForEgenDo'  => $leieobjekt->hentToalettKategori() == 2 && $leieberegning->hentLeieForEgendo() ? $this->app->kr($leieberegning->hentLeieForEgendo()) : '',
                'leieForFellesDo'  => $leieobjekt->hentToalettKategori() == 1 && $leieberegning->hentLeieForFellesdo() ? $this->app->kr($leieberegning->hentLeieForFellesdo()) : ''
            ]);
        }

        return parent::forberedData();
    }
}