<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 05/08/2020
 * Time: 17:25
 */

namespace Kyegil\Leiebasen\Visning\felles\html\leieobjekt;


use DateTime;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Navn;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      leieobjekt \Kyegil\Leiebasen\Modell\Leieobjekt
 *  Mulige variabler:
 *      $leieforholdsett
 * @package Kyegil\Leiebasen\Visning\felles\html\leieobjekt
 */
class Leieforholdfelt extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/leieobjekt/Leieforholdfelt.html';

    /**
     * @param array|string $nøkkel
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function setData($nøkkel, $verdi = null)
    {
        if(in_array($nøkkel, ['leieobjekt'])) {
            return $this->settRessurs($nøkkel, $verdi);
        }
        return parent::setData($nøkkel, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Leieobjekt $leieobjekt */
        $leieobjekt = $this->hentRessurs('leieobjekt');
        $iDag = new DateTime();
        $leieforholdsett = $leieobjekt->hentLeieforhold($iDag, $iDag);
        $leieforholdVisning = [];
        /** @var Leieforhold $leieforhold */
        foreach($leieforholdsett as $leieforhold) {
            $leieforholdVisning[] = new HtmlElement('li', [], $this->app->vis(Navn::class, [
                'leieforhold' => $leieforhold
            ]));
        }
        $this->definerData([
            'leieforholdsett' => $leieforholdVisning
        ]);
        return parent::forberedData();
    }
}