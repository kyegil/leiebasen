<?php

namespace Kyegil\Leiebasen\Visning\felles\html\leieobjekt;


use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt;

/**
 * Toalettbeskrivelse
 *
 *  Ressurser:
 *      leieobjekt \Kyegil\Leiebasen\Modell\Leieobjekt
 *  Mulige variabler:
 *      $kategori
 *      $beskrivelse
 */
class Toalettbeskrivelse extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/leieobjekt/Toalettbeskrivelse.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['leieobjekt'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Leieobjekt|null $leieobjekt */
        $leieobjekt = $this->hentRessurs('leieobjekt');

        if($leieobjekt) {
            $this->definerData([
                'kategori' => $leieobjekt->hentToalettKategori(),
                'beskrivelse' => $leieobjekt->hentToalett(),
            ]);
        }
        return parent::forberedData();
    }
}