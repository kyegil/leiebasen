<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\felles\html\regning;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Class Regning
 *
 * Html-visning av regning
 *
 *  Ressurser:
 *      regning \Kyegil\Leiebasen\Modell\Leieforhold\Regning
 *  Mulige variabler:
 *      $detaljer
 *      $leieforholdbeskrivelse
 *      $dato
 *      $forfall
 *      $kid
 *      $regningsbeløp
 *      $innbetalt
 *      $utestående
 * @package Kyegil\Leiebasen\Visning\felles\html\regning
 */
class Regning extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/regning/Regning.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['regning'])) {
            return $this->settRegning($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Leieforhold\Regning $regning
     * @return Regning
     * @throws \Exception
     */
    protected function settRegning($regning)
    {
        /** @var Leieforhold\Regning $regning */
        $this->settRessurs('regning', $regning);
        return $this;
    }


    /**
     * @return Regning
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Leieforhold\Regning $regning */
        $regning = $this->hentRessurs('regning');
        $leieforhold = $regning->hentLeieforhold();
        $innbetalinger = $regning->hentInnbetalinger();
        $forfall = $regning->hentForfall() ? $regning->hentForfall()->format('d.m.Y') : '';
        if(
            $regning->hentUtestående() > 0
            && $regning->hentForfall() >= date_create_immutable()
        ) {
            $forfall .= ' ' . new HtmlElement('a', [
                'href' => "index.php?oppslag=betalingsutsettelse_skjema&leieforhold={$leieforhold}&regning={$regning}"
            ], 'Betalingsutsettelse');
        }
        $datasett = [
            'leieforholdnr' => $leieforhold->hentId(),
            'leieforholdbeskrivelse' => $leieforhold->hentBeskrivelse(),
            'dato' => $regning->hentDato() ? $regning->hentDato()->format('d.m.Y') : '',
            'forfall' => $forfall,
            'kid' => $regning->hentKid(),
            'regningsbeløp' => $this->app->kr($regning->hentBeløp()),
            'detaljer' => new ViewArray(),
            'innbetalt' => $regning->hentBeløp() - $regning->hentUtestående() != 0 ? $this->app->kr($regning->hentBeløp() - $regning->hentUtestående()) : '',
            'utestående' => $this->app->kr($regning->hentUtestående())
        ];
        /** @var Leieforhold\Krav $krav */
        foreach($regning->hentKrav() as $krav) {
            $datasett['detaljer']->addItem($this->app->vis(Kravlinje::class, [
                'tekst' => $krav->hentTekst(),
                'beløp' => $this->app->kr($krav->hentBeløp())
            ]));
        }
        $this->definerData($datasett);
        return parent::forberedData();
    }
}