<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/08/2020
 * Time: 10:37
 */

namespace Kyegil\Leiebasen\Visning\felles\html;


/**
 * Class Http403Forbidden
 *
 * Visning for http-respons 403 Forbidden
 *
 *  Mulige variabler:
 *      $h1 Overskrift
 *      $tekst
 * @package Kyegil\Leiebasen\Visning\felles\html
 */
class Http403Forbidden extends \Kyegil\Leiebasen\Visning
{
    /** @var string */
    protected $template = 'felles/html/Http403Forbidden.html';

    protected function forberedData()
    {
        $this->definerData([
            'h1' => 'Ingen adgang',
            'tekst' => 'Du har desverre ikke adgang til dette området.'
        ]);
        return parent::forberedData();
    }
}