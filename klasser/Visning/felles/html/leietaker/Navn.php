<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 05/08/2020
 * Time: 17:25
 */

namespace Kyegil\Leiebasen\Visning\felles\html\leietaker;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      leietaker \Kyegil\Leiebasen\Modell\Leieforhold\Leietaker
 *      visLenke boolean
 *  Mulige variabler:
 *      $navn
 *      $leietakerId
 *      $personId
 *      $slettet
 *      $navn
 *      $visLenke
 * @package Kyegil\Leiebasen\Visning\felles\html\leieforhold
 */
class Navn extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/leietaker/Navn.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['leietaker'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Leietaker $leietaker */
        $leietaker = $this->hentRessurs('leietaker');
        /** @var boolean $visLenke */
        $visLenke = $this->hentRessurs('visLenke');
        if ($leietaker) {
            $this->definerData([
                'leietakerId' => $leietaker->hentId(),
                'personId' => strval($leietaker->hentPerson()),
                'slettet' => $leietaker->hentSlettet() ? $leietaker->hentSlettet()->format('Y-m-d') : false,
                'navn' => $leietaker->hentNavn(),
                'visLenke' => $visLenke && $leietaker->hentPerson()
            ]);
        }
        return parent::forberedData();
    }
}