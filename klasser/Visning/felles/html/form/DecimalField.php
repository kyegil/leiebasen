<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;

/**
 * Visning for input-felt type=email i skjema
 *
 *  Ressurser:
 *      decimalSeparator
 *  Mulige variabler:
 *      $id
 *      $name
 *      $label
 *      $class
 *      $style
 *      $type
 *      $required
 *      $disabled
 *      $autocomplete
 *      $readonly
 *      $onblur
 *      $value
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class DecimalField extends Field
{

    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['decimalSeparator'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $decimalSeparator = $this->hentRessurs('decimalSeparator') ?? ',';
        $datasett = [
            'pattern' => '-*[0-9]*[.,]*[0-9]*',
        ];
        $value = $this->hent('value');
        if(isset($value)) {
            $this->sett('value', str_replace('.', $decimalSeparator, $value));
        }
        $this->definerData($datasett);
        return parent::forberedData();
    }
}