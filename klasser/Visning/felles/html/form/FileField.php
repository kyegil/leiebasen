<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;


use Kyegil\Leiebasen\Visning;

/**
 * Visning for filopplastingsfelt i skjema
 *
 * Tilgjengelige variabler:
 * * $id The HTML Id attribute
 * * $name
 * * $label
 * * $class
 * * $required
 * * $disabled
 * * $readonly
 * * $multiple
 * * $value
 * * $accept
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class FileField extends Visning
{
    protected $template = 'felles/html/form/FileField.html';

    protected function forberedData()
    {
        $fieldId = 'field_' . md5(rand());
        $datasett = [
            'id' => $fieldId,
            'name' => '',
            'label' => '',
            'class' => '',
            'required' => false,
            'readonly' => false,
            'disabled' => false,
            'multiple' => false,
            'value' => '',
            'accept' => ''
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}