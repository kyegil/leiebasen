<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for fieldset i skjema
 *
 * Ressurser:
 * Tilgjengelige variabler:
 * * $id
 * * $class
 * * $style
 * * $label (legend)
 * * $name
 * * $disabled (bool)
 * * $contents
 * * $script
 *
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class MultiFileFields extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/form/MultiFileFields.html';

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function setData($key, $value = null)
    {
        if(in_array($key, ['name', 'value'])) {
            return $this->setResource($key, $value);
        }
        return parent::setData($key, $value);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $items = new ViewArray();

        settype($this->resources['value'], 'array');

        /** @var string $id */
        $id = $this->getData('id');
        if (!isset($id)) {
            $id = 'multifileuploadcontainer_' . md5(rand());
            $this->setData('id', $id);
        }

        /** @var string $name */
        $name = $this->getResource('name') ?? md5(rand());

        /** @var array $value */
        $existingFiles = $this->getResource('value');

        $items->addItem(
            $this->app->vis(Field::class, [
                'type' => 'hidden',
                'name' => 'nye_' . $name . '_antall',
                'value' => 0
            ])
        );
        foreach ($existingFiles as $file) {
            $items->addItem(
                new HtmlElement('div', ['style' => 'display: flex; align-items:baseline;'], [
                    $this->app->vis(Field::class, [
                        'readonly' => true,
                        'name' => $name . '[]',
                        'width' => '150px',
                        'value' => basename($file)
                    ]),
                    new HtmlElement('button', [
                        'onclick' => '((btn)=>{$(btn).parent().remove();})(this)',
                    ], 'Fjern'),
                ])
            );
        }

        $items->addItem(
          new HtmlElement('a', [
              'data-name' => $name,
              'class' => 'file-upload-add button',
          ], 'Legg til')
        );

        $datasett = [
            'id' => $id,
            'class' => '',
            'style' => '',
            'label' => '',
            'name' => '',
            'disabled' => false,
            'contents' => $items,
            'script' => ''
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}