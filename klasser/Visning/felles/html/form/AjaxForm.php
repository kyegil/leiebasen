<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\js\form\AjaxForm as AjaxFormJs;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for Ajax-skjema
 *
 *  Ressurser:
 *
 *  Tilgjengelige variabler:
 * * $formId
 * * $action
 * * $method GET|POST
 * * $enctype
 * * $class
 * * $buttons
 * * $fields
 * * $reCaptchaSiteKey
 * * $script
 * * $saveButtonText
 * * bool $useModal = false Use the modal for messages
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class AjaxForm extends Visning
{
    /** @var string */
    protected $template = 'felles/html/form/AjaxForm.html';

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $formId = $this->getData('formId') ?? ('id' . md5(rand()));
        $method = $this->getData('method') ?? 'POST';
        $reCaptchaSiteKey = $this->getData('reCaptchaSiteKey');
        $useModal = (bool)$this->getData('useModal');
        $submitButton = new HtmlElement('button', ['role' => 'button', 'type' => 'submit', 'form' => $formId],
            isset($this->data['buttonText']) ? $this->hent('buttonText') : 'Lagre'
        );
        $script = $this->app->vis(AjaxFormJs::class, [
            'formId' => $formId,
            'reCaptchaSiteKey' => $reCaptchaSiteKey,
            'method' => $method,
            'enctype' => 'multipart/form-data',
            'useModal' => $useModal,
        ]);
        if($useModal) {
            $script->settMal('felles/js/form/AjaxFormModal.js');
        }
        $datasett = [
            'formId' => $formId,
            'action' => '',
            'method' => $method,
            'enctype' => 'multipart/form-data',
            'class' => 'ajaxform',
            'buttons' => new ViewArray([$submitButton]),
            'fields' => '',
            'reCaptchaSiteKey' => $reCaptchaSiteKey,
            'script' => $script,
            'useModal' => $useModal
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}