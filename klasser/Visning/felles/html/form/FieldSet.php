<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;


/**
 * Visning for fieldset i skjema
 *
 * Ressurser:
 * Tilgjengelige variabler:
 * * $id
 * * $class
 * * $style
 * * $label (legend)
 * * $formId
 * * $name
 * * $disabled (bool)
 * * $contents
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class FieldSet extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/form/FieldSet.html';

    protected function forberedData()
    {
        $fieldId = 'id' . md5(rand());
        $datasett = [
            'id' => $fieldId,
            'class' => '',
            'style' => '',
            'label' => '',
            'name' => '',
            'formId' => '',
            'disabled' => false,
            'contents' => ''
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}