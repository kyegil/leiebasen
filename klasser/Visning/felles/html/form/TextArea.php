<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/08/2020
 * Time: 10:37
 */

namespace Kyegil\Leiebasen\Visning\felles\html\form;


use Kyegil\Leiebasen\Visning;

/**
 * Visning for input-felt i skjema
 *
 *  Ressurser:
 *      hidden bool
 *  Mulige variabler:
 *      $id
 *      $name
 *      $label
 *      $class
 *      $style
 *      $required
 *      $disabled
 *      $value
 *      $maxlength
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class TextArea extends Visning
{
    protected $template = 'felles/html/form/TextArea.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['hidden'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $hidden = $this->hentRessurs('hidden');
        $style = $hidden ? 'display: none;' : '';

        $fieldId = 'field_' . md5(rand());
        $datasett = [
            'id' => $fieldId,
            'name' => '',
            'label' => '',
            'class' => '',
            'style' => $style,
            'type' => 'text',
            'required' => false,
            'disabled' => false,
            'value' => '',
            'maxlength' => '',
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}