<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;


/**
 * Visning for dato-felt i skjema
 *
 * Tilgjengelige variabler:
 * * $id The HTML Id attribute
 * * $name
 * * $label
 * * $class
 * * $style
 * * $type
 * * $required
 * * $disabled
 * * $autocomplete
 * * $readonly
 * * $onblur
 * * $min
 * * $minlength
 * * $max
 * * $maxlength
 * * $pattern
 * * $step
 * * $value
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class DateField extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/form/DateField.html';

    /**
     * @return DateField
     */
    protected function forberedData()
    {
        $fieldId = 'datefield_' . md5(rand());
        $datasett = [
            'id' => $fieldId,
            'name' => '',
            'label' => '',
            'class' => '',
            'style' => '',
            'type' => 'date',
            'required' => false,
            'readonly' => false,
            'disabled' => false,
            'autocomplete' => 'off',
            'onblur' => '',
            'min' => '',
            'minlength' => '',
            'max' => '',
            'maxlength' => '',
            'pattern' => '',
            'step' => '',
            'value' => ''
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}