<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;

/**
 * Visning for input-felt type=number i skjema
 *
 *  Ressurser:
 *      decimalSeparator
 *  Mulige variabler:
 *      $id
 *      $name
 *      $label
 *      $class
 *      $style
 *      $type
 *      $required
 *      $disabled
 *      $autocomplete
 *      $readonly
 *      $onblur
 *      $min
 *      $minlength
 *      $max
 *      $maxlength
 *      $pattern
 *      $step
 *      $value
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class NumberField extends Field
{

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $datasett = [
            'type' => 'number',
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}