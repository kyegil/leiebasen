<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;

use Kyegil\Leiebasen\Visning\felles\html\form\select\Option;
use Kyegil\ViewRenderer\ViewArray;
use stdClass;

/**
 * Visning for autocomplete-felt i skjema
 *
 * Ressurser:
 * * value
 * * options \stdClass|array
 * * forceSelection bool = false
 * * tokenSeparators array of tag separators
 * * select2Configs \stdClass Select2 config object
 *
 * Tilgjengelige variabler:
 * * $id The HTML Id attribute
 * * $name the html input name which will be submitted
 * * $label
 * * $class string containing html class or classes
 * * $size
 * * $disabled bool = false
 * * $hidden bool = false
 * * $required bool = false
 * * $multiple bool = false Allow Multiple options
 * * $optionFields String or view containing html option tags
 * * $select2Config Configurations to pass to the select2 constructor
 *
 * @link https://select2.org/configuration/options-api
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class AutoComplete extends \Kyegil\Leiebasen\Visning
{
    /** @var string */
    protected $template = 'felles/html/form/AutoComplete.html';

    /**
     * array_merge_recursive does indeed merge arrays, but it converts values with duplicate
     * keys to arrays rather than overwriting the value in the first array with the duplicate
     * value in the second array, as array_merge does. I.e., with array_merge_recursive,
     * this happens (documented behavior):
     *
     * array_merge_recursive(array('key' => 'org value'), array('key' => 'new value'));
     *     => array('key' => array('org value', 'new value'));
     *
     * array_merge_recursive_distinct does not change the datatypes of the values in the arrays.
     * Matching keys' values in the second array overwrite those in the first array, as is the
     * case with array_merge, i.e.:
     *
     * array_merge_recursive_distinct(array('key' => 'org value'), array('key' => 'new value'));
     *     => array('key' => array('new value'));
     *
     * Parameters are passed by reference, though only for performance reasons. They're not
     * altered by this function.
     *
     * @param array $array1
     * @param array $array2
     * @return array
     * @author Daniel <daniel (at) danielsmedegaardbuus (dot) dk>
     * @author Gabriel Sobrinho <gabriel (dot) sobrinho (at) gmail (dot) com>
     */
    public static function array_merge_recursive_distinct ( array &$array1, array &$array2 ): array
    {
        $merged = $array1;

        foreach ( $array2 as $key => &$value )
        {
            if ( is_array ( $value ) && isset ( $merged [$key] ) && is_array ( $merged [$key] ) )
            {
                $merged [$key] = self::array_merge_recursive_distinct ( $merged [$key], $value );
            }
            else if(is_numeric($key))
            {
                $merged[] = $value;
            }
            else
            {
                $merged [$key] = $value;
            }
        }

        return $merged;
    }

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(is_string($attributt)) {
            if($attributt == 'options') {
                return $this->setOptions($verdi);
            }
            if($attributt == 'select2Configs') {
                return $this->setSelect2ConfigIfNotSet((array)$verdi);
            }
            if(in_array($attributt, ['value', 'forceSelection', 'tokenSeparators'])) {
                return $this->settRessurs($attributt, $verdi);
            }
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * Endre arrays til objekter før de lagres
     *
     * @param array|stdClass $options
     * @return $this
     */
    public function setOptions($options) {
        $options = json_decode(json_encode($options));
        if(is_array($options)) {
            $options = array_combine($options, $options);
        }
        return $this->settRessurs('options', (object)$options);
    }

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $fieldId = $this->data['id'] ?? 'field_' . md5(rand());

        /** Feltet kan være multiselect */
        $value = $this->hentRessurs('value');
        if(!is_array($value)) {
            $value = [$value];
        }

        /** @var stdClass $options */
        $options = $this->hentRessurs('options') ?? new stdClass();
        $optionFields = new ViewArray();
        $forceSelection = $this->hentRessurs('forceSelection') ?? false;
        $tokenSeparators = $this->hentRessurs('tokenSeparators');
        foreach ($options as $option => $optionText) {
            $optionField = $this->app->vis(Option::class, [
                'label' => $optionText,
                'value' => $option,
                'selected' => in_array($option, $value)
            ]);
            $optionFields->addItem($optionField);
        }

        $this->setSelect2ConfigIfNotSet([
            'tags' => !$forceSelection,
            'tokenSeparators' => $tokenSeparators,
            'width' => '100%'
        ]);
        /** @var stdClass $select2Configs */
        $select2Configs = $this->hentRessurs('select2Configs');

        $datasett = [
            'id' => $fieldId,
            'name' => '',
            'label' => '',
            'class' => '',
            'size' => '1',
            'disabled' => false,
            'hidden' => false,
            'required' => false,
            'multiple' => false,
            'optionFields' => $optionFields,
            'select2Config' => json_encode($select2Configs)
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @param string $key
     * @param $value
     * @return $this
     */
    private function setSelect2Config(string $key, $value)
    {
        /** @var object|null $existingConfig */
        $existingConfig = $this->getResource('select2Configs');
        settype($existingConfig, 'array');
        $newConfig = $existingConfig;
        if (
            isset($existingConfig[$key])
            && is_array($existingConfig[$key])
            && is_array($value)
        ) {
            $newConfig[$key] = $this::array_merge_recursive_distinct($existingConfig[$key], $value );
        }
        else {
            $newConfig[$key] = $value;
        }
        $this->setResource('select2Configs', (object)$newConfig);

        return $this;
    }

    /**
     * @param string $key
     * @return bool
     */
    private function hasSelect2Config(string $key): bool
    {
        return isset($this->resources['select2Configs']) && property_exists($this->resources['select2Configs'], $key);
    }

    /**
     * @param mixed[] $configs
     * @return $this
     */
    private function setSelect2ConfigIfNotSet(array $configs)
    {
        foreach($configs as $key => $data) {
            if(!$this->hasSelect2Config($key)) {
                $this->setSelect2Config($key, $data);
            }
        }
        return $this;
    }

}