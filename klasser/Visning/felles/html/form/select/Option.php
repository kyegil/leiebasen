<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/08/2020
 * Time: 10:37
 */

namespace Kyegil\Leiebasen\Visning\felles\html\form\select;


class Option extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/form/select/Option.html';

    protected function forberedData()
    {
        $datasett = [
            'disabled' => false,
            'label' => '',
            'selected' => false,
            'value' => ''
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}