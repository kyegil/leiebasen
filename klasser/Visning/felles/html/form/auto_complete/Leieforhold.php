<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form\auto_complete;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold as LeieforholdModell;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use stdClass;

/**
 * Visning for autocomplete-leieforholdvelger i skjema
 *
 * Ressurser:
 * * value
 * * options \stdClass|array
 * * forceSelection bool = false
 * * tokenSeparators array of tag separators
 * * select2Configs \stdClass Select2 config object
 *
 * Tilgjengelige variabler:
 * * $id The HTML Id attribute
 * * $name the html input name which will be submitted
 * * $label
 * * $class string containing html class or classes
 * * $size
 * * $disabled bool = false
 * * $hidden bool = false
 * * $required bool = false
 * * $multiple bool = true Allow Multiple options
 * * $optionFields String or view containing html option tags
 * * $select2Config Configurations to pass to the select2 constructor
 *
 * @link https://select2.org/configuration/options-api
 * @package Kyegil\Leiebasen\Visning\felles\html\form\auto_complete
 */
class Leieforhold extends \Kyegil\Leiebasen\Visning\felles\html\form\AutoComplete
{
    /**
     * @return Leieforhold
     * @throws Exception
     */
    protected function forberedData(): Leieforhold
    {
        $select2Configs = $this->getResource('select2Configs') ?? new stdClass();
        settype($select2Configs->selectionCssClass, 'string');
        $select2Configs->selectionCssClass .= ' leieforhold-velger';
        $select2Configs->placeholder = $select2Configs->placeholder ?? 'Velg leieforhold';
        $datasett = [
            'label' => 'Leieforhold',
            'multiple' => true,
            'forceSelection' => true,
            'options' => $this->getOptions(),
            'select2Configs' => $select2Configs
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    protected function getOptions(): stdClass
    {
        $options = new stdClass();
        $options->{''} = '';
        /** @var Leieforholdsett $leieforholdsett */
        $leieforholdsett = $this->app->hentSamling(LeieforholdModell::class);
        $leieforholdsett->leggTilSortering(LeieforholdModell::hentPrimærnøkkelfelt(), true);
        $leieforholdsett->leggTilLeieobjektModell();
        foreach ($leieforholdsett as $leieforhold) {
            $options->{$leieforhold->hentId()} = $leieforhold->hentid() . ': ' . $leieforhold->hentBeskrivelse();
        }
        return $options;
    }
}