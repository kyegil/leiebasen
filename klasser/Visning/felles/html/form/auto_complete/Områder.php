<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form\auto_complete;

use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygningsett;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Modell\Områdesett;
use stdClass;

/**
 * Visning for autocomplete-områdevelger i skjema
 *
 * Ressurser:
 * * value
 * * options \stdClass|array
 * * forceSelection bool = false
 * * tokenSeparators array of tag separators
 * * select2Configs \stdClass Select2 config object
 *
 * Tilgjengelige variabler:
 * * $id The HTML Id attribute
 * * $name the html input name which will be submitted
 * * $label
 * * $class string containing html class or classes
 * * $size
 * * $disabled bool = false
 * * $hidden bool = false
 * * $required bool = false
 * * $multiple bool = true Allow Multiple options
 * * $optionFields String or view containing html option tags
 * * $select2Config Configurations to pass to the select2 constructor
 *
 * @link https://select2.org/configuration/options-api
 * @package Kyegil\Leiebasen\Visning\felles\html\form\auto_complete
 */
class Områder extends \Kyegil\Leiebasen\Visning\felles\html\form\AutoComplete
{
    /**
     * @return Områder
     * @throws Exception
     */
    protected function forberedData(): Områder
    {
        $datasett = [
            'name' => 'områder',
            'label' => 'Områder',
            'multiple' => true,
            'forceSelection' => true,
            'options' => $this->getOptions(),
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    protected function getOptions(): stdClass
    {
        $options = new stdClass();
        /** @var Områdesett $områdesett */
        $områdesett = $this->app->hentSamling(Område::class);
        foreach ($områdesett as $område) {
            $options->{$område->hentId()} = $område->hentNavn();
        }
        return $options;
    }
}