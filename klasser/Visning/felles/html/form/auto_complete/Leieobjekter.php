<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form\auto_complete;

use Exception;
use stdClass;

/**
 * Visning for autocomplete-leieforholdvelger i skjema
 *
 * Ressurser:
 * * value
 * * options \stdClass|array
 * * forceSelection bool = false
 * * tokenSeparators array of tag separators
 * * select2Configs \stdClass Select2 config object
 *
 * Tilgjengelige variabler:
 * * $id The HTML Id attribute
 * * $name the html input name which will be submitted
 * * $label
 * * $class string containing html class or classes
 * * $size
 * * $disabled bool = false
 * * $hidden bool = false
 * * $required bool = false
 * * $multiple bool = true Allow Multiple options
 * * $optionFields String or view containing html option tags
 * * $select2Config Configurations to pass to the select2 constructor
 *
 * @link https://select2.org/configuration/options-api
 * @package Kyegil\Leiebasen\Visning\felles\html\form\auto_complete
 */
class Leieobjekter extends \Kyegil\Leiebasen\Visning\felles\html\form\AutoComplete
{
    /**
     * @return Leieobjekter
     */
    protected function forberedData(): Leieobjekter
    {
        $datasett = [
            'name' => 'leieobjekter',
            'label' => 'Leieobjekter',
            'multiple' => true,
            'forceSelection' => true,
            'options' => $this->getOptions(),
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    protected function getOptions(): stdClass
    {
        $options = new stdClass();
        foreach ($this->app->hentAktiveLeieobjekter() as $leieobjekt) {
            $options->{$leieobjekt->hentId()} = $leieobjekt->hentId() . ' – ' . $leieobjekt->hentBeskrivelse();
        }
        return $options;
    }
}