<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\html\shared\HtmlInterface as Html;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;

/**
 * Visning for CKEditor5 HTML-editor i skjema
 *
 * Ressurser:
 * * $ckEditorConfig
 *
 * Tilgjengelige variabler:
 * * $id The HTML Id attribute
 * * $name
 * * $label
 * * $description
 * * $class
 * * $disabled
 * * $value
 * * $cols
 * * $rows
 * * $ckEditorConfigJs
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 * @link https://ckeditor.com/docs/ckeditor5/latest/
 */
class HtmlEditor extends Visning
{

    const CSS_SRC = '//cdn.ckeditor.com/ckeditor5/43.2.0/ckeditor5.css';
    const SCRIPT_SRC = '//cdn.ckeditor.com/ckeditor5/43.2.0/ckeditor5.js';

    protected $template = 'felles/html/form/HtmlEditor.html';

    public static function hentStandardCkEditorConfig(): object
    {
        return (object)[
            'toolbar' => (object)[
                'items' => [
                    'undo',
                    'redo',
                    '|',
                    'sourceEditing',
                    '|',
                    'heading',
                    '|',
                    'bold',
                    'italic',
                    'underline',
                    'strikethrough',
                    'subscript',
                    'superscript',
                    'code',
                    'removeFormat',
                    '|',
                    'horizontalLine',
                    'link',
                    'insertTable',
                    'highlight',
                    'blockQuote',
                    '|',
                    'alignment',
                    '|',
                    'bulletedList',
                    'numberedList',
                    'todoList',
                    'outdent',
                    'indent'
                ],
                'shouldNotGroupWhenFull' => true,
            ],
            'plugins' => [
                new JsCustom('Bold'),
                new JsCustom('AccessibilityHelp'),
                new JsCustom('Alignment'),
                new JsCustom('Autoformat'),
                new JsCustom('Autosave'),
                new JsCustom('BlockQuote'),
                new JsCustom('Code'),
                new JsCustom('Essentials'),
                new JsCustom('Font'),
                new JsCustom('GeneralHtmlSupport'),
                new JsCustom('Heading'),
                new JsCustom('Highlight'),
                new JsCustom('HorizontalLine'),
                new JsCustom('Indent'),
                new JsCustom('IndentBlock'),
                new JsCustom('Italic'),
                new JsCustom('Link'),
                new JsCustom('List'),
                new JsCustom('ListProperties'),
                new JsCustom('Paragraph'),
                new JsCustom('PasteFromOffice'),
                new JsCustom('RemoveFormat'),
                new JsCustom('SelectAll'),
                new JsCustom('SourceEditing'),
                new JsCustom('SpecialCharacters'),
                new JsCustom('Strikethrough'),
                new JsCustom('Style'),
                new JsCustom('Subscript'),
                new JsCustom('Superscript'),
                new JsCustom('Table'),
                new JsCustom('TableCaption'),
                new JsCustom('TableCellProperties'),
                new JsCustom('TableColumnResize'),
                new JsCustom('TableProperties'),
                new JsCustom('TableToolbar'),
                new JsCustom('TextTransformation'),
                new JsCustom('TodoList'),
                new JsCustom('Underline'),
                new JsCustom('Essentials'),
                new JsCustom('Font'),
                new JsCustom('Italic'),
                new JsCustom('Paragraph'),
                new JsCustom('SourceEditing'),
            ],
            'translations' => [
                new JsCustom('coreTranslations')
            ],
            'language' => (object)[
                'ui'    => 'no',
                'content' => 'no',
            ],
        ];
    }


    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(is_string($attributt)) {
            if(in_array($attributt, ['ckEditorConfig'])) {
                return $this->settRessurs($attributt, $verdi);
            }
        }
        return parent::sett($attributt, $verdi);
    }

    protected function forberedData()
    {
        /** @var Html $html */
        $this->instruerAscendant(Html::class, function($html) {
            $htmlHead = $html->getData('head');
            if(stripos($htmlHead, 'ckeditor5') === false) {
                $htmlHead
                    /**
                     * Importer CKEditor
                     * @link https://ckeditor.com/
                     */
                    ->leggTil('links', new HtmlElement('link', [
                        'rel' => "stylesheet",
                        'href' => HtmlEditor::CSS_SRC
                    ]))
                    ->leggTil('scripts', new HtmlElement('script', [
                        'type' => 'importmap',
                    ], json_encode([
                            'imports' => [
                                'ckeditor5' => HtmlEditor::SCRIPT_SRC,
                                'ckeditor5/' => dirname(HtmlEditor::SCRIPT_SRC) . '/',
                            ],
                        ])
                    ))
                    ;
            }
        });
        $standardConfig = static::hentStandardCkEditorConfig();
        $ckEditorConfig = $this->hentRessurs('ckEditorConfig') ?: $standardConfig;
        $ckEditorConfig->toolbar = $ckEditorConfig->toolbar ?? $standardConfig->toolbar;
        $ckEditorConfig->toolbar->items = $ckEditorConfig->toolbar->items ?? $standardConfig->toolbar->items;
        $ckEditorConfig->plugins = array_merge($ckEditorConfig->plugins ?? [], $standardConfig->plugins);

        $fieldId = 'field_' . md5(rand());
        $datasett = [
            'id' => $fieldId,
            'name' => '',
            'label' => '',
            'description' => '',
            'class' => '',
            'type' => 'text',
            'disabled' => false,
            'value' => '',
            'cols' => 20,
            'rows' => 2,
            'ckEditorConfigJs' => DataTable::JsonImproved($ckEditorConfig)
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}