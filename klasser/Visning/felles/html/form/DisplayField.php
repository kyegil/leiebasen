<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;


/**
 * Visning for display-felt i skjema
 *
 * Tilgjengelige variabler:
 * * $id The HTML Id attribute
 * * $label
 * * $class
 * * $type
 * * $disabled
 * * $value
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class DisplayField extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/form/DisplayField.html';

    protected function forberedData()
    {
        $fieldId = 'field_' . md5(rand());
        $datasett = [
            'id' => $fieldId,
            'label' => '',
            'class' => '',
            'type' => 'text',
            'disabled' => false,
            'value' => ''
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}