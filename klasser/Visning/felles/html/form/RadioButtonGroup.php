<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;


use Kyegil\Leiebasen\Visning;
use Kyegil\ViewRenderer\ViewArray;
use stdClass;

/**
 * Visning for radioknapp-gruppe i skjema
 *
 * Ressurser:
 * * options \stdClass|[]
 * * name
 * * value
 *
 * Tilgjengelige variabler:
 * * $id
 * * $class
 * * $label
 * * $description
 * * $required
 * * $optionFields
 *
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class RadioButtonGroup extends Visning
{
    /** @var string */
    protected $template = 'felles/html/form/RadioButtonGroup.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(is_string($attributt)) {
            if($attributt == 'options') {
                return $this->setOptions($verdi);
            }
            if(in_array($attributt, ['name', 'value'])) {
                return $this->settRessurs($attributt, $verdi);
            }
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * Endre arrays til objekter før de lagres
     *
     * @param array|stdClass $options
     * @return $this
     */
    public function setOptions($options) {
        if(is_array($options)) {
            $options = array_combine($options, $options);
        }
        return $this->settRessurs('options', (object)$options);
    }

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $fieldId = 'field_' . md5(rand());
        $name = $this->hentRessurs('name');
        $value = $this->hentRessurs('value');
        $fieldId = $this->data['id'] ?? $fieldId;

        /** @var \stdClass $options */
        $options = $this->hentRessurs('options');
        $optionFields = new ViewArray();
        foreach ($options as $option => $optionText) {
            $optionField = $this->app->vis(RadioButton::class, [
                'id' => "{$fieldId}-{$option}",
                'label' => $optionText,
                'name' => $name,
                'value' => $option,
                'checked' => $option == $value
            ]);
            $optionFields->addItem($optionField);
        }

        $datasett = [
            'id' => $fieldId,
            'class' => '',
            'label' => '',
            'description' => '',
            'optionFields' => $optionFields
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}