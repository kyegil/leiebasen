<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;


/**
 * Visning for Collapsible Section i skjema e.l.
 *
 * Ressurser:
 *
 * Tilgjengelige variabler:
 * * $id
 * * $class
 * * $style
 * * $label
 * * $name
 * * $contents
 * * $collapsed (bool) defaults to true
 * * $draggable (bool) defaults to false
 * * $ondragstart
 * * $ondrag
 * * $ondragend
 * * $ondragenter
 * * $ondragover
 * * $ondragleave
 * * $ondrop
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class CollapsibleSection extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/form/CollapsibleSection.html';

    protected function forberedData()
    {
        $fieldId = 'id' . md5(rand());
        $datasett = [
            'id' => $fieldId,
            'class' => '',
            'style' => '',
            'label' => '',
            'name' => '',
            'contents' => '',
            'collapsed' => true,

            'draggable' => false,
            'ondrag' => '',
            'ondragenter' => '',
            'ondragstart' => '',
            'ondragover' => '',
            'ondragend' => '',
            'ondragleave' => '',
            'ondrop' => '',
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}