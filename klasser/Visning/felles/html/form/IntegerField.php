<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;

/**
 * Visning for input-felt type=email i skjema
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $id
 *      $name
 *      $label
 *      $class
 *      $style
 *      $type
 *      $required
 *      $disabled
 *      $autocomplete
 *      $readonly
 *      $onblur
 *      $value
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class IntegerField extends Field
{

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $datasett = [
            'type' => 'number',
            'min' => 0,
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}