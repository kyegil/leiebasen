<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;


/**
 * Visning for radioknapp i skjema
 *
 * Tilgjengelige variabler:
 * * $id
 * * $class
 * * $name
 * * $label
 * * $value
 * * $onclick
 * * $required bool = false
 * * $disabled bool = false
 * * $checked bool = false
 *
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class RadioButton extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/form/RadioButton.html';

    /**
     * @return RadioButton
     */
    protected function forberedData()
    {
        $fieldId = 'field_' . md5(rand());
        $datasett = [
            'id' => $fieldId,
            'class' => '',
            'name' => '',
            'label' => '',
            'value' => '',
            'onclick' => '',
            'required' => false,
            'disabled' => false,
            'checked' => false,
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}