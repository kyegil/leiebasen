<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;


use Kyegil\Leiebasen\Visning;

/**
 * Visning for input-felt i skjema
 *
 * Tilgjengelige variabler:
 * * $id The HTML Id attribute
 * * $name
 * * $label
 * * $class
 * * $style
 * * $type
 * * $inputmode none|text|decimal|numeric|tel|search|email|url
 * * $required
 * * $disabled
 * * $autocomplete
 * * $readonly
 * * $onblur
 * * $min
 * * $minlength
 * * $max
 * * $maxlength
 * * $pattern
 * * $placeholder
 * * $step
 * * $list
 * * $value
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class Field extends Visning
{
    /** @var string */
    protected $template = 'felles/html/form/Field.html';

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $fieldId = 'field_' . md5(rand());
        $datasett = [
            'id' => $fieldId,
            'name' => '',
            'label' => '',
            'class' => '',
            'style' => '',
            'inputmode' => '',
            'type' => 'text',
            'required' => false,
            'readonly' => false,
            'disabled' => false,
            'autocomplete' => 'off',
            'onblur' => '',
            'min' => '',
            'minlength' => '',
            'max' => '',
            'maxlength' => '',
            'pattern' => '',
            'placeholder' => '',
            'step' => '',
            'list' => '',
            'value' => ''
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}