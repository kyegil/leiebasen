<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;


/**
 * Visning for input-felt i skjema
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $id
 *      $name
 *      $label
 *      $class
 *      $style
 *      $type
 *      $required
 *      $disabled
 *      $autocomplete
 *      $readonly
 *      $onblur
 *      $min
 *      $minlength
 *      $max
 *      $maxlength
 *      $pattern
 *      $step
 *      $value
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class DateTimeField extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/form/DateTimeField.html';

    /**
     * @return DateTimeField
     */
    protected function forberedData()
    {
        $fieldId = 'datetimefield_' . md5(rand());
        $datasett = [
            'id' => $fieldId,
            'name' => '',
            'label' => '',
            'class' => '',
            'style' => '',
            'type' => 'datetime-local',
            'required' => false,
            'readonly' => false,
            'disabled' => false,
            'autocomplete' => 'off',
            'onblur' => '',
            'min' => '',
            'minlength' => '',
            'max' => '',
            'maxlength' => '',
            'pattern' => '',
            'step' => '',
            'value' => ''
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}