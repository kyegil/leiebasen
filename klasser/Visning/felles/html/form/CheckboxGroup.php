<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;


use Kyegil\Leiebasen\Visning;
use Kyegil\ViewRenderer\ViewArray;
use stdClass;

/**
 * Visning for checkbox-gruppe i skjema
 *
 * Ressurser:
 * * options \stdClass|[]
 * * name
 * * value
 * * disabled string[] List of disabled options
 *
 * Tilgjengelige variabler:
 * * $id
 * * $class
 * * $label
 * * $description
 * * $optionFields
 * * $required
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class CheckboxGroup extends Visning
{
    /** @var string */
    protected $template = 'felles/html/form/CheckboxGroup.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(is_string($attributt)) {
            if ($attributt == 'options') {
                return $this->setOptions($verdi);
            }
            if (in_array($attributt, ['disabled', 'value'])) {
                return $this->settRessurs($attributt, (array)$verdi);
            }
            if ($attributt == 'name') {
                return $this->settRessurs($attributt, $verdi);
            }
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * Endre arrays til objekter før de lagres
     *
     * @param array|stdClass $options
     * @return $this
     */
    public function setOptions($options) {
        $options = json_decode(json_encode($options));
        if(is_array($options)) {
            $options = array_combine($options, $options);
        }
        return $this->settRessurs('options', (object)$options);
    }

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $fieldId = 'checkboxgroup_' . md5(rand());
        /** @var string $name */
        $name = $this->hentRessurs('name');
        /** @var array $value */
        $value = $this->hentRessurs('value') ?? [];
        /** @var array $disabled */
        $disabled = $this->hentRessurs('disabled') ?? [];
        $fieldId = $this->data['id'] ?? $fieldId;

        /** @var stdClass $options */
        $options = $this->hentRessurs('options') ?? new stdClass();
        $optionFields = new ViewArray();
        foreach ($options as $option => $optionText) {
            $optionField = $this->app->vis(Checkbox::class, [
                'id' => "{$fieldId}-{$option}",
                'label' => $optionText,
                'name' => $name . '[]',
                'value' => $option,
                'uncheckedValue' => null,
                'disabled' => in_array($option, $disabled),
                'checked' => in_array($option, $value),
            ]);
            $optionFields->addItem($optionField);
        }

        $datasett = [
            'id' => 'checkboxgroup_' . md5(rand()),
            'class' => '',
            'label' => '',
            'description' => '',
            'required' => false,
            'optionFields' => $optionFields
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}