<?php

namespace Kyegil\Leiebasen\Visning\felles\html\form;

use Kyegil\Leiebasen\Visning\felles\html\form\select\Option;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for select-felt i skjema
 *
 * Ressurser:
 * * value
 * * options
 *
 * Tilgjengelige variabler:
 * * $id
 * * $name
 * * $label
 * * $class
 * * $disabled
 * * $hidden
 * * $required
 * * $multiple
 * * $optionFields
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class Select extends \Kyegil\Leiebasen\Visning
{
    /** @var string */
    protected $template = 'felles/html/form/Select.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(is_string($attributt)) {
            if($attributt == 'options') {
                return $this->setOptions($verdi);
            }
            if(in_array($attributt, ['value'])) {
                return $this->settRessurs($attributt, $verdi);
            }
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * Endre arrays til objekter før de lagres
     *
     * @param array|\stdClass $options
     * @return $this
     */
    public function setOptions($options) {
        $options = json_decode(json_encode($options));
        if(is_array($options)) {
            $options = array_combine($options, $options);
        }
        return $this->settRessurs('options', (object)$options);
    }

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $fieldId = 'field_' . md5(rand());
        $fieldId = isset($this->data['id']) ? $this->data['id'] : $fieldId;
        $value = $this->hentRessurs('value');

        /** @var \stdClass $options */
        $options = $this->hentRessurs('options') ?? new \stdClass();
        $optionFields = new ViewArray();
        foreach ($options as $option => $optionText) {
            $optionField = $this->app->vis(Option::class, [
                'label' => $optionText,
                'value' => $option,
                'selected' => $option == $value
            ]);
            $optionFields->addItem($optionField);
        }

        $datasett = [
            'id' => $fieldId,
            'name' => '',
            'label' => '',
            'class' => '',
            'disabled' => false,
            'hidden' => false,
            'required' => false,
            'multiple' => false,
            'optionFields' => $optionFields
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}