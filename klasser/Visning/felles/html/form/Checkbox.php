<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/08/2020
 * Time: 10:37
 */

namespace Kyegil\Leiebasen\Visning\felles\html\form;

/**
 * Visning for checkbox i skjema
 *
 *  Ressurser:
 *
 * Tilgjengelige variabler:
 * * $id
 * * $class
 * * $name
 * * $label
 * * $value = 1
 * * $uncheckedValue Set to Null to avoid submitting unchecked value
 * * $onclick
 * * $required bool = false
 * * $disabled bool = false
 * * $checked bool = false
 *
 * @package Kyegil\Leiebasen\Visning\felles\html\form
 */
class Checkbox extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/form/Checkbox.html';

    /**
     * @return $this
     */
    protected function forberedData(): Checkbox
    {
        $fieldId = 'field_' . md5(rand());
        $data = $this->getData();
        $uncheckedValue = key_exists('uncheckedValue', $data) ? $data['uncheckedValue'] : 0;
        $datasett = [
            'id' => $fieldId,
            'class' => '',
            'name' => '',
            'label' => '',
            'value' => 1,
            'uncheckedValue' => $uncheckedValue,
            'onclick' => '',
            'required' => false,
            'disabled' => false,
            'checked' => false,
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }
}