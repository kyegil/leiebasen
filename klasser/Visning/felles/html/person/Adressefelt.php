<?php

namespace Kyegil\Leiebasen\Visning\felles\html\person;


use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning;

/**
 * Class Adressefelt
 *
 * Ressurser:
 * * person \Kyegil\Leiebasen\Modell\Person
 *
 * Tilgjengelige variabler:
 * * $navn
 * * $adresse1
 * * $adresse2
 * * $postnr
 * * $poststed
 * * $land
 * @package Kyegil\Leiebasen\Visning\felles\html\person
 */
class Adressefelt extends Visning
{
    /** @var string */
    protected $template = 'felles/html/person/Adressefelt.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(is_string($attributt)) {
            if(in_array($attributt, ['person'])) {
                return $this->settRessurs($attributt, $verdi);
            }
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Person $person */
        $person = $this->hentRessurs('person');

        $datasett = [
            'navn' => $person->hentNavn(),
            'adresse1' => $person->hentAdresse1(),
            'adresse2' => $person->hentAdresse2(),
            'postnr' => $person->hentPostnr(),
            'poststed' => $person->hentPoststed(),
            'land' => $person->hentLand() != 'Norge' ? $person->hentLand() : ''
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}