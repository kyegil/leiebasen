<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/08/2020
 * Time: 10:37
 */

namespace Kyegil\Leiebasen\Visning\felles\html\table;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Jshtml\PureJs;
use Kyegil\Leiebasen\Visning\felles\html\shared\HeadInterface;
use Kyegil\Leiebasen\Visning\felles\html\shared\HtmlInterface as Html;
use Kyegil\ViewRenderer\ViewInterface;
use stdClass;

/**
 * Visning for DataTables Tabell
 *
 *  Ressurser:
 * * $classes array
 * * $styles object
 * * object[]|string[] kolonner Konfigurasjonsobjekter som videresendes til
 *   \Kyegil\Leiebasen\Visning\felles\html\table\Th
 * * object[] data
 * * object dataTablesConfig
 *
 * Tilgjengelige variabler:
 * * $tableId
 * * $caption
 * * $tableHead
 * * $tableBody
 * * $tableFoot
 * * $class
 * * $style
 * * $dataTablesConfigJs
 * @link https://datatables.net/reference/index
 * @see \Kyegil\Leiebasen\Visning\felles\html\table\Th
 * @package Kyegil\Leiebasen\Visning\felles\html\table
 */
class DataTable extends HtmlTable
{
    const DATATABLES_SOURCE_JS = '//cdn.datatables.net/2.1.8/js/dataTables.min.js';
    const DATATABLES_SOURCE_CSS = '//cdn.datatables.net/2.1.8/css/dataTables.dataTables.min.css';
    const DATATABLES_SOURCE_RESPONSIVE_JS = '//cdn.datatables.net/responsive/3.0.3/js/dataTables.responsive.js';
    const DATATABLES_SOURCE_RESPONSIVE_CSS = '//cdn.datatables.net/responsive/3.0.3/css/responsive.dataTables.css';
    const DATATABLES_SOURCE_PLUGIN_NO_NB = '//cdn.datatables.net/plug-ins/2.1.8/i18n/no-NB.json';

    protected $template = 'felles/html/table/DataTable.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(is_string($attributt)) {
            if(in_array($attributt, ['dataTablesConfig'])) {
                return $this->settRessurs($attributt, $verdi);
            }
        }
        parent::sett($attributt, $verdi);
        return $this;
    }

    /**
     * @return $this
     */
    protected function forberedData()
    {
        /** @var Html $html */
        $this->instruerAscendant(Html::class, function($html) {
            /** @var HeadInterface|null $htmlHead */
            $htmlHead = $html->getData('head');
            if(stripos($htmlHead, 'dataTables') === false) {
                $htmlHead
                    /**
                     * Importer DataTables
                     * @link https://datatables.net/
                     */
                    ->leggTil('scripts', new HtmlElement('script', [
                        'src' => self::DATATABLES_SOURCE_JS
                    ]))
                    ->leggTil('links', new HtmlElement('link', [
                        'rel' => "stylesheet",
                        'href' => self::DATATABLES_SOURCE_CSS
                    ]))
                    /**
                     * Importer DataTables Responsive Extension
                     * @link https://datatables.net/
                     */
                    ->leggTil('scripts', new HtmlElement('script', [
                        'src' => self::DATATABLES_SOURCE_RESPONSIVE_JS
                    ]))
                    ->leggTil('links', new HtmlElement('link', [
                        'rel' => "stylesheet",
                        'href' => self::DATATABLES_SOURCE_RESPONSIVE_CSS
                    ]))
                ;

            }
        });
        /** @var array|null $classes */
        $classes = $this->hentRessurs('classes');
        settype($classes, 'array');
        $classes = array_unique(array_merge($classes, ['display']));
        $this->settRessurs('classes', $classes);

        /** @var stdClass $dataTablesConfig */
        $dataTablesConfig = $this->hentRessurs('dataTablesConfig') ?: new stdClass();
        if (!isset($dataTablesConfig->language) || !isset($dataTablesConfig->language->url)) {
            settype($dataTablesConfig->language, 'object');
            $dataTablesConfig->language->url = self::DATATABLES_SOURCE_PLUGIN_NO_NB;
        }
        if (!isset($dataTablesConfig->lengthMenu)) {
            $dataTablesConfig->lengthMenu = [50, 100, 200, 300, 400, 500];
            if(isset($dataTablesConfig->pageLength)) {
                $dataTablesConfig->lengthMenu[] = $dataTablesConfig->pageLength;
                sort($dataTablesConfig->lengthMenu);
            }
        }
        if (isset($dataTablesConfig->ajax) && !isset($dataTablesConfig->ajax->data)) {
            $dataTablesConfig->ajax->data = new JsFunction("if(typeof data.order !== 'undefined'){data.order.forEach(function(column){column.columnName = data.columns[column.column].name;});}if(typeof data.length !== 'undefined'){data.limit = data.length;};", ['data', 'settings']);
        }
        $datasett = [
            'dataTablesConfigJs' => $this::JsonImproved($dataTablesConfig)
        ];
        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }

    /**
     * @param mixed $verdi
     * @return string
     */
    public static function JsonImproved($verdi): string
    {
        $json = '';
        if($verdi instanceof PureJs) {
            $json .= $verdi;
        }
        else if($verdi instanceof ViewInterface) {
            $json .= json_encode((string)$verdi);
        }
        else if($verdi instanceof stdClass) {
            $json .= "{\n";
            $array = [];
            foreach($verdi as $property => $value) {
                $array[] = "{$property}: " . self::JsonImproved($value);
            }
            $json .= implode(",\n", $array);
            $json .= "\n}\n";
        }
        else if(is_array($verdi)) {
            $verdi = array_map([self::class, 'JsonImproved'], $verdi);
            $json .= "[\n" . implode(",\n", $verdi) . "\n]\n";
        }
        else {
            $json .= json_encode($verdi, JSON_UNESCAPED_UNICODE);
        }
        return $json;
    }
}