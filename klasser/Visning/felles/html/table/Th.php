<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/08/2020
 * Time: 10:37
 */

namespace Kyegil\Leiebasen\Visning\felles\html\table;


use Kyegil\Leiebasen\Visning;

/**
 * Visning for Tabell-Overskriftcelle
 *
 *  Ressurser:
 * * $title string
 * * $classes array
 * * $styles object
 * * $width string
 *
 * Tilgjengelige variabler:
 * * $class
 * * $style
 * * $scope
 * * $content
 * * $dataName
 * @package Kyegil\Leiebasen\Visning\felles\html\table
 */
class Th extends Visning
{
    protected $template = 'felles/html/table/Th.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(is_string($attributt)) {
            if(in_array($attributt, ['title', 'width', 'classes', 'styles'])) {
                return $this->settRessurs($attributt, $verdi);
            }
        }
        return parent::sett($attributt, $verdi);
    }

    protected function forberedData()
    {
        $datasett = [
            'class' => '',
            'style' => '',
            'scope' => '',
            'content' => '',
            'dataName' => $this->hent('data') ?? '',
        ];
        /** @var string[]|null $classes */
        $classes = $this->hentRessurs('classes');
        if($classes) {
            $datasett['class'] = implode(' ', $classes);
        }
        /** @var object|null $styles */
        $styles = (object)$this->hentRessurs('styles');
        /** @var string|null $title */
        $width = $this->hentRessurs('width');
        if($width) {
            $styles->width = $width;
        }
        foreach ($styles as $style => $value) {
            $datasett['style'] .= " {$style}: {$value};";
        }
        /** @var string|null $title */
        $title = $this->hentRessurs('title');
        if($title) {
            $datasett['content'] = $title;
        }
        $this->definerData($datasett);
        return parent::forberedData();
    }
}