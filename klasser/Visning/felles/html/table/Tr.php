<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/08/2020
 * Time: 10:37
 */

namespace Kyegil\Leiebasen\Visning\felles\html\table;


use Kyegil\Leiebasen\Visning;

/**
 * Visning for Tabell-linje
 *
 *  Ressurser:
 * * $classes array
 * * $styles object
 *
 * Tilgjengelige variabler:
 * * $class
 * * $style
 * * $dataId
 * * $cells
 * @package Kyegil\Leiebasen\Visning\felles\html\table
 */
class Tr extends Visning
{
    protected $template = 'felles/html/table/Tr.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(is_string($attributt)) {
            if(in_array($attributt, ['classes', 'styles'])) {
                return $this->settRessurs($attributt, $verdi);
            }
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $datasett = [
            'class' => '',
            'style' => '',
            'dataId' => '',
            'cells' => ''
        ];
        /** @var string[]|null $classes */
        $classes = $this->hentRessurs('classes');
        if($classes) {
            $datasett['class'] = implode(' ', $classes);
        }
        /** @var object|null $styles */
        $styles = (object)$this->hentRessurs('styles');
        foreach ($styles as $style => $value) {
            $datasett['style'] .= " {$style}: {$value};";
        }
        $this->definerData($datasett);
        return parent::forberedData();
    }
}