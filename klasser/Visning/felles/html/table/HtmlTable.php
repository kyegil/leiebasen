<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/08/2020
 * Time: 10:37
 */

namespace Kyegil\Leiebasen\Visning\felles\html\table;


use Kyegil\Leiebasen\Visning;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for HTML Table
 *
 *  Ressurser:
 * * $classes array
 * * $styles object
 * * object[]|string[] kolonner Konfigurasjonsobjekter som videresendes til
 *   \Kyegil\Leiebasen\Visning\felles\html\table\Th
 * * object[] data
 * * string $idFelt Data-feltet som representerer primærnøkkelen for linjene
 *
 * Tilgjengelige variabler:
 * * $tableId
 * * $caption
 * * $tableHead
 * * $tableBody
 * * $tableFoot
 * * $class
 * * $style
 * @see \Kyegil\Leiebasen\Visning\felles\html\table\Th
 * @package Kyegil\Leiebasen\Visning\felles\html\table
 */
class HtmlTable extends Visning
{
    protected $template = 'felles/html/table/HtmlTable.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return HtmlTable
     */
    public function sett($attributt, $verdi = null)
    {
        if(is_string($attributt)) {
            if(in_array($attributt, ['kolonner', 'data', 'classes', 'styles', 'idFelt'])) {
                return $this->settRessurs($attributt, $verdi);
            }
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return HtmlTable
     */
    protected function forberedData()
    {
        /** @var object[]|string[]|null $kolonner */
        $kolonner = $this->hentRessurs('kolonner');
        /** @var object[]|null $data */
        $data = $this->hentRessurs('data');
        if($kolonner) {
            $tableHead = $this->lagHeader($kolonner);
        }
        elseif ($data) {
            $tableHead = $this->lagHeaderFraData($data);
        }
        else {
            $tableHead = $this->app->vis(TableHead::class, [
                'rows' => $this->app->vis(Tr::class, [])
            ]);
        }
        if($data) {
            $tableBody = $this->lagBody($data);
        }
        else {
            $tableBody = $this->app->vis(TableBody::class, [
                'rows' => $this->app->vis(Tr::class, [])
            ]);
        }
        $tableId = 'table_' . md5(rand());
        $datasett = [
            'tableId' => $tableId,
            'caption' => '',
            'tableHead' => $tableHead,
            'tableBody' => $tableBody,
            'tableFoot' => '',
            'class' => '',
            'style' => ''
        ];
        /** @var string[]|null $classes */
        $classes = $this->hentRessurs('classes');
        if($classes) {
            $datasett['class'] = implode(' ', $classes);
        }
        /** @var object|null $styles */
        $styles = (object)$this->hentRessurs('styles');
        foreach ($styles as $style => $value) {
            $datasett['style'] .= " {$style}: {$value};";
        }
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @param string[]|object[]|array[] $kolonner
     * @return TableHead
     */
    protected function lagHeader(array $kolonner)
    {
        $overskrifter = new ViewArray();
        foreach($kolonner as $kolonne) {
            if(is_scalar($kolonne)) {
                $overskrifter->addItem($this->app->vis(Th::class, [
                    'content' => $kolonne,
                    'dataName' => $kolonne
                ]));
            }
            else if(is_object($kolonne) || is_array($kolonne)) {
                $overskrifter->addItem($this->app->vis(Th::class, (array)$kolonne));
            }
        }
        /** @var TableHead $tableHead */
        $tableHead = $this->app->vis(TableHead::class, [
            'rows' => $this->app->vis(Tr::class, [
                'cells' => $overskrifter
            ])
        ]);
        return $tableHead;
    }

    /**
     * @param string[]|object[]|array[] $data
     * @return Visning
     */
    protected function lagHeaderFraData(array $data)
    {
        $overskrifter = new ViewArray();
        $kolonner = reset($data);
        foreach($kolonner as $kolonne) {
            if(is_scalar($kolonne)) {
                $overskrifter->addItem($this->app->vis(Th::class, ['content' => $kolonne]));
            }
            else if((is_object($kolonne) || is_array($kolonne)) && key_exists('label', array($kolonne))) {
                $overskrifter->addItem($this->app->vis(Th::class, ['content' => array($kolonne)['label']]));
            }
        }
        return $this->app->vis(TableHead::class, [
            'rows' => $this->app->vis(Tr::class, [
                'cells' => $overskrifter
            ])
        ]);
    }

    /**
     * @param array $data
     * @return Visning|\Kyegil\ViewRenderer\ViewInterface
     */
    protected function lagBody(array $data)
    {
        $linjer = new ViewArray();
        /** @var object[]|string[]|null $kolonner */
        $kolonner = $this->hentRessurs('kolonner');
        /** @var string|null $idFelt */
        $idFelt = $this->hentRessurs('idFelt');

        foreach($data as $dataObjekt) {
            settype($dataObjekt, 'object');
            $linje = new ViewArray();
            $dataId = $idFelt && property_exists($dataObjekt, $idFelt) ? $dataObjekt->{$idFelt} : '';
            if($kolonner) {
                foreach($kolonner as $kolonneindex => $kolonne) {
                    if(property_exists($kolonne, 'data')) {
                        $dataName = $kolonne->data;
                        $content = property_exists($dataObjekt, $dataName) ? $dataObjekt->{$dataName} : '';
                    }
                    else if(!is_numeric($kolonneindex)) {
                        $dataName = $kolonneindex;
                        $content = property_exists($dataObjekt, $dataName) ? $dataObjekt->{$dataName} : '';
                    }
                    else {
                        $dataArray = array_values((array)$dataObjekt);
                        $content = $dataArray[$kolonneindex] ?? '';
                        $dataName = is_numeric($dataArray[$kolonneindex]) ? '' : $dataArray[$kolonneindex];
                    }
                    $linje->addItem($this->app->vis(Td::class, [
                        'content' => $content,
                        'classes' => $kolonne->classes ?? [],
                        'dataName' => $dataName,
                        'dataId' => $dataId,
                    ]));
                }
            }
            else{
                $dataId = '';
                foreach($dataObjekt as $index => $value) {
                    $dataId = $idFelt && !$dataId && $index == $idFelt ? $value : '';
                    $linje->addItem($this->app->vis(Td::class, [
                        'content' => $value,
                        'dataId' => $dataId,
                    ]));
                    if ($dataId) {
                        foreach ($linje as $td) {
                            $td->sett('dataId', $dataId);
                        }
                    }
                }
            }
            $linjer->addItem($this->app->vis(Tr::class, [
                'dataId' => $dataId,
                'cells' => $linje,
            ]));
        }
        return $this->app->vis(TableBody::class, [
            'rows' => $linjer
        ]);
    }
}