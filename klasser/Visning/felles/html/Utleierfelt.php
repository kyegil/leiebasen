<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/08/2020
 * Time: 10:37
 */

namespace Kyegil\Leiebasen\Visning\felles\html;


use Kyegil\Leiebasen\Visning;
use Kyegil\ViewRenderer\AppInterface;
use Kyegil\ViewRenderer\ViewFactory;

/**
 * Visning for Utleierfelt
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $navn
 *      $adresse
 *      $postnr
 *      $poststed
 *      $orgNr
 * @package Kyegil\Leiebasen\Visning\felles\html
 */
class Utleierfelt extends Visning
{
    /** @var string */
    protected $template = 'felles/html/Utleierfelt.html';

    /**
     * Utleierfelt constructor.
     * @param ViewFactory $viewFactory
     * @param AppInterface $app
     * @param array $data
     */
    public function __construct(ViewFactory $viewFactory, AppInterface $app, array $data = [])
    {
        $data['navn'] = $data['navn'] ?? $app->hentValg('utleier');
        $data['adresse'] = $data['adresse'] ?? $app->hentValg('adresse');
        $data['postnr'] = $data['postnr'] ?? $app->hentValg('postnr');
        $data['poststed'] = $data['poststed'] ?? $app->hentValg('poststed');
        $data['orgNr'] = $data['orgNr'] ?? $app->hentValg('orgnr');

        parent::__construct($viewFactory, $app, $data);
    }
}
