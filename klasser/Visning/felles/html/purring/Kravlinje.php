<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\felles\html\purring;


use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Visning;

/**
 * Class Kravlinje
 *
 * Linjeinnhold for faktura
 *
 *  Ressurser:
 *      $krav \Kyegil\Leiebasen\Modell\Leieforhold\Krav
 *  Mulige variabler:
 *      $tekst
 *      $forfallsdato
 *      $beløp
 *      $linjeNummer
 *      $antallLinjer
 * @package Kyegil\Leiebasen\Visning\felles\html\regning
 */
class Kravlinje extends Visning
{
    /** @var string  */
    protected $template = 'felles/html/purring/Kravlinje.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['krav'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     */
    protected function forberedData()
    {
        /** @var Krav $krav */
        $krav = $this->hentRessurs('krav');
        if($krav) {
            $this->definerData([
                'tekst' => $krav->hentTekst(),
                'forfallsdato' => $krav->hentForfall() ? $krav->hentForfall()->format('d.m.Y') : '',
                'beløp' => $krav->hentBeløp(),
                'linjeNummer' => '',
                'antallLinjer' => ''
            ]);
        }
        else {
            $this->definerData([
                'tekst' => '',
                'forfallsdato' => '',
                'beløp' => '',
                'linjeNummer' => '',
                'antallLinjer' => ''
            ]);
        }

        return parent::forberedData();
    }
}