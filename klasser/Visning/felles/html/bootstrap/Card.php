<?php

namespace Kyegil\Leiebasen\Visning\felles\html\bootstrap;


use Kyegil\Leiebasen\Visning\Common;

/**
 * Visning for bootstrap-kort med innhold
 *
 * Tilgjengelige variabler:
 * * $id kortets html id
 * * $class
 * * $header
 * * $tittel
 * * $innhold
 * * $footer
 * @package Kyegil\Leiebasen\Visning\felles\bootstrap
 */
class Card extends Common
{
    protected $template = 'felles/html/bootstrap/Card.html';

    protected function forberedData(): Card
    {
        $kortId = 'kort_' . md5(rand());
        $this->definerData([
            'id' => $kortId,
            'class' => '',
            'header' => '',
            'tittel' => '',
            'innhold' => '',
            'footer' => '',
        ]);
        parent::forberedData();
        return $this;
    }
}