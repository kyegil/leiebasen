<?php
/**
 * Part of boligstiftelsen.svartlamon.org
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling;


use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste as TjenesteModell;

/**
 * Panel-innhold for krav
 *
 *  Ressurser:
 *      $tjeneste \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste
 *  Mulige variabler:
 *      $tjenesteId
 *      $navn
 *      $beløp
 * @package Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling
 */
class Tjeneste extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/kostnadsdeling/Tjeneste.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['tjeneste'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var TjenesteModell $tjeneste */
        $tjeneste = $this->hentRessurs('tjeneste');

        $this->definerData([
            'tjenesteId' => strval($tjeneste),
            'navn' => $tjeneste ? $tjeneste->hentNavn() : '',
            'beskrivelse' => $tjeneste ? $tjeneste->hentBeskrivelse() : '',
            'leverandør' => $tjeneste ? $tjeneste->hentLeverandør() : '',
            'avtalereferanse' => $tjeneste ? $tjeneste->hentAvtalereferanse() : '',
        ]);
        return parent::forberedData();
    }
}