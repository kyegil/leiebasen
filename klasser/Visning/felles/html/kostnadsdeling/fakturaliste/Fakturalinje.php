<?php

namespace Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling\fakturaliste;


/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      $faktura \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad
 *  Mulige variabler:
 *      $id
 *      $fakturanummer
 *      $beløp
 *      $anleggsnummer
 *      $fraDato
 *      $tilDato
 *      $termin
 *      $forbruk
 *      $beregnet
 *      $fordelt
 * @package Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling\fakturaliste
 */
class Fakturalinje extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/kostnadsdeling/fakturaliste/Fakturalinje.html';

    /**
     * @var array
     */
    protected $data = [
        'id' => null,
        'fakturanummer' => null,
        'beløp' => null,
        'anleggsnummer' => null,
        'fraDato' => null,
        'tilDato' => null,
        'termin' => null,
        'forbruk' => null,
        'beregnet' => null,
        'fordelt' => null
    ];

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['faktura'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad $faktura */
        $faktura = $this->hentRessurs('faktura');

        if($faktura) {
            $this->definerData([
                'id' => $faktura->hentId(),
                'fakturanummer' => $faktura->hentFakturanummer(),
                'beløp' => $this->app->kr($faktura->hentFakturabeløp()),
                'anleggsnummer' => strval($faktura->hentAnlegg()),
                'fraDato' => $faktura->hentFradato() ? $faktura->hentFradato()->format('d.m.Y') : '',
                'tilDato' => $faktura->hentTildato() ? $faktura->hentTildato()->format('d.m.Y') : '',
                'termin' => $faktura->hentTermin(),
                'forbruk' => $faktura->hentForbruk() ? "{$faktura->hentForbruk()} kWh" : '',
                'beregnet' => $faktura->hentBeregnet(),
                'fordelt' => $faktura->hentFordelt()
            ]);
        }

        return parent::forberedData();
    }
}