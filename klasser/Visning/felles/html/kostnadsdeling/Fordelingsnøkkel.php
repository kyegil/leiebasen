<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 05/08/2020
 * Time: 17:25
 */

namespace Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling;


use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      fordelingsnøkkel \Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel
 *  Mulige variabler:
 *      $elementer
 * @package Kyegil\Leiebasen\Visning\felles\html\leieforhold
 */
class Fordelingsnøkkel extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/kostnadsdeling/Fordelingsnøkkel.html';

    /**
     * @var array
     */
    protected $data = [
        'elementer' => null
    ];

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['fordelingsnøkkel'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel $fordelingsnøkkel */
        $fordelingsnøkkel = $this->hentRessurs('fordelingsnøkkel');

        if($fordelingsnøkkel) {
            $elementer = new ViewArray();
            /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel\Element $element */
            foreach($fordelingsnøkkel->hentElementer() as $element) {
                $elementer->addItem($this->app->vis(fordelingsnøkkel\Element::class, [
                    'fordelingselement' => $element,
                ]));
            }
            $this->definerData([
                'elementer' => $elementer
            ]);
        }

        return parent::forberedData();
    }
}