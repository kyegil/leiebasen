<?php

namespace Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling;


use Kyegil\Leiebasen\Samling;
use Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling\fakturaliste\Fakturalinje;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      tjeneste Kyegil\Leiebasen\Samling|Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste
 *      kostnader Kyegil\Leiebasen\Samling|Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad[]
 *  Mulige variabler:
 *      $regningsliste
 * @package Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling\fakturaliste
 */
class Fakturaliste extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/kostnadsdeling/Fakturaliste.html';

    /**
     * @var array
     */
    protected $data = [
        'regningsliste' => null,
    ];

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['tjeneste','kostnader'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste|null $tjeneste */
        $tjeneste = $this->hentRessurs('tjeneste');
        /** @var Samling|\Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad[]|null $kostnader */
        $kostnader = $this->hentRessurs('kostnader');
        if (!isset($kostnader) && $tjeneste) {
            $kostnader = $tjeneste->hentRegninger();
        }

        if($kostnader) {
            $regningsliste = new ViewArray();
            foreach($kostnader as $kostnad) {
                $regningsliste->addItem($this->app->vis(Fakturalinje::class, [
                    'faktura' => $kostnad
                ]));
            }
            $this->definerData([
                'regningsliste' => $regningsliste
            ]);
        }
        return parent::forberedData();
    }
}