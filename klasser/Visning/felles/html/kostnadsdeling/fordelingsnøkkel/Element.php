<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 05/08/2020
 * Time: 17:25
 */

namespace Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling\fordelingsnøkkel;


use Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel\Element as ElementModell;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      fordelingselement \Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel\Element
 *  Mulige variabler:
 *      $andeler
 *      $prosentsats
 *      $følgerLeieobjekt
 *      $erBofellesskap
 *      $leieobjektId
 *      $leieobjektbeskrivelse
 *      $beboerliste
 * @package Kyegil\Leiebasen\Visning\felles\html\leieforhold
 */
class Element extends \Kyegil\Leiebasen\Visning
{
//    protected $template = 'felles/html/kostnadsdeling/fordelingsnøkkel/Andel.html';

    /**
     * @var array
     */
    protected $data = [
        'andeler' => null,
        'prosentsats' => null,
        'følgerLeieobjekt' => null,
        'erBofellesskap' =>  null,
        'leieobjektId' => null,
        'leieobjektbeskrivelse' => null,
        'leieforholdId' => null,
        'leieforholdbeskrivelse' => null,
        'beboerliste' => null
    ];

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['fordelingselement'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var ElementModell $fordelingselement */
        $fordelingselement = $this->hentRessurs('fordelingselement');

        if($fordelingselement) {
            $følgerLeieobjekt = $fordelingselement->hentFølgerLeieobjekt();

            $leieobjekt = $følgerLeieobjekt ? $fordelingselement->hentLeieobjekt() : '';
            $leieobjektId = $leieobjekt ? $leieobjekt->hentId() : '';
            $leieobjektbeskrivelse = $leieobjekt ? $leieobjekt->hentBeskrivelse() : '';
            $erBofellesskap = $følgerLeieobjekt && $leieobjekt ? $leieobjekt->erBofellesskap() : false;

            $leieforhold = !$følgerLeieobjekt ? $fordelingselement->hentLeieforhold() : '';
            $leieforholdId = $leieforhold ? $leieforhold->hentId() : '';
            $leieforholdbeskrivelse = $leieforhold ? $leieforhold->hentBeskrivelse() : '';

            if($følgerLeieobjekt) {
                $beboersett = $this->hentLeietakernavn($leieobjekt);
            }
            else {
                $beboersett = $leieforhold ? [$leieforhold->hentNavn()] : [];
            }

            $this->definerData([
                'andeler' => $fordelingselement->hentAndeler() ,
                'prosentsats' => $this->app->prosent($fordelingselement->hentProsentsats()),
                'følgerLeieobjekt' => $følgerLeieobjekt,
                'erBofellesskap' => $erBofellesskap,
                'leieobjektId' => $leieobjektId,
                'leieobjektbeskrivelse' => $leieobjektbeskrivelse,
                'leieforholdId' => $leieforholdId,
                'leieforholdbeskrivelse' => $leieforholdbeskrivelse,
                'beboerliste' => $this->app->liste($beboersett)
            ]);
            $this->settMal($this->velgMal($fordelingselement));
        }
        return parent::forberedData();
    }


    /**
     * @param ElementModell $element
     * @return string
     */
    private function velgMal(ElementModell $element)
    {
        switch($element->hentFordelingsmåte()) {
            case 'Fastbeløp':
                return 'felles/html/kostnadsdeling/fordelingsnøkkel/Fastbeløp.html';
            case 'Prosentvis':
                return 'felles/html/kostnadsdeling/fordelingsnøkkel/Prosent.html';
            default:
                return 'felles/html/kostnadsdeling/fordelingsnøkkel/Andel.html';
        }
    }

    /**
     * @param Leieobjekt|null $leieobjekt
     * @return array
     * @throws \Exception
     */
    protected function hentLeietakernavn(Leieobjekt $leieobjekt = null)
    {
        $resultat = [];
        if($leieobjekt) {
            $iDag = new \DateTime();
            /** @var Leieforhold $leieforhold */
            foreach($leieobjekt->hentLeieforhold($iDag, $iDag) as $leieforhold) {
                $resultat[] = $leieforhold->hentNavn();
            }
        }
        return $resultat;
    }

}