<?php

namespace Kyegil\Leiebasen\Visning\felles\html\leieforhold\kontraktliste;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Liste;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt as KontraktModell;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\html\leietaker\Navn;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      kontrakt \Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt
 *      visLenke boolean
 *      visNavn boolean
 *      visAdresselenker boolean
 *  Mulige variabler:
 *      $kontraktNr
 *      $kontraktPersoner
 *      $fødselsnummer
 *      $fødselsdato
 * @package Kyegil\Leiebasen\Visning\felles\html\leieforhold\kontraktliste
 */
class Kontrakt extends Visning
{
    protected $template = 'felles/html/leieforhold/kontraktliste/Kontrakt.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['kontrakt', 'visLenke', 'visNavn', 'visAdresselenker'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        parent::forberedData();
        /** @var KontraktModell|null $kontrakt */
        $kontrakt = $this->hentRessurs('kontrakt');
        /** @var boolean|null $visLenke */
        $visLenke = $this->hentRessurs('visLenke');
        /** @var boolean|null $visNavn */
        $visNavn = $this->hentRessurs('visNavn');
        /** @var boolean|null $visLenke */
        $visAdresselenker = $this->hentRessurs('visAdresselenker');
        $leietakere  = new Liste();
        if ($kontrakt) {
            $kontraktNr = $kontrakt->hentId();
            if ($visLenke) {
                $kontraktNr = new HtmlElement('a', [
                    'title' => 'Gå til leieavtalen',
                    'href' => Leiebase::url('kontrakt_tekst_kort', $kontraktNr,
                        null, [], $this->app->område['område']),
                ], $kontraktNr);
            }
            if ($visNavn) {
                /** @var Leietaker $leietaker */
                foreach ($kontrakt->hentLeietakere() as $leietaker) {
                    if (!$leietaker->slettet) {
                        $leietakere->addItem($this->app->vis(Navn::class, [
                            'leietaker' => $leietaker,
                            'visLenke' => (bool)$visAdresselenker
                        ]));
                    }
                }
            }
            $this->definerData([
                'kontraktNr' => $kontraktNr,
                'kontraktPersoner' => $leietakere,
                'fraDato' => $kontrakt->hentDato()->format('Y-m-d'),
                'tilDato' => $kontrakt->tildato ? $kontrakt->tildato->format('Y-m-d') : ''
            ]);
        }
        return $this;
    }
}