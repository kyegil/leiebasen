<?php

namespace Kyegil\Leiebasen\Visning\felles\html\leieforhold;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning;

/**
 * Visning for adressefelt for leieforhold
 *
 *  Ressurser:
 * * leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *
 *  Tilgjengelige variabler:
 * * $adresse1
 * * $adresse2
 * * $postnr
 * * $poststed
 * * $land
 *
 * @package Kyegil\Leiebasen\Visning\felles\html\leieforhold
 */
class Adressefelt extends Visning
{
    protected $template = 'felles/html/leieforhold/Adressefelt.html';

    /**
     * @param string $nøkkel
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function setData($nøkkel, $verdi = null)
    {
        if(in_array($nøkkel, ['leieforhold'])) {
            return $this->settRessurs($nøkkel, $verdi);
        }
        return parent::setData($nøkkel, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        parent::forberedData();
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        if ($leieforhold) {
            $adresse = $leieforhold->hentAdresse();
            $data = array_merge((array)$adresse, $this->hent());
            $this->sett($data);
        }
        return $this;
    }
}