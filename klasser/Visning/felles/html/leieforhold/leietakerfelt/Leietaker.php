<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 05/08/2020
 * Time: 18:17
 */

namespace Kyegil\Leiebasen\Visning\felles\html\leieforhold\leietakerfelt;


use Kyegil\Leiebasen\Visning;
use Kyegil\ViewRenderer\AppInterface;
use Kyegil\ViewRenderer\ViewFactory;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Mulige variabler:
 *      $navn
 *      $url
 *      $fødselsnummer
 *      $fødselsdato
 * @package Kyegil\Leiebasen\Visning\felles\html\leieforhold\leietakerfelt
 */
class Leietaker extends Visning
{
    protected $template = 'felles/html/leieforhold/leietakerfelt/Leietaker.html';

    public function __construct(ViewFactory $viewFactory, AppInterface $app, array $data = [])
    {
        $data['url'] = isset($data['url']) ? $data['url'] : '';
        $data['fødselsnummer'] = isset($data['fødselsnummer']) ? $data['fødselsnummer'] : '';
        $data['fødselsdato'] = isset($data['fødselsdato']) ? $data['fødselsdato'] : '';
        parent::__construct($viewFactory, $app, $data);
    }
}