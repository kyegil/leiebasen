<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 05/08/2020
 * Time: 17:25
 */

namespace Kyegil\Leiebasen\Visning\felles\html\leieforhold;


/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *      visLenke boolean
 *  Mulige variabler:
 *      $navn
 *      $id
 *      $visLenke
 * @package Kyegil\Leiebasen\Visning\felles\html\leieforhold
 */
class Navn extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/leieforhold/Navn.html';

    /**
     * @param array|string $nøkkel
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function setData($nøkkel, $verdi = null)
    {
        if(in_array($nøkkel, ['leieforhold'])) {
            return $this->settRessurs($nøkkel, $verdi);
        }
        return parent::setData($nøkkel, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        $this->definerData([
            'navn' => $leieforhold->hentNavn(),
            'id' => $leieforhold->hentId(),
            'visLenke' => $this->app->adgang('mine-sider', $leieforhold)
        ]);
        return parent::forberedData();
    }
}