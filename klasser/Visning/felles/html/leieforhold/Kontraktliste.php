<?php

namespace Kyegil\Leiebasen\Visning\felles\html\leieforhold;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\kontraktliste\Kontrakt as KontraktlisteElement;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\leietakerfelt\Leietaker;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *      visLenker boolean
 *  Mulige variabler:
 *      kontrakter
 * @package Kyegil\Leiebasen\Visning\felles\html\leieforhold
 */
class Kontraktliste extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/leieforhold/Kontraktliste.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['leieforhold', 'visLenker'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        parent::forberedData();
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        /** @var boolean|null $visLenke */
        $visLenker = $this->hentRessurs('visLenker');
        if ($leieforhold) {
            $kontrakter = [];
            /** @var Kontrakt $kontrakt */
            foreach ($leieforhold->hentKontrakter() as $kontrakt) {
                $kontrakter[] = $this->app->vis(KontraktlisteElement::class, [
                    'kontrakt' => $kontrakt,
                    'visLenke' => true,
                    'visNavn' => true,
                    'visAdresselenker' => true
                ]);
            }
            $this->definerData([
                'kontrakter' => $kontrakter
            ]);

            $leietakere = $leieforhold->hentLeietakere();
            $leietakereVisning = [];
            /** @var Leieforhold\Leietaker $leietaker */
            foreach($leietakere as $leietaker) {
                $leietakereVisning[] = $this->app->vis(Leietaker::class, [
                    'navn' => $leietaker->hentNavn(),
                    'url' => null,
                    'fødselsnummer' => null,
                    'fødselsdato' => null
                ]);
            }
            $this->definerData([
                'leietakere' => $leietakereVisning
            ]);
        }
        return $this;
    }
}