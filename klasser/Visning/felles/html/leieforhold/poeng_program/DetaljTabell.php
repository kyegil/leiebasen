<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/08/2020
 * Time: 18:09
 */

namespace Kyegil\Leiebasen\Visning\felles\html\leieforhold\poeng_program;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Poengprogram;
use Kyegil\Leiebasen\Poengbestyrer;
use Kyegil\Leiebasen\Visning;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      poengbestyrer \Kyegil\Leiebasen\Poengbestyrer
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *      program \Kyegil\Leiebasen\Modell\Poengprogram
 *  Mulige variabler:
 *      $tableId
 *      $caption
 *      $tableHead
 *      $tableBody
 *      $tableFoot
 *      $class
 *      $style
 * @package Kyegil\Leiebasen\Visning\felles\html\leieforhold\poeng_program
 */
class DetaljTabell extends Visning\felles\html\table\HtmlTable
{
    /**
     * @param string $nøkkel
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function setData($nøkkel, $verdi = null): DetaljTabell
    {
        if(in_array($nøkkel, ['leieforhold', 'program'])) {
            return $this->settRessurs($nøkkel, $verdi);
        }
        return parent::setData($nøkkel, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): DetaljTabell
    {
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        /** @var Poengprogram $program */
        $program = $this->hentRessurs('program');
        /** @var Poengbestyrer $poengbestyrer */
        $poengbestyrer = $this->app->hentPoengbestyrer();
        $kolonner = [
            (object)[
                'content' => '',
                'dataName' => 'id',
            ],
            (object)[
                'content' => '',
                'dataName' => 'dato',
            ],
            (object)[
                'content' => '',
                'dataName' => 'verdi',
                'classes' => ['value']
            ],
            (object)[
                'content' => '',
                'dataName' => 'tekst',
            ],
        ];
        $data = [];
        foreach ($poengbestyrer->hentPoeng($program, $leieforhold) as $enkeltPoeng) {
            $data[] = (object)[
                'id' => $enkeltPoeng->hentId(),
                'dato' => $enkeltPoeng->tid ? $enkeltPoeng->tid->format('d.m.Y') : '',
                'verdi' => $enkeltPoeng->verdi,
                'tekst' => $enkeltPoeng->tekst,
            ];
        }

        $tableFoot = $this->app->vis(Visning\felles\html\table\TableFoot::class);

        $datasett = [];
        if ($program && $leieforhold) {
            $datasett['idFelt'] = 'id';
            $datasett['tableId'] = 'poengprogram-detaljer-' . $program->kode . '-' . $leieforhold->hentId();
            $datasett['classes'] = ['poengprogram-detaljer', $program->kode, ' leieforhold-' . $leieforhold->hentId()];
            $datasett['class'] = 'poengprogram-detaljer ' . $program->kode . ' leieforhold-' . $leieforhold->hentId();
            $datasett['caption'] = $program->beskrivelse;
            $datasett['kolonner'] = $kolonner;
            $datasett['data'] = $data;
            $datasett['tableFoot'] = $tableFoot;
        }

        $this->definerData($datasett);
        parent::forberedData();
        return $this;
    }
}