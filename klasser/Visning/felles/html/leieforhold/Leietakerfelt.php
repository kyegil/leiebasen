<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 05/08/2020
 * Time: 17:25
 */

namespace Kyegil\Leiebasen\Visning\felles\html\leieforhold;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\leietakerfelt\Leietaker;

/**
 * Visning for grunnleggende html-struktur i mine sider
 *
 *  Ressurser:
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *      visFødselsnr boolean
 *  Mulige variabler:
 *      leietakere
 * @package Kyegil\Leiebasen\Visning\felles\html\leieforhold
 */
class Leietakerfelt extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/html/leieforhold/Leietakerfelt.html';

    /**
     * @param string $nøkkel
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($nøkkel, $verdi = null)
    {
        if(in_array($nøkkel, ['leieforhold', 'visFødselsnr'])) {
            return $this->settRessurs($nøkkel, $verdi);
        }
        return parent::sett($nøkkel, $verdi);
    }

    /**
     * @return $this
     */
    protected function forberedData()
    {
        parent::forberedData();
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        $visFødselsnr = $this->hentRessurs('visFødselsnr');
        $leietakere = $leieforhold->hentLeietakere();
        $leietakereVisning = [];
        /** @var Leieforhold\Leietaker $leietaker */
        foreach($leietakere as $leietaker) {
            $person = $leietaker->person;
            $leietakereVisning[] = $this->app->vis(Leietaker::class, [
                'navn' => $leietaker->hentNavn(),
                'url' => null,
                'fødselsnummer' => ($visFødselsnr && $person ? $person->hentFødselsnummer() : ''),
                'fødselsdato' => ($visFødselsnr && $person && $person->hentFødselsdato() ? $person->hentFødselsdato()->format('d.m.Y') : '')
            ]);
        }
        $this->sett([
            'leietakere' => $leietakereVisning
        ]);
        return $this;
    }
}