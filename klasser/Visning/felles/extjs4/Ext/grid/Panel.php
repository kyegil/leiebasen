<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\grid;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Visning\felles\extjs4\Common;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\button\Button;
use Kyegil\ViewRenderer\View;

/**
 * View class for ExtJs4 Grid Panel
 *
 *  Resources:
 *      config              object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly    boolean unless set to false the config is returned as a JS object only
 *      returnUrl           string
 *      cancelButton        string|Kyegil\ViewRenderer\ViewInterface
 *
 *  Template variables:
 *      $id
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.Panel
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\grid
 */
class Panel extends Common
{

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     * @throws Exception
     */
    public function setData($key, $value = null)
    {
        if(in_array($key, ['cancelButton', 'returnUrl'])) {
            return $this->setResource($key, $value);
        }
        if(in_array($key, ['id'])) {
            View::setData($key, $value);
            return $this;
        }
        return parent::setData($key, $value);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        $id = $this->getData('id') ?: 'gridpanel_' . md5(rand());
        $returnUrl = (string)$this->getResource('returnUrl');

        $cancelButton = $this->getResource('cancelButton');
        if (!isset($cancelButton) && $returnUrl) {
            $cancelButton = $this->viewFactory->createView(Button::class, [
                'configObjectOnly' => true,
                'id' => 'cancel-button',
                'itemId' => 'cancel-button',
                'text' => 'Tilbake',
                'handler' => new JsFunction('window.location = ' . json_encode($returnUrl) . ';')
            ]);
        }
        $buttons = [];
        if ($cancelButton) {
            $buttons[] = $cancelButton;
        }

        $this->setDataIfNotSet([
            'className' => 'Ext.grid.Panel',
            'autoDestroy' => false
        ]);
        $this->setConfigIfNotSet([
            'xtype' => 'gridpanel',
            'id' => $id,
            'renderTo' => 'panel',
            'autoScroll' => true,
            'frame' => true,
            'bodyPadding' => 5,
            'width' => 900,
            'height' => '100%',
            'buttons' => $buttons
        ]);

        return parent::prepareData();
    }
}