<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\grid\column;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Visning\felles\extjs4\Common;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\button\Button;
use Kyegil\ViewRenderer\View;

/**
 * View class for ExtJs4 Grid Action Column
 *
 *  Resources:
 *      config  object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly boolean if not set to false only the config is returned as a JS object
 *
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Action
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\grid\column
 */
class Action extends Common
{
    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        $this->setConfigIfNotSet([
            'xtype' => 'actioncolumn',
            'sortable' => false,
        ]);
        return parent::prepareData();
    }
}