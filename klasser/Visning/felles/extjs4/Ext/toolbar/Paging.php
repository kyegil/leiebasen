<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\toolbar;


use Exception;
use Kyegil\Leiebasen\Visning\felles\extjs4\Common;
use Kyegil\ViewRenderer\View;

/**
 * View class for ExtJs4 Paging Toolbar
 *
 *  Resources:
 *      config              object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly    boolean if true then only the the config is returned as a JS object
 *      returnUrl           string
 *
 *  Template variables:
 *      $id
 *      $variableName
 *      $className
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.tab.Panel
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\tab
 */
class Paging extends Common
{

    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        $id = $this->getData('id') ?: 'pagingtoolbar_' . md5(rand());

        $this->setDataIfNotSet([
            'className' => 'Ext.toolbar.Paging'
        ]);
        $this->setConfigIfNotSet([
            'xtype' => 'pagingtoolbar',
            'id' => $id
        ]);

        return parent::prepareData();
    }
}