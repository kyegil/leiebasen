<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\button;


use Exception;
use Kyegil\Leiebasen\Visning\felles\extjs4\Common;

/**
 * View class for ExtJs4 Button
 *
 *  Resources:
 *      config  object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly boolean if not set to false only the the config is returned as a JS object
 *
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.button.Button
 * @package Kyegil\Leiebasen\Visning\felles\extjs4
 */
class Button extends Common
{
    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        $this->setDataIfNotSet([
            'className' => 'Ext.button.Button'
        ]);
        $this->setConfigIfNotSet([
            'xtype' => 'button'
        ]);

        return parent::prepareData();
    }

}