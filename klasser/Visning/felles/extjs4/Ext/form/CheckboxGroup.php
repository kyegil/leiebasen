<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Visning\felles\extjs4\Common;
use \stdClass;

/**
 * ExtJs4 Checkbox Group
 *
 *  Resources:
 *      config              object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly boolean if not set to false only the config is returned as a JS object
 *      dataValues          object|array The local Data values,
 *                          given either as array or as a value=>text object
 *      value               array The initial value(s),
 *      submitAsArray       boolean (false) If true, then the selected values will be submitted as a simple array,
 *                          rather than as an object/associative array
 *                          (default is object with 0/1 status for each box)
 *
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.CheckboxGroup
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form
 */
class CheckboxGroup extends Common
{
    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     * @throws Exception
     */
    public function setData($key, $value = null)
    {
        // All values that should not go straight to the config object
        // must be extracted and saved as resources
        //
        if(in_array($key, ['dataValues', 'value', 'submitAsArray'])) {
            return $this->setResource($key, $value);
        }
        return parent::setData($key, $value);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        settype($this->resources['value'], 'array');

        /** @var string $id */
        $id = $this->getConfig('id');
        if (!isset($id)) {
            $id = 'checkboxgroup_' . md5(rand());
            $this->setData('id', $id);
        }

        /** @var string $name */
        $name = $this->getConfig('name') ?? md5(rand());

        /** @var string $itemId */
        $itemId = $this->getConfig('itemId');
        if (!isset($itemId)) {
            $itemId = $id;
            $this->setConfig('itemId', $itemId);
        }

        /** @var object|array $existingOptions */
        $existingOptions = $this->getResource('dataValues') ?? $this->getResource('value');
        $submitAsArray = (bool)$this->getResource('submitAsArray');

        /** @var array $value */
        $value = $this->getResource('value');
        if (is_object($value)) {
            $value = array_keys((array)$value);
        }

        /** @var stdClass[] $checkboxes The initial checkboxes */
        $checkboxes = [];

        /**
         * @var string|int $valueField
         * @var string $displayField
         */
        foreach( $existingOptions as $valueField => $displayField) {
            $boxValue = is_object($existingOptions) ? $valueField : $displayField;
            if ($submitAsArray) {
                $checkboxes[] = (object)[
                    'inputId' => "{$itemId}-{$boxValue}",
                    'name' => "{$name}[]",
                    'inputValue' => $boxValue,
                    'uncheckedValue' => new JsCustom('undefined'),
                    'boxLabel' => $displayField,
                    'checked' => in_array($boxValue, $value)
                ];
            }
            else {
                $checkboxes[] = (object)[
                    'inputId' => "{$itemId}-{$boxValue}",
                    'name' => "{$name}[{$boxValue}]",
                    'inputValue' => 1,
                    'boxLabel' => $displayField,
                    'checked' => in_array($boxValue, $value)
                ];
            }
        }

        $this->setDataIfNotSet([
            'className' => 'Ext.form.CheckboxGroup'
        ]);
        $this->setConfigIfNotSet([
            'xtype' => 'checkboxgroup',
            'width' => '100%',
            'defaults' => (object)[
                'inputValue' => 1,
                'uncheckedValue' => 0
            ],
            'items' => $checkboxes
        ]);
        return parent::prepareData();
    }
}