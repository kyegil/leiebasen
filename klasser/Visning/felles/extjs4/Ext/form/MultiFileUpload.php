<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Visning\felles\extjs4\Common;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\button\Button;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Hidden;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Text;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\MultiFileUploadEvents\AddHandler;
use Kyegil\ViewRenderer\JsArray;
use Kyegil\ViewRenderer\View;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Custom Multi File Upload Fields Group for ExtJs4
 *
 *  Resources:
 *      config              object  The config object with which the ComboBox element will be initialised
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly    boolean if not set to false only the config is returned as a JS object
 *      itemId              string The itemId will be assigned to the checkbox group
 *      name                string The HTML name of the file fields
 *      value               array The initial value(s),
 *
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.FieldSet
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form
 */
class MultiFileUpload extends Common
{
    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     * @throws Exception
     */
    public function setData($key, $value = null): MultiFileUpload
    {
        if(in_array($key, ['itemId', 'name', 'value'])) {
            return $this->setResource($key, $value);
        }
        if($key == 'id') {
            View::setData($key, $value);
            return $this;
        }
        return parent::setData($key, $value);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData(): MultiFileUpload
    {
        $items = new JsArray();

        settype($this->resources['value'], 'array');

        /** @var string $id */
        $id = $this->getData('id');
        if (!isset($id)) {
            $id = 'multifileuploadcontainer_' . md5(rand());
            $this->setData('id', $id);
        }

        /** @var string $name */
        $name = $this->getResource('name') ?? md5(rand());

        /** @var string $itemId */
        $itemId = $this->getResource('itemId');
        if (!isset($itemId)) {
            $itemId = $name ?? $id;
            $this->setResource('itemId', $itemId);
        }

        if ($this->hasConfig('fieldLabel')) {
            $this->setConfigIfNotSet(['title' => $this->getConfig('fieldLabel')]);
        }

        /** @var array $value */
        $existingFiles = $this->getResource('value');

        $items->addItem(
            $this->viewFactory->createView(Hidden::class, [
                'itemId' => 'new_' . $name . '_count',
                'name' => 'new_' . $name . '_count',
                'value' => 0
            ])
        );

        foreach ($existingFiles as $file) {
            $items->addItem(
                $this->viewFactory->createView(FieldContainer::class, [
                    'layout' => 'hbox',
                    'items' => [
                        $this->viewFactory->createView(Text::class, [
                            'readOnly' => true,
                            'name' => $name . '[]',
                            'width' => '150px',
                            'value' => basename($file)
                        ]),
                        $this->viewFactory->createView(Button::class, [
                            'text' => 'Fjern',
                            'margin' => '0 0 0 70',
                            'handler' => new JsFunction(
                                "var newUploadsCountField = button.ownerCt.ownerCt.getComponent('new_{$name}_count');
                                    newUploadsCountField.setValue(newUploadsCountField.getValue() - 1);
                                    button.ownerCt.ownerCt.remove(button.ownerCt);"
                                , ['button']
                            )
                        ]),
                    ]
                ])
            );
        }

        $items->addItem(
            $this->viewFactory->createView(Button::class, [
                'text' => 'Legg til',
                'handler' => new JsFunction($this->viewFactory->createView(AddHandler::class, [
                    'fileUploadContainerId' => $itemId,
                    'name' => $name
                ]), ['addButton', 'e'])
            ])
        );

        $this->setDataIfNotSet([
            'className' => 'Ext.form.FieldSet'
        ]);

        $this->setConfigIfNotSet([
            'id' => $id,
            'autoScroll' => true,
            'layout' => 'vbox',
            'xtype' => 'fieldset',
            'padding' => '0 5 5 5',
            'collapsible' => false,
            'items' => $items
        ]);
        if (!$this->hasConfig('itemId')) {
            $this->setConfig('itemId', $this->getConfig('name') ?? $this->getConfig('id'));
        }
        return parent::prepareData();
    }
}