<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field;


use Exception;

/**
 * ExtJs4 Text field
 *
 *  Resources:
 *      config  object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly boolean if not set to false only the config is returned as a JS object
 *
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Text
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field
 */
class Text extends Field
{
    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        $id = $this->getData('id') ?: 'textfield_' . md5(rand());
        $this->setDataIfNotSet([
            'className' => 'Ext.form.field.Text'
        ]);
        $this->setConfigIfNotSet([
            'xtype' => 'textfield',
            'id' => $id
        ]);
        return parent::prepareData();
    }
}