<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field;


use Exception;

/**
 * ExtJs4 Date field
 *
 *  Resources:
 *      config  object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly boolean if not set to false only the config is returned as a JS object
 *
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Date
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field
 */
class Date extends Field
{
    public function setData($key, $value = null)
    {
        if (in_array($key, ['value', 'inValue', 'maxValue']) && $value instanceof \DateTimeInterface) {
            $value = $value->format('Y-m-d');
        }
        return parent::setData($key, $value);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        $id = $this->getData('id') ?: 'datefield_' . md5(rand());
        $this->setDataIfNotSet([
            'className' => 'Ext.form.field.Date'
        ]);
        $this->setConfigIfNotSet([
            'xtype' => 'datefield',
            'submitFormat' => 'Y-m-d',
            'id' => $id
        ]);
        return parent::prepareData();
    }
}