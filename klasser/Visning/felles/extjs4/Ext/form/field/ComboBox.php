<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\JsonStore;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\Store;
use Kyegil\ViewRenderer\View;

/**
 * ExtJs4 ComboBox field
 *
 *  Resources:
 *      config              object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly    boolean if not set to false only the config is returned as a JS object
 *      remote              boolean The data should be loaded over AJAX
 *      url                 string The remote data url
 *      dataValues          object|array The local Data values,
 *                          given either as array or as a value=>text object
 *      listWidth           string|int The drop-down list width
 *
 *      autoLoad            An autoLoad config object which will be passed on to the default store
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.ComboBox
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field
 */
class ComboBox extends Field
{

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     * @throws Exception
     */
    public function setData($key, $value = null)
    {
        if(in_array($key, ['autoLoad', 'remote', 'url', 'dataValues', 'listWidth'])) {
            return $this->setResource($key, $value);
        }
        if(in_array($key, ['id'])) {
            View::setData($key, $value);
            return $this;
        }
        return parent::setData($key, $value);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        $id = $this->getData('id') ?: 'combobox_' . md5(rand());
        $remote = boolval($this->getResource('remote') ?? false);
        $autoLoad = $this->getResource('autoLoad');
        $listWidth = $this->getResource('listWidth');

        $this->setDataIfNotSet([
            'className' => 'Ext.form.field.ComboBox'
        ]);
        $this->setConfigIfNotSet([
            'xtype' => 'combobox',
            'id' => $id,
            'queryMode' => $remote ? 'remote' : 'local',
            'anyMatch' => true,
            'valueField' => 'value'
        ]);
        if ($listWidth) {
            $this->setConfigIfNotSet([
                'matchFieldWidth' => false,
                'listConfig' => (object)['width' => $listWidth]
            ]);
        }

        if (!$this->hasConfig('store')) {
            $this->setDefaultStore($remote, $id, $autoLoad);
        }

        return parent::prepareData();
    }

    /**
     * @param bool $remote
     * @param string $id
     * @return ComboBox
     * @throws Exception
     */
    protected function setDefaultStore(bool $remote, string $id, $autoLoad = null): ComboBox
    {
        $displayField = 'text';

        $defaultValue = $this->getConfig('value');

        if ($remote) {
            if (!isset($autoLoad)) {
                $autoLoad = (object)['params' => (object)[]];
                if ($defaultValue) {
                    $autoLoad->params->query = $defaultValue;
                    $autoLoad->params->autoload = 1;
                    $autoLoad->callback = new JsFunction("Ext.getCmp('{$id}').setValue({$defaultValue});", ['records', 'operations', 'success']);
                }
            }

            $store = $this->viewFactory->createView(JsonStore::class, [
                'configObjectOnly' => false,
                'autoLoad' => $autoLoad,
                'storeId' => $id . '_store',
                'proxy' => (object)[
                    'type' => 'ajax',
                    'url' => $this->getResource('url'),
                    'limitParam' => new JsCustom('undefined'),
                    'pageParam' => new JsCustom('undefined'),
                    'startParam' => new JsCustom('undefined'),
                    'noCache' => false,
                    'reader' => (object)[
                        'type' => 'json',
                        'root' => 'data',
                        'idProperty' => 'value'
                    ]
                ],
                'fields' => ['value', 'text', 'extra']
            ]);
        }
        else {
            /** @var object|array $dataValues */
            $dataValues = $this->getResource('dataValues') ?? [];
            if (!is_array($dataValues) && !is_object($dataValues)) {
                throw new Exception('Local Data Values must be given either as an array or as an object');
            }
            $data = [];
            if (is_object($dataValues)) {
                $fields = ['value', 'text'];
            }
            else {
                $fields = ['value'];
                $displayField = 'value';
            }
            foreach( $dataValues as $index => $value) {
                if(is_object($dataValues)) {
                    $data[] = (object)['value' => $index, 'text' => $value];
                }
                else {
                    $data[] = (object)['value' => $value];
                }
            }

            $store = $this->viewFactory->createView(Store::class, [
                'configObjectOnly' => false,
                'storeId' => $id . '_store',
                'fields' => $fields,
                'data' => $data
            ]);
        }
        $this->setConfig('store', $store);
        $this->setConfig('displayField', $displayField);
        return $this;
    }
}