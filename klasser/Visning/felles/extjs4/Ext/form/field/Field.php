<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field;


use Exception;
use Kyegil\Leiebasen\Visning\felles\extjs4\Common;

/**
 * Common class for all ExtJs4 Form fields
 *
 *  Resources:
 *      config  object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly boolean if not set to false only the config is returned as a JS object
 *
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Field
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field
 */
class Field extends Common
{
    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        $this->setConfigIfNotSet([
            'width' => '100%'
        ]);
        if (!$this->hasConfig('itemId')) {
            $this->setConfig('itemId', $this->getConfig('name') ?? $this->getConfig('id'));
        }
        return parent::prepareData();
    }
}