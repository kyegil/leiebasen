<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field;


use Exception;

/**
 * ExtJs4 Checkbox field
 *
 *  Resources:
 *      config  object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly boolean if not set to false only the config is returned as a JS object
 *
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Checkbox
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field
 */
class Checkbox extends Field
{
    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        $this->setDataIfNotSet([
            'className' => 'Ext.form.field.Checkbox'
        ]);
        $value = $this->getConfig('value') || $this->getConfig('checked');
        $this->setConfigIfNotSet([
            'xtype' => 'checkboxfield',
            'inputValue' => 1,
            'uncheckedValue' => 0,
            'value' => $value,
            'checked' => $value
        ]);
        return parent::prepareData();
    }
}