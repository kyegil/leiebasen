<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 01/09/2021
 * Time: 11:46
 */

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field;

use Exception;

/**
 * ExtJs4 Text Field with Password input format
 *
 * Resources:
 *      config  object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly boolean if not set to false only the config is returned as a JS object
 *
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Text
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field
 */
class Password extends Text
{
    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData(): Password
    {
        $this->setConfigIfNotSet([
            'inputType' => 'password'
        ]);
        return parent::prepareData();
    }
}