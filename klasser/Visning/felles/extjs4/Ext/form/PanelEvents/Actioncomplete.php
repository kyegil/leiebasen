<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\PanelEvents;


use Kyegil\ViewRenderer\View;

/**
 * View class for default ExtJs4 Form Panel actioncomplete listener
 *
 *  Resources:
 *  Template variables:
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.Panel-event-actioncomplete
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\PanelEvents
 */
class Actioncomplete extends View
{
    protected $template = 'felles/extjs4/Ext/form/PanelEvents/Actioncomplete.js';
}