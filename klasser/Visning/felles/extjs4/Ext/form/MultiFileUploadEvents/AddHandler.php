<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\MultiFileUploadEvents;


use Kyegil\ViewRenderer\View;

/**
 * View class for Add Button Handler used by the Multi File Uplad
 *
 *  Resources:
 *  Template variables:
 *      $fileUploadContainerId
 *      $name
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.button.Button-cfg-handler
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\MultiFileUploadEvents
 */
class AddHandler extends View
{
    /** @var string */
    protected $template = 'felles/extjs4/Ext/form/MultiFileUploadEvents/AddHandler.js';

    /**
     * @return $this
     */
    protected function prepareData(): AddHandler
    {
        $this->setDataIfNotSet([
            'fileUploadContainerId' => '',
            'name' => ''
        ]);
        return parent::prepareData();
    }
}