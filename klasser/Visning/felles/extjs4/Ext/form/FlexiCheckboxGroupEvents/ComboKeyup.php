<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FlexiCheckboxGroupEvents;


use Kyegil\ViewRenderer\View;

/**
 * View class for ComboBox keyup listener used by the the FlexiCheckboxGroup
 *
 *  Resources:
 *  Template variables:
 *      $checkBoxGroupItemId
 *      $name
 *      submitAsArray       boolean (false) If true, then the selected values will be submitted as a simple array,
 *                          rather than as an object/associative array
 *                          (default is object with 0/1 status for each box)
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.ComboBox-event-keyup
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FlexiCheckboxGroupEvents
 */
class ComboKeyup extends View
{
    /** @var string */
    protected $template = 'felles/extjs4/Ext/form/FlexiCheckboxGroupEvents/ComboKeyup.js';

    /**
     * @return $this
     */
    protected function prepareData()
    {
        $this->setDataIfNotSet([
            'checkBoxGroupItemId' => '',
            'name' => ''
        ]);
        return parent::prepareData();
    }
}