<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Visning\felles\extjs4\Common;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\button\Button;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\PanelEvents\Actioncomplete;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\PanelEvents\Actionfailed;
use Kyegil\ViewRenderer\View;
use stdClass;

/**
 * View class for ExtJs4 Form Panel
 *
 *  Resources:
 *      leiebasenJsObjektEgenskaper
 *      config              object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly    boolean if not set to false only the config is returned as a JS object
 *      formActionUrl       string
 *      returnUrl           string
 *      enabledSaveButton   boolean
 *      cancelButton        string|Kyegil\ViewRenderer\ViewInterface
 *      saveButton          string|Kyegil\ViewRenderer\ViewInterface
 *
 *  Template variables:
 *      $id
 *      $variableName
 *      $className
 *      $configString
 *      $formSourceUrl
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.Panel
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form
 */
class Panel extends Common
{

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     * @throws Exception
     */
    public function setData($key, $value = null)
    {
        if(in_array($key, ['leiebasenJsObjektEgenskaper', 'cancelButton', 'saveButton', 'enabledSaveButton', 'formActionUrl', 'returnUrl'])) {
            return $this->setResource($key, $value);
        }
        if(in_array($key, ['id', 'formSourceUrl'])) {
            View::setData($key, $value);
            return $this;
        }
        return parent::setData($key, $value);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        $id = $this->getData('id') ?: 'formpanel_' . md5(rand());
        $formActionUrl = (string)$this->getResource('formActionUrl');
        $returnUrl = (string)$this->getResource('returnUrl');

        $listeners = (object)[
            'actioncomplete' => new JsFunction($this->viewFactory->createView(Actioncomplete::class, []), ['form', 'action', 'eOpts']),
            'actionfailed' => new JsFunction($this->viewFactory->createView(Actionfailed::class, []), ['form', 'action', 'eOpts'])
        ];

        $config = $this->getResource('config') ?? new stdClass();
        $buttons = $config->buttons ?? [];

        $cancelButton = $this->getResource('cancelButton');
        if (!isset($cancelButton)) {
            $cancelButton = $this->viewFactory->createView(Button::class, [
                'configObjectOnly' => true,
                'id' => 'cancel-button',
                'itemId' => 'cancel-button',
                'text' => 'Avbryt',
                'handler' => new JsFunction('window.location = ' . json_encode($returnUrl) . ';')
            ]);
            $this->setResource('cancelButton', $cancelButton);
        }
        if ($cancelButton) {
            $buttons[] = $cancelButton;
        }

        $saveButton = $this->getResource('saveButton');
        if (!isset($saveButton)) {
            $saveButton = $this->viewFactory->createView(Button::class, [
                'configObjectOnly' => true,
                'id' => 'save-button',
                'itemId' => 'save-button',
                'text' => 'Lagre',
                'disabled' => !$this->getResource('enabledSaveButton'),
                'handler' => new JsFunction(
                    'button.up().up().getForm().submit({waitMsg: "Lagrer...", url: ' . json_encode($formActionUrl) . '});',
                    ['button', 'eventObject']
                )
            ]);
            $this->setResource('saveButton', $saveButton);
        }
        if ($saveButton) {
            $buttons[] = $saveButton;
        }

        $config->buttons = $buttons;

        $this->setDataIfNotSet([
            'className' => 'Ext.form.Panel',
            'formSourceUrl' => ''
        ]);

        $defaults = $this->getConfig('defaults');
        if (!$defaults) {
            $defaults = new stdClass();
        }
        if (!property_exists($defaults, 'labelWidth')) {
            $defaults->labelWidth = '25%';
        }
        if (!property_exists($defaults, 'anchor')) {
            $defaults->anchor = '100%';
        }
        $this->setConfig('defaults', $defaults);

        $this->setConfigIfNotSet([
            'xtype' => 'formpanel',
            'id' => $id,
            'renderTo' => 'panel',
            'autoScroll' => true,
            'frame' => true,
            'bodyPadding' => 5,
            'standardSubmit' => false,
            'width' => '100%',
            'height' => '100%',
            'listeners' => $listeners,
            'buttons' => $buttons
        ]);

        return parent::prepareData();
    }
}