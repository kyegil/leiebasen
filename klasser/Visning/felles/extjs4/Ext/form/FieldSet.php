<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form;


use Exception;
use Kyegil\Leiebasen\Visning\felles\extjs4\Common;

/**
 * ExtJs4 Field Set
 *
 *  Resources:
 *      config  object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly boolean if not set to false only the config is returned as a JS object
 *
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.FieldSet
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form
 */
class FieldSet extends Common
{
    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        $this->setDataIfNotSet([
            'className' => 'Ext.form.FieldSet'
        ]);
        $this->setConfigIfNotSet([
            'xtype' => 'fieldset'
        ]);
        return parent::prepareData();
    }
}