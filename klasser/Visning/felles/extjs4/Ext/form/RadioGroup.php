<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form;


use Exception;
use Kyegil\Leiebasen\Visning\felles\extjs4\Common;
use stdClass;

/**
 * ExtJs4 Radio button Group
 *
 *  Resources:
 *      config              object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly boolean if not set to false only the config is returned as a JS object
 *      dataValues          object|array The local Data values,
 *                          given either as array or as a value=>text object
 *      value               string The initial value(s),
 *
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.RadioGroup
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form
 */
class RadioGroup extends Common
{
    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     * @throws Exception
     */
    public function setData($key, $value = null)
    {
        if(in_array($key, ['itemId', 'name', 'dataValues', 'value'])) {
            return $this->setResource($key, $value);
        }
        return parent::setData($key, $value);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        /** @var string $id */
        $id = $this->getData('id');
        if (!isset($id)) {
            $id = 'checkboxgroup_' . md5(rand());
            $this->setData('id', $id);
        }

        /** @var string $name */
        $name = $this->getResource('name') ?? md5(rand());

        /** @var string $itemId */
        $itemId = $this->getResource('itemId');
        if (!isset($itemId)) {
            $itemId = $name ?? $id;
            $this->setResource('itemId', $itemId);
        }

        /** @var object|array $existingOptions */
        $existingOptions = $this->getResource('dataValues') ?? $this->getResource('value');

        /** @var string $value */
        $value = strval($this->getResource('value'));

        /** @var stdClass[] $radioButtons The initial radio buttons */
        $radioButtons = [];

        /**
         * @var string|int $valueField
         * @var string $displayField
         */
        foreach( $existingOptions as $valueField => $displayField) {
            if(is_object($existingOptions)) {
                $radioButtons[] = (object)[
                    'inputId' => "{$itemId}-{$valueField}",
                    'name' => $name,
                    'inputValue' => $valueField,
                    'boxLabel' => $displayField,
                    'checked' => $valueField == $value
                ];
            }
            else {
                $radioButtons[] = (object)[
                    'inputId' => "{$itemId}-{$displayField}",
                    'name' => $name,
                    'inputValue' => $displayField,
                    'boxLabel' => $displayField,
                    'checked' => $displayField == $value
                ];
            }
        }

        $this->setDataIfNotSet([
            'className' => 'Ext.form.RadioGroup'
        ]);
        $this->setConfigIfNotSet([
            'xtype' => 'radiogroup',
            'width' => '100%',

            'msgTarget' => 'under',
            'blankText' => 'Dette feltet er påkrevd',

            'defaults' => (object)[
                'inputValue' => 1,
                'uncheckedValue' => 0
            ],
            'items' => $radioButtons
        ]);
        if (!$this->hasConfig('itemId')) {
            $this->setConfig('itemId', $this->getConfig('name') ?? $this->getConfig('id'));
        }
        return parent::prepareData();
    }
}