<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Visning\felles\extjs4\Common;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\ComboBox;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FlexiCheckboxGroupEvents\ComboKeyup;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FlexiCheckboxGroupEvents\ComboSelect;
use Kyegil\ViewRenderer\View;
use \stdClass;

/**
 * Custom Flexi Checkbox Group for ExtJs4
 *
 *  Resources:
 *      config              object  The config object with which the ComboBox element will be initialised
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly    boolean if not set to false only the config is returned as a JS object
 *      itemId              string The itemId will be assigned to the checkbox group
 *      name                string The HTML name of the checkbox group
 *      remote              boolean The data should be loaded over AJAX
 *      url                 string The remote data url
 *      dataValues          object|array The local Data values,
 *                          given either as array or as a value=>text object
 *      value               array The initial value(s),
 *      submitAsArray       boolean (false) If true, then the selected values will be submitted as a simple array,
 *                          rather than as an object/associative array
 *                          (default is object with 0/1 status for each box)
 *
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.FieldSet
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form
 */
class FlexiCheckboxGroup extends Common
{
    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     * @throws Exception
     */
    public function setData($key, $value = null): FlexiCheckboxGroup
    {
        if(in_array($key, ['itemId', 'name', 'remote', 'url', 'dataValues', 'value', 'submitAsArray'])) {
            return $this->setResource($key, $value);
        }
        if(in_array($key, ['id'])) {
            View::setData($key, $value);
            return $this;
        }
        return parent::setData($key, $value);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        if( !isset($this->resources['value'])
        || (
            !is_array($this->resources['value'])
            && !is_object($this->resources['value'])
            )
        ) {
            settype($this->resources['value'], 'array');
        }

        /** @var string $id */
        $id = $this->getData('id');
        if (!isset($id)) {
            $id = 'flexicheckboxgroup_' . md5(rand());
            $this->setData('id', $id);
        }

        /** @var string $name */
        $name = $this->getResource('name') ?? md5(rand());

        /** @var string $itemId */
        $itemId = $this->getResource('itemId');
        if (!isset($itemId)) {
            $itemId = $name ?? $id;
            $this->setResource('itemId', $itemId);
        }

        /** @var object|array $existingOptions */
        $existingOptions = $this->getResource('dataValues') ?? $this->getResource('value');

        /** @var array $value */
        $value = $this->getResource('value');
        if (is_object($value)) {
            $value = array_keys((array)$value);
        }

        $submitAsArray = (bool)$this->getResource('submitAsArray');

        /** @var array $data */
        $data = [];
        /** @var stdClass[] $checkboxes The initial checkboxes */
        $checkboxes = [];

        /** @var stdClass $comboConfig */
        $comboConfig = clone ($this->getResource('config') ?? new stdClass());
        $comboConfig->value = null;
        $comboConfig->multiSelect = false;
        $comboConfig->submitValue = false;
        $comboConfig->enableKeyEvents = true;
        $comboConfig->itemId = $itemId . '-addcombo';
        $comboConfig->remote = $this->getResource('remote');
        $comboConfig->dataValues = $this->getResource('dataValues');
        $comboConfig->url = $this->getResource('url');
        $comboConfig->configObjectOnly = false;

        $comboConfig->listeners = (object)[
            'select' => new JsFunction($this->viewFactory->createView(ComboSelect::class, [
                'checkBoxGroupItemId' => $itemId,
                'name' => $name,
                'submitAsArray' => $submitAsArray,
            ]), ['combo', 'records'])
        ];

        if (!($comboConfig->forceSelection ?? false)) {
            $comboConfig->listeners->keyup = new JsFunction($this->viewFactory->createView(ComboKeyup::class, [
                'checkBoxGroupItemId' => $itemId,
                'name' => $name,
                'submitAsArray' => $submitAsArray,
            ]), ['combo', 'e']);
        }

        /**
         * @var string|int $valueField
         * @var string $displayField
         */
        foreach( $existingOptions as $valueField => $displayField) {
            if(is_object($existingOptions)) {
                $data[] = (object)[
                    'value' => $valueField,
                    'text' => $displayField
                ];
                if(in_array($valueField, $value)) {
                    if ($submitAsArray) {
                        $checkboxes[] = (object)[
                            'inputId' => "{$itemId}-{$valueField}",
                            'name' => $name . '[]',
                            'value' => $valueField,
                            'inputValue' => $valueField,
                            'uncheckedValue' => new JsCustom('undefined'),
                            'boxLabel' => $displayField,
                            'checked' => true
                        ];
                    }
                    else {
                        $checkboxes[] = (object)[
                            'inputId' => "{$itemId}-{$valueField}",
                            'name' => "{$name}[{$valueField}]",
                            'inputValue' => 1,
                            'boxLabel' => $displayField,
                            'checked' => true
                        ];
                    }
                }
            }
            else {
                $data[] = ['value' => $displayField];
                if(in_array($displayField, $value)) {
                    if ($submitAsArray) {
                        $checkboxes[] = (object)[
                            'inputId' => "{$itemId}-{$displayField}",
                            'name' => $name . '[]',
                            'value' => $displayField,
                            'inputValue' => $displayField,
                            'uncheckedValue' => new JsCustom('undefined'),
                            'boxLabel' => $displayField,
                            'checked' => true
                        ];
                    }
                    else {
                        $checkboxes[] = (object)[
                            'inputId' => "{$itemId}-{$displayField}",
                            'name' => "{$name}[{$displayField}]",
                            'inputValue' => 1,
                            'boxLabel' => $displayField,
                            'checked' => true
                        ];
                    }
                }
            }
        }

        $this->setDataIfNotSet([
            'className' => 'Ext.form.FieldSet'
        ]);

        $this->setConfigIfNotSet([
            'id' => $id,
            'xtype' => 'fieldset',
            'layout' => 'fit',
            'collapsible' => false,
            'padding' => 5,
            'items' => [
                $this->viewFactory->createView(ComboBox::class, (array)$comboConfig),
                $this->viewFactory->createView(CheckboxGroup::class, [
                    'configObjectOnly' => false,
                    'itemId' => $itemId,
                    'name' => $name,
                    'columns' => 4,
                    'vertical' => false,
                    'defaults' => (object)[
                        'inputValue' => 1,
                        'uncheckedValue' => 0,
                        'checked' => true,
                        'submitValue' => true
                    ],
                    'items' => $checkboxes,
                    'submitAsArray' => $submitAsArray
                ])
            ]
        ]);
        if (!$this->hasConfig('itemId')) {
            $this->setConfig('itemId', $this->getConfig('name') ?? $this->getConfig('id'));
        }
        return parent::prepareData();
    }
}