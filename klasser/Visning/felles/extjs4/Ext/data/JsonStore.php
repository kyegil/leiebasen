<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsFunction;
use Kyegil\Leiebasen\Visning\felles\extjs4\Common;
use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\listeners\Load;
use Kyegil\ViewRenderer\View;
use stdClass;

/**
 * View class for ExtJs4 JsonStore
 *
 *  Resources:
 *      config              object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly    boolean if true then only the the config is returned as a JS object
 *      url                 string The remote data url
 *
 *  Template variables:
 *      $id
 *      $variableName
 *      $className
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.JsonStore
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data
 */
class JsonStore extends Common
{

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     * @throws Exception
     */
    public function setData($key, $value = null)
    {
        if(in_array($key, ['url'])) {
            return $this->setResource($key, $value);
        }
        if(in_array($key, ['id'])) {
            View::setData($key, $value);
            return $this;
        }
        return parent::setData($key, $value);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        $id = $this->getData('id') ?: 'store_' . md5(rand());
        /** @var string $url */
        $url = $this->getResource('url');

        $this->setDataIfNotSet([
            'className' => 'Ext.data.JsonStore'
        ]);
        $this->setConfigIfNotSet([
            'storeId' => $id
        ]);

        $this->addLoadListener();

        if($url) {
            $this->setConfigIfNotSet([
                'proxy' => (object)[
                    'type' => 'ajax',
                    'url' => $url,
                    'reader' => (object)[
                        'type' => 'json',
                        'root' => 'data'
                    ]
                ]
            ]);
        }

        return parent::prepareData();
    }

    protected function addLoadListener()
    {
        $listeners = $this->getConfig('listeners') ?? new stdClass();
        if (!property_exists($listeners, 'load')) {
            $listeners->load = new JsFunction($this->viewFactory->createView(Load::class, []), ['store']);
        }
        $this->setConfig('listeners', $listeners);
        return $this;
    }
}