<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\proxy;


use Kyegil\Leiebasen\Jshtml\PureJs;
use Kyegil\Leiebasen\Visning;

/**
 * ExtJs 4 Template
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $variableName
 *      $konfigurering
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\proxy
 */
class Proxy extends Visning
{
    protected $template = 'felles/extjs4/Ext/data/proxy/Proxy.js';

    protected $xtype = 'proxy';

    protected $data = [
        'variableName' => null,
        'konfigurering' => null
    ];

    /**
     * @param array|string $attributt
     * @param mixed|null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(is_array($attributt) || in_array($attributt, ['konfigurering', 'variableName'])) {
            return parent::sett($attributt, $verdi);
        }
        return $this->settRessurs($attributt, $verdi);

    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $konfigurering = '';
        foreach ($this->resources as $key => $resource) {
            if($resource instanceof PureJs) {
                $konfigurering .= "\t{$key}: {$resource},\n";
            }
            else {
                $konfigurering .= "\t{$key}: " . json_encode($resource) . ",\n";
            }
        }
        $konfigurering .= "\ttype: '{$this->xtype}'\n";

        $this->definerData([
            'konfigurering' => $konfigurering
        ]);
        return parent::forberedData();
    }
}