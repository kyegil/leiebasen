<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\proxy;


/**
 * ExtJs 4 Template
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $variableName
 *      $konfigurering
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\proxy
 */
class Ajax extends Proxy
{
    protected $template = 'felles/extjs4/Ext/data/proxy/Ajax.js';

    protected $xtype = 'ajax';

    protected $data = [
        'variableName' => null,
        'konfigurering' => null
    ];
}