<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/08/2020
 * Time: 10:37
 */

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\listeners;


use Kyegil\Leiebasen\Visning;

/**
 * Class Http403Forbidden
 *
 * Visning for Ext Js store listener
 *
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\listeners
 */
class Load extends Visning
{
    /** @var string */
    protected $template = 'felles/extjs4/Ext/data/listeners/Load.js';
}