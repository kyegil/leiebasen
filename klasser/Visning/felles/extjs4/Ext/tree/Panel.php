<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\tree;


use Exception;
use Kyegil\Leiebasen\Visning\felles\extjs4\Common;
use Kyegil\ViewRenderer\View;

/**
 * View class for ExtJs4 Tree Panel
 *
 *  Resources:
 *      config              object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly    boolean if true then only the the config is returned as a JS object
 *
 *  Template variables:
 *      $id
 *      $variableName
 *      $className
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.tree.Panel
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\tree
 */
class Panel extends Common
{

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     * @throws Exception
     */
    public function setData($key, $value = null)
    {
        if(in_array($key, [])) {
            return $this->setResource($key, $value);
        }
        if(in_array($key, ['id'])) {
            View::setData($key, $value);
            return $this;
        }
        return parent::setData($key, $value);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        $id = $this->getData('id') ?: 'treepanel_' . md5(rand());

        $this->setDataIfNotSet([
            'className' => 'Ext.tree.Panel'
        ]);
        $this->setConfigIfNotSet([
            'xtype' => 'treepanel',
            'id' => $id
        ]);

        return parent::prepareData();
    }
}