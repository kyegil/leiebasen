<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4\Ext\tab;


use Exception;
use Kyegil\Leiebasen\Visning\felles\extjs4\Common;
use Kyegil\ViewRenderer\View;

/**
 * View class for ExtJs4 Tab Panel
 *
 *  Resources:
 *      config              object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly    boolean if true then only the the config is returned as a JS object
 *      returnUrl           string
 *
 *  Template variables:
 *      $id
 *      $variableName
 *      $className
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.tab.Panel
 * @package Kyegil\Leiebasen\Visning\felles\extjs4\Ext\tab
 */
class Panel extends Common
{

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     * @throws Exception
     */
    public function setData($key, $value = null)
    {
        if(in_array($key, ['returnUrl'])) {
            return $this->setResource($key, $value);
        }
        if(in_array($key, ['id'])) {
            View::setData($key, $value);
            return $this;
        }
        return parent::setData($key, $value);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        $id = $this->getData('id') ?: 'tabpanel_' . md5(rand());

        $this->setDataIfNotSet([
            'className' => 'Ext.tab.Panel',
            'autoDestroy' => false
        ]);
        $this->setConfigIfNotSet([
            'xtype' => 'tabpanel',
            'id' => $id
        ]);

        return parent::prepareData();
    }
}