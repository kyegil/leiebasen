<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 10:01
 */

namespace Kyegil\Leiebasen\Visning\felles\extjs4;


use Kyegil\Leiebasen\Visning;

/**
 * Skript-fil i mine sider
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $extPath
 *      $extLoaderConfig
 *      $extRequire
 *      $extBeforeOnReady
 *      $extOnReady
 * @package Kyegil\Leiebasen\Visning\felles\extjs4
 */
class Index extends Visning
{
    protected $template = 'felles/extjs4/Index.js';

    protected $data = [
        'extPath' => null,
        'extLoaderConfig' => null,
        'extRequire' => null,
        'extBeforeOnReady' => null,
        'extOnReady' => null
    ];

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        $this->definerData([
            'extLoaderConfig' => json_encode(['enabled' => true]),
            'extPath' => '/pub/lib/' . $this->app->ext_bibliotek,
            'extRequire' => json_encode([
                'Ext.data.*',
                'Ext.form.field.*',
                'Ext.layout.container.Border',
                'Ext.grid.*',
                'Ext.grid.plugin.BufferedRenderer',
                'Ext.ux.RowExpander'
            ])
        ]);
        return parent::forberedData();
    }
}