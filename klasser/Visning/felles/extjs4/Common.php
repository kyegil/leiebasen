<?php

namespace Kyegil\Leiebasen\Visning\felles\extjs4;


use Exception;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Visning;
use stdClass;

/**
 * View class for ExtJs4 object
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api
 *
 *  Resources:
 *      config  object  The config object which will be passed to Ext/Js
 *      ignoredConfigs array Configs that should not be set for this instance
 *      configObjectOnly boolean if not set to false only the config is returned as a JS object
 *
 *  Template variables:
 *      $variableName
 *      $className
 *      $configString
 * @package Kyegil\Leiebasen\Visning\felles\extjs4
 */
class Common extends Visning
{
    protected $template = 'felles/extjs4/Common.js';

    protected $data = [
        'variableName' => null,
        'className' => null,
        'configString' => null
    ];

    /**
     * array_merge_recursive does indeed merge arrays, but it converts values with duplicate
     * keys to arrays rather than overwriting the value in the first array with the duplicate
     * value in the second array, as array_merge does. I.e., with array_merge_recursive,
     * this happens (documented behavior):
     *
     * array_merge_recursive(array('key' => 'org value'), array('key' => 'new value'));
     *     => array('key' => array('org value', 'new value'));
     *
     * array_merge_recursive_distinct does not change the datatypes of the values in the arrays.
     * Matching keys' values in the second array overwrite those in the first array, as is the
     * case with array_merge, i.e.:
     *
     * array_merge_recursive_distinct(array('key' => 'org value'), array('key' => 'new value'));
     *     => array('key' => array('new value'));
     *
     * Parameters are passed by reference, though only for performance reasons. They're not
     * altered by this function.
     *
     * @param array $array1
     * @param array $array2
     * @return array
     * @author Daniel <daniel (at) danielsmedegaardbuus (dot) dk>
     * @author Gabriel Sobrinho <gabriel (dot) sobrinho (at) gmail (dot) com>
     */
    public static function array_merge_recursive_distinct ( array &$array1, array &$array2 ): array
    {
        $merged = $array1;

        foreach ( $array2 as $key => &$value )
        {
            if ( is_array ( $value ) && isset ( $merged [$key] ) && is_array ( $merged [$key] ) )
            {
                $merged [$key] = self::array_merge_recursive_distinct ( $merged [$key], $value );
            }
            else if(is_numeric($key))
            {
                $merged[] = $value;
            }
            else
            {
                $merged [$key] = $value;
            }
        }

        return $merged;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     * @throws Exception
     */
    public function setData($key, $value = null)
    {
        if($key == 'configObjectOnly') {
            return $this->setConfigObjectOnly($value);
        }
        if($key == 'ignoredConfigs') {
            return $this->setIgnoredConfigs($value);
        }
        if(is_array($key) || in_array($key, ['variableName', 'className', 'configString'])) {
            return parent::setData($key, $value);
        }
        return $this->setConfig($key, $value);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function prepareData()
    {
        $configObjectOnly = boolval($this->getResource('configObjectOnly') ?? false);
        if($configObjectOnly) {
            $this->setData('className');
        }
        return parent::prepareData();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        $config = $this->getResource('config') ?? new stdClass();
        $this->setDataIfNotSet([
            'configString' => trim(JsCustom::JsonImproved($config))
        ]);
        return parent::__toString();
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getConfig(string $key)
    {
        $config = $this->getResource('config');
        return is_object($config) && property_exists($config, $key) ? $config->$key : null;
    }

    /**
     * @param string $key
     * @param $value
     * @return $this
     */
    public function setConfig(string $key, $value)
    {
        $ignoredConfigs = $this->getResource('ignoredConfigs') ?? [];
        if (in_array($key, $ignoredConfigs)) {
            return $this;
        }
        /** @var object|null $existingConfig */
        $existingConfig = $this->getResource('config');
        settype($existingConfig, 'array');
        $newConfig = $existingConfig;
        if (
            isset($existingConfig[$key])
            && is_array($existingConfig[$key])
            && is_array($value)
        ) {
            $newConfig[$key] = $this::array_merge_recursive_distinct($existingConfig[$key], $value );
        }
        else {
            $newConfig[$key] = $value;
        }
        $this->setResource('config', (object)$newConfig);

        return $this;
    }

    /**
     * @param array $configs
     * @return $this
     */
    public function setConfigIfNotSet(array $configs)
    {
        foreach($configs as $key => $data) {
            if(!$this->hasConfig($key)) {
                $this->setConfig($key, $data);
            }
        }
        return $this;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function hasConfig(string $key): bool
    {
        return isset($this->resources['config']) && property_exists($this->resources['config'], $key);
    }

    /**
     * @param bool $value
     * @return $this
     */
    private function setConfigObjectOnly(bool $value): Common
    {
        return $this->setResource('configObjectOnly', $value);
    }

    /**
     * @param string[] $value
     * @return $this
     */
    private function setIgnoredConfigs(array $value): Common
    {
        return $this->setResource('ignoredConfigs', $value);
    }
}