<?php

namespace Kyegil\Leiebasen\Visning\felles\js\form;


/**
 * Visning for JS-innhold i AjaxForm
 *
 *  Ressurser:
 *  Tilgjengelige variabler:
 * * $formId
 * * $reCaptchaSiteKey
 * * $method
 * * $enctype
 * * bool $useModal = false Use the modal for messages
 * @package Kyegil\Leiebasen\Visning\felles\js\form
 */
class AjaxForm extends \Kyegil\Leiebasen\Visning
{
    protected $template = 'felles/js/form/AjaxForm.js';
}