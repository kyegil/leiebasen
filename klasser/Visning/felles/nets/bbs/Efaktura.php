<?php

namespace Kyegil\Leiebasen\Visning\felles\nets\bbs;


/**
 * Class Efaktura
 *
 * Mal for eFaktura i BBS flatfil-format
 *
 *  Ressurser:
 *      regning \Kyegil\Leiebasen\Modell\Leieforhold\Regning
 *      efaktura \Kyegil\Leiebasen\Modell\Leieforhold\Regning\Efaktura
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\felles\nets\bbs
 */
class Efaktura extends \Kyegil\Leiebasen\Visning
{
    /** @var */
    protected $template = 'felles/nets/bbs/Efaktura.bbs';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['regning', 'efaktura'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold\Regning $regning */
        $regning = $this->hentRessurs('regning');
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold\Regning\Efaktura $efaktura */
        $efaktura = $this->hentRessurs('efaktura');

        if($regning) {
            $datasett = [];
            $this->definerData($datasett);
        }

        return parent::forberedData();
    }
}