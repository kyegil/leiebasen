<?php

namespace Kyegil\Leiebasen\Visning\felles\epost\søknad;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Modell\Søknad\Adgang;
use Kyegil\Leiebasen\Visning;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for epost bekreftelse for mottatt søknad
 *
 *  Ressurser:
 *      søknad \Kyegil\Leiebasen\Modell\Søknad
 *      adgang \Kyegil\Leiebasen\Modell\Søknad\Adgang
 *      språk string|null
 *  Mulige variabler:
 *      $navn
 *      $adgangEpost
 *      $adgangReferanse
 *      $epostTekst
 *      $søknadOppsummering
 * @package Kyegil\Leiebasen\Visning\felles\epost\søknad
 */
class Bekreftelse extends Visning
{
    /** @var string */
    protected $template = 'felles/epost/søknad/Bekreftelse.html';

    /**
     * @param $array
     * @return string
     */
    public static function jsonSomHtml($array): string
    {
        $str = "<table><tbody>";
        foreach ($array as $key => $val) {
            $str .= "<tr>";
            $str .= !is_numeric($key) ? "<td>$key</td>" : "<td></td>";
            $str .= "<td>";
            if (is_array($val) || is_object($val)) {
                if (!empty((array)$val)) {
                    $str .= self::jsonSomHtml($val);
                }
            } else {
                $str .= "<strong>$val</strong>";
            }
            $str .= "</td></tr>";
        }
        $str .= "</tbody></table>";

        return $str;
    }

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'søknad') {
            return $this->settSøknad($verdi);
        }
        if($attributt == 'adgang') {
            return $this->settAdgang($verdi);
        }
        if($attributt == 'språk') {
            return $this->settRessurs('språk', $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Søknad $søknad
     * @return Bekreftelse
     */
    public function settSøknad(?Søknad $søknad): Bekreftelse
    {
        return $this->settRessurs('søknad', $søknad);
    }

    /**
     * @param Adgang $adgang
     * @return Bekreftelse
     */
    public function settAdgang(?Adgang $adgang): Bekreftelse
    {
        return $this->settRessurs('adgang', $adgang);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): Bekreftelse
    {
        /** @var Søknad|null $søknad */
        $søknad = $this->hentRessurs('søknad');
        /** @var Adgang|null $adgang */
        $adgang = $this->hentRessurs('adgang');
        /** @var string|null $språk */
        $språk = $this->hentRessurs('språk');

        switch($språk) {
            case 'en':
                $this->template = 'felles/epost/søknad/BekreftelseEn.html';
        }

        if($adgang) {
            $datasett = [
                'adgangEpost' => $adgang->epost,
                'adgangReferanse' => $adgang->referanse,
            ];
            $this->definerData($datasett);
        }
        if($søknad) {
            $aktiveFelter = $søknad->hentAktiveFelter();
            $layout = $this->hentLayout($søknad);

            $søknadOppsummering = new ViewArray();
            $søknadOppsummering->addItem($this->hentHovedKontaktFelt($søknad, $språk));
            foreach($aktiveFelter as $felt) {
                $label = \Kyegil\Leiebasen\Leiebase::ucfirst(str_replace('_', ' ', $felt));
                $layoutObjekt = Søknad\Type::søkObjektEtter('name', $felt, $layout);
                if ($layoutObjekt) {
                    $label = $layoutObjekt->label ?? $label;
                    $label = $layoutObjekt->{"label[{$språk}]"} ?? $label;
                    $verdi = $søknad->hent($felt);
                    if (is_array($verdi) || $verdi instanceof \stdClass) {
                        $verdi = self::jsonSomHtml($verdi);
                    }
                    if (is_bool($verdi)) {
                        switch ($språk) {
                            case 'en':
                                $verdi = $verdi ? 'Yes' : 'No';
                                break;
                            default:
                                $verdi = $verdi ? 'Ja' : 'Nei';
                        }
                    }
                    settype($verdi, 'string');
                    $søknadOppsummering->addItem(
                        new HtmlElement('div', [], [
                            new HtmlElement('div', [],
                                new HtmlElement('strong', [], $label)
                            ),
                            new HtmlElement('div', [], $verdi),
                            '<br>'
                        ])
                    );
                }

            }

            $navn = $søknad->hent(Søknad\Type::FELT_KONTAKT_FORNAVN);
            $navn = ($navn ? ($navn . ' ') : '')
                . $søknad->hent(Søknad\Type::FELT_KONTAKT_ETTERNAVN);
            $typeKonfig = $søknad->hentMetaData('konfigurering');
            $epostTekst = $typeKonfig && !empty($typeKonfig->epostTekst) ? $typeKonfig->epostTekst : '';
            $datasett = [
                'navn' => $navn,
                'epostTekst' => $epostTekst,
                'søknadOppsummering' => $søknadOppsummering,
            ];
            $this->definerData($datasett);
        }
        $this->definerData([
            'navn' => '',
            'adgangEpost' => '',
            'adgangUrl' => $this->app->http_host . '/min-søknad/index.php',
            'adgangReferanse' => '',
            'epostTekst' => '',
            'søknadOppsummering' => '',
        ]);
        return parent::forberedData();
    }

    /**
     * @param Søknad $søknad
     * @return array
     * @throws Exception
     */
    private function hentLayout(Søknad $søknad): array
    {
        $konfigurering = $søknad->hentMetaData('konfigurering');
        return property_exists($konfigurering, 'layout') ? $konfigurering->layout : [];
    }

    /**
     * @param Søknad $søknad
     * @param string|null $språk
     * @return HtmlElement
     * @throws Exception
     */
    private function hentHovedKontaktFelt(Søknad $søknad, ?string $språk)
    {
        switch($språk) {
            case 'en':
                $label = 'Main applicant';
                break;
            default:
                $label = 'Hovedsøker';
        }
        $verdi = [];
        $navn = $søknad->hent(Søknad\Type::FELT_KONTAKT_FORNAVN);
        $navn = ($navn ? $navn . ' ' : '')
            . $søknad->hent(Søknad\Type::FELT_KONTAKT_ETTERNAVN);
        $adresse1 = $søknad->hent('kontakt_adresse1');
        $adresse2 = $søknad->hent('kontakt_adresse2');
        $postnrSted = $søknad->hent('kontakt_postnr');
        $postnrSted = ($postnrSted ? $postnrSted . ' ' : '')
            . $søknad->hent('kontakt_poststed');
        $telefon = $søknad->hent('kontakt_telefon');
        $epost = $søknad->hent(Søknad\Type::FELT_KONTAKT_EPOST);
        $verdi[] = $navn;
        if ($adresse1) {
            $verdi[] = $adresse1;
        }
        if ($adresse2) {
            $verdi[] = $adresse2;
        }
        if ($postnrSted) {
            $verdi[] = $postnrSted;
        }
        if ($telefon) {
            $verdi[] = $telefon;
        }
        if ($epost) {
            $verdi[] = $epost;
        }

        $hovedKontaktFelt = new HtmlElement('div', [], [
            new HtmlElement('div', [],
                new HtmlElement('strong', [], $label)
            ),
            new HtmlElement('div', [], implode('<br>', $verdi)),
            '<br>'
        ]);
        return $hovedKontaktFelt;
    }
}