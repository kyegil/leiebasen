<?php

namespace Kyegil\Leiebasen\Visning\felles\epost\søknad;


use Exception;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Modell\Søknad\Adgang;
use Kyegil\Leiebasen\Visning;

/**
 * Visning for påminnelses-epost om søknad i ferd med å utløpe
 *
 *  Ressurser:
 *      søknad \Kyegil\Leiebasen\Modell\Søknad
 *      adgang \Kyegil\Leiebasen\Modell\Søknad\Adgang
 *      språk string|null
 *  Mulige variabler:
 *      $navn
 *      $adgangEpost
 *      $adgangReferanse
 *      $epostTekst
 *      $utleier
 *      $kontaktadresse
 * @package Kyegil\Leiebasen\Visning\felles\epost\søknad
 */
class PåminnelseOmUtløp extends Visning
{
    /** @var string */
    protected $template = 'felles/epost/søknad/PåminnelseOmUtløp.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'søknad') {
            return $this->settSøknad($verdi);
        }
        if($attributt == 'adgang') {
            return $this->settAdgang($verdi);
        }
        if($attributt == 'språk') {
            return $this->settRessurs('språk', $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Søknad|null $søknad
     * @return PåminnelseOmUtløp
     */
    public function settSøknad(?Søknad $søknad): PåminnelseOmUtløp
    {
        return $this->settRessurs('søknad', $søknad);
    }

    /**
     * @param Adgang|null $adgang
     * @return PåminnelseOmUtløp
     */
    public function settAdgang(?Adgang $adgang): PåminnelseOmUtløp
    {
        return $this->settRessurs('adgang', $adgang);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): PåminnelseOmUtløp
    {
        /** @var Søknad|null $søknad */
        $søknad = $this->hentRessurs('søknad');
        /** @var Adgang|null $adgang */
        $adgang = $this->hentRessurs('adgang');
        /** @var string|null $språk */
        $språk = $this->hentRessurs('språk');

        switch($språk) {
            case 'en':
                $this->template = 'felles/epost/søknad/PåminnelseOmUtløpEn.html';
        }

        if($adgang) {
            $datasett = [
                'adgangEpost' => $adgang->epost,
                'adgangReferanse' => $adgang->referanse,
            ];
            $this->definerData($datasett);
        }
        if($søknad) {
            $navn = $søknad->hent(Søknad\Type::FELT_KONTAKT_FORNAVN);
            $navn = ($navn ? ($navn . ' ') : '')
                . $søknad->hent(Søknad\Type::FELT_KONTAKT_ETTERNAVN);
            $datasett = [
                'navn' => $navn,
            ];
            $this->definerData($datasett);
        }
        $this->definerData([
            'navn' => '',
            'adgangEpost' => '',
            'adgangUrl' => $this->app->http_host . '/min-søknad/index.php',
            'adgangReferanse' => '',
            'epostTekst' => '',
            'utløpsdato' => '',
            'utleier' => $this->app->hentValg('utleier'),
            'kontaktadresse' => $this->app->hentValg('epost'),
        ]);
        return parent::forberedData();
    }
}