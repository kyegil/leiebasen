<?php

namespace Kyegil\Leiebasen\Visning\felles\epost\kostnadsdeling\fordelingsforslag;


use Exception;
use Kyegil\Leiebasen\Visning;

/**
 * Class Html
 *
 * Fordelingsforslag-epostmal for kostnadsdeling
 *
 *  Ressurser:
 *      regning \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad
 *      tjeneste \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste
 *      fordelingsnøkkel \Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel
 *  Mulige variabler:
 *      $tjenesteNavn
 *      $tjenesteBeskrivelse
 *      $fordelingsnøkkelVisning
 *      $periode
 *      $fakturaBeløp
 *      $kostnadBeskrivelse
 *      $fordelingstekst
 *      $fordeling
 * @package Kyegil\Leiebasen\Visning\felles\epost\kostnadsdeling\fordelingsforslag
 */
class Html extends Visning
{
    /** @var */
    protected $template = 'felles/epost/kostnadsdeling/fordelingsforslag/Html.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['regning', 'tjeneste', 'fordelingsnøkkel'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad|null $regning */
        $regning = $this->hentRessurs('regning');
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste|null $tjeneste */
        $tjeneste = $regning && $regning->tjeneste ? $regning->tjeneste : $this->hentRessurs('tjeneste');
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel|null $fordelingsnøkkel */
        $fordelingsnøkkel = $tjeneste && $tjeneste->hentFordelingsnøkkel() ? $tjeneste->hentFordelingsnøkkel() : $this->hentRessurs('fordelingsnøkkel');

        if($regning) {
            $tidsrom = $regning->fradato->format('d.m.Y');
            if ($regning->fradato->format('Y-m-d') != $regning->tildato->format('Y-m-d')) {
                $tidsrom .= ' – ' . $regning->tildato->format('d.m.Y');
            }
            $periode = $regning->termin ?: $tidsrom;
            $this->definerData([
                'periode'           => $periode,
                'fakturaBeløp'      => $this->app->kr($regning->fakturabeløp),
                'kostnadBeskrivelse' => htmlspecialchars('Faktura ' . ($regning->fakturanummer)),
                'fordelingstekst' => nl2br(htmlspecialchars($this->app->hentValg('strømfordelingstekst'))),
                'fordeling'         => $this->app->vis(\Kyegil\Leiebasen\Visning\felles\epost\kostnadsdeling\fordelingsforslag\html\Fordeling::class, ['regning' => $regning]),
            ]);
        }

        if($tjeneste) {
            $this->definerData([
                'tjenesteNavn'     => htmlspecialchars($tjeneste->hentNavn()),
                'tjenesteBeskrivelse' => htmlspecialchars($tjeneste->hentBeskrivelse()),
            ]);
        }

        if($fordelingsnøkkel) {
            $this->definerData([
                'fordelingsnøkkelVisning'  => $this->app->vis(
                    Visning\felles\html\kostnadsdeling\Fordelingsnøkkel::class,
                    ['fordelingsnøkkel' => $fordelingsnøkkel]
                ),
            ]);
        }

        $this->definerData([
            'tjenesteNavn'     => '',
            'tjenesteBeskrivelse' => '',
            'fordelingsnøkkelVisning'  => '',
            'periode'           => '',
            'fakturaBeløp'      => '',
            'kostnadBeskrivelse' => '',
            'fordelingstekst' => '',
            'fordeling'         => '',
        ]);

        return parent::forberedData();
    }
}