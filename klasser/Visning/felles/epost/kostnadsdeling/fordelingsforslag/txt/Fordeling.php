<?php

namespace Kyegil\Leiebasen\Visning\felles\epost\kostnadsdeling\fordelingsforslag\txt;


use Exception;
use Kyegil\Leiebasen\Visning;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Class Html
 *
 * Fordelingsforslag-epostmal for kostnadsdeling
 *
 *  Ressurser:
 *      regning \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad
 *  Mulige variabler:
 *      $andeler
 * @package Kyegil\Leiebasen\Visning\felles\epost\kostnadsdeling\fordelingsforslag\txt
 */
class Fordeling extends Visning
{
    /** @var */
    protected $template = 'felles/epost/kostnadsdeling/fordelingsforslag/txt/Fordeling.txt';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['regning'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad|null $regning */
        $regning = $this->hentRessurs('regning');
        $andeler = new ViewArray();

        if($regning) {
            foreach ($regning->hentAndeler() as $andel) {
                $andeler->addItem($this->app->vis(
                    Visning\felles\epost\kostnadsdeling\fordelingsforslag\txt\fordeling\Andel::class,
                    ['andel' => $andel]
                ));
            }
        }

        $this->definerData([
            'andeler'     => $andeler,
        ]);

        return parent::forberedData();
    }
}