<?php

namespace Kyegil\Leiebasen\Visning\felles\epost\kostnadsdeling\fordelingsforslag\txt\fordeling;


use Exception;
use Kyegil\Leiebasen\Visning;

/**
 * Class Html
 *
 * Fordelingsforslag-epostmal for kostnadsdeling
 *
 *  Ressurser:
 *      andel \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andel
 *  Mulige variabler:
 *      $leieforholdId
 *      $leieforholdNavn
 *      $andelTekst
 *      $andelBeløp
 * @package Kyegil\Leiebasen\Visning\felles\epost\kostnadsdeling\fordelingsforslag\txt\fordeling
 */
class Andel extends Visning
{
    /** @var */
    protected $template = 'felles/epost/kostnadsdeling/fordelingsforslag/txt/fordeling/Andel.txt';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['andel'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andel|null $andel */
        $andel = $this->hentRessurs('andel');

        if($andel) {
            $this->definerData([
                'leieforholdId' => $andel->leieforhold ? intval($andel->leieforhold->hentId()) : '',
                'leieforholdNavn' => ($andel->leieforhold ? $andel->leieforhold->hentNavn() : ''),
                'andelTekst' => ($andel->tekst),
                'andelBeløp' => $this->app->kr($andel->beløp, false),
            ]);
        }

        $this->definerData([
            'leieforholdId' => '',
            'leieforholdNavn' => '',
            'andelTekst' => '',
            'andelBeløp' => '',
        ]);

        return parent::forberedData();
    }
}