<?php

namespace Kyegil\Leiebasen\Visning\felles\epost\kostnadsdeling\fordelingsforslag;


use Exception;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\epost\kostnadsdeling\fordelingsforslag\txt\Fordeling;

/**
 * Class Txt
 *
 * Fordelingsforslag-epostmal for kostnadsdeling
 *
 *  Ressurser:
 *      regning \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad
 *      tjeneste \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste
 *      fordelingsnøkkel \Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel
 *  Mulige variabler:
 *      $tjenesteNavn
 *      $tjenesteBeskrivelse
 *      $fordelingsnøkkelVisning
 *      $periode
 *      $fakturaBeløp
 *      $kostnadBeskrivelse
 *      $fordelingstekst
 *      $fordeling
 * @package Kyegil\Leiebasen\Visning\felles\epost\kostnadsdeling\fordelingsforslag
 */
class Txt extends Visning
{
    /** @var */
    protected $template = 'felles/epost/kostnadsdeling/fordelingsforslag/Txt.txt';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['regning', 'tjeneste', 'fordelingsnøkkel'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Kostnad|null $regning */
        $regning = $this->hentRessurs('regning');
        /** @var Tjeneste|null $tjeneste */
        $tjeneste = $regning && $regning->tjeneste ? $regning->tjeneste : $this->hentRessurs('tjeneste');
        /** @var Fordelingsnøkkel|null $fordelingsnøkkel */
        $fordelingsnøkkel = $tjeneste && $tjeneste->hentFordelingsnøkkel() ? $tjeneste->hentFordelingsnøkkel() : $this->hentRessurs('fordelingsnøkkel');

        if($regning) {
            $tidsrom = $regning->fradato->format('d.m.Y');
            if ($regning->fradato->format('Y-m-d') != $regning->tildato->format('Y-m-d')) {
                $tidsrom .= ' – ' . $regning->tildato->format('d.m.Y');
            }
            $periode = $regning->termin ?: $tidsrom;
            $this->definerData([
                'periode'           => $periode,
                'fakturaBeløp'      => $this->app->kr($regning->fakturabeløp, false),
                'kostnadBeskrivelse' => ('Faktura ' . ($regning->fakturanummer)),
                'fordelingstekst' => ($this->app->hentValg('strømfordelingstekst')),
                'fordeling'         => $this->app->vis(Fordeling::class, ['regning' => $regning]),
            ]);
        }

        if($tjeneste) {
            $this->definerData([
                'tjenesteNavn'     => ($tjeneste->hentNavn()),
                'tjenesteBeskrivelse' => ($tjeneste->hentBeskrivelse()),
            ]);
        }

        if($fordelingsnøkkel) {
        $this->definerData([
                'fordelingsnøkkelVisning'  => $this->app->vis(
                    Visning\felles\txt\kostnadsdeling\Fordelingsnøkkel::class,
                    ['fordelingsnøkkel' => $fordelingsnøkkel]
                ),
            ]);
        }

        $this->definerData([
            'tjenesteNavn'     => '',
            'tjenesteBeskrivelse' => '',
            'fordelingsnøkkelVisning'  => '',
            'periode'           => '',
            'fakturaBeløp'      => '',
            'kostnadBeskrivelse' => '',
            'fordelingstekst' => '',
            'fordeling'         => '',
        ]);

        return parent::forberedData();
    }
}