<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 12/10/2020
 * Time: 22:31
 */

namespace Kyegil\Leiebasen\Visning\felles\epost\beboerstøtte\sakstråd;

use Exception;
use Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd;
use Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd\Innlegg as InnleggModell;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning;

/**
 * Visning for epost bebeoerstøtte-innlegg
 *
 *  Ressurser:
 *      mottaker \Kyegil\Leiebasen\Modell\Person
 *      sakstråd \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd
 *      innlegg \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd\Innlegg
 *  Mulige variabler:
 *      $avsendernavn
 *      $saksref
 *      $sakslenke
 *      $innleggslenke
 *      $innhold
 * @package Kyegil\Leiebasen\Visning\felles\epost\beboerstøtte\sakstråd
 */
class Innlegg extends Visning
{
    /** @var string */
    protected $template = 'felles/epost/beboerstøtte/sakstråd/Innlegg.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['mottaker', 'innlegg', 'sakstråd'])) {
            return $this->settRessurs($attributt, $verdi);
        }

        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Person|null $mottaker */
        $mottaker = $this->hentRessurs('mottaker');
        /** @var InnleggModell|null $innlegg */
        $innlegg = $this->hentRessurs('innlegg');
        /** @var Sakstråd|null $sakstråd */
        $sakstråd = $this->hentRessurs('sakstråd');
        if(!$sakstråd && $innlegg) {
            $sakstråd = $innlegg->hentSak();
        }
        if($innlegg) {
            $skadeId = (strpos($sakstråd->saksref, 'skademelding-') === 0) ? substr($sakstråd->saksref, 13) : null;
            $avsender = $innlegg->hentAvsender();
            $sakslenke = $this->app->http_host . "/mine-sider/index.php?oppslag=skademelding_kort&id={$sakstråd->hentId()}&returi=default";
            if ($sakstråd->erAdministrator($mottaker)) {
                $sakslenke = $this->app->http_host . "/drift/index.php?oppslag=skadekort&id={$skadeId}&returi=default";
            }
            $innleggslenke = $this->app->http_host . "";
            $datasett = [
                'avsendernavn' => $avsender ? $avsender->hentNavn() : '',
                'saksref' => $sakstråd->hentSaksref(),
                'sakslenke' => $sakslenke,
                'innleggslenke' => $innleggslenke,
                'innhold' => $innlegg->hentInnhold()
            ];
            $this->definerData($datasett);
        }
        return parent::forberedData();
    }
}