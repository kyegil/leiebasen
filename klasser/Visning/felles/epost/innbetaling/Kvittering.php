<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 05/01/2021
 * Time: 22:19
 */

namespace Kyegil\Leiebasen\Visning\felles\epost\innbetaling;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Adressefelt;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Class Kvittering
 *
 * Mal for epost innbetalingskvittering
 *
 *  Ressurser:
 *      innbetaling \Kyegil\Leiebasen\Modell\Innbetaling
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $logotekst
 *      $avsender
 *      $avsenderadresse
 *      $avsenderEpost
 *      $avsenderOrgNr
 *      $avsenderTelefon
 *      $avsenderBankkonto
 *      $avsenderHjemmeside
 *      $mottaker
 *      $mottakerAdresse
 *      $leieforholdnr
 *      $leieforholdbeskrivelse
 *      $betalingsdato
 *      $beløp
 *      $transaksjonsmåte
 *      $betaler
 *      $kid
 *      $referanse
 *      $bunntekst
 *      $avstemming
 *      $urlBase
 * @package Kyegil\Leiebasen\Visning\felles\epost\innbetaling
 */
class Kvittering extends Visning
{
    /** @var */
    protected $template = 'felles/epost/innbetaling/Kvittering.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['innbetaling', 'leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Innbetaling $innbetaling */
        $innbetaling = $this->hentRessurs('innbetaling');
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        if($innbetaling && $leieforhold) {
            $ocrTransaksjon = $innbetaling->hentOcrTransaksjon();
            $transaksjonsmåte = $ocrTransaksjon ? $ocrTransaksjon->hentTransaksjonsbeskrivelse() : '';

            $avstemming = new ViewArray();

            $datasett = [
                'logotekst'			=> $this->app->hentValg('utleier'),
                'avsender'			=> $this->app->hentValg('utleier'),
                'avsenderadresse'	=> $this->app->hentValg('adresse') . "<br>" . $this->app->hentValg('postnr') . "&nbsp;" . $this->app->hentValg('poststed'),
                'avsenderTelefon'	=> $this->app->hentValg('telefon'),
                'avsenderHjemmeside' => $this->app->hentValg('hjemmeside'),
                'avsenderEpost'		=> $this->app->hentValg('epost'),
                'avsenderOrgNr'		=> $this->app->hentValg('orgnr'),
                'avsenderBankkonto'	=> $this->app->hentValg('bankkonto'),
                'mottaker'			=> $leieforhold->hentNavn(),
                'mottakerAdresse'	=> $this->app->vis(Adressefelt::class, [
                    'leieforhold' => $leieforhold
                ]),
                'leieforholdnr'		=> $leieforhold->hentId(),
                'leieforholdbeskrivelse' => $leieforhold->hentBeskrivelse(),
                'betalingsdato'     => $innbetaling->hentDato()->format('d.m.Y'),
                'beløp'		        => $this->app->kr($innbetaling->hentBeløpTilLeieforhold($leieforhold)),
                'transaksjonsmåte'  => $transaksjonsmåte,
                'betaler'			=> $innbetaling->hentBetaler(),
                'kid'               => $ocrTransaksjon ? $ocrTransaksjon->kid : '',
                'referanse'         => $innbetaling->hentRef(),
                'bunntekst'			=> $this->app->hentValg('eposttekst'),
                'avstemming'		=> $avstemming,
                'urlBase'           => $this->app->http_host
            ];

            foreach($innbetaling->hentDelbeløpTilLeieforhold($leieforhold) as $delbeløp) {
                $avstemming->addItem($this->app->vis(Avstemmingslinje::class, [
                    'delbeløp' => $delbeløp,
                ]));
            }
            $this->definerData($datasett);
        }

        return parent::forberedData();
    }
}