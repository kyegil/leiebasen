<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 05/01/2021
 * Time: 22:19
 */

namespace Kyegil\Leiebasen\Visning\felles\epost\innbetaling;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Visning;

/**
 * Class Kvittering
 *
 * Mal for epost innbetalingskvittering
 *
 *  Ressurser:
 *      delbeløp \Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp
 *  Mulige variabler:
 *      $delbeløpId
 *      $beløp
 *      $avstemmingsbeskrivelse
 *      $opprinneligKravBeløp
 *      $utestående
 * @package Kyegil\Leiebasen\Visning\felles\epost\innbetaling
 */
class Avstemmingslinje extends Visning
{
    /** @var */
    protected $template = 'felles/epost/innbetaling/Avstemmingslinje.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Avstemmingslinje
    {
        if($attributt == 'delbeløp') {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): Avstemmingslinje
    {
        /** @var Delbeløp $delbeløp */
        $delbeløp = $this->hentRessurs('delbeløp');
        if($delbeløp) {
            /** @var Krav|true|null $krav */
            $krav = $delbeløp->hentKrav();
            if ($krav instanceof Krav) {
                $avstemmingsbeskrivelse = $krav->tekst;
            }
            else if ($krav === true) {
                $avstemmingsbeskrivelse = new HtmlElement('span', [], 'tilbakeført');
            }
            else {
                $avstemmingsbeskrivelse = new HtmlElement('i', [], 'ikke avstemt');
            }

            $datasett = [
                'delbeløpId'    => $delbeløp->hentId(),
                'beløp'			=> $this->app->kr($delbeløp->hentBeløp()),
                'avstemmingsbeskrivelse'	=> $avstemmingsbeskrivelse,
                'opprinneligKravBeløp'	=> $krav instanceof Krav ? $this->app->kr($krav->beløp) : '',
                'utestående'	=> $krav instanceof Krav ? $this->app->kr($krav->utestående) : '',
            ];
            $this->definerData($datasett);
        }

        return parent::forberedData();
    }
}