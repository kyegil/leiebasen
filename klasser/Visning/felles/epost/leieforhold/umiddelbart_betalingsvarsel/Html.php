<?php

namespace Kyegil\Leiebasen\Visning\felles\epost\leieforhold\umiddelbart_betalingsvarsel;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\epost\leieforhold\forfallsvarsel\html\Krav;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Class Html
 *
 * Mal for epost forfallsvarsel
 *
 *  Ressurser:
 *      kravsett \Kyegil\Leiebasen\Modell\Leieforhold\Kravsett
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $leieforholdnr
 *      $leieforholdbeskrivelse
 *      $fbo
 *      $fastKid
 *      $urlBase
 *      $linjer
 *      $bankkonto
 * @package Kyegil\Leiebasen\Visning\felles\epost\leieforhold\umiddelbart_betalingsvarsel
 */
class Html extends Visning
{
    /** @var */
    protected $template = 'felles/epost/leieforhold/umiddelbart_betalingsvarsel/Html.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['kravsett', 'leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Kravsett|null $kravsett */
        $kravsett = $this->hentRessurs('kravsett');
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $kravsett->hentFørste() ? $kravsett->hentFørste()->hentLeieforhold() : $this->hentRessurs('leieforhold');
        $epostTekst = $this->app->hentValg('umiddelbart_betalingsvarsel_tekst_epost');
        if($kravsett && $leieforhold) {
            $linjer = new ViewArray();

            foreach ($kravsett as $krav) {
                /** @var Krav $kravLinje */
                $kravLinje = $this->app->hentVisning(Krav::class, ['krav' => $krav, 'avtaleGiro' => '']);
                $linjer->addItem($kravLinje);
            }

            $datasett = [
                'leieforholdnr'		=> $leieforhold->hentId(),
                'leieforholdbeskrivelse' => $leieforhold->hentBeskrivelse(),
                'fbo'               => (bool)$leieforhold->hentFbo(),
                'fastKid'           => $leieforhold->hentKid(),
                'linjer'	    	=> $linjer,
            ];

            $this->definerData($datasett);
        }
        $this->definerData([
            'leieforholdnr'     => '',
            'leieforholdbeskrivelse' => '',
            'epostTekst'        => $epostTekst,
            'fbo'               => false,
            'ocr'               => (bool)$this->app->hentValg('ocr'),
            'fastKid'           => '',
            'linjer'            => '',
            'urlBase'           => $this->app->http_host,
            'bankkonto'         => $this->app->hentValg('bankkonto'),
        ]);

        return parent::forberedData();
    }
}