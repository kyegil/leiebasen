<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 05/01/2021
 * Time: 22:19
 */

namespace Kyegil\Leiebasen\Visning\felles\epost\leieforhold\forfallsvarsel\txt;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning;

/**
 * Class Krav
 *
 * Mal for kravlinje i epost forfallsvarsel
 *
 *  Ressurser:
 *      krav \Kyegil\Leiebasen\Modell\Leieforhold\Krav
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $forfall
 *      $tekst
 *      $beløp
 *      $utestående
 *      $kid
 *      $avtaleGiro
 * @package Kyegil\Leiebasen\Visning\felles\epost\leieforhold\forfallsvarsel\txt
 */
class Krav extends Visning
{
    /** @var */
    protected $template = 'felles/epost/leieforhold/forfallsvarsel/txt/Krav.txt';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['krav', 'leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold\Krav|null $krav */
        $krav = $this->hentRessurs('krav');
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $krav ? $krav->leieforhold : $this->hentRessurs('leieforhold');
        if($krav) {
            $regning = $krav->regning;
            if ($regning) {
                $this->definerData([
                    'avtaleGiro' => $regning->hentFboTrekk() ? 'Beløpet trekkes med AvtaleGiro.' : ''
                ]);
            }
            $datasett = [
                'forfall'   => $krav->forfall ? $krav->forfall->format('d.m.Y') : '',
                'tekst'     => $krav->tekst,
                'beløp'     => $this->app->kr($krav->beløp, false),
                'utestående' => $this->app->kr($krav->utestående, false),
                'kid'       => $regning ? $regning->kid : $leieforhold->hentKid(),
            ];

            $this->definerData($datasett);
        }
        $this->definerData([
            'forfall'   => '',
            'tekst'     => '',
            'beløp'     => '',
            'utestående' => '',
            'kid'       => '',
            'avtaleGiro' => '',
        ]);

        return parent::forberedData();
    }
}