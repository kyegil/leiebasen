<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 05/01/2021
 * Time: 22:19
 */

namespace Kyegil\Leiebasen\Visning\felles\epost\purring;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Purring\Kravlinksett;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Adressefelt;
use Kyegil\Leiebasen\Visning\felles\html\purring\Kravlinje;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Class Epostpurring
 *
 * Mal for epostpurring
 *
 *  Ressurser:
 *      purring \Kyegil\Leiebasen\Modell\Leieforhold\Purring
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $logotekst
 *      $avsender
 *      $avsenderadresse
 *      $avsenderEpost
 *      $avsenderOrgNr
 *      $avsenderTelefon
 *      $avsenderBankkonto
 *      $avsenderHjemmeside
 *      $mottaker
 *      $mottakerAdresse
 *      $leieforholdnr
 *      $leieforholdbeskrivelse
 *      $kid
 *      $dato
 *      $forfall
 *      $utestående
 *      boolean $avtalegiroTilgjengelig
 *      boolean $efakturaTilgjengelig
 *      boolean $efaktura
 *      $detaljer
 *      $girotekst
 *      $bunntekst
 *      $urlBase
 * @package Kyegil\Leiebasen\Visning\felles\epost\regning
 */
class Epostpurring extends Visning
{
    /** @var */
    protected $template = 'felles/epost/purring/Epostpurring.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['purring', 'leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold\Purring $purring */
        $purring = $this->hentRessurs('purring');
        if($purring) {
            $leieforhold = $this->hentRessurs('leieforhold') ?: $purring->hentLeieforhold();
            $efaktura = (
            (bool)$leieforhold->hentEfakturaIdentifier()
            );

            $detaljer = new ViewArray();

            $datasett = [
                'logotekst'			=> $this->app->hentValg('utleier'),
                'avsender'			=> $this->app->hentValg('utleier'),
                'avsenderadresse'	=> $this->app->hentValg('adresse') . "<br>" . $this->app->hentValg('postnr') . "&nbsp;" . $this->app->hentValg('poststed'),
                'avsenderTelefon'	=> $this->app->hentValg('telefon'),
                'avsenderHjemmeside' => $this->app->hentValg('hjemmeside'),
                'avsenderEpost'		=> $this->app->hentValg('epost'),
                'avsenderOrgNr'		=> $this->app->hentValg('orgnr'),
                'avsenderBankkonto'	=> $this->app->hentValg('bankkonto'),
                'mottaker'			=> $leieforhold->hentNavn(),
                'mottakerAdresse'	=> $this->app->vis(Adressefelt::class, [
                    'leieforhold' => $leieforhold
                ]),
                'leieforholdnr'		=> $leieforhold->hentId(),
                'leieforholdbeskrivelse' => $leieforhold->hentBeskrivelse(),
                'kid'				=> $leieforhold->hentKid(),
                'dato'				=> $purring->hentPurredato()
                    ? $purring->hentPurredato()->format('d.m.Y')
                    : '',
                'forfall'			=> $purring->hentPurreforfall() ? $purring->hentPurreforfall()->format('d.m.Y') : '',
                'utestående'		=> $this->app->kr($purring->hentUtestående()),
                'fbo'		        => $this->app->hentValg('avtalegiro') && $leieforhold->hentFbo('alle'),
                'efaktura'			=> $efaktura,
                'avtalegiroTilgjengelig' => (bool)$this->app->hentValg('avtalegiro'),
                'efakturaTilgjengelig' => (bool)$this->app->hentValg('efaktura'),
                'girotekst'			=> nl2br($this->app->hentValg('girotekst')),
                'bunntekst'			=> $this->app->hentValg('eposttekst'),
                'detaljer'			=> $detaljer,
                'urlBase'           => $this->app->http_host
            ];
            /** @var Kravlinksett $kravlinker */
            $kravlinker = $purring->hentKravlinker();
            $kravlinker->leggTilKravModell();
            $antallLinjer = $kravlinker->hentAntall();
            $linjeNummer = 0;
            /** @var $kravlink $kravlink */
            foreach($kravlinker as $kravlink) {
                $krav = $kravlink->krav;
                if ($krav) {
                    $linjeNummer++;
                    /** @var Kravlinje $kravlinje */
                    $kravlinje = $this->app->vis(Kravlinje::class, [
                        'tekst' => $krav->hentTekst(),
                        'forfallsdato' => $krav->hentForfall() ? $krav->hentForfall()->format('d.m.Y') : '',
                        'beløp' => $this->app->kr($krav->hentUtestående()),
                        'linjeNummer' => $linjeNummer,
                        'antallLinjer' => $antallLinjer
                    ]);
                    $detaljer->addItem($kravlinje);
                }
            }
            $this->definerData($datasett);
        }

        return parent::forberedData();
    }
}