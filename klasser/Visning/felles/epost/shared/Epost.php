<?php

namespace Kyegil\Leiebasen\Visning\felles\epost\shared;


use Exception;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Visning;

/**
 * Class Epost
 *
 * Ressurser:
 * * regning \Kyegil\Leiebasen\Modell\Leieforhold\Regning
 * * leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *
 * Tilgjengelige variabler:
 * * $tittel
 * * $språk
 * * $css
 * * $innhold
 * * $bunntekst
 * @package Kyegil\Leiebasen\Visning\felles\epost\regning
 */
class Epost extends Visning
{
    /** @var */
    protected $template = 'felles/epost/shared/Epost.html';

    /**
     * Konverterer html til ren-tekst for epost
     * @param string $string
     * @return string
     */
    public static function br2crnl(string $string): string
    {
        return strip_tags(
            str_ireplace(['<br>','<br>','<br>'], "\r\n", $string)
        );
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $cssPath = Leiebase::$config['leiebasen']['server']['root'] . 'pub/css/epost.css';
        if (is_readable($cssPath)) {
            $css = file_get_contents($cssPath);
        }
        $datasett = [
            'tittel' => '',
            'språk' => 'nb',
            'css' => $css,
            'innhold' => '',
            'bunntekst' => $this->app->hentValg('eposttekst')
        ];
        $this->definerData($datasett);

        return parent::forberedData();
    }
}