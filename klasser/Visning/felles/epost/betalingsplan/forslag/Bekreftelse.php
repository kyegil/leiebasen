<?php

namespace Kyegil\Leiebasen\Visning\felles\epost\betalingsplan\forslag;


use Exception;
use Kyegil\Leiebasen\Visning;

/**
 * Visning for epost bekreftelse for innsendt forslag til betalingsplan
 *
 *  Mulige variabler:
 *      $utleier
 *      $leieforholdId
 *      $leieforholdBeskrivelse
 *      $originalkrav
 *      $avdrag
 *      $avtaleGiro
 * @package Kyegil\Leiebasen\Visning\felles\epost\betalingsplan\forslag
 */
class Bekreftelse extends Visning
{
    /** @var string */
    protected $template = 'felles/epost/betalingsplan/forslag/Bekreftelse.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $this->definerData([
            'utleier' => $this->app->hentValg('utleier'),
            'leieforholdId' => '',
            'leieforholdBeskrivelse' => '',
            'originalkrav' => '',
            'avdrag' => '',
            'avtaleGiro' => '',
        ]);
        return parent::forberedData();
    }
}