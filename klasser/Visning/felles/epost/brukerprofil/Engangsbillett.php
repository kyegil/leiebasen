<?php

namespace Kyegil\Leiebasen\Visning\felles\epost\brukerprofil;


use Exception;
use Kyegil\Leiebasen\Modell\Person\Brukerprofil;
use Kyegil\Leiebasen\Modell\Person\Brukerprofil\Engangsbillett as EngangsbillettModell;
use Kyegil\Leiebasen\Visning;

/**
 * Visning for epost bekreftelse for opprettet brukerprofil
 *
 *  Ressurser:
 *      brukerprofil \Kyegil\Leiebasen\Modell\Person\Brukerprofil
 *      engangsbillett \Kyegil\Leiebasen\Modell\Person\Brukerprofil\Engangsbillett
 *  Mulige variabler:
 *      $navn
 *      $login
 *      $nettstedBeskrivelse
 *      $nettstedUrl
 *      $engangsbillettUrl
 * @package Kyegil\Leiebasen\Visning\felles\epost\beboerstøtte
 */
class Engangsbillett extends Visning
{
    /** @var string */
    protected $template = 'felles/epost/brukerprofil/Engangsbillett.html';

    /**
     * @param array|string $attributt
     * @param mixed $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['brukerprofil', 'engangsbillett'])) {
            return $this->settRessurs($attributt, $verdi);
        }

        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var EngangsbillettModell|null $engangsbillett */
        $engangsbillett = $this->hentRessurs('engangsbillett');
        /** @var Brukerprofil|null $brukerprofil */
        $brukerprofil = $engangsbillett ? $engangsbillett->brukerprofil : $this->hentRessurs('brukerprofil');
        if($brukerprofil) {
            $this->definerData([
                'navn' => $brukerprofil->hentPerson()->hentNavn(),
                'login' => $brukerprofil->login,
            ]);
        }
        $this->definerData([
            'nettstedUrl' => $this->app->http_host,
            'nettstedBeskrivelse' => '«Mine sider» for ' . $this->app->hentValg('utleier'),
            'engangsbillettUrl' => $engangsbillett ?
                $this->app->http_host
                . "/offentlig/index.php?oppslag=engangsbillett&engangsbillett={$engangsbillett->billett}"
            : ''
        ]);
        return parent::forberedData();
    }
}