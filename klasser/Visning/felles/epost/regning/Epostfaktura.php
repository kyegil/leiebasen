<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 05/01/2021
 * Time: 22:19
 */

namespace Kyegil\Leiebasen\Visning\felles\epost\regning;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Adressefelt;
use Kyegil\Leiebasen\Visning\felles\html\regning\Kravlinje;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Class Epostfaktura
 *
 * Mal for epostfaktura
 *
 *  Ressurser:
 *      regning \Kyegil\Leiebasen\Modell\Leieforhold\Regning
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $logotekst
 *      $avsender
 *      $avsenderadresse
 *      $avsenderEpost
 *      $avsenderOrgNr
 *      $avsenderTelefon
 *      $avsenderBankkonto
 *      $avsenderHjemmeside
 *      $mottaker
 *      $mottakerAdresse
 *      int $leieforholdnr
 *      $leieforholdbeskrivelse
 *      int $gironr
 *      $kid
 *      $dato
 *      $forfall
 *      $girobeløp
 *      $fradrag
 *      $utestående
 *      boolean $fbo
 *      boolean $avtalegiroTilgjengelig
 *      boolean $efakturaTilgjengelig
 *      boolean $efaktura
 *      $detaljer
 *      $girotekst
 *      $bunntekst
 *      $urlBase
 * @package Kyegil\Leiebasen\Visning\felles\epost\regning
 */
class Epostfaktura extends Visning
{
    /** @var */
    protected $template = 'felles/epost/regning/Epostfaktura.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt, ['regning', 'leieforhold'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold\Regning $regning */
        $regning = $this->hentRessurs('regning');
        if($regning) {
            $leieforhold = $this->hentRessurs('leieforhold') ?: $regning->hentLeieforhold();
            $efaktura = (bool)$leieforhold->hentEfakturaIdentifier();
            $leieobjekt = $leieforhold->hentLeieobjekt();
            $fbo = $this->app->hentValg('avtalegiro') && $leieforhold->hentFbo();
            if ($fbo) {
                $nesteTrekkForsendelse = $this->app->hentNetsProsessor()->nesteFboTrekkravForsendelse();
                $fbo = !$regning->hentFboOppdragsfrist() || $regning->hentFboOppdragsfrist() > $nesteTrekkForsendelse;
            }

            $detaljer = new ViewArray();

            $datasett = [
                'logotekst'			=> $this->app->hentValg('utleier'),
                'avsender'			=> $this->app->hentValg('utleier'),
                'avsenderadresse'	=> $this->app->hentValg('adresse') . "<br>" . $this->app->hentValg('postnr') . "&nbsp;" . $this->app->hentValg('poststed'),
                'avsenderTelefon'	=> $this->app->hentValg('telefon'),
                'avsenderHjemmeside' => $this->app->hentValg('hjemmeside'),
                'avsenderEpost'		=> $this->app->hentValg('epost'),
                'avsenderOrgNr'		=> $this->app->hentValg('orgnr'),
                'avsenderBankkonto'	=> $this->app->hentValg('bankkonto'),
                'mottaker'			=> $leieforhold->hentNavn(),
                'mottakerAdresse'	=> $this->app->vis(Adressefelt::class, [
                    'leieforhold' => $leieforhold
                ]),
                'leieforholdnr'		=> $leieforhold->hentId(),
                'leieforholdbeskrivelse' => $leieobjekt->hentBeskrivelse(),
                'gironr'			=> $regning->hentId(),
                'kid'				=> $regning->hentKid(),
                'dato'				=> $regning->hentUtskriftsdato()
                    ? $regning->hentUtskriftsdato()->format('d.m.Y')
                    : date_create()->format('d.m.Y'),
                'forfall'			=> $regning->hentForfall()->format('d.m.Y'),
                'girobeløp'			=> $this->app->kr(abs($regning->hentBeløp())),
                'utestående'		=> $this->app->kr($regning->hentUtestående()),
                'fradrag'			=> ($regning->hentBeløp() - $regning->hentUtestående()) ? $this->app->kr($regning->hentBeløp() - $regning->hentUtestående()) : '',
                'fbo'		        => $fbo,
                'efaktura'			=> $efaktura,
                'avtalegiroTilgjengelig' => (bool)$this->app->hentValg('avtalegiro'),
                'efakturaTilgjengelig' => (bool)$this->app->hentValg('efaktura'),
                'girotekst'			=> nl2br($this->app->hentValg('girotekst')),
                'bunntekst'			=> $this->app->hentValg('eposttekst'),
                'detaljer'			=> $detaljer,
                'urlBase'           => $this->app->http_host
            ];
            $antallLinjer = $regning->hentKrav()->hentAntall();
            $linjeNummer = 0;
            /** @var Krav $krav */
            foreach($regning->hentKrav() as $krav) {
                $linjeNummer++;
                /** @var Kravlinje $kravlinje */
                $kravlinje = $this->app->vis(Kravlinje::class, [
                    'tekst' => $krav->hentTekst(),
                    'beløp' => $this->app->kr($krav->hentBeløp()),
                    'linjeNummer' => $linjeNummer,
                    'antallLinjer' => $antallLinjer
                ]);
                $kravlinje->settMal('felles/html/regning/KravlinjeEpostfaktura.html');
                $detaljer->addItem($kravlinje);
            }
            $this->definerData($datasett);
        }

        return parent::forberedData();
    }
}