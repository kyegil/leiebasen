<?php

namespace Kyegil\Leiebasen\Visning\felles\fpdf;


use DateTime;
use Exception;
use Fpdf\Fpdf;
use Kyegil\Leiebasen\Modell\Leieforhold as LeieforholdModell;
use Kyegil\Leiebasen\Modell\Leieforhold\Purring as PurringModell;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning as RegningModell;

/**
 * Class Pdf
 *
 * Mal for Pdf utskrift
 *
 *  Ressurser:
 *      pdf \Fpdf\Fpdf
 *      regningsett \Kyegil\Leiebasen\Modell\Leieforhold\Regning[]|\Kyegil\Leiebasen\Modell\Leieforhold\Regningsett
 *      purringsett \Kyegil\Leiebasen\Modell\Leieforhold\Regning[]|\Kyegil\Leiebasen\Modell\Leieforhold\Regningsett
 *      leieforholdsett \Kyegil\Leiebasen\Modell\Leieforhold\Regning[]|\Kyegil\Leiebasen\Modell\Leieforhold\Regningsett
 *  Mulige variabler:
 *      innhold \Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray
 * @package Kyegil\Leiebasen\Visning\felles\fpdf
 */
class Pdf extends \Kyegil\Leiebasen\Visning
{
    /** @var string */
    protected $template = 'felles/fpdf/Pdf';

    /**
     * @return string
     */
    public function __toString(): string
    {
        $this->render();
        /**
         * Kun denne klassen skal kunne sendes ut som streng.
         * Aldri underklassene
         */
        if( static::class === self::class) {
            return $this->hentPdf()->Output('S', '', true);
        }
        else return '';
    }

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Pdf
    {
        if ($attributt == 'pdf') {
            return $this->settPdf($verdi);
        }
        if (static::class === self::class) {
            if(in_array($attributt, ['regningsett','purringsett','leieforholdsett'])) {
                if (!is_iterable($verdi)) {
                    throw new Exception( $attributt . ' er ikke gjentakende.');
                }
                return $this->settRessurs($attributt, $verdi);
            }
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Fpdf $pdf
     * @return $this
     */
    public function settPdf(Fpdf $pdf): Pdf
    {
        $this->settRessurs('pdf', $pdf);
        return $this;
    }

    /**
     * @return Fpdf
     */
    public function hentPdf(): Fpdf
    {
        $pdf = $this->hentRessurs('pdf');
        if (!is_a($pdf, Fpdf::class)) {
            $pdf = new \Fpdf\Fpdf();
            $this->settRessurs('pdf', $pdf);
        }
        return $pdf;
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): Pdf
    {
        if (static::class !== self::class) {
            return parent::forberedData();
        }

        /** @var Fpdf $fpdf */
        $pdf = $this->hentPdf();
        $sett  = [];
        $innhold = new PdfArray();
        $regningsett = $this->hentRessurs('regningsett') ?? [];
        $purringsett = $this->hentRessurs('purringsett') ?? [];
        $leieforholdsett = $this->hentRessurs('leieforholdsett') ?? [];

        foreach ($regningsett as $regning) {
            $sett[] = $regning;
        }
        foreach ($purringsett as $purring) {
            $sett[] = $purring;
        }
        foreach ($leieforholdsett as $leieforhold) {
            $sett[] = $leieforhold;
        }
        usort($sett, [$this, 'sorter']);
        foreach ($sett as $objekt) {
            if ($objekt instanceof RegningModell) {
                $visning = $objekt->hentBeløp() < 0 ? Kreditnota::class : Regning::class;
                $innhold->addItem($this->app->hentVisning($visning, [
                    'pdf' => $pdf,
                    'regning' => $objekt,
                ]));
            }

            if ($objekt instanceof PurringModell) {
                $innhold->addItem($this->app->hentVisning(Purring::class, [
                    'pdf' => $pdf,
                    'purring' => $objekt
                ]));
            }

            if ($objekt instanceof LeieforholdModell) {
                $innhold->addItem($this->app->hentVisning(StatusOversikt::class, [
                    'pdf' => $pdf,
                    'leieforhold' => $objekt,
                    'dato' => new DateTime()
                ]));
            }
        }

        $this->definerData([
            'innhold' => $innhold
        ]);

        return parent::forberedData();
    }

    /**
     * Sorteringslogikk for blanketter
     *
     * @param $a
     * @param $b
     * @return int
     */
    public static function sorter($a, $b): int
    {
        if ($a instanceof LeieforholdModell) {
            $leieforholdA = $a;
        }
        else if($a instanceof RegningModell || $a instanceof PurringModell) {
            $leieforholdA = $a->leieforhold;
        }
        else {
            return 0;
        }

        if ($b instanceof LeieforholdModell) {
            $leieforholdB = $b;
        }
        else if($b instanceof RegningModell || $b instanceof PurringModell) {
            $leieforholdB = $b->leieforhold;
        }
        else {
            return 0;
        }

        /**
         * Dersom regning til minst ett av leieforholdene skal sendes i posten,
         * så kan ikke disse sorteres på grunnlag av regningsadresse-leieobjektet
         */
        if(!$leieforholdA->regningTilObjekt || !$leieforholdB->regningTilObjekt) {
            if(!$leieforholdA->regningTilObjekt && !$leieforholdB->regningTilObjekt) {
                /** Begge skal sendes i posten, og kan sorteres på regningsmottaker */
                return intval(strval($leieforholdA->hentRegningsperson())) - intval(strval($leieforholdB->hentRegningsperson()));
            }
            /** Kun én skal sendes i posten, og denne sorterer først */
            return intval($leieforholdA->regningTilObjekt) - intval($leieforholdB->regningTilObjekt);
        }
        $rekkefølgeA = $leieforholdA->hentUtdelingsplassering();
        $rekkefølgeB = $leieforholdB->hentUtdelingsplassering();
        return $rekkefølgeA - $rekkefølgeB;
    }
}