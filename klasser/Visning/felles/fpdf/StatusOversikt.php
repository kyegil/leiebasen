<?php

namespace Kyegil\Leiebasen\Visning\felles\fpdf;

use DateTime;
use Exception;
use Fpdf\Fpdf;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Visning\felles\fpdf\purring\kravsett\Linje;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Adressefelt;

/**
 * Class StatusOversikt
 *
 * Mal for Pdf utskrift av purring
 *
 *  Ressurser:
 *
 *      pdf \Fpdf\Fpdf
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *      dato \DateTime
 *  Mulige variabler:
 *      $avsender
 *      $avsenderAdresse
 *      $avsenderBankkonto
 *      $avsenderEpost
 *      $avsenderGateadresse
 *      $avsenderHjemmeside
 *      $avsenderMobil
 *      $avsenderOrgNr
 *      $avsenderPostnr
 *      $avsenderPoststed
 *      $avsenderTelefax
 *      $avsenderTelefon
 *      $avtalegiroTilgjengelig boolean
 *      $blankettbeløp
 *      $blankettbeløpKroner
 *      $blankettbeløpØre
 *      $blankettBetalingsinfo
 *      $efakturaTilgjengelig boolean
 *      $fastKid
 *      $forfall
 *      $girobeløp
 *      $gironr
 *      $girotekst
 *      $kravsett \Kyegil\Leiebasen\Visning\felles\fpdf\Pdf|\Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray
 *      $leieforholdBeskrivelse
 *      $leieforholdId
 *      $mottakerAdresse
 *      $ocrKid
 *      $ocrKontonummer
 *      $ocrKontrollsiffer
 *      $statusDato
 *      $rammer boolean Viser rammer rundt felter i dokumentet
 *      $sisteInnbetalinger \Kyegil\Leiebasen\Visning\felles\fpdf\Pdf|\Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray
 *      $tidligereUbetalt \Kyegil\Leiebasen\Visning\felles\fpdf\Pdf|\Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray
 *      $utestående
 * @link http://www.fpdf.org/en/doc/index.php
 * @package Kyegil\Leiebasen\Visning\felles\fpdf
 */
class StatusOversikt extends Pdf
{
    /** @var string */
    protected $template = 'felles/fpdf/StatusOversikt';

    /**
     * @param string|array $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Pdf
    {
        if ($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        if ($attributt == 'dato' && $verdi instanceof DateTime) {
            return $this->settDato($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param DateTime $dato
     * @return $this
     */
    private function settDato(DateTime $dato): StatusOversikt
    {
        return $this->settRessurs('dato' , $dato);
    }

    /**
     * @param Leieforhold $leieforhold
     * @return $this
     */
    private function settLeieforhold(Leieforhold $leieforhold): StatusOversikt
    {
        return $this->settRessurs('leieforhold' , $leieforhold);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): StatusOversikt
    {
        /** @var Fpdf $pdf */
        $pdf = $this->hentRessurs('pdf');
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        /** @var DateTime $dato */
        $dato = $this->hentRessurs('dato') ?? new DateTime();
        $rammer = $this->hent('rammer') ?? false;
        $linjer = new PdfArray();
        $kravsett = $this->app->vis(purring\Kravsett::class, [
            'pdf' => $pdf,
            'linjer' => $linjer,
            'rammer' => $rammer,
            'utestående' => 0,
        ]);

        if ($leieforhold) {
            $utestående = 0;
            $antallLinjer = 0;
            /** @var Kravsett $uteståendeKrav */
            $uteståendeKrav = $leieforhold->hentUteståendeKrav()
                ->leggTilSortering('kravid')
                ->leggTilSortering('forfall');
            foreach ($uteståendeKrav as $krav) {
                if ($krav->kravdato->format('Y-m-d') <= $dato->format('Y-m-d')) {
                    if ($antallLinjer < 14) {
                        $linjer->addItem($this->app->vis(Linje::class, [
                            'pdf' => $pdf,
                            'krav' => $krav
                        ]));
                    }
                    $antallLinjer++;
                    $utestående += $krav->utestående;
                }
            }
            if ($antallLinjer >= 14) {
                $linjer->addItem($this->app->vis(Linje::class, [
                    'pdf' => $pdf,
                    'tekst' => 'Videre linjer er klippet',
                    'beløp' => '',
                    'utestående' => '...'
                ]));
            }
            $kravsett->sett('utestående', number_format($utestående, 2, ',', ' '));

            $blankettbeløp = strval($utestående);
            $blankettbeløpKroner = intval($blankettbeløp);
            $blankettbeløpØre = round(100 * ($blankettbeløp - $blankettbeløpKroner));
            $blankettbeløpØre = str_pad($blankettbeløpØre, 2, '0', STR_PAD_LEFT);

            $this->definerData([
                'blankettbeløp' => $blankettbeløp,
                'blankettbeløpKroner' => $blankettbeløpKroner,
                'blankettbeløpØre' => $blankettbeløpØre,
                'fastKid' => $leieforhold->hentKid(),
                'leieforholdBeskrivelse' => $leieforhold->hentBeskrivelse(),
                'leieforholdId' => $leieforhold->hentId(),
                'mottakerAdresse' => $leieforhold->navn . "\n"
                    . trim($this->app->vis(Adressefelt::class, ['leieforhold' => $leieforhold])->settMal('felles/html/leieforhold/Adressefelt.txt')),
                'ocrKid' => $leieforhold->hentKid(),
                'ocrKontrollsiffer' => $this->app->hentKidBestyrer()::kontrollsiffer($blankettbeløpKroner . $blankettbeløpØre),
                'statusDato' => $dato->format('d.m.Y'),
                'utestående' => $utestående,
            ]);
        }
        $this->definerData([
            'avsender' => $this->app->hentValg('utleier'),
            'avsenderAdresse' => implode("\n", [
                $this->app->hentValg('utleier'),
                $this->app->hentValg('adresse'),
                $this->app->hentValg('postnr') . ' ' . $this->app->hentValg('poststed')
            ]),
            'avsenderBankkonto' => $this->app->hentValg('bankkonto'),
            'avsenderEpost' => $this->app->hentValg('epost'),
            'avsenderGateadresse' => $this->app->hentValg('adresse'),
            'avsenderHjemmeside' => $this->app->hentValg('hjemmeside'),
            'avsenderMobil' => $this->app->hentValg('mobil'),
            'avsenderOrgNr' => $this->app->hentValg('orgnr'),
            'avsenderPostnr' => $this->app->hentValg('postnr'),
            'avsenderPoststed' => $this->app->hentValg('poststed'),
            'avsenderTelefax' => $this->app->hentValg('telefax'),
            'avsenderTelefon' => $this->app->hentValg('telefon'),
            'avtalegiroTilgjengelig' => boolval($this->app->hentValg('avtalegiro')),
            'blankettbeløp' => '',
            'blankettbeløpKroner' => '',
            'blankettbeløpØre' => '',
            'blankettBetalingsinfo' => '',
            'efakturaTilgjengelig' => boolval($this->app->hentValg('efaktura')),
            'fastKid' => '',
            'forfall' => '',
            'gironr' => '',
            'girotekst' => $this->app->hentValg('girotekst'),
            'kravsett' => $kravsett,
            'leieforholdBeskrivelse' => '',
            'leieforholdId' => '',
            'mottakerAdresse' => '',
            'ocrKid' => '',
            'ocrKontonummer' => $this->app->hentValg('bankkonto'),
            'ocrKontrollsiffer' => '',
            'rammer' => $rammer,
            'statusDato' => '',
            'utestående' => '',
            'utskriftsdato' => '',
        ]);
        return parent::forberedData();
    }
}