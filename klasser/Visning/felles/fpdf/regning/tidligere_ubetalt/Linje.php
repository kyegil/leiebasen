<?php

namespace Kyegil\Leiebasen\Visning\felles\fpdf\regning\tidligere_ubetalt;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Visning\felles\fpdf\Pdf;

/**
 * Class Linje
 *
 * Mal for Pdf utskrift av regning
 *
 *  Ressurser:
 *      pdf \Fpdf\Fpdf
 *      krav \Kyegil\Leiebasen\Modell\Leieforhold\Krav
 *  Mulige variabler:
 *      $kravId
 *      $tekst
 *      $beløp
 *      $utestående
 * @see http://www.fpdf.org/en/doc/index.php
 * @package Kyegil\Leiebasen\Visning\felles\fpdf\regning
 */
class Linje extends Pdf
{
    /** @var string */
    protected $template = 'felles/fpdf/regning/tidligere_ubetalt/Linje';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Pdf
    {
        if ($attributt == 'krav') {
            return $this->settKrav($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Krav $regning
     * @return Linje
     */
    private function settKrav(Krav $krav): Linje
    {
        return $this->settRessurs('krav' , $krav);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): Pdf
    {
        /** @var Krav|null $krav */
        $krav = $this->hentRessurs('krav');

        $this->definerData([
            'kravId' => $krav ? $krav->id : '',
            'tekst' => $krav ? $krav->tekst : '',
            'beløp' => number_format($krav ? $krav->beløp : 0, 2, ',', ' '),
            'utestående' => number_format($krav ? $krav->utestående : 0, 2, ',', ' '),
        ]);
        return parent::forberedData();
    }
}