<?php

namespace Kyegil\Leiebasen\Visning\felles\fpdf\regning;

use Exception;
use Fpdf\Fpdf;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan\Avdrag;
use Kyegil\Leiebasen\Visning\felles\fpdf\Pdf;

/**
 * Class Kravsett
 *
 * Mal for Pdf utskrift av avdrag i betalingsplan
 *
 *  Ressurser:
 *      pdf \Fpdf\Fpdf
 *      avdrag \Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan\Avdrag
 *  Mulige variabler:
 *      $vis boolean Sann for å vise feltet
 *      $avdragDato
 *      $beløp
 *      $samletGjeld
 *      $restbeløp
 *      $rammer boolean Viser rammer rundt felter i dokumentet
 * @link http://www.fpdf.org/en/doc/index.php
 * @package Kyegil\Leiebasen\Visning\felles\fpdf\regning
 */
class BetalingsplanAvdrag extends Pdf
{
    /** @var string */
    protected $template = 'felles/fpdf/regning/BetalingsplanAvdrag';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): BetalingsplanAvdrag
    {
        if ($attributt == 'avdrag') {
            return $this->settAvdrag($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Avdrag $avdrag
     * @return $this
     */
    private function settAvdrag(Avdrag $avdrag): BetalingsplanAvdrag
    {
        return $this->settRessurs('avdrag' , $avdrag);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): BetalingsplanAvdrag
    {
        /** @var Fpdf $pdf */
        $pdf = $this->hentRessurs('pdf');
        /** @var Avdrag|null $avdrag */
        $avdrag = $this->hentRessurs('avdrag');
        $rammer = $this->hent('rammer') ?? false;

        if ($avdrag) {
            $this->definerData([
                'vis' => true,
                'avdragDato' => $avdrag->hentForfallsdato()->format('d.m.Y'),
                'beløp' => number_format($avdrag->hentBeløp(), 2, ',', ' '),
                'rammer' => $rammer
            ]);
        }
        $this->definerData([
            'pdf' => $pdf,
            'vis' => false,
            'avdragDato' => '',
            'beløp' => '',
            'samletGjeld' => '',
            'restbeløp' => '',
            'rammer' => $rammer
        ]);
        return parent::forberedData();
    }
}