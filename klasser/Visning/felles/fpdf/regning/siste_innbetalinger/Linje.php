<?php

namespace Kyegil\Leiebasen\Visning\felles\fpdf\regning\siste_innbetalinger;

use Exception;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Visning\felles\fpdf\Pdf;

/**
 * Class Linje
 *
 * Mal for Pdf utskrift av siste innbetalinger på regning
 *
 *  Ressurser:
 *      pdf \Fpdf\Fpdf
 *      betaling \Kyegil\Leiebasen\Modell\Innbetaling
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *  Mulige variabler:
 *      $dato
 *      $betaler
 *      $beløp
 *      $referanse
 * @see http://www.fpdf.org/en/doc/index.php
 * @package Kyegil\Leiebasen\Visning\felles\fpdf\regning
 */
class Linje extends Pdf
{
    /** @var string */
    protected $template = 'felles/fpdf/regning/siste_innbetalinger/Linje';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Pdf
    {
        if ($attributt == 'betaling') {
            return $this->settBetaling($verdi);
        }
        if ($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param Innbetaling $betaling
     * @return $this
     */
    private function settBetaling(Innbetaling $betaling): Linje
    {
        return $this->settRessurs('betaling' , $betaling);
    }

    /**
     * @param Leieforhold $leieforhold
     * @return $this
     */
    private function settLeieforhold(Leieforhold $leieforhold): Linje
    {
        return $this->settRessurs('leieforhold' , $leieforhold);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): Pdf
    {
        /** @var Innbetaling|null $betaling */
        $betaling = $this->hentRessurs('betaling');
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        $dato = $betaling->hentDato();

        if ($betaling && $leieforhold) {
            $this->definerData([
                'dato' => $dato ? $dato->format('d.m.Y') : '',
                'betaler' => $betaling->betaler,
                'beløp' => number_format($betaling->hentBeløpTilLeieforhold($leieforhold), 2, ',', ' '),
                'referanse' => $betaling->ref,
            ]);
        }
        else {
            $this->definerData([
                'dato' => '',
                'betaler' => '',
                'beløp' => 0,
                'referanse' => '',
            ]);
        }
        return parent::forberedData();
    }
}