<?php

namespace Kyegil\Leiebasen\Visning\felles\fpdf\regning;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Fpdf\Fpdf;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\felles\fpdf\Pdf;
use Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray;
use Kyegil\Leiebasen\Visning\felles\fpdf\regning\siste_innbetalinger\Linje;

/**
 * Class SisteInnbetalinger
 *
 * Mal for Pdf utskrift av siste-innbetalinger-felt i regning
 *
 *  Ressurser:
 *      pdf \Fpdf\Fpdf
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *      dato \DateTimeInterface
 *      antall integer
 *  Mulige variabler:
 *      $linjer
 *      $vis boolean
 *      $rammer boolean Viser rammer rundt felter i dokumentet
 * @link http://www.fpdf.org/en/doc/index.php
 * @package Kyegil\Leiebasen\Visning\felles\fpdf\regning
 */
class SisteInnbetalinger extends Pdf
{
    /** @var string */
    protected $template = 'felles/fpdf/regning/SisteInnbetalinger';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): SisteInnbetalinger
    {
        if ($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        if ($attributt == 'dato') {
            return $this->settDato($verdi);
        }
        if ($attributt == 'antall') {
            return $this->settRessurs('antall', $verdi);
        }
        return parent::sett($attributt, $verdi);
    }


    /**
     * @param Leieforhold $leieforhold
     * @return $this
     */
    private function settLeieforhold(Leieforhold $leieforhold): SisteInnbetalinger
    {
        return $this->settRessurs('leieforhold' , $leieforhold);
    }

    /**
     * @param DateTimeInterface|null $dato
     * @return $this
     */
    private function settDato(?DateTimeInterface $dato): SisteInnbetalinger
    {
        return $this->settRessurs('dato' , $dato);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): SisteInnbetalinger
    {
        /** @var Fpdf $pdf */
        $pdf = $this->hentRessurs('pdf');
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        /** @var DateTimeInterface $dato */
        $dato = $this->hentRessurs('dato') ?? new DateTimeImmutable();
        $antall = intval($this->hentRessurs('antall') ?? 3);
        $rammer = $this->hent('rammer') ?? false;
        $linjer = new PdfArray();
        $vis = false;

        if ($leieforhold) {
            $sisteInnbetalinger = $this->app->hentSamling(Innbetaling::class)
                ->leggTilFilter(['leieforhold' => $leieforhold->hentId()])
                ->leggTilFilter(['dato <=' => $dato->format('Y-m-d')])
                ->leggTilFilter(['beløp >' => 0])
                ->leggTilFilter(['konto <>' => '0'])
                ->leggTilSortering('dato', true)
                ->begrens($antall)
            ;
            /** @var Innbetaling $betaling */
            foreach($sisteInnbetalinger as $betaling) {
                $vis = true;
                $linjer->addItem($this->app->vis(Linje::class, [
                    'pdf' => $pdf,
                    'betaling' => $betaling,
                    'leieforhold' => $leieforhold
                ]));
            }

        }
        $this->definerData([
            'pdf' => $pdf,
            'linjer' => $linjer,
            'vis' => $vis,
            'rammer' => $rammer
        ]);
        return parent::forberedData();
    }
}