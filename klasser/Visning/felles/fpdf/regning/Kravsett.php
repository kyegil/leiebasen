<?php

namespace Kyegil\Leiebasen\Visning\felles\fpdf\regning;

use Exception;
use Fpdf\Fpdf;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning as RegningModell;
use Kyegil\Leiebasen\Visning\felles\fpdf\Pdf;
use Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray;
use Kyegil\Leiebasen\Visning\felles\fpdf\regning\kravsett\Linje;

/**
 * Class Kravsett
 *
 * Mal for Pdf utskrift av regning
 *
 *  Ressurser:
 *      pdf \Fpdf\Fpdf
 *      regning \Kyegil\Leiebasen\Modell\Leieforhold\Regning
 *  Mulige variabler:
 *      $girobeløp
 *      $innbetalt
 *      $utestående
 *      $linjer
 *      $rammer boolean Viser rammer rundt felter i dokumentet
 * @link http://www.fpdf.org/en/doc/index.php
 * @package Kyegil\Leiebasen\Visning\felles\fpdf\regning
 */
class Kravsett extends Pdf
{
    /** @var string */
    protected $template = 'felles/fpdf/regning/Kravsett';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Kravsett
    {
        if ($attributt == 'regning') {
            return $this->settRegning($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param RegningModell $regning
     * @return $this
     */
    private function settRegning(RegningModell $regning): Kravsett
    {
        return $this->settRessurs('regning' , $regning);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): Kravsett
    {
        /** @var Fpdf $pdf */
        $pdf = $this->hentRessurs('pdf');
        /** @var RegningModell|null $regning */
        $regning = $this->hentRessurs('regning');
        $rammer = $this->hent('rammer') ?? false;
        $linjer = new PdfArray();
        $girobeløp = 0;
        $utestående = 0;
        $innbetalt = 0;

        if ($regning) {
            $girobeløp = $regning->hentBeløp();
            $utestående = $regning->hentUtestående();
            $innbetalt = $girobeløp - $utestående;
            foreach($regning->hentKrav() as $krav) {
                $linjer->addItem($this->app->vis(Linje::class, [
                    'pdf' => $pdf,
                    'krav' => $krav
                ]));
            }

        }
        $this->definerData([
            'pdf' => $pdf,
            'linjer' => $linjer,
            'girobeløp' => number_format($girobeløp, 2, ',', ' '),
            'innbetalt' => $innbetalt != 0 ? number_format($innbetalt, 2, ',', ' ') : '',
            'utestående' => number_format($utestående, 2, ',', ' '),
            'rammer' => $rammer
        ]);
        return parent::forberedData();
    }
}