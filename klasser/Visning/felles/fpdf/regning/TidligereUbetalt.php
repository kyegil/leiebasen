<?php

namespace Kyegil\Leiebasen\Visning\felles\fpdf\regning;

use DateTime;
use Exception;
use Fpdf\Fpdf;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\felles\fpdf\Pdf;
use Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray;
use Kyegil\Leiebasen\Visning\felles\fpdf\regning\tidligere_ubetalt\Linje;

/**
 * Class TidligereUbetalt
 *
 * Mal for Pdf utskrift av annet-utestående-felt i regning
 *
 *  Ressurser:
 *      pdf \Fpdf\Fpdf
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *      dato \DateTime
 *  Mulige variabler:
 *      $linjer
 *      $utestående
 *      $vis boolean
 *      $rammer boolean Viser rammer rundt felter i dokumentet
 * @link http://www.fpdf.org/en/doc/index.php
 * @package Kyegil\Leiebasen\Visning\felles\fpdf\regning
 */
class TidligereUbetalt extends Pdf
{
    /** @var string */
    protected $template = 'felles/fpdf/regning/TidligereUbetalt';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): TidligereUbetalt
    {
        if ($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        if ($attributt == 'dato') {
            return $this->settDato($verdi);
        }
        return parent::sett($attributt, $verdi);
    }


    /**
     * @param Leieforhold $leieforhold
     * @return $this
     */
    private function settLeieforhold(Leieforhold $leieforhold): TidligereUbetalt
    {
        return $this->settRessurs('leieforhold' , $leieforhold);
    }

    /**
     * @param DateTime|null $dato
     * @return $this
     */
    private function settDato(?DateTime $dato): TidligereUbetalt
    {
        return $this->settRessurs('dato' , $dato);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): TidligereUbetalt
    {
        /** @var Fpdf $pdf */
        $pdf = $this->hentRessurs('pdf');
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        /** @var DateTime $dato */
        $dato = $this->hentRessurs('dato') ?? new DateTime();
        $rammer = $this->hent('rammer') ?? false;
        $linjer = new PdfArray();
        $utestående = 0;
        $antallLinjer = 0;

        if ($leieforhold) {
            foreach ($leieforhold->hentUteståendeKrav() as $krav) {
                if ($krav->forfall && $krav->forfall < $dato) {
                    $utestående += $krav->utestående;
                    $linjer->addItem($this->app->vis(Linje::class, [
                        'pdf' => $pdf,
                        'krav' => $krav,
                    ]));
                    $antallLinjer++;
                }
            }
        }
        $this->definerData([
            'linjer' => $antallLinjer && ($antallLinjer <= 8) ? $linjer : '',
            'vis' => $utestående > 0,
            'utestående' => number_format($utestående, 2, ',', ' '),
            'rammer' => $rammer
        ]);
        return parent::forberedData();
    }
}