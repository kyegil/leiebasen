<?php

namespace Kyegil\Leiebasen\Visning\felles\fpdf\purring\kravsett;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Visning\felles\fpdf\Pdf;

/**
 * Class Linje
 *
 * Mal for Pdf utskrift av regning
 *
 *  Ressurser:
 *      pdf \Fpdf\Fpdf
 *      krav \Kyegil\Leiebasen\Modell\Leieforhold\Krav
 *  Mulige variabler:
 *      $kravId
 *      $tekst
 *      $beløp
 *      $forfallsdato
 *      $utestående
 * @see http://www.fpdf.org/en/doc/index.php
 * @package Kyegil\Leiebasen\Visning\felles\fpdf\regning\kravsett
 */
class Linje extends \Kyegil\Leiebasen\Visning\felles\fpdf\regning\kravsett\Linje
{
    /** @var string */
    protected $template = 'felles/fpdf/purring/kravsett/Linje';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): Linje
    {
        /** @var Krav|null $krav */
        $krav = $this->hentRessurs('krav');


        $this->definerData([
            'forfallsdato' => $krav && $krav->forfall ? $krav->forfall->format('d.m.Y') : '',
        ]);
        return parent::forberedData();
    }
}