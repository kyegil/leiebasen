<?php

namespace Kyegil\Leiebasen\Visning\felles\fpdf\purring;

use Exception;
use Fpdf\Fpdf;
use Kyegil\Leiebasen\Modell\Leieforhold\Purring as PurringModell;
use Kyegil\Leiebasen\Visning\felles\fpdf\Pdf;
use Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray;
use Kyegil\Leiebasen\Visning\felles\fpdf\purring\kravsett\Linje;

/**
 * Class Kravsett
 *
 * Mal for Pdf utskrift av purring
 *
 *  Ressurser:
 *      pdf \Fpdf\Fpdf
 *      purring \Kyegil\Leiebasen\Modell\Leieforhold\Purring
 *      purregebyr float
 *  Mulige variabler:
 *      $utestående
 *      $linjer
 *      $rammer boolean Viser rammer rundt felter i dokumentet
 * @link http://www.fpdf.org/en/doc/index.php
 * @package Kyegil\Leiebasen\Visning\felles\fpdf\purring
 */
class Kravsett extends Pdf
{
    /** @var string */
    protected $template = 'felles/fpdf/purring/Kravsett';

    /**
     * @param string|array $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Kravsett
    {
        if ($attributt == 'purring') {
            return $this->settPurring($verdi);
        }
        if ($attributt == 'purregebyr') {
            return $this->settRessurs('purregebyr', $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param PurringModell $purring
     * @return $this
     */
    private function settPurring(PurringModell $purring): Kravsett
    {
        return $this->settRessurs('purring' , $purring);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): Kravsett
    {
        /** @var Fpdf $pdf */
        $pdf = $this->hentRessurs('pdf');
        /** @var PurringModell|null $purring */
        $purring = $this->hentRessurs('purring');
        $purregebyr = floatval($this->hentRessurs('purregebyr') ?? 0);
        $rammer = $this->hent('rammer') ?? false;
        $linjer = new PdfArray();
        $utestående = 0;

        if ($purring) {
            $utestående = $purring->hentUtestående() + $purregebyr;
            foreach($purring->hentKravlinker() as $kravlink) {
                $linjer->addItem($this->app->vis(Linje::class, [
                    'pdf' => $pdf,
                    'krav' => $kravlink->krav
                ]));
            }

            if ($purregebyr != 0) {
                $linjer->addItem($this->app->vis(Linje::class, [
                    'pdf' => $pdf,
                    'tekst' => 'Gebyr for denne påminnelsen',
                    'beløp' => number_format($purregebyr, 2, ',', ' '),
                    'forfallsdato' => $purring->hentPurreforfall() ? $purring->hentPurreforfall()->format('d.m.Y') : '',
                    'utestående' => number_format($purregebyr, 2, ',', ' '),
                ]));
            }

        }
        $this->definerData([
            'pdf' => $pdf,
            'linjer' => $linjer,
            'utestående' => number_format($utestående, 2, ',', ' '),
            'rammer' => $rammer
        ]);
        return parent::forberedData();
    }
}