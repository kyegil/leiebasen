<?php

namespace Kyegil\Leiebasen\Visning\felles\fpdf;

use Exception;
use Fpdf\Fpdf;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan\Avdrag;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning as RegningModell;
use Kyegil\Leiebasen\Visning\felles\fpdf\regning\BetalingsplanAvdrag;
use Kyegil\Leiebasen\Visning\felles\fpdf\regning\Kravsett;
use Kyegil\Leiebasen\Visning\felles\fpdf\regning\SisteInnbetalinger;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Adressefelt;

/**
 * Class Regning
 *
 * Mal for Pdf utskrift av regning
 *
 *  Ressurser:
 *
 *      pdf \Fpdf\Fpdf
 *      regning \Kyegil\Leiebasen\Modell\Leieforhold\Regning
 *  Mulige variabler:
 *      $avsender
 *      $avsenderAdresse
 *      $avsenderGateadresse
 *      $avsenderPostnr
 *      $avsenderPoststed
 *      $avsenderOrgNr
 *      $avsenderTelefon
 *      $avsenderTelefax
 *      $avsenderMobil
 *      $avsenderEpost
 *      $avsenderHjemmeside
 *      $avtalegiro boolean
 *      $efaktura boolean
 *      $mottakerAdresse
 *      $leieforholdId
 *      $leieforholdBeskrivelse
 *      $fastKid
 *      $kid
 *      $gironr
 *      $utskriftsdato
 *      $forfall
 *      $girobeløp
 *      $utestående
 *      $girotekst
 *      $bankkonto
 *      $blankettBetalingsinfo
 *      $blankettbeløp
 *      $blankettbeløpKroner
 *      $blankettbeløpØre
 *      $ocrKid
 *      $ocrKontonummer
 *      $kontrollsiffer
 *      $kravsett \Kyegil\Leiebasen\Visning\felles\fpdf\Pdf|\Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray
 *      $tidligereUbetalt \Kyegil\Leiebasen\Visning\felles\fpdf\Pdf|\Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray
 *      $sisteInnbetalinger \Kyegil\Leiebasen\Visning\felles\fpdf\Pdf|\Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray
 *      $rammer boolean Viser rammer rundt felter i dokumentet
 * @link http://www.fpdf.org/en/doc/index.php
 * @package Kyegil\Leiebasen\Visning\felles\fpdf
 */
class Regning extends Pdf
{
    /** @var string */
    protected $template = 'felles/fpdf/Regning';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Pdf
    {
        if ($attributt == 'regning') {
            return $this->settRegning($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param RegningModell $regning
     * @return $this
     */
    private function settRegning(RegningModell $regning): Regning
    {
        return $this->settRessurs('regning' , $regning);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): Pdf
    {
        /** @var Fpdf $pdf */
        $pdf = $this->hentRessurs('pdf');
        /** @var RegningModell|null $regning */
        $regning = $this->hentRessurs('regning');
        $kravsett = '';
        $tidligereUbetalt = '';
        $sisteInnbetalinger = '';
        $rammer = $this->hent('rammer') ?? false;

        if ($regning) {
            $leieforhold = $regning->leieforhold;
            $utskriftsdato = $regning->hentUtskriftsdato();
            $forfallsdato = $regning->hentForfall();
            $blankettbeløp = strval($regning->hentUtestående());
            $fbo = $this->app->hentValg('avtalegiro') && $leieforhold->hentFbo();
            $avtaleGiro = false;
            if ($fbo) {
                $nesteTrekkForsendelse = $this->app->hentNetsProsessor()->nesteFboTrekkravForsendelse();
                $avtaleGiro = !$regning->hentFboOppdragsfrist() || $regning->hentFboOppdragsfrist() > $nesteTrekkForsendelse;
            }

            /** @var Avdrag|null $nedbetalingsAvdrag */
            $nedbetalingsAvdrag = null;
            $betalingsplan = $leieforhold->hentBetalingsplan();
            if ($betalingsplan && !$betalingsplan->aktiv) {
                $betalingsplan = null;
            }
            /*
             * Avdrag i betalingsplan legges til dersom de har samme forfallsdato som regningen,
             * men ikke dersom regningen skal sendes med AvtaleGiro (leietaker har fbo-avtale),
             * og avdraget ikke skal bruke AvtaleGiro
             */
            if ($betalingsplan && !($fbo && $betalingsplan->utenomAvtalegiro)) {
                $nedbetalingsAvdrag = $betalingsplan->hentAvdragForDato($forfallsdato);
                if ($nedbetalingsAvdrag) {
                    $blankettbeløp += $nedbetalingsAvdrag->hentBeløp();
                }
            }

            $blankettbeløpKroner = intval($blankettbeløp);
            $blankettbeløpØre = round(100 * ($blankettbeløp - $blankettbeløpKroner));
            $blankettbeløpØre = str_pad($blankettbeløpØre, 2, '0', STR_PAD_LEFT);

            /** @var Kravsett $kravsett */
            $kravsett = $this->app->vis(Kravsett::class, [
                'pdf' => $pdf,
                'regning' => $regning,
                'rammer' => $rammer
            ]);

            if ($betalingsplan && $nedbetalingsAvdrag) {
                /** @var BetalingsplanAvdrag $betalingsplanAvdrag */
                $betalingsplanAvdrag = $this->app->vis(BetalingsplanAvdrag::class, [
                    'pdf' => $pdf,
                    'avdrag' => $nedbetalingsAvdrag,
                    'samletGjeld' => number_format($betalingsplan->hentUtestående(), 2, ',', ' '),
                    'restbeløp' => number_format($betalingsplan->hentUtestående() - $nedbetalingsAvdrag->hentUtestående(), 2, ',', ' '),
                    'rammer' => $rammer,
                    'vis' => true
                ]);
            }
            else {
                $betalingsplanAvdrag = '';
            }

//            $tidligereUbetalt = $this->app->vis(TidligereUbetalt::class, [
//                'pdf' => $pdf,
//                'leieforhold' => $leieforhold,
//                'dato' => $utskriftsdato,
//                'rammer' => $rammer
//            ]);

            /** @var SisteInnbetalinger $sisteInnbetalinger */
            $sisteInnbetalinger = $this->app->vis(SisteInnbetalinger::class, [
                'pdf' => $pdf,
                'leieforhold' => $leieforhold,
                'dato' => $utskriftsdato,
                'rammer' => $rammer
            ]);

            $this->definerData([
                'mottakerAdresse' => $leieforhold->navn . "\n"
                    . trim($this->app->vis(Adressefelt::class, ['leieforhold' => $leieforhold])->settMal('felles/html/leieforhold/Adressefelt.txt')),
                'leieforholdId' => $leieforhold->hentId(),
                'leieforholdBeskrivelse' => $leieforhold->hentBeskrivelse(),
                'fastKid' => $leieforhold->hentKid(),
                'kid' => $regning->hentKid(),
                'gironr' => $regning->hentId(),
                'utskriftsdato' => $utskriftsdato ? $utskriftsdato->format('d.m.Y') : '',
                'forfall' => $forfallsdato ? $forfallsdato->format('d.m.Y') : '',
                'girobeløp' => $regning->hentBeløp(),
                'utestående' => $regning->hentUtestående(),
                'blankettbeløp' => $blankettbeløp,
                'blankettbeløpKroner' => $blankettbeløpKroner,
                'blankettbeløpØre' => $blankettbeløpØre,
                'blankettBetalingsinfo' => $avtaleGiro ? "Du trenger ikke betale denne giroen.\nBeløpet trekkes fra kontoen din med avtalegiro på forfallsdato." : '',
                'ocrKid' => $avtaleGiro ? 'Skal ikke betales manuelt.' : $regning->hentKid(),
                'ocrKontonummer' => $avtaleGiro ? 'Trekkes med AvtaleGiro.' : $this->app->hentValg('bankkonto'),
                'kontrollsiffer' => $avtaleGiro ? '' : $this->app->hentKidBestyrer()::kontrollsiffer($blankettbeløpKroner . $blankettbeløpØre)
            ]);
        }
        $this->definerData([
            'avsender' => $this->app->hentValg('utleier'),
            'avsenderAdresse' => implode("\n", [
                $this->app->hentValg('utleier'),
                $this->app->hentValg('adresse'),
                $this->app->hentValg('postnr') . ' ' . $this->app->hentValg('poststed')
            ]),
            'avsenderGateadresse' => $this->app->hentValg('adresse'),
            'avsenderPostnr' => $this->app->hentValg('postnr'),
            'avsenderPoststed' => $this->app->hentValg('poststed'),
            'avsenderOrgNr' => $this->app->hentValg('orgnr'),
            'avsenderTelefon' => $this->app->hentValg('telefon'),
            'avsenderTelefax' => $this->app->hentValg('telefax'),
            'avsenderMobil' => $this->app->hentValg('mobil'),
            'avsenderEpost' => $this->app->hentValg('epost'),
            'avsenderHjemmeside' => $this->app->hentValg('hjemmeside'),
            'bankkonto' => $this->app->hentValg('bankkonto'),
            'girotekst' => $this->app->hentValg('girotekst'),
            'efaktura' => boolval($this->app->hentValg('efaktura')),
            'avtalegiro' => boolval($this->app->hentValg('avtalegiro')),
            'mottakerAdresse' => '',
            'leieforholdId' => '',
            'leieforholdBeskrivelse' => '',
            'fastKid' => '',
            'kid' => '',
            'gironr' => '',
            'utskriftsdato' => '',
            'forfall' => '',
            'girobeløp' => '',
            'utestående' => '',
            'blankettBetalingsinfo' => '',
            'blankettbeløp' => '',
            'blankettbeløpKroner' => '',
            'blankettbeløpØre' => '',
            'ocrKid' => '',
            'kontrollsiffer' => '',
            'kravsett' => $kravsett,
            'betalingsplanAvdrag' => $betalingsplanAvdrag,
            'sisteInnbetalinger' => $sisteInnbetalinger,
            'tidligereUbetalt' => $tidligereUbetalt,
            'rammer' => $rammer
        ]);
        return parent::forberedData();
    }
}