<?php

namespace Kyegil\Leiebasen\Visning\felles\fpdf;

use Exception;
use Fpdf\Fpdf;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning as RegningModell;
use Kyegil\Leiebasen\Visning\felles\fpdf\kreditnota\Kravsett;

/**
 * Class Kreditnota
 *
 * Mal for Pdf utskrift av kreditnota
 *
 *  Ressurser:
 *
 *      pdf \Fpdf\Fpdf
 *      regning \Kyegil\Leiebasen\Modell\Leieforhold\Regning
 *  Mulige variabler:
 *      $avsender
 *      $avsenderAdresse
 *      $avsenderGateadresse
 *      $avsenderPostnr
 *      $avsenderPoststed
 *      $avsenderOrgNr
 *      $avsenderTelefon
 *      $avsenderTelefax
 *      $avsenderMobil
 *      $avsenderEpost
 *      $avsenderHjemmeside
 *      $avtalegiro boolean
 *      $efaktura boolean
 *      $mottakerAdresse
 *      $leieforholdId
 *      $leieforholdBeskrivelse
 *      $fastKid
 *      $gironr
 *      $utskriftsdato
 *      $girobeløp
 *      $girotekst
 *      $bankkonto
 *      $rammer boolean Viser rammer rundt felter i dokumentet
 * @link http://www.fpdf.org/en/doc/index.php
 * @package Kyegil\Leiebasen\Visning\felles\fpdf
 */
class Kreditnota extends Regning
{
    /** @var string */
    protected $template = 'felles/fpdf/Kreditnota';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): Pdf
    {
        /** @var Fpdf $pdf */
        $pdf = $this->hentRessurs('pdf');
        /** @var RegningModell|null $regning */
        $regning = $this->hentRessurs('regning');
        $rammer = $this->hent('rammer') ?? false;

        if ($regning) {
            $kravsett = $this->app->vis(Kravsett::class, [
                'pdf' => $pdf,
                'regning' => $regning,
                'rammer' => $rammer
            ]);
            $this->definerData([
                'kravsett' => $kravsett
            ]);
        }
        return parent::forberedData();
    }
}