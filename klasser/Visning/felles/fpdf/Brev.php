<?php

namespace Kyegil\Leiebasen\Visning\felles\fpdf;

use DateTime;
use Exception;
use Fpdf\Fpdf;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Adressefelt;

/**
 * Class Brev
 *
 * Mal for Pdf brev med brevhode
 *
 *  Ressurser:
 *
 *      pdf \Fpdf\Fpdf
 *      leieforhold \Kyegil\Leiebasen\Modell\Leieforhold
 *      dato \DateTime
 *  Mulige variabler:
 *      $avsender
 *      $avsenderAdresse
 *      $avsenderBankkonto
 *      $avsenderEpost
 *      $avsenderGateadresse
 *      $avsenderHjemmeside
 *      $avsenderMobil
 *      $avsenderOrgNr
 *      $avsenderPostnr
 *      $avsenderPoststed
 *      $avsenderTelefax
 *      $avsenderTelefon
 *      $leieforholdId
 *      $mottakerAdresse
 *      $rammer boolean Viser rammer rundt felter i dokumentet
 * @link http://www.fpdf.org/en/doc/index.php
 * @package Kyegil\Leiebasen\Visning\felles\fpdf
 */
class Brev extends Pdf
{
    /** @var string */
    protected $template = 'felles/fpdf/Brev';

    /**
     * @param string|array $attributt
     * @param mixed $verdi
     * @return Brev
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Brev
    {
        if ($attributt == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        if ($attributt == 'dato' && $verdi instanceof DateTime) {
            return $this->settDato($verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param DateTime $dato
     * @return Brev
     */
    private function settDato(DateTime $dato): Brev
    {
        return $this->settRessurs('dato' , $dato);
    }

    /**
     * @param Leieforhold $leieforhold
     * @return Brev
     */
    private function settLeieforhold(Leieforhold $leieforhold): Brev
    {
        return $this->settRessurs('leieforhold' , $leieforhold);
    }

    /**
     * @return Brev
     * @throws Exception
     */
    protected function forberedData(): Brev
    {
        /** @var Fpdf $pdf */
        $pdf = $this->hentRessurs('pdf');
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentRessurs('leieforhold');
        $rammer = $this->hent('rammer') ?? false;
        if ($leieforhold) {
            $this->definerData([
                'leieforholdId' => $leieforhold->hentId(),
                'mottakerAdresse' => $leieforhold->navn . "\n"
                    . trim($this->app->vis(Adressefelt::class, ['leieforhold' => $leieforhold])->settMal('felles/html/leieforhold/Adressefelt.txt')),
            ]);
        }
        $this->definerData([
            'rammer' => $rammer,
            'avsender' => $this->app->hentValg('utleier'),
            'avsenderAdresse' => implode("\n", [
                $this->app->hentValg('utleier'),
                $this->app->hentValg('adresse'),
                $this->app->hentValg('postnr') . ' ' . $this->app->hentValg('poststed')
            ]),
            'avsenderBankkonto' => $this->app->hentValg('bankkonto'),
            'avsenderEpost' => $this->app->hentValg('epost'),
            'avsenderGateadresse' => $this->app->hentValg('adresse'),
            'avsenderHjemmeside' => $this->app->hentValg('hjemmeside'),
            'avsenderMobil' => $this->app->hentValg('mobil'),
            'avsenderOrgNr' => $this->app->hentValg('orgnr'),
            'avsenderPostnr' => $this->app->hentValg('postnr'),
            'avsenderPoststed' => $this->app->hentValg('poststed'),
            'avsenderTelefax' => $this->app->hentValg('telefax'),
            'avsenderTelefon' => $this->app->hentValg('telefon'),
            'leieforholdId' => '',
            'mottakerAdresse' => '',
        ]);
        return parent::forberedData();
    }
}