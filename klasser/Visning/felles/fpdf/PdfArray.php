<?php

namespace Kyegil\Leiebasen\Visning\felles\fpdf;

use Exception;

/**
 * Class PdfArray
 *
 * @package Kyegil\Leiebasen\Visning\felles\fpdf
 */
class PdfArray extends \Kyegil\ViewRenderer\ViewArray
{
    /**
     * @return string
     * @throws Exception
     */
    public function render(): string
    {
        $result = [];
        foreach ($this->getItems() as $item) {
            if ($item instanceof Pdf || $item instanceof PdfArray) {
                $item->render();
            }
            else {
                $result[] = strval($item);
            }
        }
        return implode('', $result);
    }
}