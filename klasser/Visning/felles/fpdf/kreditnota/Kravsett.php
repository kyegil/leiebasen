<?php

namespace Kyegil\Leiebasen\Visning\felles\fpdf\kreditnota;

use Exception;
use Fpdf\Fpdf;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning as RegningModell;
use Kyegil\Leiebasen\Visning\felles\fpdf\Pdf;
use Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray;
use Kyegil\Leiebasen\Visning\felles\fpdf\regning\kravsett\Linje;

/**
 * Class Kravsett
 *
 * Mal for Pdf utskrift av regning
 *
 *  Ressurser:
 *      pdf \Fpdf\Fpdf
 *      regning \Kyegil\Leiebasen\Modell\Leieforhold\Regning
 *  Mulige variabler:
 *      $girobeløp
 *      $linjer
 *      $rammer boolean Viser rammer rundt felter i dokumentet
 * @link http://www.fpdf.org/en/doc/index.php
 * @package Kyegil\Leiebasen\Visning\felles\fpdf\kreditnota
 */
class Kravsett extends \Kyegil\Leiebasen\Visning\felles\fpdf\regning\Kravsett
{
    /** @var string */
    protected $template = 'felles/fpdf/kreditnota/Kravsett';

    /**
     * @param RegningModell $regning
     * @return $this
     */
    private function settRegning(RegningModell $regning): Kravsett
    {
        return $this->settRessurs('regning' , $regning);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): Kravsett
    {
        /** @var RegningModell|null $regning */
        $regning = $this->hentRessurs('regning');
        $rammer = $this->hent('rammer') ?? false;
        $girobeløp = 0;

        if ($regning) {
            $girobeløp = -$regning->hentBeløp();
        }
        $this->definerData([
            'girobeløp' => number_format($girobeløp, 2, ',', ' '),
            'rammer' => $rammer
        ]);
        return parent::forberedData();
    }
}