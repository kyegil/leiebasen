<?php

namespace Kyegil\Leiebasen\Visning\felles\fpdf;

use Exception;
use Fpdf\Fpdf;
use Kyegil\Leiebasen\Modell\Leieforhold\Purring as PurringModell;
use Kyegil\Leiebasen\Visning\felles\fpdf\purring\Kravsett;
use Kyegil\Leiebasen\Visning\felles\fpdf\regning\SisteInnbetalinger;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Adressefelt;

/**
 * Class Purring
 *
 * Mal for Pdf utskrift av purring
 *
 *  Ressurser:
 *
 *      pdf \Fpdf\Fpdf
 *      purring \Kyegil\Leiebasen\Modell\Leieforhold\Purring
 *      purregebyr float
 *  Mulige variabler:
 *      $avsender
 *      $avsenderAdresse
 *      $avsenderGateadresse
 *      $avsenderPostnr
 *      $avsenderPoststed
 *      $avsenderOrgNr
 *      $avsenderTelefon
 *      $avsenderTelefax
 *      $avsenderMobil
 *      $avsenderEpost
 *      $avsenderHjemmeside
 *      $avsenderBankkonto
 *      $avtalegiroTilgjengelig boolean
 *      $efakturaTilgjengelig boolean
 *      $mottakerAdresse
 *      $leieforholdId
 *      $leieforholdBeskrivelse
 *      $fastKid
 *      $gironr
 *      $purredato
 *      $forfall
 *      $girobeløp
 *      $utestående
 *      $girotekst
 *      $blankettBetalingsinfo
 *      $blankettbeløp
 *      $blankettbeløpKroner
 *      $blankettbeløpØre
 *      $ocrKid
 *      $ocrKontonummer
 *      $kontrollsiffer
 *      $kravsett \Kyegil\Leiebasen\Visning\felles\fpdf\Pdf|\Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray
 *      $tidligereUbetalt \Kyegil\Leiebasen\Visning\felles\fpdf\Pdf|\Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray
 *      $sisteInnbetalinger \Kyegil\Leiebasen\Visning\felles\fpdf\Pdf|\Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray
 *      $rammer boolean Viser rammer rundt felter i dokumentet
 * @link http://www.fpdf.org/en/doc/index.php
 * @package Kyegil\Leiebasen\Visning\felles\fpdf
 */
class Purring extends Pdf
{
    /** @var string */
    protected $template = 'felles/fpdf/Purring';

    /**
     * @param string|array $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null): Pdf
    {
        if ($attributt == 'purring') {
            return $this->settPurring($verdi);
        }
        if ($attributt == 'purregebyr') {
            return $this->settRessurs('purregebyr', $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @param PurringModell $purring
     * @return $this
     */
    private function settPurring(PurringModell $purring): Purring
    {
        return $this->settRessurs('purring' , $purring);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData(): Pdf
    {
        /** @var Fpdf $pdf */
        $pdf = $this->hentRessurs('pdf');
        /** @var PurringModell|null $purring */
        $purring = $this->hentRessurs('purring');
        $purregebyr = floatval($this->hentRessurs('purregebyr') ?? 0);
        $kravsett = '';
        $sisteInnbetalinger = '';
        $rammer = $this->hent('rammer') ?? false;

        if ($purring) {
            $utestående = $purring->hentUtestående() + $purregebyr;
            $leieforhold = $purring->hentLeieforhold();
            $purredato = $purring->hentPurredato();
            $forfallsdato = $purring->hentPurreforfall();
            $blankettbeløp = strval($utestående);
            $blankettbeløpKroner = intval($blankettbeløp);
            $blankettbeløpØre = round(100 * ($blankettbeløp - $blankettbeløpKroner));
            $blankettbeløpØre = str_pad($blankettbeløpØre, 2, '0', STR_PAD_LEFT);

            $kravsett = $this->app->vis(Kravsett::class, [
                'pdf' => $pdf,
                'purring' => $purring,
                'purregebyr' => $purregebyr,
                'rammer' => $rammer
            ]);

            $sisteInnbetalinger = $this->app->vis(SisteInnbetalinger::class, [
                'pdf' => $pdf,
                'leieforhold' => $leieforhold,
                'dato' => $purredato,
                'rammer' => $rammer,
                'antall' => 1
            ]);

            $this->definerData([
                'mottakerAdresse' => $leieforhold->navn . "\n"
                    . trim($this->app->vis(Adressefelt::class, ['leieforhold' => $leieforhold])->settMal('felles/html/leieforhold/Adressefelt.txt')),
                'leieforholdId' => $leieforhold->hentId(),
                'leieforholdBeskrivelse' => $leieforhold->hentBeskrivelse(),
                'fastKid' => $leieforhold->hentKid(),
                'gironr' => $purring->hentId(),
                'purredato' => $purredato ? $purredato->format('d.m.Y') : '',
                'forfall' => $forfallsdato ? $forfallsdato->format('d.m.Y') : '',
                'utestående' => $utestående,
                'blankettbeløp' => $blankettbeløp,
                'blankettbeløpKroner' => $blankettbeløpKroner,
                'blankettbeløpØre' => $blankettbeløpØre,
                'ocrKid' => $leieforhold->hentKid(),
                'ocrKontonummer' => $this->app->hentValg('bankkonto'),
                'kontrollsiffer' => $this->app->hentKidBestyrer()::kontrollsiffer($blankettbeløpKroner . $blankettbeløpØre)
            ]);
        }
        $this->definerData([
            'avsender' => $this->app->hentValg('utleier'),
            'avsenderAdresse' => implode("\n", [
                $this->app->hentValg('utleier'),
                $this->app->hentValg('adresse'),
                $this->app->hentValg('postnr') . ' ' . $this->app->hentValg('poststed')
            ]),
            'avsenderGateadresse' => $this->app->hentValg('adresse'),
            'avsenderPostnr' => $this->app->hentValg('postnr'),
            'avsenderPoststed' => $this->app->hentValg('poststed'),
            'avsenderOrgNr' => $this->app->hentValg('orgnr'),
            'avsenderTelefon' => $this->app->hentValg('telefon'),
            'avsenderTelefax' => $this->app->hentValg('telefax'),
            'avsenderMobil' => $this->app->hentValg('mobil'),
            'avsenderEpost' => $this->app->hentValg('epost'),
            'avsenderHjemmeside' => $this->app->hentValg('hjemmeside'),
            'avsenderBankkonto' => $this->app->hentValg('bankkonto'),
            'girotekst' => $this->app->hentValg('girotekst'),
            'efakturaTilgjengelig' => boolval($this->app->hentValg('efaktura')),
            'avtalegiroTilgjengelig' => boolval($this->app->hentValg('avtalegiro')),
            'mottakerAdresse' => '',
            'leieforholdId' => '',
            'leieforholdBeskrivelse' => '',
            'fastKid' => '',
            'kid' => '',
            'gironr' => '',
            'utskriftsdato' => '',
            'forfall' => '',
            'utestående' => '',
            'blankettBetalingsinfo' => '',
            'blankettbeløp' => '',
            'blankettbeløpKroner' => '',
            'blankettbeløpØre' => '',
            'ocrKid' => '',
            'kontrollsiffer' => '',
            'kravsett' => $kravsett,
            'sisteInnbetalinger' => $sisteInnbetalinger,
            'rammer' => $rammer
        ]);
        return parent::forberedData();
    }
}