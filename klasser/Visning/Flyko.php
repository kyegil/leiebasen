<?php

namespace Kyegil\Leiebasen\Visning;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Oppslag\FlykoKontroller;

class Flyko extends Common
{
    /** @var FlykoKontroller */
    protected $app;
    /**
     * Hent link-knapp
     *
     * @param string $url Lenken
     * @param string $tekst Knapp-teksten
     * @param null $title hover-tekst
     * @param string $target Målramme
     * @return HtmlElement
     */
    public function hentLinkKnapp($url, $tekst, $title = null, $target = '_self')
    {
        return new HtmlElement(
            'a',
            [
                'href' => $url,
                'target' => $target,
                'class' => 'button',
                'title' => $title ?? $tekst,
            ],
            $tekst
        );
    }
}