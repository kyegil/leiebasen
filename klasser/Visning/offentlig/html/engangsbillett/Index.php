<?php

namespace Kyegil\Leiebasen\Visning\offentlig\html\engangsbillett;


use Exception;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\Offentlig;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for engangsbillett-knapp
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $skjema
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\offentlig\html\engangsbillett
 */
class Index extends Offentlig
{
    /** @var string */
    protected $template = 'offentlig/html/engangsbillett/Index.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $engangsbillett = $this->hent('engangsbillett');
        $felter = new ViewArray();

        /** @var Field $billettFelt */
        $billettFelt = $this->app->vis(Field::class, [
            'name' => 'engangsbillett',
            'type' => 'hidden',
            'autocomplete' => 'off',
            'value' => $engangsbillett
        ]);

        $felter->addItem($billettFelt);

        $datasett = [
            'skjema' => [$this->app->vis(AjaxForm::class, [
                'action' => '/offentlig/index.php?oppslag=innlogging&oppdrag=ta_i_mot_skjema&skjema=engangsbillett_innlogging',
                'formId' => 'engangsbillett_innlogging',
                'buttonText' => 'Logg inn',
                'fields' => $felter,
                'reCaptchaSiteKey' => $this->hent('reCaptchaSiteKey')
            ])],
            'knapper' => [
            ],
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}