<?php

namespace Kyegil\Leiebasen\Visning\offentlig\html\innlogging_skjema;


use Exception;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\Offentlig;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for innloggingsskjema
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $skjema
 *      $reCaptchaSiteKey
 *      $redirectUrl
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\offentlig\html\innlogging_skjema
 */
class Index extends Offentlig
{
    /** @var string */
    protected $template = 'offentlig/html/innlogging_skjema/Index.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $felter = new ViewArray();
        $url = $this->hent('redirectUrl');

        /** @var Field $loginFelt */
        $redirectFelt = $this->app->vis(Field::class, [
            'name' => 'redirect',
            'type' => 'hidden',
            'autocomplete' => 'off',
            'value' => $url,
        ]);
        $felter->addItem($redirectFelt);

        /** @var Field $loginFelt */
        $loginFelt = $this->app->vis(Field::class, [
            'name' => 'login',
            'required' => true,
            'autocomplete' => 'username',
            'label' => 'Brukernavn',
        ]);
        $felter->addItem($loginFelt);

        /** @var Field $loginFelt */
        $passordFelt = $this->app->vis(Field::class, [
            'name' => 'passord',
            'type' => 'password',
            'required' => true,
            'autocomplete' => 'current-password',
            'label' => 'Passord',
        ]);
        $felter->addItem($passordFelt);

        $datasett = [
            'skjema' => [$this->app->vis(AjaxForm::class, [
                'action' => '/offentlig/index.php?oppslag=innlogging&oppdrag=ta_i_mot_skjema&skjema=innlogging',
                'formId' => 'innloggingsskjema',
                'buttonText' => 'Logg inn',
                'fields' => $felter,
                'reCaptchaSiteKey' => \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['google']['recaptcha3']['site_key'] ?? '',
            ])],
            'knapper' => [
                $this->hentLinkKnapp(
                    '/offentlig/index.php?oppslag=brukerregistrering'
                    . ($url ? '&url=' . urlencode($url) : ''),
                    'Registrer deg som beboer'
                ),
                $this->hentLinkKnapp(
                    '/offentlig/index.php?oppslag=glemt_passord'
                    . ($url ? '&url=' . urlencode($url) : ''),
                    'Glemt passord?'
                ),
            ],
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}