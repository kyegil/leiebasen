<?php
    /**
     * * Part of kyegil/leiebasen
     * Created by Kyegil
     * Date: 28/07/2020
     * Time: 12:46
     */

    namespace Kyegil\Leiebasen\Visning\offentlig\html\shared;


    use Kyegil\Leiebasen\Visning\felles\html\shared\HeadInterface as HtmlHead;
    use Kyegil\Leiebasen\Visning\Offentlig;

    /**
     * Visning for html head i mine sider
     *
     *  Mulige variabler:
     *      $title
     *      $extraMetaTags
     *      $links
     *      $scripts Scripts inserted to HTML Head
     * @package Kyegil\Leiebasen\Visning\mine_sider\html\shared
     */
    class Head extends Offentlig implements HtmlHead
    {
        /** @var string */
        protected $template = 'offentlig/html/shared/Head.html';

        /**
         * @return Head
         */
        protected function forberedData()
        {
            $this->definerData([
                'title' => '',
                'extraMetaTags' => '',
                'links' => '',
                'scripts' => ''
            ]);
            return parent::forberedData();
        }

    }