<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 28/07/2020
 * Time: 12:46
 */

namespace Kyegil\Leiebasen\Visning\offentlig\html\shared\body;


use Kyegil\Leiebasen\Visning\Offentlig;

/**
 * Visning for main content
 *
 *  Mulige variabler:
 *      $tittel
 *      $innhold
 *      $breadcrumbs
 *      $varsler
 *      $tilbakeknapp
 * @package Kyegil\Leiebasen\Visning\offentlig\html\shared
 */
class Main extends Offentlig
{
    /** @var string */
    protected $template = 'offentlig/html/shared/body/Main.html';

    /**
     * @return Main
     */
    protected function forberedData()
    {
        $this->definerData([
            'tittel' => '',
            'innhold' => '',
            'breadcrumbs' => '',
            'varsler' => '',
            'tilbakeknapp' => ''
        ]);
        return parent::forberedData();
    }

}