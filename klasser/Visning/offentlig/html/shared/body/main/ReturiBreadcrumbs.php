<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 28/07/2020
 * Time: 12:46
 */

namespace Kyegil\Leiebasen\Visning\offentlig\html\shared\body\main;


use Kyegil\Leiebasen\Visning\Offentlig;

class ReturiBreadcrumbs extends Offentlig
{
    protected $template = 'offentlig/html/shared/body/main/ReturiBreadcrumbs.html';
}