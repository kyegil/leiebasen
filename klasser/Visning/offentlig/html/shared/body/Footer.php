<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 28/07/2020
 * Time: 12:46
 */

namespace Kyegil\Leiebasen\Visning\offentlig\html\shared\body;


use Kyegil\Leiebasen\Visning\Offentlig;

/**
 * Visning for footer
 *
 *  Mulige variabler:
 *      $head
 *      $menu
 * @package Kyegil\Leiebasen\Visning\offentlig\html\shared
 */
class Footer extends Offentlig
{
    /** @var string */
    protected $template = 'offentlig/html/shared/body/Footer.html';

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $this->definerData([
            'head' => '',
            'menu' => ''
        ]);
        return parent::forberedData();
    }

}