<?php
    /**
     * * Part of kyegil/leiebasen
     * Created by Kyegil
     * Date: 28/07/2020
     * Time: 12:46
     */

    namespace Kyegil\Leiebasen\Visning\offentlig\html\shared;


    use Kyegil\Leiebasen\Visning\felles\html\shared\HtmlInterface;
    use Kyegil\Leiebasen\Visning\Offentlig;

    /**
     * Visning for grunnleggende html-struktur i mine sider
     *
     *  Mulige variabler:
     *      $lang språk
     *      $head HTML head
     *      $header
     *      $hovedmeny
     *      $main
     *      $footer
     *      $scripts Script satt inn til slutt før </body>
     * @package Kyegil\Leiebasen\Visning\mine_sider\html\shared
     */
    class Html extends Offentlig implements HtmlInterface
    {
        /** @var string */
        protected $template = 'offentlig/html/shared/Html.html';

        /**
         * @return Html
         */
        protected function forberedData()
        {
            $this->definerData([
                'lang' => $this->app->hentSpråk(),
                'head' => '',
                'header' => '',
                'hovedmeny' => '',
                'main' => '',
                'footer' => '',
                'scripts' => ''
            ]);
            return parent::forberedData();
        }

    }