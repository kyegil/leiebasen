<?php

namespace Kyegil\Leiebasen\Visning\offentlig\html\glemt_passord_skjema;


use Exception;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\Offentlig;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for innloggingsskjema
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $skjema
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\offentlig\html\glemt_passord_skjema
 */
class Index extends Offentlig
{
    /** @var string */
    protected $template = 'offentlig/html/glemt_passord_skjema/Index.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $felter = new ViewArray();

        /** @var Field $epostFelt */
        $epostFelt = $this->app->vis(Field::class, [
            'name' => 'epost',
            'required' => true,
            'autocomplete' => 'email',
            'label' => 'Oppgi E-postadresse',
        ]);

        $felter->addItem($epostFelt);

        $datasett = [
            'skjema' => [$this->app->vis(AjaxForm::class, [
                'action' => '/offentlig/index.php?oppslag=innlogging&oppdrag=ta_i_mot_skjema&skjema=engangsbillett',
                'formId' => 'engangsbillettskjema',
                'buttonText' => 'Send engangsbillett',
                'fields' => $felter,
                'reCaptchaSiteKey' => $this->hent('reCaptchaSiteKey')
            ])],
            'knapper' => [
            ],
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}