<?php

namespace Kyegil\Leiebasen\Visning\offentlig\html\søknad_skjema\body\main;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Modell\Søknad\Type;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\CheckboxGroup;
use Kyegil\Leiebasen\Visning\felles\html\form\CollapsibleSection;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\html\form\HtmlEditor;
use Kyegil\Leiebasen\Visning\felles\html\form\RadioButton;
use Kyegil\Leiebasen\Visning\felles\html\form\RadioButtonGroup;
use Kyegil\Leiebasen\Visning\felles\html\form\Select;
use Kyegil\Leiebasen\Visning\felles\html\form\TextArea;
use Kyegil\Leiebasen\Visning\Offentlig;
use Kyegil\ViewRenderer\ViewArray;
use stdClass;

/**
 * Visning for offentlig søknadsskjema
 *
 *  Ressurser:
 *      søknadSkjema \Kyegil\Leiebasen\Modell\Søknad\Type
 *  Mulige variabler:
 *      $språkKode
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\mine_sider\html\søknad_skjema\body\main
 */
class Index extends Offentlig
{
    const LAYOUT_TYPER = [
        'field' => Field::class,
        'datefield' => DateField::class,
        'text_area' => TextArea::class,
        'checkbox' => Checkbox::class,
        'checkbox_group' => CheckboxGroup::class,
        'radio_button' => RadioButton::class,
        'radio_button_group' => RadioButtonGroup::class,
        'select' => Select::class,
        'field_set' => FieldSet::class,
        'collapsible_section' => CollapsibleSection::class,
        'html_editor' => HtmlEditor::class
    ];

    /** @var string */
    protected $template = 'offentlig/html/søknad_skjema/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['søknadSkjema'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        /** @var Type|null $skjema */
        $skjema = $this->hentRessurs('søknadSkjema');
        /** @var string|null $språk */
        $språk = $this->hent('språkKode');

        $skjemaElementer = new ViewArray();

        if ($skjema) {
            $layout = $skjema->hentFeltLayout($språk);

            $konfigurering = $skjema->konfigurering;
            $konfigurering->layout = $layout;

            foreach ($layout as $layoutElement) {
                $skjemaElementer->addItem($this->hentElementVisning($layoutElement, $språk));
            }
        }

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/offentlig/index.php?oppslag=søknad_skjema&oppdrag=ta_i_mot_skjema&skjema=søknad&type=' . $skjema . '&l=' . $språk,
                'formId' => 'søknad',
                'buttonText' => $språk == 'en' ? 'Submit the Application' : 'Registrer søknaden',
                'fields' => $skjemaElementer,
                'reCaptchaSiteKey' => (\Kyegil\leiebasen\Leiebase::$config['leiebasen']['google']['recaptcha3']['site_key'] ?? null)
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }

    /**
     * @param stdClass|stdClass[]|string $layoutElement
     * @param string|null $språk
     * @return Visning|string
     */
    private function hentElementVisning($layoutElement, ?string $språk = null)
    {
        if(is_string($layoutElement)) {
            return $layoutElement;
        }
        if (is_array($layoutElement)) {
            $visning = new ViewArray();
            foreach($layoutElement as $element) {
                $visning->addItem($this->hentElementVisning($element, $språk));
            }
            return $visning;
        }
        $elementType = $layoutElement->element ?? null;
        $skjulElement = $layoutElement->hideFromFrontend ?? false;
        if ($skjulElement) {
            return '';
        }
        switch ($elementType) {
            /**
             * Elementet 'repetitive' setter inn innhold som skal repeteres.
             * Dersom tilgang = true settes det også inn felter for å opprette adgang til søknaden
             * * bool {count} Antall ganger verdien skal repeteres i et tom skjema (vil overstyres av eksisterende verdier)
             * * bool {scopeName} html-name som alle felter skal inkluderes i
             * * bool {content} Innholdet som skal repeteres
             */
            case 'repetitive':
                $count = $layoutElement->count ?? 1;
                $initContent = $layoutElement->content ?? '';
                $scopeName = $layoutElement->scopeName ?? null;
                $idx = 0;
                $visning = new ViewArray();

                while ($idx < $count) {
                    $content = is_object($initContent) ? clone $initContent : $initContent;
                    if ($scopeName) {
                        $content = $this->leggTilScopeName($scopeName . "[{$idx}]", $content);
                        $content = $this->subIndekser($content, $idx);
                    }
                    $visning->addItem($this->hentElementVisning($content, $språk));
                    $idx++;
                }
                break;
            /**
             * Elementet 'kontaktseksjon' setter inn standard kontaktinformasjons-felter.
             * Dersom tilgang = true settes det også inn felter for å opprette adgang til søknaden
             * * bool {tilgang}
             */
            case 'kontaktseksjon':
                $visning = $this->hentKontaktseksjon($layoutElement->tilgang ?? true, $språk);
                break;
            /**
             * Elementet string settes inn direkte i skjemaet, for informasjon
             * * string {value} Tekststrengen som skal settes inn
             * * string {value[en]} ... Tekststrengen på ulike språk
             */
            case 'string':
                $verdi = $layoutElement->value ?? '';
                $visning = $layoutElement->{"value[$språk]"} ?? $verdi;
                break;
            /**
             * Sett et HTML-element inn i skjemaet
             * * string {tag} HTML-tagen som skal settes inn
             * * array {attributes} Alle attributter som associative array
             * * mixed {content[en]} Innholdet som innkapsles i HTML-tag'en
             * * string {content[en]} ... Innholdet på ulike språk
             */
            case 'html':
                $tag = $layoutElement->tag ?? 'div';
                $attributes = (array)($layoutElement->attributes ?? []);
                $initContent = $layoutElement->content ?? '';
                $initContent = $layoutElement->{"content[$språk]"} ?? $initContent;
                $initContent = $this->hentElementVisning($initContent, $språk);
                $visning = new HtmlElement($tag, $attributes, $initContent);
                break;

            /**
             * Ulike skjema-felter
             */
            case 'field_set':
            case 'collapsible_section':
            $elementVisningsModell = static::LAYOUT_TYPER[$elementType] ?? FieldSet::class;
                $contents = (array)($layoutElement->contents ?? []);
                foreach ($contents as &$element) {
                    $element = $this->hentElementVisning($element, $språk);
                }
                $layoutElement->contents = $contents;

                $label = $layoutElement->label ?? null;
                $label = $layoutElement->{"label[$språk]"} ?? $label;
                $layoutElement->label = $label;

                if (isset($layoutElement->name)) {
                    $klammeposisjon = mb_strpos($layoutElement->name, '[');
                    if ($klammeposisjon !== false) {
                        $layoutElement->name = 'skjemadata[' . mb_substr($layoutElement->name, 0, $klammeposisjon) . ']' . mb_substr($layoutElement->name, $klammeposisjon);
                    }
                    else {
                        $layoutElement->name = 'skjemadata[' . $layoutElement->name . ']';
                    }
                }
                $visning = $this->app->vis($elementVisningsModell, (array)$layoutElement);
                break;

            /**
             * Avkrysningsgrupper krever at vi slår opp oversettelser av hver valgmulighet
             * før videre behandling
             */
            case 'checkbox_group':
            case 'radio_button_group':
            case 'select':
                /** @var object|array $options */
                $options = $layoutElement->options ?? [];
                $layoutElement->options = $layoutElement->{"options[$språk]"} ?? $options;

            /**
             * Tekstbehandlere må ha rett språk i menyer etc
             */
            case 'html_editor':
                $ckEditorConfig = (object)['language' => (object)['ui' => $språk, 'content' => $språk], 'height' => 500];
                $layoutElement->ckEditorConfig = $ckEditorConfig;

            /**
             * Ulike skjema-felter
             */
            default:
                $elementVisningsModell = static::LAYOUT_TYPER[$elementType] ?? Field::class;
                $label = $layoutElement->label ?? null;
                $layoutElement->label = $layoutElement->{"label[$språk]"} ?? $label;
                $description = $layoutElement->description ?? null;
                $layoutElement->description = $layoutElement->{"description[$språk]"} ?? $description;

                if (isset($layoutElement->name)) {
                    $klammeposisjon = mb_strpos($layoutElement->name, '[');
                    if ($klammeposisjon !== false) {
                        $layoutElement->name = 'skjemadata[' . mb_substr($layoutElement->name, 0, $klammeposisjon) . ']' . mb_substr($layoutElement->name, $klammeposisjon);
                    }
                    else {
                        $layoutElement->name = 'skjemadata[' . $layoutElement->name . ']';
                    }
                }
                $visning = $this->app->vis($elementVisningsModell, (array)$layoutElement);
                break;
        }

        return $visning;
    }

    /**
     * @param bool $inklusiveTilgang
     * @return Visning|string
     */
    private function hentKontaktseksjon(bool $inklusiveTilgang = true, $språk = null)
    {
        $l10n = [
            'kontaktopplysninger' => 'Kontaktopplysninger',
            'kontaktbeskrivelse' => 'Kontaktopplysninger for søker, eller for kontaktperson dersom flere søkere/familie',
            'fornavn' => 'For- og evt mellomnavn',
            'etternavn' => 'Etternavn',
            'adresse1' => 'Adresse',
            'adresse2' => '',
            'postnr' => 'Postnr',
            'poststed' => 'Poststed',
            'epost' => 'E-post-adresse',
            'telefon' => 'Telefonnummer',
            'egenkopi' => 'Jeg ønsker kopi av søknaden tilsendt på e-post',
            'opprett_profil' => 'Angi et passord sånn at du kan oppdatere og endre søknaden senere',
            'passord1' => 'Ønsket passord',
            'passord2' => 'Gjenta passordet',
        ];
        switch ($språk) {
            case 'en':
                $l10n = [
                    'kontaktopplysninger' => 'Contact information',
                    'kontaktbeskrivelse' => 'Contact information for the applicant or for their representative',
                    'fornavn' => 'First and middle name(s)',
                    'etternavn' => 'Family name',
                    'adresse1' => 'Address',
                    'adresse2' => '',
                    'postnr' => 'Postcode',
                    'poststed' => 'Postal area',
                    'epost' => 'Email address',
                    'telefon' => 'Telephone number',
                    'egenkopi' => 'I want a copy of the application sent to my email',
                    'opprett_profil' => 'Provide a password so that you can update and change your application later',
                    'passord1' => 'Password',
                    'passord2' => 'Repeat the password',
                ];
                break;
        }
        /** @var Field $kontaktpersonFornavn */
        $kontaktpersonFornavn = $this->app->vis(Field::class, [
            'id' => Type::FELT_KONTAKT_FORNAVN . '_felt',
            'name' => 'kontaktperson[fornavn]',
            'label' => $l10n['fornavn'],
            'required' => true,
            'autocomplete' => 'given-name'
        ]);
        /** @var Field $kontaktpersonEtternavn */
        $kontaktpersonEtternavn = $this->app->vis(Field::class, [
            'id' => Type::FELT_KONTAKT_ETTERNAVN . '_felt',
            'name' => 'kontaktperson[etternavn]',
            'label' => $l10n['etternavn'],
            'required' => true,
            'autocomplete' => 'family-name'
        ]);
        /** @var Field $adressefelt1 */
        $adressefelt1 = $this->app->vis(Field::class, [
            'id' => 'kontakt_adresse_felt1',
            'name' => 'kontaktperson[adresse1]',
            'label' => $l10n['adresse1'],
            'autocomplete' => 'address-line1'
        ]);
        /** @var Field $adressefelt2 */
        $adressefelt2 = $this->app->vis(Field::class, [
            'id' => 'kontakt_adresse_felt2',
            'name' => 'kontaktperson[adresse2]',
            'label' => $l10n['adresse2'],
            'autocomplete' => 'address-line2'
        ]);
        /** @var Field $postnr */
        $postnr = $this->app->vis(Field::class, [
            'id' => 'kontakt_postnr_felt',
            'name' => 'kontaktperson[postnr]',
            'label' => $l10n['postnr'],
            'autocomplete' => 'postal-code'
        ]);
        /** @var Field $poststed */
        $poststed = $this->app->vis(Field::class, [
            'id' => 'kontakt_poststed_felt',
            'name' => 'kontaktperson[poststed]',
            'label' => $l10n['poststed'],
            'autocomplete' => 'address-level1'
        ]);
        /** @var Field $kontaktTelefonnr */
        $kontaktTelefonnr = $this->app->vis(Field::class, [
            'id' => Type::FELT_KONTAKT_TELEFON . '_felt',
            'type' => 'tel',
            'name' => 'kontaktperson[telefon]',
            'label' => $l10n['telefon'],
            'required' => true,
            'onblur' => 'leiebasen.søknadsskjema.oppdaterPåkrevdeFelter()',
            'autocomplete' => 'tel'
        ]);
        /** @var Field $epostAdressefelt */
        $epostAdressefelt = $this->app->vis(Field::class, [
            'id' => Type::FELT_KONTAKT_EPOST . '_felt',
            'type' => 'email',
            'name' => 'kontaktperson[epost]',
            'label' => $l10n['epost'],
            'required' => true,
            'onblur' => 'leiebasen.søknadsskjema.oppdaterPåkrevdeFelter()',
            'autocomplete' => 'email'
        ]);
        /** @var Checkbox $spesielleBehov */
        $egenKopiKnapp = $this->app->vis(Checkbox::class, [
            'id' => 'egenkopi_knapp',
            'name' => 'kontaktperson[egenkopi]',
            'label' => $l10n['egenkopi'],
            'onclick' => 'leiebasen.søknadsskjema.oppdaterPåkrevdeFelter()',
            'checked' => false,
        ]);

        $felter = [
            'label' => $l10n['kontaktbeskrivelse'],
            new HtmlElement('div', [
                'style' => 'width: 100%; display: flex;'
            ], [
                new HtmlElement('div', ['style' => 'display: block; margin: 0 10px 0 0; width:100%;'], $kontaktpersonFornavn),
                new HtmlElement('div', ['style' => 'display: block; margin: 0 0 0 10px; width:100%;'], $kontaktpersonEtternavn)
            ]),
            $adressefelt1,
            $adressefelt2,
            new HtmlElement('div', [
                'class' => 'postnrsted',
                'style' => 'width: 100%; display: inline-flex;'
            ], [
                new HtmlElement('div', ['style' => 'margin: 0 10px;'], $postnr),
                new HtmlElement('div', ['style' => 'margin: 0 10px; width:100%'], $poststed)
            ]),
            $kontaktTelefonnr,
            $epostAdressefelt,
            $egenKopiKnapp
        ];

        if ($inklusiveTilgang) {
            $felter[] = $this->app->vis(CollapsibleSection::class, [
                "id" => "profil_seksjon",
                'label' => $l10n['opprett_profil'],
                'name' => 'kontaktperson[lag_profil]',
                "contents" => [
                    $this->app->vis(Field::class, [
                        "id" => "søknadspassord_felt",
                        'name' => 'kontaktperson[passord1]',
                        "type" => "password",
                        'label' => $l10n['passord1'],
                        "required" => true
                    ]),
                    $this->app->vis(Field::class, [
                        "id" => "søknadspassord_repetisjon_felt",
                        'name' => 'kontaktperson[passord2]',
                        "type" => "password",
                        'label' => $l10n['passord2'],
                        "required" => true
                    ]),
                ]
            ]);
        }

        /** @var FieldSet $kontaktInfo */
        $kontaktInfo = $this->app->vis(FieldSet::class, [
            'id' => 'kontaktinfo-feltsett',
            'label' => $l10n['kontaktopplysninger'],
            'contents' => $felter
        ]);

        return $kontaktInfo;
    }

    /**
     * @param string $scopeName
     * @param mixed $innhold
     * @return mixed
     */
    private function leggTilScopeName(string $scopeName, $innhold)
    {
        if(is_array($innhold)) {
            $resultat = [];
            foreach($innhold as $element) {
                $resultat[] = $this->leggTilScopeName($scopeName, $element);
            }
            return $resultat;
        }
        if(is_object($innhold)) {
            $resultat = clone $innhold;
            foreach($innhold as $egenskap => $verdi) {
                $resultat->$egenskap = $this->leggTilScopeName($scopeName, $verdi);
            }
            if (property_exists($innhold, 'name')) {
                $klammeposisjon = mb_strpos($innhold->name, '[');
                if ($klammeposisjon !== false) {
                    $resultat->name = $scopeName . '[' . mb_substr($innhold->name, 0, $klammeposisjon) . ']' . mb_substr($innhold->name, $klammeposisjon);
                }
                else {
                    $resultat->name = $scopeName . '[' . $innhold->name . ']';
                }
            }
            return $resultat;
        }
        return $innhold;
    }

    /**
     * @param mixed $innhold
     * @param int $idx
     * @return mixed
     */
    private function subIndekser($innhold, int $idx)
    {
        if(is_array($innhold)) {
            $resultat = [];
            foreach($innhold as $element) {
                $resultat[] = $this->subIndekser($element, $idx);
            }
            return $resultat;
        }
        if (is_object($innhold)) {
            $resultat = clone $innhold;
            foreach($innhold as $egenskap => $verdi) {
                $resultat->$egenskap = $this->subIndekser($verdi, $idx);
            }
            if (property_exists($innhold, 'id')) {
                $resultat->id = $innhold->id . '-' . ($idx + 1);
            }
            return $resultat;
        }
        return $innhold;
    }

    /**
     * @param string $sti
     * @return mixed
     * @throws Exception
     */
    private function hentSubverdi(string $sti)
    {
        $verdi = '';
        /** @var Søknad|null $søknad */
        $søknad = $this->hentRessurs('søknad');
        if ($søknad) {
            $stiElementer = explode('[', $sti);
            array_walk($stiElementer, function(&$element) {
                $element = trim($element, ']');
            });
            $verdi = $søknad->hent(array_shift($stiElementer));
            while (count($stiElementer)) {
                $element = array_shift($stiElementer);
                if (is_numeric($element)) {
                    $verdi = is_array($verdi) && isset($verdi[$element]) ? $verdi[$element] : null;
                }
                else {
                    $verdi =  is_object($verdi) && property_exists($verdi, $element) ? $verdi->{$element} : null;
                }
            }
        }
        return $verdi;
    }
}