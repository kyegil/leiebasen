<?php

namespace Kyegil\Leiebasen\Visning\offentlig\html\registrer_bruker_skjema;


use Exception;
use Kyegil\Leiebasen\Modell\Person\Brukerprofil;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\Checkbox;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\felles\html\form\RadioButtonGroup;
use Kyegil\Leiebasen\Visning\Offentlig;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Visning for innloggingsskjema
 *
 *  Ressurser:
 *  Mulige variabler:
 *      $skjema
 *      $reCaptchaSiteKey
 *      $redirectUrl
 *      $knapper
 * @package Kyegil\Leiebasen\Visning\offentlig\html\registrer_bruker_skjema
 */
class Index extends Offentlig
{
    /** @var string */
    protected $template = 'offentlig/html/registrer_bruker_skjema/Index.html';

    /**
     * @return $this
     * @throws Exception
     */
    protected function forberedData()
    {
        $url = $this->hent('redirectUrl');

        $felter = new ViewArray();
        $navnfelter = new ViewArray();
        $passordfelter = new ViewArray();
        $regningsfelter = new ViewArray();

        /** @var Field $loginFelt */
        $redirectFelt = $this->app->vis(Field::class, [
            'name' => 'redirect',
            'type' => 'hidden',
            'autocomplete' => 'off',
            'value' => $url,
        ]);
        $felter->addItem($redirectFelt);

        /** @var FieldSet $navnfeltsett */
        $navnfeltsett = $this->app->vis(FieldSet::class, [
            'id' => 'navnfeltsett',
            'label' => 'Navn på bruker',
            'contents' => $navnfelter
        ]);

        /** @var FieldSet $passordFeltsett */
        $passordFeltsett = $this->app->vis(FieldSet::class, [
            'id' => 'passordFeltsett',
            'label' => 'Passord',
            'contents' => $passordfelter
        ]);

        /** @var FieldSet $regningsfeltsett */
        $regningsfeltsett = $this->app->vis(FieldSet::class, [
            'id' => 'regningsfeltsett',
            'label' => 'Detaljer fra regning',
            'contents' => $regningsfelter
        ]);

        /** @var Field $loginFelt */
        $loginFelt = $this->app->vis(Field::class, [
            'name' => 'login',
            'required' => true,
            'minlength' => 3,
            'autocomplete' => 'username',
            'label' => 'Ønsket brukernavn',
        ]);

        /** @var Field $passordFelt */
        $passordFelt = $this->app->vis(Field::class, [
            'name' => 'passord[0]',
            'type' => 'password',
            'required' => true,
            'autocomplete' => 'new-password',
            'label' => 'Velg et passord for innlogging',
            'pattern' => Brukerprofil::$passordRegex
        ]);

        /** @var Field $gjentaPassordFelt */
        $gjentaPassordFelt = $this->app->vis(Field::class, [
            'name' => 'passord[1]',
            'type' => 'password',
            'required' => true,
            'autocomplete' => 'new-password',
            'label' => 'Gjenta passordet',
        ]);

        /** @var Field $epostFelt */
        $epostFelt = $this->app->vis(Field::class, [
            'name' => 'e-post',
            'required' => false,
            'autocomplete' => 'email',
            'label' => 'Oppgi E-postadresse',
        ]);

        /** @var Field $identifiseringsmetodeFelt */
        $identifiseringsmetodeFelt = $this->app->vis(RadioButtonGroup::class, [
            'name' => 'identifiseringsmetode',
            'required' => true,
            'label' => 'Velg identifiseringsmetode',
            'options' => (object)[
                'e-post' => 'Identifisering med e-postadresse',
//                'sms' => 'Identifisering med SMS',
                'regning' => 'Identifisering med detaljer fra regning',
            ],
        ]);

        /** @var Field $telefonFelt */
        $telefonFelt = $this->app->vis(Field::class, [
            'name' => 'telefon',
            'type' => 'tel',
            'required' => false,
            'autocomplete' => 'tel',
            'label' => 'Mobiltelefonnummer',
        ]);

        /** @var Field $fornavnFelt */
        $fornavnFelt = $this->app->vis(Field::class, [
            'name' => 'fornavn',
            'required' => true,
            'autocomplete' => 'given-name',
            'label' => 'For- og evt mellomnavn',
        ]);

        /** @var Field $etternavnFelt */
        $etternavnFelt = $this->app->vis(Field::class, [
            'name' => 'etternavn',
            'required' => true,
            'autocomplete' => 'family-name',
            'label' => 'Etternavn',
        ]);

        /** @var Field $regningsdatoFelt */
        $regningsdatoFelt = $this->app->vis(DateField::class, [
            'name' => 'regningsdato',
            'required' => true,
            'autocomplete' => 'off',
            'label' => 'Regningsdato oppgitt på regningen',
        ]);

        /** @var Field $kidFelt */
        $kidFelt = $this->app->vis(Field::class, [
            'name' => 'kid',
            'required' => true,
            'autocomplete' => 'off',
            'label' => 'KID-nummer',
        ]);

        /** @var Field $regningsbeløpFelt */
        $regningsbeløpFelt = $this->app->vis(Field::class, [
            'name' => 'regningsbeløp',
            'required' => true,
            'autocomplete' => 'off',
            'label' => 'Regningsbeløp oppgitt på regningen (før evt betalinger)',
        ]);

        /** @var Field $framleierFelt */
        $framleierFelt = $this->app->vis(Checkbox::class, [
            'name' => 'er_framleier',
            'label' => 'Jeg bor på framleie i dette aktuelle leieforholdet',
            'checked' => false
        ]);

        /** @var Field $leietakerNavnFelt */
        $leietakerNavnFelt = $this->app->vis(Field::class, [
            'name' => 'leietaker_navn',
            'autocomplete' => 'off',
            'label' => 'Navn på den du framleier fra, nøyaktig som registrert i leieavtalen',
        ]);

        $navnfelter->addItem('Oppgi navnet ditt nøyaktig som det framkommer i leieavtalen og på regninger fra ' . $this->app->hentValg('utleier'));
        $navnfelter->addItem($fornavnFelt);
        $navnfelter->addItem($etternavnFelt);

        $passordfelter->addItem($passordFelt);
        $passordfelter->addItem('Passordet må bestå av minst 8 tegn, inneholde minst ett tall, én stor bokstav og ett spesialtegn.');
        $passordfelter->addItem($gjentaPassordFelt);

        $felter->addItem($identifiseringsmetodeFelt);
        $felter->addItem($loginFelt);
        $felter->addItem($navnfeltsett);

        $felter->addItem($epostFelt);
        $felter->addItem($telefonFelt);
        $felter->addItem($regningsfeltsett);
        $felter->addItem($passordFeltsett);

        $regningsfelter->addItem($regningsdatoFelt);
        $regningsfelter->addItem($kidFelt);
        $regningsfelter->addItem($regningsbeløpFelt);
        $regningsfelter->addItem($framleierFelt);
        $regningsfelter->addItem($leietakerNavnFelt);

        $datasett = [
            'skjema' => [$this->app->vis(AjaxForm::class, [
                'action' => '/offentlig/index.php?oppslag=innlogging&oppdrag=ta_i_mot_skjema&skjema=registrer_bruker_skjema',
                'formId' => 'registrer_bruker_skjema',
                'buttonText' => 'Registrér',
                'fields' => $felter,
                'reCaptchaSiteKey' => \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['google']['recaptcha3']['site_key'] ?? ''
            ])],
            'knapper' => [
                $this->hentLinkKnapp(
                    '/offentlig/index.php?oppslag=innlogging'
                    . ($url ? '&url=' . urlencode($url) : ''),
                    'Logg inn som eksisterende bruker'
                ),
                $this->hentLinkKnapp(
                    '/offentlig/index.php?oppslag=glemt_passord'
                    . ($url ? '&url=' . urlencode($url) : ''),
                    'Glemt passord?'
                ),
            ],
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}