<?php

namespace Kyegil\Leiebasen\Visning\offentlig\js\register_bruker_skjema;


use Exception;
use Kyegil\Leiebasen\Modell\Søknad\Type;
use Kyegil\Leiebasen\Visning\Offentlig;

/**
 * Skript-fil for offentlig registreringsskjema
 *
 *  Ressurser:
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\offentlig\js\registrer_bruker_skjema
 */
class Index extends Offentlig
{
    /** @var string */
    protected $template = 'offentlig/js/registrer_bruker_skjema/Index.js';
}