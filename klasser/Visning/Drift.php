<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 31/07/2020
 * Time: 14:44
 */

namespace Kyegil\Leiebasen\Visning;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\ViewRenderer\ViewArray;
use stdClass;

class Drift extends Common
{
    /** @var DriftKontroller */
    protected $app;

    /**
     * Hent link-knapp
     *
     * @param string $url Lenken
     * @param string $tekst Knapp-teksten
     * @param string|null $title hover-tekst
     * @param string $target Målramme
     * @return HtmlElement
     */
    public function hentLinkKnapp(string $url, string $tekst, ?string $title = null, string $target = '_self'): HtmlElement
    {
        return new HtmlElement(
            'a',
            [
                'href' => $url,
                'target' => $target,
                'class' => 'button',
                'title' => $title ?? $tekst,
            ],
            $tekst
        );
    }

    /**
     * @param stdClass $leiebasenJsObjektEgenskaper
     * @return $this
     */
    public function settLeiebasenJsObjektEgenskaper(stdClass $leiebasenJsObjektEgenskaper): Drift
    {
        return $this->settRessurs('leiebasenJsObjektEgenskaper', $leiebasenJsObjektEgenskaper);
    }

    /**
     * @return ViewArray
     */
    public function gjengiLeiebasenJsObjektEgenskaper(): ViewArray
    {
        /** @var stdClass $leiebasenJsObjektEgenskaper */
        $leiebasenJsObjektEgenskaper = $this->hentRessurs('leiebasenJsObjektEgenskaper') ?? new stdClass();
        $resultat = new ViewArray();
        foreach ($leiebasenJsObjektEgenskaper as $egenskap => $verdi) {
            $resultat->addItem('leiebasen.' . $egenskap . ' = ' . JsCustom::JsonImproved($verdi) . ";\n\n");
        }
        return $resultat;
    }
}