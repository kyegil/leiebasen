<?php

namespace Kyegil\Leiebasen\Visning\sentral\js\profil_passord_skjema\body\main;


use Kyegil\Leiebasen\Visning\Sentral;

/**
 * Skript-fil i sentral
 *
 *  Ressurser:
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\sentral\js\profil_passord_skjema\body\main
 */
class Index extends Sentral
{
    protected $template = 'sentral/js/profil_passord_skjema/body/main/Index.js';

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        return parent::forberedData();
    }
}