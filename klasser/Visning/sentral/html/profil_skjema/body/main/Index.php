<?php

namespace Kyegil\Leiebasen\Visning\sentral\html\profil_skjema\body\main;


use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm;
use Kyegil\Leiebasen\Visning\felles\html\form\DateField;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\Leiebasen\Visning\felles\html\form\FieldSet;
use Kyegil\Leiebasen\Visning\Sentral;

/**
 * Visning for brukerprofil-skjema i sentral
 *
 *  Ressurser:
 *      bruker \Kyegil\Leiebasen\Modell\Person
 *  Mulige variabler:
 *      $skjema
 * @package Kyegil\Leiebasen\Visning\sentral\html\profil_skjema\body\main
 */
class Index extends Sentral
{
    /** @var string */
    protected $template = 'sentral/html/profil_skjema/body/main/Index.html';

    /**
     * @param string $attributt
     * @param mixed $verdi
     * @return $this
     * @throws \Exception
     */
    public function sett($attributt, $verdi = null)
    {
        if(in_array($attributt,['bruker'])) {
            return $this->settRessurs($attributt, $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function forberedData()
    {
        /** @var Person $bruker */
        $bruker = $this->hentRessurs('bruker');
        $brukerProfil = $bruker ? $bruker->hentBrukerProfil() : null;

        $kontaktinfo = [];
        /** @var Field $brukernavnFelt */
        $brukerIdFelt = $this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\form\Field::class, [
            'name' => 'id',
            'type' => 'hidden',
            'required' => true,
            'value' => $bruker->hentId()
        ]);

        /** @var Field $brukernavnFelt */
        $brukernavnFelt = $this->app->vis(Field::class, [
            'name' => 'login',
            'label' => 'Brukernavn',
            'required' => true,
            'value' => $brukerProfil->hentBrukernavn()
        ]);

        $endrePassordLenke = new HtmlElement('a', [
            'href' => '/sentral/index.php?oppslag=profil_passord_skjema',
            'title' => 'Endre passordet som hører til dette brukernavnet'
        ],'Endre passordet');

        /** @var Field $brukernavnFelt */
        $epostAdresseFelt = $this->app->vis(Field::class, [
            'name' => 'epost',
            'label' => 'E-postadresse',
            'required' => false,
            'value' => $bruker->hentEpost()
        ]);

        $brukerprofilFeltsett = $this->app->vis(FieldSet::class, [
            'name' => 'epost',
            'label' => 'Brukerprofil',
            'contents' => [
                $brukerIdFelt,
                $brukernavnFelt,
                $endrePassordLenke,
                $epostAdresseFelt
            ]
        ]);

        /** @var Field $navnFelt */
        $navnFelt = $this->app->vis(Field::class, [
            'label' => 'Navn',
            'readonly' => true,
            'value' => $bruker->hentNavn()
        ]);
        $kontaktinfo[] = $navnFelt;

        if($bruker->erOrg) {
            /** @var Field $personnrFelt */
            $personnrFelt = $this->app->vis(Field::class, [
                'name' => 'personnr',
                'label' => 'Org.nr',
                'readonly' => (bool)$bruker->hentPersonnr(),
                'value' => $bruker->hentPersonnr()
            ]);
            $kontaktinfo[] = $personnrFelt;

        }
        else {
            /** @var DateField $fødselsdatoFelt */
            $fødselsdatoFelt = $this->app->vis(DateField::class, [
                'name' => 'fødselsdato',
                'label' => 'Fødselsdato',
                'readonly' => (bool)$bruker->hentFødselsdato(),
                'value' => $bruker->hentFødselsdato() ? $bruker->hentFødselsdato()->format('Y-m-d') : null
            ]);
            $kontaktinfo[] = $fødselsdatoFelt;

            /** @var Field $personnrFelt */
            $personnrFelt = $this->app->vis(Field::class, [
                'name' => 'personnr',
                'label' => 'Personnummer',
                'readonly' => (bool)$bruker->hentPersonnr(),
                'value' => $bruker->hentPersonnr()
            ]);
            $kontaktinfo[] = $personnrFelt;
        }

        /** @var Field $adresse1Felt */
        $adresse1Felt = $this->app->vis(Field::class, [
            'name' => 'adresse1',
            'label' => 'Adresse',
            'value' => $bruker->hentAdresse1()
        ]);
        $kontaktinfo[] = $adresse1Felt;

        /** @var Field $adresse2Felt */
        $adresse2Felt = $this->app->vis(Field::class, [
            'name' => 'adresse2',
            'value' => $bruker->hentAdresse2()
        ]);
        $kontaktinfo[] = $adresse2Felt;

        /** @var Field $postnrFelt */
        $postnrFelt = $this->app->vis(Field::class, [
            'name' => 'postnr',
            'label' => 'Postnr',
            'value' => $bruker->hentPostnr()
        ]);
        $kontaktinfo[] = $postnrFelt;

        /** @var Field $poststedFelt */
        $poststedFelt = $this->app->vis(Field::class, [
            'name' => 'poststed',
            'label' => 'Poststed',
            'value' => $bruker->hentPoststed()
        ]);
        $kontaktinfo[] = $poststedFelt;

        /** @var Field $landFelt */
        $landFelt = $this->app->vis(Field::class, [
            'name' => 'land',
            'label' => '(Land)',
            'value' => $bruker->hentLand()
        ]);
        $kontaktinfo[] = $landFelt;

        /** @var Field $telefonFelt */
        $telefonFelt = $this->app->vis(Field::class, [
            'name' => 'telefon',
            'label' => 'Telefon',
            'value' => $bruker->hentTelefon()
        ]);
        $kontaktinfo[] = $telefonFelt;

        /** @var Field $mobilFelt */
        $mobilFelt = $this->app->vis(Field::class, [
            'name' => 'mobil',
            'label' => 'Mobilnr',
            'value' => $bruker->hentMobil()
        ]);
        $kontaktinfo[] = $mobilFelt;

        $datasett = [
            'skjema' => $this->app->vis(AjaxForm::class, [
                'action' => '/sentral/index.php?oppslag=profil_skjema&oppdrag=ta_i_mot_skjema&skjema=brukerprofil',
                'formId' => 'brukerprofil',
                'buttonText' => 'Lagre endringer',
                'fields' => [
                    $brukerprofilFeltsett,
                    $this->app->vis(FieldSet::class, [
                        'name' => 'epost',
                        'label' => 'Kontaktopplysninger',
                        'contents' => $kontaktinfo
                    ])
                ]
            ])
        ];
        $this->definerData($datasett);
        return parent::forberedData();
    }
}