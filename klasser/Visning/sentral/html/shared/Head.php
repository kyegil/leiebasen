<?php

    namespace Kyegil\Leiebasen\Visning\sentral\html\shared;


    use Kyegil\Leiebasen\Visning\felles\html\shared\HeadInterface as HtmlHead;
    use Kyegil\Leiebasen\Visning\Sentral;

    /**
     * Visning for html head i Sentral
     *
     * Tilgjengelige variabler:
     * * $title
     * * $extraMetaTags
     * * $links
     * * $scripts Scripts inserted to HTML Head
     * @package Kyegil\Leiebasen\Visning\sentral\html\shared
     */
    class Head extends Sentral implements HtmlHead
    {
        /** @var string */
        protected $template = 'sentral/html/shared/Head.html';

        /**
         * @return Head
         */
        protected function forberedData()
        {
            $this->definerData([
                'title' => '',
                'extraMetaTags' => '',
                'links' => '',
                'scripts' => ''
            ]);
            return parent::forberedData();
        }

    }