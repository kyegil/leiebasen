<?php

namespace Kyegil\Leiebasen\Visning\sentral\html\shared\body;


/**
 * Visning for main content
 *
 *  Mulige variabler:
 *      $tittel
 *      $innhold
 *      $breadcrumbs
 *      $varsler
 *      $tilbakeknapp
 * @package Kyegil\Leiebasen\Visning\sentral\html\shared
 */
class Main extends \Kyegil\Leiebasen\Visning\Sentral
{
    /** @var string */
    protected $template = 'sentral/html/shared/body/Main.html';

    /**
     * @return Main
     */
    protected function forberedData()
    {
        $this->definerData([
            'tittel' => '',
            'innhold' => '',
            'breadcrumbs' => '',
            'varsler' => '',
            'tilbakeknapp' => ''
        ]);
        return parent::forberedData();
    }

}