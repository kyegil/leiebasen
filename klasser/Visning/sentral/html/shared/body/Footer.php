<?php

namespace Kyegil\Leiebasen\Visning\sentral\html\shared\body;


use Kyegil\Leiebasen\Visning\Sentral;

/**
 * Visning for footer i sentral
 *
 *  Mulige variabler:
 *      $head
 *      $menu
 * @package Kyegil\Leiebasen\Visning\sentral\html\shared
 */
class Footer extends Sentral
{
    /** @var string */
    protected $template = 'sentral/html/shared/body/Footer.html';

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $this->definerData([
            'head' => '',
            'menu' => ''
        ]);
        return parent::forberedData();
    }

}