<?php

namespace Kyegil\Leiebasen\Visning\sentral\html\shared\body\main;


use Kyegil\Leiebasen\Visning\Sentral;

class ReturiBreadcrumbs extends Sentral
{
    protected $template = 'sentral/html/shared/body/main/ReturiBreadcrumbs.html';
}