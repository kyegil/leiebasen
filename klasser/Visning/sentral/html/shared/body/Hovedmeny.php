<?php

namespace Kyegil\Leiebasen\Visning\sentral\html\shared\body;


use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\Sentral;

/**
 * Visning for hovedmeny
 *
 *  Mulige variabler:
 *      $hovedLeieforhold
 *      $hovedLeieobekt
 * @package Kyegil\Leiebasen\Visning\sentral\html\shared
 */
class Hovedmeny extends Sentral
{
    /** @var string */
    protected $template = 'sentral/html/shared/body/Hovedmeny.html';

    /**
     * @param array|string $attributt
     * @param null $verdi
     * @return $this
     */
    public function sett($attributt, $verdi = null)
    {
        if($attributt == 'bruker') {
            return $this->settRessurs('bruker', $verdi);
        }
        return parent::sett($attributt, $verdi);
    }

    /**
     * @return $this
     */
    protected function forberedData()
    {
        /** @var Person|null $bruker */
        $bruker = $this->hentRessurs('bruker');
        return parent::forberedData();
    }

}