<?php

namespace Kyegil\Leiebasen\Visning\sentral\html\shared\body;


use Kyegil\Leiebasen\Visning\Sentral;

/**
 * Visning for brukermeny
 *
 *  Mulige variabler:
 * @package Kyegil\Leiebasen\Visning\sentral\html\shared
 */
class Brukermeny extends Sentral
{
    /** @var string */
    protected $template = 'sentral/html/shared/body/Brukermeny.html';

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $this->definerData([
        ]);
        return parent::forberedData();
    }

}