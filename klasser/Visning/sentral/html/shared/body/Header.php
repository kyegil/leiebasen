<?php

namespace Kyegil\Leiebasen\Visning\sentral\html\shared\body;


use Kyegil\Leiebasen\Visning\Sentral;

/**
 * Visning for header i Sentral-sidene
 *
 *  Mulige variabler:
 *      $språkvelger
 *      $logo
 *      $logoWidth
 *      $logoHeight
 *      $navn
 * @package Kyegil\Leiebasen\Visning\sentral\html\shared
 */
class Header extends Sentral
{
    /** @var string */
    protected $template = 'sentral/html/shared/body/Header.html';

    /**
     * @return $this
     */
    protected function forberedData()
    {
        $this->definerData([
            'språkvelger' => '',
            'logo' => '/pub/media/bilder/offentlig/blank.png',
            'logoWidth' => 1,
            'logoHeight' => 1,
            'navn' => $this->app->hentValg('utleier')
        ]);
        return parent::forberedData();
    }

}