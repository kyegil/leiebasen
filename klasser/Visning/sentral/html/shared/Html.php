<?php

    namespace Kyegil\Leiebasen\Visning\sentral\html\shared;


    use Kyegil\Leiebasen\Visning\felles\html\shared\HtmlInterface;
    use Kyegil\Leiebasen\Visning\Sentral;

    /**
     * Visning for grunnleggende html-struktur i Sentral
     *
     *  Mulige variabler:
     *      $head HTML head
     *      $header
     *      $hovedmeny
     *      $main
     *      $footer
     *      $scripts Script satt inn til slutt før </body>
     * @package Kyegil\Leiebasen\Visning\sentral\html\shared
     */
    class Html extends Sentral implements HtmlInterface
    {
        /** @var string */
        protected $template = 'sentral/html/shared/Html.html';

        /**
         * @return Html
         */
        protected function forberedData()
        {
            $this->definerData([
                'head' => '',
                'header' => '',
                'hovedmeny' => '',
                'main' => '',
                'footer' => '',
                'scripts' => ''
            ]);
            return parent::forberedData();
        }

    }