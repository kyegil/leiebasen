<?php

/**
 * @deprecated
 * @see \Kyegil\Leiebasen\Modell\Innbetaling
 */
class Innbetaling extends DatabaseObjekt {

    /**
     * @var string
     */
    protected	$tabell = "innbetalinger";	// Hvilken tabell i databasen som inneholder primærnøkkelen for dette objektet
    /**
     * @var string
     */
    protected	$idFelt = "innbetaling";	// Hvilket felt i tabellen som lagrer primærnøkkelen for dette objektet
    /**
     * @var
     */
    protected	$data;				// DB-verdiene lagret som et objekt. Null betyr at verdiene ikke er lastet
    /**
     * @var
     */
    public		$id;				// Unik id-streng for denne purresiden

    /**
     * Last
     *
     * Last betalingens kjernedata fra databasen
     *
     * @param int $id
     * @return void
     * @throws Exception
     */
    protected function last($id = null) {
        $tp = $this->mysqli->table_prefix;

        if( !$id ) {
            $id = $this->id;
        }

        $resultat = $this->mysqli->arrayData(array(
            'returnQuery'	=> true,

            'fields' =>			"{$this->tabell}.{$this->idFelt} AS id,
                                {$this->tabell}.innbetalingsid,
                                {$this->tabell}.innbetaling,
                                {$this->tabell}.konto,
                                IFNULL(kontoer.kode, {$this->tabell}.konto) AS kontokode,
                                IFNULL(kontoer.navn, {$this->tabell}.konto) AS kontonavn,
                                {$this->tabell}.ocr_transaksjon,
                                {$this->tabell}.ref,
                                {$this->tabell}.dato,
                                {$this->tabell}.merknad,
                                {$this->tabell}.betaler,
                                {$this->tabell}.registrerer,
                                {$this->tabell}.registrert,
                                {$this->tabell}.beløp,
                                {$this->tabell}.leieforhold,
                                {$this->tabell}.krav\n",

            'source' => 		"{$tp}{$this->tabell} AS {$this->tabell}\n"
                            .	"LEFT JOIN {$tp}kontoer AS kontoer ON {$this->tabell}.konto = CONCAT(kontoer.id)\n",

            'where'			=>	"{$tp}{$this->tabell}.{$this->idFelt} = '$id'",

            'orderfields'	=>	"IF(krav IS NULL, 1, 0), IF(krav = '0', 1, 0), krav, IF(leieforhold IS NULL, 1, 0), leieforhold"

        ));
        if( isset( $resultat->data[0] ) ) {
            $this->data = (object)array(
                'id			'		=> $resultat->data[0]->id,
                'innbetaling'		=> $resultat->data[0]->id,
                'konto'				=> $resultat->data[0]->konto,
                'kontokode'			=> $resultat->data[0]->kontokode,
                'kontonavn'			=> $resultat->data[0]->kontonavn,
                'ocr_transaksjon'	=> $resultat->data[0]->ocr_transaksjon,
                'beløp'				=> 0,
                'betaler'			=> $resultat->data[0]->betaler,
                'ref'				=> $resultat->data[0]->ref,
                'merknad'			=> $resultat->data[0]->merknad,
                'registrerer'		=> $resultat->data[0]->registrerer,
                'dato'				=> new DateTime( $resultat->data[0]->dato ),
                'registrert'		=> new DateTime( $resultat->data[0]->registrert ),
                'delbeløp'			=> array(),
                'leieforhold'		=> array()
            );
            $this->id = $id;

            foreach( $resultat->data as $delbeløp ) {

                $this->data->beløp = bcadd( $this->data->beløp, $delbeløp->beløp, 6 );

                $this->data->delbeløp[] = (object)array(
                    'id'			=>	$delbeløp->innbetalingsid,
                    'beløp'			=>	$delbeløp->beløp,
                    'registrerer'	=>	$delbeløp->registrerer,
                    'registrert'	=>	new DateTime( $delbeløp->registrert ),
                    'leieforhold'	=>	$delbeløp->leieforhold
                                        ? $this->leiebase->hent(\Leieforhold::class, $delbeløp->leieforhold )
                                        : null,
                    'krav'			=>	$delbeløp->krav > 0
                                        ? $this->leiebase->hent(\Krav::class, $delbeløp->krav )
                                        : $delbeløp->krav
                );

                if($delbeløp->leieforhold) {
                    $this->data->leieforhold[$delbeløp->leieforhold]
                        = $this->leiebase->hent(\Leieforhold::class, $delbeløp->leieforhold );
                }
            }
        }
        else {
            $this->id = null;
            $this->data = null;
        }
    }

    /**
     * Last OCR
     *
     * Last OCR-informasjon fra databasen
     *
     * @throws Exception
     */
    protected function lastOcr() {
        $tp = $this->mysqli->table_prefix;

        if ( $this->data == null ) {
            $this->last();
        }

        $resultat = $this->mysqli->arrayData(array(
            'returnQuery'	=> true,

            'fields' =>			"ocr_filer.registrert,
                                ocr_filer.registrerer,
                                ocr_filer.fil_id,
                                ocr_transaksjoner.forsendelsesnummer,
                                ocr_transaksjoner.oppdragsnummer,
                                ocr_transaksjoner.transaksjonstype,
                                ocr_transaksjoner.transaksjonsnummer,
                                ocr_transaksjoner.oppgjørsdato,
                                ocr_transaksjoner.delavregningsnummer,
                                ocr_transaksjoner.løpenummer,
                                ocr_transaksjoner.beløp,
                                ocr_transaksjoner.kid,
                                ocr_transaksjoner.blankettnummer,
                                ocr_transaksjoner.arkivreferanse,
                                ocr_transaksjoner.oppdragsdato,
                                ocr_transaksjoner.debetkonto,
                                ocr_transaksjoner.fritekst\n",

            'source' => 		"`{$tp}ocr_transaksjoner` AS `ocr_transaksjoner`\n"
                            .	"LEFT JOIN `{$tp}ocr_filer` AS `ocr_filer` ON `ocr_transaksjoner`.`fil_id` = `ocr_filer`.`fil_id`\n",

            'where'			=>	"`{$tp}ocr_transaksjoner`.`id` = '{$this->data->ocr_transaksjon}'"

        ));
        if( $resultat->totalRows ) {
            $this->data->ocr = $resultat->data[0];

            $this->data->ocr->registrert	= $this->data->ocr->registrert
                                            ? new DateTime( $this->data->ocr->registrert )
                                            : null;

            $this->data->ocr->oppgjørsdato	= new DateTime( $this->data->ocr->oppgjørsdato );
            $this->data->ocr->oppdragsdato	= new DateTime( $this->data->ocr->oppdragsdato );

            switch( $this->data->ocr->transaksjonstype ) {

            case "10":
                $this->data->ocr->transaksjonsbeskrivelse = "Giro belastet konto";
                break;

            case "11":
                $this->data->ocr->transaksjonsbeskrivelse = "Faste Oppdrag";
                break;

            case "12":
                $this->data->ocr->transaksjonsbeskrivelse = "Direkte Remittering";
                break;

            case "13":
                $this->data->ocr->transaksjonsbeskrivelse = "BTG (Bedrifts Terminal Giro)";
                break;

            case "14":
                $this->data->ocr->transaksjonsbeskrivelse = "SkrankeGiro";
                break;

            case "15":
                $this->data->ocr->transaksjonsbeskrivelse = "AvtaleGiro";
                break;

            case "16":
                $this->data->ocr->transaksjonsbeskrivelse = "TeleGiro";
                break;

            case "17":
                $this->data->ocr->transaksjonsbeskrivelse = "Giro - betalt kontant";
                break;

            case "18":
                $this->data->ocr->transaksjonsbeskrivelse = "Reversering med KID";
                break;

            case "19":
                $this->data->ocr->transaksjonsbeskrivelse = "Kjøp med KID";
                break;

            case "20":
                $this->data->ocr->transaksjonsbeskrivelse = "Reversering med fritekst";
                break;

            case "21":
                $this->data->ocr->transaksjonsbeskrivelse = "Kjøp med fritekst";
                break;

            default:
                $this->data->ocr->transaksjonsbeskrivelse = "";
            }

        }
        else {
            $this->data->ocr = null;
        }

    }

    /**
     * Hent egenskaper
     *
     * @param string $egenskap
     * @param array $param
     * @return mixed|null
     * @throws Exception
     */
    public function hent($egenskap, $param = array()) {
        if( !$this->id ) {
            return null;
        }

        switch( $egenskap ) {

        case "beløp":
        case "betaler":
        case "dato":
        case "delbeløp":
            // Hvert delbeløp er et objekt med egenskapene:
            //	id (heltall)
            //	beløp (tall)
            //	registrerer (streng)
            //	registrert (DateTime-objekt)
            //	leieforhold (Leieforhold-objekt eller null)
            //	krav (Krav-objekt eller null)
        case "id":
        case "innbetaling":
        case "konto":
        case "kontokode":
        case "kontonavn":
        case "leieforhold":
        case "merknad":
        case "ocr_transaksjon":
        case "ref":
        case "registrerer":
        case "registrert":
        {
            if ( $this->data == null ) {
                $this->last();
            }
            return $this->data->$egenskap;
        }

        case "ikkeKontert": {
            $ikkeKontert = 0;
            foreach( $this->hent('delbeløp') as $del ) {
                if($del->leieforhold === null) {
                    $ikkeKontert += $del->beløp;
                }
            }
            return $ikkeKontert;
        }

        case "ikkeFordelt": {
            $ikkeFordelt = 0;
            foreach( $this->hent('delbeløp') as $del ) {
                if($del->krav === null) {
                    $ikkeFordelt += $del->beløp;
                }
            }
            return $ikkeFordelt;
        }

        case "ocr": {
            if ( !isset( $this->data->ocr ) ) {
                $this->lastOcr();
            }

            /** @var stdClass|null $ocr */
            $ocr = $this->data->$egenskap;
            return $ocr;
        }

        default: {
            return null;
        }
        }
    }

    /**
     * Opprett
     *
     * Oppretter en ny innbetaling i databasen og tildeler egenskapene til dette objektet
     *
     * @param array|stdClass $egenskaper Alle egenskapene det nye objektet skal initieres med
     * @return $this|bool
     * @throws Exception
     */
    public function opprett($egenskaper = array()) {
        throw new Exception('Bruk av utdatert metode ' . __METHOD__);
    }
}