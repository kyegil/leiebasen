<?php

namespace Kyegil\Leiebasen;

use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Exception;
use IntlDateFormatter;
use Kyegil;
use Kyegil\CoreModel\CoreModel;
use Kyegil\CoreModel\CoreModelCollectionFactory;
use Kyegil\CoreModel\CoreModelFactory;
use Kyegil\CoreModel\CoreModelRepository;
use Kyegil\CoreModel\Interfaces\AppInterface;
use Kyegil\CoreModel\Interfaces\CoreModelRepositoryInterface;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Epostprosessor\AbstraktProsessor as EpostProsessor;
use Kyegil\Leiebasen\Interfaces\AutorisererInterface;
use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Delkravtypesett;
use Kyegil\Leiebasen\Modell\Eav\Verdi as EavVerdi;
use Kyegil\Leiebasen\Modell\Innbetaling as InnbetalingsModell;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Modell\Leieforhold as LeieforholdModell;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan\Avdrag;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan\Avdragsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Depositum;
use Kyegil\Leiebasen\Modell\Leieforhold\Depositumsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Fbo;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav as KravModell;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Oppsigelse;
use Kyegil\Leiebasen\Modell\Leieforhold\Purring as PurringModell;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Modell\Leieforhold\Regningsett;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjektsett;
use Kyegil\Leiebasen\Modell\Ocr\Transaksjon;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Personsett;
use Kyegil\Leiebasen\Nets\Kid;
use Kyegil\Leiebasen\Nets\Oppkopling;
use Kyegil\Leiebasen\Nets\Prosessor as NetsProsessor;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\Leiebasen\Sms\AbstraktProsessor as SmsProsessor;
use Kyegil\Leiebasen\Visning\felles\epost\purring\Epostpurring;
use Kyegil\MysqliConnection\MysqliConnection;
use Kyegil\Nets\Forsendelse\Oppdrag\AvtaleGiro\BetalingskravOppdrag;
use Kyegil\Nets\Forsendelse\Oppdrag\AvtaleGiro\Sletteoppdrag;
use Kyegil\Returi\Returi;
use Kyegil\ViewRenderer\ViewFactory;
use Kyegil\ViewRenderer\ViewInterface;
use mysqli;
use Psr\Log\LoggerInterface;
use stdClass;
use Throwable;
use function mb_strlen;
use function mb_strtolower;
use function mb_strtoupper;
use function mb_substr;

/**
 * Class Kyegil\Leiebasen\Leiebase
 */
class Leiebase
{
    const VARSELNIVÅ_ALARM = 'alarm';
    const VARSELNIVÅ_ADVARSEL = 'advarsel';
    const VARSELNIVÅ_ORIENTERING = 'orientering';

    const EPOST_TRANSPORT_PHP = 'php';
    const EPOST_TRANSPORT_TEST_MAIL = 'testmailer';
    const SMS_TRANSPORT_VOID = '';

    const EPOST_TRANSPORT_PHPMAILER = 'phpmailer';

    public static $config;

    /** @var class-string<EpostProsessor>[] */
    public static $epostprosessorMapping = [
        self::EPOST_TRANSPORT_TEST_MAIL => Kyegil\Leiebasen\Epostprosessor\TestMail::class,
        self::EPOST_TRANSPORT_PHP => Kyegil\Leiebasen\Epostprosessor\Php::class,
        self::EPOST_TRANSPORT_PHPMAILER => Kyegil\Leiebasen\Epostprosessor\PhpMailer::class
    ];

    /** @var class-string<SmsProsessor>[] */
    public static $smsProsessorMapping = [
        self::SMS_TRANSPORT_VOID => Kyegil\Leiebasen\Sms\VoidSms::class,
    ];

    /**
     * @var string
     */
    protected $filarkiv;

    /**
     * @var MysqliConnection
     */
    public $mysqli; // object - MySQLi-forbindelsen

    /**
     * Viktige ting som trenger oppmerksomhet ved innlogging.
     * Meldingene er gruppert i kategori
     *  0-teknisk alarm
     *  1-advarsel
     *  2-orientering/påminnelse
     * @var array[]
     */
    protected $advarsler; // Viktige ting som trenger oppmerksomhet ved innlogging. Meldingene er gruppert i kategori 0-teknisk alarm, 1-advarsel, og 2-orientering/påminnelse

    /**
     * @var AutorisererInterface
     */
    protected $autoriserer; // grensesnitt mot autoriseringsskript

    /**
     * @var array
     */
    public array $bruker = [];

    /**
     * @var string
     */
    public $ext_bibliotek;

    /**
     * @var
     */
    public $fra; // Angivelse av fradato i oppslag

    /**
     * @var array
     */
    public array $GET = [];

    /**
     * @var array
     */
    public $hoveddata;

    /**
     * @var string
     */
    public $http_host;

    /**
     * @var bool
     */
    public bool $live; // Sett denne til false for å hindre epostsendinger etc.

    /**
     * @var string
     */
    public string $mal = '';

    /**
     * @var array[ område: string, ?leieforhold: string ]
     */
    public array $område = [];

    /**
     * @var
     */
    public $oppslag;

    /**
     * @var array
     */
    public array $POST = [];

    /**
     * @var Returi
     */
    public Returi $returi; // Hele RETURI samles i dette objektet

    /**
     * @var string
     */
    public $root;

    /**
     * @var
     */
    public $til; // Angivelse av tildato i oppslag

    /**
     * @var string
     */
    public $tittel = 'leiebasen';

    /**
     * @var array|null
     */
    protected ?array $valg;

    /**
     * @var array
     */
    public array $sorteringsegenskap = [];    // Brukes ved sortering av objekter

    /**
     * @var array
     */
    public array $tillegg = [];    // Tillegg

    /**
     * @var array
     */
    public array $knagger = [];    // Ulike handlinger knyttet til framdriftsstadier

    /** @var LoggerInterface */
    public $logger;

    /**
     * @var object $mellomlager
     */
    public $mellomlager;
    /** @var AppInterface */
    private $coreModelImplementering;
    /** @var ModellFabrikk */
    public $modellFabrikk;
    /** @var ModellOppbevaring */
    public $modellOppbevaring;
    /** @var SamlingsFabrikk */
    public $samlingsFabrikk;
    /** @var ViewFactory */
    protected $viewFactory;
    /** @var Oppkopling */
    protected $netsForbindelse = [];
    /** @var array */
    protected $tilgjengeligeSpråk = [
        'nb-NO' => 'Norsk (Bokmål)'
    ];

    /** @var Utskriftsprosessor */
    protected $utskriftsprosessor;

    /** @var NetsProsessor */
    protected $netsProsessor;

    /** @var $språk */
    private $språk;

    /** @var DatabaseInstallerer */
    private $databaseInstallerer;

    /** @var EpostProsessor */
    protected $epostprosessor;

    /** @var SmsProsessor */
    protected Sms\AbstraktProsessor $smsProsessor;

    /** @var IntlDateFormatter */
    public IntlDateFormatter $dateFormatter;

    /**
     * array_merge_recursive does indeed merge arrays, but it converts values with duplicate
     * keys to arrays rather than overwriting the value in the first array with the duplicate
     * value in the second array, as array_merge does. I.e., with array_merge_recursive,
     * this happens (documented behavior):
     *
     * array_merge_recursive(array('key' => 'org value'), array('key' => 'new value'));
     *     => array('key' => array('org value', 'new value'));
     *
     * array_merge_recursive_distinct does not change the datatypes of the values in the arrays.
     * Matching keys' values in the second array overwrite those in the first array, as is the
     * case with array_merge, i.e.:
     *
     * array_merge_recursive_distinct(array('key' => 'org value'), array('key' => 'new value'));
     *     => array('key' => array('new value'));
     *
     * Parameters are passed by reference, though only for performance reasons. They're not
     * altered by this function.
     *
     * @param array $array1
     * @param array $array2
     * @return array
     * @author Daniel <daniel (at) danielsmedegaardbuus (dot) dk>
     * @author Gabriel Sobrinho <gabriel (dot) sobrinho (at) gmail (dot) com>
     */
    public static function array_merge_recursive_distinct(array &$array1, array &$array2): array
    {
        $merged = $array1;

        foreach ($array2 as $key => &$value) {
            if (!is_numeric($key) && is_array($value) && isset ($merged [$key]) && is_array($merged [$key])) {
                $merged [$key] = self::array_merge_recursive_distinct($merged [$key], $value);
            } else if (is_numeric($key)) {
                $merged[] = $value;
            } else {
                $merged [$key] = $value;
            }
        }

        return $merged;
    }

    /**
     * Set config and add additional configs
     *
     * @param $config
     */
    public static function setConfig($config)
    {
        self::$config = $config;
        $additionalConfigs = $config['additional_configs'] ?? [];
        foreach ($additionalConfigs as $additionalConfigFile) {
            $additionalConfig = include (self::$config['leiebasen']['server']['root'] . $additionalConfigFile) ?? [];
            self::$config = self::array_merge_recursive_distinct($additionalConfig, self::$config);
        }
    }

    /**
     * Returns the preferred class name according to config preferences
     *
     * @param class-string $classOrInterface
     * @return class-string
     */
    public static function getClassPreference(string $classOrInterface): string
    {
        return self::$config['class_preferences'][$classOrInterface] ?? $classOrInterface;
    }

    /**
     * Returnerer et tidsrom dersom gyldig angivelse er gitt, og null ellers
     *
     * @param string|null $duration
     * @param bool $allowNull
     * @return DateInterval|null
     */
    public static function parseDateInterval(?string $duration, bool $allowNull = true): ?DateInterval
    {
        if(empty(trim($duration))) {
            return $allowNull ? null : new DateInterval('P0D');
        }
        try {
            return preg_match('/(P([1-9]|[1-9][0-9])([YWDMH])|PT([1-9]|[1-9][0-9])H)/', strtoupper($duration))
                ? new DateInterval(strtoupper($duration))
                : null;
        } catch (Exception $e) {
            return $allowNull ? null : new DateInterval('P0D');
        }
    }

    /**
     * @param object|string $subject
     * @param string $method
     * @param array $args
     * @return array $args
     */
    public static function preStatic($subject, string $method, array $args): array
    {
        return EventManager::getInstance()->triggerEvent(
            is_object($subject) ? get_class($subject) : $subject,
            'pre' . self::ucfirst($method),
            is_object($subject) ? $subject : null,
            $args
        );
    }

    /**
     * @param object|string $subject
     * @param $method
     * @param $result
     * @return mixed
     */
    public static function postStatic($subject, $method, $result, array $args = [])
    {
        array_unshift($args, $result);
        $result = EventManager::getInstance()->triggerEvent(
            is_object($subject) ? get_class($subject) : $subject,
            'post' . self::ucfirst($method),
            is_object($subject) ? $subject : null,
            $args
        );
        return isset($result) ? reset($result) : null;
    }

    public static function htmlInputName(array $array, string $base = ''): array
    {
        $result = [];
        foreach($array as $index => $verdi) {
            if(is_iterable($verdi)) {
                $result = array_merge($result, self::htmlInputName((array)$verdi, $base ? "{$base}[$index]" : $index));
            }
            else {
                $result[$base ? "{$base}[$index]" : $index] = $verdi;
            }
        }
        return $result;
    }

    /**
     * Multibyte proof version of ucfirst
     *
     * @param string $string
     * @return string
     */
    public static function ucfirst(string $string): string
    {
        return mb_strtoupper(mb_substr($string, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr($string, 1, null, 'UTF-8');
    }

    /**
     * Bankfridager
     * Returnerer liste med alle røde dager (ikke vanlige helger) i inneværende eller angitt år
     *
     * @param int|null $år
     * @return string[] Liste med datostrenger i formatet 'm-d'
     * @throws Exception
     */
    public static function bankfridager(int $år = null): array
    {
        $år = $år ?? date('Y');

        $helligdager = ['01-01', '05-01', '05-17', '12-24', '12-25', '12-26'];

        $påskesøndag = date_create_immutable("{$år}-03-21")
            ->add(new DateInterval('P' . easter_days($år) . 'D'));

        $skjærtorsdag = $påskesøndag->sub(new DateInterval('P3D'));
        $langfredag = $påskesøndag->sub(new DateInterval('P2D'));
        $andrePåskedag = $påskesøndag->add(new DateInterval('P1D'));
        $kristiHimmelfartsdag = $påskesøndag->add(new DateInterval('P39D'));
        $førstePinsedag = $påskesøndag->add(new DateInterval('P49D'));
        $andrePinsedag = $påskesøndag->add(new DateInterval('P50D'));

        $helligdager[] = $skjærtorsdag->format('m-d');
        $helligdager[] = $langfredag->format('m-d');
        $helligdager[] = $andrePåskedag->format('m-d');
        $helligdager[] = $kristiHimmelfartsdag->format('m-d');
        $helligdager[] = $førstePinsedag->format('m-d');
        $helligdager[] = $andrePinsedag->format('m-d');

        sort($helligdager);
        return $helligdager;
    }

    /**
     * @param DateTimeImmutable $forfallsdato
     * @return DateTimeImmutable
     * @throws Exception
     */
    public static function førsteBankdag(DateTimeImmutable $forfallsdato): DateTimeImmutable
    {
        while (
            in_array($forfallsdato->format('m-d'), self::bankfridager())
            || $forfallsdato->format('N') > 5
        ) {
            $forfallsdato = $forfallsdato->add(new DateInterval('P1D'));
        }
        return $forfallsdato;
    }

    /**
     * Liste
     *
     * Setter sammen ei tekstliste over innholdet i et array
     *
     * @param array $array
     * @param string $skillestreng
     * @param string $sisteskillestreng
     * @return string
     */
    public static function liste(array $array = [], string $skillestreng = ", ", string $sisteskillestreng = " og "): string
    {
        $array = array_values($array);
        $streng = '';
        $ant = count($array);
        foreach ($array as $nr => $verdi) {
            $streng .= $verdi;
            if ($nr < $ant - 2) {
                $streng .= $skillestreng;
            }
            if ($nr == $ant - 2) {
                $streng .= $sisteskillestreng;
            }
        }
        return $streng;
    }

    /**
     * @param string $eier
     * @return string
     * @link https://www.sprakradet.no/sprakhjelp/Skriveregler/tegn/Apostrof/#apostrof
     */
    public static function genitivApostrof(string $eier): string
    {
        if (
            is_numeric(substr($eier, -1, 1))
            || substr($eier, -2, 1) != ' '
            || substr($eier, -3, 1) != ' '
            || mb_strlen($eier) < 3
        ) {
            return "{$eier}'s";
        }

        if (in_array(substr($eier, -1, 1), ['s', 'x', 'z'])) {
            return "{$eier}'";
        }
        return "{$eier}s";
    }

    /**
     * Periodeformat
     *
     * Funksjon som formaterer en tidsperiode
     *
     * @param string|DateInterval $periode Verdien som skal formateres
     * @param bool $somIso8601 På for å returnere perioden som interval_spec
     * @param bool $utelatEntall På for å utelate ett-tallet i 1 måned, en uke etc.
     * @return string
     */
    public static function periodeformat($periode, bool $somIso8601 = false, bool $utelatEntall = false): string
    {

        if (!is_a($periode, DateInterval::class)) {
            $periode = self::parseDateInterval($periode);
        }

        if ($somIso8601) {
            return Modell::prepareIso8601IntervalSpecification($periode);
        }

        $resultat = [];

        if($periode) {
            if ($periode->y) {
                $resultat[] = "{$periode->y} år";
            }

            if ($periode->m > 1) {
                $resultat[] = "{$periode->m} måneder";
            } else if ($periode->m) {
                $resultat[] = "1 måned";
            }

            if ($periode->d == 7) {
                $resultat[] = "1 uke";
            } else if ($periode->d and $periode->d % 7 == 0) {
                $resultat[] = ($periode->d / 7) . " uker";
            } else if ($periode->d > 1) {
                $resultat[] = "{$periode->d} dager";
            } else if ($periode->d) {
                $resultat[] = "1 dag";
            }

            if ($periode->h > 1) {
                $resultat[] = "{$periode->h} timer";
            } else if ($periode->h) {
                $resultat[] = "1 time";
            }

            if ($periode->i > 1) {
                $resultat[] = "{$periode->i} minutter";
            } else if ($periode->i) {
                $resultat[] = "1 minutt";
            }

            if ($periode->s > 1) {
                $resultat[] = "{$periode->s} sekunder";
            } else if ($periode->s) {
                $resultat[] = "1 sekund";
            }

            if ($utelatEntall && count($resultat) == 1 && strpos($resultat[0], '1 ') === 0) {
                $resultat[0] = substr($resultat[0], 2);
            }
        }

        return Leiebase::liste($resultat);
    }

    /**
     * @param string $oppslag
     * @param string|null $id
     * @param string|null $oppdrag
     * @param array $queryParams
     * @param string $område
     * @return string
     */
    public static function url(
        string $oppslag,
        ?string $id = null,
        ?string $oppdrag = null,
        array $queryParams = [],
        string $område = ''
    ): string {
        $url = Leiebase::$config['leiebasen']['server']['installation_url'] ?? '';
        unset($queryParams['område']);
        unset($queryParams['oppslag']);
        unset($queryParams['id']);
        unset($queryParams['oppdrag']);

        $queryParams = array_filter($queryParams);
        ksort($queryParams);

        $url = implode('/', array_filter([$url, $område, $oppslag, $id, $oppdrag]));
        if($queryParams) {
            $url .= '?' . http_build_query($queryParams);
        }
        return $url;
    }

    /**
     * @return string
     */
    public static function currentUrl(
    ): string {
        return self::url($_GET['oppslag'] ?? '', $_GET['id'] ?? null, $_GET['oppdrag'] ?? null, $_GET, $_GET['område'] ?? '');
    }

    /**
     * Kyegil\Leiebasen\Leiebase constructor.
     * @param object[] $di Dependency Injection objects
     * @param string[] $config Configurations
     * @throws Exception|Throwable
     */
    public function __construct(array $di = [], array $config = [])
    {
        self::$config = array_merge(self::$config, $config);

        $diRequired = [
            MysqliConnection::class,
            LoggerInterface::class,
            CoreModelCollectionFactory::class,
            CoreModelFactory::class,
            CoreModelRepository::class,
        ];
        foreach ($diRequired as $class) {
            if (!isset($di[$class]) || !is_a($di[$class], $class)) {
                throw new Exception("Required instance of " . $class . " has not been injected to " . static::class . " constructor");
            }
        }

        global $leiebase;

        $this->ext_bibliotek = $this->ext_bibliotek ?: self::$config['leiebasen']['config']['extjs_version'] ?? '';
        $this->filarkiv = self::$config['leiebasen']['server']['file_storage'] ?? '';
        $this->http_host = self::$config['leiebasen']['server']['installation_url'] ?? '';
        $this->root = self::$config['leiebasen']['server']['root'] ?? '';
        $this->live = boolval(self::$config['leiebasen']['server']['production'] ?? false);
        settype($this->mellomlager, 'object');
        settype($this->mellomlager->camelCase, 'array');
        settype($this->mellomlager->underscore, 'array');

        $leiebase = $this;

        $this->logger = $di[LoggerInterface::class];
        $this->mysqli = $di[MysqliConnection::class];
        $this->mysqli->setLogger(CoreModel::$logger ?? $this->logger);

        CoreModel::$tablePrefix = self::$config['leiebasen']['db']['prefix'] ?? '';
        $this->modellFabrikk = $di[CoreModelFactory::class];
        $this->modellOppbevaring = $di[CoreModelRepository::class];
        $this->samlingsFabrikk = $di[CoreModelCollectionFactory::class];

        $templateFolderPaths = self::$config['leiebasen']['server']['template_folders'] ?? [];

        if (file_exists($this->root . DatabaseInstallerer::FLAGG)) {
                header('Refresh: 1;');
            die(file_get_contents($this->root . DatabaseInstallerer::FLAGG) . '<br><br>Vennligst vent...');
        }

        $this->viewFactory = new ViewFactory(
            $this->hentCoreModelImplementering(),
            $templateFolderPaths
        );
        $this->viewFactory
            ->setBaseNameSpace(Visning::class)
            ->setDefaultViewClass(Visning::class);

        $this->settUtskriftsprosessor(new Utskriftsprosessor($this, $this->logger, $this->mysqli));

        date_default_timezone_set("Europe/Oslo");
        setlocale(LC_TIME, 'nb_NO', 'nb_NO.utf8');
        setlocale(LC_COLLATE, 'nb_NO', 'nb_NO.utf8');
        $this->dateFormatter = new IntlDateFormatter('nb_NO', IntlDateFormatter::FULL, IntlDateFormatter::FULL);

        $installererKlasse = self::getClassPreference(DatabaseInnhold::class);
        $this->databaseInstallerer = new $installererKlasse($this, $this->mysqli);
        if (!$this->databaseInstallerer->erOppdatert()) {
            $this->databaseInstallerer->oppdater();
                header("Refresh: 0;\r\n");
            die();
        }

        // Oppretter Kyegil\Leiebasen\Leiebase::POST og Kyegil\Leiebasen\Leiebase::GET klare for mysql-innsmetting
        $this->escape();

        if (isset($_GET['oppslag'])) {
            $this->oppslag = $_GET['oppslag'];
        }
        $this->returi = new Returi(self::$config['leiebasen']['config']['default_url'] ?? '');
        $this->returi->setCurrentLocationProvider([self::class, 'currentUrl']);
    }


    /**
     * Sjekker om brukeren har adgang til et angitt område.
     *    Samtidig sjekkes og oppdateres økten, og innlogging kreves om nødvendig
     *    $this->bruker fylles også ut som følger:
     *        navn:        Fullt navn
     *        id:            Brukerens id, i samsvar med personadressekort
     *        brukernavn: Brukernavn for innlogging
     *        epost:        Brukerens epostadresse
     *
     * @param string $katalog Området det ønskes adgang til
     * @param int $leieforhold Aktuelt leieforhold dersom området er 'mine-sider'
     * @return bool             Sant dersom innlogget bruker har adgang til området
     * @throws Exception
     */
    public function adgang($katalog = "", $leieforhold = null)
    {
        /** @var Autoriserer $autoriserer */

        $autoriserer = $this->hentAutoriserer();

        settype($leieforhold, 'string');
        settype($leieforhold, 'integer');

        if (!$leieforhold && isset($this->område['leieforhold'])) {
            $leieforhold = $this->område['leieforhold'];
        }

        $this->bruker['navn'] = $autoriserer->hentNavn();
        $this->bruker['id'] = $autoriserer->hentId();
        $this->bruker['brukernavn'] = $autoriserer->hentBrukernavn();
        $this->bruker['epost'] = $autoriserer->hentEpost();

        if ($this->coreModelImplementering) {
            $this->coreModelImplementering->bruker = $this->bruker;
        }
        $autoriserer->krevIdentifisering();

        if ($katalog == "sentral") {
            return true;
        }

        $resultat = $this->mysqli->arrayData(array(
            'source' => "adganger",
            'returnQuery' => true,
            'where' => "personid = '{$this->bruker['id']}'
            AND adgang = '{$katalog}'"
                . ((in_array($katalog, ['mine-sider']) && (int)strval($leieforhold) > 0) ? " AND leieforhold = '{$leieforhold}'" : "")
        ));
        return (bool)$resultat->totalRows;
    }


    /**
     * @param $kontraktnr
     * @return string
     * @throws Exception
     */
    public function adresse($kontraktnr)
    {
        $adresse = $this->mysqli->select(array(
            'source' => "kontrakter LEFT JOIN personer ON kontrakter.regningsperson = personer.personid LEFT JOIN leieobjekter ON kontrakter.regningsobjekt = leieobjekter.leieobjektnr",
            'where' => "kontrakter.kontraktnr = " . $kontraktnr,
            'fields' => "regningsperson, regning_til_objekt, regningsobjekt, regningsadresse1, regningsadresse2, kontrakter.postnr AS kontraktpostnr, kontrakter.poststed AS kontraktpoststed, kontrakter.land AS kontraktland, adresse1 AS personadresse1, adresse2 AS personadresse2, personer.postnr AS personpostnr, personer.poststed AS personpoststed, personer.land AS personland, leieobjekter.navn AS leieobjektnavn, leieobjekter.gateadresse AS leieobjektadresse, leieobjekter.postnr AS leieobjektpostnr, leieobjekter.poststed AS leieobjektpoststed"
        ))->data[0];

        if ($adresse->regning_til_objekt) {
            $resultat = ($adresse->leieobjektnavn ? "{$adresse->leieobjektnavn}\n" : "")
                . $adresse->leieobjektadresse . "\n"
                . "{$adresse->leieobjektpostnr} {$adresse->leieobjektpoststed}";
        } else if ($adresse->regningsperson) {
            $resultat = ($adresse->personadresse1 ? ($adresse->personadresse1 . "\n") : "")
                . ($adresse->personadresse2 ? ($adresse->personadresse2 . "\n") : "")
                . $adresse->personpostnr . " " . $adresse->personpoststed . "\n"
                . ($adresse->personland != "Norge" ? $adresse->personland : "");
        } else {
            $resultat = ($adresse->regningsadresse1 ? ($adresse->regningsadresse1 . "\n") : "")
                . ($adresse->regningsadresse2 ? ($adresse->regningsadresse2 . "\n") : "")
                . $adresse->kontraktpostnr . " " . $adresse->kontraktpoststed . "\n"
                . ($adresse->kontraktland != "Norge" ? $adresse->kontraktland : "");
        }

        return $resultat;
    }

    /**
     * @param string $sql
     * @return mixed|object
     * @throws Exception
     */
    public function arrayData($sql)
    {
        $data = $this->mysqli->select(array(
            'sql' => $sql
        ));
        $data = json_decode(json_encode($data), true);
        settype($data['data'], 'array');
        return $data;
    }

    /**
     * Beregn utestående
     * Beregner utestående krav på bestemte datoer
     *
     * @param string[]|DateTimeInterface[] $datoer Datoen(e) utestående skal beregnes for
     * @param bool $ignorerForskuddsbetaling Ignorer forskuddsbetaling
     * @return array                            Utestående beløp
     * @throws Exception
     */
    public function beregnUtestående(array $datoer = [], bool $ignorerForskuddsbetaling = false): array
    {
        $tp = $this->mysqli->table_prefix;
        $alleTransaksjoner = [];
        $resultat = [];
        settype($datoer, 'array');

        foreach ($datoer as &$dato) {
            if ($dato instanceof DateTimeInterface) {
                $dato = $dato->format('Y-m-d');
            }
        }
        sort($datoer);

        if ($ignorerForskuddsbetaling) {
            $sql = <<<SQL
                SELECT `dato`, `beløp`, (@saldo := @saldo + `beløp`) AS `saldo`
                FROM (
                    SELECT `dato`, SUM(`beløp`) AS `beløp`
                    FROM ((
                            SELECT `kravdato` AS `dato`, `beløp` AS `beløp`
                            FROM `{$tp}krav` AS `krav`
                        ) UNION ALL (
                            SELECT GREATEST(`innbetalinger`.`dato`, `krav`.`kravdato`) AS `dato`, -`innbetalinger`.`beløp` AS `beløp`
                            FROM `{$tp}innbetalinger` AS `innbetalinger` INNER JOIN `{$tp}krav` AS `krav` ON `innbetalinger`.`krav` = `krav`.`id`
                        )
                        ORDER BY `dato`
                    ) AS `transaksjoner`
                    GROUP BY `dato`
                ) AS `summering`
                SQL;
        } else {
            $sql = <<<SQL
                SELECT `dato`, `beløp`, (@saldo := @saldo + `beløp`) AS `saldo`
                FROM (
                    SELECT `dato`, SUM(`beløp`) AS `beløp`
                    FROM ((
                            SELECT `kravdato` AS `dato`, `beløp` AS `beløp`
                            FROM `{$tp}krav` AS `krav`
                        ) UNION ALL (
                            SELECT `dato` AS `dato`, -`beløp` AS `beløp`
                            FROM `{$tp}innbetalinger` AS `innbetalinger`
                        )
                        ORDER BY `dato`
                    ) AS `transaksjoner`
                    GROUP BY `dato`
                ) AS `summering`
                SQL;
        }

        $this->mysqli->query("SET @saldo:=0;");
        $historikk = $this->mysqli->select(['sql' => $sql]);

        foreach ($historikk->data as $bevegelse) {
            $alleTransaksjoner[$bevegelse->dato] = $bevegelse->saldo;
        }

        if (!$datoer) {
            return $alleTransaksjoner;
        }

        unset($dato);
        foreach ($datoer as $dato) {
            if (isset($alleTransaksjoner[$dato])) {
                $resultat[$dato] = $alleTransaksjoner[$dato];
            } else if (strlen($dato) == 10) {
                foreach($alleTransaksjoner as $transaksjonDato => $transaksjonBeløp) {
                    if($transaksjonDato > $dato) {
                        break;
                    }
                    $resultat[$dato] = $transaksjonBeløp;
                }
            } else {
                foreach ($alleTransaksjoner as $transaksjonDato => $transaksjonBeløp) {
                    if (substr($transaksjonDato, 0, strlen($dato)) == $dato) {
                        settype($resultat[$dato], 'array');
                        $resultat[$dato][] = $transaksjonBeløp;
                    }
                }
            }
        }

        $siste = 0;
        foreach ($resultat as &$saldo) {
            if (is_array($saldo)) {
                if ($saldo) {
                    $saldo = (array_sum($saldo) / count($saldo));
                } else {
                    $saldo = $siste;
                }
            } else {
                $siste = $saldo;
            }
        }

        return $resultat;
    }


    /**
     * @param $p
     * @return bool
     * @throws Exception
     */
    public function brukPolett($p)
    {
        $this->mysqli->query("DELETE FROM poletter WHERE utløper < " . time());
        $match = $this->arrayData("SELECT * FROM poletter WHERE polett = '$p'");
        if (count($match['data']) != 1) {
            return false;
        } else if (!$this->mysqli->query("DELETE FROM poletter WHERE polett = '$p'")) {
            return false;
        }
        return true;
    }


    /**
     * @param float $verdi
     * @return string
     * @deprecated
     * @see \Kyegil\Fraction\Fraction::parseDecimal()
     */
    public function brok($verdi)
    {
        $v = round($verdi, 4);
        for ($i = 120; $i > 1; $i--) {
            for ($ii = 1; $ii < $i; $ii++) {
                $uttrykk = "$ii/$i";
                if (round($this->evaluerAndel($uttrykk), 4) == round($v, 4)) {
                    $resultat = $uttrykk;
                }
            }
        }
        if (!isset($resultat)) {
            $resultat = $verdi;
        }
        return $resultat;
    }


    /**
     * returnerer et array med alle kontraktnr i et leieobjekt for en gitt dato
     *
     * @param int $leieobjektnr
     * @param int $dato
     * @return array
     * @throws Exception
     */
    public function dagensBeboere($leieobjektnr, $dato = 0)
    {
        if (!$dato) $dato = time();
        $resultat = array();
        $sql = "SELECT MAX(kontrakter.kontraktnr) AS kontraktnr, kontrakter.leieforhold
                FROM `kontrakter` LEFT JOIN oppsigelser ON kontrakter.leieforhold = oppsigelser.leieforhold
                WHERE leieobjekt = '$leieobjektnr'
                AND fradato <= '" . date('Y-m-d', $dato) . "'
                AND (fristillelsesdato IS NULL OR fristillelsesdato > '" . date('Y-m-d', $dato) . "')
                GROUP BY kontrakter.leieforhold";
        $a = $this->arrayData($sql);
        foreach ($a['data'] as $kontrakt) {
            $resultat[] = $kontrakt['kontraktnr'];
        }
        return $resultat;
    }


    /**
     * Print design
     */
    public function design()
    {
        echo "\n<div id=\"panel\"></div>\n";
    }


    /**
     * Funksjon som klargjør alle GET- og POST-verdier for å smettes inn i databasen
     */
    public function escape()
    {
        $GET = $_GET;
        $POST = $_POST;
        array_walk_recursive($GET, function ($value) {
            return $this->mysqli->real_escape_string($value);
        });
        array_walk_recursive($POST, function ($value) {
            return $this->mysqli->real_escape_string($value);
        });
        $this->GET = $GET;
        $this->POST = $POST;
        if (isset($_GET['fra']))
            $this->fra = $this->GET['fra'];
        if (isset($_GET['til']))
            $this->til = $this->GET['til'];
        if (isset($_POST['fra']) && $this->POST['fra']) {
            $this->fra = $this->POST['fra'];
        }
        if (isset($_POST['til']) && $this->POST['til']) {
            $this->til = $this->POST['til'];
        }
    }

    /**
     * Epostpurringer
     *
     * @param PurringModell[] $purringer
     * @return $this
     */
    public function epostpurring($purringer = array())
    {
        $emne = "Betalingspåminnelse";

        foreach ($purringer as $purring) {
            try {
                /** @var LeieforholdModell $leieforhold */
                $leieforhold = $purring->hentLeieforhold();
                $brukerepost = $leieforhold->hentEpost();
                if ($purring->hentPurremåte() == 'epost' && $brukerepost) {
                    $this->sendMail((object)array(
                        'to' => implode(",", $brukerepost),
                        'subject' => $emne,
                        'html' => $this->vis(Epostpurring::class, ['purring' => $purring]),
                        'text' => $this->vis(Epostpurring::class, ['purring' => $purring])
                            ->settMal('felles/epost/purring/Epostpurring.txt')
                            ->settMalForElementer(Visning\felles\html\purring\Kravlinje::class, 'felles/html/purring/Kravlinje.txt'),
                        'type' => 'epostpurring'
                    ));
                }
            } catch (Exception $e) {
                continue;
            }
        }
        return $this;
    }

    /**
     * Etasjerenderer
     *
     * @param string $v
     * @return string
     */
    public function etasjerenderer($v)
    {
        if ($v == '+') return "loft";
        else if ($v == '0') return "sokkel";
        else if ($v == '-1') return "kjeller";
        else if ((int)$v) return "$v. etg.";
        else return $v;
    }

    /**
     * @param $uttrykk
     * @return bool|mixed
     * @deprecated Skal erstattes med fraBrøk()
     */
    public function evaluerAndel($uttrykk)
    {
        $uttrykk = str_replace(",", ".", $uttrykk);
        $uttrykk = str_replace("%", "/100", $uttrykk);
        $uttrykk = str_replace(array(",", "%", " "), array(".", "/100", ""), $uttrykk);
        $andel = eval("return $uttrykk;");
        if ($andel > 1 or $andel < 0) return false;
        else return $andel;
    }

    /**
     * Multibyte- forenklet versjon av str_pad
     * Setter en streng til en fast lengde
     *
     * @param string $streng Strengen som skal formateres
     * @param int $lengde Lengden på den formaterte teksten
     * @param string $fyll Tegnet strengen fylles med
     * @param int $side STR_PAD_RIGHT = venstrejustering, STR_PAD_LEFT = høyrejustering
     * @return string           formatert streng
     */
    public function fastStrenglengde($streng, $lengde, $fyll = " ", $side = STR_PAD_RIGHT)
    {
        $streng = mb_substr($streng, 0, $lengde, 'UTF-8');
        $fyll = mb_substr($fyll, 0, 1, 'UTF-8');
        if ($side == STR_PAD_LEFT) {
            return str_repeat($fyll, $lengde - mb_strlen($streng, 'UTF-8')) . $streng;
        } else {
            return $streng . str_repeat($fyll, $lengde - mb_strlen($streng, 'UTF-8'));
        }
    }

    /**
     * @return BetalingskravOppdrag|null
     * @throws Exception
     */
    public function fboLagBetalingskravOppdrag(): ?BetalingskravOppdrag
    {
        if (!$this->hentValg('avtalegiro')) {
            return null;
        }

        $oppdragskonto = preg_replace('/[^0-9]+/', '', $this->hentValg('bankkonto'));

        $oppdrag = new BetalingskravOppdrag();
        $oppdrag->oppdragskonto = $oppdragskonto;
        $oppdrag->oppdragsnr = $this->hentNetsProsessor()->opprettOppdragsnummer();

        $this->fboLeggRegningerTilBetalingskravOppdrag($oppdrag);
        $this->fboLeggAvdragTilBetalingskravOppdrag($oppdrag);
        return count($oppdrag->transaksjoner) > 0 ? $oppdrag : null;
    }

    /**
     * @param BetalingskravOppdrag $oppdrag
     * @return $this
     * @throws Throwable
     */
    private function fboLeggRegningerTilBetalingskravOppdrag(BetalingskravOppdrag $oppdrag): Leiebase
    {
        $nesteForsendelse = $this->hentNetsProsessor()->nesteFboTrekkravForsendelse();
        /** @var Regningsett $regningsett */
        $regningsett = $this->hentSamling(Regning::class);
        $regningsett
            ->leggTilInnerJoinForKrav()
            ->leggTilLeftJoinForFboTrekk()
            ->leggTilBeløpFelt()
            ->leggTilUteståendeFelt()
            ->leggTilForfallFelt()
            ->leggTilInnerJoin([Fbo::hentTabell()], '`' . Fbo::hentTabell() . '`.`leieforhold` = `' . Regning::hentTabell() . '`.`leieforhold`');
        $regningsett->leggTilFilter(['`' . KravModell::hentTabell() . '`.`forfall` < DATE_ADD(NOW(), INTERVAL 1 YEAR)']);
        $regningsett->leggTilFilter(['`' . KravModell::hentTabell() . '`.`forfall` >= DATE_ADD(NOW(), INTERVAL 4 DAY)']);
        $regningsett->leggTilFilter(['`' . KravModell::hentTabell() . '`.`utestående` >' => 0]);
        $regningsett->leggTilFilter(['`' . Fbo\Trekk::hentTabell() . '`.`' . Fbo\Trekk::hentPrimærnøkkelfelt() . '`' => null]);
        $regningsett->leggTilFilter(['`' . Fbo::hentTabell() . '`.`registrert` <= CURDATE()']);
        $regningsett->leggTilFilter(['or' => [
            ['utskriftsdato' => null],
            ['utskriftsdato >= `' . Fbo::hentTabell() . '`.`registrert`'],
        ]]);

        if ($regningsett->hentAntall()) {
            $this->logger->info('NETS – Lager trekkoppdrag for ' . $regningsett->hentAntall() . ' regninger som skal betales med AvtaleGiro.');

            foreach ($regningsett as $regning) {
                $fristForNyttOppdrag = $regning->hentLeieforhold()->hentFbo()->oppdragsfrist($regning->hentForfall());

                $leieforhold = $regning->leieforhold;
                $fbo = $leieforhold->hentFbo();

                $bankvarsel = $fbo && $fbo->hentBankvarsel();

                /*
                 * Betalingskrav kan sendes med forfallsdato inntil 12 måneder frem i tid.
                 * Dersom banken skal varsle via sms kan forfallsdato være max 50 dager frem i tid
                 */
                $femtiDager = new DateInterval('P50D');
                if ($bankvarsel && $fbo->mobilnr && $regning->hentForfall() > date_create_immutable()->add($femtiDager)) {
                    $this->logger->info('NETS – Ignorerer regning ' . $regning->hentId() . ' fordi den er for langt fram i tid for bankvarsling via SMS.', ['forfall' => $regning->hentForfall()->format('Y-m-d'), 'bankvarsel' => (int)$bankvarsel]);
                    continue;
                }
                if ($fristForNyttOppdrag && $nesteForsendelse > $fristForNyttOppdrag) {
                    $this->logger->info('NETS – Ignorerer regning ' . $regning->hentId() . ' fordi det er for kort frist for varsling.', ['forfall' => $regning->hentForfall()->format('Y-m-d'), 'bankvarsel' => (int)$bankvarsel]);
                    continue;
                }
                /** @var Fbo\Trekk $avtalegiroTrekk */
                $avtalegiroTrekk = $this->nyModell(Fbo\Trekk::class, (object)[
                    'leieforhold' => $regning->leieforhold,
                    'regning' => $regning,
                    'kid' => $regning->kid,
                    'beløp' => $regning->hentUtestående(),
                    'oppdrag' => $oppdrag->oppdragsnr,
                    'forfallsdato' => $regning->hentForfall(),
                    'varslet' => $regning->hentUtskriftsdato(),
                    'mobilnr' => $fbo->mobilnr,
                    'egenvarsel' => !$bankvarsel
                ]);

                if (!$avtalegiroTrekk->hentId()) {
                    $this->logger->critical('NETS – Regning ' . $regning->hentId() . ' klarte ikke lagre avtale-giro-trekk.');
                    throw new Exception('Klarte ikke lagre avtale-giro-trekk for regning ' . $regning->hentId());
                }

                $transaksjon = $avtalegiroTrekk->hentNetsTransaksjon();
                $oppdrag->transaksjoner[] = $transaksjon;

                $this->logger->info('NETS – Regning ' . $regning->hentId() . ' lagt til i trekkoppdraget.');
            }
        }
        return $this;
    }

    /**
     * @param BetalingskravOppdrag $oppdrag
     * @return $this
     * @throws Throwable
     */
    private function fboLeggAvdragTilBetalingskravOppdrag(BetalingskravOppdrag $oppdrag): Leiebase
    {
        /** @var Avdragsett $avdragsett */
        $avdragsett = $this->hentSamling(Avdrag::class);
        $avdragsett
            ->leggTilBetalingsplanModell()
            ->leggTilLeftJoinForFboTrekk()
            ->leggTilInnerJoin([Fbo::hentTabell()], '`' . Fbo::hentTabell() . '`.`leieforhold` = `' . Betalingsplan::hentTabell() . '`.`leieforhold_id`');
        $avdragsett->leggTilFilter(['`' . Avdrag::hentTabell() . '`.`forfallsdato` > DATE_ADD(NOW(), INTERVAL 4 DAY)']);
        $avdragsett->leggTilFilter(['`' . Avdrag::hentTabell() . '`.`forfallsdato` < DATE_ADD(NOW(), INTERVAL 50 DAY)']);
        $avdragsett->leggTilFilter(['`' . Avdrag::hentTabell() . '`.`beløp` >' => 0]);
        $avdragsett->leggTilFilter(['`' . Fbo\Trekk::hentTabell() . '`.`' . Fbo\Trekk::hentPrimærnøkkelfelt() . '`' => null]);

        if ($avdragsett->hentAntall()) {
            $this->logger->info('NETS – Lager trekkoppdrag for ' . $avdragsett->hentAntall() . ' betalingsplan-avdrag som skal betales med AvtaleGiro.', ['avdrag' => $avdragsett->hentIdNumre()]);

            foreach ($avdragsett as $avdrag) {
                $leieforhold = $avdrag->betalingsplan->leieforhold;
                $fbo = $leieforhold->hentFbo();
                if (!$fbo || $avdrag->betalingsplan->utenomAvtalegiro) {
                    continue;
                }

                // Egenvarsel skal være sann i alle tilfeller hvor banken ikke trenger sende varsel.
                //	Dvs:
                // - Dersom betaler ikke ønsker varsel
                // - Dersom betaler ønsker varsel sendt via SMS
                // - Dersom leiebasen har epostadresse på leieforholdet
                $egenvarsel = !$fbo->varsel
                    || ($this->hentValg('avtalegiro_sms') && $fbo->mobilnr)
                    || $leieforhold->hentEpost();

                /** @var Fbo\Trekk $avtalegiroTrekk */
                $avtalegiroTrekk = $this->nyModell(Fbo\Trekk::class, (object)[
                    'leieforhold' => $leieforhold,
                    'regning' => null,
                    'kid' => $leieforhold->hentKid(),
                    'beløp' => $avdrag->hentBeløp(),
                    'oppdrag' => $oppdrag->oppdragsnr,
                    'forfallsdato' => $avdrag->hentForfallsdato(),
                    'mobilnr' => $fbo->mobilnr,
                    'egenvarsel' => $egenvarsel
                ]);

                if (!$avtalegiroTrekk->hentId()) {
                    $this->logger->critical('NETS – Betalingsplan-avdrag ' . $avdrag->hentId() . ' klarte ikke lagre avtale-giro-trekk.');
                    throw new Exception('Klarte ikke lagre avtale-giro-trekk for betalingsplan-avdrag ' . $avdrag->hentId());
                }

                $transaksjon = $avtalegiroTrekk->hentNetsTransaksjon();
                $transaksjon->fremmedreferanse = 'Avdrag betalingsplan';
                $transaksjon->spesifikasjon = 'Avdrag for ' . $avdrag->forfallsdato->format('d.m.Y') . ' i henhold til inngått betalingsplan: kr ' . number_format($avdrag->hentBeløp(), 2, ',', ' ');
                $oppdrag->transaksjoner[] = $transaksjon;
                $avdrag->settAvtalegiroTrekk($avtalegiroTrekk);

                $this->logger->info('NETS – Betalingsplan-avdrag ' . $avdrag->hentId() . ' lagt til i trekkoppdraget.');
            }
        }
        return $this;
    }

    /**
     * Oppretter sletteoppdrag for AvtaleGiro
     * for overføring til NETS
     *
     * AvtaleGiro-trekkene slettes dersom det har vært endring i utes†ående beløp eller forfallsdato
     * så fremt det er tid for å sende inn nytt krav,
     * eller dersom kravet er betalt eller slettet i sin helhet
     *
     * @return Sletteoppdrag|null
     * @throws Exception
     */
    public function fboLagSletteOppdrag(): ?Sletteoppdrag
    {
        if (!$this->hentValg('avtalegiro')) {
            return null;
        }

        $oppdragskonto = preg_replace('/[^0-9]+/', '', $this->hentValg('bankkonto'));
        $sletteoppdrag = new Sletteoppdrag();
        $sletteoppdrag->oppdragskonto = $oppdragskonto;
        $sletteoppdrag->oppdragsnr = $this->hentNetsProsessor()->opprettOppdragsnummer();
        $this->leggTrekkTilSletteoppdrag($sletteoppdrag);

        if ($sletteoppdrag->transaksjoner) {
            $this->logger->info('NETS – Sletteoppdrag med ' . count($sletteoppdrag->transaksjoner) . ' transaksjoner opprettet.');
            return $sletteoppdrag;
        }
        return null;
    }

    /**
     * Faste Betalingsoppdrag - Slett trekkrav
     * Sletter fbo trekkrav for regninger som har blitt endret siden innsending til Nets,
     * sånn at de kan sendes inn på nytt igjen
     *
     * @return Sletteoppdrag|null
     * @throws Throwable
     */
    public function forberedRegningsTrekkForSletting(): Leiebase
    {
        /** @var Fbo\Trekksett $trekkSett */
        $trekkSett = $this->hentSamling(Fbo\Trekk::class);
        $trekkSett->leggTilRegningModell();
        $trekkSett->leggTilLeftJoin(KravModell::hentTabell(),
            '`' . Fbo\Trekk::hentTabell() . '`.`gironr` = `' . KravModell::hentTabell() . '`.`gironr`');

        $trekkSett->leggTilFilter(['`fbo_trekkrav`.`gironr` IS NOT NULL'], true);
        $trekkSett->leggTilFilter(['til_sletting' => null]);
        $trekkSett->leggTilFilter(['`' . Fbo\Trekk::hentTabell() . '`.`forfallsdato` > CURDATE()'], true);

        /*
         * Kravet flagges for sletting dersom enten:
         * * Regningen mangler forfallsdato
         * * Forfallsdato har blitt endret
         * * Utestående beløp er endret
         */
        $trekkSett->leggTilHaving(['or' => [
            'MIN(`krav`.`forfall`)' => null,
            '`fbo_trekkrav`.`forfallsdato` != MIN(`krav`.`forfall`)',
            '`fbo_trekkrav`.`beløp` != SUM(`krav`.`utestående`)',
        ]]);
        $trekkSett->låsFiltre();

        if ($trekkSett->hentAntall()) {
            $this->logger->info('NETS – Flagger ' . $trekkSett->hentAntall() . ' trekk (for regninger) som har blitt endret for sletting.', ['trekk' => $trekkSett->hentIdNumre()]);

            foreach ($trekkSett as $feiltrekk) {
                $regning = $feiltrekk->regning;
                $utestående = $regning ? $regning->hentUtestående() : 0;
                $nesteForsendelse = $this->hentNetsProsessor()->nesteFboTrekkravForsendelse();
                $fbo = $feiltrekk->hentLeieforhold()->hentFbo();
                if ($fbo) {
                    $fristForNyttOppdrag = $fbo->oppdragsfrist($regning ? $regning->hentForfall() : $feiltrekk->forfallsdato);
                    // Trekket kan slettes dersom avdraget er helt betalt
                    //	eller dersom dersom det tid til å sende nytt trekk
                    if (
                        ($utestående <= 0 && $nesteForsendelse <= $feiltrekk->hentSlettefrist()) // Den er helt betalt
                        || !$fristForNyttOppdrag
                        || $nesteForsendelse <= $fristForNyttOppdrag // Det kan fortsatt sendes nytt trekk
                    ) {
                        $feiltrekk->settTilSletting(new DateTime());
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Faste Betalingsoppdrag - Slett trekkrav for betalingsplaner
     * Flagger fbo trekkrav for betalingsplan-avdrag som har blitt endret eller betalt siden innsending til Nets
     * for sletting sånn at de kan sendes inn på nytt igjen
     *
     * @return Leiebase
     * @throws Throwable
     */
    public function forberedBetalingsplanTrekkForSletting(): Leiebase
    {
        $tp = $this->mysqli->table_prefix;

        /** @var Fbo\Trekksett $trekkSett */
        $trekkSett = $this->hentSamling(Fbo\Trekk::class);

        $subQuery = 'SELECT `eav_verdier`.`objekt_id`, `eav_verdier`.`verdi` '
            . ' FROM `' . $tp . Modell\Eav\Egenskap::hentTabell() . '` AS `' . Modell\Eav\Egenskap::hentTabell() . '`'
            . ' INNER JOIN `' . $tp . Modell\Eav\Verdi::hentTabell() . '` AS `' . Modell\Eav\Verdi::hentTabell() . '`'
            . ' ON `' . Modell\Eav\Egenskap::hentTabell() . '`.`' . Modell\Eav\Egenskap::hentPrimærnøkkelfelt() . '` = `' . Modell\Eav\Verdi::hentTabell() . '`.`egenskap_id`'
            . ' WHERE `' . Modell\Eav\Egenskap::hentTabell() . '`.`kode` = \'avtalegiro_trekk\' AND `' . Modell\Eav\Egenskap::hentTabell() . '`.`modell` = \'' . addslashes(Avdrag::class) . '\'';
        $trekkSett->leggTilSubQueryJoin(
            $subQuery,
            'avdragslink',
            '`avdragslink`.`verdi` = `' . Fbo\Trekk::hentTabell() . '`.`' . Fbo\Trekk::hentPrimærnøkkelfelt() . '`',
            'left',
        );
        $trekkSett->leggTilLeftJoin(Avdrag::hentTabell(),
            '`avdragslink`.`objekt_id` = `' . Avdrag::hentTabell() . '`.`' . Avdrag::hentPrimærnøkkelfelt() . '`'
        );
        $trekkSett->leggTilLeftJoin(
            Betalingsplan::hentTabell(),
            '`' . Avdrag::hentTabell() . '`.`betalingsplan_id`'
            . ' = `' . Betalingsplan::hentTabell() . '`.`' . Betalingsplan::hentPrimærnøkkelfelt() . '`'
            . ' AND '
            . '`' . Betalingsplan::hentTabell() . '`.`aktiv`'
        );
        $subQuery = 'SELECT `eav_verdier`.`objekt_id`, `eav_verdier`.`verdi` '
            . ' FROM `' . $tp . Modell\Eav\Egenskap::hentTabell() . '` AS `' . Modell\Eav\Egenskap::hentTabell() . '`'
            . ' INNER JOIN `' . $tp . Modell\Eav\Verdi::hentTabell() . '` AS `' . Modell\Eav\Verdi::hentTabell() . '`'
            . ' ON `' . Modell\Eav\Egenskap::hentTabell() . '`.`' . Modell\Eav\Egenskap::hentPrimærnøkkelfelt() . '` = `' . Modell\Eav\Verdi::hentTabell() . '`.`egenskap_id`'
            . ' WHERE `'
            . Modell\Eav\Egenskap::hentTabell() . '`.`kode` = \'utenom_avtalegiro\' '
            . 'AND `' . Modell\Eav\Egenskap::hentTabell() . '`.`modell` = \'' . addslashes(Betalingsplan::class) . '\'';
        $trekkSett->leggTilSubQueryJoin(
            $subQuery,
            'utenom_avtalegiro',
            '`utenom_avtalegiro`.`objekt_id` = `' . Betalingsplan::hentTabell() . '`.`' . Betalingsplan::hentPrimærnøkkelfelt() . '`',
            'left'
        );

        $trekkSett->leggTilFilter(['til_sletting' => null]);
        // Nets anbefaler at fil med slettinger sendes en til to dager etter at filen med betalingskrav er sendt
        // slik at man sikrer at betalingskravet vil slettes
        $trekkSett->leggTilFilter(['DATE_ADD(`' . Fbo\Trekk::hentTabell() . '`.`overføringsdato`, INTERVAL 1 DAY) < NOW()'], true);
        $trekkSett->leggTilFilter(['`' . Fbo\Trekk::hentTabell() . '`.`forfallsdato` > CURDATE()'], true);
        $trekkSett->leggTilFilter(['`' . Fbo\Trekk::hentTabell() . '`.`gironr` IS NULL'], true);

        $trekkSett->leggTilFilter(['or' => [
            '`utenom_avtalegiro`.`verdi`' => true,
            '`' . Avdrag::hentTabell() . '`.`' . Avdrag::hentPrimærnøkkelfelt() . '` IS NULL',
            '`' . Avdrag::hentTabell() . '`.`utestående` <> `' . Fbo\Trekk::hentTabell() . '`.`beløp`',
        ]]);
        $trekkSett->låsFiltre();

        $trekkSett->leggTilFelt(Avdrag::hentTabell(), 'utestående');
        $trekkSett->leggTilFelt(Avdrag::hentTabell(), 'forfallsdato');

        if ($trekkSett->hentAntall()) {
            $this->logger->info('NETS – Flagger ' . $trekkSett->hentAntall() . ' trekk (for betalingsplan-avdrag) som har blitt endret for sletting.', ['trekk' => $trekkSett->hentIdNumre()]);

            foreach ($trekkSett as $feiltrekk) {
                $utestående = $feiltrekk->getRawData()->{Avdrag::hentTabell()}->utestående ?? 0;
                $nesteFboTrekkravForsendelse = $this->hentNetsProsessor()->nesteFboTrekkravForsendelse();
                $fristForNyttTrekk = $feiltrekk->hentRegning() && $feiltrekk->hentFbo()
                    ? $feiltrekk->hentFbo()->oppdragsfrist($feiltrekk->hentRegning()->hentForfall())
                    : null;

                // Trekket kan slettes dersom avdraget er helt betalt og slettefristen kan opprettholdes
                //	eller dersom dersom det tid til å sende nytt trekk
                if (
                    ($utestående <= 0 && $nesteFboTrekkravForsendelse <= $feiltrekk->hentSlettefrist())
                    || !$fristForNyttTrekk
                    || $nesteFboTrekkravForsendelse <= $fristForNyttTrekk
                ) {
                    $feiltrekk->settTilSletting(new DateTime());
                }
            }
        }

        return $this;
    }

    /**
     * Faste Betalingsoppdrag - Slett trekkrav
     * Sletter fbo trekkrav for regninger som har blitt endret siden innsending til Nets,
     * sånn at de kan sendes inn på nytt igjen
     *
     * @return Leiebase
     * @throws Throwable
     */
    public function leggTrekkTilSletteoppdrag(Sletteoppdrag $sletteoppdrag): Leiebase
    {
        /** @var Fbo\Trekksett $trekkSett */
        $trekkSett = $this->hentSamling(Fbo\Trekk::class);
        $trekkSett->leggTilRegningModell();
        $trekkSett->leggTilLeftJoin(KravModell::hentTabell(), '`' . Fbo\Trekk::hentTabell() . '`.`gironr` = `' . KravModell::hentTabell() . '`.`gironr`');

        // Nets anbefaler at fil med slettinger sendes en til to dager etter at filen med betalingskrav er sendt
        // slik at man sikrer at betalingskravet vil slettes
        $trekkSett->leggTilFilter(['DATE_ADD(`' . Fbo\Trekk::hentTabell() . '`.`overføringsdato`, INTERVAL 1 DAY) < NOW()'], true);
        // Slett aldri trekk som ikke har blitt sendt
        $trekkSett->leggTilFilter(['`' . Fbo\Trekk::hentTabell() . '`.`forsendelse`'], true);
        // Sletteanmodninger må være mottatt i Nets på en virkedag
        // senest kl 14:00 dagen før forfallsdato
        $trekkSett->leggTilFilter(['`' . Fbo\Trekk::hentTabell() . '`.`forfallsdato` > CURDATE()'], true);
        $trekkSett->leggTilFilter(['`fbo_trekkrav`.`til_sletting`'], true);

        if ($trekkSett->hentAntall()) {
            $this->logger->info('NETS – Legger til ' . $trekkSett->hentAntall() . ' trekk til sletteoppdrag.');

            foreach ($trekkSett as $feiltrekk) {
                $sletteoppdrag->transaksjoner[] = $feiltrekk->hentNetsTransaksjon();

                $feiltrekk->slett();
            }
        }

        return $this;
    }

    /**
     * Gjør om en brøkverdi til desimal
     *
     * @param string $brøk verdien som brøk
     * @return string verdien som desimaltall
     * @see \Kyegil\Fraction\Fraction::asDecimal()
     * @deprecated
     */
    public function fraBrøk($brøk)
    {
        $brøk = str_replace(array(",", "%"), array(".", "/100"), $brøk);

        $brøk = str_replace("%", "/100", $brøk);

        $brøk = explode("/", $brøk);

        if (empty($brøk[1])) {
            $brøk[1] = 1;
        }

        return bcdiv($brøk[0], $brøk[1], 12);
    }

    /**
     * Hent et objekt
     *
     * Spesialtilpasset funksjon som brukes istedet for new på egendefinerte objekter
     *
     * @param string $class
     * @param mixed $arg argumenter som sendes til constructor
     * @return \DatabaseObjekt
     * @see Leiebase::hentModell()
     * @deprecated
     */
    public function hent($class, $arg = null)
    {
        $objekt = new $class($arg);
        return $objekt;
    }


    /**
     * Hent autoriserer
     *
     * @return AutorisererInterface
     * @throws Throwable
     */
    public function hentAutoriserer(): AutorisererInterface
    {
        if (!isset($this->autoriserer)) {
            $autorisererKlasse = static::getClassPreference(Autoriserer\LaminasAuth::class);
            $this->autoriserer = new $autorisererKlasse($this->hentCoreModelImplementering());
        }
        return $this->autoriserer;
    }

    /**
     * Hent valg
     *
     * @param string|null $valg
     * @return string[]|string|null
     */
    public function hentValg(?string $valg = null)
    {
        if (!isset($valg) || !isset($this->valg)) {
            $this->valg = [];
            $sett = $this->mysqli->select([
                'source' => 'valg'
            ])->data;
            foreach ($sett as $linje) {
                $this->valg[$linje->innstilling] = $linje->verdi;
            }
        }
        if($valg === null) {
            return $this->valg;
        }
        return $this->valg[$valg] ?? null;
    }


    /**
     * Katalog
     *
     * Returnerer omsluttende mappe for ei fil
     *
     * @param string $fil
     * @return string mappenavn
     */
    public function katalog($fil)
    {
        $bane = array_reverse(explode("/", $fil));
        return $bane[1];
    }


    /**
     * Kontrakt
     *
     * Returnerer objekt med kontraktdetaljene
     *
     * @param int $kontraktnr
     * @return bool|stdClass
     * @throws Exception
     * @deprecated
     */
    public function kontrakt($kontraktnr)
    {
        $sql = "SELECT * FROM kontrakter WHERE kontraktnr = $kontraktnr";
        if ($a = $this->arrayData($sql))
            return $a['data'][0];
        else return false;
    }


    /**
     * Kontraktobjekt
     *
     * returnerer leieobjektet i angitt kontrakt
     *
     * @param int $kontraktnr
     * @return int
     * @throws Exception
     * @deprecated
     * @see Kyegil\Leiebasen\Modell\Leieforhold::hentLeieobjekt()
     */
    public function kontraktobjekt($kontraktnr)
    {
        $sql = "SELECT leieobjekt FROM kontrakter WHERE kontraktnr = '$kontraktnr'";
        $a = $this->arrayData($sql);
        return $a['data'][0]['leieobjekt'] ?? null;
    }


    /**
     * Kontraktpersoner
     *
     * returnerer et sett bestående av leietakernes navn indexert etter deres adressekorts ID
     *
     * @param int $kontraktnr
     * @return array
     * @throws Exception
     */
    public function kontraktpersoner($kontraktnr)
    {
        $sql = "SELECT person AS id, IF(etternavn IS NULL, leietaker, IF(er_org, etternavn, CONCAT(fornavn, ' ', etternavn))) AS navn\n"
            . "FROM kontraktpersoner LEFT JOIN personer ON kontraktpersoner.person = personer.personid\n"
            . "WHERE kontraktpersoner.kontrakt = '{$kontraktnr}' AND slettet IS NULL";
        $a = $this->arrayData($sql);
        $resultat = array();
        foreach ($a['data'] as $person) {
            $resultat[$person['id']] = $person['navn'];
        }
        return $resultat;
    }

    /**
     * Kontrollrutiner
     *
     * Kontrollrutinene tester leiebasen og ser etter viktige ting som bør taes hånd om
     *
     * @return $this
     * @throws Exception
     */
    public function kontrollrutiner(): Leiebase
    {
        EventManager::getInstance()->triggerEvent(get_class($this), '`kontrollrutiner`', $this, [$this->advarsler], $this->hentCoreModelImplementering());
        return $this;
    }


    /**
     * kr
     *
     * Funksjon som formaterer en verdi som kroneverdi
     *
     * @param float|null $verdi Verdien som skal formateres
     * @param bool $html Om verdien skal formateres for html
     * @param bool $prefiks Setter kr foran beløpet
     * @return string tekststreng
     */
    public function kr(?string $verdi, bool $html = true, bool $prefiks = true): string
    {
        if (!is_numeric($verdi)) {
            return '';
        }
        $resultat = str_replace(',00', ',–', ($prefiks ? "kr " : "") . number_format($verdi, 2, ",", " "));
        if ($html) {
            return "<span>" . str_replace(" ", "&nbsp;", $resultat) . "</span>";
        } else {
            return $resultat;
        }
    }

    /**
     * Hent Krav fra Kid
     *
     * @param string $kid
     * @return Kravsett
     * @throws Exception
     */
    public function hentKravFraKid(string $kid): Kravsett
    {
        $kid = trim($kid);
        /** @var Kravsett $kravsett */
        $kravsett = $this->hentSamling(KravModell::class);
        $kravsett->leggTilFilter(['`' . Regning::hentTabell() . '`.`kid`' => $kid]);
        $kravsett->leggTilRegningModell();
        return $kravsett;
    }

    /**
     * Legg til intervall
     *
     * @param int $timestamp
     * @param string $intervall
     * @return false|int
     */
    public function leggtilIntervall($timestamp, $intervall)
    {
        $enhet = substr($intervall, -1);
        $verdi = (int)substr($intervall, 1);
        $date_time_array = getdate($timestamp);
        $day = $date_time_array['mday'];
        $month = $date_time_array['mon'];
        $year = $date_time_array['year'];
        switch ($enhet) {
            case 'Y':
                $year += $verdi;
                break;
            case 'M':
                $month += $verdi;
                break;
            case 'D':
                $day += $verdi;
                break;
        }
        $timestamp = mktime(0, 0, 0, $month, $day, $year);
        return $timestamp;
    }


    /**
     * Leieforhold
     *
     * Finner leieforhold på grunnlag av kontraktnummer
     *
     * @param int|\Leieforhold| $kontrakt kontraktnummeret
     * @param bool $somObjekt Dersom på returneres Leieforhold-objektet istedetfor leieforholdnummeret
     * @return false|int|\Leieforhold leieforholdnummeret eller leieforhold-objektet for en leieavtale
     * @throws Exception
     */
    public function leieforhold($kontrakt, $somObjekt = false)
    {
        if ($kontrakt instanceof \Leieforhold and $kontrakt->hentId()) {
            return $kontrakt;
        }
        $kontraktNr = intval(strval($kontrakt));
        /** @var Kontrakt $kontrakt */
        $kontrakt = $this->hentModell(Kontrakt::class, $kontraktNr);
        $leieforholdId = intval(strval($kontrakt->leieforhold));
        if (!$leieforholdId) {
            return false;
        }
        if ($somObjekt) {
            /** @var \Leieforhold $leieforhold */
            $leieforhold = $this->hent(\Leieforhold::class, $leieforholdId);
            return $leieforhold;
        }
        return $leieforholdId;
    }

    /**
     * @param string $kid
     * @return LeieforholdModell|null
     * @throws Throwable
     */
    public function hentLeieforholdFraKid(string $kid): ?LeieforholdModell
    {
        list('kid' => $kid) = $this->pre($this, __FUNCTION__, ['kid' => $kid]);
        $leieforholdId = $this->hentKidBestyrer()->hentLeieforholdIdFraKid($kid);
        /** @var LeieforholdModell $leieforhold */
        $leieforhold = $this->hentModell(LeieforholdModell::class, $leieforholdId);
        return $this->post($this, __FUNCTION__, $leieforhold->hentId() ? $leieforhold : null);
    }

    /**
     * Leieobjekt
     *
     * returnerer beskrivelse av oppgitt leieobjekt
     * Om inklnr er sann taes leieobjektnummeret med i beskrivels
     * I kortversjonen taes vises kun 'bolig nr. XX' / 'lokale nr XX'
     *
     * @param int $leieobjektnr
     * @param bool $inklnr
     * @param bool $kortversjon
     * @return string
     * @throws Exception
     */
    public function leieobjekt($leieobjektnr, $inklnr = false, $kortversjon = false)
    {
        $a = $this->mysqli->select([
            'source' => "leieobjekter",
            'where' => "leieobjektnr = '$leieobjektnr'"
        ]);

        $resultat = "";

        if ($a->totalRows) {
            $a = $a->data[0];

            if ($inklnr or $kortversjon) {
                $resultat = ($a->boenhet ? "bolig nr. " : "lokale nr. ") . $a->leieobjektnr;
                if ($kortversjon) {
                    return $resultat;
                }
                $resultat .= ": ";
            }
            $resultat .= ($a->navn ? ("{$a->navn} ") : "");
            $resultat .= $a->etg ? ($this->etasjerenderer($a->etg) . " ") : "";
            $resultat .= $a->beskrivelse ? ("{$a->beskrivelse} ") : "";
            $resultat .= $a->gateadresse;
        }

        return $resultat;
    }

    /**
     * Navn
     *
     * @param int $personid
     * @param bool $lenke
     * @return array|string
     * @throws Exception
     * @deprecated
     * @see Person::hentNavn()
     */
    public function navn($personid, $lenke = false)
    {
        if (is_array($personid)) {
            foreach ($personid as $id) {
                $a = $this->mysqli->arrayData(array(
                    'source' => "personer",
                    'where' => "personid = '$id'"
                ));

                if ($a->totalRows) {
                    $navn = $a->data[0]->er_org ? $a->data[0]->etternavn : "{$a->data[0]->fornavn} {$a->data[0]->etternavn}";
                    $resultat[] = $lenke ? "<a title=\"Klikk for å gå til adressekortet\" href=\"index.php?oppslag=personadresser_kort&id=$id>$navn</a>" : $navn;
                }

            }
            return $resultat;
        }

        $a = $this->mysqli->arrayData(array(
            'source' => "personer",
            'where' => "personid = '$personid'"
        ));
        if ($a->totalRows) {
            $navn = $a->data[0]->er_org ? $a->data[0]->etternavn : "{$a->data[0]->fornavn} {$a->data[0]->etternavn}";
            return $lenke ? "<a title=\"Klikk for å gå til adressekortet\" href=\"index.php?oppslag=personadresser_kort&id=$personid>$navn</a>" : $navn;
        }
        return '';
    }

    /**
     * NETS Slett usendte Avtalegioer
     *
     * Sletter Avtalegiroer som ikke har blitt overført (pga feil) ifra fbo_trekkrav-tabellen
     *
     * @return $this
     */
    public function netsSlettUsendteAvtalegiroer(): Leiebase
    {
        $tp = $this->mysqli->table_prefix;

        $this->mysqli->query('DELETE FROM `' . $tp . 'fbo_trekkrav` WHERE !`forsendelse`');
        return $this;
    }


    /**
     * Ny forfallsdato
     *
     * Returnerer en ny forfallsdato som tilfredstiller kravene som er satt i innstillingene for leiebasen.
     *
     * @return DateTimeImmutable Ny forfallsdato
     * @throws Exception
     */
    public function nyForfallsdato(): DateTimeImmutable
    {
        $forfallsfrist = new DateInterval($this->hentValg('min_frist_regninger'));
        $forfallsdato = new DateTimeImmutable();
        $forfallsdato = $forfallsdato->setTime(0, 0, 0);
        $forfallsdato = $forfallsdato->add($forfallsfrist);

        // Om forfall faller på en helg- eller helligdag flyttes det
        $bankfridager = Leiebase::bankfridager($forfallsdato->format('Y'));
        while (
            in_array($forfallsdato->format('m-d'), $bankfridager)
            || $forfallsdato->format('N') > 5
        ) {
            $forfallsdato = $forfallsdato->add(new DateInterval('P1D'));
        }

        return $forfallsdato;
    }


    /**
     * Oppdater ubetalt
     *
     * @param int $utelattDelbeløpId innbetalingsid for evt delbeløp som skal ignoreres
     * @return $this
     */
    public function oppdaterUbetalt(int $utelattDelbeløpId = 0): Leiebase
    {
        return $this;
    }


    /**
     * Oppdrag
     *
     * @param string $oppdrag
     * @throws Exception
     */
    public function oppdrag($oppdrag = "")
    {
        if ($oppdrag == "taimotskjema") {
            $this->taimotSkjema(isset($_GET['skjema']) ? $_GET['skjema'] : null);
        } else if ($oppdrag == "hentdata") {
            echo $this->hentData($_GET['data'] ?? null);
        } else if ($oppdrag == "lagpdf") {
            $this->lagPDF(isset($_GET['pdf']) ? (int)$_GET['pdf'] : null);
        } else if ($oppdrag == "manipuler") {
            $this->manipuler($_GET['data'] ?? null);
        } else if ($oppdrag == "utskrift") {
            $this->mal = "_utskrift.php";
            $this->skrivHTML();
        } else if ($oppdrag == "oppgave") {
            $this->oppgave($_GET['oppgave']);
        }
    }


    /**
     * Opprett
     *
     * Oppretter et nytt database-objekt som lagres
     *
     * @param $type string Class-navnet på objektet som skal opprettes. Det må være arvtaker av 'DatabaseObjekt'
     * @param array $egenskaper
     * @return bool|\DatabaseObjekt Objektet som ble opprettet, eller false dersom det ikke kunne opprettes
     * @see Leiebase::nyModell()
     * @deprecated
     */
    public function opprett($type, $egenskaper = array())
    {
        if (!is_a($type, \DatabaseObjekt::class, true)) {
            return false;
        }
        $objekt = new $type;

        if ($objekt->opprett($egenskaper) === false) {
            return false;
        }
        return $objekt;
    }


    /**
     * Opprett leiekrav
     *
     * Erstatter eller oppretter nye leiekrav (forfall) i alle leieforhold som ikke er oppsagt.
     * For tidsbegrensede kontrakter opprettes krav fram til utløpsdato.
     * Om denne har utløpt opprettes kun en termin ad gangen,
     * men kun dersom alle eksisterende terminer har passert.
     * For ikke tidsbegrensede kontrakter settes kun hele terminer,
     * og ikke senere enn oppsigelsestiden beregnet ifra dagens dato.
     * For oppsagte kontrakter opprettes krav fram til oppsigelsestiden opphører,
     * men bare dersom leia ikke er beregna fram til leieavtalen er oppsagt.
     *
     * @param null|DateTime $fradato Dersom $fradato er angitt vil eksisterende krav slettes fra denne datoen før nye legges til
     * @param bool $løsneInnbetalinger Dersom denne er sann vil alle innbetalinger løsnes før kravene slettes, med mindre det allerede er skrevet ut giro
     * @param int $ukedag Ukedag (1-7) terminene skal starte på. Angitt i hht ISO-8601. 1=mandag, 7=søndag
     * @param int $kalenderdag Dag i måneden (1-31) terminene skal starte på. Alt over 27 regnes som siste dagen i måneden
     * @param string|false $fastDato Dato i formatet 'm-d' som starter en ny termin
     * @return $this
     * @throws Exception
     */
    public function opprettLeiekrav(
        DateTime $fradato = null,
        bool     $løsneInnbetalinger = false,
        int      $ukedag = 0,
        int      $kalenderdag = 0,
                 $fastDato = false
    ): Leiebase
    {
        /** @var Leieforholdsett $leieforholdsett */
        $leieforholdsett = $this->hentSamling(LeieforholdModell::class);
        $leieforholdsett->leggTilOppsigelseModell(['fristillelsesdato','oppsigelsestid_slutt']);
        $leieforholdsett->leggTilEavFelt('forfall_fast_ukedag');
        $leieforholdsett->leggTilEavFelt('forfall_fast_dag_i_måneden');
        $leieforholdsett->leggTilEavFelt('forfall_fast_dato');
        $leieforholdsett->leggTilFilter([
            '`' . Oppsigelse::hentTabell() . '`.`' . Oppsigelse::hentPrimærnøkkelfelt() . '`' => null
        ]);
        foreach ($leieforholdsett as $leieforhold) {
            $leieforhold->opprettLeiekrav(
                $fradato,
                $løsneInnbetalinger,
                $ukedag,
                $kalenderdag,
                $fastDato
            );
        }
        return $this;
    }


    /**
     * Opprett polett
     *
     * @return bool|string
     */
    public function opprettPolett()
    {
        $p = session_id() . time();
        $this->mysqli->query("DELETE FROM poletter WHERE utløper < " . time());
        if ($this->mysqli->query("INSERT INTO poletter SET polett = '$p', utløper = " . (time() + 6 * 3600))) return $p;
        else return false;
    }


    /**
     * Oppsagt
     *
     * returnerer fristillelsesdatoen dersom er leieforhold er oppsagt, og NULL om det ikke er det
     *
     * @param int $kontraktnr
     * @return false|int|null
     * @throws Exception
     */
    public function oppsagt($kontraktnr)
    {
        $sql = "SELECT fristillelsesdato\n"
            . "FROM oppsigelser\n"
            . "WHERE leieforhold = '" . $this->leieforhold($kontraktnr) . "'";
        $a = $this->arrayData($sql);

        if (isset($a['data'][0]['fristillelsesdato'])) {
            return strtotime($a['data'][0]['fristillelsesdato']);
        } else {
            return null;
        }
    }


    /**
     * Oppsigelsestidrenderer
     *
     * @param string $v
     * @return string
     */
    public function oppsigelsestidrenderer($v)
    {
        $periode = substr($v, -1);
        $antall = (int)substr($v, 1);

        if (!$antall) {
            return "ingen oppsigelsestid";
        }
        if ($periode == 'Y') $periode = 'år';
        if ($periode == 'M') $periode = 'måned';
        if ($periode == 'D') $periode = 'dag';
        if ($periode == 'dag' and ($antall / 7) == (int)($antall / 7)) {
            $antall = $antall / 7;
            $periode = 'uke';
        }
        if ($antall > 1 and $periode == 'uke') $periode = 'uker';
        if ($antall > 1 and $periode == 'dag') $periode = 'dager';
        if ($antall > 1 and $periode == 'måned') $periode = 'måneder';
        return "$antall $periode";
    }


    /**
     * Pre HTML
     * Funksjon som smetter inn behandling umiddelbart før HTML sendes ut
     * Denne funksjonen kan påvirke malen som skal brukes for å sende ut oppslaget
     * Dersom funksjonen returnerer usann vil den stoppe utsendelsen av malen
     *
     * @return bool Sann for å skrive ut HTML-malen, usann for å stoppe den
     */
    public function preHTML()
    {
        return true;
    }


    /**
     * Prosent
     *
     * Funksjon som formaterer en verdi som prosenter
     *
     * @param float $verdi
     * @param int $antallDesimaler
     * @param bool $html Om verdien skal formateres for html
     * @return string
     */
    public function prosent($verdi, $antallDesimaler = 1, $html = true)
    {
        $resultat = number_format(round($verdi * 100, $antallDesimaler), $antallDesimaler, ",", " ") . " %";
        if ($html) {
            return "<span>" . str_replace(" ", "&nbsp;", $resultat) . "</span>";
        } else {
            return $resultat;
        }
    }


    /**
     * Registrer betaling
     *
     * Funksjon som oppretter ei betaling på grunnlag av en OCR-transaksjon
     * og forsøker å avstemme denne mot et krav
     *
     * @param Transaksjon $ocrTransaksjon fra OCR konteringsforsendelse fra NETS
     * @return InnbetalingsModell
     * @throws Exception
     */
    public function registrerBetaling(Transaksjon $ocrTransaksjon): InnbetalingsModell
    {
        list('ocrTransaksjon' => $ocrTransaksjon) = $this->pre($this, __FUNCTION__, ['ocrTransaksjon' => $ocrTransaksjon]);
        $restbeløp = $ocrTransaksjon->beløp;

        $leieforhold = $this->hentLeieforholdFraKid($ocrTransaksjon->kid);
        $betalingsplan = $leieforhold ? $leieforhold->hentBetalingsplan() : null;
        if (!$betalingsplan || !$betalingsplan->aktiv) {
            $betalingsplan = null;
        }

        /**
         * Innbetalings-id som skal brukes for betalinga som blir oppretta fra denne transaksjonen
         */
        $innbetalingsId = InnbetalingsModell::beregnId(
            $ocrTransaksjon->oppgjørsdato,
            $ocrTransaksjon->forsendelsesnummer . '-' . $ocrTransaksjon->løpenummer,
            $ocrTransaksjon->debetkonto,
            $ocrTransaksjon
        );

        /**
         * Forsøk først å betale for regninga som er oppgitt i Kid
         */
        $kravsett = $this->hentKravFraKid($ocrTransaksjon->kid);
        $kravsett->leggTilFilter(['utestående >' => 0]);
        $kravsett
            ->leggTilSortering('kravdato')
            ->leggTilSortering('forfall');
        $kravsett->sorterLastede(function (KravModell $kravA, KravModell $kravB) use ($restbeløp) {
            return abs($kravA->hentUtestående() - $restbeløp) - abs($kravB->hentUtestående() - $restbeløp);
        });

        foreach ($kravsett as $krav) {
            if ($restbeløp <= 0) {
                break;
            }
            $subtrahend = min($krav->utestående, $restbeløp);
            $this->nyModell(Delbeløp::class, (object)[
                'innbetaling' => $innbetalingsId,
                'krav' => $krav,
                'betaler' => $ocrTransaksjon->debetkonto,
                'leieforhold' => $krav->leieforhold,
                'beløp' => $subtrahend,
                'konto' => 'OCR-giro',
                'ocr_transaksjon' => $ocrTransaksjon->id,
                'ref' => $ocrTransaksjon->forsendelsesnummer . '-' . $ocrTransaksjon->løpenummer,
                'dato' => $ocrTransaksjon->oppgjørsdato,
                'registrerer' => $this->bruker['navn']
            ]);
            $krav->utestående -= $subtrahend;
            $restbeløp -= $subtrahend;
        }

        /**
         * Det ble ikke funnet noen eksakt match på KID.
         * Vi må nå ta hensyn til om det foreligger en betalingsplan,
         * for rett prioritering av krav og avtalte avdrag
         */
        if ($leieforhold && ($restbeløp > 0)) {
            $kommendeForfall = $leieforhold->hentKommendeForfall($restbeløp);

            /** @var KravModell|Avdrag $tilForfall */
            foreach ($kommendeForfall as $tilForfall) {
                if ($restbeløp <= 0) {
                    break;
                }
                if ($tilForfall instanceof KravModell) {
                    $subtrahend = min($tilForfall->utestående, $restbeløp);
                    if ($subtrahend > 0) {
                        $this->nyModell(Delbeløp::class, (object)[
                            'innbetaling' => $innbetalingsId,
                            'krav' => $tilForfall,
                            'betaler' => $ocrTransaksjon->debetkonto,
                            'leieforhold' => $tilForfall->leieforhold,
                            'beløp' => $subtrahend,
                            'konto' => 'OCR-giro',
                            'ocr_transaksjon' => $ocrTransaksjon->id,
                            'ref' => $ocrTransaksjon->forsendelsesnummer . '-' . $ocrTransaksjon->løpenummer,
                            'dato' => $ocrTransaksjon->oppgjørsdato,
                            'registrerer' => $this->bruker['navn']
                        ]);
                        $tilForfall->utestående -= $subtrahend;
                        $restbeløp -= $subtrahend;
                    }
                } else if ($tilForfall instanceof Avdrag) {
                    $avdragMaksbeløp = min($restbeløp, $tilForfall->utestående);
                    $betalingsplanKravsett = $betalingsplan->hentKravsett()->leggTilFilter(['utestående >' => 0]);
                    foreach ($betalingsplanKravsett as $kravIBetalingsplan) {
                        if ($avdragMaksbeløp <= 0) {
                            break;
                        }
                        if ($kravIBetalingsplan->utestående <= 0) {
                            continue;
                        }
                        $subtrahend = min($kravIBetalingsplan->utestående, $avdragMaksbeløp);
                        $this->nyModell(Delbeløp::class, (object)[
                            'innbetaling' => $innbetalingsId,
                            'krav' => $kravIBetalingsplan,
                            'betaler' => $ocrTransaksjon->debetkonto,
                            'leieforhold' => $leieforhold,
                            'beløp' => $subtrahend,
                            'konto' => 'OCR-giro',
                            'ocr_transaksjon' => $ocrTransaksjon->id,
                            'ref' => $ocrTransaksjon->forsendelsesnummer . '-' . $ocrTransaksjon->løpenummer,
                            'dato' => $ocrTransaksjon->oppgjørsdato,
                            'registrerer' => $this->bruker['navn']
                        ]);
                        $kravIBetalingsplan->utestående -= $subtrahend;
                        $avdragMaksbeløp -= $subtrahend;
                        $restbeløp -= $subtrahend;
                    }
                }
            }
        }

        // Resterende beløp krediteres leieforhold, men avstemmes ikke
        if ($restbeløp > 0) {
            $this->nyModell(Delbeløp::class, (object)[
                'innbetaling' => $innbetalingsId,
                'krav' => null,
                'betaler' => $ocrTransaksjon->debetkonto,
                'leieforhold' => $leieforhold,
                'beløp' => $restbeløp,
                'konto' => 'OCR-giro',
                'ocr_transaksjon' => $ocrTransaksjon->id,
                'ref' => $ocrTransaksjon->forsendelsesnummer . '-' . $ocrTransaksjon->løpenummer,
                'dato' => $ocrTransaksjon->oppgjørsdato,
                'registrerer' => $this->bruker['navn']
            ]);
        }

        if ($betalingsplan) {
            $betalingsplan->nullstill()->oppdaterUteståendeAvdrag();
            if($betalingsplan->hentUtestående() <= 0) {
                $betalingsplan->settAktiv(false);
            }
        }
        /** @var InnbetalingsModell $innbetaling */
        $innbetaling = $this->hentModell(InnbetalingsModell::class, $innbetalingsId);

        EventManager::getInstance()->triggerEvent(
            InnbetalingsModell::class, 'endret',
            $innbetaling->samle(), ['ny' => true],
            $this->hentCoreModelImplementering()
        );
        return $this->post($this, __FUNCTION__, $innbetaling);
    }


    /**
     * Sammenlikn egenskaper
     * Funksjon som sammenlikner to objekters egenskaper for sortering.
     * Egenskapene som skal sammenliknes settes i $this->sorteringsegenskap
     * Flere sorteringsegenskaper kan settes for finsortering
     *
     * @param object $objekt1
     * @param object $objekt2
     * @return int
     *      < 0 dersom $objekt1 er minst,
     *      > 0 dersom $objekt1 er størst,
     *      og 0 dersom $objekt1 og $objekt2 er like
     */
    public function sammenliknEgenskaper($objekt1, $objekt2)
    {
        settype($this->sorteringsegenskap, 'array');
        settype($objekt1, 'object');
        settype($objekt2, 'object');

        foreach ($this->sorteringsegenskap as $egenskap) {
            $verdi1 = @$objekt1->$egenskap;
            $verdi2 = @$objekt2->$egenskap;

            //	Strengverdier sorteres etter andre verdier
            if (is_string($verdi1) xor is_string($verdi2)) {
                return intval(is_string($verdi1)) - intval(is_string($verdi2));
            }

            if (!is_numeric($verdi1) and !is_numeric($verdi2)) {
                if (function_exists('collator_compare')) {
                    return collator_compare(new \Collator('no_NB'), $verdi1, $verdi2);
                } else {
                    return strcasecmp($verdi1, $verdi2);
                }
            }

            if ($verdi1 < $verdi2) {
                return -1;
            }

            if ($verdi1 > $verdi2) {
                return 1;
            }
        }
        return 0;
    }

    /**
     * Sammenlikn transaksjonsdatoer i krav og innbetalinger
     *
     * Brukes i usort() for å sortere transaksjoner etter dato
     *
     * @param stdClass|\Innbetaling|\Krav $transaksjon1
     * @param stdClass|\Innbetaling|\Krav $transaksjon2
     * @return int
     *      <0 dersom $transaksjon1 er mindre enn $transaksjon2,
     *      >0 dersom $transaksjon1 er større enn $transaksjon2,
     *      og 0 dersom $transaksjon1 og $transaksjon2 er like
     * @throws Exception
     */
    public function sammenliknTransaksjonsdatoer($transaksjon1, $transaksjon2): int
    {

        if ($transaksjon1 instanceof stdClass) {
            $dato1 = $transaksjon1->dato ?? null;
        } else {
            $dato1 = $transaksjon1->hent('dato');
        }

        if ($transaksjon2 instanceof stdClass) {
            $dato2 = $transaksjon2->dato ?? null;
        } else {
            $dato2 = $transaksjon2->hent('dato');
        }

        if ($dato1 < $dato2) {
            return -1;
        }
        if ($dato1 > $dato2) {
            return 1;
        }

        // Dersom datoene er like vil innbetalingene sorteres før kravene
        if (
            ($transaksjon1 instanceof \Innbetaling || $transaksjon1 instanceof stdClass)
            && $transaksjon2 instanceof \Krav
        ) {
            return -1;
        }
        if (
            $transaksjon1 instanceof \Krav
            && ($transaksjon2 instanceof \Innbetaling || $transaksjon2 instanceof stdClass)) {
            return 1;
        }

        // Innenfor samme dato vil to transaksjoner av samme type sorteres etter id
        if (
            ($transaksjon1 instanceof \Krav && $transaksjon2 instanceof \Krav)
            || ($transaksjon1 instanceof \Innbetaling && $transaksjon2 instanceof \Innbetaling)
        ) {
            return intval($transaksjon1->hentId()) - intval($transaksjon2->hentId());
        }

        return 0;
    }

    /**
     * Send e-post
     *
     * Epostens prioritet 0 = lav prioritet (standardverdi), 100 = høy
     *
     * @param object|array{
     *      type: string,
     *      to: string,
     *      cc: string,
     *      bcc: string,
     *      from: string,
     *      auto: bool,
     *      testcopy: bool ,
     *      reply: string,
     *      subject: string,
     *      html: string,
     *      text: string,
     *      attachment: string,
     *      priority: int
     *  } $config
     * @return Modell\Melding
     * @throws Exception
     */
    public function sendMail($config): Modell\Melding
    {
        settype($config, 'array');
        list('config' => $config) = $this->pre($this, __FUNCTION__, ['config' => $config]);
        if(!isset($config['testcopy']) && !empty($config['type'])) {
            $config['testcopy'] = in_array($config['type'], Leiebase::$config['leiebasen']['debug']['email_dev_copy'] ?? []);
        }
        $result = $this->hentEpostprosessor()->forberedEpost((array)$config);
        return $this->post($this, __FUNCTION__, $result);
    }

    /**
     * Siste kontrakt
     *
     * Returnerer siste kontraktnr i et leieforhold. $kontraktnr kan være hvilken som helst kontrakt i leieforholdet.
     *
     * @param int $kontraktnr
     * @return bool|int
     * @throws Exception
     */
    public function sistekontrakt($kontraktnr)
    {
        $kontraktnr = intval(strval($kontraktnr));

        $a = $this->arrayData("SELECT MAX(kontraktnr) AS kontraktnr FROM kontrakter WHERE leieforhold = '" . $this->leieforhold($kontraktnr) . "'");
        if ($a['data'][0]['kontraktnr']) return $a['data'][0]['kontraktnr'];
        else return false;
    }


    /**
     * Skriv header
     *
     * @param string $title
     * @throws Exception
     */
    public function skrivHeader($title = "")
    {
        $bibliotek = $this->http_host . "/pub/lib/" . $this->ext_bibliotek;
        ?>
        <meta charset="utf-8">
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
        <title><?php echo $title ? $title : $this->tittel; ?></title>

        <link rel="stylesheet" type="text/css" href="<?php echo $bibliotek; ?>/resources/css/ext-all.css"
              media="screen">
        <?php if ($this->ext_bibliotek != 'ext-3.4.0'): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo $bibliotek; ?>/resources/css/ext-all-gray.css"
              media="screen">
    <?php else: ?>
        <link rel="stylesheet" type="text/css" href="<?php echo $bibliotek . '/resources/css/xtheme-gray.css'; ?>"
              media="screen">
    <?php endif; ?>
<!--        <link rel="stylesheet" type="text/css" href="/pub/css/leiebase.css" media="screen">-->

        <?php if ($this->ext_bibliotek == 'ext-3.4.0'): ?>
        <script type="text/javascript" src="<?php echo $bibliotek . '/adapter/ext/ext-base.js'; ?>"></script>
    <?php endif; ?>

        <script type="text/javascript" src="<?php echo $bibliotek; ?>/ext-all.js"></script>
        <?php if ($this->ext_bibliotek == 'ext-3.4.0'): ?>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/pub/lib/ext-ux/GroupSummary.js"></script>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/pub/lib/ext-ux/RowExpander.js"></script>
        <script type="text/javascript" src="<?php echo $bibliotek; ?>/src/locale/ext-lang-no_NB.js"></script>
    <?php else: ?>
        <script type="text/javascript" src="<?php echo $bibliotek; ?>/locale/ext-lang-no_NB.js"></script>
    <?php endif; ?>
        <script type="text/javascript" src="<?php echo $this->http_host; ?>/pub/js/fellesfunksjoner.js"></script>

        <script type="text/javascript">
            <?php echo $this->skript();?>
        </script>
        <?php
    }


    /**
     * Skriv HTML
     * Laster malen for siden og sender innholdet ut til nettleseren
     * Utsendelsen kan stoppes ved at preHTML() returneres usann
     *
     * @return bool
     * @throws Exception
     */
    public function skrivHTML()
    {
        if (!$this->preHTML()) {
            return false;
        }
        if (!include($this->mal)) {
            throw new Exception("Finner ikke fila '{$this->mal}' i '{$this->katalog($_SERVER['PHP_SELF'])}'");
        }
        return true;
    }


    /**
     * Sluttdato
     * returnerer dagen før fristillelsesdato for et leieforhold dersom leieforholdet er oppsagt.
     * returnerer sluttdatoen dersom leieforholdet er tidsbegrenset, og NULL dersom det ikke er tidsbegrenset
     *
     * @param int $kontraktnr
     * @return false|int|null tidsstempel for sluttdato
     * @throws Exception
     */
    public function sluttdato($kontraktnr)
    {
        if ($b = $this->oppsagt($kontraktnr))
            return $b - 24 * 3600;

        $sql = "SELECT *\n"
            . "FROM `kontrakter` AS a INNER JOIN kontrakter AS b ON a.leieforhold = b.leieforhold\n"
            . "WHERE a.kontraktnr = $kontraktnr AND a.tildato IS NULL";
        $a = $this->arrayData($sql);
        if (count($a['data']) > 0)
            return null;

        $sql = "SELECT MAX(a.tildato) AS sluttdato\n"
            . "FROM `kontrakter` AS a INNER JOIN kontrakter AS b ON a.leieforhold = b.leieforhold\n"
            . "WHERE b.kontraktnr = $kontraktnr";
        $a = $this->arrayData($sql);

        if (isset($a['data'][0]['sluttdato'])) {
            return strtotime($a['data'][0]['sluttdato']);
        } else {
            return null;
        }
    }


    /**
     * Sorter objekter
     *
     * Funksjon som sorterer ett array av objekter basert på gitte egenskaper
     *
     * @param array $objekter Objektene som skal sorteres
     * @param string $egenskap Egenskapen objektene skal sorteres etter
     * @param bool $synkende Sorteres i synkende rekkefølge (normalt av for stigende)
     * @param bool $type
     * @return array De sorterte objektene
     * @throws Exception
     */
    public function sorterObjekter($objekter, $egenskap, $synkende = false, $type = false)
    {
        settype($egenskap, 'array');
        $this->sorteringsegenskap = (array)$egenskap;

        if (!usort($objekter, array($this, 'sammenliknEgenskaper'))) {
            throw new Exception("Kunne ikke sammenlikne etter " . json_encode($this->sorteringsegenskap));
        }

        if ($synkende) {
            return $objekter = array_reverse($objekter);
        }

        return $objekter;
    }


    /**
     * Streng eller null
     *
     * Returnerer en streng i hermetegn, eller 'NULL'
     * @param string $streng
     * @param string $hermetegn Enkle eller doble hermetegn
     * @return string
     */
    public function strengellernull($streng, $hermetegn = "'")
    {
        if ($streng != "" and $streng != null)
            return $hermetegn . $streng . $hermetegn;
        else return 'NULL';
    }


    /**
     * Til brøk
     *
     * Formaterer en desimalverdi som brøk
     *
     * @param float $verdi
     * @return string
     * @deprecated
     * @see \Kyegil\Fraction\Fraction::parseDecimal()
     */
    public function tilBrøk($verdi)
    {

        // Rund av verdien til maks 6 desimaler
        $heltall = (int)$verdi;
        $desimal = bcsub(bcadd($verdi, '0.0000005', 6), $heltall, 6);

        // Loop gjennom for å finne en match, opp til og med 120-deler
        for ($nevner = 2; $nevner < 121; $nevner++) {
            $teller = round(($desimal * $nevner), 4);

            if ((int)$teller and $teller == (int)$teller) {
                return ($heltall ? "{$heltall} " : "") . (int)$teller . "/{$nevner}";
            }
        }

        // Alle brøkmuligheter til og med 120-deler har blitt forsøkt
        //	uten å finne en som kan representere desimaltallet
        //	Derfor returneres tallet i prosent med to desimaler
        return bcmul($verdi, 100, 2) . "%";
    }


    /**
     * Tidligst mulige kravdato
     *
     * Returnerer tidligst mulige kravdato i hht innstillingene i leiebasen
     *
     * @return null|DateTime Siste mulige kravdato, eller false dersom denne ikke begrenset
     * @throws Exception
     */
    public function tidligstMuligeKravdato():? DateTime
    {
        $sperredato = $this->hentValg('sperredato_for_etterregistrering_av_krav');

        if (!$sperredato) {
            return null;
        }
        $tidligstekravdato = new DateTime(date('Y-m-01'));

        if ($sperredato > date('j')) {
            $tidligstekravdato->sub(new DateInterval('P1M'));
        }
        return $tidligstekravdato;
    }

    /**
     * Tolk KID
     *
     * Skriver ut tekststreng med krav og leieforhold fra KID
     *
     * @param string $kid
     * @return string
     * @throws Exception
     */
    public function tolkKid($kid)
    {
        $leieforhold = $this->hentLeieforholdFraKid($kid);

        $a = [];

        $kravsett = $this->hentKravFraKid($kid);
        foreach ($kravsett as $krav) {
            $a[] = $krav->hentTekst();
        }

        $resultat = Leiebase::liste($a);
        $leieforholdBeskrivelse = $leieforhold
            ? DriftKontroller::lenkeTilLeieforholdKort($leieforhold, $leieforhold->hentNavn())
            : 'Ukjent leieforhold';
        if(trim($resultat)) {
            $resultat .= ' (' . $leieforholdBeskrivelse . ')';
        }
        else {
            $resultat = $leieforholdBeskrivelse;
        }
        return $resultat;
    }

    /**
     * Utskrift
     *
     * Utskriftsbehandlingen må defineres i hvert enkelt oppslag
     *
     * @throws Exception
     */
    public function utskrift()
    {
        throw new Exception("Docu::utskrift er ikke definert.");
    }


    /**
     * Utskriftsadresser
     *
     * Denne funksjonen henter alle leieforholdene i den pågående utskriften
     * som ikke har intern levering men må sendes i posten
     *
     * @return \Leieforhold[]|false
     * @throws Exception
     */
    public function utskriftsadresser()
    {
        $tp = $this->mysqli->table_prefix;

        if (!$utskriftsforsøk = $this->hentUtskriftsprosessor()->hentLagretUtskriftsforsøk()) {
            return false;
        }

        settype($utskriftsforsøk->giroer, 'array');
        settype($utskriftsforsøk->purringer, 'array');
        settype($utskriftsforsøk->statusoversikter, 'array');

        $resultat = $this->mysqli->arrayData(array(
            'distinct' => true,
            'class' => \Leieforhold::class,
            'source' => "{$tp}kontrakter AS kontrakter
                            LEFT JOIN {$tp}krav AS krav ON kontrakter.kontraktnr = krav.kontraktnr
                            LEFT JOIN {$tp}purringer AS purringer ON purringer.krav = krav.id",

            'fields' => "kontrakter.leieforhold AS id",

            'where' => "
                (krav.gironr = '" . implode("' OR krav.gironr = '", $utskriftsforsøk->giroer) . "'		
                OR purringer.blankett = '" . implode("' OR purringer.blankett = '", $utskriftsforsøk->purringer) . "'
                OR kontrakter.leieforhold = '" . implode("' OR kontrakter.leieforhold = '", $utskriftsforsøk->statusoversikter) . "')
                AND !kontrakter.regning_til_objekt
            "
        ));

        if ($resultat->success) {
            return $resultat->data;
        } else return false;
    }


    /**
     * Varsle forfall
     *
     * @return $this
     * @throws Exception
     */
    public function varsleForfall(): Leiebase
    {
        $sisteVarsel = date_create_from_format('U', $this->hentValg('varselstempel_forfall'));

        $frist = new DateTime();
        $frist->setTimezone(new DateTimeZone('UTC'));
        $frist->add(new DateInterval($this->hentValg('forfallsvarsel_innen')));

        /** @var Kravsett $kravsett */
        $kravsett = $this->hentSamling(KravModell::class);
        $kravsett->leggTilFilter(['`' . KravModell::hentTabell() . '`.`utestående` > 0']);
        $kravsett->leggTilFilter(['`' . KravModell::hentTabell() . '`.`forfall` >' => $sisteVarsel->format('Y-m-d')]);
        $kravsett->leggTilFilter(['`' . KravModell::hentTabell() . '`.`forfall` <=' => $frist->format('Y-m-d')]);

        // Lag et leieforholdsett som et array,
        //	med ett element per leieforhold
        //	og der forfallskravene, i form av et nytt array, er verdien:
        $kravPerLeieforhold = [];
        foreach ($kravsett as $krav) {
            settype($kravPerLeieforhold[$krav->leieforhold->hentId()], 'array');
            $kravPerLeieforhold[$krav->leieforhold->hentId()][] = $krav;
        }

        $tittel = 'Påminnelse om forfall';

        /** @var KravModell[] $kravArray */
        foreach ($kravPerLeieforhold as $leieforholdId => $kravArray) {
            /** @var Kravsett $leieforholdKravsett */
            $leieforholdKravsett = clone $kravsett;
            $leieforholdKravsett
                ->leggTilFilter(['leieforhold' => $leieforholdId])
                ->låsFiltre()
                ->lastFra($kravArray);

            /** @var Visning\felles\epost\shared\Epost $html */
            $html = $this->hentVisning(Visning\felles\epost\shared\Epost::class, [
                'tittel' => $tittel,
                'innhold' => $this->hentVisning(Visning\felles\epost\leieforhold\forfallsvarsel\Html::class, ['kravsett' => $leieforholdKravsett])
            ]);
            /** @var Visning\felles\epost\shared\Epost $tekst */
            $tekst = $this->hentVisning(Visning\felles\epost\shared\Epost::class, [
                'tittel' => $tittel,
                'innhold' => $this->hentVisning(Visning\felles\epost\leieforhold\forfallsvarsel\Txt::class, ['kravsett' => $leieforholdKravsett]),
                'bunntekst' => Visning\felles\epost\shared\Epost::br2crnl($this->hentValg('eposttekst')),
            ])->settMal('felles/epost/shared/Epost.txt');

            $krav = $leieforholdKravsett->hentFørste();
            $leieforhold = $krav->leieforhold;
            $brukerepost = $leieforhold->hentEpost('forfallsvarsel');

            if ($brukerepost) {
                $this->sendMail((object)[
                    'to' => implode(',', $brukerepost),
                    'subject' => $tittel,
                    'html' => $html,
                    'text' => $tekst,
                    'type' => 'forfallsvarsel',
                ]);
            }
        }

        $this->settValg('varselstempel_forfall', $frist->format('U'));
        return $this;
    }

    /**
     * Varsle forfall
     *
     * @return $this
     * @throws Exception
     */
    public function sendUmiddelbareBetalingsvarsler(string $type = 'epost'): Leiebase
    {
        $sisteVarsel = date_create_immutable($this->hentValg('varselstempel_umiddelbart_betalingsvarsel') ?? 'now');
        $antDager  = $this->hentValg('umiddelbart_betalingsvarsel_ant_dager') ?? '';
        if(trim($antDager) === '') {
            $this->settValg('varselstempel_umiddelbart_betalingsvarsel', date_create()->format('c'));
            return $this;
        }

        settype($antDager, 'integer');

        $tilDato = new DateTimeImmutable();
        $tilDato->setTimezone(new DateTimeZone('UTC'));
        $tilDato = $tilDato->sub(new DateInterval('P' . $antDager . 'D'));
        $fraDato = $tilDato->sub(new DateInterval('P5D'));

        /** @var Kravsett $kravsett */
        $kravsett = $this->hentSamling(KravModell::class);
        $kravsett->leggTilFilter(['`' . KravModell::hentTabell() . '`.`utestående` > 0']);
        $kravsett->leggTilFilter(['`' . KravModell::hentTabell() . '`.`forfall` >' => $sisteVarsel->format('Y-m-d')]);
        $kravsett->leggTilFilter(['`' . KravModell::hentTabell() . '`.`forfall` >' => $fraDato->format('Y-m-d')]);
        $kravsett->leggTilFilter(['`' . KravModell::hentTabell() . '`.`forfall` <=' => $tilDato->format('Y-m-d')]);

        // Lag et leieforholdsett som et array,
        //	med ett element per leieforhold
        //	og der forfallskravene, i form av et nytt array, er verdien:
        $kravPerLeieforhold = [];
        foreach ($kravsett as $krav) {
            settype($kravPerLeieforhold[$krav->leieforhold->hentId()], 'array');
            $kravPerLeieforhold[$krav->leieforhold->hentId()][] = $krav;
        }

        $tittel = 'Varsel om manglende betaling';

        /** @var KravModell[] $kravArray */
        foreach ($kravPerLeieforhold as $leieforholdId => $kravArray) {
            /** @var Kravsett $leieforholdKravsett */
            $leieforholdKravsett = clone $kravsett;
            $leieforholdKravsett
                ->leggTilFilter(['leieforhold' => $leieforholdId])
                ->låsFiltre()
                ->lastFra($kravArray);

            /** @var Visning\felles\epost\shared\Epost $html */
            $html = $this->hentVisning(Visning\felles\epost\shared\Epost::class, [
                'tittel' => $tittel,
                'innhold' => $this->hentVisning(Visning\felles\epost\leieforhold\umiddelbart_betalingsvarsel\Html::class, ['kravsett' => $leieforholdKravsett])
            ]);
            /** @var Visning\felles\epost\shared\Epost $tekst */
            $tekst = $this->hentVisning(Visning\felles\epost\shared\Epost::class, [
                'tittel' => $tittel,
                'innhold' => $this->hentVisning(Visning\felles\epost\leieforhold\umiddelbart_betalingsvarsel\Txt::class, ['kravsett' => $leieforholdKravsett]),
                'bunntekst' => Visning\felles\epost\shared\Epost::br2crnl($this->hentValg('eposttekst')),
            ])->settMal('felles/epost/shared/Epost.txt');

            $krav = $leieforholdKravsett->hentFørste();
            $leieforhold = $krav->leieforhold;
            $brukerepost = $leieforhold->hentEpost('umiddelbart_betalingsvarsel');

            if ($brukerepost) {
                $this->sendMail((object)[
                    'to' => implode(',', $brukerepost),
                    'subject' => $tittel,
                    'html' => $html,
                    'text' => $tekst,
                    'type' => 'umiddelbart_betalingsvarsel',
                ]);
            }
        }

        $this->settValg('varselstempel_umiddelbart_betalingsvarsel', max($tilDato, $sisteVarsel)->format('c'));
        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function varsleBetalingsplanKommendeAvdrag(): Leiebase
    {
        /** @var Avdragsett $kommendeAvdrag */
        $kommendeAvdrag = $this->hentSamling(Avdrag::class);
        $kommendeAvdrag->leggTilBetalingsplanModell();
        $kommendeAvdrag->leggTilFilter(['påminnelse_sendt' => null]);
        $kommendeAvdrag->leggTilFilter(['utestående >' => 0]);
        $kommendeAvdrag->leggTilFilter(['`' . Betalingsplan::hentTabell() . '`.`aktiv`' => 1], true);
        $kommendeAvdrag->leggTilFilter(['`' . Betalingsplan::hentTabell() . '`.`forfallspåminnelse_ant_dager` IS NOT NULL'], true);
        $kommendeAvdrag->leggTilFilter(['DATE_ADD(`' . Avdrag::hentTabell() . '`.`forfallsdato`, INTERVAL (-1 * `' . Betalingsplan::hentTabell() . '`.`forfallspåminnelse_ant_dager`) DAY)  < NOW()'], true);
        $kommendeAvdrag->leggTilFilter(['`' . Avdrag::hentTabell() . '`.`forfallsdato` >= NOW()'], true);

        foreach ($kommendeAvdrag as $avdrag) {
            $betalingsplan = $avdrag->betalingsplan;
            $leieforhold = $betalingsplan->leieforhold;
            $epost = $leieforhold->hentEpost();
            if (!$epost) {
                $betalingsplan->forfallspåminnelseAntDager = null;
                continue;
            }
            $innhold = $this->hentValg('betalingsplan_påminnelse_mal') ?? '';
            $innhold = str_replace([
                '{forfallsdato}',
                '{beløp}',
                '{leieforholdnr}',
                '{leieforhold}',
                '{kid}',
            ],
                [
                    $avdrag->forfallsdato->format('d.m.Y'),
                    $this->kr($avdrag->beløp),
                    $leieforhold->hentId(),
                    $leieforhold->hentBeskrivelse(),
                    $leieforhold->hentKid()
                ],
                $innhold);
            $tittel = 'Påminnelse om kommende avdrag i betalingsplan';
            /** @var Visning\felles\epost\shared\Epost $html */
            $html = $this->hentVisning(Visning\felles\epost\shared\Epost::class, [
                'tittel' => $tittel,
                'innhold' => $innhold
            ]);
            $tekst = $this->hentVisning(Visning\felles\epost\shared\Epost::class, [
                'tittel' => $tittel,
                'innhold' => Visning\felles\epost\shared\Epost::br2crnl($innhold)
            ])->settMal('felles/epost/shared/Epost.txt');

            $this->sendMail((object)[
                'to' => implode(',', $epost),
                'subject' => $tittel,
                'html' => $html,
                'text' => $tekst,
                'priority' => 60,
                'type' => 'betalingsplan_kommende_avdrag'
            ]);
            $avdrag->påminnelseSendt = new DateTime();
        }
        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function varsleBetalingsplanManglendeAvdrag(): Leiebase
    {
        $this->pre($this, __FUNCTION__, []);
        /** @var Avdragsett $forfalteAvdrag */
        $forfalteAvdrag = $this->hentSamling(Avdrag::class);
        $forfalteAvdrag->leggTilBetalingsplanModell();
        $forfalteAvdrag->leggTilFilter(['utestående >' => 0]);
        $forfalteAvdrag->leggTilFilter(['`' . Betalingsplan::hentTabell() . '`.`aktiv`' => 1], true);
        $forfalteAvdrag->leggTilFilter(['etterlysning_sendt' => null]);
        $forfalteAvdrag->leggTilFilter(['DATE_ADD(`' . Avdrag::hentTabell() . '`.`forfallsdato`, INTERVAL ' . ($this->hentValg('betalingsplan_etterlysning_ant_dager') ?? 25) . ' DAY)  < NOW()'], true);

        foreach ($forfalteAvdrag as $avdrag) {
            $avdrag->sendEtterlysning();
        }
        return $this->post($this, __FUNCTION__, $this);
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function varsleBetalingsplanBruttAvtale(): Leiebase
    {
        /** @var Avdragsett $forfalteAvdrag */
        $forfalteAvdrag = $this->hentSamling(Avdrag::class);
        $forfalteAvdrag->leggTilBetalingsplanModell();
        $forfalteAvdrag->leggTilFilter(['utestående >' => 0]);
        $forfalteAvdrag->leggTilFilter(['`' . Betalingsplan::hentTabell() . '`.`aktiv`' => 1], true);
        $forfalteAvdrag->leggTilFilter(['DATE_ADD(`' . Avdrag::hentTabell() . '`.`forfallsdato`, INTERVAL ' . ($this->hentValg('betalingsplan_bruddvarsel_ant_dager') ?? 32) . ' DAY)  < NOW()'], true);

        foreach ($forfalteAvdrag as $avdrag) {
            $betalingsplan = $avdrag->betalingsplan;
            if($betalingsplan->aktiv) {
                $betalingsplan->deaktiverVedBrudd();
            }
        }
        return $this;
    }

    /**
     * Varsle fornying
     *
     * @return $this
     * @throws Exception
     */
    public function varsleFornying(): Leiebase
    {
        $this->hentValg();
        $utc = new DateTimeZone('utc');
        $varselstempelKontraktutløp = date_create_immutable('@' . $this->hentValg('varselstempel_kontraktutløp'), $utc);

        $frist = date_create_immutable('now', $utc)->add(new DateInterval('P1M'));

        /** @var Leieforholdsett $leieforholdsett */
        $leieforholdsett = $this->hentSamling(LeieforholdModell::class);
        $leieforholdsett->leggTilFilter(['`' . Oppsigelse::hentTabell() . '`.`' . Oppsigelse::hentPrimærnøkkelfelt() . '`' => null]);
        $leieforholdsett->leggTilFilter(['`' . LeieforholdModell::hentTabell() . '`.`tildato` >' => $varselstempelKontraktutløp->format('Y-m-d H:i:s')]);
        $leieforholdsett->leggTilFilter(['`' . LeieforholdModell::hentTabell() . '`.`tildato` <=' => $frist->format('Y-m-d H:i:s')]);

        foreach ($leieforholdsett as $leieforhold) {
            $adressefelt = implode(', ', $leieforhold->hentEpost());

            if (trim($adressefelt)) {
                $html = str_ireplace(
                    ["<br>", "<br>", "<br>"],
                    "<br>\r\n",
                    str_replace(
                        [
                            '{leieobjekt}',
                            '{tildato}'],
                        [
                            $leieforhold->leieobjekt->hentType() . ' nr. ' . $leieforhold->leieobjekt->hentId() . ': ' . $leieforhold->leieobjekt->hentBeskrivelse(),
                            $leieforhold->tildato->format('d.m.Y')
                        ],
                        $this->hentValg('utløpsvarseltekst') . '<br>' .
                        $this->hentValg('eposttekst')
                    )
                );

                $this->sendMail((object)array(
                    'to' => $adressefelt,
                    'type' => 'leieforhold_utløpsvarsel',
                    'subject' => "Påminnelse om at din leieavtale må fornyes",
                    'html' => $html
                ));
            }
        }
        $this->settValg('varselstempel_kontraktutløp', $frist->format('U'));
        return $this;
    }


    /**
     * @param DateTimeInterface|null $tid Første betaling som kan varsles. Normalt 'nå'.
     * @return $this
     * @throws Exception
     */
    public function varsleNyeInnbetalinger(DateTimeInterface $tid = null)
    {
        $tid = $tid ?: new DateTimeImmutable();
        $tid->setTimezone(new DateTimeZone('utc'));

        $varselstempelInnbetalinger = date_create_immutable("@{$this->hentValg('varselstempel_innbetalinger')}");

        if ($tid < $varselstempelInnbetalinger) {
            return $this;
        }

        $innbetalinger = $this->hentSamling(InnbetalingsModell::class)
            ->leggTilFilter([
                'leieforhold IS NOT NULL',
                "konto != '0'",
                'registrert >' => $varselstempelInnbetalinger->format('Y-m-d H:i:s'),
                'registrert <' => $tid->format('Y-m-d H:i:s'),
            ]);

        /** @var InnbetalingsModell $innbetaling */
        foreach ($innbetalinger as $innbetaling) {
            try {
                $innbetaling->sendKvitteringsepost();
            } catch (Exception $e) {
                $this->logger->error('Kunne ikke sende kvittering for betaling ' . $innbetaling->hentId());
                continue;
            }
        }

        $this->settValg('varselstempel_innbetalinger', $tid->getTimestamp());
        return $this;
    }

    /**
     * Ved
     *
     * Knagger en handling til et stadium
     *
     * @param string $stadium stadiet det skal knagges en handling til
     * @param callable|array $handlinger
     * @throws Exception
     */
    public function ved($stadium, $handlinger)
    {
        settype($handlinger, 'array');

        foreach ($handlinger as $handling) {
            if (is_callable($handling)) {
                $this->knagger[$stadium][] = $handling;
            } else {
                throw new Exception("Kan ikke utføre " . print_r($handling, true) . "");
            }
        }

    }

    /**
     * @param $svaradresse
     * @return \Person|null
     */
    public function hentPersonFraEpost($epostadresse)
    {
        $epostadresse = trim($epostadresse);
        $tp = $this->mysqli->table_prefix;
        $resultat = $this->mysqli->arrayData([
            'class' => \Person::class,
            'fields' => ['id' => "personid"],
            'source' => "{$tp}personer",
            'where' => ['epost' => $epostadresse]
        ]);
        if ($resultat->totalRows) {
            $person = $resultat->data[0];
        } else {
            $person = null;
        }
        return $person;
    }

    /**
     * @param string $saksref
     * @return \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd|null
     */
    public function hentBeboerstøtteSakFraRef($saksref)
    {
        $saksref = trim($saksref);
        /** @var \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd|null $sakstråd */
        $sakstråd = $this->hentSamling(\Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd::class)
            ->leggTilFilter(['saksref' => $saksref])->låsFiltre()->hentFørste();
        return $sakstråd;
    }

    /**
     * @param string $klasse
     * @param $id
     * @return Modell
     * @throws Exception
     */
    public function hentModell($klasse, $id)
    {
        $klasse = self::getClassPreference($klasse);
        $modell = $this->modellOppbevaring->hent($klasse, $id);
        if (!$modell) {
            // Prepare dependency injection
            $di = [
                AppInterface::class => $this->hentCoreModelImplementering(),
                mysqli::class => $this->mysqli,
                CoreModelRepositoryInterface::class => $this->modellOppbevaring
            ];
            $modell = $this->modellFabrikk::lag($klasse, $di, $id);
        }

        return $modell;
    }

    /**
     * @param string|array $modell
     * @return Samling
     * @noinspection PhpDocMissingThrowsInspection
     */
    public function hentSamling($modell): Samling
    {
        $di = [
            AppInterface::class => $this->hentCoreModelImplementering(),
            mysqli::class => $this->mysqli,
            CoreModelRepositoryInterface::class => $this->modellOppbevaring,
            CoreModelFactory::class => $this->modellFabrikk,
        ];
        /** @var Samling $samling */
        $samling = $this->samlingsFabrikk::lag($modell, $di);
        return $samling;
    }

    /**
     * @return Leieobjektsett
     */
    public function hentAktiveLeieobjekter(): Leieobjektsett
    {
        return $this->hentSamling(Modell\Leieobjekt::class)
            ->leggTilFilter(['ikke_for_utleie' => false])
            ->låsFiltre();
    }

    /**
     * @param \class-string<Kyegil\CoreModel\Interfaces\CoreModelInterface> $klasse
     * @param stdClass $parametere
     * @return Modell
     * @throws Exception
     */
    public function nyModell($klasse, stdClass $parametere = null): \Kyegil\CoreModel\Interfaces\CoreModelInterface
    {
        if (!isset($parametere)) {
            $parametere = new stdClass();
        }
        // Prepare dependency injection
        $di = [
            AppInterface::class => $this->hentCoreModelImplementering(),
            mysqli::class => $this->mysqli,
            CoreModelRepositoryInterface::class => $this->modellOppbevaring
        ];

        $modell = $this->modellFabrikk::lag($klasse, $di);
        $modell->create($parametere);
        if ($modell->getId()) {
            $this->modellOppbevaring->oppbevar($modell);
        } else {
            throw new Exception('Klarte ikke lage modell av ' . $klasse . '. Parametere:' . json_encode($parametere));
        }
        return $modell;
    }

    /**
     * @return CoreModelImplementering
     * @noinspection PhpDocMissingThrowsInspection
     */
    public function hentCoreModelImplementering(): CoreModelImplementering
    {
        if ($this instanceof CoreModelImplementering) {
            return $this;
        }
        if ($this->coreModelImplementering instanceof CoreModelImplementering) {
            return $this->coreModelImplementering;
        }
        $this->coreModelImplementering = new \Kyegil\Leiebasen\Oppslag\AbstraktKontroller(\Kyegil\Leiebasen\Oppslagsbestyrer::forberedDependencies(), self::$config);
        $this->coreModelImplementering->mysqli = $this->mysqli;
        $this->coreModelImplementering->tillegg = $this->tillegg;
        $this->coreModelImplementering->bruker = $this->bruker;
        $this->coreModelImplementering->modellOppbevaring = $this->modellOppbevaring;
        return $this->coreModelImplementering;
    }

    /**
     * @param string $underscore
     * @return string Camelcase version of the string
     */
    public function tilCamelCase($underscore)
    {
        if (!isset($this->mellomlager->camelCase[$underscore])) {
            $camelCase = str_replace(' ', '', ucwords(str_replace('_', ' ', mb_strtolower($underscore))));
            $this->mellomlager->camelCase[$underscore] = $camelCase;
            $this->mellomlager->underscore[$camelCase] = $underscore;
        }
        return $this->mellomlager->camelCase[$underscore];
    }

    /**
     * @param string $camelCase
     * @return string Underscore version of the string
     */
    public function tilUnderscore($camelCase)
    {
        if (!isset($this->mellomlager->underscore[$camelCase])) {
            $underscore = mb_strtolower(mb_ereg_replace('(?<=\\w)([A-ZÆØÅ])', '_\\1', $camelCase));
            $this->mellomlager->underscore[$camelCase] = $underscore;
            $this->mellomlager->camelCase[$underscore] = $camelCase;
        }
        return $this->mellomlager->underscore[$camelCase];
    }

    /**
     * @param object|string $subject
     * @param string $method
     * @param array $args
     * @return array $args
     */
    public function pre($subject, string $method, array $args): array
    {
        return EventManager::getInstance()->triggerEvent(
            is_object($subject) ? get_class($subject) : $subject,
            'pre' . self::ucfirst($method),
            is_object($subject) ? $subject : null,
            $args,
            $this->hentCoreModelImplementering()
        );
    }

    /**
     * @param object|string $subject
     * @param $method
     * @param $result
     * @param array $args Additional arguments
     * @return mixed
     */
    public function post($subject, $method, $result, array $args = [])
    {
        array_unshift($args, $result);
        $result = EventManager::getInstance()->triggerEvent(
                is_object($subject) ? get_class($subject) : $subject,
                'post' . self::ucfirst($method),
                is_object($subject) ? $subject : null,
                $args,
                $this->hentCoreModelImplementering()
        );
        return isset($result) ? reset($result) : null;
    }

    /**
     * Svarer med ei 403 Forbidden status-side
     */
    public function responder403()
    {
        $protocol = $_SERVER["SERVER_PROTOCOL"] ?? 'HTTP/1.1';
        header($protocol . ' 403 Forbidden');
        echo $this->vis(\Kyegil\Leiebasen\Visning\felles\html\Http403Forbidden::class);
        die();
    }

    /**
     * Svarer med ei 404 Not Found status-side
     */
    public function responder404()
    {
        $protocol = $_SERVER["SERVER_PROTOCOL"] ?? 'HTTP/1.1';
        header($protocol . ' 404 Not Found');
        echo $this->vis(\Kyegil\Leiebasen\Visning\felles\html\Http404NotFound::class);
        die();
    }

    /**
     * Hent et View-objekt
     *
     * @param class-string<Visning> $mal
     * @param array $data
     * @return Visning
     */
    public function hentVisning($mal, array $data = []): ViewInterface
    {
        $mal = self::getClassPreference($mal);
        return $this->viewFactory->createView($mal, $data);
    }

    /**
     * Vis (alias for hentVisning())
     *
     * @param class-string<Visning> $mal
     * @param array $data
     * @return Visning
     */
    public function vis($mal, array $data = []): ViewInterface
    {
        return $this->hentVisning($mal, $data);
    }

    /**
     * @param int $tjeneste
     * Oppkopling::TJENESTE_OCR = 9 |
     * Oppkopling::TJENESTE_AVTALEGIRO = 21 |
     * Oppkopling::TJENESTE_EFAKTURA = 42
     * @return Oppkopling
     */
    public function hentNetsForbindelse(int $tjeneste): Oppkopling
    {
        $this->logger->debug('GIROUTSKRIFT – Henter NETS-forbindelse', ['tjeneste' => $tjeneste]);
        /** OCR og AvtaleGiro deler forbindelse */
        if ($tjeneste == Oppkopling::TJENESTE_AVTALEGIRO) {
            $tjeneste = Oppkopling::TJENESTE_OCR;
        }
        if (!isset($this->netsForbindelse[$tjeneste])) {
            $this->netsForbindelse[$tjeneste] = new Oppkopling($this, $tjeneste, $this->filarkiv);
        }
        return $this->netsForbindelse[$tjeneste];
    }

    /**
     * @return string
     */
    public function hentFilarkivBane()
    {
        return $this->filarkiv;
    }

    /**
     * Hent RFC-5646 språk-kode for oppslaget
     *
     * @return string
     * @link https://www.w3.org/International/articles/language-tags/
     */
    public function hentSpråk()
    {
        if (!isset($this->språk)) {
            $preferanse = $this->hentSpråkPreferanse();
            if (in_array(substr($preferanse, 0, 2), ['no', 'nb', 'nn'])) {
                $this->språk = 'nb-NO';
            } else {
                foreach (array_keys($this->tilgjengeligeSpråk) as $kode) {
                    if ($preferanse == $kode) {
                        $this->språk = $kode;
                    } else if (
                        !isset($this->språk)
                        && substr($preferanse, 0, 2) == substr($kode, 0, 2)
                    ) {
                        $this->språk = $kode;
                    } else if (substr($kode, 0, 2) == 'en') {
                        $this->språk = $kode;
                    }
                }
                if (!isset($this->språk) && isset($this->tilgjengeligeSpråk['en'])) {
                    $this->språk = 'en';
                }
                $this->språk = $this->språk ?: 'nb-NO';
            }
        }
        return $this->språk;
    }

    /**
     * Sett språk for siden
     *
     * @param string|null $språk
     * @return $this
     */
    public function settSpråk($språk = null)
    {
        if (!isset($språk) || in_array($språk, array_keys($this->tilgjengeligeSpråk))
        ) {
            $this->språk = $språk;
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function hentSpråkPreferanse()
    {
        $preferanse = str_replace('_', '-', \Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE'] ?? ''));
        if (isset($this->bruker['språk'])) {
            $preferanse = $this->bruker['språk'];
        }
        return $preferanse;
    }

    /**
     * Valider en e-postadresse
     *
     * @param string $epost
     * @return $this
     * @throws Exception
     */
    public function validerEpost(string $epost)
    {
        if ($epost && !filter_var($epost, FILTER_VALIDATE_EMAIL)) {
            throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Ugyldig epostadresse');
        }
        return $this;
    }

    /**
     * @return Delkravtypesett
     * @throws Exception
     */
    public function hentDelkravtyper(): Delkravtypesett
    {
        /** @var Delkravtypesett $delkravtypesett */
        $delkravtypesett = $this->hentSamling(Delkravtype::class);
        $delkravtypesett
            ->leggTilFilter(['aktiv' => true])
            ->leggTilSortering('orden');
        return clone $delkravtypesett;
    }

    /**
     * Slett gamle midlertidige filer (f.eks. ubrukte epostvedlegg)
     *
     * @return $this
     */
    public function slettGamleMidlertidigeFiler()
    {
        $mappe = $this->hentFilarkivBane() . '/midlertidig/';
        if (is_dir($mappe)) {
            $filer = scandir($mappe);
            foreach ($filer as $fil) {
                if (in_array($fil, ['.', '..'])) {
                    continue;
                }
                if (time() - filemtime($mappe . $fil) > 86400) {
                    // Fila er mer enn 24t gammel
                    unlink($mappe . $fil);
                }
            }
        }
        return $this;
    }

    /**
     * @return Utskriftsprosessor
     */
    public function hentUtskriftsprosessor(): Utskriftsprosessor
    {
        return $this->utskriftsprosessor;
    }

    public function hentPoengbestyrer(): Poengbestyrer
    {
        return Poengbestyrer::getInstance($this->hentCoreModelImplementering());
    }

    /**
     * @param string $innstilling
     * @param string $verdi
     * @return $this
     * @throws Exception
     */
    public function settValg(string $innstilling, string $verdi): Leiebase
    {
        $tp = $this->mysqli->table_prefix;
        $this->mysqli->save([
            'table' => $tp . 'valg',
            'replace' => true,
            'fields' => [
                'innstilling' => $innstilling,
                'verdi' => $verdi
            ],
        ]);
        $this->valg = null;
        return $this;
    }

    /**
     * @return NetsProsessor
     */
    public function hentNetsProsessor(): NetsProsessor
    {
        if (!isset($this->netsProsessor)) {
            $this->netsProsessor = new NetsProsessor($this);
        }
        return $this->netsProsessor;
    }

    /**
     * @return Kid
     */
    public function hentKidBestyrer(): Kid
    {
        /** @var class-string<Kid> $klasse */
        $klasse = self::getClassPreference(Kid::class);
        return $klasse::getInstance($this);
    }

    /**
     * @return Epostprosessor
     * @throws Exception
     */
    public function hentEpostprosessor(): EpostProsessor
    {
        if(!isset($this->epostprosessor)) {
            EventManager::getInstance()->triggerEvent(Leiebase::class, 'mapEpostprosessor', $this, [], $this->hentCoreModelImplementering());
            $epostTransport = $this->hentValg('epost_transport');
            if(!in_array($epostTransport, array_keys(self::$epostprosessorMapping))) {
                $epostTransport = self::EPOST_TRANSPORT_TEST_MAIL;
            }
            /** @var class-string<EpostProsessor> $prosessorKlasse */
            $prosessorKlasse = self::$epostprosessorMapping[$epostTransport] ?? Kyegil\Leiebasen\Epostprosessor\Php::class;
            $prosessorKlasse = self::getClassPreference($prosessorKlasse);
            if(!is_a($prosessorKlasse, EpostProsessor::class, true)) {
                $prosessorKlasse = \Kyegil\Leiebasen\Epostprosessor\Php::class;
            }
            $this->epostprosessor = $prosessorKlasse::getInstance($this->hentCoreModelImplementering());
        }
        return $this->epostprosessor;
    }

    /**
     * @return SmsProsessor
     */
    public function hentSmsProsessor(): SmsProsessor
    {
        if(!isset($this->smsProsessor)) {
            EventManager::getInstance()->triggerEvent(Leiebase::class, 'mapSmsProsessor', $this, [], $this->hentCoreModelImplementering());
            $smsTransport = $this->hentValg('sms_transport');
            if(!in_array($smsTransport, array_keys(self::$smsProsessorMapping))) {
                $smsTransport = self::SMS_TRANSPORT_VOID;
            }
            /** @var class-string<SmsProsessor> $prosessorKlasse */
            $prosessorKlasse = self::$smsProsessorMapping[$smsTransport] ?? \Kyegil\Leiebasen\Sms\VoidSms::class;
            $prosessorKlasse = self::getClassPreference($prosessorKlasse);
            if(!is_a($prosessorKlasse, SmsProsessor::class, true)) {
                $prosessorKlasse = \Kyegil\Leiebasen\Sms\VoidSms::class;
            }
            $this->smsProsessor = $prosessorKlasse::getInstance($this->hentCoreModelImplementering());
        }
        return $this->smsProsessor;
    }

    /**
     * @param int $nivå
     * @return array
     * @throws Exception
     */
    public function hentAdvarsler(string $nivå): array {
        if(!isset($this->advarsler)) {
            $this->advarsler[self::VARSELNIVÅ_ALARM] = [];
            $this->advarsler[self::VARSELNIVÅ_ADVARSEL] = [];
            $this->advarsler[self::VARSELNIVÅ_ORIENTERING] = [];
            $this->kontrollrutiner();
        }
        return $this->advarsler[$nivå] ?? [];
    }

    /**
     * @param int $nivå
     * @param array $varsel
     * @return $this
     * @throws Exception
     */
    public function settAdvarsel(string $nivå, array $varsel): Leiebase {
        if(!isset($this->advarsler)) {
            $this->hentAdvarsler($nivå);
        }
        if(in_array($nivå, [
                self::VARSELNIVÅ_ALARM,
            self::VARSELNIVÅ_ADVARSEL,
            self::VARSELNIVÅ_ORIENTERING
        ])) {
            $this->advarsler[$nivå][] = $varsel;
        }
        return $this;
    }

    /**
     * @param string $meny
     * @param Person|null $bruker
     * @return object[]|string[]
     * @throws Exception
     */
    public function hentMenyStruktur(string $meny = '', ?Kyegil\Leiebasen\Modell\Person $bruker = null): array
    {
        return [];
    }

    /**
     * @param string|null $oppslag
     * @return array
     */
    public function hentAktiveMenyElementer(?string $oppslag = null): array
    {
        $oppslag = $oppslag ?? ($_GET['oppslag'] ?? null);

        $menyElementMapping = [];
        return $menyElementMapping[$oppslag] ?? [$oppslag];
    }

    /**
     * @param Utskriftsprosessor $utskriftsprosessor
     * @return Leiebase
     */
    public function settUtskriftsprosessor(Utskriftsprosessor $utskriftsprosessor): Leiebase
    {
        $this->utskriftsprosessor = $utskriftsprosessor;
        return $this;
    }

    /**
     * @param DateInterval $betalingsfrist
     * @return string
     * @throws Throwable
     */
    public function betalingsfristFormatert(DateInterval $betalingsfrist): string
    {
        if(!$betalingsfrist->d && !$betalingsfrist->m && !$betalingsfrist->y) {
            return 'forskuddsvis';
        }
        return 'innen ' . Leiebase::periodeformat($betalingsfrist);
    }

    /**
     * @param Modell\Leieobjekt $leieobjekt
     * @param Personsett $leietakere
     * @param int $antallTerminer
     * @param DateTimeInterface $fradato
     * @param Fraction|null $andel
     * @param DateTimeInterface|null $tildato
     * @param DateInterval|null $oppsigelsestid
     * @param float $årligLeiebeløp
     * @param string $avtalemal
     * @param string|null $fastDato
     * @param int|null $dagIMåneden
     * @param int|null $ukedag
     * @param DateInterval|null $terminBetalingsfrist
     * @param Personsett|null $andreBeboere
     * @param float[] $delkravSatser Sats i kr eller prosent for hver delkravtype-id
     * @param bool $periodiskStrømavstemming
     * @return LeieforholdModell
     * @throws Throwable
     */
    public function opprettLeieforhold(
        Modell\Leieobjekt  $leieobjekt,
        Personsett         $leietakere,
        int                $antallTerminer,
        DateTimeInterface  $fradato,
        ?Fraction          $andel = null,
        ?DateTimeInterface $tildato = null,
        ?DateInterval      $oppsigelsestid = null,
        float              $årligLeiebeløp = 0,
        string             $avtalemal = '',
        ?string            $fastDato = null,
        ?int               $dagIMåneden = null,
        ?int               $ukedag = null,
        ?DateInterval      $terminBetalingsfrist = null,
        ?Personsett        $andreBeboere = null,
        array              $delkravSatser = [],
        bool               $periodiskStrømavstemming = false
    ): LeieforholdModell
    {
        extract($this->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $andel = $andel ?? new Fraction(1);
        $oppsigelsestid = $oppsigelsestid ?? new DateInterval('P0D');

        if(!$leietakere->hentAntall()) {
            throw new Exception('Leieforholdet må ha minst én leietaker');
        }

        /** @var Delkravtypesett $alleDelkravtyper */
        $alleDelkravtyper = $this->hentDelkravtyper()->leggTilFilter(
            ['kravtype' => KravModell::TYPE_HUSLEIE]
        )->låsFiltre();

        /** @var Delkravtypesett $delkravtyper */
        $delkravtyper = $this->hentDelkravtyper()
            ->leggTilFilter(['kravtype' => KravModell::TYPE_HUSLEIE])
            ->låsFiltre();

        $basisleie = $this->beregnBasisLeie($årligLeiebeløp, $delkravSatser);

        /** @var LeieforholdModell $leieforhold */
        $leieforhold = $this->nyModell(LeieforholdModell::class, (object)[
            'leieobjekt' => $leieobjekt,
            'leietakere' => $leietakere,
            'andel' => $andel,
            'fradato' => $fradato,
            'tildato' => $tildato,
            'termin_betalingsfrist' => $terminBetalingsfrist,
            'oppsigelsestid' => $oppsigelsestid,
            'ant_terminer' => $antallTerminer,
            'årlig_basisleie' => $basisleie,
            'avtaletekstmal' => $avtalemal,
            'regning_til_objekt' => true,
            'regningsobjekt' => $leieobjekt
        ]);
        if (!$leieforhold->hentId()) {
            throw new Exception('Kunne ikke opprette leieforholdet av ukjent grunn.');
        }
        $leieforhold->settNavn()->settBeskrivelse()->settTilfeldigRegningsperson();
        $leieforhold
            ->settForfallFastDato($fastDato)
            ->settForfallFastDagIMåneden($dagIMåneden)
            ->settForfallFastUkedag($ukedag);

        $leieforhold->oppdaterLeie();

        if ($andreBeboere->hentAntall()) {
            $leieforhold->settAndreBeboere($andreBeboere);
        }

        foreach ($delkravtyper as $delkravtype) {
            $sats = $delkravSatser[$delkravtype->hentId()] ?? 0;
            if($sats > 0) {
                $leieforhold->leggTilDelkravtype(
                    $delkravtype,
                    null,
                    null,
                    $delkravSatser[$delkravtype->hentId()] ?? 0,
                    $periodiskStrømavstemming && ($delkravtype->hentId() == $this->hentValg('delkravtype_fellesstrøm'))
                );
            }

        }
        $leieforhold->kontrakt->settTekst($leieforhold->gjengiAvtaletekst());
        return $this->post($this, __FUNCTION__, $leieforhold, $args);
    }

    /**
     * @param DateTimeInterface $frist
     * @return $this
     */
    public function sjekkOgVarsleUtløpendeDepositum(DateTimeInterface $frist): Leiebase
    {
        list('frist' => $frist) = $this->pre($this, __FUNCTION__, ['frist' => $frist]);
        $this->logger->debug('DEPOSITUM – Ser etter depositumsgarantier i ferd med å utløpe');

        /** @var Depositumsett $depositumSett */
        $depositumSett = $this->hentSamling(Depositum::class);
        $depositumSett->leggTilFilter(['`' . Depositum::hentTabell() . '`.`utløpsdato` <' => $frist->format('Y-m-d')])
            ->låsFiltre();

        /** @var LeieforholdModell $datoGrupper */
        $datoGrupper = [];
        /** @var array[][] $depositumVarsler */
        $depositumVarsler = [];
        $varselNivå = Leiebase::VARSELNIVÅ_ORIENTERING;

        if($depositumSett->hentAntall()) {
            $this->logger->info('DEPOSITUM – ' . $depositumSett->hentAntall() . ' garantier er i ferd med å utløpe', ['depositum' => $depositumSett->hentIdNumre()]);
            foreach ($depositumSett as $depositum) {
                if ($varselNivå == Leiebase::VARSELNIVÅ_ORIENTERING &&
                    $depositum->hentUtløpsdato() <= date_create_immutable()
                ) {
                    $varselNivå = Leiebase::VARSELNIVÅ_ADVARSEL;
                }
                $datoGrupper[$depositum->hentUtløpsdato()->format('Y-m-d')][] = $depositum->hentLeieforhold();
            }
            ksort($datoGrupper);

            /**
             * @var string $utløpsdato
             * @var LeieforholdModell $leieforholdsett
             */
            foreach ($datoGrupper as $utløpsdato => $leieforholdsett) {
                /** @var DateTimeImmutable $utløpsdato */
                $utløpsdato = date_create_immutable($utløpsdato);
                settype($depositumVarsler[$varselNivå], 'array');
                $datoAntall = count($leieforholdsett);
                /** @var string[] $leieforholdLenker */
                $leieforholdLenker = [];
                foreach ($leieforholdsett as $leieforhold) {
                    $leieforholdLenker[] = DriftKontroller::lenkeTilLeieforholdKort($leieforhold, $leieforhold->hentNavn());
                }
                $oppsummering = $datoAntall . ' depositumsgaranti' . ($datoAntall > 1 ? 'er' : '') . ' utløper snart';
                $tekst = 'Depositumsgarantien utløper den ' . $utløpsdato->format('d.m.Y')
                    . ' for ' . $this->liste($leieforholdLenker);

                $depositumVarsler[$varselNivå][] = [
                    'oppsummering' => $oppsummering,
                    'tekst' => $tekst,
                ];
            }
        }
        $advarsler = json_decode($this->hentValg('advarsler_drift')) ?? new \stdClass();
        $advarsler->utløpende_depositum = $depositumVarsler;
        $this->settValg('advarsler_drift', json_encode($advarsler));
        $varselMal = $this->hentValg('depositum_utløpsvarsel_tekst');
        $this->logger->debug('DEPOSITUM – Oppdatert varsel i drift');

        $depositumSett->forEach(function (Depositum $depositum) use ($varselMal) {

            $utløpspåminnelser = $depositum->hentUtløpspåminnelser() ?? [];
            $sisteVarsel = $utløpspåminnelser ? max($utløpspåminnelser) : null;
            if (!$sisteVarsel || $sisteVarsel < date_create_immutable()->sub(new \DateInterval('P1M'))) {
                if ($brukerepost = $depositum->leieforhold->hentEpost()) {
                    $varselTekst = str_replace(
                        ['{leieforhold}', '{utløpsdato}'],
                        [$depositum->leieforhold, $depositum->hentUtløpsdato()->format('d.m.Y')],
                        $varselMal);
                    $tittel = 'Depositumsgarantien din utløper den ' . $depositum->hentUtløpsdato()->format('d.m.Y');

                    /** @var Visning\felles\epost\shared\Epost $html */
                    $html = $this->hentVisning(Visning\felles\epost\shared\Epost::class, [
                        'tittel' => $tittel,
                        'innhold' => $varselTekst
                    ]);

                    $this->sendMail((object)[
                        'to' => implode(',', $brukerepost),
                        'subject' => $tittel,
                        'html' => $html,
                        'type' => 'depositum_utløpsvarsel',
                    ]);
                    $utløpspåminnelser[] = new DateTimeImmutable();
                    $depositum->utløpspåminnelser = $utløpspåminnelser;
                    $this->logger->info('DEPOSITUM – Sendt påminnelse om fornying av garanti til leieforhold ' . $depositum->leieforhold);
                }
            }
        });
        $this->logger->debug('DEPOSITUM – Fullført sjekk');
        return $this->post($this, __FUNCTION__, $this);
    }

    /**
     *  Lås krav som har blitt helt eller delvis betalt
     *
     * @return $this
     * @throws Exception
     */
    public function låsKravMedBetalinger(): Leiebase
    {
        $kravsett = $this->hentSamling(KravModell::class);
        $kravsett->leggTilInnerJoin(InnbetalingsModell::hentTabell(),
            '`' . KravModell::hentTabell() . '`.`' . KravModell::hentPrimærnøkkelfelt() . '` = `' . InnbetalingsModell::hentTabell() . '`.`krav`'
        );
        $kravsett
            ->leggTilFilter(['`' . KravModell::hentTabell() . '`.`beløp` >' => 0])
            ->leggTilFilter(['`' . KravModell::hentTabell() . '`.`reell_krav_id`' => null])
            ->leggTilFilter(['`' . InnbetalingsModell::hentTabell() . '`.`oppdatert` < NOW() - INTERVAL 1 DAY'])
            ->låsFiltre();
        $kravsett->forEach(function (KravModell $krav) {
            $krav->settReellKravId();
        });
        return $this;
    }

    /**
     * Logg feilmeldinger
     *
     * @param Throwable $e
     * @return $this
     */
    public function loggException(Throwable $e): Leiebase
    {
        $this->logger->warning('Exception: ' . $e->getMessage() . ' in ' . $e->getFile() . ' on line ' . $e->getLine(), $e->getTrace());
        return $this;
    }

    /**
     * @param \DateTimeInterface|null $dato
     * @return Leieforholdsett
     * @throws \Kyegil\CoreModel\CoreModelException
     */
    public function hentAktiveLeieforhold(?\DateTimeInterface $dato = null): Leieforholdsett
    {
        $dato = $dato ?: new \DateTimeImmutable();
        /** @var Leieforholdsett $leieforholdsett */
        $leieforholdsett = $this->hentSamling(LeieforholdModell::class);
        $leieforholdsett->leggTilOppsigelseModell();
        $leieforholdsett->leggTilFilter(['`' . LeieforholdModell::hentTabell() . '`.`fradato` <=' => $dato->format('Y-m-d')]);
        $leieforholdsett->leggTilFilter(['or' => [
            '`' . Oppsigelse::hentTabell() . '`.`fristillelsesdato` >' => $dato->format('Y-m-d'),
            '`' . Oppsigelse::hentTabell() . '`.`fristillelsesdato` IS NULL',
        ]]);
        $leieforholdsett->låsFiltre();
        return $leieforholdsett;
    }

    /**
     * Korriger delkrav-satsene og beregn basis-leie
     *
     * @param float $bruttoleie
     * @param float[] $delkravbeløp Delkravbeløpene angitt som Delkrav-id => beløp
     * @return float
     * @throws Exception
     */
    public function beregnBasisLeie(float $bruttoleie, array &$delkravbeløp): float
    {
        $basisleie = $bruttoleie;
        // Nevneren representerer summen av alle relative delbeløp (ikke tillegg) + 100%
        $nevner = 1;
        /** @var Delkravtypesett $delkravtyper */
        $delkravtyper = $this->hentDelkravtyper()
            ->leggTilFilter(['kravtype' => KravModell::TYPE_HUSLEIE])
            ->låsFiltre();
        /** @var Delkravtype $delkravtype */
        foreach ($delkravtyper as $delkravtype) {
            $angittSats = $delkravbeløp[$delkravtype->id] ?? 0;
            if ($angittSats != 0) {
                // Alle relative satser endres fra prosent- til desimal-notering
                if ($delkravtype->relativ) {
                    $angittSats = $delkravbeløp[$delkravtype->id] = $angittSats / 100;
                }

                // Alle delbeløp, både faste og relative, er med på å redusere basisleia
                if (!$delkravtype->selvstendigTillegg && $basisleie > 0 && $angittSats > 0) {
                    if ($delkravtype->relativ) {
                        $nevner += $angittSats;
                    } else {
                        $angittSats = min($basisleie, $angittSats);
                        $delkravbeløp[$delkravtype->id] = $angittSats;
                        $basisleie -= $angittSats;
                        $basisleie = max($basisleie, 0);
                    }
                }
            }
        }

        return (string)round($basisleie / $nevner, 2);
    }
}