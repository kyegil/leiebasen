<?php

/**
 * @deprecated
 * @see \Kyegil\Leiebasen\Modell\Leieobjekt
 */
class Leieobjekt extends DatabaseObjekt {

    /**
     * @var string
     */
    protected    $tabell = "leieobjekter";    // Hvilken tabell i databasen som inneholder primærnøkkelen for dette objektet
    /**
     * @var string
     */
    protected    $idFelt = "leieobjektnr";    // Hvilket felt i tabellen som lagrer primærnøkkelen for dette objektet
    /**
     * @var
     */
    protected    $data;                // DB-verdiene lagret som et objekt. Null betyr at verdiene ikke er lastet
    /**
     * @var
     */
    public        $id;                // Unik id-streng for denne purresiden


    /**
     * Leieobjekt constructor.
     * @param array|object|null $param
     *  ->id    (heltall) gironummeret
     * @throws Exception
     */
    public function __construct($param = null ) {
        parent::__construct( $param );
    }



    /**
     * Last Leieobjektets kjernedata fra databasen
     *
     * @param int $id gironummeret
     * @return void
     * @throws Exception
     */
    protected function last($id = 0) {
        $tp = $this->mysqli->table_prefix;

        settype($id, 'integer');
        if( !$id ) {
            $id = $this->id;
        }

        $resultat = $this->mysqli->arrayData(array(
            'returnQuery'    => true,

            'fields' =>            "{$this->tabell}.{$this->idFelt} AS id,
                                {$this->tabell}.leieobjektnr,
                                {$this->tabell}.bygning,
                                {$this->tabell}.navn,
                                {$this->tabell}.gateadresse,
                                {$this->tabell}.postnr,
                                {$this->tabell}.poststed,
                                {$this->tabell}.etg,
                                {$this->tabell}.beskrivelse,
                                {$this->tabell}.bilde,
                                {$this->tabell}.areal,
                                {$this->tabell}.ant_rom,
                                {$this->tabell}.bad,
                                {$this->tabell}.toalett,
                                {$this->tabell}.boenhet,
                                {$this->tabell}.merknader,
                                {$this->tabell}.toalett_kategori,
                                {$this->tabell}.ikke_for_utleie,\n"

                            .    "bygninger.kode AS bygningskode,
                                bygninger.navn AS bygningsnavn,
                                bygninger.bilde AS bygningsbilde\n",

            'source' =>         "{$tp}{$this->tabell} AS {$this->tabell}\n"
                            .    "LEFT JOIN {$tp}bygninger AS bygninger ON {$this->tabell}.bygning = bygninger.id\n",

            'where'            =>    "{$tp}{$this->tabell}.{$this->idFelt} = '$id'"

        ));

        if( isset( $resultat->data[0] ) ) {
            $this->data = (object)array(
                'id'                => $resultat->data[0]->id,
                'leieobjektnr'        => $resultat->data[0]->leieobjektnr,
                'bygning'            => $resultat->data[0]->bygning,
                'navn'                => $resultat->data[0]->navn,
                'gateadresse'        => $resultat->data[0]->gateadresse,
                'postnr'            => $resultat->data[0]->postnr,
                'poststed'            => $resultat->data[0]->poststed,
                'etg'                => $resultat->data[0]->etg,
                'beskrivelse'        => $resultat->data[0]->beskrivelse,
                'bilde'                => $resultat->data[0]->bilde,
                'areal'                => $resultat->data[0]->areal,
                'antRom'            => $resultat->data[0]->ant_rom,
                'bad'                => $resultat->data[0]->bad,
                'toalett'            => $resultat->data[0]->toalett,
                'boenhet'            => (bool)$resultat->data[0]->boenhet,
                'merknader'            => $resultat->data[0]->merknader,
                'toalettkategori'    => $resultat->data[0]->toalett_kategori,
                'ikkeForUtleie'        => (bool)$resultat->data[0]->ikke_for_utleie
            );

            if($this->data->etg === '+') {
                $this->data->etg = "loft";
            }
            else if($this->data->etg === '0') {
                $this->data->etg = "sokkel";
            }
            else if($this->data->etg === '-1') {
                $this->data->etg = "kjeller";
            }
            else if((int)$this->data->etg) {
                $this->data->etg .= ". etg.";
            }

            $this->data->type
                = $this->data->boenhet
                ? "bolig"
                : "lokale"
            ;

            $this->data->adresse
                    = (
                    $this->data->navn
                    ? "{$this->data->navn}\n"
                    : ""
                )
                . "{$this->data->gateadresse}\n"
                . "{$this->data->postnr} {$this->data->poststed}\n";

            $this->data->beskrivelse
                = ( $this->data->navn ? "{$this->data->navn}, " : "" )
                . ( $this->data->etg ? "{$this->data->etg} " : "" )
                . ( $this->data->beskrivelse ? "{$this->data->beskrivelse} " : "" )
                . $this->data->gateadresse;
            $this->id = $id;
        }
        else {
            $this->id = null;
            $this->data = null;
        }

    }


    /**
     * Hent egenskaper
     *
     * @param string $egenskap
     * @param array $param
     * @return mixed|null
     * @throws Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Leieobjekt::hent()
     */
    public function hent($egenskap, $param = array()) {
        if( !$this->id ) {
            return null;
        }

        switch( $egenskap ) {
        case "id":
        case "adresse":
        case "antRom":
        case "areal":
        case "bad":
        case "beskrivelse":
        case "bilde":
        case "boenhet":
        case "bygning":
        case "etg":
        case "gateadresse":
        case "ikkeForUtleie":
        case "leieobjektnr":
        case "merknader":
        case "navn":
        case "postnr":
        case "poststed":
        case "toalett":
        case "toalettkategori":
        case "type":
            if ( $this->data == null ) {
                $this->last();
            }
            return $this->data->$egenskap;

    //    Parametre:
    //        dato    (DateTime-objekt) Datoen det spørres for
    //    --------------------------------------
        case "leietakere":
        case "beboere":
            if( !isset($param['dato']) ) {
                $dato = new DateTime();
            }
            else if( !is_a($param['dato'], "DateTime") ) {
                $dato = new DateTime($param['dato']);
            }
            else {
                $dato = $param['dato'];
            }

            $leietakere = array();
            $beboere = "";

            $leieforhold = $this->hentUtleie( $dato )->faktiskeLeieforhold;

            $ant = count( $leieforhold );

            foreach( $leieforhold as $nr => $lf) {
                array_push( $leietakere, $lf->hent('leietakere'));

                $beboere .= $lf->hent('navn');
                if($nr < $ant-2) $beboere .= ", ";
                if($nr == $ant-2) $beboere .= " og ";
            }

            return $$egenskap;

        default:
            return null;
        }
    }



    /**
     * Hent Leieforhold og utleiegrad for et bestemt tidsrom
     *
     *
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Leieobjekt::hentLeieforhold()
     * @see \Kyegil\Leiebasen\Modell\Leieobjekt::hentUtleiegrad()
     * @see \Kyegil\Leiebasen\Modell\Leieobjekt::hentLedighet()
     * @see \Kyegil\Leiebasen\Modell\Leieobjekt::hentUtleiegradForTidsrom()
     * @see \Kyegil\Leiebasen\Modell\Leieobjekt::hentLedighetForTidsrom()
     * @param DateTime|string $fra starten på tidsrommet utleien skal returneres for
     * @param null|DateTime|string $til normalt null (=uendelighet), slutten på tidsrommet utleien skal returneres for
     * @param Leieforhold|int|null $seBortFraLeieforhold evt. leieforhold som skal utelates
     * @return stdClass
     *  ->faktiskeLeieforhold: liste med Leieforhold-objekter eksklusive avsluttede leieforhold
     *  ->proFormaLeieforhold: liste med Leieforhold-objekter inklusive oppsigelsestid
     *  ->grad: (desimalstreng med 12 desimaler) Hvor stor del av leieobjektet som er utleid
     *  ->ledig: (desimalstreng med 12 desimaler) Hvor stor del av leieobjektet som står ledig
     * @throws Exception
     */
    public function hentUtleie($fra, $til = null, $seBortFraLeieforhold = null) {
        $tp = $this->mysqli->table_prefix;
        $resultat = (object)array(
            'faktiskeLeieforhold'    => array(),
            'proFormaLeieforhold'    => array(),
            'grad'                    => null,
            'ledig'                    => null
        );

        // Gjør om datoene til strenger
        if( $fra instanceof DateTime ) {
            $fra = $fra->format('Y-m-d');
        }
        settype( $fra, 'string' );
        if( $til instanceof DateTime ) {
            $til = $til->format('Y-m-d');
        }
        else if( !strtotime($til) ) {
            $til = '';
        }
        settype( $til, 'string' );
        settype( $seBortFraLeieforhold, 'string' );


        // Hent alle leieforhold som spenner over det aktuelle tidsrommet
        $aktuelle = $this->mysqli->arrayData(array(
            'returnQuery'    => true,
            'source'    => "{$tp}kontrakter AS kontrakter\n"
                        .   "LEFT JOIN {$tp}oppsigelser AS oppsigelser ON kontrakter.leieforhold = oppsigelser.leieforhold\n",
            'fields'        => "kontrakter.leieforhold,\n"
                            .    "MIN(fradato) AS fradato,\n"
                            .    "MAX(kontrakter.andel) AS andel,\n"
                            .    "DATE_SUB(oppsigelser.fristillelsesdato, INTERVAL 1 DAY) AS tildato,\n"
                            .    "oppsigelser.fristillelsesdato,\n"
                            .    "oppsigelser.oppsigelsestid_slutt\n",
            'groupfields'    => "kontrakter.leieforhold,\n"
                            .    "oppsigelser.fristillelsesdato,\n"
                            .    "oppsigelser.oppsigelsestid_slutt",
            'orderfields'    => "MIN(fradato) ASC\n",
            'where'            => "kontrakter.leieobjekt = '{$this->id}'\n"
                            .    "AND kontrakter.leieforhold != '" . (int)$seBortFraLeieforhold . "'\n"
                            .    "AND kontrakter.kontraktnr = kontrakter.leieforhold\n"
                            .    ($til ? "AND kontrakter.fradato <= '$til'\n" : "")
                            .    "AND (oppsigelser.oppsigelsestid_slutt > '$fra' OR oppsigelser.oppsigelsestid_slutt IS NULL)\n"
        ));

        // Loop gjennom resultatet første gang
        //    for å opprette Leieforhold-objektene
        //    og for å fastsette datoer hvor det kan være endringer i utleiegraden
        $datoer = array();
        settype( $datoer[$fra], 'string' );

        foreach( $aktuelle->data as $lf) {

            // Putt leieforholdet i beholderne proFormaLeieforhold og evt faktiskeLeieforhold
            $leieforhold = new Leieforhold( $lf->leieforhold );
            $resultat->proFormaLeieforhold[] = $leieforhold;

            if ($lf->tildato >= $fra or !$lf->tildato ) {
                // Leieobjektet ble ikke oppsagt før det angitte tidsrommet
                $resultat->faktiskeLeieforhold[] = $leieforhold;
            }

            // Legg kontrolldatoene i $datoer
            //    Leieforholdets fradato skal taes med dersom det er innenfor det angitte tidsrommet
            $lf->fradato = max($lf->fradato, $fra);
            settype( $datoer[$lf->fradato], 'string' );

            //    Leieforholdets fristillelsesdato skal taes med dersom det er innenfor det angitte tidsrommet
            if($til and $lf->tildato) {
                $lf->tildato = min($lf->tildato, $til);
                if($lf->fristillelsesdato <= $til) {
                    settype( $datoer[$lf->fristillelsesdato], 'string' );
                }
            }
            else {
                $lf->tildato = $til;
            }

        }

        // Loop gjennom resultatet andre gang
        //    for å beregne utleiegrad per dato.
        //    Den høyeste verdien returneres som utleiegrad
        foreach( $datoer as $dato => &$andel) {
            foreach( $aktuelle->data as $lf) {
                $utleie = $this->fraBrøk( $lf->andel );

                if( $lf->fradato <= $dato and (!$lf->tildato or $lf->tildato >= $dato) ) {
                    $andel = bcadd( $andel, $utleie, 12);
                }
            }
        }

        $resultat->grad = round(intval(max($datoer)),10);
        $resultat->ledig = max(0, round(bcsub("1", max($datoer), 12), 10));

        return $resultat;
    }



    /**
     * Oppretter et nytt leieobjekt i databasen og tildeler egenskapene til dette objektet
     *
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Leieobjekt::opprett()
     * @param array|stdClass $egenskaper Alle egenskapene det nye objektet skal initieres med
     * @return void
     * @throws Exception
     */
    public function opprett($egenskaper = array()) {
        throw new Exception('Bruk av utdatert metode ' . __METHOD__);
    }
}