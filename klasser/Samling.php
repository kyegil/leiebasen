<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 02/09/2020
 * Time: 11:50
 */

namespace Kyegil\Leiebasen;


use Closure;
use Kyegil\CoreModel\CoreModelCollection;
use Kyegil\CoreModel\Interfaces\CoreModelCollectionInterface;
use stdClass;

/**
 * Interface Samling
 * @package Kyegil\Leiebasen
 */
interface Samling
{
    /**
     * @param int $start
     * @param int|null $lengde
     * @return Samling
     */
    public function beskjær(int $start, int $lengde = null): Samling;

    /**
     * @param Samling $collection
     * @return Samling
     */
    public function blandMed(CoreModelCollection $collection): Samling;

    /**
     * @return Modell|null
     */
    public function hentFørste();

    /**
     * @return Modell|null
     */
    public function hentSiste();

    /**
     * @return Modell|null
     */
    public function hentTilfeldig();

    /**
     * @return array
     */
    public function hentIdNumre(): array;

    /**
     * Legg til et nytt filter til de eksisterende
     * Samlingen må lastes på nytt etter at et filter er lagt til.
     *
     * Filteret kan enten angis i formatet ['id' => null] eller som en streng 'id IS NULL'
     *
     * Venstre side kan avsluttes med en av de følgende sammenkliknings-uttrykkene
     * '=', '>', '<', '<>', '<=', '>=', 'IS', 'NOT', 'LIKE' eller 'IN'
     *
     * @param string|string[]|array<string,mixed> $filter
     * @param bool $ikkeForm Sann for å forhindre videre forming av filteret
     * @return Samling
     */
    public function leggTilFilter($filter, bool $ikkeForm = false): Samling;

    /**
     * @param string|array|stdClass $tabellnavn
     * @param string $betingelse
     * @return Samling
     */
    public function leggTilInnerJoin($tabellnavn, string $betingelse): Samling;

    /**
     * @param string|array|stdClass $tabellnavn
     * @param string $betingelse
     * @return Samling
     */
    public function leggTilLeftJoin($tabellnavn, string $betingelse): Samling;

    /**
     * @param string $alias Tabellnavn/alias
     * @return Samling
     */
    public function fjernJoin(string $alias): Samling;
    /**
     * Legg til ny sortering til MySql-spørringen
     * Den siste sorteringen som er lagt til har alltid høyest prioritet
     *
     * @param string $felt
     * @param bool $synkende
     * @param string|null $tabell
     * @return Samling
     */
    public function leggTilSortering(string $felt, bool $synkende = false, string $tabell = null): Samling;

    /**
     * @return Samling
     */
    public function låsFiltre(): Samling;

    /**
     * @return array
     */
    public function hentFiltre(): array;
    /**
     * @return Samling
     */
    public function snu(): Samling;

    /**
     * @param Closure $sorteringsfunksjon
     * @return Samling
     */
    public function sorterLastede(Closure $sorteringsfunksjon): Samling;

    /**
     * @return array
     */
    public function hentElementer(): array;

    /**
     * @return int
     */
    public function hentAntall(): int;

    /**
     * @param string $egenskap
     * @param mixed $verdi
     * @return Samling
     */
    public function sett($egenskap, $verdi): Samling;

    /**
     * @param string $tabell
     * @param string $felt
     * @param string|null $aggregatFunksjon
     * @param string|null $egenskapsnavn
     * @param string|null $navneområde
     * @return Samling
     */
    public function leggTilFelt(
        string $tabell,
        string $felt,
        string $aggregatFunksjon = null,
        string $egenskapsnavn = null,
        string $navneområde = null
    );

    /**
     * @return Samling
     */
    public function fjernTilleggsfelter();

    /**
     * @return stdClass[]
     */
    public function hentTilleggsfelter();

    /**
     * @param string $modell
     * @param string|null $navneområde
     * @param string|null $tabellKilde
     * @param array|null $felter
     * @return Samling
     */
    public function leggTilModell(string $modell, ?string $navneområde = null, ?string $tabellKilde = null, ?array $felter = null);

    /**
     * @param int $start
     * @return Samling
     */
    public function settStart(int $start): Samling;

    /**
     * @param int|null $begrensning
     * @return Samling
     */
    public function begrens($begrensning): Samling;

    /**
     * @return Samling
     */
    public function nullstill(): Samling;

    /**
     * Legg til et spesialtilpasset uttrykk til spørringen
     *
     * Eksempler på hvordan de ulike verdiene skrives ut:
     * * uttrykk as `$navneområde.egenskapsnavn`
     * * aggregat(uttrykk) as `$navneområde.egenskapsnavn`
     *
     * @param string $navneområde
     * @param string $egenskapsnavn
     * @param string $uttrykk
     * @param string|null $aggregatFunksjon
     * @return $this
     */
    public function leggTilUttrykk(
        string $navneområde,
        string $egenskapsnavn,
        string $uttrykk,
        string $aggregatFunksjon = null
    ):Samling;

    /**
     * @param Modell[] $data
     * @return $this
     */
    public function lastFra(array $data): Samling;

    /**
     * @param stdClass $verdier
     * @return $this
     */
    public function settVerdier(stdClass $verdier): Samling;

    /**
     * @param int[]|string[] $idNumre
     * @return $this
     */
    public function filtrerEtterIdNumre(array $idNumre): Samling;

    /**
     * @return bool
     */
    public function harLastet(): bool;

    /**
     * @return $this
     */
    public function slettAlle(): Samling;

    /**
     * @return $this
     */
    public function slettHver(): Samling;

    /**
     * Inkluder et annet sett basert på én-til-mange-forholdet
     *
     * @param string $referanse
     * @param Sett|null $samling
     * @param string|null $modell
     * @param string|null $fremmedNøkkel
     * @param array $filtre
     * @param callable|null $callback
     * @return Samling|null
     * @throws \Exception
     */
    public function inkluder(
        string    $referanse, ?Sett $samling = null,
        ?string   $modell = null,
        ?string   $fremmedNøkkel = null,
        array     $filtre = [],
        ?callable $callback = null
    ): ?CoreModelCollectionInterface;
}