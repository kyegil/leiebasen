<?php
    /**
     * * Part of kyegil/leiebasen
     * Created by Kyegil
     * Date: 12/06/2020
     * Time: 09:45
     */

    namespace Kyegil\Leiebasen;


    use Exception;
    use Kyegil\CoreModel\Interfaces\CoreModelInterface;

    class ModellFabrikk extends \Kyegil\CoreModel\CoreModelFactory
    {
        /**
         * @param \class-string<CoreModelInterface> $modell
         * @param $di
         * @param int|string|null $id
         * @return \Kyegil\CoreModel\Interfaces\CoreModelInterface
         * @throws Exception
         */
        public static function lag($modell, $di, $id = null)
        {
            if(!is_a($modell, CoreModelInterface::class, true)) {
                throw new Exception($modell . ' er ikke av typen Kyegil\\CoreModel\\CoreModelInterface.');
            }
            return parent::create($modell, $di, $id);
        }
    }