<?php

namespace Kyegil\Leiebasen;

use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontraktsett;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegningsett;

/**
 *
 */
class DatabaseInnhold extends DatabaseInstallerer
{
    /**
     * @return array
     */
    protected function hentVersjonSkript(): array
    {
        $tp = $this->hentTablePrefix();
        $skript = [];

        /**
         * Versjon 0.0.0
         *
         * Installeringsskript for basis installering
         * Denne må kjøres for alle versjoner, og har derfor fått versjon 0.0.0
         * Versjon '0' regnes som 'ikke installert'
         * Versjon '0.0.0' finnes ikke, men befinner seg semantisk mellom '0' og alle reelle versjoner.
         */
        $skript['0.0.0'] = [
            [
                '+i' => 'Oppretter tabellen innstillinger',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}valg` (`innstilling` varchar(64) COLLATE utf8_danish_ci NOT NULL, `verdi` text COLLATE utf8_danish_ci NOT NULL, PRIMARY KEY (`innstilling`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci",
                ],
            ],
            [
                '+i' => 'Oppretter versjon-nummer',
                '+' => [
                    "INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES ('versjon','0')",
                ],
            ],
            [
                '+i' => 'Fyller innstillinger med standardverdier',
                '+' => [
                    <<< EOQ
INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`)
VALUES
	('_temp_email_content',''),
	('adresse',''),
	('auto_OCR','0'),
	('auto_OCR_stempel','0'),
	('autoavsender',''),
	('avtalegiro','0'),
	('avtalegiro_sms','0'),
	('backup_siste_nedlastet','0'),
	('bankkonto',''),
	('beboerstøtte_epost',''),
	('beboerstøtte_skademeldinger_admin',''),
	('betalingsplan_bruddvarsel_ant_dager','32'),
	('betalingsplan_bruddvarsel_mal',''),
	('betalingsplan_etterlysning_ant_dager','25'),
	('betalingsplan_etterlysning_mal',''),
	('betalingsplan_påminnelse_mal',''),
	('cronsuksess','0'),
	('delkravtype_fellesstrøm',''),
	('efaktura','0'),
	('efaktura_referansenummer',''),
	('efaktura_tekst1',''),
	('efaktura_tekst2',''),
	('epost',''),
	('epost_faktura_aktivert','0'),
	('epost_kreditnota_aktivert','0'),
	('epost_purringer_aktivert','0'),
	('epost_svaradresser',''),
	('eposttekst',''),
	('forfallsdato','1'),
	('forfallsfrist','P14D'),
	('forfallsvarsel_innen','P2D'),
	('girotekst',''),
	('hjemmeside',''),
	('konvolutt_marg_topp','25'),
	('konvolutt_marg_venstre','30'),
	('leiereguleringsbrevmal',''),
	('mobil',''),
	('nets_avtaleID_fbo',''),
	('nets_avtaleID_ocr',''),
	('nets_kundeenhetID',''),
	('nets_siste_forsendelsesnr',''),
	('nets_siste_oppdragsnr',''),
	('ocr','0'),
	('OCR_feilmelding',''),
	('orgnr',''),
	('postnr',''),
	('poststed',''),
	('purreforfallsfrist','P14D'),
	('purregebyr','0'),
	('purreintervall','P14D'),
	('siste_fbo_trekkrav',''),
	('solidaritetsfondbeløp','0'),
	('sperredato_for_etterregistrering_av_krav','4'),
	('strømfordelingstekst',''),
	('telefax',''),
	('telefon',''),
	('utdelingsrute','1'),
	('utleier',''),
	('utløpsvarseltekst',''),
	('utskriftsforsøk',''),
	('varselstempel_forfall','0'),
	('varselstempel_innbetalinger','0'),
	('varselstempel_kontraktutløp','0'),
	('varselstempel_krav','0')
EOQ
                ],
            ],
            [
                '+i' => 'Oppretter tabellen adganger',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}adganger` (`adgangsid` int(11) NOT NULL AUTO_INCREMENT,`personid` int(11) unsigned DEFAULT NULL,`adgang` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`leieforhold` int(11) unsigned DEFAULT NULL,`epostvarsling` tinyint(1) NOT NULL DEFAULT '1',`innbetalingsbekreftelse` tinyint(4) DEFAULT '1' COMMENT 'Bekreftelse på epost for registrerte innbetalinger',`forfallsvarsel` tinyint(4) DEFAULT '1' COMMENT 'Påminnelse om forfall selv om en i utgangspunktet er ajour',PRIMARY KEY (`adgangsid`),UNIQUE KEY `adgang` (`personid`,`adgang`,`leieforhold`),KEY `kontraktnr` (`leieforhold`),KEY `epostvarsling` (`epostvarsling`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen avtalemaler',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}avtalemaler` (`malnr` tinyint(4) NOT NULL AUTO_INCREMENT,`type` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT 'leieavtale',`malnavn` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`mal` text COLLATE utf8_danish_ci NOT NULL,`er_framleiemal` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Er dette en framleiemal',PRIMARY KEY (`malnr`),KEY `framleiemal` (`er_framleiemal`),KEY `type` (`type`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen beboerstøtte',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}beboerstøtte` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`saksref` varchar(255) COLLATE utf8_danish_ci DEFAULT '',`tittel` varchar(255) COLLATE utf8_danish_ci DEFAULT '',`skademelding_id` int(11) DEFAULT NULL,`status` varchar(255) COLLATE utf8_danish_ci DEFAULT NULL,`område` varchar(255) COLLATE utf8_danish_ci DEFAULT NULL,`privat_person_id` int(11) DEFAULT NULL,PRIMARY KEY (`id`),KEY `saksref` (`saksref`),KEY `skademelding_id` (`skademelding_id`),KEY `status` (`status`),KEY `område` (`område`),KEY `privat_person_id` (`privat_person_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen beboerstøtte_abonnent',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}beboerstøtte_abonnent` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`sak_id` int(11) NOT NULL,`bruker_id` int(11) NOT NULL,PRIMARY KEY (`id`),KEY `sak_id` (`sak_id`),KEY `bruker_id` (`bruker_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen beboerstøtte_innlegg',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}beboerstøtte_innlegg` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`sak_id` int(11) DEFAULT NULL,`avsender_id` int(11) DEFAULT NULL,`innhold` text COLLATE utf8_danish_ci,`tidspunkt` timestamp NULL DEFAULT CURRENT_TIMESTAMP,`vedlegg` varchar(1024) COLLATE utf8_danish_ci DEFAULT NULL,`saksopplysning` tinyint(4) NOT NULL,`privat` tinyint(4) NOT NULL,PRIMARY KEY (`id`),KEY `sak_id` (`sak_id`),KEY `tidspunkt` (`tidspunkt`),KEY `saksopplysning` (`saksopplysning`),KEY `privat` (`privat`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen betalingsplan_avdrag',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}betalingsplan_avdrag` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`betalingsplan_id` int(11) NOT NULL,`beløp` decimal(8,2) DEFAULT NULL,`utestående` decimal(8,2) DEFAULT NULL,`forfallsdato` date DEFAULT NULL,`giro_id` int(11) unsigned DEFAULT NULL,`påminnelse_sendt` datetime DEFAULT NULL,`etterlysning_sendt` datetime DEFAULT NULL,PRIMARY KEY (`id`),KEY `betalingsplan_id` (`betalingsplan_id`),KEY `forfallsdato` (`forfallsdato`),KEY `utestående` (`utestående`),KEY `giro_id` (`giro_id`),KEY `påminnelse_sendt` (`påminnelse_sendt`),KEY `etterlysning_sendt` (`etterlysning_sendt`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen betalingsplan_originalkrav',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}betalingsplan_originalkrav` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`krav_id` int(11) NOT NULL,`betalingsplan_id` int(11) NOT NULL,PRIMARY KEY (`id`),KEY `krav_id` (`krav_id`),KEY `betalingsplan_id` (`betalingsplan_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen betalingsplaner',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}betalingsplaner` (`id` int(11) unsigned NOT NULL,`leieforhold_id` int(11) unsigned NOT NULL,`aktiv` tinyint(1) NOT NULL DEFAULT '0',`dato` date DEFAULT NULL,`startgjeld` decimal(8,2) unsigned NOT NULL,`betalingsavtale` text NOT NULL,`avdragsbeløp` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',`terminlengde` varchar(255) NOT NULL DEFAULT 'P1M',`start_dato` date DEFAULT NULL,`forfallspåminnelse_ant_dager` int(2) DEFAULT NULL,PRIMARY KEY (`id`),KEY `leieforhold_id` (`leieforhold_id`),KEY `aktiv` (`aktiv`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen bygninger',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}bygninger` (`id` smallint(6) NOT NULL AUTO_INCREMENT,`kode` varchar(100) COLLATE utf8_danish_ci DEFAULT NULL,`navn` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`bilde` text COLLATE utf8_danish_ci NOT NULL,PRIMARY KEY (`id`),UNIQUE KEY `navn` (`navn`),UNIQUE KEY `kode` (`kode`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen delkrav',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}delkrav` (`id` int(11) NOT NULL AUTO_INCREMENT,`kravid` int(11) NOT NULL,`delkravtype` int(11) NOT NULL,`beløp` decimal(10,2) NOT NULL DEFAULT '0.00',PRIMARY KEY (`id`),KEY `kravid` (`kravid`),KEY `delkravtype` (`delkravtype`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen delkravtyper',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}delkravtyper` (`id` int(11) NOT NULL AUTO_INCREMENT,`orden` int(11) NOT NULL,`navn` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Navn på delkravet',`kode` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Kode eller kortnavn for delkravtypen',`beskrivelse` text COLLATE utf8_danish_ci NOT NULL COMMENT 'Utfyllende beskrivelse av delkravtypen',`kravtype` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Hvilken kravtype delkravene brukes i, f.eks. Husleie eller Fellesstrøm',`selvstendig_tillegg` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Et selvstendig tillegg opprettes som et eget krav ved siden av husleia, istedetfor som en del av den',`valgfritt` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Dersom delkravet er frivillig vil beløpet kunne redigeres ved opprettelse av leieavtalen',`relativ` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Satsen er ikke oppgitt i kroner, men som en relativ fraksjon som legges til hoveddelen av kravet',`sats` decimal(10,3) NOT NULL DEFAULT '0.000' COMMENT 'Satsen oppgis som kronebeløp (per år dersom relevant), eller som fraksjon dersom relativ er sann.',`aktiv` tinyint(1) NOT NULL DEFAULT '1',PRIMARY KEY (`id`),KEY `navn` (`navn`),KEY `kode` (`kode`),KEY `aktiv` (`aktiv`),KEY `orden` (`orden`),KEY `selvstendig_tillegg` (`selvstendig_tillegg`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen eav_egenskaper',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}eav_egenskaper` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`modell` varchar(255) DEFAULT NULL,`kode` varchar(255) NOT NULL DEFAULT '',`type` varchar(255) NOT NULL DEFAULT '',`konfigurering` text,`beskrivelse` varchar(255) DEFAULT '',`rekkefølge` int(11) DEFAULT NULL,PRIMARY KEY (`id`),KEY `modell` (`modell`(191)),KEY `kode` (`kode`(191)),KEY `rekkefølge` (`type`(191))) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4",
                ],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen oppsigelsestid for framleie',
                '+' => [
                    <<< EOQ
INSERT INTO `{$tp}eav_egenskaper` (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
SELECT
    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Framleie','oppsigelsestid','interval',NULL,'',NULL
FROM dual
    WHERE NOT EXISTS
    (
        SELECT *
        FROM `{$tp}eav_egenskaper`
        WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Framleie'
        AND `kode` = 'oppsigelsestid'
    )
EOQ
                ],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen signert_fil for kontrakter',
                '+' => [
                    <<< EOQ
INSERT INTO `{$tp}eav_egenskaper` (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
SELECT
    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Kontrakt','signert_fil','string',NULL,'',NULL
FROM dual
    WHERE NOT EXISTS
    (
        SELECT *
        FROM `{$tp}eav_egenskaper`
        WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Kontrakt'
        AND `kode` = 'signert_fil'
    )
EOQ
                ],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen avtalegiro_trekk for betalingsplaner',
                '+' => [
                    <<< EOQ
INSERT INTO `{$tp}eav_egenskaper` (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
SELECT
    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Betalingsplan\\\\Avdrag','avtalegiro_trekk','Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Fbo\\\\Trekk','{\"allowNull\":true}','',NULL
FROM dual
    WHERE NOT EXISTS
    (
        SELECT *
        FROM `{$tp}eav_egenskaper`
        WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Betalingsplan\\\\Avdrag'
        AND `kode` = 'avtalegiro_trekk'
    )
EOQ
                ],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen notat_id for betalingsplaner',
                '+' => [
                    <<< EOQ
INSERT INTO `{$tp}eav_egenskaper` (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
SELECT
    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Betalingsplan','notat_id','Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Notat','{\"target\":\"notat\"}','',NULL
FROM dual
    WHERE NOT EXISTS
    (
        SELECT *
        FROM `{$tp}eav_egenskaper`
        WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Betalingsplan'
        AND `kode` = 'notat_id'
    )
EOQ
                ],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen utenom_avtalegiro for betalingsplaner',
                '+' => [
                    <<< EOQ
INSERT INTO `{$tp}eav_egenskaper` (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
SELECT
    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Betalingsplan','utenom_avtalegiro','boolean',NULL,'',NULL
FROM dual
    WHERE NOT EXISTS
    (
        SELECT *
        FROM `{$tp}eav_egenskaper`
        WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Betalingsplan'
        AND `kode` = 'utenom_avtalegiro'
    )
EOQ
                ],
            ],
            [
                '+i' => 'Oppretter tabellen eav_verdier',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}eav_verdier` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`egenskap_id` int(11) unsigned DEFAULT NULL,`objekt_id` int(11) unsigned DEFAULT NULL,`verdi` text,PRIMARY KEY (`id`),UNIQUE KEY `IDX_COMBO` (`egenskap_id`,`objekt_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen efaktura_avtaler',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}efaktura_avtaler` (`id` int(11) NOT NULL AUTO_INCREMENT,`leieforhold` int(11) DEFAULT NULL,`efakturareferanse` varchar(80) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`avtalestatus` varchar(1) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'P=Pending, A=Active, C=Changed, N=NoActive(avvist)',`avtalevalg` varchar(1) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Siste forespørsel fra NETS: A=Add, C=Change, D=Delete',`nets_brukerid` varchar(80) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`fornavn` varchar(80) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`etternavn` varchar(80) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`adresse1` varchar(80) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`adresse2` varchar(80) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`postnr` varchar(10) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`poststed` varchar(80) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`land` varchar(80) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`telefon` varchar(80) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`email` varchar(80) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`feilkode` varchar(2) COLLATE utf8_danish_ci DEFAULT NULL,`registrert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,PRIMARY KEY (`id`),UNIQUE KEY `efakturareferanse` (`efakturareferanse`),UNIQUE KEY `leieforhold` (`leieforhold`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen efakturaer',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}efakturaer` (`id` int(11) NOT NULL AUTO_INCREMENT,`giro` int(11) NOT NULL,`forsendelsesdato` date DEFAULT NULL,`forsendelse` varchar(7) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`oppdrag` int(11) NOT NULL,`transaksjon` int(11) DEFAULT NULL,`kvittert_dato` date DEFAULT NULL,`kvitteringsforsendelse` varchar(7) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`status` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '',PRIMARY KEY (`id`),KEY `giro` (`giro`,`forsendelsesdato`,`forsendelse`,`oppdrag`,`transaksjon`,`kvittert_dato`,`kvitteringsforsendelse`,`status`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen beboerstøtte_innlegg',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}eksportoppsett` (`id` int(11) NOT NULL AUTO_INCREMENT,`navn` varchar(500) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`oppsett` text COLLATE utf8_danish_ci NOT NULL,PRIMARY KEY (`id`),KEY `navn` (`navn`(255))) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen beboerstøtte_innlegg',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}epostlager` (`id` int(11) NOT NULL AUTO_INCREMENT,`prioritet` int(11) NOT NULL DEFAULT '0',`til` varchar(200) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`emne` varchar(70) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`innhold` longblob NOT NULL,`hode` text COLLATE utf8_danish_ci NOT NULL,`param` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT '',PRIMARY KEY (`id`),KEY `prioritet` (`prioritet`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci COMMENT='Mellomlager for eposter på vei ut'"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen beboerstøtte_innlegg',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}fbo` (`id` int(11) NOT NULL AUTO_INCREMENT,`leieforhold` int(11) NOT NULL,`type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = ignoreres, 1 = Husleie, 2 = Fellesstrøm',`varsel` tinyint(1) NOT NULL DEFAULT '1',`registrert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`slettet` tinyint(1) NOT NULL DEFAULT '0',`mobilnr` varchar(20) NOT NULL DEFAULT '' COMMENT 'Dette feltet kommer ikke fra NETS, men må oppdateres manuelt i leiebasen',PRIMARY KEY (`id`),UNIQUE KEY `type` (`leieforhold`,`type`) COMMENT 'unik type per leieforhold',KEY `varsel` (`varsel`),KEY `registrert` (`registrert`),KEY `slettet` (`slettet`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Faste betalingsoppdrag (avtalegiro)'"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen beboerstøtte_innlegg',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}fbo_trekkrav` (`id` int(11) NOT NULL AUTO_INCREMENT,`leieforhold` int(11) NOT NULL,`gironr` int(11) DEFAULT NULL,`kid` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`overføringsdato` date DEFAULT NULL,`forsendelse` varchar(7) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`oppdrag` varchar(7) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`beløp` decimal(10,2) NOT NULL DEFAULT '0.00',`forfallsdato` date DEFAULT NULL,`varslet` datetime DEFAULT NULL,`egenvarsel` tinyint(1) NOT NULL DEFAULT '1',`mobilnr` varchar(20) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`mottakskvittering` text COLLATE utf8_danish_ci NOT NULL,`prosessert_kvittering` text COLLATE utf8_danish_ci NOT NULL,`til_sletting` datetime DEFAULT NULL,`sletteoppdrag_sendt` datetime DEFAULT NULL,PRIMARY KEY (`id`),UNIQUE KEY `gironr` (`gironr`),KEY `leieforhold` (`leieforhold`),KEY `overføringsdato` (`overføringsdato`),KEY `beløp` (`beløp`),KEY `forfallsdato` (`forfallsdato`),KEY `varslet` (`varslet`),KEY `egenvarsel` (`egenvarsel`),KEY `mobilnr` (`mobilnr`),KEY `forsendelse` (`forsendelse`,`oppdrag`),KEY `kid` (`kid`),KEY `til_sletting` (`til_sletting`),KEY `sletteoppdrag_sendt` (`sletteoppdrag_sendt`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen beboerstøtte_innlegg',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}framleie` (`nr` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id-nr for framleieforholdet',`leieforhold` int(11) NOT NULL COMMENT 'Leieforholdet som framleies',`fradato` date DEFAULT NULL,`tildato` date DEFAULT NULL,`avtalemal` text COLLATE utf8_danish_ci NOT NULL,`avtale` text COLLATE utf8_danish_ci NOT NULL COMMENT 'Framleie-avtaletekst',PRIMARY KEY (`nr`),KEY `kontraktnr` (`leieforhold`,`fradato`,`tildato`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen framleiepersoner',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}framleiepersoner` (`nr` int(11) unsigned NOT NULL AUTO_INCREMENT,`framleieforhold` int(11) unsigned NOT NULL COMMENT 'Id-nr for framleieforholdet',`personid` int(11) unsigned NOT NULL,PRIMARY KEY (`nr`),KEY `kontraktnr` (`framleieforhold`,`personid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen fs_andeler',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}fs_andeler` (`id` int(11) NOT NULL AUTO_INCREMENT,`faktura_id` int(11) DEFAULT NULL,`anleggsnr` bigint(11) DEFAULT NULL,`fom` date DEFAULT NULL,`tom` date DEFAULT NULL,`faktura` varchar(127) COLLATE utf8_danish_ci DEFAULT NULL,`leieforhold` int(10) unsigned DEFAULT NULL,`kontraktnr` int(11) unsigned DEFAULT NULL,`andel` float DEFAULT NULL,`beløp` float DEFAULT NULL,`krav` int(11) DEFAULT NULL,`tekst` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`epostvarsel` datetime DEFAULT NULL,PRIMARY KEY (`id`),KEY `tekst` (`tekst`),KEY `epostvarsel` (`epostvarsel`),KEY `anleggsnr` (`anleggsnr`),KEY `fom` (`fom`),KEY `tom` (`tom`),KEY `faktura` (`faktura`),KEY `kontraktnr` (`kontraktnr`),KEY `andel` (`andel`),KEY `krav` (`krav`),KEY `faktura_id` (`faktura_id`),KEY `leieforhold` (`leieforhold`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen fs_fellesstrømanlegg',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}fs_fellesstrømanlegg` (`anleggsnummer` varchar(127) COLLATE utf8_danish_ci NOT NULL,`målernummer` varchar(127) COLLATE utf8_danish_ci DEFAULT NULL,`plassering` varchar(150) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`formål` varchar(150) COLLATE utf8_danish_ci NOT NULL DEFAULT '',PRIMARY KEY (`anleggsnummer`),KEY `målernummer` (`målernummer`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen fs_fordelingsnøkler',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}fs_fordelingsnøkler` (`nøkkel` int(11) unsigned NOT NULL AUTO_INCREMENT,`anleggsnummer` bigint(20) NOT NULL,`fordelingsmåte` varchar(25) COLLATE utf8_danish_ci NOT NULL DEFAULT 'Andeler',`følger_leieobjekt` tinyint(4) NOT NULL,`leieobjekt` smallint(6) DEFAULT NULL,`leieforhold` int(11) unsigned DEFAULT NULL,`andeler` float NOT NULL,`prosentsats` float NOT NULL,`fastbeløp` float NOT NULL,`forklaring` text COLLATE utf8_danish_ci NOT NULL,PRIMARY KEY (`nøkkel`),KEY `anleggsnummer` (`anleggsnummer`,`fordelingsmåte`),KEY `fordelingsmåte` (`fordelingsmåte`),KEY `følger_leieobjekt` (`følger_leieobjekt`),KEY `leieobjekt` (`leieobjekt`),KEY `leieforhold` (`leieforhold`),KEY `andeler` (`andeler`),KEY `prosentsats` (`prosentsats`),KEY `fastbeløp` (`fastbeløp`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen fs_originalfakturaer',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}fs_originalfakturaer` (`id` int(11) NOT NULL AUTO_INCREMENT,`fakturanummer` varchar(127) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`fakturabeløp` decimal(10,2) DEFAULT NULL,`anleggsnr` bigint(20) DEFAULT NULL,`fradato` date DEFAULT NULL,`tildato` date DEFAULT NULL,`termin` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`kWh` float DEFAULT NULL COMMENT 'Antall kiloWattimer',`beregnet` tinyint(4) NOT NULL DEFAULT '0',`fordelt` tinyint(1) NOT NULL DEFAULT '0',`lagt_inn_av` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT '',PRIMARY KEY (`id`),KEY `fakturabeløp` (`fakturabeløp`),KEY `anleggsnr` (`anleggsnr`),KEY `fradato` (`fradato`),KEY `tildato` (`tildato`),KEY `termin` (`termin`),KEY `kWh` (`kWh`),KEY `fordelt` (`fordelt`),KEY `fakturanummer` (`fakturanummer`),KEY `beregnet` (`beregnet`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen giroer',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}giroer` (`gironr` int(11) NOT NULL AUTO_INCREMENT,`sammensatt` timestamp NULL DEFAULT CURRENT_TIMESTAMP,`utskriftsdato` datetime DEFAULT NULL COMMENT 'Tidspunkt for første utskrift av giro',`kid` varchar(30) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`leieforhold` int(11) unsigned NOT NULL,`format` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Giroformat: papir, eFaktura, epost e.l.',PRIMARY KEY (`gironr`),KEY `utskriftsdato` (`utskriftsdato`),KEY `leieforhold` (`leieforhold`),KEY `kid` (`kid`),KEY `format` (`format`),KEY `sammensatt` (`sammensatt`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci ROW_FORMAT=DYNAMIC"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen innbetalinger',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}innbetalinger` (`innbetalingsid` int(11) NOT NULL AUTO_INCREMENT,`innbetaling` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Id-streng som samler alle delbetalinger til en innbetaling',`krav` int(11) DEFAULT NULL,`leieforhold` smallint(6) DEFAULT NULL COMMENT 'Leieforholdet dersom krav ikke er angitt',`beløp` decimal(8,2) NOT NULL DEFAULT '0.00',`konto` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`OCRtransaksjon` int(11) DEFAULT NULL,`ref` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`dato` date DEFAULT NULL,`merknad` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`betaler` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`registrerer` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`registrert` timestamp NULL DEFAULT CURRENT_TIMESTAMP,PRIMARY KEY (`innbetalingsid`),KEY `leieforhold` (`leieforhold`),KEY `beløp` (`beløp`),KEY `krav` (`krav`),KEY `betalingsmåte` (`konto`),KEY `dato` (`dato`),KEY `betaler` (`betaler`),KEY `betaler_2` (`betaler`),KEY `OCR-transaksjon` (`OCRtransaksjon`),KEY `innbetaling` (`innbetaling`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen innbetalt',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}innbetalt` (`krav` int(11) NOT NULL,`sum` decimal(8,2) NOT NULL DEFAULT '0.00',PRIMARY KEY (`krav`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen internmeldinger',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}internmeldinger` (`id` int(11) NOT NULL AUTO_INCREMENT,`tekst` text COLLATE utf8_danish_ci NOT NULL,`avsender` int(11) DEFAULT NULL,`tidspunkt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`drift` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Meldingen skal vises i drifts meldingspanel',`flyko` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Meldingen skal vises i flykos meldingspanel',PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen kontoer',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}kontoer` (`id` int(11) NOT NULL AUTO_INCREMENT,`kode` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`navn` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '',PRIMARY KEY (`id`),UNIQUE KEY `kode` (`kode`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci",
                ],
            ],
            [
                '+i' => 'Oppretter kontoer',
                '+' => [
                    <<< EOQ
INSERT IGNORE INTO `{$tp}kontoer` (`kode`, `navn`)
VALUES
       ('kontant', 'Kontant'),
       ('giro', 'Giro'),
       ('ocr', 'OCR-giro'),
       ('annet', 'Annet'),
       ('kreditt', 'Kreditt')
EOQ
                    ,
                    "UPDATE `{$tp}kontoer` SET `id` = 0 WHERE `kode` = 'kreditt'"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen kontrakter',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}kontrakter` (`kontraktnr` int(11) unsigned NOT NULL AUTO_INCREMENT,`leieobjekt` int(11) unsigned DEFAULT NULL,`andel` varchar(16) COLLATE utf8_danish_ci NOT NULL DEFAULT '1',`fradato` date DEFAULT NULL,`tildato` date DEFAULT NULL,`oppsigelsestid` varchar(8) COLLATE utf8_danish_ci NOT NULL DEFAULT 'P3M',`årlig_basisleie` decimal(8,2) NOT NULL DEFAULT '0.00',`leiebeløp` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',`solfondbeløp` decimal(8,2) NOT NULL DEFAULT '0.00',`er_fornyelse` tinyint(2) unsigned NOT NULL DEFAULT '0',`leieforhold` int(11) unsigned DEFAULT NULL,`ant_terminer` tinyint(3) unsigned NOT NULL DEFAULT '12',`tekst` text COLLATE utf8_danish_ci COMMENT 'Kontraktteksten',`frosset` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Om avtalen er frosset eller ikke',`stopp_oppfølging` tinyint(1) NOT NULL DEFAULT '0',`avvent_oppfølging` date DEFAULT NULL,`regningsperson` smallint(5) unsigned DEFAULT NULL,`regning_til_objekt` tinyint(2) unsigned NOT NULL DEFAULT '0',`regningsobjekt` smallint(5) unsigned DEFAULT NULL,`regningsadresse1` varchar(64) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`regningsadresse2` varchar(64) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`postnr` varchar(16) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`poststed` varchar(32) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`land` varchar(32) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`giroformat` varchar(32) COLLATE utf8_danish_ci DEFAULT 'papir',PRIMARY KEY (`kontraktnr`),KEY `leieforhold` (`leieforhold`),KEY `leieobjekt` (`leieobjekt`),KEY `andel` (`andel`),KEY `fradato` (`fradato`),KEY `tildato` (`tildato`),KEY `leiebeløp` (`leiebeløp`),KEY `ant_terminer` (`ant_terminer`),KEY `oppsigelsestid` (`oppsigelsestid`),KEY `regningsperson` (`regningsperson`),KEY `frosset` (`frosset`),KEY `regning_til_objekt` (`regning_til_objekt`),KEY `regningsobjekt` (`regningsobjekt`),KEY `stopp_oppfølging` (`stopp_oppfølging`),KEY `avvent_oppfølging` (`avvent_oppfølging`),KEY `giroformat` (`giroformat`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen kontraktpersoner',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}kontraktpersoner` (`kopling` int(11) unsigned NOT NULL AUTO_INCREMENT,`person` int(11) unsigned DEFAULT NULL,`leieforhold` int(10) unsigned DEFAULT NULL,`kontrakt` int(11) unsigned DEFAULT NULL,`slettet` date DEFAULT NULL,`leietaker` varchar(64) COLLATE utf8_danish_ci NOT NULL DEFAULT '',PRIMARY KEY (`kopling`),KEY `slettet` (`slettet`),KEY `person` (`person`),KEY `kontrakt` (`kontrakt`),KEY `leieforhold` (`leieforhold`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen krav',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}krav` (`kontraktnr` int(11) unsigned DEFAULT NULL,`leieforhold` int(10) unsigned DEFAULT NULL,`gironr` int(11) unsigned DEFAULT NULL,`id` int(11) NOT NULL AUTO_INCREMENT,`kravdato` date DEFAULT NULL COMMENT 'Datoen kravet aktiveres',`tekst` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`beløp` decimal(8,2) NOT NULL DEFAULT '0.00',`solidaritetsfondbeløp` decimal(8,2) DEFAULT NULL COMMENT 'Hvor mye av kravbeløpet som kreves inn på vegne av solidaritetsfondet',`type` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`leieobjekt` smallint(6) DEFAULT NULL,`andel` varchar(16) COLLATE utf8_danish_ci DEFAULT NULL,`termin` varchar(50) COLLATE utf8_danish_ci DEFAULT NULL,`fom` date DEFAULT NULL COMMENT 'startdato for en termin',`tom` date DEFAULT NULL COMMENT 'sluttdato for en termin',`anleggsnr` bigint(20) DEFAULT NULL,`opprettet` datetime DEFAULT NULL COMMENT 'Tidspunktet kravet er opprettet',`oppretter` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`utskriftsdato` datetime DEFAULT NULL COMMENT 'Tidspunkt for første utskrift av giro',`forfall` date DEFAULT NULL COMMENT 'Forfallsdato',`utestående` decimal(8,2) NOT NULL DEFAULT '0.00',PRIMARY KEY (`id`),KEY `kontraktnr` (`kontraktnr`),KEY `type` (`type`),KEY `leieobjekt` (`leieobjekt`),KEY `andel` (`andel`),KEY `utestående` (`utestående`),KEY `beløp` (`beløp`),KEY `fom` (`fom`),KEY `tom` (`tom`),KEY `termin` (`termin`),KEY `kravdato` (`kravdato`),KEY `anleggsnr` (`anleggsnr`),KEY `utskriftsdato` (`utskriftsdato`),KEY `forfall` (`forfall`),KEY `gironr` (`gironr`),KEY `leieforhold` (`leieforhold`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen leieberegning',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}leieberegning` (`nr` smallint(6) NOT NULL AUTO_INCREMENT,`navn` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`beskrivelse` tinytext COLLATE utf8_danish_ci NOT NULL,`leie_objekt` float NOT NULL DEFAULT '0',`leie_kontrakt` float NOT NULL DEFAULT '0',`leie_kvm` float NOT NULL DEFAULT '0',`leie_var_bad` float NOT NULL DEFAULT '0',`leie_var_fellesdo` float NOT NULL DEFAULT '0',`leie_var_egendo` float NOT NULL DEFAULT '0',`solfond` float NOT NULL DEFAULT '0',PRIMARY KEY (`nr`),UNIQUE KEY `navn` (`navn`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen leieforhold',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}leieforhold` (`leieforholdnr` int(10) unsigned NOT NULL,`leieobjekt` int(10) unsigned DEFAULT NULL,`andel` varchar(16) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL DEFAULT '1',`fradato` date DEFAULT NULL,`tildato` date DEFAULT NULL,`oppsigelsestid` varchar(8) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL DEFAULT 'P3M',`årlig_basisleie` decimal(8,2) NOT NULL DEFAULT '0.00',`leiebeløp` decimal(8,2) unsigned NOT NULL DEFAULT '0.00',`ant_terminer` tinyint(3) unsigned NOT NULL DEFAULT '12',`avtaletekstmal` text CHARACTER SET utf8 COLLATE utf8_danish_ci,`siste_kontrakt` int(10) unsigned DEFAULT NULL,`frosset` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Om avtalen er frosset eller ikke',`stopp_oppfølging` tinyint(1) NOT NULL DEFAULT '0',`regning_til_objekt` tinyint(2) unsigned NOT NULL DEFAULT '0',`avvent_oppfølging` date DEFAULT NULL,`regningsperson` smallint(5) unsigned DEFAULT NULL,`regningsobjekt` smallint(5) unsigned DEFAULT NULL,`regningsadresse1` varchar(64) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL DEFAULT '',`regningsadresse2` varchar(64) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL DEFAULT '',`postnr` varchar(16) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL DEFAULT '',`poststed` varchar(32) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL DEFAULT '',`land` varchar(32) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL DEFAULT '',`giroformat` varchar(32) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT 'papir',PRIMARY KEY (`leieforholdnr`),UNIQUE KEY `siste_kontrakt` (`siste_kontrakt`),KEY `leieobjekt` (`leieobjekt`),KEY `andel` (`andel`),KEY `fradato` (`fradato`),KEY `tildato` (`tildato`),KEY `leiebeløp` (`leiebeløp`),KEY `ant_terminer` (`ant_terminer`),KEY `oppsigelsestid` (`oppsigelsestid`),KEY `regningsperson` (`regningsperson`),KEY `frosset` (`frosset`),KEY `regning_til_objekt` (`regning_til_objekt`),KEY `regningsobjekt` (`regningsobjekt`),KEY `stopp_oppfølging` (`stopp_oppfølging`),KEY `avvent_oppfølging` (`avvent_oppfølging`),KEY `giroformat` (`giroformat`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen leieforhold_delkrav',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}leieforhold_delkrav` (`id` int(11) NOT NULL AUTO_INCREMENT,`leieforhold` int(11) DEFAULT NULL,`delkravtype` int(11) DEFAULT NULL,`selvstendig_tillegg` tinyint(1) NOT NULL DEFAULT '0',`relativ` tinyint(1) NOT NULL DEFAULT '0',`sats` decimal(10,3) NOT NULL DEFAULT '0.000',`periodisk_avstemming` tinyint(1) NOT NULL DEFAULT '0',PRIMARY KEY (`id`),UNIQUE KEY `delkravtype` (`delkravtype`,`leieforhold`),KEY `relativ` (`relativ`),KEY `selvstendig_tillegg` (`selvstendig_tillegg`),KEY `periodisk_avstemming` (`periodisk_avstemming`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen leieobjekter',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}leieobjekter` (`leieobjektnr` smallint(5) unsigned NOT NULL AUTO_INCREMENT,`bygning` smallint(6) DEFAULT NULL,`navn` varchar(32) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`gateadresse` varchar(64) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`postnr` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`poststed` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`etg` varchar(16) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`beskrivelse` varchar(64) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`bilde` text COLLATE utf8_danish_ci NOT NULL,`areal` float unsigned DEFAULT NULL,`ant_rom` varchar(50) COLLATE utf8_danish_ci DEFAULT NULL,`bad` varchar(8) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`toalett` varchar(40) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`boenhet` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Er leieobjektet en boenhet eller et lokale for annen bruk?',`leieberegning` tinyint(128) unsigned NOT NULL DEFAULT '1',`merknader` text COLLATE utf8_danish_ci NOT NULL,`toalett_kategori` tinyint(1) NOT NULL DEFAULT '0',`ikke_for_utleie` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Angir at leieobjektet ikke skal leies ut',PRIMARY KEY (`leieobjektnr`),KEY `gateadresse` (`gateadresse`,`areal`,`bad`,`toalett`),FULLTEXT KEY `merknader` (`merknader`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen logg',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}logg` (`id` int(11) NOT NULL AUTO_INCREMENT,`personid` int(11) DEFAULT NULL,`tid` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`type` int(11) DEFAULT NULL,`tekst` mediumtext COLLATE utf8_danish_ci NOT NULL,PRIMARY KEY (`id`),KEY `personid` (`personid`,`tid`,`type`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen notater',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}notater` (`notatnr` int(11) NOT NULL AUTO_INCREMENT,`leieforhold` int(11) DEFAULT NULL COMMENT 'Leieforholdet notatet gjelder',`dato` date DEFAULT NULL,`notat` text COLLATE utf8_danish_ci NOT NULL COMMENT 'Selve notatet',`henvendelse_fra` varchar(20) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'I tilfelle notatet gjelder en henvendelse fra utleier eller leietaker',`kategori` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT 'notat' COMMENT 'Hva slags notat er dette',`brevtekst` text COLLATE utf8_danish_ci NOT NULL COMMENT 'Om dette er et brev/henvendelse går teksten in her',`vedlegg` varchar(1000) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`vedleggsnavn` varchar(1000) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`dokumentreferanse` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Referanse til tilhørende dokument',`dokumenttype` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Hva slags dokument som refereres',`registrert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Tidspunktet notatet ble registrert',`registrerer` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Den som opprettet notatet',`skjul_for_leietaker` tinyint(3) unsigned NOT NULL,PRIMARY KEY (`notatnr`),KEY `leieforhold` (`leieforhold`),KEY `dato` (`dato`),KEY `kategori` (`kategori`),KEY `dokumentreferanse` (`dokumentreferanse`),KEY `dokumenttype` (`dokumenttype`),KEY `skjul_for_leietaker` (`skjul_for_leietaker`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen ocr_filer',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}ocr_filer` (`filID` int(11) NOT NULL AUTO_INCREMENT,`forsendelsesnummer` int(11) DEFAULT NULL,`oppgjørsdato` date DEFAULT NULL,`OCR` text COLLATE utf8_danish_ci NOT NULL,`registrert` datetime DEFAULT NULL,`registrerer` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT '',PRIMARY KEY (`filID`),KEY `forsendelsesnummer` (`forsendelsesnummer`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen OCRdetaljer',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}OCRdetaljer` (`id` int(11) NOT NULL AUTO_INCREMENT,`filID` int(11) DEFAULT NULL,`forsendelsesnummer` int(11) DEFAULT NULL,`oppdragsnummer` int(11) DEFAULT NULL,`oppdragskonto` varchar(20) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`avtaleid` varchar(20) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`transaksjonstype` int(11) DEFAULT NULL,`transaksjonsnummer` int(11) DEFAULT NULL,`oppgjørsdato` date DEFAULT NULL,`bankdatasentral` varchar(20) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`delavregningsnummer` int(11) DEFAULT NULL,`løpenummer` int(11) DEFAULT NULL,`beløp` decimal(18,2) DEFAULT NULL,`kid` varchar(30) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`blankettnummer` varchar(20) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`arkivreferanse` varchar(20) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`oppdragsdato` date DEFAULT NULL,`debetkonto` varchar(20) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`fritekst` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT '',PRIMARY KEY (`id`),KEY `filID` (`filID`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen område_deltakelse',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}område_deltakelse` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`område_id` int(11) unsigned NOT NULL,`medlem_type` varchar(255) NOT NULL DEFAULT '',`medlem_id` int(11) unsigned NOT NULL,PRIMARY KEY (`id`),KEY `område_id` (`område_id`),KEY `medlem_type` (`medlem_type`(191)),KEY `medlem_id` (`medlem_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen områder',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}områder` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`navn` varchar(255) NOT NULL DEFAULT '',`beskrivelse` text NOT NULL,PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen oppsigelser',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}oppsigelser` (`kontraktnr` int(11) unsigned NOT NULL,`leieforhold` int(11) unsigned NOT NULL,`oppsigelsesdato` date DEFAULT NULL,`fristillelsesdato` date DEFAULT NULL,`oppsigelsestid_slutt` date DEFAULT NULL,`ref` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`merknad` text COLLATE utf8_danish_ci NOT NULL,`oppsagt_av_utleier` tinyint(4) NOT NULL DEFAULT '0',PRIMARY KEY (`leieforhold`),KEY `oppsigelsesdato` (`oppsigelsesdato`),KEY `fristillelsesdato` (`fristillelsesdato`),KEY `oppsigelsestid_slutt` (`oppsigelsestid_slutt`),KEY `ref` (`ref`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen personegenskaper',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}personegenskaper` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`kode` varchar(255) NOT NULL DEFAULT '',`type` varchar(255) DEFAULT NULL,`konfigurering` text,`beskrivelse` varchar(255) DEFAULT NULL,`rekkefølge` int(11) DEFAULT NULL,PRIMARY KEY (`id`),KEY `kode` (`kode`(191))) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4",
                ],
            ],
            [
                '+i' => 'Oppretter brukerprofil_preferanser',
                '+' => [
                    <<< EOQ
INSERT INTO `{$tp}personegenskaper` (`kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
SELECT
    'brukerprofil_preferanser','json',NULL,'Preferanser Brukerprofil',10
FROM dual
    WHERE NOT EXISTS
    (
        SELECT *
        FROM `{$tp}personegenskaper`
        WHERE `kode` = 'brukerprofil_preferanser'
    )
EOQ
                ],
            ],
            [
                '+i' => 'Oppretter tabellen personegenskaper_verdier',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}personegenskaper_verdier` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`egenskap_id` int(11) NOT NULL,`person_id` int(11) NOT NULL,`verdi` text,PRIMARY KEY (`id`),KEY `egenskap_id` (`egenskap_id`),KEY `person_id` (`person_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen personer',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}personer` (`personid` int(11) unsigned NOT NULL AUTO_INCREMENT,`fornavn` varchar(64) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'for- og mellomnavn',`etternavn` varchar(64) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'etter- eller foretaksnavn',`er_org` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT 'Er dette firma / organisasjon',`fødselsdato` date DEFAULT NULL,`personnr` varchar(16) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'person- eller org.nr',`adresse1` varchar(64) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'adresselinje 1',`adresse2` varchar(64) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'adresselinje 2',`postnr` varchar(16) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Postnummer',`poststed` varchar(32) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Poststed',`land` varchar(64) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Land',`telefon` varchar(32) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Telefonnummer',`mobil` varchar(32) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Mobiltelefonnummer',`epost` varchar(64) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'E-postadresse',PRIMARY KEY (`personid`),KEY `fornavn` (`fornavn`,`etternavn`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci COMMENT='Kunde / personregister'"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen poletter',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}poletter` (`polett` varchar(200) COLLATE utf8_danish_ci NOT NULL,`utløper` int(11) NOT NULL DEFAULT '0',PRIMARY KEY (`polett`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen purringer',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}purringer` (`nr` int(11) NOT NULL AUTO_INCREMENT,`blankett` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`krav` int(11) DEFAULT NULL,`purredato` date DEFAULT NULL,`purremåte` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT 'giro',`purrer` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`purregebyr` int(11) DEFAULT '0' COMMENT 'Evt. purregebyrkrav for denne purringen',`purreforfall` date DEFAULT NULL,PRIMARY KEY (`nr`),KEY `krav` (`krav`),KEY `purredato` (`purredato`),KEY `purregebyr` (`purregebyr`),KEY `purreforfall` (`purreforfall`),KEY `blankett` (`blankett`),KEY `purremåte` (`purremåte`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen regnskapsprosjekter',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}regnskapsprosjekter` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`kode` varchar(127) DEFAULT '',`navn` varchar(255) NOT NULL DEFAULT '',`beskrivelse` text NOT NULL,`konfigurering` text,PRIMARY KEY (`id`),UNIQUE KEY `kode` (`kode`),KEY `navn` (`navn`(191))) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen returi_log',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}returi_log` (`id` int(11) NOT NULL AUTO_INCREMENT,`time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`session` varchar(256) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`uri` varchar(2000) COLLATE utf8_danish_ci NOT NULL DEFAULT '',PRIMARY KEY (`id`),KEY `session` (`session`(255)),KEY `time` (`time`),FULLTEXT KEY `uri` (`uri`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen skadekategorier',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}skadekategorier` (`id` int(11) NOT NULL AUTO_INCREMENT,`skadeid` int(11) DEFAULT NULL,`kategori` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '',PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen skader',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}skader` (`id` int(11) NOT NULL AUTO_INCREMENT,`leieobjektnr` smallint(6) DEFAULT NULL,`bygning` smallint(6) DEFAULT NULL,`registrerer_id` int(11) DEFAULT NULL,`registrerer` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`registrert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`skade` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`beskrivelse` text COLLATE utf8_danish_ci NOT NULL,`utført` date DEFAULT NULL,`sluttregistrerer_id` int(11) DEFAULT NULL,`sluttregistrerer` varchar(50) COLLATE utf8_danish_ci NOT NULL DEFAULT '',`sluttrapport` text COLLATE utf8_danish_ci,`beboerstøtte_id` int(11) unsigned DEFAULT NULL,PRIMARY KEY (`id`),KEY `leieobjektnr` (`leieobjektnr`),KEY `utført` (`utført`),KEY `beboerstøtte_id` (`beboerstøtte_id`),KEY `bygning` (`bygning`),KEY `registrerer_id` (`registrerer_id`),KEY `sluttregistrerer_id` (`sluttregistrerer_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen søknad_adganger',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}søknad_adganger` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`søknad_id` int(11) unsigned NOT NULL,`login` varchar(255) NOT NULL DEFAULT '',`epost` varchar(255) NOT NULL,`referanse` varchar(255) NOT NULL DEFAULT '',`pw` varchar(255) DEFAULT '',`nødpassord` varchar(255) DEFAULT NULL,PRIMARY KEY (`id`),KEY `søknad_id` (`søknad_id`),KEY `login` (`login`(191)),KEY `epost` (`epost`(191)),KEY `referanse` (`referanse`(191))) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen søknader',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}søknader` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`søknadstype` int(11) unsigned DEFAULT NULL,`søknadstekst` mediumtext NOT NULL,`registrert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`oppdatert` timestamp NULL DEFAULT NULL,`meta_data` text,PRIMARY KEY (`id`),KEY `søknadstype` (`søknadstype`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen søknadstyper',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}søknadstyper` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`kode` varchar(127) DEFAULT NULL,`navn` text NOT NULL,`beskrivelse` text NOT NULL,`konfigurering` text,PRIMARY KEY (`id`),UNIQUE KEY `kode` (`kode`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen utdelingsorden',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}utdelingsorden` (`rute` tinyint(4) NOT NULL DEFAULT '1',`leieobjekt` int(11) DEFAULT NULL,`plassering` int(11) NOT NULL DEFAULT '0',KEY `leieojekt` (`leieobjekt`),KEY `plassering` (`plassering`),KEY `rute` (`rute`),KEY `leieobjekt` (`leieobjekt`),KEY `plassering_2` (`plassering`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci"
                ],
            ],
        ];

        /**
         * Versjon 0.1.7
         *
         * Lagt til tabell for søknadskategorier
         */
        $skript['0.1.7'] = [
            [
                '+i' => 'Oppretter tabellen søknad_kategorier',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}søknad_kategorier` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`søknad_id` int(11) unsigned NOT NULL,`kategori` varchar(255) NULL NULL DEFAULT NULL,PRIMARY KEY (`id`),KEY `søknad_id` (`søknad_id`),KEY `kategori` (`kategori`(191))) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen søknad_notater',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}søknad_notater` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`søknad_id` int(11) unsigned NOT NULL,`person_id` int(11) unsigned NOT NULL COMMENT 'Brukeren som har registert notatet',`tidspunkt` timestamp DEFAULT CURRENT_TIMESTAMP,`systemgenerert` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT 'Systemgenererte notater kan ikke slettes av brukere',`tekst` text COLLATE utf8_danish_ci NOT NULL,PRIMARY KEY (`id`),KEY `søknad_id` (`søknad_id`),KEY `person_id` (`person_id`),KEY `tidspunkt` (`tidspunkt`),KEY `systemgenerert` (`systemgenerert`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
        ];

        /**
         * Versjon 0.1.17
         *
         * Fjerner varselstempel_krav ifra valg-tabellen
         */
        $skript['0.1.17'] = [
            [
                '+i' => 'Fjerner unødvendig innstilling varselstempel_krav',
                '+' => [
                    "DELETE FROM `{$tp}valg` WHERE `innstilling` = 'varselstempel_krav'"
                ],
                '-i' => 'Gjenoppretter innstilling for varselstempel_krav',
                '-' => [
                    "INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES ('varselstempel_krav', UNIX_TIMESTAMP())",
                ],
            ],
        ];

        /**
         * Versjon 0.2.0
         *
         * Lagt til brukere
         */
        $skript['0.2.0'] = [
            [
                '+i' => 'Oppretter tabell for brukerprofiler',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}brukerprofiler` (`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, `login` varchar(191) NOT NULL DEFAULT '', `passord` varchar(255) NOT NULL DEFAULT '', `person_id` int(11) DEFAULT NULL, PRIMARY KEY (`id`), UNIQUE KEY `login` (`login`), KEY `person_id` (`person_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
            [
                '+i' => 'Oppretter tabell for engangsbilletter',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `engangsbilletter` (`id` int(11) NOT NULL AUTO_INCREMENT, `brukerprofil_id` int(11) NOT NULL, `billett` varchar(191) COLLATE utf8mb4_danish_ci NOT NULL, `url` text COLLATE utf8mb4_danish_ci NOT NULL, `utløper` datetime NOT NULL, PRIMARY KEY (`id`), UNIQUE KEY `billett` (`billett`), KEY `brukerprofil_id` (`brukerprofil_id`), KEY `utløper` (`utløper`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
        ];

        /**
         * Versjon 0.3.0
         *
         * Versjon 0.3.0 omdøper fellesstrøm til tjenestefordeling,
         * og lenker tillegg til hovedkrav
         */
        $skript['0.3.0'] = [

            /**
             * Legg lenke mellom tillegg og hovedkrav
             */
            [
                '+i' => 'Legger til feltet `hovedkrav_id` for tillegg i kravtabellen',
                '+' => ["ALTER TABLE `{$tp}krav` ADD `hovedkrav_id` INT NULL AFTER `utestående`, ADD INDEX (`hovedkrav_id`)"],
                '-i' => 'Sletter felt for `hovedkrav_id` i kravtabellen',
                '-' => ["ALTER TABLE `{$tp}krav` DROP COLUMN `hovedkrav_id`"],
            ],

            /**
             * Legg til ny primærnøkkelfelt `id` for tjenestene
             */
            [
                '+i' => 'Endrer primærnøkkel i tabell for fellesstrømanlegg fra `anleggsnummer` til `id`',
                '+' => [
                    "ALTER TABLE `{$tp}fs_fellesstrømanlegg` DROP PRIMARY KEY",
                    "ALTER TABLE `{$tp}fs_fellesstrømanlegg` ADD `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT FIRST",
                ],
                '-i' => 'Endrer primærnøkkel i tabell for fellesstrømanlegg fra `id` til `anleggsnummer`',
                '-' => [
                    "ALTER TABLE `{$tp}fs_fellesstrømanlegg` CHANGE `id` `id` INT UNSIGNED NOT NULL",
                    "ALTER TABLE `{$tp}fs_fellesstrømanlegg` DROP COLUMN `id`",
                    "ALTER TABLE `{$tp}fs_fellesstrømanlegg` ADD PRIMARY KEY (`anleggsnummer`)",
                ],
            ],
            [
                '+i' => 'Legger til felt for `tjeneste_id` i tabell for andeler',
                '+' => ["ALTER TABLE `{$tp}fs_andeler` ADD `tjeneste_id` INT NULL AFTER `id`, ADD INDEX (`tjeneste_id`)"],
                '-i' => 'Sletter felt for `tjeneste_id` i tabell for andeler',
                '-' => ["ALTER TABLE `{$tp}fs_andeler` DROP COLUMN `tjeneste_id`"],
            ],
            [
                '+i' => 'Øker størrelsen på tekst-feltet',
                '+' => ["ALTER TABLE `{$tp}fs_andeler` MODIFY COLUMN `tekst` VARCHAR(255)"],
                '-i' => 'Reduserer størrelsen på tekst-feltet',
                '-' => ["ALTER TABLE `{$tp}fs_andeler` MODIFY COLUMN `tekst` VARCHAR(100)"],
            ],
            [
                '+i' => 'Fyller `tjeneste_id` i andelene med verdier',
                '+' => ["UPDATE `{$tp}fs_andeler` INNER JOIN `{$tp}fs_fellesstrømanlegg` ON `{$tp}fs_andeler`.`anleggsnr` = `{$tp}fs_fellesstrømanlegg`.`anleggsnummer` SET `{$tp}fs_andeler`.`tjeneste_id` = `{$tp}fs_fellesstrømanlegg`.`id`"],
            ],
            [
                '+i' => 'Legger til felt for `tjeneste_id` i tabell for fordelingsnøkler',
                '+' => ["ALTER TABLE `{$tp}fs_fordelingsnøkler` ADD `tjeneste_id` INT NULL AFTER `nøkkel`, ADD INDEX (`tjeneste_id`)"],
                '-i' => 'Sletter felt for `tjeneste_id` i tabell for fordelingsnøkler',
                '-' => ["ALTER TABLE `{$tp}fs_fordelingsnøkler` DROP COLUMN `tjeneste_id`"],
            ],
            [
                '+i' => 'Fyller `tjeneste_id` i fordelingsnøklene med verdier',
                '+' => ["UPDATE `{$tp}fs_fordelingsnøkler` INNER JOIN `{$tp}fs_fellesstrømanlegg` ON `{$tp}fs_fordelingsnøkler`.`anleggsnummer` = `{$tp}fs_fellesstrømanlegg`.`anleggsnummer` SET `{$tp}fs_fordelingsnøkler`.`tjeneste_id` = `{$tp}fs_fellesstrømanlegg`.`id`"],
            ],
            [
                '+i' => 'Legger til felt for `tjeneste_id` i tabell for strømregninger',
                '+' => ["ALTER TABLE `{$tp}fs_originalfakturaer` ADD `tjeneste_id` INT NULL AFTER `id`, ADD INDEX (`tjeneste_id`)"],
                '-i' => 'Sletter felt for `tjeneste_id` i tabell for regninger',
                '-' => ["ALTER TABLE `{$tp}fs_originalfakturaer` DROP COLUMN `tjeneste_id`"],
            ],
            [
                '+i' => 'Fyller `tjeneste_id` i strømregningene med verdier',
                '+' => ["UPDATE `{$tp}fs_originalfakturaer` INNER JOIN `{$tp}fs_fellesstrømanlegg` ON `{$tp}fs_originalfakturaer`.`anleggsnr` = `{$tp}fs_fellesstrømanlegg`.`anleggsnummer` SET `{$tp}fs_originalfakturaer`.`tjeneste_id` = `{$tp}fs_fellesstrømanlegg`.`id`"],
            ],

            /**
             * Legg til nye felter i tjenestene
             */
            [
                '+i' => 'Legger til felt for `type` i tabell for fellesstrømanlegg',
                '+' => ["ALTER TABLE `{$tp}fs_fellesstrømanlegg` ADD `type` VARCHAR(255) NULL DEFAULT NULL AFTER `id`, ADD INDEX (`type`)"],
                '-i' => 'Sletter felt for `type` i tabell for fellesstrømanlegg',
                '-' => ["ALTER TABLE `{$tp}fs_fellesstrømanlegg` DROP COLUMN `type`"],
            ],
            [
                '+i' => 'Fyller `type` i fellesstrømanleggene med verdier',
                '+' => ["UPDATE `{$tp}fs_fellesstrømanlegg` SET `type` = 'Fellesstrøm' WHERE 1"],
                '-i' => 'Sletter alle tjenester som ikke er av typen `Fellesstrøm` i fra fellesstrømanleggene',
                '-' => ["DELETE FROM `{$tp}fs_fellesstrømanlegg` WHERE `type` <> 'Fellesstrøm'"],
            ],
            [
                '+i' => 'Endrer `anleggsnummer` til `avtalereferanse` i tabellen for fellesstrømanlegg',
                '+' => ["ALTER TABLE `{$tp}fs_fellesstrømanlegg` CHANGE `anleggsnummer` `avtalereferanse` VARCHAR(255) NULL DEFAULT NULL, ADD INDEX(`avtalereferanse`)"],
                '-i' => 'Endrer `avtalereferanse` til `anleggsnummer` i tabellen for fellesstrømanlegg',
                '-' => ["ALTER TABLE `{$tp}fs_fellesstrømanlegg` CHANGE `avtalereferanse` `anleggsnummer` VARCHAR(255) NULL DEFAULT NULL, ADD INDEX(`anleggsnummer`)"],
            ],

            [
                '+i' => 'Legger til felt for `navn` i tabell for fellesstrømanlegg',
                '+' => ["ALTER TABLE `{$tp}fs_fellesstrømanlegg` ADD `navn` VARCHAR(255) NULL DEFAULT NULL AFTER `avtalereferanse`"],
                '-i' => 'Sletter felt for `navn` i tabell for fellesstrømanlegg',
                '-' => ["ALTER TABLE `{$tp}fs_fellesstrømanlegg` DROP COLUMN `navn`"],
            ],
            [
                '+i' => 'Fyller `navn` i fellesstrømanleggene med verdier fra fellesstrøm bruksområde',
                '+' => ["UPDATE `{$tp}fs_fellesstrømanlegg` SET `navn` = CONCAT('Strøm: ', `formål`) WHERE 1"],
            ],

            [
                '+i' => 'Legger til felt for `leverandør` i tabell for fellesstrømanlegg',
                '+' => ["ALTER TABLE `{$tp}fs_fellesstrømanlegg` ADD `leverandør` VARCHAR(255) NULL DEFAULT NULL AFTER `navn`"],
                '-i' => 'Sletter felt for `leverandør` i tabell for fellesstrømanlegg',
                '-' => ["ALTER TABLE `{$tp}fs_fellesstrømanlegg` DROP COLUMN `leverandør`"],
            ],

            [
                '+i' => 'Endrer `formål` til `beskrivelse` i tabellen for fellesstrømanlegg',
                '+' => ["ALTER TABLE `{$tp}fs_fellesstrømanlegg` CHANGE `formål` `beskrivelse` TEXT NOT NULL DEFAULT '' AFTER `leverandør`"],
                '-i' => 'Endrer `beskrivelse` til `formål` i tabellen for fellesstrømanlegg',
                '-' => ["ALTER TABLE `{$tp}fs_fellesstrømanlegg` CHANGE `beskrivelse` `formål` VARCHAR(255) NOT NULL DEFAULT '' AFTER `plassering`"],
            ],

            [
                '+i' => 'Legger til felt for `registrer_forbruk` i tabell for fellesstrømanlegg',
                '+' => ["ALTER TABLE `{$tp}fs_fellesstrømanlegg` ADD `registrer_forbruk` tinyint(2) unsigned NOT NULL DEFAULT '0' AFTER `beskrivelse`, ADD INDEX (`registrer_forbruk`)"],
                '-i' => 'Sletter felt for `registrer_forbruk` i tabell for fellesstrømanlegg',
                '-' => ["ALTER TABLE `{$tp}fs_fellesstrømanlegg` DROP COLUMN `registrer_forbruk`"],
            ],

            [
                '+i' => 'Legger til felt for `forbruksenhet` i tabell for fellesstrømanlegg',
                '+' => ["ALTER TABLE `{$tp}fs_fellesstrømanlegg` ADD `forbruksenhet` VARCHAR(255) NULL DEFAULT NULL AFTER `registrer_forbruk`, ADD INDEX (`forbruksenhet`)"],
                '-i' => 'Sletter felt for `forbruksenhet` i tabell for fellesstrømanlegg',
                '-' => ["ALTER TABLE `{$tp}fs_fellesstrømanlegg` DROP COLUMN `forbruksenhet`"],
            ],
            [
                '+i' => 'Setter alle strømanleggene til å registrere forbruk, med `kWh` som forbruksenhet',
                '+' => ["UPDATE `{$tp}fs_fellesstrømanlegg` SET `registrer_forbruk` = 1, `forbruksenhet` = 'kWh' WHERE 1"],
            ],

            /**
             * Oppdater feltene i fs_originalfakturaer
             */
            [
                '+i' => 'Endrer `kWh` til `forbruk` i tabellen for originalfakturaer',
                '+' => [
                    "ALTER TABLE `{$tp}fs_originalfakturaer` DROP INDEX `kWh`",
                    "ALTER TABLE `{$tp}fs_originalfakturaer` CHANGE `kWh` `forbruk` DECIMAL(10,2) NULL DEFAULT NULL COMMENT 'Forbruk oppgitt i relevant enhet', ADD INDEX (`forbruk`)",
                ],
                '-i' => 'Endrer `forbruk` til `kWh` i tabellen for originalfakturaer',
                '-' => [
                    "ALTER TABLE `{$tp}fs_originalfakturaer` DROP INDEX `forbruk`",
                    "ALTER TABLE `{$tp}fs_originalfakturaer` CHANGE `forbruk` `kWh` float DEFAULT NULL COMMENT 'Antall kiloWattimer', ADD INDEX (`kWh`)",
                ],
            ],
            [
                '+i' => 'Legger til felt for `forbruksenhet` i tabell for originalfakturaer',
                '+' => ["ALTER TABLE `{$tp}fs_originalfakturaer` ADD `forbruksenhet` VARCHAR(255) NULL DEFAULT NULL AFTER `forbruk`, ADD INDEX (`forbruksenhet`)"],
                '-i' => 'Sletter felt for `forbruksenhet` i tabell for originalfakturaer',
                '-' => ["ALTER TABLE `{$tp}fs_originalfakturaer` DROP COLUMN `forbruksenhet`"],
            ],
            [
                '+i' => 'Setter forbruksenheten i alle eksisterende originalfakturaer til `kWh`',
                '+' => ["UPDATE `{$tp}fs_originalfakturaer` SET `forbruksenhet` = 'kWh' WHERE 1"],
            ],

            /**
             * Endre navn på tabellene
             */
            [
                '+i' => 'Tabellen `fs_fellesstrømanlegg` endrer navn til `kostnadsdeling_tjenester`',
                '+' => ["RENAME TABLE `{$tp}fs_fellesstrømanlegg` TO `{$tp}kostnadsdeling_tjenester`"],
                '-i' => 'Tabellen `kostnadsdeling_tjenester` endrer navn til `fs_fellesstrømanlegg`',
                '-' => ["RENAME TABLE `{$tp}kostnadsdeling_tjenester` TO `{$tp}fs_fellesstrømanlegg`"],
            ],
            [
                '+i' => 'Tabellen `fs_fordelingsnøkler` endrer navn til `kostnadsdeling_fordelingsnøkler`',
                '+' => ["RENAME TABLE `{$tp}fs_fordelingsnøkler` TO `{$tp}kostnadsdeling_fordelingsnøkler`"],
                '-i' => 'Tabellen `kostnadsdeling_fordelingsnøkler` endrer navn til `fs_fordelingsnøkler`',
                '-' => ["RENAME TABLE `{$tp}kostnadsdeling_fordelingsnøkler` TO `{$tp}fs_fordelingsnøkler`"],
            ],
            [
                '+i' => 'Tabellen `fs_originalfakturaer` endrer navn til `kostnadsdeling_kostnader`',
                '+' => ["RENAME TABLE `{$tp}fs_originalfakturaer` TO `{$tp}kostnadsdeling_kostnader`"],
                '-i' => 'Tabellen `kostnadsdeling_kostnader` endrer navn til `fs_originalfakturaer`',
                '-' => ["RENAME TABLE `{$tp}kostnadsdeling_kostnader` TO `{$tp}fs_originalfakturaer`"],
            ],
            [
                '+i' => 'Tabellen `fs_andeler` endrer navn til `kostnadsdeling_andeler`',
                '+' => ["RENAME TABLE `{$tp}fs_andeler` TO `{$tp}kostnadsdeling_andeler`"],
                '-i' => 'Tabellen `kostnadsdeling_andeler` endrer navn til `fs_andeler`',
                '-' => ["RENAME TABLE `{$tp}kostnadsdeling_andeler` TO `{$tp}fs_andeler`"],
            ],

            /**
             * Fjern ubrukte felter
             */
            [
                '+i' => 'Sletter felt for `kontraktnr` i tabell for kostnadsdeling_andeler',
                '+' => ["ALTER TABLE `{$tp}kostnadsdeling_andeler` DROP COLUMN `kontraktnr`"],
                '-i' => 'Legger til felt for `kontraktnr` i tabell for kostnadsdeling_andeler',
                '-' => [
                    "ALTER TABLE `{$tp}kostnadsdeling_andeler` ADD `kontraktnr` int(11) unsigned DEFAULT NULL AFTER `leieforhold`, ADD INDEX (`kontraktnr`)",
                    "UPDATE `{$tp}kostnadsdeling_andeler` INNER JOIN `{$tp}leieforhold` ON `{$tp}kostnadsdeling_andeler`.`leieforhold` = `{$tp}leieforhold`.`leieforholdnr` SET `{$tp}kostnadsdeling_andeler`.`kontraktnr` = `{$tp}leieforhold`.`siste_kontrakt`",
                ],
            ],
        ];

        /**
         * Versjon 0.3.2
         *
         * Legger til oppdateringskolonne for innbetalinger og indeksering av EAV-verdier
         */
        $skript['0.3.2'] = [
            [
                '+i' => 'Indekserer registrert-tidspunkt for innbetalinger',
                '+' => ["ALTER TABLE `{$tp}innbetalinger` ADD INDEX (`registrert`)"],
                '-i' => 'Dropper registrert-indeks for innbetalinger',
                '-' => ["ALTER TABLE `{$tp}innbetalinger` DROP INDEX `registrert`"],
            ],
            [
                '+i' => 'Legger til oppdatert-felt for innbetalinger',
                '+' => ["ALTER TABLE `{$tp}innbetalinger` ADD `oppdatert` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `registrert`, ADD INDEX (`oppdatert`)"],
                '-i' => 'Fjerner oppdatert-felt for innbetalinger',
                '-' => ["ALTER TABLE `{$tp}innbetalinger` DROP COLUMN `oppdatert`"],
            ],
            [
                '+i' => 'Indekserer EAV-verdier',
                '+' => ["ALTER TABLE `{$tp}eav_verdier` ADD INDEX `verdi` (`verdi`(127))"],
                '-i' => 'Dropper EAV-verdier-indeks',
                '-' => ["ALTER TABLE `{$tp}eav_verdier` DROP INDEX `verdi`"],
            ],
            [
                '+i' => 'Indekserer personegenskaper-verdier',
                '+' => ["ALTER TABLE `{$tp}personegenskaper_verdier` ADD INDEX `verdi` (`verdi`(127))"],
                '-i' => 'Dropper personegenskaper-verdier-indeks',
                '-' => ["ALTER TABLE `{$tp}personegenskaper_verdier` DROP INDEX `verdi`"],
            ],
        ];

        /**
         * Versjon 0.3.3
         *
         * Legger till EAV-egenskap betalingsplan-forslag for leieforhold
         */
        $skript['0.3.3'] = [
            [
                '+i' => 'Oppretter eav_egenskapen betalingsplan_forslag for leieforhold',
                '+' => [
                    <<< EOQ
INSERT INTO `{$tp}eav_egenskaper` (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
SELECT
    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold','betalingsplan_forslag','json',NULL,'',NULL
FROM dual
    WHERE NOT EXISTS
    (
        SELECT *
        FROM `{$tp}eav_egenskaper`
        WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold'
        AND `kode` = 'betalingsplan_forslag'
    )
EOQ
                ],
            ],
        ];

        /**
         * Versjon 0.3.16
         *
         * Oppretter innstillingen epost_transport
         */
        $skript['0.3.16'] = [
            [
                '+i' => 'Oppretter innstillingen epost_transport',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('epost_transport','phpmailer')"],
            ],
        ];

        /**
         * Versjon 0.3.17
         *
         * Forbedringer av epost-lageret
         */
        $skript['0.3.17'] = [
            [
                '+i' => 'Øker størrelsen på epost-til-feltet',
                '+' => ["ALTER TABLE `{$tp}epostlager` CHANGE `til` `til` VARCHAR(2048) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL DEFAULT ''"],
                '-i' => 'Reduserer størrelsen på epost-til-feltet',
                '-' => ["ALTER TABLE `{$tp}epostlager` CHANGE `til` `til` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL DEFAULT ''"],
            ],
            [
                '+i' => 'Øker størrelsen på epost-emne-feltet',
                '+' => ["ALTER TABLE `{$tp}epostlager` CHANGE `emne` `emne` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL DEFAULT ''"],
                '-i' => 'Reduserer størrelsen på emne-til-feltet',
                '-' => ["ALTER TABLE `{$tp}epostlager` CHANGE `emne` `emne` VARCHAR(70) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL DEFAULT ''"],
            ],
            [
                '+i' => 'Endrer epost-headers-feltet',
                '+' => ["ALTER TABLE `{$tp}epostlager` CHANGE `hode` `headers` TEXT CHARACTER SET utf8 COLLATE utf8_danish_ci NULL DEFAULT ''"],
                '-i' => 'Tilbakestiller epost-headers-feltet',
                '-' => ["ALTER TABLE `{$tp}epostlager` CHANGE `headers` `hode` TEXT CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL DEFAULT ''"],
            ],
            [
                '+i' => 'Øker størrelsen på epost-params-feltet',
                '+' => ["ALTER TABLE `{$tp}epostlager` CHANGE `param` `params` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NULL DEFAULT 'NULL'"],
                '-i' => 'Reduserer størrelsen på epost-params-feltet',
                '-' => ["ALTER TABLE `{$tp}epostlager` CHANGE `params` `param` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL DEFAULT ''"],
            ],
        ];

        /**
         * Versjon 0.4.0
         *
         * Oppgradering til eFaktura 2.0 (Ja takk til alle)
         */
        $skript['0.4.0'] = [
            [
                '+i' => 'Indekserer nets_brukerid',
                '+' => ["ALTER TABLE `{$tp}efaktura_avtaler` ADD INDEX (`nets_brukerid`)"],
                '-i' => 'Fjerner person_id-felt fra efaktura_avtaler',
                '-' => ["ALTER TABLE `{$tp}efaktura_avtaler` DROP INDEX `nets_brukerid`"],
            ],
            [
                '+i' => 'Legger til person_id-felt for efaktura_avtaler',
                '+' => ["ALTER TABLE `{$tp}efaktura_avtaler` ADD `person_id` INT NULL DEFAULT NULL AFTER `id`, ADD UNIQUE INDEX (`person_id`)"],
                '-i' => 'Fjerner person_id-felt fra efaktura_avtaler',
                '-' => ["ALTER TABLE `{$tp}efaktura_avtaler` DROP COLUMN `person_id`"],
            ],
            [
                '+i' => 'Beregner verdier for person_id',
                '+' => [
                    "DROP TEMPORARY TABLE IF EXISTS `nets_brukerid_leieforhold`",
                    <<<EOQ
CREATE TEMPORARY TABLE `nets_brukerid_leieforhold`
SELECT ea.`nets_brukerid`, MAX(ea.`leieforhold`) AS leieforhold
FROM `{$tp}efaktura_avtaler` ea
GROUP BY ea.`nets_brukerid`
EOQ
                    ,
                    "DROP TEMPORARY TABLE IF EXISTS `nets_brukerid_person`",
                    <<<EOQ
CREATE TEMPORARY TABLE `nets_brukerid_person`
SELECT
`ea`.`nets_brukerid`,
MIN(`personid`) AS person_id
FROM `{$tp}efaktura_avtaler` ea
INNER JOIN `{$tp}kontraktpersoner` k ON `ea`.`leieforhold` = `k`.`leieforhold`
INNER JOIN `{$tp}personer` p ON `k`.`person` = `p`.`personid` AND (`ea`.`fornavn` = `p`.`fornavn` OR `ea`.`etternavn` = `p`.`etternavn`)
GROUP BY `ea`.`nets_brukerid`
EOQ
                    ,
                    <<<EOQ
UPDATE `{$tp}efaktura_avtaler` ea
INNER JOIN `nets_brukerid_leieforhold` nbl ON `ea`.`leieforhold` = `nbl`.`leieforhold`
INNER JOIN `nets_brukerid_person` nbp ON `nbl`.`nets_brukerid` = `nbp`.`nets_brukerid`
SET `ea`.`person_id` = `nbp`.`person_id` WHERE 1
EOQ
                    ,
                ],
            ],
            [
                '+i' => 'Oppretter eav-egenskapen efaktura1_referanse',
                '+' => [
                    <<< EOQ
INSERT INTO `{$tp}personegenskaper` (`kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
SELECT
    'efaktura1_referanse','string',NULL,'Efaktura v 1.0 efakturareferanse',20
FROM dual
    WHERE NOT EXISTS
    (
        SELECT *
        FROM `{$tp}personegenskaper`
        WHERE `kode` = 'efaktura1_referanse'
    )
EOQ
                ],
            ],
            [
                '+i' => 'Fyller efaktura1_referanse med verdier',
                '+' => [
                    <<< EOQ
                    INSERT INTO `{$tp}personegenskaper_verdier` (`egenskap_id`, `person_id`, `verdi`)
                    SELECT
                        (SELECT `id` FROM `{$tp}personegenskaper` WHERE `kode` = 'efaktura1_referanse') AS egenskap_id,
                        ea.`person_id`,
                        ea.`efakturareferanse`
                    FROM `{$tp}efaktura_avtaler` ea
                        LEFT JOIN `{$tp}personegenskaper_verdier` pv
                            ON ea.`person_id` = pv.`person_id` AND pv.`egenskap_id` = (SELECT `id` FROM `{$tp}personegenskaper` WHERE `kode` = 'efaktura1_referanse')
                    WHERE ea.`person_id` IS NOT NULL
                        AND pv.`person_id` IS NULL
                    EOQ
                ],
            ],
            [
                '+i' => 'Oppretter tabellen efaktura_identifiers',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}efaktura_identifiers` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `person_id` int(11) unsigned NULL DEFAULT NULL, `efaktura_identifier` varchar(127) DEFAULT NULL, `has_auto_accept_agreements` tinyint(1) unsigned NOT NULL DEFAULT '0', `blocked_by_receiver` tinyint(1) unsigned NULL DEFAULT NULL, `has_efaktura_agreement` tinyint(1) unsigned NULL DEFAULT NULL, `siste_response_result` varchar(127) DEFAULT NULL, `siste_identifier_key` text, `oppdatert` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY (`id`),UNIQUE KEY `person_id` (`person_id`),UNIQUE KEY `efaktura_identifier` (`efaktura_identifier`), KEY `has_auto_accept_agreements` (`has_auto_accept_agreements`), KEY `blocked_by_receiver` (`blocked_by_receiver`), KEY `has_efaktura_agreement` (`has_efaktura_agreement`), KEY `siste_response_result` (`siste_response_result`), KEY `oppdatert` (`oppdatert`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
        ];

        /**
         * Versjon 0.4.3
         *
         * Stikkord i søknader
         */
        $skript['0.4.3'] = [
            [
                '+i' => 'Legger til felt for `stikkord` i søknadstabellen',
                '+' => ["ALTER TABLE `{$tp}søknader` ADD `stikkord` text COLLATE utf8_danish_ci NOT NULL AFTER `søknadstekst`, ADD INDEX (`stikkord`(768))"],
                '-i' => 'Sletter felt for `stikkord` i søknadstabellen',
                '-' => ["ALTER TABLE `{$tp}søknader` DROP COLUMN `stikkord`"],
            ],
        ];

        $skript['0.4.5'] = [
            [
                '-i' => 'Gjenoppretter utskriftsdato-verdier',
                '-' => ["UPDATE `{$tp}krav` INNER JOIN `{$tp}giroer` ON `{$tp}krav`.`gironr` = `{$tp}giroer`.`gironr` SET `{$tp}krav`.`utskriftsdato` = `{$tp}giroer`.`utskriftsdato`"],
            ],
            [
                '+i' => 'Fjerner overflødig utskriftsdato fra krav-tabellen',
                '+' => ["ALTER TABLE `{$tp}krav` DROP COLUMN `utskriftsdato`"],
                '-i' => 'Gjenoppretter utskriftsdato krav-tabellen',
                '-' => ["ALTER TABLE `{$tp}krav` ADD `utskriftsdato` datetime DEFAULT NULL COMMENT 'Tidspunkt for første utskrift av giro' AFTER `oppretter`, ADD INDEX (`utskriftsdato`)"],
            ],
            [
                '-i' => 'Fjerner null-verdier',
                '-' => ["ALTER TABLE `{$tp}oppsigelser` CHANGE `kontraktnr` `kontraktnr` INT(11)  UNSIGNED  NOT NULL"],
            ],
            [
                '-i' => 'Gjenoppretter kontraktnr-verdier',
                '-' => ["UPDATE `{$tp}oppsigelser` INNER JOIN `{$tp}leieforhold` ON `{$tp}oppsigelser`.`leieforhold` = `{$tp}leieforhold`.`leieforholdnr` SET `{$tp}oppsigelser`.`kontraktnr` = `{$tp}leieforhold`.`siste_kontrakt`"],
            ],
            [
                '+i' => 'Fjerner overflødig kontraktnr fra oppsigelser',
                '+' => ["ALTER TABLE `{$tp}oppsigelser` DROP COLUMN `kontraktnr`"],
                '-i' => 'Gjenoppretter utskriftsdato krav-tabellen med midlertidige null-verdier',
                '-' => ["ALTER TABLE `{$tp}oppsigelser` ADD `kontraktnr` int(11) unsigned DEFAULT NULL FIRST, ADD INDEX (`kontraktnr`)"],
            ],
        ];

        /**
         * Versjon 0.5.0
         *
         * Påminnelse om at søknaden utløper
         */
        $skript['0.5.0'] = [
            [
                '+i' => 'Legger til aktiv-felt for søknader',
                '+' => ["ALTER TABLE `{$tp}søknader` ADD `aktiv` tinyint(1) NOT NULL DEFAULT '1' AFTER `søknadstype`, ADD INDEX (`aktiv`)"],
                '-i' => 'Sletter aktiv-felt for søknader',
                '-' => ["ALTER TABLE `{$tp}søknader` DROP COLUMN `aktiv`"],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen utløp_varslet for søknader',
                '+' => [
                    <<< EOQ
INSERT INTO `{$tp}eav_egenskaper` (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
SELECT
    'Kyegil\\\\Leiebasen\\\\Modell\\\\Søknad','utløp_varslet','datetime',NULL,'',NULL
FROM dual
    WHERE NOT EXISTS
    (
        SELECT *
        FROM `{$tp}eav_egenskaper`
        WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Søknad'
        AND `kode` = 'utløp_varslet'
    )
EOQ
                ],
            ],
        ];

        /**
         * Versjon 0.6.0
         *
         * Automatisk leiejustering
         */
        $skript['0.6.0'] = [
            [
                '+i' => 'Legger til standardverdi for notater',
                '+' => ["ALTER TABLE `{$tp}notater` CHANGE `notat` `notat` TEXT COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Selve notatet', CHANGE `brevtekst` `brevtekst` TEXT COLLATE utf8_danish_ci DEFAULT NULL COMMENT 'Om dette er et brev/henvendelse går teksten in her', CHANGE `vedlegg` `vedlegg` VARCHAR(1000) COLLATE utf8_danish_ci DEFAULT NULL, CHANGE `vedleggsnavn` `vedleggsnavn` VARCHAR(1000) COLLATE utf8_danish_ci DEFAULT NULL, CHANGE `dokumentreferanse` `dokumentreferanse` VARCHAR(100) COLLATE utf8_danish_ci DEFAULT NULL COMMENT 'Referanse til tilhørende dokument', CHANGE `dokumenttype` `dokumenttype` VARCHAR(100) COLLATE utf8_danish_ci DEFAULT NULL COMMENT 'Hva slags dokument som refereres', CHANGE `skjul_for_leietaker` `skjul_for_leietaker` TINYINT(1) unsigned NOT NULL DEFAULT '0';"],
                '-i' => 'Fjerner standardverdi for notater',
                '-' => [
                    "UPDATE `{$tp}notater` SET `notat` = '' WHERE `notat` IS NULL;",
                    "UPDATE `{$tp}notater` SET `brevtekst` = '' WHERE `brevtekst` IS NULL;",
                    "UPDATE `{$tp}notater` SET `vedlegg` = '' WHERE `vedlegg` IS NULL;",
                    "UPDATE `{$tp}notater` SET `vedleggsnavn` = '' WHERE `vedleggsnavn` IS NULL;",
                    "UPDATE `{$tp}notater` SET `dokumentreferanse` = '' WHERE `dokumentreferanse` IS NULL;",
                    "UPDATE `{$tp}notater` SET `dokumenttype` = '' WHERE `dokumenttype` IS NULL;",
                    "ALTER TABLE `{$tp}notater` CHANGE `notat` `notat` TEXT COLLATE utf8_danish_ci NOT NULL COMMENT 'Selve notatet', CHANGE `brevtekst` `brevtekst` TEXT COLLATE utf8_danish_ci NOT NULL COMMENT 'Om dette er et brev/henvendelse går teksten in her', CHANGE `vedlegg` `vedlegg` VARCHAR(1000) COLLATE utf8_danish_ci NOT NULL DEFAULT '', CHANGE `vedleggsnavn` `vedleggsnavn` VARCHAR(1000) COLLATE utf8_danish_ci NOT NULL DEFAULT '', CHANGE `dokumentreferanse` `dokumentreferanse` VARCHAR(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Referanse til tilhørende dokument', CHANGE `dokumenttype` `dokumenttype` VARCHAR(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Hva slags dokument som refereres', CHANGE `skjul_for_leietaker` `skjul_for_leietaker` TINYINT(3) unsigned NOT NULL;"
                ],
            ],
            [
                '+i' => 'Optimerer dataformatet for leieberegningsmetoder',
                '+' => ["ALTER TABLE `{$tp}leieberegning` CHANGE `leie_objekt` `leie_objekt` DECIMAL(18,6)  NOT NULL  DEFAULT '0', CHANGE `leie_kontrakt` `leie_kontrakt` DECIMAL(18,6)  NOT NULL  DEFAULT '0', CHANGE `leie_kvm` `leie_kvm` DECIMAL(18,6)  NOT NULL  DEFAULT '0';"],
                '-i' => 'Gjenoppretter originalt dataformat for leieberegningsmetoder',
                '-' => ["ALTER TABLE `{$tp}leieberegning` CHANGE `leie_objekt` `leie_objekt` FLOAT  NOT NULL  DEFAULT '0',  CHANGE `leie_kontrakt` `leie_kontrakt` FLOAT  NOT NULL  DEFAULT '0',  CHANGE `leie_kvm` `leie_kvm` FLOAT  NOT NULL  DEFAULT '0';"],
            ],
            [
                '+i' => 'Legger til felt for `spesialregler`, i tabell for leieberegningsmetoder',
                '+' => ["ALTER TABLE `{$tp}leieberegning` ADD `spesialregler` text COLLATE utf8_danish_ci NOT NULL AFTER `leie_kvm`"],
                '-i' => 'Sletter felt for `spesialregler` i tabell for leieberegningsmetoder',
                '-' => ["ALTER TABLE `{$tp}leieberegning` DROP COLUMN `spesialregler`"],
            ],
            [
                '+i' => 'Oppdaterer leieberegningssatsene til årlige beløp',
                '+' => ["UPDATE `{$tp}leieberegning` SET `leie_objekt` = `leie_objekt` * 12, `leie_kontrakt` = `leie_kontrakt` * 12, `leie_kvm` = `leie_kvm` * 12 WHERE 1"],
                '-i' => 'Returnerer leieberegningssatsene til månedlige beløp',
                '-' => ["UPDATE `{$tp}leieberegning` SET `leie_objekt` = `leie_objekt` / 12, `leie_kontrakt` = `leie_kontrakt` / 12, `leie_kvm` = `leie_kvm` / 12 WHERE 1"],
            ],
            [
                '+i' => 'Oppretter spesialregler på grunnlag av feltene leie_var_bad, leie_var_fellesdo og leie_var_egendo',
                '+' => [
                    function(\mysqli $mysqli, CoreModelImplementering $app) {
                        /** @var Leieberegningsett $leieberegningsett */
                        $leieberegningsett = $app->hentSamling(Leieberegning::class);
                        $leieberegningsett
                            ->leggTilFelt('leieberegning', 'leie_var_bad', null, null, 'leieberegning_ekstra')
                            ->leggTilFelt('leieberegning', 'leie_var_fellesdo', null, null, 'leieberegning_ekstra')
                            ->leggTilFelt('leieberegning', 'leie_var_egendo', null, null, 'leieberegning_ekstra');
                        /** @var Leieberegning $leieberegning */
                        foreach($leieberegningsett as $leieberegning) {
                            $spesialregler = [];
                            $leieVarBad = $leieberegning->getRawData()->leieberegning_ekstra->leie_var_bad ?? 0;
                            $leieVarFellesdo = $leieberegning->getRawData()->leieberegning_ekstra->leie_var_fellesdo ?? 0;
                            $leieVarEgendo = $leieberegning->getRawData()->leieberegning_ekstra->leie_var_egendo ?? 0;
                            if ($leieVarBad) {
                                $spesialregler[] = [
                                    'forklaring' => 'Tillegg for tilgang på bad/dusj',
                                    'sorteringsorden' => 1,
                                    'prosessor' => Leieberegning::class . '::formelProsessor',
                                    'sats' => $leieVarBad * 12,
                                    'unntattFraLeiejustering' => false,
                                    'param' => [
                                        'kriterier' => ['leieobjekt.bad'],
                                        'formel' => '{sats}'
                                    ]
                                ];
                            }
                            if ($leieVarFellesdo) {
                                $spesialregler[] = [
                                    'forklaring' => 'Tillegg for tilgang på felles do i samme bygning',
                                    'sorteringsorden' => 2,
                                    'prosessor' => Leieberegning::class . '::formelProsessor',
                                    'sats' => $leieVarFellesdo * 12,
                                    'unntattFraLeiejustering' => false,
                                    'param' => [
                                        'kriterier' => ['leieobjekt.toalett_kategori' => 1],
                                        'formel' => '{sats}'
                                    ]
                                ];
                            }
                            if ($leieVarEgendo) {
                                $spesialregler[] = [
                                    'forklaring' => 'Tillegg for egen do',
                                    'sorteringsorden' => 3,
                                    'prosessor' => Leieberegning::class . '::formelProsessor',
                                    'sats' => $leieVarEgendo * 12,
                                    'unntattFraLeiejustering' => false,
                                    'param' => [
                                        'kriterier' => ['leieobjekt.toalett_kategori' => 2],
                                        'formel' => '{sats}'
                                    ]
                                ];
                            }
                            $leieberegning->sett('spesialregler', $spesialregler);
                        }
                    }
                ],
                '-i' => 'Gjenoppretter leie_var_bad, leie_var_fellesdo og leie_var_egendo fra spesialreglene',
                '-' => [
                    function(\mysqli $mysqli, CoreModelImplementering $app) {
                        /** @var \stdClass $leieberegninger */
                        $leieberegninger = [];
                        $tp = $this->hentTablePrefix();
                        /** @var Leieberegning $leieberegning */
                        $sql = "SELECT `nr`, `spesialregler` FROM `{$tp}leieberegning`";
                        /** @var \mysqli_result $mysqliResult */
                        $mysqliResult = $mysqli->query($sql);
                        /** @var \stdClass $leieberegning */
                        while ($leieberegning = $mysqliResult->fetch_object()) {
                            $leieberegning->leieForBad = null;
                            $leieberegning->leieForFellesDo = null;
                            $leieberegning->leieForEgenDo = null;

                            /** @var object[] $spesialregler */
                            $spesialregler = json_decode($leieberegning->spesialregler) ?: [];
                            foreach ($spesialregler as $regel) {
                                if (
                                    !isset($leieberegning->leieForBad)
                                    && ($regel->prosessor ?? null) === 'Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning::formelProsessor'
                                    && isset($regel->param)
                                    && is_object($regel->param)
                                    && ($regel->param->kriterier ?? null) === ['leieobjekt.bad']
                                    && ($regel->param->formel ?? null) === '{sats}'
                                ) {
                                    $leieberegning->leieForBad = ($regel->sats ?? 0) / 12;
                                } else if (
                                    !isset($leieberegning->leieForFellesDo)
                                    && ($regel->prosessor ?? null) === 'Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning::formelProsessor'
                                    && isset($regel->param)
                                    && is_object($regel->param)
                                    && (array)($regel->param->kriterier ?? []) === ['leieobjekt.toalett_kategori' => 1]
                                    && ($regel->param->formel ?? null) === '{sats}'
                                ) {
                                    $leieberegning->leieForFellesDo = ($regel->sats ?? 0) / 12;
                                } else if (
                                    !isset($leieberegning->leieForEgenDo)
                                    && ($regel->prosessor ?? null) === 'Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning::formelProsessor'
                                    && isset($regel->param)
                                    && is_object($regel->param)
                                    && (array)($regel->param->kriterier ?? []) === ['leieobjekt.toalett_kategori' => 2]
                                    && ($regel->param->formel ?? null) === '{sats}'
                                ) {
                                    $leieberegning->leieForEgenDo = ($regel->sats ?? 0) / 12;
                                }
                            }

                            $leieberegning->leieForBad = $leieberegning->leieForBad ?? 0;
                            $leieberegning->leieForFellesDo = $leieberegning->leieForFellesDo ?? 0;
                            $leieberegning->leieForEgenDo = $leieberegning->leieForEgenDo ?? 0;
                            if ($leieberegning->leieForBad || $leieberegning->leieForFellesDo || $leieberegning->leieForEgenDo) {
                                $leieberegninger[] = $leieberegning;
                            }
                        }
                        foreach ($leieberegninger as $leieberegning) {
                            $sql = "UPDATE `{$tp}leieberegning`\n";
                            $sql .= "SET `leie_var_bad` = ?, `leie_var_fellesdo` = ?, `leie_var_egendo` = ?\n";
                            $sql .= "WHERE `nr` = ?";
                            $statement = $mysqli->prepare($sql);
                            $statement->bind_param('sssi',
                                $leieberegning->leieForBad,
                                $leieberegning->leieForFellesDo,
                                $leieberegning->leieForEgenDo,
                                $leieberegning->nr
                            );
                            $statement->execute();
                            $statement->close();
                        }
                    }
                ],
            ],
            [
                '+i' => 'Sletter felt for `leie_var_bad` i tabell for leieberegningsmetoder',
                '+' => ["ALTER TABLE `{$tp}leieberegning` DROP COLUMN `leie_var_bad`"],
                '-i' => 'Gjenoppretter felt for `leie_var_bad` i tabell for leieberegningsmetoder',
                '-' => ["ALTER TABLE `{$tp}leieberegning` ADD `leie_var_bad` float NOT NULL DEFAULT '0' AFTER `leie_kvm`"],
            ],
            [
                '+i' => 'Sletter felt for `leie_var_fellesdo` i tabell for leieberegningsmetoder',
                '+' => ["ALTER TABLE `{$tp}leieberegning` DROP COLUMN `leie_var_fellesdo`"],
                '-i' => 'Gjenoppretter felt for `leie_var_fellesdo` i tabell for leieberegningsmetoder',
                '-' => ["ALTER TABLE `{$tp}leieberegning` ADD `leie_var_fellesdo` float NOT NULL DEFAULT '0' AFTER `leie_kvm`"],
            ],
            [
                '+i' => 'Sletter felt for `leie_var_egendo` i tabell for leieberegningsmetoder',
                '+' => ["ALTER TABLE `{$tp}leieberegning` DROP COLUMN `leie_var_egendo`"],
                '-i' => 'Gjenoppretter felt for `leie_var_egendo` i tabell for leieberegningsmetoder',
                '-' => ["ALTER TABLE `{$tp}leieberegning` ADD `leie_var_egendo` float NOT NULL DEFAULT '0' AFTER `leie_kvm`"],
            ],
            [
                '+i' => 'Sletter felt for `solfond` i tabell for leieberegningsmetoder',
                '+' => ["ALTER TABLE `{$tp}leieberegning` DROP COLUMN `solfond`"],
                '-i' => 'Gjenoppretter felt for `solfond` i tabell for leieberegningsmetoder',
                '-' => ["ALTER TABLE `{$tp}leieberegning` ADD `solfond` float NOT NULL DEFAULT '0' AFTER `leie_kvm`"],
            ],
            [
                '+i' => 'Endrer innstillingen `leiereguleringsbrevmal` til `leiejustering_brevmal`',
                '+' => ["UPDATE `{$tp}valg` SET `innstilling` = 'leiejustering_brevmal' WHERE `innstilling` = 'leiereguleringsbrevmal'"],
                '-i' => 'Reverserer endring av innstillingen `leiereguleringsbrevmal`',
                '-' => ["UPDATE `{$tp}valg` SET `innstilling` = 'leiereguleringsbrevmal' WHERE `innstilling` = 'leiejustering_brevmal'"],
            ],
            [
                '+i' => 'Oppretter innstillingen leiejustering_automatisk',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('leiejustering_automatisk','0')"],
                '-i' => 'Fjerner innstillingen leiejustering_automatisk',
                '-' => ["DELETE FROM `{$tp}valg` WHERE `innstilling` = 'leiejustering_automatisk'"],
            ],
            [
                '+i' => 'Oppretter innstillingen leiejustering_gjeldende_indeks',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('leiejustering_gjeldende_indeks','0')"],
                '-i' => 'Fjerner innstillingen leiejustering_gjeldende_indeks',
                '-' => ["DELETE FROM `{$tp}valg` WHERE `innstilling` = 'leiejustering_gjeldende_indeks'"],
            ],
            [
                '+i' => 'Oppretter innstillingen leiejustering_auto_kpi',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('leiejustering_auto_kpi','0')"],
                '-i' => 'Fjerner innstillingen leiejustering_auto_kpi',
                '-' => ["DELETE FROM `{$tp}valg` WHERE `innstilling` = 'leiejustering_auto_kpi'"],
            ],
            [
                '+i' => 'Oppretter innstillingen leiejustering_pos_forankring',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('leiejustering_pos_forankring','0.5')"],
                '-i' => 'Fjerner innstillingen leiejustering_pos_forankring',
                '-' => ["DELETE FROM `{$tp}valg` WHERE `innstilling` = 'leiejustering_pos_forankring'"],
            ],
            [
                '+i' => 'Oppretter innstillingen leiejustering_neg_forankring',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('leiejustering_neg_forankring','1')"],
                '-i' => 'Fjerner innstillingen leiejustering_neg_forankring',
                '-' => ["DELETE FROM `{$tp}valg` WHERE `innstilling` = 'leiejustering_neg_forankring'"],
            ],
            [
                '+i' => 'Oppretter innstillingen leiejustering_intervall',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('leiejustering_intervall','P12M')"],
                '-i' => 'Fjerner innstillingen leiejustering_intervall',
                '-' => ["DELETE FROM `{$tp}valg` WHERE `innstilling` = 'leiejustering_intervall'"],
            ],
            [
                '+i' => 'Oppretter innstillingen leiejustering_varselfrist',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('leiejustering_varselfrist','P1M')"],
                '-i' => 'Fjerner innstillingen leiejustering_varselfrist',
                '-' => ["DELETE FROM `{$tp}valg` WHERE `innstilling` = 'leiejustering_varselfrist'"],
            ],
            [
                '+i' => 'Oppretter innstillingen leiejustering_forslagsfrist',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('leiejustering_forslagsfrist','P1M')"],
                '-i' => 'Fjerner innstillingen leiejustering_forslagsfrist',
                '-' => ["DELETE FROM `{$tp}valg` WHERE `innstilling` = 'leiejustering_forslagsfrist'"],
            ],
            [
                '+i' => 'Oppretter innstillingen leiejustering_auto_godkjenning_tid_før_frist',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('leiejustering_auto_godkjenning_tid_før_frist','P4D')"],
                '-i' => 'Fjerner innstillingen leiejustering_auto_godkjenning_tid_før_frist',
                '-' => ["DELETE FROM `{$tp}valg` WHERE `innstilling` = 'leiejustering_auto_godkjenning_tid_før_frist'"],
            ],
            [
                '+i' => 'Oppretter innstillingen varsler_drift',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('advarsler_drift','{}')"],
                '-i' => 'Fjerner innstillingen varsler_drift',
                '-' => ["DELETE FROM `{$tp}valg` WHERE `innstilling` = 'advarsler_drift'"],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen sats_historikk for leieberegningsmetoder',
                '+' => [
                    <<< EOQ
INSERT INTO `{$tp}eav_egenskaper` (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
SELECT
    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieobjekt\\\\Leieberegning','sats_historikk','json',NULL,'Historiske leieberegningssatser',NULL
FROM dual
    WHERE NOT EXISTS
    (
        SELECT *
        FROM `{$tp}eav_egenskaper`
        WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieobjekt\\\\Leieberegning'
        AND `kode` = 'sats_historikk'
    )
EOQ
                ],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen sats_oppdatert for leieberegningsmetoder',
                '+' => [
                    <<< EOQ
INSERT INTO `{$tp}eav_egenskaper` (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
SELECT
    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieobjekt\\\\Leieberegning','sats_oppdatert','datetime',NULL,'Tidsstempel for når leieberegningssatser sist ble oppdatert',NULL
FROM dual
    WHERE NOT EXISTS
    (
        SELECT *
        FROM `{$tp}eav_egenskaper`
        WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieobjekt\\\\Leieberegning'
        AND `kode` = 'sats_oppdatert'
    )
EOQ
                ],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen leie_historikk for leieforhold',
                '+' => [
                    <<< EOQ
INSERT INTO `{$tp}eav_egenskaper` (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
SELECT
    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold','leie_historikk','json',NULL,'Historisk leie',NULL
FROM dual
    WHERE NOT EXISTS
    (
        SELECT *
        FROM `{$tp}eav_egenskaper`
        WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold'
        AND `kode` = 'leie_historikk'
    )
EOQ
                ],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen leie_oppdatert for leieforhold',
                '+' => [
                    <<< EOQ
INSERT INTO `{$tp}eav_egenskaper` (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
SELECT
    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold','leie_oppdatert','datetime',NULL,'Tidsstempel for når leien sist ble oppdatert',NULL
FROM dual
    WHERE NOT EXISTS
    (
        SELECT *
        FROM `{$tp}eav_egenskaper`
        WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold'
        AND `kode` = 'leie_oppdatert'
    )
EOQ
                ],
            ],
            [
                '+i' => 'Beregner verdier for leie_historikk og leie_oppdatert',
                '+' => [
                    <<< EOQ
SET SESSION group_concat_max_len = 1000000;
EOQ
                    ,
                    <<< EOQ
DROP TEMPORARY TABLE IF EXISTS `leie_historikk`;
EOQ
                    ,
                    <<< EOQ
CREATE TEMPORARY TABLE `leie_historikk`
SELECT
    (SELECT `id` FROM `{$tp}eav_egenskaper` AS `eav_egenskaper` WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold' AND `kode` = 'leie_historikk') AS `leie_historikk_egenskap_id`,
    (SELECT `id` FROM `{$tp}eav_egenskaper` AS `eav_egenskaper` WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold' AND `kode` = 'leie_oppdatert') AS `leie_oppdatert_egenskap_id`,
    `krav`.`leieforhold`,
    MIN(`krav`.`fom`) AS `dato`,
    `leieforhold`.`ant_terminer` * (`krav`.`beløp` - (SELECT SUM(`beløp`) FROM `{$tp}delkrav` AS `delkrav` WHERE `kravid` = `krav`.`id`)) AS `basisleie`,
    IF(MIN(`krav`.`fom`) = `leieforhold`.`fradato`, "false", "true") AS `automatisk`,
    CONCAT(YEAR(DATE_SUB(MIN(`krav`.`fom`), INTERVAL 2 MONTH)), 'M', SUBSTRING(DATE_SUB(MIN(`krav`.`fom`), INTERVAL 2 MONTH), 6, 2)) AS gjeldende_kpi,
    CONCAT(CONCAT('"',MIN(`krav`.`fom`),'T00:00:00+00:00"'),':{"basisleie":',`leieforhold`.`ant_terminer` * (`krav`.`beløp` - (SELECT SUM(`beløp`) FROM `{$tp}delkrav` AS `delkrav` WHERE `kravid` = `krav`.`id`)),',"automatisk":',IF(MIN(`krav`.`fom`) = `leieforhold`.`fradato`, "false", "true"),',"gjeldende_kpi":"',CONCAT(YEAR(DATE_SUB(MIN(`krav`.`fom`), INTERVAL 2 MONTH)), 'M', SUBSTRING(DATE_SUB(MIN(`krav`.`fom`), INTERVAL 2 MONTH), 6, 2)),'"}') AS json

FROM `{$tp}krav` AS `krav`
         INNER JOIN `{$tp}leieforhold` AS `leieforhold` ON `krav`.`leieforhold` = `leieforhold`.`leieforholdnr`
WHERE `krav`.`type` = 'Husleie'
  AND `krav`.`beløp` > 0
  AND `krav`.`fom` < CURDATE()
GROUP BY `krav`.`leieforhold`, `krav`.`beløp`;
EOQ
                    ,
                    <<< EOQ
INSERT INTO `{$tp}eav_verdier` (`egenskap_id`,`objekt_id`,`verdi`)
SELECT
    `leie_historikk`.`leie_historikk_egenskap_id` AS `egenskap_id`,
    `leie_historikk`.`leieforhold` AS `objekt_id`,
    CONCAT("{",GROUP_CONCAT(`leie_historikk`.`json`), "}") AS `verdi`
FROM `leie_historikk`
LEFT JOIN `{$tp}eav_verdier` AS `eav_verdier` ON `eav_verdier`.`egenskap_id` = `leie_historikk`.`leie_historikk_egenskap_id` AND `eav_verdier`.`objekt_id` = `leie_historikk`.`leieforhold`
WHERE `eav_verdier`.`id` IS NULL
GROUP BY `leie_historikk`.`leieforhold`;
EOQ
                    ,
                    <<< EOQ
INSERT INTO `{$tp}eav_verdier` (`egenskap_id`,`objekt_id`,`verdi`)
SELECT
    `leie_historikk`.`leie_oppdatert_egenskap_id` AS `egenskap_id`,
    `leie_historikk`.`leieforhold` AS `objekt_id`,
    MAX(`leie_historikk`.`dato`) AS `verdi`
FROM `leie_historikk`
LEFT JOIN `{$tp}eav_verdier` AS `eav_verdier` ON `eav_verdier`.`egenskap_id` = `leie_historikk`.`leie_oppdatert_egenskap_id` AND `eav_verdier`.`objekt_id` = `leie_historikk`.`leieforhold`
WHERE `eav_verdier`.`id` IS NULL
GROUP BY `leie_historikk`.`leieforhold`;
EOQ
                    ,
                ],
                '-i' => 'Sletter verdiene for leie_historikk og leie_oppdatert',
                '-' => [
                    <<< EOQ
DELETE `eav_verdier`.*
FROM `{$tp}eav_verdier` AS `eav_verdier`
         INNER JOIN `{$tp}eav_egenskaper` AS `eav_egenskaper` ON `eav_verdier`.`egenskap_id` = `eav_egenskaper`.`id`
WHERE `eav_egenskaper`.`modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold' AND `eav_egenskaper`.`kode` IN('leie_historikk', 'leie_oppdatert');
EOQ
                    ,
                ],
            ],
            [
                '+i' => 'Oppretter felt for sats_oppdatert for delkravtyper',
                '+' => ["ALTER TABLE `{$tp}delkravtyper` ADD `sats_oppdatert` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `aktiv`, ADD INDEX (`sats_oppdatert`)"],
                '-i' => 'Fjerner sats_oppdatert for delkravtyper',
                '-' => ["ALTER TABLE `{$tp}delkravtyper` DROP COLUMN `sats_oppdatert`"],
            ],
            [
                '+i' => 'Oppretter felt for sats_historikk for delkravtyper',
                '+' => ["ALTER TABLE `{$tp}delkravtyper` ADD `sats_historikk` text COLLATE utf8_danish_ci NOT NULL AFTER `sats_oppdatert`"],
                '-i' => 'Fjerner felt for for delkravtyper',
                '-' => ["ALTER TABLE `{$tp}delkravtyper` DROP COLUMN `sats_historikk`"],
            ],
            [
                '+i' => 'Beregner sats-historikk for delkravtyper',
                '+' => [
                    <<< EOQ
UPDATE `{$tp}delkravtyper`
SET `sats_historikk` = CONCAT('{"', REPLACE(`sats_oppdatert`, ' ', 'T'), '+00:00":{"automatisk":false,"relativ":', IF(`relativ`, 'true', 'false'),',"sats":', `sats`,'}','}')
WHERE true;
EOQ
                ],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen indeksreguleres for delkravtyper',
                '+' => [
                    <<< EOQ
INSERT INTO `{$tp}eav_egenskaper` (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
SELECT
    'Kyegil\\\\Leiebasen\\\\Modell\\\\Delkravtype','indeksreguleres','boolean',NULL,'Satsen skal indeksreguleres',NULL
FROM dual
    WHERE NOT EXISTS
    (
        SELECT *
        FROM `{$tp}eav_egenskaper`
        WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Delkravtype'
        AND `kode` = 'indeksreguleres'
    )
EOQ
                ],
            ],
        ];

        /**
         * Versjon 0.6.1
         *
         * Stikkord i søknader
         */
        $skript['0.6.1'] = [
            [
                '+i' => 'Legger til default value for `stikkord` i søknadstabellen',
                '+' => ["ALTER TABLE `{$tp}søknader` CHANGE `stikkord` `stikkord` text COLLATE utf8_danish_ci NOT NULL DEFAULT '' AFTER `søknadstekst`"],
                '-i' => 'Fjerner default value for `stikkord` i søknadstabellen',
                '-' => ["ALTER TABLE `{$tp}søknader` CHANGE `stikkord` `stikkord` text COLLATE utf8_danish_ci NOT NULL AFTER `søknadstekst`"],
            ],
        ];

        /**
         * Versjon 0.7.0
         *
         * Mulighet for å be om betalingsutsettelser
         */
        $skript['0.7.0'] = [
            [
                '+i' => 'Oppretter innstilling: Aktivér selvstyrt betalingsutsettelse',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('betalingsutsettelse_aktivert','0')"],
                '-i' => 'Fjerner innstilling: Aktivér selvstyrt betalingsutsettelse',
                '-' => ["DELETE FROM `{$tp}valg` WHERE `innstilling` = 'betalingsutsettelse_aktivert'"],
            ],
            [
                '+i' => 'Oppretter innstilling: Antall dager utsettelse som kan fordeles på ubetalte regninger i et leieforhold uten utestående regninger',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('betalingsutsettelse_spillerom_standard','45')"],
                '-i' => 'Fjerner innstilling: Standard spillerom for antall dager betalingsutsettelse per leieforhold',
                '-' => ["DELETE FROM `{$tp}valg` WHERE `innstilling` = 'betalingsutsettelse_spillerom_standard'"],
            ],
            [
                '+i' => 'Oppretter innstilling: Antall dager utsettelse som kan fordeles på ubetalte regninger i et leieforhold med nylig forfalte regninger',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('betalingsutsettelse_spillerom_kortsiktig_gjeld','14')"],
            ],
            [
                '+i' => 'Oppretter innstilling: Muligheten for automatisk betalingsutsettelse opphører for leieforhold med forfalte regninger eldre enn antall dager',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('betalingsutsettelse_spillerom_kortsiktighet','10')"],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen original_forfallsdato for regninger',
                '+' => [
                    <<< EOQ
                    INSERT INTO `{$tp}eav_egenskaper`
                        (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
                    SELECT
                        'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Regning',
                        'original_forfallsdato',
                        'date',
                        NULL,
                        'Original forfallsdato ved betalingsutsettelse',
                        NULL
                    FROM dual
                        WHERE NOT EXISTS
                        (
                            SELECT *
                            FROM `{$tp}eav_egenskaper`
                            WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Regning'
                            AND `kode` = 'original_forfallsdato'
                        )
                    EOQ
                ],
            ],
        ];


        /**
         * Versjon 0.8.0
         *
         * Mulighet for å be om betalingsutsettelser
         */
        $skript['0.8.0'] = [
            [
                '+i' => 'Oppretter eav_egenskapen original_forfallsdato for regninger',
                '+' => [
                    <<< EOQ
                    INSERT INTO `{$tp}eav_egenskaper`
                        (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
                    SELECT
                        'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Regning',
                        'utsettelse_forespørsel',
                        'json',
                        NULL,
                        'Forespørsel om betalingsutsettelse',
                        NULL
                    FROM dual
                        WHERE NOT EXISTS
                        (
                            SELECT *
                            FROM `{$tp}eav_egenskaper`
                            WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Regning'
                            AND `kode` = 'utsettelse_forespørsel'
                        )
                    EOQ
                ],
            ],
        ];

        /**
         * Versjon 0.9.0
         *
         * Poeng-program
         */
        $skript['0.9.0'] = [
            [
                '+i' => 'Utvid innstilling-lengden-feltet i valg-tabellen',
                '+' => [
                    "ALTER TABLE `{$tp}valg` CHANGE `innstilling` `innstilling` VARCHAR(100)  CHARACTER SET utf8  COLLATE utf8_danish_ci  NOT NULL;"
                ],
            ],
            [
                '+i' => 'Oppretter tabellen eav_verdier_vc_id (Eav-verdier for modeller med varchar id)',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}eav_verdier_vc_id` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`egenskap_id` int(11) unsigned DEFAULT NULL, `objekt_id` VARCHAR(255)  CHARACTER SET utf8  COLLATE utf8_danish_ci NULL, `verdi` text, PRIMARY KEY (`id`), UNIQUE KEY `IDX_COMBO` (`egenskap_id`,`objekt_id`), KEY `verdi` (`verdi`(127))) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
            [
                '+i' => 'Oppretter innstilling: Umiddelbart betalingsvarsel sendes antall dager etter forfall',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('umiddelbart_betalingsvarsel_ant_dager','1')"],
            ],
            [
                '+i' => 'Oppretter innstilling: Tekst for umiddelbart betalingsvarsel',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('umiddelbart_betalingsvarsel_tekst_epost','')"],
            ],
            [
                '+i' => 'Oppretter innstilling: Umiddelbart betalingsvarsel sist sendt for forfall',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('varselstempel_umiddelbart_betalingsvarsel',NOW())"],
                '-i' => 'Fjerner innstilling: Umiddelbart betalingsvarsel sist sendt for forfall',
                '-' => ["DELETE FROM `{$tp}valg` WHERE `innstilling` = 'varselstempel_umiddelbart_betalingsvarsel'"],
            ],
            [
                '+i' => 'Oppretter tabell for poeng-program',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}poeng_program` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `kode` varchar(127) DEFAULT NULL COMMENT 'Unik kode for poengprogrammet', `aktivert` datetime NULL DEFAULT NULL COMMENT 'Aktivering/deaktivering av poengprogrammet (tidspunkt for aktivering hvis aktivt)', `navn` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Navn på poengprogrammet', `beskrivelse` text COLLATE utf8_danish_ci NOT NULL COMMENT 'Utfyllende beskrivelse av poengprogrammet', `levetid` varchar(20) COLLATE utf8_danish_ci NULL DEFAULT NULL, `konfigurering` text, PRIMARY KEY (`id`),UNIQUE KEY `kode` (`kode`), KEY `aktivert` (`aktivert`), KEY `navn` (`navn`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
            [
                '+i' => 'Oppretter poeng-tabellen',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}poeng` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `program_id` int(11) unsigned NOT NULL COMMENT 'Programmet som poenget er gitt i', `type` varchar(127) DEFAULT NULL COMMENT 'Type-kategorisering av grunnen til poenggivingen', `tid` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `foreldes` datetime NULL DEFAULT NULL, `leieforhold_id` int(11) unsigned DEFAULT NULL, `verdi` int(11) DEFAULT '0' COMMENT 'poeng-verdien', `tekst` varchar(255) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Beskrivelse av poengene', `registrerer` varchar(100) COLLATE utf8_danish_ci NOT NULL DEFAULT '', PRIMARY KEY (`id`), KEY `program_id` (`program_id`), KEY `type` (`type`), KEY `tid` (`tid`), KEY `foreldes` (`foreldes`), KEY `leieforhold_id` (`leieforhold_id`), KEY `verdi` (`verdi`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen umiddelbart_betalingsvarsel_epost for adganger',
                '+' => [
                    <<< EOQ
                    INSERT INTO `{$tp}eav_egenskaper`
                        (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
                    SELECT
                        'Kyegil\\\\Leiebasen\\\\Modell\\\\Person\\\\Adgang',
                        'umiddelbart_betalingsvarsel_epost',
                        'boolean',
                        '{\"allowNull\":true}',
                        'Ønsker umiddelbart betalingsvarsel tilsendt etter forfall',
                        NULL
                    FROM dual
                        WHERE NOT EXISTS
                        (
                            SELECT *
                            FROM `{$tp}eav_egenskaper`
                            WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Person\\\\Adgang'
                            AND `kode` = 'umiddelbart_betalingsvarsel_epost'
                        )
                    EOQ
                ],
            ],
        ];

        /**
         * Versjon 0.10.0
         *
         * Fast forfallsdato for hvert leieforhold
         */
        $skript['0.10.0'] = [
            [
                '+i' => 'Oppretter eav_egenskapen forfall_fast_dato for leieforhold',
                '+' => [
                    <<< EOQ
                    INSERT INTO `{$tp}eav_egenskaper`
                        (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
                    SELECT
                        'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold',
                        'forfall_fast_dato',
                        'string',
                        NULL,
                        'Juster forfall årlig til denne datoen',
                        NULL
                    FROM dual
                        WHERE NOT EXISTS
                        (
                            SELECT *
                            FROM `{$tp}eav_egenskaper`
                            WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold'
                            AND `kode` = 'forfall_fast_dato'
                        )
                    EOQ
                ],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen forfall_fast_dag_i_måneden for leieforhold',
                '+' => [
                    <<< EOQ
                    INSERT INTO `{$tp}eav_egenskaper`
                        (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
                    SELECT
                        'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold',
                        'forfall_fast_dag_i_måneden',
                        'integer',
                        NULL,
                        'Juster forfall månedlig til denne datoen',
                        NULL
                    FROM dual
                        WHERE NOT EXISTS
                        (
                            SELECT *
                            FROM `{$tp}eav_egenskaper`
                            WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold'
                            AND `kode` = 'forfall_fast_dag_i_måneden'
                        )
                    EOQ
                ],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen forfall_fast_ukedag for leieforhold',
                '+' => [
                    <<< EOQ
                    INSERT INTO `{$tp}eav_egenskaper`
                        (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
                    SELECT
                        'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold',
                        'forfall_fast_ukedag',
                        'integer',
                        NULL,
                        'Juster forfall fast til denne ukedagen',
                        NULL
                    FROM dual
                        WHERE NOT EXISTS
                        (
                            SELECT *
                            FROM `{$tp}eav_egenskaper`
                            WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold'
                            AND `kode` = 'forfall_fast_ukedag'
                        )
                    EOQ
                ],
            ],
        ];

        /**
         * Versjon 0.11.0
         *
         * Endrer innstillingen epost_purringer_aktivert til epost_purregebyr_aktivert
         */
        $skript['0.11.0'] = [
            [
                '+i' => 'Endrer innstillingen epost_purringer_aktivert til epost_purregebyr_aktivert',
                '+' => ["UPDATE `{$tp}valg` SET `innstilling` = 'epost_purregebyr_aktivert' WHERE `innstilling` = 'epost_purringer_aktivert'"],
                '-i' => 'Endrer innstillingen epost_purregebyr_aktivert tilbake til epost_purringer_aktivert',
                '-' => ["UPDATE `{$tp}valg` SET `innstilling` = 'epost_purringer_aktivert' WHERE `innstilling` = 'epost_purregebyr_aktivert'"],
            ],
        ];

        /**
         * Versjon 0.12.0
         *
         * Legger til feltet reell_krav_id til krav
         */
        $skript['0.12.0'] = [
            [
                '+i' => 'Re-strukturerer krav-tabellen',
                '+' => ["ALTER TABLE `{$tp}krav` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT FIRST"],
                '-i' => 'Gjenoppretter opprinnelig krav-tabell-sruktur',
                '-' => ["ALTER TABLE `{$tp}krav` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT AFTER `gironr`"],
            ],
            [
                '+i' => 'Legger til felt for `reell_krav_id` i tabell for krav',
                '+' => ["ALTER TABLE `{$tp}krav` ADD `reell_krav_id` INT UNSIGNED NULL AFTER `id`, ADD CONSTRAINT UNIQUE KEY `reell_krav_id` (`reell_krav_id`)"],
                '-i' => 'Sletter felt for `reell_krav_id` i tabell for krav',
                '-' => ["ALTER TABLE `{$tp}krav` DROP COLUMN `reell_krav_id`"],
            ],
            [
                '+i' => 'Oppretter `reell_krav_id`-verdi for eksisterende krav',
                '+' => [
                    "SET @i:=0",
                    "UPDATE `{$tp}krav` AS `krav` SET `krav`.`reell_krav_id` = @i:=(@i+1) WHERE `krav`.`gironr` IS NOT NULL ORDER BY `krav`.`gironr`, `krav`.`id`"
                ],
            ],
        ];

        /**
         * Versjon 0.13.0
         *
         * Automatisk utskrift av regninger
         */
        $skript['0.13.0'] = [
            [
                '+i' => 'Oppretter innstilling: Send regninger automatisk',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('automatisk_faktura','0')"],
            ],
            [
                '+i' => 'Oppretter innstilling: Forbered manuell utskrift på følgende datoer',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('automatisk_faktura_datoer','*')"],
            ],
            [
                '+i' => 'Oppretter innstilling: Kun på følgende ukedager',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('automatisk_faktura_ukedager','1,2,3,4,5')"],
            ],
            [
                '+i' => 'Oppretter innstilling: Inkluder krav med forfall fram til',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('automatisk_faktura_tidsrom','P1M')"],
            ],
            [
                '+i' => 'Oppretter innstilling: Ekskluder kravtyper',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('automatisk_faktura_ekskluder_kravtyper','')"],
            ],
            [
                '+i' => 'Oppretter innstilling: Separate regninger for hver hovedkravtype',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('automatisk_faktura_adskilt','0')"],
            ],
            [
                '+i' => 'Oppretter innstilling: Purr ved utskrift',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('automatisk_purring','1')"],
            ],
        ];

        /**
         * Versjon 0.14.0
         *
         * Fjerner diverse database-problemer
         */
        $skript['0.14.0'] = [
            [
                '+i' => 'Fjerner unik indeks for efaktura-identifier',
                '+' => ["ALTER TABLE `{$tp}efaktura_identifiers` DROP INDEX `person_id`, ADD INDEX (`person_id`)"],
                '-i' => 'Gjenoppretter unik indeks for efaktura-identifier',
                '-' => ["ALTER TABLE `{$tp}efaktura_identifiers` DROP INDEX `person_id`, ADD CONSTRAINT UNIQUE KEY `person_id` (`person_id`)"],
            ],
            [
                '+i' => 'Fjerner transaksjon-kolonnen for efaktura',
                '+' => ["ALTER TABLE `{$tp}efakturaer` DROP COLUMN `transaksjon`"],
                '-i' => 'Gjenoppretter transaksjon-kolonnen for efaktura',
                '-' => ["ALTER TABLE `{$tp}efakturaer` ADD `transaksjon` int(11) DEFAULT NULL AFTER `oppdrag`, DROP INDEX `giro`, ADD INDEX `giro` (`giro`,`forsendelsesdato`,`forsendelse`,`oppdrag`,`transaksjon`,`kvittert_dato`,`kvitteringsforsendelse`,`status`)"],
            ],
            [
                '+i' => 'Fjerner ubrukte kolonner i fbo_trekkrav',
                '+' => ["ALTER TABLE `{$tp}fbo_trekkrav` DROP `mottakskvittering`, DROP `prosessert_kvittering`, DROP `sletteoppdrag_sendt`"],
                '-i' => 'Gjenoppretter ubrukte kolonner i fbo_trekkrav',
                '-' => ["ALTER TABLE `{$tp}fbo_trekkrav` ADD `mottakskvittering` text COLLATE utf8_danish_ci NOT NULL AFTER `mobilnr`, ADD `prosessert_kvittering` text COLLATE utf8_danish_ci NOT NULL AFTER `mottakskvittering`, ADD `sletteoppdrag_sendt` datetime DEFAULT NULL AFTER `til_sletting`, ADD INDEX (`sletteoppdrag_sendt`)"],
            ],
        ];

        /**
         * Versjon 0.15.0
         *
         * Felt for øvrige beboere i leieforhold
         */
        $skript['0.15.0'] = [
            [
                '+i' => 'Oppretter eav_egenskapen andre_beboere for leieforhold',
                '+' => [
                    <<< EOQ
                    INSERT INTO `{$tp}eav_egenskaper`
                        (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
                    SELECT
                        'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold',
                        'andre_beboere',
                        'Kyegil\\\\Leiebasen\\\\Modell\\\\Person',
                        '{\"allowMultiple\":true}',
                        'Andre husstandsmedlemmer som ikke er listet som kontraktholdere',
                        NULL
                    FROM dual
                        WHERE NOT EXISTS
                        (
                            SELECT *
                            FROM `{$tp}eav_egenskaper`
                            WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold'
                            AND `kode` = 'andre_beboere'
                        )
                    EOQ
                ],
            ],
            [
                '+i' => 'Fjerner innstillingen `forfallsdato`',
                '+' => ["UPDATE `{$tp}valg` SET `innstilling` = '_forfallsdato' WHERE `innstilling` = 'forfallsdato'"],
                '-i' => 'Gjenoppretter innstillingen `forfallsdato`',
                '-' => ["UPDATE `{$tp}valg` SET `innstilling` = 'forfallsdato' WHERE `innstilling` = '_forfallsdato'"],
            ],
            [
                '+i' => 'Omdøper innstillingen `forfallsfrist` til `min_frist_regninger`',
                '+' => ["UPDATE `{$tp}valg` SET `innstilling` = 'min_frist_regninger' WHERE `innstilling` = 'forfallsfrist'"],
                '-i' => 'Reverserer endring av innstillingen `forfallsfrist`',
                '-' => ["UPDATE `{$tp}valg` SET `innstilling` = 'forfallsfrist' WHERE `innstilling` = 'min_frist_regninger'"],
            ],
            [
                '+i' => 'Oppretter innstilling: Vanlig betalingsfrist for leie og faste krav',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('betalingsfrist_faste_krav','P0D')"],
            ],
            [
                '+i' => 'Legger til feltet `termin_betalingsfrist` i leieforholdtabellen',
                '+' => ["ALTER TABLE `{$tp}leieforhold` ADD `termin_betalingsfrist` varchar(50) COLLATE utf8_danish_ci NULL DEFAULT 'P0M' AFTER `ant_terminer`, ADD INDEX (`termin_betalingsfrist`)"],
                '-i' => 'Sletter felt for `termin_betalingsfrist` i leieforholdtabellen',
                '-' => ["ALTER TABLE `{$tp}leieforhold` DROP COLUMN `termin_betalingsfrist`"],
            ],
            [
                '+i' => 'Fyller leieavtalene med utfyllt tekst',
                '+' => [
                    function(\mysqli $mysqli, CoreModelImplementering $app) {
                        /** @var Kontraktsett $kontraktsett */
                        $kontraktsett = $app->hentSamling(Kontrakt::class);
                        $kontraktsett->leggTilLeieforholdModell();
                        $leietakere = $kontraktsett->inkluderLeietakere();
                        $leietakere->leggTilPersonModell();
                        foreach($kontraktsett as $kontrakt) {
                            $leieforhold = $kontrakt->leieforhold;
                            $kontrakt->settTekst($leieforhold->gjengiAvtaletekst(true, $kontrakt, $kontrakt->hentTekst()));
                        }
                    }
                ],
                '-i' => 'Gjenoppretter ikke-utfylt tekst i leieavtalene',
                '-' => ["UPDATE `{$tp}kontrakter` `kontrakter` INNER JOIN `{$tp}leieforhold` `leieforhold` ON `kontrakter`.`kontraktnr` = `leieforhold`.`siste_kontrakt` SET `kontrakter`.`tekst` = `leieforhold`.`avtaletekstmal` WHERE 1"],
            ],
        ];

        /**
         * Versjon 0.15.3
         *
         * Fjerner database-problemer som ikke ble fanget opp i 0.14.0
         */
        $skript['0.15.3'] = [
            [
                '+i' => 'Fjerner unik indeks for efaktura-identifier',
                '+' => ["ALTER TABLE `{$tp}efaktura_identifiers` DROP INDEX `efaktura_identifier`, ADD INDEX (`efaktura_identifier`)"],
                '-i' => 'Gjenoppretter unik indeks for efaktura-identifier',
                '-' => ["ALTER TABLE `{$tp}efaktura_identifiers` DROP INDEX `efaktura_identifier`, ADD CONSTRAINT UNIQUE KEY `efaktura_identifier` (`efaktura_identifier`)"],
            ],
        ];

        /**
         * Versjon 0.16.0
         *
         * Forbinder innbetalinger med kontoer
         */
        $skript['0.16.0'] = [
            [
                '+i' => 'Legger til feltet `konto_id` for kontoer i innbetalinger',
                '+' => ["ALTER TABLE `{$tp}innbetalinger` ADD `konto_id` INT NULL AFTER `innbetaling`, ADD INDEX (`konto_id`)"],
                '-i' => 'Sletter felt for `konto_id` i innbetalinger',
                '-' => ["ALTER TABLE `{$tp}innbetalinger` DROP COLUMN `konto_id`"],
            ],
            [
                '+i' => 'Fyller konto_id for eksisterende innbetalinger',
                '+' => ["UPDATE `{$tp}innbetalinger` LEFT JOIN `{$tp}kontoer` ON `{$tp}innbetalinger`.`konto` = `{$tp}kontoer`.`navn` SET `{$tp}innbetalinger`.`konto_id` = `{$tp}kontoer`.`id` WHERE 1"],
            ],
        ];

        /**
         * Versjon 0.17.0
         *
         * eFaktura API
         */
        $skript['0.17.0'] = [
            [
                '+i' => 'Oppretter innstilling: E-faktura kommunikasjonsmåte',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) SELECT 'efaktura_kommunikasjonsmåte', IF(MAX(`verdi`) > 0, 'sftp', '') FROM valg WHERE `innstilling` = 'efaktura'"],
            ],
            [
                '+i' => 'Oppretter innstilling: E-faktura API consignment_id',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) SELECT 'nets_siste_api_consignment_id', ''"],
            ],
            [
                '+i' => 'Endrer `forsendelse` til å akseptere NULL i efakturaer',
                '+' => ["ALTER TABLE `{$tp}efakturaer` CHANGE `forsendelse` `forsendelse` VARCHAR(11) NULL DEFAULT NULL"],
                '-i' => 'Reverserer `forsendelse` til ikke å akseptere NULL i efakturaer',
                '-' => ["ALTER TABLE `{$tp}efakturaer` CHANGE `forsendelse` `forsendelse` VARCHAR(7) NOT NULL DEFAULT ''"],
            ],
            [
                '+i' => 'Endrer `oppdrag` til å akseptere NULL i efakturaer',
                '+' => ["ALTER TABLE `{$tp}efakturaer` CHANGE `oppdrag` `oppdrag` INT(11) NULL DEFAULT NULL"],
                '-i' => 'Reverserer `oppdrag` til ikke å akseptere NULL i efakturaer',
                '-' => ["ALTER TABLE `{$tp}efakturaer` CHANGE `oppdrag` `oppdrag` INT(11) NOT NULL"],
            ],
            [
                '+i' => 'Endrer `kvitteringsforsendelse` til å akseptere NULL i efakturaer',
                '+' => ["ALTER TABLE `{$tp}efakturaer` CHANGE `kvitteringsforsendelse` `kvitteringsforsendelse` VARCHAR(11) NULL DEFAULT NULL"],
                '-i' => 'Reverserer `kvitteringsforsendelse` til ikke å akseptere NULL i efakturaer',
                '-' => ["ALTER TABLE `{$tp}efakturaer` CHANGE `kvitteringsforsendelse` `kvitteringsforsendelse` VARCHAR(7) NOT NULL DEFAULT ''"],
            ],
            [
                '+i' => 'Endrer `status` til å inkludere standardverdi',
                '+' => ["ALTER TABLE `{$tp}efakturaer` CHANGE `status` `status` varchar(100) NOT NULL DEFAULT 'kan_sendes'"],
                '-i' => 'Reverserer standardverdi for `status`',
                '-' => ["ALTER TABLE `{$tp}efakturaer` CHANGE `status` `status` varchar(100) NOT NULL DEFAULT ''"],
            ],
        ];

        /**
         * Versjon 0.17.4
         *
         * Øker lengden for desimalfelter
         */
        $skript['0.17.4'] = [
            [
                '+i' => 'Øker lengden for `innbetalinger`.`beløp`',
                '+' => ["ALTER TABLE `{$tp}innbetalinger` CHANGE `beløp` `beløp` decimal(10,2) NOT NULL DEFAULT '0.00'"],
                '-i' => 'Tilbakestiller lengden for `innbetalinger`.`beløp`',
                '-' => ["ALTER TABLE `{$tp}innbetalinger` CHANGE `beløp` `beløp` decimal(8,2) NOT NULL DEFAULT '0.00'"],
            ],
            [
                '+i' => 'Øker lengden for `krav`.`beløp`',
                '+' => ["ALTER TABLE `{$tp}krav` CHANGE `beløp` `beløp` decimal(10,2) NOT NULL DEFAULT '0.00'"],
                '-i' => 'Tilbakestiller lengden for `krav`.`beløp`',
                '-' => ["ALTER TABLE `{$tp}krav` CHANGE `beløp` `beløp` decimal(8,2) NOT NULL DEFAULT '0.00'"],
            ],
            [
                '+i' => 'Øker lengden for `innbetalt`.`sum`',
                '+' => ["ALTER TABLE `{$tp}innbetalt` CHANGE `sum` `sum` decimal(10,2) NOT NULL DEFAULT '0.00'"],
                '-i' => 'Tilbakestiller lengden for `innbetalinger`.`beløp`',
                '-' => ["ALTER TABLE `{$tp}innbetalt` CHANGE `sum` `sum` decimal(8,2) NOT NULL DEFAULT '0.00'"],
            ],
            [
                '+i' => 'Øker lengden for `krav`.`utestående`',
                '+' => ["ALTER TABLE `{$tp}krav` CHANGE `utestående` `utestående` decimal(10,2) NOT NULL DEFAULT '0.00'"],
                '-i' => 'Tilbakestiller lengden for `krav`.`utestående`',
                '-' => ["ALTER TABLE `{$tp}krav` CHANGE `utestående` `utestående` decimal(8,2) NOT NULL DEFAULT '0.00'"],
            ],
            [
                '+i' => 'Øker lengden for `betalingsplan_avdrag`.`beløp`',
                '+' => ["ALTER TABLE `{$tp}betalingsplan_avdrag` CHANGE `beløp` `beløp` decimal(10,2) NOT NULL DEFAULT '0.00'"],
                '-i' => 'Tilbakestiller lengden for `betalingsplan_avdrag`.`beløp`',
                '-' => ["ALTER TABLE `{$tp}betalingsplan_avdrag` CHANGE `beløp` `beløp` decimal(8,2) NOT NULL DEFAULT '0.00'"],
            ],
            [
                '+i' => 'Øker lengden for `kontrakter`.`årlig_basisleie`',
                '+' => ["ALTER TABLE `{$tp}kontrakter` CHANGE `årlig_basisleie` `årlig_basisleie` decimal(10,2) NOT NULL DEFAULT '0.00'"],
                '-i' => 'Tilbakestiller lengden for `kontrakter`.`årlig_basisleie`',
                '-' => ["ALTER TABLE `{$tp}kontrakter` CHANGE `årlig_basisleie` `årlig_basisleie` decimal(8,2) NOT NULL DEFAULT '0.00'"],
            ],
            [
                '+i' => 'Øker lengden for `leieforhold`.`årlig_basisleie`',
                '+' => ["ALTER TABLE `{$tp}leieforhold` CHANGE `årlig_basisleie` `årlig_basisleie` decimal(10,2) NOT NULL DEFAULT '0.00'"],
                '-i' => 'Tilbakestiller lengden for `leieforhold`.`årlig_basisleie`',
                '-' => ["ALTER TABLE `{$tp}leieforhold` CHANGE `årlig_basisleie` `årlig_basisleie` decimal(8,2) NOT NULL DEFAULT '0.00'"],
            ],
        ];

        /**
         * Versjon 0.18.0
         *
         * Brukerdefinerte egenskaper
         */
        $skript['0.18.0'] = [
            [
                '+i' => 'Legger til felt for `er_egendefinert` i `eav_egenskaper`',
                '+' => ["ALTER TABLE `{$tp}eav_egenskaper` ADD `er_egendefinert` tinyint(1) NOT NULL DEFAULT 0 AFTER `kode`, ADD INDEX (`er_egendefinert`)"],
                '-i' => 'Sletter felt for `er_egendefinert` for eav_egenskaper',
                '-' => ["ALTER TABLE `{$tp}eav_egenskaper` DROP COLUMN `er_egendefinert`"],
            ],
            [
                '+i' => 'Oppdaterer `er_egendefinert` i eksisterende eav-egenskaper',
                '+' => [<<<EOQ
                UPDATE `{$tp}eav_egenskaper`
                SET `er_egendefinert` = '1'
                WHERE CONCAT(`modell`, ' ', `kode`) NOT IN (
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Delkravtype indeksreguleres',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold betalingsplan_forslag',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold leie_historikk',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold leie_oppdatert',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold forfall_fast_dato',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold forfall_fast_dag_i_måneden',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold forfall_fast_ukedag',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold andre_beboere',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Betalingsplan notat_id',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Betalingsplan utenom_avtalegiro',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Betalingsplan\\\\Avdrag avtalegiro_trekk',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Framleie oppsigelsestid',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Kontrakt signert_fil',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Regning original_forfallsdato',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Regning utsettelse_forespørsel',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieobjekt\\\\Leieberegning sats_historikk',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieobjekt\\\\Leieberegning sats_oppdatert',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Person\\\\Adgang umiddelbart_betalingsvarsel_epost',
                    'Kyegil\\\\Leiebasen\\\\Modell\\\\Søknad utløp_varslet'
                )
                EOQ
                ],
            ],
            [
                '+i' => 'Legger til felter for modell og kode i `eav_verdier`',
                '+' => ["ALTER TABLE `{$tp}eav_verdier` ADD `modell` VARCHAR(255) NULL DEFAULT NULL AFTER `egenskap_id`, ADD `kode` VARCHAR(255) NULL DEFAULT NULL AFTER `modell`, ADD INDEX (`modell`), ADD INDEX (`kode`)"],
                '-i' => 'Sletter felter for modell og kode i eav_verdier',
                '-' => ["ALTER TABLE `{$tp}eav_verdier` DROP COLUMN `modell`,  DROP COLUMN `kode`"],
            ],
            [
                '+i' => 'Legger til felter for modell og kode i `eav_verdier_vc_id`',
                '+' => ["ALTER TABLE `{$tp}eav_verdier_vc_id` ADD `modell` VARCHAR(255) NULL DEFAULT NULL AFTER `egenskap_id`, ADD `kode` VARCHAR(255) NULL DEFAULT NULL AFTER `modell`, ADD INDEX (`modell`), ADD INDEX (`kode`)"],
                '-i' => 'Sletter felter for modell og kode i eav_verdier',
                '-' => ["ALTER TABLE `{$tp}eav_verdier_vc_id` DROP COLUMN `modell`,  DROP COLUMN `kode`"],
            ],
            [
                '+i' => 'Fyller modell og kode i eksisterende `eav_verdier`',
                '+' => ["UPDATE `{$tp}eav_verdier` `eav_verdier` INNER JOIN `{$tp}eav_egenskaper` `eav_egenskaper` ON `eav_verdier`.`egenskap_id` = `eav_egenskaper`.`id` SET `eav_verdier`.`modell` = `eav_egenskaper`.`modell`, `eav_verdier`.`kode` = `eav_egenskaper`.`kode` WHERE 1"],
                '-i' => 'Fjerner personverdier fra `eav_verdier`',
                '-' => ["DELETE FROM `{$tp}eav_verdier` WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Person'"],
            ],
            [
                '+i' => 'Fyller modell og kode i eksisterende `eav_verdier_vc_id`',
                '+' => ["UPDATE `{$tp}eav_verdier_vc_id` `eav_verdier` INNER JOIN `{$tp}eav_egenskaper` `eav_egenskaper` ON `eav_verdier`.`egenskap_id` = `eav_egenskaper`.`id` SET `eav_verdier`.`modell` = `eav_egenskaper`.`modell`, `eav_verdier`.`kode` = `eav_egenskaper`.`kode` WHERE 1"],
            ],
            [
                '+i' => 'Flytter personegenskaper til `eav_egenskaper`',
                '+' => ["INSERT IGNORE INTO `{$tp}eav_egenskaper` (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`) SELECT 'Kyegil\\\\Leiebasen\\\\Modell\\\\Person' AS `modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge` FROM `{$tp}personegenskaper`"],
                '-i' => 'Fjerner personegenskaper fra `eav_egenskaper`',
                '-' => ["DELETE FROM `{$tp}eav_egenskaper` WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Person'"],
            ],
            [
                '+i' => 'Flytter `personegenskaper_verdier` til `eav_verdier`',
                '+' => [<<<EOQ
                    INSERT IGNORE INTO `{$tp}eav_verdier` (`egenskap_id`, `modell`, `kode`, `objekt_id`, `verdi`)
                    SELECT DISTINCT `eav_egenskaper`.`id`, `eav_egenskaper`.`modell`, `eav_egenskaper`.`kode`, `personegenskaper_verdier`.`person_id`, `personegenskaper_verdier`.`verdi`
                    FROM `{$tp}personegenskaper_verdier` `personegenskaper_verdier`
                    INNER JOIN `{$tp}personegenskaper` `personegenskaper` ON `personegenskaper_verdier`.`egenskap_id` = `personegenskaper`.`id`
                    INNER JOIN `{$tp}eav_egenskaper` `eav_egenskaper` ON `personegenskaper`.`kode` = `eav_egenskaper`.`kode` AND `eav_egenskaper`.`modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Person'
                    EOQ
                ],
                '-i' => 'Flytter `eav_verdier` til `personegenskaper_verdier`',
                '-' => [<<<EOQ
                    INSERT IGNORE INTO `{$tp}personegenskaper_verdier` (`egenskap_id`, `person_id`, `verdi`)
                    SELECT DISTINCT `personegenskaper`.`id`, `eav_verdier`.`objekt_id`, `eav_verdier`.`verdi`
                    FROM `{$tp}eav_verdier` `eav_verdier`
                    INNER JOIN `{$tp}personegenskaper` `personegenskaper` ON `eav_verdier`.`kode` = `personegenskaper`.`kode`
                    WHERE `{$tp}eav_verdier`.`modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Person'
                    EOQ
                ],
            ],
            [
                '-i' => 'Flytter personegenskaper til `personegenskaper`',
                '-' => ["INSERT IGNORE INTO `{$tp}personegenskaper` (`kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`) SELECT `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge` FROM `{$tp}eav_egenskaper` `eav_egenskaper` WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Person'"],
            ],
            [
                '+i' => 'Sletter tabellen `personegenskaper_verdier`',
                '+' => ["DROP TABLE `{$tp}personegenskaper_verdier`"],
                '-i' => 'Gjenoppretter tabellen personegenskaper_verdier',
                '-' => ["CREATE TABLE IF NOT EXISTS `{$tp}personegenskaper_verdier` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`egenskap_id` int(11) NOT NULL,`person_id` int(11) NOT NULL,`verdi` text,PRIMARY KEY (`id`),KEY `egenskap_id` (`egenskap_id`),KEY `person_id` (`person_id`), KEY `verdi`(`verdi`(127))) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"],
            ],
            [
                '+i' => 'Sletter tabellen `personegenskaper`',
                '+' => ["DROP TABLE `{$tp}personegenskaper`"],
                '-i' => 'Gjenoppretter tabellen personegenskaper',
                '-' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}personegenskaper` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,`kode` varchar(255) NOT NULL DEFAULT '',`type` varchar(255) DEFAULT NULL,`konfigurering` text,`beskrivelse` varchar(255) DEFAULT NULL,`rekkefølge` int(11) DEFAULT NULL,PRIMARY KEY (`id`),KEY `kode` (`kode`(191))) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4",
                ],
            ],

            [
                '+i' => 'Oppretter eav_egenskapen felt_type for eav-egenskaper',
                '+' => [
                    <<< EOQ
                    INSERT INTO `{$tp}eav_egenskaper`
                        (`modell`, `kode`, `er_egendefinert`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
                    SELECT
                        'Kyegil\\\\Leiebasen\\\\Modell\\\\Eav\\\\Egenskap',
                        'felt_type',
                        0,
                        'json',
                        NULL,
                        'Data-felttype for eav-egenskaper',
                        NULL
                    FROM dual
                        WHERE NOT EXISTS
                        (
                            SELECT *
                            FROM `{$tp}eav_egenskaper`
                            WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Eav\\\\Egenskap'
                            AND `kode` = 'felt_type'
                        )
                    EOQ
                ],
                '-i' => 'Sletter eav_egenskapen felt_type for eav-egenskaper',
                '-' => ["DELETE FROM `{$tp}eav_egenskaper` WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Eav\\\\Egenskap' AND `kode` = 'felt_type'"],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen visning for eav-egenskaper',
                '+' => [
                    <<< EOQ
                    INSERT INTO `{$tp}eav_egenskaper`
                        (`modell`, `kode`, `er_egendefinert`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
                    SELECT
                        'Kyegil\\\\Leiebasen\\\\Modell\\\\Eav\\\\Egenskap',
                        'visning',
                        0,
                        'json',
                        NULL,
                        'Visningskonfigurering for eav-egenskaper',
                        NULL
                    FROM dual
                        WHERE NOT EXISTS
                        (
                            SELECT *
                            FROM `{$tp}eav_egenskaper`
                            WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Eav\\\\Egenskap'
                            AND `kode` = 'visning'
                        )
                    EOQ
                ],
                '-i' => 'Sletter eav_egenskapen visning for eav-egenskaper',
                '-' => ["DELETE FROM `{$tp}eav_egenskaper` WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Eav\\\\Egenskap' AND `kode` = 'visning'"],
            ],
        ];

        /**
         * Versjon 0.19.0
         *
         * Depositum
         */
        $skript['0.19.0'] = [
            [
                '+i' => 'Oppretter tabellen depositum',
                '+' => [
                    <<<EOQ
                    CREATE TABLE IF NOT EXISTS `{$tp}depositum` (
                        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                        `leieforhold_id` int(11) unsigned DEFAULT NULL,
                        `type` varchar(200) COLLATE utf8_danish_ci NOT NULL DEFAULT 'depositum' COMMENT 'depositum eller garanti',
                        `dato` date DEFAULT NULL,
                        `beløp` decimal(10,2) NOT NULL DEFAULT '0.00',
                        `betaler` varchar(200) COLLATE utf8_danish_ci NOT NULL DEFAULT '' COMMENT 'Betaler/garantist for depositum/depositumsgaranti',
                        `notat` text COLLATE utf8_danish_ci NOT NULL COMMENT 'Evt tekst/merknader',
                        `konto` varchar(100) COLLATE utf8_danish_ci NULL DEFAULT NULL COMMENT 'Evt. depositumskonto',
                        `fil` varchar(500) COLLATE utf8_danish_ci NULL DEFAULT NULL COMMENT 'Filbane for evt garantidokument',
                        `utløpsdato` date DEFAULT NULL COMMENT 'Utløpsdato for depositumsgaranti',
                        `utløpspåminnelser` text COMMENT 'tidsstempel for utsendte påminnelser',
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `leieforhold_id` (`leieforhold_id`),
                        KEY `type` (`type`), KEY `dato` (`dato`), KEY `beløp` (`beløp`), KEY `betaler` (`betaler`), KEY `utløpsdato` (`utløpsdato`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci
                    EOQ
                ],
            ],
            [
                '+i' => 'Oppretter innstilling: depositum_utløpsvarsel_tekst',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('depositum_utløpsvarsel_tekst','Dette er en påminnelse om at din depositumsgaranti for leieforhold {leieforhold} utløper den {utløpsdato}')"],
                '-i' => 'Sletter innstilling: depositum_utløpsvarsel_tekst',
                '-' => ["DELETE FROM `{$tp}valg` WHERE `innstilling` = 'depositum_utløpsvarsel_tekst'"],
            ],
            [
                '+i' => 'Oppretter innstilling: depositum_utløpsvarsel_tid',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('depositum_utløpsvarsel_tid','P3M')"],
                '-i' => 'Sletter innstilling: depositum_utløpsvarsel_tid',
                '-' => ["DELETE FROM `{$tp}valg` WHERE `innstilling` = 'depositum_utløpsvarsel_tid'"],
            ],
        ];

        /**
         * Versjon 0.20.0
         *
         * Mulighet til å ignorere ocr-transaksjoner
         */
        $skript['0.20.0'] = [
            [
                '+i' => 'Endrer tabellnavn fra `OCRdetaljer` til `ocr_transaksjoner`',
                '+' => ["RENAME TABLE `{$tp}OCRdetaljer` TO `{$tp}ocr_transaksjoner`"],
                '-i' => 'Endrer tabellnavn tilbake fra `ocr_transaksjoner` til `OCRdetaljer`',
                '-' => ["RENAME TABLE `{$tp}ocr_transaksjoner` TO `{$tp}OCRdetaljer`"],
            ],
            [
                '+i' => 'Endrer `filID` til `fil_id` i ocr_transaksjoner',
                '+' => ["ALTER TABLE `{$tp}ocr_transaksjoner` CHANGE `filID` `fil_id` int(11) DEFAULT NULL, ADD INDEX(`fil_id`)"],
                '-i' => 'Endrer `fil_id` tilbake til `filID` i ocr_transaksjoner',
                '-' => ["ALTER TABLE `{$tp}ocr_transaksjoner` CHANGE `fil_id` `filID` int(11) DEFAULT NULL, ADD INDEX(`filID`)"],
            ],
            [
                '+i' => 'Endrer `filID` til `fil_id` i ocr_filer',
                '+' => ["ALTER TABLE `{$tp}ocr_filer` CHANGE `filID` `fil_id` int(11) NOT NULL AUTO_INCREMENT"],
                '-i' => 'Endrer `fil_id` tilbake til `filID` i ocr_filer',
                '-' => ["ALTER TABLE `{$tp}ocr_filer` CHANGE `fil_id` `filID` int(11) NOT NULL AUTO_INCREMENT"],
            ],
            [
                '+i' => 'Endrer `OCR` til `ocr` i ocr_filer',
                '+' => ["ALTER TABLE `{$tp}ocr_filer` CHANGE `OCR` `ocr` text COLLATE utf8_danish_ci NOT NULL"],
                '-i' => 'Endrer `ocr` tilbake til `OCR` i ocr_filer',
                '-' => ["ALTER TABLE `{$tp}ocr_filer` CHANGE `ocr` `OCR` text COLLATE utf8_danish_ci NOT NULL"],
            ],
            [
                '+i' => 'Endrer `OCRtransaksjon` til `ocr_transaksjon` i innbetalinger',
                '+' => ["ALTER TABLE `{$tp}innbetalinger` CHANGE `OCRtransaksjon` `ocr_transaksjon` int(11) DEFAULT NULL, ADD INDEX(`ocr_transaksjon`)"],
                '-i' => 'Endrer `ocr_transaksjon` tilbake til `OCRtransaksjon` i innbetalinger',
                '-' => ["ALTER TABLE `{$tp}innbetalinger` CHANGE `ocr_transaksjon` `OCRtransaksjon` int(11) DEFAULT NULL, ADD INDEX(`OCRtransaksjon`)"],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen er_fremmed_beløp for innbetaling-delbeløp',
                '+' => [
                    <<< EOQ
                    INSERT INTO `{$tp}eav_egenskaper`
                        (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
                    SELECT
                        'Kyegil\\\\Leiebasen\\\\Modell\\\\Innbetaling\\\\Delbeløp',
                        'er_fremmed_beløp',
                        'boolean',
                        NULL,
                        'Indikerer at dette er et fremmedbeløp, som skal ignoreres av leiebasen',
                        NULL
                    FROM dual
                        WHERE NOT EXISTS
                        (
                            SELECT *
                            FROM `{$tp}eav_egenskaper`
                            WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Innbetaling\\\\Delbeløp'
                            AND `kode` = 'er_fremmed_beløp'
                        )
                    EOQ
                ],
                '-i' => 'Sletter eav_egenskapen er_fremmed_beløp for innbetaling-delbeløp',
                '-' => ["DELETE FROM `{$tp}eav_egenskaper` WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Innbetaling\\\\Delbeløp' AND `kode` = 'er_fremmed_beløp'"],
            ],
        ];

        /**
         * Versjon 0.21.0
         *
         * UX-forbedringer i betalingsplaner
         */
        /** @noinspection SqlWithoutWhere */
        $skript['0.21.0'] = [
            [
                '+i' => 'Legger til feltet `utestående_grunnlag` i betalingsplan_originalkrav',
                '+' => ["ALTER TABLE `{$tp}betalingsplan_originalkrav` ADD `utestående_grunnlag` decimal(10,2) NOT NULL DEFAULT '0.00' AFTER `betalingsplan_id`, ADD INDEX (`utestående_grunnlag`)"],
                '-i' => 'Sletter felt for `utestående_grunnlag` i betalingsplan_originalkrav',
                '-' => ["ALTER TABLE `{$tp}betalingsplan_originalkrav` DROP COLUMN `utestående_grunnlag`"],
            ],
            [
                '+i' => 'Beregner eksisterende verdier for `utestående_grunnlag`',
                '+' => [<<<EOQ
                    UPDATE `{$tp}betalingsplan_originalkrav`
                    SET `utestående_grunnlag` = (
                        SELECT `k`.`utestående` + IFNULL(SUM(`i`.`beløp`),0) AS `utestående_grunnlag`
                    
                        FROM `{$tp}betalingsplan_originalkrav` `bo`
                        INNER JOIN `{$tp}betalingsplaner` `b` ON `b`.`id` = `bo`.`betalingsplan_id`
                        INNER JOIN `{$tp}krav` `k` ON `k`.`id` = `bo`.`krav_id`
                        LEFT JOIN `{$tp}innbetalinger` `i` ON `bo`.`krav_id` = `i`.`krav` AND `i`.`dato` >= `b`.`dato`
                        WHERE `bo`.`id` = `betalingsplan_originalkrav`.`id`
                    )
                    EOQ
                ],
            ],
        ];

        /**
         * Versjon 0.22.0
         *
         * Betalingsplaner følger husleie
         */
        /** @noinspection SqlWithoutWhere */
        $skript['0.22.0'] = [
            [
                '+i' => 'Legger til feltet `følger_leiebetalinger` i betalingsplaner',
                '+' => ["ALTER TABLE `{$tp}betalingsplaner` ADD `følger_leiebetalinger` tinyint(1) NOT NULL DEFAULT '0' AFTER `avdragsbeløp`, ADD INDEX (`følger_leiebetalinger`)"],
                '-i' => 'Sletter felt for `følger_leiebetalinger` i betalingsplaner',
                '-' => ["ALTER TABLE `{$tp}betalingsplaner` DROP COLUMN `følger_leiebetalinger`"],
            ],
        ];

        /**
         * Versjon 0.23.0
         *
         * Lagt til kode, navn og beskrivelse til leieforhold
         */
        /** @noinspection SqlWithoutWhere */
        $skript['0.23.0'] = [
            [
                '+i' => 'Legger til feltet `kode` i leieforhold',
                '+' => ["ALTER TABLE `{$tp}leieforhold` ADD `kode` varchar(127) NULL DEFAULT NULL COMMENT 'Mulig egendefinert identifikator' AFTER `leieforholdnr`, ADD UNIQUE INDEX (`kode`)"],
                '-i' => 'Sletter felt for `kode` i leieforhold',
                '-' => ["ALTER TABLE `{$tp}leieforhold` DROP COLUMN `kode`"],
            ],
            [
                '+i' => 'Legger til feltet `navn` i leieforhold',
                '+' => ["ALTER TABLE `{$tp}leieforhold` ADD `navn` varchar(255) NOT NULL DEFAULT '' COMMENT 'Mellomlager for leietakernes navn' AFTER `leieobjekt`, ADD INDEX (`navn`)"],
                '-i' => 'Sletter felt for `navn` i leieforhold',
                '-' => ["ALTER TABLE `{$tp}leieforhold` DROP COLUMN `navn`"],
            ],
            [
                '+i' => 'Legger til feltet `beskrivelse` i leieforhold',
                '+' => ["ALTER TABLE `{$tp}leieforhold` ADD `beskrivelse` varchar(500) NOT NULL DEFAULT '' COMMENT 'Mellomlager for beskrivelse av leietakere og leieobjekt' AFTER `navn`, ADD INDEX (`beskrivelse`)"],
                '-i' => 'Sletter felt for `beskrivelse` i leieforhold',
                '-' => ["ALTER TABLE `{$tp}leieforhold` DROP COLUMN `beskrivelse`"],
            ],
            [
                '+i' => 'Fyller navn og beskrivelse i eksisterende leieforhold',
                '+' => [
                    <<< EOQ
                    UPDATE
                        `{$tp}leieforhold` AS `leieforhold`
                        LEFT JOIN `{$tp}leieobjekter` AS `leieobjekter` ON `leieforhold`.`leieobjekt` = `leieobjekter`.`leieobjektnr`
                    
                    SET `leieforhold`.`navn` = (  SELECT
                      IF(COUNT(DISTINCT `personer`.`personid`) > 2, CONCAT(LEFT(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', '), CHAR_LENGTH(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL,`kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`,`personer`.`etternavn`,CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', ')) - INSTR(REVERSE(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', ')), ' ,') - 1), ' og ', SUBSTRING(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', '), CHAR_LENGTH(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', ')) - INSTR(REVERSE(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', ')), ' ,') + 2)),   GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ' og ')) AS `navn`
                    
                      FROM `{$tp}kontraktpersoner` AS `kontraktpersoner`
                      LEFT JOIN `{$tp}personer` AS `personer` ON `personer`.`personid` = `kontraktpersoner`.`person`
                      WHERE `kontraktpersoner`.`slettet` IS NULL
                      AND `kontraktpersoner`.`leieforhold` = `leieforhold`.`leieforholdnr`
                      ORDER BY `kontraktpersoner`.`kopling`
                    ),
                    
                    `leieforhold`.`beskrivelse` = CONCAT((  SELECT
                          IF(COUNT(DISTINCT `personer`.`personid`) > 2, CONCAT(LEFT(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', '), CHAR_LENGTH(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL,`kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`,`personer`.`etternavn`,CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', ')) - INSTR(REVERSE(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', ')), ' ,') - 1), ' og ', SUBSTRING(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', '), CHAR_LENGTH(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', ')) - INSTR(REVERSE(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', ')), ' ,') + 2)),   GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ' og ')) AS `navn`
                        
                          FROM `{$tp}kontraktpersoner` AS `kontraktpersoner`
                          LEFT JOIN `{$tp}personer` AS `personer` ON `personer`.`personid` = `kontraktpersoner`.`person`
                          WHERE `kontraktpersoner`.`slettet` IS NULL
                          AND `kontraktpersoner`.`leieforhold` = `leieforhold`.`leieforholdnr`
                          ORDER BY `kontraktpersoner`.`kopling`
                        ),
                        ' i ',
                        IF(`leieobjekter`.`navn` != '', CONCAT(`leieobjekter`.`navn`, ' '), ''),
                        CASE `leieobjekter`.`etg`
                          WHEN '+' THEN 'loft '
                          WHEN '-' THEN 'kjeller '
                          WHEN '0' THEN 'sokkel '
                          WHEN '' THEN ''
                          ELSE CONCAT(`leieobjekter`.`etg`, '. etg. ')
                        END,
                        IF(`leieobjekter`.`beskrivelse` != '', CONCAT(`leieobjekter`.`beskrivelse`, ' '), ''),
                        `leieobjekter`.`gateadresse`
                    )

                    WHERE 1
                    EOQ
                ],
            ],
        ];

        /**
         * Versjon 0.24.0
         *
         * Endrer epostlager til melding-kø
         */
        $skript['0.24.0'] = [
            [
                '+i' => 'Endrer tabellen `epostlager` til `melding_kø`',
                '+' => ["ALTER TABLE `{$tp}epostlager` RENAME TO `{$tp}melding_kø`, ADD `medium` varchar(100) NOT NULL DEFAULT 'epost' AFTER `id`, ADD INDEX (`medium`)"],
                '-i' => 'Sletter felt for `følger_leiebetalinger` i betalingsplaner',
                '-' => ["ALTER TABLE `{$tp}melding_kø` RENAME TO `{$tp}epostlager`, DROP COLUMN `medium`"],
            ],
            [
                '+i' => 'Oppretter innstillingen sms_transport',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('sms_transport','')"],
            ],
            [
                '+i' => 'Oppretter innstillingen sms_avsender',
                '+' => ["INSERT IGNORE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES	('sms_avsendere','')"],
            ],
        ];

        /**
         * Versjon 0.25.0
         *
         * CMS-sider
         */
        $skript['0.25.0'] = [
            [
                '+i' => 'Oppretter tabell for cms-sider',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}cms_sider` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `område` varchar(127) DEFAULT NULL COMMENT 'Område for siden', `kode` varchar(255) DEFAULT '' COMMENT 'Kode', `tittel` varchar(255) DEFAULT '' COMMENT 'Tittel', `html` text DEFAULT '' COMMENT 'HTML-innhold', `css` text DEFAULT '' COMMENT 'Css-styling', `script` text DEFAULT '' COMMENT 'JavaScript', `konfigurering` text, PRIMARY KEY (`id`),UNIQUE KEY `url_bane` (`område`, `kode`), KEY `tittel` (`tittel`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
            ],
        ];

        /**
         * Versjon 0.26.0
         *
         * Konsoliderer lagrede andel-formater
         */
        /** @noinspection SqlWithoutWhere */
        $skript['0.26.0'] = [
            [
                '+i' => 'Konsoliderer lagrede andel-formater for leieforhold ved å fjerne prosent-symboler',
                '+' => ["UPDATE `{$tp}leieforhold` SET `andel` = REPLACE(`andel`, '%', '/100')"],
            ],
            [
                '+i' => 'Konsoliderer lagrede andel-formater for kontrakter ved å fjerne prosent-symboler',
                '+' => ["UPDATE `{$tp}kontrakter` SET `andel` = REPLACE(`andel`, '%', '/100')"],
            ],
        ];

        /**
         * Versjon 0.27.0
         *
         * Egen tabell for andel-historikk
         */
        /** @noinspection SqlWithoutWhere */
        /** @noinspection SqlIdentifier */
        $skript['0.27.0'] = [
            [
                '+i' => 'Korrigerer data-typen for EAV-egenskapen forfall_fast_dag_i_måneden',
                '+' => ["UPDATE `{$tp}eav_egenskaper` SET `type` = 'string' WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold' AND `kode` = 'forfall_fast_dag_i_måneden'"],
            ],
            [
                '+i' => 'Dropper duplikate indekser i adganger',
                '+' => ["ALTER TABLE `{$tp}adganger` DROP INDEX IF EXISTS `kontraktnr`"],
            ],
            [
                '+i' => 'Dropper duplikate indekser i kostnadsdeling_fordelingsnøkkel',
                '+' => ["ALTER TABLE `{$tp}kostnadsdeling_fordelingsnøkler` DROP KEY IF EXISTS `anleggsnummer`, ADD KEY IF NOT EXISTS `anleggsnummer` (`anleggsnummer`)"],
            ],
            [
                '+i' => 'Dropper duplikate indekser i leieforhold_delkrav',
                '+' => ["ALTER TABLE `{$tp}leieforhold_delkrav` DROP KEY IF EXISTS `delkravtype`, ADD KEY IF NOT EXISTS `delkravtype` (`delkravtype`), ADD KEY IF NOT EXISTS `leieforhold` (`leieforhold`)"],
            ],
            [
                '+i' => 'Dropper duplikate indekser i leieobjekter',
                '+' => ["ALTER TABLE `{$tp}leieobjekter` DROP KEY IF EXISTS `gateadresse`, ADD KEY IF NOT EXISTS `gateadresse` (`gateadresse`), ADD KEY IF NOT EXISTS `areal` (`areal`)"],
            ],
            [
                '+i' => 'Dropper duplikate indekser i ocr_transaksjoner',
                '+' => ["ALTER TABLE `{$tp}ocr_transaksjoner` DROP KEY IF EXISTS `filID`"],
            ],
            [
                '+i' => 'Dropper duplikate indekser i utdelingsorden',
                '+' => ["ALTER TABLE `{$tp}utdelingsorden` DROP KEY IF EXISTS `leieobjekt`, DROP KEY IF EXISTS `plassering_2`, ADD KEY IF NOT EXISTS `leieobjekt` (`leieobjekt`)"],
            ],
            [
                '+i' => 'Korrigerer indekser i innbetalinger',
                '+' => ["ALTER TABLE `{$tp}innbetalinger` DROP KEY IF EXISTS `betaler_2`, ADD KEY IF NOT EXISTS `ref` (`ref`)"],
            ],
            [
                '+i' => 'Oppretter eav-egenskapen annullering for krav',
                '+' => [
                    <<< EOQ
                    INSERT INTO `{$tp}eav_egenskaper`
                        (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
                    SELECT
                        'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Krav',
                        'annullering',
                        'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Krav',
                        NULL,
                        'Annullering (kreditering) av kravet',
                        NULL
                    FROM dual
                        WHERE NOT EXISTS
                        (
                            SELECT *
                            FROM `{$tp}eav_egenskaper`
                            WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Krav'
                            AND `kode` = 'annullering'
                        )
                    EOQ
                ],
                '-i' => 'Sletter eav_egenskapen annullering for krav',
                '-' => ["DELETE FROM `{$tp}eav_egenskaper` WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Krav' AND `kode` = 'annullering'"],
            ],
            [
                '+i' => 'Fyller `annulering` for eksisterende krav',
                '+' => [<<<EOQ
                    DROP TEMPORARY TABLE IF EXISTS `krediterte_krav`
                    EOQ,
                    <<<EOQ
                    CREATE TEMPORARY TABLE `krediterte_krav`
                    SELECT
                        `krav`.`id`,
                        `krav`.`leieforhold`,
                        `krav`.`tekst`,
                        `krav`.`beløp`,
                        MIN(`innbetalinger`.`ref`) AS `ref`
                    FROM `{$tp}krav` AS `krav`
                    INNER JOIN `{$tp}innbetalinger` AS `innbetalinger` ON `krav`.`id` = `innbetalinger`.`krav`
                    WHERE `krav`.`beløp` >= 0
                    AND `innbetalinger`.`konto_id` = 0
                    GROUP BY `krav`.`id`
                    HAVING COUNT(`innbetalinger`.`innbetalingsid`) = 1 AND SUM(`innbetalinger`.`beløp`) = `krav`.`beløp`
                    ORDER BY `krav`.`kravdato`, `krav`.`id`
                    EOQ,
                    <<<EOQ
                    INSERT INTO `{$tp}eav_verdier` (`egenskap_id`, `modell`, `kode`, `objekt_id`, `verdi`)
                    SELECT
                        (
                            SELECT MIN(`id`)
                            FROM `{$tp}eav_egenskaper`
                            WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Krav'
                            AND `kode` = 'annullering'
                        ) AS `egenskap_id`,
                        'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Krav' AS `modell`,
                        'annullering' AS `kode`,
                        `krediterte_krav`.`id` AS `objekt_id`,
                        MIN(`innbetalinger`.`krav`) AS `verdi`
                    FROM `krediterte_krav` INNER JOIN `{$tp}innbetalinger` AS `innbetalinger`
                        ON `krediterte_krav`.`leieforhold` = `innbetalinger`.`leieforhold`
                        AND CONCAT('Kreditering av ', `krediterte_krav`.`tekst`) = `innbetalinger`.`merknad`
                        AND (`krediterte_krav`.`beløp` + `innbetalinger`.`beløp`) = 0
                    WHERE `innbetalinger`.`konto_id` = 0
                        AND `innbetalinger`.`beløp` < 0
                    GROUP BY `krediterte_krav`.`id`
                    EOQ,
                ],
            ],
            [
                '+i' => 'Oppretter tabell for andelsperioder',
                '+' => [
                    "CREATE TABLE IF NOT EXISTS `{$tp}andelsperioder` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `leieobjekt` int(11) unsigned DEFAULT NULL, `leieforhold` int(11) unsigned DEFAULT NULL, `andel` varchar(16) NOT NULL DEFAULT '1/1', `fradato` date DEFAULT NULL, `tildato` date DEFAULT NULL, PRIMARY KEY (`id`), KEY `leieobjekt` (`leieobjekt`), KEY `leieforhold` (`leieforhold`), KEY `andel` (`andel`), KEY `fradato` (`fradato`), KEY `tildato` (`tildato`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4"
                ],
                '-i' => 'Sletter tabellen `andelsperioder`',
                '-' => ["DROP TABLE IF EXISTS `{$tp}andelsperioder`"],
            ],
            [
                '+i' => 'Fyller `andelsperioder` med eksisterende andeler',
                '+' => [<<<EOQ
                    INSERT INTO `{$tp}andelsperioder` (`leieobjekt`, `leieforhold`, `andel`, `fradato`)
                    SELECT
                    `kontrakter`.`leieobjekt`,
                    `kontrakter`.`leieforhold`,
                    CONCAT(`kontrakter`.`andel`, IF(LOCATE('/',`kontrakter`.`andel`) = 0, '/1', '')) AS `andel`,
                    MIN(`kontrakter`.`fradato`) AS `fradato`
                    
                    FROM `{$tp}kontrakter` AS `kontrakter`
                    GROUP BY 
                    `kontrakter`.`leieobjekt`,
                    `kontrakter`.`leieforhold`,
                    SUBSTRING_INDEX(CONCAT(`kontrakter`.`andel`, IF(LOCATE('/',`kontrakter`.`andel`) = 0, '/1', '')), '/', 1) /
                    SUBSTRING_INDEX(CONCAT(`kontrakter`.`andel`, IF(LOCATE('/',`kontrakter`.`andel`) = 0, '/1', '')), '/', -1)
                    ORDER BY `kontrakter`.`leieforhold`, `fradato`
                    EOQ,
                    <<<EOQ
                    UPDATE `{$tp}andelsperioder` AS `andelsperioder` INNER JOIN `{$tp}oppsigelser` AS `oppsigelser` ON `andelsperioder`.`leieforhold` = `oppsigelser`.`leieforhold`
                    SET `andelsperioder`.`tildato` = `oppsigelser`.`fristillelsesdato` - INTERVAL 1 DAY
                    EOQ,
                    <<<EOQ
                    DROP TEMPORARY TABLE IF EXISTS `tidslinje_TEMP`
                    EOQ,
                    <<<EOQ
                    CREATE TEMPORARY TABLE `tidslinje_TEMP`
                    SELECT *,
                    LEAD(`leieforhold`, 1) OVER (ORDER BY `leieforhold`, `fradato`, `id`) AS `neste_leieforhold`,
                    LEAD(`fradato`, 1) OVER (ORDER BY `leieforhold`, `fradato`, `id`) AS `neste_fradato`
                    FROM `{$tp}andelsperioder` AS `andelsperioder`
                    ORDER BY `leieforhold`, `fradato`, `id`
                    EOQ,
                    <<<EOQ
                    UPDATE `{$tp}andelsperioder` AS `andelsperioder` INNER JOIN `tidslinje_TEMP` ON `andelsperioder`.`id` = `tidslinje_TEMP`.`id`
                    SET `andelsperioder`.`tildato` = `tidslinje_TEMP`.`neste_fradato` - INTERVAL 1 DAY
                    WHERE `andelsperioder`.`leieforhold` = `tidslinje_TEMP`.`neste_leieforhold`
                    EOQ,
                    <<<EOQ
                    DELETE `andelsperioder`.* FROM `{$tp}andelsperioder` AS `andelsperioder` WHERE `tildato` < `fradato`
                    EOQ,
                ],
            ],
        ];

        /**
         * Versjon 0.28.0
         *
         * Kontra-transaksjoner for refunderte betalinger
         */
        /** @noinspection SqlWithoutWhere */
        /** @noinspection SqlIdentifier */
        $skript['0.28.0'] = [
            [
                '+i' => 'Korrigerer null-verdier for OCR-transaksjon i innbetalinger',
                '+' => ["UPDATE `{$tp}innbetalinger` SET `ocr_transaksjon` = NULL WHERE `ocr_transaksjon` = 0"],
            ],
            [
                '+i' => 'Korrigerer konfigurasjon for eav-egenskapen annullering for krav',
                '+' => [
                    <<<EOQ
                    UPDATE `{$tp}eav_egenskaper`
                    SET `konfigurering` = '{"rawDataContainer":"annullering"}'
                    WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Leieforhold\\\\Krav'
                      AND `kode` = 'annullering'
                    EOQ],
            ],
            [
                '+i' => 'Oppretter eav_egenskapen tilbakeføring for delbeløp',
                '+' => [
                    <<< EOQ
                    INSERT INTO `{$tp}eav_egenskaper`
                        (`modell`, `kode`, `type`, `konfigurering`, `beskrivelse`, `rekkefølge`)
                    SELECT
                        'Kyegil\\\\Leiebasen\\\\Modell\\\\Innbetaling\\\\Delbeløp',
                        'tilbakeføring',
                        'Kyegil\\\\Leiebasen\\\\Modell\\\\Innbetaling\\\\Delbeløp',
                        '{\"rawDataContainer\":\"tilbakeføring\"}',
                        'Tilbakeføring (utbetaling) av innbetalt beløp',
                        NULL
                    FROM dual
                        WHERE NOT EXISTS
                        (
                            SELECT *
                            FROM `{$tp}eav_egenskaper`
                            WHERE `modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Innbetaling\\\\Delbeløp'
                            AND `kode` = 'tilbakeføring'
                        )
                    EOQ
                ],
                '-i' => 'Sletter eav_egenskapen tilbakeføring for delbeløp',
                '-' => [<<<EOQ
                    DELETE `eav_egenskaper`.*, `eav_verdier`.*
                        FROM `{$tp}eav_egenskaper` AS `eav_egenskaper`
                            INNER JOIN `{$tp}eav_verdier` AS `eav_verdier` ON `eav_egenskaper`.`id` = `eav_verdier`.`egenskap_id`
                        WHERE `eav_egenskaper`.`modell` = 'Kyegil\\\\Leiebasen\\\\Modell\\\\Innbetaling\\\\Delbeløp'
                          AND `eav_egenskaper`.`kode` = 'tilbakeføring'
                    EOQ],
            ],
        ];

        return $skript;
    }
}