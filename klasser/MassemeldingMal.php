<?php

namespace Kyegil\Leiebasen;

use DateInterval;
use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Fbo;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav\LeieforholdDelkravtype;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Modell\Leieforhold\Oppsigelse;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Adressefelt as LeieforholdAdressefelt;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Leietakerfelt;
use Kyegil\Leiebasen\Visning\felles\html\leieobjekt\Adressefelt as LeieobjektAdressefelt;
use Kyegil\ViewRenderer\ViewInterface;
use stdClass;

class MassemeldingMal
{
    /** @var Leieforhold */
    private $leieforhold;

    /** @var stdClass */
    private $lokaleSpesifikasjoner;

    /**
     * @param Leieforhold $leieforhold
     */
    public function __construct(Leieforhold $leieforhold)
    {
        $this->leieforhold = $leieforhold;
    }

    /**
     * Fyller en maltekst med innhold fra leieforholdet
     *
     * @param string $mal Tekststreng med variabel-plassholdere
     * @param stdClass|array $spesifikasjoner Object med spesifikasjoner for bruk i tekstmalen,
     * f.eks en bestemt leietaker eller en kontrakt dersom leieforholdet inneholder flere.
     * @return string Tekststrengen fylt med verdier fra leieforholdet
     * @throws Exception
     */
    public function fyllMal(string $mal = "", $spesifikasjoner = []): string
    {
        $this->lokaleSpesifikasjoner = (object)$spesifikasjoner;
        $resultat = $mal;

        /**
         * loop gjennom alle kontrakter
         */
        $resultat = $this->loopKontrakter($resultat);

        /**
         * loop gjennom alle delkrav
         */
        $resultat = $this->loopDelkrav($resultat);

        /**
         * loop gjennom alle tillegg
         */
        $resultat = $this->loopTillegg($resultat);

        /**
         * loop gjennom alle leietakere
         */
        $resultat = $this->loopLeietakere($resultat);

        /**
         * Fyll inn data fra innstillingene
         */
        $resultat = $this->fyllInnstillinger($resultat);

        /**
         * Fyll inn data fra leieforholdet
         */
        $resultat = $this->fyllLeieforhold($resultat);

        /**
         * Fyll inn data fra leieobjektet
         */
        $resultat = $this->fyllLeieobjekt($resultat);

        /**
         * Fyll inn data fra en kontrakt
         */
        $resultat = $this->fyllKontrakt($resultat);

        /**
         * Fyll inn data fra en leietaker
         */
        $resultat = $this->fyllLeietaker($resultat);

        /**
         * Fyll inn data fra et delkrav eller et tillegg
         */
        $resultat = $this->fyllDelkrav($resultat);

        /**
         * Fyll inn data fra oppsigelse, efaktura-avtale eller faste betalingsoppdrag
         */
        return $this->fyllAnnet($resultat);
    }

    /**
     * Loop gjennom alle kontrakter
     *
     * @param string $resultat
     * @return array|string|string[]|null
     * @throws Exception
     */
    protected function loopKontrakter(string $resultat)
    {
        return preg_replace_callback(
            '/{for hver kontrakt}(.+?){\/for hver kontrakt}/s',
            function ($treff) {
                $mal = $treff[1];
                $resultat = "";
                /** @var Kontrakt $kontrakt */
                foreach ($this->leieforhold->hentKontrakter() as $kontrakt) {
                    $resultat .= $this->fyllMal($mal, ['kontrakt' => $kontrakt]);
                }
                return $resultat;
            },
            $resultat
        );
    }

    /**
     * Loop gjennom alle delkrav
     *
     * @param $resultat
     * @return array|string|string[]|null
     * @throws Exception
     */
    protected function loopDelkrav($resultat)
    {
        return preg_replace_callback(
            '/{for hvert delkrav}(.+?){\/for hvert delkrav}/s',
            function ($treff) {
                $mal = $treff[1];
                $resultat = "";
                /** @var LeieforholdDelkravtype $delkrav */
                foreach ($this->leieforhold->hentDelkravtyper() as $delkrav) {
                    if (!$delkrav->selvstendigTillegg) {
                        $resultat .= $this->fyllMal($mal, ['delkrav' => $delkrav]);
                    }
                }
                return $resultat;
            },
            $resultat
        );
    }

    /**
     * Loop gjennom alle tillegg
     *
     * @param $resultat
     * @return array|string|string[]|null
     * @throws Exception
     */
    protected function loopTillegg($resultat)
    {
        return preg_replace_callback(
            '/{for hvert tillegg}(.+?){\/for hvert tillegg}/s',
            function ($treff) {
                $mal = $treff[1];
                $resultat = "";
                /** @var LeieforholdDelkravtype $delkrav */
                foreach ($this->leieforhold->hentDelkravtyper() as $delkrav) {
                    if ($delkrav->selvstendigTillegg) {
                        $resultat .= $this->fyllMal($mal, ['tillegg' => $delkrav]);
                    }
                }
                return $resultat;
            },
            $resultat
        );
    }

    /**
     * Loop gjennom alle leietakere
     *
     * @param $resultat
     * @return array|string|string[]|null
     * @throws Exception
     */
    protected function loopLeietakere($resultat)
    {
        return preg_replace_callback(
            '/{for hver leietaker}(.+?){\/for hver leietaker}/s',
            function ($treff) {
                $mal = $treff[1];
                $resultat = "";
                /** @var Leietaker $leietaker */
                foreach ($this->leieforhold->hentLeietakere() as $leietaker) {
                    $resultat .= $this->fyllMal($mal, ['leietaker' => $leietaker]);
                }
                return $resultat;
            },
            $resultat
        );
    }

    /**
     * Fyll inn data fra innstillingene
     *
     * @param $resultat
     * @return array|string|string[]|null
     */
    protected function fyllInnstillinger($resultat)
    {
        return preg_replace_callback(
            '/{utleier\.(.+?)}/',
            function ($treff) {
                if ($treff[1] == 'navn') {
                    return $this->leieforhold->app->hentValg('utleier');
                } else {
                    return $this->leieforhold->app->hentValg($treff[1]);
                }
            },
            $resultat
        );
    }

    /**
     * Fyll inn data fra leieforholdet
     *
     * @param $resultat
     * @return array|string|string[]|null
     * @throws Exception
     */
    protected function fyllLeieforhold($resultat)
    {
        return preg_replace_callback(
            '/{leieforhold\.(.+?)}/',
            function ($treff) {
                if ($treff[1] == 'adressefelt') {
                    $resultat = $this->leieforhold->app->hentVisning(LeieforholdAdressefelt::class, ['leieforhold' => $this->leieforhold]);
                } else if ($treff[1] == 'leietakerfelt') {
                    $resultat = $this->leieforhold->app->hentVisning(Leietakerfelt::class, ['leieforhold' => $this->leieforhold]);
                } else if ($treff[1] == 'kontraktnr') {
                    $resultat = $this->leieforhold->hentKontrakt()->id;
                } else if ($treff[1] == 'andel') {
                    if ($this->leieforhold->hentAndel() && $this->leieforhold->hentAndel()->asDecimal() != 1) {
                        if ($this->leieforhold->hentAndel()->getDenominator() == 100) {
                            $resultat = (100 * $this->leieforhold->hentAndel()->asDecimal()) . '%';
                        } else {
                            $resultat = $this->leieforhold->hentAndel()->asMixedNumber();
                        }
                    } else {
                        $resultat = '';
                    }
                } else {
                    $resultat = $this->leieforhold->{'hent' . $this->leieforhold->app::ucfirst($treff[1])}();
                }
                if ($resultat instanceof DateTime) {
                    return $resultat->format('d.m.Y');
                } else if ($resultat instanceof DateInterval) {
                    return $this->leieforhold->app->periodeformat($resultat, false);
                }
                if (in_array($treff[1], ['årligBasisleie', 'leiebeløp', 'forfalt', 'utestående'])) {
                    return $this->leieforhold->app->kr($resultat);
                }
                if (
                    $resultat instanceof ViewInterface
                    || (!is_array($resultat) && !is_object($resultat))
                ) {
                    return $resultat;
                }
                return '';
            },
            $resultat
        );
    }

    /**
     * Fyll inn data fra leieobjektet
     *
     * @param $resultat
     * @return array|string|string[]|null
     * @throws Exception
     */
    protected function fyllLeieobjekt($resultat)
    {
        return preg_replace_callback(
            '/{leieobjekt\.(.+?)}/',
            function ($treff) {
                $leieobjekt = $this->leieforhold->hentLeieobjekt();
                if (!$leieobjekt) {
                    return '';
                }
                if ($treff[1] == 'adresse') {
                    $resultat = $this->leieforhold->app->hentVisning(LeieobjektAdressefelt::class, ['leieobjekt' => $leieobjekt]);
                } else {
                    $resultat = $leieobjekt->{'hent' . $this->leieforhold->app::ucfirst($treff[1])}();
                }
                if ($resultat instanceof DateTime) {
                    return $resultat->format('d.m.Y');
                } else if ($resultat instanceof DateInterval) {
                    return $this->leieforhold->app->periodeformat($resultat, false);
                }
                if (
                    $resultat instanceof ViewInterface
                    || (!is_array($resultat) && !is_object($resultat))
                ) {
                    return $resultat;
                }
                return '';
            },
            $resultat
        );
    }

    /**
     * Fyll inn data fra kontrakten
     *
     * @param $resultat
     * @return array|string|string[]|null
     * @throws Exception
     */
    protected function fyllKontrakt($resultat)
    {
        return preg_replace_callback(
            '/{kontrakt\.(.+?)}/',
            function ($treff) {
                if (property_exists($this->lokaleSpesifikasjoner, 'kontrakt') && $this->lokaleSpesifikasjoner->kontrakt instanceof Kontrakt) {
                    /** @var Kontrakt|null $kontrakt */
                    $kontrakt = $this->lokaleSpesifikasjoner->kontrakt;
                } else {
                    /** @var Kontrakt|null $kontrakt */
                    $kontrakt = $this->leieforhold->hentKontrakt();
                }

                if (!$kontrakt) {
                    return '';
                }
                if ($treff[1] == 'avtaletekst') {
                    $resultat = $this->leieforhold->gjengiAvtaletekst(true, $kontrakt);
                } else {
                    $resultat = $kontrakt->{'hent' . $this->leieforhold->app::ucfirst($treff[1])}();
                }
                if ($resultat instanceof DateTime) {
                    return $resultat->format('d.m.Y');
                } else if ($resultat instanceof DateInterval) {
                    return $this->leieforhold->app->periodeformat($resultat, false);
                }
                if (
                    $resultat instanceof ViewInterface
                    || (!is_array($resultat) && !is_object($resultat))
                ) {
                    return $resultat;
                }
                return '';
            },
            $resultat
        );
    }

    /**
     * Fyll inn data fra en leietaker
     *
     * @param $resultat
     * @return array|string|string[]|null
     * @throws Exception
     */
    protected function fyllLeietaker($resultat)
    {
        return preg_replace_callback(
            '/{leietaker\.(.+?)}/',
            function ($treff) {
                if (property_exists($this->lokaleSpesifikasjoner, 'leietaker') && $this->lokaleSpesifikasjoner->leietaker instanceof Leietaker) {
                    /** @var Leietaker|null $leietaker */
                    $leietaker = $this->lokaleSpesifikasjoner->leietaker;
                } else {
                    /** @var Leietaker|null $leietaker */
                    $leietaker = $this->leieforhold->hentLeietakere()->hentFørste();
                }

                if (!$leietaker) {
                    return '';
                }
                if ($treff[1] == 'adresse') {
                    $resultat = $leietaker->person ? $leietaker->person->hentPostadresse() : null;
                }
                else if ($treff[1] == 'epostadresse') {
                    $resultat = $leietaker->person ? $leietaker->person->hentEpost() : null;
                }
                else if ($treff[1] == 'orgnr') {
                    $resultat = $leietaker->person ? $leietaker->person->hentOrgNr() : null;
                }
                else if (in_array($treff[1], ['fornavn', 'etternavn'])) {
                    $leietakerNavn = $leietaker->hentLeietakerNavn();
                    $leietakerNavn = explode(' ', $leietakerNavn);
                    $del['etternavn'] = array_pop($leietakerNavn);
                    $del['fornavn'] = implode(' ', $leietakerNavn);
                    $resultat = $leietaker->person ? $leietaker->person->{'hent' . $this->leieforhold->app::ucfirst($treff[1])}() : $del[$treff[1]];
                }
                else if (in_array($treff[1], ['etternavn', 'fødselsdato', 'fødselsnummer', 'telefon', 'mobil'])) {
                    $resultat = $leietaker->person ? $leietaker->person->{'hent' . $this->leieforhold->app::ucfirst($treff[1])}() : null;
                }
                else {
                    $resultat = $leietaker->{'hent' . $this->leieforhold->app::ucfirst($treff[1])}();
                }
                if ($resultat instanceof DateTime) {
                    return $resultat->format('d.m.Y');
                } else if ($resultat instanceof DateInterval) {
                    return $this->leieforhold->app->periodeformat($resultat, false);
                }
                if (
                    $resultat instanceof ViewInterface
                    || (!is_array($resultat) && !is_object($resultat))
                ) {
                    return $resultat;
                }
                return '';
            },
            $resultat
        );
    }

    /**
     * Fyll inn data fra oppsigelse, efaktura-avtale eller faste betalingsoppdrag
     *
     * @param $resultat
     * @return array|string|string[]|null
     * @throws Exception
     */
    protected function fyllAnnet($resultat)
    {
        return preg_replace_callback(
            '/{(oppsigelse|fbo)\.(.+?)}/',
            function ($treff) {
                $type = $treff[1];
                $egenskap = $treff[2];
                /** @var Oppsigelse|Fbo|null $objekt */
                $objekt = $this->leieforhold->hent($type);
                if (!$objekt) {
                    return '';
                }
                $resultat = $objekt->$egenskap;
                if ($resultat instanceof DateTime) {
                    return $resultat->format('d.m.Y');
                }
                if ($resultat instanceof DateInterval) {
                    return $this->leieforhold->app->periodeformat($resultat, false);
                }
                if (is_array($resultat) || is_object($resultat)) {
                    return '';
                }
                return $resultat;
            },
            $resultat
        );
    }

    /**
     * Fyll inn data fra et delkrav eller et tillegg
     *
     * @param $resultat
     * @return array|string|string[]|null
     * @throws Exception
     */
    protected function fyllDelkrav($resultat)
    {
        return preg_replace_callback(
            '/{(delkrav|tillegg)\.(.+?)}/',
            function ($treff) {
                $type = $treff[1];
                $egenskap = $treff[2];
                /** @var LeieforholdDelkravtype|null $objekt */
                if (property_exists($this->lokaleSpesifikasjoner, $type) && $this->lokaleSpesifikasjoner->{$type} instanceof LeieforholdDelkravtype) {
                    $objekt = $this->lokaleSpesifikasjoner->{$type};
                } else {
                    $objekt = null;
                }
                if (!$objekt) {
                    return '';
                }
                return $objekt->hent($egenskap);
            },
            $resultat
        );
    }
}