<?php

namespace Kyegil\Leiebasen;

/**
 *
 */
class UtskriftStatus implements \JsonSerializable
{
    /**
     * @var string
     */
    const STATUS_UVIRKSOM = 'uvirksom';

    /**
     * @var string
     */
    const STATUS_IGANGSATT = 'igangsatt';

    /**
     * @var string
     */
    const STATUS_PÅGÅR = 'pågår';

    /**
     * @var string
     */
    const STATUS_KOMPLETT = 'komplett';

    /**
     * @var string
     */
    const STATUS_MISLYKKET = 'mislykket';

    /**
     * @var string
     */
    const STATUS_AVBRUTT = 'avbrutt';

    /**
     * @var string
     */
    protected $sessionVariabel = 'utskriftstatus';

    /**
     * @return $this
     */
    public function initier(): UtskriftStatus
    {
        settype($_SESSION['leiebasen'], 'array');
        settype($_SESSION['leiebasen'][$this->sessionVariabel], 'array');
        settype($_SESSION['leiebasen'][$this->sessionVariabel]['status'], 'string');
        settype($_SESSION['leiebasen'][$this->sessionVariabel]['framdrift'], 'float');
        settype($_SESSION['leiebasen'][$this->sessionVariabel]['detaljer'], 'string');
        settype($_SESSION['leiebasen'][$this->sessionVariabel]['meldinger'], 'array');
        return $this;
    }

    /**
     * @return $this
     */
    public function exitier(): UtskriftStatus
    {
        unset($_SESSION['leiebasen'][$this->sessionVariabel]);
        return $this;
    }

    /**
     * @return mixed
     */
    public function jsonSerialize()
    {
        return $this->hent();
    }

    /**
     * @param string $status
     * @return $this
     */
    public function settStatus(string $status): UtskriftStatus
    {
        $this->initier();
        $_SESSION['leiebasen'][$this->sessionVariabel]['status'] = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function hentStatus():string
    {
        return $_SESSION['leiebasen'][$this->sessionVariabel]['status'] ?? self::STATUS_UVIRKSOM;
    }

    /**
     * Legg til en ny melding om forhold oppstått under prosessen
     * @param string $melding
     * @return $this
     */
    public function leggTilMelding(string $melding): UtskriftStatus
    {
        $_SESSION['leiebasen'][$this->sessionVariabel]['meldinger'][] = $melding;
        return $this;
    }

    /**
     * Hent akkumulerte meldinger
     * @return string[]
     */
    public function hentMeldinger():array
    {
        return $_SESSION['leiebasen'][$this->sessionVariabel]['meldinger'] ?? [];
    }

    /**
     * Oppdater utskriftsstatusen
     *
     * @param string $status 'uvirksom'|'igangsatt'|'pågår'|'komplett'|'mislykket'|'avbrutt'
     * @param float $framdrift framdrift angitt fra 0 til 1
     * @param string $detaljer tekst Beskrivelse av nåv'rende status
     * @return $this
     */
    public function oppdaterStatus(string $status, float $framdrift, string $detaljer): UtskriftStatus
    {
        $this->initier();
        $_SESSION['leiebasen'][$this->sessionVariabel]['status'] = $status;
        $_SESSION['leiebasen'][$this->sessionVariabel]['framdrift'] = $framdrift;
        $_SESSION['leiebasen'][$this->sessionVariabel]['detaljer'] = $detaljer;
        return $this;
    }

    /**
     * @return object
     */
    public function hent()
    {
        return (object)($_SESSION['leiebasen'][$this->sessionVariabel] ?? []);
    }
}