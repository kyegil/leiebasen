<?php
/**
 * Leiebasen
 */

namespace Kyegil\Leiebasen;

use Exception;
use JsonSerializable;
use Kyegil\CoreModel\CoreModel;
use Kyegil\CoreModel\Interfaces\AppInterface;
use Kyegil\CoreModel\Interfaces\CoreModelCollectionInterface;
use Kyegil\CoreModel\Interfaces\EavValuesInterface;
use Kyegil\Leiebasen\Modell\Eav\Egenskap as EavEgenskap;
use Kyegil\Leiebasen\Modell\Eav\Egenskapsett;
use Kyegil\Leiebasen\Modell\Eav\Verdi;
use Kyegil\Leiebasen\Modell\Eav\Verdi as EavVerdi;
use Kyegil\Leiebasen\Modell\Eav\Verdisett;
use stdClass;

/** @property CoreModelImplementering $app */

class Modell extends CoreModel implements JsonSerializable
{
    protected static $collectionModel = Sett::class;

    protected $samlinger;

    protected $leiebase;

    /**
     * @deprecated
     * @access private
     * @return array
     */
    public static function getDbFields(): array
    {
        return static::hentDbFelter();
    }

    /**
     * Multibyte stor forbokstav
     *
     * @param string $string
     * @return string
     */
    public static function ucfirst(string $string): string {
        return mb_strtoupper(mb_substr($string, 0, 1, 'utf8'), 'utf8') . mb_substr($string, 1, null, 'utf8');
    }

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     * * type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     * * allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     * * allowMultiple: boolsk. Standardverdi er false. True vil tillate flere verdier av samme type.
     *      Bør brukes med forsiktighet, for dette vil komplisere tabell-relasjoner.
     * * target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     * * toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     * * rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien
     *
     * @return array
     */
    protected static function hentDbFelter()
    {
        return parent::getDbFields();
    }

    /**
     * Modell constructor.
     *
     * @param $di
     * @param null $id
     * @throws \Kyegil\CoreModel\CoreModelDiException
     */
    public function __construct($di, $id = null)
    {
        parent::__construct($di, $id);
        $this->samlinger = $this->collections;
        $this->leiebase = $this->app;
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws Exception
     */
    public function __call($name, $arguments)
    {
        $underscore = $this->app->makeUnderscore($name);
        if(substr($underscore, 0,5) == 'hent_') {
            $attribute = substr($underscore, 5);

            list('attribute' => $attribute)
                = $this->app->before($this, $name, ['attribute' => $attribute]);
            $result = $this->hent($attribute);
        }
        elseif(substr($underscore, 0,5) == 'sett_') {
            $attribute = substr($underscore, 5);
            if(count($arguments) == 0) {
                throw new Exception($name . '() requires a value');
            }
            list('value' => $arguments)
                = $this->app->before($this, $name, ['value' => $arguments]);

            $result = $this->sett($attribute, reset($arguments));
        }
        else{
            throw new Exception('Ukjent metode ' . static::class . '::' . $name . '()');
        }
        $result = $this->app->after($this, $name, $result);
        return $result;
    }

    /**
     * @param $name
     * @return mixed
     * @throws Exception
     */
    public function __get($name)
    {
        $getter = 'hent' . self::ucfirst($name);
        $result = $this->$getter();
        return $result;
    }

    /**
     * @param $name
     * @param $value
     * @return CoreModel
     * @throws Exception
     */
    public function __set($name, $value)
    {
        $setter = 'sett' . self::ucfirst($name);
        return $this->$setter($value);
    }

    /**
     * @return string
     */
    public static function hentTabell(): string
    {
        return parent::getDbTable();
    }

    /**
     * @return string
     */
    public static function hentPrimærnøkkelfelt(): string
    {
        return parent::getDbPrimaryKeyField();
    }

    /**
     * @param string $referanse
     * @param string|null $modell
     * @param string|null $fremmedNøkkel
     * @param array $filtre
     * @param callable|null $callback
     * @return array|null
     * @throws Exception
     */
    public static function harFlere(string $referanse, ?string $modell = null, ?string $fremmedNøkkel = null, array $filtre = [], ?callable $callback = null): ?array
    {
        return parent::hasMany($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }
    public static function hasMany(string $reference, ?string $model = null, ?string $foreignKey = null, array $filters = [], ?callable $callback = null): ?array
    {
        return static::harFlere($reference, $model, $foreignKey, $filters, $callback);
    }


    /**
     * Sjekker om en egenskap har blitt forberedt i modellen
     *
     * @param string $egenskap
     * @return bool
     */
    public function har(string $egenskap): bool
    {
        return parent::hasData($egenskap);
    }

    /**
     * @param string $egenskap
     * @return mixed
     * @throws Exception
     */
    public function hent($egenskap = null)
    {
        return parent::getData($egenskap);
    }

    /**
     * Dekk over en egenskap med en verdi, for å forhindre at den reelle verdien lastes.
     *
     * Dette vil sette en kun-les-verdi på modellen.
     *
     * @param $egenskap
     * @param $verdi
     * @return $this
     */
    public function dekk($egenskap, $verdi): Modell
    {
        parent::coverData($egenskap, $verdi);
        return $this;
    }
    /**
     * @deprecated
     * @access private
     * @return stdClass
     */
    public function getEavConfigs(): stdClass
    {
        return $this->hentEavKonfigurering();
    }

    /**
     * @param string $source
     * @return stdClass|null
     * @throws Exception
     * @deprecated
     * @access private
     */
    public function getEavValueObjects(string $source): ?stdClass
    {
        return $this->hentEavVerdiObjekter($source);
    }

    /**
     * @param string $egenskap
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($egenskap, $verdi): Modell
    {
        parent::setData($egenskap, $verdi);
        return $this;
    }

    /**
     * @param string $property
     * @param $value
     * @return $this
     * @throws Exception
     * @access private
     */
    public function setData(string $property, $value): CoreModel
    {
        return $this->sett($property, $value);
    }

    /**
     * @throws Exception
     * @ignore
     */
    public function getId()
    {
        return $this->hentId();
    }

    /**
     * @return string|int
     * @noinspection PhpDocMissingThrowsInspection
     */
    public function hentId()
    {
        return parent::getId();
    }

    /**
     * @param string $id
     * @return $this
     */
    public function settId($id): Modell
    {
        return parent::setId($id);
    }

    /**
     * @return AppInterface
     */
    public function hentApp(): AppInterface
    {
        return parent::getApp();
    }

    /**
     * @deprecated
     * @access private
     * @param stdClass $params
     * @return CoreModel
     * @throws Exception
     * @see Leiebase::nyModell()
     */
    public function create(stdClass $params): CoreModel
    {
        return $this->opprett($params);
    }

    /**
     * @param stdClass $parametere angitt som underscore
     * @return $this
     * @throws Exception
     * @see Leiebase::nyModell()
     */
    public function opprett(stdClass $parametere): Modell
    {
        parent::create($parametere);
        return $this;
    }

    /**
     * @return void
     * @throws Exception
     */
    public function delete()
    {
        $this->slett();
    }

    /**
     * @return void
     * @throws Exception
     */
    public function slett()
    {
        $eavKonfigurering = $this->hentEavKonfigurering();
        foreach (array_keys(get_object_vars($eavKonfigurering)) as $kilde) {
            $eavVerdier = $this->hentEavVerdiObjekter($kilde);
            /** @var Verdi $verdi */
            foreach ($eavVerdier as $verdi) {
                $verdi->slett();
            }
        }
        parent::delete();
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    public function jsonSerialize()
    {
        return $this->getFlatData();
    }

    public function load(): CoreModel
    {
        return static::last();
    }

    /**
     * Last modellen fra databasen
     *
     * @return $this
     * @throws Exception
     */
    public function last(): Modell
    {
        parent::load();
        return $this;
    }


    /**
     * Henter alle Eav-konfigurasjoner
     *
     * Konfigurasjonene returneres i et flerdimensjonalt objekt
     * i formen:
     * kilde->kode = \Kyegil\CoreModel\Interfaces\EavValuesInterface
     *
     * @return stdClass
     * @throws Exception
     */
    public function hentEavKonfigurering(): object
    {
        if(!isset($this->eavConfigs)) {
            if(!isset(EavEgenskap::$mellomlager[static::class])) {
                /** @var Egenskapsett $egenskaper */
                $egenskaper = $this->app->hentSamling(EavEgenskap::class)
                    ->leggTilFilter(['modell' => static::class])
                    ->låsFiltre()
                    ->leggTilSortering('rekkefølge');
                $kilde = EavEgenskap::hentTabell();
                EavEgenskap::$mellomlager[static::class] = (object)[$kilde => (object)[]];
                /** @var EavEgenskap $egenskap */
                foreach ($egenskaper as $egenskap) {
                    $kode = $egenskap->hent('kode');
                    EavEgenskap::$mellomlager[static::class]->{$kilde}->{$kode} = $egenskap;
                }
            }
            $this->eavConfigs = EavEgenskap::$mellomlager[static::class];
        }
        return parent::getEavConfigs();
    }

    /**
     * Returnerer et objekt med ett Verdi-objekt tilhørende hver kode,
     * for den verdi-kilden eller tabellen som er oppgitt,
     * eller null dersom denne verdikilden ikke finnes
     *
     * @param string $kilde (f.eks tabell)
     * @return stdClass|null
     * @throws Exception
     */
    public function hentEavVerdiObjekter(string $kilde): ?stdClass
    {
        if(!$this->eavValueObjects) {
            $verdiKilde = EavEgenskap::hentTabell();
            $this->eavValueObjects = (object)[
                $verdiKilde => new stdClass()
            ];

            /** @var Verdisett $eavVerdiObjekter */
            $eavVerdiObjekter = $this->app->hentSamling(EavVerdi::class)

                ->leggTilInnerJoin(EavEgenskap::hentTabell(), '`' . EavVerdi::hentTabell() . '`.`egenskap_id` = ' . EavEgenskap::hentTabell() . '.id')
                ->leggTilFilter([
                    '`' . EavEgenskap::hentTabell() . '`.`modell`' => static::class,
                    '`' . EavVerdi::hentTabell() . '`.`objekt_id`' => $this->getId()
                ]);
            /** @var EavValuesInterface $eavVerdiObjekt */
            foreach ($eavVerdiObjekter as $eavVerdiObjekt) {
                $kode = $eavVerdiObjekt->getConfig()->getSourceField();
                $this->eavValueObjects->{$verdiKilde}->{$kode} = $eavVerdiObjekt;
            }
        }

        if(isset($this->eavValueObjects->{$kilde})) {
            return $this->eavValueObjects->{$kilde};
        }
        return null;
    }

    /**
     * @return $this
     */
    public function nullstill(): Modell
    {
        parent::reset();
        $this->samlinger = $this->collections;
        return $this;
    }

    /**
     * @return $this
     */
    public function reset(): CoreModel
    {
        return $this->nullstill();
    }

    /**
     * @param $property
     * @return string|null
     */
    public function getStringValue($property): ?string
    {
        return $this->hentStrengVerdi($property);
    }

    /**
     * @param $egenskap
     * @return string|null
     * @throws Exception
     */
    public function hentStrengVerdi($egenskap): ?string
    {
        return parent::getStringValue($egenskap);
    }

    /**
     * @param string $referanse
     * @return Samling|null
     * @throws Exception
     */
    public function hentSamling(string $referanse): ?CoreModelCollectionInterface
    {
        return $this->getCollection($referanse);
    }
}