<?php

namespace Kyegil\Leiebasen;

use DateInterval;
use DateTimeImmutable;
use DateTimeZone;
use Exception;
use Kyegil\CoreModel\CoreModel;
use Kyegil\CoreModel\CoreModelException;
use Kyegil\Leiebasen\Modell\Eav\Verdi as EavVerdi;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløpsett;
use Kyegil\Leiebasen\Modell\Innbetalingsett;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Ocr;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Personsett;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Psr\Log\LoggerInterface;
use stdClass;

class Vedlikehold
{
    const LOG_PREFIX = 'VEDLIKEHOLD – ';
    private static Vedlikehold $instance;

    protected CoreModelImplementering $app;

    private LoggerInterface $logger;

    /**
     * @param CoreModelImplementering $app
     * @return Vedlikehold
     */
    public static function getInstance(CoreModelImplementering $app): Vedlikehold
    {
        if(!isset(self::$instance)) {
            self::$instance = new self($app);
        }
        return self::$instance;
    }

    /**
     * @param CoreModelImplementering $app
     * @param array $argumenter
     * @return array
     */
    public static function cron(CoreModelImplementering $app, array $argumenter = []): array
    {
        try {
            $cronTid = $argumenter[0] ?? date_create_immutable('now', new DateTimeZone('UTC'));
            $vedlikehold = self::getInstance($app);
            $minutter = $cronTid->format('i');

            if(!$app->live || $minutter == 25) {
                try {
                    $vedlikehold->slettTommeDelbeløp();
                } catch (Exception $e) {
                    $app->logger->critical(self::LOG_PREFIX . $e->getMessage());
                }
                try {
                    $vedlikehold->kontrollerTilbakeføringer();
                } catch (Exception $e) {
                    $app->logger->critical(self::LOG_PREFIX . $e->getMessage());
                }
                try {
                    $vedlikehold->slettManglendeAvstemmingskrav();
                } catch (Exception $e) {
                    $app->logger->critical(self::LOG_PREFIX . $e->getMessage());
                }
                try {
                    $vedlikehold->kontrollerUtestående();
                } catch (Exception $e) {
                    $app->logger->critical(self::LOG_PREFIX . $e->getMessage());
                }
                try {
                    $vedlikehold->sjekkKredittbalanse();
                } catch (Exception $e) {
                    $app->logger->critical(self::LOG_PREFIX . $e->getMessage());
                }
                try {
                    $vedlikehold->sjekkOcrInnbetalinger();
                } catch (Exception $e) {
                    $app->logger->critical(self::LOG_PREFIX . $e->getMessage());
                }
                try {
                    $vedlikehold->sjekkAdresseoppdateringer();
                } catch (Exception $e) {
                    $app->logger->critical(self::LOG_PREFIX . $e->getMessage());
                }
                try {
                    $vedlikehold->sjekkLeieavtalerSomBørFornyes();
                } catch (Exception $e) {
                    $app->logger->critical(self::LOG_PREFIX . $e->getMessage());
                }
                try {
                    $vedlikehold->sjekkOppfølgingspåminnelser();
                } catch (Exception $e) {
                    $app->logger->critical(self::LOG_PREFIX . $e->getMessage());
                }
                try {
                    $vedlikehold->sjekkRegningsUtskrifter();
                } catch (Exception $e) {
                    $app->logger->critical(self::LOG_PREFIX . $e->getMessage());
                }
                try {
                    $vedlikehold->sjekkAvstemming();
                } catch (Exception $e) {
                    $app->logger->critical(self::LOG_PREFIX . $e->getMessage());
                }
            }
        } catch (Exception $e) {
            $app->logger->critical(self::LOG_PREFIX . $e->getMessage());
        }
        return $argumenter;
    }

    /**
     * @param CoreModelImplementering $app
     */
    private function __construct(CoreModelImplementering $app)
    {
        $this->app = $app;
        $this->logger = $this->app->logger;

    }

    /**
     * Kontroller betalingsutlikninger
     *
     * Sjekker om betalinger avstemt mot hverandre er i balanse
     * og søker å korrigere ubalansen.
     *
     * Delbeløpene sjekkes ett og ett, inntill alle er korrigert eller det oppstår en exception
     *
     * @return $this
     * @throws Exception
     */
    public function kontrollerTilbakeføringer(): Vedlikehold
    {
        $tp = CoreModel::$tablePrefix;
        $subQuery = <<<SQL
            SELECT COUNT(`innbetalingsid`)
            FROM `{$tp}innbetalinger` AS `match`
            WHERE `match`.`krav` = '0'
              AND `match`.`leieforhold` = `innbetalinger`.`leieforhold`
              AND `match`.`beløp` = - `innbetalinger`.`beløp`
            SQL;
        /** @var Delbeløpsett $feilITilbakeføring */
        $feilITilbakeføring = $this->app->hentSamling(Delbeløp::class)
            ->leggTilEavFelt('tilbakeføring')
            ->leggTilLeftJoin(['tilbakeføringsbeløp' => Delbeløp::hentTabell()],
                '`tilbakeføring`.`verdi` = `tilbakeføringsbeløp`.`' . Delbeløp::hentPrimærnøkkelfelt() . '`' .
                ' AND `' . Delbeløp::hentTabell() . '`.`beløp` = -`tilbakeføringsbeløp`.`beløp`'
            )
            ->leggTilUttrykk('orden', 'eksakt_beløp', "($subQuery)")
            ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`krav`' => '0'])
            ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`beløp` >=' => 0])
            ->leggTilFilter(['`tilbakeføringsbeløp`.`' . Delbeløp::hentPrimærnøkkelfelt() . '` IS NULL'])
            ->låsFiltre();
        $feilITilbakeføring->leggTilSortering('dato');
        $feilITilbakeføring->leggTilSortering('eksakt_beløp', true, 'orden');

        if($feilITilbakeføring->hentAntall()) {
            $this->logger->warning(self::LOG_PREFIX . 'Det er registrert ubalanse i ' . $feilITilbakeføring->hentAntall() . ' beløp avstemt mot utbetalinger. Beløpene stemmer ikke overens.');
            /** @var Delbeløp $delbeløp */
            $delbeløp = $feilITilbakeføring->hentFørste();
            $this->korrigerTilbakeføring($delbeløp);
        }

        return $this;
    }

    /**
     * @param Delbeløp $delbeløp
     * @return Vedlikehold
     * @throws Exception
     */
    public function korrigerTilbakeføring(Delbeløp $delbeløp): Vedlikehold
    {
        $tilbakeføring = $delbeløp->hentTilbakeføring();
        if ($tilbakeføring) {
            $this->logger->info(self::LOG_PREFIX . 'Forsøker å korrigere ubalansen i delbeløp ' . $delbeløp->hentId(), [
                'id' => $delbeløp->hentId(),
                'beløp' => $delbeløp->hentBeløp(),
                'transaksjon' => $delbeløp->innbetaling->hentId(),
                'tilbakeføring_id' => $tilbakeføring->hentId(),
                'tilbakeføring_beløp' => $tilbakeføring->hentBeløp(),
                'tilbakeføring_transaksjon' => $tilbakeføring->innbetaling->hentId(),
            ]);

            $tilbakeførtBeløp = -$tilbakeføring->hentBeløp();
            if ($tilbakeførtBeløp < $delbeløp->hentBeløp()) {
                $this->logger->info('Beløpet reduseres til kr ' . $tilbakeførtBeløp);
                $delbeløp->splitt($delbeløp->hentBeløp() - $tilbakeførtBeløp)->fjernAvstemming();
            } else if ($tilbakeførtBeløp > $delbeløp->hentBeløp()) {
                $this->logger->info('Tilbakeføringen reduseres til kr ' . -$delbeløp->hentBeløp());
                $tilbakeføring->splitt($delbeløp->hentBeløp() - $tilbakeførtBeløp)->fjernAvstemming();
            }
        } else {
            $leieforhold = $delbeløp->hentLeieforhold();
            $this->logger->info(self::LOG_PREFIX . 'Forsøker å registrere tilbakeføring av delbeløp ' . $delbeløp->hentId(), [
                'id' => $delbeløp->hentId(),
                'leieforhold' => $leieforhold ? $leieforhold->hentId() : null,
                'beløp' => $delbeløp->hentBeløp(),
                'transaksjon' => $delbeløp->innbetaling->hentId(),
            ]);
            $muligeTilbakeføringer = $this->app->hentSamling(Delbeløp::class)
                ->leggTilLeftJoin(EavVerdi::hentTabell(),
                    '`' . Delbeløp::hentTabell() . '`.`' . Delbeløp::hentPrimærnøkkelfelt() . '` = CAST(`' . EavVerdi::hentTabell() . '`.`verdi` AS UNSIGNED INTEGER)' .
                    ' AND `' . EavVerdi::hentTabell() . "`.`modell` = '" . addslashes(Delbeløp::class) . "'" .
                    ' AND `' . EavVerdi::hentTabell() . "`.`kode` = 'tilbakeføring'"
                )
                ->leggTilUttrykk('orden', 'eksakt_beløp', 'IF(`' . Delbeløp::hentTabell() . '`.`beløp` = -' . $delbeløp->hentBeløp() . ', 1, 0)')
                ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`leieforhold`' => $leieforhold ? $leieforhold->hentId() : null])
                ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`beløp` <' => 0])
                ->leggTilFilter(['`' . EavVerdi::hentTabell() . '`.`verdi` IS NULL'])
                ->låsFiltre();
            $muligeTilbakeføringer->leggTilSortering('dato');
            $muligeTilbakeføringer->leggTilSortering('eksakt_beløp', true, 'orden');
            $this->logger->info('Beløpet fordeles mot utbetalinger.',
                ['mulige_tilbakeføringer' => $muligeTilbakeføringer->hentIdNumre()]
            );
            if ($muligeTilbakeføringer->hentAntall()) {
                $delbeløp->avstem($muligeTilbakeføringer);
            } else {
                $delbeløp->fjernAvstemming();
            }
        }
        $delbeløp->nullstill();
        // Verifiser korrigeringen
        if (
            $delbeløp->hentKrav() === true
            && (
                !$delbeløp->hentTilbakeføring()
                || $delbeløp->hentBeløp() + $delbeløp->hentTilbakeføring()->hentBeløp() != 0
            )
        ) {
            throw new Exception('Mislykket korrigering av tilbakeføring av delbeløp ' . $delbeløp->hentId());
        }
        // Sjekk på nytt for å korrigere neste delbeløp
        return $this->kontrollerTilbakeføringer();
    }

    /** Slett transaksjons-delbeløp på kr 0
     * @throws CoreModelException
     * @throws Exception
     */
    public function slettTommeDelbeløp(): Vedlikehold
    {
        /** @var Delbeløpsett $delbeløpsett */
        $delbeløpsett = $this->app->hentSamling(Delbeløp::class)
            ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`beløp`' => 0])
            ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`oppdatert` < NOW()  - INTERVAL 30 MINUTE'])
            ->låsFiltre();
        $delbeløpsett->forEach(function (Delbeløp $delbeløp) {
            $this->logger->info(self::LOG_PREFIX . 'Sletter delbeløp på kr 0.', ['transaksjon' => $delbeløp->innbetaling->hentId()]);
            $delbeløp->slett();
        });
        return $this;
    }

    /**
     * @return $this
     * @throws CoreModelException
     * @throws Exception
     */
    public function slettManglendeAvstemmingskrav(): Vedlikehold
    {
        /** @var Delbeløpsett $manglendeKrav */
        $manglendeKrav = $this->app->hentSamling(Delbeløp::class)
            ->leggTilLeftJoin(Krav::hentTabell(),
                '`' . Delbeløp::hentTabell() . '`.`krav` = `' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '`')
            ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`krav` IS NOT NULL'])
            ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`krav` !=' => 0])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '` IS NULL'])
            ->låsFiltre();
        $manglendeKrav->forEach(function (Delbeløp $delbeløp) {
            $this->logger->info(self::LOG_PREFIX . 'Fjerner ikke-eksisterende krav fra avstemming.', [
                'delbeløp' => $delbeløp->hentId(),
                'krav' => $delbeløp->getRawData()->{Delbeløp::hentTabell()}->krav ?? null,
            ]);
            $delbeløp->fjernAvstemming();
        });
        return $this;
    }

    /**
     * @return Vedlikehold
     * @throws CoreModelException
     * @throws Exception
     */
    public function kontrollerUtestående(): Vedlikehold
    {
        /** @var Kravsett $feilsett */
        $feilsett = $this->app->hentSamling(Krav::class)
            ->leggTilLeftJoin(Delbeløp::hentTabell(),
            '`' . Delbeløp::hentTabell() . '`.`krav` = `' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '`');
        $feilsett->leggTilFilter(['`' . Krav::hentTabell() . '`.`beløp` >' => 0])
            ->leggTilHaving(['SUM(`' . Delbeløp::hentTabell() . '`.`beløp`) != (`' . Krav::hentTabell() . '`.`beløp` - `' . Krav::hentTabell() . '`.`utestående`)'], true)
            ->låsFiltre();
        if($feilsett->hentAntall()) {
            $this->logger->info(self::LOG_PREFIX . 'Det er feil i utestående beløp i ' . $feilsett->hentAntall() . ' krav.', [
                'krav' => $feilsett->hentIdNumre(),
            ]);
            $feilsett->forEach(function (Krav $krav) {
                $this->logger->info(self::LOG_PREFIX . 'Korrigerer utestående beløp for ' . $krav->hentId(), [
                    'krav' => $krav->hentId(),
                    'leieforhold' => $krav->leieforhold->hentId(),
                ]);
                /** @var Delbeløp $sisteBetaling */
                $sisteBetaling = $krav->hentDelkrav()->hentSiste();
                $sisteBetaling->fjernAvstemming()->avstemMotKrav($krav);
            });
        }
        return $this;
    }

    /**
     * Kontroller kredittbalanse
     *
     * Kontrollerer at kreditt (som består av ett krav og to samsvarende betalingsbeløp) er lagret rett:
     * * Étt krav med negativt beløp (kredittkravet)
     * * Ei betaling med konto = '0' pålydende kr. 0, som består av følgende delbeløp:
     * > * Étt negativt betalingsbeløp med konto = '0' og knyttet til kredittkravet (gjennom krav-feltet)
     * > * Étt eller flere positive beløp som samlet tilsvarer det negative beløpet,
     * som kan avstemmes mot andre krav og betalinger
     *
     * @throws Exception
     */
    public function sjekkKredittbalanse(): Vedlikehold
    {
        $kredittVarsler = [];
        $varselNivå = Leiebase::VARSELNIVÅ_ALARM;

        /**
         * Sjekk at summen av betalingsdelen av kreditt alltid er 0.
         * Alle betalinger med konto = '0' skal være kr. 0
         *
         * @var Innbetalingsett $ubalanse
         */

        $ubalanse = $this->app->hentSamling(Innbetaling::class);
        $ubalanse->leggTilFilter(['konto_id' => Innbetaling::KTO_KREDITT])
            ->leggTilHaving(['SUM(`' . Innbetaling::hentTabell() . '`.`beløp`) != 0'], true)->låsFiltre();

        if($ubalanse->hentAntall()) {
            /** @var Innbetaling $innbetaling */
            foreach ($ubalanse as $innbetaling) {
                $varsel = [
                    'oppsummering' => 'Ubalanse i kreditt',
                    'tekst' => 'Det har oppstått en ubalanse i kredittransaksjonene i leiebasen.<br>'
                        . "Summen av kreditt-transaksoner lagret som innbetaling {$innbetaling->hentId()} er {$this->app->kr($innbetaling->hentBeløp())}.<br>"
                        . 'Denne skulle vært 0.<br>'
                        . 'Avviket skyldes en teknisk svikt i leiebasen som bør rettes.'
                ];
                settype($kredittVarsler[$varselNivå], 'array');
                $kredittVarsler[$varselNivå][] = $varsel;

                $developerEmail = Leiebase::$config['leiebasen']['debug']['developer_email'] ?? null;
                if ($developerEmail) {
                    $this->app->sendMail((object)array(
                        'to' => $developerEmail,
                        'subject' => $varsel['oppsummering'],
                        'html' => $varsel['tekst'],
                        'priority' => 100
                    ));
                }
            }
        }

        else {
            /**
             * Sjekk at alle kredittkrav har en tilhørende delbetaling med samme beløp som kravet, og med konto = '0'
             *
             * Kontrollen utføres toveis; både mot krav og mot delbeløp
             *
             * @var Kravsett $feilIKreditt
             */
            $feilIKreditt = $this->app->hentSamling(Krav::class);
            $feilIKreditt->leggTilLeftJoin(Innbetaling::hentTabell(),
                '`' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '` = `' . Innbetaling::hentTabell() . '`.`krav`');
            $feilIKreditt->leggTilFilter(['`' . Krav::hentTabell() . '`.`beløp` <' => 0])
                ->leggTilFilter(['or' => [
                    '`' . Krav::hentTabell() . '`.`beløp` != `' . Innbetaling::hentTabell() . '`.`beløp`',
                    '`' . Innbetaling::hentTabell() . '`.`konto_id` !=' => Innbetaling::KTO_KREDITT
                ]])
                ->låsFiltre();

            /** @var Krav $kreditt */
            foreach ($feilIKreditt as $kreditt) {
                $varsel = [
                    'oppsummering' => 'Feil i kreditt',
                    'tekst' => 'Det har oppstått en feil i kredittene i leiebasen.<br>'
                        . "Kreditt {$kreditt->hentId()} er ikke lagret riktig.<br>"
                        . 'Dette skyldes en teknisk svikt i leiebasen som bør rettes.'
                ];

                settype($kredittVarsler[$varselNivå], 'array');
                $kredittVarsler[$varselNivå][] = $varsel;
            }

            /** @var Delbeløpsett $feilIKreditt */
            $feilIKreditt = $this->app->hentSamling(Delbeløp::class);
            $feilIKreditt->leggTilLeftJoin(Krav::hentTabell(),
                '`' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '` = `' . Delbeløp::hentTabell() . '`.`krav`');
            $feilIKreditt
                ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`konto_id`' => Innbetaling::KTO_KREDITT])
                ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`beløp` <' => 0])
                ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`krav` !=' => '0'])
                ->leggTilFilter(['`' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '` IS NULL'])
                ->låsFiltre();

            /** @var Delbeløp $delbeløp */
            foreach ($feilIKreditt as $delbeløp) {
                $varsel = [
                    'oppsummering' => 'Feil i kreditt',
                    'tekst' => 'Det har oppstått en feil i kredittene i leiebasen.<br>'
                        . "Betaling {$delbeløp->innbetaling->hentId()} har {$this->app->kr(-$delbeløp->hentBeløp())} ikke knyttet til et kredittkrav.<br>"
                        . 'Dette skyldes en teknisk svikt i leiebasen som bør rettes.'
                ];

                settype($kredittVarsler[$varselNivå], 'array');
                $kredittVarsler[$varselNivå][] = $varsel;
            }
        }

        /*
         * Oppdater lagrede varsler i leiebasen
         */
        $advarsler = json_decode($this->app->hentValg('advarsler_drift')) ?? new stdClass();
        $advarsler->kreditt = $kredittVarsler;
        $this->app->settValg('advarsler_drift', json_encode($advarsler));
        return $this;
    }

    /**
     * Kontroller OCR-innbetalinger
     *
     * Kontrollrutinen sjekker om alle innbetalingene har blitt registrert i samsvar med ocr-forsendelsene
     *
     * @throws Exception
     */
    public function sjekkOcrInnbetalinger(): Vedlikehold
    {
        $orcVarsler = [];
        $varselNivå = Leiebase::VARSELNIVÅ_ALARM;
        /** @var Ocr\Transaksjonsett $ocrTransaksjonSett */
        $ocrTransaksjonSett = $this->app->hentSamling(Ocr\Transaksjon::class);
        $ocrTransaksjonSett->leggTilLeftJoin(Innbetaling::hentTabell(),
            '`' . Innbetaling::hentTabell() . '`.`ocr_transaksjon` = `' . Ocr\Transaksjon::hentTabell() . '`.`' . Ocr\Transaksjon::hentPrimærnøkkelfelt() . '`');
        $ocrTransaksjonSett->leggTilHaving(['`' . Ocr\Transaksjon::hentTabell() . '`.`beløp` != ifnull(sum(`' . Innbetaling::hentTabell() . '`.`beløp`),0)'], true);

        if ($ocrTransaksjonSett->hentAntall()) {

            $tekst = 'Det er uoverenstemmelse i mellom oppgitt beløp og registrerte innbetalinger i følgende OCR-forsendelser:<br>';

            /** @var Ocr\Transaksjon $ocrTransaksjon */
            foreach ($ocrTransaksjonSett as $ocrTransaksjon) {
                $tekst .= '<a title="Inspiser OCR-forsendelsen" href="' . DriftKontroller::url('ocr_kort', $ocrTransaksjon->hentFil()->hentId()) . '">Forsendelse '
                    . $ocrTransaksjon->hentForsendelsesnummer()
                    . ' '
                    . $ocrTransaksjon->oppdragsdato->format('d.m.Y')
                    . '</a><br>';
            }

            settype($kredittVarsler[$varselNivå], 'array');
            $orcVarsler[$varselNivå][] = [
                'oppsummering' => 'Mangler i registrerte innbetalinger.',
                'tekst' => $tekst
            ];
        }

        /*
         * Oppdater lagrede varsler i leiebasen
         */
        $advarsler = json_decode($this->app->hentValg('advarsler_drift')) ?? new stdClass();
        $advarsler->registrerte_betalinger = $orcVarsler;
        $this->app->settValg('advarsler_drift', json_encode($advarsler));
        return $this;
    }

    /**
     * Kontroller adresseoppdateringer
     *
     * @throws Exception
     */
    public function sjekkAdresseoppdateringer(): Vedlikehold
    {
        $adressekortVarsler = [];
        $varselNivå = Leiebase::VARSELNIVÅ_ORIENTERING;

        /** @var Personsett $ikkeOppdaterte */
        $ikkeOppdaterte = $this->app->hentSamling(Person::class);
        $ikkeOppdaterte->leggTilInnerJoin(Leieforhold::hentTabell(),
            '`' . Person::hentTabell() . '`.`' . Person::hentPrimærnøkkelfelt() . '` = `' . Leieforhold::hentTabell() . '`.`regningsperson`');
        $ikkeOppdaterte->leggTilInnerJoin(Leieforhold\Oppsigelse::hentTabell(),
            '`' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '` = `' . Leieforhold\Oppsigelse::hentTabell() . '`.`' . Leieforhold\Oppsigelse::hentPrimærnøkkelfelt() . '`');
        $ikkeOppdaterte->leggTilInnerJoin(Leieobjekt::hentTabell(),
            '`' . Leieforhold::hentTabell() . '`.`leieobjekt` = `' . Leieobjekt::hentTabell() . '`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '`');
        $ikkeOppdaterte->leggTilInnerJoin(Krav::hentTabell(),
            '`' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '` = `' . Krav::hentTabell() . '`.`leieforhold`');
        $ikkeOppdaterte
            ->leggTilFilter(['`' . Leieforhold\Oppsigelse::hentTabell() . '`.`fristillelsesdato` < NOW()'])
            ->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`regning_til_objekt`' => false])
            ->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`frosset`' => false])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`utestående` >' => 0])
            ->leggTilFilter(['or' => [
                '`' . Person::hentTabell() . '`.`adresse1` = `' . Leieobjekt::hentTabell() . '`.`gateadresse`',
                '`' . Person::hentTabell() . '`.`adresse2` = `' . Leieobjekt::hentTabell() . '`.`gateadresse`'
            ]])
            ->leggTilFilter(['`' . Person::hentTabell() . '`.`postnr` = `' . Leieobjekt::hentTabell() . '`.`postnr`'])
            ->låsFiltre();
        if ($ikkeOppdaterte->hentAntall()) {
            /** @var Person $eksempelPerson */
            $eksempelPerson = $ikkeOppdaterte->hentTilfeldig();
            settype($adressekortVarsler[$varselNivå], 'array');
            $adressekortVarsler[$varselNivå][] = [
                'oppsummering' => $eksempelPerson->hentNavn() . ' sitt adressekort bør oppdateres',
                'tekst' => $eksempelPerson->hentNavn()
                    . ($ikkeOppdaterte->hentAntall() > 2 ? ' og ' . ($ikkeOppdaterte->hentAntall() - 1) . ' andre' : '')
                    . ' sitt adressekort viser fortsatt samme adresse som før utflytting, og regninger sendt i posten vil muligens ikke komme fram.<br>'
                    . 'Klikk <a href="' . DriftKontroller::url('person_adresse_skjema', $eksempelPerson->hentId()) . '">her</a>'
                    . ' for å oppdatere med rett kontaktinformasjon.'
            ];
        }

        /*
         * Oppdater lagrede varsler i leiebasen
         */
        $advarsler = json_decode($this->app->hentValg('advarsler_drift')) ?? new stdClass();
        $advarsler->adressekort_oppdateringer = $adressekortVarsler;
        $this->app->settValg('advarsler_drift', json_encode($advarsler));
        return $this;
    }

    /**
     * Kontoller utløp
     *
     * @throws Exception
     */
    public function sjekkLeieavtalerSomBørFornyes(): Vedlikehold
    {
        $varsler = [];
        $iDag = new DateTimeImmutable();
        $framtildato = $iDag->add(new DateInterval('P1M'));

        /** @var Leieforholdsett $leieforholdSett */
        $leieforholdSett = $this->app->hentSamling(Leieforhold::class);
        $leieforholdSett->leggTilLeftJoinForOppsigelse();
        $leieforholdSett->leggTilFilter(['`' . Leieforhold\Oppsigelse::hentTabell() . '`.`leieforhold`' => null]);
        $leieforholdSett->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`tildato` IS NOT NULL']);
        $leieforholdSett->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`tildato` <' => $framtildato->format('Y-m-d')]);

        /** @var Leieforhold[] $utløpt */
        $utløpt = [];
        /** @var Leieforhold[] $utløper */
        $utløper = [];

        /** @var Leieforhold $leieforhold */
        foreach ($leieforholdSett as $leieforhold) {
            if ($leieforhold->hentTildato() < $iDag) {
                $utløpt[$leieforhold->hentId()] = $leieforhold;
            } else {
                $utløper[$leieforhold->hentId()] = $leieforhold;
            }
        }

        if (count($utløpt) > 2) {
            settype($varsler[Leiebase::VARSELNIVÅ_ADVARSEL], 'array');
            $varsler[Leiebase::VARSELNIVÅ_ADVARSEL][] = [
                'oppsummering' => count($utløpt) . ' leieavtaler har utløpt',
                'tekst' => count($utløpt) . " leieavtaler har utløpt uten å ha blitt registrert fornyet eller avsluttet.<br>"
                    . 'Klikk <a href="' . DriftKontroller::url('oversikt_utløpte') . '">her</a> for å åpne oversikten over leieavtaler som skulle vært fornyet.'
            ];
        } else {
            /** @var Leieforhold $leieforhold */
            foreach ($utløpt as $leieforhold) {
                settype($varsler[Leiebase::VARSELNIVÅ_ADVARSEL], 'array');
                $varsler[Leiebase::VARSELNIVÅ_ADVARSEL][] = [
                    'oppsummering' => $leieforhold->hentNavn() . ' sin leieavtale i ' . $leieforhold->leieobjekt->hentNavn() . ' har utløpt',
                    'tekst' => $leieforhold->hentNavn()
                        . ' sin leieavtale i ' . $leieforhold->leieobjekt->hentNavn()
                        . ' har utløpt uten å ha blitt registrert fornyet eller avsluttet.<br>'
                        . DriftKontroller::lenkeTilLeieforholdKort($leieforhold)
                ];
            }
        }
        if (count($utløper) > 2) {
            settype($varsler[Leiebase::VARSELNIVÅ_ORIENTERING], 'array');
            $varsler[Leiebase::VARSELNIVÅ_ORIENTERING][] = [
                'oppsummering' => 'Flere leieavtaler bør fornyes',
                'tekst' => 'Flere leieavtaler er i ferd med å utløpe, og bør fornyes så snart som mulig.<br>'
                    . 'Klikk <a href="' . DriftKontroller::url('oversikt_utløpte') . '">her</a> for å åpne oversikten over leieavtaler som skulle vært fornyet.'
            ];
        }
        else {
            /** @var Leieforhold $leieforhold */
            foreach ($utløper as $leieforhold) {
                settype($varsler[Leiebase::VARSELNIVÅ_ORIENTERING], 'array');
                $varsler[Leiebase::VARSELNIVÅ_ORIENTERING][] = [
                    'oppsummering' => $leieforhold->hentNavn() . ' sin leieavtale i ' . $leieforhold->leieobjekt->hentNavn() . ' bør fornyes innen ' . $leieforhold->tildato->format('d.m.Y'),
                    'tekst' => $leieforhold->hentNavn() . ' sin leieavtale i ' . $leieforhold->leieobjekt->hentNavn() . ' utløper den ' . $leieforhold->tildato->format('d.m.Y')
                        . DriftKontroller::lenkeTilLeieforholdKort($leieforhold)
                ];
            }
        }

        /*
         * Oppdater lagrede varsler i leiebasen
         */
        $advarsler = json_decode($this->app->hentValg('advarsler_drift')) ?? new stdClass();
        $advarsler->adressekort_oppdateringer = $varsler;
        $this->app->settValg('advarsler_drift', json_encode($advarsler));
        return $this;
    }

    /**
     * Kontroller oppfølgingspåminnelser
     *
     * @throws Exception
     */
    public function sjekkOppfølgingspåminnelser(): Vedlikehold
    {
        $varsler = [];

        /** @var Leieforholdsett $utløptePauser */
        $utløptePauser = $this->app->hentSamling(Leieforhold::class)
            ->leggTilInnerJoin(Krav::hentTabell(), Krav::hentTabell() . '.leieforhold = ' . Leieforhold::hentTabell() . '.' . Leieforhold::hentPrimærnøkkelfelt())
            ->leggTilUttrykk('leieforhold_ekstra', 'utestående', 'IF(`krav`.`kravdato`<= NOW(), `krav`.`utestående`, 0)', 'sum');
        $utløptePauser
            ->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`avvent_oppfølging` IS NOT NULL'])
            ->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`avvent_oppfølging` < NOW()'])
            ->låsFiltre();
        $utløptePauser->leggTilSortering('avvent_oppfølging');

        /**
         * @var Leieforhold $leieforhold
         */
        foreach ($utløptePauser as $leieforhold) {
            if ($leieforhold->hentUtestående() > 0) {
                settype($varsler[Leiebase::VARSELNIVÅ_ORIENTERING], 'array');
                $varsler[Leiebase::VARSELNIVÅ_ORIENTERING][] = [
                    'oppsummering' => 'Påminnelse om oppfølging',
                    'tekst' => 'Oppfølging av enkelte leieforhold som har vært satt midlertidig på vent kan nå gjenopptaes.<br>'
                        . 'Besøk <a href="' . Leiebase::url('', null, null, [], 'oppfølging') . '">oppfølging</a> for detaljer, og for å slå av denne påminnelsen.'
                ];
                break;
            }
        }

        /*
         * Oppdater lagrede varsler i leiebasen
         */
        $advarsler = json_decode($this->app->hentValg('advarsler_drift')) ?? new stdClass();
        $advarsler->oppfølging = $varsler;
        $this->app->settValg('advarsler_drift', json_encode($advarsler));
        return $this;
    }

    /**
     * Kontrollrutinen ser etter regninger som ikke har blitt skrevet ut.
     *
     * @throws Exception
     */
    public function sjekkRegningsUtskrifter(): Vedlikehold
    {
        $varsler = [];

        /** @var Utskriftsforsøk|null $utskriftsforsøk */
        $utskriftsforsøk = $this->app->hentUtskriftsprosessor()->hentLagretUtskriftsforsøk();
        if ($utskriftsforsøk) {
            $lenke = DriftKontroller::url('utskriftsveiviser');

            $tekst = 'Det har blitt forberedt en utskrift av regninger som må skrives ut og godkjennes'
                . '.<br><br><a href=' . $lenke . '>Mer...</a>.';

            settype($varsler[Leiebase::VARSELNIVÅ_ORIENTERING], 'array');
            $varselNivå = $utskriftsforsøk->autoGenerertManuellUtskrift
                ? Leiebase::VARSELNIVÅ_ORIENTERING
                : Leiebase::VARSELNIVÅ_ADVARSEL;
            $varsler[$varselNivå][] = [
                'oppsummering' => 'Det har blitt forberedt en utskrift.',
                'tekst' => $tekst
            ];
        }
        else {
            $framtildato = date_create_immutable()->add(new DateInterval($this->app->hentValg('min_frist_regninger')));
            $ikkeUtskrevet = $this->app->hentUtskriftsprosessor()->hentKravsettForUtskrift($framtildato);
            $ikkeUtskrevet
                ->leggTilFilter(['`' . Krav::hentTabell() . '`.`forfall` <=' => $framtildato->format('Y-m-d')])
                ->leggTilFilter(['`' . Krav::hentTabell() . '`.`utestående` >' => 0])
                ->låsFiltre();
            $ikkeUtskrevet->leggTilSortering('forfall');

            /** @var Krav|null $krav */
            $krav = $ikkeUtskrevet->hentFørste();
            if ($krav) {
                $forfalt = $krav->hentForfall() < date_create_immutable();
                $antall = $ikkeUtskrevet->hentAntall();
                $forfall = $krav->hentForfall();
                $lenke = DriftKontroller::url('utskriftsveiviser', null, null, [
                    'tildato' => $forfall->format('Y-m-d'),
                    'returi' => 'default',
                ]);

                $tekst = 'Det har ' . ($forfalt ? 'fortsatt' : 'ennå')
                    . " ikke blitt skrevet ut giroer for {$antall} krav som "
                    . ($forfalt ? 'har forfalt siden ' : 'forfaller fra og med ')
                    . $forfall->format('d.m.Y')
                    . '.<br>'
                    . '<a href="' . $lenke . '">Skriv ut</a>';

                $varselNivå = $forfalt
                    ? Leiebase::VARSELNIVÅ_ADVARSEL
                    : Leiebase::VARSELNIVÅ_ORIENTERING;
                $varsler[$varselNivå][] = [
                    'oppsummering' => 'Det er ikke skrevet ut giroer.',
                    'tekst' => $tekst
                ];
            }
        }

        /*
         * Oppdater lagrede varsler i leiebasen
         */
        $advarsler = json_decode($this->app->hentValg('advarsler_drift')) ?? new stdClass();
        $advarsler->regningsutsending = $varsler;
        $this->app->settValg('advarsler_drift', json_encode($advarsler));
        return $this;
    }

    /**
     * Kontrollrutinen ser etter innbetalinger som ikke er avstemt.
     *
     * @throws Exception
     */
    public function sjekkAvstemming(): Vedlikehold
    {
        $varsler = [];

        $framtildato = date_create_immutable()->sub(new DateInterval('P1M'));
        /** @var Innbetaling\Delbeløpsett $ikkeAvstemt */
        $ikkeAvstemt = $this->app->hentSamling(Innbetaling\Delbeløp::class);
        $ikkeAvstemt->leggTilFilter(['krav' => null])->låsFiltre();
        $ikkeAvstemt->leggTilSortering('dato');
        $ikkeAvstemt->begrens(1);
        /** @var Innbetaling\Delbeløp $delbeløp */
        $delbeløp = $ikkeAvstemt->hentFørste();
        if ($delbeløp) {
            $varselNivå = $delbeløp->hentDato() < $framtildato
                ? Leiebase::VARSELNIVÅ_ADVARSEL
                : Leiebase::VARSELNIVÅ_ORIENTERING;
            $varsler[$varselNivå][] = [
                'oppsummering' => 'Det er registrert innbetalinger som ikke er avstemt.',
                'tekst' => 'Det er registrert innbetalinger som ikke er avstemt.<br>'
                    . '<br>'
                    . 'Klikk <a href="' . DriftKontroller::url('utlikninger_skjema') . '">her</a> for å angi hva betalingene gjelder.'
            ];
        }

        /*
         * Oppdater lagrede varsler i leiebasen
         */
        $advarsler = json_decode($this->app->hentValg('advarsler_drift')) ?? new stdClass();
        $advarsler->avstemming = $varsler;
        $this->app->settValg('advarsler_drift', json_encode($advarsler));
        return $this;
    }
}