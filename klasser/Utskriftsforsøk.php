<?php

namespace Kyegil\Leiebasen;


/**
 * Class Utskriftsforsøk
 * @package Kyegil\Leiebasen
 */
class Utskriftsforsøk
{
    /** @var bool */
    public bool $autoGenerertManuellUtskrift = false;
    /**
     * @var int[] Regninger som skal skrives ut
     */
    public $giroer = [];

    /**
     * @var string[] Purringer som skal skrives ut
     */
    public $purringer = [];

    /**
     * @var string[] Purringer som skal gebyrlegges
     */
    public $gebyrpurringer = [];

    /**
     * @var int[] Statusoversikter som skal skrives ut
     */
    public $statusoversikter = [];

    /**
     * @var string
     */
    public $bruker;

    /**
     * @var \DateTimeInterface
     */
    public $tidspunkt;
}