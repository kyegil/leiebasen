<?php

namespace Kyegil\Leiebasen\Nets;

use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Exception;
use Kyegil\Leiebasen\CoreModelImplementering;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Fbo;
use Kyegil\Leiebasen\Modell\Leieforhold\Fbo\Trekk;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning\Efaktura;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning\Efakturasett;
use Kyegil\Leiebasen\Modell\Leieforhold\Regningsett;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\EfakturaIdentifier;
use Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient;
use Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient\EfakturaIdentifierKey;
use Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient\EfakturaIdentifierResponse;
use Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient\Invoice;
use Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient\Invoice\InvoiceSpecification;
use Kyegil\Nets\Exceptions\ApiFault;
use Kyegil\Nets\Exceptions\ServerFault;
use Kyegil\Nets\Exceptions\ValidationFault;
use Kyegil\Nets\Forsendelse\EfakturaOppdrag;
use SoapFault;
use Throwable;

/**
 *
 */
class Prosessor
{
    /** @var object */
    private object $efakturaKommunikasjonsmåter;

    /**
     * @var Leiebase
     */
    protected Leiebase $app;

    /**
     * @var array
     */
    private $efakturaApiConfig;

    /**
     * @var string
     */
    private string $efakturaIssuer;

    /**
     * @var EfakturaApiClient
     */
    protected EfakturaApiClient $efakturaApiClient;

    /**
     * @param CoreModelImplementering $leiebase
     * @param array $argumenter
     * @return array $argumenter
     */
    public static function cron(CoreModelImplementering $leiebase, array $argumenter = []): array {
        /** @var DateTimeInterface $cronTid */
        try {
            $cronTid = $argumenter[0] ?? date_create_immutable();
            $netsProsessor = $leiebase->hentNetsProsessor();
            if ($cronTid->format('H') > 0 && $leiebase->hentValg('efaktura')) {
                try {
                    $netsProsessor->oppdaterEfakturaApiForsendelseStatus();
                } catch (Exception $e) {
                    $leiebase->logger->critical('NETS – ' . $e->getMessage(), ['file' => $e->getFile(), 'line' => $e->getLine()]);
                }

                try {
                    $netsProsessor->sendOgRegistrerEfakturaer();
                } catch (Exception $e) {
                    $leiebase->logger->critical('NETS – ' . $e->getMessage(), ['file' => $e->getFile(), 'line' => $e->getLine()]);
                }

                try {
                    $netsProsessor->oppdaterEfakturaIdentifiers($cronTid);
                } catch (Exception $e) {
                    $leiebase->logger->critical('NETS – ' . $e->getMessage(), ['file' => $e->getFile(), 'line' => $e->getLine()]);
                }
            }

        } catch (Throwable $e) {
            $leiebase->logger->critical('NETS – ' . $e->getMessage(), ['file' => $e->getFile(), 'line' => $e->getLine()]);
        }
        return $argumenter;
    }

    /**
     * @param Leiebase $app
     */
    public function __construct(
        Leiebase $app
    )
    {
        $this->app = $app;
        $miljø  = $this->app->live ? 'produksjon' : 'test';
        if(isset(Leiebase::$config['leiebasen']['nets']['efaktura_api'])) {
            $this->efakturaApiConfig = Leiebase::$config['leiebasen']['nets']['efaktura_api'][$miljø] ?? [];
        }
        else {
            $this->efakturaApiConfig = Leiebase::$config['leiebasen']['nets']['efaktura_agreement_api'][$miljø] ?? [];
        }
    }

    /**
     * @param object $efakturaKommunikasjonsmåter
     * @return $this
     * @throws Throwable
     */
    public function settEfakturaKommunikasjonsmåter(object $efakturaKommunikasjonsmåter): Prosessor
    {
        list('efakturaKommunikasjonsmåter' => $efakturaKommunikasjonsmåter) = $this->app->pre($this, __FUNCTION__, ['efakturaKommunikasjonsmåter' => $efakturaKommunikasjonsmåter]);
        $this->efakturaKommunikasjonsmåter = $efakturaKommunikasjonsmåter;
        return $this;
    }

    /**
     * @return object
     * @throws Throwable
     */
    public function hentEfakturaKommunikasjonsmåter(): object
    {
        $this->app->pre($this, __FUNCTION__, []);
        if(!isset($this->efakturaKommunikasjonsmåter)) {
            $this->settEfakturaKommunikasjonsmåter((object)['sftp' => 'SFTP filoverføring', 'api' => 'Api']);
        }
        return $this->app->post($this, __FUNCTION__, $this->efakturaKommunikasjonsmåter);
    }

    /**
     * @return string
     */
    public function hentEfakturaIssuer(): string
    {
        if(!isset($this->efakturaIssuer)){
            $this->efakturaIssuer = filter_var($this->app->hentValg('orgnr'), FILTER_SANITIZE_NUMBER_INT);
        }
        return $this->efakturaIssuer;
    }

    /**
     * NETS Neste forsendelse
     *
     * Returnerer tidspunktet for neste planlagte forsendelse til NETS.
     * Cut-off for Nets er hverdager kl 14,
     * og vi ønsker at forsendelser skal kunne sendes så nært opptil dette som mulig
     *
     * Denne funksjonen tar utgangspunkt i
     * * Forsendelser sendes kun mandag-fredag mellom 10:00 og 13:59, og ikke på helligdager.
     * * Forsendelser sendes siste kvarteret av hver time.
     * * Maks én forsendelse per time, dvs. tidligst kl. 10:45, 11:45, 12:45 og 13:45.
     * *
     *
     * @param DateTimeInterface|null $etter
     * @return DateTimeImmutable Neste normerte NETS-forsendelse
     * @throws Exception
     */
    public function nesteFboTrekkravForsendelse(?DateTimeInterface $etter = null): DateTimeImmutable
    {
        list('etter' => $etter) = $this->app->pre($this, __FUNCTION__, ['etter' => $etter]);

        $tidssone = new DateTimeZone(date_default_timezone_get());
        $utc = new DateTimeZone('utc');
        $time = new DateInterval('PT1H');
        $dag = new DateInterval('P1D');
        $sisteFboTrekkKravForsendelse = new DateTimeImmutable($this->app->hentValg('siste_fbo_trekkrav') ?? null, $utc);
        $sisteFboTrekkKravForsendelse = $sisteFboTrekkKravForsendelse->setTimezone($tidssone);

        if ($etter) {
            $resultat = $etter instanceof DateTime ? DateTimeImmutable::createFromMutable($etter) : $etter;
        }
        else {
            $resultat = new DateTimeImmutable();
        }

        /*
         * Ingen flere forsendelser planlagt denne timen
         * dersom det allerede har blitt sendt en forsendelse denne timen,
         * eller etter 15 minutter på hel time
         */
        if ($resultat->format('i') >= 45
            || $resultat->format('YmdH') == $sisteFboTrekkKravForsendelse->format('YmdH')
        ) {
            $resultat = $resultat->add($time);
        }
        $resultat = $resultat->setTime($resultat->format('H'), 45);

        /*
         * Ingen forsendelser planlagt før kl. 10
         */
        if ($resultat->format('H') < 10) {
            $resultat = $resultat->setTime(10, 45);
        }

        /*
         * Ingen forsendelser planlagt etter kl 1345
         */
        if ($resultat->format('Hi') > 1345) {
            $resultat = $resultat->add($dag)->setTime(10, 45);
        }

        /*
         * Ingen forsendelser i helger eller på helligdager
         */
        while(
            $resultat->format('N') > 5
            || in_array( $resultat->format('m-d'), $this->app->bankfridager($resultat->format('Y')) )
        ) {
            $resultat = $resultat->add($dag);
        }

        return $this->app->post($this, __FUNCTION__, $resultat);
    }

    /**
     * NETS Opprett oppdragsnummer
     *
     * Oppretter et nytt oppdragsnummer for bruk i en NETS-forsendelse
     * Oppdragsnummeret er i formatet år (1 siffer) måned dato løpenr (2 siffer) (9123199)
     * og er unikt innenfor en 10-årsperiode.
     *
     * @return int 5-7-sifret oppdragsnummer
     * @throws Exception
     */
    public function opprettOppdragsnummer(): int
    {
        $this->app->pre($this, __FUNCTION__, []);
        $oppdragsnummer = $this->app->hentValg('nets_siste_oppdragsnr');

        $y = substr( date('y'), -1); // Siste sifferet i årstallet
        $sisteLøpenummer = intval(substr($oppdragsnummer, -2));
        $løpenummer = 1;

        if (substr($oppdragsnummer, 0, -2) == date("md{$y}") ) {
            $løpenummer = $sisteLøpenummer + 1;
        }
        $oppdragsnummer = date("md{$y}") . str_pad($løpenummer, 2, '0', STR_PAD_LEFT);

        $this->app->settValg('nets_siste_oppdragsnr', $oppdragsnummer);
        return $this->app->post($this, __FUNCTION__, (int)$oppdragsnummer);
    }

    /**
     * Oppretter et unikt forsendelsesnummer for NETS
     *
     * Forsendelsesnummeret er i formatet måned (2 sifre), dato (2 sifre) og løpenummer per dato (3 sifre)
     * f.eks. 1231999
     *
     * @return int<101001, 1231999> 6- eller 7-sifret forsendelsesnummer
     * @throws Exception
     */
    public function opprettForsendelsesnummer(): int
    {
        $this->app->pre($this, __FUNCTION__, []);
        $forsendelsesnummer = $this->app->hentValg('nets_siste_forsendelsesnr');

        $sisteLøpenummer = intval(substr($forsendelsesnummer, -3));
        $løpenummer = 1;
        if (substr($forsendelsesnummer, 0, -3) == date('md') ) {
            $løpenummer = $sisteLøpenummer + 1;
        }
        $forsendelsesnummer = date('md') . str_pad($løpenummer, 3, '0', STR_PAD_LEFT);

        $this->app->settValg('nets_siste_forsendelsesnr', $forsendelsesnummer);
        return $this->app->post($this, __FUNCTION__, (int)$forsendelsesnummer);
    }

    /**
     * Oppretter en unikt Consignment Id for efaktura API
     *
     * Consignment Id er i formatet år(2 sifre), måned (2 sifre), dato (2 sifre) og løpenummer per dato (5 sifre)
     * f.eks. 24123100999
     *
     * @return string 11-sifret Consignment Id
     * @throws Exception
     */
    public function opprettEfakturaApiConsignmentId(): string
    {
        $this->app->pre($this, __FUNCTION__, []);
        $sisteConsignmentId = $this->app->hentValg('nets_siste_api_consignment_id');

        $sisteLøpenummer = intval(substr($sisteConsignmentId, -5));
        $løpenummer = 1;
        if (substr($sisteConsignmentId, 0, -5) == date('ymd') ) {
            $løpenummer = $sisteLøpenummer + 1;
        }
        $consignmentId = date('ymd') . str_pad($løpenummer, 5, '0', STR_PAD_LEFT);

        $this->app->settValg('nets_siste_api_consignment_id', $consignmentId);
        return $this->app->post($this, __FUNCTION__, $consignmentId);
    }

    /**
     *
     *
     * @param DateTimeInterface $cronTid
     * @return Prosessor
     * @throws Throwable
     */
    public function oppdaterEfakturaIdentifiers(DateTimeInterface $cronTid): Prosessor
    {
        list('cronTid' => $cronTid) = $this->app->pre($this, __FUNCTION__, ['cronTid' => $cronTid]);
        if(!empty($this->efakturaApiConfig['brukernavn'])) {
            $leieforholdTabell = Leieforhold::hentTabell();
            $efakturaIdentifierTabell = EfakturaIdentifier::hentTabell();
            /** @var Leieforholdsett $leieforholdsett */
            $leieforholdsett = $this->app->hentSamling(Leieforhold::class);
            $leieforholdsett->leggTilLeftJoin(
                $efakturaIdentifierTabell,
                '`' . Leieforhold::hentTabell() . '`.`regningsperson` = `' . $efakturaIdentifierTabell . '`.`person_id`'
            );
            $leieforholdsett->leggTilFilter(['`' . $efakturaIdentifierTabell . '`.`person_id` IS NULL']);
            $leieforholdsett->leggTilFilter(['`' . $leieforholdTabell . '`.`regningsperson` IS NOT NULL']);
            $leieforholdsett->leggTilFilter(['`' . $leieforholdTabell . '`.`frosset`' => false]);
            $leieforholdsett->begrens(10);

            foreach($leieforholdsett as $leieforhold) {
                try {
                    $this->hentEfakturaIdentifierFor($leieforhold->regningsperson);
                } catch (Exception $e) {
                    $message = 'NETS – eFaktura Agreement ' . $e->getMessage();

                    $details = [
                        'leieforhold' => $leieforhold->hentId(),
                        'regningsperson' => strval($leieforhold->regningsperson)
                    ];
                    if ($e instanceof SoapFault && !$this->app->live) {
                        $this->app->logger->warning($message, $details);
                        throw $e;
                    }
                    else {
                        $this->app->logger->critical($message, $details);
                    }
                }
            }
        }
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @param Person $person
     * @return Prosessor
     * @throws Exception
     */
    public function hentEfakturaIdentifierFor(Person $person): Prosessor
    {
        $this->app->logger->info('NETS – eFaktura Agreement Identifier søkes ', ['person_id' => $person->hentId()]);
        if(!empty($this->efakturaApiConfig['brukernavn'])) {
            $efakturaIdentifierKey = new EfakturaIdentifierKey();
            $orgNr = $this->app->hentValg('orgnr');
            $land = $person->hentLand() ?: 'Norge';
            $efaktura1Referanse = $person->hent('efaktura1_referanse');
            $efakturaIdentifierKey
                ->setId($person->hentId())
                ->setSocialSecurityNumber(!$person->erOrg ? $person->hentFødselsnummer() : null)
                ->setOrganizationNumber(!$person->erOrg ? $person->hentOrgNr() : null)
                ->setTelephoneAlias(trim($person->hentTelefon()) ?: trim($person->hentMobil()))
                ->setEmailAlias($person->hentEpost())
                ->setFirstName($person->hentFornavn())
                ->setLastName($person->hentEtternavn())
                ->setAddressLine1($person->hentAdresse1())
                ->setAddressLine2($person->hentAdresse2())
                ->setPostalCode($person->hentPostnr())
                ->setCity($person->hentPoststed())
                ->setDateOfBirth($person->hentFødselsdato())
                ->setInvoiceIssuer($orgNr)
                ->setEfakturaIdentifier($person->hentEfakturaIdentifier())
                ->setCountryCode($land == 'Norge' ? 'NO' : null)
                ->setEFakturaReference($efaktura1Referanse)
                ->setContentProviderId($orgNr)
            ;

            $agreementClient = $this->hentEfakturaApiClient();

            /** @var EfakturaIdentifierResponse $response */
            foreach ($agreementClient->getEfakturaIdentifiers([$efakturaIdentifierKey]) as $response) {
                /** @var EfakturaIdentifierKey $key */
                $key = $response->getEfakturaIdentifierKey();
                $personId = $key->getId();
                /** @var Person $person */
                $person = $this->app->hentModell(Person::class, $personId);
                if($person->hentId()) {
                    $person->lagreEfakturaAgreement($response);
                }
            }
        }
        return $this;
    }

    /**
     * @param Regningsett $regningsett
     * @return Prosessor
     * @throws NetsException
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    private function sendEfakturaSftp(Regningsett $regningsett): Prosessor
    {
        list('regningsett' => $regningsett) = $this->app->pre($this, __FUNCTION__, ['regningsett' => $regningsett]);

        $efakturaoppdrag = $this->lagEfakturaOppdrag($regningsett->hentElementer());
        if ($efakturaoppdrag) {
            $efakturaOppkopling = $this->app->hentNetsForbindelse(Oppkopling::TJENESTE_EFAKTURA);
            $efakturaOppkopling->logg("Sender " . $regningsett->hentAntall() . " regninger som efaktura direkte ved utskrift.");
            $efakturaforsendelse = $efakturaOppkopling->lagUtgåendeForsendelse([$efakturaoppdrag]);
            try {
                $efakturaOppkopling->sendUtgåendePost();
            } catch (NetsException $e) {
                if (!$this->app->live) {
                    $this->app->logger->warning('NETS – ' . $e->getMessage(), ['file' => $e->getFile(), 'line' => $e->getLine()]);
                }
            }

            /** @var Regning $regning */
            foreach ($regningsett as $regning) {
                /** @var Efaktura $efaktura */
                $efaktura = $regning->hentEfaktura();
                $efaktura->forsendelsesdato = new DateTimeImmutable();
                $efaktura->settForsendelse($efakturaforsendelse->forsendelsesnummer);
                $efaktura->settOppdrag($efakturaoppdrag->oppdragsnr);
                $efaktura->settStatus(Efaktura::SENT);
            }
        }

        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @param Regningsett $regningsett
     * @return $this
     * @throws Exception
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    private function sendEfakturaApi(Regningsett $regningsett): Prosessor
    {
        list('regningsett' => $regningsett) = $this->app->pre($this, __FUNCTION__, ['regningsett' => $regningsett]);
        $dato = new DateTimeImmutable();
        $regningsett->inkluderKrav();
        $regningsett->leggTilLeieforholdModell();
        /** @var Invoice[] $eFakturaer */
        $eFakturaer = [];

        $submitterClient = $this->hentEfakturaApiClient();
        $this->app->logger->info('NETS – ' . $regningsett->hentBuntAntall() . ' efakturaer overføres via api', ['regninger' => $regningsett->hentIdNumre()]);

        foreach ($regningsett as $regning) {
            try {
                $førsteGangUtskrift = !$regning->utskriftsdato;
                if($førsteGangUtskrift) {
                    /*
                     * Sett en midlertidig utskriftsdato for bruk i pdf.
                     * Utskriftsdato settes ikke fast før eFakturaen er bekreftet fra NETS
                    */
                    $regning->dekk('utskriftsdato', $dato);
                }
                $regning->lagrePdf();
                $invoice = $this->regningTilInvoice($regning);
                $eFakturaer[] = $invoice->getData();
                if($førsteGangUtskrift) {
                    $regning->nullstill();
                }
            } catch (Exception $e) {
                $this->app->logger->critical('NETS – ' . $e->getMessage(), ['file' => $e->getFile(), 'line' => $e->getLine()]);
                if($førsteGangUtskrift) {
                    $regning->slettPdf();
                }
            }
        }
        try {
            $consignmentId = $this->opprettEfakturaApiConsignmentId();
            $submitterClient->addInvoices($eFakturaer, $consignmentId);
            $regningsett->forEach(function(Regning $regning) use ($dato, $consignmentId) {
                $regning->hentEfaktura()
                    ->settForsendelse($consignmentId)
                    ->settForsendelsesdato($dato)
                    ->settStatus(Efaktura::SENT);
            });
        } catch (ApiFault $e) {
            $this->app->logger->warning('NETS – ' . $e->getMessage(), ['file' => $e->getFile(), 'line' => $e->getLine()]);
        }
        $this->app->logger->info('NETS – api-overføring fullført');
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @return Prosessor
     * @throws Throwable
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    public function sendOgRegistrerEfakturaer(): Prosessor
    {
        $this->app->pre($this, __FUNCTION__, []);
        /** @var Regningsett $efakturaRegningssett */
        $efakturaRegningssett = $this->app->hentSamling(Regning::class);
        $efakturaRegningssett->leggTilLeftJoin(Efaktura::hentTabell(), '`' . Efaktura::hentTabell() . '`.`giro` = `' . Regning::hentTabell() . '`.`' . Regning::hentPrimærnøkkelfelt() . '`');
        $efakturaRegningssett->leggTilModell(Efaktura::class);
        $efakturaRegningssett->leggTilFilter(['`' . Efaktura::hentTabell() . '`.`status`' => Efaktura::PREPARED])->låsFiltre();
        $efakturaRegningssett->begrens(10);

        // Lag eFakturaforsendelse til NETS
        if ($efakturaRegningssett->hentAntall()) {
            $this->app->logger->info('NETS – ' . $efakturaRegningssett->hentAntall() . ' efakturaer skal overføres til Mastercard');

            switch ($this->app->hentValg('efaktura_kommunikasjonsmåte')) {
                case 'sftp':
                    $this->sendEfakturaSftp($efakturaRegningssett);
                    break;
                case 'api':
                    $this->sendEfakturaApi($efakturaRegningssett);
                    break;
            }
        }
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * NETS Lag Efaktura-oppdrag
     *
     * Lager et oppdrag for inkludering i NETS-forsendelse
     *
     * @param Regning[] $regninger
     * @return EfakturaOppdrag|null Efaktura-oppdraget
     * @throws Exception
     */
    public function lagEfakturaOppdrag(array $regninger): ?EfakturaOppdrag
    {
        list('regninger' => $regninger) = $this->app->pre($this, __FUNCTION__, ['regninger' => $regninger]);
        $this->app->logger->info('NETS – Lager NETS efaktura-oppdrag.');
        $oppdrag = new EfakturaOppdrag();
        $oppdrag->oppdragsnr = $this->opprettOppdragsnummer();
        $oppdrag->oppdragskonto = filter_var($this->app->hentValg('bankkonto'), FILTER_SANITIZE_NUMBER_INT);
        $oppdrag->referanseFakturautsteder = $this->app->hentValg('efaktura_referansenummer');

        /** @var Regning $regning */
        foreach ($regninger as $regning) {
            /** @var Leieforhold $leieforhold */
            $leieforhold = $regning->leieforhold;
            $regningsperson = $leieforhold->hentRegningsperson();
            $efakturaIdentifier = $regningsperson ? $regningsperson->hentEfakturaIdentifier() : null;

            if ($efakturaIdentifier) {
                $oppdrag->transaksjoner[] = $regning->lagEfakturaTransaksjon();
            }
        }

        if (!$oppdrag->transaksjoner) {
            return null;
        }

        return $this->app->post($this, __FUNCTION__, $oppdrag);
    }

    /**
     * @return $this
     */
    public function oppdaterEfakturaApiForsendelseStatus(): Prosessor
    {
        $this->app->pre($this, __FUNCTION__, []);
        if($this->app->hentValg('efaktura_kommunikasjonsmåte') != 'api') {
            return $this->app->post($this, __FUNCTION__, $this);
        }
        /** @var Efakturasett $efakturasett */
        $efakturasett = $this->app->hentSamling(Efaktura::class);
        $efakturasett->leggTilFilter(['status' => [Efaktura::SENT, Efaktura::CONSIGNMENT_IN_PROGRESS]])->låsFiltre();
        /** @var string[] $consignmentIds */
        $consignmentIds = [];
        foreach ($efakturasett as $efaktura) {
            $consignmentIds[] = $efaktura->hentForsendelse();
        }
        $consignmentIds = array_unique($consignmentIds);
        if($consignmentIds) {
            $submitterClient = $this->hentEfakturaApiClient();
            foreach ($consignmentIds as $consignmentId) {
                try {
                    $consignmentStatus = $submitterClient->getConsignmentStatus($consignmentId);

                    $rejectedInvoices = $consignmentStatus->rejectedInvoices ?? [];
                    if($consignmentStatus->rejectedInvoiceCount > 0) {
                        $this->app->logger->warning('NETS – ' . $consignmentStatus->rejectedInvoiceCount . ' eFaktura(er) har blitt avvist');
                    }

                    /** @var int[] $avvisteRegningIder */
                    $avvisteRegningIder = [];
                    /** @var array{invoiceNumber: string, rejectedReason: string} $rejectedInvoice */
                    foreach($rejectedInvoices['rejectedInvoice'] ?? [] as $rejectedInvoice) {
                        $this->app->logger->warning('NETS – Avvist eFaktura ' . ($rejectedInvoice['invoiceNumber'] ?? '') . ': ' . ($rejectedInvoice['rejectedReason'] ?? ''), ['regning'=> $rejectedInvoice, 'rejectedInvoices' => $rejectedInvoices]);
                        $avvisteRegningIder[] = $rejectedInvoice['invoiceNumber'];
                    }
                    /** @var Efakturasett $avvisteEfakturaer */
                    $avvisteEfakturaer = $this->app->hentSamling(Efaktura::class);
                    $avvisteEfakturaer->leggTilFilter(['forsendelse' => $consignmentId])
                        ->leggTilFilter(['giro' => $avvisteRegningIder])->låsFiltre();
                    $avvisteEfakturaer->slettHver();

                    $gjenståendeEfakturaer = $this->app->hentSamling(Efaktura::class);
                    $gjenståendeEfakturaer->leggTilFilter(['forsendelse' => $consignmentId])->låsFiltre();
                    if($consignmentStatus->status == Efaktura::CONSIGNMENT_REJECTED) {
                        $this->app->logger->warning('NETS – Avvist eFakturaforsendelse: ' . $consignmentStatus->rejectedReason, ['consignment_id'=> $consignmentId]);
                        $gjenståendeEfakturaer->forEach(function (Efaktura $efaktura) {
                            $efaktura->regning->settUtskriftsdato(null);
                            $efaktura->slett();
                        });
                    }
                    if($consignmentStatus->status == Efaktura::CONSIGNMENT_PROCESSED) {
                        $gjenståendeEfakturaer->sett('status', Efaktura::OK);
                        $gjenståendeEfakturaer->forEach(function(Efaktura $efaktura) {
                            $regning = $efaktura->regning;
                            if($regning && $regning->hentId()) {
                                if(!$regning->utskriftsdato) {
                                    $regning->settFormat(Regning::FORMAT_EFAKTURA);
                                    $regning->settUtskriftsdato($efaktura->forsendelsesdato);
                                }
                                if(!$efaktura->kvittertDato) {
                                    $efaktura->settKvittertDato(new DateTimeImmutable());
                                }
                            }
                            else {
                                // Løse efakturaer kan ryddes bort
                                $efaktura->slett();
                            }
                        });
                    }
                    else {
                        $gjenståendeEfakturaer->sett('status', $consignmentStatus->status);
                    }
                    $gjenståendeEfakturaer->sett('status', $consignmentStatus->status == Efaktura::CONSIGNMENT_PROCESSED ? Efaktura::OK : $consignmentStatus->status);
                } catch (Exception $e) {
                    $this->app->logger->error('NETS – ' . $e->getMessage(), ['file' => $e->getFile(), 'line' => $e->getLine(), 'trace' => $e->getTrace()]);
                }
            }
        }
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @return array
     */
    private function hentSoapOptions(): array
    {
        $this->app->pre($this, __FUNCTION__, []);
        $soapOptions = [];
        $clientIp = isset($this->efakturaApiConfig['client_ip'])
            ? trim($this->efakturaApiConfig['client_ip'])
            : null;
        if ($clientIp) {
            $clientIp .= strpos($clientIp, ':') ? '' : ':0';
            $soapOptions['stream_context'] = stream_context_create(['socket' => ['bindto' => $clientIp]]);
        }
        return $this->app->post($this, __FUNCTION__, $soapOptions);
    }

    /**
     * @param Regning $regning
     * @return Invoice
     * @throws Exception
     */
    private function regningTilInvoice(Regning $regning): Invoice
    {
        list('regning' => $regning) = $this->app->pre($this, __FUNCTION__, ['regning' => $regning]);
        $efakturaIssuer = $this->hentEfakturaIssuer();
        $fakturautstederKontonummer = filter_var($this->app->hentValg('bankkonto'), FILTER_SANITIZE_NUMBER_INT);
        $leieforhold = $regning->hentLeieforhold();
        $eFakturaIdentifier = $leieforhold->hentEfakturaIdentifier();
        $regningsperson = $leieforhold->regningsperson;
        $adresse = $leieforhold->hentAdresse();
        $documentType = $regning->hentFboTrekk() ? 'AVTALEGIRO' : 'EFAKTURA';

        $invoiceSpecification = new InvoiceSpecification();
        $invoiceSpecification
            ->setReceiverFirstName($regningsperson->fornavn)
            ->setReceiverLastName($regningsperson->etternavn)
            ->setReceiverAddressLine1($adresse->adresse1)
            ->setReceiverAddressLine2($adresse->adresse2)
            ->setReceiverPostalCode($adresse->postnr)
            ->setReceiverCity($adresse->poststed)
            ->setSenderLastName($this->app->hentValg('utleier'))
            ->setSenderAddressLine1($this->app->hentValg('adresse'))
            ->setSenderPostalCode($this->app->hentValg('postnr'))
            ->setSenderCity($this->app->hentValg('poststed'))
            ->setSenderCountryCode('no')
            ->setInvoiceDescription('Regning ' . $regning->hentId())
            ->setInvoiceDate(date_create_immutable());

        foreach ($regning->hentKrav() as $krav) {
            $invoiceLine = new InvoiceSpecification\InvoiceLine();
            $invoiceLine->setItemName($krav->tekst);
            $invoiceLine->setLineAmount($krav->beløp);
            $invoiceLine->setItemPrice($krav->beløp);
            $invoiceSpecification->addInvoiceLine($invoiceLine);
        }

        $alleredeInnbetalt = $regning->hentBeløp() - $regning->hentUtestående();
        if ($alleredeInnbetalt > 0) {
            $invoiceLine = new InvoiceSpecification\InvoiceLine();
            $invoiceLine->setItemName('Allerede innbetalt');
            $invoiceLine->setLineAmount(-$alleredeInnbetalt);
            $invoiceSpecification->addInvoiceLine($invoiceLine);
        }

        $invoice = new Invoice();
        $invoice
            ->setInvoiceNumber($regning->hentId())
            ->setEfakturaIdentifier($eFakturaIdentifier)
            ->setIssuer($efakturaIssuer)
            ->setFirstName($regningsperson->fornavn)
            ->setLastName($regningsperson->etternavn)
            ->setAddressLine1($adresse->adresse1)
            ->setAddressLine2($adresse->adresse2)
            ->setPostalCode($adresse->postnr)
            ->setCity($adresse->poststed)
            ->setKid($regning->hentKid())
            ->setAccountNumber($fakturautstederKontonummer)
            ->setDueDate($regning->hentForfall())
            ->setAmount($regning->hentUtestående())
            ->setDocumentType($documentType)
            ->setInvoiceDocumentType('INVOICE')
            ->setInvoiceSpecification($invoiceSpecification)
            ->setPdf($regning->hentPdf())
        ;
        return $this->app->post($this, __FUNCTION__, $invoice);
    }

    /**
     * @param Regningsett $regningssett
     * @return $this
     * @throws Exception
     */
    public function oppdaterEfakturaStatus(Regningsett $regningssett): Prosessor
    {
        list('regningssett' => $regningssett) = $this->app->pre($this, __FUNCTION__, ['regningssett' => $regningssett]);
        $issuer = $this->hentEfakturaIssuer();
        $InvoiceStatusResponses = $this->hentEfakturaApiClient()->getInvoiceStatus($issuer, $regningssett->hentIdNumre());
        foreach ($InvoiceStatusResponses as $invoiceStatusResponse) {
            $regning = $this->app->hentModell(Regning::class, $invoiceStatusResponse->invoiceNumber);
            if($invoiceStatusResponse->status == Efaktura::INVOICE_REJECTED) {
                $this->app->logger->warning('NETS – Avvist eFaktura: ' . $invoiceStatusResponse->rejectReason, ['regning'=> $invoiceStatusResponse->invoiceNumber]);
                $regning->slettEfaktura();
                $regning->settUtskriftsdato();
            }
            if($efaktura = $regning->hentEfaktura()) {
                if($invoiceStatusResponse->status == Efaktura::INVOICE_PROCESSED) {
                    $efaktura->sett('status', Efaktura::OK);
                    if(!$regning->utskriftsdato) {
                        $regning->settFormat(Regning::FORMAT_EFAKTURA);
                        $regning->settUtskriftsdato($efaktura->forsendelsesdato);
                    }
                    if(!$efaktura->kvittertDato) {
                        $efaktura->settKvittertDato(new DateTimeImmutable());
                    }
                }
                $efaktura->settStatus($invoiceStatusResponse->status);
            }
        }
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @return EfakturaApiClient
     * @throws SoapFault
     */
    protected function hentEfakturaApiClient(): EfakturaApiClient
    {
        $this->app->pre($this, __FUNCTION__, []);
        if(!isset($this->efakturaApiClient)) {
            $this->efakturaApiClient = new EfakturaApiClient(
                $this->efakturaApiConfig['brukernavn'] ?? '',
                $this->efakturaApiConfig['passord'] ?? '',
                $this->efakturaApiConfig['endpoint_url'] ?? '',
                $this->efakturaApiConfig['certificate']['location'] ?? null,
                $this->efakturaApiConfig['certificate']['private_key'] ?? null,
                $this->efakturaApiConfig['certificate']['passphrase'] ?? null,
                $this->hentSoapOptions()
            );
        }
        return $this->app->post($this, __FUNCTION__, $this->efakturaApiClient);
    }

    /**
     * @param Efaktura $efaktura
     * @return void
     * @throws ApiFault
     * @throws ServerFault
     * @throws SoapFault
     * @throws ValidationFault
     */
    public function trekkEfaktura(Efaktura $efaktura)
    {
        list('efaktura' => $efaktura) = $this->app->pre($this, __FUNCTION__, ['efaktura' => $efaktura]);
        $regning = $efaktura->regning;
        if(!$regning || !$regning->hentId()) {
            $efaktura->slett();
        }
        $issuer = $this->hentEfakturaIssuer();
        $documentType = $regning->hentFboTrekk() ? 'AVTALEGIRO' : 'EFAKTURA';
        $invoiceId = $this->hentEfakturaApiClient()->getCancellableInvoice(
            $issuer,
            $regning->hentForfall(),
            $regning->hentUtestående(),
            $documentType,
            filter_var($this->app->hentValg('bankkonto'), FILTER_SANITIZE_NUMBER_INT),
            $regning->hentKid(),
        );
        $this->hentEfakturaApiClient()->cancelInvoice($invoiceId);
        $efaktura->slett();
        return $this->app->post($this, __FUNCTION__, null);
    }

    /**
     * Faste Betalingsoppdrag - Varsle
     *
     * Sender varsel om fbo trekkrav som har blitt sendt siste 2 dager
     *
     * @param Leiebase $leiebase
     * @return Prosessor
     * @throws Throwable
     */
    public function fboVarsle(): Prosessor
    {
        $this->app->pre($this, __FUNCTION__, []);
        if (!$this->app->hentValg('avtalegiro')) {
            return $this->app->post($this, __FUNCTION__, $this);
        }

        /** @var Regning[] $efakturaRegninger */
        $efakturaRegninger = [];

        // Finn alle avtalegiro trekkrav som er mer enn 2 dager gamle,
        // men som det fortsatt ikke er sendt varsel for
        $fboTrekkSett = $this->app->hentSamling(Trekk::class)
            ->leggTilLeftJoin(Efaktura::hentTabell(),
                '`' . Trekk::hentTabell() . '`.`gironr` = `' . Efaktura::hentTabell() . '`.`giro`')
            ->leggTilFilter(['`' . Trekk::hentTabell() . '`.`varslet`' => null])
            ->leggTilFilter(['`' . Trekk::hentTabell() . '`.`egenvarsel`' => true])
            ->leggTilFilter(['`' . Trekk::hentTabell() . '`.`overføringsdato` <=' => date_create()->sub(new DateInterval('P2D'))->format('Y-m-d')])
            ->leggTilFilter(['`' . Trekk::hentTabell() . '`.`forfallsdato` <' => date_create()->add(new DateInterval('P12M'))->format('Y-m-d')])
            ->leggTilFilter(['`' . Efaktura::hentTabell() . '`.`giro`' => null])
            ->låsFiltre();

        /** @var Trekk $trekk */
        foreach ($fboTrekkSett as $trekk) {
            /** @var Regning|null $regning */
            $regning = $trekk->hentRegning();
            /** @var Leieforhold $leieforhold */
            $leieforhold = $trekk->hentLeieforhold();
            /** @var Fbo $fbo */
            $fbo = $leieforhold->hentFbo();
            $efakturaIdentifier = $leieforhold->hentEfakturaIdentifier();

            // Hopp over ubrukte giroer
            if (!$regning || !$regning->hentForfall()) {
                continue;
            }

            // Send varsel som eFaktura
            if ($this->app->hentValg('efaktura') && $efakturaIdentifier) {
                $efakturaRegninger[] = $regning;
            } else if ($fbo && $fbo->hentVarsel()) {
                // Send varsel som epost
                if ($leieforhold->hentGiroformat() == 'epost') {
                    try {
                        $this->app->logger->info('NETS – Sender varsel om trekkrav som epostfaktura.', ['regning_id' => $regning->hentId()]);
                        $regning->sendEpost();
                        $trekk->varslet = new DateTime();
                    } catch (Exception $e) {
                        $this->app->logger->warning('NETS – Kunne ikke sende epostvarsel. ' . $e->getMessage());
                    }
                }
            }
        }

        if ($efakturaRegninger) {
            /** @var Regningsett $efakturaRegningssett */
            $efakturaRegningssett = $this->app->hentSamling(Regning::class)->lastFra($efakturaRegninger);
            $this->app->logger->info('NETS – Køer efaktura som varsel for ' . $efakturaRegningssett->hentAntall() . ' AvtaleGiro-trekk.');
            $this->app->hentUtskriftsprosessor()->leggTilEfakturaKø($efakturaRegningssett);
        }
        return $this->app->post($this, __FUNCTION__, $this);
    }
}