<?php

namespace Kyegil\Leiebasen\Nets;

use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Throwable;

/**
 *
 */
class Kid
{
    /**
     * @var int[]
     */
    public static array $leieforholdIdIKid = [];
    /**
     * @var Kid
     */
    private static Kid $instance;
    /**
     * @var Leiebase
     */
    protected Leiebase $app;
    /**
     * @var array
     */
    protected array $lengde;
    /**
     * @var array
     */
    protected array $start;
    /**
     * @var array|string[]
     */
    protected array $elementOrden;
    /**
     * @var int
     */
    protected int $modulus = 10;

    /**
     * @param Leiebase $app
     * @return Kid
     */
    public static function getInstance(Leiebase $app): Kid
    {
        if (!isset(self::$instance)) {
            self::$instance = new self($app);
        }
        return self::$instance;
    }

    /**
     * @param string $tallsekvens
     * @param int<10, 11> $modulus 10 eller 11
     * @return string
     */
    public static function kontrollsiffer(string $tallsekvens, int $modulus = 10): string
    {
        // Det lages et array av KID-strengen, sånn at den kan
        // behandles fra høyre mot venstre
        $sifferliste = str_split( strrev( $tallsekvens ) );

        // Sifrene multipliseres med vekttallene 2 1 2 1 ... regnet fra høyre mot venstre
        foreach( $sifferliste as $plass => $verdi ){

            if( ( $plass/2 ) == (int)($plass/2) ) {

                // Siste siffer, og så annethvert siffer fra høyre mot venstre, dobles
                $sifferliste[ $plass ] = (string)( $verdi * 2 );
            }

            // Etter at enkelte siffer har blitt doblet
            // beregnes tverrsummen av hver posisjon
            $sifferliste[ $plass ] = self::tverrsum( $sifferliste[ $plass ], false );
        }

        // Så beregnes tverrsummen av hele strengen
        $tverrsum = array_sum($sifferliste);

        // Entallsifferet i siffersummen trekkes fra 10 og resultatet blir kontrollsifferet
        $resultat = $modulus - $tverrsum % $modulus;

        // Dersom Entallssifferet i siffersgummen blir 0, blir kontrollsifferet 0
        if($resultat == 10) {
            $resultat = $modulus == 11 ? '-' : '0';
        }
        return $resultat;
    }

    /**
     * Finn tverssummen av et tall (alle sifrene sammenlagt)
     *
     * @param int $verdi
     * @param bool $minsteTverrsum Sann for å fortsette til vi har bare ett siffer
     * @return int
     */
    public static function tverrsum(int $verdi, bool $minsteTverrsum = true): int
    {
        settype( $verdi, 'string' );

        $sifferliste = str_split( strrev( $verdi ) );
        $resultat = array_sum( $sifferliste );

        if(($resultat > 9) && $minsteTverrsum) {
            $resultat = self::tverrsum( $resultat );
        }
        return $resultat;
    }

    /**
     *
     */
    private function __construct(Leiebase $app)
    {
        $this->app = $app;
        $this->lengde['k'] = $app::$config['leiebasen']['nets']['kid']['kundenummer_lengde'] ?? 5;
        $this->lengde['b'] = $app::$config['leiebasen']['nets']['kid']['betalingstype_lengde'] ?? 1;
        $this->lengde['r'] = $app::$config['leiebasen']['nets']['kid']['referanse_lengde'] ?? 6;
        $this->elementOrden = $app::$config['leiebasen']['nets']['kid']['element_orden'] ?? ['k','b','r'];
        $this->modulus = $app::$config['leiebasen']['nets']['kid']['modulus'] ?? 10;

        $start = 0;
        foreach($this->elementOrden as $element) {
            $this->start[$element] = $start;
            $start += $this->lengde[$element];
        }
    }

    /**
     * @param int $kundenummer
     * @param int $betalingstype
     * @param int $referanse
     * @return string
     * @throws NetsException
     */
    public function genererKid(int $kundenummer, int $betalingstype = 0, int $referanse = 0): string
    {
        list('kundenummer' => $kundenummer, 'betalingstype' => $betalingstype, 'referanse' => $referanse)
            = $this->app->pre($this, __FUNCTION__, ['kundenummer' => $kundenummer, 'betalingstype' => $betalingstype, 'referanse' => $referanse]);
        $kundenummer = $this->lengde['k'] ?
            str_pad($kundenummer, $this->lengde['k'], '0', STR_PAD_LEFT)
            : '';
        $betalingstype = $this->lengde['b'] ?
            str_pad($betalingstype, $this->lengde['b'], '0', STR_PAD_LEFT)
            : '';
        $referanse = $this->lengde['r'] ?
            str_pad($referanse, $this->lengde['r'], '0', STR_PAD_LEFT)
            : '';
        if( strlen($kundenummer) != $this->lengde['k'] ) {
            throw new NetsException('Ugyldig kundenummer');
        }
        if( strlen($betalingstype) != $this->lengde['b'] ) {
            throw new NetsException('Ugyldig betalingstype');
        }
        if( strlen($referanse) != $this->lengde['r'] ) {
            throw new NetsException('Ugyldig referanse');
        }

        $resultat = '';
        foreach ($this->elementOrden as $element) {
            switch ($element) {
                case 'k':
                    $resultat .= $kundenummer;
                    break;
                case 'b':
                    $resultat .= $betalingstype;
                    break;
                case 'r':
                    $resultat .= $referanse;
                    break;
            }
        }

        // Legg til kontrollsifferet til slutt
        $resultat .= Kid::kontrollsiffer($resultat, $this->modulus);
        return $this->app->post($this, __FUNCTION__, $resultat);
    }

    /**
     * Trekk ut kundenummer fra KID
     *
     * @param string $kid
     * @return int|null
     * @throws \Exception
     */
    public function hentKidKundenummer(string $kid): ?int
    {
        list('kid' => $kid) = $this->app->pre($this, __FUNCTION__, ['kid' => $kid]);
        $kidKundenummer = intval(substr($kid, $this->start['k'], $this->lengde['k']));
        return $this->app->post($this, __FUNCTION__, $kidKundenummer);
    }

    /**
     * Trekk ut betalingstype fra KID
     *
     * @param string $kid
     * @return int|null
     * @throws \Exception
     */
    public function hentKidBetalingstype(string $kid): ?int
    {
        list('kid' => $kid) = $this->app->pre($this, __FUNCTION__, ['kid' => $kid]);
        $kidBetalingstype = intval(substr($kid, $this->start['b'], $this->lengde['b']));
        return $this->app->post($this, __FUNCTION__, $kidBetalingstype);
    }

    /**
     * Trekk ut referanse fra KID
     * *
     * @param string $kid
     * @return int|null
     * @throws \Exception
     */
    public function hentKidReferanse(string $kid): ?int
    {
        list('kid' => $kid) = $this->app->pre($this, __FUNCTION__, ['kid' => $kid]);
        $kidReferanse = intval(substr($kid, $this->start['r'], $this->lengde['r']));
        return $this->app->post($this, __FUNCTION__, $kidReferanse);
    }

    /**
     * Hent leieforhold-id fra KID
     *
     * @param string $kid
     * @return int|null
     * @throws Throwable
     */
    public function hentLeieforholdIdFraKid(string $kid): ?int
    {
        list('kid' => $kid) = $this->app->pre($this, __FUNCTION__, ['kid' => $kid]);
        $kid = trim($kid);
        if(!array_key_exists($kid, self::$leieforholdIdIKid)) {
            if (strlen($kid) == 1 + array_sum($this->lengde)) {
                self::$leieforholdIdIKid[$kid] = intval(substr($kid, $this->start['k'], $this->lengde['k']));
            }
            else if (strlen($kid) == 7) {
                if (substr($kid, 0, 1) == 1) {
                    self::$leieforholdIdIKid[$kid] = intval(substr($kid, 1, 5));
                }
                else if (substr($kid, 0, 1) == 2) {
                    /** @var Regning $regning */
                    $regning = $this->app->hentSamling(Regning::class)
                        ->leggTilFilter(['kid' => $kid])
                        ->hentFørste();
                    self::$leieforholdIdIKid[$kid] = $regning ? $regning->leieforhold->hentId() : null;
                }
            }
        }
        $leieforholdId = self::$leieforholdIdIKid[$kid] ?? null;
        return $this->app->post($this, __FUNCTION__, $leieforholdId);
    }
}