<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 13/11/2020
 * Time: 11:54
 */

namespace Kyegil\Leiebasen\Nets;


use DateTime;
use DateTimeZone;
use Exception;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Fbo;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning\Efaktura;
use Kyegil\Leiebasen\Modell\Ocr\Fil;
use Kyegil\Leiebasen\Modell\Ocr\Filsett;
use Kyegil\Leiebasen\Modell\Ocr\Transaksjon;
use Kyegil\Leiebasen\Modell\Ocr\Transaksjonsett;
use Kyegil\Nets\Forsendelse;
use Kyegil\Nets\Forsendelse\AbstractOppdrag;
use phpseclib\Crypt\RSA;
use phpseclib\Net\SFTP;
use phpseclib\Net\SSH2;
use stdClass;
use Throwable;

/**
 * Class Oppkopling
 * @package Kyegil\Leiebasen\Nets
 */
class Oppkopling
{
    /**
     *
     */
    const TJENESTE_OCR = 9;

    /**
     *
     */
    const TJENESTE_AVTALEGIRO = 21;

    /**
     *
     */
    const TJENESTE_EFAKTURA = 42;

    /** @var int */
    protected $tjeneste;

    /** @var Leiebase */
    protected $app;

    /** @var string */
    protected $filarkiv;

    /** @var bool */
    protected $innlogget = false;

    /** @var string */
    protected $host;

    /** @var string */
    protected $port;

    /** @var int */
    protected $timeout = 40;

    /** @var string */
    protected $bruker;

    /** @var RSA */
    protected $key;

    /** @var SFTP */
    protected $sftp;

    /** @var stdClass */
    protected $postInn;

    /** @var stdClass */
    protected $postUt;

    /**
     * @var array
     */
    protected $oppdragUt = [];

    /**
     * Inspeksjonsmodus
     *
     * I inspeksjonsmodus kan du kople til server og se innholdet, men ikke foreta transaksjoner
     * @var bool
     */
    protected $inspeksjonsModus = true;

    /**
     * Oppkopling constructor.
     * @param Leiebase $app
     * @param int $tjeneste
     * @param string $filarkiv
     * @param string|null $bruker
     * @param RSA|null $key
     */
    public function __construct(
        Leiebase $app,
        int      $tjeneste,
        string   $filarkiv,
        string   $bruker = null,
        RSA      $key = null
    )
    {
        $this->app = $app;
        $this->tjeneste = $tjeneste;
        $this->filarkiv = $filarkiv;
        $this->key = $key;
        $this->postInn = new stdClass();
        $this->postUt = new stdClass();
        if(!isset($bruker)) {
            if(in_array($tjeneste, [self::TJENESTE_OCR, self::TJENESTE_AVTALEGIRO])) {
                $bruker = Leiebase::$config['leiebasen']['nets']['user']['ocr'] ?? null;
            }
            elseif($tjeneste == self::TJENESTE_EFAKTURA) {
                $bruker = Leiebase::$config['leiebasen']['nets']['user']['einvoice'] ?? null;
            }
        }
        $this->bruker = $bruker;
    }

    /**
     * Gjør et siste forsøk på å sende gjenglemte filer
     *
     * @throws NetsException
     */
    public function __destruct()
    {
        $this->app->logger->debug('Behandler gjenglemt post.');
        $this->sendUtgåendePost();
        $this->behandleInnkommenPost();
    }

    /**
     * @return RSA
     * @throws NetsException
     */
    public function hentRsaKey(): RSA
    {
        if(!isset($this->key)) {
            $this->key = new RSA();
            $this->key->setPassword(
                !empty(Leiebase::$config['leiebasen']['nets']['key_pw'])
                ? Leiebase::$config['leiebasen']['nets']['key_pw']
                : false
            );
            if(is_readable(Leiebase::$config['leiebasen']['nets']['rsa_key'] ?? '')) {
                $this->key->loadKey(file_get_contents(Leiebase::$config['leiebasen']['nets']['rsa_key'] ?? ''));
            }
            else {
                throw new NetsException("Kan ikke lese " . (Leiebase::$config['leiebasen']['nets']['rsa_key'] ?? ''));
            }

        }
        return $this->key;
    }

    /**
     * @return SFTP
     */
    public function hentSftp(): SFTP
    {
        if(!isset($this->sftp)) {
            $this->sftp = new SFTP($this->hentHost(), $this->hentPort(), $this->hentTimeOut());
        }
        return $this->sftp;
    }

    /**
     * @return bool
     */
    public function hentInspeksjonsModus(): bool
    {
        return $this->inspeksjonsModus;
    }

    /**
     * @param bool $inspeksjonsModus
     * @return Oppkopling
     */
    public function settInspeksjonsModus(bool $inspeksjonsModus): Oppkopling
    {
        $this->inspeksjonsModus = $inspeksjonsModus;
        return $this;
    }

    /**
     *
     */
    public function hentNyeOCrFiler()
    {
        $this->hentSftp();
    }

    /**
     * @return string|null
     */
    public function hentHost(): ?string
    {
        if(!isset($this->host)) {
            $this->host = Leiebase::$config['leiebasen']['nets']['host'] ?? '';
        }
        return $this->host;
    }

    /**
     * @return int
     */
    public function hentPort()
    {
        if(!isset($this->port)) {
            $this->port = Leiebase::$config['leiebasen']['nets']['port'] ?? 22;
        }
        return $this->port;
    }

    /**
     * @return int
     */
    public function hentTimeout(): int
    {
        return $this->timeout;
    }

    /**
     * @param string $host
     * @return $this
     */
    public function settHost(string $host): Oppkopling
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @param int $timeout
     * @return Oppkopling
     */
    public function settTimeout(int $timeout): Oppkopling
    {
        $this->timeout = $timeout;
        return $this;
    }

    /**
     * @param int $port
     * @return $this
     */
    public function settPort($port): Oppkopling
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return int
     */
    public function hentTjeneste(): int
    {
        return $this->tjeneste;
    }

    /**
     * @param string|null $bruker
     * @return $this
     * @throws NetsException
     */
    public function opprettForbindelse($bruker = null): Oppkopling
    {
        if($bruker && ($bruker != $this->hentBruker())) {
            $this->logg("Innlogging er påkrevd");
            $this->innlogget = false;
        }

        if(!$this->innlogget) {
            if (!defined('NET_SFTP_LOGGING')) {
                define('NET_SFTP_LOGGING', SSH2::LOG_COMPLEX);
            }
            $bruker = $bruker ?: $this->hentBruker();
            $this->settBruker($bruker);
            $key = $this->hentRsaKey();
            if($this->innlogget = $this->hentSftp()->login($bruker, $key)) {
                $this->app->logger->debug("Koplet til NETS som bruker {$bruker}");
            }
        }
        if(!$this->innlogget) {
            throw new NetsException("SFTP innlogging mislyktes for bruker {$bruker}");
        }
        return $this;
    }

    /**
     * @return string|null
     */
    public function hentBruker(): ?string
    {
        return $this->bruker;
    }

    /**
     * @param string $bruker
     * @return Oppkopling
     */
    public function settBruker(string $bruker): Oppkopling
    {
        $this->bruker = $bruker;
        return $this;
    }

    /**
     * @return stdClass
     */
    public function hentPostInn(): stdClass
    {
        return $this->postInn;
    }

    /**
     * @return stdClass
     */
    public function hentPostUt(): stdClass
    {
        return $this->postUt;
    }

    /**
     * @return stdClass[]|Forsendelse\AbstractOppdrag[]
     */
    public function hentOppdragUt(): array
    {
        return $this->oppdragUt;
    }

    /**
     * @return $this
     */
    public function slettOppdragUt(): Oppkopling
    {
        $this->oppdragUt = [];
        return $this;
    }

    /**
     * @param stdClass|Forsendelse\AbstractOppdrag $oppdrag
     * @return Oppkopling
     */
    public function leggTilOppdragUt($oppdrag): Oppkopling
    {
        $this->oppdragUt[] = $oppdrag;
        return $this;
    }

    /**
     * Legg til fil for overføring til NETS
     *
     * @param string $filbane
     * @param string|null $innhold
     * @return $this
     */
    public function leggTilUtgåendePost(string $filbane, string $innhold = null): Oppkopling
    {
        if(!property_exists($this->postUt, $filbane)) {
            $innhold = $innhold ?? file_get_contents($filbane);
            $this->postUt->{$filbane} = $innhold;
        }
        return $this;
    }

    /**
     * Fjern fil fra utgående post
     *
     * @param string $filbane
     * @return $this
     */
    public function fjernFraUtgåendePost(string $filbane): Oppkopling
    {
        if(property_exists($this->postUt, $filbane)) {
            unset($this->postUt->{$filbane});
            $this->app->logger->debug('Fjernet fil fra utgående post', ['fil' => $filbane]);
        }
        return $this;
    }

    /**
     * Returnerer alle tilgjengelige filer hos Nets,
     * relativt til / (rot)
     * Ocr filer befinner seg i /Outbound/
     * AvtaleGiro forsendelseskvitteringer befinner seg i /Inbound/
     *
     * @return string[]
     * @throws NetsException
     */
    public function seEtterFilerForNedlasting(): array
    {
        $this->app->logger->debug("Ser etter filer hos NETS");
        $this->opprettForbindelse();
        $sftp = $this->hentSftp();
        if(!$sftp->chdir("/Outbound/")) {
            throw new NetsException("Klarte ikke flytte til /Outbound/ hos Nets");
        }
        /** @var string[] $obFiler */
        $obFiler = $sftp->nlist() ?: [];
        array_walk($obFiler, function(&$value) {
            $value = "/Outbound/{$value}";
        });
        if(!$sftp->chdir("/Inbound/")) {
            throw new NetsException("Klarte ikke flytte til /Inbound/ hos Nets");
        }
        /** @var string[] $ibFiler */
        $ibFiler = $sftp->nlist() ?: [];
        array_walk($ibFiler, function(&$value) {
            $value = "/Inbound/{$value}";
        });

        $filer = array_merge($obFiler, $ibFiler);
        $filer = array_diff($filer, ['/Inbound/NOR' . preg_replace('/[^0-9]+/', '', $this->app->hentValg('orgnr'))]);
        if($filer) {
            $this->logg(count($filer) . " filer tilgjengelig hos Nets", ['filer' => $filer]);
        }
        else {
            $this->app->logger->debug("Ingen filer funnet.");
        }
        return $filer;
    }

    /**
     * Laster ned filer fra NETS, og lagrer på riktig sted i filarkivet
     * Filbanene på NETS' tjener forventes angitt i forhold til '/' (rot)
     *
     * @param string[] $filerForNedlasting
     * @throws NetsException
     */
    public function lastNedFraNets(array $filerForNedlasting): array
    {
        $this->opprettForbindelse();

        $lokaleFiler = [];
        foreach($filerForNedlasting as $fil) {
            if(in_array($this->hentTjeneste(), [self::TJENESTE_OCR, self::TJENESTE_AVTALEGIRO])) {
                if(strpos($fil, "/Outbound/") !== false && strpos($fil, "OCR.") !== false) {
                    $lokaleFiler[] = $this->lastNedOcr($fil);
                }
                else{
                    $lokaleFiler[] = $this->lastNedAvtaleGiroKvittering($fil);
                }
            }
            elseif ($this->hentTjeneste() == self::TJENESTE_EFAKTURA) {
                $lokaleFiler[] = $this->lastNedEfakturafil($fil);
            }
            else {
                $this->logg('Hopper over ' . $fil);
            }
        }
        return $lokaleFiler;
    }

    /**
     * Laster ned OCR-fil fra NETS, og lagrer på egnet sted i filarkivet.
     * Filbanen på NETS' tjener forventes angitt relativ til '/' (rot)
     * Den lokale filbanen oppgis som egenskap i postInn-objektet
     *
     * @param string $fil Ekstern filbane
     * @return string Intern filbane
     * @throws NetsException
     */
    protected function lastNedOcr(string $fil): string
    {
        $this->opprettForbindelse(Leiebase::$config['leiebasen']['nets']['user']['ocr'] ?? null);
        $mnd = date('Y-m');

        if (strpos($fil, "OCR.") === false) {
            throw new NetsException("Dette ser ikke ut til å være ei ocr-fil: {$fil}");
        }

        if(!file_exists("{$this->filarkiv}/nets/inn/ocr/{$mnd}")) {
            mkdir("{$this->filarkiv}/nets/inn/ocr/{$mnd}");
        }

        $lokalFil = "{$this->filarkiv}/nets/inn/ocr/{$mnd}/" . basename($fil);

        if ( !$this->app->live) {
            throw new NetsException("Leiebasen er i testmodus. Filoverføringer ikke tilgjengelig.");
        }

        if( $this->hentSftp()->get($fil, $lokalFil) ) {
            $this->postInn->{$lokalFil} = null;
            $this->logg("Lastet ned {$fil} til {$lokalFil}");
        }
        else {
            throw new NetsException("Leiebasen klarte ikke laste ned følgende fil: {$fil}.<br>Denne fila må lastes ned manuelt fra NETS.");
        }
        return $lokalFil;
    }

    /**
     * Laster ned AvtaleGiro kvitteringsfil fra NETS, og lagrer på egnet sted i filarkivet.
     * Filbanen på NETS' tjener forventes angitt relativ til '/' (rot)
     * Den lokale filbanen oppgis som egenskap i postInn-objektet
     *
     * @param string $fil Ekstern fil
     * @return string Intern fil
     * @throws NetsException
     */
    protected function lastNedAvtaleGiroKvittering(string $fil): string
    {
        $this->opprettForbindelse();
        $mnd = date('Y-m');

        if (strpos($fil, "KV.") === false) {
            throw new NetsException("Dette ser ikke ut til å være ei AvtaleGiro-kvittering: {$fil}");
        }

        if(!file_exists("{$this->filarkiv}/nets/inn/AG-kvitteringer/{$mnd}")) {
            mkdir("{$this->filarkiv}/nets/inn/AG-kvitteringer/{$mnd}");
        }

        $lokalFil = "{$this->filarkiv}/nets/inn/AG-kvitteringer/{$mnd}/" . basename($fil);

        if ( !$this->app->live) {
            throw new NetsException("Leiebasen er i testmodus. Filoverføringer ikke tilgjengelig.");
        }

        if( $this->hentSftp()->get($fil, $lokalFil) ) {
            $this->postInn->{$lokalFil} = null;
            $this->logg("Lastet ned {$fil} til {$lokalFil}");
        }
        else {
            throw new NetsException("Leiebasen klarte ikke laste ned følgende fil: {$fil}.<br>Denne fila må lastes ned manuelt fra NETS.");
        }
        return $lokalFil;
    }

    /**
     * @param stdClass|null $postInn
     * @return Oppkopling
     * @throws NetsException
     */
    public function behandleInnkommenPost(stdClass $postInn = null): Oppkopling
    {
        $postInn = $postInn ?: $this->postInn;
        if((array)$postInn) {
            $this->logg('Starter behandling av innkommen post');
            $efakturaOppdragTilNets = [];
            foreach ($postInn as $filbane => &$innhold) {
                $this->logg("Behandler {$filbane}");
                if(!isset($innhold)) {
                    $innhold = file_get_contents($filbane);
                    $innhold = mb_convert_encoding(
                        $innhold,
                        'UTF-8',
                        mb_detect_encoding($innhold, 'UTF-8, ISO-8859-1', true)
                    );
                }
                if(substr(trim($innhold), 0, 2) == 'NY') {
                    $this->logg("Forsendelse i BBS flatfil-format");
                    $forsendelse = new Forsendelse($innhold);
                    if(!$forsendelse->valider()) {
                        throw new NetsException("Automatisk forsøk på å hente forsendelse fra NETS<br>mislyktes "
                            . date('d.m.Y') . "  kl. " . date('H:i:s')
                            . ".<br>{$filbane} inneholder feil.");
                    }

                    if($this->hentTjeneste() == self::TJENESTE_OCR
                        || $this->hentTjeneste() == self::TJENESTE_AVTALEGIRO
                    ) {
                        try {
                            $this->registrerOcrKonteringsdata($forsendelse);
                        } catch (Exception $e) {
                            $this->app->logger->critical($e->getMessage(), $e->getTrace());
                        }
                        try {
                            $this->registrerFbo($forsendelse);
                        } catch (Exception $e) {
                            $this->app->logger->critical($e->getMessage(), $e->getTrace());
                        }
                    }
                    elseif ($this->hentTjeneste() == self::TJENESTE_EFAKTURA) {
                        try {
                            $this->registrerEfakturaKvitteringer($forsendelse);
                        } catch (Exception $e) {
                            $this->app->logger->critical($e->getMessage(), $e->getTrace());
                        }
                    }
                }
                else if(substr($filbane, -5) == '.html') {
                    $this->logg("Forsendelse i HTML-format");
                    try {
                        $this->behandleAvtalegiroTrekkKvitteringer($filbane, $innhold);
                    } catch (NetsException $e) {
                        $this->app->logger->warning($e->getMessage());
                    }
                }
                else {
                    $this->app->logger->warning('Fil i ukjent format: ' . $filbane);
                }
                unset($this->postInn->{$filbane});
            }
            if($efakturaOppdragTilNets) {
                $this->lagUtgåendeForsendelse($efakturaOppdragTilNets);
            }
            $this->logg('Behandling av innkommen post fullført');
        }
        return $this;
    }

    /**
     * @param string $fil Ekstern fil
     * @return string Intern fil
     * @throws NetsException
     */
    protected function lastNedEfakturafil(string $fil): string
    {
        $this->opprettForbindelse(Leiebase::$config['leiebasen']['nets']['user']['einvoice'] ?? null);
        $mnd = date('Y-m');

        if (strpos($fil, ".bbs") === false) {
            throw new NetsException("Dette ser ikke ut til å være ei efaktura-fil: {$fil}");
        }

        if(!file_exists("{$this->filarkiv}/nets/inn/efaktura/{$mnd}")) {
            mkdir("{$this->filarkiv}/nets/inn/efaktura/{$mnd}");
        }

        $lokalFil = "{$this->filarkiv}/nets/inn/efaktura/{$mnd}/" . basename($fil);

        if ( !$this->app->live) {
            throw new NetsException("Leiebasen er i testmodus. Filoverføringer ikke tilgjengelig.");
        }

        if( $this->hentSftp()->get($fil, $lokalFil) ) {
            $this->postInn->{$lokalFil} = null;
            $this->logg("Lastet ned {$fil} til {$lokalFil}");
        }
        else {
            throw new NetsException("Leiebasen klarte ikke laste ned følgende fil: {$fil}.<br>Denne fila må lastes ned manuelt fra NETS.");
        }
        return $lokalFil;
    }

    /**
     * @param $tekst
     * @param array $kontekst
     */
    public function logg($tekst, array $kontekst = [])
    {
        $this->app->logger->info('NETS – ' . $tekst, $kontekst);
    }

    /**
     * @param Forsendelse $forsendelse
     * @return $this
     * @throws NetsException
     * @throws Exception
     */
    public function registrerOcrKonteringsdata(Forsendelse $forsendelse): Oppkopling
    {
        if (
            $forsendelse->datamottaker != $this->app->hentValg('nets_kundeenhetID')
            || $forsendelse->dataavsender != 8080
            || !$forsendelse->valider()
        ) {
            throw new NetsException("Forsendelse {$forsendelse->forsendelsesnummer} er ugyldig");
        }

        $this->logg("Ser etter OCR konteringsdata (tjeneste 9) i forsendelse {$forsendelse->forsendelsesnummer}");

        /** @var stdClass|AbstractOppdrag $oppdrag */
        foreach ($forsendelse->oppdrag as $oppdrag ) {
            if(
                $oppdrag->tjeneste == 9
                && $oppdrag->avtaleId == $this->app->hentValg('nets_avtaleID_ocr')
            ) {
                $this->logg("OCR konteringsdata (tjeneste 9) funnet i forsendelse {$forsendelse->forsendelsesnummer}");

                $oppgjørsdatoFilter = ['oppgjørsdato' => null];
                if($forsendelse->dato instanceof DateTime) {
                    $oppgjørsdatoFilter = ["oppgjørsdato BETWEEN ('{$forsendelse->dato->format('Y-m-d')}' - INTERVAL 7 DAY ) AND ('{$forsendelse->dato->format('Y-m-d')}' + INTERVAL 7 DAY )"];
                }

                /** @var Filsett $registrerteOcrFiler */
                $registrerteOcrFiler = $this->app->hentSamling(Fil::class)
                    ->leggTilFilter([
                        'forsendelsesnummer' => $forsendelse->forsendelsesnummer])
                    ->leggTilFilter($oppgjørsdatoFilter);

                if($registrerteOcrFiler->hentAntall()) {
                    $this->logg("Forsendelsesfil {$forsendelse->forsendelsesnummer} finnes fra før");
                    $ocrFil = $registrerteOcrFiler->hentFørste();
                }
                else {
                    $ocrFil = $this->app->nyModell(Fil::class, (object)[
                        'forsendelsesnummer'	=> $forsendelse->forsendelsesnummer,
                        'oppgjørsdato'			=> $oppdrag->oppgjørsdato,
                        'ocr'					=> strval($forsendelse)
                    ]);
                    if(!$ocrFil->hentId()) {
                        throw new NetsException("Klarte ikke lagre oppdrag {$oppdrag->oppdragsnr} i forsendelsesfil {$forsendelse->forsendelsesnummer} i databasen");
                    }
                    $this->logg("Forsendelsesfil {$forsendelse->forsendelsesnummer} er lagret i databasen med id {$ocrFil->hentId()}");
                }

                /** @var Forsendelse\Oppdrag\Transaksjon|stdClass $transaksjon */
                foreach( $oppdrag->transaksjoner as $transaksjon ) {
                    /** @var Transaksjonsett $registrerteTransaksjoner */
                    $registrerteTransaksjoner = $this->app->hentSamling(Transaksjon::class)
                    ->leggTilFilter([
                        'forsendelsesnummer'	=> $forsendelse->forsendelsesnummer,
                        'transaksjonsnummer'	=> $transaksjon->transaksjonsnr
                    ]);
                    if($registrerteTransaksjoner->hentAntall()) {
                        $this->app->logger->warning("NETS – Transaksjon {$transaksjon->transaksjonsnr} finnes fra før");
                    }
                    else {
                        /** @var Transaksjon $ocrTransaksjon */
                        $ocrTransaksjon = $this->app->nyModell(Transaksjon::class, (object)[
                            'arkivreferanse'		=> $transaksjon->arkivreferanse,
                            'avtale_id'				=> $oppdrag->avtaleId,
                            'bankdatasentral'		=> $transaksjon->sentralId,
                            'beløp'					=> $transaksjon->beløp,
                            'blankettnummer'		=> $transaksjon->blankettnummer,
                            'debetkonto'			=> $transaksjon->debetKonto,
                            'delavregningsnummer'	=> $transaksjon->delavregningsnr,
                            'fil'					=> $ocrFil,
                            'forsendelsesnummer'	=> $forsendelse->forsendelsesnummer,
                            'fritekst'				=> $transaksjon->fritekstmelding,
                            'kid'					=> $transaksjon->kid,
                            'løpenummer'			=> $transaksjon->løpenr,
                            'oppdragsdato'			=> $transaksjon->oppdragsdato,
                            'oppdragskonto'			=> $oppdrag->oppdragskonto,
                            'oppdragsnummer'		=> $oppdrag->oppdragsnr,
                            'oppgjørsdato'			=> $transaksjon->oppgjørsdato,
                            'transaksjonsnummer'	=> $transaksjon->transaksjonsnr,
                            'transaksjonstype'		=> $transaksjon->transaksjonstype
                        ]);

                        if(!$ocrTransaksjon->hentId()) {
                            throw new NetsException("Klarte ikke lagre transaksjon {$transaksjon->transaksjonsnr} i databasen");
                        }

                        // OCR-fil og -detaljer er lagret. Nå opprettes en betaling
                        // på grunnlag av transaksjonen:

                        $this->logg("Transaksjon {$transaksjon->transaksjonsnr} er lagret i databasen med id {$ocrTransaksjon->hentId()}");
                        $transaksjon->id = $ocrTransaksjon->hentId();
                        $transaksjon->forsendelsesnummer = $forsendelse->forsendelsesnummer;
                        try {
                            $this->app->registrerBetaling($ocrTransaksjon);
                        } catch (Exception $e) {
                            $this->logg("Klarte ikke registrere innbetalinger for transaksjon {$transaksjon->transaksjonsnr}. {$e->getMessage()}");
                        }
                    }
                }
            }
        }
        return $this;
    }

    /**
     * @param Forsendelse $forsendelse
     * @throws NetsException
     * @throws Throwable
     */
    public function registrerFbo(Forsendelse $forsendelse ) {
        if (
            $forsendelse->datamottaker != $this->app->hentValg('nets_kundeenhetID')
            || $forsendelse->dataavsender != 8080
            || !$forsendelse->valider()
        ) {
            throw new NetsException("Forsendelse {$forsendelse->forsendelsesnummer} er ugyldig");
        }
        $oppdragskonto = preg_replace('/[^0-9]+/', '', $this->app->hentValg('bankkonto'));

        $this->logg("Ser etter FBO-avtaler (tjeneste 21) i forsendelse {$forsendelse->forsendelsesnummer}");

        foreach ($forsendelse->oppdrag as $oppdrag ) {
            if(
                $oppdrag->tjeneste == 21
                && $oppdrag->oppdragstype == 24
            ) {
                $this->logg("FBO-avtaler (tjeneste 21) funnet i forsendelse {$forsendelse->forsendelsesnummer}");
                if($oppdrag->oppdragskonto != $oppdragskonto) {
                    throw new NetsException('Oppdraget tilhører en annen oppdragskonto: ' . $oppdrag->oppdragskonto);
                }

                /** @var int[] $fboIderForSletting */
                $fboIderForSletting = [];

                // registreringstype: 0 = Oppdatering av hele FBO-oversikten
                if (
                    isset( $oppdrag->transaksjoner[0] )
                    && $oppdrag->transaksjoner[0]->registreringstype == 0
                ) {
                    $this->logg("Komplett FBO-liste mottatt");
                    //	For synking klargjøres alle for sletting med mindre de er med i oversikten fra NETS
                    $fboIderForSletting = $this->app->hentSamling(Fbo::class)->hentIdNumre();
                    $this->logg("Eksisterende FBO-avtaler markert for sletting.");
                }

                foreach( $oppdrag->transaksjoner as $transaksjon ) {
                    /** @var Leieforhold $leieforhold */
                    $leieforhold = $this->app->hentLeieforholdFraKid( $transaksjon->kid );

                    /** @var string|null $kidBetalingstype */
                    $kidBetalingstype = $this->app->hentKidBestyrer()->hentKidBetalingstype($transaksjon->kid);
                    $kidBetalingstype = Fbo::AVTALETYPER[$kidBetalingstype] ?? null;
                    $skriftligVarsel = $transaksjon->skriftligVarsel == 'J';

                    if ($transaksjon->registreringstype == 2) {
                        // registreringstype: 2 = Slettemelding
                        $registrertFbo = $leieforhold->hentFbo($kidBetalingstype);
                        if($registrertFbo) {
                            $registrertFbo->slett();
                        }
                        $this->logg("FBO for krav type '{$kidBetalingstype}' slettet for  leieforhold {$leieforhold->hentId()}.");
                    }

                    elseif ( $transaksjon->registreringstype < 2 ) {
                        // registreringstype: 0 = Bekreftelse (synkronisering av avtaler)
                        // registreringstype: 1 = Ny avtale / endring av eksisterende avtale

                        if($leieforhold && $leieforhold->hentId() && $kidBetalingstype !== null) {
                            /** @var Fbo|false $fbo */
                            $fbo = $leieforhold->hentFbo($kidBetalingstype);
                            if($fbo) {
                                $fbo->settVarsel($skriftligVarsel);
                                $this->app->logger->warning("Oppdrag om ny FBO-avtale for krav type '{$kidBetalingstype}' mottatt for leieforhold {$leieforhold->hentId()}, men den eksisterer allerede med id {$fbo->hentId()}.");
                                if (($idx = array_search($fbo->hentId(), $fboIderForSletting)) !== false) {
                                    unset($fboIderForSletting[$idx]);
                                }
                            }
                            else {
                                /** @var Fbo $fbo */
                                $fbo = $this->app->nyModell(Fbo::class, (object)[
                                    'leieforhold' => $leieforhold,
                                    'type' => $kidBetalingstype,
                                    'varsel' => $skriftligVarsel
                                ]);
                                $leieforhold->nullstill();
                                $this->logg("FBO-avtale {$fbo->hentId()} for krav type '{$kidBetalingstype}' registrert for  leieforhold {$leieforhold->hentId()}.");
                            }
                        }
                    }
                }

                // Alle avtaler som ikke er oppdatert slettes hvis de er forberedt for det
                /** @var Fbo $fbo */
                foreach($this->app->hentSamling(Fbo::class)->filtrerEtterIdNumre($fboIderForSletting)->låsFiltre() as $fbo) {
                    $this->logg("Rydder i FBO-avtaler, og sletter avtale {$fbo->hentId()} for krav type '{$fbo->hentType()}' for  leieforhold {$fbo->hentLeieforhold()->hentId()}, som var markert for sletting.");
                    $fbo->slett();
                }
            }
        }
    }

    /**
     * @param Forsendelse $forsendelse
     * @return $this
     * @throws NetsException
     */
    public function registrerEfakturaKvitteringer(Forsendelse $forsendelse): Oppkopling
    {
        if (
            $forsendelse->dataavsender != 8080
            || !$forsendelse->valider()
        ) {
            throw new NetsException("Forsendelse {$forsendelse->forsendelsesnummer} er ugyldig");
        }

        foreach ($forsendelse->oppdrag as $oppdrag ) {
            /*
            Oppdragstype 4:	Status for mottatt forsendelse
                Statusalternativer:
                    0 = Forsendelsen er mottatt av NETS (men ikke ferdig behandlet).
                        Feilkoden vil også være 0.
                    2 = Forsendelsen er i sin helhet forkastet.
                        I tillegg gis forklaring i form av feilkode.

            Oppdragstype 5: Status for prosessert forsendelse
                Statusalternativer:
                    1 = Forsendelsen er prosessert av NETS (men oppdrag og transaksjoner kan være forkastet).
                    2 = Forsendelsen er i sin helhet forkastet.
                        I tillegg gis forklaring i form av feilkode.

            Oppdragstype 6: Status for prosessert oppdrag
                Statusalternativer:
                    0 = Oppdraget er godkjent.
                    1 = Oppdraget er i sin helhet forkastet
                        I tillegg gis forklaring i form av feilkode
                Feilkode på fakturanivå for avviste fakturaer i prosessert oppdrag

            */

            if(
                $oppdrag->tjeneste == 42 // Efaktura
                && (
                    $oppdrag->oppdragstype == 4 // Mottatt forsendelse
                    || $oppdrag->oppdragstype == 5 // Prosessert forsendelse
                )
                // Oppdragstype 4 kvitterer for forsendelse mottatt av NETS
                // Oppdragstype 5 kvitterer for forsendelse ferdig prosessert av NETS
            ) {
                $this->logg("Efaktura (tjeneste 42) funnet i forsendelse {$forsendelse->forsendelsesnummer}");
                $oppdrag->dataavsender;		// Opprinnelig dataavsender (Vil muligens være blanket)
                $oppdrag->datamottaker;		// Opprinnelig datamottaker (=8080 NETS)
                $oppdrag->referanseFakturautsteder;
                $oppdrag->forsendelsesnr;	// Forsendelsen det kvitteres for
                $oppdrag->statusForsendelse;
                // 00 = Forsendelsen er mottatt i BBS (men ikke ferdig behandlet)
                // 01 = Forsendelsen i seg selv er ferdig behandlet
                // 02 = Forsendelsen er i sin helhet forkastet

                $oppdrag->feilkode;		// Feilkode dersom statusForsendelse = 2
                $oppdrag->feilmelding;	// Forklaring på feilkode

                if( $oppdrag->referanseFakturautsteder != $this->app->hentValg('efaktura_referansenummer') ) {
                    throw new NetsException("Et kvitteringsoppdrag for eFaktura er mottatt fra NETS med utsteder-referanse {$oppdrag->referanseFakturautsteder}. Denne stemmer ikke med referanse {$this->app->hentValg('efaktura_referansenummer')} som er oppgitt i innstillingene for leiebasen.");
                }

                switch($oppdrag->oppdragstype) {
                    case 4: // Mottatt forsendelse
                    case 5: // Prosessert forsendelse
                        /*****************************************
                        Dersom forsendelsesstatus == 2,
                        skal hele forsendelsen med alle efakturaene i den forkastes
                         */
                        if( $oppdrag->statusForsendelse == 2 ) {
                            // Hele forsendelsen er forkastet.
                            $this->forkastEfakturaforsendelse($oppdrag, $forsendelse);
                        }

                        /*****************************************
                        Dersom forsendelsesstatus == 0,
                        er efakturaene ikke ferdig behandlet, men vil få status 'IN_PROGRESS'
                         */
                        else if( $oppdrag->statusForsendelse == 0 ) {
                            // Den aktuelle forsendelsen er ikke ferdig behandlet.
                            // Efakturaene i forsendelsen får status 'IN_PROGRESS',
                            $this->registrerEfakturaForsendelseMottatt($oppdrag, $forsendelse);
                        }

                        /*****************************************
                        Dersom forsendelsesstatus == 1,
                        er forsendelsen ferdig behandlet.
                         */
                        else {
                            $this->behandleEfakturaforsendelsePåOppdragsnivå($oppdrag, $forsendelse);
                        }
                        break;
//                    case 6: // Prosessert oppdrag
//                        break;
                    default:
                        throw new NetsException("Ukjent oppdragstype {$oppdrag->oppdragstype} i efaktura-oppdrag.");
                }
            }
        }
        return $this;
    }

    /**
     * En hel efakturaforsendelse har blitt avvist av NETS.
     * Alle efakturaene i forsendelsen må slettes.
     *
     * @param $oppdrag
     * @param Forsendelse $forsendelse
     * @return $this
     * @throws Exception
     */
    protected function forkastEfakturaforsendelse($oppdrag, Forsendelse $forsendelse): Oppkopling
    {
        $this->logg("Forsendelse {$oppdrag->forsendelsesnr} har blitt forkastet i sin helhet av NETS");
        /** @var Regning $regning */
        foreach($this->app->hentSamling(Regning::class)
                    ->leggTilInnerJoin(['efakturaer' => Efaktura::hentTabell()], 'efakturaer.giro = giroer.gironr')
                    ->leggTilFilter([
                        'efakturaer.forsendelse' => $oppdrag->forsendelsesnr,
                        'efakturaer.status <>' => Efaktura::OK
                    ]) as $regning) {
            try {
                $regning->slettEfaktura();
                $regning->settUtskriftsdato();
                $this->logg("Efaktura for regning {$regning} har blitt slettet, og utskriftsdato fjernet.");
            } catch (Exception $e) {
                $this->app->logger->error("Klarte ikke slette efaktura for regning {$regning}");
            }
        }

        // Skriver et statusvarsel på driftsforsiden,
        // og sender en epost som forklarer feilen
        $melding = "En eFakturaforsendelse har blitt forkastet i sin helhet av NETS.<br>\nAlle eFakturaene i denne forsendelsen må derfor sendes på nytt når feilen er rettet.<br><br><b>Feilkode {$oppdrag->feilkode} fra NETS:</b><br>{$oppdrag->feilmelding}<br>\n";

        $this->app->sendMail( (object)array(
            'type' => 'efakturaforsendelse_forkastet',
            'auto'		=> true,
            'to'		=> $this->app->hentValg('epost'),
            'priority'	=> 90,
            'subject'	=> "Problemer med eFaktura forsendelse",
            'html'		=> $melding
        ) );

        $this->app->mysqli->save([
            'insert'		=> true,
            'table'			=> "internmeldinger",
            'fields'		=> [
                'tekst'		=> $melding,
                'drift'		=> true
            ]
        ]);
        return $this;
    }

    /**
     * Registrer efaktura mottakskvittering fra Nets
     *
     * @param $oppdrag
     * @param Forsendelse $forsendelse
     * @return $this
     * @throws NetsException
     */
    protected function registrerEfakturaForsendelseMottatt($oppdrag, Forsendelse $forsendelse): Oppkopling
    {
        // Den aktuelle forsendelsen er ikke ferdig behandlet.
        // Efakturaene i forsendelsen får status 'IN_PROGRESS',
        $this->logg("Mottatt efaktura-kvittering 'mottatt men ikke behandlet' for forsendelse " . $oppdrag->forsendelsesnr);
        try {
            $this->app->hentSamling(Efaktura::class)
                ->leggTilFilter([
                    'forsendelse' => $oppdrag->forsendelsesnr,
                    'status <>' => Efaktura::OK
                ])->settVerdier((object)[
                    'kvittert_dato' => $forsendelse->dato,
                    'kvitteringsforsendelse' => $forsendelse->forsendelsesnummer,
                    'status' => Efaktura::CONSIGNMENT_IN_PROGRESS
                ]);
        } catch (Exception $e) {
            throw new NetsException("Klarte ikke registrere efaktura mottakskvittering. {$e->getMessage()}");
        }
        $this->logg("Efakturaer i forsendelse {$oppdrag->forsendelsesnr} har blitt oppdatert til status 'IN_PROGRESS'");
        return $this;
    }

    /**
     * @param $kvittering
     * @param Forsendelse $forsendelse
     * @return Oppkopling
     * @throws NetsException
     * @throws Exception
     */
    protected function behandleEfakturaforsendelsePåOppdragsnivå($kvittering, Forsendelse $forsendelse): Oppkopling
    {
        if($kvittering->oppdrag) {
            $this->logg("Mottatt efaktura-kvittering for 'ferdig prosesserte efakturaer'");

            foreach($kvittering->oppdrag as $efakturaOppdrag) {
                $efakturaOppdrag->oppdragsnr;		// Opprinnelig oppdragsnummer
                $efakturaOppdrag->oppdragskonto;	// Opprinnelig oppdragskonto
                $efakturaOppdrag->feilkode;		// Feilkode dersom statusOppdrag = 1
                $efakturaOppdrag->feilmelding;	// Forklaring på feilkode
                $efakturaOppdrag->antGodkjenteFakturaer;
                $efakturaOppdrag->antAvvisteFakturaer;
                $efakturaOppdrag->statusOppdrag;
                // 0 = Oppdraget i seg selv er godkjent
                // 1 = Oppdraget er i sin helhet forkastet

                if( $efakturaOppdrag->statusOppdrag == 1 ) {
                    // Det aktuelle oppdraget er forkastet i sin helhet.
                    // Alle efakturaene i oppdraget må regnes som ikke utkrevet,
                    // dvs at utskriftsdato må nulles.

                    $this->logg("Efaktura-oppdrag {$efakturaOppdrag->oppdragsnr} har blitt forkastet i sin helhet av NETS");

                    /** @var Regning $regning */
                    foreach($this->app->hentSamling(Regning::class)
                                ->leggTilInnerJoin(['efakturaer' => Efaktura::hentTabell()], 'efakturaer.giro = giroer.gironr')
                                ->leggTilFilter([
                                    'efakturaer.forsendelse' => $kvittering->forsendelsesnr,
                                    'efakturaer.oppdrag' => $efakturaOppdrag->oppdragsnr,
                                    'efakturaer.status <>' => Efaktura::OK
                                ]) as $regning) {
                        try {
                            $regning->slettEfaktura();
                            $regning->settUtskriftsdato();
                            $this->logg("Efaktura for regning {$regning} har blitt slettet, og utskriftsdato fjernet.");
                        } catch (Exception $e) {
                            $this->app->logger->error("Klarte ikke slette efaktura for regning {$regning}");
                        }
                    }

                    // Skriver et statusvarsel på driftsforsiden,
                    // og sender en epost som forklarer feilen
                    $melding = "Et eFakturaoppdrag har blitt forkastet i sin helhet av NETS.<br>\nAlle eFakturaene i dette oppdraget må derfor sendes på nytt når feilen er rettet.<br><br><b>Feilkode {$efakturaOppdrag->feilkode} fra NETS:</b><br>{$efakturaOppdrag->feilmelding}<br>\n";

                    $this->app->sendMail( (object)[
                        'type' => 'efaktura-oppdrag_forkastet',
                        'auto'		=> true,
                        'to'		=> $this->app->hentValg('epost'),
                        'priority'	=> 90,
                        'subject'	=> "Problemer med eFaktura-oppdrag",
                        'html'		=> $melding
                    ] );

                    $this->app->mysqli->save([
                        'insert'		=> true,
                        'table'			=> "internmeldinger",
                        'fields'		=> [
                            'tekst'		=> $melding,
                            'drift'		=> true
                        ]
                    ]);

                }

                else {
                    $this->logg("Efaktura-oppdrag {$efakturaOppdrag->oppdragsnr} er godkjent, men enkelt-transaksjoner kan ha blitt avvist av NETS. Godkjente efakturaer: {$efakturaOppdrag->antGodkjenteFakturaer}. Avviste: {$efakturaOppdrag->antAvvisteFakturaer}");

                    // Oppdraget i sin helhet er ok.
                    //	Slett evt avviste enkelttransaksjoner
                    //	før de gjenværende transaksjonene godkjennes
                    foreach( $efakturaOppdrag->transaksjoner as $transaksjon ) {
                        $transaksjon->forfallsdato; 	// Som oppgitt i originalfila
                        $transaksjon->kid; 				// Som oppgitt i originalfila
                        $transaksjon->efakturaRef;		// Som oppgitt i originalfila
                        $transaksjon->feilkode;			// Feilkode for faktura
                        $transaksjon->feilmelding;		// Forklaring på feilkode
                        $transaksjon->feilreferanse;	// Data fra felt med feil

                        // Den aktuelle eFakturaen har feil og er forkastet.
                        // efakturaen må regnes som ikke utskrevet,
                        // dvs at utskriftsdato må fjernes.
                        // Gå gjennom feilkoden for å finne og rette feilen

                        /** @var Regning|null $regning */
                        $regning = $this->app->hentSamling(Regning::class)
                            ->leggTilFilter([
                                'giroer.kid' => $transaksjon->kid
                            ])->hentFørste();

                        if(!$regning) {
                            throw new NetsException("Finner ingen regning med kid {$transaksjon->kid}");
                        }
                        $efaktura = $regning->hentEfaktura();
                        if($efaktura
                            && $efaktura->hentForsendelse() == $kvittering->forsendelsesnr
                            && $efaktura->hentOppdrag() == $efakturaOppdrag->oppdragsnr
                            && $efaktura->hentStatus() != Efaktura::OK
                        ) {
                            if($transaksjon->feilkode > 0) {
                                $melding = "Efaktura for Regning {$regning->hentId()} ble avvist av Nets. Feilmelding fra Nets: ({$transaksjon->feilkode}) {$transaksjon->feilmelding}. Feilreferanse: {$transaksjon->feilreferanse}";
                                $this->logg($melding);
                                $regning->slettEfaktura();
                                $regning->settUtskriftsdato();

                                /*
                                 * 503 Betalingskravet avvist pga. av manglende aktiv avtale
                                 * 507 Ugyldig ”Utsteders referansenummer” (muligens blokkert i nettbank)
                                 */
                                if (
                                    $transaksjon->feilkode == 503
                                    || ($transaksjon->feilkode == 507 && $transaksjon->feilreferanse == 'EfakturaIdentifier')
                                ) {
                                    $regningsperson = $regning->hentLeieforhold()->regningsperson;
                                    if ($regningsperson && $this->app->hentValg('efaktura')) {
                                        try{
                                            $netsProsessor = $this->app->hentNetsProsessor();
                                            $netsProsessor->hentEfakturaIdentifierFor($regningsperson);
                                        }
                                        catch (Exception $e) {
                                            $this->app->logger->debug($e->getMessage());
                                        }
                                    }
                                }

                                else {
                                    $this->app->sendMail( (object)[
                                        'type' => 'efaktura-oppdrag_forkastet',
                                        'auto'		=> true,
                                        'to'		=> $this->app->hentValg('epost'),
                                        'priority'	=> 90,
                                        'subject'	=> "Problemer med eFaktura-oppdrag",
                                        'html'		=> $melding
                                    ] );

                                    $this->app->mysqli->save([
                                        'insert'		=> true,
                                        'table'			=> "internmeldinger",
                                        'fields'		=> [
                                            'tekst'		=> $melding,
                                            'drift'		=> true
                                        ]
                                    ]);
                                }
                            }
                        }
                    }

                    // De resterende efakturaene i oppdraget får status 'OK',
                    $this->app->hentSamling(Efaktura::class)
                        ->leggTilFilter([
                            'forsendelse' => $kvittering->forsendelsesnr,
                            'oppdrag' => $efakturaOppdrag->oppdragsnr,
                            'status <>' => Efaktura::OK
                        ])->forEach(function(Efaktura $efaktura) use ($forsendelse) {
                            $efaktura->settKvittertDato($forsendelse->dato)
                                ->settKvitteringsforsendelse($forsendelse->forsendelsesnummer)
                                ->settStatus(Efaktura::OK);
                            if($efaktura->regning && !$efaktura->regning->hentUtskriftsdato()) {
                                $efaktura->regning->settUtskriftsdato($forsendelse->dato)
                                    ->settFormat(Regning::FORMAT_EFAKTURA);
                            }
                        });
                    $this->logg("E-fakturaer i efaktura-oppdrag {$efakturaOppdrag->oppdragsnr} har blitt godkjent.");
                }
            }
        }
        return $this;
    }

    /**
     * @param stdClass[] $avtaleGiroOppdrag
     * @param string $prefiks 'Dirrem' for Avtalegiro-oppdrag
     * @return Forsendelse
     * @throws NetsException
     * @throws Exception
     */
    public function lagAvtalegiroForsendelse(array $avtaleGiroOppdrag = [], string $prefiks = 'Dirrem'): Forsendelse
    {
        $utc = new DateTimeZone('utc');
        if(!in_array($this->tjeneste, [self::TJENESTE_OCR, self::TJENESTE_AVTALEGIRO])) {
            throw new NetsException('Kan ikke sende AvtaleGiro-forsendelse via e-faktura-oppkopling');
        }
        if(!$avtaleGiroOppdrag) {
            $avtaleGiroOppdrag = $this->hentOppdragUt();
            $this->slettOppdragUt();
        }
        $avtaleGiroForsendelse = $this->lagUtgåendeForsendelse($avtaleGiroOppdrag, $prefiks);

        // Oppdater datoen for siste trekkrav i innstillingene
        $this->app->settValg('siste_fbo_trekkrav', date_create_immutable('now', $utc)->format('Y-m-d H:i:s'));

        foreach($avtaleGiroOppdrag as $sendtOppdrag ) {

            // Før på forsendelsesnummer i fbo_trekkrav-tabellen
            if( $sendtOppdrag->tjeneste == 21 && $sendtOppdrag->oppdragstype  == 0) {
                /** @var Fbo\Trekksett $trekk */
                $trekk = $this->app->hentSamling(Fbo\Trekk::class)
                    ->leggTilFilter([
                        'oppdrag' => $sendtOppdrag->oppdragsnr,
                        'forsendelse' => ''
                    ])->låsFiltre();
                $trekk
                    ->sett('overføringsdato', date_create_immutable())
                    ->sett('forsendelse', $avtaleGiroForsendelse->forsendelsesnummer)
                ;
            }
        }
        return $avtaleGiroForsendelse;
    }

    /**
     * @param Forsendelse\AbstractOppdrag[]|stdClass[] $oppdrag
     * @param string $prefiks
     * @return Forsendelse
     * @throws NetsException|Exception
     */
    public function lagUtgåendeForsendelse(array $oppdrag = [], string $prefiks = 'prodefakbbs'): Forsendelse
    {
        $efaktura = $this->hentTjeneste() == self::TJENESTE_EFAKTURA;
        if($efaktura) {
            $this->logg("Lager utgående forsendelse for eFaktura");
        }
        else {
            if($prefiks == 'prodefakbbs') {
                $prefiks = 'Dirrem';
            }
            $this->logg("Lager utgående forsendelse for AvtaleGiro");
        }
        $forsendelse = new Forsendelse();
        $forsendelse->dataavsender = $this->app->hentValg('nets_kundeenhetID');
        $forsendelse->datamottaker = 8080;
        $forsendelse->forsendelsesnummer = $this->app->hentNetsProsessor()->opprettForsendelsesnummer();
        $forsendelse->produksjon = $this->app->live;
        $forsendelse->oppdrag = $oppdrag;

        if(!$records = $forsendelse->skriv()) {
            throw new NetsException("Klarte ikke å skrive NETS-forsendelse");
        }

        // Lag en ny undermappe i filarkivet hver måned
        $mnd = date('Y-m');
        if(!file_exists("{$this->filarkiv}/nets/ut/{$mnd}")) {
            $this->logg('Oppretter ' . "{$this->filarkiv}/nets/ut/{$mnd}");
            mkdir("{$this->filarkiv}/nets/ut/{$mnd}");
        }

        if($efaktura) {
            $lokalFil = "{$this->filarkiv}/nets/ut/{$mnd}/{$prefiks}" . date('Y-') . str_pad($forsendelse->forsendelsesnummer, 7, '0', STR_PAD_LEFT) . ".txt";
        }
        else{
            $lokalFil = "{$this->filarkiv}/nets/ut/{$mnd}/{$prefiks}" . date('Ymd-') . str_pad($forsendelse->forsendelsesnummer, 7, '0', STR_PAD_LEFT) . ".txt";
        }

        if (file_exists($lokalFil)) {
            throw new NetsException("Forsendelsesfil '" . $lokalFil . "' finnes fra før.");
        }

        file_put_contents(
            $lokalFil,
            mb_convert_encoding(
                implode( "\n", str_replace(['–'], ['-'], $records) ),
                'ISO-8859-1',
                'UTF-8'
            )
        );
        $this->leggTilUtgåendePost($lokalFil);
        $this->logg("Forsendelse {$forsendelse->forsendelsesnummer} i {$lokalFil} er klar til overføring.");
        return $forsendelse;
    }

    /**
     * @return $this
     * @throws NetsException
     */
    public function sendUtgåendePost(): Oppkopling
    {
        if((array)$this->postUt) {
            $this->logg("Starter filoverføring til Nets");
            $sftp = $this->hentSftp();
            $this->opprettForbindelse();

            foreach($this->postUt as $fil => $innhold) {
                if(!$sftp->chdir("/Inbound/")) {
                    throw new NetsException("Klarte ikke flytte til /Inbound/ hos Nets");
                }

                if ($this->app->live) {
                    $this->logg("Overfører {$fil}");
                    if(!$sftp->put( basename($fil), $fil, $sftp::SOURCE_LOCAL_FILE)) {
                        throw new NetsException("Leiebasen klarte ikke sende følgende fil: {$fil}.");
                    }
                }
                else {
                    $this->logg("Leiebasen er i testmodus. Overføring av {$fil} ignoreres");
                }

                $this->fjernFraUtgåendePost($fil);
            }
            $this->logg("Filoverføring fullført");
        }
        return $this;
    }

    /**
     * @param string|null $melding
     * @return Oppkopling
     * @throws Exception
     */
    public function settOcrFeilmelding(string $melding = null): Oppkopling
    {
        $this->app->settValg('OCR_feilmelding', strval($melding));

        return $this;
    }

    /**
     * @param string $filbane
     * @param string $innhold
     * @return $this
     * @throws NetsException
     */
    protected function behandleAvtalegiroTrekkKvitteringer(string $filbane, string $innhold): Oppkopling
    {
        $filnavn = basename($filbane);
        if(strpos($filnavn, 'KV.AVVIST') === 0) {
            $overføringstidspunkt = date_create_from_format('ymd\.\THis', substr($filnavn, 11, 14));
            throw new NetsException('AvtaleGiro trekk-overføring ' . $overføringstidspunkt->format('d.m.Y H:i:s') . ' har blitt avvist');
        }
        elseif(strpos($filnavn, 'KV.GODKJENT') === 0) {
            $overføringstidspunkt = date_create_from_format('ymd\.\THis', substr($filnavn, 22, 14));
            $originalForsendelse = preg_match('/id="forsendelsesnummer">([0-9]*)</m', $innhold, $matches) ? $matches[1] : null;
            $innlesingsStatus = preg_match('/id="forsendelse-status">\s*(.*)\s*</', $innhold, $matches) ? $matches[1] : null;
            $antRegistrerteAvtaleGiro = preg_match('/id="avtalegiro-antall">\s*([0-9]*)\s*</m', $innhold, $matches) ? $matches[1] : null;
            $antAvvisteAvtaleGiro = preg_match('/id="avtalegiro-antall-avviste">\s*([0-9]*)\s*</m', $innhold, $matches) ? $matches[1] : null;
            $antGodkjenteAvtaleGiro = preg_match('/id="avtalegiro-antall-godkjente">\s*([0-9]*)\s*</m', $innhold, $matches) ? $matches[1] : null;
            $this->logg('AvtaleGiro trekk-forsendelse ' . $originalForsendelse . ' sendt ' . $overføringstidspunkt->format('d.m.Y H:i:s') . ' har blitt ' . $innlesingsStatus, ['antallRegistrerte' => $antRegistrerteAvtaleGiro, 'antallAvviste' => $antAvvisteAvtaleGiro, 'antallGodkjente' => $antGodkjenteAvtaleGiro]);
        }
        else {
            throw new NetsException($filbane . ' gjenkjennes ikke som AvtaleGiro-kvittering');
        }
        return $this;
    }
}