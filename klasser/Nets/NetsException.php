<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 13/11/2020
 * Time: 11:25
 */

namespace Kyegil\Leiebasen\Nets;

/**
 * Class NetsException
 * @package Kyegil\Leiebasen
 */
class NetsException extends \Exception
{

}