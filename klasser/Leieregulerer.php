<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 02/06/2023
 * Time: 23:37
 */

namespace Kyegil\Leiebasen;

use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Exception;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Delkravtypesett;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegningsett;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Adressefelt;
use Kyegil\Leiebasen\Visning\felles\html\Utleierfelt;

/**
 *
 */
class Leieregulerer
{
    /**
     *
     */
    const LOG_PREFIX = 'LEIEJUSTERING – ';
    const VARSELTYPE_EPOST = 'e-post';
    const VARSELTYPE_BREV = 'brev';
    public static array $varseltyper = [
        self::VARSELTYPE_BREV,
        self::VARSELTYPE_EPOST
    ];

    /** @var Leieregulerer */
    private static Leieregulerer $instance;

    /**
     * Historisk konsumprisindeks, hentet ifra Statistisk Sentralbyrå (SSB)
     *
     * @var array
     * @see self::hentKpiFraSSb()
     */
    private static array $kpiHistorikk;

    /**
     * Leiebasen
     *
     * @var CoreModelImplementering
     */
    protected CoreModelImplementering $app;

    /**
     * Nå
     *
     * @var DateTimeImmutable
     */
    private DateTimeImmutable $tidsstempel;

    /**
     * @see hentOppdateringFristForSatser()
     * @var DateTimeImmutable
     */
    private DateTimeImmutable $satsOppdateringFrist;
    private DateInterval $enMnd;
    private DateInterval $toMnd;

    /**
     * Er automatisk leieregulering aktivert?
     *
     * @var bool
     */
    public bool $automatisk = false;
    /**
     * Hvorvidt vi automatisk skal hente faktisk konsumprisindeks,
     * i stedet for å justere i hht en fast årlig justeringssats
     *
     * @var bool
     */
    public bool $autoKpi = false;

    /**
     * Fast (manuelt satt) justeringsindeks per år, angitt i prosenter
     *
     * @var float
     */
    public float $justeringGjeldendeIndeks = 0;

    /**
     * Minimumsintervallet mellom hver indeksregulering av et leieforhold (lovbestemt)
     *
     * @var DateInterval
     */
    public DateInterval $justeringIntervall;

    /**
     * Minimumsfrist for å varsle leietakere om en kommende leiejustering, før denne kan tre i kraft
     *
     * @var DateInterval
     */
    public DateInterval $varselfrist;

    /**
     * Leiebasen vil liste forslag til leiejustering for alle leieforhold som kan justeres i løpet av dette tidsrommet
     *
     * @var DateInterval
     */
    public DateInterval $forslagsfrist;

    /**
     * Dersom et tidsrom er oppgitt her,
     * så vil leiebasen bruke dette for å automatisk godkjenne og varsle om leiejusteringer
     * så lang tid før varslingsfristen utløper
     *
     * Varsles som må sendes som brevpost vil ikke bli automatisk godkjent
     *
     * @var DateInterval|null
     */
    public ?DateInterval $autoGodkjenningFørFrist;

    /**
     * Terskelfaktor for positiv forankring til foreslått leie
     *
     * Dersom den justerte leien i et leieforhold avviker fra foreslått leie i hht beregningsmodellen for leieobjektet,
     * så kan justeringen økes for å samstille med forslaget, såfremt økningen er innenfor denne verdien.
     *
     * F.eks om verdien settes til 0.005, så kan justeringen økes med 0,5% for å samstille med beregningsmodellen.
     * Om avviket er større vil de ikke samstilles.
     *
     * @var float|int
     */
    public float $posForankring = 0;

    /**
     * Terskelfaktor for negativ forankring til foreslått leie
     *
     * Dersom den justerte leien i et leieforhold avviker fra foreslått leie i hht beregningsmodellen for leieobjektet,
     * så kan justeringen reduseres for å samstille med forslaget, såfremt reduksjonen er innenfor denne verdien.
     *
     * F.eks om verdien settes til 0.01, så kan justeringen reduseres med 1% for å samstille med beregningsmodellen.
     * Om avviket er større vil de ikke samstilles.
     *
     * @var float|int
     */
    public float $negForankring = 0;

    /**
     * @param CoreModelImplementering $app
     * @param DateTimeInterface|null $tidsstempel
     * @return Leieregulerer
     */
    public static function getInstance(
        CoreModelImplementering $app,
        ?DateTimeInterface $tidsstempel = null
    ): Leieregulerer
    {
        if ( !isset( self::$instance ) )
        {
            $tidsstempel = $tidsstempel ?? new DateTimeImmutable();
            self::$instance = new self($app, $tidsstempel);
        }
        return self::$instance;
    }

    /**
     * Automatiske prosesser som kjøres via cron
     *
     * @param CoreModelImplementering $leiebase
     * @param array $argumenter
     * @return array $argumenter
     */
    public static function cron(CoreModelImplementering $leiebase, array $argumenter = []): array
    {
        /** @var DateTimeInterface $cronTid */
        try {
            $cronTid = $argumenter[0] ?? date_create_immutable('now', new DateTimeZone('UTC'));
            $regulerer = self::getInstance($leiebase, $cronTid);
            if (!$regulerer->automatisk) {
                return $argumenter;
            }
            // Hver hele time
            if ($cronTid->format('i') == 0) {
                $regulerer->autojusterLeieberegningSatser();
                $regulerer->autojusterDelkravSatser();
            }
            // Hvert kvarter
            if (in_array($cronTid->format('i'), [0,15,30,45])) {
                $regulerer->utførForeståendeReguleringer($cronTid);
                $regulerer->sjekkForeståendeBrevVarsler($cronTid);
            }
        } catch (Exception $e) {
            $leiebase->logger->critical(self::LOG_PREFIX  . $e->getMessage());
        }
        return $argumenter;
    }

    /**
     * Henter KPI-historikk ifra SSB
     *
     * Kpi-ene returneres som et array med måned i formatet '2022M12' som peker
     *
     * @return array
     * @throws Exception
     */
    public static function hentKpiFraSSb(): array
    {
        if (!isset(self::$kpiHistorikk)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://data.ssb.no/api/v0/dataset/45590.json");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            $responseObject = json_decode($response, true);
            if (isset($responseObject['dataset'])) {
                $datasett = $responseObject['dataset'];
                self::$kpiHistorikk = [];
                $perioder = $datasett['dimension']['Tid']['category']['index'] ?? [];
                foreach ($perioder as $mnd => $peker) {
                    self::$kpiHistorikk[$mnd] = $datasett['value'][$peker];
                }
            } else {
                throw new Exception('Klarte ikke hente KPI i fra SSB');
            }
            ksort(self::$kpiHistorikk);
        }
        return self::$kpiHistorikk;
    }

    /**
     * @param CoreModelImplementering $app
     * @param DateTimeInterface $tidsstempel
     */
    private function __construct(CoreModelImplementering $app, DateTimeInterface $tidsstempel)
    {
        $this->app = $app;
        $this->settTidsstempel($tidsstempel);
        $this->enMnd = new DateInterval('P1M');
        $this->toMnd = new DateInterval('P2M');

        $this->automatisk = (bool)$this->app->hentValg('leiejustering_automatisk');
        $this->autoKpi = (bool)$this->app->hentValg('leiejustering_auto_kpi');
        $this->justeringGjeldendeIndeks = (float)($this->app->hentValg('leiejustering_gjeldende_indeks') ?? 0);
        $this->justeringIntervall = Leiebase::parseDateInterval($this->app->hentValg('leiejustering_intervall')) ?? new DateInterval('P0D');
        $this->varselfrist = Leiebase::parseDateInterval($this->app->hentValg('leiejustering_varselfrist')) ?? new DateInterval('P0D');
        $this->forslagsfrist = Leiebase::parseDateInterval($this->app->hentValg('leiejustering_forslagsfrist')) ?? new DateInterval('P0D');
        $this->autoGodkjenningFørFrist = Leiebase::parseDateInterval($this->app->hentValg('leiejustering_auto_godkjenning_tid_før_frist'));
        $this->posForankring = $this->app->hentValg('leiejustering_pos_forankring') / 100;
        $this->negForankring = $this->app->hentValg('leiejustering_neg_forankring') / 100;
    }

    /**
     * Hent leieberegninger som er modne for sats-justering
     *
     * @param DateTimeImmutable $oppdateringfrist
     * @return Leieberegningsett
     * @throws Exception
     */
    private function hentLeieberegningerForJustering(DateTimeImmutable $oppdateringfrist): Leieberegningsett
    {
        /** @var Leieberegningsett $leieberegningsett */
        $leieberegningsett = $this->app->hentSamling(Leieberegning::class);
        $leieberegningsett->leggTilEavFelt('sats_oppdatert');
        $leieberegningsett->leggTilFilter(['or' => [
            ['`sats_oppdatert`.`verdi` IS NULL'],
            ['`sats_oppdatert`.`verdi` <' => $oppdateringfrist->format('Y-m-d 00:00:00')]
        ]]);
        return $leieberegningsett;
    }

    /**
     * Hent delkravtyper som er modne for sats-justering
     *
     * @param DateTimeImmutable $oppdateringfrist
     * @return Delkravtypesett
     * @throws Exception
     */
    private function hentDelkravForJustering(DateTimeImmutable $oppdateringfrist): Delkravtypesett
    {
        /** @var Delkravtypesett $delkravtypesett */
        $delkravtypesett = $this->app->hentSamling(Delkravtype::class);
        $delkravtypesett->leggTilFilter(['aktiv' => true]);
        $delkravtypesett->leggTilFilter(['relativ' => false]);
        $delkravtypesett->leggTilEavFelt('indeksreguleres');
        $delkravtypesett->leggTilFilter(['`indeksreguleres`.`verdi`' => true]);
        $delkravtypesett->leggTilFilter(['`' . Delkravtype::hentTabell() . '`.`sats_oppdatert` <'
        => $oppdateringfrist->format('Y-m-d 00:00:00')
        ]);
        return $delkravtypesett;
    }

    /**
     * Auto-juster leieberegning-satser (Cron-oppgave)
     *
     * @return Leieregulerer
     * @throws Exception
     */
    public function autojusterLeieberegningSatser(): Leieregulerer
    {
        $this->app->before($this, __FUNCTION__, []);
        $this->app->logger->debug(self::LOG_PREFIX . 'Autojustering av leieberegning-satser påbegynt');
        $oppdateringFrist = $this->hentOppdateringFristForSatser();
        $leieberegningsett = $this->hentLeieberegningerForJustering($oppdateringFrist);

        /** @var Leieberegning $leieberegning */
        foreach ($leieberegningsett as $leieberegning) {
            /** @var Fraction $justeringsfaktor */
            try {
                $justeringsfaktor = $this->hentMånedensJusteringsfaktorForSatser($leieberegning, $this->tidsstempel);
                if($justeringsfaktor->compare('!=', 1)) {
                    $this->app->logger->info(self::LOG_PREFIX . 'Autojustering av leieberegning ' . $leieberegning->hentId(), [
                        'faktor' => $justeringsfaktor->asFraction()
                    ]);
                    $leieberegning->justerSatser($justeringsfaktor, true);
                }
            } catch (Exception $e) {
                $this->app->logger->warning(self::LOG_PREFIX . $e->getMessage());
            }
        }
        $this->app->logger->debug(self::LOG_PREFIX . 'Autojustering av leieberegning-satser fullført');
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * Auto-juster delkrav-satser (Cron-oppgave)
     *
     * @return Leieregulerer
     * @throws Exception
     */
    public function autojusterDelkravSatser(): Leieregulerer
    {
        $this->app->before($this, __FUNCTION__, []);
        $this->app->logger->debug(self::LOG_PREFIX . 'Autojustering av delkravtype-satser påbegynt');
        $oppdateringFrist = $this->hentOppdateringFristForSatser();
        $delkravsett = $this->hentDelkravForJustering($oppdateringFrist);

        /** @var Delkravtype $delkravtype */
        foreach ($delkravsett as $delkravtype) {
            /** @var Fraction $justeringsfaktor */
            try {
                $justeringsfaktor = $this->hentMånedensJusteringsfaktorForSatser($delkravtype, $this->tidsstempel);
                $this->app->logger->info(self::LOG_PREFIX . 'Autojustering av delkravtype ' . $delkravtype->hentId(), [
                    'faktor' => $justeringsfaktor->asFraction()
                ]);
                $delkravtype->justerSats($justeringsfaktor, true);
            } catch (Exception $e) {
                $this->app->logger->warning(self::LOG_PREFIX . $e->getMessage());
            }
        }
        $this->app->logger->debug(self::LOG_PREFIX . 'Autojustering av delkravtype-satser fullført');
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * Sett tidsstempel (for å overstyre tiden)
     *
     * @param DateTimeInterface $tidsstempel
     * @return $this
     */
    public function settTidsstempel(DateTimeInterface $tidsstempel): Leieregulerer
    {
        if($tidsstempel instanceof DateTime) {
            $tidsstempel = DateTimeImmutable::createFromMutable($tidsstempel);
        }
        $this->tidsstempel = $tidsstempel;
        unset($this->satsOppdateringFrist);
        return $this;
    }

    /**
     * Returnerer seneste dato for sist oppdatering av leieberegningssatser
     * for at de skal være aktuelle for justering igjen nå.
     *
     * Satsene oppdateres i utganspunktet hver måned, men med noen dagers slingringsmonn
     *
     * @return DateTimeImmutable
     */
    private function hentOppdateringFristForSatser(): DateTimeImmutable
    {
        if(!isset($this->satsOppdateringFrist)) {
            /*
             * Vi vil oppdatere alle satser som ikke har blitt oppdatert på én måned.
             * men legger inn 4 dagers slingringsmonn
             * for å fange opp satser som sist ble oppdatert forrige måned men litt senere enn denne måned
             */
            $this->satsOppdateringFrist = $this->tidsstempel->sub(new DateInterval('P1M'))->add(new DateInterval('P4D'));
            if ($this->satsOppdateringFrist->format('m') === $this->tidsstempel->format('m')) {
                $this->satsOppdateringFrist = $this->satsOppdateringFrist->sub(new DateInterval('P1M'));
            }

            /* SSB oppdaterer KPI kl. 08:00 norsk tid den 10. hver måned,
            * så om vi bruker auto kpi-justering så vil vi ikke justere tidligere enn det
            */
            if ($this->autoKpi && $this->tidsstempel->format('dH') < 1010) {
                $this->satsOppdateringFrist = $this->satsOppdateringFrist->sub(new DateInterval('P1M'));
            }
        }
        return $this->satsOppdateringFrist;
    }

    /**
     * Hent månedens justeringsfaktor for satser
     *
     * Basert enten på faktisk konsumprisindeks, eller på fast årlig justeringsbeløp,
     * så returnes en faktor for denne måneds satsjustering. (1/1 = ingen justering)
     *
     * @param Leieberegning|Delkravtype $satsModell
     * @param DateTimeImmutable|null $tid
     * @return Fraction
     * @throws Exception
     */
    public function hentMånedensJusteringsfaktorForSatser(Modell $satsModell, ?DateTimeImmutable $tid = null): Fraction
    {
        $tid = $tid ?? $this->tidsstempel;
        $justeringsfaktor = new Fraction(1);
        $this->app->logger->debug(self::LOG_PREFIX . 'Justeringsfaktor beregnes', [
            'auto_kpi' => $this->autoKpi
        ]);
        if($this->autoKpi) {
            $gjeldendeKpiMåned = array_key_last(Leieregulerer::hentKpiFraSSb());
            if ($gjeldendeKpiMåned) {
                $sistJusterteKpiMåned = $this->hentSistBrukteKpi($satsModell) ?: $gjeldendeKpiMåned;
                $justeringsfaktor = $this->hentJusteringsfaktorAutoKpi($gjeldendeKpiMåned, $sistJusterteKpiMåned);
            }
        }
        else {
            $sistEndretSats = $satsModell->hentSatsOppdatert();
            if ($sistEndretSats) {
                $justeringsfaktor = $this->hentJusteringsfaktorManuellKpi($tid, $sistEndretSats);
            }
            else {
                $satsModell->oppdaterSatsHistorikk(true);
            }
        }
        return $justeringsfaktor;

    }

    /**
     * Hent sist brukte konsumpris-indeks for en leieberegning- eller delkravtype-sats
     *
     * @param Leieberegning|Delkravtype $satsModell
     * @return string|null
     */
    public function hentSistBrukteKpi(Modell $satsModell): ?string
    {
        $satsHistorikk = (array)$satsModell->hentSatsHistorikk();
        ksort($satsHistorikk);
        $sisteJustering = end($satsHistorikk);
        $sistBrukteKpi = $sisteJustering ? ($sisteJustering->gjeldende_kpi ?? null) : null;
        return $this->app->after($this, __FUNCTION__, $sistBrukteKpi);
    }

    /**
     * Foreslå justert basisleie for et leieforhold for en gitt dato
     *
     * @param Modell\Leieforhold $leieforhold
     * @param DateTimeImmutable $justeringsdato
     * @return float
     * @throws Exception
     */
    public function foreslåJustertBasisLeie(Modell\Leieforhold $leieforhold, DateTimeImmutable $justeringsdato): float
    {
        list('leieforhold' => $leieforhold, 'justeringsdato' => $justeringsdato)
            = $this->app->before($this, __FUNCTION__, [
            'leieforhold' => $leieforhold, 'justeringsdato' => $justeringsdato
        ]);
        $eksisterendeLeie = $leieforhold->hentÅrligBasisleie();
        $justeringsfaktor = $this->hentJusteringsfaktorForLeieforhold($justeringsdato, $leieforhold);
        $nyLeie = round($justeringsfaktor->multiply($eksisterendeLeie)->asDecimal());
        $nyLeie = round($this->forankreLeieTilLeieforslag($nyLeie, $leieforhold));
        return $this->app->after($this, __FUNCTION__, $nyLeie);
    }

    /**
     * Foreslå justert terminleie for et leieforhold
     *
     * @param Leieforhold $leieforhold
     * @param DateTimeImmutable $justeringsdato
     * @param float|null $basisleie
     * @return float
     * @throws Exception
     */
    public function foreslåJustertTerminLeie(Modell\Leieforhold $leieforhold, DateTimeImmutable $justeringsdato, ?float $basisleie = null): float
    {
        $terminfaktor = new Fraction(1, $leieforhold->hentAntTerminer() ?? 1);
        $årligLeie = $basisleie ?? $this->foreslåJustertBasisLeie($leieforhold, $justeringsdato);
        $leieforholdDelkravtypesett = $leieforhold->hentDelkravtyper();
        $leieforholdDelkravtypesett->leggTilFilter(['selvstendig_tillegg' => false])->låsFiltre();
        foreach ($leieforholdDelkravtypesett as $leieforholdDelkravtype) {
            if(!$leieforholdDelkravtype->relativ
                && $leieforholdDelkravtype->delkravtype->hent('indeksreguleres')
            ) {
                if($leieforholdDelkravtype->delkravtype->valgfritt) {
                    $justeringsfaktor = $this->hentJusteringsfaktorForLeieforhold($justeringsdato, $leieforhold);
                    $årligLeie += round($justeringsfaktor->multiply($leieforholdDelkravtype->hentSats())->asDecimal());
                }
                else {
                    $årligLeie += $leieforholdDelkravtype->delkravtype->hentSats();
                }
            }
            else if ($leieforholdDelkravtype->relativ) {
                $årligLeie += ($leieforholdDelkravtype->hentSats() / 100 * $årligLeie);
            }
            else {
                if($leieforholdDelkravtype->delkravtype->valgfritt) {
                    $årligLeie += $leieforholdDelkravtype->hentSats();
                }
                else {
                    $årligLeie += $leieforholdDelkravtype->delkravtype->hentSats();
                }
            }
        }
        return round($terminfaktor->multiply($årligLeie)->asDecimal());
    }

    /**
     * Returnerer sist brukte KPI-måned for et leieforhold,
     * i formatet '20023M12' som er identisk med formatet som SSB bruker.
     *
     * @param Modell\Leieforhold $leieforhold
     * @return string
     * @throws Exception
     */
    public function hentSistBrukteKpiMndForLeieforhold(Modell\Leieforhold $leieforhold): string
    {
        $leieHistorikk = (array)$leieforhold->hent('leie_historikk');
        ksort($leieHistorikk);
        $sisteEndring = end($leieHistorikk);
        if($sisteEndring) {
            $anvendtKpi = $sisteEndring->gjeldende_kpi ?? null;
            if($anvendtKpi) {
                return $anvendtKpi;
            }
            $dato = date_create_immutable(array_key_last($sisteEndring));
        }
        $anvendtKpi = $sisteEndring->gjeldende_kpi ?? null;
        if($anvendtKpi) {
            return $anvendtKpi;
        }
        if(empty($dato)) {
            $dato = $leieforhold->hentFradato();
        }
        if($dato instanceof DateTime) {
            $dato = DateTimeImmutable::createFromMutable($dato);
        }
        // KPI offentliggjøres normalt den 10. en mnd i ettertid
        $dato = $dato->sub($dato->format('d') < 10 ? $this->toMnd : $this->enMnd);
        return $this->kpiMnd($dato);
    }

    /**
     * Returnerer gjeldende KPI-måned for en gitt dato, i formatet '2023M12'
     *
     * @param DateTimeImmutable $dato
     * @return string
     * @throws Exception
     */
    private function kpiMnd(DateTimeImmutable $dato): string
    {
        $mnd = $dato->format('Y\Mm');
        $kpiHistorikk = self::hentKpiFraSSb();
        if ($kpiHistorikk && $mnd < array_key_first($kpiHistorikk)) {
            return array_key_first($kpiHistorikk);
        }
        else if ($kpiHistorikk && $mnd > array_key_last($kpiHistorikk)) {
            return array_key_last($kpiHistorikk);
        }
        return $mnd;
    }

    /**
     * Hent manuell leiejusteringsfaktor på grunnlag av fra- og til-dato
     *
     * @param DateTimeInterface $nå
     * @param DateTimeInterface $sist
     * @return Fraction
     */
    private function hentJusteringsfaktorManuellKpi(DateTimeInterface $nå, DateTimeInterface $sist): Fraction
    {
        $justeringsfaktor = new Fraction(1);
        $antallDager = $nå->diff($sist)->days;
        $antallDagerIÅret = $sist->format('L') ? 366 : 365;

        $årligFaktor = (100 + $this->justeringGjeldendeIndeks) / 100;
        $this->app->logger->debug(self::LOG_PREFIX . 'Justeringsfaktor beregnes', [
            'antall_dager' => $antallDager,
            'antall_dager_i_året' => $antallDagerIÅret,
        ]);
        $justeringsfaktor->setNumerator($årligFaktor ** ($antallDager/$antallDagerIÅret));
        return $justeringsfaktor;
    }

    /**
     * Hent leiejusteringsfaktor for automatisk KPI på grunnlag av nåværende og forrige KPI-måned
     *
     * @param string $nyKpiMnd
     * @param string $tidlKpiMnd
     * @return Fraction
     * @throws Exception
     */
    private function hentJusteringsfaktorAutoKpi(string $nyKpiMnd, string $tidlKpiMnd): Fraction
    {
        $justeringsfaktor = new Fraction(1);
        $tidlKpi = Leieregulerer::hentKpiFraSSb()[$tidlKpiMnd] ?? 0;
        $nyKpi = Leieregulerer::hentKpiFraSSb()[$nyKpiMnd] ?? 0;
        $this->app->logger->debug(self::LOG_PREFIX . 'Justeringsfaktor beregnes', [
            'ny_kpi' => [$nyKpiMnd => $nyKpi],
            'tidl_kpi' => [$tidlKpiMnd => $tidlKpi],
        ]);
        if (!$nyKpi || !$tidlKpi) {
            throw new Exception('Leiejustering – Klarte ikke finne gjeldende KPI');
        }
        $justeringsfaktor->setNumerator($nyKpi)->setDenominator($tidlKpi);
        return $justeringsfaktor;
    }

    /**
     * Foreslå justeringsfaktor for et leieforhold
     * basert på automatisk kpi eller manuell reguleringssatss og tiden siden siste justering
     *
     * @param DateTimeImmutable $justeringsdato
     * @param Modell\Leieforhold $leieforhold
     * @return Fraction
     * @throws Exception
     */
    public function hentJusteringsfaktorForLeieforhold(DateTimeImmutable $justeringsdato, Modell\Leieforhold $leieforhold): Fraction
    {
        if ($this->autoKpi) {
            $kpiMnd = $this->kpiMnd($justeringsdato);
            $sistBrukteKpiMnd = $this->hentSistBrukteKpiMndForLeieforhold($leieforhold);
            $justeringsfaktor = $this->hentJusteringsfaktorAutoKpi($kpiMnd, $sistBrukteKpiMnd);
        } else {
            $sistEndretDato = $leieforhold->hent('leie_oppdatert') ?? $leieforhold->hentFradato();
            $justeringsfaktor = $this->hentJusteringsfaktorManuellKpi($justeringsdato, $sistEndretDato);
        }
        return $justeringsfaktor;
    }

    /**
     * Foreslå justeringsdato for et leieforhold
     *
     * @param Modell\Leieforhold $leieforhold
     * @return DateTimeImmutable
     * @throws Exception
     */
    public function foreslåJusteringsdato(Modell\Leieforhold $leieforhold): DateTimeImmutable
    {
        /** @var DateTimeInterface $sisteLeieJustering */
        $sisteLeieJustering = $leieforhold->hent('leie_oppdatert')
            ?? $leieforhold->hentFradato();
        if ($sisteLeieJustering instanceof DateTime) {
            $sisteLeieJustering = DateTimeImmutable::createFromMutable($sisteLeieJustering);
        }
        $justeringsdato = max(
            $sisteLeieJustering->add($this->justeringIntervall),
            $this->tidsstempel->add($this->varselfrist)
        );
        $nesteLeie = $leieforhold->hentKrav()
            ->leggTilFilter(['type' => Krav::TYPE_HUSLEIE])
            ->leggTilFilter(['fom >=' => $justeringsdato->format('Y-m-d')])
            ->låsFiltre()->hentFørste();
        if ($nesteLeie) {
            $justeringsdato = $nesteLeie->fom;
        }
        if ($justeringsdato instanceof DateTime) {
            $justeringsdato = DateTimeImmutable::createFromMutable($justeringsdato);
        }
        return $justeringsdato;
    }

    /**
     * Fyll justeringsvarsel-mal for et leieforhold
     *
     * @param Leieforhold $leieforhold
     * @param string $mal
     * @param DateTimeInterface|null $justeringsdato
     * @param float|null $nyBasisleie
     * @param float|null $nyLeie
     * @return string
     * @throws Exception
     */
    public function fyllJusteringsVarselForLeieforhold(
        Leieforhold $leieforhold,
        string $mal,
        ?DateTimeInterface $justeringsdato = null,
        ?float $nyBasisleie = null,
        ?float $nyLeie = null
    ): string
    {
        list('leieforhold' => $leieforhold, 'mal' => $mal, 'justeringsdato' => $justeringsdato)
            = $this->app->before($this, __FUNCTION__, [
            'leieforhold' => $leieforhold, 'mal' => $mal, 'justeringsdato' => $justeringsdato
        ]);
        if (!$justeringsdato) {
            $tidligsteForslagsdato = date_create_immutable()->add($this->varselfrist);
            $justeringsdato  = $leieforhold->getRawData()->leieforhold->neste_dato ?? null;
            $justeringsdato = $justeringsdato ? date_create_immutable($justeringsdato) : $tidligsteForslagsdato;
        }
        if ($justeringsdato instanceof DateTime) {
            $justeringsdato = DateTimeImmutable::createFromMutable($justeringsdato);
        }

        $nyBasisleie = $nyBasisleie ?? $this->foreslåJustertBasisLeie($leieforhold, $justeringsdato);
        $nyLeie = $nyLeie ?? $this->foreslåJustertTerminLeie($leieforhold, $justeringsdato, $nyBasisleie);
        $justering = new Fraction($nyBasisleie, $leieforhold->hentÅrligBasisleie());

        $variabler = [
            '{kontraktnr}' => $leieforhold->hentKontrakt(),
            '{leieforhold}' => $leieforhold,
            '{fast KID}' => $leieforhold->hentKid(),
            '{leieobjektbeskrivelse}' => $leieforhold->hentBeskrivelse(),
            '{dato}' => date_create_immutable()->format('d.m.Y'),
            '{virkning fra dato}' => $justeringsdato->format('d.m.Y'),
            '{utleier}' => $this->app->hentValg('utleier'),
            '{utleieradresse}' => $this->app->vis(Utleierfelt::class),
            '{leietaker}' => $leieforhold->hentNavn(),
            '{leietakeradresse}' => $this->app->vis(Adressefelt::class, ['leieforhold' => $leieforhold]),
            '{justering}' => $this->app->prosent($justering->subtract(1)->asDecimal()),
            '{gammel bruttoleie}' => $this->app->kr($leieforhold->hentLeiebeløp()),
            '{gammel årsleie}' => $this->app->kr($leieforhold->hentÅrligBasisleie()),
            '{ny nettoleie}' => $this->app->kr($nyBasisleie),
            '{ny bruttoleie}' => $this->app->kr($nyLeie),
            '{terminlengde}' => $this->app->periodeformat(
                $leieforhold->hentTerminlengde(),
                false, true
            ),
        ];

        array_walk($variabler, function (&$verdi, $variabel) {
            $verdi = new HtmlElement('span', [
                'data-mal-variabel' => $variabel
            ], $verdi);
        });

        $tekst = str_replace(array_keys($variabler), $variabler, $mal);
        return $this->app->after($this, __FUNCTION__, $tekst);
    }

    /**
     * Hent varseltype for et leieforhold
     *
     * @param Leieforhold|null $leieforhold
     * @return string
     * @throws Exception
     */
    public function hentVarseltypeForLeieforhold(?Leieforhold $leieforhold): string
    {
        list('leieforhold' => $leieforhold)
            = $this->app->before($this, __FUNCTION__, [
            'leieforhold' => $leieforhold
        ]);
        $varselType = $leieforhold->hentEpost() ? Leieregulerer::VARSELTYPE_EPOST : Leieregulerer::VARSELTYPE_BREV;
        return $this->app->after($this, __FUNCTION__, $varselType);
    }

    /**
     * Send varsel om leiejustering til et leieforhold
     * Brev-varsler vil ikke bli sendt
     *
     * @param Leieforhold $leieforhold
     * @param string $varselTekst
     * @param string|null $varselType
     * @return string varseltypen som ble brukt
     * @throws Exception
     */
    public function sendReguleringsVarsel(
        Leieforhold $leieforhold,
        string $varselTekst,
        ?string $varselType = null
    ): string
    {
        list('leieforhold' => $leieforhold, 'varselTekst' => $varselTekst, 'varselType' => $varselType)
            = $this->app->before($this, __FUNCTION__, [
            'leieforhold' => $leieforhold, 'varselTekst' => $varselTekst, 'varselType' => $varselType
        ]);
        $varselType = $varselType ?? $this->hentVarseltypeForLeieforhold($leieforhold);

        if ($varselType == Leieregulerer::VARSELTYPE_EPOST) {
            $adressefelt = $leieforhold->hentEpost();
            $this->app->sendMail((object)[
                'to' => implode(',', $adressefelt),
                'subject' => 'Regulering av leie',
                'html' => $varselTekst,
                'type' => 'leieregulering'
            ]);
        }

        return $this->app->after($this, __FUNCTION__, $varselType);
    }

    /**
     * Forankre leien til leien som er foreslått av leieberegningsmetoden
     *
     * Dersom leien er innenfor forankringsradius for leieberegningsmetoden,
     * så justeres den til å følge beregningen.
     *
     * @param float $leie
     * @param Leieforhold $leieforhold
     * @return float
     */
    private function forankreLeieTilLeieforslag(float $leie, Leieforhold $leieforhold): float
    {
        list('leie' => $leie, 'leieforhold' => $leieforhold)
            = $this->app->before($this, __FUNCTION__, [
            'leie' => $leie, 'leieforhold' => $leieforhold
        ]);
        if ($this->posForankring || $this->negForankring) {
            $beregnetLeie = $leieforhold->leieobjekt ? $leieforhold->leieobjekt->beregnBasisleie($leieforhold->hentAndel()) : $leie;
            if(
                ($beregnetLeie && $this->negForankring > abs($leie / $beregnetLeie - 1))
                || ($leie && $this->posForankring > abs($beregnetLeie / $leie - 1))
            ) {
                $leie = $beregnetLeie;
            }
        }
        return $this->app->after($this, __FUNCTION__, $leie);
    }

    /**
     * Utfør forestående reguleringer (Cron-oppdrag)
     *
     * @param DateTimeImmutable|null $tid
     * @return Leieregulerer
     * @throws Exception
     */
    public function utførForeståendeReguleringer(?DateTimeImmutable $tid = null): Leieregulerer
    {
        list('tid' => $tid)
            = $this->app->before($this, __FUNCTION__, [
            'tid' => $tid
        ]);
        if($this->autoGodkjenningFørFrist) {
            $this->app->logger->debug(self::LOG_PREFIX . 'Automatisk justering av leie påbegynt');
            $tid = $tid ?? $this->tidsstempel;
            $tidligsteJusteringsdato = $tid->add($this->varselfrist);

            $forslagSett = $this->hentForeståendeLeieforholdEmner($tid);

            // Inkluder kun leieforhold der minst én leietaker vil ha epostvarsling
            $forslagSett->leggTilInnerJoin(Adgang::hentTabell(), '`' . Adgang::hentTabell() . '`.`leieforhold` = `' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '` and `' . Adgang::hentTabell() . '`.`epostvarsling` = 1');

            // Utelat utløpte leieforhold
            $forslagSett->leggTilFilter(['or' => [
                'tildato' => null,
                '`' . Leieforhold::hentTabell() . '`.`tildato` >' => $tidligsteJusteringsdato->format('Y-m-d'),
            ]]);

            // Begrens til forslag som faller innenfor terskel for automatisk godkjenning
            $forslagSett->leggTilHaving([
                '`leieforhold.neste_dato` <=' => $tid->add($this->varselfrist)->add($this->autoGodkjenningFørFrist)->format('Y-m-d'),
            ]);

            $forslagSett->låsFiltre();

            $forslagSett->leggTilSortering('neste_dato', false, 'leieforhold');
            $forslagSett->begrens(10);

            /** Repositoriet tømmes fordi vi forventer tilleggsdata i denne samlingen */
            $this->app->modellOppbevaring->tøm();
            if($forslagSett->getChunkRows()) {
                $this->app->logger->info(self::LOG_PREFIX
                    . 'Justerer leie for ' . $forslagSett->getChunkRows() . ' leieforhold', [
                        'fra' => $tidligsteJusteringsdato->format('Y-m-d'),
                        'til' => $tidligsteJusteringsdato->add($this->autoGodkjenningFørFrist)->format('Y-m-d'),
                    ]
                );

                foreach ($forslagSett as $leieforhold) {
                    $this->justerLeieForLeieforhold($leieforhold);
                }
            }

            $this->app->logger->debug(self::LOG_PREFIX . 'Automatisk justering av leie fullført');
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * Justér leien for ett enkelt leieforhold i hht satser for leiejustering
     *
     * @param Leieforhold $leieforhold
     * @param DateTimeImmutable|null $justeringsdato
     * @return Leieregulerer
     * @throws Exception
     */
    public function justerLeieForLeieforhold(Leieforhold $leieforhold, ?DateTimeImmutable $justeringsdato = null): Leieregulerer
    {
        list('leieforhold' => $leieforhold, 'justeringsdato' => $justeringsdato)
            = $this->app->before($this, __FUNCTION__, [
            'leieforhold' => $leieforhold, 'justeringsdato' => $justeringsdato
        ]);
        $originalLeie = $leieforhold->hentÅrligBasisleie();
        try {
            $tidligsteForslagsdato = date_create_immutable()->add($this->varselfrist);
            if(!$justeringsdato) {
                $justeringsdato  = $leieforhold->getRawData()->leieforhold->neste_dato ?? null;
                $justeringsdato = $justeringsdato ? date_create_immutable($justeringsdato) : $tidligsteForslagsdato;
            }

            /* Ignorer utløpte leieforhold, som evt måtte ha blitt med */
            if($leieforhold->tildato && $leieforhold->tildato <= $justeringsdato) {
                return $this->app->after($this, __FUNCTION__, $this);
            }
            $basisLeie = $this->foreslåJustertBasisLeie($leieforhold, $justeringsdato);
            $varselType = $this->hentVarseltypeForLeieforhold($leieforhold);
            $mal = $this->app->hentValg('leiejustering_brevmal') ?? '';
            $justeringsvarsel = $this->fyllJusteringsVarselForLeieforhold($leieforhold, $mal, $justeringsdato, $basisLeie );

            $leieforholdDelkravtyper = $leieforhold->hentDelkravtyper();
            $leieforholdDelkravtyper->leggTilEavVerdiJoin('indeksreguleres', true, Delkravtype::class);
            $leieforholdDelkravtyper
                ->leggTilFilter(['relativ' => false])->låsFiltre()
                ->leggTilFilter(['`indeksreguleres`.`verdi`' => true]);

            foreach ($leieforholdDelkravtyper as $leieforholdDelkravtype) {
                if ($leieforholdDelkravtype->delkravtype->valgfritt) {
                    $justeringsfaktor = $this->hentJusteringsfaktorForLeieforhold($justeringsdato, $leieforhold);
                    $originalSats = $leieforholdDelkravtype->hentSats();
                    $nySats = round($justeringsfaktor->multiply($originalSats)->asDecimal());
                }
                else {
                    $nySats = $leieforholdDelkravtype->delkravtype->sats;
                }
                $leieforholdDelkravtype->settSats($nySats);
            }

            $this->sendReguleringsVarsel($leieforhold, $justeringsvarsel, $varselType);

            $leieforhold->settÅrligBasisleie($basisLeie);
            $leieforhold->oppdaterLeieHistorikk(true, $justeringsdato);
            $leieforhold->opprettLeiekrav($justeringsdato, true);

            /** Registrer justeringsvarselet som et notat på leieforholdet */
            $this->app->nyModell(Notat::class, (object)[
                'leieforhold' => $leieforhold,
                'dato' => date_create_immutable(),
                'henvendelse_fra' => Notat::FRA_UTLEIER,
                'kategori' => 'brev',
                'brevtekst' => $justeringsvarsel,
            ]);
            $this->app->logger->info(self::LOG_PREFIX
                . 'Leiejustering for leieforhold ' . $leieforhold->hentId() . ' utført', ['fra' => $originalLeie, 'til' => $basisLeie]
            );

        } catch (Exception $e) {
            $this->app->logger->critical(Leieregulerer::LOG_PREFIX . $e->getMessage());
            $leieforhold->settÅrligBasisleie($originalLeie);
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * Nullstill advarsler om forestående justeringer som må varsles som brev
     *
     * @return $this
     * @throws Exception
     */
    public function nullstillAdvarslerOmForeståendeBrev(): Leieregulerer {
        $advarsler = json_decode($this->app->hentValg('advarsler_drift')) ?? new \stdClass();
        unset($advarsler->leiejustering_brevvarsler);
        $this->app->settValg('advarsler_drift', json_encode($advarsler));
        return $this;
    }

    /**
     * Sett advarsler om forestående justeringer som må varsles som brev
     *
     * @param int $antallLeieforhold
     * @return $this
     * @throws Exception
     */
    public function settAdvarselOmForeståendeBrev(int $antallLeieforhold): Leieregulerer {
        $advarsler = json_decode($this->app->hentValg('advarsler_drift')) ?? new \stdClass();
        $advarsler->leiejustering_brevvarsler = [Leiebase::VARSELNIVÅ_ORIENTERING => [[
            'oppsummering' => 'Varsel om leiejustering må sendes i posten',
            'tekst' => 'Det foreligger ' . $antallLeieforhold . ' forslag til leiejustering der varsler må sendes i posten. Du finner disse under <a href="/drift/index.php?oppslag=leieregulering_liste" title="Leieregulering">leieregulering</a>.',
        ]]];
        $this->app->settValg('advarsler_drift', json_encode($advarsler));
        return $this;
    }

    /**
     * Se etter forestående justeringer som må varsles som brev
     *
     * @param DateTimeImmutable|null $tid
     * @return Leieforholdsett
     * @throws Exception
     */
    public function hentForeståendeBrevVarsler(?DateTimeImmutable $tid = null): Leieforholdsett
    {
        $this->app->before($this, __FUNCTION__, []);
        if($this->autoGodkjenningFørFrist) {
            $tid = $tid ?? $this->tidsstempel;
            $tidligsteJusteringsdato = $tid->add($this->varselfrist);

            /** @var Leieforholdsett $forslagSett */
            $forslagSett = $this->app->hentSamling(Leieforhold::class);

            // Inkluder kun leieforhold der ingen av leietakerne vil ha epostvarsling
            $forslagSett->leggTilLeftJoin(Adgang::hentTabell(), '`' . Adgang::hentTabell() . '`.`leieforhold` = `' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '` and `' . Adgang::hentTabell() . '`.`epostvarsling` = 1');
            $forslagSett->leggTilFilter(['`' . Adgang::hentTabell() . '`.`leieforhold`' => null]);

            $forslagSett->leggTilEavFelt('leie_oppdatert');

            $forslagSett->leggTilLeftJoin(Leieforhold\Krav::hentTabell(),
                '`' . Leieforhold\Krav::hentTabell() . '`.`leieforhold` = `' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`'
                . ' AND `' . Leieforhold\Krav::hentTabell() . "`.`type` = '" . Leieforhold\Krav::TYPE_HUSLEIE .  "'"
                . ' AND `' . Leieforhold\Krav::hentTabell() . "`.`fom` < '{$tidligsteJusteringsdato->format('Y-m-d')}'"
            );
            $forslagSett->leggTilUttrykk('leieforhold', 'neste_dato', "IFNULL(DATE_ADD(MAX(`krav`.`tom`), INTERVAL 1 DAY), '{$tidligsteJusteringsdato->format('Y-m-d')}')");

            // Utelat oppsagte leieforhold
            $forslagSett->leggTilLeftJoinForOppsigelse();
            $forslagSett->leggTilFilter([
                '`' . Leieforhold\Oppsigelse::hentTabell() . '`.`' . Leieforhold\Oppsigelse::hentPrimærnøkkelfelt() . '`' => null,
            ]);

            // Utelat utløpte leieforhold
            $forslagSett->leggTilFilter(['or' => [
                'tildato' => null,
                '`' . Leieforhold::hentTabell() . '`.`tildato` >' => $tidligsteJusteringsdato->format('Y-m-d'),
            ]]);

            // Utelat leieforhold med leiebeløp 0
            $forslagSett->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`leiebeløp` <>' => 0]);

            // Utelat leieforhold som har blitt fornyet senere enn justeringsintervallet
            $forslagSett->leggTilFilter(['`leie_oppdatert`.`verdi` <=' => $tidligsteJusteringsdato->sub($this->justeringIntervall)->format('Y-m-d')]);

            // Begrens til forslag som faller innenfor tidsvinduet for justeringsforslag
            $forslagSett->leggTilHaving([
                '`leieforhold.neste_dato` <=' => $tidligsteJusteringsdato->add($this->forslagsfrist)->format('Y-m-d'),
            ]);

            $forslagSett->låsFiltre();
        }
        return $this->app->after($this, __FUNCTION__, $forslagSett);
    }

    /**
     * Se etter forestående brev-varsler om leieregulering
     *
     * @param DateTimeImmutable|null $tid
     * @return Leieregulerer
     * @throws Exception
     */
    public function sjekkForeståendeBrevVarsler(?DateTimeImmutable $tid = null): Leieregulerer
    {
        $this->app->before($this, __FUNCTION__, []);
        $this->nullstillAdvarslerOmForeståendeBrev();
        /** @var Leieforholdsett $forslagSett */
        $forslagSett = $this->hentForeståendeBrevVarsler($tid);
        if($this->automatisk && $forslagSett->hentAntall()) {
            $this->settAdvarselOmForeståendeBrev($forslagSett->hentAntall());
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * Hent leieforhold som skal reguleres i nærmeste framtid
     *
     * @param DateTimeImmutable|null $tid
     * @return Leieforholdsett
     * @throws Exception
     */
    public function hentForeståendeLeieforholdEmner(?DateTimeImmutable $tid = null): Leieforholdsett
    {
        list('tid' => $tid)
            = $this->app->before($this, __FUNCTION__, [
            'tid' => $tid
        ]);
        $tid = $tid ?? $this->tidsstempel;
        $tidligsteJusteringsdato = $tid->add($this->varselfrist);

        /** @var Leieforholdsett $forslagSett */
        $forslagSett = $this->app->hentSamling(Leieforhold::class);

        // Legg til felt for sist opppdaterte leie
        $forslagSett->leggTilEavFelt('leie_oppdatert');

        // Legg til første mulige dato for leieregulering av leieforholdet,
        // dvs førstkommende leietermin som kommer etter varslingsfristen
        $forslagSett->leggTilLeftJoin(Leieforhold\Krav::hentTabell(),
            '`' . Leieforhold\Krav::hentTabell() . '`.`leieforhold` = `' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`'
            . ' AND `' . Leieforhold\Krav::hentTabell() . "`.`type` = '" . Leieforhold\Krav::TYPE_HUSLEIE .  "'"
            . ' AND `' . Leieforhold\Krav::hentTabell() . "`.`fom` < '{$tidligsteJusteringsdato->format('Y-m-d')}'"
        );
        $forslagSett->leggTilUttrykk('leieforhold', 'neste_dato', "IFNULL(DATE_ADD(MAX(`krav`.`tom`), INTERVAL 1 DAY), '{$tidligsteJusteringsdato->format('Y-m-d')}')");

        // Utelat oppsagte leieforhold
        $forslagSett->leggTilLeftJoinForOppsigelse();
        $forslagSett->leggTilFilter([
            '`' . Leieforhold\Oppsigelse::hentTabell() . '`.`' . Leieforhold\Oppsigelse::hentPrimærnøkkelfelt() . '`' => null,
        ]);

        // Utelat leieforhold med leiebeløp 0
        $forslagSett->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`leiebeløp` <>' => 0]);

        // Utelat leieforhold som har blitt regulert innenfor justeringsintervallet
        $dateAdd = "DATE_ADD(DATE_ADD(DATE_ADD(`leie_oppdatert`.`verdi`, INTERVAL {$this->justeringIntervall->y} YEAR), INTERVAL {$this->justeringIntervall->m} MONTH), INTERVAL {$this->justeringIntervall->d} DAY)";
        $forslagSett->leggTilHaving([
            "`leieforhold.neste_dato` >= {$dateAdd}",
        ], true);

        // Begrens til forslag som faller innenfor tidsvinduet for justeringsforslag
        $forslagSett->leggTilHaving([
            '`leieforhold.neste_dato` <=' => $tidligsteJusteringsdato->add($this->forslagsfrist)->format('Y-m-d'),
        ]);

        $forslagSett->låsFiltre();
        $forslagSett->leggTilSortering('neste_dato', false, 'leieforhold');
        return $this->app->after($this, __FUNCTION__, $forslagSett);
    }
}