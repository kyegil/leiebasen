<?php

namespace Kyegil\Leiebasen\EavVerdiVisningRenderer;

use Kyegil\Leiebasen\EavVerdiVisningRenderer;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Verdi;
use stdClass;

class Hake extends EavVerdiVisningRenderer
{
    /**
     * @return string
     */
    public static function hentNavn(): string
    {
        return 'Hake';
    }

    /**
     * @return stdClass|null
     */
    public static function hentStandardOppsett(): ?stdClass
    {
        return (object)[
            'true' => '☑',
            'false' => '☐',
            'null' => '☐',
        ];
    }

    /**
     * @param Verdi|null $verdiObjekt
     * @param stdClass|null $oppsett
     * @param Egenskap $egenskap
     * @param string $visningType
     * @param string $bruk
     * @return string|\Kyegil\ViewRenderer\ViewInterface
     */
    public static function vis(
        ?Verdi    $verdiObjekt,
        ?stdClass $oppsett,
        Egenskap  $egenskap,
        string    $visningType,
        string    $bruk
    )
    {
        $verdi = $verdiObjekt ? $verdiObjekt->hentVerdi() : null;
        if($verdi === null) {
            return $oppsett->null;
        }
        return $verdi ? $oppsett->true : $oppsett->false;
    }
}