<?php

namespace Kyegil\Leiebasen\EavVerdiVisningRenderer;

use DateInterval;
use Kyegil\CoreModel\CoreModel;
use Kyegil\Leiebasen\EavVerdiVisningRenderer;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Verdi;
use stdClass;

class Intervall extends EavVerdiVisningRenderer
{
    /**
     * @return string
     */
    public static function hentNavn(): string
    {
        return 'Intervall';
    }

    public static function hentStandardOppsett(): ?stdClass
    {
        return (object)[
            'iso' => false,
        ];
    }

    /**
     * @param Verdi|null $verdiObjekt
     * @param stdClass|null $oppsett
     * @param Egenskap $egenskap
     * @param string $visningType
     * @param string $bruk
     * @return string|\Kyegil\ViewRenderer\ViewInterface
     */
    public static function vis(
        ?Verdi    $verdiObjekt,
        ?stdClass $oppsett,
        Egenskap  $egenskap,
        string    $visningType,
        string    $bruk
    )
    {

        $verdi = $verdiObjekt ? $verdiObjekt->hentVerdi() : null;
        if($verdi instanceof DateInterval) {
            if($oppsett->iso ?? false) {
                return CoreModel::prepareIso8601IntervalSpecification($verdi);
            }
            $elementer = [];
            if($verdi->y) {
                $elementer[] = $verdi->y . ' år';
            }
            if($verdi->m) {
                $elementer[] = $verdi->m . ($verdi->m > 1 ? ' måneder' : ' måned');
            }
            if($verdi->d) {
                $elementer[] = $verdi->d . ($verdi->d > 1 ? ' dager' : ' dag');
            }
            if($verdi->h) {
                $elementer[] = $verdi->h . ($verdi->h > 1 ? ' timer' : ' time');
            }
            if($verdi->i) {
                $elementer[] = $verdi->i . ($verdi->i > 1 ? ' minutter' : ' minutt');
            }
            if($verdi->s) {
                $elementer[] = $verdi->s . ($verdi->s > 1 ? ' sekunder' : ' sekund');
            }
            if($verdi->f) {
                $elementer[] = $verdi->f . ($verdi->f > 1 ? ' mikrosekunder' : ' mikrosekund');
            }
            return implode(', ', $elementer);
        }
        return null;
    }
}