<?php

namespace Kyegil\Leiebasen\EavVerdiVisningRenderer;

use Exception;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\EavVerdiVisningRenderer;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Verdi;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;
use stdClass;

class Html extends EavVerdiVisningRenderer
{
    /**
     * @return string
     */
    public static function hentNavn(): string
    {
        return 'Html';
    }

    /**
     * @return stdClass|null
     */
    public static function hentStandardOppsett(): ?stdClass
    {
        return null;
    }

    /**
     * @param Verdi|null $verdiObjekt
     * @param stdClass|null $oppsett
     * @param Egenskap $egenskap
     * @param string $visningType
     * @param string $bruk
     * @return string|ViewInterface
     * @throws Exception
     */
    public static function vis(
        ?Verdi    $verdiObjekt,
        ?stdClass $oppsett,
        Egenskap  $egenskap,
        string    $visningType,
        string    $bruk
    )
    {
        $verdi = $verdiObjekt ? $verdiObjekt->hentVerdi() : null;
        if($verdi === null) {
            return null;
        }
        if($verdi instanceof Fraction) {
            return $verdi->asFraction();
        }
        if(is_object($verdi) && !method_exists($verdi, '__toString')) {
            return $verdiObjekt->hentStrengVerdi('verdi');
        }
        if(is_array($verdi)) {
            $verdi = array_map([static::class, 'vis'], $verdi);
            $verdi = new ViewArray($verdi);
            $verdi->setGlue(', ');
        }
        return strval($verdi);
    }
}