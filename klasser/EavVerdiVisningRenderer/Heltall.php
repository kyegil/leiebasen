<?php

namespace Kyegil\Leiebasen\EavVerdiVisningRenderer;

use Kyegil\Leiebasen\EavVerdiVisningRenderer;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Verdi;
use Kyegil\ViewRenderer\ViewArray;
use stdClass;

class Heltall extends EavVerdiVisningRenderer
{
    /**
     * @return string
     */
    public static function hentNavn(): string
    {
        return 'Heltall';
    }

    /**
     * @return stdClass|null
     */
    public static function hentStandardOppsett(): ?stdClass
    {
        return null;
    }

    /**
     * @param Verdi|null $verdiObjekt
     * @param stdClass|null $oppsett
     * @param Egenskap $egenskap
     * @param string $visningType
     * @param string $bruk
     * @return string|\Kyegil\ViewRenderer\ViewInterface
     */
    public static function vis(
        ?Verdi    $verdiObjekt,
        ?stdClass $oppsett,
        Egenskap  $egenskap,
        string    $visningType,
        string    $bruk
    )
    {
        $verdi = $verdiObjekt ? $verdiObjekt->hentVerdi() : null;
        if($verdi === null) {
            return null;
        }
        if(is_object($verdi) && !method_exists($verdi, '__toString')) {
            return null;
        }
        if(is_array($verdi)) {
            $verdi = array_map([static::class, 'vis'], $verdi);
            $verdi = new ViewArray($verdi);
            $verdi->setGlue(', ');
        }
        return (int)strval($verdi);
    }
}