<?php

namespace Kyegil\Leiebasen\EavVerdiVisningRenderer;

use Kyegil\Leiebasen\CoreModelImplementering;
use Kyegil\Leiebasen\EavVerdiVisningRenderer;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Verdi;
use stdClass;

class Beløp extends EavVerdiVisningRenderer
{
    /**
     * @return string
     */
    public static function hentNavn(): string
    {
        return 'Beløp';
    }

    /**
     * @return stdClass|null
     */
    public static function hentStandardOppsett(): ?stdClass
    {
        return (object)[
            'html' => true,
            'prefiks' => true,
        ];
    }

    /**
     * @param Verdi|null $verdiObjekt
     * @param stdClass|null $oppsett
     * @param Egenskap $egenskap
     * @param string $visningType
     * @param string $bruk
     * @return string|\Kyegil\ViewRenderer\ViewInterface
     */
    public static function vis(
        ?Verdi    $verdiObjekt,
        ?stdClass $oppsett,
        Egenskap  $egenskap,
        string    $visningType,
        string    $bruk
    )
    {
        $verdi = $verdiObjekt ? $verdiObjekt->hentVerdi() : null;
        /** @var CoreModelImplementering $app */
        $app = $egenskap->hentApp();
        if($verdi === null) {
            return null;
        }
        return $app->kr(
            Html::vis($verdiObjekt, $oppsett, $egenskap, $visningType, $bruk),
            $oppsett->html ?? true,
            $oppsett->prefiks ?? true
        );
    }
}