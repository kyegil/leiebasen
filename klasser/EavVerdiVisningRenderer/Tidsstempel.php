<?php

namespace Kyegil\Leiebasen\EavVerdiVisningRenderer;

use DateTimeInterface;
use Kyegil\Leiebasen\EavVerdiVisningRenderer;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Verdi;
use stdClass;

class Tidsstempel extends EavVerdiVisningRenderer
{
    /**
     * @return string
     */
    public static function hentNavn(): string
    {
        return 'Tidsstempel';
    }

    /**
     * @return stdClass|null
     */
    public static function hentStandardOppsett(): ?stdClass
    {
        return (object)[
            'format' => 'd.m.Y H:i:s',
        ];
    }

    /**
     * @param Verdi|null $verdiObjekt
     * @param stdClass|null $oppsett
     * @param Egenskap $egenskap
     * @param string $visningType
     * @param string $bruk
     * @return string|\Kyegil\ViewRenderer\ViewInterface
     */
    public static function vis(
        ?Verdi    $verdiObjekt,
        ?stdClass $oppsett,
        Egenskap  $egenskap,
        string    $visningType,
        string    $bruk
    )
    {
        $verdi = $verdiObjekt ? $verdiObjekt->hentVerdi() : null;
        if(!is_a($verdi, DateTimeInterface::class)) {
            return null;
        }
        return $verdi->format($oppsett->format ?? 'd.m.Y H:i:s');
    }
}