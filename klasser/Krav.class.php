<?php

use Kyegil\Leiebasen\Modell\Leieforhold\Krav as KravModell;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;

/**
 * @deprecated
 * @see \Kyegil\Leiebasen\Modell\Leieforhold\Krav
 */
class Krav extends DatabaseObjekt {

    /**
     * @var string
     */
    protected	$tabell = "krav";	// Hvilken tabell i databasen som inneholder primærnøkkelen for dette objektet
    /**
     * @var string
     */
    protected	$idFelt = "id";		// Hvilket felt i tabellen som lagrer primærnøkkelen for dette objektet
    /**
     * @var
     */
    protected	$data;				// DB-verdiene lagret som et objekt Null betyr at verdiene ikke er lastet
    /**
     * @var
     */
    protected	$delkrav;			// Liste med stdClass-objekter. Null betyr at verdiene ikke er lastet
    /**
     * @var
     */
    protected	$purringer;			// Array over alle purringene på dette kravet. Null betyr at purringene ikke er lastet.
    /**
     * @var array
     */
    protected	$utskriftsposisjon = array();	// Utskriftsposisjonen for hver enkelt rute, sortert som et array med rutenummeret som nøkkel
    /**
     * @var
     */
    public		$id;				// Unikt id-heltall for dette objektet


    /**
     * Krav constructor.
     * @param array|object|null $param
     *  ->id	(heltall) Krav-id'en
     * @throws Exception
     */
    public function __construct($param = null ) {
    	parent::__construct( $param );
    }


    /**
     * Last kravets kjernedata fra databasen
     *
     * @param int $id gironummeret
     * @return bool|mixed
     * @throws Exception
     */
    protected function last($id = 0) {
        $tp = $this->mysqli->table_prefix;

        settype($id, 'integer');
        if( !$id ) {
            $id = $this->id;
        }

        $resultat = $this->mysqli->arrayData(array(
            'returnQuery'	=> true,

            'fields' =>			"{$this->tabell}.*,\n"
                            .	"leieforhold.leieforhold,
                                leieforhold.leieobjekt,
                                leieforhold.regningsperson,
                                leieforhold.regning_til_objekt,
                                leieforhold.regningsobjekt,
                                leieforhold.regningsadresse1,
                                leieforhold.regningsadresse2,
                                leieforhold.postnr,
                                leieforhold.poststed,
                                leieforhold.land",

            'source' => 		"{$tp}{$this->tabell} AS {$this->tabell}\n"
                            .	"LEFT JOIN {$tp}kontrakter AS leieforhold ON {$this->tabell}.kontraktnr = leieforhold.kontraktnr\n"
                            .	"LEFT JOIN {$tp}leieobjekter AS leieobjekter ON leieforhold.leieobjekt = leieobjekter.leieobjektnr\n",

            'where'			=>	"{$tp}{$this->tabell}.{$this->idFelt} = '$id'"
        ));

        if( $resultat->totalRows ) {
            $this->data = $resultat->data[0];
            $this->id = $id;

            $this->data->leieforhold	= $this->leiebase->hent(\Leieforhold::class, $this->data->leieforhold );
            $this->data->kravdato		= new DateTime( $this->data->kravdato );
            $this->data->dato			= $this->data->kravdato;
            $this->data->opprettet		= new DateTime( $this->data->opprettet );

            if( $this->data->gironr ) {
                $this->data->giro = $this->leiebase->hent(\Giro::class, $this->data->gironr );
            }
            else {
                $this->data->giro = null;
            }

            return true;
        }
        else {
            $this->id = null;
            $this->data = null;

            return false;
        }

    }


    /**
     * Last alle delkravene på kravet fra databasen
     *
     * @return bool suksess
     * @throws Exception
     */
    protected function lastDelkrav() {
        $tp = $this->mysqli->table_prefix;

        if ( !$id = $this->id ) {
            $this->delkrav = null;
            return false;
        }

        $resultat = $this->mysqli->arrayData(array(
            'returnQuery'	=> true,
            'fields'		=> "delkravtyper.id, delkravtyper.kode, delkravtyper.navn, delkravtyper.beskrivelse, delkrav.beløp\n",
            'source'		=> "{$tp}delkrav AS delkrav\n"
                            . "INNER JOIN {$tp}delkravtyper AS delkravtyper ON delkrav.delkravtype = delkravtyper.id\n",
            'where'			=> "delkrav.kravid = '$id'",
            'orderfields'	=> "delkravtyper.orden, delkrav.id"
        ));

        $this->delkrav = $resultat->data;
        return true;
    }


    /**
     * Last utskriftsposisjonen for kravet
     *
     * @param null $rute utdelingsruten som bestemmer utskriftsrekkefølgen
     * @throws Exception
     */
    protected function lastUtskriftsposisjon($rute = null):void {
        $tp = $this->mysqli->table_prefix;
        settype($rute, 'integer');
        if ( !$id = $this->id ) {
            $this->purringer = null;
            return;
        }

        if( $this->hent('regning_til_objekt') ) {
            $this->utskriftsposisjon[$rute] = 1000000 * ($this->mysqli->arrayData(array(
                'returnQuery'	=> true,

                'fields' =>			"utdelingsorden.plassering\n",
                'source' => 		"{$tp}utdelingsorden AS utdelingsorden\n",
                'where'			=>	"utdelingsorden.leieobjekt = '{$this->hent('regningsobjekt')}' AND rute = '{$rute}'"
            ))->data[0]->plassering)
            + $this->hent('leieforhold');
        }
        else {
            $this->utskriftsposisjon[$rute] = intval((string)$this->hent('leieforhold'));
        }
    }


    /**
     * Hent egenskaper
     *
     * @param string $egenskap
     * @param array|stdClass $param
     * @return bool|mixed|null
     * @throws Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Leieforhold\Krav::hent()
     */
    public function hent($egenskap, $param = []) {

        if( !$this->id ) {
            return null;
        }

        /** @var KravModell $kravModell */
        $kravModell = $this->leiebase->hentModell(KravModell::class, $this->id);
        if( !$kravModell->hentId() ) {
            return null;
        }
        switch( $egenskap ) {

            case $this->idFelt: {
                if ( $this->data === null and !$this->last()) {
                    return false;
                }
                return $this->data->$egenskap;
            }

            case 'beløp':
                return $kravModell->hentBeløp();
            case 'dato':
            case 'kravdato':
                return $kravModell->hentDato();
            case 'fom':
                return $kravModell->hentFom();
            case 'tom':
                return $kravModell->hentTom();
            case 'forfall':
                return $kravModell->hentForfall();
            case 'oppretter':
                return $kravModell->hentOppretter();
            case 'opprettet':
                return $kravModell->hentOpprettet();
            case 'tekst':
                return $kravModell->hentTekst();
            case 'termin':
                return $kravModell->hentTermin();
            case "type":
                return $kravModell->hentType();
            case "utestående":
               return $kravModell->hentUtestående();
            case 'regning_til_objekt':
                return $kravModell->leieforhold->regningTilObjekt;
            case 'utskriftsdato':
                return $kravModell->regning ? $kravModell->regning->hentUtskriftsdato() : null;
            case 'anleggsnr':
            case "giro":
            case "gironr":
            case "leieforhold":
            case "leieobjekt":
            case "regningsobjekt": {
                if ( $this->data === null and !$this->last()) {
                    throw new Exception("Klarte ikke laste Krav({$this->id})");
                }
                return $this->data->$egenskap;
            }

            case "utskriftsposisjon": {
                if ( $this->data == null ) {
                    $this->last();
                }

                if ( !$this->utskriftsposisjon[$param['rute']] ) {
                    $this->lastUtskriftsposisjon($param['rute']);
                }
                return $this->utskriftsposisjon[$param['rute']];
            }

            /*
            retur: (serie) stdClass-objekter med egenskapene
                id	(heltall):	Id for denne delkravtypen
                kode (streng):	Koden for denne delkravtypen
                navn (streng): Navnet på denne delkravtypen
                beskrivelse (streng): Beskrivelse av denne delkravtypen
                beløp (tall): Beløpet
            */
            case "delkrav": {
                if ( $this->delkrav === null ) {
                    $this->lastDelkrav();
                }
                return $this->delkrav;
            }

            case "solidaritetsfondbeløp": {
                return $this->hentDel(1);
            }

            case "antallPurringer":
            /** @var KravModell $kravmodell */
            $kravmodell = $this->leiebase->hentModell(KravModell::class, $this->id);
            return $kravmodell->hentAntallPurringer();

            /*
            retur: (DateTime-objekt eller false) Evt dato da kravet ble betalt ned
            */
            case "betalt": {
                if ( $this->hent('utestående') != 0 ) {
                    return false;
                }
                $betalinger = $this->hentBetalinger();
                if(!$betalinger) {
                    return $this->hent('dato');
                }
                return end($betalinger)->hent('dato');
            }

            default: {
                return null;
            }
        }

    }



    /**
     * Hent Betalinger
     *
     * Se etter betalinger ført mot dette kravet
     *
     * @return array Innbetalingene
     * @throws Exception
     */
    public function hentBetalinger() {
        $tp = $this->mysqli->table_prefix;

        $innbetalinger = $this->mysqli->arrayData(array(
            'source'		=>	"{$tp}innbetalinger as innbetalinger",
            'where'			=> "innbetalinger.krav = '{$this->hentId()}'",
            'distinct'		=> true,
            'fields'		=> "innbetalinger.dato, innbetalinger.innbetaling as id",
            'orderfields'	=> "innbetalinger.dato, innbetalinger.innbetaling",
            'class'			=> \Innbetaling::class
        ));

        return $innbetalinger->data;
    }



    /**
     * Hent del
     *
     * Hent et bestemt delbeløp i kravet
     *
     * @param int|string $delkravtype Angi delkravtypen som id eller kode
     *  Dersom delkravtypen ikke er angitt returneres alle delkravene som et objekt
     *  med delkravkodene som egenskaper
     * @return float|stdclass aktuelle delbeløpe(-t/-ne)
     * @throws Exception
     */
    public function hentDel($delkravtype = null) {
        $resultat = new stdclass;

        foreach( $this->hent('delkrav') as $delkrav ) {
            if(
                $delkravtype === null
                || $delkravtype == $delkrav->id
                || $delkravtype == $delkrav->kode
            ) {
                if(
                    $delkravtype !== null
                ) {
                    return $delkrav->beløp;
                }

                $resultat->{$delkrav->kode} = $delkrav->beløp;
            }
        }

        //	Ingen treff
        if(
            $delkravtype !== null
        ) {
            return 0;
        }

        return $resultat;
    }

    /**
     * Hent Krediteringer
     *
     * Se etter evt kredittkrav opprettet for å motvirke dette kravet.
     *
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Leieforhold\Krav::hentAnnullering()
     * @return \Krav[] Kredittene som Krav-objekter
     * @throws Exception
     */
    public function hentKrediteringer() {
        $tp = $this->mysqli->table_prefix;

        if($this->hent('beløp') < 0) {
            return array();
        }

        $kredittkrav = $this->mysqli->arrayData(array(
            'source'	=>	"{$tp}krav as krav INNER JOIN {$tp}innbetalinger as innbetalinger ON krav.id = innbetalinger.krav",
            'where'		=> "innbetalinger.konto = '0' AND krav.beløp < 0 AND innbetalinger.ref = '{$this->hentId()}'",
            'distinct'	=> true,
            'fields'	=> "krav.id",
            'class'		=> \Krav::class
        ));

        return $kredittkrav->data;
    }



    /**
     * Hent Kredittkopling
     *
     * Dersom dette er kreditt hentes koplingen som er lagret som innbetaling
     *
     * @return false|Innbetaling Den aktuelle koplingen
     * @throws Exception
     */
    public function hentKredittkopling() {
        $tp = $this->mysqli->table_prefix;

        if($this->hent('beløp') > 0) {
            return false;
        }

        $kopling = $this->mysqli->arrayData(array(
            'source'	=>	"{$tp}innbetalinger as innbetalinger",
            'where'		=> "innbetalinger.konto = '0'
                            AND innbetalinger.krav = '{$this->hentId()}'",
            'distinct'	=> true,
            'fields'	=> "innbetalinger.innbetaling AS id",
            'class'		=> \Innbetaling::class
        ));

        return reset($kopling->data);
    }



    /**
     * Hent Utlikninger
     *
     * Se etter delbetalinger ført mot dette kravet
     *
     * @return array stdClass-objekter med egenskapene:
     *  ->id:	(heltall) Id'en for dette delbeløpet
     *  ->beløp: (tall) Delbeløpet som er ført mot dette kravet
     *  ->innbetaling: (Innbetaling-objekt) Innbetalinga
     * @throws Exception
     */
    public function hentUtlikninger() {
        $tp = $this->mysqli->table_prefix;

        $innbetalinger = $this->mysqli->arrayData(array(
            'source'		=>	"{$tp}innbetalinger as innbetalinger",
            'where'			=> "innbetalinger.krav = '{$this->hentId()}'",
            'distinct'		=> true,
            'fields'		=> "innbetalinger.innbetalingsid as id, innbetalinger.innbetaling, innbetalinger.beløp",
            'orderfields'	=> "innbetalinger.dato, innbetalinger.innbetaling",
            'class'			=> "stdClass"
        ));
        foreach( $innbetalinger->data as $del ) {
            $del->innbetaling = $this->leiebase->hent(\Innbetaling::class, $del->innbetaling);
        }

        return $innbetalinger->data;
    }



    /**
     * Oppretter et nytt krav i databasen og tildeler egenskapene til dette objektet
     *
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Leieforhold\Krav::opprett()
     * @param array|stdClass $egenskaper
     * @throws Exception
     */
    public function opprett($egenskaper = []) {
        throw new Exception('The \\Krav Model has been deprecated');
    }
}