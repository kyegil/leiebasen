<?php

namespace Kyegil\Leiebasen;

use Exception;
use mysqli;

/**
 *
 */
abstract class DatabaseInstallerer
{
    const FLAGG = 'var/DB_OPPDATERING';

    /** @var string */
    protected $table_prefix = '';

    /** @var Leiebase */
    protected $app;

    /** @var mysqli */
    protected $mysqli;

    /** @var string */
    protected $kodeVersjon;

    /** @var bool */
    protected $nedgradering = false;

    protected $dbVersjonFelt = 'versjon';

    protected $objektbeskrivelse = 'databasen';

    /**
     * @param Leiebase $app
     * @param mysqli $mysqli
     */
    public function __construct(
        Leiebase $app,
        mysqli $mysqli
    )
    {
        $this->app = $app;
        $this->mysqli = $mysqli;
    }

    /**
     * @return string
     */
    public function hentDbVersjon(): string
    {
        try {
            if ($this->app->hentValg($this->dbVersjonFelt)) {
                $versjonOgTrinn = explode('|', $this->app->hentValg($this->dbVersjonFelt));
                return reset($versjonOgTrinn);
            }
            return '0';
        }
        catch (Exception $e) {
            if(strpos($e->getMessage(), "doesn't exist") !== false) {
                return '0';
            }
            throw $e;
        }
    }

    /**
     * @param string $dbVersjon
     * @param int|null $installeringstrinn
     * @return $this
     */
    public function settDbVersjon(string $dbVersjon, ?int $installeringstrinn = null): DatabaseInstallerer
    {
        $tp = $this->hentTablePrefix();
        if (isset($installeringstrinn)) {
            $dbVersjon .= '|' . $installeringstrinn;
        }
        $this->mysqli->query("REPLACE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES ('" . $this->mysqli->real_escape_string($this->dbVersjonFelt) . "','" . $this->mysqli->real_escape_string($dbVersjon) . "')");
        return $this;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function hentKodeVersjon(): string
    {
        if (!isset($this->kodeVersjon)) {
            $composerFile = $this->app->root . 'composer.json';
            if(!is_readable($composerFile)) {
                throw new Exception('composer.json is not readable');
            }
            $composerJson = file_get_contents($composerFile);
            $composerContent = json_decode($composerJson);
            if (!$composerContent) {
                throw new Exception('Could not parse composer.json');
            }
            $this->kodeVersjon = $composerContent->version ?? '0';
        }
        return $this->kodeVersjon;
    }

    /**
     *
     * Dersom et trinn er angitt i tillegg til database-versjon,
     * så indikerer dette at en oppdatering til neste tilgjengelige versjon har blitt påbegynt,
     * Trinnet viser til hvor mange trinn i oppdateringsprosessen mot neste versjon som har blitt vellykket fullført
     * * null betyr at vi har en ren versjon
     * * Et heltall viser hvor mange trinn i en påbegynt oppdatering som er fullført.
     *
     * @return int|null
     */
    public function hentTrinn(): ?int
    {
        try {
            if ($this->app->hentValg($this->dbVersjonFelt)) {
                $versjonOgTrinn = explode('|', $this->app->hentValg($this->dbVersjonFelt));
                return count($versjonOgTrinn) > 1 ? intval($versjonOgTrinn[1]) : null;
            }
            return null;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @param int|null $trinn
     * @return $this
     * @throws Exception
     */
    public function settTrinn(?int $trinn): DatabaseInstallerer
    {
        $tp = $this->hentTablePrefix();
        $dbVersjon = $this->hentDbVersjon();
        if (isset($trinn)) {
            $dbVersjon .= '|' . $trinn;
        }

        $trinnOppdatert = $this->mysqli->query("REPLACE INTO `{$tp}valg` (`innstilling`, `verdi`) VALUES ('" . $this->mysqli->real_escape_string($this->dbVersjonFelt) . "','" . $this->mysqli->real_escape_string($dbVersjon) . "')");
        if (!$trinnOppdatert) {
            throw new Exception('Klarte ikke oppdatere oppdateringstrinn ' . $dbVersjon . ': ' . $this->mysqli->error);
        }
        return $this;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function erOppdatert(): bool
    {
        $this->app->pre($this, __FUNCTION__, []);
        $oppdatert = version_compare($this->hentKodeVersjon(), $this->hentDbVersjon()) === 0
            && $this->hentTrinn() === null;
        return $this->app->post($this, __FUNCTION__, $oppdatert);
    }

    /**
     * Installerer eller oppdaterer databasen så den matcher kodeversjonen
     *
     * @return $this
     * @throws \Throwable
     */
    public function oppdater(): DatabaseInstallerer
    {
        $this->app->pre($this, __FUNCTION__, []);
        $kodeVersjon = $this->hentKodeVersjon();
        $dbVersjon = $this->hentDbVersjon();
        if (version_compare($kodeVersjon, $dbVersjon) === -1) {
            $this->nedgradering = true;
        }
        else if (version_compare($kodeVersjon, $dbVersjon) === 0
            && $this->hentTrinn() !== null
        ) {
            $this->nedgradering = true;
        }
        else if (version_compare($kodeVersjon, $dbVersjon) === 0) {
            $this->fjernDbOppdateringsflagg();
            return $this->app->post($this, __FUNCTION__, $this);
        }

        $this->settDbOppdateringsflagg($dbVersjon, $kodeVersjon, $this->nedgradering);

        try {
            $skript = $this->hentAktuelleSkript($dbVersjon, $this->hentTrinn(), $kodeVersjon);
            if($this->nedgradering) {
                foreach(array_reverse($skript) as $skriptVersjon => $trinn) {
                    $this->utførNedgraderingerTilVersjon($skriptVersjon, $trinn);
                }
            }
            else {
                foreach ($skript as $skriptVersjon => $trinn) {
                    $this->utførOppdateringerForVersjon($skriptVersjon, $trinn);
                }
            }
            $this->settDbVersjon($kodeVersjon);
                $this->fjernDbOppdateringsflagg();
        } catch (\Throwable $e) {
            $this->fjernDbOppdateringsflagg();
            throw $e;
        }
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @return string
     */
    public function hentTablePrefix(): string
    {
        return $this->table_prefix;
    }

    /**
     * @param string $table_prefix
     * @return $this
     */
    public function settTablePrefix(string $table_prefix): DatabaseInstallerer
    {
        $this->table_prefix = $table_prefix;
        return $this;
    }

    /**
     * @return array
     */
    abstract protected function hentVersjonSkript(): array;

    /**
     * @param string $skriptVersjon
     * @param array $instrukser
     * @return $this
     * @throws Exception
     */
    private function utførOppdateringerForVersjon(string $skriptVersjon, array $instrukser): DatabaseInstallerer
    {
        $fullførtTrinn = 0;
        file_put_contents($this->app->root . self::FLAGG, "Oppdaterer til versjon {$skriptVersjon}<br>", FILE_APPEND);
        foreach($instrukser as $instruks) {
            if($this->hentTrinn() && $fullførtTrinn < $this->hentTrinn()) {
                $fullførtTrinn++;
                continue;
            }

            $kommandoer = $instruks['+'] ?? [];
            $kommandoBeskrivelse = 'Trinn ' . ($fullførtTrinn + 1) . ' av ' . count($instrukser) . (isset($instruks['+i']) ? " ({$instruks['+i']})" : '');
            file_put_contents($this->app->root . self::FLAGG, "{$kommandoBeskrivelse}<br>", FILE_APPEND);
            foreach ($kommandoer as $kommando) {
                set_time_limit(300);
                if (is_callable($kommando)) {
                    $kommando($this->mysqli, $this->app);
                }
                else if (is_string($kommando)) {
                    $utført = $this->mysqli->query($kommando);
                    if (!$utført) {
                        throw new Exception('Mislykket oppdateringskommando for versjon ' . $skriptVersjon . ' i trinn ' . ($fullførtTrinn + 1) . ': ' . $this->mysqli->error);
                    }
                }
                else {
                    throw new Exception('Ugyldig oppdateringskommando for versjon' . $skriptVersjon . ' i trinn ' . ($fullførtTrinn + 1));
                }
            }
            $fullførtTrinn++;
            $this->settTrinn($fullførtTrinn);
        }
        $this->settDbVersjon($skriptVersjon);
        $this->app->hentValg();
        return $this;
    }

    /**
     * @param string $skriptVersjon
     * @param array $instrukser
     * @return $this
     * @throws Exception
     */
    private function utførNedgraderingerTilVersjon(string $skriptVersjon, array $instrukser): DatabaseInstallerer
    {
        $fullførtTrinn = count($instrukser);
        $instrukser = array_reverse($instrukser);
        file_put_contents($this->app->root . self::FLAGG, "Nedgraderer til versjon {$skriptVersjon}<br>", FILE_APPEND);
        foreach($instrukser as $instruks) {
            if($this->hentTrinn() !== null && $fullførtTrinn > $this->hentTrinn()) {
                $fullførtTrinn--;
                continue;
            }

            $kommandoer = $instruks['-'] ?? [];
            $kommandoBeskrivelse = 'Reverserer trinn ' . ($fullførtTrinn) . (isset($instruks['-i']) ? " ({$instruks['-i']})" : '');
            file_put_contents($this->app->root . self::FLAGG, "{$kommandoBeskrivelse}<br>", FILE_APPEND);
            foreach ($kommandoer as $kommando) {
                set_time_limit(300);
                if (is_callable($kommando)) {
                    $kommando($this->mysqli, $this->app);
                }
                else if (is_string($kommando)) {
                    $utført = $this->mysqli->query($kommando);
                    if (!$utført) {
                        throw new Exception('Mislykket nedgraderingskommando til versjon ' . $skriptVersjon . ' i trinn ' . ($fullførtTrinn + 1) . ': ' . $this->mysqli->error);
                    }
                }
                else {
                    throw new Exception('Ugyldig nedgraderingskommando til versjon' . $skriptVersjon . ' i trinn ' . ($fullførtTrinn + 1));
                }
            }
            $fullførtTrinn--;
            $this->settDbVersjon($skriptVersjon, $fullførtTrinn);
        }

        $this->settDbVersjon($skriptVersjon);
        $this->app->hentValg();
        return $this;
    }

    /**
     * @param string $startVersjon
     * @param int|null $fullførtTrinn
     * @param string $målVersjon
     * @return array
     */
    protected function hentAktuelleSkript(
        string $startVersjon,
        ?int   $fullførtTrinn,
        string $målVersjon
    ): array
    {
        $resultat = [];
        if (
            version_compare($målVersjon, $startVersjon) === 0
            && $fullførtTrinn === null
        ) {
            return [];
        }

        $versjonSkript = $this->hentVersjonSkript();
        uksort($versjonSkript, function ($versjonA, $versjonB) {
            return version_compare($versjonA, $versjonB);
        });

        /**
         * Hver versjon i versjon-skriptene beveger seg fra én versjon (bunn-versjon) til en ny (topp-versjon)
         *
         * @var string $bunnVersjon
         * @var string $toppVersjon
         * @var array[][] $kommandoTrinn
         */
        $bunnVersjon = '0';
        /** @var array[][]|null $påbegyntSkript */
        $påbegyntSkript = null;
        foreach ($versjonSkript as $toppVersjon => $kommandoTrinn) {
            if($fullførtTrinn !== null) {
                if(version_compare($bunnVersjon, $startVersjon) < 0) {
                    $påbegyntSkript = $kommandoTrinn;
                }
                else if ($påbegyntSkript && $this->nedgradering) {
                    $resultat[$startVersjon] = $påbegyntSkript;
                }
            }
            if ($this->nedgradering) {
                /**
                 * Ved nedgradering vurderer vi om toppversjonen er mindre enn eller lik versjonen vi kommer fra,
                 * og om den er større enn versjonen vi skal nedgradere til.
                 */
                if (
                    (version_compare($toppVersjon, $startVersjon) <= 0  && version_compare($toppVersjon, $målVersjon) > 0)
                    || ($bunnVersjon == $målVersjon && $fullførtTrinn !== null)
                ) {
                    if (version_compare($bunnVersjon, $målVersjon) <= 0) {
                        $resultat[$målVersjon] = $kommandoTrinn;
                    }
                    else {
                        $resultat[$bunnVersjon] = $kommandoTrinn;
                    }
                }
            }
            else {
                /**
                 * Ved oppgradering vurderer vi om toppversjonen er større enn versjonen vi kommer fra,
                 * og om den er mindre enn eller lik versjonen vi skal oppdatere til.
                 */
                if (
                    (version_compare($toppVersjon, $startVersjon) == 1 && version_compare($toppVersjon, $målVersjon) <= 0)
                    || ($bunnVersjon == $startVersjon && $fullførtTrinn !== null)
                ) {
                    $resultat[$toppVersjon] = $kommandoTrinn;
                }
            }
            $bunnVersjon = $toppVersjon;
        }
        if ($this->nedgradering) {
            if (!$resultat && $påbegyntSkript) {
                $resultat[$startVersjon] = $påbegyntSkript;
            }
            settype($resultat[$startVersjon], 'array');
        }
        return $resultat;
    }

    /**
     * @param string $fraVersjon
     * @param string $tilVersjon
     * @param bool $nedgradering
     * @return $this
     * @throws Exception
     */
    private function settDbOppdateringsflagg(string $fraVersjon, string $tilVersjon, bool $nedgradering)
    {
        if (!file_exists($this->app->root . self::FLAGG)) {
            ob_end_clean();
            header("Connection: close\r\n");
            header("Content-Encoding: none\r\n");
            ignore_user_abort(true);
            ob_start();
            echo ($nedgradering ? 'Nedgraderer' : 'Oppdaterer') . " {$this->objektbeskrivelse} fra versjon {$fraVersjon} til versjon {$tilVersjon}<br><br>---<br>";
            $size = ob_get_length();
            header("Content-Length: $size\r\n");
            header("Refresh: 1;\r\n");
            ob_end_flush();flush(); // Strange behaviour, will not work unless both are called !
        }

        $dbOppdateringsflaggInnhold = $this->hentDbOppdateringsflaggInnhold($fraVersjon, $tilVersjon, $nedgradering);
        if(file_put_contents($this->app->root . self::FLAGG, $dbOppdateringsflaggInnhold, FILE_APPEND) === false) {
            throw new Exception('Kan ikke sette database oppdateringsflagg');
        }
        return $this;
    }

    /**
     *
     */
    public function fjernDbOppdateringsflagg()
    {
        $this->app->pre($this, __FUNCTION__, []);
        if (file_exists($this->app->root . self::FLAGG)) {
            unlink($this->app->root . self::FLAGG);
        }
        return $this->app->post($this, __FUNCTION__, $this);

    }

    /**
     * @param string $fraVersjon
     * @param string $tilVersjon
     * @param bool $nedgradering
     * @return string[]
     */
    public function hentDbOppdateringsflaggInnhold(string $fraVersjon, string $tilVersjon, bool $nedgradering): array
    {
        return [
            'Oppdatering av databasen pågår<br>',
            "Fra versjon: {$fraVersjon}<br>",
            "Til versjon: {$tilVersjon}<br>",
            '<br>',
            '----<br>',
            '<br>'
        ];
    }
}