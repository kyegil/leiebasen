<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 29/05/2023
 * Time: 12:43
 */

namespace Kyegil\Leiebasen;

use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Modell\Søknad\Type;
use Kyegil\Leiebasen\Modell\Søknad\Typesett;
use Kyegil\Leiebasen\Modell\Søknadsett;
use Kyegil\Leiebasen\Visning\felles\epost\shared\Epost;
use Kyegil\Leiebasen\Visning\felles\epost\søknad\PåminnelseOmUtløp;

/**
 *
 */
class Søknadprosessor
{
    /** @var Søknadprosessor */
    private static Søknadprosessor $instance;
    /** @var CoreModelImplementering */
    protected CoreModelImplementering $app;

    /**
     * @param CoreModelImplementering $app
     * @return Søknadprosessor
     */
    public static function getInstance(CoreModelImplementering $app): Søknadprosessor
    {
        if ( !isset( self::$instance ) )
        {
            self::$instance = new self($app);
        }
        return self::$instance;
    }

    /**
     * @param CoreModelImplementering $leiebase
     * @param array $argumenter
     * @return array
     */
    public static function cron(CoreModelImplementering $leiebase, array $argumenter = []): array {
        /** @var DateTimeInterface $cronTid */
        try {
            $cronTid = $argumenter[0] ?? date_create_immutable();
            if ($cronTid->format('H') > 2) {
                $prosessor = self::getInstance($leiebase);
                $prosessor->prosesserSøknadstyper();
            }
        } catch (Exception $e) {
            $leiebase->logger->critical('Søknadsprosessor ' . $e->getMessage());
        }
        return $argumenter;
    }

    /**
     * @param CoreModelImplementering $app
     */
    private function __construct(CoreModelImplementering $app)
    {
        $this->app = $app;
    }

    /**
     * @param string|null $mal
     * @param Søknad $søknad
     * @return string
     * @throws Exception
     */
    public function gjengiVarselmal(?string $mal, Søknad $søknad): string
    {
        settype($mal, 'string');
        $gyldighet = $this->hentGyldighet($søknad->type);
        $navn = $søknad->hent(Søknad\Type::FELT_KONTAKT_FORNAVN);
        $navn = ($navn ? ($navn . ' ') : '')
            . $søknad->hent(Søknad\Type::FELT_KONTAKT_ETTERNAVN);
        /** @var DateTimeImmutable $oppdatert */
        $oppdatert = $søknad->hentOppdatert();
        if($oppdatert instanceof DateTime) {
            $oppdatert = DateTimeImmutable::createFromMutable($oppdatert);
        }
        $variabler = [
            'navn' => $navn,
            'adgangEpost' => $søknad->hent(Søknad\Type::FELT_KONTAKT_EPOST),
            'adgangUrl' => $this->app->http_host . '/min-søknad/index.php',
            'adgangReferanse' => '',
            'utleier' => $this->app->hentValg('utleier'),
            'kontaktadresse' => $this->app->hentValg('epost'),
            'registrert' => $søknad->registrert->format('d.m.Y'),
            'oppdatert' => $søknad->oppdatert->format('d.m.Y'),
            'utløper' => null
        ];
        if($gyldighet && $søknad->hentOppdatert()) {
            $variabler['utløper'] = $oppdatert->add($gyldighet)->format('d.m.Y');
        }
        foreach($variabler as $variabel => $verdi) {
            $mal = str_replace("{{$variabel}}", $verdi, $mal);
        }
        return $mal;
    }

    /**
     * @param Type $søknadstype
     * @return DateInterval|null
     */
    public function hentGyldighet(Søknad\Type $søknadstype): ?DateInterval
    {
        try {
            $gyldighet = $søknadstype->hentKonfigurering('søknad_gyldighet') ?? '';
            return trim($gyldighet) ? new DateInterval($gyldighet) : null;
        }
        catch (Exception $e) {
            return null;
        }
    }

    /**
     * @return Søknadprosessor
     * @throws Exception
     */
    public function prosesserSøknadstyper(): Søknadprosessor
    {
        $this->app->before($this, __FUNCTION__, []);
        /** @var Typesett $søknadstypesett */
        $søknadstypesett = $this->app->hentSamling(Type::class);
        foreach ($søknadstypesett as $søknadstype) {
            try {
                $this->sendPåminnelserOmSøknadfornyingForType($søknadstype);
                $this->deaktiverUtløpteSøknader($søknadstype);
            } catch (Exception $e) {
                $this->app->logger->critical('Søknadsprosessor ' . $e->getMessage());
                continue;
            }
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @param Type $søknadstype
     * @return Søknadprosessor
     * @throws Exception
     */
    public function sendPåminnelserOmSøknadfornyingForType(Søknad\Type $søknadstype): Søknadprosessor
    {
        $this->app->before($this, __FUNCTION__, []);
        $gyldighet = $this->hentGyldighet($søknadstype);
        if(empty($gyldighet)) {
            return $this;
        }
        $påminnelsesfrist = new DateInterval('P' . (int)$søknadstype->hentKonfigurering('søknad_fornyingspåminnelse_dager_før_utløp') . 'D');
        $oppdatertFrist = date_create_immutable()->sub($gyldighet)->add($påminnelsesfrist);

        /** @var Søknadsett $søknadSett */
        $søknadSett = $søknadstype->hentAktiveSøknader()
            ->leggTilEavVerdiJoin('utløp_varslet')
            ->leggTilFilter(['`utløp_varslet`.`verdi`' => null])
            ->leggTilFilter(['`' . Søknad::hentTabell() . '`.`oppdatert` <' => $oppdatertFrist->format('Y-m-d')])
        ;

        foreach ($søknadSett as $søknad) {
            try {
                $this->sendPåminnelseOmSøknadfornying($søknad);
            } catch (Exception $e) {
                $this->app->logger->critical('Søknadsprosessor ' . $e->getMessage(), ['søknad' => $søknad->hentId()]);
            }
        }

        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @param Type $søknadstype
     * @return Søknadprosessor
     * @throws Exception
     */
    public function deaktiverUtløpteSøknader(Søknad\Type $søknadstype): Søknadprosessor
    {
        list('søknadstype' => $søknadstype)
            = $this->app->before($this, __FUNCTION__, ['søknadstype' => $søknadstype]);
        $gyldighet = $this->hentGyldighet($søknadstype);
        if(empty($gyldighet)) {
            return $this;
        }
        try {
            $spillerom = new DateInterval($søknadstype->hentKonfigurering('søknad_gyldighet_spillerom'));
        } catch (Exception $e) {
            $spillerom = new DateInterval('P0D');
        }
        $oppdatertFrist = date_create_immutable()->sub($gyldighet)->sub($spillerom);

        /** @var Søknadsett $søknadSett */
        $søknadSett = $søknadstype->hentAktiveSøknader()
            ->leggTilFilter(['`' . Søknad::hentTabell() . '`.`oppdatert` <' => $oppdatertFrist->format('Y-m-d')])
        ;

        foreach ($søknadSett as $søknad) {
            try {
                $søknad->aktiv = false;
            } catch (Exception $e) {
                $this->app->logger->critical('Søknadsprosessor ' . $e->getMessage(), ['søknad' => $søknad->hentId()]);
            }
        }

        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @param Søknad $søknad
     * @return Søknadprosessor
     * @throws Exception
     */
    public function sendPåminnelseOmSøknadfornying(Søknad $søknad): Søknadprosessor
    {
        list('søknad' => $søknad)
            = $this->app->before($this, __FUNCTION__, ['søknad' => $søknad]);
        $adgang = $søknad->hentAdgang();
        $språk = $søknad->hentMetaData('språk') ?? 'no';
        /** @var string $epost */
        $søker = implode(' ', [$søknad->hent(Søknad\Type::FELT_KONTAKT_FORNAVN),$søknad->hent(Søknad\Type::FELT_KONTAKT_ETTERNAVN)]);
        /** @var string $epost */
        $epost = $søknad->hent(Søknad\Type::FELT_KONTAKT_EPOST);
        $avsenderEpost = $søknad->hentType()->hentKonfigurering('avsenderEpost') ?? $this->app->hentValg('epost');
        $søknad->settUtløpVarslet(new DateTimeImmutable());
        if($epost) {
            try {
                if (!$søknad->hentType()->hentKonfigurering('fornyingspåminnelse_mal[' . $språk . ']')) {
                    $språk = 'no';
                    $mal = $søknad->hentType()->hentKonfigurering('fornyingspåminnelse_mal');
                }
                else {
                    $mal = $søknad->hentType()->hentKonfigurering('fornyingspåminnelse_mal[' . $språk . ']');
                }

                switch ($språk) {
                    case 'en':
                        $emne = 'Reminder of an application about to expire';
                        break;
                    default:
                        $emne = 'Påminnelse om søknad i ferd med å utløpe';
                }
                $epostTekst = $this->gjengiVarselmal($mal, $søknad);
                $html = $this->app->hentVisning(Epost::class, [
                    'språk' => $språk,
                    'tittel' => $emne,
                    'innhold' => $this->app->hentVisning(PåminnelseOmUtløp::class, [
                        'søknad' => $søknad,
                        'adgang' => $adgang,
                        'språk' => $språk,
                        'epostTekst' => $epostTekst,
                        'utløpsdato' => $søknad->hentUtløpsdato() ? $søknad->hentUtløpsdato()->format('d.m.Y') : ''
                    ]),
                    'bunntekst' => ''
                ]);
                $this->app->sendMail((object)[
                    'to' => "$søker <$epost>",
                    'reply' => $avsenderEpost,
                    'subject' => $emne,
                    'html' => $html,
                    'priority' => 10,
                    'type' => 'søknadsskjema_fornyingspåminnelse'
                ]);
            } catch (Exception $e) {
                $søknad->settUtløpVarslet(null);
            }
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }
}