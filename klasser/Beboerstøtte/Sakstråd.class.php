<?php


namespace Kyegil\Leiebasen\Beboerstøtte;


/**
 * Class Sakstråd
 *
 * @package Kyegil\Leiebasen\Beboerstøtte
 * @property null|string $id
 * @property null|string $saksref
 * @property null|string $tittel
 * @property null|string $status
 * @property null|string $område
 * @property Innlegg[] $innlegg
 * @property Innlegg[] $saksopplysninger
 * @property \Person[] $abonnenter
 * @property \Person[] $administratorer
 * @property \Person $privatPerson
 * @deprecated
 * @see \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd
 *
 */
class Sakstråd extends \DatabaseObjekt
{
    /**
     * Hvilken tabell i databasen som inneholder primærnøkkelen for dette objektet
     *
     * @var
     */
    protected $tabell = "beboerstøtte";

    /**
     * Hvilket felt i tabellen som lagrer primærnøkkelen for dette objektet
     *
     * @var
     */
    protected $idFelt = "id";

    /**
     * Alle abonnentene som følger denne saken
     *
     * @var null|\Person[]
     */
    protected $abonnenter;

    /**
     * Alle innleggene i dene saken
     *
     * @var null|Innlegg[]
     */
    protected $innlegg;


    /**
     * Alle saksopplysningene i dene saken
     *
     * @var null|Innlegg[]
     */
    protected $saksopplysninger;

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function last($id = 0)
    {
        $tp = $this->mysqli->table_prefix;

        settype($id, 'integer');
        if( !$id ) {
            $id = $this->id;
        }

        $resultat = $this->mysqli->arrayData([
            'returnQuery'	=> true,
            'fields' => [
                'id' => "{$this->tabell}.{$this->idFelt}",
                "{$this->tabell}.*"
            ],
            'source' => 		"{$tp}{$this->tabell} AS {$this->tabell}\n",
            'where'			=>	"{$tp}{$this->tabell}.{$this->idFelt} = '$id'"
        ]);

        if( $resultat->totalRows ) {
            $this->data = $resultat->data[0];
            $this->id = $id;

            $this->data->privat_person
                = $this->data->privat_person_id
                ? $this->leiebase->hent(\Person::class, $this->data->privat_person_id)
                : null;

            return true;
        }
        else {
            $this->id = null;
            $this->data = null;

            return false;
        }

    }

    /**
     * Last alle abonnentene som følger saken
     *
     * @throws \Exception
     */
    protected function lastAbonnenter() {
        $tp = $this->mysqli->table_prefix;
        $this->abonnenter = $this->mysqli->arrayData(array(
            'returnQuery'	=> true,
            'class'     => '\Person',
            'source'	=> "{$tp}beboerstøtte_abonnent AS beboerstøtte_abonnent",
            'fields'	=> [
                'id' => "beboerstøtte_abonnent.bruker_id"
            ],
            'orderfields' => "beboerstøtte_abonnent.id ASC",
            'where'		=> "beboerstøtte_abonnent.sak_id = '{$this->hentId()}'"
        ))->data;
    }

    /**
     * Last alle innleggene i saken
     *
     * @throws \Exception
     */
    protected function lastInnlegg() {
        $tp = $this->mysqli->table_prefix;
        $this->innlegg = $this->mysqli->arrayData(array(
            'returnQuery'	=> true,
            'class'     => '\Kyegil\Leiebasen\Beboerstøtte\Innlegg',
            'source'	=> "{$tp}beboerstøtte_innlegg AS beboerstøtte_innlegg",
            'fields'	=> [
                "beboerstøtte_innlegg.id"
            ],
            'orderfields' => "beboerstøtte_innlegg.tidspunkt ASC",
            'where'		=> "beboerstøtte_innlegg.sak_id = '{$this->hentId()}'"
        ))->data;
    }

    /**
     * @param string $egenskap
     * @param array $param
     * @return mixed|null
     * @throws \Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd::hent()
     */
    public function hent($egenskap, $param = array()) {
        if( !$this->id ) {
            return null;
        }

        switch( $egenskap ) {

            case $this->idFelt:
            case "id":
            case "saksref":
            case "tittel":
            case "skademelding_id":
            case "status":
            case "område":
            case "privat_person":
                {
                    if ( $this->data === null and !$this->last()) {
                        throw new \Exception("Klarte ikke laste beboerstøtte sak ({$this->id})");
                    }
                    return $this->data->$egenskap;
                }

            case "innlegg":
                {
                    if ( $this->innlegg === null || @$param['oppdater'] ) {
                        $this->lastInnlegg();
                    }
                    return $this->innlegg;
                }

            case "abonnenter":
                {
                    if ( $this->abonnenter === null || @$param['oppdater'] ) {
                        $this->lastAbonnenter();
                    }
                    return $this->abonnenter;
                }

            case "saksopplysninger":
                return $this->hentSaksopplysninger();

            case "administratorer":
                return $this->hentAdministratorer();

            default: {
                return null;
            }
        }
    }

    /**
     * Oppretter en ny sak i databasen og tildeler egenskapene til dette objektet
     *
     * @param array|object $egenskaper Alle egenskapene det nye objektet skal initieres med
     * @return $this|false
     * @throws \Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd::opprett()
     */
    public function opprett($egenskaper = array()) {
        $tp = $this->mysqli->table_prefix;
        settype( $egenskaper, 'array');

        if( $this->id ) {
            throw new \Exception('Ny beboerstøtte-sak forsøkt opprettet, men den eksisterer allerede');
        }

        $databasefelter = array();
        $resterendeFelter = array();

        foreach($egenskaper as $egenskap => $verdi) {
            switch( $egenskap ) {

                case "saksref":
                case "tittel":
                case "skademelding_id":
                case "status":
                case "område":
                    $databasefelter[$egenskap] = $verdi;
                    break;

                default:
                    $resterendeFelter[$egenskap] = $verdi;
                    break;
            }
        }

        $this->id = $this->mysqli->saveToDb(array(
            'insert'	=> true,
            'id'		=> $this->id,
            'table'		=> "{$tp}{$this->tabell}",
            'fields'	=> $databasefelter
        ))->id;

        if( !$this->hentId() ) {
            throw new \Exception('Ny beboerstøtte-sak forsøkt opprettet, men den kunne ikke lagres i databasen');
        }

        foreach( $resterendeFelter as $egenskap => $verdi ) {
            $this->sett($egenskap, $verdi);
        }

        return $this;
    }

    /**
     * Sett
     *
     * Sett en verdi
     *
     * @param string $egenskap
     * @param mixed $verdi
     * @return bool|null
     * @throws \Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd::sett()
     */
    public function sett($egenskap, $verdi = null) {
        $tp = $this->mysqli->table_prefix;

        if( !$this->id ) {
            return null;
        }

        if($egenskap == 'privat_person' && $verdi instanceof \Person) {
            $egenskap = 'privat_person_id';
            $verdi = $verdi->hentId();
        }

        switch( $egenskap ) {
            case "saksref":
            case "tittel":
            case "skademelding_id":
            case "status":
            case "område":
            case "privat_person_id":
                return $this->mysqli->saveToDb(array(
                    'update'	=> true,
                    'id'        => $this->id,
                    'table'		=> "{$tp}{$this->tabell} as {$this->tabell}",
                    'where'		=> "{$this->tabell}.{$this->idFelt} = '{$this->id}'",
                    'fields'	=> array(
                        "{$this->tabell}.{$egenskap}"	=> $verdi
                    )
                ))->success;

            default:
                return false;

        }
    }

    /**
     * Slett
     *
     * @return bool|\mysqli_result
     * @throws \Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd::slett()
     */
    public function slett() {
        $tp = $this->mysqli->table_prefix;

        foreach($this->innlegg as $innlegg) {
            $innlegg->slett();
        }

        $sql = "DELETE FROM
            {$tp}{$this->tabell}
            WHERE {$tp}{$this->tabell}.{$this->idFelt} = '" . (int)$this->id . "'";

        $resultat = $this->mysqli->query( $sql );

        if($resultat) {
            $sql = "DELETE FROM
            {$tp}beboerstøtte_abonnent
            WHERE {$tp}sak_id = '" . (int)$this->id . "'";

            $resultat = $this->mysqli->query( $sql );
        }

        // Tøm alle mellomlagrede data
        if($resultat) {
            $this->id = null;
            $this->data = null;
            $this->innlegg = null;
            $this->abonnenter = null;
        }

        return $resultat;
    }

    /**
     * @param \stdClass $param
     * @return Innlegg
     * @throws \Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd::leggTilInnlegg()
     */
    public function leggTilInnlegg(\stdClass $param)
    {
        if(!$this->hentId()) {
            throw new \Exception('Kan ikke legge til innlegg til ikke-eksisterende tråd');
        }
        $param->sak = $this;
        /** @var \Kyegil\Leiebasen\Beboerstøtte\Innlegg $innlegg */
        $innlegg = $this->leiebase->opprett(\Kyegil\Leiebasen\Beboerstøtte\Innlegg::class, $param);

        $this->innlegg = null;
        return $innlegg;
    }

    /**
     * @param \Person|integer $personid
     * @return $this
     * @throws \Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd::abonner()
     */
    public function abonner($personid)
    {
        $sakId = $this->hentId();
        $personid = intval(strval($personid));
        foreach($this->hent('abonnenter') as $abonnent) {
            if($personid == $abonnent->hentId()) {
                return $this;
            }
        }
        $tp = $this->mysqli->table_prefix;
        $this->mysqli->saveToDb([
            'table' => "{$tp}beboerstøtte_abonnent",
            'insert' => true,
            'fields' => [
                'sak_id' => $sakId,
                'bruker_id' => $personid
            ]
        ]);
        $this->abonnenter = null;
        return $this;
    }

    /**
     * @return Innlegg[]
     * @throws \Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd::hentSaksopplysninger()
     */
    public function hentSaksopplysninger()
    {
        if(!isset($this->saksopplysninger)) {
            $this->saksopplysninger = [];
            /** @var Innlegg $innlegg */
            foreach($this->hent('innlegg') as $innlegg) {
                if($innlegg->saksopplysning) {
                    $this->saksopplysninger[] = $innlegg;
                }
            }
        }
        return $this->saksopplysninger;
    }

    /**
     * @return array|false|string[]
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd::hentAdministratorer()
     */
    public function hentAdministratorer()
    {
        $administratorer = $this->leiebase->hentValg('beboerstøtte_skademeldinger_admin') ? explode(',', $this->leiebase->hentValg('beboerstøtte_skademeldinger_admin')) : [];
        foreach($administratorer as &$administrator) {
            /** @var \Person|null $administrator */
            $administrator = $this->leiebase->hent(\Person::class, $administrator);
        }
        return $administratorer;
    }
}