<?php


namespace Kyegil\Leiebasen\Beboerstøtte;


/**
 * Class Innlegg
 *
 * @property null|string $id
 * @property null|Sakstråd $sak
 * @property null|\Person $avsender
 * @property null|string $innhold
 * @property null|\DateTime $tidspunkt
 * @property null|string $vedlegg
 * @property bool $saksopplysning
 * @property bool $privat
 * @deprecated
 * @see \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd\Innlegg
 */
class Innlegg extends \DatabaseObjekt
{
    /**
     * Hvilken tabell i databasen som inneholder primærnøkkelen for dette objektet
     *
     * @var
     */
    protected $tabell = "beboerstøtte_innlegg";

    /**
     * Hvilket felt i tabellen som lagrer primærnøkkelen for dette objektet
     *
     * @var
     */
    protected $idFelt = "id";

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     * @deprecated
     */
    public function last($id = 0)
    {
        $tp = $this->mysqli->table_prefix;

        settype($id, 'integer');
        if( !$id ) {
            $id = $this->id;
        }

        $resultat = $this->mysqli->arrayData([
            'returnQuery'	=> true,
            'fields' => [
                'id' => "{$this->tabell}.{$this->idFelt}",
                "{$this->tabell}.*"
            ],
            'source' => 		"{$tp}{$this->tabell} AS {$this->tabell}\n",
            'where'			=>	"{$tp}{$this->tabell}.{$this->idFelt} = '$id'"
        ]);

        if( $resultat->totalRows ) {
            $this->data = $resultat->data[0];
            $this->id = $id;

            $this->data->sak = $this->leiebase->hent(\Kyegil\Leiebasen\Beboerstøtte\Sakstråd::class, $this->data->sak_id);
            $this->data->avsender = $this->leiebase->hent(\Person::class, $this->data->avsender_id);
            $this->data->tidspunkt = new \DateTime($this->data->tidspunkt);

            settype($this->data->saksopplysning, 'boolean');
            settype($this->data->privat, 'boolean');

            return true;
        }
        else {
            $this->id = null;
            $this->data = null;

            return false;
        }

    }

    /**
     * @param string $egenskap
     * @param array $param
     * @return mixed|null
     * @throws \Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd\Innlegg::hent()
     */
    public function hent($egenskap, $param = array()) {
        if( !$this->id ) {
            return null;
        }

        switch( $egenskap ) {

            case $this->idFelt:
            case "id":
            case "sak":
            case "avsender":
            case "innhold":
            case "tidspunkt":
            case "vedlegg":
            case "saksopplysning":
            case "privat":
                {
                    if ( $this->data === null and !$this->last()) {
                        throw new \Exception("Klarte ikke laste beboerstøtteinnlegg ({$this->id})");
                    }
                    return $this->data->$egenskap;
                }

            default: {
                return null;
            }
        }
    }

    /**
     * Oppretter et nytt innlegg i databasen og tildeler egenskapene til dette objektet
     *
     * @param array|object $egenskaper Alle egenskapene det nye objektet skal initieres med
     * @return $this|false
     * @throws \Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd\Innlegg::opprett()
     */
    public function opprett($egenskaper = array()) {
        $tp = $this->mysqli->table_prefix;
        settype( $egenskaper, 'array');

        if( $this->id ) {
            throw new \Exception('Nytt beboerstøtte-innlegg forsøkt opprettet, men det eksisterer allerede');
        }

        $databasefelter = array();
        $resterendeFelter = array();

        foreach($egenskaper as $egenskap => $verdi) {
            switch( $egenskap ) {

                case "sak":
                case "avsender":
                    $databasefelter[$egenskap . '_id'] = (string)$verdi;
                    break;

                case "sak_id":
                case "avsender_id":
                case "innhold":
                case "tidspunkt":
                case "vedlegg":
                case "saksopplysning":
                case "privat":
                    $databasefelter[$egenskap] = $verdi;
                    break;

                default:
                    $resterendeFelter[$egenskap] = $verdi;
                    break;
            }
        }

        $this->id = $this->mysqli->saveToDb(array(
            'insert'	=> true,
            'id'		=> $this->id,
            'table'		=> "{$tp}{$this->tabell}",
            'fields'	=> $databasefelter
        ))->id;

        if( !$this->hentId() ) {
            throw new \Exception('Nytt beboerstøtte-innlegg forsøkt opprettet, men det kunne ikke lagres i databasen');
        }

        foreach( $resterendeFelter as $egenskap => $verdi ) {
            $this->sett($egenskap, $verdi);
        }

        return $this;
    }

    /**
     * Sett
     *
     * Sett en verdi
     *
     * @param string $egenskap
     * @param mixed $verdi
     * @return bool|null
     * @throws \Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd\Innlegg::sett()
     */
    public function sett($egenskap, $verdi = null) {
        $tp = $this->mysqli->table_prefix;

        if( !$this->id ) {
            return null;
        }

        switch( $egenskap ) {
            case "sak":
            case "avsender":
            case "innhold":
            case "tidspunkt":
            case "vedlegg":
            case "saksopplysning":
            case "privat":
                return $this->mysqli->saveToDb(array(
                    'update'	=> true,
                    'id'        => $this->id,
                    'table'		=> "{$tp}{$this->tabell} as {$this->tabell}",
                    'where'		=> "{$this->tabell}.{$this->idFelt} = '{$this->id}'",
                    'fields'	=> array(
                        "{$this->tabell}.{$egenskap}"	=> $verdi
                    )
                ))->success;

            default:
                return false;
        }
    }

    /**
     * Slett
     *
     * @return bool|\mysqli_result
     * @throws \Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd\Innlegg::slett()
     */
    public function slett() {
        $tp = $this->mysqli->table_prefix;

        $sql = "DELETE FROM
            {$tp}{$this->tabell}
            WHERE {$tp}{$this->tabell}.{$this->idFelt} = '" . (int)$this->id . "'";

        $resultat = $this->mysqli->query( $sql );

        // Tøm alle mellomlagrede data
        if($resultat) {
            $this->id = null;
            $this->data = null;
        }

        return $resultat;
    }

    /**
     * @throws \Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd\Innlegg::sendEpost()
     */
    public function sendEpost()
    {
        if(!$this->hentId()) {
            throw new \Exception('Innlegget er ikke lagret');
        }

        $saksabonnenter = $this->sak->abonnenter;
        if($this->privat) {
            $saksabonnenter = $this->sak->administratorer;
        }
        if($this->sak->privatPerson) {
            $saksabonnenter = array_merge($saksabonnenter, [$this->sak->privatPerson]);
        }
        $emnefelt = "[{$this->sak->saksref}] {$this->sak->tittel}";
        $innhold = $this->gjengi('epost_beboerstøtte_innlegg', [
            'saksref' => $this->sak->saksref,
            'innhold' => $this->innhold
        ]);
        foreach($saksabonnenter as $abonnent) {
            $this->leiebase->sendMail((object)[
                'to' => $abonnent,
                'from' => $this->avsender,
                'reply' => "{$this->leiebase->hentValg('utleier')} <{$this->leiebase->hentValg('beboerstøtte_epost')}>",
                'subject' => $emnefelt,
                'html' => $innhold,
                'priority' => 100,
                'type' => 'beboerstøtte_innlegg'
            ]);
        }
    }

    /**
     * @return bool
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd\Innlegg::erAvsenderAdmin()
     */
    public function erAvsenderAdmin()
    {
        $avsender = $this->avsender;
        $område = $this->sak->område;
        return $avsender->harAdgangTil($område);
    }
}