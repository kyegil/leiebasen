<?php

namespace Kyegil\Leiebasen;

use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Exception;
use Fpdf\Fpdf;
use Kyegil\CoreModel\CoreModel;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Delkravtypesett;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Modell\Leieforhold\Fbo;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Purring;
use Kyegil\Leiebasen\Modell\Leieforhold\Purringsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning\Efaktura;
use Kyegil\Leiebasen\Modell\Leieforhold\Regningsett;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\EfakturaIdentifier;
use Kyegil\Leiebasen\Nets\NetsException;
use Kyegil\MysqliConnection\MysqliConnection;
use Psr\Log\LoggerInterface;
use stdClass;
use Throwable;

/**
 *
 * @property array $fasteKravtyper
 */
class Utskriftsprosessor
{
    /** @var DateTimeImmutable */
    private static DateTimeImmutable $nesteManuelleUtskriftsdato;
    /**
     * @var Leiebase
     */
    private Leiebase $app;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * @var MysqliConnection
     */
    private MysqliConnection $mysqli;

    /** @var int */
    public int $automatiskUtskriftBuntbegrensning = 100;

    /**
     * @var UtskriftStatus
     */
    private UtskriftStatus $status;

    /**
     * @var array
     */
    private array $selvBekreftendeUtskriftsformater = [
        Regning::FORMAT_EPOST,
        Regning::FORMAT_EFAKTURA,
    ];

    /**
     * @var array
     */
    private array $manueltBekreftedeUtskriftsformater = [
        Regning::FORMAT_PAPIR
    ];

    /**
     * @param Leiebase $app
     * @param LoggerInterface $logger
     * @param MysqliConnection $mysqliConnection
     */
    public function __construct(
        Leiebase         $app,
        LoggerInterface  $logger,
        MysqliConnection $mysqliConnection
    )
    {
        $this->app = $app;
        $this->logger = $logger;
        $this->mysqli = $mysqliConnection;
        $this->status = new UtskriftStatus();
        $this->automatiskUtskriftBuntbegrensning
            = intval(Leiebase::$config['leiebasen']['config']['automatisk_utskrift']['buntbegrensning']
            ?? $this->automatiskUtskriftBuntbegrensning);
    }

    /**
     * Funksjon som bunter kravene sammen til regninger for utskrift og påfører forfallsdato.
     * Funksjonen returnerer de nye regningene
     *
     * @param int[] $kravIder Krav-id'er
     * @return Regningsett de resulterende regningene
     * @throws Throwable
     */
    public function buntRegninger(array $kravIder = []): Regningsett
    {
        list(
            'kravIder' => $kravIder,
            )
            = $this->app->pre($this, __FUNCTION__, [
            'kravIder' => $kravIder,
        ]);
        $this->logger->info('GIROUTSKRIFT – Bunter krav til giroer.', ['krav' => $kravIder]);
        $tp = $this->mysqli->table_prefix;
        /** @var Regningsett $resultat */
        $resultat = $this->app->hentSamling(Regning::class);
        /** @var int[] $leieforholdIder */
        $leieforholdIder = [];
        /** @var int[] $regningsIder */
        $regningsIder = [];
        /** @var Regning[][] $regningerPerForfallsdato */
        $regningerPerForfallsdato = [];
        /** @var string[][] $forfallsdatoerPerLeieforhold */
        $forfallsdatoerPerLeieforhold = [];

        if ($kravIder) {

            /**
             * Kravene grupperes etter leieforhold og forfallsdato
             * Kreditt som allerede er avstemt kommer på separate kreditnotaer
             *
             * @var stdClass[] $bunter
             * * int leieforhold
             * * string forfall
             * * bool avstemt_kreditt
             */
            $bunter = $this->mysqli->select([
                'fields' => [
                    '`krav`.`leieforhold`',
                    'forfall' => '`krav`.`forfall`',
                    'avstemt_kreditt' => 'IF((`krav`.`beløp` < 0 AND `innbetalinger`.`krav` IS NOT NULL), 1, 0)',
                ],

                'source' => '`' . $tp . 'krav` AS `krav` LEFT JOIN `' . $tp . 'innbetalinger` AS `innbetalinger` ON `krav`.`id` = `innbetalinger`.`krav`',
                'where' => [
                    '`krav`.`gironr`' => null,
                    '`krav`.`id` IN' => $kravIder
                ],
                'groupfields' => [
                    '`krav`.`leieforhold`',
                    '`krav`.`forfall`',
                    'avstemt_kreditt'
                ],
                'sortfields' => ['`krav`.`forfall`', '`krav`.`leieforhold`']
            ])->data;

            /*
             * Fastsett forfallsdatoer per leieforhold,
             * sånn at krav uten bestemt forfallsdato kan legges til et av disse
             */
            foreach ($bunter as $kravbunt) {
                $leieforholdIder[] = $kravbunt->leieforhold;
                settype($forfallsdatoerPerLeieforhold[$kravbunt->leieforhold], 'array');
                if($kravbunt->forfall) {
                    $forfallsdatoerPerLeieforhold[$kravbunt->leieforhold][] = $kravbunt->forfall;
                }
            }
            /** @var Leieforholdsett $leieforholdSett */
            $leieforholdSett = $this->app->hentSamling(Leieforhold::class)
                ->filtrerEtterIdNumre(array_unique($leieforholdIder))->låsFiltre();
            $leieforholdSett
                ->leggTilEavFelt('forfall_fast_dag_i_måneden')
            ;

            foreach ($leieforholdSett as $leieforhold) {
                $forfallsdatoerPerLeieforhold[$leieforhold->hentId()][] = $leieforhold->nyForfallsdato()->format('Y-m-d');
            }

            /**
             * For hver gruppe opprettes en giro
             * Kravene knyttes til giroen og kravet får påført forfallsdato
             * En kravbunt er liste over forfallsdatoer per leieforhold
             *
             * var stdClass $kravbunt
             */
            foreach ($bunter as $kravbunt) {
                //	Lag en ny giro.
                // Dersom dette mislykkes, hoppes det over denne giroen.
                try {
                    $regningsForfall = min($forfallsdatoerPerLeieforhold[$kravbunt->leieforhold]);
                    $regning = $this->app->nyModell(Regning::class, (object)['leieforhold' => $kravbunt->leieforhold]);
                    $this->logger->info('GIROUTSKRIFT – Opprettet giro', ['giro' => $regning->hentId(), 'memory_usage' => memory_get_usage()]);
                    $regningerPerForfallsdato[$kravbunt->forfall ?: $regningsForfall][] = $regning->hentId();
                    $regningsIder[] = $regning->hentId();
                } catch (Throwable $e) {
                    $this->logger->warning('GIROUTSKRIFT – Kunne ikke opprette giro', ['leieforhold' => $kravbunt->leieforhold, 'exception' => $e->getMessage()]);
                    continue;
                }
            }

            /**
             * @var string $forfallsdato
             * @var int[] $regningsIder
             */
            foreach ($regningerPerForfallsdato as $forfallsdato => $regningsnumre) {
                $sql = 'UPDATE `' . Krav::hentTabell() . '`' . ' INNER JOIN `' . Regning::hentTabell() . '`'
                    . ' ON `' . Krav::hentTabell() . '`.`leieforhold` = `' . Regning::hentTabell() . '`.`leieforhold`'
                    . ' AND IFNULL(`' . Krav::hentTabell() . '`.`forfall`, \'' . $forfallsdato . '\') = \'' . $forfallsdato . '\''
                    . ' AND `' . Regning::hentTabell() . '`.`' . Regning::hentPrimærnøkkelfelt() . '` IN(' . implode(',', $regningsnumre) . ')'
                    . ' SET `' . Krav::hentTabell() . '`.`gironr` = `' . Regning::hentTabell() . '`.`' . Regning::hentPrimærnøkkelfelt() . '`,'
                    . ' `' . Krav::hentTabell() . '`.`forfall` = \'' . $forfallsdato . '\''
                    . ' WHERE `' . Regning::hentTabell() . '`.`' . Regning::hentPrimærnøkkelfelt() . '` IN(' . implode(',', $regningsnumre) . ')'
                    . ' AND `' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '` IN(' . implode(',', $kravIder) . ')'
                ;
                $this->mysqli->query($sql);
            }
            $this->oppdaterReellKravIder();
        }
        $resultat->filtrerEtterIdNumre($regningsIder);
        $resultat->låsFiltre();
        return $this->app->post($this, __FUNCTION__, $resultat);
    }

    /**
     * @param Kravsett $kravsett
     * @return Utskriftsprosessor
     * @throws Throwable
     */
    public function utsettTilMinimumForfallsfrist(Kravsett $kravsett): Utskriftsprosessor
    {
        list(
            'kravsett' => $kravsett,
            )
            = $this->app->pre($this, __FUNCTION__, [
            'kravsett' => $kravsett,
        ]);
        $kravIder = $kravsett->hentIdNumre();
        if ($kravIder) {

            /**
             * Forfall utsettes for alle ikke-utskrevne regninger med for kort forfallsfrist
             */
            $førsteMuligeForfall = $this->app->nyForfallsdato();
            $this->logger->info('GIROUTSKRIFT – Ikke utskrevne men forfalte regninger får forfall utsatt til ' . $førsteMuligeForfall->format('Y-m-d'));

            $kravFilter = ['or' => [
                'and' => [
                    '`' . Regning::hentTabell() . '`.`utskriftsdato`' => null,
                    '`' . Krav::hentTabell() . '`.`forfall` <' => $førsteMuligeForfall->format('Y-m-d'),
                    '`' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '`' => $kravIder,
                ]
            ]];

            /** @var Regningsett $regningerMedForTidligForfall */
            $regningerMedForTidligForfall = $this->app->hentSamling(Regning::class)
                ->leggTilLeftJoin(Krav::hentTabell(), '`' . Krav::hentTabell() . '`.`gironr` = `' . Regning::hentTabell() . '`.`' . Regning::hentPrimærnøkkelfelt() . '`')
                ->leggTilFilter(['`' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '`' => $kravIder])
                ->leggTilFilter(['`' . Krav::hentTabell() . '`.`forfall` <' => $førsteMuligeForfall->format('Y-m-d')]);

            if ($regningerMedForTidligForfall->hentAntall()) {
                $kravFilter['or'][] = ['`' . Krav::hentTabell() . '`.`gironr`' => $regningerMedForTidligForfall->hentIdNumre()];
            }
            /** @var Kravsett $kravsett */
            $kravsett = $this->app->hentSamling(Krav::class);
            $kravsett->leggTilLeieforholdModell();
            $kravsett->leggTilRegningModell()
                ->leggTilFilter($kravFilter)->låsFiltre();
            foreach ($kravsett as $krav) {
                $krav->settForfall($krav->leieforhold->nyForfallsdato());
                $this->logger->info('GIROUTSKRIFT – Flytter forfall for krav ' . $krav->hentId() . ' til ' . $krav->leieforhold->nyForfallsdato()->format('Y-m-d'));
            }
            $this->logger->info('GIROUTSKRIFT – Forfall oppdatert på ' . $kravsett->hentAntall() . ' ikke-utskrevne krav');
        }
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * Filter ut betalte papirgiroer fra utskriftsbunken
     *
     * @param int[] $regningsnumre
     * @return int[]
     * @throws Throwable
     */
    public function filtrerUtBetalteRegninger(array $regningsnumre): array
    {
        list(
            'regningsnumre' => $regningsnumre,
            )
            = $this->app->pre($this, __FUNCTION__, [
            'regningsnumre' => $regningsnumre,
        ]);
        if ($regningsnumre) {
            try {
                /** @var Kravsett $kravsett */
                $kravsett = $this->app->hentSamling(Krav::class);
                $kravsett->leggTilLeftJoinForRegning();
                $kravsett->leggTilFilter(['`' . Krav::hentTabell() . '`.`beløp` >' => 0])
                    ->leggTilFilter(['utestående' => 0])
                    ->leggTilFilter(['`' . Regning::hentTabell() . '`.`' . Regning::hentPrimærnøkkelfelt() . '`' => $regningsnumre])
                    ->låsFiltre();
                foreach ($kravsett as $krav) {
                    try {
                        if ($krav->regning->hentBeløp() > 0 && $krav->regning->hentUtestående() == 0) {
                            if (($key = array_search($krav->regning->hentId(), $regningsnumre)) !== false) {
                                $this->logger->info('GIROUTSKRIFT – Hopper over betalt papirgiro', ['giro' => $krav->regning->hentId()]);
                                unset($regningsnumre[$key]);
                            }
                        }
                    } catch (Throwable $e) {
                        continue;
                    }
                }
            } catch (Throwable $e) {
                $this->logger->warning($e);
            }
        }
        return $this->app->post($this, __FUNCTION__, $regningsnumre);
    }

    /**
     * Lag utskriftsfil
     *
     * Denne funksjonen samler giro-, purre- og andre objekter i et array,
     * og oppretter en pdf som lagres for utskrift
     *
     * @param Utskriftsforsøk $utskriftskonfigurasjon kontroller
     *      ->giroer        array Giro-objekter som skal skrives ut
     *      -> purringer    array Purre-objekter som skal skrives ut
     *      ->gebyrpurringer    array id'ene for purringene som kan tillegges purregebyr
     *      ->statusoversikter  array Leieforhold-objekter for statusoversikter
     *      ->bruker    string Navn på innlogget bruker
     *      ->tidspunkt DateTime Tidspunkt
     * @return Utskriftsprosessor
     * @throws Throwable
     */
    public function lagUtskriftsfil(Utskriftsforsøk $utskriftskonfigurasjon): Utskriftsprosessor
    {
        list(
            'utskriftskonfigurasjon' => $utskriftskonfigurasjon,
            )
            = $this->app->pre($this, __FUNCTION__, [
            'utskriftskonfigurasjon' => $utskriftskonfigurasjon,
        ]);
        /** @var Regning[]|Purring[]|Leieforhold[] $utskrift */
        $utskrift = [];

        $this->logger->info('GIROUTSKRIFT – Lager utskriftsfil');
        $fil = "{$this->app->hentFilarkivBane()}/giroer/_utskriftsbunke.pdf";
        if (file_exists($fil)) {
            unlink($fil);
        }

        if ($utskriftskonfigurasjon->giroer) {
            /** @var Regningsett $regningssett */
            $regningssett = $this->app->hentSamling(Regning::class);
            $regningssett
                ->leggTilLeftJoinForLeieforholdUtdelingsplassering()
                ->leggTilLeieforholdUtdelingsplasseringFelt();

            $regningssett->leggTilLeftJoin(['leieobjekt' => Leieobjekt::hentTabell()], '`' . Leieforhold::hentTabell() . '`.`leieobjekt` = `leieobjekt`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '`')
                ->leggTilModell(Leieobjekt::class, 'leieforhold.leieobjekt', 'leieobjekt');

            $regningssett->leggTilLeftJoin(['regningsobjekt' => Leieobjekt::hentTabell()], '`' . Leieforhold::hentTabell() . '`.`regningsobjekt` = `regningsobjekt`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '`')
                ->leggTilModell(Leieobjekt::class, 'leieforhold.regningsobjekt', 'regningsobjekt');

            $regningssett->leggTilBeløpFelt()
                ->leggTilUteståendeFelt();

            $regningssett->filtrerEtterIdNumre($utskriftskonfigurasjon->giroer);
            /** @var Regning $regning */
            foreach ($regningssett as $regning) {
                /** @var Leieforhold $leieforhold */
                $leieforhold = $regning->leieforhold;

                // Se først etter alternative fakturaformater,
                // som eFaktura eller epostgiro

                // Giroer som skal sendes med efaktura eller epost utelates
                if(in_array($leieforhold->giroformat, $this->hentManueltBekreftedeUtskriftsformater())
                    || ($leieforhold->giroformat == Regning::FORMAT_INGEN_UTSKRIFT)
                ) {
                    $utskrift[] = $regning;
                }
            }
        }

        if ($utskriftskonfigurasjon->purringer) {
            /** @var Purringsett $purringsett */
            $purringsett = $this->app->hentSamling(Purring::class);
            $purringsett
                ->leggTilLeftJoinForLeieforhold()
                ->leggTilModell(Leieforhold::class, 'leieforhold', 'leieforhold')

                ->leggTilLeftJoinForLeieforholdLeieobjekt()
                ->leggTilModell(Leieobjekt::class, 'leieforhold.leieobjekt', 'leieobjekt')

                ->leggTilLeftJoinForLeieforholdRegningsobjekt()
                ->leggTilModell(Leieobjekt::class, 'leieforhold.regningsobjekt', 'regningsobjekt')

                ->leggTilLeftJoinForLeieforholdUtdelingsplassering()
                ->leggTilLeieforholdUtdelingsplasseringFelt()
            ;
            $purringsett->filtrerEtterIdNumre($utskriftskonfigurasjon->purringer)->låsFiltre();
            /** @var Purring\Kravlinksett $kravlinksett */
            $kravlinksett = $purringsett->inkluderKravlinker();
            $kravlinksett->leggTilKravModell();

            /** @var Purring $purring */
            foreach ($purringsett as $purring) {
                if ($purring->purremåte == 'giro') {
                    $utskrift[] = $purring;
                }
            }
        }

        foreach ($utskriftskonfigurasjon->statusoversikter as $leieforhold) {
            $utskrift[] = $this->app->hentModell(Leieforhold::class, $leieforhold);
        }

        usort($utskrift, [Visning\felles\fpdf\Pdf::class, 'sorter']);

        /** @var Fpdf $pdf */
        $pdf = new Fpdf();
        /** @var DateTime $dato */
        $pdf->SetAutoPageBreak(false);
        foreach ($utskrift as $index => $side) {
            if ($side instanceof Regning) {
                if ($side->hentBeløp() < 0) {
                    $this->app->vis(Visning\felles\fpdf\Kreditnota::class, [
                        'pdf' => $pdf,
                        'regning' => $side
                    ])->render();
                } else {
                    $this->app->vis(Visning\felles\fpdf\Regning::class, [
                        'pdf' => $pdf,
                        'regning' => $side
                    ])->render();
                }
            } elseif ($side instanceof Purring) {
                $purregebyr = in_array($side->hentId(), $utskriftskonfigurasjon->gebyrpurringer)
                    ? floatval($this->app->hentValg('purregebyr') ?? 0) : 0;
                $this->app->vis(Visning\felles\fpdf\Purring::class, [
                    'pdf' => $pdf,
                    'purring' => $side,
                    'purregebyr' => $purregebyr
                ])->render();
            } elseif ($side instanceof Leieforhold) {
                $this->app->vis(Visning\felles\fpdf\StatusOversikt::class, [
                    'pdf' => $pdf,
                    'leieforhold' => $side,
                    'dato' => new DateTime()
                ])->render();
            }
            $this->status->oppdaterStatus(UtskriftStatus::STATUS_PÅGÅR, 0.3 + 0.7 * ($index + 1) / count($utskrift), 'Side ' . ($index + 1) . ' av ' . count($utskrift) . ' prosessert');
        }

        $pdf->Output($fil, 'F');
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * Forbered utskrift
     *
     * Denne funksjonen laster eller oppretter en utskriftskonfigurasjon
     * som inneholder oversikt over hvilke regninger, purringer og statusoversikter som skrives ut.
     *
     * Resultatet kan deretter sendes til lagUtskriftsfil.
     * Resultatet kan lagres serialisert i databasen for senere bekreftelse
     *
     * @param Kravsett|null $kravsett Kravene det skal sendes regninger på
     * @param bool $adskilt Det sendes separat regning for hver kravtype
     * @param Regningsett|null $regningsettForPurringer Regningene som skal purres
     * @param bool $kombipurringer Det sendes én felles purring for regninger i et leieforhold
     * @param array $purreformater Purreformatene per leieforhold-Id
     * @param Leieforholdsett|null $gebyrleieforhold Leieforholdene som kan gebyrlegges
     * @param Leieforholdsett|null $statusoversikter Leieforholdene som det skal sendes statusoversikt til
     * @return Utskriftsforsøk
     * @throws Throwable
     */
    public function forberedUtskrift(
        ?Kravsett       $kravsett = null,
        bool            $adskilt = false,
        ?Regningsett    $regningsettForPurringer = null,
        bool            $kombipurringer = false,
        array           $purreformater = [],
        ?Leieforholdsett $gebyrleieforhold = null,
        ?Leieforholdsett $statusoversikter = null
    ): Utskriftsforsøk
    {
        list(
            'kravsett' => $kravsett,
            'adskilt' => $adskilt,
            'regningsettForPurringer' => $regningsettForPurringer,
            'kombipurringer' => $kombipurringer,
            'purreformater' => $purreformater,
            'gebyrleieforhold' => $gebyrleieforhold,
            'statusoversikter' => $statusoversikter,
            )
            = $this->app->pre($this, __FUNCTION__, [
            'kravsett' => $kravsett,
            'adskilt' => $adskilt,
            'regningsettForPurringer' => $regningsettForPurringer,
            'kombipurringer' => $kombipurringer,
            'purreformater' => $purreformater,
            'gebyrleieforhold' => $gebyrleieforhold,
            'statusoversikter' => $statusoversikter,
        ]);
        $this->logger->info('GIROUTSKRIFT – Utskriften forberedes');
        $this->status->oppdaterStatus(UtskriftStatus::STATUS_PÅGÅR, 0, 'Utskriften forberedes');

        $resultat = new Utskriftsforsøk();
        $resultat->bruker = $this->app->bruker['navn'];
        $resultat->tidspunkt = new DateTime();

        $this->utsettTilMinimumForfallsfrist($kravsett);

        if ($kravsett && $kravsett->hentAntall()) {
            $this->logger->info('GIROUTSKRIFT – ' . $kravsett->hentAntall() . ' krav skal skrives ut', ['adskilt' => $adskilt]);
            $this->logger->debug('GIROUTSKRIFT – Krav for utskrift: ',
                ['kravIder' => $kravsett->hentIdNumre()]
            );
            // kravene buntes sammen og påføres forfallsdato
            $resultat->giroer = $this->hentRegninger($adskilt, $kravsett)->hentIdNumre();
        }
        $this->status->settStatus(0.1);


        /**
         * Så opprettes purringer for giroene som skal purres
         */
        if ($regningsettForPurringer && $regningsettForPurringer->hentAntall()) {
            $this->logger->info('GIROUTSKRIFT – ' . $regningsettForPurringer->hentAntall() . ' regninger skal purres',
                ['kombipurringer' => $kombipurringer, 'purreformater' => $purreformater]
            );
            $this->logger->debug('GIROUTSKRIFT – Regninger for purring: ', ['regningsIder' => $regningsettForPurringer->hentIdNumre()]);

            $purreomgang = time();
            foreach ($regningsettForPurringer as $regning) {
                $leieforhold = $regning->leieforhold;

                /* Det må ikke være mulig å velge Ingen Utskrift for leieforhold med utestående */
                if($leieforhold->hentGiroformat() == Regning::FORMAT_INGEN_UTSKRIFT) {
                    $leieforhold->settGiroformat(Regning::FORMAT_PAPIR);
                }

                // Lag purreblankettreferanse
                /** @var string $purreblankett */
                $purreblankett =
                    $purreomgang
                    . "-{$leieforhold}"
                    . ($kombipurringer ? '' : "-{$regning->hentId()}");

                $purremåte = Purring::PURREMÅTE_GIRO;
                if (
                    isset($purreformater[$leieforhold->hentId()])
                    && $purreformater[$leieforhold->hentId()] == Purring::PURREMÅTE_EPOST
                ) {
                    $purremåte = Purring::PURREMÅTE_EPOST;
                }

                // Her sjekkes om det kan legges på purregebyr
                if (
                    $this->app->hentValg('purregebyr') != 0
                    && ($this->app->hentValg('epost_purregebyr_aktivert') || $purremåte == Purring::PURREMÅTE_GIRO)
                ) {
                    $sisteForfall = $regning->hentSisteForfall();
                    if (
                        $regning->hentUtskriftsdato() != null
                        && in_array($regning->hentLeieforhold()->hentId(), $gebyrleieforhold->hentIdNumre())
                        && $regning->hentAntallPurregebyr() < 2
                        && $sisteForfall->add(new DateInterval($this->app->hentValg('purreintervall'))) < new DateTime
                    ) {
                        $resultat->gebyrpurringer[$purreblankett] = $purreblankett;
                    }
                }

                if ($regning->purr($purremåte, null, $kombipurringer, null, $purreblankett)) {
                    $resultat->purringer[$purreblankett] = $purreblankett;
                }
            }
            $resultat->gebyrpurringer = array_values($resultat->gebyrpurringer);
            $resultat->purringer = array_values($resultat->purringer);
        }
        $this->status->settStatus(0.2);

        // Så opprettes statusoversikter
        $resultat->statusoversikter = $statusoversikter ? $statusoversikter->hentIdNumre() : [];

        $this->status->oppdaterStatus(UtskriftStatus::STATUS_PÅGÅR, 0.3, 'Forbereding komplett');
        return $this->app->post($this, __FUNCTION__, $resultat);
    }

    /**
     * @param bool $adskilt
     * @param Kravsett $kravsett
     * @return Regningsett
     * @throws Throwable
     */
    public function hentRegninger(bool $adskilt, Kravsett $kravsett): Regningsett
    {
        list(
            'adskilt' => $adskilt,
            'kravsett' => $kravsett,
            )
            = $this->app->pre($this, __FUNCTION__, [
            'adskilt' => $adskilt,
            'kravsett' => $kravsett,
        ]);
        if ($adskilt) {
            /** @var int[] $husleie */
            $husleie = [];
            /** @var int[] $fellesstrøm */
            $fellesstrøm = [];
            /** @var int[] $annet */
            $annet = [];
            foreach ($kravsett as $krav) {
                if ($krav->hentType() == Krav::TYPE_HUSLEIE) {
                    $husleie[] = $krav->hentId();
                } else if ($krav->hentType() == Krav::TYPE_STRØM) {
                    $fellesstrøm[] = $krav->hentId();
                } else {
                    $annet[] = $krav->hentId();
                }
            }
            $this->buntRegninger($husleie);
            $this->buntRegninger($fellesstrøm);
            $this->buntRegninger($annet);
        }
        else {
            $this->buntRegninger($kravsett->hentIdNumre());
        }

        /** @var Regningsett $regninger */
        $regninger = $this->app->hentSamling(Regning::class)
            ->leggTilInnerJoin(Krav::hentTabell(), '`' . Krav::hentTabell() . '`.`gironr` = `' . Regning::hentTabell(). '`.`' . Regning::hentPrimærnøkkelfelt(). '`')
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '`' => $kravsett->hentIdNumre()]);
        return $this->app->post($this, __FUNCTION__, $regninger);
    }

    /**
     * Forkast Utskrift
     *
     * Forkaster det siste utskriftsforsøket
     * Denne funksjonen løsner krav fra giroer, sletter purringer, og sletter innholdet i utskriftsforsøk-innstillingen
     *
     * @return $this
     * @throws Throwable
     */
    public function forkastUtskrift(): Utskriftsprosessor
    {
        $this->app->pre($this, __FUNCTION__, []);
        $this->logger->info('GIROUTSKRIFT – Forkaster pågående utskrift');
        $filbane = "{$this->app->hentFilarkivBane()}/giroer/";
        if (!is_writable($filbane)) {
            $this->logger->critical('GIROUTSKRIFT – Kan ikke skrive', ['filbane' => $filbane]);
            throw new Exception("Nødvendige tillatelser mangler for å skrive til '{$filbane}'");
        }
        $fil = "{$filbane}_utskriftsbunke.pdf";
        if (file_exists($fil)) {
            if (!is_writable($fil)) {
                $this->logger->critical('GIROUTSKRIFT – Kan ikke skrive', ['fil' => $fil]);
                throw new Exception("Nødvendige tillatelser mangler for å skrive til '{$fil}'");
            }
            if (!unlink($fil)) {
                $this->logger->critical('GIROUTSKRIFT – Kan ikke slette', ['fil' => $fil]);
                throw new Exception("Klarte ikke slette den eksisterende utskriftsfila '{$fil}'");
            }
        }

        if ($utskriftsforsøk = $this->hentLagretUtskriftsforsøk()) {
            $this->slettUbrukteGiroer($utskriftsforsøk->giroer);

            if (is_array($utskriftsforsøk->purringer)) {
                /** @var Purringsett $purringsett */
                $this->logger->info('GIROUTSKRIFT – Sletter purringer', ['purringer' => $utskriftsforsøk->purringer]);
                /** @var Purringsett $purringsett */
                $purringsett = $this->app->hentSamling(Purring::class)->filtrerEtterIdNumre($utskriftsforsøk->purringer);
                $purringsett->slettAlle();
            }
        }

        $this->logger->info('GIROUTSKRIFT – Sletter utskriftsforsøket ifra databasen');
        $this->lagreUtskriftsforsøk(null);
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * Slett ubrukte giroer
     *
     * Gjenoppløser krav som er samlet til giro, men som fortsatt ikke har blitt skrevet ut.
     * Selve gironummerene slettes ikke,
     * fordi de inneholder viktig kopling mellom KID og leieforhold.
     *
     * @param int[] $giroIder
     * @return Utskriftsprosessor
     * @throws Throwable
     */
    public function slettUbrukteGiroer(array $giroIder = []): Utskriftsprosessor
    {
        list(
            'giroIder' => $giroIder,
            )
            = $this->app->pre($this, __FUNCTION__, [
            'giroIder' => $giroIder,
        ]);
        $this->logger->info('GIROUTSKRIFT – Sletter giroer i utskriftsfil', ['antall' => count($giroIder)]);

        /** @var Kravsett $kravsett */
        $kravsett = $this->app->hentSamling(Krav::class)
            ->leggTilInnerJoin(Regning::hentTabell(),
                '`' . Krav::hentTabell() . '`.`gironr` = `' . Regning::hentTabell() . '`.`' . Regning::hentPrimærnøkkelfelt() . '`'
            );
        $kravsett->leggTilFilter(['`' . Regning::hentTabell() . '`.`utskriftsdato`' => null])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`utestående` >' => 0])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`gironr`' => $giroIder])
            ->låsFiltre();
        $kravsett->sett('regning', null);

        $this->logger->info('GIROUTSKRIFT – Sletter giroer i utskriftsfil');
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * Registrer utskrift
     *
     * Denne funksjonen registrerer utskriftsdato og oppretter purregebyr,
     * samt sender eFaktura og epost-faktura,
     * og sletter innholdet i utskriftsforsøk-innstillingen
     *
     * @param Utskriftsforsøk $utskriftsforsøk
     * @return $this
     * @throws NetsException
     * @throws Throwable
     */
    public function sendOgRegistrerUtskrift(Utskriftsforsøk $utskriftsforsøk): Utskriftsprosessor
    {
        list('utskriftsforsøk' => $utskriftsforsøk)
            = $this->app->pre($this, __FUNCTION__, [
            'utskriftsforsøk' => $utskriftsforsøk
        ]);
        /** @var Regning[] $efakturaregninger */
        $efakturaregninger = [];

        if (is_array($utskriftsforsøk->giroer)) {
            /** @var Regningsett $regningsett */
            $regningsett = $this->app->hentSamling(Regning::class)
                ->filtrerEtterIdNumre($utskriftsforsøk->giroer)->låsFiltre();
            $regningsett->leggTilLeieforholdModell()->leggTilBeløpFelt();
            $regningsett
                ->leggTilLeftJoin(Person::hentTabell(),
                    '`' . Leieforhold::hentTabell() . '`.`regningsperson` = `'
                    . Person::hentTabell() . '`.`' . Person::hentPrimærnøkkelfelt() . '`')
                ->leggTilModell(Modell\Person::class, 'leieforhold.regningsperson', null, []);

            $regningsett->forEach(function(Regning $regning) use ($utskriftsforsøk, &$efakturaregninger) {
                /** @var Leieforhold $leieforhold */
                $leieforhold = $regning->hentLeieforhold();
                /** @var float $beløp */
                $beløp = $regning->hentBeløp();
                $epostFakturaAvtale = $leieforhold->hentGiroformat() == 'epost'
                    && ($beløp >= 0 ? $this->app->hentValg('epost_faktura_aktivert') : $this->app->hentValg('epost_kreditnota_aktivert'));
                $efakturaAvtale = $this->app->hentValg('efaktura') && $leieforhold->hentEfakturaIdentifier();

                /**
                 * Dersom det brukes kombinasjon av fbo (avtalegiro) og efaktura
                 * skal ikke eFaktura sendes nå men om to dager.
                 * I disse tilfellene skal det heller ikke påføres utskriftsdato nå.
                 */
                $efakturaAvtalegiroKombi = (
                    $leieforhold->hentFboOgEfaktura()
                    && $regning->hentUtestående() > 0
                );

                /**
                 * Ignorer utskrevne regninger og kombiavtaler
                 */
                if ($regning->hentUtskriftsdato()) {
                    $this->logger->warning("GIROUTSKRIFT - Regning {$regning} er allerede skrevet ut");
                    return;
                }
                /**
                 * Ignorer utskrevne regninger og kombiavtaler
                 */
                if ($efakturaAvtalegiroKombi) {
                    $this->logger->notice("GIROUTSKRIFT - Regning {$regning} skal sendes som forsinket eFaktura");
                    return;
                }
                // Samle eFaktura i egen bunke
                if (($efakturaAvtale) && $beløp > 0) {
                    $this->logger->info("GIROUTSKRIFT - Regning {$regning} skal sendes som eFaktura");
                    $efakturaregninger[] = $regning;
                } else {
                    if ($epostFakturaAvtale) {
                        try {
                            $regning->sendEpost();
                            $this->logger->info("GIROUTSKRIFT - Regning {$regning} lagt i kø som epost-faktura");
                        } catch (Throwable $e) {
                            $this->logger->warning("GIROUTSKRIFT - Regning {$regning} kunne ikke sendes som epost-faktura: {$e->getMessage()}");
                            return;
                        }
                    } else {
                        $regning->settFormat('papir');
                        $regning->settUtskriftsdato($utskriftsforsøk->tidspunkt);
                        $this->logger->info("GIROUTSKRIFT - Regning {$regning} registrert skrevet ut i papirformat");
                    }
                }
            });
        }

        if($efakturaregninger) {
            /** @var Regningsett $efakturaRegningssett */
            $efakturaRegningssett = $this->app->hentSamling(Regning::class)->lastFra($efakturaregninger);
            $this->logger->info('GIROUTSKRIFT – Utskriften inneholder ' . $efakturaRegningssett->hentAntall() . ' efakturaer som køes for oversending');
            $this->leggTilEfakturaKø($efakturaRegningssett);
        }

        foreach ($utskriftsforsøk->purringer as &$purring) {
            if (!is_a($purring, Purring::class)) {
                /** @var Purring $purring */
                $purring = $this->app->hentModell(Purring::class, $purring);
            }
            if (in_array($purring, $utskriftsforsøk->gebyrpurringer)) {
                $purring->opprettGebyr();
            }
        }
        $this->app->epostpurring($utskriftsforsøk->purringer);

        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @return Utskriftsforsøk|null
     * @throws Exception
     */
    public function hentLagretUtskriftsforsøk(): ?Utskriftsforsøk
    {
        $this->app->pre($this, __FUNCTION__, []);
        $utskriftsforsøk = null;
        if ($this->app->hentValg('utskriftsforsøk')) {
            /** @var Utskriftsforsøk $utskriftsforsøk */
            $utskriftsforsøk = unserialize($this->app->hentValg('utskriftsforsøk'));
        }
        return $this->app->post($this, __FUNCTION__, $utskriftsforsøk);
    }

    /**
     * @param Utskriftsforsøk|null $utskriftsforsøk
     * @return Utskriftsprosessor
     * @throws Throwable
     */
    public function lagreUtskriftsforsøk(?Utskriftsforsøk $utskriftsforsøk): Utskriftsprosessor
    {
        list('utskriftsforsøk' => $utskriftsforsøk)
            = $this->app->pre($this, __FUNCTION__, [
            'utskriftsforsøk' => $utskriftsforsøk
        ]);
        $this->app->settValg('utskriftsforsøk',
            $utskriftsforsøk ? serialize($utskriftsforsøk) : ''
        );
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @param CoreModelImplementering $leiebase
     * @param array $argumenter
     * @return array
     * @throws Throwable
     * @noinspection PhpUnusedParameterInspection
     */
    public function cron(CoreModelImplementering $leiebase, array $argumenter = []): array
    {
        $cronTid = $argumenter[0] ?? date_create_immutable('now', new DateTimeZone('UTC'));

        if ($cronTid->format('H') > 3) {
            $this->fjernGamleUtskriftsforsøk($cronTid);

            if(
                $this->app->hentValg('automatisk_faktura')
                && in_array($cronTid->format('i'), [5,10,15,20,25,30,35,40,45,50,55])
            ) {
                if($this->erManuellUtskriftsdato($cronTid)) {
                    $this->sendAutoRegninger($cronTid, $this->hentManueltBekreftedeUtskriftsformater());
                }
                $this->sendAutoRegninger($cronTid, $this->hentSelvBekreftendeUtskriftsformater());
            }
        }

        return $argumenter;
    }

    /**
     * Finn alle krav som er klare til å skrives ut
     *
     * @param DateTimeInterface $forfallFramTil
     * @param array $kravtypeFilter
     * @param array $inkLeieforhold
     * @param array $eksLeieforhold
     * @return Kravsett
     * @throws Exception
     */
    public function hentKravsettForUtskrift(
        DateTimeInterface $forfallFramTil,
        array $kravtypeFilter = [],
        array $inkLeieforhold = [],
        array $eksLeieforhold = []
    ): Kravsett
    {
        list('forfallFramTil' => $forfallFramTil,
            'kravtypeFilter' => $kravtypeFilter,
            'inkLeieforhold' => $inkLeieforhold,
            'eksLeieforhold' => $eksLeieforhold)
            = $this->app->pre($this, __FUNCTION__, [
            'forfallFramTil' => $forfallFramTil,
            'kravtypeFilter' => $kravtypeFilter,
            'inkLeieforhold' => $inkLeieforhold,
            'eksLeieforhold' => $eksLeieforhold
        ]);
        $kravtypeFilter = array_map('strval', $kravtypeFilter);
        $inkLeieforhold = array_map('intval', array_map('strval', $inkLeieforhold));
        $eksLeieforhold = array_map('intval', array_map('strval', $eksLeieforhold));

        /** @var Kravsett $kravsett */
        $kravsett = $this->app->hentSamling(Krav::class);
        $kravsett->leggTilLeftJoinForLeieforhold();
        $kravsett->leggTilLeftJoinForRegning();
        $kravsett->leggTilLeftJoin(
            Efaktura::hentTabell(),
            '`' . Efaktura::hentTabell() . '`.`giro` = `'
            . Regning::hentTabell() . '`.`' . Regning::hentPrimærnøkkelfelt() . '`'
        );
        $kravsett->forhåndslastLeieforholdNavn();

        $kravsett->leggTilLeftJoin(
            ['regningsperson' => Person::hentTabell()],
            '`regningsperson`.`personid` = `' . Leieforhold::hentTabell() . '`.`regningsperson`'
        );

        $kravsett->leggTilLeftJoin(
            EfakturaIdentifier::hentTabell(),
            '`' . EfakturaIdentifier::hentTabell() . '`.`person_id` = `' . Leieforhold::hentTabell() . '`.`regningsperson`'
        );
        $kravsett->leggTilLeftJoin(Fbo\Trekk::hentTabell(),
            '`' . Krav::hentTabell() . '`.`gironr` = `' . Fbo\Trekk::hentTabell() . '`.`gironr`'
        );

        /* Ekskluder e-faktura som skal sendes forsinket pga kombinasjon med AvtaleGiro */
        $ikkeForsinketEfakturaFilter = ['or' => [
            '`' . Fbo\Trekk::hentTabell() . '`.`gironr`' => null,
            '`' . EfakturaIdentifier::hentTabell() . '`.`efaktura_identifier`' => null,
            '`' . EfakturaIdentifier::hentTabell() . '`.`has_auto_accept_agreements`' => false,
            '`' . EfakturaIdentifier::hentTabell() . '`.`blocked_by_receiver`' => true,
        ]];
        $kravsett->leggTilFilter($ikkeForsinketEfakturaFilter);

        $kravsett
            ->leggTilFilter(['`' . Regning::hentTabell() . '`.`utskriftsdato`' => null])
            ->leggTilFilter(['`' . Efaktura::hentTabell() . '`.`giro`' => null])
            ->leggTilFilter(['or' => [
                '`' . Krav::hentTabell() . '`.`forfall` <=' => $forfallFramTil->format('Y-m-d'),
                '`' . Krav::hentTabell() . '`.`forfall`' => null
            ]])
            ->leggTilModell(Leieforhold::class)
            ->leggTilSortering('kravdato')
        ;

        if ($kravtypeFilter) {
            if (in_array(strtolower(Krav::TYPE_ANNET), $kravtypeFilter)) {
                $kravtypeFilter[] = Krav::TYPE_PURREGEBYR;
            }
            $kravfilter = ['`' . Krav::hentTabell() . '`.`type`' => $kravtypeFilter];
            $kravsett->leggTilFilter($kravfilter);
        }

        if ($inkLeieforhold) {
            $kravsett->leggTilFilter(['`' . Krav::hentTabell() . '`.`leieforhold`' => $inkLeieforhold]);
        }
        if ($eksLeieforhold) {
            $kravsett->leggTilFilter(['`' . Krav::hentTabell() . '`.`leieforhold` NOT IN' => $eksLeieforhold]);
        }
        $kravsett->låsFiltre();

        return $this->app->post($this, __FUNCTION__, $kravsett);
    }

    /**
     * @param DateTimeInterface|null $purretidspunkt
     * @param array $inkLeieforhold
     * @param array $eksLeieforhold
     * @return Kravsett
     * @throws Throwable
     */
    public function hentKravsettForPurring(
        ?DateTimeInterface $purretidspunkt = null,
        array              $inkLeieforhold = [],
        array              $eksLeieforhold = []
    ): Kravsett
    {
        list('purretidspunkt' => $purretidspunkt,
            'inkLeieforhold' => $inkLeieforhold,
            'eksLeieforhold' => $eksLeieforhold)
            = $this->app->pre($this, __FUNCTION__, [
            'purretidspunkt' => $purretidspunkt,
            'inkLeieforhold' => $inkLeieforhold,
            'eksLeieforhold' => $eksLeieforhold
        ]);
        $purretidspunkt = $purretidspunkt ?? date_create_immutable();
        /** @var Kravsett $ubetalteKrav */
        $ubetalteKrav = $this->app->hentSamling(Krav::class);
        $ubetalteKrav->leggTilLeftJoinForRegning();
        $ubetalteKrav
            ->leggTilRegningModell([])
            ->leggTilLeftJoinForLeieforhold()
            ->leggTilLeieforholdModell()
            ->forhåndslastLeieforholdNavn()
            ->leggTilRegningForfallFelt()
            ->leggTilRegningSistePurringFelt()
            ->leggTilRegningSisteForfallFelt()

            ->leggTilLeftJoin(Betalingsplan\Originalkrav::hentTabell(), '`' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '` = `' . Betalingsplan\Originalkrav::hentTabell() . '`.`krav_id`')
            ->leggTilLeftJoin(Betalingsplan::hentTabell(), '`' . Betalingsplan::hentTabell() . '`.`' . Betalingsplan::hentPrimærnøkkelfelt() . '` = `' . Betalingsplan\Originalkrav::hentTabell() . '`.`betalingsplan_id`')
            ->leggTilModell(Betalingsplan::class)
        ;
        $ubetalteKrav
            ->leggTilFilter(['`' . Regning::hentTabell() . '`.`utskriftsdato` IS NOT NULL'])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`forfall` <' => $purretidspunkt->format('Y-m-d')])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`utestående` >' => 0])
            ->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`frosset`' => false])
            ->leggTilFilter(['`' . Betalingsplan\Originalkrav::hentTabell() . '`.`krav_id`' => null])
        ;
        if ($inkLeieforhold) {
            $ubetalteKrav->leggTilFilter(['`' . Krav::hentTabell() . '`.`leieforhold`' => $inkLeieforhold]);
        }
        if ($eksLeieforhold) {
            $ubetalteKrav->leggTilFilter(['`' . Krav::hentTabell() . '`.`leieforhold` NOT IN' => $eksLeieforhold]);
        }
        $ubetalteKrav->låsFiltre();

        return $this->app->post($this, __FUNCTION__, $ubetalteKrav);
    }

    /**
     * @param DateTimeImmutable $tid
     * @param string[] $giroformater
     * @return Utskriftsprosessor
     * @throws Throwable
     */
    public function sendAutoRegninger(DateTimeImmutable $tid, array $giroformater = []): Utskriftsprosessor
    {
        list('tid' => $tid, 'giroformater' => $giroformater, )
            = $this->app->pre($this, __FUNCTION__, ['tid' => $tid, 'giroformater' => $giroformater, ]);
        try {
            $manueltBekreftedeUtskriftsformater = $this->hentManueltBekreftedeUtskriftsformater();

            if($this->app->hentValg('automatisk_faktura')) {
                $lagretUtskriftsforsøk = $this->hentLagretUtskriftsforsøk();

                /* Dersom minst et av giroformatene krever manuell bekreftelse så er dette en manuell utskrift */
                $manuellUtskrift = (bool)array_intersect($giroformater, $manueltBekreftedeUtskriftsformater);

                /* Det må ikke påbegynnes nye manuelle utskrifter dersom en allerede er igangsatt */
                if($manuellUtskrift && $lagretUtskriftsforsøk) {
                    return $this->app->post($this, __FUNCTION__, $this);
                }
                try {
                    $fakturaTidsrom = new DateInterval(strval($this->app->hentValg('automatisk_faktura_tidsrom')));
                } catch (Exception $e) {
                    $fakturaTidsrom = new DateInterval('P1M');
                }
                $seFramTil = $tid->add($fakturaTidsrom);

                $adskilt = !empty($this->app->hentValg('automatisk_faktura_adskilt'));

                $ekskluderteKravtyper = $this->app->hentValg('automatisk_faktura_ekskluder_kravtyper');
                $ekskluderteKravtyper = $ekskluderteKravtyper ? explode(',', $ekskluderteKravtyper) : [];
                /** @var string[] $ekskluderteKravtyper */
                $ekskluderteKravtyper = array_map('trim', $ekskluderteKravtyper);

                $formatfilterRegninger = ['or' => [
                    '`' . Leieforhold::hentTabell() . '`.`giroformat`' => $giroformater
                ]];
                if($manuellUtskrift) {
                    $formatfilterRegninger['or'][] = [
                        'and' => [
                            '`' . Leieforhold::hentTabell() . '`.`giroformat`' => Regning::FORMAT_INGEN_UTSKRIFT,
                            '`' . Krav::hentTabell() . '`.`type` NOT IN' => $this->hentFasteKravtyper(),
                        ]
                    ];
                }
                else {
                    $formatfilterRegninger['or'][] = [
                        'and' => [
                            '`' . Leieforhold::hentTabell() . '`.`giroformat`' => Regning::FORMAT_INGEN_UTSKRIFT,
                            '`' . Krav::hentTabell() . '`.`type` IN' => $this->hentFasteKravtyper(),
                        ]
                    ];
                }

                /*
                 * E-faktura-filteret inkluderer eller utelater regninger som skal sendes med e-Faktura,
                 * avhengig av om eFaktura skal sendes nå
                 */
                if(in_array(Regning::FORMAT_EFAKTURA, $giroformater)) {
                    $efakturaFilter = ['or' => [
                        ['and' => [
                            '`' . EfakturaIdentifier::hentTabell() . '`.`efaktura_identifier` IS NOT NULL',
                            '`' . EfakturaIdentifier::hentTabell() . '`.`has_auto_accept_agreements`' => true,
                            '`' . EfakturaIdentifier::hentTabell() . '`.`blocked_by_receiver`' => false,
                        ]],
                        $formatfilterRegninger
                    ]];
                }
                else {
                    $efakturaFilter = ['and' => [
                        ['or' => [
                            '`' . EfakturaIdentifier::hentTabell() . '`.`efaktura_identifier`' => null,
                            '`' . EfakturaIdentifier::hentTabell() . '`.`has_auto_accept_agreements`' => false,
                            '`' . EfakturaIdentifier::hentTabell() . '`.`blocked_by_receiver`' => true,
                        ]],
                        $formatfilterRegninger
                    ]];
                }
                $this->logger->debug('AUTO_GIROUTSKRIFT – Forbereder automatisk utskrift', ['formater' => $giroformater]);

                $kravsett = $this->hentKravsettForUtskrift($seFramTil);

                // Ingen krav skal skrives ut automatisk sammme dag som de ble opprettet
                $kravsett->leggTilFilter(['`' . Krav::hentTabell() . '`.`opprettet` <' => $tid->format('Y-m-d 00:00:00')]);
                $kravsett->leggTilFilter($efakturaFilter);
                $kravsett->leggTilFilter(['`' . Krav::hentTabell() . '`.`type` NOT IN' => $ekskluderteKravtyper]);
                $this->leggTilFilterForEksisterendeForfall($kravsett, $seFramTil, $seFramTil->add($fakturaTidsrom));

                if($lagretUtskriftsforsøk) {
                    $kravsett->leggTilFilter(['`' . Krav::hentTabell() . '`.`gironr` NOT IN' => $lagretUtskriftsforsøk->giroer]);
                }
                $kravsett->låsFiltre();
                $this->begrensBunt($kravsett);

                /** @var string[] $leieforholdPurreformater */
                $leieforholdPurreformater = [];
                $ubetalteRegninger = null;
                $purreLeieforhold = null;

                $automatiskPurring = $this->erAutomatiskePurringerTilgjengelig($giroformater);
                if($automatiskPurring) {
                    $this->logger->debug('AUTO_GIROUTSKRIFT – Forbereder ' . implode('-purringer og ', $giroformater)  . '-purringer.');

                    $purreIntervall = $this->app->hentValg('purreintervall') ?: 'P0D';
                    $purreIntervall = new DateInterval($purreIntervall);
                    $purrefrist = $tid->sub($purreIntervall);

                    /** @var int[] $regningsIder */
                    $regningsIder = [];
                    /** @var int[] $leieforholdIder */
                    $leieforholdIder = [];

                    $ubetalteKrav = $this->hentKravsettForPurring($tid);
                    $this->leggTilAutofiltreForKravsettForPurringer($ubetalteKrav, $purrefrist, $tid);

                    /*
                     * Dersom purregebyr er aktivert, men epost-purringer ikke kan gebyrlegges,
                     * så må leieforhold med regningsformat e-post purres per manuelt format
                     */
                    $formatfilterForPurringer = ['or' => ['`' . Leieforhold::hentTabell() . '`.`giroformat`' => $giroformater]];
                    if($manuellUtskrift
                    ) {
                        $formatfilterForPurringer['or'][] = ['`' . Leieforhold::hentTabell() . '`.`giroformat`' => ''];
                        $formatfilterForPurringer['or'][] = ['`' . Leieforhold::hentTabell() . '`.`giroformat`' => null];
                        if($this->app->hentValg('purregebyr') != 0
                            && !$this->app->hentValg('epost_purregebyr_aktivert')
                        ) {
                            $formatfilterForPurringer['or'][] = ['`' . Leieforhold::hentTabell() . '`.`giroformat`' => Regning::FORMAT_EPOST];
                        }
                    }
                    $ubetalteKrav->leggTilFilter($formatfilterForPurringer);
                    $ubetalteKrav->låsFiltre();
                    $this->begrensBunt($ubetalteKrav);

                    foreach ($ubetalteKrav as $krav) {
                        $regningsIder[] = $krav->hentRegning()->hentId();
                        $leieforholdIder[] = $krav->hentLeieforhold()->hentId();
                        $leieforholdPurreformater[(string)$krav->leieforhold] = $krav->leieforhold->hentPurreformat();
                    }
                    $regningsIder = array_unique($regningsIder);
                    $leieforholdIder = array_unique($leieforholdIder);

                    /** @var Regningsett $ubetalteRegninger */
                    $ubetalteRegninger = $this->app->hentSamling(Regning::class);
                    $ubetalteRegninger->filtrerEtterIdNumre($regningsIder)->låsFiltre();
                    $ubetalteRegninger
                        ->leggTilLeieforholdModell()
                        ->leggTilSisteForfallFelt();
                    $ubetalteRegninger->inkluder('purringer');
                    $ubetalteRegninger->inkluder('krav');

                    /** @var Leieforholdsett $purreLeieforhold */
                    $purreLeieforhold = $this->app->hentSamling(Leieforhold::class);
                    $purreLeieforhold->filtrerEtterIdNumre($leieforholdIder)->låsFiltre();
                }

                if(
                    !$kravsett->hentAntall()
                    && (!$ubetalteRegninger || !$ubetalteRegninger->hentAntall())
                ) {
                    $this->app->logger->debug('AUTO_GIROUTSKRIFT – Ingen utskrift nødvendig');
                }
                else {
                    $utskrift = $this->forberedUtskrift(
                        $kravsett,
                        $adskilt,
                        $ubetalteRegninger,
                        true,
                        $leieforholdPurreformater,
                        $purreLeieforhold
                    );
                    $this->app->logger->info('AUTO_GIROUTSKRIFT – Utskriftskonfigurasjonen opprettet', ['memory_usage' => memory_get_usage()]);

                    if($manuellUtskrift) {
                        if($utskrift->giroer || $utskrift->purringer || $utskrift->statusoversikter) {
                            $utskrift->autoGenerertManuellUtskrift = true;
                            $this->lagreUtskriftsforsøk($utskrift);
                            $utskrift->giroer = $this->filtrerUtBetalteRegninger($utskrift->giroer);
                            $this->lagUtskriftsfil($utskrift);
                            $this->varsleForberedtUtskrift();
                            $this->logger->info('AUTO_GIROUTSKRIFT – Manuell utskrift forberedt');
                        }
                    }
                    else {
                        $this->sendOgRegistrerUtskrift($utskrift);
                    }
                }
            }
        } catch (Throwable $e) {
            $this->app->logger->critical($e->getMessage(), $e->getTrace());
        }
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * Sjekker om dette er tiden for å forberede manuell utskrift
     *
     * @param DateTimeInterface $dato
     * @return bool
     */
    private function erManuellUtskriftsdato(DateTimeInterface $dato): bool
    {
        $automatiskFakturaDatoer = $this->app->hentValg('automatisk_faktura_datoer');
        $automatiskFakturaDatoer = $automatiskFakturaDatoer
            ? explode(',', $automatiskFakturaDatoer)
            : [];
        $automatiskFakturaDatoer = array_map('trim', $automatiskFakturaDatoer);
        $automatiskFakturaUkedager = $this->app->hentValg('automatisk_faktura_ukedager');
        $automatiskFakturaUkedager = $automatiskFakturaUkedager
            ? explode(',', $automatiskFakturaUkedager)
            : [];
        $automatiskFakturaUkedager = array_map('trim', $automatiskFakturaUkedager);
        return
            (
                in_array($dato->format('j'), $automatiskFakturaDatoer)
                || in_array($dato->format('j') - $dato->format('t') - 1, $automatiskFakturaDatoer)
                || in_array('*', $automatiskFakturaDatoer)
            )
            && in_array($dato->format('N'), $automatiskFakturaUkedager);
    }

    /**
     * @param DateTimeInterface $tid
     * @return Utskriftsprosessor
     * @throws Throwable
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    private function fjernGamleUtskriftsforsøk(DateTimeInterface $tid): Utskriftsprosessor
    {
        list('tid' => $tid)
            = $this->app->pre($this, __FUNCTION__, ['tid' => $tid,]);
        try {
            $utskrift = $this->hentLagretUtskriftsforsøk();
            if (!$utskrift || !$utskrift->autoGenerertManuellUtskrift) {
                return $this->app->post($this, __FUNCTION__, $this);
            }
            if ($utskrift->tidspunkt->format('Ymd') == $tid->format('Ymd')) {
                return $this->app->post($this, __FUNCTION__, $this);
            }
            $this->forkastUtskrift();
        } catch (Throwable $e) {
            $this->app->logger->critical($e->getMessage(), $e->getTrace());
        }
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @return Utskriftsprosessor
     * @throws Throwable
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    private function varsleForberedtUtskrift(): Utskriftsprosessor
    {
        $this->app->pre($this, __FUNCTION__, []);
        $lenke = $this->app->http_host . '/drift/index.php?oppslag=utskriftsveiviser&returi=default&last_ned';
        $this->logger->info('AUTO_GIROUTSKRIFT – Sender varsel om forberedt manuell utskrift');
        $lenkeKnapp = new HtmlElement(
            'a', [
            'class' => 'button',
            'href' => $lenke,
            'title' => 'Logg inn for å vise utskriften'
        ], 'Vis utskriften'
        );
        $tittel = 'Manuell utskrift av regninger';

        $innhold = 'En manuell utskrift av regninger har blitt forberedt i leiebasen.<br>';
        $innhold .= 'Regningene må skrives ut og bekreftes sendt snarest, og i senest løpet av dagen.<br>';
        $innhold .= 'Ubekreftede utskrifter slettes etter 24 timer.<br>';
        $innhold = '<p>' . $innhold . '</p>';

        /** @var Visning\felles\epost\shared\Epost $html */
        $html = $this->app->hentVisning(Visning\felles\epost\shared\Epost::class, [
            'tittel' => $tittel,
            'innhold' => $innhold . '<p>' . $lenkeKnapp . '</p>',
            'bunntekst' => ''
        ]);
        /** @var Visning\felles\epost\shared\Epost $tekst */
        $tekst = $this->app->hentVisning(Visning\felles\epost\shared\Epost::class, [
            'tittel' => $tittel,
            'innhold' => Visning\felles\epost\shared\Epost::br2crnl($innhold) . "\r\n" . $lenke,
            'bunntekst' => ''
        ])->settMal('felles/epost/shared/Epost.txt');

        $this->app->sendMail((object)[
            'to' => $this->app->hentValg('utleier') . '<' . $this->app->hentValg('epost') . '>',
            'subject' => $tittel,
            'html' => $html,
            'text' => $tekst,
            'type' => 'utskrift_forberedt'
        ]);

        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * Legg til filter for eksisterende forfall
     *
     * Dette er et filter som utelater krav med ubestemt forfall
     * dersom det ikke skal sendes andre regninger i tidsrommet,
     * men det er planlagt andre regninger senere.
     *
     * @param Kravsett $kravsett
     * @param DateTimeImmutable $maksForfallsdato
     * @param DateTimeImmutable $maksUtsettelse
     * @return Utskriftsprosessor
     * @throws Throwable
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    private function leggTilFilterForEksisterendeForfall(
        Kravsett          $kravsett,
        DateTimeImmutable $maksForfallsdato,
        DateTimeImmutable $maksUtsettelse
    ): Utskriftsprosessor
    {
        list(
            'kravsett' => $kravsett,
            'maksForfallsdato' => $maksForfallsdato,
            'maksUtsettelse' => $maksUtsettelse,
            )
            = $this->app->pre($this, __FUNCTION__, [
            'kravsett' => $kravsett,
            'maksForfallsdato' => $maksForfallsdato,
            'maksUtsettelse' => $maksUtsettelse,
        ]);
        $tp = CoreModel::$tablePrefix;
        $anvendeligForfallSubQuery = <<< SQL
            SELECT `leieforhold`, MIN(`forfall`) AS `forfall`
            FROM `{$tp}krav`
            WHERE `forfall` <= '{$maksForfallsdato->format('Y-m-d')}'
              AND `gironr` IS NULL
            GROUP BY `leieforhold`
            SQL;

        $framtidigForfallSubQuery = <<< SQL
            SELECT `leieforhold`, MIN(`forfall`) AS `forfall`
            FROM `{$tp}krav`
            WHERE `forfall` > '{$maksForfallsdato->format('Y-m-d')}'
              AND `forfall` <= '{$maksUtsettelse->format('Y-m-d')}'
              AND `gironr` IS NULL
            GROUP BY `leieforhold`
            SQL;

        $kravsett->leggTilSubQueryJoin($anvendeligForfallSubQuery, 'anvendelig_forfall',
            '`' . Krav::hentTabell() . '`.`leieforhold` = `anvendelig_forfall`.`leieforhold`'
        );
        $kravsett->leggTilSubQueryJoin($framtidigForfallSubQuery, 'framtidig_forfall',
            '`' . Krav::hentTabell() . '`.`leieforhold` = `framtidig_forfall`.`leieforhold`'
        );
        $kravsett->leggTilFilter(['or' => [
            '`anvendelig_forfall`.`forfall` IS NOT NULL',
            '`framtidig_forfall`.`forfall`' => null,
        ]]);
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @param Kravsett $ubetalteKrav
     * @param DateTimeImmutable $purrefrist
     * @param DateTimeImmutable $tid
     * @return Utskriftsprosessor
     * @throws Throwable
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    private function leggTilAutofiltreForKravsettForPurringer(Kravsett $ubetalteKrav, DateTimeImmutable $purrefrist, DateTimeImmutable $tid): Utskriftsprosessor
    {
        list(
            'ubetalteKrav' => $ubetalteKrav,
            'purrefrist' => $purrefrist,
            'tid' => $tid,
            )
            = $this->app->pre($this, __FUNCTION__, [
            'ubetalteKrav' => $ubetalteKrav,
            'purrefrist' => $purrefrist,
            'tid' => $tid,
        ]);
        /* Leieforhold med oppfølgingsstopp skal ikke purres automatisk */
        $ubetalteKrav->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`stopp_oppfølging`' => false]);
        $ubetalteKrav->leggTilFilter(['or' => [
            ['`' . Leieforhold::hentTabell() . '`.`avvent_oppfølging`' => null],
            ['`' . Leieforhold::hentTabell() . '`.`avvent_oppfølging` <' => $tid->format('Y-m-d')]
        ]]);

        /* Krav som inngår i en aktiv betalingsavtale skal ikke purres automatisk */
        $ubetalteKrav->leggTilFilter(['or' => [
            ['`' . Betalingsplan\Originalkrav::hentTabell() . '`.`' . Betalingsplan\Originalkrav::hentPrimærnøkkelfelt() . '`' => null],
            ['`' . Betalingsplan::hentTabell() . '`.`aktiv`' => false]
        ]]);

        /* Krav som nylig har blitt purret skal ikke purres automatisk */
        $ubetalteKrav->leggTilHaving(['`giroer.regning_ekstra.siste_forfall` <' => $purrefrist->format('Y-m-d')]);
        $ubetalteKrav->låsFiltre();
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * Hvilke giroformater er selvbekreftende og kan dermed sendes automatisk?
     *
     * @return string[]
     */
    public function hentSelvBekreftendeUtskriftsformater(): array
    {
        return $this->selvBekreftendeUtskriftsformater;
    }

    /**
     * @param array $selvBekreftendeUtskriftsformater
     * @return Utskriftsprosessor
     */
    public function settSelvBekreftendeUtskriftsformater(array $selvBekreftendeUtskriftsformater): Utskriftsprosessor
    {
        $this->selvBekreftendeUtskriftsformater = $selvBekreftendeUtskriftsformater;
        return $this;
    }

    /**
     * Hvilke giroformater må bekreftes manuelt?
     *
     * @return string[]
     */
    public function hentManueltBekreftedeUtskriftsformater(): array
    {
        return $this->manueltBekreftedeUtskriftsformater;
    }

    /**
     * @param array $manueltBekreftedeUtskriftsformater
     * @return Utskriftsprosessor
     */
    public function settManueltBekreftedeUtskriftsformater(array $manueltBekreftedeUtskriftsformater): Utskriftsprosessor
    {
        $this->manueltBekreftedeUtskriftsformater = $manueltBekreftedeUtskriftsformater;
        return $this;
    }

    /**
     * Er automatiske purringer tilgjengelig?
     *
     * Automatiske purringer er tilgjengelig dersom automatisk purring er aktivert.
     *
     * For e-post-purringer kreves imidlertid også at epost purregebyr er aktivert
     * dersom regningene skal gebyrlegges
     *
     * @param string[] $giroformater
     * @return bool
     * @throws Throwable
     */
    public function erAutomatiskePurringerTilgjengelig(array $giroformater): bool
    {
        $this->app->pre($this, __FUNCTION__, []);
        $automatiskPurring =
            $this->app->hentValg('automatisk_purring')
            && (
                $this->app->hentValg('purregebyr') == 0
                || $this->app->hentValg('epost_purregebyr_aktivert')
                || !in_array(Regning::FORMAT_EPOST, $giroformater)
            );
        return $this->app->post($this, __FUNCTION__, $automatiskPurring);
    }

    /**
     * @param bool $manuellUtskrift
     * @return DateTimeImmutable|null
     */
    public function hentNesteUtskriftsdato(bool $manuellUtskrift): ?DateTimeImmutable
    {
        $dato = new DateTimeImmutable();
        $enDag = new DateInterval('P1D');
        $dato = $dato->settime(0, 0);
        if($manuellUtskrift) {
            if(!isset(self::$nesteManuelleUtskriftsdato)) {
                if(!trim($this->app->hentValg('automatisk_faktura_datoer'))
                    || !trim($this->app->hentValg('automatisk_faktura_ukedager'))
                ){
                    self::$nesteManuelleUtskriftsdato = $dato->sub($enDag);
                }
                else {
                    $dato = $dato->add($enDag);
                    while (!$this->erManuellUtskriftsdato($dato)) {
                        $dato = $dato->add($enDag);
                    }
                    self::$nesteManuelleUtskriftsdato = $dato;
                }
            }
            return self::$nesteManuelleUtskriftsdato < $dato ? null : self::$nesteManuelleUtskriftsdato;
        }
        return $dato->add($enDag);
    }

    /**
     * Tildel reellkrav-id til nylig utsendte krav
     *
     * @return $this
     * @throws Throwable
     */
    public function oppdaterReellKravIder(): Utskriftsprosessor
    {
        /** @var Kravsett $kravsett */
        $kravsett = $this->app->hentSamling(Krav::class);
        $kravsett->leggTilFilter(['reell_krav_id' => null])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`gironr` IS NOT NULL'])
            ->låsFiltre();
        $kravsett->leggTilSortering('kravdato');
        foreach ($kravsett as $krav) {
            $krav->settReellKravId();
        }
        return $this;
    }

    /**
     * @param Kravsett $kravsett
     * @return Kravsett
     * @throws Throwable
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    private function begrensBunt(Kravsett $kravsett): Kravsett
    {
        list('kravsett' => $kravsett)
            = $this->app->pre($this, __FUNCTION__, ['kravsett' => $kravsett]);
        $kravsett->leggTilSortering('leieforhold');
        $kravsett->begrens($this->automatiskUtskriftBuntbegrensning);
        $this->automatiskUtskriftBuntbegrensning = max(
            0, $this->automatiskUtskriftBuntbegrensning - $kravsett->hentBuntAntall()
        );

        /*
         * Vi vil ikke at et leieforhold unødig skal bli fordelt over flere utskriftsrunder
         */
        if($kravsett->hentBuntAntall() < $kravsett->hentAntall()) {
            $leieforholdIder = [];
            foreach($kravsett as $krav) {
                $leieforhold = $krav->leieforhold;
                if($leieforhold) {
                    $leieforholdIder[] = $leieforhold->hentId();
                }
            }
            $leieforholdIder = array_unique($leieforholdIder);
            sort($leieforholdIder);
            array_pop($leieforholdIder);
            $kravsett->leggTilFilter(['`' . Krav::hentTabell() . '`.`leieforhold`' => array_unique($leieforholdIder)]);
        }
        return $this->app->post($this, __FUNCTION__, $kravsett);
    }

    /**
     * @return string[]
     * @throws Throwable
     */
    public function hentFasteKravtyper(): array
    {
        $this->app->pre($this, __FUNCTION__, []);
        if(!isset($this->fasteKravtyper)) {
            $this->fasteKravtyper = [Krav::TYPE_HUSLEIE];
            /** @var Delkravtypesett $husleieTillegg */
            $husleieTillegg = $this->app->hentDelkravtyper()
                ->leggTilFilter(['selvstendig_tillegg' => true])
                ->leggTilFilter(['kravtype' => Krav::TYPE_HUSLEIE])
                ->låsFiltre();
            foreach($husleieTillegg as $tillegg) {
                $this->fasteKravtyper[] = $tillegg->kode;
            }
        }
        return $this->app->post($this, __FUNCTION__, $this->fasteKravtyper);
    }

    /**
     * Lag eFakturaforsendelse som senere overføres til NETS
     *
     * @param Regningsett $efakturaRegningssett
     * @return $this
     * @throws Exception
     */
    public function leggTilEfakturaKø(Regningsett $efakturaRegningssett): Utskriftsprosessor
    {
        list('efakturaRegningssett' => $efakturaRegningssett)
            = $this->app->pre($this, __FUNCTION__, ['efakturaRegningssett' => $efakturaRegningssett]);
        $efakturaRegningssett->forEach(function (Regning $regning) {
            $this->logger->info('GIROUTSKRIFT – Køer eFaktura for regning ' . $regning->hentId());
            $regning->opprettEfaktura();
        });
        return $this->app->post($this, __FUNCTION__, $this);
    }
}