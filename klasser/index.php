<?php

if(defined('LEGAL')) {
    spl_autoload_register(function ($class) {
        $psr4Roots = ['Kyegil\\Leiebasen\\' => ''];
        $psr4Roots = array_merge($psr4Roots, \Kyegil\Leiebasen\Leiebase::$config['psr4_roots'] ?? []);
//        $class = str_replace('Kyegil\\Leiebasen\\', '', $class);
        foreach($psr4Roots as $NamespacePart => $location) {
            $class = str_replace($NamespacePart, $location, $class);
        }
        $class = str_replace('\\', '/', $class);

        $possiblePaths = [
            __DIR__ . "/../plugins/{$class}.php",
            __DIR__ . "/{$class}.php",
            __DIR__ . "/{$class}.class.php",
            __DIR__ . "/{$class}.klaso.php",
            __DIR__ . "/interfaces/{$class}.php"
        ];
        if(strpos($class, 'Oppslag/') === 0) {
            $possiblePaths = [
                __DIR__ . '/../' . lcfirst($class) . '.php'
            ];
        }

        $attempts = [];

        foreach($possiblePaths as $path) {
            if(file_exists($path)) {
                require $path;
                return true;
            }
            else {
                $attempts[] = $path;
            }
        }

        return false;
    });

    if(file_exists(__DIR__ . '/../vendor/autoload.php')) {
        require_once(__DIR__ . '/../vendor/autoload.php');
    }

	require_once('Leiebase.php');

	require_once('DatabaseObjekt.class.php');

    require_once('Innbetaling.class.php');
	require_once('Person.class.php');
	require_once('Leieobjekt.class.php');
	require_once('Leieforhold.class.php');
	require_once('Krav.class.php');
	require_once('Giro.class.php');
}

else {
	throw new Exception("Illegal access to directory");
}
