<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 12/06/2020
 * Time: 14:02
 */

namespace Kyegil\Leiebasen;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\CoreModel\Interfaces\CoreModelCollectionInterface;
use Kyegil\MysqliConnection\MysqliConnectionInterface;
use mysqli;

/**
 * Class Sett
 * @package Kyegil\Leiebasen
 *
 * @property MysqliConnectionInterface $mysqli
 * @method Modell current()
 */
class Sett extends \Kyegil\CoreModel\CoreModelCollection implements \Kyegil\Leiebasen\Samling
{
    use Samlingstrekk;

    /** @var CoreModelImplementering */
    protected $app;

    public function __construct(string $model, array $di)
    {
        if(!is_a($di[mysqli::class], MysqliConnectionInterface::class)) {
            throw new CoreModelDiException("Required instance of " . mysqli::class . " has not been injected to " . static::class . " constructor");
        }
        parent::__construct($model, $di);
    }

    /**
     * Inkluder et annet sett basert på én-til-mange-forholdet
     *
     * @param string $referanse
     * @param Sett|null $samling
     * @param string|null $modell
     * @param string|null $fremmedNøkkel
     * @param array $filtre
     * @param callable|null $callback
     * @return CoreModelCollectionInterface|null
     * @throws \Exception
     */
    public function inkluder(
        string    $referanse,
        ?Sett     $samling = null,
        ?string   $modell = null,
        ?string   $fremmedNøkkel = null,
        array     $filtre = [],
        ?callable $callback = null
    ): ?CoreModelCollectionInterface {
        return $this->linkCollection($referanse, $samling, $modell, $fremmedNøkkel, $filtre, $callback);
    }
}