<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/12/2022
 * Time: 12:22
 */

namespace Kyegil\Leiebasen\Epostprosessor;

use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Melding;
use Kyegil\Leiebasen\Modell\Meldingsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Personsett;

abstract class AbstraktProsessor
{
    /** @var Leiebase */
    protected $app;

    /**
     * @param Leiebase $leiebase
     * @return AbstraktProsessor
     */
    abstract public static function getInstance(Leiebase $leiebase):AbstraktProsessor;

    /**
     * @param array{
     *     type: string,
     *     to: string,
     *     cc: string,
     *     bcc: string,
     *     from: string,
     *     auto: bool,
     *     testcopy: bool ,
     *     reply: string,
     *     subject: string,
     *     html: string,
     *     text: string,
     *     attachment: string,
     *     priority: int
     * } $params
     * @return Melding
     */
    abstract public function forberedEpost(array $params): Melding;

    /**
     * @return int|null
     */
    abstract public function hentPartiBegrensning():?int;

    /**
     * @param Melding $epost
     * @return AbstraktProsessor
     */
    abstract public function send(Melding $epost):AbstraktProsessor;

    /**
     * Alle adresser formateres som et assosiativt array med e-post som nøkkel og navn som verdi
     *
     * @param array $config
     * @return array
     */
    protected function forberedAdresser(array $config): array {
        foreach(['to', 'cc', 'bcc'] as $adressefelt) {
            $formatert = [];
            if(isset($config[$adressefelt])) {
                if ($config[$adressefelt] instanceof Personsett) {
                    $config[$adressefelt] = $config[$adressefelt]->hentElementer();
                }
                if (is_string($config[$adressefelt])) {
                    $config[$adressefelt] = explode(',', $config[$adressefelt]);
                }
                foreach ($config[$adressefelt] as $index => $mottaker) {
                    $mottaker = $this->formaterAdresse($mottaker, $index);
                    if ($mottaker) {
                        $formatert = array_merge($formatert, $mottaker);
                    }
                }
            }
            $config[$adressefelt] = $formatert;
        }

        $config['reply'] = $this->formaterAdresse($config['reply'] ?? null);

        if (empty($config['from'])) {
            $config['from'][$this->app->hentValg('autoavsender')] = $this->app->hentValg('utleier');
        }
        else {
            $config['from'] = $this->formaterAdresse($config['from']);
        }
        return $config;
    }

    /**
     * Formaterer parametere
     *
     * @param array $params
     * @return array
     */
    public function forberedParams(array $params): array {
        $params = $this->forberedAdresser($params);
        $params['auto'] = $params['auto'] ?? true;

        settype($params['subject'], 'string');
        settype($params['html'], 'string');
        settype($params['text'], 'string');

        settype($params['priority'], 'integer');
        settype($params['auto'], 'boolean');

        $developerEmail = trim(Leiebase::$config['leiebasen']['debug']['developer_email'] ?? '');
        $params['testcopy'] = $params['testcopy'] && $developerEmail;
        if($params['testcopy']) {
            $params['bcc'][$developerEmail] = null;
        }

        if ($params['html'] && !$params['text']) {
            $params['text'] = html_entity_decode(strip_tags(str_ireplace(
                ['&nbsp;', '<br>', '<br>', '<br>'],
                [' ', "\r\n", "\r\n", "\r\n"],
                $params['html']
            )));
        }

        $params['attachment'] = $params['attachment'] ?? [];
        if(is_string($params['attachment'])) {
            $params['attachment'] = trim($params['attachment']) ? explode(',', $params['attachment']) : [];
        }
        settype($params['attachment'], 'array');
        return $params;
    }

    /**
     * Sender en batch e-poster ifra køen
     *
     * @return AbstraktProsessor
     * @throws \Exception
     */
    public function leverEpost(): AbstraktProsessor
    {
        /** @var Meldingsett $epostsett */
        $epostsett = $this->app->hentSamling(Melding::class);
        $epostsett->leggTilFilter(['medium' => \Kyegil\Leiebasen\Modell\Melding::MEDIUM_EPOST])->låsFiltre();
        $epostsett->leggTilSortering('id')->leggTilSortering('prioritet', true);
        $epostsett->begrens($this->hentPartiBegrensning());

        foreach($epostsett as $epost) {
            if($this->hentPartiBegrensning() > 0) {
                try {
                    $this->send($epost);
                } catch (\Throwable $e) {
                    $this->app->logger->critical($e);
                }
            }
        }
        return $this;
    }

    /**
     * Alle adresser formateres som et assosiativt array med e-post som nøkkel og navn som verdi
     *
     * @param $mottaker
     * @param string $index
     * @return array|string
     */
    protected function formaterAdresse($mottaker, string $index = ''): array
    {
        if($mottaker instanceof Leietaker) {
            $mottaker = $mottaker->person;
        }
        if (!$mottaker) {
            return [];
        }
        if($mottaker instanceof \Person || $mottaker instanceof Person) {
            $epost = trim($mottaker->epost);
            $navn = trim($mottaker->navn);
            if ($epost) {
                return [$epost => $navn];
            }
            else {
                return [];
            }
        }
        settype($mottaker, 'string');
        if (strpos($mottaker, '@') === false) {
            if (strpos(trim($index), '@')) {
                return [$index => $mottaker];
            }
            else {
                return [];
            }
        }
        if (preg_match('/(.*)<(.*)>/', $mottaker, $adressedeler)) {
            return [trim($adressedeler[2]) => trim($adressedeler[1])];
        }
        return [$mottaker => null];
    }
}