<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/12/2022
 * Time: 12:22
 */

namespace Kyegil\Leiebasen\Epostprosessor;

use Exception;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Melding;

class PhpMailer extends AbstraktProsessor
{
    /** @var PhpMailer */
    private static PhpMailer $instance;
    /** @var Leiebase */
    protected $app;

    /**
     * @param Leiebase $leiebase
     * @return PhpMailer
     */
    public static function getInstance(Leiebase $leiebase): PhpMailer
    {
        if ( !isset( self::$instance ) )
        {
            self::$instance = new self($leiebase);
        }
        return self::$instance;
    }

    /**
     *
     */
    private function __construct(Leiebase $app)
    {
        $this->app = $app;
    }

    /**
     * @param array $params
     * @return Melding
     * @throws Exception
     */
    public function forberedEpost(array $params): Melding
    {
        $params = $this->forberedParams($params);
        $mail = new \PHPMailer\PHPMailer\PHPMailer(true);
        $mail->CharSet = 'UTF-8';
        $mail->addCustomHeader('Auto-Submitted', $params['auto'] ? 'auto-generated' : 'no');
        $mail->Subject = $params['subject'];


        foreach($params['to'] as $epost => $mottaker) {
            $mail->addAddress($epost, strval($mottaker));
        }
        foreach($params['from'] as $epost => $mottaker) {
            $mail->setFrom($epost, strval($mottaker));
        }
        foreach($params['reply'] as $epost => $mottaker) {
            $mail->addReplyTo($epost, strval($mottaker));
        }
        foreach($params['cc'] as $epost => $mottaker) {
            $mail->addCC($epost, strval($mottaker));
        }
        foreach($params['bcc'] as $epost => $mottaker) {
            $mail->addBCC($epost, strval($mottaker));
        }
        foreach($params['attachment'] as $attachment) {
            $mail->addAttachment($attachment);
        }

        if ($params['html']) {
            $mail->Body = $params['html'];
            $mail->AltBody = $params['text'];
            $mail->isHTML();
        } else {
            $mail->Body = $params['text'];
            $mail->isHTML(false);
        }
        $mail->preSend();

        /** @var Melding $epost */
        $epost = $this->app->nyModell(Melding::class, (object)[
            'til' => $this->skrivAdresser($params['to']),
            'emne' => $params['subject'],
            'innhold' => serialize($mail),
            'prioritet' => $params['priority'],
        ]);
        return $epost;

    }

    public function hentPartiBegrensning(): ?int
    {
        return 4;
    }

    /**
     * @param Melding $epost
     * @return PhpMailer
     * @throws Exception
     */
    public function send(Melding $epost): AbstraktProsessor
    {
        if ($epost->headers) {
            return Php::getInstance($this->app)->send($epost);
        }
        /** @var \PHPMailer\PHPMailer\PHPMailer|false $phpMailerObject */
        $phpMailerObject = unserialize($epost->innhold, ['allowed_classes' => [\PHPMailer\PHPMailer\PHPMailer::class]]);
        /** @noinspection PhpRedundantOptionalArgumentInspection */
        $til = $epost->til ? wordwrap($epost->til, 70, "\r\n") : null;
        $emne = $epost->emne ?: '';

        if(!$phpMailerObject) {
            $epost->slett();
            throw new Exception('Kunne ikke lese epost', ['til' => $til, 'emne' => $emne]);
        }

        if ($this->app->live) {
            $phpMailerObject->postSend();
        }
        $epost->slett();
        return $this;
    }

    /**
     * @param array $adresser
     * @return string
     */
    private function skrivAdresser(array $adresser): string
    {
        $resultat = [];
        foreach ($adresser as $epost => $mottaker) {
            $resultat[] = $mottaker ? "{$mottaker} <{$epost}>" : $epost;
        }
        return $resultat ? implode(',', $resultat) : '';
    }
}