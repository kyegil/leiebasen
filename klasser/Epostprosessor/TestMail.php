<?php

namespace Kyegil\Leiebasen\Epostprosessor;

use Exception;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Melding;

class TestMail extends Php
{
    /** @var TestMail */
    private static TestMail $instance;
    /** @var Leiebase */
    protected $app;

    /**
     * @param Leiebase $leiebase
     * @return TestMail
     */
    public static function getInstance(Leiebase $leiebase): TestMail
    {
        if ( !isset( self::$instance ) )
        {
            self::$instance = new self($leiebase);
        }
        return self::$instance;
    }

    /**
     *
     * @noinspection PhpMissingParentConstructorInspection
     */
    private function __construct(Leiebase $app)
    {
        $this->app = $app;
    }

    /**
     * @param array $params
     * @return Melding
     * @throws Exception
     */
    public function forberedEpost(array $params): Melding
    {
        $params = $this->forberedParams($params);
        $multipartHash = md5(date('r'));
        $developerEmail = Leiebase::$config['leiebasen']['debug']['developer_email'] ?? null;
        $headers = [
            'Auto-Submitted' => $params['auto'] ? 'auto-generated' : 'no',
            'From' => $this->skrivAdresser([$developerEmail=>$developerEmail]),
            'X-Mailer' => 'PHP/' . phpversion(),
            'MIME-Version' => '1.0',
        ];
        $developerEmail = Leiebase::$config['leiebasen']['debug']['developer_email'] ?? null;
        $to = array_filter($params['to'], function ($email) use ($developerEmail) {
            return trim($email) == trim($developerEmail);
        }, ARRAY_FILTER_USE_KEY);
        $cc = array_filter($params['cc'], function ($email) use ($developerEmail) {
            return trim($email) == trim($developerEmail);
        }, ARRAY_FILTER_USE_KEY);
        $bcc = array_filter($params['bcc'], function ($email) use ($developerEmail) {
            return trim($email) == trim($developerEmail);
        }, ARRAY_FILTER_USE_KEY);
        $to = array_merge($to, $cc, $bcc);
        $to = $this->skrivAdresser($to);
        $html = trim($params['html']);
        if ($html) {
            /** @noinspection SpellCheckingInspection */
            $headers['Content-Type'] = 'multipart/alternative; boundary="Leiebasemail-alt-' . $multipartHash . '"';
        }
        else {
            $headers['Content-Type'] = 'text/plain; charset=UTF-8';
        }

        if ($html) {
            /** @noinspection SpellCheckingInspection */
            $innhold = "--Leiebasemail-alt-$multipartHash\r\nContent-type: text/plain; charset=UTF-8\r\nContent-Transfer-Encoding: 8bit\r\n\r\n{$params['text']}\n" . "--Leiebasemail-alt-$multipartHash\r\nContent-type: text/html; charset=UTF-8\r\nContent-Transfer-Encoding: 8bit\r\n\r\n{$html}\n--Leiebasemail-alt-$multipartHash--";
        }
        else {
            $innhold = $params['text'];
        }

        /** @var Melding $epost */
        $epost = $this->app->nyModell(Melding::class, (object)[
            'til' => $to,
            'emne' => $params['subject'],
            'innhold' => $innhold,
            'headers' => $headers,
            'prioritet' => $params['priority'],
            'params' => $params['param'] ?? null
        ]);
        return $epost;
    }

    /**
     * @param Melding $epost
     * @return TestMail
     * @throws Exception
     */
    public function send(Melding $epost): TestMail
    {
        /** @var string $installationUrl */
        $installationUrl = Leiebase::$config['leiebasen']['server']['installation_url'] ?? '';
        /** @noinspection PhpRedundantOptionalArgumentInspection */
        $til = $epost->til ? wordwrap($epost->til, 70, "\r\n") : null;
        $emne = 'Test ' . $installationUrl;
        $emne .= $this->app->live ? ' LIVE ' : ' DEV ';
        $emne .= $epost->emne ?: '';
        $headers = (array)$epost->headers;
        /** @noinspection PhpRedundantOptionalArgumentInspection */
        $innhold = wordwrap($epost->innhold, 70, "\r\n");
        $params = $epost->params ?? '';
        if(trim($til)) {
            mail($til, $emne, $innhold, $headers, $params);
        }
        $epost->slett();
        return $this;
    }

    /**
     * @param array $adresser
     * @return string
     */
    private function skrivAdresser(array $adresser): string
    {
        $resultat = [];
        $developerEmail = Leiebase::$config['leiebasen']['debug']['developer_email'] ?? null;
        foreach ($adresser as $epost => $mottaker) {
            if($developerEmail && $epost == $developerEmail) {
                $resultat[] = $epost;
            }
        }
        return $resultat ? implode(',', $resultat) : '';
    }
}