<?php

/**
 * Class Leieforhold
 * @deprecated
 * @see \Kyegil\Leiebasen\Modell\Leieforhold
 */
class Leieforhold extends DatabaseObjekt {

    /**
     * @var string
     */
    protected    $tabell = "kontrakter";    // Hvilken tabell i databasen som inneholder primærnøkkelen for dette objektet
    /**
     * @var string
     */
    protected    $idFelt = "leieforhold";    // Hvilket felt i tabellen som lagrer primærnøkkelen for dette objektet
    /**
     * @var
     */
    protected    $data;            // DB-verdiene lagret som et objekt. Null om de ikke er lastet

    /**
     * @var stdClass[]
     */
    protected    $delkravtyper;    // Alle delkravtyper som hører til leieforholdet. Null om de ikke er lastet

//    Efaktura og fbo bor ikke mellomlagres, fordi betalingstype skal kunne angis ved lasting
// protected    $efakturaavtale; // Evt efakturaavtale registrert på leieforholdet
// protected    $fbo;             // Evt faste betalingsoppdrag (AvtaleGiro) registrert på leieforholdet
    /**
     * @var
     */
    protected    $leietakere;    // Liste over leietakeren(e) som inngår i leieavtalen
    /**
     * @var array
     */
    protected    $slettedeLeietakere = array();    // Liste over leietakere som er slettet fra leieavtalen
    /**
     * @var
     */
    protected    $leietakerfelt;    // Streng som lister alle leietakerne med fødsels- og personnummer
    /**
     * @var
     */
    protected    $innbetalinger;    // Liste med StdClass-objekter med innbetalingsobjekt, DateTime-objekt og delbeløp
    /**
     * @var
     */
    protected    $krav;            // Alle betalingskrav i leieforholdet
    /**
     * @var
     */
    protected    $leiekrav;        // Alle husleiekrav som er opprettet i leieavtalen
    /**
     * @var
     */
    protected    $navn;            // Navn på leietakeren(e) som inngår i leieavtalen
    /**
     * @var
     */
    protected    $kortnavn;        // Forkortet navn på leietakeren(e)
    /**
     * @var
     */
    protected    $adresse;        // stdClass-objekt med adresseelementene
    /**
     * @var
     */
    protected    $adressefelt;    // Adressefelt for utskrift
    /**
     * @var
     */
    protected    $brukerepost;    // Liste over brukerepostadresser
    /**
     * @var
     */
    protected    $oppsigelse;    // stdClass-objekt med oppsigelse. False dersom ikke oppsagt, null dersom ikke lastet
    /**
     * @var
     */
    protected    $fremtidigeKrav;    // Array over alle registrerte fremtidige krav. Null om de ikke er lastet
    /**
     * @var
     */
    protected    $ubetalteKrav;    // Array over alle ubetalte krav. Null om de ikke er lastet
    /**
     * @var array
     */
    protected    $utskriftsposisjon = array();    // Utskriftsposisjonen for hver enkelt rute, sortert som et array med rutenummeret som nøkkel
    /**
     * @var
     */
    public        $id;                // Unikt id-heltall for dette objektet

    private $fellestrømDelkrav;


    /**
     * Leieforhold constructor.
     * @param array|object|null $param
     *  ->id    (heltall) Leieforholdnummeret
     * @throws Exception
     */
    public function __construct($param = null ) {
        parent::__construct( $param );
    }


    /**
     * Last leieforholdets kjernedata fra databasen
     *
     * @param int $id leieforholdnummmeret
     * @return bool|mixed
     * @throws Exception
     */
    protected function last($id = 0) {
        $tp = $this->mysqli->table_prefix;

        settype($id, 'integer');
        if( !$id ) {
            $id = $this->id;
        }

        $resultat = $this->mysqli->arrayData(array(
            'returnQuery'    => true,

            'fields' =>        "{$this->tabell}.leieforhold AS id,\n"
                        .    "{$this->tabell}.leieforhold,\n"
                        .    "{$this->tabell}.kontraktnr,\n"
                        .    "{$this->tabell}.tekst,\n"
                        .    "{$this->tabell}.leieobjekt,\n"
                        .    "{$this->tabell}.fradato,\n"
                        .    "{$this->tabell}.tildato,\n"
                        .    "{$this->tabell}.oppsigelsestid,\n"
                        .    "{$this->tabell}.årlig_basisleie,\n"
                        .    "{$this->tabell}.ant_terminer,\n"
                        .    "{$this->tabell}.frosset,\n"
                        .    "{$this->tabell}.stopp_oppfølging,\n"
                        .    "{$this->tabell}.avvent_oppfølging,\n"
                        .    "{$this->tabell}.regningsperson,\n"
                        .    "{$this->tabell}.regning_til_objekt,\n"
                        .    "{$this->tabell}.regningsobjekt,\n"
                        .    "{$this->tabell}.regningsadresse1,\n"
                        .    "{$this->tabell}.regningsadresse2,\n"
                        .    "{$this->tabell}.postnr,\n"
                        .    "{$this->tabell}.poststed,\n"
                        .    "{$this->tabell}.land,\n"
                        .    "{$this->tabell}.giroformat\n",

            'source' =>         "{$tp}{$this->tabell} AS {$this->tabell}\n",

            'where'            =>    "{$tp}{$this->tabell}.{$this->idFelt} = '$id'",

            'orderfields'    =>    "{$this->tabell}.kontraktnr DESC"
        ));
        if( $resultat->totalRows ) {
            $this->data = $resultat->data[0];
            $this->id = $id;

            foreach($resultat->data as $kontrakt) {
                $this->data->kontrakter[$kontrakt->kontraktnr] = (object)array(
                    'dato'            => new DateTime($kontrakt->fradato),
                    'tildato'        => ($kontrakt->tildato ? new DateTime($kontrakt->tildato) : null),
                    'kontraktnr'    => $kontrakt->kontraktnr,
                    'tekst'            => $kontrakt->tekst
                );
            }

            $this->data->fradato = new DateTime( end($resultat->data)->fradato );

            $this->data->leieobjekt = $this->leiebase->hent(\Leieobjekt::class, $this->data->leieobjekt );

            $this->data->oppsigelsestid = new DateInterval( $this->data->oppsigelsestid );

            if( $this->data->regning_til_objekt ) {
                $this->data->regningsobjekt = $this->leiebase->hent(\Leieobjekt::class, $this->data->regningsobjekt );
            }
            else {
                $this->data->regningsobjekt = null;
            }

            if( $this->data->regningsperson ) {
                $this->data->regningsperson = $this->leiebase->hent(\Person::class, $this->data->regningsperson );
            }
            else {
                $this->data->regningsperson = null;
            }

            if( $this->data->tildato ) {
                $this->data->tildato = new DateTime( $this->data->tildato );
            }
            else {
                $this->data->tildato = null;
            }
            if( $this->data->avvent_oppfølging ) {
                $this->data->avvent_oppfølging
                    = new DateTime( $this->data->avvent_oppfølging );
            }

            if(!$this->data->ant_terminer) {
                $this->data->ant_terminer = 1;
            }
            switch($this->data->ant_terminer) {
            case 1:
                $this->data->terminlengde = new DateInterval("P1Y");
                break;
            case 2:
            case 3:
            case 4:
            case 6:
            case 12:
                $this->data->terminlengde = new DateInterval("P" . (12 / $this->data->ant_terminer) . "M");
                break;
            case 13:
            case 26:
            case 52:
                $this->data->terminlengde = new DateInterval("P" . (364 / $this->data->ant_terminer) . "D");
            default:
                $this->data->terminlengde = new DateInterval("P" . round(365 / $this->data->ant_terminer) . "D");
            }

            return true;
        }
        else {
            $this->id = null;
            $this->data = null;
            return false;
        }

    }



    /**
     * Last adressefeltet
     *
     * @throws Exception
     */
    protected function lastAdressefelt() {
        // Grunndetaljene, som angir adressealternativene, må lastes først
        if ( $this->data === null ) {
            $this->last();
        }
        // Leietakerne må også lastes
        if ( $this->leietakere === null ) {
            $this->lastLeietakere();
        }

        // Opprett adresseobjektet
        $this->adresse = (object)array(
            'navn'        => null,
            'adresse1'    => null,
            'adresse2'    => null,
            'postnr'    => null,
            'poststed'    => null,
            'land'        => null
        );

        $adressefelt = "";

        if( $this->data->regning_til_objekt ) {
            /** @var \Leieobjekt $leieobjekt */
            $leieobjekt = $this->hent('leieobjekt');

            if( $leieobjekt->hent('navn') ) {
                $this->adresse->adresse1 = $leieobjekt->hent('navn');
                $this->adresse->adresse2 = $leieobjekt->hent('gateadresse');
            }

            $this->adresse->navn = $this->hent('navn');
            $this->adresse->postnr = $leieobjekt->hent('postnr');
            $this->adresse->poststed = $leieobjekt->hent('poststed');
            $this->adresse->land = "";
            $adressefelt = $leieobjekt->hent('adresse');
        }

        else if( $this->data->regningsperson ) {
            $this->adresse->navn = $this->data->regningsperson->hent('navn');
            $this->adresse->adresse1 = $this->data->regningsperson->hent('adresse1');
            $this->adresse->adresse2 = $this->data->regningsperson->hent('adresse2');
            $this->adresse->postnr = $this->data->regningsperson->hent('postnr');
            $this->adresse->poststed = $this->data->regningsperson->hent('poststed');
            $this->adresse->land = $this->data->regningsperson->hent('land');
            $adressefelt = $this->data->regningsperson->hent('postadresse');
        }

        else if( !$this->data->regningsperson ) {
            $this->adresse->navn = $this->hent('navn');
            $this->adresse->adresse1 = $this->data->regningsadresse1;
            $this->adresse->adresse2 = $this->data->regningsadresse2;
            $this->adresse->postnr = $this->data->postnr;
            $this->adresse->poststed = $this->data->poststed;
            $this->adresse->land = $this->data->land;
            $adressefelt .= (
                $this->data->regningsadresse1
                ? "{$this->data->regningsadresse1}\n"
                : ""
            );
            $adressefelt .= (
                $this->data->regningsadresse2
                ? "{$this->data->regningsadresse2}\n"
                : ""
            );
            $adressefelt .= "{$this->data->postnr} {$this->data->poststed}\n";
            $adressefelt .= (
                ($this->data->land != "Norge" && $this->data->land != "")
                ? "{$this->data->land}"
                : ""
            );
        }

        else {
            $regningsperson = $this->leietakere[ $this->data->regningsperson ];
            $this->adresse->navn =    $regningsperson->hent('navn');
            $this->adresse->adresse1 = $regningsperson->hent('adresse1');
            $this->adresse->adresse2 = $regningsperson->hent('adresse2');
            $this->adresse->postnr = $regningsperson->hent('postnr');
            $this->adresse->poststed = $regningsperson->hent('poststed');
            $this->adresse->land = $regningsperson->hent('land');
            $adressefelt .= $regningsperson->hent('postadresse');
        }

        $this->adressefelt = $adressefelt;


    }



    /**
     * Last alle betalinger i leieforholdet
     *
     * @throws Exception
     */
    protected function lastBetalinger() {
        $tp = $this->mysqli->table_prefix;
        $resultat = $this->mysqli->arrayData(array(
            'source'        => "{$tp}innbetalinger AS innbetalinger\n",
            'distinct'        => true,
            'fields'        => "innbetalinger.innbetaling, innbetalinger.dato, SUM(innbetalinger.beløp) AS beløp, innbetalinger.ref",
            'orderfields'    => "innbetalinger.dato, innbetalinger.ref",
            'groupfields'    => "innbetalinger.innbetaling, innbetalinger.dato, innbetalinger.ref",
            'where'            => "innbetalinger.konto != '0'\n"
                            .    "AND innbetalinger.leieforhold = '{$this->hentId()}'"
        ));

        foreach($resultat->data as $delbeløp) {
            $delbeløp->innbetaling = $this->leiebase->hent(\Innbetaling::class, $delbeløp->innbetaling);
            $delbeløp->dato = new DateTime("{$delbeløp->dato} 00:00:00");
        }

        $this->innbetalinger = $resultat->data;
    }


    /**
     * Last epostadressen for alle brukerne som har adgang til leieforholdet
     *
     * @throws Exception
     */
    protected function lastBrukerepost() {
        $tp = $this->mysqli->table_prefix;
        $this->brukerepost = (object)array(
            'generelt' => array(),
            'innbetalingsbekreftelse' => array(),
            'forfallsvarsel' => array()
        );

        $resultat = $this->mysqli->arrayData(array(
            'source'    => "{$tp}adganger AS adganger INNER JOIN {$tp}personer AS personer ON adganger.personid = personer.personid",
            'where'        => "adganger.leieforhold = '{$this->hentId()}' AND epostvarsling AND adgang = 'mine-sider'",
            'fields'    => "adganger.innbetalingsbekreftelse,
                            adganger.forfallsvarsel,
                            personer.fornavn,
                            personer.etternavn,
                            personer.er_org,
                            personer.epost"
        ));
        foreach( $resultat->data as $person ) {
            $adresse = ($person->er_org ? $person->etternavn : "{$person->fornavn} {$person->etternavn}" ) . " <{$person->epost}>";
            $this->brukerepost->generelt[] = $adresse;
            if( $person->innbetalingsbekreftelse ) {
                $this->brukerepost->innbetalingsbekreftelse[] = $adresse;
            }
            if( $person->forfallsvarsel ) {
                $this->brukerepost->forfallsvarsel[] = $adresse;
            }
        }
    }



    /**
     * Last alle delkravtyper som er finnes på leieforholdet
     *
     * @throws Exception
     */
    protected function lastDelkravtyper() {
        $tp = $this->mysqli->table_prefix;
        $this->delkravtyper = $this->mysqli->arrayData(array(
            'returnQuery'    => true,
            'source'    => "{$tp}delkravtyper AS delkravtyper\n"
                        .    "INNER JOIN {$tp}leieforhold_delkrav AS leieforhold_delkrav ON delkravtyper.id = leieforhold_delkrav.delkravtype",
            'fields'    => "delkravtyper.id AS id,\n"
                        .    "delkravtyper.navn AS navn,\n"
                        .    "delkravtyper.kode AS kode,\n"
                        .    "delkravtyper.kravtype AS kravtype,\n"
                        .    "leieforhold_delkrav.selvstendig_tillegg AS selvstendig_tillegg,\n"
                        .    "leieforhold_delkrav.periodisk_avstemming AS periodisk_avstemming,\n"
                        .    "delkravtyper.beskrivelse AS beskrivelse,\n"
                        .    "leieforhold_delkrav.relativ AS relativ,\n"
                        .    "leieforhold_delkrav.sats AS sats,\n"
                        .    "ROUND(IF(leieforhold_delkrav.relativ, (leieforhold_delkrav.sats * '" . $this->hent('årlig_basisleie') . "'), (leieforhold_delkrav.sats))) AS beløp\n",
            'where'        => "leieforhold_delkrav.leieforhold = '{$this->hentId()}'"
        ))->data;
    }


    /**
     * Last evt betalingskrav i leieforholdet
     *
     * @throws Exception
     */
    protected function lastKrav() {
        $tp = $this->mysqli->table_prefix;
        $resultat = $this->mysqli->arrayData(array(
            'source'        => "{$tp}krav AS krav\n"
                            .    "INNER JOIN {$tp}kontrakter AS kontrakter ON kontrakter.kontraktnr = krav.kontraktnr",
            'fields'        => "krav.id AS id",
            'orderfields'    => "krav.kravdato, krav.id",
            'class'            => \Krav::class,
            'where'            => "kontrakter.leieforhold = '{$this->hentId()}'"
        ));

        $this->krav = $resultat->data;
        $this->leiekrav = array();

        foreach($this->krav as $krav) {
            if($krav->hent('type') == "Husleie") {
                $this->leiekrav[] = $krav;
            }
        }
    }


    /**
     * Last leietakerne
     *
     * @return bool|void
     * @throws Exception
     */
    protected function lastLeietakere() {
        $tp = $this->mysqli->table_prefix;

        if ( !$this->id ) {
            $this->navn = null;
            return false;
        }

        $leietakere = $this->mysqli->arrayData(array(
            'returnQuery'    => true,

            'class'    =>            \Person::class,
            'fields' =>            "kontraktpersoner.person AS id, kontraktpersoner.slettet, kontraktpersoner.leietaker\n",
            'orderfields' =>    "kontraktpersoner.kopling ASC\n",
            'source' =>         "{$tp}kontraktpersoner AS kontraktpersoner\n",
            'where'            =>    "kontraktpersoner.kontrakt = '{$this->hent('kontraktnr')}'"
        ));

        $navn = array();
        $this->leietakere = array();
        $this->slettedeLeietakere = array();
        $this->leietakerfelt = "";
        /** @var \Person $person */
        foreach( $leietakere->data as $person ) {

            // Dersom personen fortsatt er leietaker og har adressekort
            if ( !$person->slettet && ($personid = $person->hent('id')) ) {
                $this->leietakere[ $personid ] = $person;

                $navn[]    = $person->hent('navn');

                $fødselsnr = $person->hent('fødselsnummer');
                $fødselsdato = $person->hent('fødselsdato');
                $this->leietakerfelt .= $person->hent('navn')
                .    (
                        $fødselsnr
                        ? " f.nr.&nbsp;{$fødselsnr}"
                        : (
                            $fødselsdato
                            ? " f.&nbsp;{$fødselsdato->format('d.m.Y')}"
                            : ""
                        )
                    )
                . "<br>\n";
            }

            // Dersom personen fortsatt er leietaker, men mangler adressekort
            else if ( !$person->slettet ) {
                $navn[]    = $person->leietaker;
                $this->leietakerfelt .= "{$person->leietaker}<br>\n";
            }

            // Dersom personen er slettet som leietaker, og har adressekort
            else if ( $person->slettet  && ($person->hent('id'))) {
                $this->slettedeLeietakere[] = $person;

                $this->leietakerfelt .= "<del>";
                $this->leietakerfelt .= $person->hent('navn')
                . "</del> Slettet&nbsp;" . date('d.m.Y', strtotime( $person->slettet )) . "<br>\n";
            }

            // Dersom personen er slettet som leietaker, og mangler adressekort
            else {
                $this->leietakerfelt .= "<del>{$person->leietaker}</del><br>\n";
            }
        }

        $this->navn = "";
        $this->kortnavn = "";
        $ant = count( $navn );

        foreach( $navn as $nr => $verdi ) {
            $this->navn .= $verdi;
            if($nr < $ant-2) $this->navn .= ", ";
            if($nr == $ant-2) $this->navn .= " og ";

            $this->kortnavn .= mb_substr($verdi, 0, (int)((11-$ant) / $ant), 'UTF-8');
            if($nr <= $ant-2) {
                $this->kortnavn .= "&";
            }
        }
    }


    /**
     * Laster evt. oppsigelse
     *
     * @return bool
     * @throws Exception
     */
    protected function lastOppsigelse() {
        $tp = $this->mysqli->table_prefix;
        if ( !$id = $this->id ) {
            $this->oppsigelse = false;
            return false;
        }

        $resultat = $this->mysqli->arrayData(array(
            'returnQuery'    => true,
            'fields'        =>    "oppsigelsesdato, fristillelsesdato, oppsigelsestid_slutt, ref, merknad, oppsagt_av_utleier\n",
            'source'        =>     "{$tp}oppsigelser AS oppsigelser\n",
            'where'            =>    "{$tp}oppsigelser.{$this->idFelt} = '$id'"
        ));

        if( $resultat->totalRows ) {
            $this->oppsigelse = $resultat->data[0];
            $this->oppsigelse->oppsigelsesdato = new DateTime( $this->oppsigelse->oppsigelsesdato . " 00:00:00" );
            $this->oppsigelse->fristillelsesdato = new DateTime( $this->oppsigelse->fristillelsesdato . " 00:00:00" );
            $this->oppsigelse->oppsigelsestidSlutt = new DateTime( $this->oppsigelse->oppsigelsestid_slutt . " 00:00:00" );
            $this->oppsigelse->oppsagtAvUtleier = (bool)$this->oppsigelse->oppsagt_av_utleier;
        }

        else {
            $this->oppsigelse = false;
        }
        return true;
    }


    /**
     * Last alle framtidige krav fra databasen
     *
     * @return bool|void
     * @throws Exception
     */
    protected function lastFremtidigeKrav() {
        $tp = $this->mysqli->table_prefix;
        if ( !$id = $this->id ) {
            $this->fremtidigeKrav = null;
            return false;
        }

        $resultat = $this->mysqli->arrayData(array(
            'returnQuery'    => true,
            'class'         =>    \Krav::class,
            'fields'        =>    "krav.id\n",
            'orderfields'    =>    "krav.kravdato, krav.id",
            'source'        =>     "{$tp}krav AS krav INNER JOIN {$tp}{$this->tabell} AS {$this->tabell} ON krav.kontraktnr = {$this->tabell}.kontraktnr\n",
            'where'            =>    "{$this->tabell}.{$this->idFelt} = '$id'
                                AND krav.kravdato > NOW()"
        ));

        $this->fremtidigeKrav = $resultat->data;

    }


    /**
     * Last alle ubetalte krav fra databasen
     *
     * @return bool|void
     * @throws Exception
     */
    protected function lastUbetalteKrav() {
        $tp = $this->mysqli->table_prefix;
        if ( !$id = $this->id ) {
            $this->ubetalteKrav = null;
            return false;
        }

        $resultat = $this->mysqli->arrayData(array(
            'returnQuery'    => true,
            'class'         =>    \Krav::class,
            'fields'        =>    "krav.id\n",
            'orderfields'    =>    "krav.kravdato, krav.id",
            'source'        =>     "{$tp}krav AS krav INNER JOIN {$tp}{$this->tabell} AS {$this->tabell} ON krav.kontraktnr = {$this->tabell}.kontraktnr\n",
            'where'            =>    "{$this->tabell}.{$this->idFelt} = '$id'
                                AND krav.utestående > 0
                                AND krav.kravdato <= NOW()"
        ));

        $this->ubetalteKrav = $resultat->data;

    }

    /**
     * Oppdater terminleiebeløpet i databasen.
     *
     * Denne operasjonen må kjøres hver gang leiebeløpet, antall terminer eller
     * delkravene har blitt endret.
     *
     * @return mixed
     * @throws Exception
     */
    protected function oppdaterLeie() {
        $tp = $this->mysqli->table_prefix;
        $this->nullstill();

        $årsleie = $this->hent('årlig_basisleie');

        foreach($this->hent('delkravtyper') AS $delkravtype) {
            if($delkravtype->kravtype == "Husleie" and !$delkravtype->selvstendig_tillegg) {
                $årsleie += $delkravtype->beløp;
            }
        }
        $this->mysqli->saveToDb(array(
            'update'    => true,
            'table'        => "{$tp}leieforhold as leieforhold",
            'where'        => "leieforhold.leieforholdnr = '{$this->id}'",
            'fields'    => array(
                "leieforhold.leiebeløp"    => round(bcdiv($årsleie, $this->hent('ant_terminer'), 1))
            )
        ));
        return $this->mysqli->saveToDb(array(
            'update'    => true,
            'table'        => "{$tp}{$this->tabell} as {$this->tabell}",
            'where'        => "{$this->tabell}.{$this->idFelt} = '{$this->id}'",
            'fields'    => array(
                "{$this->tabell}.leiebeløp"    => round(bcdiv($årsleie, $this->hent('ant_terminer'), 1))
            )
        ))->success;
    }


    /**
     * Hent egenskaper
     *
     * @param string $egenskap Egenskapen som skal hentes
     * @param stdClass|array $param Objekt med ekstra parametere
     * @return mixed
     * @throws Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Leieforhold::hent()
     */
    public function hent($egenskap, $param = array()) {
        settype($param['oppdater'], 'boolean');
        $tp = $this->mysqli->table_prefix;

        if( !$this->id ) {
            return null;
        }

        switch( $egenskap ) {

        case "id":
        case "ant_terminer":
        case "terminlengde":
        case "avvent_oppfølging":
        case "fradato":
        case "frosset":
        case "kontrakter":
        case "kontraktnr":
        case "årlig_basisleie":
        case "leieforhold":
        case "leieobjekt":
        case "oppsigelsestid":
        case "regning_til_objekt":
        case "regningsobjekt":
        case "regningsperson":
        case "stopp_oppfølging":
        case "tildato":
        case "giroformat":
        {
            if ( ( $this->data === null || @$param['oppdater'] ) and !$this->last() ) {
                return null;
            }
            return $this->data->$egenskap;
        }

        case "krav":
        case "leiekrav":
        {
            if ( $this->krav === null || @$param['oppdater'] ) {
                $this->lastKrav();
            }
            return $this->$egenskap;
        }

        case "innbetalinger":
        {
            /*
            Retur: stdClass objekt:
                ->innbetaling: Innbetalingsobjektet
                ->beløp (tall): Delbeløpet som er kreditert dette leieforholdet
                ->dato    (DateTime) Dato for sortering
            */
            if ( $this->innbetalinger === null || @$param['oppdater'] ) {
                $this->lastBetalinger();
            }

            if( !is_array( $this->innbetalinger ) ) {
                throw new Exception('Leieforhold::innbetalinger = ' . print_r( $this->innbetalinger, true ));
            }

            return $this->innbetalinger;
        }

        case "transaksjoner":
        {
            $resultat = array_merge(
                $this->hent('krav'),
                $this->hent('innbetalinger')
            );
            usort($resultat, array($this->leiebase, 'sammenliknTransaksjonsdatoer'));
            return $resultat;
        }

        case "delkravtyper":
        {
            if ( $this->delkravtyper === null || @$param['oppdater'] ) {
                $this->lastDelkravtyper();
            }
            return $this->$egenskap;
        }

        case "fellesstrøm_delkrav": {
            if(!isset($this->fellestrømDelkrav)) {
                $this->fellestrømDelkrav = false;
                foreach($this->hent('delkravtyper') as $delkravtype) {
                    if($delkravtype->id == $this->leiebase->hentValg('delkravtype_fellesstrøm')) {
                        $this->fellestrømDelkrav = $delkravtype;
                    }
                }
            }
            return $this->fellestrømDelkrav;
        }

        case "leiebeløp": // Beregnet leiebeløp per termin
        {
            $årsleie = $this->hent('årlig_basisleie');
            foreach($this->hent('delkravtyper') AS $delkravtype) {
                if($delkravtype->kravtype == "Husleie" and !$delkravtype->selvstendig_tillegg) {
                    $årsleie += $delkravtype->beløp;
                }
            }
            return round(bcdiv($årsleie, $this->hent('ant_terminer'), 1));
        }

        case "efakturareferanse":
        {
            return str_pad($this->id, 5, '0', STR_PAD_LEFT);
        }

        case "kortnavn":
        case "leietakere":
        case "leietakerfelt":
        case "slettedeLeietakere":
        case "navn":
        {
            if ( $this->leietakere === null || @$param['oppdater'] ) {
                $this->lastLeietakere();
            }
            return $this->$egenskap;
        }

        case "beskrivelse": {
            /** @var \Leieobjekt $leieobjekt */
            $leieobjekt = $this->hent('leieobjekt');
            return $this->hent('navn') . ' i ' . $leieobjekt->hent('beskrivelse');
        }

        case "oppsigelse": {
            if ( $this->oppsigelse === null || @$param['oppdater'] ) {
                $this->lastOppsigelse();
            }
            return $this->oppsigelse;
        }

        case "fremtidigeKrav": {
            if ( $this->fremtidigeKrav === null || @$param['oppdater'] ) {
                $this->lastFremtidigeKrav();
            }
            return $this->fremtidigeKrav;
        }

        case "ubetalteKrav": {
            if ( $this->ubetalteKrav === null || @$param['oppdater'] ) {
                $this->lastUbetalteKrav();
            }
            return $this->ubetalteKrav;
        }

        case "utestående": {
            if ( $this->ubetalteKrav === null || @$param['oppdater'] ) {
                $this->lastUbetalteKrav();
            }
            $utestående = 0;
            foreach( $this->ubetalteKrav as $krav ) {
                $utestående += $krav->hent('utestående');
            }
            return $utestående;
        }

        case "forfalt": {
            if ( $this->ubetalteKrav === null || @$param['oppdater'] ) {
                $this->lastUbetalteKrav();
            }
            $forfalt = 0;
            foreach( $this->ubetalteKrav as $krav ) {
                if( $krav->hent('forfall') <= new DateTime ) {
                    $forfalt += $krav->hent('utestående');
                }
            }
            return $forfalt;
        }

        case "adresse": {
            if ( $this->adressefelt === null || @$param['oppdater'] ) {
                $this->lastAdressefelt();
            }
            return $this->adresse;
        }

        case "adressefelt": {
            if ( $this->adressefelt === null || @$param['oppdater'] ) {
                $this->lastAdressefelt();
            }
            return $this->adressefelt;
        }

        /*    Hent epostadresser til brukere tilknyttet leieforholdet
        Returnerer et array med epostadresser som strenger i formatet 'Navn <epostadresse>'
        ******************************************
        Parametere:
            innbetalingsbekreftelse (boolsk, normalt ikke)
            forfallsvarsel (boolsk, normalt ikke)
        ------------------------------------------
        retur: (array) liste med epostadresser
        */
        case "brukerepost": {
            settype($param['innbetalingsbekreftelse'], 'boolean');
            settype($param['forfallsvarsel'], 'boolean');

            if ( $this->brukerepost === null ) {
                $this->lastBrukerepost();
            }
            if ($param['innbetalingsbekreftelse']) {
                return $this->brukerepost->innbetalingsbekreftelse;
            }
            if ($param['forfallsvarsel']) {
                return $this->brukerepost->forfallsvarsel;
            }
            else {
                return $this->brukerepost->generelt;
            }
        }

    //    Parametere:
    //        rute:    heltall, utskriftsrute posisjonen skal hentes fra
        case "efakturaavtale": {
            $resultat = $this->mysqli->arrayData(array(
                'source'        => "{$tp}efaktura_avtaler",
                'where'            => "leieforhold = '{$this->id}'\n
                                    AND avtalestatus = 'A'"
            ));
            if($resultat->totalRows) {
                $resultat->data[0]->registrert = new DateTime($resultat->data[0]->registrert);
                return $resultat->data[0];
            }
            else {
                return false;
            }
        }

    //    Faste betalingsoppdrag (= avtale om AvtaleGiro)
    //    Parametere:
    //        type:    heltall, normalt 0, angir evt bestemt betalingstype
    //                1 = Husleie
    //                2 = Fellesstrøm
        case "fbo": {
            settype($param['type'], 'integer');
            $resultat = $this->mysqli->arrayData(array(
                'returnQuery'    => true,
                'source'        => "{$tp}fbo",
                'orderfields'    => "type ASC",
                'where'            => "leieforhold = '{$this->id}'
                                    AND (type = '0' OR type = '{$param['type']}')"
            ));
            if($resultat->totalRows) {
                $resultat->data[0]->registrert = new DateTime($resultat->data[0]->registrert);
                return $resultat->data[0];
            }
            else {
                return false;
            }
        }

        case "epostgiro": {
            return $this->hentGiroformat() == 'epost';
        }

        default: {
            return null;
        }

        }

    }


    /**
     * Hent Saldo per dato
     *
     * Henter leieforholdets saldo ved utløpet av en gitt dato
     * En negativ saldo betyr gjeld, mens en positiv saldo er penger tilgode
     *
     * @param string|DateTime $dato
     * @return float
     * @throws Exception
     */
    public function hentSaldoPerDato($dato ) {
        $tp = $this->mysqli->table_prefix;

        if( $dato instanceof DateTime ) {
            $dato = $dato->format('Y-m-d');
        }

        $kredit = $this->mysqli->arrayData(array(
            'source'        => "{$tp}innbetalinger AS innbetalinger",
            'where'            => "innbetalinger.leieforhold = '{$this->id}'\n"
                            .    "AND innbetalinger.dato <= '{$dato}'\n",
            'fields'        =>    "SUM(innbetalinger.beløp) AS sum"
        ));
        if($kredit->totalRows) {
            $kredit = reset($kredit->data)->sum;
        }
        else {
            $kredit = 0;
        }

        $debet = $this->mysqli->arrayData(array(
            'source'        => "{$tp}{$this->tabell} AS {$this->tabell}\n"
                            .    "INNER JOIN {$tp}krav AS krav ON {$this->tabell}.kontraktnr = krav.kontraktnr\n",
            'where'            => "{$this->tabell}.{$this->idFelt} = '{$this->id}'\n"
                            .    "AND krav.kravdato <= '{$dato}'\n",
            'fields'        =>    "SUM(krav.beløp) AS sum"
        ));
        if($debet->totalRows) {
            $debet = reset($debet->data)->sum;
        }
        else {
            $debet = 0;
        }

        return round( $kredit - $debet, 2 );
    }


    /**
     * Hent Transaksjoner
     *
     * Henter alle krav og innbetalinger for dette leieforholdet innenfor et gitt tidsrom
     * Dersom fra- eller til-dato ikke er angitt, returneres alle transaksjoner
     *
     * @param string|DateTime|null $fra
     * @param string|DateTime|null $til
     * @return array Innbetaling- og Krav-objekter sortert etter dato
     * @throws Exception
     */
    public function hentTransaksjoner($fra = null, $til = null ) {
        $tp = $this->mysqli->table_prefix;

        if( $fra instanceof DateTime ) {
            $fra = $fra->format('Y-m-d');
        }
        if( $til instanceof DateTime ) {
            $til = $til->format('Y-m-d');
        }

        $filter = array("innbetalinger.leieforhold = '{$this->id}'");
        if( $fra ) {
            $filter[] = "innbetalinger.dato >= '{$fra}'";
        }
        if( $til ) {
            $filter[] = "innbetalinger.dato <= '{$til}'";
        }
        $kredit = $this->mysqli->arrayData(array(
            'distinct'        => true,
            'source'        => "{$tp}innbetalinger AS innbetalinger",
            'where'            => '(' . implode( ') AND (', $filter ) . ')',
            'fields'        =>    "innbetalinger.innbetaling AS id",
            'orderfields'    => "innbetalinger.innbetaling",
            'class'            => \Innbetaling::class
        ));


        $filter = array("kontrakter.leieforhold = '{$this->id}'");
        if( $fra ) {
            $filter[] = "krav.kravdato >= '{$fra}'";
        }
        if( $til ) {
            $filter[] = "krav.kravdato <= '{$til}'";
        }
        $debet = $this->mysqli->arrayData(array(
            'distinct'        => true,
            'source'        => "{$tp}krav AS krav\n"
                            .    "INNER JOIN kontrakter ON krav.kontraktnr = kontrakter.kontraktnr",
            'where'            => '(' . implode( ') AND (', $filter ) . ')',
            'fields'        =>    "krav.id",
            'orderfields'    => "krav.kravdato, krav.id",
            'class'            => \Krav::class
        ));

        $resultat = array_merge($kredit->data, $debet->data);

        usort($resultat, array( $this->leiebase, 'sammenliknTransaksjonsdatoer' ));
        return $resultat;
    }


    /**
     * Legg til en eller flere leietakere i leieforholdet
     *
     * @param Person|Person[]|string[]|int[] $leietakere
     * @return array Person-objekter som har blitt lagt til.
     */
    public function leggTilLeietaker($leietakere) {
        $resultat = array();
        $tp = $this->mysqli->table_prefix;
        if( !is_array($leietakere) ) {
            $leietakere = array($leietakere);
        }

        /** @var \Person $leietaker */
        foreach( $leietakere as $leietaker) {
            if(!is_a($leietaker, \Person::class)) {
                $leietaker = $this->leiebase->hent(\Person::class, (int)strval($leietaker));
            }
            if($leietaker->hentId() and $this->mysqli->query("
                INSERT INTO {$tp}kontraktpersoner (person, kontrakt, leieforhold)
                VALUES ('{$leietaker}', '{$this->kontraktnr}', '{$this->id}')
                ON DUPLICATE KEY UPDATE slettet = null
            ")) {
                $resultat[] = $leietaker;
            }
        }

        $this->leietakere = null;
        return $resultat;
    }



    /**
     * Nullstiller alle egenskapene i leieforholdet untatt id,
     * sånn at de tvinges til å lastes på nytt.
     *
     * @return bool|void
     */
    public function nullstill() {
        $this->data                = null;    // DB-verdiene lagret som et objekt. Null om de ikke er lastet
        $this->delkravtyper        = null;    // Alle delkravtyper som hører til leieforholdet
        $this->leietakere        = null;    // Liste over leietakeren(e) som inngår i leieavtalen
        $this->slettedeLeietakere = array();    // Liste over leietakere som er slettet fra leieavtalen
        $this->leietakerfelt    = null;    // Streng som lister alle leietakerne med fødsels- og personnummer
        $this->krav                = null;    // Alle betalingskrav som er opprettet i leieavtalen
        $this->leiekrav            = null;    // Alle husleiekrav som er opprettet i leieavtalen
        $this->navn                = null;    // Navn på leietakeren(e) som inngår i leieavtalen
        $this->kortnavn            = null;    // Forkortet navn på leietakeren(e)
        $this->adresse            = null;    // stdClass-objekt med adresseelementene
        $this->adressefelt        = null;    // Adressefelt for utskrift
        $this->brukerepost        = null;    // Liste over brukerepostadresser
        $this->oppsigelse        = null;    // stdClass-objekt med oppsigelse. Null dersom ikke lastet
        $this->ubetalteKrav        = null;    // Array over alle ubetalte krav. Null om de ikke er lastet
        $this->utskriftsposisjon    = array();    // Utskriftsposisjonen for hver enkelt rute
        return true;
    }


    /**
     * Oppretter et nytt leieforhold i databasen og tildeler egenskapene til dette objektet
     *
     * @param array|stdClass $egenskaper Alle egenskapene det nye objektet skal initieres med
     * @return $this|bool|mixed
     * @throws Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Leiebase::opprettLeieforhold()
     */
    public function opprett($egenskaper = array()) {
        throw new Exception('Bruk av utdatert metode ' . __METHOD__);
    }

    /**
     * Skriv en verdi
     *
     * @param string $egenskap Egenskapen som skal endres
     * @param mixed $verdi Ny verdi
     * @return bool
     * @throws Exception
     */
    public function sett($egenskap, $verdi = null) {
        $tp = $this->mysqli->table_prefix;

        if( !$this->id ) {
            return null;
        }
        if( $egenskap == 'oppsigelsestid' and !$verdi ) {
            $verdi = 'P0M';
        }

        switch( $egenskap ) {

        //    Verdier som kun skal endres i siste kontrakt:
        case "tildato":
        {
            if ( $verdi instanceof DateTime ) {
                $verdi = $verdi->format('Y-m-d');
            }
            if ( $verdi instanceof DateInterval ) {
                $verdi = $this->leiebase->periodeformat( $verdi, true );
            }

            $resultat = $this->mysqli->saveToDb(array(
                'update'    => true,
                'table'        => "{$tp}{$this->tabell} as {$this->tabell}",
                'where'        => "{$this->tabell}.kontraktnr = '{$this->hent('kontraktnr')}'",
                'fields'    => array(
                    "{$this->tabell}.{$egenskap}"    => $verdi
                )
            ))->success;
            break;
        }

        //    Verdier som skal endres for hele leieforholdet:
        case "frosset":
        case "regningsperson":
        case "regningsobjekt":
        case "regning_til_objekt":
        case "regningsadresse1":
        case "regningsadresse2":
        case "postnr":
        case "poststed":
        case "land":
        case "giroformat":
        case "ant_terminer":
        case "årlig_basisleie":
        case "oppsigelsestid":
        {
            if ( $verdi instanceof DateTime ) {
                $verdi = $verdi->format('Y-m-d');
            }
            if ( $verdi instanceof DateInterval ) {
                $verdi = $this->leiebase->periodeformat( $verdi, true );
            }

            $resultat = $this->mysqli->saveToDb(array(
                'update'    => true,
                'table'        => "{$tp}{$this->tabell} as {$this->tabell}",
                'where'        => "{$this->tabell}.{$this->idFelt} = '{$this->id}'",
                'fields'    => array(
                    "{$this->tabell}.{$egenskap}"    => $verdi
                )
            ))->success;
            $this->mysqli->saveToDb(array(
                'update'    => true,
                'table'        => "{$tp}leieforhold as leieforhold",
                'where'        => "leieforhold.leieforholdnr = '{$this->id}'",
                'fields'    => array(
                    "leieforhold.{$egenskap}"    => $verdi
                )
            ))->success;
            break;
        }

        case "delkravtyper":
        case "tillegg":
        {
            $tillegg = $egenskap == "tillegg" ? "" : "!";
            if ( $verdi !== null) {
                throw new Exception("Delkravtypene i leieforhold {$this->id} forsøkt endret på feil måte.");
            }

            $resultat = $this->mysqli->query("DELETE FROM {$tp}leieforhold_delkrav WHERE {$tillegg}{$tp}leieforhold_delkrav.selvstendig_tillegg AND {$tp}leieforhold_delkrav.leieforhold = '{$this->hentId()}'");
            break;
        }

        default:
        {
            throw new Exception("Feil bruk av Leieforhold::sett(). {$egenskap} forsøkt satt til " . var_export($verdi, true));
        }

        }


        // Tving ny lasting av data:
        switch( $egenskap ) {

        case "tildato":
        case "årlig_basisleie":
        case "ant_terminer":
        case "delkravtyper":
        {
            $this->oppdaterLeie();
            break;
        }

        case "regningsperson":
        case "regningsobjekt":
        case "regning_til_objekt":
        case "regningsadresse1":
        case "regningsadresse2":
        case "postnr":
        case "poststed":
        case "land":
        {
            $this->adresse        = null;
            $this->adressefelt    = null;
        }

        default:
        {
            $this->data    = null;
        }
        }

        return $resultat;
    }

    /**
     * @return string
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Leieforhold::hentGiroformat()
     * @throws Exception
     */
    public function hentGiroformat()
    {
        $efakturaavtale = $this->hent('efakturaavtale');
        if($this->leiebase->hentValg('efaktura') && $efakturaavtale) {
            return 'eFaktura';
        }
        $giroformat = $this->hent('giroformat');
        if($giroformat == 'epost') {
            return 'epost';
        }
        if($giroformat == 'papir') {
            return 'papir';
        }
        else {
            return '';
        }
    }
}