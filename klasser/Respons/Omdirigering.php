<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 24.02.2025
 * Time: 13:37
 */

namespace Kyegil\Leiebasen\Respons;

use Kyegil\Leiebasen\Respons;

class Omdirigering extends Respons
{
    public function __construct($url)
    {
        $this->headers = [
            'Location: ' . $url,
        ];
        parent::__construct('');
    }
}