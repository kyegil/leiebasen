<?php

namespace Kyegil\Leiebasen\Respons;


use Kyegil\Leiebasen\Respons;

/**
 *
 */
class PdfRespons extends Respons
{
    /**
     * @var string
     */
    public $filnavn = 'pdf.pdf';

    /**
     * @return string
     */
    public function __toString():string
    {
        if(!$this->headers) {
            $this->headers = [
                'Content-Type: application/pdf',
                'Content-Disposition: attachment; filename="' . $this->filnavn . '";',
                'Content-Transfer-Encoding: binary',
                'Content-Length: ' . strlen($this->innholdSomStreng()),
                'Accept-Ranges: bytes'
            ];
        }
        return parent::__toString();
    }
}