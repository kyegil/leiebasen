<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 06/09/2020
 * Time: 08:47
 */

namespace Kyegil\Leiebasen\Respons;


use Kyegil\Leiebasen\Respons;

class JsonRespons extends Respons
{
    /**
     * @return string
     */
    public function __toString():string
    {
        if(!$this->headers) {
            $this->headers = [
                'Content-Type: application/json',
            ];
        }
        return parent::__toString();
    }

    /**
     * @return string
     */
    public function innholdSomStreng(): string
    {
        return json_encode($this->innhold);
    }
}