<?php

namespace Kyegil\Leiebasen\Respons;


use Kyegil\Leiebasen\Respons;

/**
 *
 */
class InlineFilRespons extends Respons
{
    /**
     * @var string
     */
    public $filnavn = '';

    public function __construct($filnavn)
    {
        if(file_exists($filnavn) && is_readable($filnavn)) {
            $innhold = file_get_contents($filnavn);
        }
        parent::__construct($innhold);
        $this->filnavn = $filnavn;
    }

    /**
     * @return string
     */
    public function __toString():string
    {
        if(!$this->headers) {
            $this->headers = $this->getHeaders($this->filnavn);
        }
        parent::__toString();
        return readfile($this->filnavn);
    }

    public function getHeaders(string $filnavn)
    {
        $contentType = self::contentType($filnavn);
        return [
            'Content-Type: ' . $contentType,
            'Content-Length: ' . strlen($this->innholdSomStreng()),
        ];
    }

    public static function contentType(string $filnavn)
    {
        $mime = mime_content_type($filnavn);
        $extension = pathinfo($filnavn, PATHINFO_EXTENSION);
        switch($extension) {
            case 'css':
                $mime = str_replace(['text/plain', 'application/x-empty'], 'text/css', $mime);
                break;
            case 'js':
                $mime = str_replace(['text/plain', 'application/x-empty'], 'application/javascript', $mime);
                break;
        }
        return $mime;
    }
}