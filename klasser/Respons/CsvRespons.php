<?php

namespace Kyegil\Leiebasen\Respons;


use Kyegil\Leiebasen\Respons;

/**
 *
 */
class CsvRespons extends Respons
{
    /**
     * @var string
     */
    public $filnavn = 'csv.csv';

    /**
     * @var string
     */
    public $separator = ',';

    /**
     * @var string
     */
    public $enclosure = '"';

    /**
     * @var string
     */
    public $escape = "\\";

    /**
     * @return string
     */
    public function __toString():string
    {
        if(!$this->headers) {
            $this->headers = [
                'Content-Type: application/csv',
                'Content-Disposition: attachment; filename="' . $this->filnavn . '";'
            ];
        }
        return parent::__toString();
    }

    /**
     * @return string
     */
    public function innholdSomStreng(): string
    {
        $fil  = fopen('php://output', 'w');
        foreach ($this->innhold as $linje) {
            fputcsv($fil, (array)$linje, $this->separator, $this->enclosure, $this->escape);
        }
        fclose($fil);
        return '';
    }
}