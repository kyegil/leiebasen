<?php

namespace Kyegil\Leiebasen;

use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Verdi;
use Kyegil\ViewRenderer\ViewInterface;
use stdClass;

abstract class EavVerdiVisningRenderer
{
    /**
     * @return string
     */
    public abstract static function hentNavn(): string;

    /**
     * @param Verdi|null $verdiObjekt
     * @param stdClass|null $oppsett
     * @param Egenskap $egenskap
     * @param string $visningType
     * @param string $bruk
     * @return string|ViewInterface
     */
    public abstract static function vis(?Verdi $verdiObjekt, ?stdClass $oppsett, Egenskap $egenskap, string $visningType, string $bruk);

    /**
     * @return stdClass|null
     */
    public abstract static function hentStandardOppsett(): ?stdClass;
}