<?php

namespace Kyegil\Leiebasen;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Poengprogram;
use Kyegil\Leiebasen\Modell\Poengprogram\Poeng;
use Kyegil\Leiebasen\Modell\Poengprogram\Poengsett;
use Kyegil\Leiebasen\Modell\Poengprogramsett;

/**
 *
 */
class Poengbestyrer
{
    /** @var Poengbestyrer */
    private static Poengbestyrer $instance;
    /** @var CoreModelImplementering */
    protected CoreModelImplementering $app;
    /** @var array[][][][] */
    protected array $status = [];

    /**
     * @param CoreModelImplementering $app
     * @return Poengbestyrer
     */
    public static function getInstance(CoreModelImplementering $app): Poengbestyrer
    {
        if ( !isset( self::$instance ) )
        {
            self::$instance = new self($app);
        }
        return self::$instance;
    }

    /**
     * @param CoreModelImplementering $app
     */
    private function __construct(CoreModelImplementering $app)
    {
        $this->app = $app;
    }

    public static function cron(CoreModelImplementering $leiebase, array $argumenter = []): array
    {
        $poengbestyrer = self::getInstance($leiebase);
        $poengbestyrer->slettForeldetePoeng();
        return $argumenter;
    }

    /**
     * @return Poengbestyrer
     * @throws Exception
     */
    public function slettForeldetePoeng(): Poengbestyrer
    {
        /** @var Poengsett $utløptePoeng */
        $utløptePoeng = $this->app->hentSamling(Poeng::class)
            ->leggTilFilter(['`' . Poeng::hentTabell() . '`.`foreldes` <=' => date_create()])
        ;
        $utløptePoeng->slettAlle();
        return $this;
    }

    /**
     * @return Poengprogramsett
     * @throws Exception
     */
    public function hentAktiveProgram(): Poengprogramsett
    {
        /** @var Poengprogramsett $programsett */
        $programsett = $this->app->hentSamling(Poengprogram::class);
        $programsett->leggTilFilter(['`' . Poengprogram::hentTabell(). '`.`aktivert` IS NOT NULL']);
        return clone $programsett;
    }

    /**
     * @param Poengprogram $program
     * @param Leieforhold $leieforhold
     * @return Poengsett
     * @throws Exception
     */
    public function hentPoeng(Poengprogram $program, Leieforhold $leieforhold): Poengsett
    {
        if(!isset($this->status[$program->kode]['leieforhold'][$leieforhold->hentId()]['poeng'])) {
            $poengsett = $program->hentPoengsett();
            $poengsett->leggTilFilter(['leieforhold_id' => $leieforhold->hentId()])->låsFiltre();
            $this->status[$program->kode]['leieforhold'][$leieforhold->hentId()]['poeng'] = $poengsett;
        }
        /** @var Poengsett $poengsett */
        $poengsett = $this->status[$program->kode]['leieforhold'][$leieforhold->hentId()]['poeng'];
        return clone $poengsett;
    }

    /**
     * @param Poengprogram $program
     * @param Leieforhold $leieforhold
     * @return int
     * @throws Exception
     */
    public function hentPoengSum(Poengprogram $program, Leieforhold $leieforhold): int
    {
        if(!isset($this->status[$program->kode]['leieforhold'][$leieforhold->hentId()]['sum'])) {
            $this->status[$program->kode]['leieforhold'][$leieforhold->hentId()]['sum'] = 0;
            foreach($this->hentPoeng($program, $leieforhold) as $poeng) {
                $this->status[$program->kode]['leieforhold'][$leieforhold->hentId()]['sum'] += $poeng->hentVerdi();
            }
        }
        /** @var integer $sum */
        $sum = $this->status[$program->kode]['leieforhold'][$leieforhold->hentId()]['sum'];
        return $sum;
    }

    /**
     * @param string $kodeEllerId
     * @return Poengprogram|null
     * @throws Exception
     */
    public function hentProgram(string $kodeEllerId): ?Poengprogram
    {
        /** @var Poengprogramsett $programsett */
        $programsett = $this->app->hentSamling(Poengprogram::class);
        $programsett->leggTilFilter(['or' => [
            'id' => $kodeEllerId,
            'kode' => $kodeEllerId,
        ]]);
        /** @var Poengprogram|null $program */
        $program = $programsett->hentFørste();
        return $program;
    }

    /**
     * @param string $programkode
     * @param Leieforhold $leieforhold
     * @param string $tekst
     * @param string $type
     * @param int $verdi
     * @param DateTimeInterface|null $tid
     * @return Poeng|null
     * @throws Exception
     */
    public function leggTilPoeng(
        string $programkode,
        Modell\Leieforhold $leieforhold,
        string $tekst = '',
        string $type = '',
        int $verdi = 1,
        ?DateTimeInterface $tid = null
    ): ?Poeng
    {
        list(
            'programkode' => $programkode,
            'leieforhold' => $leieforhold,
            'tekst' => $tekst,
            'type' => $type,
            'verdi' => $verdi,
            'tid' => $tid
        ) = $this->app->before($this, __FUNCTION__, [
            'programkode' => $programkode,
            'leieforhold' => $leieforhold,
            'tekst' => $tekst,
            'type' => $type,
            'verdi' => $verdi,
            'tid' => $tid
        ]);
        $poeng = null;
        $tid = $tid ?? date_create_immutable();
        if ($tid instanceof DateTime) {
            $tid = DateTimeImmutable::createFromMutable($tid);
        }
        $program = $this->hentProgram($programkode);
        if($program) {
            $levetid = $program->hentLevetid();
            $foreldes = $levetid ? $tid->add($levetid) : null;

            $poeng = $this->app->nyModell(Poeng::class, (object)[
                'program' => $program,
                'leieforhold' => $leieforhold,
                'type' => $type,
                'tekst' => $tekst,
                'verdi' => $verdi,
                'tid' => $tid,
                'foreldes' => $foreldes,
                'registrerer' => $this->app->bruker['navn'] ?? ''
            ]);
        }
        return $this->app->after($this, __FUNCTION__, $poeng);
    }
}