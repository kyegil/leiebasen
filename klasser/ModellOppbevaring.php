<?php
    /**
     * * Part of kyegil/leiebasen
     * Created by Kyegil
     * Date: 12/06/2020
     * Time: 09:28
     */

    namespace Kyegil\Leiebasen;


    use Kyegil\CoreModel\Interfaces\CoreModelInterface;

    class ModellOppbevaring extends \Kyegil\CoreModel\CoreModelRepository
    {
        protected static ModellOppbevaring $forekomst;

        /**
         * @return ModellOppbevaring
         */
        public static function hentForekomst():ModellOppbevaring {
            if (!isset(self::$forekomst)) {
                self::$forekomst = new self();
            }
            return self::$forekomst;
        }

        private function __construct()
        {
        }

        /**
         * @param CoreModelInterface $objekt
         * @return CoreModelInterface
         * @throws \Exception
         */
        public function oppbevar(CoreModelInterface $objekt)
        {
            return parent::save($objekt);
        }

        /**
         * @param string $klasse
         * @param $id
         * @return mixed|null
         * @throws \Exception
         */
        public function hent($klasse, $id)
        {
            return parent::get($klasse, $id);
        }

        /**
         * @param string $klasse
         * @param $id
         * @return ModellOppbevaring
         */
        public function fjern($klasse, $id)
        {
            return parent::remove($klasse, $id);
        }

        /**
         * @return ModellOppbevaring
         */
        public function tøm()
        {
            return parent::clear();
        }

    }