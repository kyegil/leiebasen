<?php

namespace Kyegil\Leiebasen\Jshtml;


/**
 * Class JsFunction
 * @package Kyegil\SplitPos\Jshtml
 */
class JsFunction implements PureJs
{
    /**
     * @var array
     */
    protected $params;
    /**
     * @var string
     */
    protected $body;
    /**
     * @var string
     */
    protected $functionName;

    /**
     * JsFunction constructor.
     * @param string $body
     * @param array $params
     * @param string $functionName
     */
    public function __construct(
        string $body,
        array $params = [],
        string $functionName = ''
    )
    {
        $this->params = $params;
        $this->body = $body;
        $this->functionName = $functionName;
        $this->string = "function {$this->functionName}(" . implode(', ', $this->params) . ") {
        {$this->body}
        }";
    }

    /**
     * @return string
     */
    public function __toString()
    {

        return $this->string;
    }
}