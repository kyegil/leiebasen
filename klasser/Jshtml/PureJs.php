<?php
/**
 * Part of Collective-POS
 * Created by Kyegil
 * Date: 04/02/2020
 * Time: 12:25
 */

namespace Kyegil\Leiebasen\Jshtml;


/**
 * Interface PureJs
 * @package Kyegil\SplitPos\Jshtml
 */
interface PureJs
{
    /**
     * @return string
     */
    public function __toString();
}