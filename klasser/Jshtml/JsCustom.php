<?php

namespace Kyegil\Leiebasen\Jshtml;


use Kyegil\ViewRenderer\ViewInterface;
use stdClass;

/**
 * Class JsCustom
 * @package Kyegil\SplitPos\Jshtml
 */
class JsCustom implements PureJs
{
    /**
     * @var string
     */
    protected $string;

    /**
     * @var int
     */
    protected static $jsonLevel = 0;

    /**
     * JsCustom constructor.
     * @param string $string
     */
    public function __construct(string $string)
    {
        $this->string = $string;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->string;
    }

    /**
     * @param mixed $verdi
     * @return string
     */
    public static function JsonImproved($verdi): string
    {
        $json = '';
        if($verdi instanceof PureJs) {
            $json .= $verdi;
        }
        else if($verdi instanceof ViewInterface) {
            $json .= $verdi;
        }
        else if($verdi instanceof stdClass) {
            $json .= "{\n";
            $array = [];
            self::$jsonLevel++;
            foreach($verdi as $property => $value) {
                $array[] = str_repeat('  ', self::$jsonLevel) . "{$property}: " . self::JsonImproved($value);
            }
            $json .= implode(",\n", $array);
            self::$jsonLevel--;
            $json .= "\n" . str_repeat('  ', self::$jsonLevel) . "}";
        }
        else if(is_array($verdi)) {
            self::$jsonLevel++;
            $verdi = array_map([self::class, 'JsonImproved'], $verdi);
            $json .= "[\n";
            $json .= str_repeat('  ', self::$jsonLevel)
                . implode(",\n" . str_repeat('  ', self::$jsonLevel), $verdi)
                . "\n";
            self::$jsonLevel--;
            $json .= str_repeat('  ', self::$jsonLevel)
                . "]";
        }
        else {
            $json .= json_encode($verdi);
        }
        return $json;
    }
}