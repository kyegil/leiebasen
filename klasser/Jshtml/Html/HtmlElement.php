<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 10:20
 */

namespace Kyegil\Leiebasen\Jshtml\Html;


use Closure;
use Kyegil\Leiebasen\Jshtml\PureJs;
use Kyegil\ViewRenderer\ViewInterface;
use stdClass;

class HtmlElement implements ViewInterface
{
    protected string $tagName = '';

    /** @var stdClass */
    protected $attributes;

    /** @var array  */
    protected array $contents = [];

    protected ?ViewInterface $parent;

    /**
     * @var mixed
     */
    private array $instructionsForAscendants = [];

    /**
     * HtmlElement constructor.
     * @param string $tagName
     * @param array|stdClass $attributes
     * @param mixed $contents
     */
    public function __construct(string $tagName, $attributes = [], $contents = [])
    {
        $this->tagName = $tagName;
        $this->attributes = (object)$attributes;
        if($contents) {
            if(is_array($contents)) {
                $this->contents = $contents;
            }
            else {
                $this->contents = [$contents];
            }
            $this->setParentToChildren();
        }
    }

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        $contents = array_map(function($content) {
            return strval($content);
        }, $this->contents);
        $tag = $this->tagName;
        return "<{$tag}{$this->argumentsAsString()}>" . implode('', $contents) . ($contents || !$this->isSelfClosing() ? "</{$tag}>" : "") . "\n";
    }

    /**
     * @return string
     */
    protected function argumentsAsString(): string
    {
        $string = '';
        if(get_object_vars($this->attributes)) {
            foreach ($this->attributes as $attribute => $value) {
                $string .= ' ' .strtolower($attribute) . '="' . ($value instanceof PureJs ? $value : addslashes($value)) . '"';
            }
        }
        return $string;
    }

    /**
     * @param ViewInterface|null $parent
     * @return HtmlElement
     */
    public function setParent(?ViewInterface $parent = null): ViewInterface
    {
        $this->parent = $parent;
            if($parent) {
                $this->passInstructionsToParent($parent);
            }
        return $this;
    }

    /**
     * @return ViewInterface|null
     */
    public function getParent(): ?ViewInterface
    {
        return $this->parent;
    }

    /**
     * @return $this
     */
    protected function setParentToChildren(): HtmlElement
    {
        foreach($this->contents as $child) {
            $this->setParentToChild($child);
        }
        return $this;
    }

    /**
     * @param $child
     * @return $this
     */
    protected function setParentToChild($child): HtmlElement
    {
        if($child instanceof ViewInterface) {
            $child->setParent($this);
        }
        return $this;
    }

    /**
     * @param string|null $tag
     * @return boolean
     */
    public function isSelfClosing(string $tag = null): bool
    {
        $tag = $tag ?: $this->tagName;
        $selfClosingTags = ['area', 'base', 'br', 'col', 'embed', 'hr', 'img', 'input', 'link', 'meta', 'param', 'source', 'track', 'wbr'];
        return in_array($tag, $selfClosingTags);
    }

    /**
     * @return stdClass
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param string $attribute
     * @param $value
     * @return $this
     */
    public function setAttribute(string $attribute, $value): HtmlElement
    {
        $this->attributes->$attribute = $value;
        return $this;
    }

    /**
     * @return array
     */
    public function getContents(): array
    {
        return $this->contents;
    }

    /**
     * @param mixed $contents
     * @return HtmlElement
     */
    public function setContents($contents): HtmlElement
    {
        $this->contents = [];
        if($contents) {
            if(is_array($contents)) {
                $this->contents = $contents;
            }
            else {
                $this->contents = [$contents];
            }
            $this->setParentToChildren();
        }
        return $this;
    }

    /**
     * @param string $ascendantClass
     * @param array $instructions
     * @return $this
     */
    public function addInstructionsForAscendant(string $ascendantClass, array $instructions): HtmlElement {
        settype($this->instructionsForAscendants[$ascendantClass], 'array');
        $this->instructionsForAscendants[$ascendantClass] = array_merge($this->instructionsForAscendants[$ascendantClass], $instructions);
        return $this;
    }

    /**
     * @param ViewInterface $parent
     * @return $this
     */
    protected function passInstructionsToParent(ViewInterface $parent)
    {
        /**
         * @var string $class
         * @var Closure[] $instructions
         */
        foreach($this->instructionsForAscendants as $class => $instructions) {
            if($parent instanceof $class) {
                $parent->addInstructions($instructions);
            }
            else {
                $parent->addInstructionsForAscendant($class, $instructions);
            }
        }
        return $this;
    }
}