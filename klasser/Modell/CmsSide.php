<?php

namespace Kyegil\Leiebasen\Modell;

use Kyegil\Leiebasen\CoreModelImplementering;
use Kyegil\Leiebasen\Modell;

/**
 * Class CmsSide
 * @package Kyegil\Leiebasen\Modell
 *
 * @property int $id
 * @property string $område
 * @property string $kode
 * @property string $tittel
 * @property string $html
 * @property string $css
 * @property string $script
 * @property object $konfigurering
 *
 * @method int hentId()
 * @method string hentOmråde()
 * @method string hentKode()
 * @method string hentTittel()
 * @method string hentHtml()
 * @method string hentCss()
 * @method string hentScript()
 * @method object hentKonfigurering()
 *
 * @method $this settOmråde(string $område)
 * @method $this settKode(string $kode)
 * @method $this settTittel(string $tittel)
 * @method $this settHtml(string $html)
 * @method $this settCss(string $css)
 * @method $this settScript(string $script)
 * @method $this settKonfigurering(object $konfigurering)
 */
class CmsSide extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'cms_sider';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var array one-to-many
     * @noinspection PhpDocFieldTypeMismatchInspection
     */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'område'    => [
                'type'  => 'string'
            ],
            'kode'    => [
                'type'  => 'string'
            ],
            'tittel'    => [
                'type'  => 'string'
            ],
            'html'    => [
                'type'  => 'string'
            ],
            'css'    => [
                'type'  => 'string'
            ],
            'script'    => [
                'type'  => 'string'
            ],
            'konfigurering'    => [
                'type'  => 'json'
            ],
        ];
    }

    /**
     * @param CoreModelImplementering $app
     * @param string $område
     * @param string $kode
     * @return CmsSide|null
     */
    public static function hentCmsSide(CoreModelImplementering $app, string $område, string $kode): ?CmsSide {
        /** @var CmsSide|null $side */
        $side = $app->hentSamling(CmsSide::class)
        ->leggTilFilter(['område' => $område])
        ->leggTilFilter(['kode' => $kode])
        ->låsFiltre()->hentFørste();
        return $side;
    }
}