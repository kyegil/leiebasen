<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 03/08/2020
 * Time: 11:26
 */

namespace Kyegil\Leiebasen\Modell\Leieobjekt;


use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Modell\Område\Deltakelse;
use Kyegil\Leiebasen\Modell\Område\Deltakelsesett;
use Kyegil\Leiebasen\Samling;

/**
 * Class Bygning
 * @package Kyegil\Leiebasen\Leieobjekter
 *
 * @property int $id
 * @property string $kode
 * @property string $navn
 * @property string $bilde
 *
 * @method int hentId()
 * @method string hentKode()
 * @method string hentNavn()
 * @method string hentBilde()
 *
 * @method $this settId(int $id)
 * @method $this settKode(string $kode)
 * @method $this settNavn(string $navn)
 * @method $this settBilde(string $bilde)
*/
class Bygning extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'bygninger';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Bygningsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'kode'    => [
                'type'  => 'string'
            ],
            'navn'    => [
                'type'  => 'string'
            ],
            'bilde'    => [
                'type'  => 'string'
            ]
        ];
    }

    /**
     * @return Deltakelsesett
     * @throws Exception
     */
    public function hentOmrådeDeltakelse(): Samling
    {
        if (!isset($this->samlinger->områdeDeltakelse)) {
            $this->samlinger->områdeDeltakelse = $this->app->hentSamling(Deltakelse::class)
                ->leggTilFilter(['medlem_type' => static::class])
                ->leggTilFilter(['medlem_id' => $this->hentId()])
                ->låsFiltre()
            ;
        }
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $this->samlinger->områdeDeltakelse;
    }

    /**
     * @return Område[]
     * @throws Exception
     */
    public function hentOmråder(): array
    {
        $områder = [];
        /** @var Deltakelse $deltakelse */
        foreach ($this->hentOmrådeDeltakelse() as $deltakelse) {
            $områder[] = $deltakelse->område;
        }
        return $områder;
    }

    /**
     * @param $deltakere
     * @return Bygning
     * @throws Exception
     */
    public function settOmrådeDeltakere($deltakere): Bygning
    {
        $this->fjernAllOmrådeDeltakelse();
        foreach ($deltakere as $deltaker) {
            if ($deltaker instanceof Område) {
                $this->leggTilOmråde($deltaker);
            }
        }
        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function fjernAllOmrådeDeltakelse(): Bygning
    {
        $this->hentOmrådeDeltakelse()->slettAlle();
        $this->samlinger->områdeDeltakelse = null;
        return $this;
    }

    /**
     * @param Område $område
     * @return $this
     * @throws Exception
     */
    public function leggTilOmråde(Område $område): Bygning
    {
        /** @var Deltakelse $deltakelse */
        foreach($this->hentOmrådeDeltakelse() as $deltakelse) {
            if ($deltakelse->område->hentId() == $område->hentId()
            ) {
                return $this;
            }
        }
            $this->app->nyModell(Deltakelse::class, (object)[
                'område' => $område,
                'medlem' => $this
            ]);
            $this->samlinger->områdeDeltakelse = null;
            return $this;
        }

    /**
     * @return void
     * @throws Exception
     */
    public function slett()
    {
        $this->fjernAllOmrådeDeltakelse();
        parent::slett();
    }
}