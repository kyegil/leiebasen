<?php

namespace Kyegil\Leiebasen\Modell\Leieobjekt;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Sett;

/**
 * Class Leieberegningsett
 * @package Kyegil\Leiebasen\Modell\Leieobjekt
 *
 * @method Leieberegning current()
 */
class Leieberegningsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Leieberegning::class;

    /**
     * @var Leieberegning[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }
}