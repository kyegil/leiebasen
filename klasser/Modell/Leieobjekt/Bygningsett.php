<?php

namespace Kyegil\Leiebasen\Modell\Leieobjekt;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Sett;

/**
 * Class Bygningsett
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Bygning current()
 */
class Bygningsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Bygning::class;

    /**
     * @var Bygning[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }
}