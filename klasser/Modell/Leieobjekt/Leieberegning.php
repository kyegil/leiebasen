<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 03/08/2020
 * Time: 11:26
 */

namespace Kyegil\Leiebasen\Modell\Leieobjekt;


use DateTimeInterface;
use DateTimeZone;
use Exception;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Leieregulerer;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjektsett;
use stdClass;

/**
 * Class Leieberegning
 * @package Kyegil\Leiebasen\Leieobjekter
 *
 * @property int $id
 * @property string $navn
 * @property string $beskrivelse
 * @property float $leiePerObjekt Leiegrunnlag per leieobjekt
 * @property float $leiePerKontrakt Leiegrunnlag per leieforhold
 * @property float $leiePerKvadratmeter Leiegrunnlag per kvadratmeter
 * @property object[] $spesialregler
 * @property object $satsHistorikk
 * @property DateTimeInterface $satsOppdatert
 *
 * @method int hentId()
 * @method string hentNavn()
 * @method string hentBeskrivelse()
 * @method float hentLeiePerObjekt()
 * @method float hentLeiePerKontrakt()
 * @method float hentLeiePerKvadratmeter()
 * @method object[] hentSpesialregler()
 * @method object hentSatsHistorikk()
 * @method DateTimeInterface hentSatsOppdatert()
 *
 * @method $this settId(int $id)
 * @method $this settNavn(string $navn)
 * @method $this settBeskrivelse(string $beskrivelse)
 * @method $this settLeiePerObjekt(float $leiePerObjekt)
 * @method $this settLeiePerKontrakt(float $leiePerKontrakt)
 * @method $this settLeiePerKvadratmeter(float $leiePerKvadratmeter)
 * @method $this settSpesialregler(object[] $spesialregler)
 */
class Leieberegning extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'leieberegning';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'nr';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Leieberegningsett::class;

    /**
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'nr'    => [
                'target'  => 'id',
                'type'  => 'integer'
            ],
            'navn'    => [
                'type'  => 'string'
            ],
            'beskrivelse'    => [
                'type'  => 'string'
            ],
            'leie_objekt'    => [
                'target'  => 'leie_per_objekt',
                'type'  => 'string'
            ],
            'leie_kontrakt'    => [
                'target'  => 'leie_per_kontrakt',
                'type'  => 'string'
            ],
            'leie_kvm'    => [
                'target'  => 'leie_per_kvadratmeter',
                'type'  => 'string'
            ],
            'spesialregler'    => [
                'type'  => 'json'
            ],
        ];
    }

    /**
     * @param Leiebase $leiebase
     * @param object $parametere
     * @param float $sats
     * @param Leieforhold|null $leieforhold
     * @param Leieobjekt|null $leieobjekt
     * @param Bygning|null $bygning
     * @param float $beløp
     * @return float
     * @throws Exception
     */
    public static function formelProsessor(
        Leiebase     $leiebase,
        object       $parametere,
        float        $sats,
        ?Leieforhold $leieforhold = null,
        ?Leieobjekt  $leieobjekt = null,
        ?Bygning     $bygning = null,
        float        $beløp = 0
    ):float {
        if($leieforhold) {
            $leieobjekt = $leieobjekt ?? $leieforhold->leieobjekt;
        }
        if($leieobjekt) {
            $bygning = $bygning ?: $leieobjekt->bygning;
        }
        /** @var object $kriterier */
        $kriterier = $parametere->kriterier ?? new stdClass();
        if (!self::validerKriterier($kriterier, $leieforhold, $leieobjekt, $bygning)) {
            return 0;
        }

        /** @var string $formel */
        $formel = $parametere->formel ?? '';
        return self::prossesserFormel($formel, $leieforhold, $leieobjekt, $bygning, $sats, $beløp);
    }

    /**
     * Henter en verdi fra et leieforhold, et leieobjekt eller en bygning
     * @param string $uttrykk i formatet 'objekt.verdi', f.eks. 'leieobjekt.areal'
     * @return mixed
     * @throws Exception
     */
    public static function objektVerdi(
        string                     $uttrykk,
        ?Leieforhold               $leieforhold = null,
        ?Modell\Leieobjekt         $leieobjekt = null,
        ?Modell\Leieobjekt\Bygning $bygning = null
    )
    {
        $uttrykk = explode('.', $uttrykk);
        /** @var string $objektNavn */
        $objektNavn = $uttrykk[0] ?? '';
        /** @var Leieforhold|Modell\Leieobjekt|Modell\Leieobjekt\Bygning|null $objekt */
        $objekt = $$objektNavn ?? null;
        /** @var string $egenskap */
        $egenskap = $uttrykk[1] ?? '';

        if(!in_array($objektNavn, ['leieforhold', 'leieobjekt', 'bygning']) || !isset($objekt)) {
            throw new Exception('Beregningsmodellen er avhengig av å vite ' . $objektNavn);
        }
        return $objekt->hent($egenskap);
    }

    /**
     * @param object|array $kriterier
     * @param Leieforhold|null $leieforhold
     * @param Leieobjekt|null $leieobjekt
     * @param Bygning|null $bygning
     * @param bool $or
     * @return bool
     * @throws Exception
     */
    public static function validerKriterier(
                                   $kriterier,
        ?Leieforhold               $leieforhold = null,
        ?Modell\Leieobjekt         $leieobjekt = null,
        ?Modell\Leieobjekt\Bygning $bygning = null,
        bool                       $or = false
    ): bool
    {
        $kriterieOppfyllt = !$or;
        foreach($kriterier as $uttrykk => $kriterie) {
            $uttrykk = trim($uttrykk);
            $relasjonsOperator = '=';

            if (in_array($uttrykk, ['or', 'and'])) {
                $verdi = self::validerKriterier($kriterie, $leieforhold, $leieobjekt, $bygning, $uttrykk == 'or');
            }
            else {
                if(is_numeric($uttrykk)) {
                    $uttrykk = $kriterie;
                    $kriterie = true;
                }
                else if(in_array(substr($uttrykk, -1), ['=', '≠', '<', '≤', '>', '≥'])) {
                    $relasjonsOperator = substr($uttrykk, -1);
                    $uttrykk = trim(substr($uttrykk, 0, -1));
                }
                $verdi = self::objektVerdi($uttrykk, $leieforhold, $leieobjekt, $bygning);
            }
            switch($relasjonsOperator) {
                case '=': $kriterieOppfyllt = $verdi == $kriterie;
                    break;
                case '≠': $kriterieOppfyllt = $verdi != $kriterie;
                    break;
                case '<': $kriterieOppfyllt = $verdi < $kriterie;
                    break;
                case '≤': $kriterieOppfyllt = $verdi <= $kriterie;
                    break;
                case '>': $kriterieOppfyllt = $verdi > $kriterie;
                    break;
                case '≥': $kriterieOppfyllt = $verdi >= $kriterie;
                    break;
            }
            if($or == $kriterieOppfyllt) {
                return $kriterieOppfyllt;
            }
        }
        return $kriterieOppfyllt;
    }

    /**
     * @param string $formel
     * @param Leieforhold|null $leieforhold
     * @param Leieobjekt|null $leieobjekt
     * @param Bygning|null $bygning
     * @param float $sats
     * @param float $beløp
     * @return mixed
     * @throws Exception
     */
    public static function prossesserFormel(
        string       $formel,
        ?Leieforhold $leieforhold,
        ?Leieobjekt  $leieobjekt,
        ?Bygning     $bygning,
        float        $sats,
        float        $beløp
    ): string
    {
        $formelRegex = '/\{(leieforhold\.|leieobjekt\.|bygning\.)?.+}/U';
        preg_match_all($formelRegex, $formel, $formelVariabler);
        /** @var string[] $formelVariabler */
        $formelVariabler = reset($formelVariabler) ?? [];
        $formel = preg_replace($formelRegex, '_', $formel);
        $formel = preg_replace('/[^0-9() _*+\-\/?:]/', '', $formel);
        array_walk($formelVariabler, function (&$variabel) use ($leieforhold, $leieobjekt, $bygning, $sats, $beløp) {
            $variabel = trim($variabel, '{}');
            if (in_array($variabel, ['sats', 'beløp'])) {
                $variabel = (string)$$variabel;
            } else {
                $variabel = self::objektVerdi($variabel, $leieforhold, $leieobjekt, $bygning);
            }
        });
        $formel = preg_replace_callback('/_/', function () use (&$formelVariabler) {
            return array_shift($formelVariabler);
        }, $formel);
        return eval('return ' . $formel . ';');
    }

    /**
     * @param Leieobjekt|null $leieobjekt
     * @param Bygning|null $bygning
     * @param float $beløp
     * @return float
     */
    public function prosesserSpesialregler(
        ?Leieobjekt $leieobjekt = null,
        ?Bygning $bygning = null,
        float $beløp = 0
    ): float
    {
        $akkumulertRegelbeløp = 0;
        $spesialregler = $this->hentSpesialregler();
        usort($spesialregler, function (object $regelA, object $regelB) {
            $ordenA = $regelA->sorteringsorden ?? null;
            $ordenB = $regelB->sorteringsorden ?? null;
            return $ordenA - $ordenB;
        });
        foreach($spesialregler as $regel) {
            /** @var callable $prosessor */
            $prosessor = $regel->prosessor ?? null;
            $akkumulertRegelbeløp += $prosessor(
                $this->app,
                $regel->param ?? new stdClass(),
                $regel->sats ?? null,
                null,
                $leieobjekt,
                $bygning,
                $beløp + $akkumulertRegelbeløp
            );
        }
        return $akkumulertRegelbeløp;
    }

    /**
     * @param stdClass $parametere
     * @return Modell
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Modell
    {
        if (isset($parametere->sats)) {
            $parametere->sats_oppdatert = date_create_immutable();
            $parametere->sats_historikk = (object)[
                $parametere->sats_oppdatert->format('c') => (object)[
                    'manuelt_angitt' => true,
                    'sats' => $parametere->sats
                ]
            ];
        }
        return parent::opprett($parametere);
    }

    /**
     * @return Leieobjektsett
     * @throws Exception
     */
    public function hentBruktAv(): Leieobjektsett
    {
        $this->app->before($this, __FUNCTION__, []);
        /** @var Leieobjektsett $leieobjekter */
        $leieobjekter = $this->app->hentSamling(Leieobjekt::class);
        $leieobjekter->leggTilBygningModell()
            ->leggTilFilter(['leieberegning' => $this->hentId()])
            ->låsFiltre()
        ;
        if(!isset($this->samlinger->leieobjekter)) {
            $this->samlinger->leieobjekter = $leieobjekter->hentElementer();
        }
        $leieobjekter->lastFra($this->samlinger->leieobjekter);
        /** Klon samlingen for å unnngå problem med nested loops */
        return $this->app->after($this, __FUNCTION__, clone $leieobjekter);
    }

    /**
     * @param bool $automatisk
     * @return Leieberegning
     */
    public function oppdaterSatsHistorikk(bool $automatisk = false): Leieberegning
    {
        list('automatisk' => $automatisk)
            = $this->app->before($this, __FUNCTION__, [
            'automatisk' => $automatisk
        ]);
        $tidspunkt = date_create_immutable('now', new DateTimeZone('utc'));
        $kpiHistorikk = [];
        try {
            $kpiHistorikk = Leieregulerer::hentKpiFraSSb();
        } catch (Exception $e) {
            $this->app->logger->warning($e->getMessage());
        }
        $egenskap = $tidspunkt->format('c');
        $satsHistorikk = $this->hentSatsHistorikk() ?? new stdClass();
        $satsHistorikk->$egenskap = (object)[
            'satser' => (object)[
                'leie_per_objekt' => $this->hentLeiePerObjekt(),
                'leie_per_kontrakt' => $this->hentLeiePerKontrakt(),
                'leie_per_kvadratmeter' => $this->hentLeiePerKvadratmeter(),
            ],
            'spesialregler' => $this->hentSpesialregler(),
            'automatisk' => $automatisk,
            'gjeldende_kpi' => array_key_last($kpiHistorikk),
        ];
        $this->satsHistorikk = $satsHistorikk;
        $this->satsOppdatert = $tidspunkt;
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @param Fraction $justeringsfaktor
     * @param bool $automatisk
     * @return Leieberegning
     * @throws Exception
     */
    public function justerSatser(Fraction $justeringsfaktor, bool $automatisk = false): Leieberegning
    {
        list('justeringsfaktor' => $justeringsfaktor, 'automatisk' => $automatisk)
            = $this->app->before($this, __FUNCTION__, [
            'justeringsfaktor' => $justeringsfaktor, 'automatisk' => $automatisk
        ]);
        if($justeringsfaktor->compare('<>', 1)) {
            $this->settLeiePerObjekt($justeringsfaktor->multiply($this->hentLeiePerObjekt())->asDecimal());
            $this->settLeiePerKontrakt($justeringsfaktor->multiply($this->hentLeiePerKontrakt())->asDecimal());
            $this->settLeiePerKvadratmeter($justeringsfaktor->multiply($this->hentLeiePerKvadratmeter())->asDecimal());

            $spesialregler = $this->hentSpesialregler();
            /** @var object $regel */
            foreach ($spesialregler as $regel) {
                if(empty($regel->unntatt_fra_leiejustering)) {
                    $regel->sats = $justeringsfaktor->multiply($regel->sats)->asDecimal();
                }
            }
            $this->settSpesialregler($spesialregler);
            $this->oppdaterSatsHistorikk($automatisk);
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }
}