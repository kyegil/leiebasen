<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 03/08/2020
 * Time: 11:26
 */

namespace Kyegil\Leiebasen\Modell\Leieobjekt;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Personsett;
use Kyegil\Leiebasen\Samling;
use stdClass;

/**
 * Class Skade
 * @package Kyegil\Leiebasen\Modell\Leieobjekt
 *
 * @property int $id
 * @property Leieobjekt|null $leieobjekt
 * @property Bygning|null $bygning
 * @property Person|null $registrerer
 * @property string $registrererNavn
 * @property DateTime $registrert
 * @property string $skade
 * @property string $beskrivelse
 * @property DateTime|null $utført
 * @property Person|null $sluttregistrerer
 * @property string $sluttregistrererNavn
 * @property string $sluttrapport
 * @property Sakstråd|null $beboerstøtte
 *
 * @method int hentId()
 * @method Leieobjekt|null hentLeieobjekt()
 * @method Bygning|null hentBygning()
 * @method Person|null hentRegistrerer()
 * @method string hentRegistrererNavn()
 * @method DateTime hentRegistrert()
 * @method string hentSkade()
 * @method string hentBeskrivelse()
 * @method DateTime hentUtført()
 * @method Person hentSluttregistrerer()
 * @method string hentSluttregistrererNavn()
 * @method string hentSluttrapport()
 * @method Sakstråd|null hentBeboerstøtte()
 *
 * @method $this settLeieobjekt(?Leieobjekt|null $leieobjekt)
 * @method $this settBygning(?Bygning|null $bygning)
 * @method $this settRegistrerer(?Person $registrerer = null)
 * @method $this settRegistrert(DateTime $registrert)
 * @method $this settSkade(string $skade)
 * @method $this settBeskrivelse(string $beskrivelse)
 * @method $this settUtført(DateTime $utført)
 * @method $this settSluttregistrererNavn(string $sluttregistrererNavn)
 * @method $this settSluttrapport(string $sluttrapport)
 * @method $this settBeboerstøtte(?Sakstråd|null $beboerstøtte)
*/
class Skade extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'skader';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'leieobjektnr'    => [
                'type'  => Leieobjekt::class,
                'target' => 'leieobjekt',
                'allowNull' => true,
            ],
            'bygning'    => [
                'type'  => Bygning::class,
                'allowNull' => true,
            ],
            'registrerer_id'    => [
                'type'  => Person::class,
                'target' => 'registrerer'
            ],
            'registrerer'    => [
                'type'  => 'string',
                'target' => 'registrerer_navn'
            ],
            'registrert'    => [
                'type'  => 'datetime',
                'allowNull' => true
            ],
            'skade'    => [
                'type'  => 'string'
            ],
            'beskrivelse'    => [
                'type'  => 'string'
            ],
            'utført'    => [
                'type'  => 'date'
            ],
            'sluttregistrerer_id'    => [
                'type'  => Person::class,
                'target' => 'sluttregistrerer'
            ],
            'sluttregistrerer'    => [
                'type'  => 'string',
                'target' => 'sluttregistrerer_navn'
            ],
            'sluttrapport'    => [
                'type'  => 'string'
            ],
            'beboerstøtte_id'    => [
                'type'  => Sakstråd::class,
                'allowNull' => true,
                'target' => 'beboerstøtte'
            ]
        ];
    }

    /**
     * @param string $property
     * @return mixed
     * @throws Exception
     */
    public function hent($egenskap = null)
    {
        if($egenskap == 'kategorier') {
            return $this->hentKategorier();
        }
        return parent::hent($egenskap);
    }

    /**
     * @param $egenskap
     * @param $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($egenskap, $verdi): Modell
    {
        if($egenskap == 'kategorier') {
            return $this->settKategorier($verdi);
        }
        return parent::sett($egenskap, $verdi);
    }

    /**
     * @return Samling
     * @throws Exception
     */
    public function hentKategorier()
    {
        if(!isset($this->samlinger->kategorier)) {
            $this->samlinger->kategorier = $this->app->hentSamling(Leieobjekt\Skade\Kategori::class)
                ->leggTilFilter(['skadeid' => $this->hentId()])
                ->låsFiltre();
        }
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $this->samlinger->kategorier;
    }

    /**
     * @param string[] $kategorier
     * @return $this
     * @throws Exception
     */
    public function settKategorier(array $kategorier = null)
    {
        settype($kategorier, 'array');
        $kategorier = array_map(function($kategori) {
            return trim(strtolower($kategori));
        }, $kategorier);
        $kategorier = array_unique($kategorier);
        $beholdKategorier = [];
        $eksisterendeKategorier = $this->hentKategorier();
        /** @var Leieobjekt\Skade\Kategori $kategori */
        foreach($eksisterendeKategorier as $kategori) {
            if(in_array(strtolower($kategori->kategori), $kategorier)) {
                /** Kategorien skal beholdes */
                $beholdKategorier[] = strtolower($kategori->kategori);
            }
            else{
                /** Kategorien skal slettes */
                $kategori->slett();
            }
        }
        $nyeKategorier = array_diff($kategorier, $beholdKategorier);

        /** @var  $nyKategori */
        foreach($nyeKategorier as $nyKategori) {
            $this->app->nyModell(Leieobjekt\Skade\Kategori::class, (object)[
                'kategori' => $nyKategori,
                'skade' => $this
            ]);
        }
        $this->samlinger->kategorier = null;
        return $this;
    }

    /**
     * @param stdClass $parametere
     * @return $this
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Modell
    {
        if(isset($parametere->registrerer) && $parametere->registrerer instanceof Person) {
            $parametere->registrerer_navn = $parametere->registrerer->hentNavn();
        }
        return parent::opprett($parametere);
    }

    /**
     * @param Person|null $privatperson
     * @return $this
     * @throws Exception
     */
    public function opprettBeboerStøtte(Person $privatperson = null)
    {
        if($this->hentBeboerstøtte()) {
            throw new Exception('Det eksisterer allerede en sakstråd for skade ' . $this->hentId());
        }
        /** @var Sakstråd $sak */
        $sak = $this->app->nyModell(Sakstråd::class, (object)[
            'saksref' => 'skademelding-' . $this->hentId(),
            'tittel' => $this->hentSkade(),
            'status' => Sakstråd::STATUS_ÅPEN,
            'område' => 'drift',
            'skademelding' => $this->hentId(),
            'privat_person' => $privatperson
        ]);
        $this->settBeboerstøtte($sak);
        $sak->abonner($this->hentRegistrerer());
        foreach($this->hentAutoAbonnenter() as $abonnent) {
            $sak->abonner($abonnent);
        }
        $sak->abonner($privatperson);
        $sak->leggTilInnlegg((object)[
            'saksopplysning' => false,
            'privat' => false,
            'avsluttet' => false,
            'innhold' => $this->hentBeskrivelse(),
            'avsender' => $this->hentRegistrerer()
        ]);
        $sak->sendEpost();
        return $this;
    }

    /**
     * @param Person $sluttregistrerer
     * @return $this
     * @throws Exception
     */
    public function settSluttRegistrerer(Person $sluttregistrerer)
    {
        parent::sett('sluttregistrerer', $sluttregistrerer);
        return $this->settSluttregistrererNavn($sluttregistrerer->hentNavn());
    }

    /**
     * @return Person[]
     * @throws Exception
     */
    public function hentAutoAbonnenter(): array
    {
        if(!isset($this->samlinger->autoAbonnenter)) {
            $this->samlinger->autoAbonnenter = [];
            $leieobjekt = $this->hentLeieobjekt();
            $bygning = $this->hentBygning();
            $leieobjektId = $leieobjekt? $leieobjekt->hentId() : null;
            $bygningsId = $bygning ? $bygning->hentId() : null;
            $områdeIder = [];
            if ($leieobjekt) {
                foreach($leieobjekt->hentOmråder() as $område) {
                    $områdeIder[] = $område->hentId();
                }
            }
            else if ($bygning) {
                foreach($bygning->hentOmråder() as $område) {
                    $områdeIder[] = $område->hentId();
                }
            }

            /** @var Personsett $autoAbonnenter */
            $autoAbonnenter = $this->app->hentSamling(Person::class)
                ->leggTilInnerJoin(
                    Modell\Eav\Verdi::hentTabell(),
                    '`' . Modell\Eav\Verdi::hentTabell() . '`.`objekt_id` = `' . Person::hentTabell() . '`.`' . Person::hentPrimærnøkkelfelt() . '`'
                )
                ->leggTilFilter([
                    '`' . Modell\Eav\Verdi::hentTabell() . '`.`modell`' => Person::class
                ])
                ->leggTilFilter([
                    '`' . Modell\Eav\Verdi::hentTabell() . '`.`kode`' => 'brukerprofil_preferanser'
                ])
                ->leggTilFilter([
                    '`' . Modell\Eav\Verdi::hentTabell() . '`.`verdi` LIKE' => '%skade_auto_abonnement%'
                ])
                ->låsFiltre();
            foreach ($autoAbonnenter as $person) {
                $skadeAutoAbonnement = $person->hentBrukerProfilPreferanse('skade_auto_abonnement') ?? (object)[
                        'leieobjekter' => [],
                        'bygninger' => [],
                        'områder' => [],
                    ];
                if (in_array($leieobjektId, $skadeAutoAbonnement->leieobjekter)) {
                    $this->samlinger->autoAbonnenter[] = $person;
                }
                else if (in_array($bygningsId, $skadeAutoAbonnement->bygninger)) {
                    $this->samlinger->autoAbonnenter[] = $person;
                }
                else if (array_intersect($områdeIder, $skadeAutoAbonnement->områder)) {
                    $this->samlinger->autoAbonnenter[] = $person;
                }
            }
        }
        return $this->samlinger->autoAbonnenter;
    }
}