<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 03/08/2020
 * Time: 11:26
 */

namespace Kyegil\Leiebasen\Modell\Leieobjekt\Skade;


use Kyegil\Leiebasen\Modell\Leieobjekt\Skade;

/**
 * Class Kategori
 * @package Kyegil\Leiebasen\Modell\Leieobjekt\Skade
 *
 * @property int $id
 * @property Skade $skade
 * @property string $kategori
 *
 * @method int hentId()
 * @method Skade hentSkade()
 * @method string hentKategori()
 *
 * @method $this settSkade(Skade $skade)
 * @method $this settKategori(string $kategori)
 */
class Kategori extends \Kyegil\Leiebasen\Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'skadekategorier';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /**
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'skadeid'    => [
                'type'  => Skade::class,
                'target' => 'skade'
            ],
            'kategori'    => [
                'type'  => 'string'
            ]
        ];
    }
}