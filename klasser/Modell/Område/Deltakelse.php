<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 03/08/2020
 * Time: 11:26
 */

namespace Kyegil\Leiebasen\Modell\Område;


use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Område;
use stdClass;

/**
 * Class Deltakelse
 * @package Kyegil\Leiebasen\Modell\Område
 *
 * @property int $id
 * @property Område $område
 * @property string $medlemType
 * @property int $medlemId
 * @property Leieobjekt|Bygning $medlem
 *
 * @method int hentId()
 * @method Område hentOmråde()
 * @method string hentMedlemType()
 * @method int hentMedlemId()
 *
 * @method $this settOmråde(Område $område)
 * @method $this settMedlemId(int $medlem)
*/
class Deltakelse extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'område_deltakelse';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Deltakelsesett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'område_id'    => [
                'target'  => 'område',
                'type'  => Område::class,
                'rawDataContainer' => Område::hentTabell()
            ],
            'medlem_type'    => [
                'type'  => 'string'
            ],
            'medlem_id'    => [
                'type'  => 'integer'
            ]
        ];
    }

    /**
     * @param stdClass $parametere
     * @return Modell
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Modell
    {
        if(isset($parametere->medlem) and $parametere->medlem instanceof Modell) {
            $parametere->medlem_type = get_class($parametere->medlem);
        }
        return parent::opprett($parametere);
    }

    /**
     * @param string $egenskap
     * @return mixed
     * @throws Exception
     */
    public function hent($egenskap = null)
    {
        if ($egenskap == 'medlem') {
            return $this->hentMedlem();
        }
        return parent::hent($egenskap);
    }

    /**
     * @return Leieobjekt|Bygning
     * @throws Exception
     */
    public function hentMedlem(): Modell
    {
        $medlemType = $this->hent('medlem_type');
        $medlemId = $this->hent('medlem_id');
        /** @var Bygning|Leieobjekt $modell */
        $modell = $this->app->hentModell($medlemType, $medlemId);
        $modellTabell = $modell::hentTabell();
        if(property_exists($this->rawData, $modellTabell)) {
            $preloadedValues = $modell->mapNamespacedRawData($this->rawData->$modellTabell);
            $modell->setPreloadedValues($preloadedValues);
            $this->coreModelRepository->save($modell);
        }
        return $modell;
    }

    /**
     * @param string $egenskap
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($egenskap, $verdi): Modell
    {
        if ($egenskap == 'medlem') {
            return $this->settMedlem($verdi);
        }
        return parent::sett($egenskap, $verdi);
    }

    /**
     * @param Bygning|Leieobjekt $medlem
     * @return Deltakelse
     * @throws Exception
     */
    public function settMedlem(Modell $medlem): Deltakelse
    {
        return $this
            ->sett('medlem_type', get_class($medlem))
            ->sett('medlem_id', $medlem->hentId());
    }
}