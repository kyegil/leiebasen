<?php

namespace Kyegil\Leiebasen\Modell\Område;


use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Sett;

class Deltakelsesett extends Sett
{
    protected string $model = Deltakelse::class;

    /** @var Deltakelse[] */
    protected ?array $items = null;

    public function __construct(string $model, array $di)
    {
        parent::__construct($model, $di);
        $this
            ->leggTilLeftJoin(Område::hentTabell(), Deltakelse::hentTabell() . '.`område_id` = ' . Område::hentTabell() . '.' . Område::hentPrimærnøkkelfelt())
            ->leggTilModell(Område::class)

            ->leggTilLeftJoin(Leieobjekt::hentTabell(), '`' . Deltakelse::hentTabell() . '`.`medlem_type`="' . addslashes(Leieobjekt::class) . '" AND `' . Deltakelse::hentTabell() . '`.`medlem_id` = `' . Leieobjekt::hentTabell() . '`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '`')
            ->leggTilModell(Leieobjekt::class)

            ->leggTilLeftJoin(Bygning::hentTabell(), '`' . Deltakelse::hentTabell() . '`.`medlem_type`="' . addslashes(Bygning::class) . '" AND `' . Deltakelse::hentTabell() . '`.`medlem_id` = `' . Bygning::hentTabell() . '`.`' . Bygning::hentPrimærnøkkelfelt() . '`')
            ->leggTilModell(Bygning::class)
        ;
    }
}