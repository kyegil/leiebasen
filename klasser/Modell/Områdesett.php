<?php

namespace Kyegil\Leiebasen\Modell;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Sett;

/**
 * Class Områdesett
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Område current()
 */
class Områdesett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Område::class;

    /**
     * @var Område[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }
}