<?php

namespace Kyegil\Leiebasen\Modell;


use Kyegil\Leiebasen\Modell;

/**
 * Class Melding
 * @package Kyegil\Leiebasen\Modell
 *
 * @property integer $id
 * @property string $medium
 * @property integer $prioritet
 * @property string $til
 * @property string $emne
 * @property string $innhold
 * @property object|null $headers
 * @property string|null $params
 *
 * @method int hentId()
 * @method string hentMedium()
 * @method int hentPrioritet()
 * @method string hentTil()
 * @method string hentEmne()
 * @method string hentInnhold()
 * @method object|null hentHeaders()
 * @method string|null hentParams()
 *
 * @method $this settMedium(string $medium)
 * @method $this settPrioritet(int $prioritet)
 * @method $this settTil(string $til)
 * @method $this settEmne(string $emne)
 * @method $this settInnhold(string $innhold)
 * @method $this settHeaders(object|null $headers)
 * @method $this settParams(string|null $param)
 */
class Melding extends Modell
{
    const MEDIUM_EPOST = 'epost';

    const MEDIUM_SMS = 'sms';
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'melding_kø';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Meldingsett::class;

    /** @var array|null one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'medium'    => [
                'type'  => 'string'
            ],
            'prioritet'    => [
                'type'  => 'integer'
            ],
            'til'    => [
                'type'  => 'string'
            ],
            'emne'    => [
                'type'  => 'string'
            ],
            'innhold'    => [
                'type'  => 'string'
            ],
            'headers'    => [
                'type'  => 'json',
                'allowNull' => true,
            ],
            'params'    => [
                'type'  => 'string',
                'allowNull' => true,
            ],
        ];
    }
}