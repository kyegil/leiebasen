<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 01/12/2020
 * Time: 14:07
 */

namespace Kyegil\Leiebasen\Modell\Innbetaling;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Konto;
use Kyegil\Leiebasen\Modell\Kontosett;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Ocr\Transaksjon;
use Kyegil\Leiebasen\Vedlikehold;
use stdClass;
use Throwable;

/**
 * Class Delbeløp
 * @package Kyegil\Leiebasen\Modell\Innbetaling
 *
 * @property int $innbetalingsid
 * @property Innbetaling $innbetaling
 * @property Krav|true|null $krav True for utligning mot annen betaling, null dersom ikke avstemt
 * @property Leieforhold|null $leieforhold
 * @property int $kontoId
 * @property string $konto
 * @property float $beløp
 * @property Transaksjon|null $ocrTransaksjon
 * @property string $ref
 * @property DateTime $dato
 * @property string $merknad
 * @property string $betaler
 * @property string $register
 * @property DateTime $registrert
 * @property bool $erFremmedBeløp
 * @property Delbeløp|null $tilbakeføring
 *
 * @method int hentInnbetalingsid()
 * @method Innbetaling hentInnbetaling()
 * @method Krav|true|null hentKrav() True for utligning mot annen betaling, null dersom ikke avstemt
 * @method Leieforhold|null hentLeieforhold()
 * @method int hentKontoId()
 * @method string hentKonto()
 * @method float hentBeløp()
 * @method Transaksjon|null hentOcrTransaksjon()
 * @method string hentRef()
 * @method DateTime hentDato()
 * @method string hentMerknad()
 * @method string hentBetaler()
 * @method string hentRegistrerer()
 * @method DateTime hentRegistrert()
 * @method Delbeløp|null hentTilbakeføring()
 *
 * @method $this settTilbakeføring(?Delbeløp $tilbakeføring)
 */
class Delbeløp extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'innbetalinger';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'innbetalingsid';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Delbeløpsett::class;

    /** @var array one-to-many
     * @noinspection PhpDocFieldTypeMismatchInspection
     */
    protected static ?array $one2Many;

    /** @var array */
    protected static array $kontoIderVedKode;
    protected static array $kontoIderVedNavn;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array {
        return [
            'innbetalingsid' => [
                'type' => 'int'
            ],
            'innbetaling' => [
                'type' => Innbetaling::class,
                'rawDataContainer' => 'transaksjon',
            ],
            'krav' => [
                'type' => function(?int $kravId, Delbeløp $delBeløp) {
                    if($kravId > 0) {
                        /** @var Krav $krav */
                        $krav = $delBeløp->app->hentModell(Krav::class, $kravId);
                        $rawData = $delBeløp->getRawData();
                        if(property_exists($rawData, 'krav')) {
                            $preloadedValues = Krav::mapNamespacedRawData($rawData->krav);
                            $krav->setPreloadedValues($preloadedValues);
                            $delBeløp->app->modellOppbevaring->save($krav);
                        }
                        return $krav;
                    }
                    if(isset($kravId)){
                        return true;
                    }
                    return null;
                },
                'allowNull' => true,
                'toDbValue' => function($krav) {
                    if($krav instanceof Krav) {
                        return $krav->hentId();
                    }
                    return $krav === true ? '0' : null;
                }
            ],
            'leieforhold' => [
                'type' => Leieforhold::class,
                'allowNull' => true
            ],
            'beløp' => [
                'type' => 'string'
            ],
            'konto_id' => [
                'type' => 'int'
            ],
            'konto' => [
                'type' => 'string'
            ],
            'ocr_transaksjon'    => [
                'type'  => Transaksjon::class,
                'allowNull' => true
            ],
            'ref'    => [
                'type'  => 'string'
            ],
            'dato'    => [
                'type'  => 'date',
                'allowNull' => true
            ],
            'merknad'    => [
                'type'  => 'string'
            ],
            'betaler'    => [
                'type'  => 'string'
            ],
            'registrerer'    => [
                'type'  => 'string'
            ],
            'registrert'    => [
                'type'  => 'datetime'
            ]
        ];
    }

    /**
     * @param string|null $erFremmedBeløp
     * @return bool
     */
    public static function erFremmedBeløpFraStreng(?string $erFremmedBeløp): bool
    {
        return boolval($erFremmedBeløp);
    }

    /**
     * @param bool|null $erFremmedBeløp
     * @return string|null
     */
    public static function erFremmedBeløpTilStreng(?bool $erFremmedBeløp):? string {
        return $erFremmedBeløp ? '1' : null;
    }

    /**
     * @param stdClass $parametere
     * @return Delbeløp
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Modell
    {
        $parametere->registrerer = $parametere->registrerer ?? $this->app->bruker['navn'];
        if(!isset($parametere->konto_id) && isset($parametere->konto)) {
            if($parametere->konto == '0') {
                $parametere->konto_id = 0;
            }
            else {
                $parametere->konto_id = $this->hentKontoIdForNavn($parametere->konto);
            }
        }
        return parent::opprett($parametere);
    }

    /**
     * Splitt dette delbeløpet.
     *
     * Skiller ut en ny deltransaksjon pålydende angitt beløp,
     * og returnerer den nyopprettede delen pålydende dette beløpet.
     * Det nye delbeløpet tilhører samme transaksjon,
     * er avstemt likt med originalen,
     * og er likt som originalen for alle verdier angitt i hovedtabellen.
     *
     * Dersom beløpet er likt nåværende beløp, foretaes det ingen oppsplitting,
     * og eksisterende deltransaksjon returneres uendret.
     *
     * Eav-verdier blir ikke automatisk kopiert over.
     *
     * Det eksisterende delbeløpet har blitt redusert til gjenværende verdi, men er ellers uendret.
     *
     * @param float $beløp Beløpet kan ikke være 0 eller ha motsatt fortegn av eksisterende beløp.
     * Absolutt-verdien av beløpet kan ikke være større enn eksisterende absoluttverdi.
     * @return Delbeløp Det nye delbeløpet som ble utskilt i splitteoperasjonen pålydende angitt beløp.
     * @throws Exception
     * @noinspection PhpDocMissingThrowsInspection
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function splitt(float $beløp): Delbeløp
    {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $originalBeløp = $this->hentBeløp();
        $gjenværendeBeløp = $originalBeløp - $beløp;
        if($beløp == 0) {
            throw new Exception('Delbeløpet kan ikke splittes til 0');
        }
        if($beløp * $originalBeløp < 0) {
            throw new Exception('Delbeløpet kan ikke splittes til et beløp med motsatt fortegn');
        }
        if(abs($beløp) > abs($originalBeløp)) {
            throw new Exception('Delbeløpet kan ikke splittes til et høyere beløp');
        }
        $krav = $this->hentKrav();

        if ($originalBeløp == $beløp) {
            return $this;
        }
        else {
            try {
                $this->sett('beløp', $gjenværendeBeløp);
                $rawData = clone $this->getRawData()->{self::hentTabell()};
                unset($rawData->{self::hentPrimærnøkkelfelt()});
                $rawData->beløp = $beløp;
                /** @var Delbeløp $nyTransaksjonsdel */
                $nyTransaksjonsdel = $this->app->nyModell(Delbeløp::class, new stdClass());
                $nyTransaksjonsdel->saveRawValues($rawData);
                if ($nyTransaksjonsdel->hentBeløp() != $beløp) {
                    $nyTransaksjonsdel->slett();
                    throw new Exception("Det skjedde en feil under splitting av delbeløpet");
                }
            } catch (Throwable $e) {
                $this->sett('beløp', $originalBeløp);
                throw $e;
            }

            /*
             * Dersom dette delbeløpet har blitt tilbakeført
             * så må tilbakeføringen også splittes sånn at beløpene direkte motsvarer hverandre.
             *
             * De utskilte delene avstemmes ikke mot hverandre
             *
             * @var Delbeløp|null $tilbakeføring
             */
            $tilbakeføring = $this->hentTilbakeføring();
            if($tilbakeføring
                && ($gjenværendeBeløp - $tilbakeføring->hentBeløp() != 0)
            ) {
                $nyTilbakeføring = $tilbakeføring->splitt($gjenværendeBeløp - $tilbakeføring->hentBeløp());
                $nyTilbakeføring->sett('krav', null);
            }

            $this->hentInnbetaling()->nullstill();
            $this->nullstill();
        }


        if($krav instanceof Krav) {
            $krav->oppdaterUtestående();
        }

        return $this->app->post($this, __FUNCTION__, $nyTransaksjonsdel, $args);
    }

    /**
     * Avstem delbeløpet mot et sett med krav eller tilbakeføringer
     *
     * @param Kravsett|Delbeløpsett|Krav[]|Delbeløp[] $kontrasett Liste over krav eller transaksjoner delbeløpet kan avstemmes mot.
     * @param float|null $maksbeløp Maksimalt beløp som skal brukes (ut ifra 0).
     * @return float det fordelte beløpet
     * @throws Exception
     */
    public function avstem($kontrasett, ?float $maksbeløp = null): float
    {
        $resultat = 0;
        $sumForFordeling = $this->hentBeløp();
        if(isset($maksbeløp)) {
            $sumForFordeling
                = abs($sumForFordeling) < abs($maksbeløp)
                ? $sumForFordeling
                : $maksbeløp;
        }

        /** @var Krav|Delbeløp $kontra */
        foreach($kontrasett as $kontra ) {
            if( $sumForFordeling != 0 ) {
                /*
                 * For kompabilitet med gammel kode vil vi akseptere true eller Innbetalinger i kontrasettet
                 */
                if( $kontra === true || $kontra instanceof Innbetaling) {
                    /** @var Delbeløpsett $delbeløpsett */
                    $delbeløpsett = $this->app->hentSamling(Delbeløp::class)
                        ->leggTilEavVerdiJoin('tilbakeføring')
                        ->leggTilFilter(['`'. Delbeløp::hentTabell(). '`.`leieforhold`' => $this->hentLeieforhold()])
                        ->leggTilFilter(['`'. Delbeløp::hentTabell(). '`.`krav`' => 0])
                        ->leggTilFilter(['`tilbakeføring`.`verdi` IS NULL']);
                    if($this->hentBeløp() < 0) {
                        $delbeløpsett->leggTilFilter(['`'. Delbeløp::hentTabell(). '`.`beløp` >' => 0]);
                    }
                    else {
                        $delbeløpsett->leggTilFilter(['`'. Delbeløp::hentTabell(). '`.`beløp` <' => 0]);
                    }
                    $delbeløpsett->låsFiltre();
                    $kontra = $delbeløpsett->hentFørste();
                }

                if ($kontra instanceof Delbeløp) {
                    $fordeling = $this->avstemMotBetaling($kontra, $sumForFordeling);
                    $sumForFordeling -= $fordeling;
                    $resultat += $fordeling;
                }
                if ($kontra instanceof Krav) {
                    $fordeling = $this->avstemMotKrav($kontra, $sumForFordeling);
                    $sumForFordeling -= $fordeling;
                    $resultat += $fordeling;
                }
            }
        }

        return $resultat;
    }

    /**
     * Avstemmer delbeløpet mot et sett med krav eller tilbakeføringer
     *
     * @param Delbeløp $kontraDelbeløp
     * @param float|null $maksbeløp Maksimalt beløp som skal brukes (ut ifra 0).
     *          Beløpet kommer i tillegg til evt eksisterende delbeløp
     *          Dersom null vil hele den ikke-konterte delen av betalinga føres på det angitte leieforholdet.
     *          Maksbeløp brukes kun dersom motkrav er oppgitt.
     * @return float det fordelte beløpet
     * @throws Exception
     */
    public function avstemMotBetaling(Delbeløp $kontraDelbeløp, ?float $maksbeløp = null): float
    {
        $tilgjengeligBeløp = -$kontraDelbeløp->hentBeløp();

        if($this->hentBeløp() < 0 && $tilgjengeligBeløp < 0) {
            return 0 - $kontraDelbeløp->avstemMotBetaling($this, $maksbeløp);
        }
        // Utlikningsbeløpet må være innenfor kravbeløpet og delbeløpet
        $utlikningsbeløp = min($tilgjengeligBeløp, $this->hentBeløp());
        if($maksbeløp !== null) {
            $utlikningsbeløp = min($utlikningsbeløp, max(0, $maksbeløp));
        }

        /*
         * Motkravet må ha riktig fortegn,
         * og ikke allerede være del av en tilbakeføring
         */
        if($tilgjengeligBeløp > 0 && $utlikningsbeløp > 0
            && !$this->hentTilbakeføring()
            && !$kontraDelbeløp->erTilbakeføring()
        ) {
            $nyDelbetaling = $this->splitt($utlikningsbeløp);
            $nyKontra = $kontraDelbeløp->splitt(-$utlikningsbeløp);
            $nyDelbetaling->sett('krav', true);
            $nyKontra->sett('krav', true);
            $nyDelbetaling->settTilbakeføring($nyKontra);

            Vedlikehold::getInstance($this->app)->kontrollerTilbakeføringer();
            return $utlikningsbeløp;
        }
        return 0;
    }

    /**
     * Avstemmer delbeløpet mot et krav
     *
     * @param Krav $krav Krav som delbeløpet kan avstemmes mot.
     * @param float|null $maksbeløp Maksimalt beløp som skal brukes (ut ifra 0).
     * @return float det fordelte beløpet
     * @throws Exception
     */
    public function avstemMotKrav(Krav $krav, ?float $maksbeløp = null): float
    {
        $utestående =  $krav->hentUtestående();
        // Utlikningsbeløpet må være innenfor kravbeløpet og delbeløpet
        $beløp
            = abs( $utestående) < abs($this->hentBeløp())
            ?  $utestående
            : $this->hentBeløp();

        // Dersom maksbeløp er angitt, vil utlikningsbeløpet reduserers til dette
        if($maksbeløp !== null) {
            $beløp
                = abs($beløp) < abs($maksbeløp)
                ? $beløp
                : $maksbeløp;
        }

        // Motkravet må ha riktig fortegn
        if($this->hentBeløp() *  $utestående > 0) {
            if($beløp != 0) {
                if($this->erAvstemt()) {
                    $this->fjernAvstemming();
                }
                $nyDelbetaling = $this->splitt($beløp);
                $nyDelbetaling->sett('krav', $krav);
                $krav->oppdaterUtestående();
            }
        }

        return $beløp;
    }

    /**
     * @param Krav|null $krav
     * @return $this
     * @throws Exception
     * @deprecated
     * @see avstemMotKrav()
     */
    public function settKrav(?Krav $krav): Delbeløp
    {
        return $this->sett('krav', $krav);
    }

    /**
     * Returnerer id-nummeret for en bankkonto basert på kode
     *
     * Dersom kontoen ikke finnes, returnereres kontoen med kode `annet`
     *
     * @param string|null $kode
     * @return int|int[]
     * @throws Exception
     */
    public function hentKontoIdForKode(?string $kode = null)
    {
        if(!isset(self::$kontoIderVedKode)) {
            self::$kontoIderVedKode = [];
            self::$kontoIderVedNavn = [];
            /** @var Kontosett $kontoer */
            $kontoer = $this->app->hentSamling(Konto::class);
            foreach ($kontoer as $konto) {
                self::$kontoIderVedKode[$konto->hentKode()] = $konto->hentId();
                self::$kontoIderVedNavn[$konto->hentNavn()] = $konto->hentId();
            }
        }
        if (isset($kode)) {
            return self::$kontoIderVedKode[$kode] ?? self::$kontoIderVedKode['annet'];
        }
        return self::$kontoIderVedKode;
    }

    /**
     * Returnerer id-nummeret for en bankkonto med dette navnet
     *
     * Dersom kontoen ikke finnes, returnereres kontoen med kode `annet`
     *
     * @param string $navn
     * @return int|int[]
     * @throws Exception
     */
    public function hentKontoIdForNavn(string $navn)
    {
        $this->hentKontoIdForKode();
        return self::$kontoIderVedNavn[$navn] ?? self::$kontoIderVedKode['annet'];
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function hentErFremmedBeløp(): bool
    {
        return (bool)$this->hent('er_fremmed_beløp');
    }

    /**
     * @param Leieforhold|null $leieforhold
     * @return Delbeløp
     * @throws Exception
     */
    public function settLeieforhold(?Leieforhold $leieforhold): Delbeløp
    {
        if($leieforhold) {
            $this->sett('er_fremmed_beløp', null);
        }
        return $this->sett('leieforhold', $leieforhold);
    }

    /**
     * @param bool|null $erFremmedBeløp
     * @return Delbeløp
     * @throws Exception
     */
    public function settErFremmedBeløp(?bool $erFremmedBeløp): Delbeløp
    {
        if($erFremmedBeløp) {
            $this->sett('krav', null)->sett('leieforhold', null);
        }
        return $this->sett('er_fremmed_beløp', $erFremmedBeløp ?: null);
    }

    /**
     * Dersom dette er en utbetaling som utgjør en tilbakeføring av et innbetalt beløp
     * så returnereres det originale innbetalte delbeløpet som har blitt tilbakeført
     *
     * @return Delbeløp|null
     * @throws Exception
     */
    public function hentTilbakeførtDelbeløp(): ?Delbeløp
    {
        if($this->hentBeløp() >= 0) {
            return null;
        }
        /** @var Delbeløpsett $delbeløpsett */
        $delbeløpsett = $this->app->hentSamling(Delbeløp::class)
            ->leggTilEavFelt('tilbakeføring')
            ->leggTilFilter(['`tilbakeføring`.`verdi`' => $this->hentId()])
            ->låsFiltre();
        ;
        /** @var Delbeløp|null $tilbakeførtDelbeløp */
        $tilbakeførtDelbeløp = $delbeløpsett->hentFørste();
        return $tilbakeførtDelbeløp;
    }

    /**
     * Returnerer sann dersom dette er en utbetaling som utgjør en tilbakeføring av et innbetalt beløp
     *
     * @return bool
     * @throws Exception
     */
    public function erTilbakeføring(): bool
    {
        return (bool)$this->hentTilbakeførtDelbeløp();
    }

    /**
     *
     * @return $this
     * @throws Exception
     */
    public function fjernAvstemming(): Delbeløp
    {
        $krav = $this->hentKrav();
        if($krav === true) {
            $tilbakeført = $this->hentTilbakeførtDelbeløp();
            if($tilbakeført) {
                return $tilbakeført->fjernAvstemming();
            }
            $tilbakeføring = $this->hentTilbakeføring();
            $this->settTilbakeføring(null);
            if($tilbakeføring) {
                $tilbakeføring->sett('krav', null);
            }
        }
        if($krav) {
            $this->sett('krav', null);
        }
        if ($krav instanceof Krav) {
            $krav->oppdaterUtestående();
        }
        return $this;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function erAvstemt()
    {
        return (bool)$this->hent('krav');
    }

    /**
     * @return Krav|Delbeløp|null
     */
    public function hentAvstemming(): ?Modell {
        $krav = $this->hentKrav();
        if($krav === true) {
            return $this->hentTilbakeføring();
        }
        return $krav;
    }

    /**
     * @return void
     * @throws Exception
     */
    public function slett()
    {
        $this->fjernAvstemming();
        parent::slett();
    }
}