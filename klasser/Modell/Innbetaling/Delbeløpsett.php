<?php

namespace Kyegil\Leiebasen\Modell\Innbetaling;


use Exception;
use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Sett;

/**
 * Delbeløpsett
 * @package Kyegil\Leiebasen\Modell\Innbetaling
 *
 * @method Delbeløp current()
 */
class Delbeløpsett extends Sett
{
    /** @var string */
    protected string $model = Delbeløp::class;

    /** @var Delbeløp[] */
    protected ?array $items = null;

    /**
     * @param string $model
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $model, array $di)
    {
        parent::__construct($model, $di);
    }

    /**
     * Legg til LEFT JOIN for krav
     *
     * @return Delbeløpsett
     */
    public function leggTilLeftJoinForKrav(): Delbeløpsett
    {
        return $this->leggTilLeftJoin(Leieforhold\Krav::hentTabell(), '`' . Delbeløp::hentTabell() . '`.`krav` = `' . Leieforhold\Krav::hentTabell() . '`.`' . Leieforhold\Krav::hentPrimærnøkkelfelt() . '`');
    }

    /**
     * Legg til Krav-modell
     *
     * @return Delbeløpsett
     * @throws Exception
     */
    public function leggTilKravModell(): Delbeløpsett
    {
        return $this->leggTilLeftJoinForKrav()
            ->leggTilModell(Leieforhold\Krav::class);
    }

    /**
     * Legg til LEFT JOIN for leieforhold
     *
     * @return Delbeløpsett
     */
    public function leggTilLeftJoinForLeieforhold(): Delbeløpsett
    {
        return $this->leggTilLeftJoin(Leieforhold::hentTabell(), '`' . Delbeløp::hentTabell() . '`.`leieforhold` = `' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`');
    }

    /**
     * Legg til join for å laste leieobjektet gjennom leieforholdet
     *
     * Join for leieforholdet må legges til først
     *
     * @param string|null $alias
     * @param string|null $LeieforholdAlias
     * @return Delbeløpsett
     */
    public function leggTilLeftJoinForLeieforholdLeieobjekt(string $alias = 'leieforholdobjekt', ?string $LeieforholdAlias = null): Delbeløpsett
    {
        $LeieforholdAlias = $LeieforholdAlias ?: Leieforhold::hentTabell();
        $this
            ->leggTilLeftJoin([$alias => Leieobjekt::hentTabell()], '`' . $LeieforholdAlias . '`.`leieobjekt` = `' . $alias . '`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '`')
        ;
        return $this;
    }

    /**
     * Legg til join for å laste leieobjektet gjennom leieforholdet
     *
     * Join for leieforholdet må legges til først
     *
     * @param string|null $alias
     * @param string|null $LeieforholdAlias
     * @return Delbeløpsett
     */
    public function leggTilLeftJoinForLeieforholdRegningsobjekt(string $alias = 'regningsobjekt', ?string $LeieforholdAlias = null): Delbeløpsett
    {
        $LeieforholdAlias = $LeieforholdAlias ?: Leieforhold::hentTabell();
        $this
            ->leggTilLeftJoin([$alias => Leieobjekt::hentTabell()], '`' . $LeieforholdAlias . '`.`regningsobjekt` = `' . $alias . '`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '`')
        ;
        return $this;
    }

    /**
     * Legg til Leieforhold-modell
     *
     * @return Delbeløpsett
     * @throws Exception
     */
    public function leggTilLeieforholdModell(): Delbeløpsett
    {
        return $this->leggTilLeftJoinForLeieforhold()
            ->leggTilModell(Leieforhold::class);
    }

    /**
     * Legg til LEFT JOIN for innbetaling
     *
     * @param string|null $alias
     * @return Delbeløpsett
     */
    public function leggTilLeftJoinForInnbetaling(?string $alias = null): Delbeløpsett
    {
        $alias = $alias ?: 'transaksjoner';
        return $this->leggTilLeftJoin(
            [$alias => Innbetaling::hentTabell()],
            Delbeløp::hentTabell() . '.`innbetaling` = `' . $alias . '`.`' . Innbetaling::hentPrimærnøkkelfelt() . '`'
        );
    }

    /**
     * Legg til Innbetaling-modell
     *
     * @return Delbeløpsett
     * @throws Exception
     */
    public function leggTilInnbetalingModell(): Delbeløpsett
    {
        return $this->leggTilLeftJoinForInnbetaling('transaksjoner')
            ->leggTilModell(Innbetaling::class, 'transaksjon', 'transaksjoner');
    }

    /**
     * Legg til nødvendige felter for å hente navnet på leieforholdet uten ytterligere db-spørringer
     *
     * @return Delbeløpsett
     */
    public function forhåndslastLeieforholdNavn(): Delbeløpsett
    {
        $this->leggTilUttrykk('leieforhold.leieforhold_ekstra', 'navn', '(' . Leieforholdsett::subQueryForNavn('leieforhold', Delbeløp::hentTabell()) . ')');
        return $this;
    }

    /**
     * @param array|null $felter
     * @return Delbeløpsett
     * @throws Exception
     */
    public function leggTilLeieforholdLeieobjektModell(?array $felter = ['navn', 'beskrivelse', 'etg', 'gateadresse']): Delbeløpsett
    {
        return $this
            ->leggTilLeftJoinForLeieforholdLeieobjekt()
            ->leggTilModell(
                Leieobjekt::class,
                'leieforhold.leieobjekt',
                'leieforholdobjekt',
                $felter
            );
    }

    /**
     * @param array|null $felter
     * @return Delbeløpsett
     * @throws Exception
     */
    public function leggTilLeieforholdRegningsobjektModell(?array $felter = ['navn', 'beskrivelse', 'etg', 'gateadresse']): Delbeløpsett
    {
        return $this
            ->leggTilLeftJoinForLeieforholdRegningsobjekt()
            ->leggTilModell(
                Leieobjekt::class,
                'leieforhold.regningsobjekt',
                'regningsobjekt',
                $felter
            );
    }

    /**
     * Legg til join for å laste regningen gjennom kravet
     *
     * Join for kravet må legges til først
     *
     * @param string|null $alias
     * @param string|null $LeieforholdAlias
     * @return Delbeløpsett
     */
    public function leggTilLeftJoinForKravRegning(?string $alias = null, ?string $kravAlias = null): Delbeløpsett
    {
        $this->leggTilLeftJoinForKrav();
        $alias = $alias ?: Regning::hentTabell();
        $kravAlias = $kravAlias ?: Leieforhold\Krav::hentTabell();
        $this
            ->leggTilLeftJoin([$alias => Regning::hentTabell()], '`' . $kravAlias . '`.`gironr` = `' . $alias . '`.`' . Regning::hentPrimærnøkkelfelt() . '`')
        ;
        return $this;
    }

    /**
     * @param array|null $felter
     * @return Delbeløpsett
     * @throws Exception
     */
    public function leggTilKravRegningModell(?array $felter = null): Delbeløpsett
    {
        return $this
            ->leggTilLeftJoinForKravRegning()
            ->leggTilModell(
                Regning::class,
                'krav.giroer',
                null,
                $felter
            );
    }

    /**
     * @param string|null $alias
     * @return Delbeløpsett
     */
    public function leggTilLeftJoinForLeieforholdRegningsperson(string $alias = 'regningsperson'): Delbeløpsett
    {
        $this->leggTilLeftJoinForLeieforhold();
        return $this->leggTilLeftJoin(
            [$alias => Person::hentTabell()],
            '`' . Leieforhold::hentTabell() . '`.`regningsperson` = `'
            . $alias . '`.`' . Person::hentPrimærnøkkelfelt() . '`'
        );
    }

    /**
     * @param array|null $felter
     * @return Delbeløpsett
     * @throws Exception
     */
    public function leggTilRegningspersonModell(?array $felter = null): Delbeløpsett
    {
        return $this
            ->leggTilLeftJoinForLeieforholdRegningsperson('regningsperson')
            ->leggTilModell(
                Person::class,
                'leieforhold.regningsperson',
                'regningsperson',
                $felter
            );
    }

}