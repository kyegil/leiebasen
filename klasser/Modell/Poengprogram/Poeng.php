<?php

namespace Kyegil\Leiebasen\Modell\Poengprogram;


use DateTime;
use DateTimeInterface;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Poengprogram;

/**
 * Class Poeng
 * @package Kyegil\Leiebasen\Modell\Poengprogram
 *
 * @property int $id
 * @property Poengprogram $program
 * @property string $type
 * @property DateTime $tid
 * @property DateTime|null $foreldes
 * @property Leieforhold|null $leieforhold
 * @property integer $verdi
 * @property string $tekst
 * @property string $registrerer
 *
 * @method int hentId()
 * @method Poengprogram hentProgram()
 * @method string hentType()
 * @method DateTime hentTid()
 * @method DateTime|null hentForeldes()
 * @method Leieforhold|null hentLeieforhold()
 * @method integer hentVerdi()
 * @method string hentTekst()
 * @method string hentRegistrerer()
 *
 * @method $this settProgram(Poengprogram $program)
 * @method $this settType(string $type)
 * @method $this settTid(DateTimeInterface $tid)
 * @method $this settForeldes(DateTimeInterface|null $foreldes)
 * @method $this settLeieforhold(Leieforhold|null $leieforhold)
 * @method $this settVerdi(integer $verdi)
 * @method $this settTekst(string $tekst)
 * @method $this settRegistrerer(string $registrerer)
*/
class Poeng extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'poeng';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Poengsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'program_id'    => [
                'target'  => 'program',
                'type'  => Poengprogram::class,
            ],
            'type'    => [
                'type'  => 'string'
            ],
            'tid'    => [
                'type'  => 'datetime'
            ],
            'foreldes'    => [
                'type'  => 'datetime',
                'allowNull' => true
            ],
            'leieforhold_id'    => [
                'target'  => 'leieforhold',
                'type'  => Leieforhold::class,
            ],
            'verdi'    => [
                'type'  => 'integer'
            ],
            'tekst'    => [
                'type'  => 'string'
            ],
            'registrerer'    => [
                'type'  => 'string'
            ],
        ];
    }
}