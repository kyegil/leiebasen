<?php

namespace Kyegil\Leiebasen\Modell\Poengprogram;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Sett;

/**
 * Class Poengsett
 * @package Kyegil\Leiebasen\Modell\Poengprogram
 *
 * @method Poeng current()
 */
class Poengsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Poeng::class;

    /**
     * @var Poeng[]
     */
    protected ?array $items = null;

    /**
     * @param string|null $alias
     * @return Poengsett
     */
    public function leggTilLeftJoinForLeieforhold(?string $alias = null): Poengsett
    {
        $alias = $alias ?: Leieforhold::hentTabell();
        return $this->leggTilLeftJoin(
            [$alias => Leieforhold::hentTabell()],
            '`' . Poeng::hentTabell() . '`.`leieforhold_id` = `'
            . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`'
        );
    }

    /**
     * @param string|null $alias
     * @return Poengsett
     */
    public function leggTilLeftJoinForLeieforholdLeieobjekt(?string $alias = null): Poengsett
    {
        $this->leggTilLeftJoinForLeieforhold();
        $alias = $alias ?: Leieobjekt::hentTabell();
        return $this->leggTilLeftJoin(
            [$alias => Leieobjekt::hentTabell()],
            '`' . Leieforhold::hentTabell() . '`.`leieobjekt` = `'
            . Leieobjekt::hentTabell() . '`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '`'
        );
    }

    /**
     * @param array|null $felter
     * @return Poengsett
     * @throws Exception
     */
    public function leggTilLeieforholdModell(?array $felter = null): Poengsett
    {
        return $this
            ->leggTilLeftJoinForLeieforhold()
            ->leggTilModell(Leieforhold::class,
                null,
                null,
                $felter
            );
    }

    /**
     * @param array|null $felter
     * @return Poengsett
     * @throws Exception
     */
    public function leggTilLeieobjektModell(?array $felter = ['navn', 'beskrivelse', 'etg', 'gateadresse']): Poengsett
    {
        return $this
            ->leggTilLeftJoinForLeieforholdLeieobjekt()
            ->leggTilModell(
                Leieforhold::class,
                'leieforhold.leieobjekt',
                null,
                $felter
            );
    }

    /**
     * @return Poengsett
     * @throws Exception
     */
    public function forhåndslastNavn(): Poengsett
    {
        $this->leggTilUttrykk('leieforhold.leieforhold_ekstra', 'navn', '(' . Leieforholdsett::subQueryForNavn() . ')');
        $this->leggTilLeftJoinForLeieforholdLeieobjekt()
            ->leggTilLeieforholdModell(['leieobjekt'])
            ->leggTilModell(
                Leieobjekt::class,
                'leieforhold.leieobjekt',
                null,
                ['navn', 'beskrivelse', 'etg', 'gateadresse']
            );

        return $this;
    }
}