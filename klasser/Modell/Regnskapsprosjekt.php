<?php

namespace Kyegil\Leiebasen\Modell;

use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav\Delkrav;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav\Delkravsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Område\Deltakelse as OmrådeDeltakelse;
use Kyegil\Leiebasen\Samling;
use stdClass;

/**
 * Class Regnskapsprosjekt
 *
 * Mulige konfigureringer:
 *
 ** summeringsEnhet string Enten 'kravtype.alle', 'kravtype.' + kravtype, eller 'delkrav.' + delkravkode
 ** segmentering string ''|'måned'|'uke'|'bygning'|'leieobjekt'|'kravtype'|'strømanlegg'|
 ** inkludert object
 *> * bygninger int[]|null (null for å inkludere alle)
 *> * leieobjekter int[]|null (null for å inkludere alle)
 *> * leieforhold int[]|null (null for å inkludere alle)
 *> * områder int[]|null (null for å inkludere alle)
 ** ekskludert object
 *> * bygninger int[]|null (null for å ekskludere alle)
 *> * leieobjekter int[]|null (null for å ekskludere alle)
 *> * leieforhold int[]|null (null for å ekskludere alle)
 *> * områder int[]|null (null for å ekskludere alle)
 *
 * @package Kyegil\Leiebasen\Modell
 *
 * @property integer $id
 * @property string $kode
 * @property string $navn
 * @property string $beskrivelse
 * @property stdClass|null $konfigurering
 *
 * @method int hentId()
 * @method string hentKode()
 * @method string hentNavn()
 * @method string hentBeskrivelse()
 *
 * @method $this settKode(string $kode)
 * @method $this settNavn(string $navn)
 * @method $this settBeskrivelse(string $beskrivelse)
 * @method $this settKonfigurering(stdClass|null $konfigurering)
 */
class Regnskapsprosjekt extends Modell
{
    const SEGMENTERING_MND = 'måned';
    const SEGMENTERING_UKE = 'uke';
    const SEGMENTERING_BYGNING = 'bygning';
    const SEGMENTERING_LEIEOBJEKT = 'leieobjekt';
    const SEGMENTERING_KRAVTYPE = 'kravtype';
    const SEGMENTERING_ANLEGG = 'strømanlegg';

    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'regnskapsprosjekter';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'kode'    => [
                'type'  => 'string'
            ],
            'navn'    => [
                'type'  => 'string'
            ],
            'beskrivelse'    => [
                'type'  => 'string'
            ],
            'konfigurering'    => [
                'type'  => 'json'
            ]
        ];
    }

    /**
     * Returnerer et array med alle tabellfeltene i regnskapet
     * Hvert objekt har egenskapene
     * * navn string
     * * type string 'streng'|'desimal'|'heltall'|'dato'
     * @return stdClass[]
     */
    public function hentFelter(): array
    {
        $resultat = [
            (object)[
                'navn' => 'dato',
                'type' => 'dato'
            ],
            (object)[
                'navn' => 'beskrivelse',
                'type' => 'streng'
            ],
            (object)[
                'navn' => 'beløp',
                'type' => 'desimal'
            ],
        ];
        return $resultat;
    }

    /**
     * Henter regnskapskonfigurering
     * @return stdClass
     * @throws Exception
     */
    public function hentKonfigurering(): stdClass
    {
        parent::hent('konfigurering');
        if (!$this->data->konfigurering) {
            $this->data->konfigurering = (object)[
                'summeringsEnhet' => 'kravtype.alle',
                'segmentering' => '',
                'inkludert' => (object)[
                    'bygninger' => null, // Null for å inkludere alle
                    'leieobjekter' => null, // Null for å inkludere alle
                    'leieforhold' => null, // Null for å inkludere alle
                    'områder' => null, // Null for å inkludere alle
                ],
                'ekskludert' => (object)[
                    'bygninger' => [], // [] for å ikke ekskludere noen
                    'leieobjekter' => [], // [] for å ikke ekskludere noen
                    'leieforhold' => [], // [] for å ikke ekskludere noen
                    'områder' => [], // Null for å inkludere alle
                ]
            ];
        }
        return $this->data->konfigurering;
    }

    /**
     * Kjører prosjektregnskapet, og returnerer data
     *
     * @param object $param
     * @return Kravsett|Delkravsett
     * @throws Exception
     */
    public function hentRegnskapsdata(object $param): Samling
    {
        /** @var DateTime|null $fom */
        $fom = $param->fom ?? null;
        /** @var DateTime|null $tom */
        $tom = $param->tom ?? null;

        $konfigurering = $this->hentKonfigurering();
        $summeringsEnhet = $this->hentSummeringsEnhet();
        $summeringsEnhet = explode('.', $summeringsEnhet);

        // Bestem hvilken modell vi skal bruke for regnskapet
        switch ($summeringsEnhet[0]) {
            case 'kravtype':
                /** @var Kravsett $samling */
                $samling = $this->app->hentSamling(Krav::class);
                if (in_array(strtolower($summeringsEnhet[1]), ['husleie', 'fellesstrøm', 'purregebyr', 'annet'])) {
                    $samling->leggTilFilter([
                        Krav::hentTabell() . '.type' => strtolower($summeringsEnhet[1])
                    ]);
                }
                break;
            case 'delkrav':
                $delkravtype = $this->lastDelkravPerKode($summeringsEnhet[1]);
                /** @var Delkravsett $samling */
                $samling = $this->app->hentSamling(Delkrav::class)
                    // Forbereder Leieforholdet som som det hentes fra Krav-modellen
                    ->leggTilLeftJoin(Leieobjekt::hentTabell(), Krav::hentTabell() . '.`leieobjekt` = ' . Leieobjekt::hentTabell() . '.' . Leieobjekt::hentPrimærnøkkelfelt())
                    ->leggTilModell(Leieobjekt::class, implode('.', [Krav::hentTabell(), Leieobjekt::hentTabell()]))

                    // Forbereder Strømanlegg-egenskapen for Krav-modellen
                    ->leggTilLeftJoin(Strømanlegg::hentTabell(), Krav::hentTabell() . '.`anleggsnr` = ' . Strømanlegg::hentTabell() . '.' . Strømanlegg::hentPrimærnøkkelfelt())
                    ->leggTilModell(Strømanlegg::class, implode('.', [Krav::hentTabell(), Strømanlegg::hentTabell()]))
                ;
                $samling->leggTilFilter([
                    Delkrav::hentTabell() . '.delkravtype' => $delkravtype->hentId()
                ]);

                break;
            default:
                throw new Exception('Finner ikke summerings-modell for prosjektregnskap ' . $this->hentId());
        }

        /** Left Join for evt filtrering på leieobjekter */
        $samling->leggTilLeftJoin(
            ['leieforholdobjekt' => Leieobjekt::hentTabell()],
            '`leieforholdobjekt`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '` = `' . Leieforhold::hentTabell() . '`.`leieobjekt`'
        );

        /** Left Join for evt filtrering på område via leieobjekt */
        $samling->leggTilLeftJoin(
            ['leieobjektområde' => OmrådeDeltakelse::hentTabell()],
            '`leieobjektområde`.`medlem_type` = "' . addslashes(Leieobjekt::class) . '" AND `leieobjektområde`.`medlem_id` = `leieforholdobjekt`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '`'
        );

        /** Left Join for evt filtrering på bygninger */
        $samling->leggTilLeftJoin(
            ['leieforholdbygning' => Bygning::hentTabell()],
            '`leieforholdbygning`.`' . Bygning::hentPrimærnøkkelfelt() . '` = `leieforholdobjekt`.`bygning`'
        );

        /** Left Join for evt filtrering på område via bygning */
        $samling->leggTilLeftJoin(
            ['bygningsområde' => OmrådeDeltakelse::hentTabell()],
            '`bygningsområde`.`medlem_type` = "' . addslashes(Bygning::class) . '" AND `bygningsområde`.`medlem_id` = `leieforholdbygning`.`' . Bygning::hentPrimærnøkkelfelt() . '`'
        );

        // For delkrav skal også krav-, leieforhold- og bygningsmodellene lastes
        if ($samling->hentModell() === Delkrav::class) {
            $samling->leggTilModell(
                Leieobjekt::class, implode('.', [
                    Krav::hentTabell(), Leieforhold::hentTabell(), Leieobjekt::hentTabell()
                ]),
                'leieforholdobjekt'
            );
            $samling->leggTilModell(
                Bygning::class, implode('.', [
                    Krav::hentTabell(), Leieforhold::hentTabell(), Leieobjekt::hentTabell(), Bygning::hentTabell()
                ]),
                'leieforholdbygning'
            );
        }
        // For krav skal også leieforhold- og bygningsmodellene lastes
        else if ($samling->hentModell() === Krav::class) {
            $samling->leggTilModell(
                Leieobjekt::class, implode('.', [
                    Leieforhold::hentTabell(), Leieobjekt::hentTabell()
                ]),
                'leieforholdobjekt'
            );
            $samling->leggTilModell(
                Bygning::class, implode('.', [
                    Leieforhold::hentTabell(), Leieobjekt::hentTabell(), Bygning::hentTabell()
                ]),
                'leieforholdbygning'
            );
        }

        // Lag datofilter
        $datoFilter = [];

        if ($fom) {
            $datoFilter[] = [
                'krav.kravdato >=' => $fom->format('Y-m-d')
            ];
        }
        if ($tom) {
            $datoFilter[] = [
                'krav.kravdato <=' => $tom->format('Y-m-d')
            ];
        }
        if ($datoFilter) {
            $samling->leggTilFilter($datoFilter);
        }

        // Lag filter for inkluderinger og eksludering ihht konfigurasjonene
        $inkludertFilter = [];
        $ekskludertFilter = [];

        /**
         * Behandle inkluderinger i hht konfig
         *
         * @var string $enhetsType
         * @var int[] $enhetsIder
         */
        foreach ($konfigurering->inkludert as $enhetsType => $enhetsIder) {
            switch ($enhetsType) {
                case 'bygninger':
                    if ($enhetsIder) {
                        $inkludertFilter[] = [
                            '`leieforhold.leieobjekt`.bygning IN(' . implode(',', $enhetsIder) . ')'
                        ];
                    }
                    break;
                case 'leieobjekter':
                    if ($enhetsIder) {
                        $inkludertFilter[] = [
                            Leieforhold::hentTabell() . '.leieobjekt IN(' . implode(',', $enhetsIder) . ')'
                        ];
                    }
                    break;
                case 'leieforhold':
                    if ($enhetsIder) {
                        $inkludertFilter[] = [
                            Krav::hentTabell() . '.leieforhold IN(' . implode(',', $enhetsIder) . ')'
                        ];
                    }
                    break;
                case 'områder':
                    if ($enhetsIder) {
                        $inkludertFilter[] = ['or' => [
                            'leieobjektområde.område_id IN(' . implode(',', $enhetsIder) . ')',
                            'bygningsområde.område_id IN(' . implode(',', $enhetsIder) . ')',
                        ]];
                    }
                    break;
                default:
                    break;
            }
        }

        /**
         * Behandle ekskluderinger i hht konfig
         *
         * @var string $enhetsType
         * @var int[] $enhetsIder
         */
        foreach ($konfigurering->ekskludert as $enhetsType => $enhetsIder) {
            switch ($enhetsType) {
                case 'bygninger':
                    if ($enhetsIder) {
                        $ekskludertFilter[] = [
                            '`leieforhold.leieobjekt`.bygning NOT IN(' . implode(',', $enhetsIder) . ')'
                        ];
                    }
                    break;
                case 'leieobjekter':
                    if ($enhetsIder) {
                        $ekskludertFilter[] = [
                            Leieforhold::hentTabell() . '.leieobjekt NOT IN(' . implode(',', $enhetsIder) . ')'
                        ];
                    }
                    break;
                case 'leieforhold':
                    if ($enhetsIder) {
                        $ekskludertFilter[] = [
                            Krav::hentTabell() . '.leieforhold NOT IN(' . implode(',', $enhetsIder) . ')'
                        ];
                    }
                    break;
                case 'områder':
                    if ($enhetsIder) {
                        $ekskludertFilter[] = ['or' => [
                            'leieobjektområde.område_id' => null,
                            'leieobjektområde.område_id NOT IN(' . implode(',', $enhetsIder) . ')',
                        ]];
                        $ekskludertFilter[] = ['or' => [
                            'bygningsområde.område_id' => null,
                            'bygningsområde.område_id NOT IN(' . implode(',', $enhetsIder) . ')',
                        ]];
                    }
                    break;
                default:
                    break;
            }
        }

        if ($inkludertFilter) {
            $samling->leggTilFilter(['or' => $inkludertFilter]);
        }

        if ($ekskludertFilter) {
            $samling->leggTilFilter($ekskludertFilter);
        }

        $samling->leggTilSortering('krav.id');
        $samling->leggTilSortering('krav.kravdato');

        $this->sorterSegmentering($samling);

        return $samling;
    }

    /**
     * @param string $kode
     * @return Delkravtype|null
     * @throws Exception
     */
    protected function lastDelkravPerKode(string $kode)
    {
        /** @var Delkravtype|null $delkrav */
        $delkrav = $this->app->hentSamling(Delkravtype::class)
            ->leggTilFilter(['kode' => $kode])
            ->hentFørste();
        return $delkrav;
    }

    /**
     * @return string ''|'måned'|'uke'|'bygning'|'leieobjekt'|'kravtype'|'strømanlegg'
     * @throws Exception
     */
    public function hentSegmentering(): string {
        return $this->hentKonfigurering()->segmentering ?? '';
    }

    /**
     * @param Krav|Delkrav $resultatModell
     * @return string
     * @throws Exception
     */
    public function hentSegmentVerdi(Modell $resultatModell): string
    {
        $krav = $resultatModell instanceof Delkrav ? $resultatModell->krav : $resultatModell;
        switch($this->hentSegmentering()) {
            case self::SEGMENTERING_MND:
                return $krav->kravdato->format('Y-m');
            case self::SEGMENTERING_UKE:
                return $krav->kravdato->format('Y-W');
            case self::SEGMENTERING_BYGNING:
                return $krav->leieforhold->leieobjekt->bygning ? (string)$krav->leieforhold->leieobjekt->bygning->hentId() : '';
            case self::SEGMENTERING_LEIEOBJEKT:
                return $krav->leieforhold->leieobjekt->hentId();
            case self::SEGMENTERING_KRAVTYPE:
                return $krav->type;
            case self::SEGMENTERING_ANLEGG:
                return $krav->anlegg ? (string)$krav->anlegg->hentId() : '';
            default:
                return '';
        }
    }

    /**
     * @param Kravsett|Delkravsett $samling
     * @return $this
     * @throws Exception
     */
    public function sorterSegmentering(Samling $samling): Regnskapsprosjekt
    {
        $segmentering = $this->hentSegmentering();

        switch ($segmentering) {
            case self::SEGMENTERING_ANLEGG:
                $samling->leggTilSortering('`krav`.`anleggsnr`');
                break;
            case self::SEGMENTERING_LEIEOBJEKT:
                $samling->leggTilSortering('`krav.leieforhold`.`leieobjekt`');
                break;
            case self::SEGMENTERING_BYGNING:
                $samling->leggTilSortering('`krav.leieforhold.leieobjekter`.`bygning`');
                break;
            case self::SEGMENTERING_KRAVTYPE:
                $samling->leggTilSortering('`krav`.`type`');
                if ($samling->hentModell() == Delkrav::class) {
                    $samling->leggTilSortering('`delkrav`.`delkravtype`');
                }
                break;
            default:
                break;
        }

        return $this;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function hentSummeringsEnhet(): string
    {
        $konfigurering = $this->hentKonfigurering();
        return $konfigurering->summeringsEnhet ?? 'kravtype.alle';
    }

}