<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 12/06/2020
 * Time: 10:18
 */

namespace Kyegil\Leiebasen\Modell;


use DateTimeInterface;
use DateTimeZone;
use Exception;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Leieregulerer;
use Kyegil\Leiebasen\Modell;
use stdClass;

/**
 * Class Delkravtype
 * @package Kyegil\Leiebasen
 *
 * @property int $id
 * @property int $orden
 * @property string $kode
 * @property string $navn
 * @property string $beskrivelse
 * @property string $kravtype
 * @property boolean $selvstendigTillegg
 * @property boolean $valgfritt
 * @property boolean $relativ
 * @property float $sats
 * @property boolean $aktiv
 * @property object|null $satsHistorikk
 * @property DateTimeInterface $satsOppdatert
 * @property bool|null $indeksreguleres
 *
 * @method int hentId()
 * @method int hentOrden()
 * @method string hentKode()
 * @method string hentNavn()
 * @method string hentBeskrivelse()
 * @method string hentKravtype()
 * @method float hentSats()
 * @method boolean hentSelvstendigTillegg()
 * @method boolean hentValgfritt()
 * @method boolean hentRelativ()
 * @method int hentAktiv()
 * @method object|null hentSatsHistorikk()
 * @method DateTimeInterface hentSatsOppdatert()
 * @method bool|null hentIndeksreguleres()
 *
 * @method $this settId(int $id)
 * @method $this settOrden(int $orden)
 * @method $this settKode(string $kode)
 * @method $this settNavn(string $navn)
 * @method $this settBeskrivelse(string $beskrivelse)
 * @method $this settKravtype(string $kravtype)
 * @method $this settSats(float $sats)
 * @method $this settSelvstendigTillegg(bool $selvstendigTillegg)
 * @method $this settValgfritt(bool $valgfritt)
 * @method $this settRelativ(bool $relativ)
 * @method $this settAktiv(bool $aktiv)
 * @method $this settSatsHistorikk(object $satsHistorikk)
 * @method $this settSatsOppdatert(DateTimeInterface $satsOppdatert)
 * @method bool|null settIndeksreguleres(bool $indeksreguleres)
 */
class Delkravtype extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'delkravtyper';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Delkravtypesett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'orden'    => [
                'type'  => 'integer'
            ],
            'kode'    => [
                'type'  => 'string'
            ],
            'navn'    => [
                'type'  => 'string'
            ],
            'beskrivelse'    => [
                'type'  => 'string'
            ],
            'kravtype'    => [
                'type'  => 'string'
            ],
            'selvstendig_tillegg'    => [
                'type'  => 'boolean'
            ],
            'valgfritt'    => [
                'type'  => 'boolean'
            ],
            'relativ'    => [
                'type'  => 'boolean'
            ],
            'sats'    => [
                'type'  => 'string',
            ],
            'sats_oppdatert'    => [
                'type'  => 'datetime',
            ],
            'sats_historikk'    => [
                'type'  => 'json',
                'allowNull'  => true
            ],
            'aktiv'    => [
                'type'  => 'boolean'
            ]
        ];
    }

    /**
     * @param stdClass $parametere
     * @return Modell
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Modell
    {
        if (isset($parametere->sats)) {
            $parametere->sats_oppdatert = date_create_immutable();
            $parametere->relativ = $parametere->relativ ?? false;
            $parametere->sats_historikk = (object)[
                $parametere->sats_oppdatert->format('c') => (object)[
                    'automatisk' => false,
                    'relativ' => $parametere->relativ,
                    'sats' => $parametere->sats
                ]
            ];
        }
        return parent::opprett($parametere);
    }

    /**
     * @param Fraction $justeringsfaktor
     * @param DateTimeInterface $tid
     * @param bool $automatisk
     * @return Delkravtype
     * @throws Exception
     */
    public function justerSats(Fraction $justeringsfaktor, bool $automatisk = false): Delkravtype
    {
        list('justeringsfaktor' => $justeringsfaktor, 'automatisk' => $automatisk)
            = $this->app->before($this, __FUNCTION__, [
            'justeringsfaktor' => $justeringsfaktor, 'automatisk' => $automatisk
        ]);
        if(!$this->hentRelativ() && $justeringsfaktor->compare('<>', 1)) {
            $this->settSats($justeringsfaktor->multiply($this->hentSats())->asDecimal());
            $this->oppdaterSatsHistorikk($automatisk);
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @param bool $automatisk
     * @return Delkravtype
     */
    public function oppdaterSatsHistorikk(bool $automatisk = false): Delkravtype
    {
        list('automatisk' => $automatisk)
            = $this->app->before($this, __FUNCTION__, [
            'automatisk' => $automatisk
        ]);
        $kpiHistorikk = [];
        try {
            $kpiHistorikk = Leieregulerer::hentKpiFraSSb();
        } catch (Exception $e) {
            $this->app->logger->warning($e->getMessage());
        }
        $tidspunkt = date_create_immutable('now', new DateTimeZone('utc'));
        $egenskap = $tidspunkt->format('c');
        $satsHistorikk = $this->hentSatsHistorikk() ?? new stdClass();
        $satsHistorikk->$egenskap = (object)[
            'sats' => $this->hentSats(),
            'relativ' => $this->hentRelativ(),
            'automatisk' => $automatisk,
            'gjeldende_kpi' => array_key_last($kpiHistorikk),
        ];
        $this->satsHistorikk = $satsHistorikk;
        $this->satsOppdatert = $tidspunkt;
        return $this->app->after($this, __FUNCTION__, $this);
    }
}