<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use Kyegil\CoreModel\CoreModelException;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Sett;

/**
 * Class Andelsperiodesett
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @method Andelsperiode current()
 */
class Andelsperiodesett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Andelsperiode::class;

    /**
     * @var Andelsperiode[]
     */
    protected ?array $items = null;

    /**
     * Legg til LEFT JOIN for leieobjekt
     *
     * @return $this
     */
    public function leggTilLeftJoinForLeieobjekt(): Andelsperiodesett
    {
        return $this->leggTilLeftJoin(
            Leieobjekt::hentTabell(),
            '`' . Andelsperiode::hentTabell() . '`.`leieforhold` = `' . Leieobjekt::hentTabell() . '`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '`'
        );
    }

    /**
     * Legg til LEFT JOIN for leieforhold
     *
     * @return $this
     */
    public function leggTilLeftJoinForLeieforhold(): Andelsperiodesett
    {
        return $this->leggTilLeftJoin(
            Leieforhold::hentTabell(),
            '`' . Andelsperiode::hentTabell() . '`.`leieforhold` = `' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`'
        );
    }

    /**
     * Legg til leieobjekt-modell
     *
     * @return $this
     * @throws CoreModelException
     */
    public function leggTilLeieobjektModell(): Andelsperiodesett
    {
        return $this->leggTilLeftJoinForLeieobjekt()
            ->leggTilModell(Leieobjekt::class);
    }

    /**
     * Legg til leieforhold-modell
     *
     * @return $this
     * @throws CoreModelException
     */
    public function leggTilLeieforholdModell(): Andelsperiodesett
    {
        return $this->leggTilLeftJoinForLeieforhold()
            ->leggTilModell(Leieforhold::class);
    }
}