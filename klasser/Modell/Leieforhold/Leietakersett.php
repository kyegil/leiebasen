<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Sett;

/**
 * Leietakersett
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @method Leietaker current()
 */
class Leietakersett extends Sett
{
    protected string $model = Leietaker::class;

    /**
     * @var Leietaker[]
     */
    protected ?array $items = null;

    /**
     * @return Leietakersett
     */
    public function leggTilLeftJoinForPerson(): Leietakersett
    {
        return $this
            ->leggTilLeftJoin(
                Person::hentTabell(),
                Leietaker::hentTabell() . '.`person` = `'
                . Person::hentTabell() . '`.`' . Person::hentPrimærnøkkelfelt() . '`'
            );

    }

    /**
     * @return Leietakersett
     */
    public function leggTilInnerJoinForPerson(): Leietakersett
    {
        return $this
            ->leggTilInnerJoin(
                Person::hentTabell(),
                Leietaker::hentTabell() . '.`person` = `'
                . Person::hentTabell() . '`.`' . Person::hentPrimærnøkkelfelt() . '`'
            );

    }

    /**
     * @return Leietakersett
     */
    public function leggTilPersonModell(): Leietakersett
    {
        return $this
            ->leggTilLeftJoinForPerson()
            ->leggTilModell(Person::class)
            ;
    }

    /**
     * @return Leietakersett
     */
    public function leggTilLeftJoinForLeieforhold(): Leietakersett
    {
        return $this
            ->leggTilLeftJoin(
                Leieforhold::hentTabell(),
                Leietaker::hentTabell() . '.`leieforhold` = `'
                . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`'
            );

    }

    /**
     * @return Leietakersett
     */
    public function leggTilInnerJoinForLeieforhold(): Leietakersett
    {
        return $this
            ->leggTilInnerJoin(
                Leieforhold::hentTabell(),
                Leietaker::hentTabell() . '.`leieforhold` = `'
                . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`'
            );

    }

    /**
     * @return Leietakersett
     */
    public function leggTilLeieforholdModell(): Leietakersett
    {
        return $this
            ->leggTilLeftJoinForLeieforhold()
            ->leggTilModell(Leieforhold::class)
            ;
    }

    /**
     * @return Leietakersett
     */
    public function leggTilLeftJoinForKontrakt(): Leietakersett
    {
        return $this
            ->leggTilLeftJoin(
                Kontrakt::hentTabell(),
                '`' . Leietaker::hentTabell() . '`.`kontrakt` = `'
                . Kontrakt::hentTabell() . '`.`' . Kontrakt::hentPrimærnøkkelfelt() . '`'
            );

    }

    /**
     * @param string $alias
     * @return Leietakersett
     */
    public function leggTilLeftJoinForKontraktLeieforhold(string $alias = 'kontrakt_leieforhold'): Leietakersett
    {
        return $this
            ->leggTilLeftJoinForKontrakt()
            ->leggTilLeftJoin(
                [$alias => Leieforhold::hentTabell()],
                '`' . Kontrakt::hentTabell() . '`.`leieforhold` = `'
                . $alias . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`'
            );
    }

    /**
     * @return Leietakersett
     */
    public function leggTilKontraktModell(?array $felter = null): Leietakersett
    {
        return $this
            ->leggTilLeftJoinForKontrakt()
            ->leggTilModell(Kontrakt::class, null, null, $felter);
    }

    /**
     * @param array|null $felter
     * @return Leietakersett
     * @throws \Kyegil\CoreModel\CoreModelException
     */
    public function leggTilKontraktLeieforholdModell(?array $felter = null): Leietakersett
    {
        return $this
            ->leggTilLeftJoinForKontraktLeieforhold()
            ->leggTilModell(
                Leieforhold::class,
                'kontrakter.leieforhold',
                'kontrakt_leieforhold',
                $felter
            );
    }
}