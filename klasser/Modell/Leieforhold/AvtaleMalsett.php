<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use Kyegil\Leiebasen\Sett;

/**
 * AvtaleMalsett
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @method AvtaleMal current()
 * @method AvtaleMal|null hentFørste()
 * @method AvtaleMal|null hentSiste()
 * @method AvtaleMal|null hentTilfeldig()
 */
class AvtaleMalsett extends Sett
{
    /** @var string */
    protected string $model = AvtaleMal::class;

    /** @var AvtaleMal[] */
    protected ?array $items = null;
}