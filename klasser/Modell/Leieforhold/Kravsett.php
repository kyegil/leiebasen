<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use Exception;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav\Delkravsett;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Sett;

/**
 * Kravsett
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @method Krav current()
 * @method Krav|null hentFørste()
 * @method Krav|null hentSiste()
 * @method Krav|null hentTilfeldig()
 */
class Kravsett extends Sett
{
    /** @var string */
    protected string $model = Krav::class;

    /** @var Krav[] */
    protected ?array $items = null;

    /**
     * @return Kravsett
     */
    public function leggTilLeftJoinForDelbeløp(): Kravsett{
        $this->leggTilLeftJoin(
            Delbeløp::hentTabell(), '`' . Delbeløp::hentTabell() . '`.`krav` = `'
            . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '`'
        );
        return $this;
    }

    /**
     * @return Kravsett
     */
    public function leggTilLeftJoinForRegning(): Kravsett{
        $this->leggTilLeftJoin(Regning::hentTabell(), Krav::hentTabell() . '.`gironr` = ' . Regning::hentTabell() . '.' . Regning::hentPrimærnøkkelfelt());
        return $this;
    }

    /**
     * @return Kravsett
     */
    public function leggTilLeftJoinForLeieforhold(): Kravsett{
        $this->leggTilLeftJoin(Leieforhold::hentTabell(), Krav::hentTabell() . '.`leieforhold` = ' . Leieforhold::hentTabell() . '.' . Leieforhold::hentPrimærnøkkelfelt());
        return $this;
    }

    /**
     * @return Kravsett
     */
    public function leggTilLeftJoinForLeieobjekt(): Kravsett{
        $this->leggTilLeftJoin(Leieobjekt::hentTabell(), Krav::hentTabell() . '.`leieobjekt` = ' . Leieobjekt::hentTabell() . '.' . Leieobjekt::hentPrimærnøkkelfelt());
        return $this;
    }

    /**
     * @param string|null $alias
     * @return Kravsett
     */
    public function leggTilLeftJoinForLeieforholdLeieobjekt(string $alias = 'leieforholdobjekt'): Kravsett
    {
        $this->leggTilLeftJoinForLeieforhold();
        return $this->leggTilLeftJoin(
            [$alias => Leieobjekt::hentTabell()],
            '`' . Leieforhold::hentTabell() . '`.`leieobjekt` = `'
            . $alias . '`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '`'
        );
    }

    /**
     * @param string|null $alias
     * @return Kravsett
     */
    public function leggTilLeftJoinForLeieforholdRegningsobjekt(string $alias = 'regningsobjekt'): Kravsett
    {
        $this->leggTilLeftJoinForLeieforhold();
        return $this->leggTilLeftJoin(
            [$alias => Leieobjekt::hentTabell()],
            '`' . Leieforhold::hentTabell() . '`.`regningsobjekt` = `'
            . $alias . '`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '`'
        );
    }

    /**
     * @param string|null $alias
     * @return Kravsett
     */
    public function leggTilLeftJoinForLeieforholdRegningsperson(string $alias = 'regningsperson'): Kravsett
    {
        $this->leggTilLeftJoinForLeieforhold();
        return $this->leggTilLeftJoin(
            [$alias => Person::hentTabell()],
            '`' . Leieforhold::hentTabell() . '`.`regningsperson` = `'
            . $alias . '`.`' . Person::hentPrimærnøkkelfelt() . '`'
        );
    }

    /**
     * @param array|null $felter
     * @return Kravsett
     * @throws Exception
     */
    public function leggTilLeieobjektModell(?array $felter = null): Kravsett
    {
        $this->leggTilLeftJoinForLeieobjekt()
            ->leggTilModell(Leieobjekt::class, null, null, $felter);
        return $this;
    }

    /**
     * @param array|null $felter
     * @return Kravsett
     * @throws Exception
     */
    public function leggTilRegningModell(?array $felter = null): Kravsett
    {
        $this->leggTilLeftJoinForRegning()
            ->leggTilModell(Regning::class, null, null, $felter);
        return $this;
    }

    /**
     * @param array|null $felter
     * @return Kravsett
     * @throws Exception
     */
    public function leggTilLeieforholdModell(?array $felter = null): Kravsett
    {
        $this->leggTilLeftJoinForLeieforhold()
            ->leggTilModell(Leieforhold::class, null, null, $felter);
        return $this;
    }

    /**
     * @return Kravsett
     */
    public function leggTilRegningForfallFelt(): Kravsett
    {
        $this->leggTilFelt(Krav::hentTabell(), 'forfall', '', 'forfall', 'giroer.regning_ekstra');
        return $this;
    }

    /**
     * @return Kravsett
     */
    public function leggTilRegningSisteForfallFelt(): Kravsett
    {
        $tp = $this->mysqli->table_prefix;
        $subQuery =
            '(SELECT MAX(`purreforfall`) AS `siste_forfall` FROM `' . $tp . 'purringer` WHERE `krav` = `' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '`)';

        $this->leggTilUttrykk('giroer.regning_ekstra', 'siste_forfall', 'IFNULL(' . $subQuery . ', `' . Krav::hentTabell() . '`.`forfall`)');
        return $this;
    }

    /**
     * @return Kravsett
     */
    public function leggTilRegningSistePurringFelt(): Kravsett
    {
        $tp = $this->mysqli->table_prefix;
        $subQuery =
            '(SELECT `blankett` AS `siste_purring` FROM `' . $tp . 'purringer` WHERE `krav` = `' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '` ORDER BY `nr` DESC LIMIT 1)';

        $this->leggTilUttrykk('giroer.regning_ekstra', 'siste_purring', $subQuery);
        return $this;
    }

    /**
     * Legg til nødvendige felter for å hente navnet på leieforholdet uten ytterligere db-spørringer
     *
     * @return Kravsett
     */
    public function forhåndslastLeieforholdNavn(): Kravsett
    {
        $this->leggTilUttrykk('leieforhold.leieforhold_ekstra', 'navn', '(' . Leieforholdsett::subQueryForNavn('leieforhold', Krav::hentTabell()) . ')');
        return $this;
    }

    /**
     * @param array|null $felter
     * @return Kravsett
     * @throws Exception
     */
    public function leggTilLeieforholdLeieobjektModell(?array $felter = ['navn', 'beskrivelse', 'etg', 'gateadresse']): Kravsett
    {
        return $this
            ->leggTilLeftJoinForLeieforholdLeieobjekt('leieforholdobjekt')
            ->leggTilModell(
                Leieobjekt::class,
                'leieforhold.leieobjekt',
                'leieforholdobjekt',
                $felter
            );
    }

    /**
     * @param array|null $felter
     * @return Kravsett
     * @throws Exception
     */
    public function leggTilLeieforholdRegningsobjektModell(?array $felter = ['navn', 'beskrivelse', 'etg', 'gateadresse']): Kravsett
    {
        return $this
            ->leggTilLeftJoinForLeieforholdRegningsobjekt('regningsobjekt')
            ->leggTilModell(
                Leieobjekt::class,
                'leieforhold.regningsobjekt',
                'regningsobjekt',
                $felter
            );
    }

    /**
     * @param array|null $felter
     * @return Kravsett
     * @throws Exception
     */
    public function leggTilRegningspersonModell(?array $felter = null): Kravsett
    {
        return $this
            ->leggTilLeftJoinForLeieforholdRegningsperson('regningsperson')
            ->leggTilModell(
                Person::class,
                'leieforhold.regningsperson',
                'regningsperson',
                $felter
            );
    }

    /**
     * @return Delkravsett
     * @throws Exception
     */
    public function inkluderDelkrav(): Delkravsett
    {
        /** @var Delkravsett $delkrav */
        $delkrav = $this->inkluder('delkrav');
        return $delkrav;
    }

    /**
     * @return Fraction
     * @throws \Kyegil\CoreModel\CoreModelException
     */
    public function hentSummerteAndeler(): Fraction
    {
        $summerteAndeler = new Fraction();
        $this->forEach(function (Krav $krav) use ($summerteAndeler) {
            $summerteAndeler->add($krav->andel ?? 0, true);
        });
        return $summerteAndeler->simplify();
    }
}