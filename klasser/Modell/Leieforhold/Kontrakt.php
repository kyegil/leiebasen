<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use DateTime;
use Exception;
use Kyegil\CoreModel\Interfaces\EavValuesInterface;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Eav\Egenskap as EavEgenskap;
use Kyegil\Leiebasen\Modell\Eav\Verdi as EavVerdi;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Samling;
use stdClass;

/**
 * Class Kontrakt
 *
 * @property int $id
 * @property int $kontraktnr
 * @property Leieforhold $leieforhold
 * @property DateTime $dato
 * @property DateTime|null $tildato
 * @property string $tekst
 *
 * @method int hentId()
 * @method int hentKontraktnr()
 * @method Leieforhold hentLeieforhold()
 * @method DateTime hentDato()
 * @method DateTime|null hentTildato()
 * @method string hentTekst()
 *
 * @method $this settKontraktnr(int $id)
 * @method $this settLeieforhold(Leieforhold $leieforhold)
 * @method $this settDato(DateTime $dato)
 * @method $this settTildato(DateTime|null $tildato)
 * @method $this settTekst(string $tekst)
 * @package Kyegil\Leiebasen\Modell\Leieforhold
*/
class Kontrakt extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'kontrakter';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'kontraktnr';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Kontraktsett::class;

    protected static ?array $one2Many;

    public static function harFlere(string $reference, ?string $model = null, ?string $foreignKey = null, array $filters = [], ?callable $callback = null): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::harFlere(
                'leietakere',
                Leietaker::class,
                'kontrakt',
                [],
                function(Leietakersett $leietakere) {
                    $leietakere->leggTilPersonModell()
                        ->leggTilLeieforholdModell();
                }
            );
        }
        return parent::harFlere($reference, $model, $foreignKey, $filters, $callback);
    }

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'kontraktnr'    => [
                'type'  => 'integer'
            ],
            'leieforhold'    => [
                'type'  => Leieforhold::class
            ],
            'fradato'    => [
                'target' => 'dato',
                'type'  => 'date'
            ],
            'tildato'    => [
                'type'  => 'date',
                'allowNull' => true
            ],
            'tekst'    => [
                'type'  => 'string'
            ]
        ];
    }

    /**
     * @param string $kilde
     * @return stdClass|null
     * @throws Exception
     */
    public function hentEavVerdiObjekter(string $kilde): ?stdClass
    {
        if(!$this->eavValueObjects) {
            $this->eavValueObjects = new stdClass();

            /** @var Samling $eavVerdiObjekter */
            $eavVerdiObjekter = $this->app->hentSamling(EavVerdi::class)

                ->leggTilInnerJoin(EavEgenskap::hentTabell(), EavVerdi::hentTabell() . '.egenskap_id = ' . EavEgenskap::hentTabell() . '.id')
                ->leggTilFilter([
                    EavEgenskap::hentTabell() . '.modell' => self::class,
                    EavVerdi::hentTabell() . '.objekt_id' => $this->getId()
                ]);
            /** @var EavValuesInterface $eavVerdiObjekt */
            foreach ($eavVerdiObjekter as $eavVerdiObjekt) {
                $verdiKilde = $eavVerdiObjekt->getConfig()::getDbTable();
                $kode = $eavVerdiObjekt->getConfig()->getSourceField();
                settype($this->eavValueObjects->{$verdiKilde}, 'object');
                $this->eavValueObjects->{$verdiKilde}->{$kode} = $eavVerdiObjekt;
            }
        }

        if(isset($this->eavValueObjects->{$kilde})) {
            return $this->eavValueObjects->{$kilde};
        }
        return null;
    }

    /**
     * @return Leietakersett
     * @throws Exception
     */
    public function hentLeietakere(): Leietakersett
    {
        /** @var Leietakersett $leietakere */
        $leietakere = $this->hentSamling('leietakere');
        return $leietakere;
    }

    /**
     * Synk Utdaterte felter
     *
     * Synk kontrakter-tabellen med verdier som nå har blitt flyttet til leieforhold-tabellen.
     * Når koden har blitt oppdatert til å kun bruke de nye feltene,
     * kan de gamle slettes.
     *
     * @return $this
     * @throws Exception
     */
    public function synkUtdaterteFelter(): Kontrakt
    {
        $tp = $this->mysqli->table_prefix;
        $this->mysqli->saveToDb([
            'update' => true,
            'table' => '`' . $tp . self::hentTabell() . '` `' . self::hentTabell() . '` INNER JOIN `' . $tp . Leieforhold::hentTabell() . '` `' . Leieforhold::hentTabell() . '` ON `' . self::hentTabell() . '`.`leieforhold` = `' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`',
            'where' => ['`' . $tp . self::hentTabell() . '`.`' . self::hentPrimærnøkkelfelt() . '`' => $this->hentId()],
            'fields' => [
                '`' . self::hentTabell() . '`.`leieobjekt` = `'     . Leieforhold::hentTabell() . '`.`leieobjekt`',
                '`' . self::hentTabell() . '`.`andel` = `'          . Leieforhold::hentTabell() . '`.`andel`',
                '`' . self::hentTabell() . '`.`oppsigelsestid` = `' . Leieforhold::hentTabell() . '`.`oppsigelsestid`',
                '`' . self::hentTabell() . '`.`årlig_basisleie` = `'. Leieforhold::hentTabell() . '`.`årlig_basisleie`',
                '`' . self::hentTabell() . '`.`leiebeløp` = `'      . Leieforhold::hentTabell() . '`.`leiebeløp`',
                '`' . self::hentTabell() . '`.`andel` = `'          . Leieforhold::hentTabell() . '`.`andel`',
                '`' . self::hentTabell() . '`.`ant_terminer` = `'   . Leieforhold::hentTabell() . '`.`ant_terminer`',
                '`' . self::hentTabell() . '`.`frosset` = `'        . Leieforhold::hentTabell() . '`.`frosset`',
                '`' . self::hentTabell() . '`.`stopp_oppfølging` = `' . Leieforhold::hentTabell() . '`.`stopp_oppfølging`',
                '`' . self::hentTabell() . '`.`avvent_oppfølging` = `'. Leieforhold::hentTabell() . '`.`avvent_oppfølging`',
                '`' . self::hentTabell() . '`.`regningsperson` = `' . Leieforhold::hentTabell() . '`.`regningsperson`',
                '`' . self::hentTabell() . '`.`regning_til_objekt` = `'. Leieforhold::hentTabell() . '`.`regning_til_objekt`',
                '`' . self::hentTabell() . '`.`regningsobjekt` = `' . Leieforhold::hentTabell() . '`.`regningsobjekt`',
                '`' . self::hentTabell() . '`.`regningsadresse1` = `'. Leieforhold::hentTabell() . '`.`regningsadresse1`',
                '`' . self::hentTabell() . '`.`regningsadresse2` = `'. Leieforhold::hentTabell() . '`.`regningsadresse2`',
                '`' . self::hentTabell() . '`.`postnr` = `'        . Leieforhold::hentTabell() . '`.`postnr`',
                '`' . self::hentTabell() . '`.`poststed` = `'       . Leieforhold::hentTabell() . '`.`poststed`',
                '`' . self::hentTabell() . '`.`land` = `'           . Leieforhold::hentTabell() . '`.`land`',
                '`' . self::hentTabell() . '`.`giroformat` = `'     . Leieforhold::hentTabell() . '`.`giroformat`',
            ]
        ]);
        return $this;
    }

    /**
     * @param Person|null $person
     * @param string $navn
     * @return $this
     * @throws Exception
     */
    public function leggTilLeietaker(?Person $person, string $navn = ''): Kontrakt
    {
        $navn = trim($navn);
        if (empty($person) && empty($navn)) {
            throw new Exception('Kan ikke legge til leietaker. Både adressekort og navn mangler');
        }
        $eksisterende = $this->app->hentSamling(Leietaker::class)
            ->leggTilPersonModell()
            ->leggTilLeieforholdModell()
            ->leggTilKontraktModell()
            ->leggTilFilter(['kontrakt' => $this->hentId()]);
        $eksisterende->leggTilFilter($person
            ? ['person' => $person->hentId()]
            : ['navn' => $navn]
        );
        $eksisterende->låsFiltre();
        if ($eksisterende->hentAntall()) {
            $eksisterende->sett('slettet', null);
            $this->app->logger->debug('Trenger ikke legge til eksisterende leietaker',
                ['kontrakt' => $this->hentId(), 'person' => $person ? $person->hentId() : null, 'navn' => $navn]
            );
        }
        else {
            $this->app->nyModell(Leietaker::class, (object)[
                'person' => $person,
                'leietaker' => $person ? '' : $navn,
                'kontrakt' => $this,
                'leieforhold' => $this->hentLeieforhold()
            ]);
            $this->app->logger->debug('Lagt til ny leietaker',
                ['kontrakt' => $this->hentId(), 'person' => $person ? $person->hentId() : null, 'navn' => $navn]
            );
        }

        return $this;
    }

    /**
     * Returnerer navnet på leietakeren(e) som streng
     *
     * @return string
     * @throws Exception
     */
    public function hentNavn()
    {
        if(!isset($this->data->navn)) {
            if (isset($this->rawData->kontrakt_ekstra->navn)) {
                $this->data->navn = $this->rawData->kontrakt_ekstra->navn;
            }
            else {
                $navnArray = [];
                $leietakere = $this->hentLeietakere();
                /** @var Leietaker $leietaker */
                foreach ($leietakere as $leietaker) {
                    $navnArray[] = $leietaker->hentNavn();
                }
                $this->data->navn = $this->app->liste($navnArray);
            }
        }
        return $this->data->navn;
    }

    /**
     * Er dette gjeldende kontrakt?
     *
     * @return bool
     */
    public function erGjeldendeKontrakt(): bool
    {
        return $this->hentLeieforhold()->hentKontrakt()->hentId() == $this->hentId();
    }
}