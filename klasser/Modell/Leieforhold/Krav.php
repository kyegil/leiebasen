<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use IntlDateFormatter;
use Kyegil\CoreModel\CoreModel;
use Kyegil\CoreModel\CoreModelException;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanleggsett;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløpsett;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andel;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav\Delkrav;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav\Delkravsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use stdClass;


/**
 * Class Krav
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @property int $id
 * @property int|null $reellKravId
 * @property Kontrakt $kontrakt
 * @property Leieforhold $leieforhold
 * @property Regning|null $regning
 * @property DateTime $kravdato
 * @property string $tekst
 * @property float $beløp
 * @property string $type
 * @property Leieobjekt|null $leieobjekt
 * @property Fraction|null $andel
 * @property string $termin
 * @property DateTime|null $fom
 * @property DateTime|null $tom
 * @property Strømanlegg $anlegg
 * @property DateTime $opprettet
 * @property string $oppretter
 * @property DateTime|null $forfall
 * @property float $utestående
 * @property Krav|null $hovedkrav
 *
 * @method int hentId()
 * @method int|null hentReellKravId()
 * @method Kontrakt hentKontrakt()
 * @method Leieforhold hentLeieforhold()
 * @method Regning|null hentRegning()
 * @method DateTime hentKravdato()
 * @method string hentTekst()
 * @method float hentBeløp()
 * @method string hentType()
 * @method Leieobjekt|null hentLeieobjekt()
 * @method Fraction|null hentAndel()
 * @method string hentTermin()
 * @method DateTime|null hentFom()
 * @method DateTime|null hentTom()
 * @method Strømanlegg|null hentAnlegg()
 * @method DateTime hentOpprettet()
 * @method string hentOppretter()
 * @method DateTime|null hentForfall()
 * @method float hentUtestående()
 * @method Krav|null hentHovedkrav()
 * @method Krav|null hentAnnullering()
 *
 * @method $this settId(int $id)
 * @method $this settLeieforhold(Leieforhold $leieforhold)
 * @method $this settKravdato(DateTime $kravdato)
 * @method $this settTekst(string $tekst)
 * @method $this settBeløp(float $beløp)
 * @method $this settType(string $type)
 * @method $this settLeieobjekt(Leieobjekt|null $leieobjekt)
 * @method $this settAndel(Fraction|null $andel)
 * @method $this settTermin(string $termin)
 * @method $this settFom(DateTime|null $fom)
 * @method $this settTom(DateTime|null $tom)
 * @method $this settAnlegg(Strømanlegg|null $anlegg)
 * @method $this settHovedkrav(Krav|null $hovedkrav)
 * @method $this settAnnullering(Krav|null $kreditt)
 */
class Krav extends Modell
{
    /** @var string */
    const TYPE_HUSLEIE = 'Husleie';

    /** @var string */
    const TYPE_STRØM= 'Fellesstrøm';

    /** @var string */
    const TYPE_PURREGEBYR = 'Purregebyr';

    /** @var string */
    const TYPE_ANNET = 'Annet';

    /** @var int Konto som brukes for utlikningsdelen av en kreditt */
    const KTO_KREDITT = 0;

    public static array $typer = [
        self::TYPE_HUSLEIE,
        self::TYPE_STRØM,
        self::TYPE_PURREGEBYR,
        self::TYPE_ANNET
    ];

    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'krav';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Kravsett::class;

    /** @var array one-to-many
     * @noinspection PhpDocFieldTypeMismatchInspection
     */
    protected static ?array $one2Many;

    /** @var int|null  */
    private static ?int $nesteReellKravId;

    /**
     * @param string $referanse
     * @param string|null $modell
     * @param string|null $fremmedNøkkel
     * @param array $filtre
     * @param callable|null $callback
     * @return array|null
     * @throws Exception
     */
    public static function harFlere(string $referanse, ?string $modell = null, ?string $fremmedNøkkel = null, array $filtre = [], ?callable $callback = null): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::harFlere('delkrav',
                Delkrav::class,
                'kravid'
            );
            self::harFlere('innbetalinger',
                Delbeløp::class,
                'krav'
            );
            self::harFlere('purringlinker',
                Purring\Kravlink::class,
                'krav'
            );

            /* Har ikke flere, men har én, men logikken er den samme */
            self::harFlere('kredittkopling',
                Delbeløp::class,
                'krav',
                ['konto' => '0'],
                function(Delbeløpsett $delbeløpsett) {
                    $delbeløpsett->leggTilInnbetalingModell();
                }
            );
        }
        return parent::harFlere($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     * @noinspection PhpUnusedParameterInspection
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'reell_krav_id'    => [
                'type'  => 'integer',
                'allowNull'    => true,
            ],
            'kontraktnr'    => [
                'target' => 'kontrakt',
                'type'  => Kontrakt::class,
                'rawDataContainer' => Kontrakt::hentTabell()
            ],
            'leieforhold' => [
                'type' => Leieforhold::class,
                'rawDataContainer' => Leieforhold::hentTabell()
            ],
            'gironr'    => [
                'target' => 'regning',
                'type'  => Regning::class,
                'allowNull' => true,
                'rawDataContainer' => Regning::hentTabell()
            ],
            'kravdato'    => [
                'type'  => 'date'
            ],
            'tekst'    => [
                'type'  => 'string'
            ],
            'beløp'    => [
                'type'  => 'string'
            ],
            'type'    => [
                'type'  => 'string'
            ],
            'leieobjekt'    => [
                'type'  => Leieobjekt::class,
                'allowNull' => true,
                'rawDataContainer' => Leieobjekt::hentTabell()
            ],
            'andel'    => [
                'type'  => function($andel, CoreModel $modell){
                    return $andel ? new Fraction($andel) : null;
                },
                'toDbValue' => function($andel = null) {
                    if($andel instanceof Fraction) {
                        $andel->simplify();
                        $andel->fractionBar = '/';
                        return $andel->compare('=', 1) ? 1 : $andel->asFraction();
                    }
                    return $andel;
                },
                'allowNull' => true,
            ],
            'termin'    => [
                'type'  => 'string'
            ],
            'fom'    => [
                'type'  => 'date',
                'allowNull' => true
            ],
            'tom'    => [
                'type'  => 'date',
                'allowNull' => true
            ],
            'anleggsnr'    => [
                'target' => 'anlegg',
                'type' => function($anleggsnr, $krav) {
                    /** @var Strømanleggsett $anleggsett */
                    $anleggsett = $krav->app->hentSamling(Strømanlegg::class)
                        ->leggTilFilter(['type' => self::TYPE_STRØM])
                        ->leggTilFilter(['avtalereferanse' => $anleggsnr])
                        ->låsFiltre()
                    ;
                    return $anleggsett->hentFørste();
                },
                'toDbValue' => function(?Strømanlegg $strømanlegg) {
                    return $strømanlegg ? $strømanlegg->hentAnleggsnummer() : null;
                },
                'allowNull' => true,
                'rawDataContainer' => Strømanlegg::hentTabell()
            ],
            'opprettet'    => [
                'type'  => 'datetime'
            ],
            'oppretter'    => [
                'type'  => 'string'
            ],
            'forfall'    => [
                'type'  => 'date',
                'allowNull' => true
            ],
            'utestående'    => [
                'type'  => 'string'
            ],
            'hovedkrav_id'  => [
                'target' => 'hovedkrav',
                'type'  => Krav::class,
                'allowNull' => true,
            ]
        ];
    }

    /**
     * Terminangivelsen formateres som tekst
     *
     * @param DateTime $fom
     * @param DateTime $tom
     * @return string
     */
    public static function beskrivTerminen(DateTimeInterface $fom, DateTimeInterface $tom): string
    {
        /**
         * Dersom perioden dekker et kalenderår skrives den som år
         */
        if(
            $fom->format('Y-12-t') == $tom->format('Y-m-d')
            && $fom->format('Y-m-d') == $tom->format('Y-01-01')
        ) {
            return $fom->format('Y');
        }

        /**
         * Dersom perioden dekker en kalendermåned skrives den som måned - år
         */
        if(
            $fom->format('Y-m-t') == $tom->format('Y-m-d')
            && $fom->format('Y-m-d') == $tom->format('Y-m-01')
        ) {
            $datoformat = IntlDateFormatter::create('nb_NO', null, null, null, null, 'MMMM yyyy');
            return $datoformat->format($fom);
        }

        //	Dersom perioden dekker ei uke skrives den som uke - år
        else if(
            $fom->format('Y-W-N') == $tom->format('Y-W-1')
            && $fom->format('Y-W-7') == $tom->format('Y-W-N')
        ) {
            return "Uke " . $fom->format('W Y');
        }
        //	ellers skrives perioden som fradato - tildato
        else {
            return $fom->format('d.m.Y') . " – " . $tom->format('d.m.Y');
        }
    }

    /**
     * @return void
     */
    public static function nullstillNesteReellKravId(): void {
        self::$nesteReellKravId = null;
    }

    /**
     * @param string|null $egenskap
     * @return mixed
     * @throws Exception
     */
    public function hent($egenskap = null)
    {
        if($egenskap == 'dato') {
            return $this->hentDato();
        }
        if($egenskap == 'utskriftsdato') {
            return $this->hentUtskriftsdato();
        }
        return parent::hent($egenskap);
    }

    /**
     * @param string $egenskap
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($egenskap, $verdi): Modell
    {
        if($egenskap == 'reell_krav_id' && $this->hent('reell_krav_id')) {
            throw new Exception('Kan ikke endre reell krav-id');
        }
        if($egenskap == 'utskriftsdato') {
            return $this->settUtskriftsdato($verdi);
        }
        return parent::sett($egenskap, $verdi);
    }

    /**
     * @return Delbeløpsett
     * @throws Exception
     */
    public function hentInnbetalingsDelbeløp(): Delbeløpsett
    {
        /** @var Delbeløpsett $innbetalinger */
        $innbetalinger = $this->hentSamling('innbetalinger');
        return $innbetalinger;
    }

    /**
     * For backwards compatibility
     *
     * @return Regning|null
     */
    public function hentGiro(): ?Regning
    {
        return $this->hentRegning();
    }

    /**
     * En kreditt består av et kravobjekt og en virtuell betaling på kr. 0.
     * Betalingen består av flere delbeløp,
     * hvorav ett er negativt og alltid knyttet til det negative kreditt-kravet,
     * mens resten er positive og kan avstemmes mot vanlige debet-krav.
     * Delbeløpene er knyttet sammen med leieforholdnummeret og med konto 0,
     * og skal tilsammen utgjøre kr. 0.
     *
     * Denne metoden vil returnere null for ordinære krav,
     * men for et negativt krav (kreditt) så returneres den tilhørende virtuelle betalingen.
     *
     * @return Innbetaling|null
     * @throws Exception
     */
    public function hentKredittkopling(): ?Innbetaling
    {
        if(!$this->erKreditt()) {
            return null;
        }
        if(!property_exists($this->data, 'kredittkopling')) {
            $this->data->kredittkopling = null;
            /** @var Delbeløp $delbeløp */
            $delbeløp = $this->hentSamling('kredittkopling')
                ->hentFørste();
            if($delbeløp && $delbeløp->hentId()) {
                $this->data->kredittkopling = $delbeløp->hentInnbetaling();
            }
        }
        return $this->data->kredittkopling;
    }


    /**
     * Returnerer de(t) positive delbeløpene som alltid hører sammen med en kreditt,
     * og som kan avstemmes mot andre krav
     *
     * @return Delbeløpsett|null
     * @throws Exception
     */
    public function hentKredittSomDelbetalinger(): ?Delbeløpsett
    {
        $kredittKopling = $this->hentKredittkopling();
        if(!$kredittKopling) {
            return null;
        }
        return $kredittKopling->hentPositiveDelbeløp();
    }

    /**
     * @return Delkravsett
     * @throws Exception
     */
    public function hentDelkrav(): Delkravsett
    {
        /** @var Delkravsett $delkravsett */
        $delkravsett = $this->hentSamling('delkrav');
        return $delkravsett;
    }

    /**
     * Returnerer innbetalings-delbeløpene som er avstemt mot kravet
     *
     * @return Delbeløpsett
     * @throws Exception
     */
    public function hentUtlikninger(): Delbeløpsett
    {
        /** @var Delbeløpsett $utlikninger */
        $utlikninger = $this->app->hentSamling(Delbeløp::class)
            ->leggTilFilter([
                'krav' => $this->hentId()
            ]);
        if(!isset($this->samlinger->utlikninger)) {
            $this->samlinger->utlikninger = $utlikninger->hentElementer();
        }
        $utlikninger->lastFra($this->samlinger->utlikninger);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $utlikninger;
    }

    /**
     * @return Purringsett
     * @throws Exception
     * @deprecated
     * @see Krav::hentPurringlinker()
     */
    public function hentPurringer(): Purringsett
    {
        /** @var Purringsett $purringer */
        $purringer = $this->app->hentSamling(Purring::class)
            ->leggTilFilter(['krav' => $this->hentId()])
            ->låsFiltre()
            ;
        if(!isset($this->samlinger->purringer)) {
            $this->samlinger->purringer = $purringer->hentElementer();
            if(!isset($this->samlinger->gebyrpurringer)) {
                $this->samlinger->gebyrpurringer = [];
                foreach ($purringer as $purring) {
                    if($purring->hentPurregebyr()) {
                        $this->samlinger->gebyrpurringer[] = $purring;
                    }
                }
            }
        }
        $purringer->lastFra($this->samlinger->purringer);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $purringer;
    }

    /**
     * @return Purring\Kravlinksett
     * @throws Exception
     */
    public function hentPurringlinker(): Purring\Kravlinksett
    {
        /** @var Purring\Kravlinksett $purringlinker */
        $purringlinker = $this->hentSamling('purringlinker');
        return $purringlinker;
    }

    /**
     * @return Purringsett
     * @throws Exception
     */
    public function hentGebyrPurringer(): Purringsett
    {
        /** @var Purringsett $purringer */
        $purringer = $this->app->hentSamling(Purring::class)
            ->leggTilFilter(['krav' => $this->hentId()])
            ->leggTilFilter(['purregebyr IS NOT NULL'])
            ->låsFiltre()
            ->leggTilSortering('nr')
        ;
        if(!isset($this->samlinger->gebyrpurringer)) {
            $this->samlinger->gebyrpurringer = $purringer->hentElementer();
        }
        $purringer->lastFra($this->samlinger->gebyrpurringer);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $purringer;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function hentAntallPurringer(): int
    {
        return $this->hentPurringlinker()->hentAntall();
    }

    /**
     * @return int
     * @throws Exception
     */
    public function hentAntallPurregebyr(): int
    {
        return $this->hentGebyrPurringer()->hentAntall();
    }

    /**
     * @return DateTime
     */
    public function hentDato(): DateTimeInterface
    {
        return $this->hentKravdato();
    }

    /**
     * @return bool
     */
    public function erKreditt(): bool
    {
        return $this->hentBeløp() < 0;
    }

    /**
     * Oppdater Utestående
     *
     * @return $this
     * @throws Exception
     */
    public function oppdaterUtestående(): Krav {
        if($this->erKreditt()) {
            if($this->hent('utestående') != 0) {
                $this->sett('utestående', 0);
            }
            return $this;
        }
        $this->reset();
        $betalt = 0;
        /** @var Delbeløp $betaling */
        foreach ($this->hentInnbetalingsDelbeløp() as $betaling) {
            /** @var float $beløp */
            $beløp = $betaling->hentBeløp();
            $betalt += $beløp;
        }
        $this->sett('utestående', $this->hentBeløp() - $betalt);
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function hentUtskriftsdato():?DateTimeInterface
    {
        $regning = $this->hentRegning();
        return $regning ? $regning->hentUtskriftsdato() : null;
    }

    /**
     * @param DateTime|null $utskriftsdato
     * @return $this
     * @throws Exception
     */
    public function settUtskriftsdato(?DateTime $utskriftsdato = null): Krav {
        $regning = $this->hentRegning();
        if($regning) {
            $regning->settUtskriftsdato($utskriftsdato);
        }
        return $this;
    }

    /**
     * @param Regning|null $regning
     * @return $this
     * @throws Exception
     */
    public function settRegning(?Regning $regning): Krav {
        if($regning && $regning->hentUtskriftsdato()) {
            throw new Exception("Kan ikke sende krav {$this->hentId()} på eksisterende regning {$regning->hentId()}");
        }
        $this->sett('regning', $regning);
        return $this;
    }

    /**
     * @param DateTimeInterface|null $forfallsdato
     * @return $this
     * @throws Exception
     */
    public function settForfall(?DateTimeInterface $forfallsdato = null): Krav {
        if($forfallsdato instanceof DateTime) {
            $forfallsdato = DateTimeImmutable::createFromMutable($forfallsdato);
        }
        if($forfallsdato) {
            $forfallsdato = clone $forfallsdato;
            $bankfridager = $this->app->bankfridager($forfallsdato->format('Y'));
            while(
                in_array( $forfallsdato->format('m-d'), $bankfridager )
                || $forfallsdato->format('N') > 5
            ) {
                $forfallsdato = $forfallsdato->add(new DateInterval('P1D'));
            }
        }
        $regning = $this->hentRegning();
        if($regning) {
            $regning->settForfall($forfallsdato);
        }
        else {
            parent::sett('forfall', $forfallsdato);
        }
        return $this;
    }

    /**
     * @param stdClass $parametere
     * @return $this
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Krav
    {
        $parametere->opprettet = new DateTime();
        $parametere->oppretter = $parametere->oppretter ?? $this->app->bruker['navn'];

        if (empty($parametere->leieforhold) && empty($parametere->kontrakt)) {
            throw new Exception('Leieforhold og kontrakt må oppgis for krav.');
        }
        else if (empty($parametere->leieforhold) && $parametere->kontrakt instanceof Kontrakt) {
            $parametere->leieforhold = $parametere->kontrakt->leieforhold;
        }
        else if (empty($parametere->kontrakt) && $parametere->leieforhold instanceof Leieforhold) {
            $parametere->kontrakt = $parametere->leieforhold->kontrakt;
        }

        $parametere->type = $parametere->type ?? Krav::TYPE_ANNET;

        $parametere->beløp = $parametere->beløp ?? 0;
        $parametere->utestående = max($parametere->beløp, 0);

        $parametere->kravdato = $parametere->kravdato ?? new DateTimeImmutable();
        $tidligstMuligeKravdato = $this->app->tidligstMuligeKravdato();
        if ($tidligstMuligeKravdato && $parametere->kravdato < $tidligstMuligeKravdato) {
            $parametere->kravdato = $tidligstMuligeKravdato;
        }
        /**
         * Trekk ut forfall og delkrav for å sette disse etter lagring
         * @var DateTime|null $forfall
         */
        $forfall = $parametere->forfall ?? null;
        $delkrav = $parametere->delkrav ?? null;
        unset($parametere->forfall, $parametere->delkrav);
        parent::opprett($parametere);
        if (!$this->hentId()) {
            throw new Exception('Klarte ikke opprette krav', ['parametere' => json_encode($parametere)]);
        }

        /**
         * Opprett delkrav
         */
        if (is_array($delkrav)) {
            foreach ($delkrav as $delkravTypeId => $beløp) {
                $delkravtype = $this->app->hentModell(Delkravtype::class, $delkravTypeId);
                if ($delkravtype->hentId()) {
                    $this->app->nyModell(Delkrav::class, (object)[
                        'krav' => $this,
                        'delkravtype' => $delkravtype,
                        'beløp' => $beløp
                    ]);
                }
            }
            $this->nullstill();
        }

        if ($this->hentBeløp() < 0) {
            $this->opprettKredittkopling();
        }
        if ($forfall) {
            $this->settForfall($forfall);
        }

        return $this;
    }


    /**
     * @return Innbetaling
     * @throws Exception
     */
    private function opprettKredittkopling(): Innbetaling
    {
        $dato = $this->hentKravdato();
        $referanse = $this->hentId();
        $betaler = $this->hentLeieforhold()->id;
        $betalingsId = Innbetaling::beregnId($dato, $referanse, $betaler);
        /** @var Delbeløp $debet */
        $this->app->nyModell(Delbeløp::class, (object)[
            'innbetaling'   => $betalingsId,
            'leieforhold'   => $this->hentLeieforhold(),
            'dato'			=> $dato,
            'beløp'			=> $this->hentBeløp() * (-1),
            'betaler'		=> $betaler,
            'ref'			=> $referanse,
            'merknad'		=> $this->hentTekst(),
            'konto'			=> "0",
            'registrerer'   => $this->hentOppretter(),
        ]);

        /** @var Delbeløp $debet */
        $debet = $this->app->nyModell(Delbeløp::class, (object)[
            'innbetaling'   => $betalingsId,
            'leieforhold'   => $this->hentLeieforhold(),
            'dato'			=> $dato,
            'beløp'			=> $this->hentBeløp(),
            'betaler'		=> $betaler,
            'ref'			=> $referanse,
            'merknad'		=> $this->hentTekst(),
            'konto'			=> "0",
            'registrerer'   => $this->hentOppretter(),
        ]);
        $debet->sett('krav', $this);
        return $this->data->kredittkopling = $debet->hentInnbetaling();
    }

    /**
     * Sletter kravet
     *
     * @param bool $slettTillegg Sann for å også slette tillegg til kravet
     * @return void
     * @throws CoreModelException
     */
    public function slett(bool $slettTillegg = true): void
    {
        /*
         * Dersom kravet er sendt ut, kan det ikke lengre slettes, men må krediteres
         */
        if ($this->hentReellKravId()) {
            $this->krediter($slettTillegg);
            return;
        }

        /*
         * Dersom kravet allerede er annullert foretas ingen ting
         */
        if ($this->erAnnullert()) {
            return;
        }

        /*
         * Dersom kravet er kreditt må også kredittkoplingen slettes
         *
         * @var Innbetaling|null $kredittkopling
         */
        $kredittkopling = $this->hentKredittkopling();
        if ($kredittkopling) {
            $kredittkopling->slett();
        }

        /*
         * Delkrav og tillegg slettes
         */
        $this->hentDelkrav()->slettHver();
        if($slettTillegg) {
            $this->hentTillegg()->slettHver();
        }

        /** @var Delbeløp $delbeløp */
        foreach($this->hentInnbetalingsDelbeløp() as $delbeløp) {
            $delbeløp->fjernAvstemming();
        }

        /*
         * Dersom dette er en kreditt som annullerer et tidligere krav,
         * så vil ikke kravet lenger være annullert
         *
         * @var Krav|null $annullertKrav
         */
        $annullertKrav = $this->erKreditt() ? $this->hentAnnullertKrav() : null;
        if($annullertKrav) {
            $annullertKrav->settAnnullering(null);
        }

        /*
         * Nullstill evt andeler
         */
        $this->app->hentSamling(Andel::class)
            ->leggTilFilter(['krav' => $this->hentId()])->låsFiltre()
            ->sett('krav', null);

        parent::slett();
    }

    /**
     * Krediterer kravet
     *
     * @param bool $slettTillegg
     * @return Krav Kreditt
     * @throws CoreModelException
     * @throws Exception
     */
    public function krediter(bool $slettTillegg = true): Krav
    {
        if ($this->erKreditt()) {
            throw new Exception('Kreditt kan ikke krediteres. Krav ' . $this->hentId() . ' er allerede kreditt');
        }

        if($this->erAnnullert()) {
            throw new Exception('Krav ' . $this->hentId() . ' har allerede blitt annullert');
        }

        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentLeieforhold();
        /** @var Fraction|null $andel */
        $andel = $this->hentAndel();
        $kredittAndel = null;
        if ($andel && $andel->compare('!=', 0)) {
            $kredittAndel = new Fraction();
            $kredittAndel = $kredittAndel->subtract($andel);
        }

        /** @var Krav $kreditt */
        $kreditt = $this->app->nyModell(self::class, (object)[
            'kravdato'		=> max(date_create(), $this->hentKravdato()),
            'kontrakt'  	=> $leieforhold->kontrakt,
            'leieforhold'	=> $leieforhold,
            'type'			=> $this->hentType(),
            'leieobjekt'	=> $this->hentLeieobjekt(),
            'fom'			=> $this->hentFom(),
            'tom'			=> $this->hentTom(),
            'tekst'			=> "Kreditering av " . $this->hentTekst(),
            'termin'		=> $this->hentTermin(),
            'beløp'			=> $this->hentBeløp() * (-1),
            'andel'			=> $kredittAndel,
            'anlegg'		=> $this->hentAnlegg(),
        ]);

        if (!$kreditt->hentId()) {
            throw new Exception('Klarte ikke kreditere krav', ['krav' => $this->hentId()]);
        }

        foreach($this->hentDelkrav() as $delkrav) {
            $this->app->nyModell(Delkrav::class, (object)[
                'krav' => $kreditt,
                'delkravtype' => $delkrav->delkravtype,
                'beløp' => $delkrav->hentBeløp() * (-1)
            ]);
        }

        if($slettTillegg) {
            foreach($this->hentTillegg() as $tillegg) {
                $tillegg->slett();
            }
        }

        if($this->hentUtestående() == $this->hentBeløp() && $kreditt->hentKredittkopling()) {
            $kreditt->hentKredittkopling()->avstem($this);
        }

        $this->settAnnullering($kreditt);
        return $kreditt->nullstill();
    }

    /**
     * Nullstill purredata
     * @return $this
     */
    public function nullstillPurringer(): Krav
    {
        unset($this->samlinger->purringer, $this->collections->purringer);
        return $this;
    }

    /**
     * Hent annullert krav
     *
     * Dersom dette er en annullering av et annet krav, hentes kravet som ble annullert
     *
     * @return Krav|null
     * @throws Exception
     */
    public function hentAnnullertKrav(): ?Krav
    {
        if(!property_exists($this->data, 'annullert_krav')) {
            /** @var Krav $kreditertKrav */
            $kreditertKrav = $this->app->hentSamling(Krav::class)
                ->leggTilEavVerdiJoin('annullering', true)
                ->leggTilFilter(['`annullering`.`verdi`' => $this->hentId()])
                ->hentFørste()
            ;
            $this->data->annullert_krav = $kreditertKrav;
        }
        return $this->data->annullert_krav;

    }

    public function erAnnullert(): bool
    {
        return (bool)$this->hentAnnullering();
    }

    /**
     * @return Betalingsplan\Originalkrav|null
     * @throws Exception
     */
    public function hentBetalingsplanLink(): ?Betalingsplan\Originalkrav
    {
        if(!property_exists($this->data, 'betalingsplan_link')) {
            if (property_exists($this->rawData, 'betalingsplan_originalkrav')) {
                /** @var Betalingsplan\Originalkrav $betalingsplanlink */
                $betalingsplanlink = $this->app->hentModell(Betalingsplan\Originalkrav::class, $this->rawData->betalingsplan_originalkrav->id);
                $lastet = $betalingsplanlink::mapNamespacedRawData($this->rawData->betalingsplan_originalkrav);
                $betalingsplanlink->setPreloadedValues($lastet);
                $this->data->betalingsplan_link = $betalingsplanlink->hentId() ? $betalingsplanlink : null;
            }
            else {
                /** @var Betalingsplan\Originalkravsett $betalingsplanlinksamling */
                $betalingsplanlinksamling = $this->app->hentSamling(Betalingsplan\Originalkrav::class);
                $betalingsplanlinksamling->leggTilBetalingsplanModell()
                    ->leggTilFilter(['krav_id' => $this->hentId()])
                    ->leggTilFilter(['`' . Betalingsplan::hentTabell() . '`.`aktiv`' => true]);
                 $this->data->betalingsplan_link = $betalingsplanlinksamling->hentFørste();
            }
        }
        return $this->data->betalingsplan_link;
    }

    /**
     * @return Betalingsplan|null
     * @throws Exception
     */
    public function hentBetalingsplan(): ?Betalingsplan
    {
        if(!property_exists($this->data, 'betalingsplan')) {
            if (property_exists($this->rawData, 'betalingsplaner')) {
                /** @var Betalingsplan $betalingsplan */
                $betalingsplan = $this->app->hentModell(Betalingsplan::class, $this->rawData->betalingsplaner->id);
                $lastet = $betalingsplan::mapNamespacedRawData($this->rawData->betalingsplaner);
                $betalingsplan->setPreloadedValues($lastet);
            }

            else {
                $betalingsplanLink = $this->hentBetalingsplanLink();
                $betalingsplan = $betalingsplanLink ? $betalingsplanLink->hentBetalingsplan() : null;
            }

            $this->data->betalingsplan = $betalingsplan && $betalingsplan->hentAktiv() ? $betalingsplan : null;
        }
        return $this->data->betalingsplan;
    }

    /**
     * Hent alle evt tillegg som er opprettet sammen med dette kravet.
     *
     * @return Kravsett
     * @throws Exception
     */
    public function hentTillegg(): Kravsett
    {
        /** @var Kravsett $tillegg */
        $tillegg = $this->app->hentSamling(Krav::class)
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`hovedkrav_id`' => $this->hentId()])
            ->låsFiltre()
        ;
        if(!isset($this->samlinger->tillegg)) {
            $this->samlinger->tillegg = $tillegg->hentElementer();
        }
        $tillegg->lastFra($this->samlinger->tillegg);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $tillegg;
    }

    /**
     * Hent nedbetalt dato
     *
     * @return DateTimeInterface|null
     * @throws Exception
     */
    public function hentBetalt():?DateTimeImmutable
    {
        if ($this->hentUtestående() != 0) {
            return null;
        }
        /** @var Delbeløp|null $sisteBetaling */
        $sisteBetaling = $this->hentInnbetalingsDelbeløp()->hentSiste();
        $dato = $sisteBetaling ? $sisteBetaling->hentDato() : $this->hentDato();
        if ($dato instanceof DateTime) {
            $dato = DateTimeImmutable::createFromMutable($dato);
        }
        return $dato;
    }

    /**
     * @param string $kodeEllerTypeId
     * @return float $beløp
     * @throws Exception
     */
    public function hentDelkravBeløp(string $kodeEllerTypeId): float {
        if(!trim($kodeEllerTypeId)) {
            throw new Exception('Ugyldig delkravtype');
        }

        /** @var Delkrav $delkrav */
        $delkrav = $this->hentDelkrav()
            ->leggTilLeftJoinForDelkravtype()
            ->leggTilFilter(['or' => [
                '`' . Delkravtype::hentTabell(). '`.`' . Delkravtype::hentPrimærnøkkelfelt(). '`' => $kodeEllerTypeId,
                '`' . Delkravtype::hentTabell(). '`.`kode`' => $kodeEllerTypeId
            ]])->låsFiltre()->hentFørste();

        return $delkrav ? $delkrav->hentBeløp() : 0;
    }

    /**
     * @param string $kodeEllerId
     * @param float $beløp
     * @return $this
     * @throws Exception
     */
    public function settDelkravBeløp(string $kodeEllerId, float $beløp): Krav {
        if(!trim($kodeEllerId)) {
            throw new Exception('Ugyldig delkravtype');
        }
        if($beløp * $this->hentBeløp() < 0) {
            throw new Exception('Alle delbeløp i et krav må ha samme fortegn som totalbeløpet');
        }
        $del = null;
        $fordelbart = $this->hentBeløp();
        foreach ($this->hentDelkrav() as $delkrav) {
            if(
                $delkrav->hentDelkravtype()->hentId() == $kodeEllerId
                || $delkrav->hentDelkravtype()->hentKode() == $kodeEllerId
            ) {
                $del = $delkrav;
            }
            else {
                $fordelbart -= $delkrav->beløp;
            }
        }

        if($beløp * $fordelbart < 0) {
            return $this;
        }
        if(abs($fordelbart) < abs($beløp)) {
            $beløp = $fordelbart;
        }

        if($del) {
            $del->settBeløp($beløp);
        }
        else {
            $delkravtype = $this->app->hentDelkravtyper()
                ->leggTilFilter(['id' => $kodeEllerId])
                ->leggTilFilter(['or' => [
                    'id' => $kodeEllerId,
                    'kode' => $kodeEllerId
                ]])
                ->leggTilFilter(['kravtype' => $this->hentType()])
                ->hentFørste();
            if(!$delkravtype) {
                throw new Exception('Ugyldig delkravtype ' . $kodeEllerId);
            }
            $this->app->nyModell(Delkrav::class, (object)[
                'krav' => $this->hentId(),
                'delkravtype' => $delkravtype,
                'beløp' => $beløp
            ]);
        }
        return $this;
    }

    /**
     * Returnerer det neste relle krav-id nummeret som kan brukes.
     *
     * Reell krav-id er et unikt id-nummer per krav, som følger i en stigende uavbrutt serie.
     * Krav som har fått tildelt reell krav-id kan ikke slettes
     *
     * @return int
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    private function hentNesteReellKravId(): int
    {
        if(!isset(self::$nesteReellKravId)) {
            $tp = $this->mysqli->table_prefix;
            $select = $this->mysqli->select([
                'source' => $tp . static::getDbTable(),
                'fields' => [
                    'reell_krav_id' => 'MAX(`reell_krav_id`)'
                ],
            ])->data;
            $sisteReellKravId = $select[0] ? $select[0]->reell_krav_id : 0;
            self::$nesteReellKravId = $sisteReellKravId + 1;

        }
        return self::$nesteReellKravId;
    }

    /**
     * Øk det neste reelle krav-id nummeret med 1,
     * og returner resultatet.
     *
     * Reell krav-id er et unikt id-nummer per krav, som følger i en stigende uavbrutt serie.
     * Krav som har fått tildelt reell krav-id kan ikke slettes
     *
     * @return int
     */
    public function økNesteReellKravId(): int
    {
        $this->hentNesteReellKravId();
        return ++self::$nesteReellKravId;
    }

    /**
     * Returner det neste reelle krav-id-nummeret,
     * og øk det deretter med 1 sånn at det igjen er klart for neste forespørsel
     *
     * @return int
     */
    public function hentNesteReellKravIdOgØk(): int {
        $this->hentNesteReellKravId();
        return self::$nesteReellKravId++;
    }

    /**
     * Tildel kravet en reell krav-id
     *
     * Reell krav-id er et unikt id-nummer per krav, som følger i en stigende uavbrutt serie.
     * Krav som har fått tildelt reell krav-id kan ikke slettes
     *
     * @return $this
     * @throws Exception
     */
    public function settReellKravId(): Krav
    {
        if(!$this->hent('reell_krav_id')) {
            $this->sett('reell_krav_id', $this->hentNesteReellKravIdOgØk());
        }
        return $this;
    }
}