<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 01/12/2020
 * Time: 16:26
 */

namespace Kyegil\Leiebasen\Modell\Leieforhold\Framleie;


use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleie as Framleieforhold;
use Kyegil\Leiebasen\Modell\Person;

/**
 * Class AvtaleMal
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @property integer $nr
 * @property Framleieforhold $framleieforhold
 * @property Person $person
 *
 * @method integer hentNr()
 * @method Framleieforhold hentFramleieforhold()
 * @method Person hentPerson()
 *
 * @method $this settFramleieforhold(Framleieforhold $framleieforhold)
 * @method $this settPerson(Person $person)
 */
class Framleietaker extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'framleiepersoner';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'nr';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Framleietakersett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array {
        return [
            'nr' => [
                'type' => 'integer'
            ],
            'framleieforhold'    => [
                'type'  => Framleieforhold::class,
            ],
            'personid'    => [
                'target'  => 'person',
                'type'  => Person::class
            ]
        ];
    }
}