<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold\Framleie;


use Kyegil\Leiebasen\Modell\Leieforhold\Framleie;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Sett;

/**
 * Framleiesett
 * @package Kyegil\Leiebasen\Modell\Leieforhold\Framleie
 *
 * @method Framleietaker current()
 */
class Framleietakersett extends Sett
{
    protected string $model = Framleietaker::class;

    /** @var Framleietaker[] */
    protected ?array $items = null;

    /**
     * @return Framleietakersett
     */
    public function leggTilLeftJoinForFramleieforhold()
    {
        return $this
            ->leggTilLeftJoin(
                Framleie::hentTabell(),
                '`' . Framleietaker::hentTabell() . '`.`framleieforhold` = `'
                . Framleie::hentTabell() . '`.`' . Framleie::hentPrimærnøkkelfelt() . '`'
            );
    }

    /**
     * @return Framleietakersett
     */
    public function leggTilInnerJoinForFramleieforhold()
    {
        return $this
            ->leggTilInnerJoin(
                Framleie::hentTabell(),
                '`' . Framleietaker::hentTabell() . '`.`framleieforhold` = `'
                . Framleie::hentTabell() . '`.`' . Framleie::hentPrimærnøkkelfelt() . '`'
            );
    }

    /**
     * @return Framleietakersett
     */
    public function leggTilLeftJoinForPerson()
    {
        return $this
            ->leggTilLeftJoin(
                Person::hentTabell(),
                '`' . Framleietaker::hentTabell() . '`.`personid` = `'
                . Person::hentTabell() . '`.`' . Person::hentPrimærnøkkelfelt() . '`'
            );
    }

    /**
     * @return Framleietakersett
     */
    public function leggTilInnerJoinForPerson()
    {
        return $this
            ->leggTilInnerJoin(
                Person::hentTabell(),
                '`' . Framleietaker::hentTabell() . '`.`personid` = `'
                . Person::hentTabell() . '`.`' . Person::hentPrimærnøkkelfelt() . '`'
            );
    }

    /**
     * @return Framleietakersett
     */
    public function leggTilFramleieModell()
    {
        return $this->leggTilLeftJoinForFramleieforhold()->leggTilModell(Framleie::class);
    }

    /**
     * @return Framleietakersett
     */
    public function leggTilPersonModell()
    {
        return $this->leggTilLeftJoinForPerson()->leggTilModell(Person::class);
    }
}