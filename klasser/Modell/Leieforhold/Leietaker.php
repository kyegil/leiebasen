<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 03/08/2020
 * Time: 11:26
 */

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use DateTime;
use DateTimeInterface;
use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;

/**
 * Class Leietaker
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @property int $id
 * @property int $kopling
 * @property Person|null $person
 * @property Leieforhold|null $leieforhold
 * @property Kontrakt $kontrakt
 * @property DateTimeInterface|null $slettet
 * @property string $leietakerNavn
 *
 * @method int hentId()
 * @method int hentKopling()
 * @method Person|null hentPerson()
 * @method Leieforhold|null hentLeieforhold()
 * @method Kontrakt hentKontrakt()
 * @method DateTime|null hentSlettet()
 * @method string hentLeietakerNavn()
 *
 * @method $this settKopling(int $id)
 * @method $this settPerson(Person|null $person) Leietakerens adressekort, dersom et eksisterer
 * @method $this settKontrakt(Kontrakt $kontrakt)
 * @method $this settSlettet(DateTimeInterface|null $slettet) Leietakeren har forlatt kontrakten/leieforholdet
 * @method $this settLeietakerNavn(string $leietakerNavn) Navn på leietakeren dersom adressekort ikke eksisterer
*/
class Leietaker extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'kontraktpersoner';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'kopling';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Leietakersett::class;

    /**
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'kopling'    => [
                'type'  => 'integer'
            ],
            'person'    => [
                'type'  => Person::class,
                'allowNull' => true,
                'rawDataContainer' => Person::hentTabell()
            ],
            'leieforhold' => [
                'type' => Leieforhold::class,
                'allowNull' => true,
                'rawDataContainer' => Leieforhold::hentTabell()
            ],
            'kontrakt'    => [
                'type'  => Kontrakt::class,
                'rawDataContainer' => Kontrakt::hentTabell()
            ],
            'slettet'    => [
                'type'  => 'date',
                'allowNull' => true
            ],
            'leietaker'    => [
                'target'  => 'leietaker_navn',
                'type'  => 'string'
            ]
        ];
    }

    /**
     * @return string
     * @throws Exception
     */
    public function hentNavn()
    {
        /** @var Person|null $person */
        $person = $this->hentPerson();
        if(!$person || !$person->hentId()) {
            return $this->hentLeietakerNavn();
        }
        return $person->hentNavn();
    }
}