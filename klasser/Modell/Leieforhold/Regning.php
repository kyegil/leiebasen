<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Kyegil\CoreModel\CoreModelException;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Fbo\Trekk;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning\Efaktura;
use Kyegil\Leiebasen\Nets\NetsException;
use Kyegil\Leiebasen\Sett;
use Kyegil\Leiebasen\Visning\felles\epost\regning\Epostfaktura;
use Kyegil\Leiebasen\Visning\felles\fpdf\Pdf;
use Kyegil\Leiebasen\Visning\felles\html\regning\Kravlinje;
use Kyegil\Nets\Forsendelse\Oppdrag\Transaksjon\Efaktura as EfakturaTransaksjon;
use stdClass;

/**
 * Class Regning
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @property int $regningsnr
 * @property DateTime $sammensatt
 * @property DateTime|null $utskriftsdato
 * @property string $kid
 * @property Leieforhold $leieforhold
 * @property string $format
 * @property DateTimeInterface|null $originalForfallsdato
 * @property object|null $utsettelseForespørsel
 *
 * @method int hentRegningsnr()
 * @method DateTime hentSammensatt()
 * @method DateTime|null hentUtskriftsdato()
 * @method string hentKid()
 * @method Leieforhold hentLeieforhold()
 * @method string hentFormat()
 * @method DateTimeInterface|null hentOriginalForfallsdato()
 * @method object|null hentUtsettelseForespørsel
 *
 * @method $this settSammensatt(DateTime $sammensatt)
 * @method $this settKid(string $kid)
 * @method $this settLeieforhold(Leieforhold $leieforhold)
 * @method $this settFormat(string $format)
 * @method $this settOriginalForfallsdato(DateTimeInterface|null $format)
 * @method $this settUtsettelseForespørsel(object|null $format)
 */
class Regning extends Modell
{
    const FORMAT_INGEN_UTSKRIFT = '';
    const FORMAT_PAPIR = 'papir';
    const FORMAT_EFAKTURA = 'eFaktura';
    const FORMAT_EPOST = 'epost';



    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'giroer';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'gironr';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Regningsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    public static function harFlere(string $reference, ?string $model = null, ?string $foreignKey = null, array $filters = [], ?callable $callback = null): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [
                'purringer' => [
                    'model' => Purring::class,
                    'foreignTable' =>  Krav::hentTabell(),
                    'foreignKey' => 'gironr',
                    'filters' => [],
                    'callback' => function(Purringsett $purringer) {
                        $purringer->leggTilInnerJoin(Krav::hentTabell(),
                            '`' . Purring::hentTabell() . '`.`krav` = `' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '`'
                        );
                        $purringer->leggTilModell(Krav::class, null, null, ['gironr']);
                    }
                ]
            ];
            self::harFlere('krav',
                Krav::class,
                'gironr'
            );
            self::harFlere('husleier',
                Krav::class,
                'gironr',
                [
                    ['`' . Krav::hentTabell() . '`.`type`' => Krav::TYPE_HUSLEIE]
                ]
            );
            self::harFlere('betalingsplanavdrag',
                Leieforhold\Betalingsplan\Avdrag::class,
                'giro_id'
            );
        }
        return parent::harFlere($reference, $model, $foreignKey, $filters, $callback);
    }

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'gironr'    => [
                'target' => 'regningsnr',
                'type'  => 'integer'
            ],
            'sammensatt'    => [
                'type'  => 'datetime',
                'allowNull' => true
            ],
            'utskriftsdato'    => [
                'type'  => 'datetime',
                'allowNull' => true
            ],
            'kid'    => [
                'type'  => 'string'
            ],
            'leieforhold'    => [
                'type'  => Leieforhold::class,
                'rawDataContainer' => 'leieforhold'
            ],
            'format'    => [
                'type'  => 'string'
            ]
        ];
    }

    /**
     * @param stdClass $parametere
     * @return Regning
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Regning
    {
        if (!isset($parametere->leieforhold)) {
            throw new Exception('Leieforhold er påkrevet for alle regninger');
        }
        parent::opprett($parametere);
        $kid = $this->genererKid();
        $this->sett('kid', $kid);
        return $this;
    }

    /**
     * @return DateTime|null
     * @throws Exception
     */
    public function hentDato()
    {
        return $this->hent('utskriftsdato');
    }

    /**
     * @return Kravsett
     * @throws Exception
     */
    public function hentKrav(): Kravsett
    {
        /** @var Kravsett $krav */
        $krav = $this->hentSamling('krav');
        return $krav;
    }

    /**
     * @return Purringsett
     * @throws Exception
     */
    public function hentPurringer(): Purringsett
    {
        /** @var Purringsett $purringer */
        $purringer = $this->hentSamling('purringer');
        if(!isset($this->samlinger->purringer)) {
            $this->samlinger->purringer = $purringer->hentElementer();
        }
        $purringer->lastFra($this->samlinger->purringer);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $purringer;
    }

    /**
     * @param string $egenskap
     * @return mixed
     * @throws Exception
     */
    public function hent($egenskap = null)
    {
        return parent::hent($egenskap);
    }

    /**
     * @param string $egenskap
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($egenskap, $verdi): Modell
    {
        if($egenskap == 'utskriftsdato') {
            return $this->settUtskriftsdato($verdi);
        }
        return parent::sett($egenskap, $verdi);
    }

    /**
     * @return DateTime|null
     * @throws Exception
     */
    public function hentForfall()
    {
        if(!property_exists($this->data, 'forfall')) {
            if (isset($this->rawData->regning_ekstra)
            && property_exists($this->rawData->regning_ekstra, 'forfall')) {
                $this->data->forfall = $this->rawData->regning_ekstra->forfall;
                if($this->data->forfall) {
                    $this->data->forfall = date_create_from_format('Y-m-d', $this->rawData->regning_ekstra->forfall);
                    $this->data->forfall->settime(0, 0);
                }
            }
            else {
                /** @var Krav $krav */
                $krav = $this->hentKrav()->hentFørste();
                $this->data->forfall = $krav ? $krav->hentForfall() : null;
            }
        }
        return $this->data->forfall;
    }

    /**
     * @return float
     * @throws Exception
     */
    public function hentBeløp()
    {
        if(!isset($this->data->beløp)) {
            if (isset($this->rawData->regning_ekstra->beløp)) {
                $this->data->beløp = $this->rawData->regning_ekstra->beløp;
            }
            else {
                $this->data->beløp = 0;
                $this->data->utestående = 0;
                /** @var Krav $krav */
                foreach($this->hentKrav() as $krav) {
                    $this->data->beløp += $krav->hentBeløp();
                    $this->data->utestående += $krav->hentUtestående();
                }
            }
        }
        return $this->data->beløp;
    }

    /**
     * @return float
     * @throws Exception
     */
    public function hentUtestående()
    {
        if(!isset($this->data->utestående)) {
            if (isset($this->rawData->regning_ekstra->utestående)) {
                $this->data->utestående = $this->rawData->regning_ekstra->utestående;
            }
            else {
                $this->data->beløp = 0;
                $this->data->utestående = 0;
                /** @var Krav $krav */
                foreach($this->hentKrav() as $krav) {
                    $this->data->beløp += $krav->hentBeløp();
                    $this->data->utestående += $krav->hentUtestående();
                }
            }
        }
        return $this->data->utestående;
    }

    /**
     * @return Sett
     * @throws Exception
     */
    public function hentInnbetalinger()
    {
        if(!isset($this->samlinger->innbetalinger)) {
            $kravIdNumre = implode(',', $this->hentKrav()->hentIdNumre());
            /** @var Sett $innbetalinger */
            $innbetalinger = $this->app->hentSamling(Innbetaling::class)
                ->leggTilFilter(["krav IN({$kravIdNumre})"])
                ->låsFiltre()
            ;
            $this->samlinger->innbetalinger = $innbetalinger;
        }
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $this->samlinger->innbetalinger;
    }

    /**
     * Hent evt tilhørende eFaktura
     *
     * @return Efaktura|null
     * @throws Exception
     */
    public function hentEfaktura(): ?Efaktura
    {
        if(!property_exists($this->data, 'efaktura')) {
            /** @var Efaktura|null $efaktura */
            $efaktura = $this->app->hentSamling(Efaktura::class)
                ->leggTilFilter([
                    'giro' => $this->hentId()
                ])
                ->hentFørste();
            $this->data->efaktura = $efaktura;
        }
        return $this->data->efaktura;
    }

    /**
     * Hent AvtaleGiro-trekk dersom det finnes
     *
     * @return Trekk|null
     * @throws Exception
     */
    public function hentFboTrekk(): ?Trekk
    {
        if(!property_exists($this->data, 'fboTrekk')) {
            /** @var Trekk|null $fboTrekk */
            $fboTrekk = $this->app->hentSamling(Trekk::class)
                ->leggTilFilter([
                    'gironr' => $this->hentId()
                ])
                ->hentFørste();
            $this->data->fboTrekk = $fboTrekk;
        }
        return $this->data->fboTrekk;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function slettEfaktura()
    {
        $efaktura = $this->hentEfaktura();
        if($efaktura) {
            $efaktura->slett();
            $this->data->efaktura = null;
        }
        return $this;
    }

    /**
     * Opprett Efaktura
     *
     * Kopler giroen til en oppføring i eFaktura-tabellen
     *
     * @param DateTimeInterface|null $forsendelsesdato
     * @param string|null $forsendele
     * @param int|null $oppdrag
     * @param string $status
     *      ->forsendelsesdato:	påkrevd DateTime-objekt
     *      ->forsendelse:		påkrevd streng
     *      ->oppdrag:			påkrevd heltall oppdragsnr
     *      ->status:			null, 'IN_PROGRESS', 'OK'
     * @return $this
     * @throws Exception
     */
    public function opprettEfaktura(
        ?DateTimeInterface $forsendelsesdato = null,
        ?string $forsendelse = null,
        ?int $oppdrag = null,
        string $status = Efaktura::PREPARED
    ): Regning
    {
        if(!$this->hentEfaktura()) {
            $this->data->efaktura = $this->app->nyModell(Efaktura::class, (object)[
                'regning' => $this,
                'forsendelsesdato' => $forsendelsesdato,
                'forsendelse' => $forsendelse,
                'oppdrag' => $oppdrag,
                'status' => $status
            ]);
        }
        if(
            in_array($status, [Efaktura::CONSIGNMENT_PROCESSED, Efaktura::OK])
            && !$this->hentUtskriftsdato()
        ) {
            $this->settUtskriftsdato($forsendelsesdato);
        }
        return $this;
    }

    /**
     * Registrer utskrift av ei regning
     *
     * Normalt bør ikke utskriftsdato registreres før en utskrift har blitt bekreftet,
     * men det bør allikevel taes hensyn til at en utskrift vil måtte forkastes i ettertid
     *
     * @param DateTime|null $utskriftsdato
     * @return Regning
     * @throws Exception
     */
    public function settUtskriftsdato(DateTimeInterface $utskriftsdato = null): Regning
    {
        /** @var Trekk $fboTrekk */
        $fboTrekk = $this->hentFboTrekk();
        if($fboTrekk) {
            $fboTrekk->settVarslet($utskriftsdato)->nullstill();
        }
        if(!$utskriftsdato) {
            $this->slettPdf();
        }
        parent::sett('utskriftsdato', $utskriftsdato);
        return $this;
    }

    /**
     * @param DateTimeInterface|null $forfallsdato
     * @return $this
     * @throws \Throwable
     */
    public function settForfall(DateTimeInterface $forfallsdato = null): Regning
    {
        list(
            'forfallsdato' => $forfallsdato,
            ) = $this->app->pre($this, __FUNCTION__, [
            'forfallsdato' => $forfallsdato,
        ]);
        if($forfallsdato instanceof DateTime) {
            $forfallsdato = \DateTimeImmutable::createFromMutable($forfallsdato);
        }
        /**
         * Forfallsdato kan ikke nulles på regninger som allerede er skrevet ut
         */
        if(!$forfallsdato && $this->hentUtskriftsdato()) {
            return $this->app->post($this, __FUNCTION__, $this);
        }

        if(
            ($forfallsdato === $this->hentUtskriftsdato()) || (
                $forfallsdato && $this->hentUtskriftsdato()
                && $forfallsdato->format('Ymd') == $this->hentUtskriftsdato()->format('Ymd')
            )
        ) {
            /* forfallsdato er uendret */
            return $this->app->post($this, __FUNCTION__, $this);
        }

        $this->endreForfall($forfallsdato);
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function erKreditNota(): bool {
        return $this->hentBeløp() < 0;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function sendEpost(): Regning
    {
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentLeieforhold();
        if($leieforhold->hentGiroformat() == 'epost') {

            $brukerepost = $leieforhold->hentEpost();
            // Du kan ikke velge epost som giroformat uten epostadresse
            if(!$brukerepost) {
                $leieforhold->settGiroformat('papir');
                throw new Exception("Leieforhold {$leieforhold->id} mangler epostadresse for epostregninger.");
            }

            $emne = "Regning {$this->id} for leieforhold {$leieforhold}";
            /** @var Epostfaktura $html */
            $html = $this->app->hentVisning(Epostfaktura::class, ['regning' => $this]);
            /** @var Epostfaktura $renTekst */
            $renTekst = $this->app->hentVisning(Epostfaktura::class, ['regning' => $this])
                ->settMal('felles/epost/regning/Epostfaktura.txt')
                ->settMalForElementer(Kravlinje::class, 'felles/html/regning/KravlinjeEpostfaktura.txt');

            if($this->erKreditNota()) {
                $emne = "Kreditnota {$this->id} for leieforhold {$this->leieforhold}";;
                $html->settMal('felles/epost/regning/Kreditnota.html');
                $renTekst->settMal('felles/epost/regning/Kreditnota.txt');
            }

            $epost = $this->app->sendMail((object)[
                'to' => implode(",", $brukerepost),
                'subject' => $emne,
                'html' => $html,
                'text' => $renTekst,
                'type' => 'epostregning'
            ]);
            if(!$epost) {
                throw new Exception("Epostforsendelsen mislyktes");
            }
            if(!$this->hentUtskriftsdato()) {
                $this->settUtskriftsdato(new DateTime() );
                $this->settFormat('epost' );
            }

        }
        return $this;
    }

    /**
     * Opprett en ny efaktura-transaksjon, og fyller denne med innhold
     *
     * @return EfakturaTransaksjon
     * @throws Exception
     */
    public function lagEfakturaTransaksjon(): EfakturaTransaksjon
    {
        $leieforhold = $this->hentLeieforhold();
        $efakturaIdentifier = $leieforhold->regningsperson ? $leieforhold->regningsperson->hentEfakturaIdentifier() : null;
        $leieobjekt = $leieforhold->hentLeieobjekt();
        $leieforholdAdresse = $leieforhold->hentAdresse();
        $utskriftsdato = $this->hentUtskriftsdato() ?: date_create();

        // 01 = NETS' efakturamal 1
        // 02 = NETS' efakturamal 2
        $fakturamal = 2;
        $kolonnebredde = $fakturamal == 2 ? 96 : 80;

        $efaktura = new EfakturaTransaksjon();

        /** @var int summaryType 0 eFaktura, 1 = Avtalegiro */
        $efaktura->summaryType = (bool)$this->hentFboTrekk();
        $efaktura->mal = $fakturamal;
        $efaktura->reklame = 0;
        $efaktura->fakturatype = 'eFaktura regning';
        $efaktura->ledetekstBeløp = 'Å betale';
        $efaktura->ledetekstFakturanummer = 'Regningsnr';
        $efaktura->ledetekstFakturadato = 'Dato';
        $efaktura->ledetekstBetalingsfrist = 'Forfallsdato';

        $efaktura->efakturaIdentifier       = $efakturaIdentifier;
        $efaktura->efakturareferanse        = '';
        $efaktura->fremmedreferanse         = "Regning nr. {$this->hentId()}";
        $efaktura->overskriftFakturakunde   = 'Leietaker';
        $efaktura->overskriftFakturabetaler = 'Betaler';

        /** Opptil 5 variable felter variableTextFields */
        $efaktura->variableTextFields = [
            (object)[
                'label' => 'Leieforhold',
                'value' => "{$leieforhold}: {$leieforhold->hentNavn()}"
            ],
            (object)[
                'label' => 'Leieobjekt',
                'value' => "{$leieobjekt->hentType()} {$leieobjekt}"
            ],
            (object)[
                'label' => '&nbsp;',
                'value' => "{$leieobjekt->hentBeskrivelse()}"
            ],
        ];

        $efaktura->freeTextBeforeInvoiceDetails = $this->app->hentValg('efaktura_tekst1');
        $efaktura->freeTextAfterInvoiceDetails = $this->app->hentValg('efaktura_tekst2');

        $efaktura->fakturautstederNavn      = $this->app->hentValg('utleier');
        $efaktura->fakturautstederAdresse1  = $this->app->hentValg('adresse');
        $efaktura->fakturautstederPostnummer = $this->app->hentValg('postnr');
        $efaktura->fakturautstederPoststed  = $this->app->hentValg('poststed');
        $efaktura->fakturautstederTelefon   = $this->app->hentValg('telefon');
        $efaktura->fakturautstederTelefaks  = $this->app->hentValg('telefax');
        $efaktura->fakturautstederEmail     = $this->app->hentValg('epost');
        $efaktura->fakturautstedersOrganisasjonsnummer   = preg_replace('/[^0-9]+/', '', $this->app->hentValg('orgnr'));
        $efaktura->fakturautstederKontonummer   = preg_replace('/[^0-9]+/', '', $this->app->hentValg('bankkonto'));

        $efaktura->forbrukerNavn            = $leieforhold->hentNavn();
        $efaktura->forbrukerAdresse1        = $leieforholdAdresse->adresse1;
        $efaktura->forbrukerAdresse2        = $leieforholdAdresse->adresse2;
        $efaktura->forbrukerPostnummer      = $leieforholdAdresse->postnr;
        $efaktura->forbrukerPoststed        = $leieforholdAdresse->poststed;
        $efaktura->forbrukerLandkode        = $leieforholdAdresse->land != 'Norge' ? $leieforholdAdresse->land : '';
        $efaktura->forkortetNavn            = $leieforhold->hentKortnavn(10);

        $efaktura->fakturanummer            = $this->hentId();
        $efaktura->kid                      = $this->hentKid();
        $efaktura->forfallsdato             = $this->hentForfall();
        $efaktura->beløp                    = $this->hentUtestående();
        $efaktura->fakturadato              = $utskriftsdato;

        $efaktura->overskriftDetaljer
            = $efaktura->strFix( 'Detaljer', $kolonnebredde - 20)
            . $efaktura->strFix( '         Beløp', 20);

        /** @var Krav $krav */
        foreach ($this->hentKrav() as $krav) {
            $efaktura->invoiceDetails[] = $efaktura->strFix( $krav->hentTekst(), $kolonnebredde - 20 )
                . $efaktura->pad( "kr. " . number_format($krav->hentBeløp(), 2, ",", " "), 20, " ", STR_PAD_LEFT );
        }

        $efaktura->invoiceDetails[] = $efaktura->strFix( "Totalt", $kolonnebredde - 20 )
            . $efaktura->pad( "kr. " . number_format(($this->hentBeløp()), 2, ",", " "), 20, " ", STR_PAD_LEFT );

        if( $this->hentBeløp() != $this->hentUtestående() ) {
            $efaktura->invoiceDetails[] = $efaktura->strFix( "Betalinger til fradrag", $kolonnebredde - 20 )
                . $efaktura->pad( "kr. " . number_format(($this->hentUtestående() - $this->hentBeløp()), 2, ",", " "), 20, " ", STR_PAD_LEFT );
        }

        return $efaktura;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function slett()
    {
        if($this->hentUtskriftsdato()) {
            throw new \Exception('Regning ' . $this->hentId() . ' er allerede skrevet ut og kan ikke slettes');
        }
        $krav = $this->hentKrav();
        $krav->settVerdier((object)['regning' => null]);
        $this->samlinger->krav = null;
        return $this;
    }

    /**
     * Lagre PDF
     *
     * Lagre giroen som PDF på tjeneren
     *
     * @param string|null $navn Filnavnet
     * @param bool $erstatt Sann for å erstatte tidligere utskrift
     * @return $this;
     * @throws Exception
     */
    public function lagrePdf(string $navn = null, $erstatt = false): Regning
    {
        $navn = $navn ?: $this->hentId() . '.pdf';

        if( !$this->hentUtskriftsdato() ) {
            throw new Exception('Denne regningen har ikke blitt skrevet ut');
        }

        $filarkiv = Leiebase::$config['leiebasen']['server']['file_storage'] ?? '';
        $fil = "{$filarkiv}/giroer/{$navn}";

        if(file_exists($fil) && !$erstatt) {
            return $this;
        }
        else {
            $pdf = new \Fpdf\Fpdf();
            $this->app->vis(Pdf::class, [
                'pdf' => $pdf,
                'regningsett' => [$this]
            ])->render();
            if (file_exists($fil)) {
                unlink($fil);
            }
            $pdf->Output($fil, 'F');
        }
        return $this;
    }

    public function slettPdf(): Regning
    {
        $this->app->pre($this, __FUNCTION__, []);
        $navn = $this->hentId() . '.pdf';
        $filarkiv = Leiebase::$config['leiebasen']['server']['file_storage'] ?? '';
        $fil = "{$filarkiv}/giroer/{$navn}";
        if (file_exists($fil)) {
            unlink($fil);
        }
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * Henter regninga som pdf
     *
     * @return string Filinnholdet som streng
     * @throws Exception
     */
    public function hentPdf()
    {
        $fil = $this->app->hentFilarkivBane() . "/giroer/{$this->hentId()}.pdf";
        if(!file_exists($fil)) {
            $this->lagrePdf();
        }
        $resultat = file_get_contents($fil);
        if($resultat === false) {
            throw new Exception($fil . ' kunne ikke leses');
        }
        return $resultat;
    }

    /**
     * @return DateTimeInterface|null
     * @throws Exception
     */
    public function hentSisteForfall(): ?DateTimeInterface
    {
        if(!property_exists($this->data, 'siste_forfall')) {
            if (isset($this->rawData->regning_ekstra)
                && property_exists($this->rawData->regning_ekstra, 'siste_forfall')
            ) {
                $this->data->siste_forfall = $this->rawData->regning_ekstra->siste_forfall;
                if ($this->data->siste_forfall) {
                    $this->data->siste_forfall = new DateTime($this->data->siste_forfall);
                    $this->data->siste_forfall->settime(0, 0);
                }
                else {
                    $this->data->siste_forfall = clone $this->hentForfall();
                }
            }
            else {
                /** @var Purring|null $sistePurring */
                $sistePurring = $this->hentSistePurring();
                if ($sistePurring) {
                    $this->data->siste_forfall = clone $sistePurring->hentPurreforfall();
                }
                else {
                    $this->data->siste_forfall = clone $this->hentForfall();
                }
            }
        }
        return $this->data->siste_forfall ? clone $this->data->siste_forfall : null;
    }

    /**
     * @return Purring|null
     * @throws Exception
     */
    public function hentSistePurring(): ?Purring
    {
        if(!property_exists($this->data, 'siste_purring')) {
            if (isset($this->rawData->regning_ekstra)
            && property_exists($this->rawData->regning_ekstra, 'siste_purring')) {
                $this->data->siste_purring = $this->rawData->regning_ekstra->siste_purring ? $this->app->hentModell(Purring::class, $this->rawData->regning_ekstra->siste_purring) : null;
            }
            else {
                $purringsett = $this->hentPurringer();
                $this->data->siste_purring = $purringsett->hentSiste();
            }
        }
        return $this->data->siste_purring;
    }

    /**
     * @return Krav|null
     * @throws Exception
     */
    public function hentSistePurregebyr(): ?Krav
    {
        if(!property_exists($this->data, 'siste_purregebyr')) {
            if (isset($this->rawData->regning_ekstra)
            && property_exists($this->rawData->regning_ekstra, 'siste_purregebyr')) {
                $this->data->siste_purregebyr = $this->rawData->regning_ekstra->siste_purregebyr ? $this->app->hentModell(Krav::class, $this->rawData->regning_ekstra->siste_purregebyr) : null;
            }
            else {
                $sisteGebyrPurring = $this->hentSisteGebyrpurring();
                $this->data->siste_purregebyr = $sisteGebyrPurring ? $sisteGebyrPurring->hentPurregebyr() : null;
            }
        }
        return $this->data->siste_purregebyr;
    }

    /**
     * @return Purring|null
     * @throws Exception
     */
    public function hentSisteGebyrpurring(): ?Purring
    {
        $this->app->pre($this, __FUNCTION__, []);
        if(!property_exists($this->data, 'siste_gebyrpurring')) {
            $this->data->siste_gebyrpurring = null;
            if (isset($this->rawData->regning_ekstra)
            && property_exists($this->rawData->regning_ekstra, 'siste_gebyrpurring')) {
                $this->data->siste_gebyrpurring = $this->rawData->regning_ekstra->siste_gebyrpurring ? $this->app->hentModell(Purring::class, $this->rawData->regning_ekstra->siste_gebyrpurring) : null;
            }
            else {
                $this->hentAntallPurregebyr();
            }
        }
        /** @var Purring|null $purring */
        $purring = $this->app->post($this, __FUNCTION__, $this->data->siste_gebyrpurring);
        return $purring;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function hentAntallPurregebyr(): int
    {
        if(!isset($this->data->antall_purregebyr)) {
            if (isset($this->rawData->regning_ekstra->antall_purregebyr)) {
                $this->data->antall_purregebyr = $this->rawData->regning_ekstra->antall_purregebyr;
            }
            else {
                $this->data->antall_purregebyr = 0;
                foreach($this->hentPurringer() as $purring) {
                    $this->data->siste_purring = $purring;
                    if($purring->hentPurregebyr()) {
                        $this->data->antall_purregebyr++;
                        $this->data->siste_gebyrpurring = $purring;
                    }
                }
            }
        }
        return (int)$this->data->antall_purregebyr;
    }

    /**
     * @param string $purremåte
     * @param DateTime|null $purredato
     * @param bool $kombipurring
     * @param Krav|null $purregebyr
     * @param string $blankettId
     * @return Purring|null
     * @throws Exception
     */
    public function purr(
        string    $purremåte = Purring::PURREMÅTE_GIRO,
        ?DateTime $purredato = null,
        bool      $kombipurring = false,
        ?Krav     $purregebyr = null,
        string    $blankettId = ''
    ): ?Purring
    {
        list(
            'purremåte' => $purremåte,
            'purredato' => $purredato,
            'kombipurring' => $kombipurring,
            'purregebyr' => $purregebyr,
            'blankettId' => $blankettId
        ) = $this->app->pre($this, __FUNCTION__, [
            'purremåte' => $purremåte,
            'purredato' => $purredato,
            'kombipurring' => $kombipurring,
            'purregebyr' => $purregebyr,
            'blankettId' => $blankettId
        ]);
        $purretidspunkt = time();
        if(empty($blankettId)) {
            $blankettId = $purretidspunkt . '-' . $this->hentLeieforhold()->hentId()
                . ($kombipurring ? '' : '-' . $this->hentId());
        }
        $purredato = $purredato ?: new DateTime();
        $purreforfall = clone $purredato;
        $purreforfall->add(new DateInterval($this->app->hentValg('purreforfallsfrist')));
        $bankfridager = $this->app->bankfridager($purreforfall->format('Y'));
        while(
            in_array( $purreforfall->format('m-d'), $bankfridager )
            || $purreforfall->format('N') > 5
        ) {
            $purreforfall->add(new DateInterval('P1D'));
        }

        $sisteForfall = $this->hentSisteForfall();
        if (!$sisteForfall || $sisteForfall > $purredato) {
            return null;
        }

        foreach ($this->hentKrav() as $krav) {
            if($krav->utestående != 0) {
                $this->app->nyModell(Purring::class, (object)[
                    'blankett' => $blankettId,
                    'krav' => $krav,
                    'purredato' => $purredato,
                    'purremåte' => $purremåte,
                    'purrer' => $this->app->bruker['navn'],
                    'purregebyr' => $purregebyr,
                    'purreforfall' => $purreforfall
                ]);
                $krav->nullstillPurringer();
            }
        }
        $this->nullstillPurringer();
        /** @var Purring $purring */
        $purring = $this->app->hentModell(Purring::class, $blankettId);
        return $this->app->post($this, __FUNCTION__, $purring);
    }

    /**
     * Nullstill purredata
     * @return $this
     */
    public function nullstillPurringer(): Regning
    {
        unset($this->samlinger->purringer, $this->collections->purringer);
        unset($this->data->siste_purring, $this->rawData->regning_ekstra->siste_purring);
        unset($this->data->siste_purregebyr, $this->rawData->regning_ekstra->siste_purregebyr);
        unset($this->data->siste_gebyrpurring, $this->rawData->regning_ekstra->siste_gebyrpurring);
        unset($this->data->antall_purregebyr, $this->rawData->regning_ekstra->antall_purregebyr);
        return $this;
    }

    /**
     * Hent FBO Oppdragsfrist
     *
     * Returnerer siste mulige anledning til å sende krav om trekk via AvtaleGiro
     * og opprettholde NETS' frister for varsling.
     * Dersom forfallsdato ikke er fastsatt vil fristen settes til null
     *
     * @param DateTime|null $forfallsdato
     * @return DateTimeImmutable|null
     * @throws Exception
     */
    public function hentFboOppdragsfrist(?DateTimeInterface $forfallsdato = null ): ?DateTimeImmutable
    {
        $forfallsdato = $forfallsdato ?? $this->hentForfall();

        if (!$forfallsdato) {
            return null;
        }

        $leieforhold = $this->hentLeieforhold();
        $fbo = $leieforhold->hentFbo();
        if( !$fbo ) {
            return null;
        }
        return $fbo->oppdragsfrist($forfallsdato);
    }

    /**
     * @param DateTimeImmutable|null $forfallsdato
     * @return Regning
     * @throws CoreModelException
     * @throws \Throwable
     */
    private function endreForfall(?DateTimeImmutable $forfallsdato): Regning
    {
        list(
            'forfallsdato' => $forfallsdato,
            ) = $this->app->pre($this, __FUNCTION__, [
            'forfallsdato' => $forfallsdato,
        ]);

        if($forfallsdato) {
            $forfallsdato = clone $forfallsdato;
            $bankfridager = $this->app->bankfridager($forfallsdato->format('Y'));
            while(
                in_array( $forfallsdato->format('m-d'), $bankfridager )
                || $forfallsdato->format('N') > 5
            ) {
                $forfallsdato = $forfallsdato->add(new DateInterval('P1D'));
            }
        }

        $kravsett = $this->hentKrav();
        $kravsett->sett('forfall', $forfallsdato);
        foreach ($kravsett as $krav) {
            $krav->nullstill();
        }
        $this->samlinger->krav = null;

        /** @var Leieforhold\Betalingsplan\Avdragsett $betalingsplanavdrag */
        $betalingsplanavdragsett = $this->hentSamling('betalingsplanavdrag');
        $betalingsplanavdragsett->forEach(function (Leieforhold\Betalingsplan\Avdrag $avdrag) use ($forfallsdato) {
            $avdrag->settForfallsdato($forfallsdato);
        });

        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @return string
     * @throws NetsException
     */
    private function genererKid(): string
    {
        $this->app->pre($this, __FUNCTION__, []);
        $kidBestyrer = $this->app->hentKidBestyrer();
        $kid = $kidBestyrer->genererKid((int)strval($this->hent('leieforhold')), 0, $this->id);
        return $this->app->post($this, __FUNCTION__, $kid);
    }
}