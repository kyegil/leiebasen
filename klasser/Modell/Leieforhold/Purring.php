<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Kyegil\CoreModel\Interfaces\EavValuesInterface;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Eav\Egenskap as EavEgenskap;
use Kyegil\Leiebasen\Modell\Eav\Egenskapsett;
use Kyegil\Leiebasen\Modell\Eav\EgenskapVarcharObjekt;
use Kyegil\Leiebasen\Modell\Eav\VerdiVarcharObjekt;
use Kyegil\Leiebasen\Modell\Eav\VerdiVarcharObjektsett;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Purring\Kravlink;
use stdClass;

/**
 * Class Purring
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @property string $blankett
 * @property DateTime $purredato
 * @property string $purremåte
 * @property string $purrer
 * @property Krav|null $purregebyr
 * @property DateTime|null $purreforfall
 *
 * @method string hentBlankett()
 * @method DateTime hentPurredato()
 * @method string hentPurremåte()
 * @method string hentPurrer()
 * @method Krav|null hentPurregebyr()
 * @method DateTime|null hentPurreforfall()
 *
 * @method $this settPurregebyr(Krav $krav)
 */
class Purring extends Modell
{
    /**
     *
     */
    const PURREMÅTE_GIRO = 'giro';

    /**
     *
     */
    const PURREMÅTE_EPOST = 'epost';

    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'purringer';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'blankett';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Purringsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * @param string $referanse
     * @param string|null $modell
     * @param string|null $fremmedNøkkel
     * @param array $filtre
     * @param callable|null $callback
     * @return array|null
     * @throws Exception
     */
    public static function harFlere(string $referanse, ?string $modell = null, ?string $fremmedNøkkel = null, array $filtre = [], ?callable $callback = null): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::harFlere('kravlinker',
                Purring\Kravlink::class,
                'blankett'
            );
        }
        return parent::harFlere($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array {
        return [
            'blankett' => [
                'type' => 'string'
            ],
            'purredato'    => [
                'type'  => 'date',
                'allowNull' => true
            ],
            'purremåte'    => [
                'type'  => 'string'
            ],
            'purrer'    => [
                'type'  => 'string'
            ],
            'purregebyr'    => [
                'type' => Krav::class,
                'allowNull' => true,
                'rawDataContainer' => 'purregebyr'
            ],
            'purreforfall'    => [
                'type'  => 'date',
                'allowNull' => true
            ]
        ];
    }

    /**
     * @param stdClass $parametere
     * @return Modell
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Modell
    {
        if(!property_exists($parametere, 'blankett')) {
            throw new Exception('Id-feltet blankett må oppgis for alle nye purringer');
        }
        $id = $parametere->blankett;

        /**
         * Spesiell håndtering av oppretting av purringer,
         * fordi id-feltet ikke er databasens primærnøkkelfelt
         */

        /**
         * Trekk ut primær-parameterene for å opprette ny modell.
         * De resterende parameterene settes etterpå
         */
        $parametere = clone $parametere;
        $primærParametere = new stdClass();
        $mapping = $this->getDataMapping();
        foreach($parametere as $egenskap => $verdi) {
            if (isset($mapping[$egenskap])) {
                $primærParametere->{$egenskap} = $verdi;
                unset($parametere->{$egenskap});
            }
        }

        /**
         * Endre primærnøkkelfeltet midlertidig
         * @var $primærnøkkelfelt
         */
        parent::opprett($primærParametere);
        $this->settId($id);

        /**
         * Sett de resterende parameterene
         */
        foreach($parametere as $egenskap => $verdi) {
            $this->sett($egenskap, $verdi);
        }
        return $this;
    }

    /**
     * @return Leieforhold|null
     * @throws Exception
     */
    public function hentLeieforhold(): ?Leieforhold
    {
        if(!property_exists($this->data, 'leieforhold')) {
            $this->data->leieforhold = null;
            if (property_exists($this->rawData, 'leieforhold')) {
                $leieforholdId = $this->rawData->leieforhold->{Leieforhold::hentPrimærnøkkelfelt()} ?? null;
            }
            else {
                $blankettRefElementer = explode('-', $this->hentId());
                $leieforholdId = $blankettRefElementer[1] ?? null;
            }
            if ($leieforholdId) {
                /** @var Leieforhold $leieforhold */
                $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);
                if (property_exists($this->rawData, 'leieforhold')) {
                    $leieforhold->setPreloadedValues($leieforhold->mapNamespacedRawData($this->rawData->leieforhold));
                }
                if($leieforhold->hentId()) {
                    $this->data->leieforhold = $leieforhold;
                }
            }
        }
        return $this->data->leieforhold;
    }

    /**
     * @param array|object|null $param
     *      float beløp
     *      string tekst
     * @return Purring
     * @throws Exception
     */
    public function opprettGebyr($param = null): Purring
    {
        if( !$this->hentId() ) {
            return $this;
        }
        if( $this->hentPurregebyr() ) {
            return $this;
        }
        settype($param, 'object');
        $param->beløp = $param->beløp ?? $this->app->hentValg('purregebyr');
        $param->tekst = isset($param->tekst) ? $param->beløp : "Purregebyr for betalingspåminnelse den {$this->hentPurredato()->format('d.m.Y')}";

        /** @var Krav $gebyrkrav */
        $gebyrkrav = $this->app->nyModell(Krav::class, (object)[
            'beløp' => $param->beløp,
            'leieforhold' => $this->hentLeieforhold(),
            'kontrakt' => $this->hentLeieforhold()->hentKontrakt(),
            'kravdato' => $this->hentPurredato(),
            'oppretter' => $this->hentPurrer(),
            'opprettet' => $this->hentPurredato(),
            'tekst' => $param->tekst,
            'type' => Krav::TYPE_PURREGEBYR,
            'utestående' => $param->beløp,
            'utskriftsdato' => $this->hentPurredato()
        ]);

        $this->settPurregebyr($gebyrkrav);
        return $this;
    }

    /**
     * @return Purring\Kravlinksett
     * @throws Exception
     */
    public function hentKravlinker(): Purring\Kravlinksett
    {
        /** @var Purring\Kravlinksett $kravlinker */
        $kravlinker = $this->hentSamling('kravlinker');
        return $kravlinker;
    }

    /**
     * @return Kravsett
     * @throws Exception
     * @deprecated
     * @see Purring::hentKravlinker()
     */
    public function hentKrav(): Kravsett
    {
        /** @var Kravsett $krav */
        $krav = $this->app->hentSamling(Krav::class);
        $krav->leggTilInnerJoin(static::hentTabell(), static::hentTabell() . '.krav = ' . Krav::hentTabell() . '.' . Krav::hentPrimærnøkkelfelt())
            ->leggTilFilter([
                '`' . static::hentTabell() . '`.`' . static::hentPrimærnøkkelfelt() . '`' => $this->hentId()
            ])
        ;
        if(!isset($this->samlinger->krav)) {
            $this->samlinger->krav = $krav->hentElementer();
        }
        $krav->lastFra($this->samlinger->krav);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $krav;
    }

    /**
     * @return float
     * @throws Exception
     */
    public function hentUtestående()
    {
        if (!isset($this->data->utestående)) {
            $this->data->utestående = 0;
            /** @var Kravlink $kravlink */
            foreach($this->hentKravlinker() as $kravlink) {
                if($kravlink->krav) {
                    $this->data->utestående += $kravlink->krav->utestående;
                }
            }
        }
        return $this->data->utestående;
    }

    /**
     * @param $egenskap
     * @param $verdi
     * @return Modell
     * @throws Exception
     */
    public function sett($egenskap, $verdi): Modell
    {
        if($egenskap == 'krav') {
            return $this->settKrav($verdi);
        }
        return parent::sett($egenskap, $verdi);
    }

    /**
     * @param Krav $krav
     * @return Purring
     * @throws Exception
     */
    public function settKrav(Krav $krav): Purring
    {
        $this->mysqli->save([
            'table' => $this->mysqli->table_prefix . self::hentTabell(),
            'update' => true,
            'limit' => 1,
            'fields' => [
                'krav' => $krav->hentId()
            ],
            'where' => [
                self::hentPrimærnøkkelfelt() => $this->hentId(),
                'krav' => null
            ]
        ]);

        return $this;
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    public function hentEavKonfigurering(): stdClass
    {
        if(!isset($this->eavConfigs)) {
            if(!isset(EgenskapVarcharObjekt::$mellomlager[static::class])) {
                /** @var Egenskapsett $egenskaper */
                $egenskaper = $this->app->hentSamling(EgenskapVarcharObjekt::class)
                    ->leggTilFilter(['modell' => static::class])
                    ->låsFiltre()
                    ->leggTilSortering('rekkefølge');
                $kilde = EgenskapVarcharObjekt::hentTabell();
                EgenskapVarcharObjekt::$mellomlager[static::class] = (object)[$kilde => (object)[]];
                /** @var EgenskapVarcharObjekt $egenskap */
                foreach ($egenskaper as $egenskap) {
                    $kode = $egenskap->hent('kode');
                    EgenskapVarcharObjekt::$mellomlager[static::class]->{$kilde}->{$kode} = $egenskap;
                }
            }
            $this->eavConfigs = EgenskapVarcharObjekt::$mellomlager[static::class];
        }
        return parent::hentEavKonfigurering();
    }

    /**
     * @param string $kilde
     * @return stdClass|null
     * @throws Exception
     */
    public function hentEavVerdiObjekter(string $kilde): ?stdClass
    {
        if(!$this->eavValueObjects) {
            $this->eavValueObjects = new stdClass();

            /** @var VerdiVarcharObjektsett $eavVerdiObjekter */
            $eavVerdiObjekter = $this->app->hentSamling(VerdiVarcharObjekt::class)

                ->leggTilInnerJoin(EavEgenskap::hentTabell(), VerdiVarcharObjekt::hentTabell() . '.egenskap_id = ' . EavEgenskap::hentTabell() . '.id')
                ->leggTilFilter([
                    EavEgenskap::hentTabell() . '.modell' => static::class,
                    VerdiVarcharObjekt::hentTabell() . '.objekt_id' => $this->getId()
                ]);
            /** @var EavValuesInterface $eavVerdiObjekt */
            foreach ($eavVerdiObjekter as $eavVerdiObjekt) {
                $verdiKilde = $eavVerdiObjekt->getConfig()::getDbTable();
                $kode = $eavVerdiObjekt->getConfig()->getSourceField();
                settype($this->eavValueObjects->{$verdiKilde}, 'object');
                $this->eavValueObjects->{$verdiKilde}->{$kode} = $eavVerdiObjekt;
            }
        }

        return parent::hentEavVerdiObjekter($kilde);
    }

    /**
     * @param DateTimeInterface|null $forfallsdato
     * @return Modell|Purring
     * @throws Exception
     */
    public function settPurreforfall(DateTimeInterface $forfallsdato = null)
    {
        if($forfallsdato instanceof DateTime) {
            $forfallsdato = DateTimeImmutable::createFromMutable($forfallsdato);
        }
        if($forfallsdato) {
            $bankfridager = $this->app->bankfridager($forfallsdato->format('Y'));
            while(
                in_array( $forfallsdato->format('m-d'), $bankfridager )
                || $forfallsdato->format('N') > 5
            ) {
                $forfallsdato = $forfallsdato->add(new DateInterval('P1D'));
            }
        }
        return parent::sett('purreforfall', $forfallsdato);
    }
}