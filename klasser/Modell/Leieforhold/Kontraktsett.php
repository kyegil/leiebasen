<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Sett;

/**
 * Class Kontraktsett
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @method Kontrakt current()
 */
class Kontraktsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Kontrakt::class;

    /**
     * @var Kontrakt[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }

    /**
     * Legg til LEFT JOIN for leieforhold
     *
     * @return $this
     */
    public function leggTilLeftJoinForLeieforhold(): Kontraktsett
    {
        return $this->leggTilLeftJoin(
            Leieforhold::hentTabell(),
            '`' . Kontrakt::hentTabell() . '`.`leieforhold` = `' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`'
        );
    }

    /**
     * Legg til leieforhold-modell
     *
     * @return $this
     */
    public function leggTilLeieforholdModell(): Kontraktsett
    {
        return $this->leggTilLeftJoinForLeieforhold()
            ->leggTilModell(Leieforhold::class);
    }

    public function forhåndslastNavn(): Kontraktsett
    {
        $tp = $this->mysqli->table_prefix;
        $leietakerTabell = $tp . Leietaker::hentTabell();
        $personTabell = $tp . Person::hentTabell();
        $subQuery = <<< SQL
  SELECT
  IF(COUNT(DISTINCT `personer`.`personid`) > 2, CONCAT(LEFT(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', '), CHAR_LENGTH(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL,`kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`,`personer`.`etternavn`,CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', ')) - INSTR(REVERSE(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', ')), ' ,') - 1), ' og ', SUBSTRING(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', '), CHAR_LENGTH(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', ')) - INSTR(REVERSE(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', ')), ' ,') + 2)),   GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ' og ')) AS `navn`

  FROM `{$leietakerTabell}` AS `kontraktpersoner`
  LEFT JOIN `{$personTabell}` AS `personer` ON `personer`.`personid` = `kontraktpersoner`.`person`
  WHERE `kontraktpersoner`.`slettet` IS NULL
  AND `kontraktpersoner`.`kontrakt` = `kontrakter`.`kontraktnr`
  ORDER BY `kontraktpersoner`.`kopling`
SQL;
        $this->leggTilUttrykk('kontrakt_ekstra', 'navn', '(' . $subQuery . ')');
        return $this;
    }

    /**
     * @return Leietakersett
     */
    public function inkluderLeietakere(): Leietakersett {
        /** @var Leietakersett $leietakere */
        $leietakere = $this->inkluder('leietakere');
        return $leietakere;
    }
}