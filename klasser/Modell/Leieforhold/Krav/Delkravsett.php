<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold\Krav;


use Exception;
use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Sett;

/**
 * Delkravsett
 * @package Kyegil\Leiebasen\Modell\Leieforhold\Krav
 *
 * @method Delkrav current()
 */
class Delkravsett extends Sett
{
    /** @var string */
    protected string $model = Delkrav::class;

    /** @var Delkrav[] */
    protected ?array $items = null;

    /**
     * @param string $model
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $model, array $di)
    {
        parent::__construct($model, $di);
        $this
            ->leggTilLeftJoin(Krav::hentTabell(), Delkrav::hentTabell() . '.`kravid` = ' . Krav::hentTabell() . '.' . Krav::hentPrimærnøkkelfelt())

            ->leggTilLeftJoinForDelkravtype()

            ->leggTilLeftJoin(Leieforhold::hentTabell(), Krav::hentTabell() . '.`leieforhold` = ' . Leieforhold::hentTabell() . '.' . Leieforhold::hentPrimærnøkkelfelt())
            ->leggTilModell(Leieforhold::class, implode('.', [Krav::hentTabell(), Leieforhold::hentTabell()]))
        ;
    }

    /**
     * @return Delkravsett
     */
    public function leggTilLeftJoinForDelkravtype(): Delkravsett
    {
        return $this
            ->leggTilLeftJoin(Delkravtype::hentTabell(), Delkrav::hentTabell() . '.`delkravtype` = ' . Delkravtype::hentTabell() . '.' . Delkravtype::hentPrimærnøkkelfelt());
    }

    /**
     * @return Delkravsett
     * @throws Exception
     */
    public function leggTilKravModell(): Delkravsett
    {
        return $this->leggTilModell(Krav::class);
    }

    /**
     * @return Delkravsett
     * @throws Exception
     */
    public function leggTilDelkravtypeModell(): Delkravsett
    {
        return $this->leggTilLeftJoinForDelkravtype()->leggTilModell(Delkravtype::class);
    }

    /**
     * @return Delkravsett
     * @throws Exception
     */
    public function leggTilLeieforholdModell(): Delkravsett
    {
        return $this->leggTilModell(Leieforhold::class, implode('.', [Krav::hentTabell(), Leieforhold::hentTabell()]));
    }
}