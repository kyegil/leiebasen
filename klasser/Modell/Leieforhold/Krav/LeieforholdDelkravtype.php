<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 12/06/2020
 * Time: 10:42
 */

namespace Kyegil\Leiebasen\Modell\Leieforhold\Krav;


use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Leieforhold;
use stdClass;

/**
 * Class LeieforholdDelkravtype
 * @package Kyegil\Leiebasen\Modell\Leieforhold\Krav
 *
 * @property int $id
 * @property Leieforhold $leieforhold
 * @property Delkravtype $delkravtype
 * @property boolean $selvstendigTillegg
 * @property boolean $periodiskAvstemming
 * @property boolean $relativ
 * @property float $sats
 *
 * @method int hentId()
 * @method Leieforhold hentLeieforhold()
 * @method Delkravtype hentDelkravtype()
 * @method boolean hentSelvstendigTillegg()
 * @method boolean hentPeriodiskAvstemming()
 * @method boolean hentRelativ()
 * @method float hentSats()
 *
 * @method $this settId(int $id)
 * @method $this settDelkravtype(Delkravtype $delkravtype)
 * @method $this settSelvstendigTillegg(bool $selvstendigTillegg)
 * @method $this settPeriodiskAvstemming(bool $periodiskAvstemming)
 * @method $this settRelativ(bool $relativ)
 * @method $this settSats(float $sats)
 */
class LeieforholdDelkravtype extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'leieforhold_delkrav';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = LeieforholdDelkravtypesett::class;

    /**
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'leieforhold'    => [
                'type' => Leieforhold::class
            ],
            'delkravtype'    => [
                'type'  => Delkravtype::class,
                'rawDataContainer' => Delkravtype::hentTabell()
            ],
            'selvstendig_tillegg'    => [
                'type'  => 'boolean'
            ],
            'relativ'    => [
                'type'  => 'boolean'
            ],
            'sats'    => [
                'type'  => 'string'
            ],
            'periodisk_avstemming'    => [
                'type'  => 'boolean'
            ]
        ];
    }

    /**
     * @return int
     * @throws Exception
     */
    public function hentDelkravTypeId()
    {
        return $this->hentDelkravtype()->hentId();
    }

    /**
     * @return string
     */
    public function hentKode()
    {
        return $this->hentDelkravtype()->hentKode();
    }

    /**
     * @return string
     */
    public function hentNavn()
    {
        return $this->hentDelkravtype()->hentNavn();
    }

    /**
     * @return string
     */
    public function hentBeskrivelse()
    {
        return $this->hentDelkravtype()->hentBeskrivelse();
    }

    /**
     * @return float
     */
    public function hentBeløp()
    {
        if(!isset($this->data->beløp)) {
            if($this->hentRelativ()) {
                $this->data->beløp = round($this->hentSats() * $this->hentLeieforhold()->årligBasisleie);
            }
            else {
                $this->data->beløp = $this->hentSats();
            }
        }
        return $this->data->beløp;
    }

    /**
     * @param Leieforhold $leieforhold
     * @return $this
     * @throws Exception
     */
    public function settLeieforhold(Leieforhold $leieforhold): LeieforholdDelkravtype
    {
        /** @var Leieforhold $eksisterendeLeieforhold */
        $eksisterendeLeieforhold = $this->hent('leieforhold');
        parent::sett('leieforhold', $leieforhold);
        if ($eksisterendeLeieforhold
            && $eksisterendeLeieforhold->hentId()
            && $eksisterendeLeieforhold->hentId() != $leieforhold->hentId()
        ) {
            $eksisterendeLeieforhold->oppdaterLeie();
        }
        $leieforhold->oppdaterLeie();
        return $this;
    }

    /**
     * @param string $egenskap
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($egenskap, $verdi): LeieforholdDelkravtype
    {
        if($egenskap == 'leieforhold') {
            return $this->settLeieforhold($verdi);
        }
        parent::sett($egenskap, $verdi);
        if (in_array($egenskap, ['delkravtype', 'selvstendig_tillegg', 'relativ', 'sats'])) {
            $this->leieforhold->oppdaterLeie();
        }
        return $this;
    }

    /**
     * @param stdClass $parametere
     * @return LeieforholdDelkravtype
     * @throws Exception
     */
    public function opprett(stdClass $parametere): LeieforholdDelkravtype
    {
        parent::opprett($parametere);
        if(isset($parametere->leieforhold) && $parametere->leieforhold instanceof Leieforhold) {
            $parametere->leieforhold->oppdaterLeie();
        }
        return $this;
    }

    /**
     * @return float
     */
    public function hentTerminbeløp()
    {
        $antTerminer = $this->hentLeieforhold()->hentAntTerminer() ?: 1;
        return $this->hentBeløp() / $antTerminer;
    }
}