<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold\Krav;


use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Sett;

/**
 * LeieforholdDelkravtypesett
 * @package Kyegil\Leiebasen\Modell\Leieforhold\Krav
 *
 * @method LeieforholdDelkravtype current()
 */
class LeieforholdDelkravtypesett extends Sett
{
    protected string $model = LeieforholdDelkravtype::class;

    /**
     * @var LeieforholdDelkravtype[]
     */
    protected ?array $items = null;

    public function __construct(string $model, array $di)
    {
        parent::__construct($model, $di);
        $this
            ->leggTilLeieforholdModell()
            ->leggTilDelkravTypeModell()
        ;
    }

    /**
     * @return LeieforholdDelkravtypesett
     */
    public function leggTilLeftJoinForLeieforhold(): LeieforholdDelkravtypesett
    {
        return $this
            ->leggTilLeftJoin(Leieforhold::hentTabell(), LeieforholdDelkravtype::hentTabell() . '.leieforhold = ' . Leieforhold::hentTabell() . '.' . Leieforhold::hentPrimærnøkkelfelt());
    }

    public function leggTilLeieforholdModell(): LeieforholdDelkravtypesett
    {
        return $this
            ->leggTilLeftJoinForLeieforhold()
            ->leggTilModell(Leieforhold::class)
            ;
    }

    /**
     * @return LeieforholdDelkravtypesett
     */
    public function leggTilLeftJoinForDelkravtype(): LeieforholdDelkravtypesett
    {
        return $this
            ->leggTilLeftJoin(Delkravtype::hentTabell(), LeieforholdDelkravtype::hentTabell() . '.delkravtype = ' . Delkravtype::hentTabell() . '.' . Delkravtype::hentPrimærnøkkelfelt());
    }

    public function leggTilDelkravTypeModell(): LeieforholdDelkravtypesett
    {
        return $this
            ->leggTilLeftJoinForDelkravtype()
            ->leggTilModell(Delkravtype::class)
            ;
    }
}