<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 12/06/2020
 * Time: 10:42
 */

namespace Kyegil\Leiebasen\Modell\Leieforhold\Krav;


use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;

/**
 * Class Delkrav
 * @package Kyegil\Leiebasen\Modell\Leieforhold\Krav
 *
 * @property int $id
 * @property Krav $krav
 * @property Delkravtype $delkravtype
 * @property float $beløp
 *
 * @method int hentId()
 * @method Krav hentKrav()
 * @method Delkravtype hentDelkravtype()
 * @method float hentBeløp()
 *
 * @method $this settId(int $id)
 * @method $this settKrav(Krav $krav)
 * @method $this settDelkravtype(Delkravtype $delkravtype)
 * @method $this settBeløp(float $beløp)
 */
class Delkrav extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'delkrav';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Delkravsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'kravid'    => [
                'target' => 'krav',
                'type'  => Krav::class,
                'rawDataContainer' => Krav::hentTabell()
            ],
            'delkravtype'    => [
                'type'  => Delkravtype::class,
                'rawDataContainer' => Delkravtype::hentTabell()
            ],
            'beløp'    => [
                'type'  => 'string'
            ]
        ];
    }
}