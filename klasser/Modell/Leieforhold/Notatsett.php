<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 03/05/2021
 * Time: 09:56
 */

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use Kyegil\Leiebasen\Sett;

/**
 * Notatsett
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @method Notat current()
 * @method Notat|null hentFørste()
 * @method Notat|null hentSiste()
 * @method Notat|null hentTilfeldig()
 */
class Notatsett extends Sett
{
    protected string $model = Notat::class;

    /** @var Notat[] */
    protected ?array $items = null;

    public function __construct(string $model, array $di)
    {
        parent::__construct($model, $di);
    }
}