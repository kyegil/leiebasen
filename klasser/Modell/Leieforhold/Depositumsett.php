<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use Kyegil\Leiebasen\Sett;

/**
 * Depositumsett
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @method Depositum current()
 * @method Depositum|null hentFørste()
 * @method Depositum|null hentSiste()
 * @method Depositum|null hentTilfeldig()
 */
class Depositumsett extends Sett
{
    /** @var string */
    protected string $model = Depositum::class;

    /** @var Depositum[] */
    protected ?array $items = null;
}