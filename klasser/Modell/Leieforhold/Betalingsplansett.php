<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use Kyegil\Leiebasen\Sett;

/**
 * Betalingsplansett
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @method Betalingsplan current()
 * @method Betalingsplan|null hentFørste()
 * @method Betalingsplan|null hentSiste()
 * @method Betalingsplan|null hentTilfeldig()
 */
class Betalingsplansett extends Sett
{
    /** @var string */
    protected string $model = Betalingsplan::class;

    /** @var Betalingsplan[] */
    protected ?array $items = null;
}