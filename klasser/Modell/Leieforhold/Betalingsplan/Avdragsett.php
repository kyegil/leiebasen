<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Modell\Leieforhold\Fbo\Trekk;
use Kyegil\Leiebasen\Sett;

/**
 * Avdragsett
 * @package Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan
 *
 * @method Avdrag current()
 * @method Avdrag|null hentFørste()
 * @method Avdrag|null hentSiste()
 * @method Avdrag|null hentTilfeldig()
 */
class Avdragsett extends Sett
{
    /** @var string */
    protected string $model = Avdrag::class;

    /** @var Avdrag[] */
    protected ?array $items = null;

    /**
     * @param string $model
     * @param array $di
     * @throws \Kyegil\CoreModel\CoreModelDiException
     */
    public function __construct(string $model, array $di)
    {
        parent::__construct($model, $di);
    }

    /**
     * Legg til join for betalingsplan
     *
     * @param string|null $tabellAlias
     * @return $this
     * @throws Exception
     */
    public function leggTilLeftJoinForBetalingsplan(?string $tabellAlias = null):Avdragsett {
        $tabellAlias = $tabellAlias ?: Betalingsplan::hentTabell();
        $this
            ->leggTilLeftJoin([$tabellAlias => Betalingsplan::hentTabell()], '`' . $tabellAlias . '`.`' . Betalingsplan::hentPrimærnøkkelfelt() . '` = `' . Avdrag::hentTabell() . '`.`betalingsplan_id`')
        ;
        return $this;
    }

    /**
     * Legg til join for trekk i faste betalingsoppdrag
     *
     * @param string|null $tabellAlias
     * @return $this
     * @throws Exception
     */
    public function leggTilLeftJoinForFboTrekk(?string $tabellAlias = null):Avdragsett {
        $tabellAlias = $tabellAlias ?: Trekk::hentTabell();
        $this
            ->leggTilEavVerdiJoin('avtalegiro_trekk')
            ->leggTilLeftJoin([$tabellAlias => Trekk::hentTabell()], '`' . $tabellAlias . '`.`' . Trekk::hentPrimærnøkkelfelt() . '` = `avtalegiro_trekk`.`verdi`')
        ;
        return $this;
    }

    /**
     * Legg til betalingsplan-modell
     *
     * @return $this
     * @throws Exception
     */
    public function leggTilBetalingsplanModell(): Avdragsett
    {
        $this
            ->leggTilLeftJoinForBetalingsplan()
            ->leggTilModell(Betalingsplan::class)
        ;
        return $this;
    }
}