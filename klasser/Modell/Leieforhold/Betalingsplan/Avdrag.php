<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;

use DateTime;
use DateTimeInterface;
use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Modell\Leieforhold\Fbo\Trekk;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Visning\felles\epost\shared\Epost;

/**
 * Class Avdrag
 * @package Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan
 *
 * @property int $id
 * @property Betalingsplan $betalingsplan
 * @property DateTime $forfallsdato
 * @property float $beløp
 * @property float $utestående
 * @property Regning|null $regning
 * @property Trekk|null $avtalegiroTrekk
 * @property DateTime|null $påminnelseSendt
 * @property DateTime|null $etterlysningSendt
 *
 * @method int hentId()
 * @method Betalingsplan hentBetalingsplan()
 * @method DateTime hentForfallsdato()
 * @method float hentBeløp()
 * @method float hentUtestående()
 * @method Regning|null hentRegning()
 * @method Trekk|null hentAvtalegiroTrekk()
 * @method DateTime|null hentPåminnelseSendt()
 * @method DateTime|null hentEtterlysningSendt()
 *
 * @method $this settBetalingsplan(Betalingsplan $betalingsplan)
 * @method $this settForfallsdato(DateTimeInterface $forfallsdato)
 * @method $this settBeløp(float $beløp)
 * @method $this settUtestående(float $utestående)
 * @method $this settRegning(Regning|null $regning)
 * @method $this settAvtalegiroTrekk(?Trekk $avtalegiroTrekk = null)
 * @method $this settPåminnelseSendt(DateTimeInterface|null $påminnelseSendt)
 * @method $this settEtterlysningSendt(DateTimeInterface|null $etterlysningSendt)
 */
class Avdrag extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'betalingsplan_avdrag';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Avdragsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'betalingsplan_id'    => [
                'target' => 'betalingsplan',
                'type' => Betalingsplan::class,
                'rawDataContainer' => Betalingsplan::hentTabell()
            ],
            'beløp'    => [
                'type'  => 'string'
            ],
            'utestående'    => [
                'type'  => 'string'
            ],
            'forfallsdato'    => [
                'type'  => 'date'
            ],
            'giro_id'    => [
                'type'  => Regning::class,
                'target' => 'regning',
                'allowNull'  => true
            ],
            'påminnelse_sendt'    => [
                'type'  => 'datetime',
                'allowNull'  => true
            ],
            'etterlysning_sendt'    => [
                'type'  => 'datetime',
                'allowNull'  => true
            ]
        ];
    }

    /**
     * @throws Exception
     */
    public function slett()
    {
        /** @var Trekk|null $avtalegiroTrekk */
        $avtalegiroTrekk = $this->hent('avtalegiro_trekk');
        if($avtalegiroTrekk  && $avtalegiroTrekk->hentId()) {
            $avtalegiroTrekk->settTilSletting(new \DateTime());
        }
        parent::slett();
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function sendEtterlysning()
    {
        $this->app->pre($this, __FUNCTION__, []);
        $betalingsplan = $this->hentBetalingsplan();
        $leieforhold = $betalingsplan->leieforhold;
        $epost = $leieforhold->hentEpost();
        if (!$epost) {
            $betalingsplan->forfallspåminnelseAntDager = null;
            return $this->app->post($this, __FUNCTION__, $this);
        }
        $innhold = $this->app->hentValg('betalingsplan_etterlysning_mal') ?? '';
        $innhold = str_replace([
            '{forfallsdato}',
            '{beløp}',
            '{leieforholdnr}',
            '{leieforhold}',
            '{kid}',
        ],
            [
                $this->hentForfallsdato()->format('d.m.Y'),
                $this->app->kr($this->hentBeløp()),
                $leieforhold->hentId(),
                $leieforhold->hentBeskrivelse(),
                $leieforhold->hentKid()
            ],
            $innhold);
        $tittel = 'Har du glemt betalingsplanen din?';
        /** @var Epost $html */
        $html = $this->app->hentVisning(Epost::class, [
            'tittel' => $tittel,
            'innhold' => $innhold
        ]);
        $tekst = $this->app->hentVisning(Epost::class, [
            'tittel' => $tittel,
            'innhold' => Epost::br2crnl($innhold)
        ])->settMal('felles/epost/shared/Epost.txt');

        $this->app->sendMail((object)[
            'to' => implode(',', $epost),
            'subject' => $tittel,
            'html' => $html,
            'text' => $tekst,
            'priority' => 10,
            'type' => 'betalingsplan_manglende_avdrag',
        ]);
        $this->etterlysningSendt = new DateTime();
        return $this->app->post($this, __FUNCTION__, $this);
    }
}