<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;

use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;

/**
 * Class Originalkrav
 * @package Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan
 *
 * @property int $id
 * @property Krav $krav
 * @property Betalingsplan $betalingsplan
 * @propert string|float $uteståendeGrunnlag
 *
 * @method int hentId()
 * @method Krav hentKrav()
 * @method Betalingsplan hentBetalingsplan()
 *
 * @method $this settKrav(Krav $krav)
 * @method $this settBetalingsplan(Betalingsplan $betalingsplan)
 * @method $this settUteståendeGrunnlag(float $uteståendeGrunnlag)
 */
class Originalkrav extends \Kyegil\Leiebasen\Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'betalingsplan_originalkrav';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Originalkravsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * @inheritDoc
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'krav_id' => [
                'target' => 'krav',
                'type' => Krav::class,
                'rawDataContainer' => Krav::hentTabell()
            ],
            'betalingsplan_id'    => [
                'target' => 'betalingsplan',
                'type' => Betalingsplan::class,
                'rawDataContainer' => Betalingsplan::hentTabell()
            ],
            'utestående_grunnlag'    => [
                'type' => 'string',
            ],
        ];
    }
}