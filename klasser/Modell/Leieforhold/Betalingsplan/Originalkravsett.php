<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Sett;

/**
 * Originalkravsett
 * @package Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan
 *
 * @method Originalkrav current()
 * @method Originalkrav|null hentFørste()
 * @method Originalkrav|null hentSiste()
 * @method Originalkrav|null hentTilfeldig()
 */
class Originalkravsett extends Sett
{
    /** @var string */
    protected string $model = Originalkrav::class;

    /** @var Originalkrav[] */
    protected ?array $items = null;

    /**
     * @param string $model
     * @param array $di
     * @throws \Kyegil\CoreModel\CoreModelDiException
     */
    public function __construct(string $model, array $di)
    {
        parent::__construct($model, $di);
    }

    /**
     * Legg til join for trekk i faste betalingsoppdrag
     *
     * @param string|null $tabellAlias
     * @return $this
     * @throws Exception
     */
    public function leggTilLeftJoinForBetalingsplan(?string $tabellAlias = null):Originalkravsett {
        $tabellAlias = $tabellAlias ?: Betalingsplan::hentTabell();
        $this
            ->leggTilLeftJoin([$tabellAlias => Betalingsplan::hentTabell()], '`' . $tabellAlias . '`.`' . Betalingsplan::hentPrimærnøkkelfelt() . '` = `' . Originalkrav::hentTabell() . '`.`betalingsplan_id`')
        ;
        return $this;
    }

    /**
     * Legg til join for trekk i faste betalingsoppdrag
     *
     * @param string|null $tabellAlias
     * @return $this
     * @throws Exception
     */
    public function leggTilLeftJoinForKrav(?string $tabellAlias = null):Originalkravsett {
        $tabellAlias = $tabellAlias ?: Krav::hentTabell();
        $this
            ->leggTilLeftJoin(
                [$tabellAlias => Krav::hentTabell()],
                '`' . $tabellAlias . '`.`' . Krav::hentPrimærnøkkelfelt() . '` = `' . Originalkrav::hentTabell() . '`.`krav_id`'
            )
        ;
        return $this;
    }

    /**
     * Legg til betalingsplan-modell
     *
     * @return $this
     * @throws Exception
     */
    public function leggTilBetalingsplanModell(): Originalkravsett
    {
        $this
            ->leggTilLeftJoinForBetalingsplan()
            ->leggTilModell(Betalingsplan::class)
        ;
        return $this;
    }

    /**
     * Legg til krav-modell
     *
     * @return $this
     * @throws Exception
     */
    public function leggTilKravModell(): Originalkravsett
    {
        $this
            ->leggTilLeftJoinForKrav()
            ->leggTilModell(Krav::class)
        ;
        return $this;
    }
}