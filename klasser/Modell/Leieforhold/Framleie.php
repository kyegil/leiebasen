<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 01/12/2020
 * Time: 16:26
 */

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use DateTime;
use Exception;
use Kyegil\CoreModel\Interfaces\EavValuesInterface;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Eav\Egenskap as EavEgenskap;
use Kyegil\Leiebasen\Modell\Eav\Verdi as EavVerdi;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleie\Framleietaker;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Personsett;
use Kyegil\Leiebasen\Samling;
use stdClass;

/**
 * Class Framleie
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @property integer $nr
 * @property Leieforhold $leieforhold
 * @property DateTime $fradato
 * @property DateTime $tildato
 * @property string $avtalemal
 * @property string $avtale
 *
 * @method integer hentNr()
 * @method Leieforhold hentLeieforhold()
 * @method DateTime hentFradato()
 * @method DateTime hentTildato()
 * @method string hentAvtalemal()
 * @method string hentAvtale()
 *
 * @method $this settLeieforhold(Leieforhold $leieforhold)
 * @method $this settFradato(DateTime $fradato)
 * @method $this settTildato(DateTime $tildato)
 * @method $this settAvtalemal(string $avtalemal)
 * @method $this settAvtale(string $avtale)
 */
class Framleie extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'framleie';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'nr';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Framleiesett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array {
        return [
            'nr' => [
                'type' => 'integer'
            ],
            'leieforhold'    => [
                'type'  => Leieforhold::class,
                'rawDataContainer' => Leieforhold::hentTabell()
            ],
            'fradato'    => [
                'type'  => 'date'
            ],
            'tildato'    => [
                'type'  => 'date'
            ],
            'avtalemal'    => [
                'type'  => 'string'
            ],
            'avtale'    => [
                'type'  => 'string'
            ]
        ];
    }

    /**
     * @param string $kilde
     * @return stdClass|null
     * @throws Exception
     */
    public function hentEavVerdiObjekter(string $kilde): ?stdClass
    {
        if(!$this->eavValueObjects) {
            $this->eavValueObjects = new stdClass();

            /** @var Samling $eavVerdiObjekter */
            $eavVerdiObjekter = $this->app->hentSamling(EavVerdi::class)

                ->leggTilInnerJoin(EavEgenskap::hentTabell(), EavVerdi::hentTabell() . '.egenskap_id = ' . EavEgenskap::hentTabell() . '.id')
                ->leggTilFilter([
                    EavEgenskap::hentTabell() . '.modell' => self::class,
                    EavVerdi::hentTabell() . '.objekt_id' => $this->getId()
                ]);
            /** @var EavValuesInterface $eavVerdiObjekt */
            foreach ($eavVerdiObjekter as $eavVerdiObjekt) {
                $verdiKilde = $eavVerdiObjekt->getConfig()::getDbTable();
                $kode = $eavVerdiObjekt->getConfig()->getSourceField();
                settype($this->eavValueObjects->{$verdiKilde}, 'object');
                $this->eavValueObjects->{$verdiKilde}->{$kode} = $eavVerdiObjekt;
            }
        }

        if(isset($this->eavValueObjects->{$kilde})) {
            return $this->eavValueObjects->{$kilde};
        }
        return null;
    }

    /**
     * Hent samling av Personer
     *
     * @return Personsett
     * @throws Exception
     */
    public function hentFramleiepersoner(): Personsett
    {
        /** @var Personsett $framleietakere */
        $framleietakere = $this->app->hentSamling(Person::class)
            ->leggTilInnerJoin('framleiepersoner', Person::getDbTable(). '.' .Person::hentPrimærnøkkelfelt() . '=`framleiepersoner`.`personid`')
            ->leggTilFilter(['`framleiepersoner`.`framleieforhold`' => $this->hentId()])
            ->låsFiltre();
        if (!isset($this->samlinger->framleietakere)) {
            $this->samlinger->framleietakere = $framleietakere->hentElementer();
        }
        $framleietakere->lastFra($this->samlinger->framleietakere);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $framleietakere;
    }

    /**
     * @param Person $framleier
     * @return $this
     * @throws Exception
     */
    public function leggTilFramleietaker(Person $framleier): Framleie
    {
        /** @var Person|null $eksisterende */
        $eksisterende = $this->hentFramleiepersoner()->leggTilFilter(['`framleiepersoner`.`personid`' => $framleier->hentId()])->hentFørste();
        if (!$eksisterende) {
            $this->app->nyModell(Framleietaker::class, (object)[
                'framleieforhold' => $this,
                'person' => $framleier
            ]);
        }
        return $this->nullstill();
    }

    /**
     * @return void
     * @throws Exception
     */
    public function slett()
    {
        /** @var Framleietaker $framleietaker */
        foreach (
            $this->app->hentSamling(Framleietaker::class)
                ->leggTilFilter([Framleietaker::hentTabell() . '.`framleieforhold`' => $this->hentId()])
            as $framleietaker
        ) {
            $framleietaker->slett();
        }
        parent::slett();
    }
}