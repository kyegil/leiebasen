<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold\Purring;


use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Purring;
use Kyegil\Leiebasen\Sett;

/**
 * Kravlinksett
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @method Kravlink current()
 * @method Kravlink|null hentFørste()
 * @method Kravlink|null hentSiste()
 * @method Kravlink|null hentTilfeldig()
 */
class Kravlinksett extends Sett
{
    /** @var string */
    protected string $model = Kravlink::class;

    /** @var Kravlink[] */
    protected ?array $items = null;

    /**
     * @return $this
     */
    public function leggTilLeftJoinForPurring(): Kravlinksett
    {
        $this
            ->leggTilLeftJoin(['purring' => Purring::hentTabell()],
                '`' . Kravlink::hentTabell() . '`.`blankett` = `purring`.`' . Purring::hentPrimærnøkkelfelt() . '`')
        ;
        return $this;
    }

    /**
     * @return $this
     */
    public function leggTilLeftJoinForKrav(): Kravlinksett
    {
        $this
            ->leggTilLeftJoin(Krav::hentTabell(),
                '`' . Kravlink::hentTabell() . '`.`krav` = `' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '`')
        ;
        return $this;
    }

    /**
     * @return $this
     */
    public function leggTilPurringModell(): Kravlinksett
    {
        $this
            ->leggTilLeftJoinForPurring()
            ->leggTilModell(Purring::class, 'purring', 'purring')
        ;
        return $this;
    }

    /**
     * @return $this
     */
    public function leggTilKravModell(): Kravlinksett
    {
        $this
            ->leggTilLeftJoinForKrav()
            ->leggTilModell(Krav::class, 'krav', 'krav')
        ;
        return $this;
    }
}