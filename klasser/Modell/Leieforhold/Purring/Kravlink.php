<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold\Purring;


use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Purring;

/**
 * Class Purring
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @property integer $nr
 * @property Purring $purring
 * @property Krav $krav
 *
 * @method Purring hentPurring()
 * @method Krav hentKrav()
 *
 * @method $this settPurring(Purring $purring)
 * @method $this settKrav(Krav $krav)
 */
class Kravlink extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'purringer';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'nr';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Kravlinksett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * @param string $referanse
     * @param string|null $modell
     * @param string|null $fremmedNøkkel
     * @param array $filtre
     * @param callable|null $callback
     * @return array|null
     * @throws Exception
     */
    public static function harFlere(string $referanse, ?string $modell = null, ?string $fremmedNøkkel = null, array $filtre = [], ?callable $callback = null): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
        }
        return parent::harFlere($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array {
        return [
            'nr' => [
                'type' => 'integer'
            ],
            'blankett' => [
                'type' => Purring::class,
                'target' => 'purring',
            ],
            'krav'    => [
                'type'  => Krav::class,
            ],
        ];
    }
}