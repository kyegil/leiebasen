<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 13/08/2020
 * Time: 16:11
 */

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;

/**
 * Class Fbo
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @property int $id
 * @property Leieforhold $leieforhold
 * @property string $type alle|husleie|fellesstrøm
 * @property string $mobilnr
 * @property boolean $varsel
 * @property DateTime $registrert
 *
 * @method int hentId()
 * @method Leieforhold hentLeieforhold()
 * @method string hentType() alle|husleie|fellesstrøm
 * @method string hentMobilnr()
 * @method string hentVarsel()
 * @method DateTime hentRegistrert()
 *
 * @method $this settLeieforhold(Leieforhold $leieforhold)
 * @method $this settType(string $type)
 * @method $this settMobilnr(string $mobilnr)
 * @method $this settVarsel(boolean $varsel)
 * @method $this settRegistrert(DateTime $registrert)
 */
class Fbo extends \Kyegil\Leiebasen\Modell
{
    const AVTALETYPE_ALLE = 0;
    const AVTALETYPE_HUSLEIE = 1;
    const AVTALETYPE_FELLESSTRØM = 2;
    const AVTALETYPER = [
        self::AVTALETYPE_ALLE => 'alle',
        self::AVTALETYPE_HUSLEIE => 'husleie',
        self::AVTALETYPE_FELLESSTRØM => 'fellesstrøm'
    ];
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'fbo';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'leieforhold'    => [
                'type'  => Leieforhold::class
            ],
            'type'    => [
                'type'  => function($type) {
                    return self::AVTALETYPER[$type];
                },
                'toDbValue' => function($type) {
                    $avtaletyper = array_flip(self::AVTALETYPER);
                    if(isset($avtaletyper[strtolower(trim($type))])) {
                        return $avtaletyper[strtolower(trim($type))];
                    }
                    return self::AVTALETYPE_ALLE;
                }
            ],
            'mobilnr'    => [
                'type'  => 'string'
            ],
            'varsel'    => [
                'type'  => 'boolean'
            ],
//            'slettet'    => [
//                'type'  => 'boolean'
//            ],
            'registrert'    => [
                'type'  => 'datetime'
            ]
        ];
    }

    /**
     * Oppdragsfrist
     *
     * Returnerer siste mulige tidspunkt for overføring til NETS
     *
     * @param DateTimeInterface|null $forfallsdato
     * @return DateTimeImmutable|null
     * @throws Exception
     */
    public function oppdragsfrist(?DateTimeInterface $forfallsdato = null ): ?DateTimeImmutable {
        if (!$forfallsdato) {
            return null;
        }

        if ($forfallsdato instanceof DateTime) {
            $resultat = DateTimeImmutable::createFromMutable($forfallsdato);
        }
        else {
            /** @var DateTimeImmutable $resultat */
            $resultat = clone $forfallsdato;
        }
        $resultat = $resultat->setTime(13, 59);

        $enDag = new DateInterval('P1D');
        $bankvarsel = $this->hentBankvarsel();

        /*
         * Dersom betalingsmottaker ønsker at varsel skal formidles via bank,
         * må betalingskravet være mottatt innen kl 14:00 siste virkedag i måneden før varsel produseres.
         * Alle betalingskrav som har forfallsdato fra og med 15. i førstkommende måned til og med 14. i påfølgende måned,
         * må sendes innen denne tidsfristen
         */
        if( $bankvarsel ) {
            if( $resultat->format('d') > 14 ) {
                $resultat = $resultat->sub(new DateInterval('P1M'));
            }
            else {
                $resultat = $resultat->sub(new DateInterval('P2M'));
            }
            $resultat = new DateTimeImmutable( $resultat->format('Y-m-t 13:59') );
        }

        /*
         * Betalingskrav varslet direkte fra betalingsmottaker,
         * må være mottatt i Nets på en virkedag innen kl 14:00, 4 kalenderdager før forfallsdato.
         * Varsel til betaler skal sendes minst 3 virkedager før belastning finner sted,
         * og 4 uker før forfall dersom betalingsmottaker ønsker å avskjære tilbakeføringsrettet fra betaler
         */
        else {
            $resultat = $resultat->sub(new DateInterval('P4D'));
        }

        /*
         * Ved helg eller helligdager må fristen fremskyndes
         */
        while(
            $resultat->format('N') > 5
            || in_array( $resultat->format('m-d'), $this->leiebase->bankfridager($resultat->format('Y')))
        ) {
            $resultat = $resultat->sub($enDag);
        }

        return $resultat;
    }

    /**
     * Ønsker utleier at banken skal varsle om AvtaleGiro-trekket?
     *
     * Bankvarsel må sendes dersom betaler ønsker varsel,
     * men utleier ikke har noen varslingsmulighet
     * gjennom sms, eFaktura eller e-post
     *
     * @return bool
     * @throws Exception
     */
    public function hentBankvarsel(): bool
    {
        $this->app->before($this, __FUNCTION__, []);
        $leieforhold = $this->hentLeieforhold();
        $bankvarsel = $this->hentVarsel()
            && !($this->app->hentValg('avtalegiro_sms') && $this->hentMobilnr())
            && !$leieforhold->hentEfakturaIdentifier()
            && !$leieforhold->hentEpost()
            ;
        return $this->app->after($this, __FUNCTION__, $bankvarsel);
    }

    /**
     * Skal utleier selv varsle betaler om AvtaleGiro-trekket?
     *
     * Egenvarsel skal være sann i alle tilfeller hvor banken ikke trenger sende varsel.
     *
     * @return bool
     * @throws Exception
     * @see Fbo::hentBankvarsel()
     */
    public function hentEgenvarsel()
    {
        return !$this->hentBankvarsel();
    }


}