<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 03/08/2020
 * Time: 11:26
 */

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Leieforhold;
use stdClass;

/**
 * Class Notat
 * @package Kyegil\Leiebasen\Leieobjekter
 *
 * @property int $notatnr
 * @property Leieforhold $leieforhold
 * @property DateTime $dato
 * @property string $notat
 * @property string $henvendelseFra
 * @property string $kategori
 * @property string $brevtekst
 * @property string $vedlegg
 * @property string $vedleggsnavn
 * @property string $dokumentreferanse
 * @property string $dokumenttype
 * @property DateTime $registrert
 * @property string $registrerer
 * @property bool $skjulForLeietaker
 *
 * @method int hentNotatnr()
 * @method Leieforhold hentLeieforhold()
 * @method DateTime hentDato()
 * @method string hentNotat()
 * @method string hentHenvendelseFra()
 * @method string hentKategori()
 * @method string hentBrevtekst()
 * @method string hentVedlegg()
 * @method string hentVedleggsnavn()
 * @method string hentDokumentreferanse()
 * @method string hentDokumenttype()
 * @method string hentRegistrert()
 * @method DateTime hentRegistrerer()
 * @method bool hentSkjulForLeietaker()
 *
 * @method $this settLeieforhold(Leieforhold $leieforhold)
 * @method $this settDato(DateTime $dato)
 * @method $this settNotat(string $notat)
 * @method $this settHenvendelseFra(string $henvendelseFra)
 * @method $this settKategori(string $kategori)
 * @method $this settBrevtekst(string $brevtekst)
 * @method $this settVedlegg(string $vedlegg)
 * @method $this settVedleggsnavn(string $vedleggsnavn)
 * @method $this settDokumentreferanse(string $dokumentreferanse)
 * @method $this settDokumenttype(string $dokumenttype)
 * @method $this settRegistrert(DateTime $registrert)
 * @method $this settRegistrerer(string $registrerer)
 * @method $this settSkjulForLeietaker(bool $skult)
 */
class Notat extends Modell
{
    const FRA_UTLEIER = 'fra utleier';

    const FRA_LEIETAKER = 'fra leietaker';

    const FRA_FRAMLEIER = 'fra framleier';

    const FRA_ANDRE = 'fra andre';

    const KATEGORI_BETALINGSPLAN = 'betalingsplan';

    const KATEGORI_BETALINGSUTSETTELSE_FORESPØRSEL = 'betalingsutsettelseforespørsel';

    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'notater';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'notatnr';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Notatsett::class;

    /**
     * @var array
     */
    protected static $kategorier = [
        'notat' => 'Notat',
        'spørsmål' => 'Spørsmål',
        'brev' => 'Brev',
        'purring' => 'Purring / Betalingspåminnelse',
        'inkassosak' => 'Inkassosak',
        self::KATEGORI_BETALINGSUTSETTELSE_FORESPØRSEL => 'Forespørsel om betalingsutsettelse',
        self::KATEGORI_BETALINGSPLAN => 'Betalingsplan',
        'avtale' => 'Avtale',
        'utløpsvarsel' => 'Varsel om at leieavtalen utløper / må fornyes',
        '§4.18-varsel' => 'Varsel etter tvangsfullbyrdelseslovens §4.18 sendt',
        'forliksvarsel' => 'Sendt varsel (ihht tvisteloven  §5-2) om at saken kan bli klaget inn for forliksrådet',
        'forliksklage' => 'Forliksklage sendt til forliksrådet',
        'inkassovarsel' => 'Sendt varsel om at saken kan bli oversendt til inkasso',
        'tvangsfravikelsesbegjæring' => 'Begjæring om fravikelse sendt namsmannen (ihht tvangsfullbyrdelsesloven § 13-2)',
        'utleggsbegjæring' => 'Begjæring om utlegg sendt namsmannen'
    ];

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'notatnr'    => [
                'type'  => 'integer'
            ],
            'leieforhold'    => [
                'type'  => Leieforhold::class
            ],
            'dato'    => [
                'type'  => 'date'
            ],
            'notat'    => [
                'type'  => 'string'
            ],
            'henvendelse_fra'    => [
                'type'  => 'string'
            ],
            'kategori'    => [
                'type'  => 'string'
            ],
            'brevtekst'    => [
                'type'  => 'string'
            ],
            'vedlegg'    => [
                'type'  => 'string'
            ],
            'vedleggsnavn'    => [
                'type'  => 'string'
            ],
            'dokumentreferanse'    => [
                'type'  => 'string'
            ],
            'dokumenttype'    => [
                'type'  => 'string'
            ],
            'registrert'    => [
                'type'  => 'datetime'
            ],
            'registrerer'    => [
                'type'  => 'string'
            ],
            'skjul_for_leietaker'    => [
                'type'  => 'boolean'
            ],
        ];
    }

    /**
     * @return stdClass
     */
    public static function hentKategorier(): stdClass
    {
        return (object)static::$kategorier;
    }

    /**
     * @param stdClass $kategorier
     */
    public function settKategorier(stdClass $kategorier): void
    {
        static::$kategorier = $kategorier;
    }

    /**
     * @param stdClass $parametere
     * @return Modell
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Modell
    {
        $parametere->registrert = new DateTime();
        $parametere->registrerer = $this->app->bruker['navn'];
        $parametere->notat = $parametere->notat ?? '';
        return parent::opprett($parametere);
    }
}