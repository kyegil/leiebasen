<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;

use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløpsett;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan\Avdrag;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan\Avdragsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan\Originalkrav;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan\Originalkravsett;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Adressefelt;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Leietakerfelt;
use Kyegil\Leiebasen\Visning\felles\html\Utleierfelt;
use Kyegil\ViewRenderer\ViewArray;
use stdClass;

/**
 * Class Betalingsplan
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @property int $id
 * @property Leieforhold $leieforhold
 * @property bool $aktiv
 * @property DateTimeInterface $dato
 * @property string $startgjeld
 * @property string $avtale
 * @property string $avdragsbeløp
 * @property bool $følgerLeiebetalinger
 * @property DateInterval $terminlengde
 * @property DateTimeInterface $startDato
 * @property bool|null $utenomAvtalegiro
 * @property DateInterval|null $forfallspåminnelseAntDager
 *
 * @method int hentId()
 * @method Leieforhold hentLeieforhold()
 * @method bool hentAktiv()
 * @method DateTimeInterface hentDato()
 * @method string hentStartgjeld()
 * @method string hentAvtale()
 * @method string hentAvdragsbeløp()
 * @method bool hentFølgerLeiebetalinger()
 * @method DateInterval hentTerminlengde()
 * @method DateTimeInterface hentStartDato()
 * @method bool|null hentUtenomAvtalegiro()
 * @method DateInterval|null hentForfallspåminnelseAntDager()
 *
 * @method $this settLeieforhold(Leieforhold $leieforhold)
 * @method $this settAktiv(bool $aktiv)
 * @method $this settDato(DateTimeInterface $dato)
 * @method $this settStartgjeld(string $startgjeld)
 * @method $this settAvtale(string $betalingsavtale)
 * @method $this settAvdragsbeløp(string $avdragsbeløp)
 * @method $this settFølgerLeiebetalinger(bool $følgerLeiebetalinger)
 * @method $this settTerminlengde(DateInterval $terminlengde)
 * @method $this settStartDato(DateTimeInterface $startDato)
 * @method $this settUtenomAvtalegiro(bool|null $utenomAvtalegiro)
 * @method $this settForfallspåminnelseAntDager(DateInterval|null $forfallspåminnelseTidsrom)
 * @method $this settNotat(Notat|null $notat)
 */
class Betalingsplan extends \Kyegil\Leiebasen\Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'betalingsplaner';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Betalingsplansett::class;

    /** @var array one-to-many
     * @noinspection PhpDocFieldTypeMismatchInspection
     */
    protected static ?array $one2Many;

    /**
     * @param string $referanse
     * @param string|null $modell
     * @param string|null $fremmedNøkkel
     * @param array $filtre
     * @param callable|null $callback
     * @return array|null
     * @throws Exception
     */
    public static function harFlere(
        string $referanse,
        ?string $modell = null,
        ?string $fremmedNøkkel = null,
        array $filtre = [],
        ?callable $callback = null
    ): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::harFlere('originalkrav',
                Originalkrav::class,
                'betalingsplan_id',[],
                function(Originalkravsett $originalkravsett){
                    $originalkravsett->leggTilKravModell();
                }
            );
            self::harFlere('avdrag',
                Avdrag::class,
                'betalingsplan_id'
            );
        }
        return parent::harFlere($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }

    /**
     * @inheritDoc
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'leieforhold_id' => [
                'target' => 'leieforhold',
                'type' => Leieforhold::class,
                'rawDataContainer' => Leieforhold::hentTabell()
            ],
            'aktiv' => [
                'type' => 'boolean',
            ],
            'dato' => [
                'type' => 'date',
                'allowNull' => true,
            ],
            'startgjeld'    => [
                'type' => 'string',
            ],
            'betalingsavtale'    => [
                'target' => 'avtale',
            ],
            'avdragsbeløp'    => [
                'type' => 'string',
            ],
            'følger_leiebetalinger' => [
                'type' => 'boolean',
            ],
            'terminlengde'    => [
                'type' => 'interval',
            ],
            'start_dato'    => [
                'type' => 'date',
                'allowNull' => true,
            ],
            'forfallspåminnelse_ant_dager'    => [
                'type' => 'int',
                'allowNull' => true,
            ]
        ];
    }

    /**
     * @param string $mal
     * @param \Kyegil\Leiebasen\Leiebase $app
     * @param Leieforhold|null $leieforhold
     * @return string
     * @throws Exception
     */
    public static function gjengiAvtalemal(string $mal, Leiebase $app, ?Leieforhold $leieforhold = null): string
    {
        $tekst = $mal;
        $variabler = [
            'utleier' => $app->hentValg('utleier') ?? '',
            'utleierfelt' => $app->vis(Utleierfelt::class),
            'kontonummer' => $app->hentValg('bankkonto') ?? '',
            'dato' => date_create()->format('d.m.Y'),
        ];
        if ($leieforhold) {
            $variabler['leietaker'] = $leieforhold->hentNavn();
            $variabler['kid'] = $leieforhold->hentKid();
            $variabler['leiebeløp'] = $app->kr($leieforhold->hentLeiebeløp());
            $variabler['leietakerfelt'] = $app->vis(Leietakerfelt::class, [
                'leieforhold' => $leieforhold
            ]) . $app->vis(Adressefelt::class, [
                    'leieforhold' => $leieforhold
                ]);
        }
        foreach($variabler as $variabel => $verdi) {
            $tekst = str_replace("{{$variabel}}", $verdi, $tekst);
        }
        return $tekst;
    }

    public function opprett(stdClass $parametere): Modell
    {
        $parametere->startgjeld = $parametere->startgjeld ?? 0;
        $parametere->avtale = $parametere->avtale ?? '';
        $parametere->id = intval(strval($parametere->leieforhold));
        return parent::opprett($parametere);
    }

    /**
     * Hent original-kravsett, inklusive utestående beløp da betalingaplanen ble inngått
     *
     * @return Originalkravsett
     * @throws Exception
     */
    public function hentOriginalkravsett(): Originalkravsett
    {
        /** @var Originalkravsett $originalkravsett */
        $originalkravsett = $this->hentSamling('originalkrav');
        return $originalkravsett;
    }

    /**
     * Hent kravene som skal betales ned med betalingsplanen
     *
     * @return Kravsett
     * @throws Exception
     */
    public function hentKravsett(): Kravsett
    {
        /** @var Kravsett $kravsett */
        $kravsett = $this->app->hentSamling(Krav::class)
            ->leggTilInnerJoin(Originalkrav::hentTabell(), '`' . Originalkrav::hentTabell() . '`.`krav_id` = `' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '`')
            ->leggTilFilter(['`' . Originalkrav::hentTabell() . '`.`betalingsplan_id`' => $this->hentId()])
            ->låsFiltre()
            ->leggTilSortering('kravdato', false, Krav::hentTabell())
        ;
        if(!isset($this->samlinger->krav)) {
            $this->samlinger->krav = [];
            $this->hentOriginalkravsett()->forEach(function (Originalkrav $originalkrav) {
                $this->samlinger->krav[] = $originalkrav->krav;
            });
        }
        $kravsett->lastFra($this->samlinger->krav);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $kravsett;
    }

    /**
     * @return Avdragsett
     * @throws Exception
     */
    public function hentAvdragsett(): Avdragsett
    {
        /** @var Avdragsett $avdragsett */
        $avdragsett = $this->hentSamling('avdrag');
        return $avdragsett;
    }

    /**
     * @return Delbeløpsett
     */
    public function hentInnbetalinger(): Delbeløpsett
    {
        /** @var Delbeløpsett $delbeløpsett */
        $delbeløpsett = $this->app->hentSamling(Delbeløp::class)
            ->leggTilInnerJoin(Originalkrav::hentTabell(),
                '`' . Delbeløp::hentTabell() . '`.`krav` = `' . Originalkrav::hentTabell() . '`.`krav_id`'
            )
            ->leggTilFilter(['`' . Originalkrav::hentTabell() . '`.`betalingsplan_id`' => $this->hentId()])
            ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`dato` >=' => $this->hentDato()->format('Y-m-d')])
            ->låsFiltre()
        ;
        if(!isset($this->samlinger->innbetalinger)) {
            $this->samlinger->innbetalinger = $delbeløpsett->hentElementer();
        }
        else {
            $delbeløpsett->lastFra($this->samlinger->innbetalinger);
        }
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $delbeløpsett;
    }

    /**
     * @param Kravsett|null $kravsett
     * @return Betalingsplan
     * @throws Exception
     */
    public function settKravsett(?Kravsett $kravsett = null): Betalingsplan
    {
        $leieforholdId = $this->hentLeieforhold()->id;
        $gjeld = 0;
        $this->app->hentSamling(Originalkrav::class)
            ->leggTilFilter(['`' . Originalkrav::hentTabell() . '`.`betalingsplan_id`' => $this->hentId()])
            ->slettAlle();
        if ($kravsett) {
            foreach ($kravsett as $krav) {
                if ($krav->leieforhold->id != $leieforholdId) {
                    throw new Exception('Krav ' . $krav->id . ' tilhører ikke leieforhold ' . $leieforholdId);
                }
                $this->app->nyModell(Originalkrav::class, (object)[
                    'betalingsplan' => $this,
                    'krav' => $krav,
                    'utestående_grunnlag' => $krav->utestående
                ]);
                $gjeld += $krav->utestående;
            }
        }
        $this->sett('startgjeld', $gjeld);
        return $this->nullstill();
    }

    /**
     * @return Modell|Betalingsplan
     * @throws Exception
     */
    public function slettAlleAvdrag()
    {
        foreach($this->hentAvdragsett() as $avdrag) {
            $avdrag->slett();
        }
        return $this->nullstill();
    }

    /**
     * Legg til et avdrag til modellen
     *
     * @param DateTimeInterface $dato
     * @param float $beløp
     * @return Avdrag
     * @throws Exception
     */
    public function leggTilAvdrag(DateTimeInterface $dato, float $beløp): Avdrag
    {
        /** @var Avdrag $avdrag */
        $avdrag = $this->app->nyModell(Avdrag::class, (object)[
            'betalingsplan' => $this,
            'forfallsdato' => $dato,
            'beløp' => $beløp,
            'utestående' => $beløp
        ]);
        $this->samlinger->avdrag = null;
        return $avdrag;
    }

    /**
     * @return DateTimeInterface|null
     * @throws Exception
     */
    public function hentSluttDato(): ?DateTimeInterface
    {
        if(!property_exists($this->data, 'slutt_dato')) {
            if (isset($this->rawData->betalingsplan_ekstra) && property_exists($this->rawData->betalingsplan_ekstra, 'slutt_dato')) {
                $this->data->slutt_dato = $this->rawData->betalingsplan_ekstra->slutt_dato;
                if($this->data->slutt_dato) {
                    $this->data->slutt_dato = date_create_immutable_from_format('Y-m-d', $this->data->slutt_dato);
                    $this->data->slutt_dato->settime(0, 0);
                }
            }
            else {
                $sisteAvdrag = $this->hentAvdragsett()->hentSiste();
                $this->data->slutt_dato = $sisteAvdrag ? $sisteAvdrag->hentForfallsdato() : null;
            }
        }
        return $this->data->slutt_dato;
    }

    /**
     * @param string $avtaletekst
     * @return array|string|string[]
     * @throws Exception
     */
    public function visUtfyltAvtaletekst(string $avtaletekst)
    {
        $kravoversikt = $this->hentHtmlKravoversikt();
        $avdragsoversikt = $this->hentHtmlAvdragsoversikt();
        $startdato = $this->hentStartDato() ? $this->hentStartDato()->format('d.m.Y') : '';
        $startgjeld = $this->hentStartgjeld();
        $sluttdato = $this->hentSluttDato() ? $this->hentSluttDato()->format('d.m.Y') : '';

        $avtaletekst = str_replace('{kravoversikt}', $kravoversikt, $avtaletekst);
        $avtaletekst = str_replace('{avdragsoversikt}', $avdragsoversikt, $avtaletekst);
        $avtaletekst = str_replace('{startdato}', $startdato, $avtaletekst);
        $avtaletekst = str_replace('{sluttdato}', $sluttdato, $avtaletekst);
        $avtaletekst = str_replace('{utestående}', $this->app->kr($startgjeld), $avtaletekst);
        return $avtaletekst;
    }

    /**
     * @return HtmlElement
     * @throws Exception
     */
    public function hentHtmlKravoversikt(): HtmlElement
    {
        $linjer = new ViewArray();
        foreach ($this->hentKravsett() as $krav) {
            $linjer->addItem(new HtmlElement('tr', [], [
                new HtmlElement('td', [], $krav->tekst),
                new HtmlElement('td', ['class' => 'beløp'], $this->app->kr($krav->utestående)),
            ]));
        }
        return new HtmlElement('table', ['id' => 'betalingsavtale-kravoversikt'], new HtmlElement('tbody', [], $linjer));
    }

    /**
     * @return HtmlElement
     * @throws Exception
     */
    public function hentHtmlAvdragsoversikt(): HtmlElement
    {
        $linjer = new ViewArray();
        foreach ($this->hentAvdragsett() as $avdrag) {
            $linjer->addItem(new HtmlElement('tr', [], [
                new HtmlElement('td', [], $avdrag->forfallsdato->format('d.m.Y')),
                new HtmlElement('td', ['class' => 'beløp'], $this->app->kr($avdrag->beløp)),
            ]));
        }
        $thead = new HtmlElement('thead', [], new HtmlElement('tr', [], [
            new HtmlElement('th', [], 'Dato'),
            new HtmlElement('th', ['class' => 'beløp'], 'Avdrag')
        ]));
        $tbody = new HtmlElement('tbody', [], $linjer);
        return new HtmlElement('table', ['id' => 'betalingsavtale-avdragsoversikt'], [$thead, $tbody]);
    }

    /**
     * Hent avdraget som er satt for en bestemt dato
     *
     * @param DateTimeInterface|null $forfallsdato
     * @return Avdrag|null
     * @throws Exception
     */
    public function hentAvdragForDato(?DateTimeInterface $forfallsdato): ?Avdrag
    {
        if (!$forfallsdato) {
            return null;
        }
        if (!isset($this->data->avdragPerDato)) {
            $this->data->avdragPerDato = new stdClass();
            foreach ($this->hentAvdragsett() as $avdrag) {
                $this->data->avdragPerDato->{$avdrag->hentForfallsdato()->format('Y-m-d')} = $avdrag;
            }
        }
        return $this->data->avdragPerDato->{$forfallsdato->format('Y-m-d')} ?? null;
    }

    /**
     * @return float
     * @throws Exception
     */
    public function hentUtestående()
    {
        if (!isset($this->data->utestående)) {
            if (isset($this->rawData->betalingsplan_ekstra)
                && property_exists($this->rawData->betalingsplan_ekstra, 'utestående')) {
                $this->data->utestående = $this->rawData->betalingsplan_ekstra->utestående;
            }
            else {
                $this->data->utestående = 0;
                foreach($this->hentKravsett() as $krav) {
                    $this->data->utestående += $krav->hentUtestående();
                }
            }
        }
        return $this->data->utestående;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function oppdaterUteståendeAvdrag(): Betalingsplan
    {
        $startgjeld = $this->hentStartgjeld();
        $utestående = $this->hentUtestående();
        $innbetalt = $startgjeld - $utestående;
        foreach ($this->hentAvdragsett() as $avdrag) {
            $avdragInnbetalt = min($innbetalt, $avdrag->beløp);
            $avdrag->utestående = $avdrag->beløp - $avdragInnbetalt;
            $innbetalt -= $avdragInnbetalt;
        }
        return $this;
    }

    /**
     * @return Notat|null
     * @throws Exception
     */
    public function hentNotat(): ?Notat
    {
        if(!property_exists($this->data, 'notat')) {
            if(!$this->hent('notat')) {
                $notater = $this->hentLeieforhold()->hentBetalingsNotater();
                $notater
                    ->leggTilFilter(['kategori' => 'betalingsplan'])
                    ->leggTilSortering('dato', true)
                ;
                $this->data->notat = $notater->hentFørste();
            }
        }
        return $this->data->notat;
    }

    /**
     * @param DateTimeInterface|null $dato
     * @return float
     * @throws Exception
     */
    public function hentUteståendePer(DateTimeInterface $dato = null): float
    {
        $dato = $dato ?? new DateTimeImmutable();
        if (!isset($this->data->utestående_per)) {
            $this->data->utestående_per = [];
            $utestående = 0;
            foreach($this->hentAvdragsett() as $avdrag) {
                $utestående += $avdrag->utestående;
                $this->data->utestående_per[$avdrag->forfallsdato->format('Y-m-d')] = $utestående;
            }
            ksort($this->data->utestående_per);
        }
        if (!isset($this->data->utestående_per[$dato->format('Y-m-d')])) {
            $utestående = 0;
            foreach ($this->data->utestående_per as $forfallsato => $uteståendePerDato) {
                if ($forfallsato > $dato->format('Y-m-d')) {
                    break;
                }
                $utestående = $uteståendePerDato;
            }
            $this->data->utestående_per[$dato->format('Y-m-d')] = $utestående;
            ksort($this->data->utestående_per);
        }
        return $this->data->utestående_per[$dato->format('Y-m-d')];
    }

    /**
     * @param DateTimeInterface|null $dato
     * @return Kravsett
     * @throws Exception
     */
    public function hentKravForfaltEtterPlanen(DateTimeInterface $dato = null): Kravsett
    {
        $dato = $dato ?? new DateTimeImmutable();

        if(!isset($this->data->krav_forfalt_etter_planen)) {
            $this->data->krav_forfalt_etter_planen = [];
        }
        if(!isset($this->data->krav_forfalt_etter_planen[$dato->format('Y-m-d')])) {
            $utestående = $this->hentUteståendePer($dato);
            $uteståendeKravIder = [];
            foreach ($this->hentKravsett() as $krav) {
                if($utestående <=0) {
                    break;
                }
                if ($krav->utestående) {
                    $uteståendeKravIder[] = $krav->hentId();
                    $utestående -= min($utestående, $krav->utestående);
                }
            }
            /** @var Kravsett $kravsett */
            $kravsett = $this->app->hentSamling(Krav::class);
            $kravsett->filtrerEtterIdNumre($uteståendeKravIder);
            $this->data->krav_forfalt_etter_planen[$dato->format('Y-m-d')] = $kravsett;
        }
        return $this->data->krav_forfalt_etter_planen[$dato->format('Y-m-d')];

    }

    /**
     * @param DateTimeInterface|null $dato
     * @return Kravsett
     * @throws Exception
     */
    public function hentKravForNesteForfall(DateTimeInterface $dato = null): Kravsett
    {
        $dato = $dato ?? new DateTimeImmutable();
        $nesteAvdragForfall = $this->hentNesteForfall();
        return $this->hentKravForfaltEtterPlanen($nesteAvdragForfall);
    }

    /**
     * @return Avdrag|null
     * @throws Exception
     */
    public function hentNesteAvdrag(): ?Avdrag
    {
        $avdrag = $this->hentAvdragsett();
        $avdrag->leggTilFilter(['utestående >' => 0]);
        return $avdrag->hentFørste();
    }

    /**
     * @return DateTimeInterface|null
     * @throws Exception
     */
    public function hentNesteForfall(): ?DateTimeInterface
    {
        $avdrag = $this->hentNesteAvdrag();
        return $avdrag ? $avdrag->forfallsdato : null;
    }

    /**
     * @return mixed|null
     * @throws Exception
     */
    public function deaktiverVedBrudd()
    {
        $this->app->pre($this, __FUNCTION__, []);
        if ($this->hentAktiv()) {
            $leieforhold = $this->hentLeieforhold();
            $innhold = $this->app->hentValg('betalingsplan_bruddvarsel_mal') ?? '';
            $innhold = str_replace([
                '{leieforholdnr}',
                '{leieforhold}',
            ],
                [
                    $leieforhold->hentId(),
                    $leieforhold->hentBeskrivelse(),
                ],
                $innhold);
            $tittel = 'Varsel om brutt betalingsplan';
            /** @var Visning\felles\epost\shared\Epost $html */
            $html = $this->app->hentVisning(Visning\felles\epost\shared\Epost::class, [
                'tittel' => $tittel,
                'innhold' => $innhold
            ]);
            /** @var Visning\felles\epost\shared\Epost $tekst */
            $tekst = $this->app->hentVisning(Visning\felles\epost\shared\Epost::class, [
                'tittel' => $tittel,
                'innhold' => Visning\felles\epost\shared\Epost::br2crnl($innhold)
            ])->settMal('felles/epost/shared/Epost.txt');

            $this->app->sendMail((object)[
                'to' => $this->app->hentValg('utleier') . '<' . $this->app->hentValg('epost') . '>',
                'subject' => $tittel,
                'html' => $html,
                'text' => $tekst,
                'type' => 'betalingsplan_brutt_avtale'
            ]);
            $this->settAktiv(false);
        }
        return $this->app->post($this, __FUNCTION__, $this);
    }
}