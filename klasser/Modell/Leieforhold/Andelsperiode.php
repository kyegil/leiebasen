<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use DateTime;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;

/**
 * Class Andelsperiode
 *
 * @property int $id
 * @property Leieobjekt $leieobjekt
 * @property Leieforhold $leieforhold
 * @property Fraction $andel
 * @property DateTime $fradato
 * @property DateTime|null $tildato
 *
 * @method int hentId()
 * @method Leieobjekt hentLeieobjekt()
 * @method Leieforhold hentLeieforhold()
 * @method Fraction hentAndel()
 * @method DateTime hentFradato()
 * @method DateTime|null hentTildato()
 *
 * @method $this settLeieobjekt(Leieobjekt $leieobjekt)
 * @method $this settLeieforhold(Leieforhold $leieforhold)
 * @method $this settFradato(DateTime $dato)
 * @method $this settTildato(DateTime|null $tildato)
 * @package Kyegil\Leiebasen\Modell\Leieforhold
*/
class Andelsperiode extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'andelsperioder';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Andelsperiodesett::class;

    protected static ?array $one2Many;

    public static function harFlere(string $referanse, ?string $modell = null, ?string $fremmedNøkkel = null, array $filtre = [], ?callable $callback = null): ?array
    {
        return parent::harFlere($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     * * type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     * * allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     * * allowMultiple: boolsk. Standardverdi er false. True vil tillate flere verdier av samme type.
     *      Bør brukes med forsiktighet, for dette vil komplisere tabell-relasjoner.
     * * target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     * * toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     * * rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien
     *
     * @return array
     * @noinspection PhpUnusedParameterInspection
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'leieobjekt'    => [
                'type'  => Leieobjekt::class
            ],
            'leieforhold'    => [
                'type'  => Leieforhold::class
            ],
            'andel'    => [
                'type'  => function($andel, Andelsperiode $kontrakt){
                    return new Fraction($andel);
                },
                'toDbValue'  => function($andel){
                    if ($andel instanceof Fraction) {
                        $andel->fractionBar = '/';
                        return $andel->asFraction();
                    }
                    return $andel;
                }
            ],
            'fradato'    => [
                'type'  => 'date'
            ],
            'tildato'    => [
                'type'  => 'date',
                'allowNull' => true
            ],
        ];
    }
}