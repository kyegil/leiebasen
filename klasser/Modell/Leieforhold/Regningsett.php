<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Fbo\Trekk;
use Kyegil\Leiebasen\Sett;

/**
 * Regningsett
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @method Regning current()
 * @method Regning|null hentFørste()
 * @method Regning|null hentSiste()
 * @method Regning|null hentTilfeldig()
 */
class Regningsett extends Sett
{
    /** @var string */
    protected string $model = Regning::class;

    /** @var Regning[] */
    protected ?array $items = null;

    /**
     * @param string $model
     * @param array $di
     * @throws \Kyegil\CoreModel\CoreModelDiException
     */
    public function __construct(string $model, array $di)
    {
        parent::__construct($model, $di);
        $this->leggTilLeieforholdModell();
    }

    /**
     * Legg til leieforhold-modell
     *
     * @return $this
     */
    public function leggTilLeieforholdModell(): Regningsett
    {
        $this
            ->leggTilLeftJoin(Leieforhold::hentTabell(), '`' . Regning::hentTabell() . '`.`leieforhold` = `' . Leieforhold::hentTabell(). '`.`' . Leieforhold::hentPrimærnøkkelfelt(). '`')
            ->leggTilModell(Leieforhold::class)
        ;
        return $this;
    }

    /**
     * Legg til leieforhold-modell
     *
     * @return $this
     */
    public function leggTilLeieforholdFboModell(): Regningsett
    {
        $this
            ->leggTilLeftJoin(Fbo::hentTabell(), '`' . Regning::hentTabell() . '`.`leieforhold` = `' . Fbo::hentTabell(). '`.`leieforhold`')
            ->leggTilModell(Fbo::class, 'leieforhold.fbo')
        ;
        return $this;
    }

    /**
     * Legg til join for å filtrere på krav
     *
     * @param string|null $tabellAlias
     * @return $this
     */
    public function leggTilInnerJoinForKrav(?string $tabellAlias = null):Regningsett {
        $tabellAlias = $tabellAlias ?: Krav::hentTabell();
        $this
            ->leggTilInnerJoin([$tabellAlias => Krav::hentTabell()], '`' . $tabellAlias . '`.`gironr` = `' . Regning::hentTabell() . '`.`' . Regning::hentPrimærnøkkelfelt() . '`')
        ;
        return $this;
    }

    /**
     * Legg til join for trekk i faste betalingsoppdrag
     *
     * @param string|null $tabellAlias
     * @return $this
     */
    public function leggTilLeftJoinForFboTrekk(?string $tabellAlias = null):Regningsett {
        $tabellAlias = $tabellAlias ?: Trekk::hentTabell();
        $this
            ->leggTilLeftJoin([$tabellAlias => Trekk::hentTabell()], '`' . $tabellAlias . '`.`gironr` = `' . Regning::hentTabell() . '`.`' . Regning::hentPrimærnøkkelfelt() . '`')
        ;
        return $this;
    }

    /**
     * Legg til join for å laste leieforholdets utdelingsorden
     *
     * Join for leieforholdet må legges til først
     *
     * @param string|null $alias
     * @param string|null $leieforholdAlias
     * @return $this
     */
    public function leggTilLeftJoinForLeieforholdUtdelingsplassering(?string $alias = null, ?string $leieforholdAlias = null):Regningsett {
        $alias = $alias ?: 'utdelingsorden';
        $leieforholdAlias = $leieforholdAlias ?: Leieforhold::hentTabell();
        $this
            ->leggTilLeftJoin([$alias => 'utdelingsorden'], '`' . $leieforholdAlias . '`.`regningsobjekt` = `' . $alias . '`.`leieobjekt` AND `utdelingsorden`.`rute` = ' . $this->app->hentValg('utdelingsrute'))
        ;
        return $this;
    }

    /**
     * @return $this
     */
    public function leggTilBeløpFelt(): Regningsett
    {
        $tp = $this->mysqli->table_prefix;
        $subQuery =
            '(SELECT SUM(`beløp`) AS `beløp` FROM `' . $tp . 'krav` WHERE `gironr` = `' . Regning::hentTabell() . '`.`' . Regning::hentPrimærnøkkelfelt() . '`)';

        $this->leggTilUttrykk('regning_ekstra', 'beløp', $subQuery);
        return $this;
    }

    /**
     * @return $this
     */
    public function leggTilUteståendeFelt(): Regningsett
    {
        $tp = $this->mysqli->table_prefix;
        $subQuery =
            '(SELECT SUM(`utestående`) AS `utestående` FROM `' . $tp . 'krav` WHERE `gironr` = `' . Regning::hentTabell() . '`.`' . Regning::hentPrimærnøkkelfelt() . '`)';

        $this->leggTilUttrykk('regning_ekstra', 'utestående', $subQuery);
        return $this;
    }

    /**
     * @return $this
     */
    public function leggTilForfallFelt(): Regningsett
    {
        $tp = $this->mysqli->table_prefix;
        $subQuery =
            '(SELECT MIN(`forfall`) AS `forfall` FROM `' . $tp . 'krav` WHERE `gironr` = `' . Regning::hentTabell() . '`.`' . Regning::hentPrimærnøkkelfelt() . '`)';

        $this->leggTilUttrykk('regning_ekstra', 'forfall', $subQuery);
        return $this;
    }

    /**
     * @return $this
     */
    public function leggTilSisteForfallFelt(): Regningsett
    {
        $tp = $this->mysqli->table_prefix;
        $subQuery =
            '(SELECT MAX(`purreforfall`) AS `siste_forfall` FROM `' . $tp . 'purringer` AS `purringer` INNER JOIN `' . $tp . 'krav` AS `siste_forfall_krav` ON `purringer`.`krav` = `siste_forfall_krav`.`id` WHERE `siste_forfall_krav`.`gironr` = `' . Regning::hentTabell() . '`.`' . Regning::hentPrimærnøkkelfelt() . '`)';

        $this->leggTilUttrykk('regning_ekstra', 'siste_forfall', $subQuery);
        return $this;
    }

    /**
     * Legger til felt for utdelingsplassering
     *
     * Krever eksisterende Join med leieforhold
     *
     * @return $this
     */
    public function leggTilLeieforholdUtdelingsplasseringFelt(): Regningsett
    {
        $this->leggTilFelt('utdelingsorden', 'plassering', null, 'utdelingsplassering', 'leieforhold.leieforhold_ekstra');

        return $this;
    }

    /**
     * Inkluder Krav
     *
     * @return Kravsett|null
     * @throws \Exception
     */
    public function inkluderKrav(): ?Kravsett
    {
        /** @var Kravsett $kravsett */
        $kravsett = $this->inkluder('krav');
        return $kravsett;
    }

    /**
     * Inkluder Husleier
     *
     * @return Kravsett|null
     * @throws \Exception
     */
    public function inkluderHusleier(): ?Kravsett
    {
        /** @var Kravsett $kravsett */
        $kravsett = $this->inkluder('husleier');
        return $kravsett;
    }
}