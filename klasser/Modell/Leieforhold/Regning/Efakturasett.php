<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold\Regning;


use Kyegil\CoreModel\CoreModelException;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Sett;

/**
 * Efakturasett
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @method Efaktura current()
 * @method Efaktura|null hentFørste()
 * @method Efaktura|null hentSiste()
 * @method Efaktura|null hentTilfeldig()
 */
class Efakturasett extends Sett
{
    /** @var string */
    protected string $model = Efaktura::class;

    /** @var Efaktura[] */
    protected ?array $items = null;

    /**
     * Legg til regning-modell
     *
     * @return $this
     * @throws CoreModelException
     */
    public function leggTilRegningModell(): Efakturasett
    {
        $this
            ->leggTilLeftJoinForRegning()
            ->leggTilModell(Regning::class)
        ;
        return $this;
    }

    /**
     * Legg til join for regning
     *
     * @param string|null $tabellAlias
     * @return $this
     */
    public function leggTilLeftJoinForRegning(?string $tabellAlias = null):Efakturasett {
        $tabellAlias = $tabellAlias ?: Regning::hentTabell();
        $this
            ->leggTilLeftJoin([$tabellAlias => Regning::hentTabell()], '`' . $tabellAlias . '`.`' . Efaktura::hentPrimærnøkkelfelt() . '` = `' . Efaktura::hentTabell() . '`.`giro`')
        ;
        return $this;
    }
}