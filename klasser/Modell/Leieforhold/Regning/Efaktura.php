<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 08/12/2020
 * Time: 15:27
 */

namespace Kyegil\Leiebasen\Modell\Leieforhold\Regning;


use DateTimeInterface;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;

/**
 * Class Efaktura
 * @package Kyegil\Leiebasen\Modell\Leieforhold\Regning
 *
 * @property int $id
 * @property Regning $regning
 * @property DateTimeInterface|null $forsendelsesdato
 * @property string|null $forsendelse
 * @property int|null $oppdrag
 * @property DateTimeInterface|null $kvittertDato
 * @property string|null $kvitteringsforsendelse
 * @property string $status
 *
 * @method Regning hentRegning()
 * @method DateTimeInterface|null hentForsendelsesdato()
 * @method string|null hentForsendelse()
 * @method int|null hentOppdrag()
 * @method DateTimeInterface|null hentKvittertDato()
 * @method string|null hentKvitteringsforsendelse()
 * @method string hentStatus()
 *
 * @method $this settRegning(Regning $regning)
 * @method $this settForsendelsesdato(DateTimeInterface|null $forsendelsesdato)
 * @method $this settForsendelse(string|null $forsendelse)
 * @method $this settOppdrag(int|null $oppdrag)
 * @method $this settKvittertDato(DateTimeInterface|null $kvittertDato)
 * @method $this settKvitteringsforsendelse(string|null $kvitteringsforsendelse)
 * @method $this settStatus(string $status)
 */
class Efaktura extends \Kyegil\Leiebasen\Modell
{
    const PREPARED = 'prepared';
    const SENT = 'sent';
    const OK = 'ok';
    const CONSIGNMENT_IN_PROGRESS = 'IN_PROGRESS';
    const CONSIGNMENT_PROCESSED = 'PROCESSED';
    const CONSIGNMENT_REJECTED = 'REJECTED';
    const CONSIGNMENT_STOPPED = 'STOPPED';
    const INVOICE_NOT_FOUND = 'NOT_FOUND';
    const INVOICE_CANCELLED = 'CANCELLED';
    const INVOICE_DUPLICATE = 'DUPLICATE';
    const INVOICE_IN_PROGRESS = 'IN_PROGRESS';
    const INVOICE_PROCESSED = 'PROCESSED';
    const INVOICE_REJECTED = 'REJECTED';

    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'efakturaer';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    protected static function hentDbFelter()
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'giro'    => [
                'target' => 'regning',
                'type'  => Regning::class
            ],
            'forsendelsesdato'    => [
                'type'  => 'date',
                'allowNull' => true
            ],
            'forsendelse'    => [
                'type'  => 'string',
                'allowNull' => true
            ],
            'oppdrag'    => [
                'type'  => 'int',
                'allowNull'=>true
            ],
            'kvittert_dato'    => [
                'type'  => 'date',
                'allowNull' => true
            ],
            'kvitteringsforsendelse'    => [
                'type'  => 'string',
                'allowNull' => true
            ],
            'status'    => [
                'type'  => 'string'
            ]
        ];
    }
}