<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use Kyegil\Leiebasen\Modell\Eav\EgenskapVarcharObjekt as Egenskap;
use Kyegil\Leiebasen\Modell\Eav\VerdiVarcharObjekt as Verdi;
use Kyegil\Leiebasen\Modell\Eav\VerdiVarcharObjektsett as Verdisett;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Purring\Kravlinksett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Sett;

/**
 * Purringsett
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @method Purring current()
 * @method Purring|null hentFørste()
 * @method Purring|null hentSiste()
 * @method Purring|null hentTilfeldig()
 */
class Purringsett extends Sett
{
    /** @var string */
    protected string $model = Purring::class;

    /** @var Purring[] */
    protected ?array $items = null;

    /**
     * @return $this
     */
    private function leggTilPurregebyrModell(): Purringsett
    {
        $this
            ->leggTilLeftJoin(['purregebyr' => Krav::hentTabell()], Purring::hentTabell() . '.`purregebyr` = purregebyr.' . Krav::hentPrimærnøkkelfelt())
            ->leggTilModell(Krav::class, 'purregebyr', 'purregebyr')
        ;
        return $this;
    }

    /**
     * @param string $model
     * @param array $di
     * @throws \Kyegil\CoreModel\CoreModelDiException
     */
    public function __construct(string $model, array $di)
    {
        parent::__construct($model, $di);
        $this
            ->leggTilPurregebyrModell()
            ->leggTilSortering('purredato')
        ;
    }

    /**
     * @return $this
     */
    public function leggTilLeftJoinForKrav(?string $alias = null): Purringsett
    {
        $alias = $alias ?: Krav::hentTabell();
        $this
            ->leggTilInnerJoin([$alias => Krav::hentTabell()], Purring::hentTabell() . '.`krav` = `' . $alias . '`.' . Krav::hentPrimærnøkkelfelt())
        ;
        return $this;
    }

    /**
     * Legg til join for å laste leieforholdmodellen
     *
     * @param bool $samtKrav
     * @param string|null $alias
     * @param string|null $kravAlias
     * @return $this
     */
    public function leggTilLeftJoinForLeieforhold(bool $samtKrav = true, ?string $alias = null, ?string $kravAlias = null): Purringsett
    {
        $alias = $alias ?: Leieforhold::hentTabell();
        $kravAlias = $kravAlias ?: Krav::hentTabell();
        if ($samtKrav) {
            $this->leggTilLeftJoinForKrav($kravAlias);
        }
        $this
            ->leggTilLeftJoin([$alias => Leieforhold::hentTabell()], '`' . $kravAlias . '`.`leieforhold` = `' . $alias . '`.' . Leieforhold::hentPrimærnøkkelfelt())
        ;
        return $this;
    }

    /**
     * Legg til join for å laste leieobjektet gjennom leieforholdet
     *
     * Join for leieforholdet må legges til først
     *
     * @param string|null $alias
     * @param string|null $LeieforholdAlias
     * @return $this
     */
    public function leggTilLeftJoinForLeieforholdLeieobjekt(?string $alias = null, ?string $LeieforholdAlias = null): Purringsett
    {
        $alias = $alias ?: 'leieobjekt';
        $LeieforholdAlias = $LeieforholdAlias ?: Leieforhold::hentTabell();
        $this
            ->leggTilLeftJoin([$alias => Leieobjekt::hentTabell()], '`' . $LeieforholdAlias . '`.`leieobjekt` = `' . $alias . '`.' . Leieobjekt::hentPrimærnøkkelfelt())
        ;
        return $this;
    }

    /**
     * Legg til join for å laste leieforholdets regningsobjekt
     *
     * Join for leieforholdet må legges til først
     *
     * @param string|null $alias
     * @param string|null $LeieforholdAlias
     * @return $this
     */
    public function leggTilLeftJoinForLeieforholdRegningsobjekt(?string $alias = null, ?string $LeieforholdAlias = null): Purringsett
    {
        $alias = $alias ?: 'regningsobjekt';
        $LeieforholdAlias = $LeieforholdAlias ?: Leieforhold::hentTabell();
        $this
            ->leggTilLeftJoin([$alias => Leieobjekt::hentTabell()], '`' . $LeieforholdAlias . '`.`regningsobjekt` = `' . $alias . '`.' . Leieobjekt::hentPrimærnøkkelfelt())
        ;
        return $this;
    }

    /**
     * Legg til join for å laste leieforholdets utdelingsorden
     *
     * Join for leieforholdet må legges til først
     *
     * @param string|null $alias
     * @param string|null $LeieforholdAlias
     * @return $this
     */
    public function leggTilLeftJoinForLeieforholdUtdelingsplassering(?string $alias = null, ?string $LeieforholdAlias = null): Purringsett
    {
        $alias = $alias ?: 'utdelingsorden';
        $LeieforholdAlias = $LeieforholdAlias ?: Leieforhold::hentTabell();
        $this
            ->leggTilLeftJoin([$alias => 'utdelingsorden'], '`' . $LeieforholdAlias . '`.`regningsobjekt` = `' . $alias . '`.`leieobjekt` AND `utdelingsorden`.`rute` = ' . $this->app->hentValg('utdelingsrute'))
        ;
        return $this;
    }

    /**
     * Legger til felt for utdelingsplassering
     *
     * Krever eksisterende Join med leieforhold
     *
     * @return $this
     */
    public function leggTilLeieforholdUtdelingsplasseringFelt(): Purringsett
    {
        $this->leggTilFelt('utdelingsorden', 'plassering', null, 'utdelingsplassering', 'leieforhold.leieforhold_ekstra');

        return $this;
    }

    /**
     * @return Kravlinksett
     * @throws \Exception
     */
    public function inkluderKravlinker(): Kravlinksett
    {
        /** @var Kravlinksett $kravlinksett */
        $kravlinksett = $this->inkluder('kravlinker');
        return $kravlinksett;
    }

    public function slettAlle(): Purringsett
    {
        $this->app->pre($this, __FUNCTION__, []);
        /** @var Verdisett $eavVerdier */
        $eavVerdier = $this->app->hentSamling(Verdi::class);
        $eavVerdier->leggTilInnerJoin(Egenskap::hentTabell(),
            '`' . Verdi::hentTabell() . '`.`egenskap_id` = `' . Egenskap::hentTabell() . '`.`' . Egenskap::hentPrimærnøkkelfelt() . '`');
        $eavVerdier->leggTilFilter(['`' . Egenskap::hentTabell() . '`.`modell`' => $this->model])
            ->leggTilFilter(['`' . Verdi::hentTabell() . '`.`objekt_id`' => $this->hentIdNumre()]);
        $eavVerdier->låsFiltre()->slettAlle();

        return $this->app->post($this, __FUNCTION__, parent::slettAlle());
    }
}