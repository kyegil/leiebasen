<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 01/12/2020
 * Time: 16:26
 */

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use Kyegil\Leiebasen\Modell;

/**
 * Class AvtaleMal
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @property integer $malnr
 * @property string $type
 * @property string $malnavn
 * @property string $mal
 * @property boolean $erFramleiemal
 *
 * @method integer hentMalnr()
 * @method string hentType()
 * @method string hentMalnavn()
 * @method string hentMal()
 * @method boolean hentErFramleiemal()
 *
 * @method $this settType(string $type)
 * @method $this settMalnavn(string $malnavn)
 * @method $this settMal(string $mal)
 * @method $this settErFramleiemal(boolean $erFramleiemal)
 */
class AvtaleMal extends Modell
{
    const TYPE_LEIEAVTALE = 'leieavtale';

    const TYPE_FRAMLEIEAVTALE = 'framleieavtale';

    const TYPE_BETALINGSPLAN = 'betalingsplan';

    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'avtalemaler';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'malnr';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = AvtaleMalsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array {
        return [
            'malnr' => [
                'type' => 'integer'
            ],
            'type'    => [
                'type'  => 'string',
            ],
            'malnavn'    => [
                'type'  => 'string',
            ],
            'mal'    => [
                'type'  => 'string'
            ],
            'er_framleiemal'    => [
                'type'  => 'boolean'
            ]
        ];
    }
}