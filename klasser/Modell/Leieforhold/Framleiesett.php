<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Sett;

/**
 * Framleiesett
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @method Framleie current()
 */
class Framleiesett extends Sett
{
    protected string $model = Framleie::class;

    /** @var Framleie[] */
    protected ?array $items = null;

    /**
     * @return Framleiesett
     */
    public function leggTilLeftJoinForLeieforhold()
    {
        return $this
            ->leggTilLeftJoin(
                Leieforhold::hentTabell(),
                '`' . Framleie::hentTabell() . '`.`leieforhold` = `'
                . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`'
            );
    }

    /**
     * @return Framleiesett
     */
    public function leggTilLeieforholdModell()
    {
        return $this->leggTilLeftJoinForLeieforhold()->leggTilModell(Leieforhold::class);
    }
}