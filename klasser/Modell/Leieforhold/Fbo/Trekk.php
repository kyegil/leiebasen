<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 08/12/2020
 * Time: 16:39
 */

namespace Kyegil\Leiebasen\Modell\Leieforhold\Fbo;


use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Fbo;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Nets\Forsendelse\Oppdrag\Transaksjon\AvtaleGiro\Betalingskrav;
use stdClass;

/**
 * Class Trekk
 * @package Kyegil\Leiebasen\Modell\Leieforhold\Fbo
 *
 * @property int $id
 * @property Leieforhold $leieforhold
 * @property Regning|null $regning
 * @property string $kid
 * @property DateTimeInterface $overføringsdato (Datoen for overføring til NETS)
 * @property integer $forsendelse
 * @property integer $oppdrag
 * @property float $beløp
 * @property DateTimeInterface $forfallsdato
 * @property DateTimeInterface $varslet
 * @property boolean $egenvarsel
 * @property string $mobilnr
 * @property DateTimeInterface|null $tilSletting
 *
 * @method Leieforhold hentLeieforhold()
 * @method Regning|null hentRegning()
 * @method string hentKid()
 * @method DateTimeInterface hentOverføringsdato()
 * @method int hentForsendelse()
 * @method string hentOppdrag()
 * @method float hentBeløp()
 * @method DateTimeInterface hentForfallsdato()
 * @method DateTimeInterface hentVarslet()
 * @method boolean hentEgenvarsel()
 * @method string hentMobilnr()
 * @method DateTimeInterface|null hentTilSletting()
 *
 * @method $this settLeieforhold(Leieforhold $leieforhold)
 * @method $this settRegning(Regning|null $regning)
 * @method $this settKid(string $kid)
 * @method $this settOverføringsdato(DateTimeInterface $overføringsdato)
 * @method $this settForsendelse(int $forsendelse)
 * @method $this settOppdrag(string $oppdrag)
 * @method $this settBeløp(float $beløp)
 * @method $this settForfallsdato(DateTimeInterface $forfallsdato)
 * @method $this settVarslet(DateTimeInterface $varslet)
 * @method $this settEgenvarsel(boolean $egenvarsel)
 * @method $this settMobilnr(string $mobilnr)
 * @method $this settTilSletting(DateTimeInterface|null $tilSletting)
 */
class Trekk extends \Kyegil\Leiebasen\Modell
{
    /** @var string Databasetabellen som holder primærnokkelen for modellen */
    protected static $dbTable = 'fbo_trekkrav';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Trekksett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    protected static function hentDbFelter()
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'leieforhold'    => [
                'type'  => Leieforhold::class
            ],
            'gironr'    => [
                'target' => 'regning',
                'type'  => Regning::class,
                'allowNull' => true
            ],
            'kid'    => [
                'type'  => 'string'
            ],
            'overføringsdato'    => [
                'type'  => 'date',
                'allowNull' => true
            ],
            'forsendelse'    => [
                'type'  => 'integer'
            ],
            'oppdrag'    => [
                'type'  => 'integer'
            ],
            'beløp'    => [
                'type'  => 'string',
            ],
            'forfallsdato'    => [
                'type'  => 'date',
                'allowNull' => true
            ],
            'varslet'    => [
                'type'  => 'datetime',
                'allowNull' => true
            ],
            'egenvarsel'    => [
                'type'  => 'boolean'
            ],
            'mobilnr'    => [
                'type'  => 'string'
            ],
            'til_sletting'    => [
                'type'  => 'datetime',
                'allowNull' => true
            ]
        ];
    }

    /**
     * @return string|null
     */
    public function hentForsendelsesKvitteringsfil()
    {
        if(!property_exists($this->data, 'forsendelsesKvitteringsfil')) {
            $this->data->forsendelsesKvitteringsfil = null;
            $overføringdato = $this->hentOverføringsdato();
            if($overføringdato) {
                $dato = clone $overføringdato;
                while($dato->diff($overføringdato)->days < 7) {
                    $filbane = $this->app->hentFilarkivBane() . '/nets/inn/AG-kvitteringer/' . $dato->format('Y-m') . '/KV.*.F' . str_pad($this->hentForsendelse(), 7, '0', STR_PAD_LEFT) . '.D' . $overføringdato->format('ymd') . '*';
                    $treff = glob($filbane);
                    if($treff) {
                        $this->data->forsendelsesKvitteringsfil = reset($treff);
                        break;
                    }
                    $dato->add(new DateInterval('P1D'));
                }
            }
        }
        return $this->data->forsendelsesKvitteringsfil;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function hentForsendelsesKvittering()
    {
        $fil = $this->hentForsendelsesKvitteringsfil();
        if($fil) {
            $innhold = file_get_contents($fil);
            $innhold = mb_convert_encoding($innhold, 'UTF-8',
                mb_detect_encoding($innhold, 'UTF-8, ISO-8859-1', true));
            if($innhold === false) {
                throw new Exception("Klarte ikke lese kvitteringsfil {$fil}");
            }
            return $innhold;
        }
        return null;
    }

    /**
     * @return Betalingskrav
     * @throws Exception
     */
    public function hentNetsTransaksjon(): Betalingskrav
    {
        if (!isset($this->data->nets_transaksjon)) {

            /** @var string[] $spesifikasjoner */
            $spesifikasjoner = [];
            $regning = $this->hentRegning();
            if ($regning) {
                foreach ($regning->hentKrav() as $krav) {
                    $spesifikasjoner[]
                        = $krav->tekst
                        . ' kr. ' . number_format( $krav->beløp, 2, ',', ' ' );
                }
            }

            $transaksjon = new Betalingskrav();
            $transaksjon->forfallsdato = $this->hentForfallsdato();
            $transaksjon->transaksjonstype = $this->hentEgenvarsel() ? Betalingskrav::TYPE_EGENVARSEL : Betalingskrav::TYPE_VARSLING_FRA_BANK;
            $transaksjon->mobilnr = $this->mobilnr;
            $transaksjon->beløp = $this->hentBeløp();
            $transaksjon->kid = $this->hentKid();
            $transaksjon->forkortetNavn = $this->leieforhold->hentKortnavn();
            $transaksjon->fremmedreferanse = $regning ? ('Regning ' . $this->hentRegning()) : null ;
            $transaksjon->spesifikasjon = implode("\n", $spesifikasjoner);

            $this->data->nets_transaksjon = $transaksjon;
        }
        return $this->data->nets_transaksjon;
    }

    /**
     * @param stdClass $parametere
     * @return Modell
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Modell
    {
        if ($parametere->regning instanceof Regning) {
            $parametere->leieforhold = $parametere->leieforhold ?? $parametere->regning->leieforhold;
            $parametere->kid = $parametere->kid ?? $parametere->regning->kid;
            $parametere->varslet = $parametere->varslet ?? $parametere->regning->utskriftsdato;
            $parametere->forfallsdato = $parametere->forfallsdato ?? $parametere->regning->hentForfall();
            $parametere->beløp = $parametere->beløp ?? $parametere->regning->hentBeløp();
        }
//        $parametere->overføringsdato = $parametere->overføringsdato ?? new DateTime();
        return parent::opprett($parametere);
    }

    /**
     * @return DateTimeImmutable|null
     * @throws Exception
     */
    public function hentSlettefrist(): ?DateTimeImmutable
    {
        if (!property_exists($this->data, 'slettefrist')) {
            /** @var DateTimeImmutable|null $slettefrist */
            $forfallsdato = $this->hentForfallsdato();

            if ($forfallsdato instanceof DateTime) {
                $slettefrist = DateTimeImmutable::createFromMutable($forfallsdato);
            }
            else {
                /** @var DateTimeImmutable $slettefrist */
                $slettefrist = clone $forfallsdato;
            }
            $slettefrist = $slettefrist->setTime(13, 59);

            $enDag = new DateInterval('P1D');

            $leieforhold = $this->hentLeieforhold();
            $fbo = $leieforhold->hentFbo('alle');
            if( !$fbo ) {
                $this->data->slettefrist = null;
                return null;
            }

            /*
             * Sletteanmodninger
             * være mottatt i Nets på en virkedag senest kl 14:00, dagen før forfallsdato.
             */
            $slettefrist = $slettefrist->sub( $enDag );

            while(
                $slettefrist->format('N') > 5
                || in_array( $slettefrist->format('m-d'), $this->leiebase->bankfridager($slettefrist->format('Y')))
            ) {
                $slettefrist = $slettefrist->sub($enDag);
            }

            $this->data->slettefrist = $slettefrist;
        }
        return $this->data->slettefrist;
    }

    /**
     * Hent Fast Betalingsoppdrag
     *
     * @return Fbo|null
     * @throws Exception
     */
    public function hentFbo(): ?Fbo
    {
        return $this->hentLeieforhold() && $this->hentLeieforhold()->hentFbo() ? $this->hentLeieforhold()->hentFbo() : null;
    }
}