<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold\Fbo;


use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Sett;

/**
 * Trekksett
 * @package Kyegil\Leiebasen\Modell\Leieforhold\Fbo
 *
 * @method Trekk current()
 * @method Trekk|null hentFørste()
 * @method Trekk|null hentSiste()
 * @method Trekk|null hentTilfeldig()
 */
class Trekksett extends Sett
{
    /** @var string */
    protected string $model = Trekk::class;

    /** @var Trekk[] */
    protected ?array $items = null;

    /**
     * @param string $model
     * @param array $di
     * @throws \Kyegil\CoreModel\CoreModelDiException
     */
    public function __construct(string $model, array $di)
    {
        parent::__construct($model, $di);
    }

    /**
     * Legg til LEFT JOIN for regning
     *
     * @return Trekksett
     */
    public function leggTilLeftJoinForRegning(): Trekksett
    {
        return $this->leggTilLeftJoin(Regning::hentTabell(), '`' . Trekk::hentTabell() . '`.`gironr` = `' . Regning::hentTabell() . '`.`' . Regning::hentPrimærnøkkelfelt() . '`');
    }

    /**
     * Legg til LEFT JOIN for leieforhold
     *
     * @return Trekksett
     */
    public function leggTilLeftJoinForLeieforhold(): Trekksett
    {
        return $this->leggTilLeftJoin(
            Leieforhold::hentTabell(),
            '`' . Trekk::hentTabell() . '`.`leieforhold`'
            . ' = `' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`');
    }

    /**
     * @return Trekksett
     */
    public function leggTilRegningModell(): Trekksett
    {
        return $this->leggTilLeftJoinForRegning()
            ->leggTilModell(Regning::class);
    }

    /**
     * @return Trekksett
     */
    public function leggTilLeieforholdModell(): Trekksett
    {
        return $this->leggTilLeftJoinForLeieforhold()
            ->leggTilModell(Leieforhold::class);
    }
}