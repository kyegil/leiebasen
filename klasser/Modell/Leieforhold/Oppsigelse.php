<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 03/08/2020
 * Time: 11:26
 */

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;

/**
 * Class Oppsigelse
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @property int $id
 * @property Leieforhold $leieforhold
 * @property DateTime $oppsigelsesdato
 * @property DateTime $fristillelsesdato
 * @property DateTime $oppsigelsestidSlutt
 * @property string $ref
 * @property string $merknad
 * @property boolean $oppsagtAvUtleier
 *
 * @method Leieforhold hentLeieforhold()
 * @method DateTime hentOppsigelsesdato()
 * @method DateTime hentFristillelsesdato()
 * @method DateTime hentOppsigelsestidSlutt()
 * @method string hentRef()
 * @method string hentMerknad()
 * @method string hentOppsagtAvUtleier()
 *
 * @method $this settLeieforhold(Leieforhold $leieforhold)
 * @method $this settOppsigelsesdato(DateTime $dato)
 * @method $this settFristillelsesdato(DateTime $dato)
 * @method $this settOppsigelsestidSlutt(DateTime $dato)
 * @method $this settRef(string $ref)
 * @method $this settMerknad(string $merknad)
 * @method $this settOppsagtAvUtleier(boolean $oppsagtAvUtleier)
*/
class Oppsigelse extends \Kyegil\Leiebasen\Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'oppsigelser';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'leieforhold';

    /**
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'leieforhold'    => [
                'type'  => Leieforhold::class
            ],
            'oppsigelsesdato'    => [
                'type'  => 'date'
            ],
            'fristillelsesdato'    => [
                'type'  => 'date'
            ],
            'oppsigelsestid_slutt'    => [
                'type'  => 'date'
            ],
            'ref'    => [
                'type'  => 'string'
            ],
            'merknad'    => [
                'type'  => 'string'
            ],
            'oppsagt_av_utleier'    => [
                'type'  => 'boolean'
            ]
        ];
    }

    /**
     * For oppsigelser vil vi laste oppsigelsen, men ikke leieforholdet, for å returnere id
     *
     * @return int|null
     * @throws Exception
     */
    public function hentId():? int
    {
        $leieforhold = $this->hent(static::$dbPrimaryKeyField);
        return $leieforhold && isset($this->rawData->{self::hentTabell()}->{self::$dbPrimaryKeyField}) ? intval($this->rawData->{self::hentTabell()}->{self::$dbPrimaryKeyField}) : null;
    }

    public function slett()
    {
        /** @var Andelsperiode $sisteAndelsperiode */
        $sisteAndelsperiode = $this->hentLeieforhold()->hentAndelsperioder()->hentSiste();
        $this->hentLeieforhold()->nullstill();
        parent::slett();
        if($sisteAndelsperiode) {
            $sisteAndelsperiode->settTildato(null);
        }
    }
}