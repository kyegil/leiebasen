<?php

namespace Kyegil\Leiebasen\Modell\Leieforhold;


use DateTimeInterface;
use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Leieforhold;

/**
 * Class Depositum
 * @package Kyegil\Leiebasen\Modell\Leieforhold
 *
 * @property int $id
 * @property Leieforhold $leieforhold
 * @property string $type
 * @property DateTimeInterface|null $dato
 * @property string $beløp
 * @property string $betaler
 * @property string $notat
 * @property string|null $konto
 * @property string|null $fil
 * @property DateTimeInterface|null $utløpsdato
 * @property DateTimeInterface[]|null $utløpspåminnelser
 *
 * @method Leieforhold hentLeieforhold()
 * @method string hentType()
 * @method DateTimeInterface|null hentDato()
 * @method string hentBeløp()
 * @method string hentBetaler()
 * @method string hentNotat()
 * @method string|null hentKonto()
 * @method string|null hentFil()
 * @method DateTimeInterface|null hentUtløpsdato()
 * @method DateTimeInterface[]|null hentUtløpspåminnelser()
 *
 * @method $this settLeieforhold(Leieforhold $leieforhold)
 * @method $this settType(string $type)
 * @method $this settDato(DateTimeInterface|null $dato)
 * @method $this settBeløp(string $beløp)
 * @method $this settBetaler(string $betaler)
 * @method $this settNotat(string $notat)
 * @method $this settKonto(string|null $konto)
 * @method $this settFil(string|null $fil)
 * @method $this settUtløpspåminnelser(DateTimeInterface[]|null $utløpspåminnelser)
*/
class Depositum extends Modell
{
    const TYPE_DEPOSITUM = 'depositum';
    const TYPE_GARANTI = 'garanti';

    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'depositum';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /**
     * @inheritDoc
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'int',
            ],
            'leieforhold_id'    => [
                'target' => 'leieforhold',
                'type'  => Leieforhold::class,
            ],
            'type'    => [
                'type'  => 'string',
            ],
            'dato'    => [
                'type'  => 'date',
                'allowNull' => true,
            ],
            'beløp'    => [
                'type'  => 'string',
            ],
            'betaler'    => [
                'type'  => 'string',
            ],
            'notat'    => [
                'type'  => 'string',
            ],
            'konto'    => [
                'type'  => 'string',
                'allowNull' => true,
            ],
            'fil'    => [
                'type'  => 'string',
                'allowNull' => true,
            ],
            'utløpsdato'    => [
                'type'  => 'date',
                'allowNull' => true,
            ],
            'utløpspåminnelser'    => [
                'type'  => 'datetime',
                'allowNull' => true,
                'allowMultiple' => true,
            ],
        ];
    }

    /**
     * @return void
     * @throws Exception
     */
    public function slett()
    {
        $this->hentLeieforhold()->nullstill();
        parent::slett();
    }

    /**
     * @param DateTimeInterface|null $utløpsdato
     * @return Depositum|$this
     * @throws Exception
     */
    public function settUtløpsdato(?DateTimeInterface $utløpsdato): Depositum
    {
        $eksisterendeUtløpsdato = $this->hent('utløpsdato');
        $eksisterendeDatoStreng = $eksisterendeUtløpsdato ? $eksisterendeUtløpsdato->format('Ymd') : '';
        $datoStreng = $utløpsdato ? $utløpsdato->format('Ymd') : '';
        if($datoStreng == $eksisterendeDatoStreng) {
            return $this;
        }
        $this->settUtløpspåminnelser(null);
        return $this->sett('utløpsdato', $utløpsdato);
    }
}