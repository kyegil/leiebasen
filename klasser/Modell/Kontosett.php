<?php

namespace Kyegil\Leiebasen\Modell;


use Kyegil\Leiebasen\Sett;

/**
 * Class Kontosett
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Konto current()
 */
class Kontosett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Konto::class;

    /**
     * @var Konto[]
     */
    protected ?array $items = null;
}