<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 06/04/2023
 * Time: 12:40
 */

namespace Kyegil\Leiebasen\Modell;

class SøknadTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @test
     * @return array
     */
    public function testTrekkNøkkelord(): void
    {
        $innhold = 'Dette er en tekststreng, den inneholder små (4) og store (1234567890) tall';
        $stikkord = ['dette', 'tekststreng', 'den', 'inneholder', 'små', 'store', '1234567890', 'tall'];

        $this->assertContains('dette', Søknad::trekkNøkkelord($innhold));

        $this->assertEquals($stikkord, Søknad::trekkNøkkelord($innhold));
    }
}