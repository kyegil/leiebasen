<?php

namespace Kyegil\Leiebasen\Modell;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Modell\Søknad\Type;
use Kyegil\Leiebasen\Sett;

/**
 * Class Søknadsett
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Søknad current()
 */
class Søknadsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Søknad::class;

    /**
     * @var Søknad[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }

    /**
     * Legg til LEFT JOIN for søknadstype
     *
     * @return Søknadsett
     */
    public function leggTilInnerJoinForSøknadType(): Søknadsett
    {
        return $this
            ->leggTilLeftJoin(Type::hentTabell(), Søknad::hentTabell() . '.`søknadstype` = ' . Type::hentTabell() . '.' . Type::hentPrimærnøkkelfelt());
    }

    /**
     * Legg til søknadstype-modell
     *
     * @return Søknadsett
     */
    public function leggTilSøknadTypeModell(): Søknadsett
    {
        return $this->leggTilInnerJoinForSøknadType()
            ->leggTilModell(Type::class);
    }
}