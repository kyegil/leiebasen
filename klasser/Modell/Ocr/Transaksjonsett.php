<?php

namespace Kyegil\Leiebasen\Modell\Ocr;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Sett;

/**
 * Class Transaksjonsett
 *
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Transaksjon current()
 */
class Transaksjonsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Transaksjon::class;

    /**
     * @var Transaksjon[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }

    /**
     * @return Transaksjonsett
     */
    public function leggTilInnbetalingModell(): Transaksjonsett {
        $this->leggTilLeftJoin(Innbetaling::hentTabell(), '`' . Innbetaling::hentTabell() . '`.`ocr_transaksjon` = `' . Transaksjon::hentTabell() . '`.`' . Transaksjon::hentPrimærnøkkelfelt() . '`');
        return $this->leggTilFelt(Innbetaling::hentTabell(), Innbetaling::hentPrimærnøkkelfelt(), 'MIN', 'innbetaling', 'ocr_transaksjoner_ekstra');
    }
}