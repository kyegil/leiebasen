<?php

namespace Kyegil\Leiebasen\Modell\Ocr;


use DateTime;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Innbetalingsett;

/**
 * Class Transaksjon
 * @package Kyegil\Leiebasen\Modell\Ocr
 *
 * @property int $id
 * @property Fil $fil
 * @property int $forsendelsesnummer
 * @property int $oppdragsnummer
 * @property string $oppdragskonto
 * @property int $avtaleId
 * @property int $transaksjonstype
 * @property int $transaksjonsnummer
 * @property DateTime $oppgjørsdato
 * @property string $bankdatasentral
 * @property int $delavregningsnummer
 * @property int $løpenummer
 * @property float $beløp
 * @property string $kid
 * @property string $blankettnummer
 * @property string $arkivreferanse
 * @property DateTime $oppdragsdato
 * @property string $debetkonto
 * @property string $fritekst
 *
 * @method int hentId()
 * @method Fil hentFil()
 * @method int hentForsendelsesnummer()
 * @method int hentOppdragsnummer()
 * @method string hentOppdragskonto()
 * @method int hentAvtaleId()
 * @method int hentTransaksjonstype()
 * @method int hentTransaksjonsnummer()
 * @method DateTime hentOppgjørsdato()
 * @method string hentBankdatasentral()
 * @method int hentDelavregningsnummer()
 * @method int hentLøpenummer()
 * @method float hentBeløp()
 * @method string hentKid()
 * @method string hentBlankettnummer()
 * @method string hentArkivreferanse()
 * @method DateTime hentOppdragsdato()
 * @method string hentDebetkonto()
 * @method string hentFritekst()
 *
 * @method $this settId(int $id)
 * @method $this settFil(Fil $fil)
 * @method $this settForsendelsesnummer(int $forsendelsesnummer)
 * @method $this settOppdragsnummer(int $oppdragsnummer)
 * @method $this settOppdragskonto(string $oppdragskonto)
 * @method $this settAvtaleId(int $avtaleId)
 * @method $this settTransaksjonstype(int $transaksjonstype)
 * @method $this settTransaksjonsnummer(int $transaksjonsnummer)
 * @method $this settOppgjørsdato(DateTime $oppgjørsdato)
 * @method $this settBankdatasentral(string $bankdatasentral)
 * @method $this settDelavregningsnummer(int $delavregningsnummer)
 * @method $this settLøpenummer(int $løpenummer)
 * @method $this settBeløp(float $beløp)
 * @method $this settKid(string $kid)
 * @method $this settBlankettnummer(string $blankettnummer)
 * @method $this settArkivreferanse(string $arkivreferanse)
 * @method $this settOppdragsdato(DateTime $oppdragsdato)
 * @method $this settDebetkonto(string $debetkonto)
 * @method $this settFritekst(string $fritekst)
 */
class Transaksjon extends Modell
{
    const TYPE_BESKRIVELSE = [
        10 => 'Giro belastet konto',
        11 => 'Faste Oppdrag',
        12 => 'Direkte Remittering',
        13 => 'BTG (Bedrifts Terminal Giro)',
        14 => 'SkrankeGiro',
        15 => 'AvtaleGiro',
        16 => 'TeleGiro',
        17 => 'Giro - betalt kontant',
        18 => 'Reversering med KID',
        19 => 'Kjøp med KID',
        20 => 'Reversering med fritekst',
        21 => 'Kjøp med fritekst'
    ];

    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'ocr_transaksjoner';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Transaksjonsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * @inheritDoc
     */
    public static function harFlere(string $reference, ?string $model = null, ?string $foreignKey = null, array $filters = [], ?callable $callback = null): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::harFlere('innbetalinger', Innbetaling::class, 'ocr_transaksjon');
        }
        return parent::harFlere($reference, $model, $foreignKey, $filters, $callback);
    }

    /**
     * @inheritDoc
     */
    protected static function hentDbFelter()
    {
        return [
            'id' => [
                'type' => 'int'
            ],
            'fil_id' => [
                'type' => Fil::class,
                'target' => 'fil'
            ],
            'forsendelsesnummer' => [
                'type' => 'int',
            ],
            'oppdragsnummer' => [
                'type' => 'int',
            ],
            'oppdragskonto' => [
                'type' => 'string',
            ],
            'avtaleid' => [
                'target' => 'avtale_id',
                'type' => 'int',
            ],
            'transaksjonstype' => [
                'type' => 'int',
            ],
            'transaksjonsnummer' => [
                'type' => 'int',
            ],
            'oppgjørsdato' => [
                'type' => 'date',
            ],
            'bankdatasentral' => [
                'type' => 'string',
            ],
            'delavregningsnummer' => [
                'type' => 'int',
            ],
            'løpenummer' => [
                'type' => 'int',
            ],
            'beløp' => [
                'type' => 'string',
            ],
            'kid' => [
                'type' => 'string',
            ],
            'blankettnummer' => [
                'type' => 'string',
            ],
            'arkivreferanse' => [
                'type' => 'string',
            ],
            'oppdragsdato' => [
                'type' => 'date',
            ],
            'debetkonto' => [
                'type' => 'string',
            ],
            'fritekst' => [
                'type' => 'string',
            ]
        ];
    }

    /**
     * @return string
     */
    public function hentTransaksjonsbeskrivelse(): string
    {
        return isset(self::TYPE_BESKRIVELSE[$this->hentTransaksjonstype()])
            ? static::TYPE_BESKRIVELSE[$this->hentTransaksjonstype()]
            : '';
    }

    /**
     * @return Innbetaling|null
     * @throws \Exception
     */
    public function hentInnbetaling(): ?Innbetaling {
        if(!property_exists($this->data, 'innbetaling')) {
            if (isset($this->rawData->ocr_transaksjoner_ekstra)
                && property_exists($this->rawData->ocr_transaksjoner_ekstra, 'innbetaling')
            ) {
                $this->data->innbetaling =
                    $this->rawData->ocr_transaksjoner_ekstra->innbetaling ?
                    $this->app->hentModell(Innbetaling::class, $this->rawData->ocr_transaksjoner_ekstra->innbetaling)
                    : null;
            }
            else {
                /** @var Innbetalingsett $innbetalingssett */
                $innbetalingssett = $this->hentSamling('innbetalinger');
                $this->data->innbetaling = $innbetalingssett->hentFørste();
            }
        }
        return $this->data->innbetaling;

    }
}