<?php

namespace Kyegil\Leiebasen\Modell\Ocr;


use DateTime;
use Exception;
use stdClass;

/**
 * Class Fil
 * @package Kyegil\Leiebasen\Modell\Ocr
 *
 * @property int $filId
 * @property int $forsendelsesnummer
 * @property DateTime $oppgjørsdato
 * @property string $ocr
 * @property DateTime $registrert
 * @property string $registrerer
 *
 * @method int hentFilId()
 * @method int hentForsendelsesnummer()
 * @method DateTime hentOppgjørsdato()
 * @method string hentOcr()
 * @method DateTime hentRegistrert()
 * @method string hentRegistrerer()
 *
 * @method $this settForsendelsesnummer(int $forsendelsesnummer)
 * @method $this settOppgjørsdato(DateTime $oppgjørsdato)
 * @method $this settOcr(string $ocr)
 * @method $this settRegistrert(DateTime $registrert)
 * @method $this settRegistrerer(string $registrerer)
 */
class Fil extends \Kyegil\Leiebasen\Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'ocr_filer';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'fil_id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Filsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * @inheritDoc
     */
    public static function harFlere(string $reference, ?string $model = null, ?string $foreignKey = null, array $filters = [], ?callable $callback = null): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::harFlere('transaksjoner', Transaksjon::class, 'fil_id', [],
                function(Transaksjonsett $transaksjonsett) {
                    $transaksjonsett->leggTilSortering('transaksjonsnummer');
                }
            );
        }
        return parent::harFlere($reference, $model, $foreignKey, $filters, $callback);
    }

    /**
     * @inheritDoc
     */
    protected static function hentDbFelter()
    {
        return [
            'fil_id' => [
                'type' => 'int'
            ],
            'forsendelsesnummer' => [
                'type' => 'int'
            ],
            'oppgjørsdato' => [
                'type' => 'date'
            ],
            'ocr' => [
                'type' => 'string'
            ],
            'registrert' => [
                'type' => 'datetime'
            ],
            'registrerer' => [
                'type' => 'string'
            ]
        ];
    }

    /**
     * @param stdClass $parametere
     * @return Fil
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Fil
    {
        $parametere->registrert = new DateTime();
        $parametere->registrerer = $this->app->bruker['navn'];
        return parent::opprett($parametere);
    }

    /**
     * @return Transaksjonsett
     * @throws Exception
     */
    public function hentTransaksjoner(): Transaksjonsett
    {
        /** @var Transaksjonsett $transaksjonsett */
        $transaksjonsett = $this->hentSamling('transaksjoner');

        if(!isset($this->samlinger->transaksjoner)) {
            $this->samlinger->transaksjoner = $transaksjonsett->hentElementer();
        }
        $transaksjonsett->lastFra($this->samlinger->transaksjoner);
        return clone $transaksjonsett;
    }
}