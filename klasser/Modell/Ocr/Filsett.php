<?php

namespace Kyegil\Leiebasen\Modell\Ocr;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Sett;

/**
 * Class Filsett
 *
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Fil current()
 */
class Filsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Fil::class;

    /**
     * @var Fil[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }
}