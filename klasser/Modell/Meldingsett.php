<?php

namespace Kyegil\Leiebasen\Modell;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Sett;

/**
 * Class Meldingsett
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Melding current()
 */
class Meldingsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Melding::class;

    /**
     * @var Melding[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }
}