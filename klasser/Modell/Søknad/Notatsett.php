<?php

namespace Kyegil\Leiebasen\Modell\Søknad;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Sett;

/**
 * Class Notatsett
 *
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Notat current()
 */
class Notatsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Notat::class;

    /**
     * @var Notat[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }
}