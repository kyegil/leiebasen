<?php

namespace Kyegil\Leiebasen\Modell\Søknad;

use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Eav\Egenskap as EavEgenskap;
use Kyegil\Leiebasen\Modell\Eav\Egenskapsett;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Modell\Søknadsett;
use stdClass;

/**
 * Class Type
 *
 * @package Kyegil\Leiebasen\Modell\Søknad
 *
 * @property integer $id
 * @property string $kode
 * @property string $navn
 * @property string $beskrivelse
 * @property object|null $konfigurering
 *
 * @method int hentId()
 * @method string hentKode()
 * @method string hentNavn()
 * @method string hentBeskrivelse()
 *
 * @method $this settKode(string $kode)
 * @method $this settNavn(string $navn)
 * @method $this settBeskrivelse(string $beskrivelse)
 */
class Type extends Modell
{
    const FELT_KONTAKT_ETTERNAVN = 'kontaktperson_etternavn';
    const FELT_KONTAKT_FORNAVN = 'kontaktperson_fornavn';
    const FELT_KONTAKT_TELEFON = 'kontakt_telefon';
    const FELT_KONTAKT_EPOST = 'kontakt_epost';

    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'søknadstyper';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Typesett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'kode'    => [
                'type'  => 'string'
            ],
            'navn'    => [
                'type'  => 'string'
            ],
            'beskrivelse'    => [
                'type'  => 'string'
            ],
            'konfigurering'    => [
                'type'  => 'json'
            ]
        ];
    }

    /**
     * Rekursivt søk etter et objekt i et array av objekter, eller blant underobjekter i et objekt
     *
     * @param string $egenskap
     * @param string $verdi
     * @param mixed $objekt
     * @return object
     */
    public static function søkObjektEtter(string $egenskap, string $verdi, $objekt)
    {
        if (is_object($objekt)) {
            if (property_exists($objekt, $egenskap) && $objekt->$egenskap == $verdi) {
                return $objekt;
            }
        }
        else if (is_array($objekt)) {
            foreach($objekt as $element) {
                if ($resultat = self::søkObjektEtter($egenskap, $verdi, $element)) {
                    return $resultat;
                }
            }
            return null;
        }
        return null;
    }

    /**
     * Henter konfigureringsverdi
     *
     * Mulige verdier er:
     * * felter Alle skjemafeltene i rekkefølge
     * * avsenderEpost
     * * aktiveFelter Feltene som skal vises på skjema
     * * layout
     * * bekreftelsesTekst
     * * bekreftelsesTekst
     *
     * @param string|null $egenskap
     * @param string|null $språk
     * @return mixed
     * @throws Exception
     */
    public function hentKonfigurering(?string $egenskap = null, ?string $språk = null)
    {
        $konfigurering = $this->hent('konfigurering');
        if($egenskap && is_object($konfigurering)) {
            $resultat = $konfigurering->{$egenskap} ?? null;
            if ($språk) {
                $resultat = property_exists($konfigurering, "{$egenskap}[{$språk}]")
                    ? $konfigurering->{"{$egenskap}[{$språk}]"}
                    : $resultat
                ;
            }
            return $resultat;
        }
        return $konfigurering;
    }

    /**
     * @param mixed $data
     * @param string|null $egenskap
     * @return $this
     * @throws Exception
     */
    public function settKonfigurering($data = null, ?string $egenskap = null): Type
    {
        if ($egenskap) {
            $konfigurering = $this->hent('konfigurering');
            settype($konfigurering, 'object');
            $konfigurering->{$egenskap} = $data;
            return $this->sett('konfigurering', $konfigurering);
        }
        $data = $data ? (object)$data : null;
        return $this->sett('konfigurering', $data);
    }

    /**
     * @return string[]
     * @throws Exception
     */
    public function hentFelter(): array
    {
        return $this->hentKonfigurering('felter') ?? [];
    }

    /**
     * @param string[] $felter
     * @return $this
     * @throws Exception
     */
    public function settFelter(array $felter): Type
    {
        return $this->settKonfigurering($felter, 'felter');
    }

    /**
     * @param string[] $aktiveFelter
     * @return $this
     * @throws Exception
     */
    public function settAktiveFelter(array $aktiveFelter): Type
    {
        return $this->settKonfigurering($aktiveFelter, 'aktive_felter');
    }

    /**
     * @return Egenskapsett
     * @throws Exception
     */
    public function hentFeltKonfigurasjoner(): Egenskapsett
    {
        $felter = $this->hentFelter();
        /** @var Egenskapsett $feltKonfigurasjoner */
        $feltKonfigurasjoner = $this->app->hentSamling(EavEgenskap::class);
        $feltKonfigurasjoner
            ->leggTilFilter(['modell' => Søknad::class])
            ->leggTilFilter(['kode' => array_values($felter)])
            ->låsFiltre()
            ->leggTilSortering('rekkefølge');
        if(!isset($this->samlinger->feltKonfigurasjoner)) {
            $this->samlinger->feltKonfigurasjoner = $feltKonfigurasjoner->hentElementer();
            /**
             * @var EavEgenskap $a
             * @var EavEgenskap $b
             */
            usort($this->samlinger->feltKonfigurasjoner, function ($a, $b) {
                $felter = array_values($this->hentFelter());
                $posA = array_search($a->kode, $felter);
                $posB = array_search($b->kode, $felter);

                /**
                 * Dersom et felt ikke finnes i feltkonfigurasjonen,
                 * sorteres dette sist
                 */
                if ($posA === false || $posB === false) {
                    return intval($posA === false) - intval($posB === false);
                }

                /**
                 * Feltene sorteres etter posisjon i feltkonfigurasjonen
                 */
                return $posA - $posB;
            });
        }
        $feltKonfigurasjoner->lastFra($this->samlinger->feltKonfigurasjoner);
        return clone $feltKonfigurasjoner;
    }

    /**
     * @return string[]
     * @throws Exception
     */
    public function hentKonfigAktiveFelter(?string $språk = null): array
    {
        /** @var stdClass|null $konfigurering */
        return $this->hentKonfigurering('aktive_felter', $språk) ?? [];
    }

    /**
     * @param string|null $språk
     * @return stdClass[]
     * @throws Exception
     */
    public function hentFeltLayout(?string $språk = null)
    {
        return $this->hentKonfigurering('layout', $språk) ?? [];
    }

    /**
     * @param string|null $språk
     * @return Egenskapsett
     * @throws Exception
     */
    public function hentFeltsett(?string $språk = null): Egenskapsett
    {
        /** @var Egenskapsett $feltsett */
        $feltsett = $this->app->hentSamling(EavEgenskap::class);
        $feltsett
            ->leggTilFilter(['modell' => Søknad::class])
            ->leggTilFilter(['`' . EavEgenskap::hentTabell() . '`.`kode` IN' => $this->hentFelter($språk)])
            ->leggTilSortering('rekkefølge')
        ;
        if (!isset($this->samlinger->feltsett)) {
            $this->samlinger->feltsett = $feltsett->hentElementer();
        }
        $feltsett->lastFra($this->samlinger->feltsett);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $feltsett;
    }

    /**
     * @return string[]
     * @throws Exception
     */
    public function hentKategorier()
    {
        if(!isset($this->data->kategorier)) {
            $tp = $this->app->mysqli->table_prefix;
            $this->data->kategorier = $this->app->mysqli->arrayData([
                'source' => '`' . $tp . Kategori::hentTabell() . '` AS `' . Kategori::hentTabell() . '` INNER JOIN `' . $tp . Søknad::hentTabell() . '` AS `' . Søknad::hentTabell() . '` ON `' . Kategori::hentTabell() . '`.`søknad_id` = `' . Søknad::hentTabell() . '`.`' . Søknad::hentPrimærnøkkelfelt() . '`',
                'fields' => ['kategori'],
                'flat' => true,
                'distinct' => true,
                'where' => ['`' . Søknad::hentTabell() . '`.`søknadstype`' => $this->hentId()]
            ])->data;
        }

        return $this->data->kategorier;
    }

    public function hentAktiveSøknader(): Søknadsett
    {
        /** @var Søknadsett $søknadsett */
        $søknadsett = $this->app->hentSamling(Søknad::class)
            ->leggTilFilter(['søknadstype' => $this->hentId()])
            ->leggTilFilter(['aktiv' => true])
            ->låsFiltre()
            ->leggTilSortering('oppdatert')
        ;
        if(!isset($this->samlinger->søknader)) {
            $this->samlinger->søknader = $søknadsett->hentElementer();
        }
        $søknadsett->lastFra($this->samlinger->søknader);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $søknadsett;
    }
}