<?php

namespace Kyegil\Leiebasen\Modell\Søknad;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Sett;

/**
 * Class Adgangsett
 *
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Adgang current()
 */
class Adgangsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Adgang::class;

    /**
     * @var Adgang[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }
}