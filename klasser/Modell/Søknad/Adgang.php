<?php

namespace Kyegil\Leiebasen\Modell\Søknad;

use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Søknad;

/**
 * Class Adgang
 *
 * @package Kyegil\Leiebasen\Modell\Søknad
 *
 * @property integer $id
 * @property Søknad $søknad
 * @property string $login
 * @property string $epost
 * @property string $referanse
 * @property string $pw
 * @property string $nødpassord
 *
 * @method int hentId()
 * @method Søknad hentSøknad()
 * @method string hentLogin()
 * @method string hentEpost()
 * @method string hentReferanse()
 * @method string hentPw()
 * @method string hentNødpassord()
 *
 * @method $this settSøknad(Søknad $søknad)
 * @method $this settLogin(string $login)
 * @method $this settEpost(string $epost)
 * @method $this settReferanse(string $referanse)
 * @method $this settPw(string $pw)
 * @method $this settNødpassord(string $nødpassord)
 */
class Adgang extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'søknad_adganger';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Adgangsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'søknad_id'    => [
                'target' => 'søknad',
                'type'  => Søknad::class
            ],
            'login'    => [
                'type'  => 'string'
            ],
            'epost'    => [
                'type'  => 'string'
            ],
            'referanse'    => [
                'type'  => 'string'
            ],
            'pw'    => [
                'type'  => 'string'
            ],
            'nødpassord'    => [
                'type'  => 'string',
                'allowNull' => true
            ]
        ];
    }
}