<?php

namespace Kyegil\Leiebasen\Modell\Søknad;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Sett;

/**
 * Class Kategorisett
 *
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Kategori current()
 */
class Kategorisett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Kategori::class;

    /**
     * @var Kategori[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }
}