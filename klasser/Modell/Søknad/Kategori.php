<?php

namespace Kyegil\Leiebasen\Modell\Søknad;

use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Søknad;

/**
 * Class Kategori
 *
 * @package Kyegil\Leiebasen\Modell\Søknad
 *
 * @property integer $id
 * @property Søknad $søknad
 * @property string $kategori
 *
 * @method int hentId()
 * @method Søknad hentSøknad()
 * @method string hentKategori()
 *
 * @method $this settSøknad(Søknad $søknad)
 * @method $this settKategori(string $kategori)
 */
class Kategori extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'søknad_kategorier';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Kategorisett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'søknad_id'    => [
                'target' => 'søknad',
                'type'  => Søknad::class
            ],
            'kategori'    => [
                'type'  => 'string'
            ],
        ];
    }
}