<?php

namespace Kyegil\Leiebasen\Modell\Søknad;

use DateTime;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Søknad;

/**
 * Class Notat
 *
 * @package Kyegil\Leiebasen\Modell\Søknad
 *
 * @property integer $id
 * @property Søknad|null $søknad
 * @property Person|null $forfatter
 * @property DateTime $tidspunkt
 * @property bool $systemgenerert
 * @property string $tekst
 *
 * @method int hentId()
 * @method Søknad|null hentSøknad()
 * @method Person|null hentForfatter()
 * @method DateTime hentTidspunkt()
 * @method bool hentSystemgenerert()
 * @method string hentTekst()
 *
 * @method $this settSøknad(Søknad $søknad)
 * @method $this settForfatter(Person $forfatter)
 * @method $this settTidspunkt(DateTime $tidspunkt)
 * @method $this settSystemgenerert(bool $systemgenerert)
 * @method $this settTekst(string $tekst)
 */
class Notat extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'søknad_notater';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Notatsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'søknad_id'    => [
                'target' => 'søknad',
                'type'  => Søknad::class
            ],
            'person_id'    => [
                'target' => 'forfatter',
                'type'  => Person::class
            ],
            'tidspunkt'    => [
                'type'  => 'datetime'
            ],
            'systemgenerert'    => [
                'type'  => 'boolean'
            ],
            'tekst'    => [
                'type'  => 'string'
            ],
        ];
    }
}