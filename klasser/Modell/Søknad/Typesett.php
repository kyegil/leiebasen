<?php

namespace Kyegil\Leiebasen\Modell\Søknad;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Sett;

/**
 * Class Typesett
 *
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Type current()
 */
class Typesett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Type::class;

    /**
     * @var Type[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }
}