<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 03/05/2021
 * Time: 09:56
 */

namespace Kyegil\Leiebasen\Modell;


use Kyegil\Leiebasen\Sett;

/**
 * Class Delkravtypesett
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Delkravtype current()
 */
class Delkravtypesett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Delkravtype::class;

    /**
     * @var Delkravtype[]
     */
    protected ?array $items = null;
}