<?php

namespace Kyegil\Leiebasen\Modell;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Kyegil\CoreModel\CoreModel;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Modell\Leieforhold\Andelsperiode;
use Kyegil\Leiebasen\Modell\Leieforhold\Andelsperiodesett;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Oppsigelse;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning;
use Kyegil\Leiebasen\Modell\Område\Deltakelse;
use Kyegil\Leiebasen\Modell\Område\Deltakelsesett;
use Kyegil\Leiebasen\Samling;

/**
 * Class Leieobjekt
 * @package Kyegil\Leiebasen\Modell
 *
 * @property int $id
 * @property int $leieobjektnr
 * @property Bygning|null $bygning
 * @property string $navn
 * @property string $gateadresse
 * @property string $postnr
 * @property string $poststed
 * @property string $etg
 * @property string $beskrivelse
 * @property string $merknader
 * @property string $bilde
 * @property integer|null $areal
 * @property integer|null $antRom
 * @property boolean $bad
 * @property string $toalett
 * @property boolean $boenhet
 * @property Leieberegning|null $leieberegning
 * @property integer $toalettKategori
 * @property boolean $ikkeForUtleie
 *
 * @method int hentId()
 * @method int hentLeieobjektnr()
 * @method Bygning|null hentBygning()
 * @method string hentNavn()
 * @method string hentGateadresse()
 * @method string hentPostnr()
 * @method string hentPoststed()
 * @method string hentEtg()
 * @method string hentMerknader()
 * @method string hentBilde()
 * @method integer|null hentAreal()
 * @method integer|null hentAntRom()
 * @method boolean hentBad()
 * @method string hentToalett()
 * @method boolean hentBoenhet()
 * @method Leieberegning|null hentLeieberegning()
 * @method integer hentToalettKategori()
 * @method boolean hentIkkeForUtleie()
 *
 * @method $this settLeieobjektnr(int $id)
 * @method $this settBygning(Bygning $bygning = null)
 * @method $this settNavn(string $navn)
 * @method $this settGateadresse(string $gateadresse)
 * @method $this settPostnr(string $postnr)
 * @method $this settPoststed(string $poststed)
 * @method $this settEtg(string $etg)
 * @method $this settBeskrivelse(string $beskrivelse)
 * @method $this settMerknader(string $merknader)
 * @method $this settBilde(string $bilde)
 * @method $this settAreal(integer $areal = null)
 * @method $this settAntRom(integer $antRom = null)
 * @method $this settBad(boolean $bad)
 * @method $this settToalett(string $toalett)
 * @method $this settBoenhet(boolean $boenhet)
 * @method $this settLeieberegning(Leieberegning $leieberegning = null)
 * @method $this settToalettKategori(integer $toalettKategori)
 * @method $this settIkkeForUtleie(boolean $ikkeForUtleie)
 */
class Leieobjekt extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'leieobjekter';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'leieobjektnr';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Leieobjektsett::class;

    /** @var array one-to-many
     * @noinspection PhpDocFieldTypeMismatchInspection
     */
    protected static ?array $one2Many;

    /**
     * Fordi det ikke er noen fordel å hente tidslinjer for ett og ett leieobjekt,
     * kan vi like gjerne lagre alle her, med leieobjekt-id-en som nøkkel
     *
     * @var array<string, DateTimeInterface[]>
     */
    protected static array $tidslinjer = [];

    public static function harFlere(
        string $referanse,
        ?string $modell = null,
        ?string $fremmedNøkkel = null,
        array $filtre = [],
        ?callable $callback = null
    ): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::harFlere('leieforhold',
                Leieforhold::class,
                'leieobjekt'
            );
            self::harFlere('husleier',
                Krav::class,
                'leieobjekt',
                [['`' . Krav::hentTabell() . '`.`type`' => Krav::TYPE_HUSLEIE]]
            );
            self::harFlere('andelsperioder',
                Leieforhold\Andelsperiode::class,
                'leieobjekt', [],
                function($sett){
                    $sett->leggTilSortering('fradato');
                }
            );
        }
        return parent::harFlere($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'leieobjektnr'    => [
                'type'  => 'integer'
            ],
            'bygning'    => [
                'type'  => Bygning::class,
                'allowNull' => true,
                'rawDataContainer' => Bygning::hentTabell()
            ],
            'navn'    => [
                'type'  => 'string'
            ],
            'gateadresse'    => [
                'type'  => 'string'
            ],
            'postnr'    => [
                'type'  => 'string'
            ],
            'poststed'    => [
                'type'  => 'string'
            ],
            'etg'    => [
                'type'  => function($etg) {
                    return self::lesEtg($etg);
                }
            ],
            'beskrivelse'    => [
                'type'  => 'string'
            ],
            'bilde'    => [
                'type'  => 'string'
            ],
            'areal'    => [
                'allowNull' => true
            ],
            'ant_rom'    => [
                'type'  => 'integer',
                'allowNull' => true
            ],
            'bad'    => [
                'type'  => 'boolean'
            ],
            'toalett'    => [
                'type'  => 'string'
            ],
            'toalett_kategori'    => [
                'type'  => 'integer'
            ],
            'leieberegning'    => [
                'type'  => Leieberegning::class,
                'rawDataContainer' => Leieberegning::hentTabell()
            ],
            'merknader'    => [
                'type'  => 'string'
            ],
            'boenhet'    => [
                'type'  => 'boolean'
            ],
            'ikke_for_utleie'    => [
                'type'  => 'boolean'
            ]
        ];
    }

    /**
     * @return string
     */
    public static function sqlQueryForTidslinjer(): string {
        $andelsTabell = static::$tablePrefix . Andelsperiode::hentTabell();
        return
            <<<SQL
                SELECT `$andelsTabell`.`leieobjekt`, `$andelsTabell`.`fradato` AS `dato` FROM `$andelsTabell`
                UNION DISTINCT
                SELECT `$andelsTabell`.`leieobjekt`, `$andelsTabell`.`tildato` AS `dato` FROM `$andelsTabell`
                WHERE `andelsperioder`.`tildato` IS NOT NULL
                ORDER BY `leieobjekt`, `dato`
                SQL;
    }

    /**
     * @param string $etg
     * @return string
     */
    private static function lesEtg(string $etg): string
    {
        switch ($etg) {
            case "+":
                $resultat = "loft";
                break;
            case "-":
                $resultat = "kjeller";
                break;
            case "0":
                $resultat = "sokkel";
                break;
            case "":
                $resultat = "";
                break;
            default:
                $resultat = "{$etg}. etg.";
                break;
        }
        return $resultat;
    }

    /**
     * @param string $etg
     * @return int|string
     */
    private static function skrivEtg(string $etg) {
        $etg = strtolower(trim($etg));
        switch ($etg) {
            case "loft":
                $resultat = "+";
                break;
            case "kjeller":
                $resultat = "-";
                break;
            case "sokkel":
                $resultat = "0";
                break;
            case "":
                $resultat = "";
                break;
            default:
                $resultat = intval($etg);
                break;
        }
        return $resultat;
    }

    /**
     * Hent alle leieforhold som noensinne har vært i leieobjektet
     *
     * @return Leieforholdsett
     */
    public function hentAlleLeieforhold(): Leieforholdsett {
        /** @var Leieforholdsett $leieforhold */
        $leieforhold = $this->hentSamling('leieforhold');
        return $leieforhold;
    }

    /**
     * @return Andelsperiodesett
     * @throws Exception
     */
    public function hentAndelsperioder():Andelsperiodesett
    {
        /** @var Andelsperiodesett $andelsperioder */
        $andelsperioder = $this->hentSamling('andelsperioder');
        return $andelsperioder;
    }

    /**
     * Hent alle husleiekrav i leieobjektet
     *
     * @return Kravsett
     * @throws Exception
     */
    public function hentHusleier(): Kravsett
    {
        /** @var Kravsett $husleier */
        $husleier = $this->hentSamling('husleier');
        return $husleier;
    }

    /**
     * Hent alle leieforhold i et leieobjekt
     *
     * Returnerer alle leieforhold i dette leieobjektet, som ikke var fristilt før det angitte tidsrommet.
     *
     * Dersom fra- og til-dato er angitt,
     * hentes alle leieforhold i dette tidsrommet.
     * For å returnere alle leieforhold på en spesifikk dato,
     * så må denne datoen oppgis både som til-dato og fra-dato.
     *
     * For å hente alle historiske leieforhold i et leieobjekt,
     * bruk hentAlleLeieforhold()
     * @see Leieobjekt::hentAlleLeieforhold()
     *
     * @param DateTimeInterface|null $fraDato Dersom null brukes dagens dato
     * @param DateTimeInterface|null $tilDato Dersom null brukes dagens dato
     * @return Leieforholdsett
     * @throws Exception
     */
    public function hentLeieforhold(DateTimeInterface $fraDato = null, DateTimeInterface $tilDato = null): Leieforholdsett
    {
        $fraDato = $fraDato ?? new DateTimeImmutable();
        $tilDato = $tilDato ?? new DateTimeImmutable();
        $data = [];

        /** @var Leieforholdsett $leieforholdsett */
        $leieforholdsett = $this->hentSamling('leieforhold');
        $leieforholdsett->leggTilFilter(['`leieforhold`.`fradato` <=' => $tilDato->format('Y-m-d')]);
        $leieforholdsett->leggTilFilter([
            'or' => [
                ['`oppsigelser`.`fristillelsesdato`' => null],
                ['`oppsigelser`.`fristillelsesdato` >' => $fraDato->format('Y-m-d')]
            ]
        ]);
        $leieforholdsett->låsFiltre();
        $leieforholdsett->leggTilOppsigelseModell();

        /** @var Leieforhold $leieforhold */
        foreach($leieforholdsett as $leieforhold) {
            if($leieforhold->fradato->format('Y-m-d') <= $tilDato->format('Y-m-d')) {
                if(
                    !$leieforhold->hentOppsigelse()
                    || ($leieforhold->hentOppsigelse()->fristillelsesdato > $fraDato)
                ) {
                    $data[] = $leieforhold;
                }
            }
        }
        return $leieforholdsett->lastFra($data);
    }

    /**
     * @param $egenskap
     * @param $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($egenskap, $verdi): Modell
    {
        if($egenskap == 'etg') {
            $verdi = self::skrivEtg($verdi);
        }
        return parent::sett($egenskap, $verdi);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function hentBeskrivelse(): string
    {
        return ( $this->hentNavn() ? "{$this->hentNavn()}, " : "" )
            . ( $this->hentEtg() ? "{$this->hentEtg()} " : "" )
            . ( $this->hent('beskrivelse') ? "{$this->hent('beskrivelse')} " : "" )
            . $this->hent('gateadresse');
    }

    /**
     * @return Fraction
     * @throws Exception
     */
    public function hentTypiskLeieandel(): Fraction
    {
        if(!isset($this->data->typisk_leieandel)) {
            $this->data->typisk_leieandel = new Fraction(1);
            /** @var Andelsperiode $sisteAndelsperiode */
            $sisteAndelsperiode = $this->hentAndelsperioder()->hentSiste();
            if($sisteAndelsperiode) {
                $this->data->typisk_leieandel = $sisteAndelsperiode->hentAndel();
            }
        }

        return $this->data->typisk_leieandel;
    }

    /**
     * @param DateTime|null $dato
     * @return bool
     * @throws Exception
     */
    public function erBofellesskap(DateTime $dato = null): bool
    {
        if(!$dato) {
            $dato = new DateTime();
        }
        return $this->hentLeieforhold($dato, $dato)->hentAntall() > 1;
    }

    /**
     * @param DateTime|null $dato
     * @return bool
     * @throws Exception
     */
    public function hentLeietakere(DateTime $dato = null): bool
    {
        if(!$dato) {
            $dato = new DateTime();
        }
        return $this->hentLeieforhold($dato, $dato)->hentAntall() > 1;
    }

    /**
     * @return string
     */
    public function hentType(): string
    {
        return $this->hentBoenhet() ? 'bolig' : 'lokale';
    }

    /**
     * Returnerer et array med alle datoer hvor det har vært utleieendringer i leieobjektet
     *
     * Dvs:
     * * Datoen før og når en kontrakt trer i kraft
     * * Datoen før og på fristillelsesdato for et avsluttet leieforhold
     *
     *
     * @param DateTimeInterface|null $fraBegrensning
     * @param DateTimeInterface|null $tilBegrensning
     * @param bool $returnerTilOgFraDato
     * @return DateTimeImmutable[]
     * @throws Exception
     */
    public function hentTidslinje(
        ?DateTimeInterface $fraBegrensning = null,
        ?DateTimeInterface $tilBegrensning = null,
        bool $returnerTilOgFraDato = true
    ): array
    {
        $fraBegrensning = ($fraBegrensning instanceof DateTime) ? DateTimeImmutable::createFromMutable($fraBegrensning) : $fraBegrensning;
        $tilBegrensning = ($tilBegrensning instanceof DateTime) ? DateTimeImmutable::createFromMutable($tilBegrensning) : $tilBegrensning;

        if(!isset($this->data->tidslinje)) {
            if (!isset(static::$tidslinjer[$this->hentId()])) {
                $sql = self::sqlQueryForTidslinjer();
                $statement = $this->mysqli->prepare($sql);
                $statement->execute();
                CoreModel::log($sql);
                $resultSet = $statement->get_result();
                while ($linje = $resultSet->fetch_object()) {
                    if($linje->leieobjekt) {
                        static::$tidslinjer[$linje->leieobjekt][] = new DateTimeImmutable($linje->dato);
                    }
                }
            }
            $this->data->tidslinje = static::$tidslinjer[$this->hentId()] ?? [];
            sort($this->data->tidslinje);
        }
        if($fraBegrensning || $tilBegrensning) {
            /** @var DateTimeImmutable[] $resultat */
            $resultat = [];
            /** @var DateTimeImmutable $dato */
            foreach($this->data->tidslinje as $dato) {
                if($fraBegrensning && $dato->format('Ymd') < $fraBegrensning->format('Ymd')) {
                    continue;
                }
                if($tilBegrensning && $dato->format('Ymd') > $tilBegrensning->format('Ymd')) {
                    break;
                }
                $resultat[$dato->format('Ymd')] = $dato;
            }

            if($returnerTilOgFraDato && $fraBegrensning) {
                $resultat[$fraBegrensning->format('Ymd')] = clone $fraBegrensning;
            }
            if($returnerTilOgFraDato && $tilBegrensning) {
                $resultat[$tilBegrensning->format('Ymd')] = clone $tilBegrensning;
            }
            sort($resultat);
        }
        else {
            /** @var DateTimeImmutable[] $resultat */
            $resultat = $this->data->tidslinje;
        }
        return $resultat;
    }


    /**
     * Hent utleiegrad for en bestemt dato eller for dagens dato
     *
     * @param DateTimeInterface|null $dato null for i dag
     * @param Leieforhold|null $utenom
     * @return Fraction
     * @throws Exception
     */
    public function hentUtleiegrad(?DateTimeInterface $dato = null, ?Leieforhold $utenom = null): Fraction
    {
        if(!$dato) {
            $dato = new DateTimeImmutable();
        }
        if (!property_exists($this->data, 'utleiegrad') || !isset($this->data->utleiegrad[$dato->format('Y-m-d')])) {
            $utleiegrad = new Fraction();

            $andelsperioder = $this->hentAndelsperioder();
            $andelsperioder->leggTilFilter(['`' . Andelsperiode::hentTabell() . '`.`fradato` <=' => $dato->format('Y-m-d')]);
            $andelsperioder->leggTilFilter(['or' => [
                ['`' . Andelsperiode::hentTabell() . '`.`tildato` >=' => $dato->format('Y-m-d')],
                ['`' . Andelsperiode::hentTabell() . '`.`tildato` IS NULL'],
            ]]);
            if($utenom) {
                $andelsperioder->leggTilFilter(['`' . Andelsperiode::hentTabell() . '`.`leieforhold` !=' => $utenom->hentId()]);
            }
            $andelsperioder->låsFiltre();
            /** @var Andelsperiode $andelsperiode */
            foreach($andelsperioder as $andelsperiode) {
                $utleiegrad->add($andelsperiode->hentAndel(), true);
            }
            $this->data->utleiegrad[$dato->format('Y-m-d')] = $utleiegrad;
        }
        return $this->data->utleiegrad[$dato->format('Y-m-d')];
    }

    /**
     * @param DateTimeInterface|null $dato
     * @param Leieforhold|null $utenom
     * @return Fraction
     * @throws Exception
     */
    public function hentLedighet(
        ?DateTimeInterface $dato = null,
        ?Leieforhold $utenom = null
    ): Fraction
    {
        $fullUtleie = new Fraction('1');
        $utleiegrad = $this->hentUtleiegrad($dato, $utenom);
        return $fullUtleie->subtract($utleiegrad);
    }

    /**
     * Returner max utleie for et bestemt tidsrom
     *
     * @param DateTimeInterface|null $fraDato
     * @param DateTimeInterface|null $tilDato
     * @param Leieforhold|null $utenom
     * @return Fraction
     * @throws Exception
     */
    public function hentUtleiegradForTidsrom(
        ?DateTimeInterface $fraDato = null,
        ?DateTimeInterface $tilDato = null,
        ?Leieforhold $utenom = null
    ): Fraction
    {
        $tidslinje = $this->hentTidslinje($fraDato, $tilDato, true);
        $maxUtleie = new Fraction();
        foreach ($tidslinje as $dato) {
            $utleie = $this->hentUtleiegrad($dato, $utenom);
            if($utleie->compare('>', $maxUtleie)) {
                $maxUtleie = $utleie;
            }
        }
        return $maxUtleie;
    }

    /**
     * Returner min ledighet for et bestemt tidsrom
     *
     * @param DateTimeInterface|null $fraDato
     * @param DateTimeInterface|null $tilDato
     * @param Leieforhold|null $utenom
     * @return Fraction
     * @throws Exception
     */
    public function hentLedighetForTidsrom(
        ?DateTimeInterface $fraDato = null,
        ?DateTimeInterface $tilDato = null,
        ?Leieforhold $utenom = null
    ): Fraction
    {
        $tidslinje = $this->hentTidslinje($fraDato, $tilDato, true);
        $minLedighet = new Fraction(1);
        foreach ($tidslinje as $dato) {
            $ledighet = $this->hentLedighet($dato, $utenom);
            if ($ledighet->compare('<', $minLedighet)) {
                $minLedighet = clone $ledighet;
            }
        }
        return $minLedighet;
    }

    /**
     * @param DateTimeInterface|null $dato
     * @return Leieforhold|null
     * @throws Exception
     */
    public function hentSisteLeieforhold(DateTimeInterface $dato = null): ?Leieforhold
    {
        $avsluttedeLeieforhold = $this->hentAvsluttedeLeieforhold($dato);
        /** @var Leieforhold $sisteLeieforhold */
        $sisteLeieforhold = $avsluttedeLeieforhold->hentSiste();
        return $sisteLeieforhold;
    }

    /**
     * Returnerer alle leieforhold som er avsluttet per oppgitt dato ifølge fristillelsesdato
     *
     * @param DateTimeInterface|null $dato
     * @return Leieforholdsett
     * @throws Exception
     */
    public function hentAvsluttedeLeieforhold(DateTimeInterface $dato = null): Leieforholdsett
    {
        if(!$dato) {
            $dato = new DateTimeImmutable();
        }
        /** @var Leieforholdsett $avsluttedeLeieforhold */
        $avsluttedeLeieforhold = clone $this->hentAlleLeieforhold();
        $avsluttedeLeieforhold->leggTilSortering('oppsigelser.fristillelsesdato')
            ->leggTilFilter(['oppsigelser.fristillelsesdato <=' => $dato->format('Y-m-d')]);
        return $avsluttedeLeieforhold;
    }

    /**
     * @return Deltakelsesett
     * @throws Exception
     */
    public function hentOmrådeDeltakelse(): Samling
    {
        if (!isset($this->samlinger->områdeDeltakelse)) {
            $this->samlinger->områdeDeltakelse = $this->app->hentSamling(Deltakelse::class)
                ->leggTilFilter(['medlem_type' => static::class])
                ->leggTilFilter(['medlem_id' => $this->hentId()])
                ->låsFiltre()
            ;
        }
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $this->samlinger->områdeDeltakelse;
    }

    /**
     * @return Område[]
     * @throws Exception
     */
    public function hentOmråder(): array
    {
        $områder = [];
        /** @var Deltakelse $deltakelse */
        foreach ($this->hentOmrådeDeltakelse() as $deltakelse) {
            $områder[$deltakelse->område->id] = $deltakelse->område;
        }

        /** @var Bygning $bygning */
        $bygning = $this->hentBygning();
        if ($bygning) {
            foreach ($bygning->hentOmrådeDeltakelse() as $deltakelse) {
                $områder[$deltakelse->område->id] = $deltakelse->område;
            }
        }
        return array_values($områder);
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function fjernAllOmrådeDeltakelse(): Leieobjekt
    {
        $this->hentOmrådeDeltakelse()->slettAlle();
        $this->samlinger->områdeDeltakelse = null;
        return $this;
    }

    /**
     * @param Område $område
     * @return $this
     * @throws Exception
     */
    public function leggTilOmråde(Område $område): Leieobjekt
    {
        /** @var Deltakelse $deltakelse */
        foreach($this->hentOmråder() as $eksisterendeOmråde) {
            if ($eksisterendeOmråde->hentId() == $område->hentId()
            ) {
                return $this;
            }
        }
        $this->app->nyModell(Deltakelse::class, (object)[
            'område' => $område,
            'medlem' => $this
        ]);
        $this->samlinger->områdeDeltakelse = null;
        return $this;
    }

    /**
     * Foreslår leie per år før evt delkravtyper legges til
     *
     * @param Fraction|null $andel
     * @return float
     */
    public function beregnBasisleie(Fraction $andel = null): float
    {
        if ($andel) {
            $andel = clone $andel;
        }
        else {
            $andel = new Fraction(1);
        }
        $basisleie = new Fraction();
        /** @var Leieberegning $leieberegning */
        $leieberegning = $this->hentLeieberegning();

        if($leieberegning) {
            $basisleie = $andel->multiply(
                $leieberegning->leiePerObjekt
                + $leieberegning->leiePerKvadratmeter * $this->hentAreal()
            );
            $basisleie->add($leieberegning->leiePerKontrakt, true);
            $basisleie->add($leieberegning->prosesserSpesialregler($this, $this->hentBygning(), $basisleie->asDecimal()), true);
        }
        return round($basisleie->asDecimal());
    }

    /**
     * Henter alle leiekrav for leieforhold som er i oppsigelsestid i et bestemt tidsrom,
     * og som dermed skal slettes dersom leieobjektet blir leid ut på nytt i dette tidsrommet.
     *
     * @param DateTimeInterface $fraDato
     * @param DateTimeInterface $tilDato
     * @return Kravsett
     * @throws Exception
     */
    public function hentLeiekravIOppsigelsestid(DateTimeInterface $fraDato, DateTimeInterface $tilDato): Kravsett
    {
        /** @var Kravsett $kravsett */
        $kravsett = $this->app->hentSamling(Krav::class)
            ->leggTilInnerJoin(Oppsigelse::hentTabell(), '`' . Krav::hentTabell(). '`.`leieforhold` = ' . '`' . Oppsigelse::hentTabell(). '`.`leieforhold`')
            ->leggTilLeftJoin(Delbeløp::hentTabell(), '`' . Krav::hentTabell(). '`.`' . Krav::hentPrimærnøkkelfelt() . '` = ' . '`' . Delbeløp::hentTabell(). '`.`krav`')
            ->leggTilEavFelt('annullering')
            ->leggTilUttrykk('krav_ekstra', 'har_betaling', 'IF(`' . Delbeløp::hentTabell() . '`.`krav` IS NULL, 0, 1)')
            ->leggTilModell(Oppsigelse::class, null, null, ['oppsigelsestid_slutt'])

            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`type`' => Krav::TYPE_HUSLEIE])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`leieobjekt`' => $this->hentId()])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`fom` <=' => $tilDato->format('Y-m-d')])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`tom` >=' => $fraDato->format('Y-m-d')])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`beløp` >=' => 0])
            ->leggTilFilter(['`' . Oppsigelse::hentTabell() . '`.`fristillelsesdato` <= `' . Krav::hentTabell() . '`.`fom`'])
            ->leggTilFilter(['`' . Oppsigelse::hentTabell() . '`.`oppsigelsestid_slutt` > `' . Krav::hentTabell() . '`.`tom`'])
            ->leggTilFilter(['`annullering`.`verdi` IS NULL'])
            ->låsFiltre()

            ->leggTilSortering('leieforhold')
            ->leggTilSortering('fom')
            ->leggTilSortering('oppsigelsestid_slutt', false, Oppsigelse::hentTabell())
            ->leggTilSortering('har_betaling', false, 'krav_ekstra')
        ;
        return $kravsett;
    }

    /**
     * @param int $rute
     * @return int
     * @throws Exception
     */
    public function hentUtskriftsposisjon(int $rute = 0): int
    {
        $rute = $rute ?: intval($this->app->hentValg('utdelingsrute'));

        if (!isset($this->data->utskriftsposisjon[$rute])) {
            $posisjon = $this->mysqli->select([
                'source' => static::$tablePrefix . 'utdelingsorden AS utdelingsorden',
                'where' => [
                    ['rute' => $rute],
                    ['leieobjekt' => $this->hentId()]
                ]
            ]);
            $this->data->utskriftsposisjon[$rute] = isset($posisjon->data[0]) ? $posisjon->data[0]->plassering : 0;
        }
        return $this->data->utskriftsposisjon[$rute];
    }

    /**
     * Slett overflødig oppsigelsestid
     *
     * Sletter leie i andre leieforhold sin oppsigelsestid,
     * og returnerer beskrivelse av leiene som er sletta
     *
     * @param Kravsett $leieKravsett Leia i det nye leieforholdet som det må ryddes plass for
     * @return string[] Beskrivelse av kravene som er sletta
     * @throws Exception
     */
    public function slettOverflødigOppsigelsestid(Kravsett $leieKravsett): array
    {
        $resultat = [];
        $this->app->logger->debug('Eventuell sletting av overflødig oppsigelsestid for å gi plass til ny leie');
        foreach ($leieKravsett as $husleie) {
            if ($husleie->type == Krav::TYPE_HUSLEIE
                && $husleie->leieobjekt->hentId() == $this->hentId()
            ) {
                $plassbehov = $husleie->andel ?? new Fraction();
                $forrigeLeiegrad = new Fraction();

                /**
                 * Dersom leiekravene i andre leieforhold sin oppsigelsestid overstiger det som er ledig utenom de nye leiekravene,
                 * så slettes ett og ett av disse inntil total utleiegrad ikke lenger overtiger 100%.
                 * Prosessen avbrytes dersom utleiegraden forblir uendret, for å unngå å fortsette i det uendelige.
                 */
                while (
                    ($leiekravIOppsigelsestid = $this->hentLeiekravIOppsigelsestid($husleie->fom, $husleie->tom))->hentAntall()
                    && $plassbehov->add($leiegrad = $leiekravIOppsigelsestid->hentSummerteAndeler())->compare('>', 1)
                    && $leiegrad->compare('!=', $forrigeLeiegrad)
                ) {
                    $this->app->logger->info('Forsøker å gi plass til ny husleie ved å slette overflødig oppsigelsestid', [
                        'leieobjekt' => $this->hentId(),
                        'fom' => $husleie->fom->format('Y-m-d'),
                        'tom' => $husleie->tom->format('Y-m-d'),
                    ]);
                    /**
                     * Gå gjennom de enkelt kravene for å finne det første som ikke er kreditt
                     * og som ikke allerede er slettet
                     *
                     * @var Krav $kravForSletting
                     */
                    foreach ($leiekravIOppsigelsestid as $kravForSletting) {
                        /* Hopp over kreditt og alt som allerede er slettet */
                        if($kravForSletting->erKreditt() || $kravForSletting->erAnnullert()) {
                            $this->app->logger->debug('Hopper over krav', [
                                'krav' => $kravForSletting->hentId(),
                                'leieforhold' => $kravForSletting->leieforhold->hentId(),
                                'tekst' => $kravForSletting->hentTekst(),
                            ]);
                            continue;
                        }

                        $resultat[] = $kravForSletting->leieforhold->hentNavn() . ' sin leie for ' . $kravForSletting->termin;
                        $this->app->logger->info('Sletter husleie', [
                            'krav' => $kravForSletting->hentId(),
                            'leieforhold' => $kravForSletting->leieforhold->hentId(),
                            'tekst' => $kravForSletting->hentTekst(),
                        ]);
                        $kravForSletting->slett();
                        $forrigeLeiegrad = $leiegrad;
                        break;
                    }
                }
            }
        }
        $this->app->logger->debug('Eventuell sletting av oppsigelsestid fullført', [
            'resultat' => $resultat,
        ]);
        return $resultat;
    }

    /**
     * Sjekker om leieobjektet er opptatt i overskuelig framtid
     *
     * Et leieobjekt er opptatt i overskuelig framtid dersom det er fullt utleid,
     * og det ikke foreligger noen oppsigelser på noen av leieforholdene
     *
     * Et leieobjekt som er overskuelig opptatt kan ikke leies ut
     *
     * @return bool
     */
    public function hentErOverskueligOpptatt(): bool
    {
        if(!isset($this->data->er_overskuelig_opptatt)){
            if(isset($this->rawData->leieobjekt_ekstra->er_overskuelig_opptatt)) {
                $this->data->er_overskuelig_opptatt = boolval($this->rawData->leieobjekt_ekstra->er_overskuelig_opptatt);
            }
            else {
                $overskueligeLeieforhold = $this->hentAlleLeieforhold()
                    ->leggTilLeftJoinForOppsigelse()
                    ->leggTilFilter(['`' . Oppsigelse::hentTabell() . '`.`' . Oppsigelse::hentPrimærnøkkelfelt() . '` IS NULL'])
                ->låsFiltre();
                $utleiegrad = new Fraction();
                foreach ($overskueligeLeieforhold as $leieforhold) {
                    $utleiegrad->add($leieforhold->hentAndel(), true);
                }
                $this->data->er_overskuelig_opptatt = $utleiegrad->compare('>=', 1);
            }
        }
        return $this->data->er_overskuelig_opptatt;
    }

    /**
     * Hent leieobjektets første ledige dato
     *
     * Hvis leieobjektet ikke vil bli ledig i overskuelig framtid, returneres null.
     * Hvis leieobjektet aldri har vært utleid, returneres datoen 0000-00-00.
     *
     * Dersom $andel er angitt returneres første ledige dato som vil gi rom for denne andelen.
     *
     * @param Fraction|null $andel
     * @return DateTimeInterface|null
     * @throws Exception
     */
    public function hentFørsteLedigeDato(?Fraction $andel = null): ?DateTimeInterface
    {
        if(!empty($this->data->er_overskuelig_opptatt)) {
            return null;
        }
        $tidslinje = $this->hentTidslinje();
        rsort($tidslinje);
        $forrigeDato = null;
        foreach ($tidslinje as $dato) {
            if($this->hentLedighet($dato)->compare('<=', $andel ?? 0)) {
                return $forrigeDato;
            }
            $forrigeDato = $dato;
        }
        return new DateTimeImmutable('0000-00-00 00:00:00');
    }

    /**
     * @param DateTimeInterface|null $dato
     * @return Andelsperiode|null
     * @throws Exception
     */
    public function hentSisteAvsluttedeAndelsperiode(?DateTimeInterface $dato): ?Andelsperiode
    {
        $dato = $dato ?? new DateTimeImmutable();
        $andelsperioder = $this->hentAndelsperioder();
        $andelsperioder->leggTilFilter(['`' . Andelsperiode::hentTabell() . '`.`tildato` <' => $dato->format('Y-m-d')]);
        $andelsperioder->låsFiltre();
        $andelsperioder->leggTilSortering('tildato', true)->begrens(1);
        /** @var Andelsperiode|null $sisteAvsluttedeAndelsperiode */
        $sisteAvsluttedeAndelsperiode = $andelsperioder->hentFørste();
        return $sisteAvsluttedeAndelsperiode;
    }
}