<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 12/10/2020
 * Time: 17:52
 */

namespace Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\felles\epost\beboerstøtte\sakstråd\Innlegg as InnleggsEpostVisning;

/**
 * Class Innlegg
 * @package Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd
 *
 * @property int $id
 * @property Sakstråd $sak
 * @property Person $avsender
 * @property string $innhold
 * @property DateTime $tidspunkt
 * @property string $vedlegg
 * @property boolean $saksopplysning
 * @property boolean $privat
 *
 * @method int hentId()
 * @method Sakstråd hentSak()
 * @method Person hentAvsender()
 * @method string hentInnhold()
 * @method DateTime hentTidspunkt()
 * @method string hentVedlegg()
 * @method boolean hentSaksopplysning()
 * @method boolean hentPrivat()
 *
 * @method $this settSak(Sakstråd $sak)
 * @method $this settAvsender(Person $avsender)
 * @method $this settInnhold(string $innhold)
 * @method $this settTidspunkt(DateTime $tidspunkt)
 * @method $this settVedlegg(string $vedlegg)
 * @method $this settSaksopplysning(boolean $saksopplysning)
 * @method $this settPrivat(boolean $privat)
 */
class Innlegg extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'beboerstøtte_innlegg';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /**
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'sak_id'    => [
                'type'  => Sakstråd::class,
                'target' => 'sak'
            ],
            'avsender_id'    => [
                'type'  => Person::class,
                'target' => 'avsender'
            ],
            'innhold'    => [
                'type'  => 'string'
            ],
            'tidspunkt'    => [
                'type'  => 'datetime',
            ],
            'vedlegg'    => [
                'type'  => 'string'
            ],
            'saksopplysning'    => [
                'type'  => 'boolean'
            ],
            'privat'    => [
                'type'  => 'boolean'
            ]
        ];
    }

    /**
     * @throws Exception
     */
    public function sendEpost()
    {
        if(!$this->hentId()) {
            throw new Exception('Innlegget er ikke lagret');
        }
        /** @var Sakstråd $sak */
        $sak = $this->hentSak();

        $saksabonnenter = $sak->hentAbonnenter();
        if($this->privat) {
            $saksabonnenter = $sak->hentAdministratorer();
        }
        if($sak->hentPrivatPerson()) {
            $saksabonnenter = array_unique(array_merge($saksabonnenter->hentElementer(), [$this->sak->privatPerson]));
        }
        $emnefelt = "[{$sak->hentSaksref()}] {$sak->hentTittel()}";
        /** @var Person $abonnent */
        foreach($saksabonnenter as $abonnent) {
            /** @var InnleggsEpostVisning $innhold */
            $innhold = $this->app->vis(InnleggsEpostVisning::class, [
                'mottaker' => $abonnent,
                'sakstråd' => $sak,
                'innlegg' => $this
            ]);
            try {
                $this->app->sendMail((object)[
                    'to' => $abonnent,
                    'from' => $this->hentAvsender(),
                    'reply' => "{$this->app->hentValg('utleier')} <{$this->app->hentValg('beboerstøtte_epost')}>",
                    'subject' => $emnefelt,
                    'html' => $innhold,
                    'priority' => 100,
                    'type' => 'beboerstøtte_sakstråd_innlegg'
                ]);
            } catch (Exception $e) {
                continue;
            }
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function erAvsenderAdmin()
    {
        $avsender = $this->hentAvsender();
        $område = $this->hentSak()->hentOmråde();
        return $avsender->harAdgangTil($område);
    }
}