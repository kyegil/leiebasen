<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 12/10/2020
 * Time: 17:52
 */

namespace Kyegil\Leiebasen\Modell\Beboerstøtte;


use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd\Innlegg;
use Kyegil\Leiebasen\Modell\Leieobjekt\Skade;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Samling;
use Kyegil\Leiebasen\Visning\felles\epost\beboerstøtte\Sakstråd as SakstrådEpostVisning;
use stdClass;

/**
 * Class Sakstråd
 * @package Kyegil\Leiebasen\Modell\Beboerstøtte
 *
 * @property int $id
 * @property string $saksref
 * @property string $tittel
 * @property Skade $skademelding
 * @property string $status
 * @property string $område
 * @property Person|null $privatPerson
 *
 * @method int hentId()
 * @method string hentSaksref()
 * @method string hentTittel()
 * @method Skade hentSkademelding()
 * @method string hentStatus()
 * @method string hentOmråde()
 * @method Person|null hentPrivatPerson()
 *
 * @method $this settSaksref(string $saksref)
 * @method $this settTittel(string $tittel)
 * @method $this settSkademelding(Skade $skademelding)
 * @method $this settStatus(string $status)
 * @method $this settOmråde(string $område)
 * @method $this settPrivatPerson(Person|null $privatPerson)
 */
class Sakstråd extends Modell
{
    const STATUS_ÅPEN = 'åpen';

    const STATUS_AVSLUTTET = 'avsluttet';

    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'beboerstøtte';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var array|null one-to-many */
    protected static ?array $one2Many;

    public static function harFlere(
        string $referanse,
        ?string $modell = null,
        ?string $fremmedNøkkel = null,
        array $filtre = [],
        ?callable $callback = null
    ): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::harFlere('innlegg',
                Innlegg::class,
                'sak_id'
            );
        }
        return parent::harFlere($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'saksref'    => [
                'type'  => 'string'
            ],
            'tittel'    => [
                'type'  => 'string'
            ],
            'skademelding_id'    => [
                'type'  => Skade::class,
                'target' => 'skademelding',
                'allowNull' => true
            ],
            'status'    => [
                'type'  => 'string'
            ],
            'område'    => [
                'type'  => 'string'
            ],
            'privat_person_id'    => [
                'type'  => Person::class,
                'allowNull' => true,
                'target' => 'privat_person',
            ]
        ];
    }

    /**
     * @param string|null $egenskap
     * @return Samling|mixed
     * @throws Exception
     */
    public function hent($egenskap = null)
    {
        if($egenskap == 'abonnenter') {
            return $this->hentAbonnenter();
        }
        if($egenskap == 'innlegg') {
            return $this->hentInnlegg();
        }
        if($egenskap == 'saksopplysninger') {
            return $this->hentSaksopplysninger();
        }
        if($egenskap == 'administratorer') {
            return $this->hentAdministratorer();
        }
        return parent::hent($egenskap);
    }

    /**
     * @return \Kyegil\Leiebasen\Samling
     * @throws Exception
     */
    public function hentAbonnenter(): Samling
    {
        if(!isset($this->samlinger->abonnenter)) {
            $this->samlinger->abonnenter = $this->app->hentSamling(Person::class)
                ->leggTilInnerJoin('beboerstøtte_abonnent', 'beboerstøtte_abonnent.bruker_id = ' . Person::hentTabell() . '.' . Person::hentPrimærnøkkelfelt())
                ->leggTilFilter(['beboerstøtte_abonnent.sak_id' => $this->hentId()])
                ->låsFiltre()
            ;
        }
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $this->samlinger->abonnenter;
    }

    /**
     * @return \Kyegil\Leiebasen\Samling
     * @throws Exception
     */
    public function hentInnlegg(): Samling
    {
        /** @var Samling $innlegg */
        $innlegg = $this->hentSamling('innlegg');
        return $innlegg;
    }

    /**
     * @return Samling
     * @throws Exception
     */
    public function hentSaksopplysninger() {
        if(!isset($this->samlinger->saksopplysninger)) {
            $saksopplysninger = [];
            /** @var Innlegg $innlegg */
            foreach($this->hentInnlegg() as $innlegg) {
                if($innlegg->saksopplysning) {
                    $saksopplysninger[] = $innlegg;
                }
            }
            /** @var Samling saksopplysninger */
            $this->samlinger->saksopplysninger = $this->app->hentSamling(Innlegg::class)
                ->lastFra($saksopplysninger);
        }
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $this->samlinger->saksopplysninger;
    }

    /**
     * @return Samling
     * @throws Exception
     */
    public function hentAdministratorer()
    {
        if(!isset($this->samlinger->administratorer)) {
            $administratorer
                = ($this->app->hentValg('beboerstøtte_skademeldinger_admin')
                ? $this->app->hentValg('beboerstøtte_skademeldinger_admin')
                : 'null');
            $this->samlinger->administratorer = $this->app->hentSamling(Person::class)
                ->leggTilFilter([Person::hentPrimærnøkkelfelt() . " IN({$administratorer})"])
                ->låsFiltre();
        }
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $this->samlinger->administratorer;
    }

    /**
     * @param Person|int $person
     * @return bool
     * @throws Exception
     */
    public function erAdministrator($person = null): bool
    {
        settype($person, 'string');
        /** @var Person $administrator */
        foreach($this->hentAdministratorer() as $administrator) {
            if ($administrator->hentId() == $person) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param Person|integer $personid
     * @return bool
     * @throws Exception
     */
    public function abonnerer($personid): bool {
        $personid = intval(strval($personid));
        /** @var Person $abonnent */
        foreach($this->hentAbonnenter() as $abonnent) {
            if(strval($personid) == $abonnent->hentId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param Person|integer $personid
     * @return $this
     * @throws Exception
     */
    public function abonner($personid)
    {
        $sakId = $this->hentId();
        if($this->abonnerer($personid)) {
            return $this;
        }
        $personid = intval(strval($personid));
        $tp = $this->mysqli->table_prefix;
        $this->mysqli->saveToDb([
            'table' => "{$tp}beboerstøtte_abonnent",
            'insert' => true,
            'fields' => [
                'sak_id' => $sakId,
                'bruker_id' => $personid
            ]
        ]);
        unset($this->samlinger->abonnenter);
        return $this;
    }

    /**
     * @param Person|integer $personid
     * @return $this
     * @throws Exception
     */
    public function stoppAbonnement($personid)
    {
        $sakId = (int)$this->hentId();
        $personid = intval(strval($personid));
        $tp = $this->mysqli->table_prefix;
        $this->mysqli->query("DELETE FROM {$tp}beboerstøtte_abonnent WHERE `sak_id` = {$sakId} AND `bruker_id` = {$personid}");
        unset($this->samlinger->abonnenter);
        return $this;
    }

    /**
     * @param stdClass $param
     * @return Innlegg
     * @throws Exception
     */
    public function leggTilInnlegg(stdClass $param)
    {
        if(!$this->hentId()) {
            throw new Exception('Kan ikke legge til innlegg til ikke-eksisterende tråd');
        }
        $param->sak = $this;
        /** @var Innlegg $innlegg */
        $innlegg = $this->app->nyModell(Innlegg::class, $param);

        unset($this->samlinger->innlegg);
        return $innlegg;
    }

    /**
     * @return void
     * @throws Exception
     */
    public function slett()
    {
        $tp = $this->mysqli->table_prefix;

        $sql = "DELETE FROM
            {$tp}beboerstøtte_abonnent
            WHERE {$tp}sak_id = '" . (int)$this->hentId() . "'";
        $this->mysqli->query( $sql );

        /** @var Innlegg $innlegg */
        foreach($this->hentInnlegg() as $innlegg) {
            $innlegg->slett();
        }
        parent::slett();
    }

    /**
     * @param stdClass $params
     * @return $this
     * @throws Exception
     */
    public function opprett(stdClass $params): Modell
    {
        parent::opprett($params);
        /** @var Person $administrator */
        foreach ($this->hentAdministratorer() as $administrator) {
            $this->abonner($administrator);
        }
        return $this;
    }

    /**
     * @return Innlegg|null
     * @throws Exception
     */
    public function hentFørsteInnlegg(): ?Innlegg
    {
        if(!property_exists($this->data, 'førsteInnlegg')) {
            /** @var Innlegg|null $innlegg */
            $innlegg = $this->hentInnlegg()->hentFørste();
            $this->data->førsteInnlegg = $innlegg;
        }
        return $this->data->førsteInnlegg;
    }

    /**
     * @throws Exception
     */
    public function sendEpost()
    {
        if(!$this->hentId()) {
            throw new Exception('Sakstråden er ikke lagret');
        }
        /** @var Innlegg $innlegg */
        $innlegg = $this->hentFørsteInnlegg();

        /** @var Samling $saksabonnenter */
        $saksabonnenter = $this->hentAbonnenter();

        if($this->hentPrivatPerson()) {
            $saksabonnenter = array_unique(array_merge($saksabonnenter->hentElementer(), [$this->hentPrivatPerson()]));
        }
        $emnefelt = "[{$this->hentSaksref()}] {$this->hentTittel()}";
        /** @var SakstrådEpostVisning $innhold */
        foreach($saksabonnenter as $abonnent) {
            $innhold = $this->app->vis(SakstrådEpostVisning::class, [
                'mottaker' => $abonnent,
                'sakstråd' => $this,
            ]);
            try {
                $this->app->sendMail((object)[
                    'to' => $abonnent,
                    'from' => $innlegg->hentAvsender(),
                    'reply' => "{$this->app->hentValg('utleier')} <{$this->app->hentValg('beboerstøtte_epost')}>",
                    'subject' => $emnefelt,
                    'html' => $innhold,
                    'priority' => 100,
                    'type' => 'beboerstøtte_sakstråd'
                ]);
            } catch (Exception $e) {
                continue;
            }
        }
    }
}