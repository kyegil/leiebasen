<?php

namespace Kyegil\Leiebasen\Modell;


use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Område\Deltakelse;
use Kyegil\Leiebasen\Modell\Område\Deltakelsesett;

/**
 * Class Område
 * @package Kyegil\Leiebasen\Modell
 *
 * @property int $id
 * @property string $navn
 * @property string $beskrivelse
 *
 * @method int hentId()
 * @method string hentNavn()
 * @method string hentBeskrivelse()
 *
 * @method $this settNavn(string $navn)
 * @method $this settBeskrivelse(string $beskrivelse)
*/
class Område extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'områder';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Områdesett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    public static function harFlere(string $referanse, ?string $modell = null, ?string $fremmedNøkkel = null, array $filtre = [], ?callable $callback = null): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::harFlere('deltakelse',
                Deltakelse::class,
                'område_id'
            );
        }
        return parent::harFlere($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'navn'    => [
                'type'  => 'string'
            ],
            'beskrivelse'    => [
                'type'  => 'string'
            ]
        ];
    }

    /**
     * @return Deltakelsesett
     * @throws Exception
     */
    public function hentDeltakelse(): Deltakelsesett
    {
        /** @var Deltakelsesett $deltakelse */
        $deltakelse = $this->hentSamling('deltakelse');
        return $deltakelse;
    }

    /**
     * @return Bygning[]
     * @throws Exception
     */
    public function hentBygninger(): array
    {
        $bygninger = [];
        /** @var Deltakelse $deltakelse */
        foreach ($this->hentDeltakelse() as $deltakelse) {
            if ($deltakelse->medlemType == Bygning::class) {
                $bygninger[] = $deltakelse->medlem;
            }
        }
        return $bygninger;
    }

    /**
     * @return Leieobjekt[]
     * @throws Exception
     */
    public function hentLeieobjekter(): array
    {
        $leieobjekter = [];
        /** @var Deltakelse $deltakelse */
        foreach ($this->hentDeltakelse() as $deltakelse) {
            if ($deltakelse->medlemType == Leieobjekt::class) {
                $leieobjekter[] = $deltakelse->medlem;
            }
        }
        return $leieobjekter;
    }

    /**
     * @param $deltakere
     * @return $this
     * @throws Exception
     */
    public function settDeltakere($deltakere): Område
    {
        $this->fjernAllDeltakelse();
        // Legg til bygninger først, deretter Leieobjekter
        foreach ($deltakere as $deltaker) {
            if ($deltaker instanceof Bygning) {
                $this->leggTilBygning($deltaker);
            }
        }
        foreach ($deltakere as $deltaker) {
            if ($deltaker instanceof Leieobjekt) {
                $this->leggTilLeieobjekt($deltaker);
            }
        }
        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function fjernAllDeltakelse(): Område
    {
        $this->hentDeltakelse()->slettAlle();
        $this->samlinger->deltakelse = null;
        return $this;
    }

    /**
     * @param Bygning $bygning
     * @return $this
     * @throws Exception
     */
    public function leggTilBygning(Bygning $bygning): Område
    {
        /** @var Deltakelse $deltakelse */
        foreach($this->hentDeltakelse() as $deltakelse) {
            if ($deltakelse->medlemType == Bygning::class
                && $deltakelse->medlem->hentId() == $bygning->hentId()
            ) {
                return $this;
            }
        }
        $this->app->nyModell(Deltakelse::class, (object)[
            'område' => $this,
            'medlem' => $bygning
        ]);
        $this->samlinger->deltakelse = null;
        return $this;
    }

    /**
     * @param Leieobjekt $leieobjekt
     * @return $this
     * @throws Exception
     */
    public function leggTilLeieobjekt(Leieobjekt $leieobjekt): Område
    {
        $bygning = $leieobjekt->bygning;
        /** @var Deltakelse $deltakelse */
        foreach($this->hentDeltakelse() as $deltakelse) {
            if (
                ($deltakelse->medlemType == Bygning::class && $deltakelse->medlem->hentId() == $bygning->hentId())
            || ($deltakelse->medlemType == Leieobjekt::class && $deltakelse->medlem->hentId() == $leieobjekt->hentId())
            ) {
                return $this;
            }
        }
        $this->app->nyModell(Deltakelse::class, (object)[
            'område' => $this,
            'medlem' => $leieobjekt
        ]);
        $this->samlinger->deltakelse = null;
        return $this;
    }

    /**
     * @return void
     * @throws Exception
     */
    public function slett()
    {
        $this->fjernAllDeltakelse();
        parent::slett();
    }
}