<?php

namespace Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;


use Exception;
use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andelsett;
use Kyegil\Leiebasen\Sett;

/**
 * Class Kostnadsett
 * @package Kyegil\Leiebasen\Modell\Fellesstrøm\Tjeneste
 *
 * @method Kostnad current()
 */
class Kostnadsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Kostnad::class;

    /**
     * @var Kostnad[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }

    /**
     * @return Kostnadsett
     */
    public function leggTilLeftJoinForTjeneste(): Kostnadsett
    {
        return $this
            ->leggTilLeftJoin(
                Tjeneste::hentTabell(),
                '`' . Kostnad::hentTabell() . '`.`tjeneste_id` = `'
                . Tjeneste::hentTabell() . '`.`' . Tjeneste::hentPrimærnøkkelfelt() . '`'
            );
    }

    /**
     * @param array|null $felter
     * @return Kostnadsett
     */
    public function leggTilTjenesteModell(?array $felter = null): Kostnadsett
    {
        return $this->leggTilLeftJoinForTjeneste()->leggTilModell(Tjeneste::class, null, null, $felter);
    }

    /**
     * @return Andelsett
     * @throws Exception
     */
    public function inkluderAndeler(): Andelsett
    {
        /** @var Andelsett $andeler */
        $andeler = $this->inkluder('andeler');
        return $andeler;
    }
}