<?php

namespace Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;


use DateInterval;
use DateTime;
use DateTimeImmutable;
use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andel;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andelsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Visning\felles\epost\kostnadsdeling\fordelingsforslag\Html;
use Kyegil\Leiebasen\Visning\felles\epost\kostnadsdeling\fordelingsforslag\Txt;
use Kyegil\Leiebasen\Visning\felles\epost\shared\Epost;

/**
 * Class Kostnad
 * @package Kyegil\Leiebasen\Modell\Fellesstrøm\Tjeneste
 *
 * @property string $id
 * @property string $fakturanummer
 * @property float $fakturabeløp
 * @property Tjeneste|null $tjeneste
 * @property DateTime|null $fradato
 * @property DateTime|null $tildato
 * @property string $termin
 * @property float $forbruk
 * @property string|null $forbruksenhet
 * @property boolean $beregnet
 * @property boolean $fordelt
 * @property string $lagtInnAv
 *
 * @method string hentFakturanummer()
 * @method string hentFakturabeløp()
 * @method Tjeneste|null hentTjeneste()
 * @method DateTime|null hentFradato()
 * @method DateTime|null hentTildato()
 * @method string hentTermin()
 * @method float hentForbruk()
 * @method string|null hentForbruksenhet()
 * @method boolean hentBeregnet()
 * @method boolean hentFordelt()
 * @method string hentLagtInnAv()
 *
 * @method $this settFakturanummer(string $fakturanummer)
 * @method $this settFakturabeløp(float $fakturabeløp)
 * @method $this settTjeneste(Tjeneste|null $anlegg)
 * @method $this settFradato(DateTime $fradato)
 * @method $this settTildato(DateTime $tildato)
 * @method $this settTermin(string $termin)
 * @method $this settForbruk(float $forbruk)
 * @method $this settForbruksenhet(string|null $forbruksenhet)
 * @method $this settBeregnet(boolean $beregnet)
 * @method $this settFordelt(boolean $fordelt)
 * @method $this settLagtInnAv(string $lagtInnAv)
 *
 */
class Kostnad extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'kostnadsdeling_kostnader';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Kostnadsett::class;

    /** @var array|null one-to-many */
    protected static ?array $one2Many;

    public static function harFlere(string $reference, ?string $model = null, ?string $foreignKey = null, array $filters = [], ?callable $callback = null): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::harFlere('andeler',
                Andel::class,
                'faktura_id'
            );
        }
        return parent::harFlere($reference, $model, $foreignKey, $filters, $callback);
    }

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'int'
            ],
            'fakturanummer' => [
                'type'  => 'string'
            ],
            'fakturabeløp'    => [
                'type'  => 'string'
            ],
            'tjeneste_id'    => [
                'target'  => 'tjeneste',
                'type'  => Tjeneste::class,
                'allowNull' => true
            ],
            'fradato'    => [
                'type'  => 'date',
                'allowNull' => true
            ],
            'tildato'    => [
                'type'  => 'date',
                'allowNull' => true
            ],
            'termin'    => [
                'type'  => 'string'
            ],
            'forbruk'    => [
                'type'  => 'string',
            ],
            'forbruksenhet'    => [
                'type'  => 'string',
                'allowNull' => true
            ],
            'beregnet'    => [
                'type'  => 'boolean'
            ],
            'fordelt'    => [
                'type'  => 'boolean'
            ],
            'lagt_inn_av'    => [
                'type'  => 'string'
            ]
        ];
    }

    /**
     * @return float|null
     */
    public function hentDagligForbruk(): ?float
    {
        if(!property_exists($this->data, 'daglig_forbruk')) {
            $this->data->daglig_forbruk = null;
            if($this->hentForbruk() && $this->hentFradato() && $this->hentTildato()) {
                /** @var DateInterval $intervall */
                $intervall = $this->hentFradato()->diff($this->hentTildato());
                $antallDager = $intervall->days + 1;
                $this->data->daglig_forbruk = $this->data->forbruk / $antallDager;
            }
        }
        return $this->data->daglig_forbruk ? round($this->data->daglig_forbruk, 3) : null;
    }

    /**
     * @return Andelsett
     * @throws Exception
     */
    public function hentAndeler(): Andelsett
    {
        /** @var Andelsett $andeler */
        $andeler = $this->hentSamling('andeler');

        if(!isset($this->samlinger->andeler)) {
            $this->samlinger->andeler = $andeler->hentElementer();
        }
        $andeler->lastFra($this->samlinger->andeler);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $andeler;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function forkastFordelingsforslag(): Kostnad
    {
        if (!$this->hentFordelt()) {
            $this->hentAndeler()->slettHver();
            unset($this->samlinger->andeler);
        }
        return $this;
    }

    /**
     * @return DateTimeImmutable|null
     * @throws Exception
     */
    public function hentFordelingVarselSendt():?DateTimeImmutable
    {
        /** @var DateTime|DateTimeImmutable|null $varslet */
        $varslet = $this->hentAndeler()->hentFørste()
            ? $this->hentAndeler()->hentFørste()->epostvarsel
            : null;
        return $varslet instanceof DateTime ? DateTimeImmutable::createFromMutable($varslet) : $varslet;
    }

    /**
     * @param bool $medMakt
     * @return $this
     * @throws Exception
     */
    public function varsleFordeling(bool $medMakt = false): Kostnad
    {
        $tittel = 'Forslag til fordeling av kostnad';

        /** @var Epost $html */
        $html = $this->app->hentVisning(Epost::class, [
            'tittel' => $tittel,
            'innhold' => $this->app->hentVisning(Html::class, ['regning' => $this])
        ]);
        /** @var Epost $tekst */
        $tekst = $this->app->hentVisning(Epost::class, [
            'tittel' => $tittel,
            'innhold' => $this->app->hentVisning(Txt::class, ['regning' => $this]),
            'bunntekst' => html_entity_decode(Epost::br2crnl($this->app->hentValg('eposttekst'))),
        ])->settMal('felles/epost/shared/Epost.txt');

        foreach($this->hentAndeler() as $andel) {
            if(!$andel->epostvarsel || $medMakt) {
                $leieforhold = $andel->leieforhold;
                $brukerepost = $leieforhold ? $leieforhold->hentEpost('forfallsvarsel') : null;

                if ($brukerepost) {
                    $this->app->sendMail((object)[
                        'to' 		=> implode(',', $brukerepost),
                        'subject'	=> $tittel,
                        'html'		=> $html,
                        'text'		=> $tekst,
                        'type' => 'kostnadsdeling_varsel'
                    ]);
                }
                $andel->epostvarsel = new DateTimeImmutable();
            }
        }
        return $this;
    }

    /**
     * Foreslått fordeling av en delt kostnad bekreftes.
     * Andelene kreves inn, og kostnaden låses
     *
     * @return $this
     * @throws Exception
     */
    public function bekreftFordeling(): Kostnad
    {
        if($this->hentFordelt()) {
            return $this;
        }
        $tjeneste = $this->hentTjeneste();
        if (!$tjeneste) {
            throw new Exception('Kostnaden er ikke knyttet til en tjeneste');
        }
        $nyeKravIder = [];
        foreach($this->hentAndeler() as $andel) {
            try {
                $leieforhold = $andel->leieforhold;
                if ($leieforhold) {
                    /*
                     * Dersom fellesstrøm betales via et fast tillegg til husleia,
                     * så skal ikke fellesstrøm kreves inn her
                     */
                    $fellesstrømTillegg = $leieforhold->hentDelkravtypeMedId(
                        $this->app->hentValg('delkravtype_fellesstrøm')
                    );
                    if (
                        $tjeneste->hentType() == Krav::TYPE_STRØM
                        && $fellesstrømTillegg) {
                        continue;
                    }

                    if ($andel->beløp != 0) {
                        $termin = $this->hentTermin();
                        if (!$termin && $andel->hentFom() && $andel->hentTom()) {
                            $termin = Krav::beskrivTerminen($andel->hentFom(), $andel->hentTom());
                        }
                        $krav = $leieforhold->opprettKravOgDelkrav(
                            $tjeneste->hentType(),
                            $andel->hentTekst(),
                            $andel->hentBeløp(),
                            null,
                            null,
                            null,
                            $termin,
                            $andel->hentFom(),
                            $andel->hentTom()
                        );
                        $nyeKravIder[] = $krav->hentId();
                        $typeModell = $tjeneste->hentTypeModell();
                        if($typeModell instanceof Strømanlegg) {
                            $krav->settAnlegg($typeModell);
                        }
                        $andel->settKrav($krav);
                    }
                }
            } catch (Exception $e) {
                /** @var Kravsett $nyeKrav */
                $nyeKrav = $this->app->hentSamling(Krav::class);
                $nyeKrav->filtrerEtterIdNumre($nyeKravIder)->låsFiltre()->slettHver();
                throw $e;
            }
        }
        $this->settFordelt(true);

        return $this;
    }

    /**
     * @throws Exception
     */
    public function slett()
    {
        $this->hentAndeler()->slettHver();
        parent::slett();
    }
}