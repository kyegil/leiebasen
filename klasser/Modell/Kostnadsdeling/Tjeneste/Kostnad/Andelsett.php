<?php

namespace Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Sett;

/**
 * Class Andelsett
 * @package Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad
 *
 * @method Andel current()
 */
class Andelsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Andel::class;

    /**
     * @var Andel[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }

    /**
     * @return Andelsett
     */
    public function leggTilLeftJoinForTjeneste(): Andelsett
    {
        return $this
            ->leggTilLeftJoin(
                Tjeneste::hentTabell(),
                '`' . Andel::hentTabell() . '`.`tjeneste_id` = `'
                . Tjeneste::hentTabell() . '`.`' . Tjeneste::hentPrimærnøkkelfelt() . '`'
            );
    }

    /**
     * @return Andelsett
     */
    public function leggTilLeftJoinForKostnad(): Andelsett
    {
        return $this
            ->leggTilLeftJoin(
                Kostnad::hentTabell(),
                '`' . Andel::hentTabell() . '`.`faktura_id` = `'
                . Kostnad::hentTabell() . '`.`' . Kostnad::hentPrimærnøkkelfelt() . '`'
            );
    }

    /**
     * @return Andelsett
     */
    public function leggTilLeftJoinForLeieforhold(): Andelsett
    {
        return $this
            ->leggTilLeftJoin(
                Leieforhold::hentTabell(),
                '`' . Andel::hentTabell() . '`.`leieforhold` = `'
                . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`'
            );
    }

    /**
     * @return Andelsett
     */
    public function leggTilLeftJoinForKrav(): Andelsett
    {
        return $this
            ->leggTilLeftJoin(
                Krav::hentTabell(),
                '`' . Andel::hentTabell() . '`.`leieforhold` = `'
                . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '`'
            );
    }

    /**
     * @return Andelsett
     */
    public function leggTilTjenesteModell(): Andelsett
    {
        return $this->leggTilLeftJoinForTjeneste()->leggTilModell(Tjeneste::class);
    }

    /**
     * @return Andelsett
     */
    public function leggTilKostnadModell(): Andelsett
    {
        return $this->leggTilLeftJoinForKostnad()->leggTilModell(Kostnad::class);
    }

    /**
     * @param array|null $felter
     * @return Andelsett
     */
    public function leggTilLeieforholdModell(?array $felter = null): Andelsett
    {
        return $this->leggTilLeftJoinForLeieforhold()->leggTilModell(Leieforhold::class, null, null, $felter);
    }

    /**
     * @param array|null $felter
     * @return Andelsett
     */
    public function leggTilKravModell(?array $felter = null): Andelsett
    {
        return $this->leggTilLeftJoinForKrav()->leggTilModell(Krav::class, null, null, $felter);
    }
}