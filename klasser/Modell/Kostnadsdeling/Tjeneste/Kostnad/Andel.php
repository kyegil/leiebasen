<?php

namespace Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad;

use DateTime;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use stdClass;


/**
 * Class Andel
 * @package Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad
 *
 * @property int $id
 * @property Tjeneste|null $tjeneste
 * @property \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad $regning
 * @property DateTime|null $fom
 * @property DateTime|null $tom
 * @property string $faktura
 * @property Leieforhold|null $leieforhold
 * @property float $andel
 * @property float $beløp
 * @property Krav|null $krav
 * @property string $tekst
 * @property DateTime|null $epostvarsel
 *
 * @method int hentId()
 * @method Tjeneste|null hentTjeneste()
 * @method Kostnad hentRegning()
 * @method DateTime|null hentFom()
 * @method DateTime|null hentTom()
 * @method string hentFaktura()
 * @method Leieforhold|null hentLeieforhold()
 * @method float hentAndel()
 * @method float hentBeløp()
 * @method Krav|null hentKrav()
 * @method string hentTekst()
 * @method DateTime|null hentEpostvarsel()
 *
 * @method $this settId(int $id)
 * @method $this settTjeneste(Tjeneste|null $tjeneste)
 * @method $this settRegning(Kostnad $regning)
 * @method $this settFom(DateTime|null $fom)
 * @method $this settTom(DateTime|null $tom)
 * @method $this settFaktura(string $fakturaNr)
 * @method $this settLeieforhold(Leieforhold|null $leieforhold)
 * @method $this settAndel(float $andel)
 * @method $this settBeløp(float $beløp)
 * @method $this settKrav(Krav|null $krav)
 * @method $this settTekst(string $tekst)
 * @method $this settEpostvarsel(DateTime|null $epostvarsel)
 *
 */
class Andel extends \Kyegil\Leiebasen\Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'kostnadsdeling_andeler';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Andelsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'tjeneste_id'    => [
                'target' => 'tjeneste',
                'type'  => Tjeneste::class
            ],
            'faktura_id'    => [
                'target' => 'regning',
                'type'  => Kostnad::class
            ],
            'fom'    => [
                'type'  => 'date',
                'allowNull' => true,
            ],
            'tom'    => [
                'type'  => 'date',
                'allowNull' => true,
            ],
            'faktura'    => [
                'type'  => 'string'
            ],
            'leieforhold' => [
                'type' => Leieforhold::class,
                'allowNull' => true,
            ],
            'andel'    => [
                'type'  => 'string'
            ],
            'beløp'    => [
                'type'  => 'string'
            ],
            'krav'    => [
                'type'  => Krav::class,
                'allowNull' => true,
            ],
            'tekst'    => [
                'type'  => 'string'
            ],
            'epostvarsel'    => [
                'type'  => 'datetime',
                'allowNull' => true,
            ]
        ];
    }

    /**
     * @param stdClass $parametere
     * @return Modell
     * @throws \Exception
     */
    public function opprett(stdClass $parametere): Modell
    {
        if (!property_exists($parametere, 'faktura') && $parametere->regning instanceof Kostnad) {
            $parametere->faktura = $parametere->regning->hentFakturanummer();
        }
        return parent::opprett($parametere);
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function slett()
    {
        $krav = $this->hentKrav();
        if ($krav) {
            $krav->slett();
        }
        parent::slett();
    }
}