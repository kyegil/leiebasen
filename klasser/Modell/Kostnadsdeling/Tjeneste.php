<?php

namespace Kyegil\Leiebasen\Modell\Kostnadsdeling;


use Exception;
use Kyegil\CoreModel\Interfaces\AppInterface;
use Kyegil\CoreModel\Interfaces\CoreModelRepositoryInterface;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnadsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Samling;
use Kyegil\Leiebasen\Samlingstrekk;
use stdClass;

/**
 * Class Tjeneste
 * @package Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel
 *
 * @property int $id
 * @property string $type
 * @property string $navn
 * @property string $leverandør
 * @property string $avtalereferanse
 * @property string $beskrivelse
 * @property boolean $registrerForbruk
 * @property string $forbruksenhet
 *
 * @method string hentType()
 * @method string hentNavn()
 * @method string hentLeverandør()
 * @method string hentAvtalereferanse()
 * @method string hentBeskrivelse()
 * @method boolean hentRegistrerForbruk()
 * @method string hentForbruksenhet()
 *
 * @method $this settNavn(string $navn)
 * @method $this settLeverandør(string $leverandør)
 * @method $this settAvtalereferanse(string $avtalereferanse)
 * @method $this settBeskrivelse(string $beskrivelse)
 * @method $this settRegistrerForbruk(boolean $registrerForbruk)
 * @method $this settForbruksenhet(string $forbruksenhet)
 *
 */
class Tjeneste extends \Kyegil\Leiebasen\Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'kostnadsdeling_tjenester';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Tjenestesett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    public static function harFlere(
        string $referanse,
        ?string $modell = null,
        ?string $fremmedNøkkel = null,
        array $filtre = [],
        ?callable $callback = null
    ): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::harFlere('regninger',
                Kostnad::class,
                'tjeneste_id',
                [],
                function(Kostnadsett $regninger) {
                    $regninger->leggTilSortering('fradato');
                }
            );
        }
        return parent::harFlere($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id' => [
                'type'  => 'int'
            ],
            'type'    => [
                'type'  => 'string',
            ],
            'navn'    => [
                'type'  => 'string',
            ],
            'leverandør'    => [
                'type'  => 'string',
            ],
            'avtalereferanse'    => [
                'type'  => 'string',
            ],
            'beskrivelse'    => [
                'type'  => 'string',
            ],
            'registrer_forbruk'    => [
                'type'  => 'boolean',
            ],
            'forbruksenhet'    => [
                'type'  => 'string',
            ]
        ];
    }

    /**
     * @param stdClass $parametere
     * @return Tjeneste
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Tjeneste
    {
        $parametere->type = $parametere->type ?? Krav::TYPE_ANNET;
        return parent::opprett($parametere);
    }

    /**
     * @return Tjeneste
     * @throws Exception
     */
    public function hentTypeModell(): Tjeneste
    {
        $klasse = $this->hentType() == Krav::TYPE_STRØM ? Strømanlegg::class : self::class;
        if ($klasse == static::class) {
            return $this;
        }
        $modell = $this->app->modellOppbevaring->hent($klasse, $this->hentId());
        if(!$modell) {
            // Prepare dependency injection
            $di = [
                AppInterface::class => $this->hentApp(),
                \mysqli::class => $this->mysqli,
                CoreModelRepositoryInterface::class => $this->coreModelRepository
            ];
            /** @var Tjeneste $modell */
            $modell = $this->app->modellFabrikk::lag($klasse, $di, $this->hentId());
            $modell->setPreloadedValues($this->rawData);
        }
        return $modell;
    }

    /**
     * @return Fordelingsnøkkel|null
     * @throws Exception
     */
    public function hentFordelingsnøkkel()
    {
        if(!property_exists($this->data, 'fordelingsnøkkel')) {
            /** @var Fordelingsnøkkel $fordelingsnøkkel */
            $fordelingsnøkkel = $this->app->hentModell(Fordelingsnøkkel::class, $this->hentId());
            /** @var Samlingstrekk $elementer */
            $elementer = $fordelingsnøkkel->hentElementer();
            $this->data->fordelingsnøkkel = $elementer->hentAntall() ? $fordelingsnøkkel : null;
        }
        return $this->data->fordelingsnøkkel;
    }

    /**
     * @return Kostnadsett
     * @throws Exception
     */
    public function hentRegninger(): Kostnadsett
    {
        /** @var Kostnadsett $regninger */
        $regninger = $this->hentSamling('regninger');
        return $regninger;
    }

    /**
     * @param \DateTime $fra
     * @param \DateTime|null $til
     * @param bool $somSamling
     * @return \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad[]|Samling
     * @throws Exception
     */
    public function hentRegningerForTidsrom(\DateTime $fra, \DateTime $til = null, $somSamling = false)
    {
        $resultat = [];
        if(!$til) {
            $til = clone $fra;
        }
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad $regning */
        foreach($this->hentRegninger() as $regning) {
            if(
                $regning->fradato <= $til
                && $regning->tildato >= $fra
            ) {
                $resultat[] = $regning;
            }
        }
        if($somSamling) {
            return $this->app->hentSamling(Kostnad::class)->lastFra($resultat);
        }
        return $resultat;
    }
}