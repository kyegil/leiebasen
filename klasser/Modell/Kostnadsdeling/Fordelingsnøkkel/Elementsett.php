<?php

namespace Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Sett;

/**
 * Class Elementsett
 * @package Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel
 *
 * @method Element current()
 */
class Elementsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Element::class;

    /**
     * @var Element[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }
}