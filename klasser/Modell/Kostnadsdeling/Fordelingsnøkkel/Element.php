<?php

namespace Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel;

use DateTimeInterface;
use Exception;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;


/**
 * Class Element
 * @package Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel
 *
 * @property Fordelingsnøkkel $fordelingsnøkkel
 * @property string $fordelingsmåte
 * @property boolean $følgerLeieobjekt
 * @property Leieobjekt $leieobjekt
 * @property Leieforhold $leieforhold
 * @property float $andeler
 * @property float $prosentsats
 * @property float $fastbeløp
 * @property string $forklaring
 *
 * @method Fordelingsnøkkel hentFordelingsnøkkel()
 * @method string hentFordelingsmåte()
 * @method boolean hentFølgerLeieobjekt()
 * @method Leieobjekt hentLeieobjekt()
 * @method Leieforhold hentLeieforhold()
 * @method float hentAndeler()
 * @method float hentProsentsats()
 * @method float hentFastbeløp()
 * @method string hentForklaring()
 *
 * @method $this settFordelingsnøkkel(Fordelingsnøkkel $fordelingsnøkkel)
 * @method $this settFordelingsmåte(string $fordelingsmåte)
 * @method $this settFølgerLeieobjekt(boolean $følgerLeieobjekt)
 * @method $this settLeieobjekt(Leieobjekt $leieobjekt)
 * @method $this settLeieforhold(Leieforhold $leieforhold)
 * @method $this settAndeler(float $andeler)
 * @method $this settProsentsats(float $prosentsats)
 * @method $this settFastbeløp(float $fastbeløp)
 * @method $this settForklaring(string $forklaring)
 *
 */
class Element extends \Kyegil\Leiebasen\Modell
{
    const FORDELINGSMÅTE_FAST = 'Fastbeløp';

    const FORDELINGSMÅTE_PROSENT = 'Prosentvis';

    const FORDELINGSMÅTE_ANDELER = 'Andeler';

    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'kostnadsdeling_fordelingsnøkler';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'nøkkel';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Elementsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'nøkkel'    => [
                'type'  => 'integer'
            ],
            'tjeneste_id' => [
                'target' => 'fordelingsnøkkel',
                'type'  => Fordelingsnøkkel::class
            ],
            'fordelingsmåte'    => [
                'type'  => 'string'
            ],
            'følger_leieobjekt'    => [
                'type'  => 'boolean'
            ],
            'leieobjekt'    => [
                'type'  => Leieobjekt::class,
                'allowNull' => true
            ],
            'leieforhold'    => [
                'type'  => Leieforhold::class,
                'allowNull' => true
            ],
            'andeler'    => [
                'type'  => 'string'
            ],
            'prosentsats'    => [
                'type'  => 'string'
            ],
            'fastbeløp'    => [
                'type'  => 'string'
            ],
            'forklaring'    => [
                'type'  => 'string'
            ]
        ];
    }

    /**
     * @param DateTimeInterface $fradato
     * @param DateTimeInterface $tildato
     * @return Leieforholdsett
     * @throws Exception
     */
    public function hentLeieforholdForTidsrom(DateTimeInterface $fradato, DateTimeInterface $tildato): Leieforholdsett
    {
        if ($this->hentFølgerLeieobjekt()) {
            if (!$this->hentLeieobjekt()) {
                throw new Exception('Fordelingsnøkkel-element ' . $this->hentId() . ' mangler leieobjekt');
            }
            $leieforholdsett = $this->hentLeieobjekt()->hentLeieforhold($fradato, $tildato);
        }
        else {
            if (!$this->hentLeieforhold()) {
                throw new Exception('Fordelingsnøkkel-element ' . $this->hentId() . ' har ikke oppgitt leieforhold');
            }
            /** @var Leieforholdsett $leieforholdsett */
            $leieforholdsett = $this->app->hentSamling(Leieforhold::class);
            $leieforholdsett->leggTilLeftJoinForOppsigelse()
                ->leggTilFilter([Leieforhold::hentPrimærnøkkelfelt() => $this->hentLeieforhold()->hentId()])
                ->leggTilFilter(['`leieforhold`.`fradato` <=' => $tildato->format('Y-m-d')])
                ->leggTilFilter([
                    'or' => [
                        ['`oppsigelser`.`leieforhold`' => null],
                        ['`oppsigelser`.`fristillelsesdato` >' => $fradato->format('Y-m-d')]
                    ]
                ]);
        }
        return $leieforholdsett;
    }

    /**
     * Hent andelsdata for et gitt tidsrom
     *
     * Denne funksjonen returnerer et sett med andelsdata for et gitt tidsrom for dette fordelingselementet
     *
     * Hvert objekt inneholder følgende egenskaper:
     * * leieforhold: Leieforholdet som berøres av fordelingselementet, enten direkte eller via leieobjektet
     * * fom: Den første datoen for dette leieforholdet, innenfor angitt fra- og tildato
     * * tom: Den siste datoen for dette leieforholdet, innenfor angitt fra- og tildato
     * * antallDagerIAndel: Antall dager dette leieforholdet deltar (tidsrommet fom – tom)
     * * antallAndelsDagerIAndel: Antall deler som leieforholdet skal bidra med multiplisert med antall dager (f.eks 14 for 2 deler i ei uke)
     * * terminDekningsFaktor: En brøk som angir hvor stor del av det tidsrommet som er angitt som
     * fra- og tildato, som dekkes av dette leieforholdet
     * * leieforholdTerminAndel: En brøk som angir terminDekningsFaktoren kombinert med evt andelen
     * for leieforholdet, dersom dette er i bofellesskap
     *
     * Summen av alle leieforholdTerminAndeler oppdateres fortløpende i $sumLeieforholdTerminAndeler
     * Summen av ale `antallAndelsDagerIAndel`, oppdateres fortløpende i $totaltAntallAndelsDagerIAlleAndeler
     *
     * @param DateTimeInterface $fradato
     * @param DateTimeInterface $tildato
     * @param Fraction|null $sumLeieforholdTerminAndeler
     * @return \stdClass[]
     * @throws Exception
     */
    public function hentAndelsData(
        DateTimeInterface $fradato,
        DateTimeInterface $tildato,
        ?Fraction $sumLeieforholdTerminAndeler = null,
        ?int &$totaltAntallAndelsDagerIAlleAndeler = null
    ): array
    {
        $sumLeieforholdTerminAndeler = $sumLeieforholdTerminAndeler ?? new Fraction();
        $totaltAntallAndelsDagerIAlleAndeler = $totaltAntallAndelsDagerIAlleAndeler ?? 0;
        $andelsData = [];

        $antallDagerITermin = $tildato->diff($fradato, true)->days + 1;
        $berørteLeieforhold = $this->hentLeieforholdForTidsrom($fradato, $tildato);

        foreach ($berørteLeieforhold as $leieforhold) {
            $fom = max($fradato, $leieforhold->fradato);
            $tom = $leieforhold->hentOppsigelse()
                ? min($tildato, \DateTimeImmutable::createFromMutable($leieforhold->hentOppsigelse()->fristillelsesdato)->sub(new \DateInterval('P1D')))
                : $tildato;
            $antallDagerIAndel = (int)$fom->diff($tom, false)->format("%r%a") + 1;
            if (!$antallDagerIAndel) {
                continue;
            }

            $antallAndelsDagerIAndel = null;
            if ($this->hentFordelingsmåte() == self::FORDELINGSMÅTE_ANDELER) {
                $antallAndelsDagerIAndel = $this->hentAndeler() * $antallDagerIAndel;
                $totaltAntallAndelsDagerIAlleAndeler += $antallAndelsDagerIAndel;
            }

            $terminDekningsFaktor = new Fraction($antallDagerIAndel, $antallDagerITermin);
            $leieforholdTerminAndel = $terminDekningsFaktor->multiply($this->hentFølgerLeieobjekt() ? $leieforhold->hentAndel() : 1);
            $sumLeieforholdTerminAndeler->add($leieforholdTerminAndel, true);

            $andelsData[] = (object)[
                'leieforhold' => $leieforhold,
                'fom' => $fom instanceof \DateTime ? \DateTimeImmutable::createFromMutable($fom) : $fom,
                'tom' => $tom instanceof \DateTime ? \DateTimeImmutable::createFromMutable($tom) : $tom,
                'antallDagerIAndel' => $antallDagerIAndel,
                'antallAndelsDagerIAndel' => $antallAndelsDagerIAndel,
                'terminDekningsFaktor' => $terminDekningsFaktor,
                'leieforholdTerminAndel' => $leieforholdTerminAndel,
            ];
        }
        return $andelsData;
    }
}