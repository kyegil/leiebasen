<?php
/**
 * Part of boligstiftelsen.svartlamon.org
 * Created by Kyegil
 * Date: 12/06/2020
 * Time: 13:23
 */

namespace Kyegil\Leiebasen\Modell\Kostnadsdeling;


use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel\Element;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel\Elementsett;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andel;
use Kyegil\Leiebasen\Modell\Leieforhold;
use stdClass;

/**
 * Class Fordelingsnøkkel
 * @package Kyegil\Leiebasen\Modell\Kostnadsdeling
 *
 * @property int $id
 *
 * @method $this settid(int $id)
 *
 */
class Fordelingsnøkkel extends \Kyegil\Leiebasen\Modell
{
    const ELEMENTORDEN = [
        Element::FORDELINGSMÅTE_FAST => 1,
        Element::FORDELINGSMÅTE_PROSENT => 2,
        Element::FORDELINGSMÅTE_ANDELER => 3
    ];

    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'kostnadsdeling_tjenester';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /**
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id' => [
                'type' => 'string'
            ],
        ];
    }

    /**
     * @param string|null $egenskap
     * @return mixed
     * @throws Exception
     */
    public function hent($egenskap = null)
    {
        switch ($egenskap) {
            case 'elementer':
                return $this->hentElementer();
            case 'beløpelementer':
                return $this->hentBeløpelementer();
            case 'prosentelementer':
                return $this->hentProsentelementer();
            case 'andelselementer':
                return $this->hentAndelselementer();
            case 'sumProsenter':
                return $this->hentSumProsenter();
            default:
                return parent::hent($egenskap);
        }
    }

    /**
     * @return Elementsett
     * @throws Exception
     */
    public function hentElementer(): Elementsett
    {
        /** @var Elementsett $elementer */
        $elementer = $this->app->hentSamling(Element::class)
            ->leggTilFilter([
                'tjeneste_id' => $this->hentId()
            ])
            ->låsFiltre()
        ;
        if(!isset($this->samlinger->elementer)) {
            $elementer->sorterLastede(function($a, $b) {
                    $orden = self::ELEMENTORDEN;

                    if(
                        ($a->hentFordelingsmåte() != $b->hentFordelingsmåte())
                        && isset($orden[$a->hentFordelingsmåte()])
                        && isset($orden[$b->hentFordelingsmåte()])
                    ) {
                        return $orden[$a->hentFordelingsmåte()] - $orden[$b->hentFordelingsmåte()];
                    }
                    if($a->hentLeieobjekt() !== $b->hentLeieobjekt()) {
                        return strnatcmp($a->hentLeieobjekt(), $b->hentLeieobjekt());
                    }
                    if($a->hentLeieforhold() !== $b->hentLeieforhold()) {
                        return strnatcmp($a->hentLeieforhold(), $b->hentLeieforhold());
                    }
                    return strnatcmp($a, $b);
                });
            $this->samlinger->elementer = $elementer->hentElementer();
        }
        $elementer->lastFra($this->samlinger->elementer);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $elementer;
    }

    /**
     * @return Elementsett
     * @throws Exception
     */
    public function hentBeløpelementer(): Elementsett
    {
        if(!isset($this->samlinger->beløpelementer)) {
            /** @var Elementsett $beløpelementer */
            $beløpelementer = $this->app->hentSamling(Element::class)
                ->leggTilFilter([
                    'tjeneste_id' => $this->hentId(),
                    'fordelingsmåte' => Element::FORDELINGSMÅTE_FAST
                ])
                ->låsFiltre()
            ;
            $elementer = [];
            foreach($this->hentElementer() as $element) {
                if($element->hentFordelingsmåte() == Element::FORDELINGSMÅTE_FAST) {
                    $elementer[] = $element;
                }
            }
            $this->samlinger->beløpelementer = $beløpelementer->lastFra($elementer);
        }
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $this->samlinger->beløpelementer;
    }

    /**
     * @return Elementsett
     * @throws Exception
     */
    public function hentProsentelementer(): Elementsett
    {
        if(!isset($this->samlinger->prosentelementer)) {
            /** @var Elementsett $prosentelementer */
            $prosentelementer = $this->app->hentSamling(Element::class)
                ->leggTilFilter([
                    'tjeneste_id' => $this->hentId(),
                    'fordelingsmåte' => Element::FORDELINGSMÅTE_PROSENT
                ])
                ->låsFiltre()
            ;
            /** @var Element[] $elementer */
            $elementer = [];
            foreach($this->hentElementer() as $element) {
                if($element->hentFordelingsmåte() == Element::FORDELINGSMÅTE_PROSENT) {
                    $elementer[] = $element;
                }
            }
            $this->samlinger->prosentelementer = $prosentelementer->lastFra($elementer);
        }
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $this->samlinger->prosentelementer;
    }

    /**
     * @return Elementsett
     * @throws Exception
     */
    public function hentAndelselementer(): Elementsett
    {
        if(!isset($this->samlinger->andelselementer)) {
            /** @var Elementsett $andelselementer */
            $andelselementer = $this->app->hentSamling(Element::class)
                ->leggTilFilter([
                    'tjeneste_id' => $this->hentId(),
                    'fordelingsmåte' => Element::FORDELINGSMÅTE_ANDELER
                ])
                ->låsFiltre()
            ;
            $elementer = [];
            foreach($this->hentElementer() as $element) {
                if($element->hentFordelingsmåte() == Element::FORDELINGSMÅTE_ANDELER) {
                    $elementer[] = $element;
                }
            }
            $this->samlinger->andelselementer = $andelselementer->lastFra($elementer);
        }
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $this->samlinger->andelselementer;
    }

    /**
     * @return mixed
     * @throws float
     */
    public function hentSumProsenter(): float
    {
        if(!isset($this->data->sumProsenter)) {
            $this->data->sumProsenter = 0;
            foreach($this->hentProsentelementer() as $element) {
                $this->data->sumProsenter += $element->hentProsentsats();
            }
        }
        return $this->data->sumProsenter;

    }

    /**
     * Hent Summen av alle andeler for en gitt dato
     *
     * @param DateTimeInterface $dato
     * @return int
     * @throws Exception
     */
    public function hentSumAndeler(DateTimeInterface $dato): int
    {
        $dato = $dato instanceof DateTime ? DateTimeImmutable::createFromMutable($dato) : $dato;

        settype($this->data->sumAndeler, 'array');
        if(!isset($this->data->sumAndeler[$dato->format('Y-m-d')])) {
            $this->data->sumAndeler[$dato->format('Y-m-d')] = 0;
            $elementAndeler = 0;
            foreach($this->hentAndelselementer() as $element) {
                $andelsData = $element->hentAndelsData($dato, $dato);
                $elementAndeler += (int)$element->hentAndeler() * count($andelsData);
            }
            $this->data->sumAndeler[$dato->format('Y-m-d')] = $elementAndeler;
        }
        return $this->data->sumAndeler[$dato->format('Y-m-d')];
    }

    /**
     * @return Fordelingsnøkkel|mixed
     * @throws Exception
     */
    public function slett()
    {
        $this->app->before($this, __FUNCTION__, []);
        foreach ($this->hentElementer() as $element) {
            $element->slett();
        }
        $this->reset();

        $this->coreModelRepository->remove(static::class, $this->getId());
        $this->data = new stdClass();
        $this->rawData = new stdClass();
        $this->id = null;
        return $this->app->after($this, __FUNCTION__, null);
    }

    /**
     * @return Tjeneste|null
     * @throws Exception
     */
    public function hentTjeneste(): ?Tjeneste
    {
        if(!isset($this->data->tjeneste)) {
            /** @var Tjeneste $tjeneste */
            $tjeneste = $this->app->hentModell(Tjeneste::class, $this->hentId());
            $this->data->tjeneste = $tjeneste->hentId() ? $tjeneste->hentTypeModell() : null;
        }
        return $this->data->tjeneste;
    }

    /**
     * @param Kostnad $regning
     * @return Fordelingsnøkkel
     * @throws Exception
     */
    public function fordel(Tjeneste\Kostnad $regning): Fordelingsnøkkel
    {
        if (strval($regning->tjeneste) !== strval($this->hentTjeneste())) {
            throw new Exception('Denne regningen tilhører en annen tjeneste.');
        }
        $beløp = $regning->hentFakturabeløp();
        $fradato = $regning->hentFradato();
        $tildato = $regning->hentTildato();
        if($regning->hentFordelt()) {
            throw new Exception('Denne regningen har allerede blitt fordelt.');
        }
        if (!is_numeric($beløp)) {
            throw new Exception('Kan ikke fordele regningen før beløp er angitt.');
        }
        if(!$fradato || !$tildato) {
            throw new Exception('Kan ikke fordele regningen før fra- og tildato er angitt');
        }

        /*
         * Slett evt tidligere andeler
         */
        $regning->forkastFordelingsforslag();

        $beløp -= $this->prosesserManuelleBeløp($regning);
        if ($beløp != 0 && $beløp * $regning->hentFakturabeløp() > 0) {
            $beløp -= $this->prosesserProsentElementer($beløp, $regning);
        }
        if ($beløp != 0 && $beløp * $regning->hentFakturabeløp() > 0) {
            $this->prosesserAndelsElementerFordeltPåDeltakere($beløp, $regning);
        }
        return $this;
    }

    /**
     * Returnerer alle delperioder med ulike leieforhold innenfor en termin
     *
     * Resultatet er et array av objekter med egenskapene fom og tom
     *
     * @param DateTimeInterface|null $fradato
     * @param DateTimeInterface|null $tildato
     * @return \stdClass[]
     * @throws Exception
     */
    public function hentDelperioderForAndeler(?DateTimeInterface $fradato, ?DateTimeInterface $tildato): array
    {
        $fradato = $fradato instanceof DateTime ? DateTimeImmutable::createFromMutable($fradato) : $fradato;
        $tildato = $tildato instanceof DateTime ? DateTimeImmutable::createFromMutable($tildato) : $tildato;
        $timezone = $fradato->getTimezone();
        $delperioder = [];

        $fomDatoer = [$fradato->format('Y-m-d') => $fradato->format('Y-m-d')];
        foreach($this->hentAndelselementer() as $element) {
            foreach ($element->hentAndelsData($fradato, $tildato) as $andel) {
                $fomDatoer[$andel->fom->format('Y-m-d')] = $andel->fom->format('Y-m-d');
                $nesteFom = $andel->tom->add(new \DateInterval('P1D'));
                if ($nesteFom->format('Y-m-d') <= $tildato->format('Y-m-d')) {
                    $fomDatoer[$nesteFom->format('Y-m-d')] = $nesteFom->format('Y-m-d');
                }
            }
        }
        sort($fomDatoer);

        foreach($fomDatoer as $index => $fomDatoStreng) {
            $fom = new DateTimeImmutable($fomDatoStreng, $timezone);
            $delperioder[$index] = (object)['fom' => new DateTimeImmutable($fomDatoStreng, $timezone), 'tom' => clone $tildato];
            $forrigeTom = $fom->sub(new \DateInterval('P1D'));
            if ($index > 0) {
                $delperioder[$index - 1]->tom = $forrigeTom;
            }
        }
        return $delperioder;
    }

    /**
     * @param float $beløp
     * @param Kostnad $regning
     * @return float
     * @throws Exception
     */
    private function prosesserManuelleBeløp(Kostnad $regning)
    {
        $fordelt = 0;
        $fradato = $regning->hentFradato();
        $tildato = $regning->hentTildato();

        foreach ($this->hentBeløpelementer() as $element) {
            $elementBeløp = $element->hentFastbeløp();
            $elementFordelt = 0;
            $fullTerminAndel = new Fraction(1);
            $sumLeieforholdTerminAndeler = new Fraction();
            $andelsData = $element->hentAndelsData($fradato, $tildato, $sumLeieforholdTerminAndeler);

            foreach ($andelsData as $andelsDataElement) {
                /** @var Leieforhold $leieforhold */
                $leieforhold = $andelsDataElement->leieforhold;
                /** @var Fraction $leieforholdTerminAndel */
                $leieforholdTerminAndel = $andelsDataElement->leieforholdTerminAndel;
                /** @var DateTimeInterface $fom */
                $fom = $andelsDataElement->fom;
                /** @var DateTimeInterface $tom */
                $tom = $andelsDataElement->tom;

                $andelsbeløp = round(
                    $leieforholdTerminAndel
                        ->divide($sumLeieforholdTerminAndeler)
                        ->multiply($elementBeløp)
                        ->asDecimal()
                );

                $andelstekst = $this->hentTjeneste()->hentNavn()
                    . ' ' . Leieforhold\Krav::beskrivTerminen($fom, $tom)
                    . ': '
                    . $this->app->kr($elementBeløp, false)
                ;
                if ($element->følgerLeieobjekt && $leieforholdTerminAndel->compare('!=', 1)) {
                    $andelstekst .= ' fordelt på ' . $element->hentLeieobjekt()->hentType() . ' ' . $element->hentLeieobjekt()->hentId();
                }
                else if ($element->følgerLeieobjekt) {
                    $andelstekst .= ' gjennom ' . $element->hentLeieobjekt()->hentType() . ' ' . $element->hentLeieobjekt()->hentId();
                }

                $this->app->nyModell(Andel::class, (object)[
                    'tjeneste' => $this->hentTjeneste(),
                    'regning' => $regning,
                    'fom' => $fom,
                    'tom' => $tom,
                    'leieforhold' => $leieforhold,
                    'andel' => $leieforholdTerminAndel->asDecimal(),
                    'beløp' => $andelsbeløp,
                    'tekst' => $andelstekst,
                ]);
                $elementFordelt += $andelsbeløp;
            }

            /*
             * Dersom beløpet ikke blir fordelt fordi leieforholdet er avsluttet eller leieobjektet har stått tomt
             * legges det inn en andel hvor kontraktnr er null.
             * Denne andelen vil ikke bli krev inn
             */
            if (
                abs($elementFordelt) < abs($elementBeløp)
                && $elementFordelt * $elementBeløp > 1
            ) {
                $this->app->nyModell(Andel::class, (object)[
                    'tjeneste' => $this->hentTjeneste(),
                    'regning' => $regning,
                    'fom' => $fradato,
                    'tom' => $tildato,
                    'leieforhold' => null,
                    'andel' => $fullTerminAndel->subtract($sumLeieforholdTerminAndeler)->asDecimal(),
                    'beløp' => $elementBeløp - $elementFordelt,
                ]);
                $elementFordelt = $elementBeløp;
            }
            $fordelt += $elementFordelt;
        }
        return $fordelt;
    }

    /**
     * @param float $beløp
     * @param Kostnad $regning
     * @return float
     * @throws Exception
     */
    private function prosesserProsentElementer(float $beløp, Kostnad $regning)
    {
        $fordelt = 0;
        $fradato = $regning->hentFradato();
        $tildato = $regning->hentTildato();

        foreach ($this->hentProsentelementer() as $element) {
            $elementBeløp = round($element->hentProsentsats() * $beløp);
            $elementFordelt = 0;
            $fullTerminAndel = new Fraction(1);
            $sumLeieforholdTerminAndeler = new Fraction();
            $andelsData = $element->hentAndelsData($fradato, $tildato, $sumLeieforholdTerminAndeler);

            foreach ($andelsData as $andelsDataElement) {
                /** @var Leieforhold $leieforhold */
                $leieforhold = $andelsDataElement->leieforhold;
                /** @var Fraction $leieforholdTerminAndel */
                $leieforholdTerminAndel = $andelsDataElement->leieforholdTerminAndel;
                /** @var DateTimeInterface $fom */
                $fom = $andelsDataElement->fom;
                /** @var DateTimeInterface $tom */
                $tom = $andelsDataElement->tom;

                $andelsbeløp = round(
                    $leieforholdTerminAndel
                        ->divide($sumLeieforholdTerminAndeler)
                        ->multiply($elementBeløp)
                        ->asDecimal()
                );

                $andelstekst = $this->hentTjeneste()->hentNavn()
                    . ' ' . Leieforhold\Krav::beskrivTerminen($fom, $tom)
                    . ': '
                    . $this->app->prosent($element->hentProsentsats(), 1, false)
                ;
                if ($element->følgerLeieobjekt && $leieforholdTerminAndel->compare('!=', 1)) {
                    $andelstekst .= ' fordelt på ' . $element->hentLeieobjekt()->hentType() . ' ' . $element->hentLeieobjekt()->hentId();
                }
                else if ($element->følgerLeieobjekt) {
                    $andelstekst .= ' gjennom ' . $element->hentLeieobjekt()->hentType() . ' ' . $element->hentLeieobjekt()->hentId();
                }

                $this->app->nyModell(Andel::class, (object)[
                    'tjeneste' => $this->hentTjeneste(),
                    'regning' => $regning,
                    'fom' => $fom,
                    'tom' => $tom,
                    'leieforhold' => $leieforhold,
                    'andel' => $leieforholdTerminAndel->asDecimal(),
                    'beløp' => $andelsbeløp,
                    'tekst' => $andelstekst,
                ]);
                $elementFordelt += $andelsbeløp;
            }

            /*
             * Dersom beløpet ikke blir fordelt fordi leieforholdet er avsluttet eller leieobjektet har stått tomt
             * legges det inn en andel hvor kontraktnr er null.
             * Denne andelen vil ikke bli krevd inn
             */
            if (
                abs($elementFordelt) < abs(($elementBeløp))
                && $elementFordelt * $elementBeløp > 1
            ) {
                $this->app->nyModell(Andel::class, (object)[
                    'tjeneste' => $this->hentTjeneste(),
                    'regning' => $regning,
                    'fom' => $fradato,
                    'tom' => $tildato,
                    'leieforhold' => null,
                    'andel' => $fullTerminAndel->subtract($sumLeieforholdTerminAndeler)->asDecimal(),
                    'beløp' => $elementBeløp - $elementFordelt,
                ]);
                $elementFordelt = $elementBeløp;
            }
            $fordelt += $elementFordelt;
        }
        return $fordelt;
    }

    /**
     * Prosesser andelselementer fordelt på tid
     *
     * Denne metoden prosesserer andelelementene på en måte som fordeler beløpet jevnt over det angitte tidsrommet.
     * Dvs. at dager med få deltakere har like høyt beløp som dager med mange deltakere å fordele mellom.
     *
     * Alle fakturaer blir delt opp i delperioder etter tilkomst/frafall av deltakere,
     * og hver delperiode blir fordelt innbyrdes blant de respektive deltakerne
     *
     * @param float $beløp
     * @param Kostnad $regning
     * @return float
     * @throws Exception
     */
    private function prosesserAndelsElementerFordeltPåTid(float $beløp, Kostnad $regning)
    {
        $fordelt = 0;
        $fradato = $regning->hentFradato();
        $tildato = $regning->hentTildato();

        $delperioder = $this->hentDelperioderForAndeler($fradato, $tildato);
        $antallDagerIRegning = $tildato->diff($fradato)->days + 1;

        foreach($delperioder as $delperiode) {
            $antallDagerIDelperiode = $delperiode->tom->diff($delperiode->fom)->days + 1;
            $delPeriodeAndelAvRegning = new Fraction($antallDagerIDelperiode, $antallDagerIRegning);
            $delPeriodeBeløp = $delPeriodeAndelAvRegning->multiply($beløp);
            $sumAndeler = $this->hentSumAndeler($delperiode->fom);
            if ($sumAndeler > 0) {
                foreach ($this->hentAndelselementer() as $element) {
                    $elementAndel = new Fraction($element->hentAndeler(), $sumAndeler);
                    $andelsbeløp = round($elementAndel->multiply($delPeriodeBeløp)->asDecimal());
                    $elementFordelt = 0;
                    $sumLeieforholdTerminAndeler = new Fraction();
                    $andelsData = $element->hentAndelsData($delperiode->fom, $delperiode->tom, $sumLeieforholdTerminAndeler);

                    foreach ($andelsData as $andelsDataElement) {
                        $andelstekst = $this->hentTjeneste()->hentNavn()
                            . ' ' . Leieforhold\Krav::beskrivTerminen($andelsDataElement->fom, $andelsDataElement->tom)
                            . ': '
                            . ($elementAndel->compare('=', 1) ? '' : $elementAndel->asFraction() . ' av ')
                            . $this->app->kr($delPeriodeBeløp->asDecimal(), false)
                        ;
                        if ($element->følgerLeieobjekt) {
                            $andelstekst .= ' gjennom ' . $element->hentLeieobjekt()->hentType() . ' ' . $element->hentLeieobjekt()->hentId();
                        }

                        $this->app->nyModell(Andel::class, (object)[
                            'tjeneste' => $this->hentTjeneste(),
                            'regning' => $regning,
                            'fom' => $andelsDataElement->fom,
                            'tom' => $andelsDataElement->tom,
                            'leieforhold' => $andelsDataElement->leieforhold,
                            'andel' => $elementAndel->asDecimal(),
                            'beløp' => $andelsbeløp,
                            'tekst' => $andelstekst,
                        ]);
                        $elementFordelt += $andelsbeløp;
                    }

                    $fordelt += $elementFordelt;
                }
            }
        }
        return $fordelt;
    }

    /**
     * Prosesser andelselementer fordelt på deltakere
     *
     * Denne metoden prosesserer andelelementene på en måte som fordeler beløpet jevnt over deltakerne i det angitte tidsrommet.
     * Dvs. at dager med få deltakere har lavere beløp enn dager med mange deltakere å fordele mellom
     *
     * @param float $beløp
     * @param Kostnad $regning
     * @return float
     * @throws Exception
     */
    private function prosesserAndelsElementerFordeltPåDeltakere(float $beløp, Kostnad $regning)
    {
        $fordelt = 0;
        $fradato = $regning->hentFradato();
        $tildato = $regning->hentTildato();
        $sumLeieforholdTerminAndeler = new Fraction();
        $totaltAntallDagerIDelperioder = 0;
        /** @var stdClass[] $andelsData */
        $andelsData = [];

        foreach ($this->hentAndelselementer() as $element) {
            $andelsData = array_merge($andelsData, $element->hentAndelsData($fradato, $tildato, $sumLeieforholdTerminAndeler, $totaltAntallDagerIDelperioder));
        }
        foreach ($andelsData as $andelsDataElement) {
            $elementAndel = new Fraction($andelsDataElement->antallAndelsDagerIAndel, $totaltAntallDagerIDelperioder);
            $andelsbeløp = round($elementAndel->multiply($beløp)->asDecimal());

            $andelstekst = $this->hentTjeneste()->hentNavn()
                . ' ' . Leieforhold\Krav::beskrivTerminen($andelsDataElement->fom, $andelsDataElement->tom)
                . ': '
                . ($elementAndel->compare('=', 1) ? '' : $elementAndel->simplify()->asFraction() . ' av ')
                . $this->app->kr($beløp, false)
            ;
            $this->app->nyModell(Andel::class, (object)[
                'tjeneste' => $this->hentTjeneste(),
                'regning' => $regning,
                'fom' => $andelsDataElement->fom,
                'tom' => $andelsDataElement->tom,
                'leieforhold' => $andelsDataElement->leieforhold,
                'andel' => $elementAndel->asDecimal(),
                'beløp' => $andelsbeløp,
                'tekst' => $andelstekst,
            ]);
            $fordelt += $andelsbeløp;
        }
        return $fordelt;
    }
}