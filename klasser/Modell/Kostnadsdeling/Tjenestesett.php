<?php

namespace Kyegil\Leiebasen\Modell\Kostnadsdeling;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Sett;

/**
 * Class Tjenestesett
 * @package Kyegil\Leiebasen\Modell\Kostnadsdeling
 *
 * @method Tjeneste current()
 */
class Tjenestesett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Tjeneste::class;

    /**
     * @var Tjeneste[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }
}