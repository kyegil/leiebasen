<?php

namespace Kyegil\Leiebasen\Modell\Fellesstrøm;

use Kyegil\Leiebasen\Modell\Leieforhold\Krav;

/**
 * Class Strømanleggsett
 * @package Kyegil\Leiebasen\Modell\Kostnadsdeling
 *
 * @method Strømanlegg current()
 */
class Strømanleggsett extends \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjenestesett
{
    /**
     * @var string
     */
    protected string $model = Strømanlegg::class;

    /**
     * @var Strømanlegg[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param array $di
     * @throws \Kyegil\CoreModel\CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
        $this->leggTilFilter(['type' => Krav::TYPE_STRØM])->låsFiltre();
    }
}