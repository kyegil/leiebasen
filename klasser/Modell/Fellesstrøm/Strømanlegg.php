<?php

namespace Kyegil\Leiebasen\Modell\Fellesstrøm;

use Exception;
use Kyegil\CoreModel\CoreModel;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use stdClass;

/**
 * Class Strømanlegg
 * @package Kyegil\Leiebasen\Modell\Fellesstrøm
 *
 * @property int $id
 * @property string $type
 * @property string $anleggsnummer
 * @property string $målernummer
 * @property string $plassering
 * @property string $formål
 *
 * @method string hentType()
 * @method string hentAnleggsnummer()
 * @method string hentMålernummer()
 * @method string hentPlassering()
 * @method string hentFormål()
 *
 * @method $this settAnleggsnummer(string $anleggsnummer)
 * @method $this settMålernummer(string $målernummer)
 * @method $this settPlassering(string $plassering)
 * @method $this settFormål(string $formål)
 *
 */
class Strømanlegg extends \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste
{
    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Strømanleggsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        $dbFelter = parent::hentDbFelter();
        return array_merge($dbFelter, [
            'målernummer' => [
                'type'  => 'string'
            ],
            'plassering'    => [
                'type'  => 'string'
            ],
        ]);
    }

    /**
     * @param stdClass $parametere
     * @return $this
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Tjeneste
    {
        $parametere->type = Krav::TYPE_STRØM;
        return parent::opprett($parametere);
    }

    /**
     * @param string $egenskap
     * @return mixed
     * @throws Exception
     */
    public function hent($egenskap = null)
    {
        if ($egenskap == 'anleggsnummer') {
            $egenskap = 'avtalereferanse';
        }
        if ($egenskap == 'formål') {
            $egenskap = 'beskrivelse';
        }
        return parent::hent($egenskap);
    }

    /**
     * @param string $egenskap
     * @param mixed $verdi
     * @return Strømanlegg
     * @throws Exception
     */
    public function sett($egenskap, $verdi): Modell
    {
        if ($egenskap == 'anleggsnummer') {
            $egenskap = 'avtalereferanse';
        }
        if ($egenskap == 'formål') {
            $egenskap = 'beskrivelse';
        }
        return parent::sett($egenskap, $verdi);
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function load(): CoreModel
    {
        $tp = $this->mysqli->table_prefix;
        $load = $this->mysqli->arrayData([
            'source' => $tp . static::getDbTable(),
            'fields' => array_keys($this->fields[static::getDbTable()]),
            'where' => [
                static::getDbPrimaryKeyField() => $this->id,
                'type' => Modell\Leieforhold\Krav::TYPE_STRØM
            ]
        ]);
        if(!$load->success) {
            $this->forceReload = false;
            $this->coreModelRepository->remove(static::class, $this->id);
            throw new Exception('Could not load ' . get_class($this) . ' (' . $this->id . ')');
        }
        if($load->totalRows) {
            $this->rawData->{static::getDbTable()} = reset($load->data);
            $this->coreModelRepository->save($this);
        }
        return $this;
    }
}