<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 07/12/2020
 * Time: 10:02
 */

namespace Kyegil\Leiebasen\Modell;

use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Kyegil\CoreModel\Interfaces\EavValuesInterface;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Eav\Egenskap as EavEgenskap;
use Kyegil\Leiebasen\Modell\Eav\Verdi as EavVerdi;
use Kyegil\Leiebasen\Modell\Søknad\Kategori;
use Kyegil\Leiebasen\Modell\Søknad\Kategorisett;
use Kyegil\Leiebasen\Modell\Søknad\Notat;
use Kyegil\Leiebasen\Modell\Søknad\Notatsett;
use Kyegil\Leiebasen\Modell\Søknad\Type;
use Kyegil\Leiebasen\Samling;
use stdClass;

/**
 * Class Søknad
 * @package Kyegil\Leiebasen\Modell
 *
 * @property integer $id
 * @property Type $type
 * @property bool $aktiv
 * @property DateTimeInterface $registrert
 * @property DateTimeInterface|null $oppdatert
 * @property string $søknadstekst
 * @property string[] $stikkord
 * @property object|null $metaData
 * @property DateTimeInterface|null $utløpVarslet
 *
 * @method int hentId()
 * @method string[] hentStikkord()
 * @method Type hentType()
 * @method bool hentAktiv()
 * @method DateTimeInterface hentRegistrert()
 * @method DateTimeInterface|null hentOppdatert()
 * @method string hentSøknadstekst()
 * @method DateTimeInterface|null hentUtløpVarslet()
 *
 * @method $this settSøknadstekst(string $søknadstekst)
 * @method $this settStikkord(string[] $stikkord)
 * @method $this settAktiv(bool $aktiv)
 * @method $this settType(Type $type)
 * @method $this settRegistrert(DateTimeInterface $registrert)
 * @method $this settOppdatert(DateTimeInterface|null $oppdatert)
 * @method $this settUtløpVarslet(DateTimeInterface|null $utløpVarslet)
 */
class Søknad extends Modell
{
    const STIKKORD_REGEX = '/[0-9a-zà-öø-ÿ]{3,}/';
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'søknader';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Søknadsett::class;
    
    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'søknadstype'    => [
                'target'  => 'type',
                'type'  => Type::class,
            ],
            'registrert'    => [
                'type'  => 'datetime'
            ],
            'oppdatert'    => [
                'type'  => 'datetime',
                'allowNull' => true
            ],
            'aktiv'    => [
                'type'  => 'boolean',
            ],
            'søknadstekst'    => [
                'type'  => 'string'
            ],
            'stikkord'    => [
                'type'    => function(string $stikkord) {
                    return explode(' ', $stikkord);
                },
                'toDbValue' => function(array $stikkord) {
                    return implode(' ', $stikkord);
                }
            ],
            'meta_data'    => [
                'type'  => 'json',
                'allowNull' => true
            ]
        ];
    }

    /**
     * @param string $kilde
     * @return stdClass|null
     * @throws Exception
     */
    public function hentEavVerdiObjekter(string $kilde): ?stdClass
    {
        if(!$this->eavValueObjects) {
            $this->eavValueObjects = new stdClass();

            /** @var Samling $eavVerdiObjekter */
            $eavVerdiObjekter = $this->app->hentSamling(EavVerdi::class)

                ->leggTilInnerJoin(EavEgenskap::hentTabell(), EavVerdi::hentTabell() . '.egenskap_id = ' . EavEgenskap::hentTabell() . '.id')
                ->leggTilFilter([
                    EavEgenskap::hentTabell() . '.modell' => self::class,
                    EavVerdi::hentTabell() . '.objekt_id' => $this->getId()
                ]);
            /** @var EavValuesInterface $eavVerdiObjekt */
            foreach ($eavVerdiObjekter as $eavVerdiObjekt) {
                $verdiKilde = $eavVerdiObjekt->getConfig()::getDbTable();
                $kode = $eavVerdiObjekt->getConfig()->getSourceField();
                settype($this->eavValueObjects->{$verdiKilde}, 'object');
                $this->eavValueObjects->{$verdiKilde}->{$kode} = $eavVerdiObjekt;
            }
        }

        if(isset($this->eavValueObjects->{$kilde})) {
            return $this->eavValueObjects->{$kilde};
        }
        return null;
    }

    /**
     * @param string|null $egenskap
     * @return mixed
     * @throws Exception
     */
    public function hentMetaData(?string $egenskap = null)
    {
        $metaData = $this->hent('meta_data');
        if($egenskap && is_object($metaData)) {
            return $metaData->{$egenskap} ?? null;
        }
        return $metaData;
    }

    /**
     * @param mixed $data
     * @param string|null $egenskap
     * @return Søknad
     * @throws Exception
     */
    public function settMetaData($data = null, ?string $egenskap = null): Søknad
    {
        if ($egenskap) {
            $metaData = $this->hent('meta_data');
            settype($metaData, 'object');
            $metaData->{$egenskap} = $data;
            return $this->sett('meta_data', $metaData);
        }
        $data = $data ? (object)$data : null;
        return $this->sett('meta_data', $data);
    }

    /**
     * @return string[]
     * @throws Exception
     */
    public function hentFelter(): array
    {
        $typeKonfigurering = $this->hentTypekonfigurering();
        if (!$typeKonfigurering) {
            $type = $this->hentType();
            return $type ? $type->hentKonfigurering('felter') : [];
        }
        return (isset($typeKonfigurering->felter))
            ? $typeKonfigurering->felter
            : [];
    }

    /**
     * @return string[]
     * @throws Exception
     */
    public function hentAktiveFelter(): array
    {
        $typeKonfigurering = $this->hentTypekonfigurering();
        if (!$typeKonfigurering) {
            $type = $this->hentType();
            return $type ? $type->hentKonfigurering('aktive_felter') : [];
        }
        return (isset($typeKonfigurering->aktive_felter))
            ? $typeKonfigurering->aktive_felter
            : [];
    }

    /**
     * @param string|null $egenskap
     * @return mixed
     * @throws Exception
     */
    public function hentTypekonfigurering(?string $egenskap = null)
    {
        $konfigurering  = $this->hentMetaData('konfigurering') ?? new stdClass();
        if ($egenskap) {
            return $konfigurering->$egenskap ?? null;
        }
        return $this->hentMetaData('konfigurering');
    }

    /**
     * @return string
     * @throws Exception
     */
    public function hentSpråk(): string
    {
        return $this->hentMetaData('språk') ?? 'no';
    }

    /**
     * @return array
     * @throws Exception
     */
    public function hentKategorier(): array {
        $kategorier = [];
        /** @var Kategorisett $kategorisett */
        $kategorisett = $this->app->hentSamling(Kategori::class)
            ->leggTilFilter(['søknad_id' => $this->hentId()])
            ->låsFiltre()
            ->leggTilSortering('søknad_id')
        ;
        if(!isset($this->samlinger->kategorier)) {
            $this->samlinger->kategorier = $kategorisett->hentElementer();
        }
        $kategorisett->lastFra($this->samlinger->kategorier);

        foreach($kategorisett as $kategori) {
            $kategorier[] = $kategori->kategori;
        }

        return array_unique($kategorier);
    }

    /**
     * @return Søknad
     * @throws Exception
     */
    public function slettAlleKategorier(): Søknad
    {
        /** @var Kategorisett $kategorisett */
        $kategorisett = $this->app->hentSamling(Kategori::class)
            ->leggTilFilter(['søknad_id' => $this->hentId()])
            ->låsFiltre()
        ;
        $kategorisett->slettAlle();
        unset ($this->samlinger->kategorier);
        return $this;
    }

    /**
     * @param string[] $kategorier
     * @return $this
     * @throws Exception
     */
    public function settKategorier(array $kategorier): Søknad
    {
        $kategorier = array_unique(array_map('trim', $kategorier));
        $this->slettAlleKategorier();

        foreach (array_unique($kategorier) as $kategori) {
            $this->app->nyModell(Kategori::class, (object)[
                'søknad' => $this,
                'kategori' => trim($kategori)
            ]);
        }
        return $this;
    }

    /**
     * @return Notatsett
     * @throws Exception
     */
    public function hentNotater(): Notatsett
    {
        /** @var Notatsett $notater */
        $notater = $this->app->hentSamling(Notat::class)
            ->leggTilFilter(['søknad_id' => $this->hentId()])
            ->låsFiltre()
            ->leggTilSortering('tidspunkt')
        ;
        if(!isset($this->samlinger->notater)) {
            $this->samlinger->notater = $notater->hentElementer();
        }
        $notater->lastFra($this->samlinger->notater);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $notater;
    }

    /**
     * @param object $layoutElement
     * @return Søknad
     * @throws Exception
     */
    public function lagAutoAdminFilter(object $layoutElement): Søknad
    {
        $kategorier = (array)$layoutElement->autofilter;
        $valgt = [];
        $verdibane = explode('[' , rtrim($layoutElement->name, ']'));
        $søknadverdi = $this->hent(array_shift($verdibane));
        while($verdibane && (is_object($søknadverdi))) {
            $søknadverdi = $søknadverdi->{array_shift($verdibane)} ?? null;
        }
        switch($layoutElement->element) {
            case 'checkbox_group':
            case 'radio_button_group':
            case 'select':
                if($søknadverdi) {
                    foreach ((array)$søknadverdi as $enkeltverdi) {
                        if(isset($kategorier[$enkeltverdi])) {
                            $valgt[] = $kategorier[$enkeltverdi];
                        }
                    }
                }
                break;
            case 'checkbox':
            case 'radio_button':
                if(isset($kategorier[$søknadverdi])) {
                    $valgt[] = $kategorier[$søknadverdi];
                }
                break;
            default:
                break;
        }
        $andreKategorier = array_diff($this->hentKategorier(), $kategorier);
        $this->settKategorier(array_merge($andreKategorier, $valgt));
        return $this;
    }

    /**
     * @param stdClass $parametere
     * @return Søknad
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Søknad
    {
        $parametere->søknadstekst = $parametere->søknadstekst ?? '';
        parent::opprett($parametere);
        $this->settStikkord($this->beregnStikkord());
        return $this;
    }

    /**
     * @return string[]
     * @throws Exception
     */
    public function beregnStikkord(): array
    {
        $this->app->before($this, __FUNCTION__, []);
        $stikkord = [];
        $konfigurering = $this->hentMetaData('konfigurering') ?? new stdClass();
        $egenskaper = $konfigurering->aktive_felter ?? new stdClass();
        foreach($egenskaper as $egenskap) {
            $stikkord = array_merge($stikkord, self::trekkNøkkelord($this->hent($egenskap)));
        }

        $stikkord = array_unique($stikkord);
        usort($stikkord, function ($a, $b) {
            if(mb_strlen($a) == mb_strlen($b)) {
                return strcmp($a, $b);
            }
            return mb_strlen($b) - mb_strlen($a);
        });

        return $this->app->after($this, __FUNCTION__, $stikkord);
    }

    /**
     * @param $innhold
     * @return string[]
     */
    public static function trekkNøkkelord($innhold): array
    {
        $stikkord = [];
        if(is_array($innhold) || $innhold instanceof stdClass) {
            foreach ($innhold as $element) {
                $stikkord = array_merge($stikkord, self::trekkNøkkelord($element));
            }
        }
        else if (is_string($innhold)) {
            preg_match_all(self::STIKKORD_REGEX, mb_strtolower($innhold), $stikkord);
            $stikkord = $stikkord[0] ?? [];
        }
        return $stikkord;
    }

    /**
     * @param $egenskap
     * @param $verdi
     * @return Søknad
     * @throws Exception
     */
    public function sett($egenskap, $verdi): Søknad
    {
        parent::sett($egenskap, $verdi);
        if(!in_array($egenskap, ['id','aktiv','søknadstype','stikkord'])) {
            $this->settStikkord($this->beregnStikkord());
        }
        return $this;
    }

    /**
     * @return Søknad\Adgang|null
     * @throws Exception
     */
    public function hentAdgang(): ?Modell\Søknad\Adgang
    {
        $this->app->before($this, __FUNCTION__, []);
        if(!property_exists($this->data, 'adgang')) {
            /** @var Modell\Søknad\Adgangsett $adgangsett */
            $adgangsett = $this->app->hentSamling(Modell\Søknad\Adgang::class)
                ->leggTilFilter(['søknad_id' => $this->hentId()])
            ;
            $this->data->adgang = $adgangsett->hentFørste();
        }
        return $this->app->after($this, __FUNCTION__, $this->data->adgang);
    }

    /**
     * @return DateTimeImmutable|null
     * @throws Exception
     */
    public function hentUtløpsdato(): ?DateTimeImmutable
    {
        $this->app->before($this, __FUNCTION__, []);
        if(!property_exists($this->data, 'utløpsdato')) {
            $this->data->utløpsdato = null;
            $gyldighet = $this->hentType()->hentKonfigurering('søknad_gyldighet') ?? '';
            if ($gyldighet) {
                $gyldighet = new DateInterval($gyldighet);
                /** @var DateTimeImmutable $oppdatert */
                $oppdatert = $this->hentOppdatert();
                if($oppdatert instanceof \DateTime) {
                    $oppdatert = DateTimeImmutable::createFromMutable($oppdatert);
                }
                $this->data->utløpsdato = $oppdatert->add($gyldighet);
            }
        }
        return $this->app->after($this, __FUNCTION__, $this->data->utløpsdato);
    }
}