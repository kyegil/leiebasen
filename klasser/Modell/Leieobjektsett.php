<?php

namespace Kyegil\Leiebasen\Modell;


use Kyegil\CoreModel\CoreModel;
use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Sett;

/**
 * Class Leieobjektsett
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Leieobjekt current()
 */
class Leieobjektsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Leieobjekt::class;

    /**
     * @var Leieobjekt[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
        $this
            ->leggTilBygningModell()
        ;
    }

    /**
     * Legg til LEFT JOIN for bygning
     *
     * @return Leieobjektsett
     */
    public function leggTilLeftJoinForBygning(): Leieobjektsett
    {
        return $this
            ->leggTilLeftJoin(Bygning::hentTabell(), Leieobjekt::hentTabell() . '.`bygning` = ' . Bygning::hentTabell() . '.' . Bygning::hentPrimærnøkkelfelt());
    }

    public function leggTilBygningModell(): Leieobjektsett
    {
        return $this->leggTilLeftJoinForBygning()
            ->leggTilModell(Bygning::class);
    }

    /**
     * @return Leieobjektsett
     */
    public function forhåndslastErOverskueligOpptatt(): Leieobjektsett
    {
        $tp = CoreModel::$tablePrefix;

        $erOverskueligOpptattSubQuery = <<<SQL
            SELECT `andelsperioder`.`leieobjekt`,
            '1' AS `verdi`
            FROM `{$tp}andelsperioder` AS `andelsperioder`
            WHERE `tildato` IS NULL
            GROUP BY `andelsperioder`.`leieobjekt`
            HAVING SUM(
                  SUBSTRING_INDEX(CONCAT(`andel`, IF(LOCATE('/', `andel`) = 0, '/1', '')), '/', 1)
                / SUBSTRING_INDEX(CONCAT(`andel`, IF(LOCATE('/', `andel`) = 0, '/1', '')), '/', -1)
            ) >= 1
            SQL;
        $this->leggTilSubQueryJoin(
            $erOverskueligOpptattSubQuery,
            'er_overskuelig_opptatt',
            "`er_overskuelig_opptatt`.`leieobjekt` = `leieobjekter`.`leieobjektnr`",
        );

        $this->leggTilFelt(
            'er_overskuelig_opptatt',
            'verdi',
            null,
            'er_overskuelig_opptatt',
            'leieobjekt_ekstra'
        );
        return $this;
    }
}