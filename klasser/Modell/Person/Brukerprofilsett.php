<?php

namespace Kyegil\Leiebasen\Modell\Person;


use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Sett;

/**
 * Class Brukerprofilsett
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Brukerprofil current()
 */
class Brukerprofilsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Brukerprofil::class;

    /**
     * @var Brukerprofil[]
     */
    protected ?array $items = null;

    /**
     * @param string|null $alias
     * @return Brukerprofilsett
     */
    public function leggTilLeftJoinForPerson(?string $alias = null): Brukerprofilsett
    {
        $alias = $alias ?: Person::hentTabell();
        return $this->leggTilLeftJoin(
            [$alias => Person::hentTabell()],
            Brukerprofil::hentTabell() . '.`person_id` = `' . $alias . '`.`' . Person::hentPrimærnøkkelfelt() . '`'
        );
    }

    /**
     * @return Brukerprofilsett
     */
    public function leggTilPersonModell(): Brukerprofilsett
    {
        return $this
            ->leggTilLeftJoinForPerson()
            ->leggTilModell(Person::class)
            ;
    }

    /**
     * @param string $login
     * @return Brukerprofil|null
     * @throws \Exception
     */
    public function finnLogin(string $login): ?Brukerprofil
    {
        $this->leggTilFilter(['login' => $login]);
        /** @var Brukerprofil|null $brukerprofil */
        $brukerprofil = $this->hentFørste();
        return $brukerprofil;
    }
}