<?php

namespace Kyegil\Leiebasen\Modell\Person;


use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Sett;

/**
 * Class EfakturaIdentifiersett
 * @package Kyegil\Leiebasen\Modell
 *
 * @method EfakturaIdentifier current()
 */
class EfakturaIdentifiersett extends Sett
{
    /**
     * @var string
     */
    protected string $model = EfakturaIdentifier::class;

    /**
     * @var EfakturaIdentifier[]
     */
    protected ?array $items = null;

    /**
     * @param string|null $alias
     * @return EfakturaIdentifiersett
     */
    public function leggTilLeftJoinForPerson(?string $alias = null): EfakturaIdentifiersett
    {
        $alias = $alias ?: Person::hentTabell();
        return $this->leggTilLeftJoin(
            [$alias => Person::hentTabell()],
            EfakturaIdentifier::hentTabell() . '.`person_id` = ' . Person::hentTabell() . '.' . Person::hentPrimærnøkkelfelt()
        );
    }

    /**
     * @return EfakturaIdentifiersett
     */
    public function leggTilPersonModell(): EfakturaIdentifiersett
    {
        return $this
            ->leggTilLeftJoinForPerson()
            ->leggTilModell(Person::class)
            ;
    }
}