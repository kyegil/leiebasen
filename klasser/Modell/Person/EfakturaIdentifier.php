<?php

namespace Kyegil\Leiebasen\Modell\Person;


use DateTimeInterface;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient\EfakturaIdentifierKey;

/**
 * Class EfakturaIdentifier
 * @package Kyegil\Leiebasen\Modell\Person
 *
 * @property integer $id
 * @property Person|null $person
 * @property string $efakturaIdentifier
 * @property boolean $hasAutoAcceptAgreements
 * @property boolean|null $blockedByReceiver
 * @property boolean|null $hasEfakturaAgreement
 * @property DateTimeInterface $oppdatert
 *
 * @method integer hentId()
 * @method Person|null hentPerson()
 * @method string hentEfakturaIdentifier()
 * @method boolean hentHasAutoAcceptAgreements()
 * @method boolean|null hentBlockedByReceiver()
 * @method boolean|null hentHasEfakturaAgreement()
 * @method DateTimeInterface hentOppdatert()
 *
 * @method $this settPerson(Person $person)
 * @method $this settEfakturaIdentifier(string $efakturaIdentifier)
 * @method $this settHasAutoAcceptAgreements(boolean $hasAutoAcceptAgreements)
 * @method $this settBlockedByReceiver(boolean|null $blockedByReceiver)
 * @method $this settHasEfakturaAgreement(boolean|null $hasEfakturaAgreement)
 */
class EfakturaIdentifier extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'efaktura_identifiers';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = EfakturaIdentifiersett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'person_id'    => [
                'type'  => Person::class,
                'target' => 'person',
                'allowNull' => true
            ],
            'efaktura_identifier'    => [
                'type'  => 'string',
                'allowNull' => true
            ],
            'has_auto_accept_agreements'    => [
                'type'  => 'boolean',
            ],
            'blocked_by_receiver'    => [
                'type'  => 'boolean',
                'allowNull' => true
            ],
            'has_efaktura_agreement'    => [
                'type'  => 'boolean',
                'allowNull' => true
            ],
            'siste_response_result'    => [
                'type'  => 'string',
                'allowNull' => true
            ],
            'siste_identifier_key'    => [
                'type'  => function($json) {
                    $identifierKey = new EfakturaIdentifierKey();
                    return $identifierKey->setData(json_decode($json));
                },
                'allowNull' => true
            ],
            'oppdatert'    => [
                'type'  => 'datetime',
            ],
        ];
    }

    /**
     * @return int|null
     */
    public function hentPersonId():? int
    {
        $person = $this->hentPerson();
        return $person ? $person->id : null;
    }
}