<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 12/09/2020
 * Time: 08:34
 */

namespace Kyegil\Leiebasen\Modell\Person;


use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;

/**
 * Class Adgang
 * @package Kyegil\Leiebasen\Modell\Person
 *
 * @property Person $person
 * @property string $adgang
 * @property Leieforhold|null $leieforhold
 * @property boolean $epostvarsling
 * @property boolean $innbetalingsbekreftelse
 * @property boolean $forfallsvarsel
 * @property boolean|null $umiddelbartBetalingsvarselEpost
 *
 * @method Person hentPerson()
 * @method string hentAdgang()
 * @method Leieforhold|null hentLeieforhold()
 * @method boolean hentEpostvarsling()
 * @method boolean hentInnbetalingsbekreftelse()
 * @method boolean hentForfallsvarsel()
 * @method boolean|null hentUmiddelbartBetalingsvarselEpost()
 *
 * @method $this settPerson(Person $person)
 * @method $this settAdgang(string $adgang)
 * @method $this settLeieforhold(Leieforhold|null $leieforhold)
 * @method $this settEpostvarsling(boolean $epostvarsling)
 * @method $this settInnbetalingsbekreftelse(boolean $innbetalingsbekreftelse)
 * @method $this settForfallsvarsel(boolean $forfallsvarsel)
 * @method $this settUmiddelbartBetalingsvarselEpost(boolean|null $umiddelbartBetalingsvarselEpost)
 */
class Adgang extends \Kyegil\Leiebasen\Modell
{
    const ADGANG_SENTRAL = 'sentral';
    const ADGANG_DRIFT = 'drift';
    const ADGANG_MINESIDER = 'mine-sider';
    const ADGANG_FLYKO = 'flyko';
    const ADGANG_OPPFØLGING = 'oppfølging';

    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'adganger';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'adgangsid';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Adgangsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'adgangsid'    => [
                'type'  => 'int'
            ],
            'personid'    => [
                'target'  => 'person',
                'type'  => Person::class,
                'rawDataContainer' => Person::hentTabell()
            ],
            'adgang'    => [
                'type'  => 'string'
            ],
            'leieforhold'    => [
                'allowNull'  => true,
                'type'  => Leieforhold::class,
                'rawDataContainer' => Leieforhold::hentTabell()
            ],
            'epostvarsling'    => [
                'type'  => 'boolean'
            ],
            'innbetalingsbekreftelse'    => [
                'type'  => 'boolean'
            ],
            'forfallsvarsel'    => [
                'type'  => 'boolean'
            ]
        ];
    }

    public static function hentAdgangsområder(): array {
        return Leiebase::postStatic(static::class, __FUNCTION__, [
            self::ADGANG_DRIFT,
            self::ADGANG_MINESIDER,
            self::ADGANG_FLYKO,
            self::ADGANG_OPPFØLGING,
        ]);
    }
}