<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 03/05/2021
 * Time: 09:56
 */

namespace Kyegil\Leiebasen\Modell\Person;


use Exception;
use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Sett;

/**
 * Class Leieforholdsett
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Adgang current()
 */
class Adgangsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Adgang::class;

    /**
     * @var Adgang[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
        $this
            ->leggTilLeftJoin(Leieforhold::hentTabell(), Adgang::hentTabell() . '.`leieforhold` = ' . Leieforhold::hentTabell() . '.' . Leieforhold::hentPrimærnøkkelfelt())
            ->leggTilLeftJoin(Person::hentTabell(), Adgang::hentTabell() . '.`personid` = ' . Person::hentTabell() . '.' . Person::hentPrimærnøkkelfelt())
        ;
    }

    /**
     * @param string|null $alias
     * @return Adgangsett
     */
    public function leggTilLeftJoinForLeieforhold(?string $alias = null): Adgangsett
    {
        $alias = $alias ?: Leieforhold::hentTabell();
        return $this->leggTilLeftJoin(
            [$alias => Leieforhold::hentTabell()],
            '`' . Adgang::hentTabell() . '`.`leieforhold` = `'
            . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`'
        );
    }

    /**
     * @return Adgangsett
     * @throws Exception
     */
    public function leggTilLeieforholdModell(): Adgangsett
    {
        return $this
            ->leggTilLeftJoinForLeieforhold()
            ->leggTilModell(Leieforhold::class)
        ;
    }

    /**
     * @param string|null $alias
     * @return Adgangsett
     */
    public function leggTilLeftJoinForPerson(?string $alias = null): Adgangsett
    {
        $alias = $alias ?: Person::hentTabell();
        return $this->leggTilLeftJoin(
            [$alias => Person::hentTabell()],
            '`' . Adgang::hentTabell() . '`.`personid` = `' . Person::hentTabell() . '`.`' . Person::hentPrimærnøkkelfelt() . '`'
        );
    }

    /**
     * @return Adgangsett
     * @throws Exception
     */
    public function leggTilPersonModell(): Adgangsett
    {
        return $this
            ->leggTilLeftJoinForPerson()
            ->leggTilModell(Person::class)
            ;
    }
}