<?php

namespace Kyegil\Leiebasen\Modell\Person;


use DateInterval;
use DateTimeInterface;
use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Brukerprofil\Engangsbillett;
use Kyegil\Leiebasen\Modell\Person\Brukerprofil\Engangsbillettsett;

/**
 * Class Brukerprofil
 * @package Kyegil\Leiebasen\Modell\Person
 *
 * @property integer $id
 * @property string $login
 * @property string $passord
 * @property Person|null $person
 *
 * @method integer hentId()
 * @method string hentLogin()
 * @method string hentPassord()
 * @method Person|null hentPerson()
 *
 * @method $this settLogin(string $login)
 * @method $this settPassord(string $passord)
 * @method $this settPerson(Person $person)
 */
class Brukerprofil extends Modell
{
    public static string $passordHint = 'Passordet må bestå av minst 8 tegn inkludert 1 siffer.';
    public static ?int $passordLengde = 8;
    public static string $passordRegex = "^.*[0-9].*$";
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'brukerprofiler';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Brukerprofilsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'login'    => [
                'type'  => 'string'
            ],
            'passord'    => [
                'type'  => 'string'
            ],
            'person_id'    => [
                'type'  => Person::class,
                'target' => 'person',
                'allowNull' => true
            ],
        ];
    }

    /**
     * @param string[] $passordpar
     * @return string
     * @throws Exception
     */
    public static function validerPassord(array $passordpar): string
    {
        if(count($passordpar) !=2 || trim($passordpar[0]) != trim($passordpar[1])) {
            throw new Exception('Passordet er ikke gjentatt riktig.');
        }
        $passord = reset($passordpar);
        if(!preg_match('/' . self::$passordRegex . '/', $passord)) {
            throw new Exception('Passordet oppfyller ikke kravene.');
        }
        return password_hash($passord, PASSWORD_DEFAULT);
    }

    /**
     * @return int|null
     */
    public function hentPersonId():? int
    {
        $person = $this->hentPerson();
        return $person ? $person->id : null;
    }

    /**
     * @return string
     */
    public function hentBrukernavn(): string
    {
        return $this->hentLogin();
    }

    /**
     * @param string $brukernavn
     * @return bool
     */
    public function kanBrukernavnBenyttes(string $brukernavn): bool
    {
        return $this->app->hentAutoriserer()->erLoginTilgjengelig($brukernavn, $this->hentPersonId());
    }

    /**
     * @param string $brukernavn
     * @return $this
     * @throws Exception
     */
    public function settBrukernavn(string $brukernavn): Brukerprofil
    {
        return $this->settLogin($brukernavn);
    }

    /**
     * @param $epost
     * @return $this
     * @throws Exception
     */
    public function settEpost($epost): Brukerprofil
    {
        $this->hentPerson()->settEpost($epost);
        return $this;
    }

    /**
     * @return Engangsbillettsett
     * @throws Exception
     */
    public function hentEngangsbilletter(): Engangsbillettsett
    {
        /** @var Engangsbillettsett $engangsbilletter */
        $engangsbilletter = $this->app->hentSamling(Engangsbillett::class)
            ->leggTilFilter([
                'brukerprofil_id' => $this->hentId()
            ])
            ->låsFiltre();
        if(!isset($this->samlinger->engangsbilletter)) {
            $this->samlinger->engangsbilletter = $engangsbilletter->hentElementer();
        }
        $engangsbilletter->lastFra($this->samlinger->engangsbilletter);

        return clone $engangsbilletter;
    }

    /**
     * @throws Exception
     */
    public function slett()
    {
        $this->hentEngangsbilletter()->deleteEach();
        parent::slett();
    }

    /**
     * @param string|null $url
     * @param DateTimeInterface|null $utløper
     * @return Engangsbillett|null
     * @throws Exception
     */
    public function opprettEngangsbillett(?string $url = null, ?DateTimeInterface $utløper = null):?Engangsbillett
    {
        $engangsbillett = null;
        if($this->hentId()) {
            /** @var Engangsbillett $engangsbillett */
            $engangsbillett = $this->app->nyModell(Engangsbillett::class, (object)[
                'url' => $url ?: $this->app->http_host . '/sentral/index.php?oppslag=profil_skjema&returi=default',
                'brukerprofil' => $this,
                'utløper' => $utløper ?? date_create_immutable()->add(new DateInterval('PT6H'))
            ]);
        }
        return $engangsbillett;
    }
}