<?php

namespace Kyegil\Leiebasen\Modell\Person\Brukerprofil;


use DateTimeInterface;
use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Person\Brukerprofil;
use stdClass;

/**
 * Class Engangsbillett
 * @package Kyegil\Leiebasen\Modell\Person\Brukerprofil
 *
 * @property integer $id
 * @property Brukerprofil $brukerprofil
 * @property string $billett
 * @property string $url
 * @property DateTimeInterface $utløper
 *
 * @method integer hentId()
 * @method Brukerprofil hentBrukerprofil()
 * @method string hentBillett()
 * @method string hentUrl()
 * @method DateTimeInterface hentUtløper()
 *
 * @method $this settBrukerprofil(Brukerprofil $brukerprofil)
 * @method $this settUrl(string $url)
 * @method $this settUtløper(DateTimeInterface $utløper)
 */
class Engangsbillett extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'engangsbilletter';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Engangsbillettsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'brukerprofil_id'    => [
                'type'  => Brukerprofil::class,
                'target' => 'brukerprofil',
                'allowNull' => true,
            ],
            'billett'    => [
                'type'  => 'string'
            ],
            'url'    => [
                'type'  => 'string'
            ],
            'utløper'    => [
                'type'  => 'datetime',
                'allowNull' => true,
            ],
        ];
    }

    /**
     * @param stdClass $parametere
     * @return Modell
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Modell
    {
        if(!isset($parametere->brukerprofil)) {
            throw new Exception('Brukerprofil er påkrevet for å opprette engangsbillett');
        }
        if (empty($parametere->utløper)) {
            $parametere->utløper = date_create_immutable()->add(new \DateInterval('PT6H'));
        }
        $parametere->billett = hash('sha512', (time() * mt_rand() * (int)strval($parametere->brukerprofil)));
        return parent::opprett($parametere);
    }
}