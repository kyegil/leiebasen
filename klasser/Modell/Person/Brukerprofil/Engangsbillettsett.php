<?php

namespace Kyegil\Leiebasen\Modell\Person\Brukerprofil;


use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Sett;

/**
 * Class Engangsbillettsett
 * @package Kyegil\Leiebasen\Modell\Person\Brukerprofil
 *
 * @method Engangsbillett current()
 */
class Engangsbillettsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Engangsbillett::class;

    /**
     * @var Engangsbillett[]
     */
    protected ?array $items = null;

    /**
     * @param string|null $alias
     * @return Engangsbillettsett
     */
    public function leggTilLeftJoinForBrukerprofil(?string $alias = null): Engangsbillettsett
    {
        $alias = $alias ?: Person\Brukerprofil::hentTabell();
        return $this->leggTilLeftJoin(
            [$alias => Person\Brukerprofil::hentTabell()],
            Engangsbillett::hentTabell() . '.`brukerprofil_id` = ' . Person\Brukerprofil::hentTabell() . '.' . Person\Brukerprofil::hentPrimærnøkkelfelt()
        );
    }

    /**
     * @return Engangsbillettsett
     */
    public function leggTilBrukerprofilModell(): Engangsbillettsett
    {
        return $this
            ->leggTilLeftJoinForBrukerprofil()
            ->leggTilModell(Person\Brukerprofil::class)
            ;
    }
}