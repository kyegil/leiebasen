<?php

namespace Kyegil\Leiebasen\Modell;


use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Exception;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Blanding;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Leieregulerer;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløpsett;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andel;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andelsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Andelsperiode;
use Kyegil\Leiebasen\Modell\Leieforhold\Andelsperiodesett;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan\Avdrag;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan\Originalkrav;
use Kyegil\Leiebasen\Modell\Leieforhold\Depositum;
use Kyegil\Leiebasen\Modell\Leieforhold\Fbo;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontraktsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav\LeieforholdDelkravtype;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav\LeieforholdDelkravtypesett;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietakersett;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Modell\Leieforhold\Notatsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Oppsigelse;
use Kyegil\Leiebasen\Modell\Leieforhold\Purring;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Modell\Person\Adgangsett;
use Kyegil\Leiebasen\Modell\Person\EfakturaIdentifier;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Adressefelt as LeieforholdAdressefelt;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Leietakerfelt;
use Kyegil\Leiebasen\Visning\felles\html\leieobjekt\Toalettbeskrivelse;
use Kyegil\Leiebasen\Visning\felles\html\Utleierfelt;
use stdClass;
use Throwable;


/**
 * Class Leieforhold
 * @package Kyegil\Leiebasen
 *
 * @property int $leieforholdnr
 * @property string $kode
 * @property string $navn
 * @property string $beskrivelse
 * @property Leieobjekt $leieobjekt
 * @property DateTime $fradato
 * @property DateTime|null $tildato Til-dato oppgis kun ved midlertidig leieforhold
 * @property DateInterval $oppsigelsestid
 * @property float $årligBasisleie Årlig leiebeløp eksklusive evt delbeløp
 * @property float $leiebeløp Leiebeløp per termin inklusive evt delbeløp
 * @property integer $antTerminer Antall leieterminer per år
 * @property string $avtaletekstmal Avtaletekst med plassholdere for alle variabler
 * @property Kontrakt $kontrakt
 * @property boolean $frosset Leieforholdet er frosset (arkivert). Det sendes ikke ut purringer.
 * @property boolean $stoppOppfølging Leieforholdet og evt utestående er avskrevet og skal ikke følges opp videre.
 * @property DateTime|null $avventOppfølging Evt videre oppfølging skal avventes inntil dato
 * @property boolean $regningTilObjekt Regninger kan leveres direkte til leieobjektets adresse, eller annet internt leieobjekt
 * @property Leieobjekt|null $regningsobjekt Leieobjektet som bruker for interne regningsleveringer
 * @property Person|null $regningsperson Den av leietakerne hvis adresse regningene skal sendes til. Null for å bruke en egen regningsadresse
 * @property string $regningsadresse1
 * @property string $regningsadresse2
 * @property string $postnr
 * @property string $poststed
 * @property string $land
 * @property string $giroformat
 * @property object|null $betalingsplanForslag
 * @property string|null $forfallFastDato Fast årlig forfallsdato i formatet 'm-d'
 * @property string|null $forfallFastDagIMåneden Fast månedlig forfallsdato
 * @property integer|null $forfallFastUkedag Fast ukedag for forfall 1–7
 * @property object|stdClass $leieHistorikk
 * @property DateTimeImmutable|false $leieOppdatert
 * @property Personsett|null $andreBeboere
 * @property DateInterval|null $terminBetalingsfrist
 *
 * @method int hentLeieforholdnr()
 * @method string hentNavn()
 * @method string hentBeskrivelse()
 * @method Leieobjekt hentLeieobjekt()
 * @method DateTime hentFradato()
 * @method DateTime|null hentTildato()
 * @method DateInterval hentOppsigelsestid()
 * @method float hentÅrligBasisleie()
 * @method float hentLeiebeløp()
 * @method int hentAntTerminer()
 * @method string hentAvtaletekstmal()
 * @method Kontrakt hentKontrakt()
 * @method boolean hentFrosset()
 * @method boolean hentStoppOppfølging()
 * @method bool hentRegningTilObjekt()
 * @method Leieobjekt|null hentRegningsobjekt()
 * @method Person|null hentRegningsperson()
 * @method string hentRegningsadresse1()
 * @method string hentRegningsadresse2()
 * @method string hentPostnr()
 * @method string hentPoststed()
 * @method string hentLand()
 * @method object|null hentBetalingsplanForslag()
 * @method object|null hentLeieHistorikk()
 * @method string|null hentForfallFastDato()
 * @method string|null hentForfallFastDagIMåneden()
 * @method int|null hentForfallFastUkedag()
 * @method Personsett|null hentAndreBeboere()
 *
 * @method $this settLeieforholdnr(int $id)
 * @method $this settKode(string $kode)
 * @method $this settLeieobjekt(Leieobjekt $leieobjekt)
 * @method $this settFradato(DateTime $fradato)
 * @method $this settOppsigelsestid(DateInterval $oppsigelsestid)
 * @method $this settAvtaletekstmal(string $avtaletekstmal)
 * @method $this settFrosset(boolean $frosset)
 * @method $this settStoppOppfølging(boolean $stoppOppfølging)
 * @method $this settAvventOppfølging(DateTime|null $avventOppfølging)
 * @method $this settRegningTilObjekt(boolean $regningTilObjekt)
 * @method $this settRegningsobjekt(Leieobjekt|null $regningsobjekt)
 * @method $this settRegningsadresse1(string $regningsadresse1)
 * @method $this settRegningsadresse2(string $regningsadresse2)
 * @method $this settPostnr(string $postnr)
 * @method $this settPoststed(string $poststed)
 * @method $this settLand(string $land)
 * @method $this settGiroformat(string $giroformat)
 * @method $this settKontrakt(Kontrakt $kontrakt)
 * @method $this settBetalingsplanForslag(object|null $betalingsplanForslag)
 * @method $this settAndreBeboere(Personsett|null $andreBeboere)
 */
class Leieforhold extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'leieforhold';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'leieforholdnr';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Leieforholdsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /** @var array<string, Fraction>|null  */
    protected ?array $andeler;

    public static function harFlere(string $reference, ?string $model = null, ?string $foreignKey = null, array $filters = [], ?callable $callback = null): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::$one2Many['innbetalinger'] = [
                'model' => Innbetaling::class,
                'foreignTable' =>  'delbeløp',
                'foreignKey' => 'leieforhold',
                'filters' => [
                    ['`' . Innbetaling::hentTabell() . '`.`konto` !=' => '0']
                ],
                'callback' => function(Innbetalingsett $innbetalingsett) {
                    $innbetalingsett->leggTilInnerJoin(['delbeløp' => Delbeløp::hentTabell()],
                        '`' . Innbetaling::hentTabell(). '`.`' . Innbetaling::hentPrimærnøkkelfelt() . '` = `delbeløp`.`innbetaling`'
                    )->leggTilSortering('dato');
                }
            ];
            self::harFlere('andelsperioder',
                Andelsperiode::class,
                'leieforhold', [],
                function($sett){
                    $sett->leggTilSortering('fradato');
                }
            );
            self::harFlere('kontrakter',
                Kontrakt::class,
                'leieforhold', [],
                function($sett){
                    $sett->leggTilSortering(Kontrakt::hentPrimærnøkkelfelt(), true);
                }
            );
            self::harFlere('leietakere',
                Leietaker::class,
                'leieforhold',[
                    ['`' . Leietaker::hentTabell() . '`.`slettet`' => null]
                ],
                function(Leietakersett $leietakere){
                    $leietakere->leggTilPersonModell()->leggTilKontraktModell();
                }
            );
            self::harFlere('krav', Krav::class, 'leieforhold');
            self::harFlere('innbetaling_delbeløp',
                Delbeløp::class,
                'leieforhold', [
                    ['`' . Delbeløp::hentTabell() . '`.`konto` !=' => 0]
                ],
                function(Delbeløpsett $delbeløp) {
                    $delbeløp->leggTilSortering('dato');
                }
            );
            self::harFlere('delkravtyper',
                LeieforholdDelkravtype::class,
                'leieforhold', [
                    ['`' . Delkravtype::hentTabell() . '`.`aktiv`' => true]
                ],
                function (LeieforholdDelkravtypesett $delkravtyper) {
                    $delkravtyper->leggTilDelkravTypeModell()
                        ->leggTilSortering('orden', false, Delkravtype::hentTabell());
                }
            );
            self::harFlere('notater',
                Notat::class,
                'leieforhold'
            );
            self::harFlere('fbo', Fbo::class, 'leieforhold');
            self::harFlere('adganger', Adgang::class, 'leieforhold');
            self::harFlere('depositum', Depositum::class, 'leieforhold_id');
        }
        return parent::harFlere($reference, $model, $foreignKey, $filters, $callback);
    }

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'leieforholdnr'    => [
                'type'  => 'integer'
            ],
            'kode'    => [
                'type'  => 'string'
            ],
            'navn'    => [
                'type'  => 'string'
            ],
            'beskrivelse'    => [
                'type'  => 'string'
            ],
            'leieobjekt'    => [
                'type'  => Leieobjekt::class,
                'rawDataContainer' => 'leieobjekt'
            ],
            'fradato'    => [
                'type'  => 'date'
            ],
            'tildato'    => [
                'type'  => 'date',
                'allowNull' => true,
            ],
            'oppsigelsestid'    => [
                'type'  => 'interval'
            ],
            'årlig_basisleie'    => [
                'type' => 'string'
            ],
            'leiebeløp'    => [
                'type'  => 'string'
            ],
            'ant_terminer'    => [
                'type'  => 'integer'
            ],
            'termin_betalingsfrist'    => [
                'type'  => 'interval',
                'allowNull' => true,
            ],
            'avtaletekstmal'    => [
                'type'  => 'string'
            ],
            'siste_kontrakt' => [
                'target' => 'kontrakt',
                'type' => Kontrakt::class,
                'rawDataContainer' => 'kontrakt'
            ],
            'frosset'    => [
                'type'  => 'boolean'
            ],
            'stopp_oppfølging'    => [
                'type'  => 'boolean'
            ],
            'regning_til_objekt'    => [
                'type'  => 'boolean'
            ],
            'avvent_oppfølging'    => [
                'type'  => 'date',
                'allowNull'  => true
            ],
            'regningsperson'    => [
                'type'  => Person::class,
                'allowNull'  => true,
                'rawDataContainer' => 'regningsperson'
            ],
            'regningsobjekt'    => [
                'type'  => Leieobjekt::class,
                'allowNull'  => true,
                'rawDataContainer' => 'regningsobjekt'
            ],
            'regningsadresse1'    => [
                'type'  => 'string'
            ],
            'regningsadresse2'    => [
                'type'  => 'string'
            ],
            'postnr'    => [
                'type'  => 'string'
            ],
            'poststed'    => [
                'type'  => 'string'
            ],
            'land'    => [
                'type'  => 'string'
            ],
            'giroformat'    => [
                'type'  => 'string'
            ]
        ];
    }

    /**
     * @param stdClass $parametere
     * @return Leieforhold
     * @throws Throwable
     */
    public function opprett(stdClass $parametere): Leieforhold
    {
        if(!isset($parametere->andel)) {
            $parametere->andel = new Fraction(1);
        }
        if(
            !isset($parametere->leieobjekt)
            || !($parametere->leieobjekt instanceof Leieobjekt)
            || !$parametere->leieobjekt->hentId()
        ) {
            throw new Exception('Ugyldig leieobjekt');
        }
        if(
            !isset($parametere->fradato)
            || !($parametere->fradato instanceof DateTimeInterface)
        ) {
            throw new Exception('Ugyldig fra-dato');
        }
        if(
            isset($parametere->tildato)
            && !($parametere->tildato instanceof DateTimeInterface)
        ) {
            throw new Exception('Ugyldig til-dato');
        }
        if(
            isset($parametere->tildato)
            && $parametere->tildato < $parametere->fradato
        ) {
            throw new Exception('Til-dato kan ikke være tidligere enn fra-dato.');
        }
        if(
            !isset($parametere->oppsigelsestid)
            || !($parametere->oppsigelsestid instanceof DateInterval)
        ) {
            throw new Exception('Ugyldig oppsigelsestid');
        }
        if(
            !isset($parametere->leietakere)
            || !($parametere->leietakere instanceof Personsett)
            || !$parametere->leietakere->hentAntall()
        ) {
            throw new Exception('Leietakere mangler');
        }
        if(!isset($parametere->avtaletekstmal)) {
            throw new Exception('Avtaletekst mangler');
        }

        if($parametere->fradato instanceof DateTime) {
            $parametere->fradato = DateTimeImmutable::createFromMutable($parametere->fradato);
        }
        if($parametere->tildato instanceof DateTime) {
            $parametere->tildato = DateTimeImmutable::createFromMutable($parametere->tildato);
        }

        if(
            $parametere->leieobjekt->hentLedighetForTidsrom($parametere->fradato, $parametere->tildato)
                ->compare('<', $parametere->andel)) {
            throw new Exception('Leieobjektet er ikke ledig i dette tidsrommet');
        }

        /** @var Kontrakt $kontrakt */
        $kontrakt = $this->app->nyModell(Kontrakt::class, (object)[
            'dato' => $parametere->fradato,
            'tildato' => $parametere->tildato,
        ]);

        $parametere->leieforholdnr = $kontrakt->hentId();
        $parametere->kontrakt = $kontrakt;

        parent::opprett($parametere);
        
        $kontrakt->leieforhold = $this;
        $kontrakt->synkUtdaterteFelter();

        /** @var Person|null $person */
        foreach($parametere->leietakere as $person) {
            $kontrakt->leggTilLeietaker($person);
        }
        $this->settTilfeldigRegningsperson();

        $this->app->nyModell(Andelsperiode::class, (object)[
            'leieforhold' => $this,
            'leieobjekt' => $parametere->leieobjekt,
            'andel' => $parametere->andel,
            'fradato' => $parametere->fradato,
        ]);
        return $this;
    }

    /**
     * @param string|null $navn
     * @return Leieforhold
     * @throws Exception
     */
    public function settNavn(?string $navn = null): Leieforhold
    {
        if ($navn === null) {
            $navnArray = [];
            $leietakere = $this->hentLeietakere();
            /** @var Leietaker $leietaker */
            foreach ($leietakere as $leietaker) {
                $navnArray[] = $leietaker->hentNavn();
            }
            $navn = $this->app->liste($navnArray);
        }
        return $this->sett('navn', $navn);
    }

    /**
     * @param string|null $beskrivelse
     * @return Leieforhold
     * @throws Exception
     */
    public function settBeskrivelse(?string $beskrivelse = null): Leieforhold
    {
        if ($beskrivelse === null) {
            /** @var Leieobjekt $leieobjekt */
            $leieobjekt = $this->hentLeieobjekt();
            $beskrivelse = $this->hentNavn() . ' i ' . $leieobjekt->hentBeskrivelse();
        }
        return $this->sett('beskrivelse', $beskrivelse);

    }

    /**
     * @return string
     * @throws Exception
     */
    public function hentKode():string
    {
        return $this->hent('kode') ?? $this->hentId();
    }

    /**
     * @return Kontraktsett
     * @throws Exception
     */
    public function hentKontrakter():Kontraktsett
    {
        /** @var Kontraktsett $kontrakter */
        $kontrakter = $this->hentSamling('kontrakter');
        return $kontrakter;
    }

    /**
     * @return Andelsperiodesett
     * @throws Exception
     */
    public function hentAndelsperioder():Andelsperiodesett
    {
        /** @var Andelsperiodesett $andelsperioder */
        $andelsperioder = $this->hentSamling('andelsperioder');
        return $andelsperioder;
    }

    /**
     * Henter leietakere som Leietakersett
     *
     * @return Leietakersett
     * @throws Exception
     */
    public function hentLeietakere():Leietakersett
    {
        /** @var Leietakersett $leietakere */
        $leietakere = $this->hentSamling('leietakere');
        return $leietakere;
    }

    /**
     * @return Kravsett
     * @throws Exception
     */
    public function hentKrav(): Kravsett {
        /** @var Kravsett $krav */
        $krav = $this->hentSamling('krav');
        return $krav;
    }

    /**
     * Henter alle innbetalings-delbeløp som er kontert til leieforholdet
     *
     * @return Delbeløpsett
     * @throws Exception
     */
    public function hentInnbetalingDelbeløp(): Delbeløpsett
    {
        /** @var Delbeløpsett $delbeløp */
        $delbeløp = $this->hentSamling('innbetaling_delbeløp');
        return $delbeløp;
    }

    /**
     * Henter alle innbetalinger som er helt eller delvis kontert til leieforholdet
     *
     * @return Innbetalingsett
     * @throws Exception
     */
    public function hentInnbetalinger(): Innbetalingsett
    {
        /** @var Innbetalingsett $innbetalinger */
        $innbetalinger = $this->hentSamling('innbetalinger');
        return $innbetalinger;
    }

    /**
     * Hent transaksjoner
     *
     * Transaksjonene kan være krav eller betalinger, og er sortert etter dato i stigende rekkefølge.
     *
     * @param bool $inkluderFramtidigeKrav Inkluder planlagte transaksjoner
     * @return Blanding
     * @throws Exception
     */
    public function hentTransaksjoner(bool $inkluderFramtidigeKrav = false): Blanding
    {
        $innbetalingssett = clone $this->hentInnbetalinger();
        $innbetalingssett->inkluder('delbeløp');
        $kravsett = clone $this->hentKrav();
        if(!$inkluderFramtidigeKrav) {
            $kravsett->leggTilFilter([
                'kravdato <= NOW()'
            ]);
        }
        if(!isset($this->samlinger->transaksjoner)) {
            /** @var Blanding $transaksjoner */
            $transaksjoner = $this->app->hentSamling([$innbetalingssett, $kravsett])
                /** @var Krav|Innbetaling $transaksjon1 */
                /** @var Krav|Innbetaling $transaksjon2 */
                ->sorterLastede(function($transaksjon1, $transaksjon2 ) {
                    /** @var DateTimeInterface $dato1 */
                    $dato1 = $transaksjon1 instanceof Krav ? $transaksjon1->hentKravdato() : $transaksjon1->hentDato();
                    /** @var DateTimeInterface $dato2 */
                    $dato2 = $transaksjon2 instanceof Krav ? $transaksjon2->hentKravdato() : $transaksjon2->hentDato();

                    if( $dato1 < $dato2 ) {
                        return -1;
                    }
                    if( $dato1->format('Y-m-d') > $dato2->format('Y-m-d') ) {
                        return 1;
                    }

                    // Dersom datoene er like vil innbetalingene sorteres før kravene
                    if ( $transaksjon1 instanceof Innbetaling and $transaksjon2 instanceof Krav ) {
                        return -1;
                    }
                    if ( $transaksjon1 instanceof Krav and $transaksjon2 instanceof Innbetaling ) {
                        return 1;
                    }

                    // Innenfor samme dato vil to transaksjoner av samme type sorteres etter id
                    if (get_class($transaksjon1) == get_class($transaksjon2 )) {
                        return strcmp($transaksjon1->hentId(), $transaksjon2->hentId());
                    }

                    return 0;
                });
            $this->samlinger->transaksjoner = $transaksjoner;
        }
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $this->samlinger->transaksjoner;
    }

    /**
     * @return Kravsett
     * @throws Exception
     */
    public function hentUteståendeKrav(): Kravsett
    {
        /** @var Kravsett $uteståendeKrav */
        $uteståendeKrav = $this->hentKrav()
            ->leggTilFilter(['krav.utestående >' => 0])
            ->leggTilFilter(['krav.kravdato <=' => date('Y-m-d ')]);
        $uteståendeKrav->leggTilSortering('kravdato');

        if(!isset($this->samlinger->uteståendeKrav)) {
            $this->samlinger->uteståendeKrav = $uteståendeKrav->hentElementer();
            /** @var float $uteståendeBeløp */
            $uteståendeBeløp = 0;
            /** @var float $forfaltBeløp */
            $forfaltBeløp = 0;
            /** @var Krav $krav */
            foreach($uteståendeKrav as $krav) {
                $uteståendeBeløp += $krav->utestående;
                $forfaltBeløp += $krav->forfall && $krav->forfall <= date_create() ? $krav->utestående : 0;
            }
            $this->data->utestående = $uteståendeBeløp;
            $this->data->forfalt = $forfaltBeløp;
        }
        $uteståendeKrav->lastFra($this->samlinger->uteståendeKrav);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $uteståendeKrav;
    }

    /**
     * @return Kravsett
     * @throws Exception
     */
    public function hentFramtidigeKrav(): Kravsett
    {
        /** @var Kravsett $framtidigeKrav */
        $framtidigeKrav = $this->app->hentSamling(Krav::class)
            ->leggTilFilter(['leieforhold' => $this->hentId()])
            ->leggTilFilter(['krav.kravdato >' => date('Y-m-d ')]);
        $framtidigeKrav->leggTilSortering('kravdato');

        if(!isset($this->samlinger->framtidigeKrav)) {
            $this->samlinger->framtidigeKrav = $framtidigeKrav->hentElementer();
        }
        $framtidigeKrav->lastFra($this->samlinger->framtidigeKrav);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $framtidigeKrav;
    }

    /**
     * Hent oppsigelse på leieforholdet.
     *
     * Metoden kan returnere false av tilbakevirkende hensyn,
     * men denne returverdien må ikke brukes.
     * I framtiden kan verdien endres til null.
     *
     * @return false|Oppsigelse|null
     * @throws Exception
     */
    public function hentOppsigelse()
    {
        if(!isset($this->data->oppsigelse)) {
            /** @var Oppsigelse $oppsigelse */
            $oppsigelse = $this->app->hentModell(Oppsigelse::class, $this->hentId());
            if (property_exists($this->rawData, 'oppsigelser')) {
                $lastet = $oppsigelse::mapNamespacedRawData($this->rawData->oppsigelser);
                $oppsigelse->setPreloadedValues($lastet);
            }

            $this->data->oppsigelse = $oppsigelse->hentId() ? $oppsigelse : false;
        }
        return $this->data->oppsigelse;
    }

    /**
     * @return Depositum|null
     * @throws Exception
     */
    public function hentDepositum(): ?Depositum
    {
        if(!property_exists($this->data, 'depositum')) {
            /** @var Depositum|null $depositum */
            $depositum = $this->hentSamling('depositum')->hentFørste();
            $this->data->depositum = $depositum;
        }
        return $this->data->depositum;
    }

    /**
     * @return Betalingsplan|null
     * @throws Exception
     */
    public function hentBetalingsplan(): ?Betalingsplan
    {
        if(!property_exists($this->data, 'betalingsplan')) {
            /** @var Betalingsplan $betalingsplan */
            $betalingsplan = $this->app->hentModell(Betalingsplan::class, $this->hentId());
            if (property_exists($this->rawData, 'betalingsplaner')) {
                $lastet = $betalingsplan::mapNamespacedRawData($this->rawData->betalingsplaner);
                $betalingsplan->setPreloadedValues($lastet);
            }

            $this->data->betalingsplan = $betalingsplan->hentId() ? $betalingsplan : null;
        }
        return $this->data->betalingsplan;
    }

    /**
     * Returnerer betalingsplanen kun om den er aktiv
     *
     * @return Betalingsplan|null
     * @throws Exception
     */
    public function hentAktivBetalingsplan(): ?Betalingsplan
    {
        $betalingsplan = $this->hentBetalingsplan();
        return $betalingsplan && $betalingsplan->hentAktiv() ? $betalingsplan : null;
    }

    /**
     * Hent terminlengde
     *
     * @return DateInterval
     * @throws Exception
     */
    public function hentTerminlengde(): DateInterval
    {
        if(!isset($this->data->terminlengde)) {
            $antTerminer = $this->hentAntTerminer() ?: 1;
            switch($antTerminer) {
                case 1:
                    $this->data->terminlengde = new DateInterval("P1Y");
                    break;
                case 2:
                case 3:
                case 4:
                case 6:
                case 12:
                    $this->data->terminlengde = new DateInterval("P" . (12 / $antTerminer) . "M");
                    break;
                case 13:
                case 26:
                case 52:
                    $this->data->terminlengde = new DateInterval("P" . (364 / $antTerminer) . "D");
                    break;
                default:
                    $this->data->terminlengde = new DateInterval("P" . round(365 / $antTerminer) . "D");
            }
        }
        return $this->data->terminlengde;
    }

    /**
     * @return string
     * @throws Throwable
     */
    public function hentKid(): string {
        $this->app->pre($this, __FUNCTION__, []);
        if(!isset($this->data->kid)) {
            $this->data->kid = $this->app->hentKidBestyrer()->genererKid($this->hentId());
        }
        return $this->app->post($this, __FUNCTION__, $this->data->kid);
    }

    /**
     * @param string $type 'alle', 'husleie', eller 'fellesstrøm'
     * @return Fbo|false
     * @throws Exception
     */
    public function hentFbo(string $type = 'alle') {
        $type = strtolower($type);
        if($type != 'husleie' && $type != 'fellesstrøm') {
            $type = 'alle';
        }
        if(!isset($this->data->fbo)) {
            $this->data->fbo = (object)[
                'alle' => false,
                'husleie' => false,
                'fellesstrøm' => false
            ];
            /** @var Fbo $fbo */
            foreach($this->hentSamling('fbo') as $fbo) {
                if($fbo->type == Fbo::AVTALETYPER[Fbo::AVTALETYPE_ALLE]) {
                    $this->data->fbo->alle = $fbo;
                    $this->data->fbo->husleie = $fbo;
                    $this->data->fbo->fellesstrøm = $fbo;
                }
                elseif ($fbo->type == Fbo::AVTALETYPER[Fbo::AVTALETYPE_HUSLEIE]) {
                    $this->data->fbo->husleie = $fbo;
                }
                elseif ($fbo->type == Fbo::AVTALETYPER[Fbo::AVTALETYPE_FELLESSTRØM]) {
                    $this->data->fbo->fellesstrøm = $fbo;
                }
            }
        }
        return $this->data->fbo->$type;
    }

    /**
     * @param string $fboType
     * @return bool
     * @throws Exception
     */
    public function hentFboOgEfaktura(string $fboType = 'alle'): bool
    {
        return
            $this->app->hentValg('efaktura')
            && $this->app->hentValg('avtalegiro')
            && $this->hentFbo($fboType)
            && $this->hentEfakturaIdentifier();
    }

    /**
     * Returnerer alle aktive tillegg- og delkravtyper avtalt i leieforholdet
     *
     * @return LeieforholdDelkravtypesett
     * @throws Exception
     */
    public function hentDelkravtyper(): LeieforholdDelkravtypesett
    {
        $this->app->pre($this, __FUNCTION__, []);
        /** @var LeieforholdDelkravtypesett $delkravtyper */
        $delkravtyper = $this->hentSamling('delkravtyper');
        return $this->app->post($this, __FUNCTION__, $delkravtyper);
    }

    /**
     * @param string|null $kode
     * @return LeieforholdDelkravtype|null
     * @throws Exception
     */
    public function hentDelkravtype(?string $kode): ?LeieforholdDelkravtype
    {
        list('kode' => $kode)
            = $this->app->pre($this, __FUNCTION__, [
            'kode' => $kode
        ]);
        if ($kode) {
            /** @var LeieforholdDelkravtype $delkravtype */
            foreach ($this->hentDelkravtyper() as $delkravtype) {
                if ($kode == $delkravtype->hentKode()) {
                    return $this->app->post($this, __FUNCTION__, $delkravtype);
                }
            }
        }
        return $this->app->post($this, __FUNCTION__, null);
    }

    /**
     * @param int|null $id
     * @return LeieforholdDelkravtype|null
     * @throws Exception
     */
    public function hentDelkravtypeMedId(?int $id): ?LeieforholdDelkravtype
    {
        list('id' => $id)
            = $this->app->pre($this, __FUNCTION__, [
            'id' => $id
        ]);
        if (isset($id)) {
            /** @var LeieforholdDelkravtype $delkravtype */
            foreach ($this->hentDelkravtyper() as $delkravtype) {
                if ($delkravtype->delkravtype && $delkravtype->delkravtype->hentId() == $id) {
                    return $this->app->post($this, __FUNCTION__, $delkravtype);
                }
            }
        }
        return $this->app->post($this, __FUNCTION__, null);
    }

    /**
     * Henter regningsadresse som et objekt med føgende felter:
     * navn, adresse1, adresse2, postnr, poststed, land
     *
     * @return stdClass
     * @throws Exception
     */
    public function hentAdresse()
    {
        if(!isset($this->data->adresse)) {
            $this->data->adresse = (object)[
                'navn'        => $this->hentNavn(),
                'adresse1'    => $this->hentRegningsadresse1(),
                'adresse2'    => $this->hentRegningsadresse2(),
                'postnr'    => $this->hentPostnr(),
                'poststed'    => $this->hentPoststed(),
                'land'        => $this->hentLand()
            ];

            /** @var Leieobjekt $regningsobjekt */
            $regningsobjekt = $this->hentRegningsobjekt();
            /** @var Person $regningsperson */
            $regningsperson = $this->hentRegningsperson();

            if($this->hentRegningTilObjekt()) {
                $this->data->adresse = (object)[
                    'navn'        => $this->hentNavn(),
                    'adresse1'    => ($regningsobjekt->hentNavn() ?: $regningsobjekt->hentGateadresse()),
                    'adresse2'    => ($regningsobjekt->hentNavn() ? $regningsobjekt->hentGateadresse() : null),
                    'postnr'    => $regningsobjekt->hentPostnr(),
                    'poststed'    => $regningsobjekt->hentPoststed(),
                    'land'        => ""
                ];
            }
            elseif ($regningsperson) {
                $this->data->adresse = (object)[
                    'navn'        => $this->hentNavn(),
                    'adresse1'    => $regningsperson->hentAdresse1(),
                    'adresse2'    => $regningsperson->hentAdresse2(),
                    'postnr'    => $regningsperson->hentPostnr(),
                    'poststed'    => $regningsperson->hentPoststed(),
                    'land'        => $regningsperson->hentLand()
                ];
            }
        }
        return $this->data->adresse;
    }

    /**
     * Gjengi avtaletekst
     *
     * Gjengir avtaletekst i siste leieavtale,
     * eller i en bestemt avtale dersom en er oppgitt.
     *
     * @param bool $utfylt Avtaleteksten returneres utfylt med variabler
     * @param Kontrakt|null $kontrakt
     * @param string|null $mal Evt. ny tekst som skal fylles med kontraktverdiene
     * @return string Avtaleteksten
     * @throws Throwable
     */
    public function gjengiAvtaletekst(
        bool      $utfylt = true,
        ?Kontrakt $kontrakt = null,
        ?string   $mal = null
    ): string
    {
        list(
            'utfylt' => $utfylt,
            'kontrakt' => $kontrakt,
            'mal' => $mal,
        ) = $this->app->pre($this, __FUNCTION__, [
            'utfylt' => $utfylt,
            'kontrakt' => $kontrakt,
            'mal' => $mal,
        ]);
        $leieobjekt = $this->leieobjekt;
        $kontrakt = $kontrakt ?: $this->hentKontrakt();
        $mal = $mal ?? $this->hentAvtaletekstmal();
        $kontraktdato = $kontrakt->dato;
        $tildato = $kontrakt->tildato;

        if( !$utfylt ) {
            return $mal;
        }

        $utleierfelt = $this->app->vis(Utleierfelt::class);

        $toalett = $this->app->vis(Toalettbeskrivelse::class, [
            'kategori' => $leieobjekt->toalettKategori,
            'beskrivelse' => $leieobjekt->toalett
        ]);

        $terminlengde = $this->app->periodeformat($this->hentTerminlengde(), false, true);

        $betalingsfrist = $this->app->betalingsfristFormatert($this->hentTerminBetalingsfrist());

        // Lag loop for hver leietaker
        $mal = preg_replace_callback(
            '/{for hver leietaker}(.+?){\/for hver leietaker}/s',
            function($treff) {
                $resultat = [];
                $malLeietaker = $treff[1];

                $variablerLeietakere = [
                    '{leietaker.id}',
                    '{leietaker.navn}',
                    '{leietaker.postadresse}',
                    '{leietaker.fødselsdato}',
                    '{leietaker.fødselsnummer}',
                    '{leietaker.epost}',
                    '{leietaker.telefon}',
                ];

                /** @var Leietaker $leietaker */
                foreach( $this->hentLeietakere() as $leietaker ) {
                    $person = $leietaker->person;
                    $erstatning = [
                        $leietaker->hentId(),
                        $leietaker->hentNavn(),
                        ($person ? nl2br( $person->hentPostadresse() ) : ''),
                        (
                        $person && $person->hentFødselsdato()
                            ? $person->hentFødselsdato()->format('d.m.Y')
                            : ''
                        ),
                        ($person ? $person->hentFødselsnummer() : ''),
                        ($person ? $person->hentEpost() : ''),
                        ($person ? $person->hentTelefon() : ''),
                    ];

                    $resultat[] = str_replace($variablerLeietakere, $erstatning, $malLeietaker);
                }
                return implode('', $resultat);
            },
            $mal
        );


        // Lag loop for hver øvrige beboer
        $mal = preg_replace_callback(
            '/{for hvert husstandmedlem}(.+?){\/for hvert husstandmedlem}/s',
            function($treff) {
                $resultat = [];
                $malLeietaker = $treff[1];

                $variablerLeietakere = [
                    '{husstandmedlem.id}',
                    '{husstandmedlem.navn}',
                    '{husstandmedlem.postadresse}',
                    '{husstandmedlem.fødselsdato}',
                    '{husstandmedlem.fødselsnummer}',
                    '{husstandmedlem.epost}',
                    '{husstandmedlem.telefon}',
                ];

                /** @var Leietaker $leietaker */
                foreach( $this->hentAndreBeboere() ?? [] as $person ) {
                    $erstatning = [
                        $person->hentId(),
                        $person->hentNavn(),
                        ($person ? nl2br( $person->hentPostadresse() ) : ''),
                        (
                        $person && $person->hentFødselsdato()
                            ? $person->hentFødselsdato()->format('d.m.Y')
                            : ''
                        ),
                        ($person ? $person->hentFødselsnummer() : ''),
                        ($person ? $person->hentEpost() : ''),
                        ($person ? $person->hentTelefon() : ''),
                    ];

                    $resultat[] = str_replace($variablerLeietakere, $erstatning, $malLeietaker);
                }
                return implode('', $resultat);
            },
            $mal
        );


        // Lag loop for hver delkravtype
        $mal = preg_replace_callback(
            '/{for hvert delkrav}(.+?){\/for hvert delkrav}/s',
            function($treff) {
                $resultat = [];
                $malDelkrav = $treff[1];

                $variablerDelkrav = [
                    '{delkrav.id}',
                    '{delkrav.kode}',
                    '{delkrav.navn}',
                    '{delkrav.beskrivelse}',
                    '{delkrav.beløp}',
                    '{delkrav.terminbeløp}',
                    '{delkrav.periodisk_avstemming}'
                ];

                /** @var LeieforholdDelkravtype $delkravtype */
                foreach( $this->hentDelkravtyper() as $delkravtype ) {
                    if( !$delkravtype->selvstendigTillegg ) {
                        $erstatning = [
                            $delkravtype->hentDelkravTypeId(),
                            $delkravtype->hentKode(),
                            $delkravtype->hentNavn(),
                            $delkravtype->hentBeskrivelse(),
                            $this->leiebase->kr($delkravtype->hentBeløp()),
                            $this->leiebase->kr($delkravtype->hentTerminbeløp()),
                            $delkravtype->periodiskAvstemming ? "☑︎︎" : "☐"
                        ];

                        if($delkravtype->periodiskAvstemming) {
                            $mal = preg_replace_callback(
                                '/{dersom delkrav.periodisk_avstemming}(.+?){\/dersom delkrav.periodisk_avstemming}/s',
                                function ($treff ) {
                                    return $treff[1];
                                },
                                preg_replace(
                                    '/{dersom ikke delkrav.periodisk_avstemming}(.+?){\/dersom ikke delkrav.periodisk_avstemming}/s',
                                    '',
                                    $malDelkrav)
                            );
                        }
                        else {
                            $mal = preg_replace_callback(
                                '/{dersom ikke delkrav.periodisk_avstemming}(.+?){\/dersom ikke delkrav.periodisk_avstemming}/s',
                                function ($treff ) {
                                    return $treff[1];
                                },
                                preg_replace(
                                    '/{dersom delkrav.periodisk_avstemming}(.+?){\/dersom delkrav.periodisk_avstemming}/s',
                                    '',
                                    $malDelkrav)
                            );
                        }
                        $resultat[] = str_replace($variablerDelkrav, $erstatning, $mal);
                    }
                }
                return implode('', $resultat);
            },
            $mal
        );


        // Lag loop for hvert leietillegg
        $mal = preg_replace_callback(
            '/{for hvert tillegg}(.+?){\/for hvert tillegg}/s',
            function($treff) {
                $resultat = [];
                $malDelkrav = $treff[1];

                $variablerDelkrav = [
                    '{tillegg.id}',
                    '{tillegg.kode}',
                    '{tillegg.navn}',
                    '{tillegg.beskrivelse}',
                    '{tillegg.beløp}',
                    '{tillegg.terminbeløp}',
                    '{tillegg.periodisk_avstemming}'
                ];

                /** @var LeieforholdDelkravtype $delkravtype */
                foreach( $this->hentDelkravtyper() as $delkravtype ) {
                    if( $delkravtype->selvstendigTillegg ) {
                        $erstatning = array(
                            $delkravtype->hentDelkravTypeId(),
                            $delkravtype->hentKode(),
                            $delkravtype->hentNavn(),
                            $delkravtype->hentBeskrivelse(),
                            $this->leiebase->kr($delkravtype->hentBeløp()),
                            $this->leiebase->kr($delkravtype->hentTerminbeløp()),
                            $delkravtype->periodiskAvstemming ? "☑︎︎" : "☐"
                        );

                        if($delkravtype->periodiskAvstemming) {
                            $mal = preg_replace_callback(
                                '/{dersom tillegg.periodisk_avstemming}(.+?){\/dersom tillegg.periodisk_avstemming}/s',
                                function ($treff ) {
                                    return $treff[1];
                                },
                                preg_replace(
                                    '/{dersom ikke tillegg.periodisk_avstemming}(.+?){\/dersom ikke tillegg.periodisk_avstemming}/s',
                                    '',
                                    $malDelkrav)
                            );
                        }
                        else {
                            $mal = preg_replace_callback(
                                '/{dersom ikke tillegg.periodisk_avstemming}(.+?){\/dersom ikke tillegg.periodisk_avstemming}/s',
                                function ($treff ) {
                                    return $treff[1];
                                },
                                preg_replace(
                                    '/{dersom tillegg.periodisk_avstemming}(.+?){\/dersom tillegg.periodisk_avstemming}/s',
                                    '',
                                    $malDelkrav)
                            );
                        }
                        $resultat[] = str_replace($variablerDelkrav, $erstatning, $mal);
                    }
                }
                return implode('', $resultat);
            },
            $mal
        );


        // Erstatt dersom-variablene
        $mal = preg_replace_callback(
            '/{dersom husstandmedlemmer}(.+?){\/dersom husstandmedlemmer}/s',
            function($treff) {
                return $this->hentAndreBeboere() && $this->hentAndreBeboere()->hentAntall() ? $treff[1] : '';
            },
            $mal
        );

        $mal = preg_replace_callback(
            '/{dersom ikke husstandmedlemmer}(.+?){\/dersom ikke husstandmedlemmer}/s',
            function($treff) {
                return !$this->hentAndreBeboere() || !$this->hentAndreBeboere()->hentAntall() ? $treff[1] : '';
            },
            $mal
        );

        $mal = preg_replace_callback(
            '/{dersom bad}(.+?){\/dersom bad}/s',
            function($treff) {
                return $this->hentLeieobjekt()->hentBad() ? $treff[1] : '';
            },
            $mal
        );

        $mal = preg_replace_callback(
            '/{dersom ikke bad}(.+?){\/dersom ikke bad}/s',
            function($treff) {
                return !$this->hentLeieobjekt()->hentBad() ? $treff[1] : '';
            },
            $mal
        );

        // Dersom leieobjektet ...

        /*
         * Dersom leieobjektet ...
         *
         * Eksempel1: {dersom ikke leieobjekt bad}Leieobjektet har ikke bad{/dersom ikke leieobjekt bad}
         * Eksempel2: {dersom leieobjekt areal=30}Leieobjektet er på tredve kvadratmeter{/dersom leieobjekt areal=30}
         */
        $mal = preg_replace_callback(
            '/{dersom (ikke )?leieobjekt (.+)}(.+?){\/dersom \1?leieobjekt \2}/s',
            function($treff) {
                $negativ = $treff[1] == 'ikke ';
                $betingelse = explode('=', $treff[2]);
                if(count($betingelse) == 1) {
                    $betingelse[] = true;
                }
                $erstatningsstreng = $treff[3];

                return $this->hentLeieobjekt()->hent($betingelse[0]) == $betingelse[1]
                    xor $negativ
                    ? $erstatningsstreng
                    : '';
            },
            $mal
        );


        $mal = preg_replace_callback(
            '/{dersom bofellesskap}(.+?){\/dersom bofellesskap}/s',
            function($treff) {
                return ( $this->hentAndel()->asDecimal() != 1 ) ? $treff[1] : '';
            },
            $mal
        );

        $mal = preg_replace_callback(
            '/{dersom ikke bofellesskap}(.+?){\/dersom ikke bofellesskap}/s',
            function($treff) {
                return ( $this->hentAndel()->asDecimal() == 1 ) ? $treff[1] : '';
            },
            $mal
        );

        $mal = preg_replace_callback(
            '/{dersom tidsbestemt}(.+?){\/dersom tidsbestemt}/s',
            function($treff) {
                return ( $this->hentTildato() ) ? $treff[1] : '';
            },
            $mal
        );

        $mal = preg_replace_callback(
            '/{dersom ikke tidsbestemt}(.+?){\/dersom ikke tidsbestemt}/s',
            function($treff) {
                return ( !$this->hentTildato() ) ? $treff[1] : '';
            },
            $mal
        );

        $mal = preg_replace_callback(
            '/{dersom oppsigelsestid}(.+?){\/dersom oppsigelsestid}/s',
            function($treff) {
                return ( $this->leiebase->periodeformat($this->hentOppsigelsestid(), true) != "P0M") ? $treff[1] : '';
            },
            $mal
        );

        $mal = preg_replace_callback(
            '/{dersom ikke oppsigelsestid}(.+?){\/dersom ikke oppsigelsestid}/s',
            function($treff) {
                return ( $this->leiebase->periodeformat($this->hentOppsigelsestid(), true) == "P0M") ? $treff[1] : '';
            },
            $mal
        );

        $mal = preg_replace_callback(
            '/{dersom fornyelse}(.+?){\/dersom fornyelse}/s',
            function($treff) {
                return ( $this->hentKontrakt()->hentId() != $this->hentId() ) ? $treff[1] : '';
            },
            $mal
        );

        $mal = preg_replace_callback(
            '/{dersom ikke fornyelse}(.+?){\/dersom ikke fornyelse}/s',
            function($treff) {
                return ( $this->hentKontrakt()->hentId() == $this->hentId() ) ? $treff[1] : '';
            },
            $mal
        );

        // Fyll med leieobjekt-data
        $mal = preg_replace_callback(
            '/{leieobjekt (.+?)}/s',
            function($treff) {
                return $this->hentLeieobjekt()->hent($treff[1]) ?: '';
            },
            $mal
        );

        $variabler = [
            '{leietaker.id}',
            '{leietaker.navn}',
            '{leietaker.postadresse}',
            '{leietaker.fødselsdato}',
            '{leietaker.fødselsnummer}',
            '{leietaker.epost}',
            '{leietaker.telefon}',
        ];

        $regningsperson = $this->hentRegningsperson();
        if ($this->hentLeietakere()->hentAntall() == 1) {
            /** @var Leietaker $leietaker */
            $leietaker = $this->hentLeietakere()->hentFørste();
            $erstatningstekst = [
                $leietaker->hentPerson() ? $leietaker->hentPerson()->hentId() : '',
                $leietaker->hentNavn(),
                $regningsperson ? $regningsperson->hentPostadresse() : '',
                $leietaker->hentPerson() && $leietaker->hentPerson()->hentFødselsdato() ? $leietaker->hentPerson()->hentFødselsdato()->format('d.m.Y') : '',
                $leietaker->hentPerson() ? $leietaker->hentPerson()->hentFødselsnummer() : '',
                $leietaker->hentPerson() ? $leietaker->hentPerson()->hentEpost() : '',
                $leietaker->hentPerson() ? $leietaker->hentPerson()->hentTelefon() : '',
            ];
        }
        else {
            $erstatningstekst = [
                '',
                $this->hentNavn(),
                $regningsperson ? $regningsperson->hentPostadresse() : '',
                '',
                '',
                '',
                '',
            ];
        }
        $mal = str_replace($variabler, $erstatningstekst, $mal);

        $variabler = [
            '{kontraktnr}',
            '{leieforhold}',
            '{utleier}',
            '{utleier navn}',
            '{utleier adresse}',
            '{utleier postnr}',
            '{utleier poststed}',
            '{utleier telefonnummer}',
            '{utleier mobilnummer}',
            '{utleier organisasjonsnummer}',
            '{utleiers organisasjonsnummer}',
            '{utleierfelt}',
            '{leietaker}',
            '{leietakerfelt}',
            '{andel}',
            '{bofellesskap}',
            '{leieobjekt}',
            '{leieobjektadresse}',
            '{antallrom}',
            '{areal}',
            '{bad}',
            '{toalett}',
            '{terminleie}',
            '{årsleie}',
            '{terminer}',
            '{terminlengde}',
            '{betalingsfrist}',
            '{er_fornyelse}',
            '{startdato}',
            '{fradato}',
            '{tildato}',
            '{oppsigelsestid}',
            '{kid}',
        ];
        $erstatningstekst = [
            $kontrakt->hentId(),
            $this->hentId(),
            $this->app->hentValg('utleier'),
            $this->app->hentValg('utleier'),
            $this->app->hentValg('adresse'),
            $this->app->hentValg('postnr'),
            $this->app->hentValg('poststed'),
            $this->app->hentValg('telefon'),
            $this->app->hentValg('mobil'),
            $this->app->hentValg('orgnr'),
            $this->app->hentValg('orgnr'),
            $utleierfelt,
            $this->hentNavn(),
            $this->app->vis(Leietakerfelt::class, ['leieforhold' => $this]) . $this->app->vis(LeieforholdAdressefelt::class, ['leieforhold' => $this]),
            $this->hentAndel()->asDecimal() < 1 ? "{$this->hentAndel()->asFraction()} av " : '',
            $this->hentAndel()->asDecimal() < 1 ? ' i bofellesskap' : '',
            strval( $leieobjekt ),
            $leieobjekt->hentBeskrivelse(),
            $leieobjekt->hentAntRom(),
            $leieobjekt->hentAreal() ? "{$leieobjekt->hentAreal()}m²" : '',
            $leieobjekt->hentBad() ? 'Leieobjektet har tilgang til dusj/bad. ' : 'Leieobjektet har ikke tilgang til dusj/bad. ',
            $toalett,
            $this->leiebase->kr( $this->hentLeiebeløp() ),
            $this->leiebase->kr( $this->hentLeiebeløp() * $this->hentAntTerminer() ),
            $this->hentAntTerminer(),
            $terminlengde,
            $betalingsfrist,
            $this->hentId() != $kontrakt->hentId() ? "Leieavtalen er fornyelse av tidligere leieavtaler. Leieforholdet ble påbegynt {$this->hent('fradato')->format('d.m.Y')}<br>" : '',
            $this->hentFradato()->format('d.m.Y'),
            $kontraktdato->format('d.m.Y'),
            $tildato ? $tildato->format('d.m.Y') : '',
            $this->leiebase->periodeformat( $this->hentOppsigelsestid() ),
            $this->hentKid(),
        ];
        $mal = str_replace($variabler, $erstatningstekst, $mal);
        return $this->app->post($this, __FUNCTION__, $mal);
    }

    /**
     * @return Innbetaling|null
     * @throws Exception
     */
    public function hentSisteBetaling(): ?Innbetaling
    {
        if(!property_exists($this->data, 'sisteBetaling')) {
            $this->data->sisteBetaling = null;
            /** @var stdClass[] $sisteBetaling */
            $sisteBeløp = $this->mysqli->select([
                'source' => Innbetaling::hentTabell(),
                'fields' => 'innbetaling',
                'where' => [
                    "leieforhold" => $this->hentId(),
                    "beløp > 0",
                    "konto <> '0'",
                    "dato <= NOW()"
                ],
                'orderfields' => "dato DESC",
                'limit' => 1
            ])->data;
            if(count($sisteBeløp)) {
                /** @var Innbetaling $sisteBetaling */
                $sisteBetaling = $this->app->hentModell(Innbetaling::class, reset($sisteBeløp)->innbetaling);
                $this->data->sisteBetaling = $sisteBetaling;
            }
        }
        return $this->data->sisteBetaling;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function lastUtestående(): Leieforhold
    {
        $iDag = new DateTime();
        $this->data->utestående = 0;
        $this->data->forfalt = 0;
        /** @var Krav $krav */
        foreach($this->hentKrav() as $krav) {
            $utestående = ($krav->hentKravdato() && $krav->hentKravdato() <= $iDag) ? $krav->hentUtestående() : 0;
            $forfalt = ($krav->hentForfall() && $krav->hentForfall() <= $iDag) ? $utestående : 0;
            $this->data->utestående += $utestående;
            $this->data->forfalt += $forfalt;
        }
        return $this;
    }

    /**
     * @return float
     * @throws Exception
     */
    public function hentUtestående()
    {
        if(!isset($this->data->utestående)) {
            if (isset($this->rawData->leieforhold_ekstra->utestående)) {
                $this->data->utestående = $this->rawData->leieforhold_ekstra->utestående;
            }
            else {
                $this->lastUtestående();
            }
        }
        return $this->data->utestående;
    }

    /**
     * @return int|null
     * @throws Exception
     */
    public function hentUtdelingsplassering(): ?int
    {
        if (!$this->hentRegningTilObjekt()) {
            return null;
        }
        if(!isset($this->data->utdelingsplassering)) {
            if (isset($this->rawData->leieforhold_ekstra->utdelingsplassering)) {
                $this->data->utdelingsplassering = $this->rawData->leieforhold_ekstra->utdelingsplassering;
            }
            else {
                $this->data->utdelingsplassering = $this->hentRegningsobjekt()->hentUtskriftsposisjon();
            }
        }
        return $this->data->utdelingsplassering;

    }

    /**
     * @return float
     * @throws Exception
     */
    public function hentKontobalanse()
    {
        if(!isset($this->data->kontobalanse)) {
            $idag = new DateTime();
            $beregnUtestående = !isset($this->data->utestående);
            $this->data->kontobalanse = 0;
            if ($beregnUtestående) {
                $this->data->utestående = 0;
                $this->data->forfalt = 0;
            }
            $transaksjoner = $this->hentTransaksjoner();
            foreach($transaksjoner as $transaksjon) {
                if($transaksjon instanceof Krav && $transaksjon->hentKravdato() <= $idag) {
                    $this->data->kontobalanse -= $transaksjon->hentBeløp();
                    if ($beregnUtestående) {
                        $utestående = $transaksjon->hentUtestående();
                        $forfalt = $transaksjon->hentForfall() <= $idag ? $utestående : 0;
                        $this->data->utestående += $utestående;
                        $this->data->forfalt += $forfalt;
                    }
                }
                elseif ($transaksjon instanceof Innbetaling && $transaksjon->hentDato() <= $idag) {
                    $this->data->kontobalanse += $transaksjon->hentBeløpTilLeieforhold($this);
                }
            }
        }
        return $this->data->kontobalanse;
    }

    /**
     * @return float
     * @throws Exception
     */
    public function hentForfalt()
    {
        if(!isset($this->data->forfalt)) {
            if (isset($this->rawData->leieforhold_ekstra->forfalt)) {
                $this->data->forfalt = $this->rawData->leieforhold_ekstra->forfalt;
            }
            else {
                $this->lastUtestående();
            }
        }
        return $this->data->forfalt;
    }

    /**
     * @return Notatsett
     * @throws Exception
     */
    public function hentBetalingsNotater(): Notatsett
    {
        /** @var Notatsett $notater */
        $notater = $this->hentSamling('notater');
        return $notater;
    }

    /**
     * @return Adgangsett
     * @throws Exception
     */
    public function hentAdganger(): Adgangsett
    {
        /** @var Adgangsett $adganger */
        $adganger = $this->hentSamling('adganger');
        return $adganger;
    }

    /**
     * @param Person $person
     * @return $this
     * @throws Exception
     */
    public function slettAdgangFor(Person $person): Leieforhold
    {
        $this->hentAdganger()->leggTilFilter(['personid' => $person->hentId()])->slettAlle();
        return $this;
    }

    /**
     * Returnerer epost-mottakerne i leieforholdet som strenger i formatet <code>'Navn &lt;epost&gt;'</code>
     *
     * @param string $varseltype
     *  informasjon: Ikke reservert mot masse-epost
     *  generelt: Ønsker ta imot automatiske varsler
     *  innbetalingsbekreftelse: Ønsker kvittering for betalinger
     *  forfallsvarsel: Ønsker motta varsel om forfall
     *  umiddelbart_betalingsvarsel: Ønsker motta umiddelbart betalingsvarsel
     * @return string[]
     * @throws Exception
     */
    public function hentEpost(string $varseltype = 'generelt'): array
    {
        if(!property_exists($this->data, 'brukerepost')) {
            $this->data->brukerepost = (object)[
                'registrert' => [],
                'informasjon' => [],
                'generelt' => [],
                'innbetalingsbekreftelse' => [],
                'forfallsvarsel' => [],
                'umiddelbart_betalingsvarsel' => [],
            ];
            /** @var Adgangsett $adganger */
            $adganger = $this->hentAdganger();
            $adganger->leggTilEavFelt('umiddelbart_betalingsvarsel_epost');
            /** @var Adgang $adgang */
            foreach($adganger as $adgang) {
                $navn = $adgang->hentPerson()->hentNavn();
                $epost = $adgang->hentPerson()->hentEpost();
                if(!empty($epost)) {
                    $this->data->brukerepost->registrert[] = "{$navn} <{$epost}>";
                    $reservasjonMotMasseepost = $adgang->hentPerson()->hentBrukerProfilPreferanse('reservasjon_mot_masseepost');
                    if (!$reservasjonMotMasseepost) {
                        $this->data->brukerepost->informasjon[] = "{$navn} <{$epost}>";
                    }
                    if($adgang->hentEpostvarsling()) {
                        $this->data->brukerepost->generelt[] = "{$navn} <{$epost}>";
                        if($adgang->hentInnbetalingsbekreftelse()) {
                            $this->data->brukerepost->innbetalingsbekreftelse[] = "{$navn} <{$epost}>";
                        }
                        if($adgang->hentForfallsvarsel()) {
                            $this->data->brukerepost->forfallsvarsel[] = "{$navn} <{$epost}>";
                        }
                        if($adgang->hentUmiddelbartBetalingsvarselEpost()) {
                            $this->data->brukerepost->umiddelbart_betalingsvarsel[] = "{$navn} <{$epost}>";
                        }
                    }
                }
            }
            $this->data->brukerepost->informasjon = array_unique($this->data->brukerepost->informasjon);
            $this->data->brukerepost->generelt = array_unique($this->data->brukerepost->generelt);
            $this->data->brukerepost->innbetalingsbekreftelse = array_unique($this->data->brukerepost->innbetalingsbekreftelse);
            $this->data->brukerepost->forfallsvarsel = array_unique($this->data->brukerepost->forfallsvarsel);
            $this->data->brukerepost->umiddelbart_betalingsvarsel = array_unique($this->data->brukerepost->umiddelbart_betalingsvarsel);
        }
        if(isset($this->data->brukerepost->{$varseltype})) {
            return $this->data->brukerepost->{$varseltype};
        }
        return [];
    }
    
    /**
     * Hent andel
     * 
     * Dersom en dato er oppgitt, returneres andelen for denne datoen.
     * Ellers returneres datoen som for øyeblikket er lagret på leieforholdet
     * 
     * @param DateTimeInterface|null $dato
     * @return Fraction
     * @throws Exception
     */
    public function hentAndel(?DateTimeInterface $dato = null): Fraction
    {
        if(!$dato && isset($this->data->andel)) {
            return $this->data->andel;
        }
        if(!isset($this->andeler)) {
            $this->andeler = [];
            /** @var Andelsperiode $andelsperiode */
            foreach ($this->hentAndelsperioder() as $andelsperiode) {
                $this->andeler[$andelsperiode->hentFradato()->format('Y-m-d')] = $andelsperiode->hentAndel();
            }
        }
        $dato = $dato ?? new DateTimeImmutable();
        ksort($this->andeler);
        $andel = new Fraction();
        foreach($this->andeler as $fradato => $andel) {
            if($fradato > $dato->format('Y-m-d')) {
                break;
            }
        }
        if($dato->format('Y-m-d') == date_create_immutable()->format('Y-m-d')) {
            return $this->data->andel = $andel;
        }
        return $andel;
    }

    /**
     * Returnerer epost-mottakerne i leieforholdet som strenger i formatet <code>'Navn &lt;epost&gt;'</code>
     *
     * @param string $varseltype
     *  informasjon: Ikke reservert mot masse-epost
     *  generelt: Ønsker ta imot automatiske varsler
     *  innbetalingsbekreftelse: Ønsker kvittering for betalinger
     *  forfallsvarsel: Ønsker motta varsel om forfall
     *  umiddelbart_betalingsvarsel: Ønsker motta umiddelbart betalingsvarsel
     * @return string[]
     * @throws Exception
     */
    public function hentSmsNumre(string $varseltype = 'generelt'): array
    {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        if(!property_exists($this->data, 'telefonnumre')) {
            $this->data->telefonnumre = (object)[
                'registrert' => [],
                'informasjon' => [],
                'generelt' => [],
                'innbetalingsbekreftelse' => [],
                'forfallsvarsel' => [],
                'umiddelbart_betalingsvarsel' => [],
            ];
            /** @var Adgangsett $adganger */
            $adganger = $this->hentAdganger();
//            $adganger->leggTilEavFelt('umiddelbart_betalingsvarsel_epost');
            /** @var Adgang $adgang */
            foreach($adganger as $adgang) {
                $mobilnr = trim($adgang->hentPerson()->hentMobil() ?: $adgang->hentPerson()->hentTelefon());
                $mobilnr = preg_replace("/^\+/", "00", $mobilnr);
                $mobilnr = preg_replace("/[^0-9]/", "", $mobilnr);
                $mobilnr = preg_replace("/^00/", "+", $mobilnr);
                if($mobilnr && strpos($mobilnr, '+') === false) {
                    $mobilnr = '+47' . $mobilnr;
                }
                if(!empty($mobilnr)) {
                    $this->data->telefonnumre->registrert[] = $mobilnr;
                    $reservasjonMotMasseSms = $adgang->hentPerson()->hentBrukerProfilPreferanse('reservasjon_mot_masse_sms');
                    if (!$reservasjonMotMasseSms) {
                        $this->data->telefonnumre->informasjon[] = $mobilnr;
                    }
                    if($adgang->hentEpostvarsling()) {
                        $this->data->telefonnumre->generelt[] = $mobilnr;
                        if($adgang->hentInnbetalingsbekreftelse()) {
                            $this->data->telefonnumre->innbetalingsbekreftelse[] = $mobilnr;
                        }
                        if($adgang->hentForfallsvarsel()) {
                            $this->data->telefonnumre->forfallsvarsel[] = $mobilnr;
                        }
                        if($adgang->hentUmiddelbartBetalingsvarselEpost()) {
                            $this->data->telefonnumre->umiddelbart_betalingsvarsel[] = $mobilnr;
                        }
                    }
                }
            }
            $this->data->telefonnumre->informasjon = array_unique($this->data->telefonnumre->informasjon);
            $this->data->telefonnumre->generelt = array_unique($this->data->telefonnumre->generelt);
            $this->data->telefonnumre->innbetalingsbekreftelse = array_unique($this->data->telefonnumre->innbetalingsbekreftelse);
            $this->data->telefonnumre->forfallsvarsel = array_unique($this->data->telefonnumre->forfallsvarsel);
            $this->data->telefonnumre->umiddelbart_betalingsvarsel = array_unique($this->data->telefonnumre->umiddelbart_betalingsvarsel);
        }
        $resultat = [];
        if(isset($this->data->telefonnumre->{$varseltype})) {
            $resultat =  $this->data->telefonnumre->{$varseltype};
        }
        return $this->app->post($this, __FUNCTION__, $resultat, $args);
    }

    /**
     * Hent en forkortet versjon av leieforholdnavnet
     *
     * @param int $maksLengde
     * @return string
     * @throws Exception
     */
    public function hentKortnavn(int $maksLengde = 10): string
    {
        $navnsett = [];
        $elementer = [];
        $maksLengdePerLeietaker = $maksLengde;
        /** @var Leietaker $leietaker */
        foreach($this->hentLeietakere() as $leietaker) {
            if(!$leietaker->slettet) {
                $navnsett[] = $leietaker->hentNavn();
            }
        }
        $antall = count($navnsett);
        foreach ($navnsett as $navn) {
            $maksLengdePerLeietaker = (int)(($maksLengde + 1 - $antall) / $antall);
            $elementer[] = mb_substr($navn, 0, $maksLengdePerLeietaker, 'UTF-8');
        }
        if($maksLengdePerLeietaker) {
            return implode('&', $elementer);
        }
        return mb_substr($this->hentNavn(), 0, $maksLengde, 'UTF-8');
    }

    /**
     * @return bool
     */
    public function erBofellesskap(): bool
    {
        $andel = $this->hentAndel();
        return round($andel->asDecimal(), 2) < 1;
    }

    /**
     * Henter siste leiejustering for leieobjektet før angitt dato
     *
     * @param DateTimeInterface|null $referanseDato
     * @return stdClass
     *  ->dato DateTimeInterface
     *  ->beløp float nytt basisbeløp
     * @throws Exception
     */
    public function hentSisteLeiejustering(?DateTimeInterface $referanseDato = null): stdClass
    {
        $dato = $this->hent('leie_oppdatert') ?? $this->hentFradato();
        $beløp = $this->hentÅrligBasisleie();

        if($referanseDato) {
            $historikk = $this->hent('leie_historikk');
            ksort($historikk);
            $dato = $this->hentFradato();
            $beløp = $this->hentÅrligBasisleie();
            foreach($historikk as $justeringsTidspunkt => $leiedetaljer) {
                $justeringsTidspunkt = date_create_immutable($justeringsTidspunkt);
                if($justeringsTidspunkt > $referanseDato) {
                    break;
                }
                $dato = $justeringsTidspunkt;
                $beløp = $leiedetaljer->basisleie ?? $beløp;
            }
        }

        return (object)array(
            'dato' => $dato,
            'beløp' => $beløp
        );
    }

    /**
     * Legg til en ny kontrakt i leieforholdet
     *
     * @param DateTime $dato
     * @param DateTime|null $tilDato
     * @return Kontrakt
     * @throws Exception
     */
    public function leggTilKontrakt(
        DateTimeInterface $dato,
        ?DateTimeInterface $tilDato = null
    ): Kontrakt
    {
        if($dato < $this->hentKontrakt()->dato) {
            throw new Exception('Ny leieavtale kan ikke dateres tidligere enn eksisterende avtaler');
        }
        if($tilDato && $tilDato < $dato) {
            throw new Exception('Til-dato kan ikke være før fra-dato');
        }
        $leietakere = $this->hentLeietakere();
        /** @var Kontrakt $kontrakt */
        $kontrakt = $this->app->nyModell(Kontrakt::class, (object)[
            'leieforhold' => $this,
            'andel' => $this->hentAndel($dato),
            'dato' => $dato,
            'tildato' => $tilDato,
        ]);
        $kontrakt->tekst = $this->gjengiAvtaletekst(true, $kontrakt);

        $kontrakt->synkUtdaterteFelter();

        foreach ($leietakere as $leietaker) {
            $kontrakt->leggTilLeietaker($leietaker->person, $leietaker->hentNavn());
        }

        $this->settKontrakt($kontrakt)->oppdaterLeietakerLeieforhold();
        $this->nullstill();
        return $kontrakt;
    }

    /**
     * @param Person $person
     * @return $this
     * @throws Exception
     */
    public function slettLeietaker(Person $person, ?DateTimeInterface $tidspunkt = null): Leieforhold
    {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $tidspunkt = $tidspunkt ?? new DateTimeImmutable();
        if ($this->hentLeietakere()->hentAntall() > 1) {
            foreach ($this->hentLeietakere() as $leietaker) {
                if($leietaker->person->hentId() == $person->hentId()) {
                    $leietaker->settSlettet($tidspunkt);
                }
            }
        }
        $this->oppdaterLeietakerLeieforhold();
        if($this->hentRegningsperson() && $this->hentRegningsperson()->hentId() == $person->hentId()) {
            $this->settTilfeldigRegningsperson();
        }
        return $this->app->post($this, __FUNCTION__, $this, $args);
    }

    /**
     * Oppdater Leietaker-leieforhold
     * Det er bare for den siste kontrakten i hvert leieforhold
     * at leieforholdet skal være påført i leietaker-modellen
     *
     * @return $this
     * @throws Exception
     */
    public function oppdaterLeietakerLeieforhold(): Leieforhold
    {
        $this->app->pre($this, __FUNCTION__, []);
        $this->app->hentSamling(Leietaker::class)
            ->leggTilPersonModell()
            ->leggTilLeieforholdModell()
            ->leggTilKontraktModell()
            ->leggTilFilter(['leieforhold' => $this->hentId()])
            ->leggTilFilter(['`' . Leietaker::hentTabell() . '`.`kontrakt` <>' => $this->hentKontrakt()->hentId()])
            ->låsFiltre()
            ->sett('leieforhold', null);
        unset($this->samlinger->leietakere);
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @param Delkravtype $delkravtype
     * @param bool $selvstendig
     * @param bool $relativ
     * @param float|int $sats
     * @param bool $periodiskAvstemming
     * @return $this
     * @throws Exception
     */
    public function leggTilDelkravtype(
        Delkravtype $delkravtype,
        ?bool $selvstendig = null,
        ?bool $relativ = null,
        float $sats = 0,
        bool $periodiskAvstemming = false
    ): Leieforhold
    {
        $selvstendig = $selvstendig ?? $delkravtype->selvstendigTillegg;
        $relativ = $relativ ?? $delkravtype->relativ;
        $delkravtypeForekomst = $this->hentDelkravtype($delkravtype->kode);
        if (!$delkravtypeForekomst) {
            /** @var LeieforholdDelkravtype $leieforholdDelkravtype */
            $delkravtypeForekomst = $this->app->nyModell(LeieforholdDelkravtype::class, (object)[
                'leieforhold' => $this,
                'delkravtype' => $delkravtype
            ]);
        }
        $delkravtypeForekomst
            ->settRelativ($relativ)
            ->settSelvstendigTillegg($selvstendig)
            ->settPeriodiskAvstemming($periodiskAvstemming)
            ->settSats($sats)
        ;
        $this->samlinger->delkravtyper = null;
        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function slettAlleDelkravForekomster(): Leieforhold
    {
        $this->hentDelkravtyper()->slettAlle();
        $this->samlinger->delkravtyper = null;
        return $this;
    }

    /**
     * Erstatter eller oppretter nye leiekrav (forfall) i dette leieforholdet
     *
     * * For tidsbegrensede kontrakter opprettes krav fram til utløpsdato.
     *      Om denne har utløpt opprettes kun en termin ad gangen,
     *      men kun dersom alle eksisterende terminer har passert.
     * * For ikke tidsbegrensede kontrakter settes kun hele terminer,
     *      og ikke senere enn oppsigelsestiden beregnet ifra dagens dato.
     * * For oppsagte kontrakter opprettes krav fram til oppsigelsestiden opphører,
     *      men bare dersom leia ikke er beregna fram til leieavtalen er oppsagt.
     *
     * @param DateTimeInterface|null $fraDato Dersom fra-dato er angitt, vil eksisterende ubetalte leier overskrives.
     * Dersom fra-dato ikke er angitt, vil det kun fylles på med fortløpende leier etter de eksisterende
     * så lenge leieforholdet ikke er oppsagt.
     * @param bool $løsneInnbetalinger Denne har kun effekt dersom fra-dato er angitt.
     * Dersom $løsneInnbetalinger er sann, vil evt innbetalinger løsnes fra krav,
     * sånn at disse kan overskrives.
     * @return Kravsett
     * @throws Throwable
     */
    public function opprettLeiekrav(
        ?DateTimeInterface $fraDato = null,
        bool      $løsneInnbetalinger = false
    ): Kravsett
    {
        $ukedag = $this->hentForfallFastUkedag();
        $dagIMåneden = $this->hentForfallFastDagIMåneden();
        $fastDato = $this->hentForfallFastDato();
        /** @var int[] $resultatKravIder */
        $resultatKravIder = [];
        $oppsigelse = $this->hentOppsigelse();

        /**
         * Oppsagte leieforhold fylles kun på dersom fradato er gitt eksplisitt.
         * Ellers returneres et tomt kravsett
         */
        if ($oppsigelse && !isset($fraDato)) {
            /** @var Kravsett $kravsett */
            $kravsett = $this->app->hentSamling(Krav::class)->lastFra([]);
            return $kravsett;
        }

        /**
         * Fast dato per måned kan være 1-27,
         * eller 't' for siste dag hver måned
         */
        $dagIMåneden = ($dagIMåneden === 't' || (int)$dagIMåneden > 27) ? 't' : max(intval($dagIMåneden), 0);

        /** @var LeieforholdDelkravtype[] $delkravtyper */
        $delkravtyper = [];
        /** @var LeieforholdDelkravtype[] $tillegg */
        $tillegg = [];
        /** @var LeieforholdDelkravtype $delkravtype */
        foreach($this->hentDelkravtyper()->leggTilFilter(['`' . Delkravtype::hentTabell() . '`.`kravtype`' => Krav::TYPE_HUSLEIE]) as $delkravtype) {
            if ($delkravtype->selvstendigTillegg) {
                $tillegg[] = $delkravtype;
            }
            else {
                $delkravtyper[] = $delkravtype;
            }
        }

        $startDato = $this->førsteMuligeDatoForHusleieOppretting($fraDato, $løsneInnbetalinger);
        $sluttDato = $this->husleieSluttDato();

        // Dersom fraDato er angitt skal alle eksisterende krav fra startdato slettes før de nye opprettes
        if($fraDato) {
            $this->slettEksisterendeHusleiekrav($startDato);
        }

        // Krav for en og en termin opprettes fra startdato inntil sluttdato er nådd
        $fom = clone $startDato;
        while($fom < $sluttDato) {

            $termindel = new Fraction(1, $this->hentAntTerminer());
            $leie = $this->hentLeiebeløp();
            $delkrav = [];
            $forfallsdato = Leiebase::førsteBankdag($fom->add($this->hentTerminBetalingsfrist()));


            /**
             * Månedlige terminer kan risikere å hoppe over en kort måned
             * dersom de begynner på slutten av en lengre måned
             * og terminlengden er oppgitt i måneder
             */
            if(
                $fom->format('j') > 27 && $this->hentTerminlengde()->m
            ) {
                $nesteOrdinæreTerminstart = date_create_immutable($fom->format('Y-m-01'));    // $tom flyttes til første dag i måneden
                $nesteOrdinæreTerminstart = $nesteOrdinæreTerminstart->add($this->hentTerminlengde());                        // Intervallet legges til
                $nesteOrdinæreTerminstart = new DateTimeImmutable($nesteOrdinæreTerminstart->format('Y-m-t'));        // $tom flyttes til
            }
            else {
                $nesteOrdinæreTerminstart = clone $fom;
                $nesteOrdinæreTerminstart = $nesteOrdinæreTerminstart->add($this->hentTerminlengde());
            }
            $nesteTerminstart = clone $nesteOrdinæreTerminstart;

            // Terminen skal ikke fortsette utover oppsigelsestiden
            if( $oppsigelse && $nesteTerminstart > $oppsigelse->oppsigelsestidSlutt ) {
                $nesteTerminstart = $oppsigelse->oppsigelsestidSlutt;
                if ($nesteTerminstart instanceof DateTime) {
                    $nesteTerminstart = DateTimeImmutable::createFromMutable($nesteTerminstart);
                }
            }

            $framskyndetDato = $this->framskyndStartdatoTilFastDag($nesteTerminstart, $ukedag, $dagIMåneden, $fastDato);
            if ($framskyndetDato > $fom) {
                $nesteTerminstart = $framskyndetDato;
            }

            /** @var DateTimeImmutable $nesteTerminstart */

            /**
             * Tildato for denne terminen settes til dagen før neste termin begynner
             */
            $tom = $nesteTerminstart->sub(new DateInterval('P1D'));

            /**
             * Dersom terminen ikke har ordinær lengde, må leia beregnes på nytt
             */
            if($nesteTerminstart->diff($nesteOrdinæreTerminstart)->days) {
                $termindel = new Fraction($fom->diff($tom)->days + 1, 365);
                $leie = round($termindel->multiply($this->hentLeiebeløp() * $this->hentAntTerminer())->asDecimal());
            }

            /**
             * Sjekk om vi er i oppsigelse.
             * I oppsigelsestida skal det ikke beregnes leie dersom leieobjektet allerede er utleid
             */
            $ledig = true;
            if($oppsigelse && $fom >= $oppsigelse->fristillelsesdato) {
                $ledighet = $this->hentLeieobjekt()->hentLedighetForTidsrom($fom, $tom, $this);
                $ledig = $ledighet->compare('>=', $this->hentAndel());
            }

            /*
            Delkravene for denne terminen etableres.
            */
            foreach($delkravtyper AS $delkravtype) {
                $årligBeløp = $delkravtype->relativ
                    ? ($delkravtype->sats * $this->hentÅrligBasisleie())
                    : $delkravtype->sats;
                if ($delkravtype->delkravtype) {
                    $delkrav[$delkravtype->delkravtype->id] = round($termindel->multiply($årligBeløp)->asDecimal());
                }
            }

            /**
             * Skriv ut terminlengden i klartekst
             */
            $terminBeskrivelse = Krav::beskrivTerminen($fom, $tom);

            if($ledig) {
                /**
                 * Opprett leiekravet
                 * @var Krav $leiekrav
                 */
                $leiekrav = $this->app->nyModell(Krav::class, (object)[
                    'oppretter'        => $this->app->bruker['navn'],
                    'type'            => Krav::TYPE_HUSLEIE,
                    'leieobjekt'    => $this->hentLeieobjekt(),
                    'kravdato'        => $fom,
                    'fom'            => $fom,
                    'tom'            => $tom,
                    'andel'            => $this->hentAndel($fom),
                    'kontrakt'        => $this->hentKontrakt(),
                    'leieforhold'    => $this,
                    'beløp'            => $leie,
                    'termin'        => $terminBeskrivelse,
                    'tekst'            => "Leie for #{$this->hentLeieobjekt()} {$terminBeskrivelse}",
                    'forfall'        => $forfallsdato,
                    'delkrav'       => $delkrav
                ]);
                $resultatKravIder[] = $leiekrav->hentId();

                /**
                 * Opprett tilleggene
                 */
                foreach($tillegg as $tilleggskrav) {
                    $beløp = $termindel->multiply($tilleggskrav->relativ ? ($tilleggskrav->sats * $this->hentÅrligBasisleie()) : $tilleggskrav->sats)->asDecimal();
                    $resultatKravIder[] = $this->app->nyModell(Krav::class, (object)[
                        'oppretter'        => $this->app->bruker['navn'],
                        'type'            => $tilleggskrav->kode,
                        'leieobjekt'    => $this->hentLeieobjekt(),
                        'kravdato'        => $fom,
                        'fom'            => $fom,
                        'tom'            => $tom,
                        'kontraktnr'    => $this->hentKontrakt(),
                        'leieforhold'    => $this,
                        'beløp'            => round($beløp),
                        'termin'        => $terminBeskrivelse,
                        'tekst'            => "{$tilleggskrav->navn} {$terminBeskrivelse}",
                        'forfall'        => $forfallsdato,
                        'hovedkrav'     => $leiekrav
                    ])->hentId();
                }
            }

            /**
             * Endre fra-dato for neste krav
             */
            $fom = $tom->add(new DateInterval('P1D'));
        }

        /** @var Kravsett $kravsett */
        $kravsett = $this->app->hentSamling(Krav::class)->filtrerEtterIdNumre($resultatKravIder);
        return $kravsett;
    }

    /**
     * Hent første mulige dato for å fylle på leiekrav i leieforholdet
     *
     * Metoden returnerer i utgangspunktet den første datoen siden leieforholdet ble påbegynt
     * hvor det ikke allerede er opprettet leiekrav.
     * Dersom en dato er oppgitt som argument,
     * vil metoden forsøksvis returnere begynnelsen på den terminen som dekker denne datoen,
     * evt første påfølgende termin som kan overskrives
     * Dersom utsendte og betalte leier ikke kan overskrives vil datoen flyttes fram til det første terminen
     * som ikke er utskrevet eller betalt
     *
     * @param DateTimeInterface|null $dato Dersom en dato er angitt, returneres siste mulige dato som er før denne.
     * Dersom null beskyttes alle eksisterende krav.
     * @param bool $utsendteOgBetalteLeierKanOverskrives Dersom usann beskyttes utskrevne og betalte krav
     * @return DateTimeImmutable
     * @throws Exception
     */
    protected function førsteMuligeDatoForHusleieOppretting(
        ?DateTimeInterface $dato = null,
        bool $utsendteOgBetalteLeierKanOverskrives = false
    ): DateTimeImmutable
    {
        // Dersom fradato er oppgitt, (f.eks ved leie-endring,
        // vil dato kunne gå lengre tilbake, til krav som ikke er utsendt eller betalt
        if ($dato) {
            $dato = clone $dato;
        }

        /**
         * Last all husleie i leieforholdet, sortert etter termin.
         * Dersom fraDato er oppgitt begrenses søket til betalt eller utskrevet husleie påbegynt før denne datoen,
         * og dersom utsendte og betalte leier kan overskrives så ignoreres betaling eller utskrift-status
         *
         * @var Kravsett $kravsett
         */
        $kravsett = $this->app->hentSamling(Krav::class)
            ->leggTilLeftJoin(Delbeløp::hentTabell(), Delbeløp::hentTabell() . '.`krav` = ' . Krav::hentTabell() . '.' . Krav::hentPrimærnøkkelfelt())
            ->leggTilLeftJoin(Regning::hentTabell(), Krav::hentTabell() . '.`gironr` = ' . Regning::hentTabell() . '.' . Regning::hentPrimærnøkkelfelt())
            ->leggTilEavFelt('annullering')
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`leieforhold`' => $this->hentId()])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`beløp` >=' => 0])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`type`' => Krav::TYPE_HUSLEIE])
            ->leggTilFilter(['`annullering`.`verdi` IS NULL'])
            ->leggTilSortering('tom', true)
            ->begrens(1);

        if ($dato) {
            // Legg til til filter å redusere settet til krav som enten er eldre, skrevet ut eller betalt
            $or = [];
            $or[] = ['`' . Krav::hentTabell() .'`.`fom` <' => $dato->format('Y-m-d')];
            if (!$utsendteOgBetalteLeierKanOverskrives) {
                $or[] = ['`' . Regning::hentTabell() .'`.`utskriftsdato` IS NOT NULL'];
                $or[] = ['`' . Delbeløp::hentTabell() .'`.`krav` IS NOT NULL'];
            }
            $kravsett->leggTilFilter(['or' => $or]);
        }

        /** @var Krav|null $krav */
        $krav = $kravsett->hentFørste();
        if ($krav) {
            $tom = $krav->tom;
            $resultatDato = clone $tom;
            $resultatDato = $resultatDato->add(new DateInterval('P1D'));
        }
        else {
            $resultatDato = clone $this->hentFradato();
        }

        if($resultatDato instanceof DateTime) {
            $resultatDato = DateTimeImmutable::createFromMutable($resultatDato);
        }
        return $resultatDato;
    }

    /**
     * Fastsett sluttdato for husleie
     *
     * Sluttdato er den første datoen hvor en ny termin IKKE LENGER KAN PÅBEGYNNES
     * (altså dagen etter den siste leieterminen)
     *
     * Sluttdato avhenger av flere forhold:
     * * For en vanlig avtale vil sluttdato være dagens dato pluss oppsigelsestiden.
     * * For en tidsbegrenset leieavtale vil sluttdato være dagen etter at leieavtalen opphører.
     * > * Dersom utløpsdato for et tidsbegrenset leieforhold er passert, vil sluttdato være i morgen.
     *      Dvs at kun terminer bare kan opprettes etter at de har begynt
     * * For en leieavtale som er oppsagt vil sluttdato være lik oppsigelsestidSlutt
     *
     * @return DateTime
     * @throws Exception
     */
    protected function husleieSluttDato(): DateTime
    {
        // Dersom leieforholdet er avsluttet vil sluttdato settes til dagen etter oppsigelsestiden
        if ($this->hentOppsigelse()) {
            return clone ($this->hentOppsigelse()->oppsigelsestidSlutt);
        }

        // For en tidsbegrenset leieavtale vil sluttdato være dagen etter at leieavtalen opphører
        // Dersom utløpsdato for et tidsbegrenset leieforhold er passert, vil sluttdato være i morgen
        $tilDato = $this->hentTildato();
        if ($tilDato) {
            $sluttDato = clone $tilDato;
            if($tilDato < date_create()) {
                $sluttDato = new DateTime('now', new DateTimeZone('UTC'));
            }

            $sluttDato->add( new DateInterval('P1D') );
        }

        // Dersom leieforholdet ikke er tidsbegrenset er sluttdato lik oppsigelsestid fra i dag
        else {
            $sluttDato = new DateTime();
            $sluttDato->add($this->hentOppsigelsestid());
        }
        return $sluttDato;
    }

    /**
     * @param DateTimeImmutable $startDato
     * @param bool $inklusiveHusleietillegg
     * @return $this
     * @throws Exception
     */
    protected function slettEksisterendeHusleiekrav(DateTimeImmutable $startDato): Leieforhold
    {
        /**
         * Last alle leien i tidsrommet som skal slettes.
         *
         * @var Kravsett $leiekravITidsrommet
         */
        $leiekravITidsrommet = $this->app->hentSamling(Krav::class)
            ->leggTilLeftJoin(Delbeløp::hentTabell(), '`' . Delbeløp::hentTabell() . '`.`krav` = `' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '`')
            ->leggTilEavFelt('annullering')

            ->leggTilUttrykk('krav_ekstra', 'har_betaling', 'IF(`' . Delbeløp::hentTabell() . '`.`krav` IS NULL, 0, 1)', 'BIT_OR')
            ->leggTilUttrykk('krav_ekstra', 'fakturert', 'IF(`' . Krav::hentTabell() . '`.`reell_krav_id` IS NULL, 0, 1)')

            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`leieforhold`' => $this->hentId()])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`fom` >=' => $startDato->format('Y-m-d')])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`beløp` >' => 0])
            ->leggTilFilter(['`annullering`.`verdi` IS NULL'])
            ->leggTilFilter(['type' => Krav::TYPE_HUSLEIE]);

        /**
         * Sorter etter prioritering
         * Høyeste prioritering legges til sist
         */
        $leiekravITidsrommet
            ->leggTilSortering('fakturert', true, 'krav_ekstra')
            ->leggTilSortering('har_betaling', true, 'krav_ekstra')
        ;

        /**
         * Gå gjennom ett og ett krav i rekkefølge etter sannsynligheten for at kravet utgjør ikke-kreditert terminleie
         * Dersom det er krevd leie for kravtidsrommet slettes kravet og samt ett matchende tillegg i hver tilleggstype.
         */
        foreach ($leiekravITidsrommet as $krav) {
            if ($krav->fom && $this->leieKrevdFor($krav->fom)) {
                $krav->slett();
                $this->nullstill();
            }
        }
        return $this;
    }

    /**
     * Hent alle ikke-annullerte leiekrav for gitt tidsrom
     *
     * @param DateTimeInterface $fra
     * @param DateTimeInterface $til
     * @return Kravsett
     * @throws Exception
     */
    public function hentLeieKravITidsrommet(DateTimeInterface $fra, DateTimeInterface $til): Kravsett
    {
        /** @var Kravsett $husleiesett */
        $husleiesett = $this->app->hentSamling(Krav::class)
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`leieforhold`' => $this->hentId()])
            ->leggTilEavFelt('annullering')
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`type`' => Krav::TYPE_HUSLEIE])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`beløp` >=' => 0])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`fom` <=' => $til->format('Y-m-d')])
            ->leggTilFilter(['`' . Krav::hentTabell() . '`.`tom` >=' => $fra->format('Y-m-d')])
            ->leggTilFilter(['`annullering`.`verdi` IS NULL'])
            ->låsFiltre();
        return $husleiesett;
    }

    /**
     * Sjekk om det er krevd leie i leieforholdet for en bestemt dato ved å summere aktive og krediterte krav
     *
     * @param DateTimeInterface $dato
     * @return bool
     * @throws Exception
     */
    public function leieKrevdFor(DateTimeInterface $dato): bool
    {
        return (bool)$this->hentLeieKravITidsrommet($dato, $dato)->hentAntall();
    }

    /**
     * Beregner siste dato for en leietermin dersom den skal avbrytes som følge av faste datoer eller ukedager
     *
     * @param DateTimeInterface $opprinneligStartdato
     * @param int|null $fastUkedag
     * @param string|null $fastDatoHverMåned
     * @param string|null $fastÅrligDato
     * @return DateTimeImmutable
     * @throws Exception
     */
    private function framskyndStartdatoTilFastDag(
        DateTimeInterface $opprinneligStartdato,
        ?int     $fastUkedag,
        ?string  $fastDatoHverMåned,
        ?string  $fastÅrligDato
    ): DateTimeImmutable
    {
        $fastDatoHverMåned = intval($fastDatoHverMåned) > 27 ? 't' : str_pad((int)$fastDatoHverMåned, 2, '0', STR_PAD_LEFT);
        $resultat = clone $opprinneligStartdato;
        if ($resultat instanceof DateTime) {
            $resultat = DateTimeImmutable::createFromMutable($resultat);
        }

        /**
         * Dersom terminene skal begynne på fast ukedag
         */
        if( $fastUkedag > 0) {

            /**
             * Finn differansen mellom eksisterende ukedag og ønsket ukedag,
             * og framskynd datoen så mange dager
             */
            $differanse = intval($resultat->format('N')) - (int)$fastUkedag;
            if($differanse < 0) {
                $differanse += 7;
            }
            $resultat = $resultat->sub(new DateInterval('P' . $differanse . 'D'));
        }

        /*
        Dersom terminene alltid skal begynne på en fast dato hver måned:
        */
        if($fastDatoHverMåned == 't' || $fastDatoHverMåned > 0) {
            if($resultat->format('Y-m-d') < $resultat->format('Y-m-' . $fastDatoHverMåned)) {
                $resultat = date_create_immutable( $opprinneligStartdato->format('Y-m-01'))
                    ->sub(new DateInterval('P1M'));
            }
            $resultat = new DateTimeImmutable( $resultat->format('Y-m-' . $fastDatoHverMåned));
        }

        /*
        Dersom nye terminer alltid skal begynne på en fast dato hvert år:
        */
        if( $fastÅrligDato ) {
            if($resultat->format('Y-m-d') < $resultat->format('Y-' . $fastÅrligDato)) {
                $resultat = date_create_immutable( $opprinneligStartdato->format('Y-m-01'))
                    ->sub(new DateInterval('P1Y'));
            }
            $resultat = new DateTimeImmutable( $resultat->format('Y-' . $fastÅrligDato));
        }
        return $resultat;
    }

    /**
     * @param DateTimeInterface|null $tilDato
     * @return $this
     * @throws Exception
     */
    public function settTildato(? DateTimeInterface $tilDato): Leieforhold
    {
        $kontrakt = $this->hentKontrakt();
        if ($tilDato && $tilDato->format('Ymd') < $kontrakt->dato->format('Ymd')) {
            throw new Exception('Kan ikke sette opphørsdato til tidligere enn siste leieavtale');
        }
        $kontrakt->settTildato($tilDato);
        return $this->sett('tildato', $tilDato);
    }

    /**
     * @param integer $antTerminer
     * @return $this
     * @throws Exception
     */
    public function settAntTerminer(int $antTerminer): Leieforhold
    {
        $this->sett('ant_terminer', max($antTerminer, 1));
        return $this->oppdaterLeie();
    }

    /**
     * Sett andel
     *
     * @param Fraction $andel
     * @param DateTimeInterface|null $dato
     * @return $this
     */
    public function settAndel(Fraction $andel, ?DateTimeInterface $dato = null): Leieforhold
    {
        $dato = $dato ?? new DateTimeImmutable();
        $eksisterendeAndel = $this->hentAndel($dato);
        if($andel->compare('!=', $eksisterendeAndel)) {
            $this->andeler = null;
            $this->hentAndelsperioder()->leggTilFilter(
                ['`' . Andelsperiode::hentTabell() . '`.`fradato` >=' => $dato->format('Y-m-d')],
            )->låsFiltre()->slettAlle();
            /** @var Andelsperiode|null $sisteAndelsperiode */
            $sisteAndelsperiode = $this->hentAndelsperioder()->hentSiste();
            $this->app->nyModell(Andelsperiode::class, (object)[
                'leieforhold' => $this,
                'leieobjekt' => $this->hentLeieobjekt(),
                'andel' => $andel,
                'fradato' => $dato,
            ]);
            if($sisteAndelsperiode) {
                $sisteAndelsperiode->tildato = $dato->sub(new DateInterval('P1D'));
            }
            if($dato->format('Y-m-d') == date_create_immutable()->format('Y-m-d')) {
                $this->data->andel = $andel;
            }
        }
        return $this;
    }

    /**
     * @param float $årligBasisleie
     * @return $this
     * @throws Exception
     */
    public function settÅrligBasisleie(float $årligBasisleie): Leieforhold
    {
        $this->sett('årlig_basisleie', $årligBasisleie);
        return $this->oppdaterLeie();
    }

    /**
     * @param float $bruttoTerminLeie
     * @return $this
     * @throws Exception
     */
    public function settLeiebeløp(float $bruttoTerminLeie): Leieforhold
    {
        $antTerminer = $this->hentAntTerminer();
        $årsleie = round($bruttoTerminLeie * $antTerminer);
        $delkravtyper = $this->hentDelkravtyper();
        foreach ($delkravtyper as $delkravtype) {
            if($delkravtype->delkravtype->kravtype == Krav::TYPE_HUSLEIE && !$delkravtype->delkravtype->selvstendigTillegg) {
                $årsleie -= $delkravtype->hentBeløp();
            }
        }
        return $this->settÅrligBasisleie(max($årsleie, 0));
    }

    /**
     * Oppdater terminleiebeløpet i databasen.
     *
     * Denne operasjonen må kjøres hver gang leiebeløpet, antall terminer eller
     * delkravene har blitt endret.
     *
     * @return $this
     * @throws Exception
     */
    public function oppdaterLeie(): Leieforhold
    {
        $this->app->pre($this, __FUNCTION__, []);
        $årsleie = $this->hentÅrligBasisleie();
        $antTerminer = $this->hentAntTerminer();
        $delkravtyper = $this->hentDelkravtyper();
        foreach ($delkravtyper as $delkravtype) {
            if($delkravtype->delkravtype->kravtype == Krav::TYPE_HUSLEIE && !$delkravtype->delkravtype->selvstendigTillegg) {
                $årsleie += $delkravtype->hentBeløp();
            }
        }
        $terminbeløp = round($årsleie / $antTerminer);
        $endret = $terminbeløp != $this->hentLeiebeløp();
        $this->sett('leiebeløp', $terminbeløp);
        if($endret) {
            $this->kontrakt->settTekst($this->gjengiAvtaletekst());
        }
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @param bool $automatisk
     * @param DateTimeImmutable|null $tidspunkt
     * @return Leieforhold
     */
    public function oppdaterLeieHistorikk(bool $automatisk = false, ?DateTimeImmutable $tidspunkt = null): Leieforhold
    {
        list('automatisk' => $automatisk)
            = $this->app->pre($this, __FUNCTION__, [
            'automatisk' => $automatisk
        ]);
        $kpiHistorikk = [];
        $tidspunkt = $tidspunkt ?? date_create_immutable('now', new DateTimeZone('utc'));
        $this->app->logger->info('LEIEFORHOLD – Oppdaterer leiehistorikk' , ['leieforhold' => $this->hentId(), 'tidspunkt' => $tidspunkt->format('Y-m-d H:i:s')]);
        try {
            $kpiHistorikk = Leieregulerer::hentKpiFraSSb();
        } catch (Exception $e) {
            $this->app->logger->warning($e->getMessage());
        }
        $egenskap = $tidspunkt->format('c');
        $leieHistorikk = $this->hentLeieHistorikk() ?? new stdClass();
        $leieHistorikk->$egenskap = (object)[
            'basisleie' => $this->hentÅrligBasisleie(),
            'automatisk' => $automatisk,
            'gjeldende_kpi' => array_key_last($kpiHistorikk),
        ];
        $this->leieHistorikk = $leieHistorikk;
        $this->leieOppdatert = $tidspunkt;
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @return DateTime
     * @throws Exception
     */
    public function hentSisteFellesstrømKravdato(): DateTime
    {
        /** @var DateTime $dato */
        $dato = clone $this->hentFradato();

        /** @var Krav|null $sisteFellesstrømKrav */
        $sisteFellesstrømKrav = $this->app->hentSamling(Krav::class)
            ->leggTilFilter(['type' => Krav::TYPE_STRØM])
            ->leggTilFilter(['leieforhold' => $this->hentId()])
            ->leggTilSortering('tom', true)
            ->begrens(1)
            ->hentFørste()
        ;

        if($sisteFellesstrømKrav && $sisteFellesstrømKrav->tom) {
            $dato = clone $sisteFellesstrømKrav->tom;
            $dato->add(new DateInterval('P1D'));
        }

        return $dato;
    }

    /**
     * @return Andelsett
     * @throws Exception
     */
    public function hentStrømAndeler(): Andelsett
    {
        /** @var Andelsett $andeler */
        $andeler = $this->leiebase->hentSamling(Andel::class)
            ->leggTilFilter(['leieforhold' => $this->hentId()])
            ->låsFiltre()
        ;
        return $andeler;
    }

    /**
     * @return DateTime|null
     * @throws Exception
     */
    public function hentAvventOppfølging(): ?DateTime
    {
        $dato = $this->hent('avvent_oppfølging');
        return $dato > date_create() ? $dato : null;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function hentGiroformat(): ?string
    {
        if($this->leiebase->hentValg('efaktura')) {
            if($this->hentEfakturaIdentifier()) {
                return 'eFaktura';
            }
        }
        return $this->hent('giroformat');
    }

    /**
     * Henter en blanding av Krav og betalingsplan-avdrag
     *
     * @param float|null $beløp
     * @return Blanding
     * @throws Exception
     */
    public function hentKommendeForfall(?float $beløp = null): Blanding
    {
        /** @var Kravsett $kravsett */
        $kravsett = $this->app->hentSamling(Krav::class);
        $kravsett->leggTilLeftJoin(
            Originalkrav::hentTabell(),
            '`' . Originalkrav::hentTabell() . '`.`krav_id` = `' . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt() . '`');
        $kravsett->leggTilFilter(['leieforhold' => $this->hentId()]);
        $kravsett->leggTilFilter(['utestående >' => 0]);
        $kravsett->leggTilSortering('kravdato')->leggTilSortering('forfall');
        $kravsett->leggTilRegningModell();

        $betalingsplan = $this->hentBetalingsplan();
        if ($betalingsplan && $betalingsplan->aktiv) {
            $betalingsplan->oppdaterUteståendeAvdrag();
            $kravsett->leggTilFilter(['`' . Originalkrav::hentTabell() . '`.`krav_id`' => null]);

            $avdragsett = $this->app->hentSamling(Avdrag::class);
            $avdragsett->leggTilInnerJoin(Betalingsplan::hentTabell(),
                '`' . Betalingsplan::hentTabell() . '`.`' . Betalingsplan::hentPrimærnøkkelfelt() . '` = `' . Avdrag::hentTabell() . '`.`betalingsplan_id`'
            );
            $avdragsett->leggTilFilter(['`' . Betalingsplan::hentTabell() . '`.`leieforhold_id`' => $this->hentId()]);
            $avdragsett->leggTilFilter(['utestående >' => 0]);
            $avdragsett->leggTilSortering('forfallsdato');

            /** @var Blanding $forfallSett */
            $forfallSett = $this->app->hentSamling([$kravsett, $avdragsett]);

            /**
             * @var Krav|Avdrag $forfallA
             * @var Krav|Avdrag $forfallB
             */
            $forfallSett->sorterLastede(function (Modell $forfallA, Modell $forfallB) use ($beløp): int
            {
                $iDag = new DateTime();
                $nesteTermin = date_create()->add($this->hentTerminlengde());
                $beløpMatchA = $beløp && in_array($beløp, [$forfallA->hent('utestående'), $forfallA->hent('beløp')]);
                $beløpMatchB = $beløp && in_array($beløp, [$forfallB->hent('utestående'), $forfallB->hent('beløp')]);

                /** @var DateTime|null $datoA */
                $datoA = $forfallA instanceof Krav ? $forfallA->forfall : $forfallA->forfallsdato;
                /** @var DateTime|null $datoB */
                $datoB = $forfallB instanceof Krav ? $forfallB->forfall : $forfallB->forfallsdato;

                /**
                 * Sammenlikn to like typer (krav mot krav eller avdrag mot avdrag)
                 */
                if( get_class($forfallA) == get_class($forfallB)) {
                    if($datoA == $datoB) {
                        if($beløpMatchA != $beløpMatchB) {
                            return $beløpMatchA ? -1 : 1;
                        }
                        if ($forfallA instanceof Krav) {
                            $datoA = $forfallA->hentUtskriftsdato();
                            $datoB = $forfallB->hentUtskriftsdato();
                            if($datoA == $datoB) {
                                $datoA = $forfallA->kravdato;
                                $datoB = $forfallB->kravdato;
                            }
                        }
                        if($datoA == $datoB) {
                            return $forfallA->hentId() - $forfallB->hentId();
                        }
                    }
                    if (empty($datoA) || empty($datoB)) {
                        return empty($datoA) ? 1 : -1;
                    }
                    return $datoA < $datoB ? -1 : 1;
                }

                /**
                 * Sammenlikn krav mot avdrag
                 */
                else {
                    /** Alt som har forfalt prioriteres */
                    if (($datoA && $datoA < $iDag) || ($datoB && $datoB < $iDag)) {
                        if($datoA == $datoB) {
                            if($beløpMatchA != $beløpMatchB) {
                                return $beløpMatchA ? -1 : 1;
                            }
                            return $forfallA instanceof Avdrag ? -1 : 1;
                        }
                        if (empty($datoA) || empty($datoB)) {
                            return empty($datoA) ? 1 : -1;
                        }
                        return $datoA < $datoB ? -1 : 1;
                    }

                    /** Ingen har forfalt, men forfaller snart */
                    if (($datoA && $datoA < $nesteTermin) || ($datoB && $datoB < $nesteTermin)) {
                        if ($datoA && $datoA < $nesteTermin && $datoB && $datoB < $nesteTermin) {
                            if($beløpMatchA != $beløpMatchB) {
                                return $beløpMatchA ? -1 : 1;
                            }
                            return $forfallA instanceof Avdrag ? -1 : 1;
                        }
                        if (empty($datoA) || empty($datoB)) {
                            return empty($datoA) ? 1 : -1;
                        }
                        return $datoA < $nesteTermin ? -1 : 1;
                    }

                    /** ellers prioriteres avdrag over krav */
                    return $forfallA instanceof Avdrag ? -1 : 1;
                }
            });
        }
        else {
            /** @var Blanding $forfallSett */
            $forfallSett = $this->app->hentSamling([$kravsett]);
        }
        return $forfallSett;
    }

    /**
     * Opprett krav og delkrav
     *
     * @param string $type
     * @param string $tekst
     * @param float $beløp
     * @param DateTimeInterface|null $dato
     * @param DateTimeInterface|null $forfall
     * @param Fraction|null $andel
     * @param string|null $termin
     * @param DateTimeInterface|null $fom
     * @param DateTimeInterface|null $tom
     * @return Krav
     * @throws Exception
     */
    public function opprettKravOgDelkrav(
        string             $type,
        string             $tekst,
        float              $beløp,
        ?DateTimeInterface $dato = null, ?DateTimeInterface $forfall = null,
        ?Fraction          $andel = null,
        ?string            $termin = null,
        ?DateTimeInterface $fom = null,
        ?DateTimeInterface $tom = null
    ): Krav
    {
        if (!in_array($type, Krav::$typer)) {
            throw new Exception("Ugyldig kravtype `{$type}`. Lovlige verdier er: `" . implode('`, `', Krav::$typer) . '`');
        }
        if (!$tekst) {
            throw new Exception('Kan ikke opprette krav uten tekstbeskrivelse');
        }
        if (($tom && !$fom) || ($fom > $tom)) {
            throw new Exception('Ugyldig fra-og-til-angivelse');
        }
        /** @var LeieforholdDelkravtype[] $delkravtyper */
        $delkravtyper = [];
        /** @var LeieforholdDelkravtype[] $tillegg */
        $tillegg = [];
        /** @var float[] $delkrav */
        $delkrav = [];
        /** @var LeieforholdDelkravtype $delkravtype */
        foreach($this->hentDelkravtyper()->leggTilFilter(['`' . Delkravtype::hentTabell() . '`.`kravtype`' => $type]) as $delkravtype) {
            if ($delkravtype->selvstendigTillegg) {
                $tillegg[] = $delkravtype;
            }
            else {
                $delkravtyper[] = $delkravtype;
            }
        }
        if ($fom && ($delkravtyper || $tillegg)) {
            if($fom->format('Y-m-d') == $tom->format('Y-m-01')
                && $tom->format('Y-m-d') == $fom->format('Y-m-t')
            ) {
                $termindel = new Fraction(1, 12);
            }
            else {
                $termindel = new Fraction($fom->diff($tom)->days + 1, 365);
            }
        }
        else {
            $termindel = new Fraction(1);
        }
        $leieobjekt = $type == Krav::TYPE_HUSLEIE ? $this->hentLeieobjekt() : null;

        $sgn = 0;
        if($beløp != 0) {
            $sgn = $beløp < 0 ? -1 : 1;
        }
        $beløp = abs($beløp);

        /*
        Delkravene for denne terminen etableres.
        */
        $basisBeløp = $beløp;
        foreach($delkravtyper AS $delkravtype) {
            $delkravBeløp = $delkravtype->relativ
                ? (abs($delkravtype->sats) * $basisBeløp)
                : $termindel->multiply(abs($delkravtype->sats))->asDecimal();
            if ($delkravtype->delkravtype) {
                if($basisBeløp >= $delkravBeløp) {
                    $delkrav[$delkravtype->delkravtype->id] = round($sgn * $delkravBeløp);
                    $basisBeløp -= $delkravBeløp;
                }
                else {
                    $delkrav[$delkravtype->delkravtype->id] = round($sgn * $basisBeløp);
                    $basisBeløp = 0;
                }
            }
        }
        /** @var Krav $krav */
        $krav = $this->app->nyModell(Krav::class, (object)[
            'oppretter'        => $this->app->bruker['navn'],
            'type'            => $type,
            'leieobjekt'    => $leieobjekt,
            'kravdato'        => $dato,
            'fom'            => $fom,
            'tom'            => $tom,
            'forfall'       => $forfall,
            'andel'            => $andel,
            'kontrakt'        => $this->hentKontrakt(),
            'leieforhold'    => $this,
            'beløp'            => $sgn * $beløp,
            'termin'        => $termin,
            'tekst'            => $tekst,
            'delkrav'       => $delkrav
        ]);

        /**
         * Opprett tilleggene
         */
        foreach($tillegg as $tilleggskrav) {
            $tilleggBeløp = $tilleggskrav->relativ
                ? (abs($tilleggskrav->sats) * $beløp)
                : $termindel->multiply(abs($tilleggskrav->sats))->asDecimal();
            $this->app->nyModell(Krav::class, (object)[
                'oppretter'        => $this->app->bruker['navn'],
                'type'            => $tilleggskrav->kode,
                'leieobjekt'    => $leieobjekt,
                'kravdato'        => $dato,
                'fom'            => $fom,
                'tom'            => $tom,
                'kontraktnr'    => $this->hentKontrakt(),
                'leieforhold'    => $this,
                'beløp'            => round($sgn * $tilleggBeløp),
                'termin'        => $termin,
                'tekst'            => "{$tilleggskrav->navn} {$termin}",
                'hovedkrav'     => $krav
            ]);
        }
        return $krav;
    }

    /**
     * Sett regningsperson, eller velg tilfeldig regningsperson
     *
     * @return $this
     * @throws Throwable
     */
    public function  settTilfeldigRegningsperson(): Leieforhold
    {
        $this->app->pre($this, __FUNCTION__, []);
        /** @var Leietakersett $leietakersett */
        $leietakersett = $this->hentLeietakere()
            ->leggTilLeftJoin(EfakturaIdentifier::hentTabell(),
                '`' . Leietaker::hentTabell() . '`.`person` = `' . EfakturaIdentifier::hentTabell() . '`.`person_id`'
            );
        $leietakersett->leggTilSortering('blocked_by_receiver' , false, EfakturaIdentifier::hentTabell());
        $leietakersett->leggTilSortering('efaktura_identifier' , true, EfakturaIdentifier::hentTabell());
        $leietakersett->leggTilSortering('slettet' , true);
        /** @var Leietaker $leietaker */
        $leietaker = $leietakersett->hentFørste();
        parent::sett('regningsperson', $leietaker ? $leietaker->person : null);
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * Sett regningsperson, eller velg tilfeldig regningsperson
     *
     * @param Person|null $person
     * @return $this
     * @throws Exception
     */
    public function  settRegningsperson(?Person $person): Leieforhold
    {
        list('person' => $person)
            = $this->app->pre($this, __FUNCTION__, [
            'person' => $person
        ]);
        if($person) {
            /** @var Leietakersett $leietakersett */
            $leietakersett = $this->hentLeietakere()->leggTilFilter(['person' => $person->hentId()]);
            if(!$leietakersett->hentAntall()) {
                $this->settTilfeldigRegningsperson();
                return $this->app->post($this, __FUNCTION__, $this);
            }
        }
        parent::sett('regningsperson', $person);
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @param DateTimeInterface $oppsigelsesdato
     * @param DateTimeInterface|null $fristillelsesdato
     * @param DateTimeInterface|null $oppsigelsestidSlutt
     * @param string $ref
     * @param string $merknad
     * @param bool $oppsagtAvUtleier
     * @return $this
     * @throws Exception
     */
    public function avslutt(
        DateTimeInterface  $oppsigelsesdato,
        ?DateTimeInterface $fristillelsesdato = null,
        ?DateTimeInterface $oppsigelsestidSlutt = null,
        string             $ref = '',
        string             $merknad = '',
        bool               $oppsagtAvUtleier = false
    ): Leieforhold
    {
        list(
            'oppsigelsesdato' => $oppsigelsesdato,
            'fristillelsesdato' => $fristillelsesdato,
            'oppsigelsestidSlutt' => $oppsigelsestidSlutt,
            'ref' => $ref,
            'merknad' => $merknad,
            'oppsagtAvUtleier' => $oppsagtAvUtleier
        ) = $this->app->pre($this, __FUNCTION__, [
            'oppsigelsesdato' => $oppsigelsesdato,
            'fristillelsesdato' => $fristillelsesdato,
            'oppsigelsestidSlutt' => $oppsigelsestidSlutt,
            'ref' => $ref,
            'merknad' => $merknad,
            'oppsagtAvUtleier' => $oppsagtAvUtleier
        ]);
        if($this->hentOppsigelse()) {
            return $this;
        }
        if($oppsigelsesdato instanceof DateTime) {
            $oppsigelsesdato = DateTimeImmutable::createFromMutable($oppsigelsesdato);
        }
        if($fristillelsesdato instanceof DateTime) {
            $fristillelsesdato = DateTimeImmutable::createFromMutable($fristillelsesdato);
        }
        elseif(!$fristillelsesdato) {
            $fristillelsesdato = clone $oppsigelsesdato;
        }
        if($oppsigelsestidSlutt instanceof DateTime) {
            $oppsigelsestidSlutt = DateTimeImmutable::createFromMutable($oppsigelsestidSlutt);
        }
        elseif (!$oppsigelsestidSlutt) {
            $oppsigelsestidSlutt = $oppsigelsesdato->add($this->hentOppsigelsestid());
        }

        $this->data->oppsigelse = $this->app->nyModell(Oppsigelse::class, (object)[
            'leieforhold' => $this,
            'oppsigelsesdato' => $oppsigelsesdato,
            'fristillelsesdato' => $fristillelsesdato,
            'oppsigelsestid_slutt' => $oppsigelsestidSlutt,
            'ref' => $ref,
            'merknad' => $merknad,
            'oppsagt_av_utleier' => $oppsagtAvUtleier,
        ]);
        /** @var Andelsperiode $sisteAndelsperiode */
        $sisteAndelsperiode = $this->hentAndelsperioder()->hentSiste();
        if($sisteAndelsperiode) {
            $sisteAndelsperiode->tildato = $fristillelsesdato->sub(new DateInterval('P1D'));
        }

        $this->opprettLeiekrav( $fristillelsesdato, true );
        $this->leieobjekt->nullstill();
        $this->nullstill();
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @return Leieforhold
     * @throws Exception
     */
    public function slettOppsigelse(): Leieforhold
    {
        $this->app->pre($this, __FUNCTION__, []);
        $oppsigelse = $this->hentOppsigelse();
        if ($oppsigelse) {
            $oppsigelse->slett();
            $this->data->oppsigelse = false;
            $this->opprettLeiekrav();
        }
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * Hent spillerom for betalingsutsettelse
     *
     * Returnerer spillerommet som leietaker har for å gi seg selv betalingsutsettelse,
     * (automatisk innvilget)
     * eller null dersom dette er ubegrenset
     *
     * @return DateInterval|null
     * @throws Exception
     */
    public function hentBetalingUtsettelseSpillerom(): ?DateInterval
    {
        if(!property_exists($this->data, 'betalingUtsettelseSpillerom')) {
            /** @var int|null $spillerom */
            $spillerom = null;

            if(!$this->app->hentValg('betalingsutsettelse_aktivert')) {
                $spillerom = 0;
            }
            else {
                $iDag = new DateTimeImmutable();
                /**
                 * Standard spillerom for betalingsutsettelse er den maksimale betalingsutsettelse
                 * som en leietaker uten noe eksisterende gjeld kan innvilge seg selv.
                 *
                 * Spillerom for kortsiktig gjelder den maksimale betalingsutsettelse
                 * som en leietaker med regninger som nettopp har forfalt kan innvilge seg selv.
                 *
                 * Kortsiktighet definerer maks antall dager etter at en regning har forfalt
                 * en leietaker fortsatt kan benytte seg av spillerom for kortsiktig gjeld
                 *
                 * @var int|null $standardSpillerom
                 */
                $standardSpillerom = $this->app->hentValg('betalingsutsettelse_spillerom_standard');
                $standardSpillerom = is_numeric($standardSpillerom) ? intval($standardSpillerom) : null;

                /** @var int|null $spilleromKortsiktigGjeld */
                $spilleromKortsiktigGjeld = $this->app->hentValg('betalingsutsettelse_spillerom_kortsiktig_gjeld');
                $spilleromKortsiktigGjeld = is_numeric($spilleromKortsiktigGjeld) ? intval($spilleromKortsiktigGjeld) : null;

                /** @var int|null $kortsiktighet */
                $kortsiktighet = $this->app->hentValg('betalingsutsettelse_spillerom_kortsiktighet');
                $kortsiktighet = is_numeric($kortsiktighet) ? intval($kortsiktighet) : null;

                /** @var bool $utestående angir om leietaker har forfalte krav per i dag */
                $utestående = false;
                /** @var bool $langsiktig angir om leietaker har forfalte krav per i dag som er eldre enn $kortsiktighet */
                $langsiktig = false;

                /**
                 * Brukt spillerom er antall dager betalingsutsettelse som en leietaker allerede har
                 * benyttet seg av, per regning
                 *
                 * @var int[] $bruktSpillerom
                 */
                $bruktSpillerom = [];
                $uteståendeKravSett = $this->hentUteståendeKrav();
                foreach ($uteståendeKravSett as $krav) {
                    $betalingsplan = $krav->hentBetalingsplan();
                    if($betalingsplan) {
                        continue;
                    }
                    $regning = $krav->regning;
                    $forfall = $krav->hentForfall();
                    if ($regning && $forfall) {
                        $originalForfallsdato = $regning->hent('original_forfallsdato');
                        if ($originalForfallsdato) {
                            $bruktSpillerom[$regning->hentId()] = $forfall->diff($originalForfallsdato)->days;
                        }
                    }
                    if($forfall && $forfall < $iDag) {
                        $utestående = true;
                        if (isset($kortsiktighet) && $iDag->diff($forfall)->days > $kortsiktighet) {
                            $langsiktig = true;
                        }
                    }
                }
                if($langsiktig) {
                    $spillerom = 0;
                }
                else if($utestående && isset($spilleromKortsiktigGjeld)) {
                    $spillerom = $spilleromKortsiktigGjeld - array_sum($bruktSpillerom);
                }
                else if (isset($standardSpillerom)) {
                    $spillerom = $standardSpillerom - array_sum($bruktSpillerom);
                }
            }
            $this->data->betalingUtsettelseSpillerom =  isset($spillerom) ? new DateInterval('P'. max($spillerom, 0) . 'D') : null;
        }
        return $this->data->betalingUtsettelseSpillerom;
    }

    /**
     * @param DateTimeInterface|string|null $dato
     * @return Leieforhold
     * @throws Exception
     */
    public function settForfallFastDato($dato): Leieforhold
    {
        list('dato' => $dato)
            = $this->app->pre($this, __FUNCTION__, [
            'dato' => $dato
        ]);
        if($dato instanceof DateTimeInterface) {
            $dato = $dato->format('m-d');
        }
        if(empty($dato)) {
            $dato = null;
        }
        else {
            $elementer = explode('-', $dato);
            if(count($elementer) != 2) {
                $dato = null;
            }
            else {
                $elementer[1] = (int)$elementer[1] > 27
                    ? 't'
                    : $elementer[1];
                $dato = implode('-', $elementer);
            }
        }
        $this->sett('forfall_fast_dato', $dato);
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @param string|null $dato 1-27 eller t for siste dag i måneden
     * @return Leieforhold
     * @throws Exception
     */
    public function settForfallFastDagIMåneden(?string $dato): Leieforhold
    {
        list('dato' => $dato)
            = $this->app->pre($this, __FUNCTION__, [
            'dato' => $dato
        ]);
        if($dato != 't'
            && !intval($dato)
        ) {
            $dato = null;
        }
        else {
            $dato
                = $dato > 27 ? 't' : $dato;
        }
        $this->sett('forfall_fast_dag_i_måneden', $dato);
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * @param int|null $ukedag
     * @return Leieforhold
     * @throws Exception
     */
    public function settForfallFastUkedag(?int $ukedag): Leieforhold
    {
        list('ukedag' => $ukedag)
            = $this->app->pre($this, __FUNCTION__, [
            'ukedag' => $ukedag
        ]);
        if($ukedag < 1 || $ukedag > 7) {
            $ukedag = null;
        }
        $this->sett('forfall_fast_ukedag', $ukedag);
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * Sett fast betalingsfrist på leie og faste krav regnet ifra terminstart,
     * eller null for å falle tilbake til den normale betalingsfristen som er angitt i innstillingene.
     *
     * Et intervall på >=0 dager indikerer forskuddsvis betaling
     *
     * @param int|null $ukedag
     * @return Leieforhold
     * @throws Exception
     */
    public function settTerminBetalingsfrist(?DateInterval $terminBetalingsfrist = null): Leieforhold
    {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $standardBetalingsfrist = Leiebase::parseDateInterval($this->app->hentValg('betalingsfrist_faste_krav'));
        $standardBetalingsfrist = $standardBetalingsfrist ?? new DateInterval('P0D');
        $terminBetalingsfrist = $terminBetalingsfrist ?? $standardBetalingsfrist;
        $eksisterendeTerminBetalingsfrist = $this->hentTerminBetalingsfrist() ?? $standardBetalingsfrist;
        $iDag = new DateTimeImmutable();
        $endret = $iDag->add($terminBetalingsfrist)->format('Ymd')
            != $iDag->add($eksisterendeTerminBetalingsfrist)->format('Ymd');
        if($endret) {
            $this->sett('termin_betalingsfrist',
            ($terminBetalingsfrist)->format('Ymd') == $iDag->add($standardBetalingsfrist)->format('Ymd')
                ? $terminBetalingsfrist
                : null
            );
            $framtidigeKrav = $this->hentFramtidigeKrav();
            foreach ($framtidigeKrav as $krav) {
                if($krav->utestående > 0 && $krav->forfall) {
                    $regning = $krav->regning;
                    $originalForfallsdato = $regning ? $regning->hentOriginalForfallsdato() : null;

                    $nyForfallsdato = clone $krav->kravdato;
                    $nyForfallsdato->add($terminBetalingsfrist);

                    // Vi må ta hensyn til krav som har fått innvilget betalingsutsettelse
                    if($originalForfallsdato) {
                        if($regning->hentForfall() > $nyForfallsdato) {
                            $regning->settOriginalForfallsdato($nyForfallsdato);
                            $nyForfallsdato = clone $regning->hentForfall();
                        }
                        else {
                            $regning->settOriginalForfallsdato(null);
                        }
                    }

                    $krav->settForfall($nyForfallsdato);
                }
            }
        }

        return $this->app->post($this, __FUNCTION__, $this, $args);
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function hentEfakturaIdentifier(): ?string
    {
        return $this->hentRegningsperson()
            ? $this->hentRegningsperson()->hentEfakturaIdentifier()
            : null;
    }

    /**
     * @return string
     * @throws Throwable
     */
    public function hentPurreformat(): string
    {
        $this->app->pre($this, __FUNCTION__, []);
        $purreformat = Purring::PURREMÅTE_GIRO;
        if (
            $this->hent('giroformat') == Regning::FORMAT_EPOST
            && (
                $this->app->hentValg('purregebyr') == 0 || $this->app->hentValg('epost_purregebyr_aktivert')
            )
        ) {
            $purreformat = Purring::PURREMÅTE_EPOST;
        }
        return $this->app->post($this, __FUNCTION__, $purreformat);
    }

    /**
     * @return DateInterval
     * @throws Throwable
     */
    public function hentTerminBetalingsfrist(): DateInterval
    {
        return $this->hent('termin_betalingsfrist') ??
            new DateInterval($this->app->hentValg('betalingsfrist_faste_krav')
        );
    }

    /**
     * Henter ny forfallsdato
     *
     * @return DateTimeImmutable
     * @throws Exception
     */
    public function nyForfallsdato(): DateTimeImmutable
    {
        $forfallsfrist = new DateInterval($this->app->hentValg('min_frist_regninger') ?: 'P0D');
        $fastForfallDag = $this->hent('forfall_fast_dag_i_måneden') ?: null;
        $fastForfallDag = str_pad(
            $fastForfallDag,
            2,
            '0',
            STR_PAD_LEFT
        );

        $forfallsdato = new DateTimeImmutable();
        $forfallsdato = $forfallsdato->setTime(0, 0);
        $forfallsdato = $forfallsdato->add($forfallsfrist);

        // Dersom det er angitt fast månedlig dag for forfall,
        //    justeres forfallsdato i hht til denne.
        if ($fastForfallDag) {
            // Forfallsdato angitt i innstillingene som 28 oppfattes som siste dag i måneden.
            if ($fastForfallDag > 27) {
                $fastForfallDag = 't';
            }

            $minForfallsdato = clone $forfallsdato;
            $forfallMåned = new DateTimeImmutable($forfallsdato->format("Y-m-01 00:00:00"));

            // Så lenge forfallsdato ikke opprettholder fristen
            // forskyves den én måned
            while (
                "{$forfallMåned->format('Y-m')}-{$fastForfallDag}" < $minForfallsdato->format('Y-m-d')
            ) {
                $forfallMåned = $forfallMåned->add(new DateInterval('P1M'));
            }

            $forfallsdato = new DateTimeImmutable(
                "{$forfallMåned->format('Y-m')}-{$fastForfallDag} 00:00:00"
            );
        }

        $forfallsdato = Leiebase::førsteBankdag($forfallsdato);

        return $forfallsdato;
    }
}