<?php

namespace Kyegil\Leiebasen\Modell;


use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Poengprogram\Poeng;
use Kyegil\Leiebasen\Modell\Poengprogram\Poengsett;

/**
 * Class Poengprogram
 * @package Kyegil\Leiebasen\Modell
 *
 * @property int $id
 * @property string $kode
 * @property DateTimeInterface|null $aktivert
 * @property string $navn
 * @property string $beskrivelse
 * @property DateInterval|null $levetid
 *
 * @method int hentId()
 * @method string hentKode()
 * @method DateTimeInterface|null hentAktivert()
 * @method string hentNavn()
 * @method string hentBeskrivelse()
 * @method DateInterval|null hentLevetid()
 *
 * @method $this settKode(string $kode)
 * @method $this settNavn(string $navn)
 * @method $this settBeskrivelse(string $beskrivelse)
 * @method $this settLevetid(DateInterval|null $levetid)
*/
class Poengprogram extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnokkelen for modellen
     */
    protected static $dbTable = 'poeng_program';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Poengprogramsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * @param string $referanse
     * @param string|null $modell
     * @param string|null $fremmedNøkkel
     * @param array $filtre
     * @param callable|null $callback
     * @return array|null
     * @throws Exception
     */
    public static function harFlere(
        string $referanse,
        ?string $modell = null,
        ?string $fremmedNøkkel = null,
        array $filtre = [],
        ?callable $callback = null
    ): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::harFlere('poeng',
                Poeng::class,
                'program_id'
            );
        }
        return parent::harFlere($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'kode'    => [
                'type'  => 'string'
            ],
            'aktivert'    => [
                'type'  => 'datetime',
                'allowNull' => true
            ],
            'navn'    => [
                'type'  => 'string'
            ],
            'beskrivelse'    => [
                'type'  => 'string'
            ],
            'levetid'    => [
                'type'  => 'interval',
                'allowNull' => true
            ],
            'konfigurering' => [
                'type'  => 'json',
                'allowNull' => true
            ],
        ];
    }

    /**
     * @return Poengsett
     * @throws Exception
     */
    public function hentPoengsett(): Poengsett
    {
        /** @var Poengsett $poengsett */
        $poengsett = $this->hentSamling('poeng');
        $poengsett->forhåndslastNavn();

        if(!isset($this->samlinger->poeng)) {
            $this->samlinger->poeng = $poengsett->hentElementer();
        }
        $poengsett->lastFra($this->samlinger->poeng);
        return clone $poengsett;
    }

    /**
     * @param Leieforhold $leieforhold
     * @return Poengsett
     * @throws Exception
     */
    public function hentLeieforholdPoengsett(Leieforhold $leieforhold): Poengsett
    {
        /** @var Poengsett $poengsett */
        $poengsett = $this->hentPoengsett();
        $poengsett
            ->leggTilFilter(['leieforhold_id' => $leieforhold->hentId()])
            ->låsFiltre();
        return clone $poengsett;
    }

    /**
     * @return void
     * @throws Exception
     */
    public function slett()
    {
        $this->hentPoengsett()->slettAlle();
        parent::slett();
    }

    /**
     * @param $egenskap
     * @param $verdi
     * @return Modell
     * @throws Exception
     */
    public function sett($egenskap, $verdi): Modell
    {
        if($egenskap == 'aktivert') {
            $this->settAktivert($verdi);
        }
        return parent::sett($egenskap, $verdi);
    }

    /**
     * @param DateTimeInterface|bool|null $verdi
     * @return Poengprogram
     * @throws Exception
     */
    public function settAktivert($verdi): Poengprogram
    {
        if($verdi) {
            if($this->hent('aktivert')) {
                return $this;
            }
            if($verdi instanceof DateTimeInterface) {
                return parent::sett('aktivert', $verdi);
            }
            return parent::sett('aktivert', new DateTimeImmutable());
        }
        else {
            return parent::sett('aktivert', null);
        }
    }

    /**
     * @return Poengprogram
     * @throws Exception
     */
    public function aktiver(): Poengprogram {
        return $this->settAktivert(new DateTimeImmutable());
    }

    /**
     * @return Poengprogram
     * @throws Exception
     */
    public function deaktiver(): Poengprogram {
        return $this->settAktivert(null);
    }

    /**
     * Henter konfigureringsverdi
     *
     * @param string|null $egenskap
     * @return mixed
     * @throws Exception
     */
    public function hentKonfigurering(?string $egenskap = null)
    {
        list('egenskap' => $egenskap)
            = $this->app->pre($this, __FUNCTION__, [
            'egenskap' => $egenskap
        ]);
        $konfigurering = $this->hent('konfigurering');
        settype($konfigurering, 'object');
        if($egenskap) {
            $resultat = $konfigurering->{$egenskap} ?? null;
            return $this->app->post($this, __FUNCTION__, $resultat);
        }
        return $this->app->post($this, __FUNCTION__, $konfigurering);
    }

    /**
     * Sett konfigureringsverdi
     *
     * @param string $egenskap
     * @param mixed $verdi
     * @return Poengprogram
     * @throws Exception
     */
    public function settKonfigurering(string $egenskap, $verdi): Poengprogram
    {
        list('egenskap' => $egenskap, 'verdi' => $verdi)
            = $this->app->pre($this, __FUNCTION__, [
            'egenskap' => $egenskap, 'verdi' => $verdi
        ]);
        $konfigurering = $this->hent('konfigurering');
        settype($konfigurering, 'object');
        $konfigurering->{$egenskap} = $verdi;
        $this->sett('konfigurering', $konfigurering);
        return $this->app->post($this, __FUNCTION__, $this);
    }

    /**
     * Hent mulige poeng-typer
     *
     * @return mixed|null
     * @throws Exception
     */
    public function hentTyper()
    {
        $this->app->pre($this, __FUNCTION__, []);
        $typer = $this->hentKonfigurering('typer') ?? [];
        return $this->app->post($this, __FUNCTION__, $typer);
    }
}