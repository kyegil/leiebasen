<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 06/04/2023
 * Time: 12:40
 */

namespace Kyegil\Leiebasen\Modell;

use Kyegil\CoreModel\CoreModelRepository;
use Kyegil\CoreModel\Interfaces\AppInterface;
use Kyegil\CoreModel\Interfaces\CoreModelRepositoryInterface;
use Kyegil\Leiebasen\CoreModelImplementering;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning;

class LeieobjektTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @test
     * @return array
     */
    public function testBeregnBasisleie(): void
    {
        $leiebaseStub = $this->createStub(CoreModelImplementering::class);
        $leiebaseStub->method('before')->will($this->returnArgument(2));
        $leiebaseStub->method('after')->will($this->returnArgument(2));
        $leiebaseStub->method('makeUnderscore')->will($this->returnValueMap([
            ['hentAreal', 'hent_areal'],
            ['hentAntRom', 'hent_ant_rom'],
            ['hentBad', 'hent_bad'],
            ['hentBygning', 'hent_bygning'],
            ['hentToalettKategori', 'hent_toalett_kategori'],
            ['hentLeieberegning', 'hent_leieberegning'],
            ['hentLeiePerObjekt', 'hent_leie_per_objekt'],
            ['hentLeiePerKvadratmeter', 'hent_leie_per_kvadratmeter'],
            ['hentLeiePerKontrakt', 'hent_leie_per_kontrakt'],
            ['hentSpesialregler', 'hent_spesialregler'],
        ]));

        $mysqliMock = $this->createMock(\mysqli::class);
        $repositoryMock = $this->createMock(CoreModelRepository::class);

        $di = [
            AppInterface::class => $leiebaseStub,
            \mysqli::class => $mysqliMock,
            CoreModelRepositoryInterface::class => $repositoryMock
        ];

        $leieberegningRawDAta = (object)[
            'leieberegning' => (object)[
                'nr' => 1,
                'navn' => 1,
                'beskrivelse' => 1,
                'leie_objekt' => 11400,
                'leie_kontrakt' => 0,
                'leie_kvm' => 1020,
                'spesialregler' => json_encode([
                    (object)[
                        'forklaring' => 'Tillegg for tilgang på bad/dusj',
                        'sorteringsorden' => 1,
                        'sats' => 2220,
                        'unntattFraLeiejustering' => false,
                        'prosessor' => Leieberegning::class . '::formelProsessor',
                        'param' => (object)[
                            'kriterier' => ['leieobjekt.bad'],
                            'formel' => '{sats}',
                        ]
                    ],
                    (object)[
                        'sorteringsorden' => 2,
                        'forklaring' => 'Tillegg for tilgang på felles do i samme bygning',
                        'sats' => 2112,
                        'unntattFraLeiejustering' => false,
                        'prosessor' => Leieberegning::class . '::formelProsessor',
                        'param' => (object)[
                            'kriterier' => ['leieobjekt.toalett_kategori' => 1],
                            'formel' => '{sats}',
                        ]
                    ],
                    (object)[
                        'sorteringsorden' => 3,
                        'forklaring' => 'Tillegg for tilgang på egen do',
                        'sats' => 3348,
                        'unntattFraLeiejustering' => false,
                        'prosessor' => Leieberegning::class . '::formelProsessor',
                        'param' => (object)[
                            'kriterier' => ['leieobjekt.toalett_kategori' => 2],
                            'formel' => '{sats}',
                        ]
                    ],
                ]),
            ]
        ];
        $leieberegning = new Leieberegning($di);
        $leieberegning->setPreloadedValues($leieberegningRawDAta);

        $leiebaseStub->method('getModel')->will($this->returnValueMap([
            [Leieberegning::class, 1, $leieberegning]
        ]));

        $leieobjektRawData = (object)[
            'leieobjekter' => (object)[
                'leieobjektnr' => 1,
                'areal' => 47,
                'ant_rom' => 2,
                'bad' => 1,
                'bygning' => null,
                'toalett_kategori' => 1,
                'leieberegning' => 1,
            ],
        ];
        $leieobjekt = new Leieobjekt($di);
        $leieobjekt->setPreloadedValues($leieobjektRawData);

        $this->assertEquals(47, $leieobjekt->hentAreal());
        $this->assertEquals(63672, $leieobjekt->beregnBasisleie());
    }
}