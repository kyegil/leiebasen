<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 01/09/2020
 * Time: 16:47
 */

namespace Kyegil\Leiebasen\Modell;


use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Kyegil\CoreModel\CoreModelException;
use Kyegil\CoreModel\Interfaces\EavValuesInterface;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Eav\Egenskap as EavEgenskap;
use Kyegil\Leiebasen\Modell\Eav\Egenskapsett;
use Kyegil\Leiebasen\Modell\Eav\EgenskapVarcharObjekt;
use Kyegil\Leiebasen\Modell\Eav\VerdiVarcharObjekt;
use Kyegil\Leiebasen\Modell\Eav\VerdiVarcharObjektsett;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløpsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Visning\felles\epost\innbetaling\Avstemmingslinje;
use Kyegil\Leiebasen\Visning\felles\epost\innbetaling\Kvittering;
use stdClass;

/**
 * Class Innbetaling
 * @package Kyegil\Leiebasen
 *
 * @property string $innbetaling
 * @property int $kontoId
 * @property string $konto
 * @property Ocr\Transaksjon|null $ocrTransaksjon
 * @property string $ref
 * @property DateTime $dato
 * @property string $merknad
 * @property string $betaler
 * @property string $registrerer
 * @property DateTime $registrert
 * @property DateTime $oppdatert
 *
 * @method string hentInnbetaling()
 * @method int hentKontoId()
 * @method string hentKonto()
 * @method Ocr\Transaksjon|null hentOcrTransaksjon()
 * @method string hentRef()
 * @method DateTime hentDato()
 * @method string hentMerknad()
 * @method string hentBetaler()
 * @method string hentRegistrerer()
 * @method DateTime hentRegistrert()
 * @method DateTime hentOppdatert()
 *
 * @method $this settKontoId(int $kontoId)
 * @method $this settMerknad(string $merknad)
 * @method $this settRegistrerer(string $registrerer)
 * @method $this settRegistrert(DateTime $registrert)
 * @method $this settOppdatert(DateTime $oppdatert)
 */
class Innbetaling extends Modell
{
    /** @var int Konto som brukes for utlikningsdelen av en kreditt */
    const KTO_KREDITT = 0;

    /**
     * @var string
     */
    protected static $dbTable = 'innbetalinger';

    /**
     * @var string
     */
    protected static $dbPrimaryKeyField = 'innbetaling';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Innbetalingsett::class;

    /** @var array|null one-to-many */
    protected static ?array $one2Many;

    /**
     * @param string $referanse
     * @param string|null $modell
     * @param string|null $fremmedNøkkel
     * @param array $filtre
     * @param callable|null $callback
     * @return array|null
     * @throws Exception
     */
    public static function harFlere(
        string $referanse,
        ?string $modell = null,
        ?string $fremmedNøkkel = null,
        array $filtre = [],
        ?callable $callback = null
    ): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::harFlere('delbeløp',
                Delbeløp::class,
                'innbetaling'
            );
            self::harFlere('positive_delbeløp',
                Delbeløp::class,
                'innbetaling', [
                    ['`' . Delbeløp::hentTabell() . '`.`beløp` >' => 0]
                ]
            );
            self::harFlere('negative_delbeløp',
                Delbeløp::class,
                'innbetaling', [
                    ['`' . Delbeløp::hentTabell() . '`.`beløp` <' => 0]
                ]
            );
        }
        return parent::harFlere($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }

    /**
     * @param DateTimeInterface $dato
     * @param string $referanse
     * @param string $betaler
     * @param Ocr\Transaksjon|null $ocrTransaksjon
     * @return string
     * @throws Exception
     */
    public static function beregnId(
        DateTimeInterface $dato,
        string $referanse,
        string $betaler,
        ?Ocr\Transaksjon $ocrTransaksjon = null
    ): string
    {
        return $dato->format('Y-m-d') . '-' . mb_substr(md5($referanse . $betaler . ($ocrTransaksjon ? $ocrTransaksjon->hentTransaksjonsnummer() : '')), 0, 4);
    }

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'innbetaling'    => [
                'type'  => 'string'
            ],
            'konto_id'    => [
                'type'  => 'int'
            ],
            'konto'    => [
                'type'  => 'string'
            ],
            'ocr_transaksjon'    => [
                'type'  => Ocr\Transaksjon::class,
                'allowNull' => true,
            ],
            'ref'    => [
                'type'  => 'string'
            ],
            'dato'    => [
                'type'  => 'date',
                'allowNull' => true
            ],
            'merknad'    => [
                'type'  => 'string'
            ],
            'betaler'    => [
                'type'  => 'string'
            ],
            'registrerer'    => [
                'type'  => 'string'
            ],
            'registrert'    => [
                'type'  => 'datetime'
            ],
            'oppdatert'    => [
                'type'  => 'datetime'
            ],
        ];
    }

    /**
     * @param stdClass $parametere
     * @return Modell
     * @throws CoreModelException
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Modell
    {
        $parametere = clone $parametere;
        list('parametere' => $parametere)
            = $this->app->before($this, __FUNCTION__, [
            'parametere' => $parametere
        ]);
        $parametere->registrerer = $parametere->registrerer ?? $this->app->bruker['navn'];
        $parametere->registrert = $parametere->registrert ?? new DateTimeImmutable();
        $parametere->{self::getDbPrimaryKeyField()}
            = $parametere->{self::getDbPrimaryKeyField()}
            ?? self::beregnId(
                $parametere->dato ?? null,
                $parametere->ref ?? null,
                $parametere->betaler ?? null,
                $parametere->ocr_transaksjon ?? null
            );
        /** @var Delbeløp $delbeløp */
        $delbeløp = $this->app->nyModell(Delbeløp::class, (object)[
            'innbetaling'       => $parametere->innbetaling,
            'konto'             => $parametere->konto ?? null,
            'ocr_transaksjon'	=> $parametere->ocr_transaksjon ?? null,
            'ref'               => $parametere->ref ?? null,
            'dato'              => $parametere->dato ?? null,
            'merknad'           => $parametere->merknad ?? null,
            'betaler'           => $parametere->betaler ?? null,
            'registrerer'       => $parametere->registrerer,
            'registrert'        => $parametere->registrert ?? null,
            'beløp'             => $parametere->beløp ?? 0,
        ]);
        $rawData = $delbeløp->getRawData();
        $id = $rawData->{static::$dbTable}->{static::$dbPrimaryKeyField} ?? null;
        $this->settId($id);
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @param string|null $egenskap
     * @return mixed
     * @throws Exception
     */
    public function hent($egenskap = null)
    {
        if($egenskap == 'beløp') {
            return $this->hentBeløp();
        }
        return parent::hent($egenskap);
    }

    /**
     * @param $egenskap
     * @param $verdi
     * @return Modell
     * @throws Exception
     */
    public function sett($egenskap, $verdi): Modell
    {
        list('egenskap' => $egenskap, 'verdi' => $verdi)
            = $this->app->before($this, __FUNCTION__, [
            'egenskap' => $egenskap, 'verdi' => $verdi
        ]);
        switch ($egenskap) {
            case 'beløp':
                $this->settBeløp($verdi);
                break;
            case 'dato':
                $this->settDato($verdi);
                break;
            case 'ref':
                $this->settRef($verdi);
                break;
            case 'betaler':
                $this->settBetaler($verdi);
                break;
            case 'ocr_transaksjon':
                $this->settOcrTransaksjon($verdi);
                break;
            default:
                parent::sett($egenskap, $verdi);
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @param DateTimeInterface $dato
     * @return Innbetaling
     * @throws Exception
     */
    public function settDato(DateTimeInterface $dato): Innbetaling
    {
        list('dato' => $dato)
            = $this->app->before($this, __FUNCTION__, [
            'dato' => $dato
        ]);
        $eksisterendeVerdi = $this->hentDato();
        if($dato->format('Y-m-d') != $eksisterendeVerdi->format('Y-m-d')) {
            $nyId = self::beregnId(
                $dato,
                $this->hentRef(),
                $this->hentBetaler(),
                $this->hentOcrTransaksjon()
            );
            /** @var Delbeløpsett $delbeløpsett */
            $delbeløpsett = $this->hentDelbeløp();
            $delbeløpsett->settVerdier((object)[
                'innbetaling' => $nyId,
                'dato' => $dato,
            ]);
            $this->oppdaterId($nyId);
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @param Ocr\Transaksjon|null $ocrTransaksjon
     * @return Innbetaling
     * @throws Exception
     */
    public function settOcrTransaksjon(?Ocr\Transaksjon $ocrTransaksjon = null): Innbetaling
    {
        list('ocrTransaksjon' => $ocrTransaksjon)
            = $this->app->before($this, __FUNCTION__, [
            'ocrTransaksjon' => $ocrTransaksjon
        ]);
        $eksisterendeVerdi = $this->hentOcrTransaksjon();
        if(strval($ocrTransaksjon) != strval($eksisterendeVerdi)) {
            $nyId = self::beregnId(
                $this->hentDato(),
                $this->hentRef(),
                $this->hentBetaler(),
                $ocrTransaksjon
            );
            /** @var Delbeløpsett $delbeløpsett */
            $delbeløpsett = $this->hentDelbeløp();
            $delbeløpsett->settVerdier((object)[
                'innbetaling' => $nyId,
                'ocr_transaksjon' => $ocrTransaksjon,
            ]);
            $this->oppdaterId($nyId);
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @param string $betaler
     * @return Innbetaling
     * @throws Exception
     */
    public function settBetaler(string $betaler): Innbetaling
    {
        list('betaler' => $betaler)
            = $this->app->before($this, __FUNCTION__, [
            'betaler' => $betaler
        ]);
        $eksisterendeVerdi = $this->hentBetaler();
        if($betaler != $eksisterendeVerdi) {
            $nyId = self::beregnId(
                $this->hentDato(),
                $this->hentRef(),
                $betaler,
                $this->hentOcrTransaksjon()
            );
            /** @var Delbeløpsett $delbeløpsett */
            $delbeløpsett = $this->hentDelbeløp();
            $delbeløpsett->settVerdier((object)[
                'innbetaling' => $nyId,
                'betaler' => $betaler,
            ]);
            $this->oppdaterId($nyId);
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @param string $ref
     * @return Innbetaling
     * @throws Exception
     */
    public function settRef(string $ref): Innbetaling
    {
        list('ref' => $ref)
            = $this->app->before($this, __FUNCTION__, [
            'ref' => $ref
        ]);
        $eksisterendeVerdi = $this->hentRef();
        if($ref != $eksisterendeVerdi) {
            $nyId = self::beregnId(
                $this->hentDato(),
                $ref,
                $this->hentBetaler(),
                $this->hentOcrTransaksjon()
            );
            /** @var Delbeløpsett $delbeløpsett */
            $delbeløpsett = $this->hentDelbeløp();
            $delbeløpsett->settVerdier((object)[
                'innbetaling' => $nyId,
                'ref' => $ref,
            ]);
            $this->oppdaterId($nyId);
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @noinspection PhpPossiblePolymorphicInvocationInspection
     * @param $konto
     * @return Innbetaling
     * @throws Exception
     */
    public function settKonto($konto): Innbetaling
    {
        list('konto' => $konto)
            = $this->app->before($this, __FUNCTION__, [
            'konto' => $konto
        ]);
        $eksisterendeVerdi = $this->hentKonto();
        if($konto != $eksisterendeVerdi) {
            /** @var Delbeløpsett $delbeløpsett */
            $delbeløpsett = $this->hentDelbeløp();
            $kontoId = $delbeløpsett->hentFørste()->hentKontoIdForNavn($konto);
            $delbeløpsett->settVerdier((object)[
                'konto_id' => $kontoId,
                'konto' => $konto,
            ]);
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function hentBeløp()
    {
        if(!isset($this->data->beløp)) {
            $this->data->beløp = 0;
            /** @var Modell\Innbetaling\Delbeløp $delbeløp */
            foreach($this->hentDelbeløp() as $delbeløp) {
                $this->data->beløp += $delbeløp->beløp;
            }

        }
        return $this->data->beløp;
    }

    /**
     * @param string $beløp
     * @return Innbetaling
     * @throws Exception
     */
    public function settBeløp(string $beløp): Innbetaling
    {
        list('beløp' => $beløp)
            = $this->app->before($this, __FUNCTION__, [
            'beløp' => $beløp
        ]);
        $originalBeløp = $this->hentBeløp();
        if ($beløp != $originalBeløp) {
            $rest = $beløp;
            $reduksjon = $beløp - $originalBeløp;

            /*
             * Dersom beløpets absolutt-verdi skal reduseres
             * (dvs opprinnnlig verdi og endringsdifferansen har ulike fortegn),
             * så må overskytende delbeløp trekkes ifra
             */
            if(($originalBeløp * $reduksjon) < 0) {
                foreach($this->hentDelbeløp() as $delbeløp) {
                    /*
                     * Delbeløpene må reduseres.
                     * Dersom rest og delbeløp har ulikt fortegn
                     * så må delbeløpet fjernes.
                     * Resten forblir uendret.
                     *
                     * Dersom rest og delbeløp har samme fortegn
                     * men absoluttverdien av resten er mindre enn absoluttverdien av delbeløpet,
                     * så må delbeløpet reduseres til resten.
                     * Resten blir 0.
                     */
                    $ulikeFortegn = ($rest * $delbeløp->hentBeløp()) < 0;
                    if(
                        $ulikeFortegn
                        ||	abs($rest) < abs($delbeløp->hentBeløp())
                    ) {
                        $delbeløp->sett('beløp', $ulikeFortegn ? 0 : $rest);
                        $rest = $ulikeFortegn ? $rest : 0;
                    }

                    /*
                     * Dersom rest og delbeløp har samme fortegn
                     * og absoluttverdien av resten er lik eller større enn absoluttverdien av delbeløpet,
                     * så er delbeløpet innenfor resten
                     * og kan beholdes uendret.
                     * Resten redusereres med delbeløpet
                     */
                    else {
                        $rest = round($rest - $delbeløp->hentBeløp(), 2);
                    }

                }
            }

            /*
             * Dersom beløpets absolutt-verdi skal økes
             * så legges det til et nytt delbeløp med økningen
             */
            else {
                $rest = round($rest - $this->hent('beløp'), 2);
            }
            if ($rest != 0) {
                $this->app->nyModell(Delbeløp::class, (object)[
                    'innbetaling'       => $this->hentId(),
                    'konto'             => $this->hent('konto'),
                    'ocr_transaksjon'	=> $this->hent('ocr_transaksjon'),
                    'ref'               => $this->hent('ref'),
                    'dato'              => $this->hent('dato'),
                    'merknad'           => $this->hent('merknad'),
                    'betaler'           => $this->hent('betaler'),
                    'registrerer'       => $this->hent('registrerer'),
                    'registrert'        => $this->hent('registrert'),
                    'beløp'             => $rest,
                ]);
            }

            $this->nullstill();
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @param Leieforhold|int $leieforholdId
     * @return float
     * @throws Exception
     */
    public function hentBeløpTilLeieforhold($leieforholdId)
    {
        settype($leieforholdId, 'string');
        if(!isset($this->data->leieforholdBeløp)) {
            $this->data->leieforholdBeløp = [];
            /** @var Modell\Innbetaling\Delbeløp $delbeløp */
            foreach($this->hentDelbeløp() as $delbeløp) {
                $delbeløpLeieforholdId = $delbeløp->leieforhold ? $delbeløp->leieforhold->hentId() : null;
                if($delbeløpLeieforholdId) {
                    settype($this->data->leieforholdBeløp[$delbeløpLeieforholdId], 'float');
                    $this->data->leieforholdBeløp[$delbeløpLeieforholdId] += $delbeløp->beløp;
                }
            }

        }
        return $this->data->leieforholdBeløp[$leieforholdId] ?? 0;
    }

    /**
     * @param int|null $delbeløpId
     * @return Delbeløp|Delbeløpsett|null
     * @throws Exception
     */
    public function hentDelbeløp(?int $delbeløpId = null)
    {
        /** @var Delbeløpsett $delbeløpsett */
        $delbeløpsett = $this->hentSamling('delbeløp');
        if(isset($delbeløpId)) {
            foreach ($delbeløpsett as $delbeløp) {
                if($delbeløp->hentId() == $delbeløpId) {
                    return $delbeløp;
                }
            }
            return null;
        }
        else {
            settype($this->samlinger->negative_delbeløp, 'array');
            settype($this->samlinger->positive_delbeløp, 'array');
            foreach ($delbeløpsett as $delbeløp) {
                if($delbeløp->hentBeløp() < 0) {
                    $this->samlinger->negative_delbeløp[] = $delbeløp;
                }
                else {
                    $this->samlinger->positive_delbeløp[] = $delbeløp;
                }
            }
        }
        return $delbeløpsett;
    }

    /**
     * Returnerer kun de delbeløpene i ei betaling som har positive fortegn.
     * En kreditt består av ett negativt, og ett eller flere positive delbeløp,
     * sammen med ett negativt krav.
     *
     * @return Delbeløpsett
     * @throws Exception
     */
    public function hentPositiveDelbeløp(): Delbeløpsett
    {
        /** @var Delbeløpsett $positiveDelbeløp */
        $positiveDelbeløp = $this->hentSamling('positive_delbeløp');
        return $positiveDelbeløp;
    }

    /**
     * Returnerer kun de delbeløpene i ei betaling som har negative fortegn.
     * En kreditt består av ett negativt, og ett eller flere positive delbeløp,
     * sammen med ett negativt krav.
     *
     * @return Delbeløpsett
     * @throws Exception
     */
    public function hentNegativeDelbeløp(): Delbeløpsett
    {
        /** @var Delbeløpsett $negativeDelbeløp */
        $negativeDelbeløp = $this->hentSamling('negative_delbeløp');
        return $negativeDelbeløp;
    }

    /**
     * Fjern avstemming
     *
     * Fjerner ei avstemming fra transaksjonen.
     *
     * * Dersom første argument er et krav, vil innbetalinga løsnes fra dette kravet
     * * Dersom første argument er et betalings-delbeløp, vil innbetalinga løsnes fra dette tilbakeføringen
     * * Dersom første argument er et vilkårlig Innbetaling-objekt, eller boolsk sann,
     * vil innbetalinga løsnes fra alle tilbakeføringer
     * * Dersom første argument er et leieforhold,
     * vil innbetalinga løsnes fra alle krav og tilbakeføringer tilhørende dette leieforholdet
     * * Dersom første argument er null, vil bare ufordelte deler av betalinga berøres,
     * for å frakople leieforhold. (argument 2 og 2)
     *
     * @param Krav|Delbeløp|Innbetaling|Leieforhold|null|true $avstemming Motkravet som betalinga skal koples fra
     * @param bool $frakopleLeieforhold
     *      Dersom sann vil betalinga ikke bare løsnes fra det enkelte kravet eller motbetalinga,
     *      men også frakoples fra leieforholdet sånn at det kan krediteres et annet leieforhold.
     * @param Leieforhold|null $leieforhold
     *      Dersom $motkrav er et Innbetalingsobjekt,
     *      må det angis her hvilket leieforhold den delen av betalinga som skal frakoples tilhører.
     *      Om denne er null vil all utlikning mot andre innbetalinger frakoples.
     * @return $this
     * @throws Exception
     */
    public function fjernAvstemming(
        $avstemming = null,
        bool $frakopleLeieforhold = false,
        ?Leieforhold $leieforhold = null
    ): Innbetaling
    {
        if ($avstemming instanceof Innbetaling) {
            $avstemming = true;
        }

        /** @var Delbeløp $delbeløp */
        foreach($this->hentDelbeløp() as $delbeløp) {
            if($avstemming instanceof Krav) {
                if($delbeløp->hentKrav() instanceof Krav
                    && $avstemming->hentId() == $delbeløp->hentKrav()->hentId()) {
                    $delbeløp->fjernAvstemming();
                    if($frakopleLeieforhold) {
                        $delbeløp->sett('leieforhold', null);
                    }
                }
            }

            elseif ($avstemming instanceof Leieforhold) {
                if($delbeløp->hentLeieforhold() instanceof Leieforhold
                    && $avstemming->hentId() == $delbeløp->hentLeieforhold()->hentId()) {
                    $delbeløp->fjernAvstemming();
                    if($frakopleLeieforhold) {
                        $delbeløp->sett('leieforhold', null);
                    }
                }
            }

            elseif ($avstemming instanceof Delbeløp) {
                if($delbeløp->hentKrav() === true
                    && $delbeløp->hentTilbakeføring()
                    && $avstemming->hentId() == $delbeløp->hentTilbakeføring()->hentId()) {
                    $delbeløp->fjernAvstemming();
                    if($frakopleLeieforhold) {
                        $delbeløp->sett('leieforhold', null);
                    }
                }
            }

            elseif ($avstemming === true) {
                if(
                    $delbeløp->hentKrav() === true
                    && (
                        $leieforhold === null
                        || (
                            $delbeløp->hentLeieforhold()
                            && $leieforhold->hentId() == $delbeløp->hentLeieforhold()->hentId()
                        )
                    )
                ) {
                    $delbeløp->fjernAvstemming();
                    if($frakopleLeieforhold) {
                        $delbeløp->sett('leieforhold', null);
                    }
                }
            }

            elseif ($avstemming === null) {
                if(
                    $frakopleLeieforhold
                    && $leieforhold
                    && $delbeløp->hentLeieforhold()
                    && $leieforhold->hentId() == $delbeløp->hentLeieforhold()->hentId()
                    && !$delbeløp->hentKrav()
                ) {
                    $delbeløp->sett('leieforhold', null);
                }
            }
        }
        return $this;
    }

    /**
     * Fordel
     *
     * Fordeler deler av eller hele betalinga mot krav eller motsatte innbetalinger.
     * Dersom et motkrav angis vil betalinga avstemmes mot dette kravet,
     * inntil maksbeløpet eller til betalinga er oppbrukt.
     * Dersom et hvilket som helst betalingsobjekt angis som motkrav,
     * vil betalinga avstemmes mot vilkårlige motbetalinger i leieforholdet som er oppgitt.
     * Dersom motkrav ikke er oppgitt vil de løse delbeløpene av betalinga avstemmes etter beste evne
     *
     * @param Krav|Delbeløp|null $mot Kravet som betalinga skal brukes mot
     *          Motkravet kan være et Krav,
     *          eller en tilbakeføring (delbeløp med motsatt fortegn)
     * @param string|null $maksbeløp Maksimalt beløp som skal brukes (ut ifra 0).
     *          Beløpet kommer i tillegg til evt eksisterende delbeløp
     *          Dersom null vil hele den ikke-konterte delen av betalinga føres på det angitte leieforholdet.
     *          Maksbeløp brukes kun dersom motkrav er oppgitt.
     * @param Leieforhold|null $leieforhold Dersom $krav er null eller et Innbetalingsobjekt,
     *          angis her hvilket leieforhold fordelingen skal foregå i.
     *          Dersom leieforhold er null vil alle eksisterende leieforhold i betalinga bli fordelt.
     *          Ved utlikning mot betalinger er denne påkrevd
     * @return float det fordelte beløpet
     * @throws Exception
     */
    public function avstem($mot = null, ?string $maksbeløp = null, ?Leieforhold $leieforhold = null) {
        $resultat = 0;
        /** @var Ocr\Transaksjon $ocr */
        $ocr = $this->hentOcrTransaksjon();
        $kid = $ocr ? $ocr->hentKid() : null;
        $kidKrav = $kid ? $this->leiebase->hentKravFraKid($kid)->hentElementer() : [];

        // Maksbeløp skal kun brukes dersom motkrav er angitt
        if( $mot === null ) {
            $maksbeløp = null;
        }

        // Kontroller at maksbeløpet har rett fortegn
        if( $maksbeløp !== null && ($this->hentBeløp() * $maksbeløp) < 0) {
            throw new Exception('Maksbeløp har feil fortegn');
        }

        // Kontroller at leieforhold er oppgitt dersom utlikning mot andre betalinger
        if( $mot instanceof Innbetaling && !$leieforhold) {
            throw new Exception('Leieforhold er påkrevd ved utlikning mot betaling.');
        }

        // Ved utlikning mot krav hentes leieforholdet fra dette kravet
        if( $mot instanceof Krav || $mot instanceof Delbeløp ) {
            $leieforhold = $mot->hentLeieforhold() ?? $leieforhold;
        }

        // Rydd opp før fordeling
        $this->samle();

        /** @var Delbeløp $del */
        foreach( $this->hentDelbeløp() as $del ) {

            // Fordelinga utføres kun mot de delene av betalinga som er kontert på dette leieforholdet
            //	men som ikke allerede er fordelt
            if(
                $del->hentKrav() === null
                && $del->hentLeieforhold()
                && (
                    $leieforhold === null
                    || ($del->leieforhold->hentId() == $leieforhold->hentId())
                )
            ) {
                if($mot instanceof Innbetaling) {
                    $mot->samle();
                    /** @var Delbeløpsett $kontrasett */
                    $kontrasett = $mot->hentDelbeløp()
                        ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`leieforhold`' => $leieforhold->hentId()])
                        ->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`krav` IS NULL']);
                    if($del->hentBeløp() < 0) {
                        $kontrasett->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`beløp` >' => $leieforhold->hentId()]);
                    }
                    else {
                        $kontrasett->leggTilFilter(['`' . Delbeløp::hentTabell() . '`.`beløp` <' => $leieforhold->hentId()]);
                    }
                    $kontrasett->låsFiltre();
                }
                elseif( $mot ) {
                    $kontrasett = [$mot];
                }
                else {
                    $kontrasett = [];
                    if( strval($this->leiebase->hentKidBestyrer()->hentLeieforholdIdFraKid($kid)) == strval($del->hentLeieforhold()) ) {
                        $kontrasett = $kidKrav;
                    }
                    $uteståendeKrav = $del->hentLeieforhold()->hentUteståendeKrav()->hentElementer();
                    $kontrasett = array_merge($kontrasett, $uteståendeKrav);
                }

                $resultat += $del->avstem($kontrasett, $maksbeløp);
            }
        }
        $this->nullstill();
        $this->samle();
        return $resultat;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function sendKvitteringsepost(): Innbetaling
    {
        $this->samle();
        $leieforholdsett = $this->hentLeieforhold();

        /** @var Leieforhold $leieforhold */
        foreach($leieforholdsett as $leieforhold) {
            $emne =	"Kvittering for registrert betaling";
            $html = $this->app->vis(Kvittering::class, [
                'innbetaling' => $this,
                'leieforhold' => $leieforhold
            ]);
            $tekst = $this->app->vis(Kvittering::class, [
                'innbetaling' => $this,
                'leieforhold' => $leieforhold
            ])->settMal('felles/epost/innbetaling/Kvittering.txt')
            ->settMalForElementer(Avstemmingslinje::class, 'felles/epost/innbetaling/Avstemmingslinje.txt');
            $adressefelt = $leieforhold->hentEpost('innbetalingsbekreftelse');
            if ($adressefelt) {
                $this->leiebase->sendMail((object)[
                    'to'		=> implode(',', $adressefelt),
                    'subject'	=> $emne,
                    'html'		=> $html,
                    'text'		=> $tekst,
                    'type' => 'betalingskvittering'
                ]);
            }
        }

        return $this;
    }

    /**
     * @param Leieforhold|int $leieforholdId
     * @return Delbeløp[]
     * @throws Exception
     */
    public function hentDelbeløpTilLeieforhold($leieforholdId): array
    {
        settype($leieforholdId, 'string');
        if (!$leieforholdId) {
            return [];
        }
        if(!isset($this->samlinger->leieforholdDelbeløp)) {
            $this->samlinger->leieforholdDelbeløp = [];
            /** @var Delbeløp $delbeløp */
            foreach($this->hentDelbeløp() as $delbeløp) {
                $delbeløpLeieforholdId = strval($delbeløp->leieforhold);
                if($delbeløpLeieforholdId) {
                    settype($this->samlinger->leieforholdDelbeløp[$delbeløpLeieforholdId] , 'array');
                    $this->samlinger->leieforholdDelbeløp[$delbeløpLeieforholdId][] = $delbeløp;
                }
            }
        }
        return $this->samlinger->leieforholdDelbeløp[$leieforholdId] ?? [];
    }

    /**
     * Frakople motbetalinger
     *
     * Subfunksjon for metoden frakople()
     * Denne metoden påvirker ikke denne betalinga, kun andre betalinger med motsatt fortegn
     *
     * @param int $leieforholdId Leieforhold-id
     * @param float $beløp Beløpet som skal koples til denne betalinga.
     * @return float det frakoplede beløpet
     * @throws Exception
     * @noinspection PhpUnused
     */
    protected function frakopleMotbetalinger(int $leieforholdId, string $beløp ) {
        $resultat = 0;

        if(!is_numeric($beløp)) {
            throw new Exception('$beløp må være numerisk');
        }
        $beløp = -$beløp;
        $resterendeBeløp = $beløp;

        $motbetalinger = $this->app->hentSamling(Delbeløp::class)
            ->leggTilFilter([
                'leieforhold' => strval($leieforholdId),
                'beløp ' . ($beløp > 0 ? ">" : "<") => 0,
                'krav' => '0'
            ])
        ;

        /** @var Delbeløp $motbetaling */
        foreach( $motbetalinger as $motbetaling ) {
            if( abs($motbetaling->beløp) <= abs($resterendeBeløp) ) {
                // Delbeløpet kan frakoples i sin helhet
                $motbetaling->sett('krav', null);
                $resterendeBeløp -= $motbetaling->beløp;
                $resultat += $motbetaling->beløp;
            }

            else if( $resterendeBeløp ) {
                // Delbeløpet må splittes så en del av det kan koples fra
                $nyttDelbeløp = $motbetaling->splitt($resterendeBeløp);
                $nyttDelbeløp->sett('krav', null);

                $resterendeBeløp	= 0;
                $resultat	+= $resterendeBeløp;
            }
        }
        return $resultat;
    }

    /**
     * Samle
     *
     * Rydder opp i unødig fragmentering ved å samle delbeløp til ett større
     * der dette er hensiktsmessig.
     * Samlede delbeløp beholder id-nummeret til det første de identiske delbeløpene.
     *
     * @return $this
     * @throws Exception
     */
    public function samle(): Innbetaling
    {
        /** @var string[][][] $a Array med [leieforholdId][kravId][delbeløpId] = beløp */
        $a = [];
        $delbeløpsett = $this->hentDelbeløp();
        $delbeløpsett->leggTilEavFelt('er_fremmed_beløp');
        $delbeløpsett->leggTilEavFelt('tilbakeføring');

        /** @var Delbeløp $delbeløp */
        foreach( $delbeløpsett as $delbeløp ) {
            $tilbakeføring = null;
            $leieforholdId = strval($delbeløp->leieforhold);
            $leieforholdId = $delbeløp->hentErFremmedBeløp() ? 'fremmed' : $leieforholdId;
            settype($a[$leieforholdId], 'array');

            /*
             * Siden null som nøkkel formes som en tom streng, vil null som krav behandles annerledes enn 0
             * (null => '' og '0' => 0).
             * For å unngå at betalinger med blandet transaksjonsretning kollapser
             * (f.eks kredittlinker med et positivt og et negativt beløp)
             * så må disse beløpene behandles adskilt.
             * Dette angis med positive og negative kravnøkler
             */
            $krav = $delbeløp->hentKrav();
            $kravId = $krav instanceof Krav ? $krav->hentId() : 'null';
            if($krav === true) {
                $tilbakeføring = $delbeløp->hentTilbakeføring();
                if($tilbakeføring) {
                    $kravId = 'betaling-' . $tilbakeføring->hentId();
                }
                elseif ($delbeløp->erTilbakeføring()) {
                    $kravId = '-betaling-' . $delbeløp->hentId();
                }
                else {
                    $kravId = $delbeløp->hentBeløp() < 0 ? '-betaling' : 'betaling';
                }
            }

            settype(
                $a [$leieforholdId] [$kravId],
                'array'
            );
            $a [$leieforholdId] [$kravId] [$delbeløp->hentId()] = $delbeløp->beløp;
        }

        /** @var string[][] $kravsett */
        foreach( $a as $kravsett ) {
            /** @var string[] $krav */
            foreach( $kravsett as $krav ) {
                if( count($krav) > 1) {
                    $beløp = array_sum( $krav );
                    $førsteDelbeløpId = min(array_keys($krav));
                    $delbeløp = $this->app->hentModell(Delbeløp::class, $førsteDelbeløpId);
                    $delbeløp->sett('beløp', $beløp);
                    foreach($krav as $delbeløpId => $beløp) {
                        if($delbeløpId != $førsteDelbeløpId) {
                            $delbeløp = $this->app->hentModell(Delbeløp::class, $delbeløpId);
                            $delbeløp->slett();
                        }
                    }
                }
            }
        }

        $this->reset();
        return $this;
    }

    /**
     * @return Leieforholdsett
     * @throws Exception
     */
    public function hentLeieforhold(): Leieforholdsett
    {
        /** @var Leieforholdsett $leieforholdsett */
        $leieforholdsett = $this->app->hentSamling(Leieforhold::class)
            ->leggTilInnerJoin(static::hentTabell(), static::hentTabell() . '.leieforhold = ' . Leieforhold::hentTabell() . '.' . Leieforhold::hentPrimærnøkkelfelt())
            ->leggTilFilter([static::hentTabell() . '.' . static::hentPrimærnøkkelfelt() => $this->hentId()]);

        if (!isset($this->samlinger->leieforhold)) {
            $this->samlinger->leieforhold = $leieforholdsett->hentElementer();
        }
        $leieforholdsett->lastFra($this->samlinger->leieforhold);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $leieforholdsett;
    }

    /**
     * Konter
     *
     * Konterer deler av eller hele betalinga mot et bestemt leieforhold.
     * Dersom ei betaling skal flyttes fra et leieforhold til et annet
     * må den eksisterende konteringa frakoples først med Innbetaling::frakople().
     *
     * @param $leieforhold Leieforhold Leieforholdet som beløpet skal konteres mot, i tillegg til tidligere konteringer
     * @param float|null $maksbeløp Maksimalt beløp som skal konteres mot dette leieforholdet (ut ifra 0).
     *          Beløpet kommer i tillegg til evt tidligere konterte delbeløp
     *          Dersom null vil hele den ikke-konterte delen av betalinga føres på det angitte leieforholdet.
     * @return Innbetaling
     * @throws Exception
     */
    public function konter(Leieforhold $leieforhold, ?float $maksbeløp = null ): Innbetaling
    {
        list('leieforhold' => $leieforhold, 'maksbeløp' => $maksbeløp)
            = $this->app->before($this, __FUNCTION__, [
            'leieforhold' => $leieforhold, 'maksbeløp' => $maksbeløp
        ]);

        if( (isset($maksbeløp) && $this->hentBeløp() * $maksbeløp) < 0) {
            throw new Exception("Feil fortegn gitt på maksbeløp i Innbetaling::konter().");
        }
        if(!strval($leieforhold)) {
            throw new Exception("Ugyldig leieforhold");
        }

        $this->samle();

        $rest = $maksbeløp;
        foreach ($this->hentDelbeløp() as $delbeløp) {
            if($delbeløp->leieforhold === null) {
                if ($rest === null) {
                    $delbeløp->leieforhold = $leieforhold;
                }
                elseif ($rest && $delbeløp->beløp * $maksbeløp >=0) {
                    if($delbeløp->beløp <= $rest) {
                        $delbeløp->leieforhold = $leieforhold;
                        $rest -= $delbeløp->beløp;
                    }
                    else {
                        $nyDel = $delbeløp->splitt($rest);
                        $nyDel->leieforhold = $leieforhold;
                        $rest = 0;
                    }
                }
            }
        }

        $this->samle();
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * Henter kravet i en kredittkombinasjon
     *
     * En kreditt består av ett negativt, og ett eller flere positive delbeløp,
     * sammen med ett negativt krav.
     *
     * @return Krav|null
     * @throws Exception
     */
    public function hentKredittkrav(): ?Krav
    {
        if( $this->hentKonto() != self::KTO_KREDITT) {
            return null;
        }

        /** @var Delbeløp $negativtDelbeløp */
        $negativtDelbeløp = $this->hentNegativeDelbeløp()->hentFørste();
        return $negativtDelbeløp->hentKrav();
    }

    /**
     * @return object
     * @throws Exception
     */
    public function hentEavKonfigurering(): object
    {
        if(!isset($this->eavConfigs)) {
            if(!isset(EgenskapVarcharObjekt::$mellomlager[static::class])) {
                /** @var Egenskapsett $egenskaper */
                $egenskaper = $this->app->hentSamling(EgenskapVarcharObjekt::class)
                    ->leggTilFilter(['modell' => static::class])
                    ->låsFiltre()
                    ->leggTilSortering('rekkefølge');
                $kilde = EgenskapVarcharObjekt::hentTabell();
                EgenskapVarcharObjekt::$mellomlager[static::class] = (object)[$kilde => (object)[]];
                /** @var EgenskapVarcharObjekt $egenskap */
                foreach ($egenskaper as $egenskap) {
                    $kode = $egenskap->hent('kode');
                    EgenskapVarcharObjekt::$mellomlager[static::class]->{$kilde}->{$kode} = $egenskap;
                }
            }
            $this->eavConfigs = EgenskapVarcharObjekt::$mellomlager[static::class];
        }
        return parent::hentEavKonfigurering();
    }

    /**
     * @param string $kilde
     * @return stdClass|null
     * @throws Exception
     */
    public function hentEavVerdiObjekter(string $kilde): ?stdClass
    {
        if(!$this->eavValueObjects) {
            $this->eavValueObjects = new stdClass();

            /** @var VerdiVarcharObjektsett $eavVerdiObjekter */
            $eavVerdiObjekter = $this->app->hentSamling(VerdiVarcharObjekt::class)

                ->leggTilInnerJoin(EavEgenskap::hentTabell(), VerdiVarcharObjekt::hentTabell() . '.egenskap_id = ' . EavEgenskap::hentTabell() . '.id')
                ->leggTilFilter([
                    '`' . EavEgenskap::hentTabell() . '`.`modell`' => static::class,
                    '`' . VerdiVarcharObjekt::hentTabell() . '`.`objekt_id`' => $this->getId()
                ]);
            /** @var EavValuesInterface $eavVerdiObjekt */
            foreach ($eavVerdiObjekter as $eavVerdiObjekt) {
                $verdiKilde = $eavVerdiObjekt->getConfig()::getDbTable();
                $kode = $eavVerdiObjekt->getConfig()->getSourceField();
                settype($this->eavValueObjects->{$verdiKilde}, 'object');
                $this->eavValueObjects->{$verdiKilde}->{$kode} = $eavVerdiObjekt;
            }
        }

        return parent::hentEavVerdiObjekter($kilde);
    }

    /**
     * @param string $nyId
     * @return Innbetaling
     * @throws CoreModelException
     * @throws Exception
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    private function oppdaterId(string $nyId): Innbetaling
    {
        list('nyId' => $nyId)
            = $this->app->before($this, __FUNCTION__, [
            'nyId' => $nyId
        ]);
        $originalId = $this->hentId();
        $this->nullstill()->settId($nyId);

        /* Oppdater EAV-verdier */
        /** @var VerdiVarcharObjektsett $eavVerdiObjekter */
        $eavVerdiObjekter = $this->app->hentSamling(VerdiVarcharObjekt::class)

            ->leggTilInnerJoin(EavEgenskap::hentTabell(), VerdiVarcharObjekt::hentTabell() . '.egenskap_id = ' . EavEgenskap::hentTabell() . '.id')
            ->leggTilFilter([
                EavEgenskap::hentTabell() . '.modell' => static::class,
                VerdiVarcharObjekt::hentTabell() . '.objekt_id' => $originalId
            ])->låsFiltre();
        $eavVerdiObjekter->sett('objekt', $nyId);

        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @return void
     * @throws CoreModelException
     */
    public function slett()
    {
        /** @var Delbeløpsett $delbeløpSett */
        $delbeløpSett = $this->hentDelbeløp();
        $delbeløpIder = $delbeløpSett->hentIdNumre();
        $delbeløpSett->forEach(function (Delbeløp $delbeløp) {
            $delbeløp->fjernAvstemming();
        });
        parent::slett();
        $this->app->hentSamling(Modell\Eav\VerdiVarcharObjekt::class)
            ->leggTilFilter(['modell' => Delbeløp::class])
            ->leggTilFilter(['objekt_id' => $delbeløpIder])
            ->låsFiltre()->slettAlle();
    }
}