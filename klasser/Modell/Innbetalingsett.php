<?php

namespace Kyegil\Leiebasen\Modell;


use Exception;
use Kyegil\CoreModel\Interfaces\CoreModelInterface;
use Kyegil\Leiebasen\Modell\Eav\EgenskapVarcharObjekt as Egenskap;
use Kyegil\Leiebasen\Modell\Eav\VerdiVarcharObjekt as Verdi;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløpsett;
use Kyegil\Leiebasen\Modell\Ocr\Transaksjon;
use Kyegil\Leiebasen\Samling;
use Kyegil\Leiebasen\Sett;

/**
 * Innbetalingsett
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Innbetaling current()
 */
class Innbetalingsett extends Sett
{
    /** @var string */
    protected string $model = Innbetaling::class;

    /** @var Innbetaling[] */
    protected ?array $items = null;

    /**
     * Legg til LEFT JOIN for OCR-transaksjon
     *
     * @return Innbetalingsett
     */
    public function leggTilLeftJoinForOcrTransaksjon(): Innbetalingsett
    {
        return $this
            ->leggTilLeftJoin(Transaksjon::hentTabell(),
                '`' . Innbetaling::hentTabell() . '`.`ocr_transaksjon` = `'
                . Transaksjon::hentTabell() . '`.`' . Transaksjon::hentPrimærnøkkelfelt() . '`'
            );
    }

    /**
     * @return Innbetalingsett
     * @throws Exception
     */
    public function leggTilOcrTransaksjonModell(): Innbetalingsett
    {
        return $this->leggTilLeftJoinForOcrTransaksjon()
            ->leggTilModell(Transaksjon::class);
    }

    /**
     * @return Delbeløpsett
     * @throws Exception
     */
    public function inkluderDelbeløp(): Delbeløpsett
    {
        /** @var Delbeløpsett $delbeløp */
        $delbeløp = $this->inkluder('delbeløp');
        return $delbeløp;
    }

    /**
     * Legg til Join for EAV-verdier
     *
     * @param string $feltKode Felt-egenskaps-koden som skal hentes
     * @param bool $innerJoin
     * @param string|null $modell Modell for egenskapen, dersom annet enn gjeldende modell benyttes
     * @param string|null $joinSource Tabell for Join-kriterie, dersom annen kriterie enn modellens primærnøkkel
     * @param string|null $joinField Felt for Join-kriterie, dersom annen kriterie enn modellens primærnøkkel
     * @param string|null $feltAlias
     * @return Samling
     * @throws Exception
     */
    public function leggTilEavVerdiJoin(
        string $feltKode,
        bool $innerJoin = false,
        ?string $modell = null,
        ?string $joinSource = null,
        ?string $joinField = null,
        ?string $feltAlias = null
    ): Samling
    {
        $modell = $modell ?? $this->hentModell();
        $feltAlias = $feltAlias ?? $feltKode;
        if (is_a($modell, CoreModelInterface::class, true)) {
            $joinSource = $joinSource ?: $modell::getDbTable();
            $joinField = $joinField ?: $modell::getDbPrimaryKeyField();
            $condition = $condition ?? '`' . $feltAlias . '`.`objekt_id` = `' . $joinSource . '`.`' . $joinField . '`';
        }
        $tp = $this->mysqli->table_prefix;
        $egenskapTabell = $tp . Egenskap::hentTabell();
        $verdiTabell = $tp . Verdi::hentTabell();
        $subQuery = <<<SUBQUERY
            (
             SELECT `{$this->mysqli->escape($verdiTabell)}`.`objekt_id`, `{$this->mysqli->escape($verdiTabell)}`.`verdi`
             FROM `{$this->mysqli->escape($verdiTabell)}` INNER JOIN `{$this->mysqli->escape($egenskapTabell)}` ON `{$this->mysqli->escape($verdiTabell)}`.`egenskap_id` = `{$this->mysqli->escape($egenskapTabell)}`.`id`
             WHERE `{$this->mysqli->escape($egenskapTabell)}`.`kode` = '{$this->mysqli->escape($feltKode)}'
             AND `{$this->mysqli->escape($egenskapTabell)}`.`modell` = '{$this->mysqli->escape($modell)}'
            ) 
            SUBQUERY;
        $join = ($innerJoin ? 'INNER ': 'LEFT ') . "JOIN $subQuery AS `{$this->mysqli->escape($feltAlias)}` ON {$this->mysqli->escape($condition)}";
        if (isset($this->joins[$feltAlias]) && str_replace('`', '', $this->joins[$feltAlias]) != str_replace('`', '', $join)) {
            throw new Exception("Kan ikke re-definere alias `{$feltAlias}`");
        }
        $this->joins[$feltAlias] = $join;
        return $this;
    }
}