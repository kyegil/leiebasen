<?php

namespace Kyegil\Leiebasen\Modell;


use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\Leiebasen\Modell\Person\Adgangsett;
use Kyegil\Leiebasen\Sett;

/**
 * Class Personsett
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Person current()
 */
class Personsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Person::class;

    /**
     * @var Person[]
     */
    protected ?array $items = null;

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
    }

    /**
     * @return Adgangsett
     * @throws \Exception
     */
    public function inkluderAdganger(): Adgangsett
    {
        /** @var Adgangsett $adgangsett */
        $adgangsett = $this->inkluder('adganger');
        return $adgangsett;
    }
}