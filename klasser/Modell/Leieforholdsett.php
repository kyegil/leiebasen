<?php

namespace Kyegil\Leiebasen\Modell;


use Exception;
use Kyegil\CoreModel\CoreModel;
use Kyegil\CoreModel\CoreModelDiException;
use Kyegil\CoreModel\CoreModelException;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløpsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontraktsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav\LeieforholdDelkravtypesett;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietakersett;
use Kyegil\Leiebasen\Modell\Leieforhold\Notatsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Oppsigelse;
use Kyegil\Leiebasen\Modell\Person\Adgangsett;
use Kyegil\Leiebasen\Sett;

/**
 * Class Leieforholdsett
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Leieforhold current()
 */
class Leieforholdsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Leieforhold::class;

    /**
     * @var Leieforhold[]
     */
    protected ?array $items = null;

    /**
     * Legg til nødvendige felter for å hente navnet på leieforholdet uten ytterligere db-spørringer
     *
     * @return $this
     */
    public static function subQueryForNavn(
        string $joinFelt = 'leieforholdnr',
        ?string $joinKilde = 'leieforhold'
    ): string
    {
        $join = isset($joinKilde)
            ? "`$joinKilde`.`$joinFelt`"
            : "`$joinFelt`";
        $tp = CoreModel::$tablePrefix;
        $leietakerTabell = $tp . Leietaker::hentTabell();
        $personTabell = $tp . Person::hentTabell();
        $subQuery = <<< SQL
  SELECT
  IF(COUNT(DISTINCT `personer`.`personid`) > 2, CONCAT(LEFT(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', '), CHAR_LENGTH(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL,`kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`,`personer`.`etternavn`,CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', ')) - INSTR(REVERSE(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', ')), ' ,') - 1), ' og ', SUBSTRING(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', '), CHAR_LENGTH(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', ')) - INSTR(REVERSE(GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ', ')), ' ,') + 2)),   GROUP_CONCAT(DISTINCT IF(`personer`.`personid` IS NULL, `kontraktpersoner`.`leietaker`, IF(`personer`.`er_org`, `personer`.`etternavn`, CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`))) SEPARATOR ' og ')) AS `navn`

  FROM `{$leietakerTabell}` AS `kontraktpersoner`
  LEFT JOIN `{$personTabell}` AS `personer` ON `personer`.`personid` = `kontraktpersoner`.`person`
  WHERE `kontraktpersoner`.`slettet` IS NULL
  AND `kontraktpersoner`.`leieforhold` = {$join}
  ORDER BY `kontraktpersoner`.`kopling`
SQL;
        return $subQuery;
    }

    /**
     * @param string $modell
     * @param object[] $di
     * @throws CoreModelDiException
     */
    public function __construct(string $modell, array $di)
    {
        parent::__construct($modell, $di);
        $this
            ->leggTilLeftJoinForSisteKontrakt()
            ->leggTilLeftJoinForLeieobjekt()
            ->leggTilLeftJoinForOppsigelse()
        ;
    }

    /**
     * Legg til filter for søk på leietakernavn
     *
     * @param string $søkestreng
     * @return $this
     * @throws Exception
     * @deprecated Det kan nå søkes på navn direkte i leieforhold-tabellen
     */
    public function leggTilLeietakerNavnFilter(string $søkestreng): Leieforholdsett
    {
        $søkestreng = trim($søkestreng);
        if (!$søkestreng) {
            return $this;
        }
        $søkeOrd = explode(' ', $søkestreng);
        $filter = [];
        if ($søkeOrd) {
            foreach ($søkeOrd as $ord) {
                if (trim($ord)) {
                    $filter[] = [
                        'OR' => [
                            '`leietaker_navn_filter`.`leietaker` LIKE' => '%' . $ord . '%',
                            '`personfilter`.`fornavn` LIKE' => '%' . $ord . '%',
                            '`personfilter`.`etternavn` LIKE' => '%' . $ord . '%',
                        ]
                    ];
                }
            }
        }

        if (!$filter) {
            return $this;
        }

        $bundneVerdier = [];
        $subQuery = $this->mysqli->prepareSelectQueryFromConfig((object)[
            'fields' => ['`leietaker_navn_filter`.`leieforhold`'],
            'distinct' => true,
            'source' => '`' . $this->mysqli->table_prefix . Leietaker::hentTabell() . '` AS `leietaker_navn_filter`'
            . 'LEFT JOIN `' . $this->mysqli->table_prefix . Person::hentTabell() . '` AS `personfilter` ON `leietaker_navn_filter`.`person` = `personfilter`.`personid`',
            'where' => $filter
        ],$bundneVerdier);
        $this->leggTilSubQueryJoin(
            $subQuery,
            'leietaker_navn_filter',
            "`leietaker_navn_filter`.`leieforhold` = `{$this->dbTable}`.`{$this->dbPrimaryKeyField}`",
            'inner',
            $bundneVerdier
        );
        return $this;
    }

    /**
     * Legg til join for å laste leieforholdets utdelingsorden
     *
     * @param string|null $alias
     * @return $this
     */
    public function leggTilLeftJoinForUtdelingsplassering(?string $alias = null): Leieforholdsett {
        $alias = $alias ?: 'utdelingsorden';
        $this
            ->leggTilLeftJoin([$alias => 'utdelingsorden'], '`' . Leieforhold::hentTabell() . '`.`regningsobjekt` = `' . $alias . '`.`leieobjekt` AND `utdelingsorden`.`rute` = ' . $this->app->hentValg('utdelingsrute'))
        ;
        return $this;
    }

    /**
     * Legg til utdelingsplassering
     *
     * Join for utdelingsplassering må legges til først
     *
     * @param bool $synkende
     * @param string $feltAlias Mulig alias for feltnavnet 'plassering'
     * @param string|null $tabellAlias Mulig alias for tabellen 'utdelingsorden'
     * @return $this
     */
    public function leggTilUtdelingsplassering(bool $synkende = false, string $feltAlias = 'plassering', string $tabellAlias = 'utdelingsorden'): Leieforholdsett
    {
        $this->leggTilFelt(
            $tabellAlias,
            $feltAlias,
            null,
            'utdelingsplassering',
            'leieforhold_ekstra'
        );
        $this->leggTilSortering('utdelingsplassering', $synkende, 'leieforhold_ekstra');
        return $this;
    }

    /**
     * Legg til LEFT JOIN for kontrakt
     *
     * @return $this
     */
    public function leggTilLeftJoinForSisteKontrakt(): Leieforholdsett
    {
        return $this
            ->leggTilLeftJoin(Kontrakt::hentTabell(), Leieforhold::hentTabell() . '.`siste_kontrakt` = ' . Kontrakt::hentTabell() . '.' . Kontrakt::hentPrimærnøkkelfelt());
    }

    /**
     * Legg til LEFT JOIN for leieobjekt
     *
     * @return $this
     */
    public function leggTilLeftJoinForLeieobjekt(): Leieforholdsett
    {
        return $this
            ->leggTilLeftJoin(Leieobjekt::hentTabell(), '`' . Leieforhold::hentTabell() . '`.`leieobjekt` = `' . Leieobjekt::hentTabell() . '`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '`');
    }

    /**
     * Legg til LEFT JOIN for regningsobjekt
     *
     * @return $this
     */
    public function leggTilLeftJoinForRegningsobjekt(): Leieforholdsett
    {
        return $this
            ->leggTilLeftJoin(['regningsobjekt' => Leieobjekt::hentTabell()], '`' . Leieforhold::hentTabell() . '`.`regningsobjekt` = `regningsobjekt`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '`');
    }

    /**
     * Legg til LEFT JOIN for oppsigelse
     *
     * @return $this
     */
    public function leggTilLeftJoinForOppsigelse(): Leieforholdsett
    {
        return $this
            ->leggTilLeftJoin(Oppsigelse::hentTabell(), Leieforhold::hentTabell() . '.' . Leieforhold::hentPrimærnøkkelfelt() . '=' . Oppsigelse::hentTabell() . '.' . Oppsigelse::hentPrimærnøkkelfelt());
    }

    /**
     * Legg til LEFT JOIN for betalingsplan
     *
     * @return $this
     */
    public function leggTilLeftJoinForBetalingsplan(): Leieforholdsett
    {
        return $this
            ->leggTilLeftJoin(Leieforhold\Betalingsplan::hentTabell(), Leieforhold\Betalingsplan::hentTabell() . '.`leieforhold_id` = ' . Leieforhold::hentTabell() . '.' . Leieforhold::hentPrimærnøkkelfelt());
    }

    /**
     * Legg til kontrakt-modell
     *
     * @return $this
     */
    public function leggTilKontraktModell(): Leieforholdsett
    {
        return $this->leggTilLeftJoinForSisteKontrakt()
            ->leggTilModell(Kontrakt::class, 'kontrakt');
    }

    /**
     * Legg til leieobjekt-modell
     *
     * @param array|null $felter
     * @return $this
     * @throws CoreModelException
     */
    public function leggTilLeieobjektModell(?array $felter = null): Leieforholdsett
    {
        return $this->leggTilLeftJoinForLeieobjekt()
            ->leggTilModell(Leieobjekt::class, 'leieobjekt', null, $felter);
    }

    /**
     * Legg til leieobjekt-modell
     *
     * @param array|null $felter
     * @return $this
     * @throws CoreModelException
     */
    public function leggTilRegningsobjektModell(?array $felter = null): Leieforholdsett
    {
        return $this->leggTilLeftJoinForRegningsobjekt()
            ->leggTilModell(Leieobjekt::class, 'regningsobjekt', 'regningsobjekt', $felter);
    }

    /**
     * Legg til oppsigelse-modell
     *
     * @param array|null $felter
     * @return $this
     * @throws CoreModelException
     */
    public function leggTilOppsigelseModell(?array $felter = null): Leieforholdsett
    {
        return $this->leggTilLeftJoinForOppsigelse()
            ->leggTilModell(Oppsigelse::class, null, null, $felter);
    }

    /**
     * Legg til betalingsplan-modell
     *
     * @param array|null $felter
     * @return $this
     * @throws CoreModelException
     */
    public function leggTilBetalingsplanModell(?array $felter = null): Leieforholdsett
    {
        return $this->leggTilLeftJoinForBetalingsplan()
            ->leggTilModell(Betalingsplan::class, null, null, $felter);
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function forhåndslastBetalinger(): Leieforholdsett
    {
        /** @var Delbeløp[][] $beløpPerLeieforhold */
        $beløpPerLeieforhold = [];
        $filtre = $this->hentFiltre();
        array_walk($filtre, function (&$filter){
            if (strpos($filter, '`' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`') === 0) {
                $filter = str_replace(
                    '`' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`',
                    '`' . Delbeløp::hentTabell() . '`.`leieforhold`',
                    $filter
                );
            }
        });
        /** @var Delbeløpsett $delbeløpsett */
        $delbeløpsett = $this->app->hentSamling(Delbeløp::class);
        $delbeløpsett
            ->filtrer($filtre)
            ->leggTilSortering('dato')
        ;
        foreach($delbeløpsett as $delbeløp) {
            $rawData = $delbeløp->getRawData();
            $beløpPerLeieforhold[$rawData->innbetalinger->leieforhold][] = $delbeløp;
        }

        /** @var Leieforhold $leieforhold */
        foreach($this->hentElementer() as $leieforhold) {
            $leieforhold->setPreloadCollectionItems('innbetalinger', $beløpPerLeieforhold[$leieforhold->hentId()] ?? []);
        }

        return $this;
    }

    /**
     * @return Kontraktsett
     * @throws Exception
     */
    public function inkluderKontrakter(): Kontraktsett {
        /** @var Kontraktsett $kontrakter */
        $kontrakter = $this->inkluder('kontrakter');
        return $kontrakter;
    }

    /**
     * @return Leietakersett
     * @throws Exception
     */
    public function inkluderLeietakere(): Leietakersett {
        /** @var Leietakersett $leietakere */
        $leietakere = $this->inkluder('leietakere');
        return $leietakere;
    }

    /**
     * @param Kravsett|null $krav
     * @return Kravsett
     * @throws Exception
     */
    public function inkluderKrav(?Kravsett $krav = null): Kravsett {
        /** @var Kravsett $krav */
        $krav = $this->inkluder('krav', $krav);
        return $krav;
    }

    /**
     * @return Delbeløpsett
     * @throws Exception
     */
    public function inkluderInnbetalingDelbeløp(): Delbeløpsett {
        /** @var Delbeløpsett $innbetalingDelbeløp */
        $innbetalingDelbeløp = $this->inkluder('innbetaling_delbeløp');
        return $innbetalingDelbeløp;
    }

    /**
     * @return LeieforholdDelkravtypesett
     * @throws Exception
     */
    public function inkluderDelkravtyper(): LeieforholdDelkravtypesett {
        /** @var LeieforholdDelkravtypesett $delkravtyper */
        $delkravtyper = $this->inkluder('delkravtyper');
        return $delkravtyper;
    }

    /**
     * @return Notatsett
     * @throws Exception
     */
    public function inkluderNotater(): Notatsett {
        /** @var Notatsett $notater */
        $notater = $this->inkluder('notater');
        return $notater;
    }

    /**
     * @return Adgangsett
     * @throws Exception
     */
    public function inkluderAdganger(): Adgangsett {
        /** @var Adgangsett $adganger */
        $adganger = $this->inkluder('adganger');
        return $adganger;
    }
}