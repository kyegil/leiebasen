<?php

namespace Kyegil\Leiebasen\Modell;


use Kyegil\Leiebasen\Sett;

/**
 * Class Eksportprofilsett
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Eksportprofil current()
 */
class Eksportprofilsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Eksportprofil::class;

    /**
     * @var Eksportprofil[]
     */
    protected ?array $items = null;
}