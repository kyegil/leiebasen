<?php

namespace Kyegil\Leiebasen\Modell;


use DateTimeImmutable;
use Exception;
use Kyegil\CoreModel\Interfaces\CoreModelInterface;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Samling;
use stdClass;

/**
 * Class Konto
 * @package Kyegil\Leiebasen\Modell
 *
 * @property integer $id
 * @property string $navn
 * @property stdClass $oppsett
 *
 * @method int hentId()
 * @method string hentNavn()
 * @method stdClass hentOppsett()
 *
 * @method $this settKode(string $kode)
 * @method $this settNavn(string $navn)
 * @method $this settOppsett(stdClass $oppsett)
 */
class Eksportprofil extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'eksportoppsett';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Eksportprofilsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'id'    => [
                'type'  => 'integer'
            ],
            'navn'    => [
                'type'  => 'string'
            ],
            'oppsett'    => [
                'type'  => 'json',
            ],
        ];
    }

    /**
     * Returnerer filtrene
     * med alle forekomster av '{fra}' og '{til}' erstattet med faktiske verdier
     *
     * @param array $valg
     * @param array $filter
     * @return array $filter
     */
    public static function settDatofilter(array $valg, array $filter): array
    {
        array_walk_recursive($filter, function (&$element) use ($valg) {
            /** @var DateTimeImmutable|null $fradato */
            $fradato = $valg['fradato'] ?? null;
            /** @var DateTimeImmutable|null $tildato */
            $tildato = $valg['tildato'] ?? null;
            if ($element == '{fra}') {
                $element = $fradato ? $fradato->format('Y-m-d') : null;
            } else if ($element == '{til}') {
                $element = $tildato ? $tildato->format('Y-m-d') : null;
            } else if (is_string($element)) {
                $element = str_replace(
                    ['{fra}', '{til}'],
                    [
                        $fradato ? $fradato->format('Y-m-d') : null,
                        $tildato ? $tildato->format('Y-m-d') : null
                    ],
                    $element
                );
            }
        });
        return $filter;
    }

    /**
     * @param string $datoformat
     * @param string|null $verdi
     * @return string|null
     */
    public static function formaterDato(string $datoformat, ?string $verdi): ?string
    {
        if ($dato = date_create_from_format('Y-m-d', $verdi)) {
            return $dato->format($datoformat);
        }
        if ($dato = date_create_from_format('Y-m-d H:i:s', $verdi)) {
            return $dato->format($datoformat . ' H:i:s');
        }
        return $verdi;
    }

    /**
     * @param array $valg
     * @return array
     * @throws Exception
     */
    public function eksporter(array $valg): array
    {
        $eksportOppsett = $this->hentOppsett();
        $eksportOppsett->filter = $this->settDatofilter($valg, isset($eksportOppsett->filter) ? (array)$eksportOppsett->filter : []);

        $kilde = $eksportOppsett->kilde ?? null;
        if (is_a($kilde, CoreModelInterface::class, true)) {
            return $this->hentDataFraSamling($valg, $eksportOppsett);
        } else {
            return $this->hentDataFraSpørring($valg, $eksportOppsett);
        }
    }
    /**
     * Dersom kilden oppgies som en modell,
     * utføres spørringen via en Modell-samling
     *
     * @param array $valg
     * @param stdClass $eksportOppsett
     * @return array[]
     * @throws Exception
     */
    public function hentDataFraSamling(array $valg, stdClass $eksportOppsett): array
    {
        list('valg' => $valg, 'eksportOppsett' => $eksportOppsett)
            = $this->app->before($this, __FUNCTION__, [
            'valg' => $valg, 'eksportOppsett' => $eksportOppsett
        ]);
        $eksportOppsett = $eksportOppsett ?? $this->hentOppsett();

        /**
         * For å muliggjøre flere eksportlinjer per modell,
         * for eksempel for ulike konteringsfelter,
         * kan feltene oppgis over flere linjer
         * @var stdClass[] $feltLinjer
         */
        $feltLinjer = ($eksportOppsett->felter ?? []);
        /** @var array $filter */
        if (!is_array($feltLinjer)) {
            $feltLinjer = [$feltLinjer];
        }

        $filter = $eksportOppsett->filter ?? [];
        /** @var array $sortering */
        $sortering = isset($eksportOppsett->sortering) ? (array)$eksportOppsett->sortering : [];

        $overskrifter =  [];
        foreach ($feltLinjer as $felter) {
            /** @var string[] $overskrifter */
            $overskrifter = array_merge($overskrifter, array_combine(array_keys((array)$felter), array_keys((array)$felter)));
        }

        $resultat = [$overskrifter];
        /** @var Samling $samling */
        $samling = $this->app->hentSamling($eksportOppsett->kilde);
        $joins = isset($eksportOppsett->joins) ? (array)$eksportOppsett->joins : [];
        $samling->setJoins($joins);
        $samling->setFilters($filter, true)->låsFiltre();

        foreach ($sortering as $sorteringsfelt) {
            if(is_array($sorteringsfelt) && $sorteringsfelt) {
                $samling->leggTilSortering(
                    $sorteringsfelt[0],
                    $sorteringsfelt[1] ?? false,
                    $sorteringsfelt[2] ?? null
                );
            }
        }
        /** @var Modell $objekt */
        foreach ($samling as $objekt) {
            foreach ($feltLinjer as $felter) {
                $linje = [];
                foreach ($overskrifter as $overskrift) {
                    $mapping = $felter->$overskrift ?? null;
                    if (!isset($mapping)) {
                        $linje[$overskrift]
                            = static::formaterDato($valg['datoformat']
                            ?? 'Y-m-d', $objekt->getStringValue($overskrift));
                    }
                    if(is_callable($mapping)) {
                        $linje[$overskrift] = $mapping($objekt, $this->app, $valg['datoformat']);
                    }
                    else {
                        $linje[$overskrift]
                            = static::formaterDato($valg['datoformat']
                            ?? 'Y-m-d', $objekt->getStringValue($mapping));
                    }
                }
                $resultat[] = $linje;
            }
        }
        return $this->app->after($this, __FUNCTION__, $resultat);
    }

    /**
     * Dersom kilden oppgies som en tabell,
     * utføres spørringen som SQL-spørring
     *
     * @param array $valg
     * @param stdClass $eksportOppsett
     * @return array[]
     * @throws Exception
     */
    public function hentDataFraSpørring(array $valg, stdClass $eksportOppsett): array
    {
        list('eksportOppsett' => $eksportOppsett)
            = $this->app->before($this, __FUNCTION__, [
            'eksportOppsett' => $eksportOppsett
        ]);
        $eksportOppsett = $eksportOppsett ?? $this->hentOppsett();

        $overskrifter = array_keys((array)($eksportOppsett->felter ?? []));
        $resultat = [$overskrifter];
        $datasett = $this->app->mysqli->select([
            'source' => $eksportOppsett->kilde,
            'where' => $eksportOppsett->filter,
            'fields' => $eksportOppsett->felter,
            'orderfields' => $eksportOppsett->sortering ?? null,
            'groupfields' => $eksportOppsett->groupfields ?? null,
            'distinct' => $eksportOppsett->distinct ?? false,
            'having' => $eksportOppsett->having ?? [],
            'sql' => $eksportOppsett->sql ?? null,
        ]);
        foreach($datasett->data as $objekt) {
            $linje = [];
            foreach ($objekt as $overskrift => $verdi) {
                $linje[$overskrift] = static::formaterDato($valg['datoformat'] ?? 'Y-m-d', $verdi);
            }
            $resultat[] = $linje;
        }
        return $this->app->after($this, __FUNCTION__, $resultat);
    }
}