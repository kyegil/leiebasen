<?php

namespace Kyegil\Leiebasen\Modell\Eav;


use Exception;
use Kyegil\CoreModel\Interfaces\EavConfigInterface;
use Kyegil\CoreModel\Interfaces\EavValuesInterface;
use Kyegil\Leiebasen\Modell;
use stdClass;

/**
 * Class Verdi
 * @package Kyegil\Leiebasen\Modell\Eav
 *
 * @property integer $id
 * @property Egenskap $egenskap
 * @property string $modell
 * @property string $kode
 * @property string $verdi
 *
 * @method integer hentId()
 * @method Egenskap hentEgenskap()
 * @method string hentModell()
 * @method string hentKode()
 * @method Modell hentObjekt()
 * @method string hentVerdi()
 *
 * @method $this settEgenskap(Egenskap $egenskap)
 * @method $this settVerdi(string $verdi)
 */
class Verdi extends Modell implements EavValuesInterface
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'eav_verdier';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Verdisett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    public static function harFlere(
        string $referanse,
        ?string $modell = null,
        ?string $fremmedNøkkel = null,
        array $filtre = [],
        ?callable $callback = null
    ): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
        }
        return parent::harFlere($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }


    /**
         * Hent Db-felter
         *
         * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
         * og et array med konfigurasjoner for hvert felt som verdi
         *
         * Mulige konfigurasjoner:
         *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
         *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
         *      eller en callable,
         *      som former databaseverdien i riktig format
         *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
         *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
         *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
         *
     * @return array|\string[][]
     */
    protected static function hentDbFelter()
    {
        return [
            'id' => [
                'type' => 'integer'
            ],
            'egenskap_id' => [
                'type' => Egenskap::class,
                'target' => 'egenskap'
            ],
            'modell' => [
                'type' => 'string',
            ],
            'kode' => [
                'type' => 'string',
            ],
            'objekt_id' => [
                'type' => function($objektId, $thisObjekt){
                    $modell = $thisObjekt->hent('egenskap')->hent('modell');
                    if (!is_a($modell, Modell::class, true)) {
                        throw new Exception("{$modell} er ikke et 'Modell'-objekt");
                    }
                    return $thisObjekt->app->hentModell($modell, $objektId);
                },
                'target' => 'objekt'
            ],
            'verdi' => [
                'type' => 'string'
            ]
        ];
    }

    /**
     * @inheritDoc
     */
    public function getConfig(): EavConfigInterface
    {
        return $this->hent('egenskap');
    }

    /**
     * @param stdClass $parametere
     * @return Modell
     * @throws Exception
     */
    public function opprett(stdClass $parametere): Modell
    {
        if($parametere->egenskap instanceof Egenskap) {
            $parametere->modell = $parametere->egenskap->modell;
            $parametere->kode = $parametere->egenskap->kode;
        }
        elseif ($parametere->egenskap) {
            $egenskap = $this->app->hentModell(Egenskap::class, $parametere->egenskap);
            $parametere->modell = $egenskap->modell;
            $parametere->kode = $egenskap->kode;
        }
        return parent::opprett($parametere);
    }

    /**
     * @param $egenskap
     * @param $verdi
     * @return Modell
     * @throws \Kyegil\CoreModel\CoreModelException
     */
    public function sett($egenskap, $verdi): Modell
    {
        if(!in_array($egenskap, ['modell', 'kode'])) {
            parent::sett($egenskap, $verdi);
        }
        if($egenskap == 'egenskap') {
            if($verdi instanceof Egenskap) {
                $eavEgenskap = $verdi;
            }
            else {
                $eavEgenskap = $this->app->hentModell(Egenskap::class, $verdi);
            }
            parent::saveToMainTable(['modell', $eavEgenskap->modell, 'kode' => $eavEgenskap->kode]);
        }
        return $this;
    }
}