<?php

namespace Kyegil\Leiebasen\Modell\Eav;


/**
 * Class VerdiVarcharObjekt
 * @package Kyegil\Leiebasen\Modell\Eav
 */
class VerdiVarcharObjekt extends Verdi
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'eav_verdier_vc_id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = VerdiVarcharObjektsett::class;


    /** @var array one-to-many */
    protected static ?array $one2Many;
}