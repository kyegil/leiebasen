<?php

namespace Kyegil\Leiebasen\Modell\Eav;

/**
 * Class EgenskapVarcharObjekt
 *
 * @package Kyegil\Leiebasen\Modell\Eav
 *
 * @method EgenskapVarcharObjekt current()
 */
class EgenskapVarcharObjektsett extends Egenskapsett
{
    /**
     * @var string
     */
    protected string $model = EgenskapVarcharObjekt::class;

    /**
     * @var EgenskapVarcharObjekt[]
     */
    protected ?array $items = null;

}