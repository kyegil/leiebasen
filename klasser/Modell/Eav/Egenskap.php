<?php

namespace Kyegil\Leiebasen\Modell\Eav;


use Exception;
use Kyegil\CoreModel\Interfaces\EavConfigInterface;
use Kyegil\CoreModel\Interfaces\EavValuesInterface;
use Kyegil\Leiebasen\EavVerdiFeltTypeRenderer;
use Kyegil\Leiebasen\EavVerdiVisningRenderer;
use Kyegil\Leiebasen\Modell;
use stdClass;

/**
 * Class Egenskap
 * @package Kyegil\Leiebasen\Modell\Eav
 *
 * @property integer $id
 * @property string $modell
 * @property string $kode
 * @property bool $erEgendefinert
 * @property string $type
 * @property stdClass|null $konfigurering
 * @property string $beskrivelse
 * @property integer $rekkefølge
 * @property stdClass|null $feltType
 * @property stdClass|null $visning
 *
 * @method integer hentId()
 * @method string hentModell()
 * @method string hentKode()
 * @method bool hentErEgendefinert()
 * @method string hentType()
 * @method stdClass|null hentKonfigurering()
 * @method string hentBeskrivelse()
 * @method integer hentRekkefølge()
 * @method stdClass|null hentFeltType()
 * @method stdClass|null hentVisning()
 *
 * @method $this settModell(string $modell)
 * @method $this settKode(string $kode)
 * @method $this settErEgendefinert(bool $erEgendefinert)
 * @method $this settType(string $type)
 * @method $this settKonfigurering(stdClass $konfigurering)
 * @method $this settBeskrivelse(string $beskrivelse)
 * @method $this settRekkefølge(integer $rekkefølge)
 * @method $this settFeltType(stdClass|null $feltType)
 * @method $this settVisning(stdClass|null $visning)
 */
class Egenskap extends Modell implements EavConfigInterface
{
    /** @var string Databasetabellen som holder primærnøkkelen for modellen */
    protected static $dbTable = 'eav_egenskaper';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'id';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Egenskapsett::class;

    /** @var array one-to-many */
    protected static ?array $one2Many;

    public static array $renderProsessorMapping = [
        'visning' => [
            'html'          => EavVerdiVisningRenderer\Html::class,
            'ren-tekst'     => EavVerdiVisningRenderer\RenTekst::class,
            'hake'          => EavVerdiVisningRenderer\Hake::class,
            'heltall'       => EavVerdiVisningRenderer\Heltall::class,
            'desimal'       => EavVerdiVisningRenderer\Desimal::class,
            'beløp'         => EavVerdiVisningRenderer\Beløp::class,
            'dato'          => EavVerdiVisningRenderer\Dato::class,
            'klokkeslett'   => EavVerdiVisningRenderer\Klokkeslett::class,
            'tidsstempel'   => EavVerdiVisningRenderer\Tidsstempel::class,
            'intervall'     => EavVerdiVisningRenderer\Intervall::class,
        ],
        'felt_type' => [
            'textfield'          => EavVerdiFeltTypeRenderer\TextField::class,
            'checkbox'           => EavVerdiFeltTypeRenderer\CheckboxField::class,
        ],
    ];

    public static function harFlere(
        string $referanse,
        ?string $modell = null,
        ?string $fremmedNøkkel = null,
        array $filtre = [],
        ?callable $callback = null
    ): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::harFlere('verdier',
                Verdi::class,
                'egenskap_id'
            );
        }
        return parent::harFlere($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }

    /**
     * Mellomlager for Eav-konfigurasjoner for hver enkelt modell
     *
     * @var array|null
     */
    public static ?array $mellomlager = [];

    /**
         * Hent Db-felter
         *
         * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
         * og et array med konfigurasjoner for hvert felt som verdi
         *
         * Mulige konfigurasjoner:
         *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
         *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
         *      eller en callable,
         *      som former databaseverdien i riktig format
         *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
         *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
         *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
         *
     * @return array|\string[][]
     */
    protected static function hentDbFelter()
    {
        return [
            'id' => [
                'type' => 'integer'
            ],
            'modell' => [
                'type' => 'string'
            ],
            'kode' => [
                'type' => 'string'
            ],
            'er_egendefinert' => [
                'type' => 'boolean'
            ],
            'type' => [
                'type' => 'string'
            ],
            'konfigurering' => [
                'type' => 'json',
                'allowNull' => true,
            ],
            'beskrivelse' => [
                'type' => 'string'
            ],
            'rekkefølge' => [
                'type' => 'integer',
                'allowNull' => true,
            ],
        ];
    }


    /**
     * @return array
     */
    public function getConfiguration(): array
    {
        $konfigurering = $this->hentKonfigurering();
        return $konfigurering ? (array)$konfigurering : [];
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getTarget(): string
    {
        $konfigurering = $this->hentKonfigurering();
        if($konfigurering && isset($konfigurering->target)) {
            return $konfigurering->target;
        }
        return $this->hent('kode');
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getSourceField(): string
    {
        return $this->hent('kode');
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getDataType()
    {
        $type = $this->hent('type');
        switch($type) {
            case 'float':
                $dataType = 'string';
                break;
            default:
                $dataType = $type;
                break;
        }
        return $dataType;
    }

    /**
     * @return Verdisett
     * @throws Exception
     */
    public function getValuesCollection(): Verdisett
    {
        /** @var Verdisett $samling */
        $samling = $this->hentSamling('verdier');
        return $samling;
    }

    /**
     * @return string
     */
    public function getEavCode(): string
    {
        return 'kode';
    }

    /**
     * @return string
     */
    public function getEavValueField(): string
    {
        return 'verdi';
    }

    /**
     * @return class-string<Verdi>
     */
    public function getValuesModelName(): string
    {
        return Verdi::class;
    }

    /**
     * @return string
     */
    public function getValuesJoinField(): string
    {
        return 'objekt';
    }

    /**
     * @return string
     */
    public function getConfigJoinField(): string
    {
        return 'egenskap';
    }

    /**
     * @return bool
     */
    public function getAllowNull(): bool
    {
        return true;
    }

    /**
     * @return bool
     */
    public function getAllowMultiple(): bool
    {
        $konfigurering = $this->hentKonfigurering();
        return !empty($konfigurering->allowMultiple);
    }

    /**
     * @param $value
     * @return EavConfigInterface
     */
    public function validateValue($value): EavConfigInterface
    {
        return $this;
    }

    /**
     * Henter alle Eav-konfigurasjoner for egenskapen selv
     *
     * Konfigurasjonene returneres i et flerdimensjonalt objekt
     * i formen:
     * kilde->kode = \Kyegil\CoreModel\Interfaces\EavValuesInterface
     *
     * @return stdClass
     * @throws Exception
     */
    public function hentEavKonfigurering(): object
    {
        if(!isset($this->eavConfigs)) {
            if(!isset(Egenskap::$mellomlager[static::class])) {
                /** @var Egenskapsett $egenskaper */
                $egenskaper = $this->app->hentSamling(Egenskap::class)
                    ->leggTilFilter(['modell' => static::class])
                    ->låsFiltre()
                    ->leggTilSortering('rekkefølge');
                $kilde = 'egne_eav_egenskaper';
                Egenskap::$mellomlager[static::class] = (object)[$kilde => (object)[]];
                /** @var Egenskap $egenskap */
                foreach ($egenskaper as $egenskap) {
                    $kode = $egenskap->hent('kode');
                    Egenskap::$mellomlager[static::class]->{$kilde}->{$kode} = $egenskap;
                }
            }
            $this->eavConfigs = Egenskap::$mellomlager[static::class];
        }
        return parent::hentEavKonfigurering();
    }

    /**
     * Returnerer et objekt med ett Verdi-objekt tilhørende hver kode,
     * for den verdi-kilden eller tabellen som er oppgitt,
     * eller null dersom denne verdikilden ikke finnes
     *
     * @param string $kilde (f.eks tabell)
     * @return stdClass|null
     * @throws Exception
     */
    public function hentEavVerdiObjekter(string $kilde): ?stdClass
    {
        if(!$this->eavValueObjects) {
        $verdiKilde = 'egne_eav_egenskaper';
            $this->eavValueObjects = (object)[
                $verdiKilde => new stdClass()
            ];

            /** @var Verdisett $eavVerdiObjekter */
            $eavVerdiObjekter = $this->app->hentSamling(Verdi::class)

                ->leggTilInnerJoin(Egenskap::hentTabell(), '`' . Verdi::hentTabell() . '`.`egenskap_id` = `' . Egenskap::hentTabell() . '`.`id`')
                ->leggTilFilter([
                    '`' . Egenskap::hentTabell() . '`.`modell`' => static::class,
                    '`' . Verdi::hentTabell() . '`.`objekt_id`' => $this->getId()
                ]);
            /** @var EavValuesInterface $eavVerdiObjekt */
            foreach ($eavVerdiObjekter as $eavVerdiObjekt) {
                $kode = $eavVerdiObjekt->getConfig()->getSourceField();
                $this->eavValueObjects->{$verdiKilde}->{$kode} = $eavVerdiObjekt;
            }
        }

        if(isset($this->eavValueObjects->{$kilde})) {
            return $this->eavValueObjects->{$kilde};
        }
        return null;
    }

    /**
     * @return Verdisett
     * @throws Exception
     */
    public function hentVerdier(): Verdisett
    {
        /** @var Verdisett $verdier */
        $verdier = $this->hentSamling('verdier');

        if(!isset($this->samlinger->verdier)) {
            $this->samlinger->verdier = $verdier->hentElementer();
        }
        $verdier->lastFra($this->samlinger->verdier);
        /** Klon samlingen for å unnngå problem med nested loops */
        return clone $verdier;
    }

    /**
     * @return void
     * @throws Exception
     */
    public function slett()
    {
        $this->hentVerdier()->slettHver();
        parent::slett();
    }

    /**
     * @param Verdi|null $verdi
     * @param string $bruk
     * @return mixed|null
     * @throws Exception
     */
    public function visVerdi(?Verdi $verdi, string $bruk = 'visning') {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $visning = $this->hent($bruk);
        if(!$visning || !isset($visning->type) || !$visning->type) {
            return null;
        }
        /** @var string|null $visningType */
        $visningType = $visning->type;
        /** @var stdClass|null $visningOppsett */
        $visningOppsett = $visning->oppsett ?? null;
        /** @var class-string<EavVerdiVisningRenderer> $renderKlasse */
        $renderKlasse = self::$renderProsessorMapping[$bruk][$visningType] ?? null;
        $renderKlasse = $renderKlasse ?? self::$renderProsessorMapping[$bruk]['html'] ?? null;
        $renderKlasse = $renderKlasse ?? self::$renderProsessorMapping['visning']['html'] ?? null;
        $resultat = null;
        if(is_a($renderKlasse, EavVerdiVisningRenderer::class, true)) {
            /** @var string|null $resultat */
            $resultat = $renderKlasse::vis($verdi, $visningOppsett, $this, $visningType, $bruk);
        }
        return $this->app->post($this, __FUNCTION__, $resultat, $args);
    }
}