<?php

namespace Kyegil\Leiebasen\Modell\Eav;


/**
 * Class Verdisett for modeller med varchar id-felt
 *
 * @package Kyegil\Leiebasen\Modell\Eav
 *
 * @method VerdiVarcharObjekt current()
 */
class VerdiVarcharObjektsett extends Verdisett
{
    /**
     * @var string
     */
    protected string $model = VerdiVarcharObjekt::class;

    /**
     * @var VerdiVarcharObjekt[]
     */
    protected ?array $items = null;
}