<?php

namespace Kyegil\Leiebasen\Modell\Eav;


use Kyegil\Leiebasen\Sett;

/**
 * Class Egenskapsett
 *
 * @package Kyegil\Leiebasen\Modell\Eav
 *
 * @method Egenskap current()
 */
class Egenskapsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Egenskap::class;

    /**
     * @var Egenskap[]
     */
    protected ?array $items = null;

}