<?php

namespace Kyegil\Leiebasen\Modell\Eav;


use Kyegil\Leiebasen\Sett;

/**
 * Class Verdisett
 *
 * @package Kyegil\Leiebasen\Modell\Eav
 *
 * @method Verdi current()
 */
class Verdisett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Verdi::class;

    /**
     * @var Verdi[]
     */
    protected ?array $items = null;
}