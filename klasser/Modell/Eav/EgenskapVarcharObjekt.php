<?php

namespace Kyegil\Leiebasen\Modell\Eav;


/**
 *
 */
class EgenskapVarcharObjekt extends Egenskap
{

    /** @var array one-to-many */
    protected static ?array $one2Many;

    public static function harFlere(
        string $referanse,
        ?string $modell = null,
        ?string $fremmedNøkkel = null,
        array $filtre = [],
        ?callable $callback = null
    ): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::harFlere('verdier',
                VerdiVarcharObjekt::class,
                'egenskap_id'
            );
        }
        return parent::harFlere($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }

    /**
     * @return string
     */
    public function getValuesModelName(): string
    {
        return VerdiVarcharObjekt::class;
    }


}