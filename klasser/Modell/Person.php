<?php

namespace Kyegil\Leiebasen\Modell;


use DateTime;
use DateTimeInterface;
use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietakersett;
use Kyegil\Leiebasen\Modell\Leieforhold\Oppsigelse;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Modell\Person\Adgangsett;
use Kyegil\Leiebasen\Modell\Person\Brukerprofilsett;
use Kyegil\Leiebasen\Modell\Person\EfakturaIdentifier;
use Kyegil\Leiebasen\Sett;
use Kyegil\Nets\B2cOnlineAgreement\EfakturaApiClient\EfakturaIdentifierResponse;
use stdClass;

/**
 * Class Person
 * @package Kyegil\Leiebasen\Modell
 *
 * @property integer $id
 * @property integer $personid
 * @property string $fornavn
 * @property string $etternavn
 * @property boolean $erOrg
 * @property DateTime|null $fødselsdato
 * @property string $personnr
 * @property string $orgNr
 * @property string $adresse1
 * @property string $adresse2
 * @property string $postnr
 * @property string $poststed
 * @property string $land
 * @property string $telefon
 * @property string $mobil
 * @property string $epost
 * @property stdClass $brukerprofilPreferanser
 *
 * @method int hentId()
 * @method int hentPersonid()
 * @method string hentFornavn()
 * @method string hentEtternavn()
 * @method boolean hentErOrg()
 * @method DateTime|null hentFødselsdato()
 * @method string hentPersonnr()
 * @method string hentOrgNr()
 * @method string hentAdresse1()
 * @method string hentAdresse2()
 * @method string hentPostnr()
 * @method string hentPoststed()
 * @method string hentLand()
 * @method string hentTelefon()
 * @method string hentMobil()
 * @method string hentEpost()
 * @method stdClass hentBrukerprofilPreferanser()
 *
 * @method $this settPersonid(int $id)
 * @method $this settFornavn(string $fornavn)
 * @method $this settEtternavn(string $etternavn)
 * @method $this settErOrg(boolean $erOrg)
 * @method $this settOrgNr(string $orgNr)
 * @method $this settAdresse1(string $adresse1)
 * @method $this settAdresse2(string $adresse2)
 * @method $this settPostnr(string $postnr)
 * @method $this settPoststed(string $poststed)
 * @method $this settLand(string $land)
 * @method $this settTelefon(string $telefon)
 * @method $this settMobil(string $mobil)
 * @method $this settBrukerprofilPreferanser(stdClass $brukerprofilPreferanser)
 */
class Person extends Modell
{
    /**
     * @var string Databasetabellen som holder primærnøkkelen for modellen
     */
    protected static $dbTable = 'personer';

    /** @var string Primærnøkkelen for modellen i databasen */
    protected static $dbPrimaryKeyField = 'personid';

    /** @var string Samlingsmodellen for denne modellen */
    protected static $collectionModel = Personsett::class;

    /**
     * Array med datoer, hvor hver dato inneholder alle leieforholdene personen har inngått i.
     * @var stdClass[]
     */
    protected	$leieforhold;

    /** @var array|null one-to-many */
    protected static ?array $one2Many;

    public static function harFlere(
        string $referanse,
        ?string $modell = null,
        ?string $fremmedNøkkel = null,
        array $filtre = [],
        ?callable $callback = null
    ): ?array
    {
        if(!isset(self::$one2Many)) {
            self::$one2Many = [];
            self::harFlere('kontrakt_linker',
                Leietaker::class,
                'person',[],
                function(Leietakersett $leietakere){
                    $leietakere->leggTilKontraktModell();
                }
            );
            self::harFlere('adganger',
                Adgang::class,
                'personid',
                [],
                function(Adgangsett $adganger) {
                    $adganger->leggTilSortering('adgang')->leggTilSortering('leieforhold', true);
                }
            );
            self::harFlere('efaktura_identifiers',
                EfakturaIdentifier::class,
                'person_id'
            );
        }
        return parent::harFlere($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }

    /**
     * Hent Db-felter
     *
     * Returnerer et array med databasefeltene i modellens tabell som nøkkel,
     * og et array med konfigurasjoner for hvert felt som verdi
     *
     * Mulige konfigurasjoner:
     *  type: Datatypen. Standardverdi er 'string'. 'string'|'int'|'boolean'|'date'|'time'|'datetime'|'interval'|'json',
     *      en klasse av typen \Kyegil\CoreModel\Interfaces\CoreModelInterface,
     *      eller en callable,
     *      som former databaseverdien i riktig format
     *  allowNull: boolsk. Standardverdi er true. Sett til false for å forby null-verdier
     *  target: string. Dersom angitt, vil denne overstyre tabellfelt-navnet.
     *  toDbValue: callable. Denne brukes brukes for å konvertere verdien til en streng-verdi som kan lagres i databasen.
     *  rawDataContainer. rawData-attributten som holder tabell-data-formatet av denne verdien    *
     *
     * @return array
     */
    public static function hentDbFelter(): array
    {
        return [
            'personid'    => [
                'type'  => 'integer'
            ],
            'fornavn'    => [
                'type'  => 'string'
            ],
            'etternavn'    => [
                'type'  => 'string'
            ],
            'er_org'    => [
                'type'  => 'boolean'
            ],
            'fødselsdato'    => [
                'type'  => 'date',
                'allowNull' => true
            ],
            'personnr'    => [
                'type'  => 'string'
            ],
            'adresse1'    => [
                'type'  => 'string'
            ],
            'adresse2'    => [
                'type'  => 'string'
            ],
            'postnr'    => [
                'type'  => 'string'
            ],
            'poststed'    => [
                'type'  => 'string'
            ],
            'land'    => [
                'type'  => 'string'
            ],
            'telefon'    => [
                'type'  => 'string'
            ],
            'mobil'    => [
                'type'  => 'string'
            ],
            'epost'    => [
                'type'  => 'string'
            ]
        ];
    }

    /**
     * @param string $egenskap
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function sett($egenskap, $verdi): Modell
    {
        if($egenskap == 'epost') {
            return $this->settEpost($verdi);
        }
        if($egenskap == 'org_nr') {
            return parent::sett('personnr', $verdi);
        }
        return parent::sett($egenskap, $verdi);
    }

    /**
     * @return string
     */
    public function hentNavn(): string
    {
        if(!isset($this->data->navn)) {
            if($this->hentErOrg()) {
                $this->data->navn = $this->etternavn;
            }
            else{
                $this->data->navn = trim(implode(' ', [$this->fornavn, $this->etternavn]));
            }
        }
        return $this->data->navn;
    }

    /**
     * @return string
     */
    public function hentPostadresse(): string
    {
        if(!isset($this->data->postadresse)) {
            $this->data->postadresse
                = ( $this->hentAdresse1() ? "{$this->hentAdresse1()}\n" : "")
                . ( $this->hentAdresse2() ? "{$this->hentAdresse2()}\n" : "")
                . "{$this->hentPostnr()} {$this->hentPoststed()}"
                . ( ($this->hentLand() && $this->hentLand() != "Norge") ? "\n{$this->hentLand()}" : "");
        }
        return $this->data->postadresse;
    }

    /**
     * @return string
     */
    public function hentFødselsnummer(): string
    {
        if(!isset($this->data->fødselsnummer)) {
            $this->data->fødselsnummer = '';
            if($this->hentFødselsdato() && $this->hentPersonnr()) {
                $this->data->fødselsnummer = $this->hentFødselsdato()->format('dmy') . $this->hentPersonnr();
            }
        }
        return $this->data->fødselsnummer;
    }

    /**
     * @return Leieforhold|null
     * @throws Exception
     */
    public function hentHovedLeieforhold(): ?Leieforhold
    {
        if(!property_exists($this->data, 'hovedLeieforhold')) {
            /** @var Sett $adganger */
            $adganger = $this->hentAdganger()
                ->leggTilFilter(['adgang' => Adgang::ADGANG_MINESIDER]);
            if(!$adganger->hentAntall()) {
                return $this->data->hovedLeieforhold = null;
            }
            $this->data->hovedLeieforhold = $adganger->hentFørste()->leieforhold;
            /** @var Adgang $adgang */
            foreach($adganger as $adgang) {
                if(
                    $adgang->leieforhold && $adgang->leieforhold->hentId()
                    && $adgang->leieforhold->hentId() == $this->hentBrukerProfilPreferanse('standard_leieforhold')
                ) {
                    $this->data->hovedLeieforhold = $adgang->leieforhold;
                    break;
                }
            }
        }
        return $this->data->hovedLeieforhold;
    }

    /**
     * @param Leieforhold|string $leieforhold
     * @return $this
     * @throws Exception
     */
    public function settHovedLeieforhold($leieforhold): Person
    {
        return $this->settBrukerprofilPreferanse('standard_leieforhold', strval($leieforhold));
    }

    /**
     * Last personens leieforhold fra databasen
     *
     * @return Person
     * @throws Exception
     */
    protected function lastLeieforhold(): Person
    {
        $this->leieforhold = [];

        $kontraktLinker = $this->hentKontraktLinker();
        $kontraktLinker->leggTilKontraktModell(['dato'])
            ->leggTilKontraktLeieforholdModell();

        $kontraktLinker->leggTilLeftJoin(
            Oppsigelse::hentTabell(),
            '`kontrakt_leieforhold`.`' . Leieforhold::hentPrimærnøkkelfelt() .'` = `'
            . Oppsigelse::hentTabell() . '`.`' . Oppsigelse::hentPrimærnøkkelfelt() . '`'
        );
        $kontraktLinker->leggTilModell(
            Oppsigelse::class,
            'kontrakter.leieforhold.oppsigelser',
            null,
            ['fristillelsesdato']
        );

        $kontraktLinker->leggTilFilter(['`kontrakt_leieforhold`.`' . Leieforhold::hentPrimærnøkkelfelt() . '` IS NOT NULL']);

        /** @var Leietaker $kontraktLink */
        foreach ($kontraktLinker as $kontraktLink) {
            $kontrakt = $kontraktLink->hentKontrakt();
            $leieforhold = $kontrakt->hentLeieforhold();
            $oppsigelse = $leieforhold->hentOppsigelse();

            $this->leieforhold[] = (object)[
                'fradato' => $kontrakt->hentDato(),
                'tildato' => $kontraktLink->hentSlettet() ?? ($oppsigelse ? $oppsigelse->hentFristillelsesdato() : null),
                'leieforhold' => $leieforhold
            ];
        }

        return $this;
    }

    /**
     * Sjekker om denne personen eller organisasjonen er eller var leietaker hos utleier på en gitt dato
     *
     * @param DateTimeInterface|null $dato
     * @return Leieforholdsett
     * @throws Exception
     */
    public function hentLeieforhold(DateTimeInterface $dato = null ): Leieforholdsett
    {
        $leieforholdArray = [];
        /** @var Leieforholdsett $resultat */
        $resultat = $this->app->hentSamling(Leieforhold::class);
        /** @var int[] $leieforholdIder */
        $leieforholdIder = [];

        if ( !isset( $this->leieforhold ) ) {
            $this->lastLeieforhold();
        }
        foreach( $this->leieforhold as $forhold ) {
            if(
                $dato === null
                || (
                    $forhold->fradato <= $dato
                    && (
                        $forhold->tildato === null
                        || $forhold->tildato >= $dato
                    )
                )
            ) {
                $leieforholdArray[] = $forhold->leieforhold;
                $leieforholdIder[] = $forhold->leieforhold->hentId();
            }
        }

        $resultat->filterByItemIds($leieforholdIder)->lastFra($leieforholdArray);
        return $resultat;
    }

    /**
     * @return Leieobjekt|null
     * @throws Exception
     */
    public function hentHovedLeieobjekt(): ?Leieobjekt
    {
        if(!isset($this->data->hovedLeieobjekt)) {
            $hovedLeieforhold = $this->hentHovedLeieforhold();
            $this->data->hovedLeieobjekt = $hovedLeieforhold ? $hovedLeieforhold->hentLeieobjekt() : null;
        }
        return $this->data->hovedLeieobjekt;
    }

    /**
     * @return Leietakersett
     * @throws Exception
     */
    public function hentKontraktLinker(): Leietakersett
    {
        /** @var Leietakersett $kontraktLinker */
        $kontraktLinker = $this->hentSamling('kontrakt_linker');
        return $kontraktLinker;
    }

    /**
     * @return Adgangsett
     * @throws Exception
     */
    public function hentAdganger(): Adgangsett
    {
        /** @var Adgangsett $adganger */
        $adganger = $this->hentSamling('adganger');
        return $adganger;
    }

    /**
     * @return Brukerprofilsett
     * @throws Exception
     */
    public function hentBrukerprofiler(): Brukerprofilsett
    {
        /** @var Brukerprofilsett $brukerprofiler */
        $brukerprofiler = $this->app->hentSamling(Modell\Person\Brukerprofil::class)
            ->leggTilFilter([
                'person_id' => $this->hentId()
            ])
            ->låsFiltre();
        if(!isset($this->samlinger->brukerprofiler)) {
            $this->samlinger->brukerprofiler = $brukerprofiler->hentElementer();
        }
        $brukerprofiler->lastFra($this->samlinger->brukerprofiler);

        return clone $brukerprofiler;
    }

    /**
     * @return Leietakersett
     * @throws Exception
     */
    public function hentLeieforholdDeltakelse(): Leietakersett
    {
        /** @var Leietakersett $leietakere */
        $leietakere = $this->app->hentSamling(Leietaker::class);
        $leietakere
            ->leggTilPersonModell()
            ->leggTilLeieforholdModell()
            ->leggTilKontraktModell()
            ->leggTilFilter([
                'person' => $this->hentId()
            ])
            ->leggTilFilter([
                '`' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '` IS NOT NULL'
            ])
            ->leggTilSortering('slettet', true)
            ->leggTilSortering('leieforhold', false, 'kontrakter')
            ->låsFiltre();
        if(!isset($this->samlinger->leietakere)) {
            $this->samlinger->leietakere = $leietakere->hentElementer();
        }
        $leietakere->lastFra($this->samlinger->leietakere);
        return $leietakere;
    }

    /**
     * @param null $egenskap
     * @return mixed|stdClass
     * @throws Exception
     */
    public function hent($egenskap = null)
    {
        if($egenskap == 'brukerprofil_preferanser') {
            return parent::hent($egenskap) ?: new stdClass();
        }
        if($egenskap == 'org_nr') {
            return $this->hentErOrg() ? parent::hent('personnr') : null;
        }
        return parent::hent($egenskap);
    }

    /**
     * @param string $egenskap
     * @return mixed
     * @throws Exception
     */
    public function hentBrukerProfilPreferanse(string $egenskap)
    {
        $preferanser = $this->hent('brukerprofil_preferanser');
        return property_exists($preferanser, $egenskap) ? $preferanser->{$egenskap} : null;
    }

    /**
     * @param string $egenskap
     * @param mixed $verdi
     * @return $this
     * @throws Exception
     */
    public function settBrukerProfilPreferanse(string $egenskap, $verdi): Person
    {
        $preferanser = $this->hentBrukerprofilPreferanser();
        $preferanser->$egenskap = $verdi;
        return $this->settBrukerprofilPreferanser($preferanser);
    }

    /**
     * @param string $område
     * @param Leieforhold|int|null $leieforhold
     * @return Adgang|null
     * @throws Exception
     */
    public function hentAdgangTil(string $område, $leieforhold = null): ?Adgang
    {
        /** @var Adgang $adgang */
        foreach($this->hentAdganger() as $adgang) {
            if($adgang->adgang == $område) {
                if($område != Adgang::ADGANG_MINESIDER
                    || ($leieforhold && strval($leieforhold) == $adgang->leieforhold->hentId())
                ) {
                    return $adgang;
                }
            }
        }
        return null;
    }

    /**
     * @param string $område
     * @param Leieforhold|int|null $leieforhold
     * @return bool
     * @throws Exception
     */
    public function harAdgangTil(string $område, $leieforhold = null): bool
    {
        if($område == Adgang::ADGANG_SENTRAL) {
            return true;
        }
        /** @var Adgang $adgang */
        foreach($this->hentAdganger() as $adgang) {
            if($adgang->adgang == $område) {
                if($område != Adgang::ADGANG_MINESIDER
                    || $leieforhold === null
                    || strval($leieforhold) == $adgang->leieforhold->hentId()
                ) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return Person\Brukerprofil|null
     * @throws Exception
     */
    public function hentBrukerProfil(): ?Person\Brukerprofil
    {
        if(!property_exists($this->data, 'brukerprofil')) {
            /** @var Modell\Person\Brukerprofil|null $brukerprofil */
            $brukerprofil = $this->app->hentSamling(Modell\Person\Brukerprofil::class)
                ->leggTilFilter(['person_id' => $this->hentId()])->hentFørste();
            $this->data->brukerprofil = $brukerprofil;
        }
        return $this->data->brukerprofil;
    }

    /**
     * @param DateTimeInterface|null $fødselsdato
     * @return $this
     * @throws Exception
     */
    public function settFødselsdato(?DateTimeInterface $fødselsdato = null): Person
    {
        $this->validerFødselsdato($fødselsdato);
        return parent::sett('fødselsdato', $fødselsdato);
    }

    /**
     * @param $epost
     * @return $this
     * @throws Exception
     */
    public function settEpost($epost): Person
    {
        $epost = trim($epost);
        $this->validerEpost($epost);
        return parent::sett('epost', $epost);
    }

    /**
     * @param string $personnr
     * @return $this
     * @throws Exception
     */
    public function settPersonnr(string $personnr): Person
    {
        if(!$this->hentErOrg()) {
            $personnr = str_replace(' ', '', trim($personnr));
            $fødselsdato = $this->hentFødselsdato();
            if(strlen($personnr) == 11) {
                $fødselsnummer = substr($personnr, 0, 6);
                $personnr = substr($personnr, 6);
                /** D-nummer har 400000 lagt til fødselsdato */
                if($fødselsnummer > 400000) {
                    $fødselsnummer = str_pad($fødselsnummer - 400000, 6, 0, STR_PAD_LEFT);
                }
                $fødselsdato = date_create_from_format('dmy', $fødselsnummer);
                if($fødselsdato) {
                    $this->settFødselsdato($fødselsdato);
                }
            }
            $this->validerPersonnr($personnr, $fødselsdato);
        }
        return parent::sett('personnr', $personnr);
    }

    /**
     * @param string $epost
     * @return $this
     * @throws Exception
     */
    protected function validerEpost(string $epost): Person
    {
        $this->app->validerEpost($epost);
        return $this;
    }

    /**
     * @param DateTime|null $fødselsdato
     * @return void
     * @throws Exception
     */
    protected static function validerFødselsdato(DateTimeInterface $fødselsdato = null): void
    {
        if($fødselsdato) {
            $iDag = new DateTime();
            $alder = $iDag->diff($fødselsdato);
            if($alder->days > 50000 || $alder->days < 0) {
                throw new Exception('Ugyldig fødselsdato.');
            }
        }
    }

    /**
     * @param string $personnr
     * @param DateTime|null $fødselsdato
     * @param bool $erOrganisasjon
     * @return void
     * @throws Exception
     */
    public static function validerPersonnr(string $personnr, ?DateTime $fødselsdato = null, bool $erOrganisasjon = false): void
    {
        if ($erOrganisasjon || !$personnr) {
            return;
        }
        // Fødselsdato må være angitt sammen med personnummeret
        if(strlen($personnr) == 11) {
            if($fødselsdato) {
                $fødselsnummer = $fødselsdato->format('dmy');
                $dNummer = intval($fødselsnummer) + 400000;
                if(!in_array(substr($personnr, 0, 6), [$fødselsnummer, $dNummer])) {
                    throw new Exception('Personnummeret matcher ikke fødselsdato.');
                }
            }
        }
        else if(strlen($personnr) == 5) {
            if(!$fødselsdato) {
                throw new Exception('Personnummer på 5 siffer må oppgis sammen med fødselsdato.');
            }
        }
        else{
            throw new Exception('Personnummeret må oppgis som 11 siffer, eller som 5 siffer sammen med fødselsdato.');
        }
    }

    /**
     * Sjekker om personen har vært leietaker i et leieforhold, evt på en bestemt dato
     *
     * @param Leieforhold $leieforhold
     * @param DateTime|null $dato
     * @return bool
     * @throws Exception
     */
    public function harVærtLeietakerI(Leieforhold $leieforhold, DateTime $dato = null): bool
    {
        /** @var Leietaker $leietaker */
        foreach ($this->hentLeieforholdDeltakelse() as $leietaker) {
            if ($leietaker->kontrakt->leieforhold->hentId() == $leieforhold->hentId()) {
                if (!$dato) {
                    return true;
                }
                else if ($leietaker->kontrakt->dato > $dato) {
                    continue;
                }
                else if ($leietaker->slettet && $leietaker->slettet <= $dato){
                    continue;
                }
                else {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param Leieforhold $leieforhold
     * @return bool
     * @throws Exception
     */
    public function erLeietakerI(Leieforhold $leieforhold): bool
    {
        return $this->harVærtLeietakerI($leieforhold, new DateTime());
    }

    /**
     * @param string $område
     * @param Leieforhold|null $leieforhold
     * @return Adgang
     * @throws Exception
     */
    public function tildelAdgang(string $område, Leieforhold $leieforhold = null): Adgang
    {
        /** @var Adgang $adgang */
        foreach($this->hentAdganger() as $adgang) {
            if ($adgang->adgang == $område) {
                if ($område == Adgang::ADGANG_MINESIDER) {
                    if (
                        !$leieforhold
                        || !$leieforhold->hentId()
                        || $adgang->leieforhold->hentId() == $leieforhold->hentId()
                    ) {
                        return $adgang;
                    }
                }
                else {
                    return $adgang;
                }
            }
        }
        $adgang = $this->app->nyModell(Adgang::class, (object)[
            'adgang' => $område,
            'leieforhold' => $leieforhold,
            'person' => $this
        ]);
        if ($leieforhold) {
            $leieforhold->nullstill();
        }
        $this->nullstill();
        return $adgang;
    }

    /**
     * @throws Exception
     */
    public function slett()
    {
        foreach($this->hentLeieforholdDeltakelse() as $deltakelse) {
            $leieforhold = $deltakelse->leieforhold;
            $leietakere = $leieforhold->hentLeietakere();
            $leietakere->leggTilFilter(['`' . Leietaker::hentTabell() . '`.`person` IS NOT NULL']);
            if($leietakere->hentAntall() > 1) {
                $leieforhold->settTilfeldigRegningsperson();
            }
            else {
                $leieforhold->settRegningsperson(null);
            }
            $deltakelse->leietakerNavn = $this->hentNavn();
            $deltakelse->person = null;
        }
        $this->hentAdganger()->deleteEach();
        $this->hentBrukerprofiler()->deleteEach();
        if ($this->hentEfakturaAgreement()) {
            $this->hentEfakturaAgreement()->slett();
        }
        parent::slett();
    }

    /**
     * @return Person\EfakturaIdentifier|null
     * @throws Exception
     */
    public function hentEfakturaAgreement(): ?EfakturaIdentifier
    {
        if(!property_exists($this->data, 'efaktura_agreement')) {
            /** @var EfakturaIdentifier|null $efakturaIdentifier */
            $efakturaIdentifier = $this->hentSamling('efaktura_identifiers')
                ->hentFørste();
            $this->data->efaktura_agreement = $efakturaIdentifier;
        }
        return $this->data->efaktura_agreement;
    }

    /**
     * @param EfakturaIdentifierResponse|null $efakturaIdentifierResponse
     * @return void
     * @throws Exception
     */
    public function lagreEfakturaAgreement(EfakturaIdentifierResponse $efakturaIdentifierResponse)
    {
        $rawValues = (object)[
            'person' => $this->hentId(),
            'siste_response_result' => $efakturaIdentifierResponse->getResponseResult(),
            'siste_identifier_key' => $efakturaIdentifierResponse->getEfakturaIdentifierKey(),
        ];
        if($efakturaIdentifierResponse->getEfakturaIdentifier()) {
            $rawValues->efaktura_identifier = $efakturaIdentifierResponse->getEfakturaIdentifier();
        }
        if($efakturaIdentifierResponse->getHasAutoAcceptAgreements() !== null) {
            $rawValues->has_auto_accept_agreements = (int)$efakturaIdentifierResponse->getHasAutoAcceptAgreements();
        }
        if($efakturaIdentifierResponse->getHasEfakturaAgreement() !== null) {
            $rawValues->has_efaktura_agreement = (int)$efakturaIdentifierResponse->getHasEfakturaAgreement();
        }
        if($efakturaIdentifierResponse->getBlockedByReceiver() !== null) {
            $rawValues->blocked_by_receiver = (int)$efakturaIdentifierResponse->getBlockedByReceiver();
        }
        if($this->hentEfakturaAgreement())  {
            unset($rawValues->person);
            $this->hentEfakturaAgreement()->saveRawValues($rawValues);
        }
        else {
            $this->data->efaktura_agreement
            = $this->app->nyModell(EfakturaIdentifier::class, $rawValues);
        }
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function hentEfakturaIdentifier(): ?string
    {
        $efakturaAgreement = $this->hentEfakturaAgreement();
        return $efakturaAgreement
            && $efakturaAgreement->efakturaIdentifier
            && $efakturaAgreement->hasAutoAcceptAgreements
            && !$efakturaAgreement->blockedByReceiver
            ? $efakturaAgreement->efakturaIdentifier : null;
    }

    /**
     * @return Person
     * @throws Exception
     */
    public function tildelAdgangTilEgneLeieforhold(): Person
    {
        $this->app->pre($this, __FUNCTION__, []);
        $egneLeieforhold = $this->hentLeieforhold();
        foreach ($egneLeieforhold as $leieforhold) {
            $this->tildelAdgang(Adgang::ADGANG_MINESIDER, $leieforhold);
        }
        return $this->app->post($this, __FUNCTION__, $this);
    }
}