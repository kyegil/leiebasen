<?php

namespace Kyegil\Leiebasen\Modell;


use Kyegil\Leiebasen\Sett;

/**
 * Class Poengprogramsett
 * @package Kyegil\Leiebasen\Modell
 *
 * @method Poengprogram current()
 */
class Poengprogramsett extends Sett
{
    /**
     * @var string
     */
    protected string $model = Poengprogram::class;

    /**
     * @var Poengprogram[]
     */
    protected ?array $items = null;
}