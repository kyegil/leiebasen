<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 12/06/2020
 * Time: 14:02
 */

namespace Kyegil\Leiebasen;


use Closure;
use Exception;
use Kyegil\CoreModel\CoreModelException;
use Kyegil\CoreModel\Interfaces\CoreModelCollectionInterface;
use Kyegil\CoreModel\Interfaces\CoreModelInterface;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Verdi;
use stdClass;

/**
 * Trait Samlingstrekk
 * @package Kyegil\Leiebasen
 */
trait Samlingstrekk
{
    /**
     * @param string|string[]|array<string,mixed> $filter
     * @return $this
     */
    public abstract function addFilter($filter): CoreModelCollectionInterface;

    /**
     * @param $filter
     * @return $this
     */
    public abstract function addHaving($havingFilter): CoreModelCollectionInterface;

    /**
     * @param array $filters
     * @param $doNotMold
     * @return $this
     */
    public abstract function setFilters(array $filters, $doNotMold = false): CoreModelCollectionInterface;

    /**
     * @param array $filters
     * @param boolean $doNotMold
     * @return $this
     */
    public abstract function setHaving(array $havingFilters, bool $doNotMold): CoreModelCollectionInterface;

    /**
     * @return $this
     */
    public abstract function massDelete(): CoreModelCollectionInterface;

    /**
     * @return $this
     */
    public abstract function lockFilters(): CoreModelCollectionInterface;

    /**
     * @return $this
     */
    public abstract function reset(): CoreModelCollectionInterface;

    /**
     * @param string|array|stdClass $tabellnavn
     * @param string $betingelse
     * @return $this
     */
    public abstract function addInnerJoin($tabellnavn, string $betingelse): CoreModelCollectionInterface;

    /**
     * @param string|array|stdClass $tabellnavn
     * @param string $betingelse
     * @return Samling
     */
    public abstract function addLeftJoin($tabellnavn, string $betingelse): CoreModelCollectionInterface;

    /**
     * @param array|null $fields
     * @return Samling
     */
    public abstract function setMainModelFields(?array $fields = null): CoreModelCollectionInterface;

    /**
     * @param array|null $fields
     * @return $this
     */
    public function settHovedFelter(?array $fields = null): Samling
    {
        $this->setMainModelFields($fields);
        return $this;
    }

    /**
     * @param string $subQuery
     * @param string $alias
     * @param string $joinCondition
     * @param string $joinType
     * @param array $bindValues
     * @return CoreModelCollectionInterface
     */
    public abstract function addSubQueryJoin(
        string $subQuery,
        string $alias,
        string $joinCondition,
        string $joinType = 'LEFT',
        array $bindValues = []
    ): CoreModelCollectionInterface;

    /**
     * @param string $alias
     * @return $this
     */
    public abstract function removeJoin(string $alias): CoreModelCollectionInterface;

    /**
     * @param array $itemIds
     * @return $this
     */
    public abstract function filterByItemIds(array $itemIds): CoreModelCollectionInterface;

    /**
     * @return string
     */
    public abstract function getModel(): string;

    /**
     * @return array
     */
    public abstract function getFilters(): array;

    /**
     * @return array
     */
    public abstract function getHaving(): array;

    /**
     * @return Modell|null
     */
    public abstract function getFirstItem(): ?CoreModelInterface;

    /**
     * @return Modell|null
     */
    public abstract function getLastItem(): ?CoreModelInterface;

    /**
     * @return Modell|null
     */
    public abstract function getRandomItem(): ?CoreModelInterface;

    /**
     * @return bool
     */
    public abstract function hasLoaded(): bool;

    /**
     * @param CoreModelCollectionInterface $collection
     * @return $this
     */
    public abstract function mixWith(CoreModelCollectionInterface $collection): CoreModelCollectionInterface;

    /**
     * @return $this
     */
    public abstract function reverse(): CoreModelCollectionInterface;

    /**
     * @param Closure $sortfunction
     * @return $this
     */
    public abstract function sortLoadedItems(Closure $sortfunction): CoreModelCollectionInterface;

    /**
     * @param array $array
     * @return $this
     */
    public abstract function loadFromArray(array $array): CoreModelCollectionInterface;

    /**
     * @param int $start
     * @return $this
     */
    public abstract function setStart(int $start): CoreModelCollectionInterface;

    /**
     * @param int|null $limit
     * @return $this
     */
    public abstract function setLimit(int $limit = null): CoreModelCollectionInterface;

    /**
     * @return array
     */
    public abstract function getItemsArray(): array;

    /**
     * @param string $namespace
     * @param string $property
     * @param string $expression
     * @param string|null $aggregate
     * @return $this
     */
    public abstract function addExpression(
        string $namespace,
        string $property,
        string $expression,
        string $aggregate = null
    ): CoreModelCollectionInterface;

    /**
     * Preload all the Eav attributes for the entire collection
     *
     * @return $this
     */
    public function loadAllEavAttributes(): CoreModelCollectionInterface
    {
        return $this->lastAlleEavAttributter();
    }

    /**
     * @param int $start
     * @param int|null $lengde
     * @return $this
     */
    public function beskjær(int $start, int $lengde = null): Samling{
        return $this->slice($start, $lengde);
    }

    /**
     * @param array $filtre
     * @param bool $ikkeForm
     * @return $this
     */
    public function filtrer($filtre, bool $ikkeForm = false): Samling
    {
        return $this->setFilters($filtre, $ikkeForm);
    }

    /**
     * @param array $idNumre
     * @return $this
     */
    public function filtrerEtterIdNumre(array $idNumre): Samling
    {
        return $this->filterByItemIds($idNumre);
    }

    /**
     * @param string|string[]|array<string,mixed> $filter
     * @param bool $ikkeForm
     * @return $this
     */
    public function leggTilFilter($filter, bool $ikkeForm = false): Samling
    {
        return $this->addFilter($filter, $ikkeForm);
    }

    /**
     * @param $havingFilter
     * @param bool $ikkeForm
     * @return $this
     */
    public function leggTilHaving($havingFilter, bool $ikkeForm = false): Samling
    {
        return $this->addHaving($havingFilter, $ikkeForm);
    }

    /**
     * @return $this
     */
    public function låsFiltre(): Samling
    {
        return $this->lockFilters();
    }

    /**
     * @param string|array|stdClass $tabellnavn Tabellnavnet, eller [alias => tabellnavn]
     * @param string $betingelse
     * @return $this
     */
    public function leggTilInnerJoin($tabellnavn, string $betingelse): Samling
    {
        return $this->addInnerJoin($tabellnavn, $betingelse);
    }

    /**
     * @param string|array|stdClass $tabellnavn
     * @param string $betingelse
     * @return $this
     */
    public function leggTilLeftJoin($tabellnavn, string $betingelse): Samling
    {
        return $this->addLeftJoin($tabellnavn, $betingelse);
    }

    /**
     * @param string $subQuery
     * @param string $alias
     * @param string $betingelse
     * @param string $type
     * @return Samling
     */
    public function leggTilSubQueryJoin(
        string $subQuery,
        string $alias,
        string $betingelse,
        string $type = 'LEFT',
        array $bundneVerdier = []
    ): Samling
    {
        $this->addSubQueryJoin($subQuery, $alias, $betingelse, $type, $bundneVerdier);
        return $this;
    }

    /**
     * @param string $felt
     * @param bool $synkende
     * @param null $tabell
     * @param bool $nullTilSlutt
     * @return Samling
     */
    public function leggTilSortering(string $felt, bool $synkende = false, $tabell = null, bool $nullSomMaks = false): Samling
    {
        return $this->addSortOrder($felt, $synkende, $tabell, $nullSomMaks);
    }

    /**
     * @return bool
     */
    public function harLastet(): bool {
        return $this->hasLoaded();
    }

    /**
     * @return Modell|null
     * @throws Exception
     */
    public function hentFørste()
    {
        return $this->getFirstItem();
    }

    /**
     * @return Modell|null
     * @throws Exception
     */
    public function hentSiste()
    {
        return $this->getLastItem();
    }

    /**
     * @return Modell|null
     * @throws Exception
     */
    public function hentTilfeldig()
    {
        return $this->getRandomItem();
    }

    /**
     * @param CoreModelCollectionInterface $collection
     * @return $this
     */
    public function blandMed(CoreModelCollectionInterface $collection): Samling
    {
        return $this->mixWith($collection);
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function snu(): Samling
    {
        return $this->reverse();
    }

    /**
     * @param Closure $sortfunction
     * @return $this
     * @throws Exception
     */
    public function sorterLastede(Closure $sorteringsfunksjon): Samling
    {
        return $this->sortLoadedItems($sorteringsfunksjon);
    }

    /**
     * @return string[]
     */
    public function hentIdNumre(): array
    {
        return $this->getItemIds();
    }

    /**
     * @return int
     */
    public function hentAntall(): int {
        return $this->getTotalRows();
    }

    /**
     * @return int
     */
    public function hentBuntAntall(): int {
        return $this->getChunkRows();
    }

    /**
     * @param Modell[] $data
     * @return $this
     */
    public function lastFra(array $data): Samling{
        return $this->loadFromArray($data);
    }

    /**
     * @return array
     */
    public function hentElementer(): array
    {
        return $this->getItemsArray();
    }

    /**
     * @param int $start
     * @return $this
     */
    public function settStart(int $start): Samling
    {
        return $this->setStart($start);
    }

    /**
     * @param int|null $begrensning
     * @return $this
     */
    public function begrens($begrensning): Samling
    {
        return $this->setLimit($begrensning);
    }

    /**
     * @param string $egenskap
     * @param mixed $verdi
     * @return $this
     * @throws CoreModelException
     */
    public function sett($egenskap, $verdi): Samling
    {
        return $this->setData($egenskap, $verdi);
    }

    /**
     * @param stdClass $verdier
     * @return $this
     */
    public function settVerdier(stdClass $verdier): Samling
    {
        $this->setMultipleData($verdier);
        return $this;
    }

    /**
     *
     *
     * @param string $tabell Tabellen, eller tabell-aliaset verdien skal hentes ifra
     * @param string $felt Feltet verdien er lagret i
     * @param string|null $aggregatFunksjon F.eks 'SUM', 'MIN', 'MAX' etc
     * @param string|null $egenskapsnavn Egenskapen verdien skal returneres som
     * @param string|null $navneområde Navneområdet som skal brukes på egenskapen
     * @return $this
     */
    public function leggTilFelt(
        string $tabell,
        string $felt,
        string $aggregatFunksjon = null,
        string $egenskapsnavn = null,
        string $navneområde = null
    )
    {
        return $this->addField($tabell, $felt, $aggregatFunksjon, $egenskapsnavn, $navneområde);
    }

    /**
     * Legg til et SQL uttrykk i formatet `$aggregatFunksjon($uttrykk) AS $navneområde.$egenskapsnavn`
     *
     * @param string $navneområde
     * @param string $egenskapsnavn
     * @param string $uttrykk
     * @param string|null $aggregatFunksjon
     * @return Samling
     */
    public function leggTilUttrykk(
        string $navneområde,
        string $egenskapsnavn,
        string $uttrykk,
        string $aggregatFunksjon = null
    ):Samling
    {
        return $this->addExpression($navneområde, $egenskapsnavn, $uttrykk, $aggregatFunksjon);
    }

    /**
     * @return $this
     */
    public function fjernTilleggsfelter()
    {
        return $this->clearAdditionalFields();
    }

    /**
     * @return stdClass[]
     */
    public function hentTilleggsfelter()
    {
        return $this->getAdditionalFields();
    }

    /**
     * @param string $modell
     * @param string|null $navneområde
     * @param string|null $tabellKilde
     * @param array|null $felter
     * @return $this
     * @throws CoreModelException
     */
    public function leggTilModell(string $modell, string $navneområde = null, string $tabellKilde = null, ?array $felter = null)
    {
        if(
            isset($felter)
            && is_a($modell, CoreModelInterface::class, true)
        ) {
            $felter = array_merge($felter, [$modell::getDbPrimaryKeyField()]);
            $tabellKilde = $tabellKilde ?? $modell::getDbTable();
            $modellFelter = array_keys($modell::getDbFields());
            foreach ($felter as $felt) {
                if (in_array($felt, $modellFelter)) {
                    $this->addField($tabellKilde, $felt, null, null, $navneområde);
                }
            }
        }
        else {
            $this->addModel($modell, $navneområde, $tabellKilde);
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function nullstill(): Samling
    {
        return $this->reset();
    }

    /**
     * @return string
     */
    public function hentModell(): string {
        return $this->getModel();
    }

    /**
     * @return $this
     */
    public function slettAlle(): Samling
    {
        return $this->massDelete();
    }

    /**
     * @return Samling
     */
    public function deleteEach(): CoreModelCollectionInterface
    {
        return $this->slettHver();
    }

    /**
     * @return Samling
     */
    public function slettHver(): Samling
    {
        parent::deleteEach();
        return $this;
    }

    /**
     * @return $this
     */
    public function fjernJoin(string $alias): Samling
    {
        return $this->removeJoin($alias);
    }

    /**
     * @return array
     */
    public function hentFiltre(): array
    {
        return $this->getFilters();
    }

    /**
     * Preload all the Eav attributes for the entire collection
     * @return $this
     *
     * @todo Implement collection pre-loading of eav attributes.
     */
    public function lastAlleEavAttributter(): CoreModelCollectionInterface
    {
        return $this;
    }

    /**
     * Legg til Join for EAV-verdier
     *
     * ```
     * LEFT|INNER JOIN (
     *   SELECT `eav_verdier`.`objekt_id`, `eav_verdier`.`verdi`
     *   FROM `eav_verdier`
     *   WHERE `kode` = `$feltode` AND `modell` = `$modell`
     * ) AS `$feltAlias` ON `$feltAlias`.`objekt_id` = `$joinSource`.`$joinField`
     * ```
     *
     * @param string $feltKode Felt-egenskaps-koden som skal hentes
     * @param bool $innerJoin True for INNER JOIN, false for LEFT JOIN
     * @param class-string<CoreModelInterface>|null $modell Modellen som egenskapen tilhører, dersom denne er en annen enn samlingens egen modell
     * @param string|null $joinSource Tabell for Join-kriterie, dersom annen enn modellens egen tabell
     * @param string|null $joinField Felt for Join-kriterie, dersom annen enn modellens primærnøkkel
     * @param string|null $feltAlias Alias for feltet, dersom ikke egenskap-koden skal brukes som navn
     * @return Samling
     * @throws Exception
     */
    public function leggTilEavVerdiJoin(
        string $feltKode,
        bool $innerJoin = false,
        ?string $modell = null,
        ?string $joinSource = null,
        ?string $joinField = null,
        ?string $feltAlias = null
    ): Samling
    {
        $modell = $modell ?? $this->hentModell();
        $feltAlias = $feltAlias ?? $feltKode;
        if (is_a($modell, CoreModelInterface::class, true)) {
            $joinSource = $joinSource ?: $modell::getDbTable();
            $joinField = $joinField ?: $modell::getDbPrimaryKeyField();
            $condition = $condition ?? '`' . $feltAlias . '`.`objekt_id` = `' . $joinSource . '`.`' . $joinField . '`';
        }
        $tp = $this->mysqli->table_prefix;
        $egenskapTabell = $tp . Egenskap::hentTabell();
        $verdiTabell = $tp . Verdi::hentTabell();
        $subQuery = <<<SUBQUERY
            (
             SELECT `{$this->mysqli->escape($verdiTabell)}`.`objekt_id`, `{$this->mysqli->escape($verdiTabell)}`.`verdi`
             FROM `{$this->mysqli->escape($verdiTabell)}` INNER JOIN `{$this->mysqli->escape($egenskapTabell)}` ON `{$this->mysqli->escape($verdiTabell)}`.`egenskap_id` = `{$this->mysqli->escape($egenskapTabell)}`.`id`
             WHERE `{$this->mysqli->escape($egenskapTabell)}`.`kode` = '{$this->mysqli->escape($feltKode)}'
             AND `{$this->mysqli->escape($egenskapTabell)}`.`modell` = '{$this->mysqli->escape($modell)}'
            ) 
            SUBQUERY;
        $join = ($innerJoin ? 'INNER ': 'LEFT ') . "JOIN $subQuery AS `{$this->mysqli->escape($feltAlias)}` ON {$this->mysqli->escape($condition)}";
        if (isset($this->joins[$feltAlias]) && str_replace('`', '', $this->joins[$feltAlias]) != str_replace('`', '', $join)) {
            throw new Exception("Kan ikke re-definere alias `{$feltAlias}`");
        }
        $this->joins[$feltAlias] = $join;
        return $this;
    }

    /**
     * @param string[] $felter angitt som alias => $feltnavn
     * @param string $modell
     * @param string $joinSource
     * @param string $joinField
     * @return Samling
     * @throws Exception
     */
    public function leggTilLeftJoinForEavVerdier(
        array $felter,
        string $modell,
        string $joinSource = '',
        string $joinField = ''
    ): Samling
    {
        foreach ($felter as $alias => $felt) {
            if (is_numeric($alias)) {
                $alias = $felt;
            }
            $this->leggTilEavVerdiJoin($felt, false, $modell, $joinSource, $joinField, $alias);
        }
        return $this;
    }

    /**
     * @param string $feltKode
     * @param string|null $navneområde
     * @return $this
     * @throws Exception
     */
    public function leggTilEavFelt(string $feltKode, ?string $navneområde = null)
    {
        $navneområde = $navneområde ?? Egenskap::hentTabell();
        $this->leggTilEavVerdiJoin($feltKode);
        $this->leggTilFelt($feltKode, 'verdi', null, $feltKode, $navneområde);
        return $this;
    }

    /**
     * @param string $referanse
     * @param Sett|null $samling
     * @param string|null $modell
     * @param string|null $fremmedNøkkel
     * @param array $filtre
     * @param callable|null $callback
     * @return Samling|null
     */
    public function inkluder(
        string    $referanse, ?Sett $samling = null,
        ?string   $modell = null,
        ?string   $fremmedNøkkel = null,
        array     $filtre = [],
        ?callable $callback = null
    ): ?CoreModelCollectionInterface
    {
        return $this->include($referanse, $modell, $fremmedNøkkel, $filtre, $callback);
    }
}