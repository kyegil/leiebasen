<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 21/09/2022
 * Time: 22:41
 */

namespace Kyegil\Leiebasen;


use Exception;
use Kyegil\Leiebasen\Modell\Person;

/**
 * Class Kyegil\Leiebasen\Autoriserer
 */
abstract class Autoriserer implements Interfaces\AutorisererInterface
{
    protected Leiebase $app;

    protected ?Person\Brukerprofil $brukerprofil = null;

    protected ?Person $bruker = null;

    /**
     * Kyegil\Leiebasen\Autoriserer constructor.
     */
    public function __construct(Leiebase $app)
    {
        $this->app = $app;
    }

    /**
     * @return int|null
     * @throws Exception
     */
    public function hentId(): ?int
    {
        return $this->hentBruker() ? $this->hentBruker()->hentId() : null;
    }

    /**
     * @return string|null
     */
    public function hentNavn(): ?string
    {
        return $this->hentBruker() ? $this->hentBruker()->navn : null;
    }

    /**
     * @return string|null
     */
    public function hentEpost(): ?string
    {
        return $this->hentBruker() ? $this->hentBruker()->epost : null;
    }

    /**
     * @return string|null
     */
    public function hentBrukernavn(): ?string
    {
        return $this->hentBrukerprofil() ? $this->hentBrukerprofil()->login : null;
    }

    /**
     * @return bool
     */
    public function erInnlogget(): bool
    {
        return (bool)$this->hentBruker();
    }

    /**
     * @param string $login
     * @param int|null $personId
     * @return bool
     * @throws Exception
     */
    public function erLoginTilgjengelig(string $login, ?int $personId = null):bool
    {
        /** @var Person\Brukerprofilsett $profilsett */
        $profilsett = $this->app->hentSamling(Person\Brukerprofil::class);
        if ($personId) {
            $profilsett->leggTilFilter(['person_id !=' => $personId]);
        }
        /** @var Person\Brukerprofil|null $eksisterende */
        $eksisterende = $profilsett->finnLogin($login);
        return !$eksisterende;
    }

    /**
     * @return bool
     */
    abstract public function loggUt();

    /**
     * @param string $login
     * @param string $passord
     * @return bool
     */
    abstract public function loggInn(string $login = '', string $passord = ''): bool;

    /**
     * @param array|null $egenskaper
     * @return void
     */
    abstract public function krevIdentifisering(array $egenskaper = null);

    /**
     * @return Person|null
     */
    public function hentBruker(): ?Person
    {
        if (!isset($this->bruker)) {
            $this->bruker = $this->hentBrukerprofil() ? $this->hentBrukerprofil()->hentPerson() : null;
        }
        return $this->bruker;
    }

    /**
     * @return Person\Brukerprofil|null
     */
    abstract function hentBrukerprofil(): ?Person\Brukerprofil;

    /**
     * @param int $personId
     * @return string|null
     * @throws Exception
     */
    public function finnBrukernavnForPersonId(int $personId): ?string
    {
        /** @var Person $personModell */
        $personModell = $this->app->hentModell(Person::class, $personId);
        return $personModell && $personModell->hentBrukerProfil() ? $personModell->hentBrukerProfil()->login : null;
    }
}