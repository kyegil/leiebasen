<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 10:24
 */

namespace Kyegil\Leiebasen;


use Kyegil\CoreModel\CoreModel;
use Kyegil\CoreModel\CoreModelCollectionFactory;
use Kyegil\CoreModel\CoreModelFactory;
use Kyegil\CoreModel\CoreModelRepository;
use Kyegil\Leiebasen\Oppslag\AbstraktKontroller;
use Kyegil\MysqliConnection\MysqliConnection;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

class Oppslagsbestyrer
{
    const BASE_NAMESPACE = 'Kyegil\\Leiebasen\\Oppslag\\';
    const STANDARDOPPSLAG = 'Oversikt';

    /**
     * @var array
     */
    private static array $di;

    /** @var string Fila (f.eks. index.php) som prosesser forespørselen */
    public string $fil;

    /** @var string Område (eks. drift, mine-sider e.l. */
    protected string $område;

    /** @var array Alle $_GET-verdiene */
    protected array $get;

    /** @var array Alle $_SERVER-verdiene */
    protected array $server;

    /** @var string Hele den forespurte URIen  */
    protected $requestUri;

    /** @var callable[]  */
    protected array $controllerMappers = [];

    protected array $ruter = [
        'sentral' => [],
        'offentlig' => [],
        'drift' => [],
        'mine-sider' => [],
        'flyko' => [],
        'oppfølging' => [],
        'min-søknad' => [],
    ];

    /** @var string[] */
    protected array $inkluderinger;

    /**
     * Klassen som oppslaget er av
     *
     * @var class-string<AbstraktKontroller>|null
     */
    protected ?string $klasse = null;

    /** @var AbstraktKontroller|null Oppslaget (kontrolleren) som forestår responsen fra leiebasen */
    protected ?AbstraktKontroller $oppslag = null;

    /**
     * @var array
     */
    protected array $områdeTilNameSpaceMapping = [];

    /**
     * @param array $overrides
     * @return array
     */
    public static function forberedDependencies(array $overrides = []):array
    {
        if(!isset(self::$di)) {
            self::$di = [];
            $defaults = [
                MysqliConnection::class => function(){return new MysqliConnection(
                    Leiebase::$config['leiebasen']['db']['host'] ?? null,
                    Leiebase::$config['leiebasen']['db']['user'] ?? null,
                    Leiebase::$config['leiebasen']['db']['password'] ?? null,
                    Leiebase::$config['leiebasen']['db']['name'] ?? null
                );},
                LoggerInterface::class => function(){
                    $logger = new Logger('Leiebasen');
                    if(!$logger->getHandlers()) {
                        $logger->pushHandler(new StreamHandler((
                            Leiebase::$config['leiebasen']['server']['root'] ?? '') . 'var/log/Leiebasen.log',
                                Leiebase::$config['leiebasen']['logger']['level'] ?? Logger::INFO)
                        );
                    }

                    return $logger;
                },
                CoreModelCollectionFactory::class => function(){return  new SamlingsFabrikk();},
                CoreModelFactory::class => function(){return new ModellFabrikk();},
                CoreModelRepository::class => function(){return ModellOppbevaring::hentForekomst();},
            ];

            foreach ($defaults as $key => $process) {
                self::$di[$key] = $process();
            }
        }
        return array_merge(self::$di, $overrides);
    }

    /**
     * Oppslagsbestyrer constructor
     *
     * @param string $fil Fila (f.eks. index.php) som prosesser forespørselen
     */
    public function __construct(string $fil)
    {
        extract(Leiebase::preStatic(static::class, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $this->fil = $fil;
        $this->get = $_GET ?? [];
        $this->server = $_SERVER ?? [];
        $this->ruter = array_merge($this->ruter, Leiebase::$config['routes'] ?? []);
        $this->requestUri = $_SERVER['REQUEST_URI'] ?? null;

        $coreModelLogger = new Logger('CoreModel');
        CoreModel::$logger = $coreModelLogger;
        $coreModelLogger->pushHandler(new StreamHandler((
                Leiebase::$config['leiebasen']['server']['root'] ?? '') . 'var/log/CoreModel.log',
            Leiebase::$config['leiebasen']['logger']['level'] ?? Logger::INFO
        ));
        $this->leggTilControllerMapper([$this, 'hentKontrollerNavn']);
        Leiebase::postStatic($this, __FUNCTION__, $this, $args);
    }

    /**
     * @return Leiebase
     */
    public function hentOppslag($di = [], $config = [])
    {
        if(!isset($this->oppslag) && isset($this->klasse)) {
            $di = $this->forberedDependencies($di);
            $this->oppslag = new $this->klasse($di, $config);
        }

        return $this->oppslag;
    }

    /**
     * Filer som må inkluderes før oppslaget initieres
     *
     * @return string[]
     */
    public function hentInkluderinger(): array
    {
        if(!isset($this->inkluderinger)) {
            $this->inkluderinger = [];
            $this->prosesserOppslag();
        }
        return $this->inkluderinger;
    }

    /**
     * Prosesser oppslaget
     *
     * @return $this
     */
    protected function prosesserOppslag(): Oppslagsbestyrer
    {
        $område = strtolower(empty($this->get['område'])
            ? basename(dirname($this->fil))
            : $this->get['område']);
        $this->område = array_key_exists($område, $this->ruter) ? $område : '';
        /** @var string $oppslagsforespørsel */
        $oppslagsforespørsel = $this->get['oppslag'] ?? '';

        $this->klasse = $this->ruter[$this->område][$oppslagsforespørsel] ?? null;

        if(!$this->klasse) {
            foreach($this->hentControllerMappers() as $mapper) {
                $this->klasse = $mapper($oppslagsforespørsel, $this);
                if($this->klasse) {
                    break;
                }
            }
        }
        if(!$this->klasse) {
            /** Vi finner ikke noen kontroller i hht til det nye designet. Gå tilbake til tidligere design */
            $this->prosesserOppsett();
        }
        return $this;
    }

    /**
     * Fallback for tidligere design
     * der det ble brukt uttallige definisjoner av oppsett
     *
     * Prosesser oppsettet
     *
     * @return $this
     */
    protected function prosesserOppsett(): Oppslagsbestyrer
    {
        if($this->område == 'drift') {
            $fil = "";
            if( !empty($this->get['oppslag'])) {
                $fil = dirname($this->fil) . '/drift/' . $this->get['oppslag'].".php";
            }
            if(!$fil || !file_exists($fil)) {
                $fil = dirname($this->fil) . '/drift/forsiden.php';
            }
            $this->inkluderinger[] = $fil;
            $this->klasse = '\oppsett';
        }
        return $this;
    }

    /**
     * Område til namespace
     * Endrer et element i url fra formatet område-navn til OmrådeNavn for bruk i controller-namespace
     *
     * @param string $forespørsel
     * @return string
     */
    public static function tilNamespace(string $forespørsel): string
    {
        $forespørsel = str_replace('_', '-', $forespørsel);
        $elementer = explode('-', $forespørsel);
        $elementer = array_map(function($element) {
            return Leiebase::ucfirst($element);
        }, $elementer);
        return implode('', $elementer);
    }

    /**
     * @param string $område
     * @param string|null $namespace
     * @return Oppslagsbestyrer
     */
    public function settOmrådeTilNameSpaceMapping(string $område, ?string $namespace): Oppslagsbestyrer
    {
        if (isset($namespace)) {
            $this->områdeTilNameSpaceMapping[$område] = $namespace;
        }
        else {
            unset($this->områdeTilNameSpaceMapping[$område]);
        }
        return $this;
    }

    /**
     * @param string $oppslagsforespørsel
     * @return string|null
     */
    public function hentKontrollerNavn(string $oppslagsforespørsel): ?string
    {
        $base = self::BASE_NAMESPACE;
        /** @var class-string<AbstraktKontroller> $klasse */
        $klasse = $base
            . Oppslagsbestyrer::tilNamespace($this->hentOmråde())
            . '\\' . Oppslagsbestyrer::tilNamespace($oppslagsforespørsel);
        if (class_exists($klasse) && is_a($klasse, AbstraktKontroller::class, true)) {
            return $klasse;
        }
        $klasse = $base . Oppslagsbestyrer::tilNamespace($this->hentOmråde()) . 'Kontroller';
        if (
            is_a($klasse, AbstraktKontroller::class, true)
            && property_exists($klasse, 'standardoppslag')
            && is_a($klasse::$standardoppslag, AbstraktKontroller::class, true)
        ) {
            return $klasse::$standardoppslag;
        }
        $klasse = $base . Oppslagsbestyrer::tilNamespace($this->hentOmråde()) . '\\' . self::STANDARDOPPSLAG;
        if (is_a($klasse, AbstraktKontroller::class, true)) {
            return $klasse;
        }
        return null;
    }

    /**
     * @return callable[]
     */
    public function hentControllerMappers(): array
    {
        return $this->controllerMappers;
    }

    /**
     * @param callable $mapper
     * @return $this
     */
    public function leggTilControllerMapper(callable $mapper): Oppslagsbestyrer
    {
        $this->controllerMappers[] = $mapper;
        return $this;
    }

    /**
     * @param string $område
     * @return $this
     */
    public function settOmråde(string $område): Oppslagsbestyrer
    {
        $this->område = $område;
        return $this;
    }

    /**
     * @return string
     */
    public function hentOmråde(): string
    {
        return $this->område;
    }
}