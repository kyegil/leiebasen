<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 17/05/2020
 * Time: 10:49
 */

namespace Kyegil\Leiebasen;


use Exception;

/**
 *
 */
class Opplasting
{
    const ERROR_TEXT = [
        UPLOAD_ERR_OK => 'There is no error, the file uploaded with success.',
        UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
        UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the form.',
        UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded.',
        UPLOAD_ERR_NO_FILE => 'No file was uploaded.',
        UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder.',
        UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk.',
        UPLOAD_ERR_EXTENSION => 'A PHP extension stopped the file upload.',
    ];

    /** @var string */
    protected $filnavn;

    /** @var int */
    protected $filStørrelse;

    /** @var string */
    protected $tmpName;

    /** @var string */
    protected $lagringsmappe;

    /** @var int  */
    protected $error = 0;

    /** @var int */
    protected $maksFilstørrelse = 1000000;

    /** @var string[] */
    protected $tillatteMimes = [
        'application/msword',
        'application/pdf',
        'application/vnd.oasis.opendocument.spreadsheet',
        'application/vnd.oasis.opendocument.text',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/x-tar',
        'application/zip',
        'image/jpeg',
        'image/png',
        'text/plain'
    ];

    /** @var string */
    protected $opplastetFilbane;

    /**
     * Opplasting constructor.
     * @param array $vedlegg
     */
    public function __construct($vedlegg = null)
    {
        if(isset($vedlegg['tmp_name'])) {
            $this->tmpName = $vedlegg['tmp_name'];
        }
        if(isset($vedlegg['name'])) {
            $this->filnavn = basename($vedlegg['name']);
        }
        if(isset($vedlegg['size'])) {
            $this->filStørrelse = (int)$vedlegg['size'];
        }
        if(isset($vedlegg['error'])) {
            $this->error = $vedlegg['error'];
        }
    }

    /**
     * @return string
     */
    public function hentFilnavn()
    {
        return $this->filnavn;
    }

    /**
     * @return int
     */
    public function hentFilStørrelse(): int
    {
        return $this->filStørrelse;
    }

    /**
     * @return string
     */
    public function hentLagringsmappe()
    {
        return $this->lagringsmappe;
    }

    /**
     * @return int
     */
    public function hentMaksFilstørrelse(): int
    {
        return $this->maksFilstørrelse;
    }

    /** string|null */
    public function hentOpplastetFilbane():? string
    {
        return $this->opplastetFilbane;
    }

    /**
     * Sett filbane og filnavn direkte.
     * Denne kan brukes for filer som allerede har blitt flyttet fra tmp
     *
     * @param string $filbane
     * @return $this
     */
    public function settOpplastetFilbane(string $filbane): Opplasting
    {
        if(file_exists($filbane)) {
            $this->opplastetFilbane = $filbane;
        }
        return $this;
    }

    /**
     * @return string[]
     */
    public function hentTillatteMimes(): array
    {
        return $this->tillatteMimes;
    }

    /**
     * @return string
     */
    public function hentTmpName(): string
    {
        return $this->tmpName;
    }

    /**
     * @param string $filnavn
     * @return Opplasting
     */
    public function settFilnavn(string $filnavn)
    {
        $this->filnavn = $filnavn;
        return $this;
    }

    /**
     * @param int $filStørrelse
     * @return Opplasting
     */
    public function settFilStørrelse(int $filStørrelse): Opplasting
    {
        $this->filStørrelse = $filStørrelse;
        return $this;
    }

    /**
     * @param mixed $lagringsmappe
     * @return Opplasting
     */
    public function settLagringsmappe($lagringsmappe): Opplasting
    {
        $this->lagringsmappe = $lagringsmappe;
        return $this;
    }

    /**
     * @param int $maksFilstørrelse
     * @return Opplasting
     */
    public function settMaksFilstørrelse(int $maksFilstørrelse): Opplasting
    {
        $this->maksFilstørrelse = $maksFilstørrelse;
        return $this;
    }

    /**
     * @param string[] $tillatteMimes
     * @return Opplasting
     */
    public function settTillatteMimes(array $tillatteMimes): Opplasting
    {
        $this->tillatteMimes = $tillatteMimes;
        return $this;
    }

    /**
     * @param string $tmpName
     * @return Opplasting
     */
    public function settTmpName(string $tmpName): Opplasting
    {
        $this->tmpName = $tmpName;
        return $this;
    }

    /**
     * @param null $destinasjon
     * @return Opplasting
     * @throws Exception
     */
    public function lagre($destinasjon = null): Opplasting
    {
        if(isset($destinasjon)) {
            $this->settLagringsmappe($destinasjon);
        }
        $this->valider();
        $destinasjon = $this->hentLagringsmappe();
        if (!file_exists($destinasjon)) {
            mkdir($destinasjon);
            chmod($destinasjon, 0755);
        }
        if(!is_writable($destinasjon)) {
            throw new Exception('Den opplastede fila kan ikke lagres');
        }

        while (file_exists($destinasjon . '/' . $this->hentFilnavn())) {
            $parts = pathinfo($this->hentFilnavn());
            $filnavn = $parts['filename'];
            $navneElementer = explode('--', $filnavn);
            if ($navneElementer && count($navneElementer) > 1 && is_numeric(end($navneElementer))) {
                $navneElementer[] = array_pop($navneElementer) + 1;
                $filnavn = implode('--', $navneElementer);
            }
            else {
                $filnavn .= '--1';
            }
            if (isset($parts['extension'])) {
                $this->settFilnavn($filnavn . '.' . $parts['extension']);
            }
            else {
                $this->settFilnavn($filnavn);
            }
        }
        if (!move_uploaded_file($this->hentTmpName(), $destinasjon . '/' . $this->hentFilnavn())) {
            throw new Exception('Den opplastede fila kunne ikke lagres');
        }
        $this->opplastetFilbane = $destinasjon . '/' . $this->hentFilnavn();
        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function valider(): Opplasting
    {
        if($this->error) {
            throw new Exception("Det oppstod en feil med filopplastingen:<br>" . self::ERROR_TEXT[$this->error]);
        }
        if(!$this->hentFilnavn() || !$this->hentTmpName()) {
            throw new Exception('Opplastingen ble ikke funnet.');
        }
        $mime = $this->fastsettMime();
        if(!$mime) {
            throw new Exception('Kunne ikke fastsette filtypen av den opplastede fila.');
        }
        if(!in_array(($mime), $this->hentTillatteMimes())) {
            throw new Exception('Den opplastede fila er av et ikke tillat format.');
        }
        if($this->filStørrelse > $this->hentMaksFilstørrelse()){
            throw new Exception("Den opplastede fila må ikke overskride {$this->maksFilstørrelse} bytes.");
        }
        return $this;
    }

    /**
     * @return string|null
     */
    protected function fastsettMime()
    {
        $mime = null;
        if(function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $this->tmpName);
            finfo_close($finfo);
        }

        if(!$mime && function_exists('mime_content_type')) {
            $mime = mime_content_type($this->tmpName);
        }
        return $mime;
    }

    /**
     * @return $this
     * @throws Exception
     */
    public function slettOpplastetFil(): Opplasting
    {
        if(file_exists($this->opplastetFilbane)) {
            if(!is_writable($this->opplastetFilbane)) {
                throw new Exception("{$this->opplastetFilbane} kan ikke slettes.");
            }
            unlink($this->opplastetFilbane);
        }
        return $this;
    }
}