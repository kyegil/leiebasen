<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 11/06/2020
 * Time: 21:57
 */

namespace Kyegil\Leiebasen;


use Kyegil\CoreModel\Interfaces\AppInterface as CoreModelAppInterface;
use Kyegil\CoreModel\Interfaces\CoreModelCollectionInterface;
use Kyegil\CoreModel\Interfaces\CoreModelInterface;
use Kyegil\ViewRenderer\AppInterface as ViewAppInterface;
use Kyegil\ViewRenderer\ViewFactory;
use Kyegil\ViewRenderer\ViewInterface;
use stdClass;

class CoreModelImplementering extends Leiebase implements CoreModelAppInterface, ViewAppInterface
{

    /**
     * @inheritDoc
     */
    public function getModel(string $class, $id): CoreModelInterface
    {
        return parent::hentModell($class, $id);
    }

    /**
     * @inheritDoc
     */
    public function newModel(string $class, stdClass $params = null): CoreModelInterface
    {
        return parent::nyModell($class, $params);
    }

    /**
     * @inheritDoc
     */
    public function getModelCollection($class, bool $reuse = true, string $id = ''): CoreModelCollectionInterface
    {
        return parent::hentSamling($class, $reuse, $id);
    }

    /**
     * @inheritDoc
     */
    public function dispatchEvent($event, $args = null)
    {
        return EventManager::getInstance()->triggerEvent(get_class($this), $event, $this, $args);
    }

    /**
     * @param string $camelCase
     * @return string
     */
    public function makeUnderscore(string $camelCase): string
    {
        return $this->tilUnderscore($camelCase);
    }

    /**
     * @param string $underscore
     * @return string
     */
    public function makeCamelCase($underscore): string
    {
        return $this->tilCamelCase($underscore);
    }

    /**
     * @param $subject
     * @param string $method
     * @param array $args
     * @return array
     */
    public function before($subject, string $method, array $args): array
    {
        return parent::pre($subject, $method, $args);
    }

    /**
     * @param $subject
     * @param string $method
     * @param $result
     * @return mixed
     */
    public function after($subject, $method, $result)
    {
        return parent::post($subject, $method, $result);
    }

    /**
     * @return string
     */
    public function getCMTemplatesPath(): string
    {
        return "{$this->root}_gjengivelser/";
    }

    /**
     * @param string $template
     * @param array $data
     * @return ViewInterface
     */
    public function getView(string $template, array $data): ViewInterface
    {
        return $this->hentVisning($template, $data);
    }

    /**
     * @return ViewFactory
     */
    public function getViewFactory(): ViewFactory
    {
        return $this->viewFactory;
    }
}