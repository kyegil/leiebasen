<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 30/09/2022
 * Time: 20:14
 */

namespace Kyegil\Leiebasen;

use Kyegil\Leiebasen\Interfaces\EventManagerInterface;

class EventManager implements EventManagerInterface
{
    /**
     * @var int
     */
    public static int $triggerCount = 0;
    /** @var EventManager */
    private static $instance;

    private static $listeners = [];

    private static $inheritedListeners = [];

    public static function addListener(string $class, string $event, callable $action, ?bool $priority = false)
    {
        settype(self::$listeners[$class][$event], 'array');
        if ($priority) {
            array_unshift(self::$listeners[$class][$event], $action);
        }
        else {
            self::$listeners[$class][$event][] = $action;
        }
        self::$inheritedListeners = [];
    }

    /**
     * @param string $class
     * @param string $event
     * @return callable[]
     */
    public static function getListeners(string $class, string $event): array
    {
        if (!class_exists($class)) {
            return [];
        }
        if(!isset(self::$inheritedListeners[$class])) {
            self::$inheritedListeners[$class] = [];
            $childClass = $class;
            while($childClass = get_parent_class($childClass)) {
                if (self::$listeners[$childClass] ?? []) {
                    settype(self::$inheritedListeners[$class], 'array');
                    self::$inheritedListeners[$class] = array_merge_recursive(self::$inheritedListeners[$class], self::$listeners[$childClass] ?? []);
                }
            }
        }
        return array_merge(self::$inheritedListeners[$class][$event] ?? [], self::$listeners[$class][$event] ?? []);
    }

    /**
     * @param string $class
     * @param string|null $event
     */
    public static function clearListeners(string $class, ?string $event = null)
    {
        if(isset($event)) {
            self::$listeners[$class][$event] = [];
        }
        else {
            self::$listeners[$class] = [];
        }
    }

    /**
     * @param class-string $class
     * @param string $event
     * @param object|null $subject
     * @param array $arguments
     * @param object|null $app
     * @return array|null
     */
    public function triggerEvent(string $class, string $event, ?object $subject, array $arguments = [], ?object $app = null):?array
    {
        self::$triggerCount++;
        $listeners = self::getListeners($class, $event);
        foreach ($listeners as $action) {
            $arguments = $action($subject, $arguments, $app);
        }
        return $arguments;
    }

    /**
     * @return EventManager
     */
    public static function getInstance()
    {
        if ( !isset( self::$instance ) )
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     *
     */
    private function __construct()
    {
        /**
         * @var string $class
         * @var  $listeners
         */
        foreach(\Kyegil\Leiebasen\Leiebase::$config['listeners'] as $class => $listeners) {
            /**
             * @var string $event
             * @var callable[] $actions
             */
            foreach ($listeners as $event => $actions) {
                foreach ($actions as $action) {
                    if(is_callable($action)) {
                        self::addListener($class, $event, $action);
                    }
                }
            }
        }
    }
}