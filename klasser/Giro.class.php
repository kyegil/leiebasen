<?php

use Kyegil\Leiebasen\Modell\Leieforhold\Regning;

/**
 * Class Giro
 *
 * @deprecated
 * @see Regning
 */
class Giro extends DatabaseObjekt {

    /**
     * @var string
     */
    protected	$tabell = "giroer";	// Hvilken tabell i databasen som inneholder primærnøkkelen for dette objektet
    /**
     * @var string
     */
    protected	$idFelt = "gironr";	// Hvilket felt i tabellen som lagrer primærnøkkelen for dette objektet
    /**
     * @var
     */
    protected	$data;				// DB-verdiene lagret som et objekt Null betyr at verdiene ikke er lastet
    /**
     * @var \Krav[]
     */
    protected	$krav;				// Array over alle kravene på denne giroen. Null betyr at kravene ikke er lastet.
    /**
     * @var
     */
    protected	$efaktura;			// stdClass-objekt. Usann betyr at efaktura ikke finnes. Null betyr at verdiene ikke er lastet.
    /**
     * @var
     */
    protected	$fboTrekkrav;		// stdClass-objekt. Usann betyr at krav ikke finnes. Null betyr at verdiene ikke er lastet.
    /**
     * @var \Purring[]
     */
    protected	$purringer;				// Array over alle purringene på denne giroen. Null betyr at purringene ikke er lastet.
    /**
     * @var array
     */
    protected	$utskriftsposisjon = array();	// Utskriftsposisjonen for hver enkelt rute, sortert som et array med rutenummeret som nøkkel
    /**
     * @var
     */
    public		$id;				// Unikt id-heltall for dette objektet


    /**
     * Giro constructor.
     * @param null $param
     *      ->id int gironummeret
     * @throws Exception
     */
    public function __construct($param = null ) {
        parent::__construct( $param );
    }


    /**
     * FBO Oppdragsfrist
     *
     * Returnerer siste mulige anledning til å sende krav om trekk via AvtaleGiro
     * og opprettholde NETS' frister for varsling.
     * Dersom forfallsdato ikke er fastsatt vil fristen settes til null
     *
     * @param DateTime|null $forfall
     * @return DateTime|null
     * @throws Exception
     * @deprecated
     * @see Regning::hentFboOppdragsfrist()
     */
    public function fboOppdragsfrist($forfall = null ) {
        if( $forfall === null ) {
            $forfall = $this->hent('forfall');
        }
        else if( !is_a($forfall, DateTime::class) ) {
            $forfall = new DateTime( $forfall );
        }

        if( !is_a($forfall, 'DateTime') ) {
            return null;
        }

        $resultat = clone $forfall;

        /** @var \Leieforhold $leieforhold */
        $leieforhold = $this->hent('leieforhold');
        $fbo = $leieforhold->hent('fbo');
        if( !$fbo ) {
            return null;
        }
        $bankvarsel = $fbo->varsel
            && !$leieforhold->hent('brukerepost')
            && $leieforhold->hent('efakturaavtale');

        if( $bankvarsel ) {
            if( $resultat->format('d') > 14 ) {
                $resultat->sub( new DateInterval('P1M') );
            }
            else {
                $resultat->sub( new DateInterval('P2M') );
            }
            $resultat = new DateTime( $resultat->format('Y-m-t') );
        }

        else {
            $resultat->sub( new DateInterval('P9D') );
        }

        $bankfridager = $this->leiebase->bankfridager( $resultat->format('Y') );
        while(
            in_array( $resultat->format('m-d'), $bankfridager )
            or $resultat->format('N') > 5
        ) {
            $resultat->sub(new DateInterval('P1D'));
        }

        $resultat->setTime(14, 0, 0);

        return $resultat;
    }


// Hent egenskaper
/****************************************/
//	$param
//		id	(heltall) gironummeret	
//	--------------------------------------
    /**
     * Hent egenskaper
     *
     * @param string $egenskap
     * @param array $param
     *      ['id'] int gironummeret
     * @return mixed|null
     * @throws Exception
     */
    public function hent($egenskap, $param = array()) {

        if( !$this->id ) {
            return null;
        }

        switch( $egenskap ) {

        case $this->idFelt:
        {
            if ( $this->data === null and !$this->last()) {
                return false;
            }
            return $this->data->$egenskap;
        }

        case "utskriftsdato":
        case "kid":
        case "leieforhold":
        case "leieobjekt":
        case "format":
        case "forfall":
        case "beløp":
        case "utestående":
        case "regning_til_objekt":
        case "regningsobjekt":
        {
            if ( $this->data === null and !$this->last()) {
                throw new Exception("Klarte ikke laste Giro({$this->id})");
            }
            return $this->data->$egenskap;
        }

        case "utskriftsposisjon":
        {
            if ( $this->data == null ) {
                $this->last();
            }

            if ( !isset($this->utskriftsposisjon[$param['rute']]) ) {
                $this->lastUtskriftsposisjon($param['rute']);
            }
            return $this->utskriftsposisjon[$param['rute']];
        }

        case "krav":
        {
            if ( $this->krav === null ) {
                $this->lastKrav();
            }
            return $this->krav;
        }

        case "betalt":
        {
            if ( $this->hent('utestående') != 0 ) {
                return false;
            }
            $betalinger = $this->hentBetalinger();
            if(!$betalinger) {
                return $this->hent('forfall');
            }
            return end($betalinger)->hent('dato');
        }

        case "sistePurring":
        {
            /** @var Regning $regning */
            $regning = $this->leiebase->hentModell(Regning::class, $this->id);
            return $regning->hentSistePurring();

        }

        case "sisteForfall":
        {
            /** @var \Kyegil\Leiebasen\Modell\Leieforhold\Purring|null $sistePurring */
            $sistePurring = $this->hent('sistePurring');
            if ($sistePurring) {
                return $sistePurring->hentPurreforfall();
            }
            return $this->hent('forfall');
        }

        case "fboTrekkrav":
        {
            if ( $this->fboTrekkrav === null ) {
                $this->lastFboTrekkrav();
            }
            return $this->fboTrekkrav;
        }

        case "efaktura":
        {
            if ( $this->efaktura === null ) {
                $this->lastEfaktura();
            }
            return $this->efaktura;
        }

        default:
        {
            return null;
        }
        }

    }


    /**
     * Hent Betalinger
     *
     * Se etter betalinger ført mot denne giroen
     *
     * @return array Innbetalingene
     * @throws Exception
     */
    public function hentBetalinger() {
        $tp = $this->mysqli->table_prefix;

        $innbetalinger = $this->mysqli->arrayData(array(
            'source'		=>	"{$tp}innbetalinger as innbetalinger INNER JOIN {$tp}krav AS krav ON innbetalinger.krav = krav.id",
            'where'			=> "krav.gironr = '{$this->hentId()}'",
            'distinct'		=> true,
            'fields'		=> "innbetalinger.innbetaling as id",
            'orderfields'	=> "innbetalinger.dato, innbetalinger.innbetaling",
            'class'			=> \Innbetaling::class
        ));

        return $innbetalinger->data;
    }



    /**
     * Last giroens kjernedata fra databasen
     *
     * @param int $id gironummeret
     * @return bool|mixed
     * @throws Exception
     */
    protected function last($id = 0) {
        $tp = $this->mysqli->table_prefix;

        settype($id, 'integer');
        if( !$id ) {
            $id = $this->id;
        }

        $resultat = $this->mysqli->arrayData(array(
            'returnQuery'	=> true,

            'fields' =>			"{$this->tabell}.{$this->idFelt} AS id,
                                {$this->tabell}.*,\n"
                            .	"MIN(krav.forfall) AS forfall,
                                SUM(krav.beløp) AS beløp,
                                SUM(krav.utestående) AS utestående,\n"
                            .	"leieforhold.leieobjekt,
                                leieforhold.regningsperson,
                                leieforhold.regning_til_objekt,
                                leieforhold.regningsobjekt,
                                leieforhold.regningsadresse1,
                                leieforhold.regningsadresse2,
                                leieforhold.postnr,
                                leieforhold.poststed,
                                leieforhold.land",

            'source' => 		"{$tp}{$this->tabell} AS {$this->tabell}\n"
                            .	"LEFT JOIN {$tp}krav AS krav ON {$this->tabell}.{$this->idFelt} = krav.gironr\n"
                            .	"LEFT JOIN (
        SELECT kontrakter.*
        FROM (
            SELECT MAX(kontraktnr) as kontraktnr, leieforhold
            FROM kontrakter
            GROUP BY leieforhold
        ) as sistekontrakt
        INNER JOIN kontrakter on sistekontrakt.kontraktnr = kontrakter.kontraktnr
    ) AS leieforhold ON {$this->tabell}.leieforhold = leieforhold.leieforhold\n"
                            .	"LEFT JOIN {$tp}leieobjekter AS leieobjekter ON leieforhold.leieobjekt = leieobjekter.leieobjektnr\n",

            'groupfields'	=>	"{$this->tabell}.{$this->idFelt}",
            'where'			=>	"{$tp}{$this->tabell}.{$this->idFelt} = '$id'"
        ));
        if( $this->data = @$resultat->data[0] ) {
            $this->id = $id;

            $this->data->leieforhold = new \Leieforhold( $this->data->leieforhold );

            if( $this->data->utskriftsdato ) {
                $this->data->utskriftsdato = new DateTime( $this->data->utskriftsdato );
            }

            if( $this->data->forfall ) {
                $this->data->forfall = new DateTime( $this->data->forfall );
            }
            return true;
        }
        else {
            $this->id = null;
            $this->data = null;
            return false;
        }

    }


    /**
     * Last evt efakturadetaljer fra databasen
     *
     * @return bool
     * @throws Exception
     */
    protected function lastEfaktura() {
        $tp = $this->mysqli->table_prefix;
        if ( !$id = $this->id ) {
            $this->efaktura = false;
            return false;
        }

        $resultat = $this->mysqli->arrayData(array(
            'returnQuery'	=> true,
            'fields'		=>	"efakturaer.id,
                                efakturaer.forsendelsesdato,
                                efakturaer.forsendelse,
                                efakturaer.oppdrag,
                                efakturaer.transaksjon,
                                efakturaer.kvittert_dato,
                                efakturaer.kvitteringsforsendelse,
                                efakturaer.status",
            'source'		=> 	"{$tp}efakturaer AS efakturaer\n",
            'where'			=>	"efakturaer.giro = '$id'"
        ));

        if( $resultat->totalRows ) {
            $this->efaktura = $resultat->data[0];
            $this->efaktura->forsendelsesdato = date_create_from_format('Y-m-d', $this->efaktura->forsendelsesdato);
            $this->efaktura->kvittertDato = date_create_from_format('Y-m-d', $this->efaktura->kvittert_dato);
        }
        else {
            $this->efaktura = false;
        }
        return true;
    }


    /**
     * Last evt fbo trekk-krav (avtalegiro) fra databasen
     *
     * @return bool
     * @throws Exception
     */
    protected function lastFboTrekkrav() {
        $tp = $this->mysqli->table_prefix;
        if ( !$id = $this->id ) {
            $this->fboTrekkrav = false;
            return false;
        }

        $resultat = $this->mysqli->arrayData(array(
            'returnQuery'	=> true,
            'fields'		=>	"fbo_trekkrav.id,
                                fbo_trekkrav.overføringsdato,
                                fbo_trekkrav.varslet,
                                fbo_trekkrav.egenvarsel,
                                fbo_trekkrav.mobilnr",
            'source'		=> 	"{$tp}fbo_trekkrav AS fbo_trekkrav\n",
            'where'			=>	"fbo_trekkrav.gironr = '$id'"
        ));

        if( $resultat->totalRows ) {
            $this->fboTrekkrav = $resultat->data[0];
            $this->fboTrekkrav->overføringsdato = date_create_from_format('Y-m-d', $this->fboTrekkrav->overføringsdato);
        }
        else {
            $this->fboTrekkrav = false;
        }
        return true;
    }


    /**
     * Last kravene i giroen fra databasen
     *
     * @throws Exception
     */
    protected function lastKrav():void {
        $tp = $this->mysqli->table_prefix;
        if ( !$id = $this->id ) {
            $this->krav = null;
            return;
        }

        $resultat = $this->mysqli->arrayData(array(
            'returnQuery'	=> true,
            'class' 		=>	\Krav::class,
            'fields'		=>	"krav.id\n",
            'source'		=> 	"{$tp}krav AS krav\n",
            'where'			=>	"krav.gironr = '$id'",
            'orderfields'	=>	"IF(krav.forfall IS NULL, 1, 0), krav.forfall, krav.kravdato, krav.type, krav.id"
        ));

        $this->krav = $resultat->data;
    }


    /**
     * Last utskriftsposisjonen for giroen
     *
     * @param null|int $rute utdelingsruten som bestemmer utskriftsrekkefølgen
     * @throws Exception
     */
    protected function lastUtskriftsposisjon($rute = null): void {
        $tp = $this->mysqli->table_prefix;
        settype($rute, 'integer');
        if ( !$id = $this->id ) {
            $this->utskriftsposisjon = array();
            return;
        }

        $this->utskriftsposisjon[$rute] = intval((string)$this->hent('leieforhold'));

        if( $this->hent('regning_til_objekt') ) {
            $posisjon = $this->mysqli->arrayData(array(
                'returnQuery'	=> true,

                'fields' =>			"utdelingsorden.plassering\n",
                'source' => 		"{$tp}utdelingsorden AS utdelingsorden\n",
                'where'			=>	"{$tp}utdelingsorden.leieobjekt = '{$this->hent('regningsobjekt')}' AND rute = '{$rute}'"
            ));
            if( $posisjon->totalRows ) {
                $this->utskriftsposisjon[$rute] = 1000000 * $posisjon->data[0]->plassering
                + intval((string)$this->hent('leieforhold'));
            }
        }
    }


     /**
     * Oppretter en ny giro i databasen og tildeler egenskapene til dette objektet
     *
     * @param stdClass|array $egenskaper Alle egenskapene det nye objektet skal initieres med
     * @return bool|mixed
     * @throws Exception
     */
    public function opprett($egenskaper = array()) {
        throw new Exception('Bruk av utdatert metode ' . __METHOD__);
    }

    /**
     * Sett
     *
     * Sett en verdi
     *
     * @param string $egenskap
     * @param mixed $verdi
     * @return bool
     * @throws Exception
     * @deprecated
     * @see Regning::sett()
     */
    public function sett(string $egenskap, $verdi = null): bool
    {
        /** @var Regning $regning */
        $regning = $this->leiebase->hentModell(Regning::class, $this->id);
        if( !$this->id  || !$regning->hentId()) {
            return false;
        }

        switch( $egenskap ) {

        case "utskriftsdato":
            if ( $verdi && !($verdi instanceof DateTimeInterface) ) {
                $verdi = new DateTime( $verdi );
            }
            $regning->settUtskriftsdato($verdi);
            // Tving ny lasting av data:
            $this->data = null;
            $this->krav = null;

            return true;

        case "forfall":
            if ( $verdi && !($verdi instanceof DateTimeInterface) ) {
                $verdi = new DateTime( $verdi );
            }

            // Forfallsdato kan ikke nulles på giroer som allerede er skrevet ut
            if(!$verdi && $regning->hentUtskriftsdato()) {
                return false;
            }

            $regning->settForfall($verdi);
            // Tving ny lasting av data:
            $this->data = null;
            $this->krav = null;

            if(!$verdi && $regning->hentUtskriftsdato()) {
                return false;
            }

            // Tving ny lasting av data:
            $this->data = null;
            $this->krav = null;

            return true;

        case "format":
            $regning->settFormat($verdi);
            return true;

        default:
            return false;
        }

    }
}