<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 04/09/2020
 * Time: 16:11
 */

namespace Kyegil\Leiebasen;


class Respons
{
    /** @var array  */
    protected $headers = [];


    protected $innhold;

    /**
     * Respons constructor.
     *
     * @param $innhold
     */
    public function __construct($innhold) {
        $this->innhold = $innhold;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        foreach ($this->headers as $header) {
            header($header);
        }
        return $this->innholdSomStreng();
    }

    /**
     * @param array $headers
     * @return Respons
     */
    public function settHeaders(array $headers): Respons
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @param array $headers
     * @return Respons
     */
    public function header(string $header): Respons
    {
        $this->headers[] = $header;
        return $this;
    }

    /**
     * Sett innhold
     *
     * @param string $innhold
     * @return Respons
     */
    public function settInnhold(string $innhold): Respons
    {
        $this->innhold = $innhold;
        return $this;
    }

    /**
     * Hent innhold
     *
     * @return mixed
     */
    public function hentInnhold()
    {
        return $this->innhold;
    }

    /**
     * @return string
     */
    public function innholdSomStreng()
    {
        return strval($this->innhold);
    }
}