<?php



/**
 * Class Person
 *
 * @deprecated
 * @see \Kyegil\Leiebasen\Modell\Person
 *
 */
class Person extends DatabaseObjekt {

    /**
     * @var string
     */
    protected	$tabell = "personer";	// Hvilken tabell i databasen som inneholder primærnøkkelen for dette objektet
    /**
     * @var string
     */
    protected	$idFelt = "personid";	// Hvilket felt i tabellen som lagrer primærnøkkelen for dette objektet
    /**
     * @var
     */
    protected	$data;			// DB-verdiene lagret som et objekt. Null om de ikke er lastet
    //	@property string $navn Navn på leietakeren(e) som inngår i leieavtalen
    //	protected	$kortnavn;		// Forkortet navn på leietakeren(e)
    //	protected	$adresse;		// stdClass-objekt med adresseelementene
    //	protected	$adressefelt;	// Adressefelt for utskrift
    //	protected	$brukerepost;	// Liste over brukerepostadresser
    //	protected	$oppsigelse;	// stdClass-objekt med oppsigelse. False dersom ikke oppsagt, null dersom ikke lastet
    //	protected	$utskriftsposisjon = array();	// Utskriftsposisjonen for personen
    /**
     * @var
     */
    protected	$leieforhold;		// Array med datoer, hvor hver dato inneholder alle leieforholdene personen har inngått i. Null dersom leieforholdene ikke er lastet
    /**
     * @var
     */
    public		$id;				// Unikt id-heltall for dette objektet


    /**
     * Person constructor.
     * @param null|array|\stdClass $param
     *  id integer personid'en
     * @throws Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Leiebase::hentModell()
     */
    public function __construct($param = null ) {
        parent::__construct( $param );
    }


    /**
     * Last personens kjernedata fra databasen
     *
     * @param int $id
     * @return bool
     * @throws Exception
     */
    protected function last($id = 0) {
        $tp = $this->mysqli->table_prefix;

        settype($id, 'integer');
        if( !$id ) {
            $id = $this->id;
        }

        $resultat = $this->mysqli->arrayData(array(
            'returnQuery'	=> true,

            'fields' =>		"{$this->tabell}.personid AS id,\n"
                        .	"{$this->tabell}.personid,\n"
                        .	"{$this->tabell}.fornavn,\n"
                        .	"{$this->tabell}.etternavn,\n"
                        .	"{$this->tabell}.er_org,\n"
                        .	"{$this->tabell}.fødselsdato,\n"
                        .	"{$this->tabell}.personnr,\n"
                        .	"{$this->tabell}.adresse1,\n"
                        .	"{$this->tabell}.adresse2,\n"
                        .	"{$this->tabell}.postnr,\n"
                        .	"{$this->tabell}.poststed,\n"
                        .	"{$this->tabell}.land,\n"
                        .	"{$this->tabell}.telefon,\n"
                        .	"{$this->tabell}.mobil,\n"
                        .	"{$this->tabell}.epost\n",

            'source' => 		"{$tp}{$this->tabell} AS {$this->tabell}\n",

            'where'			=>	"{$tp}{$this->tabell}.{$this->idFelt} = '$id'"
        ));
        if( $resultat->totalRows ) {
            $this->data = $resultat->data[0];
            $this->id = $id;

            $this->data->postadresse
                = ( $this->data->adresse1 ? "{$this->data->adresse1}\n" : "")
                . ( $this->data->adresse2 ? "{$this->data->adresse2}\n" : "")
                . "{$this->data->postnr} {$this->data->poststed}"
                . ( ($this->data->land and $this->data->land != "Norge") ? "\n{$this->data->land}" : "");

            $this->data->fødselsnummer = null;
            $this->data->orgNr = null;

            if( $this->data->fødselsdato ) {
                $this->data->fødselsdato = new DateTime( $this->data->fødselsdato );
            }

            $this->data->org = (bool)$this->data->er_org;

            if( $this->data->org ) {
                $this->data->navn = $this->data->etternavn;
                $this->data->orgNr = $this->data->personnr;
            }
            else {
                if( $this->data->fornavn ) {
                    $this->data->navn = $this->data->fornavn . " " . $this->data->etternavn;
                }
                else {
                    $this->data->navn = $this->data->etternavn;
                }

                if( $this->data->fødselsdato && $this->data->personnr ) {
                    $this->data->fødselsnummer = $this->data->fødselsdato->format('dmy') . $this->data->personnr;
                }
            }

            return true;
        }
        else {
            $this->id = null;
            $this->data = null;
            return false;
        }

    }


    /**
     * Last personens leieforhold fra databasen
     *
     * @throws Exception
     */
    protected function lastLeieforhold() {
        $tp = $this->mysqli->table_prefix;

        $id = (int)$this->id;
        $this->leieforhold = array();

        foreach( $this->mysqli->arrayData(array(
            'returnQuery'	=> true,
            'distinct'		=> true,

            'fields'		=> "MAX(kontraktpersoner.slettet) AS slettet, MIN(kontrakter.fradato) AS fradato, kontrakter.leieforhold AS leieforhold, oppsigelser.fristillelsesdato AS fristillelsesdato",

            'groupfields'		=> "kontrakter.leieforhold",

            'source'		=> "{$tp}kontrakter AS kontrakter\n"
                            .	"INNER JOIN {$tp}kontraktpersoner AS kontraktpersoner ON kontraktpersoner.kontrakt = kontrakter.kontraktnr\n"
                            .	"LEFT JOIN {$tp}oppsigelser AS oppsigelser ON kontrakter.leieforhold = oppsigelser.leieforhold",

            'where'			=>	"kontraktpersoner.person = '$id'"
        ))->data as $leieforhold ) {
            $leieforhold->leieforhold = $this->leiebase->hent(\Leieforhold::class, $leieforhold->leieforhold);
            $leieforhold->fradato = new DateTime( $leieforhold->fradato );
            if( $leieforhold->slettet ) {
                $leieforhold->tildato = new DateTime( $leieforhold->slettet );
            }
            else if( $leieforhold->fristillelsesdato ) {
                $leieforhold->tildato = new DateTime( $leieforhold->fristillelsesdato );
            }
            else{
                $leieforhold->tildato = null;
            }

            unset($leieforhold->slettet, $leieforhold->fristillelsesdato);
            $this->leieforhold[] = $leieforhold;
        }
    }


    /**
     * Hent egenskaper
     *
     * @param string $egenskap
     * @param null|array $param
     * @return mixed|null
     * @throws Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Person::hent()
     */
    public function hent($egenskap, $param = array()) {
        $tp = $this->mysqli->table_prefix;

        if( !$this->id ) {
            return null;
        }

        switch( $egenskap ) {

        case "id":
        case "adresse1":
        case "adresse2":
        case "epost":
        case "etternavn":
        case "fødselsdato":
        case "fødselsnummer":
        case "fornavn":
        case "land":
        case "mobil":
        case "navn":
        case "org":
        case "orgNr":
        case "personid":
        case "personnr":
        case "postadresse":
        case "postnr":
        case "poststed":
        case "telefon":
        {
            if ( $this->data === null and !$this->last() ) {
                return null;
            }
            return $this->data->$egenskap;
        }

        default: {
            return null;
        }

        }

    }


    /**
     * Sjekker om denne personen eller organisasjonen er eller var leietaker hos utleier på en gitt dato
     *
     * @param \DateTime $dato
     * @return array
     * @throws Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Person::hentLeieforhold()
     */
    public function hentLeieforhold($dato = null ) {
        $resultat = array();

        if ( $dato !== null and !is_a($dato, 'DateTime') ) {
            throw new Exception('Forventet argument $dato er ikke DateTime-objekt: ' . print_r($dato, true));
        }
        if ( !is_array( $this->leieforhold ) ) {
            $this->lastLeieforhold();
        }

        foreach( $this->leieforhold as $leieforhold ) {
            if(
                $dato === null
                or (
                    $leieforhold->fradato <= $dato
                    and (
                        $leieforhold->tildato === null
                        or $leieforhold->tildato >= $dato
                    )
                )
            ) {
                $resultat[] = $leieforhold->leieforhold;
            }
        }

        return $resultat;
    }


    /**
     * Oppretter et nytt adressekort i databasen og tildeler egenskapene til dette objektet
     *
     * @param \stdClass|array $egenskaper
     * @return void
     * @throws Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Person::opprett()
     */
    public function opprett($egenskaper = array()) {
        throw new Exception('Bruk av utdatert metode ' . __METHOD__);
    }


    /**
     * Skriv en verdi
     *
     * @param string $egenskap Egenskapen som skal endres
     * @param mixed $verdi Ny verdi
     * @return bool suksessangivelse
     * @throws Exception
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Person::sett()
     */
    public function sett($egenskap, $verdi = null) {
        $tp = $this->mysqli->table_prefix;

        if( !$this->id ) {
            return false;
        }

        if($egenskap == 'navn') {
            $egenskap = 'etternavn';
        }
        if($egenskap == 'org') {
            $egenskap = 'er_org';
        }
        if($egenskap == 'orgNr') {
            $egenskap = 'personnr';
        }
        if($egenskap == 'personnr') {
            $verdi = preg_replace('/[^0-9]+/', '', $verdi);
        }

        if($egenskap == 'fødselsdato' and !$verdi) {
            $verdi = null;
        }

        // Dersom personnummer er oppgitt med 11 siffer
        if($egenskap == 'personnr' and strlen($verdi) == 11) {
            $fødselsnr = substr($verdi, 0, 6);
            $personnr = substr($verdi, 6, 5);

            if( $fødselsdato = $this->hent('fødselsdato')) {
                if($fødselsdato->format('dmy') != $fødselsnr) {
                    return false;
                }
            }
            else {
                $this->sett('fødselsdato', date_create_from_format('dmy', $fødselsnr));
            }
        }

        switch( $egenskap ) {

        case "adresse1":
        case "adresse2":
        case "epost":
        case "etternavn":
        case "fødselsdato":
        case "fornavn":
        case "land":
        case "mobil":
        case "navn":
        case "er_org":
        case "orgNr":
        case "personnr":
        case "postnr":
        case "poststed":
        case "telefon":
            if ( $verdi instanceof DateTime ) {
                $verdi = $verdi->format('Y-m-d');
            }

            $resultat = $this->mysqli->saveToDb(array(
                'update'	=> true,
                'table'		=> "{$tp}{$this->tabell} as {$this->tabell}",
                'where'		=> "{$this->tabell}.{$this->idFelt} = '{$this->id}'",
                'fields'	=> array(
                    "{$this->tabell}.{$egenskap}"	=> $verdi
                )
            ))->success;

            // Tving ny lasting av data:
            $this->data = null;

            return $resultat;

        default:
            return false;
        }

    }

    /**
     * @param string $område
     * @param \Leieforhold|int|null $leieforhold
     * @return bool
     * @deprecated
     * @see \Kyegil\Leiebasen\Modell\Person::harAdgangTil()
     */
    public function harAdgangTil($område, $leieforhold = null)
    {
        $tp = $this->mysqli->table_prefix;
        if($område == 'sentral') {
            return true;
        }
        $filter = [
            'personid' => $this->hentId(),
            'adgang' => $område
        ];
        if(in_array($område, ['mine-sider'])) {
            $filter['leieforhold'] = (string)$leieforhold;
        }
        $spørring = $this->mysqli->arrayData([
            'source' => "{$tp}adganger AS adganger",
            'where' => $filter
        ]);

        return (bool)$spørring->totalRows;
    }
}