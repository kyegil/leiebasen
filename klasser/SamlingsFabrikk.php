<?php
    /**
     * * Part of kyegil/leiebasen
     * Created by Kyegil
     * Date: 12/06/2020
     * Time: 09:53
     */

    namespace Kyegil\Leiebasen;


    use Kyegil\CoreModel\CoreModelCollectionFactory;
    use Kyegil\CoreModel\Interfaces\CoreModelCollectionInterface;
    use Kyegil\CoreModel\Interfaces\CoreModelInterface;

    /**
     * Class SamlingsFabrikk
     * @package Kyegil\Leiebasen
     */
    class SamlingsFabrikk extends CoreModelCollectionFactory
    {
        /**
         * @param string|array $modell
         * @param array $di
         * @return Samling|Blanding
         */
        public static function lag($modell, $di) {
            if(is_array($modell)) {
                return new Blanding($modell, $di);
            }
            /** @var Samling $samling */
            $samling = parent::create($modell, $di);
            return $samling;
        }
    }