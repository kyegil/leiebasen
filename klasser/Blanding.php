<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 12/06/2020
 * Time: 14:02
 */

namespace Kyegil\Leiebasen;


use Kyegil\CoreModel\CoreModelCollection;
use Kyegil\CoreModel\Interfaces\CoreModelCollectionInterface;

class Blanding extends \Kyegil\CoreModel\MixedCollection implements Samling
{
    use Samlingstrekk;
}