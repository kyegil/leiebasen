<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 05/08/2021
 * Time: 07:27
 */

namespace Kyegil\Leiebasen;

/**
 *
 */
class Liste extends \Kyegil\ViewRenderer\ViewArray
{
    /**
     * @var string
     */
    protected $glue = ', ';

    /**
     * @return string
     */
    public function __toString(): string
    {
        if (count($this->items) < 2) {
            return parent::__toString();
        }
        $sisteElement = array_pop($this->items);
        $liste = parent::__toString();
        $this->items[] = $sisteElement;
        $liste = implode(' og ', [$liste, $sisteElement]);
        return $liste;
    }
}