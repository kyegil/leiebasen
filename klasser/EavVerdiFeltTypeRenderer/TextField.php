<?php

namespace Kyegil\Leiebasen\EavVerdiFeltTypeRenderer;

use Kyegil\Leiebasen\CoreModelImplementering;
use Kyegil\Leiebasen\EavVerdiVisningRenderer;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Verdi;
use Kyegil\Leiebasen\Visning\felles\html\form\Field;
use Kyegil\ViewRenderer\ViewInterface;
use stdClass;

class TextField extends EavVerdiVisningRenderer
{
    /**
     * @return string
     */
    public static function hentNavn(): string
    {
        return 'Tekstfelt';
    }

    /**
     * @return stdClass|null
     */
    public static function hentStandardOppsett(): ?stdClass
    {
        return (object)[
            'id' => null,
            'name' => null,
            'label' => null,
            'type' => null,
            'required' => null,
            'disabled' => null,
            'autocomplete' => null,
            'pattern' => null,
            'placeholder' => null,
            'step' => null,
            'value' => null,
        ];
    }

    /**
     * @param Verdi|null $verdiObjekt
     * @param stdClass|null $oppsett
     * @param Egenskap $egenskap
     * @param string $visningType
     * @param string $bruk
     * @return string|ViewInterface
     * @throws \Exception
     */
    public static function vis(
        ?Verdi    $verdiObjekt,
        ?stdClass $oppsett,
        Egenskap  $egenskap,
        string    $visningType,
        string    $bruk
    )
    {
        /** @var CoreModelImplementering $app */
        $app = $egenskap->hentApp();
        $oppsett = (array)$oppsett;
        $oppsett['name'] = $oppsett['name'] ?? $egenskap->hentKode();
        $oppsett['label'] = $oppsett['label'] ?? $egenskap->hentBeskrivelse();
        $oppsett['value'] = $verdiObjekt ? $verdiObjekt->hentStrengVerdi('verdi') : ($oppsett['value'] ?? null);
        return $app->vis(Field::class, $oppsett);
    }
}