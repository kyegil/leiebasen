<?php

namespace Kyegil\Leiebasen\Oppslag;

use DateInterval;
use Exception;
use Kyegil;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Personsett;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\drift\html\shared\body\Main;
use Kyegil\Leiebasen\Visning\drift\html\shared\Head;
use Kyegil\Leiebasen\Visning\drift\html\shared\Html;
use Kyegil\Leiebasen\Visning\felles\html\shared\body\main\ReturiBreadcrumbs;
use Kyegil\Leiebasen\Visning\felles\html\shared\body\main\ReturiElement;
use Kyegil\ViewRenderer\ViewArray;
use stdClass;

class DriftKontroller extends AbstraktKontroller
{
    /** @var string */
    public $tittel = "Drift";
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @inheritdoc */
    public array $område = ['område' => 'drift'];

    /**
     * @param string $oppslag
     * @param string|null $id
     * @param string|null $oppdrag
     * @param array $queryParams
     * @param string $område
     * @return string
     */
    public static function url(
        string $oppslag,
        ?string $id = null,
        ?string $oppdrag = null,
        array $queryParams = [],
        string $område = ''
    ): string {
        return parent::url($oppslag, $id, $oppdrag, $queryParams, 'drift');
    }

    /**
     * @param Leieobjekt|null $leieobjekt
     * @param string|null $tekst
     * @param array|null $klasser
     * @return string
     * @noinspection PhpDocMissingThrowsInspection
     */
    public static function lenkeTilLeieobjektKort(
        ?Leieobjekt $leieobjekt,
        ?string     $tekst = null,
        ?array      $klasser = null
    ): string {
        if(!$leieobjekt || !$leieobjekt->hentId()) {
            return '';
        }
        $tekst = $tekst ?? "Leieforhold {$leieobjekt}";
        $klasser = $klasser ?? ['inline-link', 'leieobjekt-kort'];
        return trim(new HtmlElement(
            'a',
            [
                'href' => static::url('leieobjekt-kort', $leieobjekt->hentId()),
                'class' => implode(' ', $klasser),
                'title' => $leieobjekt->hentBeskrivelse(),
            ],
            $tekst
        ));
    }

    /**
     * @param Leieforhold|null $leieforhold
     * @param string|null $tekst
     * @param array|null $klasser
     * @return string
     * @noinspection PhpDocMissingThrowsInspection
     */
    public static function lenkeTilLeieforholdKort(
        ?Leieforhold $leieforhold,
        ?string $tekst = null,
        ?array $klasser = null
    ): string {
        if(!$leieforhold || !$leieforhold->hentId()) {
            return '';
        }
        $tekst = $tekst ?? "Leieforhold {$leieforhold}";
        $klasser = $klasser ?? ['inline-link','leieforhold-kort'];
        return trim(new HtmlElement(
            'a',
            [
                'href' => static::url('leieforholdkort', $leieforhold->hentId()),
                'class' => implode(' ', $klasser),
                'title' => $leieforhold->hentBeskrivelse(),
            ],
            $tekst
        ));
    }

    /**
     * @param Krav|null $krav
     * @param string|null $tekst
     * @param array|null $klasser
     * @return string
     * @noinspection PhpDocMissingThrowsInspection
     */
    public static function lenkeTilKravKort(
        ?Krav   $krav,
        ?string $tekst = null,
        ?array  $klasser = null
    ): string {
        if(!$krav || !$krav->hentId()) {
            return '';
        }
        $tekst = $tekst ?? $krav->hentTekst();
        $klasser = $klasser ?? ['inline-link','krav-kort'];
        return trim(new HtmlElement(
            'a',
            [
                'href' => static::url('krav-kort', $krav->hentId()),
                'class' => implode(' ', $klasser),
                'title' => $krav->hentTekst(),
            ],
            $tekst
        ));
    }

    /**
     * @param Innbetaling|null $innbetaling
     * @param string|null $tekst
     * @param array|null $klasser
     * @return string
     * @noinspection PhpDocMissingThrowsInspection
     */
    public static function lenkeTilBetaling(
        ?Innbetaling $innbetaling,
        ?string      $tekst = null,
        ?array       $klasser = null
    ): string {
        if(!$innbetaling || !$innbetaling->hentId()) {
            return '';
        }
        $tekst = $tekst ?? 'Vis betaling';
        $title = $innbetaling->hentBeløp() < 0
            ? 'Utbetalt ' : 'Innbetalt ';
        $title .= $innbetaling->app->kr(abs($innbetaling->hentBeløp()), false);
        if(trim($betaler = $innbetaling->hentBetaler())) {
            $title .= $innbetaling->hentBeløp() < 0 ? ' til ' : ' fra ';
            $title .= $betaler;
        }
        $title .= ' den ' . $innbetaling->hentDato()->format('d.m.Y');
        $klasser = $klasser ?? ['inline-link','betaling-kort'];
        return trim(new HtmlElement(
            'a',
            [
                'href' => static::url('innbetalingskort', $innbetaling->hentId()),
                'class' => implode(' ', $klasser),
                'title' => $title,
            ],
            $tekst
        ));
    }

    /**
     * DriftKontroller constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = [])
    {
        parent::__construct($di, $config);
        $this->returi->setBaseUrl($this->http_host . '/drift/', 'Drift');
        $this->forberedHovedData();
    }

    /**
     * @return Respons
     */
    public function hentRespons()
    {
        if(!isset($this->respons)) {
            if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
                $this->returi->reset();
            }
            $this->returi->set(
                self::url($_GET['oppslag'] ?? '', $_GET['id'] ?? null, $_GET['oppdrag'] ?? null, $_GET),
                $this->tittel
            );

            /** @var Person $bruker */
            $bruker = $this->hoveddata['bruker'] ?? null;
            $brukermeny = $this->vis(Visning\felles\html\shared\body\Brukermeny::class, [
                'bruker' => $bruker
            ]);
            $varsler = ''; // $this->vis(Visning\drift\html\shared\body\Varsler::class);
            $hovedmeny = $this->vis(Visning\felles\html\shared\body\Hovedmeny::class, [
                'bruker' => $bruker,
                'logo' => '<h3><a href="' . static::url('', null, null,['returi' => 'default']) . '">Drift</a></h3>'
            ]);
            $this->respons = new Respons($this->vis( Html::class, [
                'head' => $this->vis(Head::class, [
                    'title' => $this->tittel,
                ]),
                'brukernavn' => $bruker->hentNavn(),
                'brukermeny' => $brukermeny,
                'hovedmeny' => $hovedmeny,
                'varsler' => $varsler,
                'header' => $this->vis(Visning\drift\html\shared\body\Header::class),
                'footer' => $this->vis(Visning\drift\html\shared\body\Footer::class),
                'main' => $this->vis(Main::class, [
                    'tittel' => $this->tittel,
                    'innhold' => $this->innhold(),
                    'breadcrumbs' => $this->visBreadcrumbs(),
                    'varsler' => $this->visVarsler(),
                    'knapper' => ''
                ]),
                'scripts' => $this->skript()
            ]));
        }
        return parent::hentRespons();
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function skrivHTML()
    {
        if( !$this->preHTML() ) {
            $this->responder404();
            return false;
        }
        $html = $this->hentRespons();
        echo $html;
        return true;
    }

    /**
     * @return string
     */
    public function innhold()
    {
        return '';
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     */
    public function visBreadcrumbs()
    {
        $elementer = new ViewArray();
        $sti = $this->returi->getTrace();
        /** @var bool $rot Det første elementet er rot */
        $rot = true;
        foreach($sti as $url) {
            $current = $this->returi->getCurrentLocation() == $url->url;
            if($url->title) {
                $elementer->addItem(
                    $this->vis(ReturiElement::class, [
                        'url' => $url->url,
                        'tittel' => $url->title,
                        'rot' => $rot,
                        'current' => $current
                    ])
                );
            }
            $rot = false;
        }
        return $this->vis(ReturiBreadcrumbs::class, [
            'crumbs' => $elementer
        ]);
    }

    /**
     * @return string
     */
    public function visTilbakeknapp()
    {
        $sti = $this->returi->getTrace();
        if(count($sti) > 1) {
            return new HtmlElement('a',
                [
                    'role' => 'button',
                    'href' => $this->returi->get()->url,
                    'title' => "Tilbake til {$this->returi->get()->title}",
                    'class' => "btn btn-secondary tilbakeknapp"
                ],
                'Tilbake'
            );
        }
        return '';
    }

    /**
     * @return string|ViewArray
     */
    public function visKnappRad() {
        return new ViewArray([$this->visTilbakeknapp()]);
    }

    /**
     * @return array
     */
    protected function visVarsler()
    {
        $varsler = [];
        foreach($this->hentAdvarsler(\Kyegil\Leiebasen\Leiebase::VARSELNIVÅ_ADVARSEL) as $advarsel) {
            $varsler[] = new HtmlElement('div', ['class' => 'advarsel'], $advarsel['oppsummering']);
        }
        foreach($this->hentAdvarsler(\Kyegil\Leiebasen\Leiebase::VARSELNIVÅ_ORIENTERING) as $påminnelse) {
            $varsler[] = new HtmlElement('div', ['class' => 'påminnelse'], $påminnelse['oppsummering']);
        }
        return $varsler;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        $this->hoveddata['bruker'] = $this->hentModell(Person::class, $this->bruker['id']);
        return parent::preHTML();
    }

    /**
     *
     */
    public function responder404()
    {
        $this->respons = new Respons($this->vis( Html::class, [
            'head' => $this->vis(Head::class, [
                'title' => 'Ugyldig oppslag',
            ]),
            'main' => [new HtmlElement('h1', [], 'Http Feilkode 404'), new HtmlElement('p', [], 'Adressen du har oppgitt er ugyldig. Oppslaget finnes ikke.')]
        ]));
        $protocol = isset($_SERVER["SERVER_PROTOCOL"]) ? $_SERVER["SERVER_PROTOCOL"] : 'HTTP/1.1';
        $this->respons->header($protocol . ' 404 Not Found');
        die($this->hentRespons());
    }

    /**
     * @param string|null $oppslag
     * @return array
     */
    public function hentAktiveMenyElementer(?string $oppslag = null): array
    {
        $oppslag = $oppslag ?? ($_GET['oppslag'] ?? null);

        $menyElementMapping = [
            'adgang_liste'  => ['drift-meny', 'adgang_liste'],
            'adgang_skjema'  => ['drift-meny', 'adgang_liste', 'adgang_skjema'],
            'adresse_liste'  => ['leieforhold-meny', 'adresse_liste'],
            'avtalemal_skjema'  => ['driftmeny', 'innstillinger'],
            'betaling_skjema'  => ['innbetalinger-meny', 'betaling_skjema'],
            'bygning_skjema'  => ['leieobjekter-meny', 'bygningsliste', 'bygning_skjema'],
            'bygningsliste'     => ['leieobjekter-meny', 'bygningsliste'],
            'delkrav_skjema'  => ['driftmeny', 'innstillinger'],
            'delte_kostnader_fordelingselement_skjema'     => ['eksterne-tjenester-meny', 'delte_kostnader_tjenesteliste', 'delte_kostnader_fordelingselement_skjema'],
            'delte_kostnader_fordelingsnøkkel_skjema'     => ['eksterne-tjenester-meny', 'delte_kostnader_tjenesteliste', 'delte_kostnader_fordelingsnøkkel_skjema'],
            'delte_kostnader_kostnad_arkiv'     => ['eksterne-tjenester-meny', 'delte_kostnader_kostnad_arkiv'],
            'delte_kostnader_kostnad_import'     => ['eksterne-tjenester-meny', 'delte_kostnader_kostnad_liste', 'delte_kostnader_kostnad_import'],
            'delte_kostnader_kostnad_kort'     => ['eksterne-tjenester-meny', 'delte_kostnader_kostnad_liste', 'delte_kostnader_kostnad_kort'],
            'delte_kostnader_kostnad_liste'     => ['eksterne-tjenester-meny', 'delte_kostnader_kostnad_liste'],
            'delte_kostnader_tjeneste_kort'     => ['eksterne-tjenester-meny', 'delte_kostnader_tjenesteliste', 'delte_kostnader_tjeneste_kort'],
            'delte_kostnader_tjeneste_skjema'     => ['eksterne-tjenester-meny', 'delte_kostnader_tjenesteliste', 'delte_kostnader_tjeneste_skjema'],
            'delte_kostnader_tjenesteliste'     => ['eksterne-tjenester-meny', 'delte_kostnader_tjenesteliste'],
            'efakturaliste'     => ['leieforhold-meny', 'efakturaliste'],
            'eksport'     => ['rapporter-meny', 'eksport'],
            'massemelding_skjema'     => ['leieforhold-meny', 'massemelding_skjema'],
            'fbo-kravliste'     => ['innbetalinger-meny', 'fbo-meny', 'fbo-kravliste'],
            'fboliste'          => ['innbetalinger-meny', 'fbo-meny', 'fboliste'],
            'forsiden'          => ['drift-meny', 'forsiden'],
            'framleie_liste'     => ['leieforhold-meny', 'framleie_liste'],
            'framleie_nye_personer_skjema'     => ['leieforhold-meny', 'framleie_liste'],
            'framleie_skjema'     => ['leieforhold-meny', 'framleie_liste'],
            'fs_anlegg_kort'     => ['fs_anlegg_kort'],
            'fs_anlegg_liste'     => ['eksterne-tjenester-meny', 'fellesstrøm-meny', 'fs_anlegg_liste'],
            'fs_anlegg_skjema'     => ['eksterne-tjenester-meny', 'fellesstrøm-meny', 'fs_anlegg_liste'],
            'innbetaling_liste' => ['innbetalinger-meny', 'innbetaling_liste'],
            'innbetalinger'     => ['innbetalinger-meny', 'innbetalinger'],
            'innbetalingskort'     => ['innbetalinger-meny', 'innbetaling_liste'],
            'innstillinger'     => ['drift-meny', 'innstillinger'],
            'internmeldinger_skjema'     => ['drift-meny', 'forsiden'],
            'kontrakt_opplasting_skjema'     => ['leieforhold-meny', 'leieavtaler-meny', 'leieforhold_liste'],
            'kontrakt_signert'     => ['leieforhold-meny', 'leieavtaler-meny', 'leieforhold_liste'],
            'kontrakt_tekst_kort'     => ['leieforhold-meny', 'leieavtaler-meny', 'leieforhold_liste'],
            'kontrakt_tekst_skjema'     => ['leieforhold-meny', 'leieavtaler-meny', 'leieforhold_liste'],
            'kontrakt_utskrift'     => ['leieforhold-meny', 'leieavtaler-meny', 'leieforhold_liste'],
            'krav-kort'     => ['leieforhold-meny', 'krav_liste'],
            'krav_liste'     => ['leieforhold-meny', 'krav_liste'],
            'kravimport'     => ['leieforhold-meny', 'kravimport'],
            'leieberegning_skjema'     => ['drift-meny', 'innstillinger'],
            'leieforhold_personer_skjema'     => ['leieforhold-meny', 'leieavtaler-meny', 'leieforhold_liste'],
            'leieforhold_kontoforløp_utskrift'     => ['leieforhold-meny', 'leieavtaler-meny', 'leieforhold_liste'],
            'leieforhold_regningsadresse'     => ['leieforhold-meny', 'leieavtaler-meny', 'leieforhold_liste'],
            'leieforhold_liste'     => ['leieforhold-meny', 'leieavtaler-meny', 'leieforhold_liste'],
            'leieforhold_brukerprofiler_skjema'     => ['leieforhold-meny', 'leieforhold_brukerprofiler_skjema'],
            'leieforholdfilter'     => ['leieforhold-meny', 'massemelding_skjema', 'leieforholdfilter'],
            'leieforholdkort'     => ['leieforhold-meny', 'leieavtaler-meny', 'leieforhold_liste'],
            'leieforhold-skjema'     => ['leieforhold-meny', 'leieavtaler-meny', 'leieforhold_liste'],
            'leieobjekt-kort'     => ['leieobjekter-meny', 'leieobjekt_liste'],
            'leieobjekt_liste'     => ['leieobjekter-meny', 'leieobjekt_liste'],
            'leieobjekt_skjema'     => ['leieobjekter-meny', 'leieobjekt_liste'],
            'leieregulering_liste'     => ['leieforhold-meny', 'leieregulering_liste'],
            'leieregulering_skjema'     => ['leieforhold-meny', 'leieregulering_liste'],
            'ocr_fil'     => ['innbetalinger-meny', 'ocr_liste'],
            'ocr_liste'     => ['innbetalinger-meny', 'ocr_liste'],
            'område_liste'     => ['leieobjekter-meny', 'område_liste'],
            'område_skjema'     => ['leieobjekter-meny', 'område_liste'],
            'oppsigelse_liste'     => ['leieforhold-meny', 'oppsigelse_liste'],
            'oppsigelsesskjema'     => ['leieforhold-meny', 'oppsigelse_liste'],
            'oversikt-utleietabell'     => ['rapporter-meny', 'oversikt-utleietabell'],
            'oversikt_bygningsinntekter'     => ['rapporter-meny', 'oversikt_bygningsinntekter'],
            'oversikt_delkrav'     => ['rapporter-meny', 'oversikt_innbetalinger'],
            'oversikt_gjeld'     => ['leieforhold-meny', 'oversikt_gjeld'],
            'oversikt_husleiekrav'     => ['rapporter-meny', 'rapporter-denne-måneds', 'oversikt_husleiekrav'],
            'oversikt_innbetalinger'     => ['rapporter-meny', 'oversikt_innbetalinger'],
            'oversikt_kontobevegelser'     => ['rapporter-meny', 'rapporter-denne-måneds', 'oversikt_kontobevegelser'],
            'oversikt_krav'     => ['rapporter-meny', 'rapporter-denne-måneds', 'oversikt_krav'],
            'leieobjekter-ledige'     => ['leieobjekter-meny', 'leieobjekter-ledige'],
            'oversikt_utløpte'     => ['leieforhold-meny', 'leieavtaler-meny', 'oversikt_utløpte'],
            'personadresser_kort'     => ['leieforhold-meny', 'adresse_liste'],
            'person_adresse_skjema'     => ['leieforhold-meny', 'adresse_liste'],
            'personadresser_utskrift'     => ['leieforhold-meny', 'adresse_liste'],
            'poeng_liste'     => ['rapporter-meny', 'poeng_liste'],
            'poeng_skjema'     => ['rapporter-meny', 'poeng_liste'],
            'profil_passord_skjema'     => ['drift-meny', 'adgang_liste'],
            'profil-skjema'     => ['drift-meny', 'adgang_liste'],
            'rapport-anvendte-gironummer'     => ['rapporter-meny', 'rapport-anvendte-gironummer'],
            'rapport_avstemming'     => ['rapporter-meny', 'rapport_avstemming'],
            'rapport_gjeldsnedbetaling'     => ['rapporter-meny', 'rapport_gjeldsnedbetaling'],
            'rapport_kontoutskrift_leieforhold'     => ['rapporter-meny', 'rapport_kontoutskrift_leieforhold'],
            'rapport_regnskap'     => ['rapporter-meny', 'rapport_regnskap'],
            'regnskapsprosjekt_kort'     => ['rapporter-meny', 'regnskapsprosjekter_liste'],
            'regnskapsprosjekt_skjema'     => ['rapporter-meny', 'regnskapsprosjekter_liste'],
            'regnskapsprosjekter_liste'     => ['rapporter-meny', 'regnskapsprosjekter_liste'],
            'skade_skjema'     => ['leieobjekter-meny', 'skadeliste'],
            'skade_utbedring_skjema'     => ['leieobjekter-meny', 'skadeliste'],
            'skade_utbedringskort'     => ['leieobjekter-meny', 'skadeliste'],
            'skadekort'     => ['leieobjekter-meny', 'skadeliste'],
            'skadeliste'     => ['leieobjekter-meny', 'skadeliste'],
            'statistikk_gjeldshistorikk'     => ['rapporter-meny', 'statistikk_gjeldshistorikk'],
            'statistikk_innbetalinger'     => ['rapporter-meny', 'statistikk_innbetalinger'],
            'statistikk_ubetalt'     => ['rapporter-meny', 'statistikk_ubetalt'],
            'tilbakemelding_skjema'     => ['drift-meny', 'tilbakemelding_skjema'],
            'tilpasset_eav_felt_liste'     => ['drift-meny', 'tilpasset_eav_felt_liste'],
            'tilpasset_eav_felt_skjema'     => ['drift-meny', 'tilpasset_eav_felt_liste', 'tilpasset_eav_felt_skjema'],
            'utskriftsveiviser'     => ['leieforhold-meny', 'utskriftsveiviser'],
            'utskriftsveiviser_purringer'     => ['leieforhold-meny', 'utskriftsveiviser'],
            'utskriftsveiviser_regninger'     => ['leieforhold-meny', 'utskriftsveiviser'],
        ];
        return $menyElementMapping[$oppslag] ?? [$oppslag];
    }

    /**
     * @param string $nivå
     * @return array
     * @throws Exception
     */
    public function hentAdvarsler(string $nivå): array
    {
        if(!isset($this->advarsler)) {
            parent::hentAdvarsler($nivå);

            $lagredeAdvarsler = json_decode($this->hentValg('advarsler_drift'), true) ?? new stdClass();
            foreach($lagredeAdvarsler as $advarselSett) {
                foreach ($advarselSett as $varslNivå => $varsler) {
                    $this->advarsler[$varslNivå] = array_merge($this->advarsler[$varslNivå], $varsler);
                }
            }
        }
        return $this->advarsler[$nivå] ?? [];
    }

    /**
     * Hent menystruktur for drift
     *
     * @param string $meny
     * @param Person|null $bruker
     * @return array|object[]|string[]
     * @throws Exception
     */
    public function hentMenyStruktur(string $meny = '', ?Person $bruker = null): array
    {
        extract($this->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        /** @var \Kyegil\Leiebasen\Modell\Person $bruker */
        $bruker = $bruker ?: $this->hentModell(Kyegil\Leiebasen\Modell\Person::class, $this->bruker['id']);
        if (!$bruker->hentId()) {
            $bruker = null;
        }

        $aktiveMenyElementer = $this->hentAktiveMenyElementer();
        $nåværendeElement = end($aktiveMenyElementer);

        switch($meny) {
            case 'brukermeny':
                $resultat = [
                    (object)[
                        'text' => 'Brukerprofil',
                        'url' => static::url('profil-skjema', $this->bruker['id'], null, ['returi' => 'default']),
                        'active' => in_array('profil-skjema', $aktiveMenyElementer),
                        'nåværende' => 'profil-skjema' === $nåværendeElement,
                    ],
                    (object)[
                        'id' => 'gotil',
                        'text' => 'Gå til',
                        'items' => [
                            (object)[
                                'text' => 'Oppfølging',
                                'url' => '/oppfølging/'
                            ],
                            (object)[
                                'text' => 'Mine sider',
                                'url' => '/mine-sider/index.php?returi=default'
                            ],
                        ],
                        'active' => in_array('gotil', $aktiveMenyElementer),
                    ],
                    (object)[
                        'text' => 'Logg av',
                        'url' => '/offentlig/index.php?oppslag=index&oppdrag=avslutt',
                        'extraClasses' => ['fas', 'fa-power-off'],
                    ],
                ];
                return $this->post($this, __FUNCTION__, $resultat, $args);
            default:

                $poengProgrammerMeny = (object)[
                    'text' => 'Poeng-programmer',
                    'items' => [],
                    'active' => in_array('poeng-programmer', $aktiveMenyElementer),
                ];

                foreach($this->hentPoengbestyrer()->hentAktiveProgram() as $program) {
                    $poengProgrammerMeny->items[] = (object)[
                        'text' => $program->hentNavn(),
                        'url' => self::url('poeng_liste', null, null, [
                            'kode' => $program->hentKode(),
                            'returi' => 'default',
                        ]),
                        'active' => in_array('poeng-program-' . $program->hentId(), $aktiveMenyElementer),
                        'nåværende' => 'poeng-program-' . $program->hentId() === $nåværendeElement,
                    ];
                }
                /** @var object $driftMeny */
                $driftMeny = (object)[
                    'id' => 'drift',
                    'extraClasses' => ['fas', 'fa-toolbox'],
                    'text' => 'Drift',
                    'active' => in_array('drift-meny', $aktiveMenyElementer),
                    'symbol' => null,
                    'items' => [
                        (object)[
                            'text' => 'Forsiden',
                            'url' => static::url('', null, null, ['returi' => 'default']),
                            'active' => in_array('forsiden', $aktiveMenyElementer),
                            'nåværende' => 'forsiden' === $nåværendeElement,
                        ],
                        '-',
                        (object)[
                            'text' => 'Adganger',
                            'url' => '/drift/index.php?oppslag=adgang_liste&returi=default',
                            'active' => in_array('adganger', $aktiveMenyElementer),
                        ],
                        '-',
                        (object)[
                            'text' => 'Innstillinger',
                            'url' => '/drift/index.php?oppslag=innstillinger&returi=default',
                            'active' => in_array('innstillinger', $aktiveMenyElementer),
                            'nåværende' => 'innstillinger' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Spesialtilpassede felter',
                            'url' => '/drift/index.php?oppslag=tilpasset_eav_felt_liste&returi=default',
                            'active' => in_array('tilpasset_eav_felt_liste', $aktiveMenyElementer),
                            'nåværende' => 'tilpasset_eav_felt_liste' === $nåværendeElement,
                        ],
                        '-',
                        (object)[
                            'text' => 'Brukerveiledning',
                            'url' => '/pub/dokumentasjon.pdf',
                            'active' => in_array('brukerveiledning', $aktiveMenyElementer),
                        ],
                        (object)[
                            'text' => 'Bugrapportering',
                            'url' => '/drift/index.php?oppslag=tilbakemelding_skjema&returi=default',
                            'active' => in_array('tilbakemelding_skjema', $aktiveMenyElementer),
                            'nåværende' => 'tilbakemelding_skjema' === $nåværendeElement,
                        ],
                        '-',
                        (object)[
                            'text' => 'Logg av',
                            'url' => '/offentlig/index.php?oppslag=index&oppdrag=avslutt',
                            'active' => in_array('avslutt', $aktiveMenyElementer),
                        ],
                    ]
                ];

                /** @var object $leieforholdMeny */
                $leieforholdMeny = (object)[
                    'id' => 'leieforhold',
                    'text' => 'Leieforhold',
                    'extraClasses' => ['fas', 'fa-handshake'],
                    'symbol' => null,
                    'active' => in_array('leieforhold-meny', $aktiveMenyElementer),
                    'items' => [
                        (object)[
                            'text' => 'Leieavtaler',
                            'items' => [
                                (object)[
                                    'text' => 'Alle leieavtaler',
                                    'url' => '/drift/index.php?oppslag=leieforhold_liste&returi=default',
                                    'active' => in_array('leieforhold_liste', $aktiveMenyElementer),
                                    'nåværende' => 'leieforhold_liste' === $nåværendeElement,
                                ],
                                (object)[
                                    'text' => 'Leieavtaler som må fornyes',
                                    'url' => '/drift/index.php?oppslag=oversikt_utløpte&returi=default',
                                    'active' => in_array('oversikt_utløpte', $aktiveMenyElementer),
                                    'nåværende' => 'oversikt_utløpte' === $nåværendeElement,
                                ],
                                (object)[
                                    'text' => 'Opprett ny leieavtale',
                                    'url' => static::url('leieforhold-skjema', '*', null, ['returi' => 'default']),
                                    'active' => in_array('leieforhold-skjema', $aktiveMenyElementer),
                                    'nåværende' => 'leieforhold-skjema' === $nåværendeElement,
                                ],
                            ],
                            'active' => in_array('leieavtaler-meny', $aktiveMenyElementer),
                        ],
                        (object)[
                            'text' => 'Oppsigelser',
                            'url' => '/drift/index.php?oppslag=oppsigelse_liste&returi=default',
                            'active' => in_array('oppsigelse_liste', $aktiveMenyElementer),
                            'nåværende' => 'oppsigelse_liste' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Framleie',
                            'url' => '/drift/index.php?oppslag=framleie_liste&returi=default',
                            'active' => in_array('framleie_liste', $aktiveMenyElementer),
                            'nåværende' => 'framleie_liste' === $nåværendeElement,
                        ],
                        '-',
                        (object)[
                            'text' => 'Leie- og kravoversikt',
                            'url' => '/drift/index.php?oppslag=krav_liste&returi=default',
                            'active' => in_array('krav_liste', $aktiveMenyElementer),
                            'nåværende' => 'krav_liste' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Adresser',
                            'url' => '/drift/index.php?oppslag=adresse_liste&returi=default',
                            'active' => in_array('adresse_liste', $aktiveMenyElementer),
                            'nåværende' => 'adresse_liste' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'eFaktura',
                            'items' => [
                                (object)[
                                    'text' => 'Fakturaer',
                                    'url' => '/drift/index.php?oppslag=efakturaliste&returi=default',
                                    'active' => in_array('efakturaliste', $aktiveMenyElementer),
                                    'nåværende' => 'efakturaliste' === $nåværendeElement,
                                ],
                            ]
                        ],
                        (object)[
                            'text' => 'Gjeldsoversikt',
                            'url' => '/drift/index.php?oppslag=oversikt_gjeld&returi=default',
                            'active' => in_array('oversikt_gjeld', $aktiveMenyElementer),
                            'nåværende' => 'oversikt_gjeld' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Send masse-meldinger',
                            'url' => '/drift/index.php?oppslag=massemelding_skjema&returi=default',
                            'active' => in_array('massemelding_skjema', $aktiveMenyElementer),
                            'nåværende' => 'massemelding_skjema' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Oppfølging',
                            'url' => '/oppfølging/index.php',
                            'active' => in_array('oppfølging', $aktiveMenyElementer),
                            'nåværende' => 'oppfølging' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Opprett ny leieavtale',
                            'url' => static::url('leieforhold-skjema', '*', null, ['returi' => 'default']),
                            'active' => in_array('leieforhold-skjema', $aktiveMenyElementer),
                            'nåværende' => 'leieforhold-skjema' === $nåværendeElement,
                        ],
                        '-',
                        (object)[
                            'text' => 'Leieregulering',
                            'url' => '/drift/index.php?oppslag=leieregulering_liste&returi=default',
                            'active' => in_array('leieregulering_liste', $aktiveMenyElementer),
                            'nåværende' => 'leieregulering_liste' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Masse-import av krav',
                            'url' => '/drift/index.php?oppslag=kravimport&returi=default',
                            'active' => in_array('kravimport', $aktiveMenyElementer),
                            'nåværende' => 'kravimport' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Skriv ut giroer',
                            'url' => '/drift/index.php?oppslag=utskriftsveiviser&returi=default',
                            'active' => in_array('utskriftsveiviser', $aktiveMenyElementer),
                            'nåværende' => 'utskriftsveiviser' === $nåværendeElement,
                        ],
                    ]
                ];

                /** @var object $innbetalingerMeny */
                $innbetalingerMeny = (object)[
                    'id' => 'innbetalinger-meny',
                    'text' => 'Innbetalinger',
                    'symbol' => null,
                    'extraClasses' => ['fas', 'fa-money-bill'],
                    'items' => [
                        (object)[
                            'text' => 'Vis / Søk i innbetalinger',
                            'url' => '/drift/index.php?oppslag=innbetaling_liste&returi=default',
                            'active' => in_array('innbetaling_liste', $aktiveMenyElementer),
                            'nåværende' => 'innbetaling_liste' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Registrer betalinger',
                            'items' => [
                                (object)[
                                    'text' => 'Registrer ny betaling',
                                    'url' => '/drift/index.php?oppslag=betaling_skjema&id=*&returi=default',
                                    'active' => in_array('betaling_skjema', $aktiveMenyElementer),
                                    'nåværende' => 'betaling_skjema' === $nåværendeElement,
                                ],
                                (object)[
                                    'text' => 'Registrer/endre flere innbetalinger',
                                    'url' => '/drift/index.php?oppslag=innbetalinger&returi=default',
                                    'active' => in_array('innbetalinger', $aktiveMenyElementer),
                                    'nåværende' => 'innbetalinger' === $nåværendeElement,
                                ],
                            ]
                        ],
                        (object)[
                            'text' => 'Avstemming av innbetalinger mot betalingskrav',
                            'url' => '/drift/index.php?oppslag=utlikninger_skjema&returi=default',
                            'active' => in_array('utlikninger_skjema', $aktiveMenyElementer),
                            'nåværende' => 'utlikninger_skjema' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'OCR-filer',
                            'url' => '/drift/index.php?oppslag=ocr_liste&returi=default',
                            'active' => in_array('ocr_liste', $aktiveMenyElementer),
                            'nåværende' => 'ocr_liste' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Faste betalingsoppdrag',
                            'items' => [
                                (object)[
                                    'text' => 'Registrerte avtaler',
                                    'url' => '/drift/index.php?oppslag=fboliste&returi=default',
                                    'active' => in_array('fboliste', $aktiveMenyElementer),
                                    'nåværende' => 'fboliste' === $nåværendeElement,
                                ],
                                (object)[
                                    'text' => 'Trekkoppdrag for AvtaleGiro',
                                    'url' => '/drift/index.php?oppslag=fbo-kravliste&returi=default',
                                    'active' => in_array('fbo-kravliste', $aktiveMenyElementer),
                                    'nåværende' => 'fbo-kravliste' === $nåværendeElement,
                                ],
                            ],
                            'active' => in_array('fbo-meny', $aktiveMenyElementer),
                        ],
                        (object)[
                            'text' => 'Månedlige kontobevegelser (innbetalinger)',
                            'url' => '/drift/index.php?oppslag=oversikt_kontobevegelser&returi=default',
                            'active' => in_array('oversikt_kontobevegelser', $aktiveMenyElementer),
                            'nåværende' => 'oversikt_kontobevegelser' === $nåværendeElement,
                        ],
                    ],
                    'active' => in_array('innbetalinger-meny', $aktiveMenyElementer),
                ];

                $leieobjekterMeny = (object)[
                    'id' => 'leieobjekter-meny',
                    'text' => 'Leieobjekter',
                    'symbol' => null,
                    'extraClasses' => ['fas', 'fa-building'],
                    'items' => [
                        (object)[
                            'text' => 'Bygninger',
                            'url' => '/drift/index.php?oppslag=bygningsliste&returi=default',
                            'active' => in_array('bygningsliste', $aktiveMenyElementer),
                            'nåværende' => 'bygningsliste' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Leiligheter og lokaler',
                            'url' => '/drift/index.php?oppslag=leieobjekt_liste&returi=default',
                            'active' => in_array('leieobjekt_liste', $aktiveMenyElementer),
                            'nåværende' => 'leieobjekt_liste' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Ledige leiligheter',
                            'url' => static::url('leieobjekter-ledige', null, null, ['returi' => 'default']),
                            'active' => in_array('leieobjekter-ledige', $aktiveMenyElementer),
                            'nåværende' => 'leieobjekter-ledige' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Områder',
                            'url' => '/drift/index.php?oppslag=område_liste&returi=default',
                            'active' => in_array('område_liste', $aktiveMenyElementer),
                            'nåværende' => 'område_liste' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Skaderegister',
                            'url' => '/drift/index.php?oppslag=skadeliste&returi=default',
                            'active' => in_array('skadeliste', $aktiveMenyElementer),
                            'nåværende' => 'skadeliste' === $nåværendeElement,
                        ],
                    ],
                    'active' => in_array('leieobjekter-meny', $aktiveMenyElementer),
                ];

                $eksterneTjenesterMeny = (object)[
                    'id' => 'eksterne-tjenester-meny',
                    'text' => 'Eksterne tjenester',
                    'symbol' => null,
                    'extraClasses' => ['fas', 'fa-bolt'],
                    'items' => [
                        (object)[
                            'text' => 'Alle delte tjenester',
                            'url' => '/drift/index.php?oppslag=delte_kostnader_tjenesteliste&returi=default',
                            'active' => in_array('delte_kostnader_tjenesteliste', $aktiveMenyElementer),
                            'nåværende' => 'delte_kostnader_tjenesteliste' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Påbeløpte kostnader for fordeling',
                            'url' => '/drift/index.php?oppslag=delte_kostnader_kostnad_liste&returi=default',
                            'active' => in_array('delte_kostnader_kostnad_liste', $aktiveMenyElementer),
                            'nåværende' => 'delte_kostnader_kostnad_liste' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Arkiverte fordelinger',
                            'url' => '/drift/index.php?oppslag=delte_kostnader_kostnad_arkiv&returi=default',
                            'active' => in_array('delte_kostnader_kostnad_arkiv', $aktiveMenyElementer),
                            'nåværende' => 'delte_kostnader_kostnad_arkiv' === $nåværendeElement,
                        ],
                        '-',
                        (object)[
                            'text' => 'Fellesstrøm',
                            'items' => [
                                (object)[
                                    'text' => 'Anleggsoversikt med fordelingsnøkler',
                                    'url' => '/drift/index.php?oppslag=fs_anlegg_liste&returi=default',
                                    'active' => in_array('fs_anlegg_liste', $aktiveMenyElementer),
                                    'nåværende' => 'fs_anlegg_liste' === $nåværendeElement,
                                ],
                                (object)[
                                    'text' => 'Avstemming av faste fellesstrøm-krav',
                                    'url' => '/drift/index.php?oppslag=strømavstemming_skjema&returi=default',
                                    'active' => in_array('strømavstemming_skjema', $aktiveMenyElementer),
                                    'nåværende' => 'strømavstemming_skjema' === $nåværendeElement,
                                ],
                            ],
                            'active' => in_array('fellesstrøm-meny', $aktiveMenyElementer),
                        ],
                    ],
                    'active' => in_array('eksterne-tjenester-meny', $aktiveMenyElementer),
                ];

                $rapporterMeny = (object)[
                    'id' => 'rapporter-meny',
                    'text' => 'Rapporter og statistikk',
                    'symbol' => null,
                    'extraClasses' => ['fas', 'fa-chart-bar'],
                    'items' => [
                        (object)[
                            'text' => 'Utleietabell',
                            'url' => '/drift/index.php?oppslag=oversikt-utleietabell&returi=default',
                            'active' => in_array('oversikt-utleietabell', $aktiveMenyElementer),
                            'nåværende' => 'oversikt-utleietabell' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Månedsvis oppsummering',
                            'url' => '/drift/index.php?oppslag=oversikt_innbetalinger&returi=default',
                            'active' => in_array('oversikt_innbetalinger', $aktiveMenyElementer),
                            'nåværende' => 'oversikt_innbetalinger' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Denne måneds ...',
                            'items' => [
                                (object)[
                                    'text' => 'innbetalinger',
                                    'url' => '/drift/index.php?oppslag=oversikt_kontobevegelser&returi=default',
                                    'active' => in_array('oversikt_kontobevegelser', $aktiveMenyElementer),
                                    'nåværende' => 'oversikt_kontobevegelser' === $nåværendeElement,
                                ],
                                (object)[
                                    'text' => 'krav om betaling',
                                    'url' => '/drift/index.php?oppslag=oversikt_krav&returi=default',
                                    'active' => in_array('oversikt_krav', $aktiveMenyElementer),
                                    'nåværende' => 'oversikt_krav' === $nåværendeElement,
                                ],
                                (object)[
                                    'text' => 'husleiekrav',
                                    'url' => '/drift/index.php?oppslag=oversikt_husleiekrav&returi=default',
                                    'active' => in_array('oversikt_husleiekrav', $aktiveMenyElementer),
                                    'nåværende' => 'oversikt_husleiekrav' === $nåværendeElement,
                                ],

                            ],
                            'active' => in_array('rapporter-denne-måneds', $aktiveMenyElementer),
                        ],
                        (object)[
                            'text' => 'Inntekter per bygning',
                            'url' => '/drift/index.php?oppslag=oversikt_bygningsinntekter&returi=default',
                            'active' => in_array('oversikt_bygningsinntekter', $aktiveMenyElementer),
                            'nåværende' => 'oversikt_bygningsinntekter' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Diagrammer',
                            'items' => [
                                (object)[
                                    'text' => 'Utestående krav over tid',
                                    'url' => '/drift/index.php?oppslag=statistikk_gjeldshistorikk&returi=default',
                                    'active' => in_array('statistikk_gjeldshistorikk', $aktiveMenyElementer),
                                    'nåværende' => 'statistikk_gjeldshistorikk' === $nåværendeElement,
                                ],
                                (object)[
                                    'text' => 'Diagram over krav og innbetalinger',
                                    'url' => '/drift/index.php?oppslag=statistikk_innbetalinger&returi=default',
                                    'active' => in_array('statistikk_innbetalinger', $aktiveMenyElementer),
                                    'nåværende' => 'statistikk_innbetalinger' === $nåværendeElement,
                                ],
                            ]
                        ],
                        (object)[
                            'text' => 'Eksport av data',
                            'url' => '/drift/index.php?oppslag=eksport&returi=default',
                            'active' => in_array('eksport', $aktiveMenyElementer),
                            'nåværende' => 'eksport' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Rapporter',
                            'items' => [
                                (object)[
                                    'text' => 'Prosjektregnskaper',
                                    'url' => '/drift/index.php?oppslag=regnskapsprosjekter_liste&returi=default',
                                    'active' => in_array('regnskapsprosjekter_liste', $aktiveMenyElementer),
                                    'nåværende' => 'regnskapsprosjekter_liste' === $nåværendeElement,
                                ],
                                (object)[
                                    'text' => 'Regnskapsrapport',
                                    'url' => '/drift/index.php?oppslag=rapport_regnskap&returi=default',
                                    'active' => in_array('rapport_regnskap', $aktiveMenyElementer),
                                    'nåværende' => 'rapport_regnskap' === $nåværendeElement,
                                ],
                                (object)[
                                    'text' => 'Kontoforløp per leieforhold',
                                    'url' => '/drift/index.php?oppslag=rapport_kontoutskrift_leieforhold&returi=default',
                                    'active' => in_array('rapport_kontoutskrift_leieforhold', $aktiveMenyElementer),
                                    'nåværende' => 'rapport_kontoutskrift_leieforhold' === $nåværendeElement,
                                ],
                                (object)[
                                    'text' => 'Rapport over innbetalinger mot krav fra bestemt tidsrom',
                                    'url' => '/drift/index.php?oppslag=rapport_gjeldsnedbetaling&returi=default',
                                    'active' => in_array('rapport_gjeldsnedbetaling', $aktiveMenyElementer),
                                    'nåværende' => 'rapport_gjeldsnedbetaling' === $nåværendeElement,
                                ],
                                (object)[
                                    'text' => 'Rapport over utestående husleie på bestemt dato',
                                    'url' => '/drift/index.php?oppslag=rapport_avstemming&returi=default',
                                    'active' => in_array('rapport_avstemming', $aktiveMenyElementer),
                                    'nåværende' => 'rapport_avstemming' === $nåværendeElement,
                                ],
                                (object)[
                                    'text' => 'Oversikt over anvendte gironummerserier',
                                    'url' => '/drift/index.php?oppslag=rapport-anvendte-gironummer&returi=default',
                                    'active' => in_array('rapport-anvendte-gironummer', $aktiveMenyElementer),
                                    'nåværende' => 'rapport-anvendte-gironummer' === $nåværendeElement,
                                ],
                            ]
                        ],
                    ],
                    'active' => in_array('rapporter-meny', $aktiveMenyElementer),
                ];

                if($poengProgrammerMeny->items) {
                    array_splice($rapporterMeny->items, 5, 0, [$poengProgrammerMeny]);
                }

                $resultat = [
                    $driftMeny,
                    $leieforholdMeny,
                    $innbetalingerMeny,
                    $leieobjekterMeny,
                    $eksterneTjenesterMeny,
                    $rapporterMeny,
                ];
                return $this->post($this, __FUNCTION__, $resultat, $args);
        }
    }

    /**
     * @return DriftKontroller
     * @throws Exception
     */
    public function kontrollrutiner(): DriftKontroller
    {
        $this->kontrollerCron();
        $this->kontrollerNetsOppkopling();
        $this->kontrollerEpostkø();
        return parent::kontrollrutiner();
    }

    /**
     * Kontroller cron
     *
     * Kontrollrutinen ser om automatiske kronprosedyrer har blitt utført siste 24 timer
     */
    public function kontrollerCron()
    {
        if (($this->hentValg('cronsuksess') + 24 * 3600) < time()) {
            $this->settAdvarsel(Leiebase::VARSELNIVÅ_ADVARSEL, [
                'oppsummering' => 'Problemer med cron.',
                'tekst' => 'Det ser ikke ut til at automatiske cron-prosesser har blitt utført på over 24 timer.<br>Dette skyldes muligens en feilkonfigurering av serveren som leiebasen er installert på.<br>Kontakt med server-administrator for å sette opp automatisk cronjobb som bør kjøres ca hvert 5. minutt.'
            ]);
        }
        return $this;
    }

    /**
     * Kontrollrutinen ser om det har vært problemer med henting av OCR-fil
     */
    public function kontrollerNetsOppkopling(): DriftKontroller
    {
        if ($this->hentValg('OCR_feilmelding')) {
            $this->settAdvarsel(Leiebase::VARSELNIVÅ_ADVARSEL, [
                'oppsummering' => "Problemer med oppkopling mot NETS.",
                'tekst' => $this->hentValg('OCR_feilmelding')
            ]);
        }
        return $this;
    }

    /**
     * Kontrollrutinen ser etter eposter under utsendelse
     */
    public function kontrollerEpostkø()
    {
        /** @var Kyegil\Leiebasen\Modell\Meldingsett $epostsett */
        $epostsett = $this->hentSamling(Kyegil\Leiebasen\Modell\Melding::class);
        $epostsett->leggTilFilter(['medium' => \Kyegil\Leiebasen\Modell\Melding::MEDIUM_EPOST])->låsFiltre();
        $antallEposter = $epostsett->hentAntall();
        if ($antallEposter) {
            $this->settAdvarsel(Leiebase::VARSELNIVÅ_ORIENTERING, [
                'oppsummering' => "{$antallEposter} e-poster i kø.",
                'tekst' => "{$antallEposter} e-poster står for øyeblikket i kø for utsendelse."
            ]);
        }
    }
}