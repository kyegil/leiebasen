<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/07/2024
 * Time: 23:39
 */

namespace Kyegil\Leiebasen\Oppslag;

use Kyegil\Leiebasen\Oppslag\Eks\EksMottak;
use Kyegil\Leiebasen\Oppslag\Eks\EksOppdrag;
use Kyegil\Leiebasen\Oppslag\Eks\EksUtlevering;
use Kyegil\Leiebasen\Visning\drift\html\shared\Head;
use Kyegil\Leiebasen\Visning\drift\html\shared\Html;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class EksKontroller extends DriftKontroller
{
    /**
     * @return \Kyegil\Leiebasen\Respons
     */
    public function hentRespons()
    {
        $respons = parent::hentRespons();
        $innhold = $respons->hentInnhold();
        if ($innhold instanceof Html) {
            $htmlHead = $this->vis(Head::class, [
                'extPath' => '/pub/lib/' . $this->ext_bibliotek,
                'title' => $this->tittel,
            ]);
            $innhold->sett('head', $htmlHead);
        }
        return $respons;
    }

    /**
     * @param string $oppdrag
     * @return AbstraktOppdrag|null
     */
    public function hentOppdrag(string $oppdrag): ?AbstraktOppdrag
    {
        if(!isset($this->oppdrag[$oppdrag])) {
            $this->oppdrag[$oppdrag] = EksOppdrag::class;
        }
        return parent::hentOppdrag($oppdrag);
    }

    /**
     * @return AbstraktUtlevering|null
     */
    public function hentUtlevering(): ?AbstraktUtlevering
    {
        if(!isset($this->utlevering)) {
            $this->utlevering = new EksUtlevering($this, [
                'get' => $_GET,
                'post' => $_POST
            ]);
        }
        return parent::hentUtlevering();
    }

    /**
     * @return AbstraktMottak|null
     */
    public function hentMottak(): ?AbstraktMottak
    {
        if(!isset($this->mottak)) {
            $files = [];
            if (isset($_FILES)) {
                foreach ($_FILES as $fileName => $fileData) {
                    $files[$fileName] = $this->reArrayFiles($fileData);
                }
            }
            $this->mottak = new EksMottak($this, [
                'get' => $_GET,
                'post' => $_POST,
                'files' => $files
            ]);
        }
        return parent::hentMottak();
    }

    /**
     * @return string
     */
    public function innhold(){
        ob_start();
        $this->design();
        return ob_get_clean();
    }


    /**
     * @return string|ViewInterface|ViewArray
     */
    public function visKnappRad()
    {
        return '';
    }
}