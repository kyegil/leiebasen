<?php

namespace Kyegil\Leiebasen\Oppslag\MinSøknad;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\Head;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\Html;
use Kyegil\Leiebasen\Visning\offentlig\js\søknad_skjema\body\main\Index as Js;

class Index extends \Kyegil\Leiebasen\Oppslag\MinSøknadKontroller
{
    /** @var string */
    public $tittel = 'Min søknad';

    protected $tilgjengeligeSpråk = [
        'nb-NO' => 'Norsk (Bokmål)',
        'en' => 'English'
    ];

    /**
     * Index constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        if(isset($_GET['l'])) {
            $this->settSpråk($_GET['l']);
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Søknad $søknad */
        $søknad = $this->hoveddata['søknad'] ?? null;
        if (!$søknad || !$søknad->hentId()) {
            return false;
        }
        $språk = $this->hentSpråk();
        if ($språk == 'en') {
            $this->tittel = 'My application';
        }
        /** @var Html $innhold */
        $innhold = $this->hentRespons()->hentInnhold();
        $innhold->hent('header')->sett('språkvelger', $this->lagSpråkvelger());
        /** @var Head $htmlHead */
        $htmlHead = $innhold->getData('head');

        $htmlHead
            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]));

        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Respons
     */
    public function hentRespons()
    {
        $this->returi->reset();
        return parent::hentRespons();
    }

    /**
     * @return Visning
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Søknad\Adgang $adgang */
        $adgang = $this->hoveddata['søknad_adgang'] ?? null;
        /** @var Søknad|null $søknad */
        $søknad = $adgang ? $adgang->søknad : ($this->hoveddata['søknad'] ?? null);

        return $this->vis(\Kyegil\Leiebasen\Visning\min_søknad\html\index\Index::class, [
            'språkKode' => $this->hentSpråk(),
            'søknad' => $søknad,
            'søknadAdgang' => $adgang,
            'søknadSkjema' => $søknad ? $søknad->type : null
        ]);
    }

    /**
     * @return Js|string
     */
    public function skript()
    {
        /** @var Søknad\Adgang $adgang */
        $adgang = $this->hoveddata['søknad_adgang'] ?? null;
        /** @var Søknad|null $søknad */
        $søknad = $adgang ? $adgang->søknad : ($this->hoveddata['søknad'] ?? null);

        $språk = $this->hentSpråk();
        return $this->vis(Js::class, [
            'søknadSkjema' => $søknad ? $søknad->type : null,
            'språkKode' => $språk
        ]);
    }

    /**
     * @return HtmlElement|string
     */
    public function visTilbakeknapp()
    {
        /** @var HtmlElement $tilbakeKnapp */
        return parent::visTilbakeknapp();
    }
}