<?php

namespace Kyegil\Leiebasen\Oppslag\MinSøknad\Index;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Personsett;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Visning\felles\epost\shared\Epost;
use stdClass;

class Mottak extends AbstraktMottak
{
    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Søknaden er oppdatert."
        ];
        $get = $_GET;
        $post = $_POST;
        $files = $_FILES;
        $skjema = isset($get['skjema']) ? $get['skjema'] : '';
        try {
            switch ($skjema) {
                case 'søknad':
                    $språk = $this->app->hentSpråk();
                    /** @var Søknad $søknad */
                    $søknad = $this->app->hoveddata['søknad'] ?? null;
                    $kontaktpersonFornavn = $post['kontaktperson']['fornavn'] ?? null;
                    $kontaktpersonEtternavn = $post['kontaktperson']['etternavn'] ?? null;
                    $kontaktAdresse1 = $post['kontaktperson']['adresse1'] ?? null;
                    $kontaktAdresse2 = $post['kontaktperson']['adresse2'] ?? null;
                    $kontaktPostnr = $post['kontaktperson']['postnr'] ?? null;
                    $kontaktPoststed = $post['kontaktperson']['poststed'] ?? null;
                    $kontaktTelefon = $post['kontaktperson']['telefon'] ?? null;
                    $kontaktEpost = $post['kontaktperson']['epost'] ?? null;

                    $this->validerKontaktInfo(
                        $kontaktpersonEtternavn,
                        $kontaktpersonFornavn,
                        $kontaktTelefon,
                        $kontaktEpost,
                        $kontaktAdresse1,
                        $kontaktAdresse2,
                        $kontaktPostnr,
                        $kontaktPoststed,
                        $språk
                    );

                    if ($språk == 'en') {
                        $resultat->msg = 'Your application has been updated';
                    }

                    $skjemaData = $post['skjemadata'] ?? [];
                    $søknadsTekst = $skjemaData['søknadstekst'] ?? null;

                    if (!trim(strip_tags($søknadsTekst))) {
                        throw new Exception(
                            $språk == 'en'
                                ? 'Presentation is required'
                                : 'Søknadsteksten mangler'
                        );
                    }

                    $metaData = $søknad->hentMetaData();
                    settype($metaData, 'object');
                    $metaData->konfigurering = $søknad->type->hentKonfigurering();
                    $søknad->settSøknadstekst($søknadsTekst)
                        ->sett(Søknad\Type::FELT_KONTAKT_FORNAVN, $kontaktpersonFornavn)
                        ->sett(Søknad\Type::FELT_KONTAKT_ETTERNAVN, $kontaktpersonEtternavn)
                        ->sett('kontakt_adresse1', $kontaktAdresse1)
                        ->sett('kontakt_adresse2', $kontaktAdresse2)
                        ->sett('kontakt_postnr', $kontaktPostnr)
                        ->sett('kontakt_postnr', $kontaktPostnr)
                        ->sett(Søknad\Type::FELT_KONTAKT_TELEFON, $kontaktTelefon)
                        ->sett(Søknad\Type::FELT_KONTAKT_EPOST, $kontaktEpost)
                        ->settMetaData($metaData)
                        ->settAktiv(true)
                        ->settOppdatert(new DateTime())
                        ;

                    $skjemafeltsett = $søknad->type->hentFeltsett();
                    foreach ($skjemafeltsett as $skjemafelt) {
                        $skjemafeltKode = $skjemafelt->hentKode();
                        if (isset($skjemaData[$skjemafeltKode])) {
                            $søknad->sett($skjemafeltKode, $skjemaData[$skjemafeltKode]);
                        }
                    }

                    $layout = $søknad->type->hentKonfigurering('layout');
                    if(is_iterable($layout)) {
                        foreach ($layout as $layoutElement) {
                            if($layoutElement->autofilter ?? null) {
                                $søknad->lagAutoAdminFilter($layoutElement);
                            }
                        }
                    }

                    $resultat->url = strval($this->app->returi->get(1));

                    try {
                        $this->sendEpostNotifikasjon($søknad);
                    } catch (Exception $e) {
                        $this->app->logger->critical($e->getMessage(), $e->getTrace());
                    }

                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param $epost
     * @return $this
     * @throws Exception
     */
    protected function validerEpost($epost): Mottak
    {
        if($epost && !filter_var($epost, FILTER_VALIDATE_EMAIL)) {
            throw new Exception('Ugyldig epostadresse');
        }
        return $this;
    }

    /**
     * @param string|null $kontaktpersonEtternavn
     * @param string|null $kontaktpersonFornavn
     * @param string|null $kontaktTelefon
     * @param string|null $kontaktEpost
     * @param string|null $kontaktAdresse1
     * @param string|null $kontaktAdresse2
     * @param string|null $kontaktPostnr
     * @param string|null $kontaktPoststed
     * @param string|null $språk
     * @return Mottak
     * @throws Exception
     */
    private function validerKontaktInfo(
        ?string $kontaktpersonEtternavn,
        ?string $kontaktpersonFornavn,
        ?string $kontaktTelefon,
        ?string $kontaktEpost,
        ?string $kontaktAdresse1,
        ?string $kontaktAdresse2,
        ?string $kontaktPostnr,
        ?string $kontaktPoststed,
        ?string $språk = null
    ): Mottak
    {
        if (!trim($kontaktEpost) && !trim($kontaktTelefon)) {
            throw new Exception(
                $språk == 'en'
                    ? 'Either email or phone number is required'
                    : 'Enten telefonnummer eller epost må oppgis'
            );

        }
        return $this;
    }

    /**
     * @param Søknad $søknad
     * @return Mottak
     * @throws Exception
     */
    private function sendEpostNotifikasjon(
        Søknad $søknad
    ): Mottak
    {
        $søknadType = $søknad->type;
        $avsenderEpost = $søknadType->hentKonfigurering('avsenderEpost') ?? null;
        $notifikasjonsMottakere = $this->hentNotifikasjonsMottakere();
        foreach ($notifikasjonsMottakere as $mottaker) {
            try {
                $this->app->sendMail((object)[
                    'to' => "{$mottaker->hentNavn()}<{$mottaker->hentEpost()}>",
                    'subject' => 'Oppdatering av søknad – ' . $søknad->type->hentNavn(),
                    'reply' => $avsenderEpost,
                    'html' => $this->app->hentVisning(Epost::class, [
                        'tittel' => 'Oppdatering av søknad',
                        'innhold' => new HtmlElement('div', [], [
                            $søknad->type->hentNavn(),
                            'Dette er en melding om at en søknad har blitt oppdatert.',
                            new HtmlElement('a', [
                                'class' => 'button',
                                'href' => $this->app->http_host . '/flyko/index.php?oppslag=søknad_liste&søknad_type=' . $søknadType . '&returi=default'
                            ],
                                'Klikk her for å se søknaden')
                        ]),
                        'bunntekst' => ''
                    ]),
                    'priority' => 100,
                    'type' => 'søknadsskjema_notifikasjon'
                ]);
            } catch (Exception $e) {
                $this->app->logger->critical($e->getMessage(), $e->getTrace());
            }
        }
        return $this;
    }

    /**
     * @return Personsett
     * @throws Exception
     */
    private function hentNotifikasjonsMottakere(): Personsett
    {
        $this->app->before($this, __FUNCTION__, []);
        /** @var Personsett $mottakere */
        $mottakere = $this->app->hentSamling(Person::class);
        $mottakere->leggTilInnerJoin(Person\Adgang::hentTabell(),
            '`' . Person::hentTabell(). '`.`' . Person::hentPrimærnøkkelfelt(). '` = `' . Person\Adgang::hentTabell(). '`.`personid`'
        );
        $mottakere->leggTilFilter(['`' . Person\Adgang::hentTabell(). '`.`adgang`' => Person\Adgang::ADGANG_FLYKO]);
        $mottakere->leggTilFilter(['`' . Person\Adgang::hentTabell(). '`.`epostvarsling`' => true]);
        $mottakere->leggTilFilter(['`' . Person::hentTabell(). '`.`epost` <>' => '']);
        $mottakere->låsFiltre();
        return $this->app->after($this, __FUNCTION__, $mottakere);
    }
}