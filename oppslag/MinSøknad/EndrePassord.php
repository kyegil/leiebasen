<?php

namespace Kyegil\Leiebasen\Oppslag\MinSøknad;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Modell\Søknad\Adgang;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\Head;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\Html;

class EndrePassord extends \Kyegil\Leiebasen\Oppslag\MinSøknadKontroller
{
    /** @var string */
    public $tittel = 'Endre passord for søknad';

    protected $tilgjengeligeSpråk = [
        'nb-NO' => 'Norsk (Bokmål)',
        'en' => 'English'
    ];

    /**
     * Oversikt constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        if(isset($_GET['l'])) {
            $this->settSpråk($_GET['l']);
        }
        if(isset($_GET['ref']) && isset($_GET['token'])) {
            $this->settNødadgang(trim($_GET['ref']), trim($_GET['token']));
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        /** @var Adgang|null $nødadgang */
        $nødadgang = $this->hoveddata['nødadgang'] ?? null;
        if ($nødadgang) {
            $nødpassord = $nødadgang->nødpassord;
            $nødadgang->nødpassord = null;
            $deler = explode('.', $nødpassord);
            $frist = $deler[1] ?? 0;

            if ($frist < time()) {
                return false;
            }
//            $_SESSION['leiebasen']['min-søknad']['referanse'] = $nødadgang->referanse;
//            $_SESSION['leiebasen']['min-søknad']['epost'] = $nødadgang->epost;
//            $_SESSION['leiebasen']['min-søknad']['login'] = $nødadgang->login;
//            $_SESSION['leiebasen']['min-søknad']['adgang'] = $nødadgang->hentId();
//            $_SESSION['leiebasen']['min-søknad']['søknad'] = $nødadgang->søknad->hentId();
            $_SESSION['Config']['time'] = time() + \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['session']['timeout'] ?? 3600;
            $this->hentAutoriserer()->hentAuthenticationService()->getStorage()->write($nødadgang->referanse);
        }
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Søknad $søknad */
        $søknad = $this->hoveddata['søknad'] ?? null;
        if (!$søknad || !$søknad->hentId()) {
            return false;
        }
        /** @var Html $innhold */
        $innhold = $this->hentRespons()->hentInnhold();
        $innhold->hent('header')->sett('språkvelger', $this->lagSpråkvelger());
        /** @var Head $htmlHead */
        $htmlHead = $innhold->getData('head');

        $reCaptcha3SiteKey = \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['google']['recaptcha3']['site_key'] ?? null;
        if ($reCaptcha3SiteKey) {
            $htmlHead
                /**
                 * Google reCaptcha
                 * @link https://developers.google.com/recaptcha/docs/v3
                 */
                ->leggTil('scripts', new HtmlElement('script', [
                    'src' => "https://www.google.com/recaptcha/api.js?render=" . $reCaptcha3SiteKey
                ]));
        }

        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Respons
     */
    public function hentRespons()
    {
        $this->returi->reset();
        return parent::hentRespons();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function innhold()
    {
        /** @var Adgang $adgang */
        $adgang = $this->hoveddata['søknad_adgang'] ?? null;
        /** @var Søknad $søknad */
        $søknad = $adgang ? $adgang->søknad : ($this->hoveddata['søknad'] ?? null);

        return $this->vis(\Kyegil\Leiebasen\Visning\min_søknad\html\endre_passord\Index::class, [
            'adgang' => $adgang,
        ]);
    }

    /**
     * @param string $referanse
     * @param string $nødPassord
     * @return $this
     * @throws Exception
     */
    private function settNødadgang(string $referanse, string $nødPassord)
    {
        $adgang = $this->hentSamling(Adgang::class)
            ->leggTilFilter(['referanse' => $referanse])
            ->leggTilFilter(['nødpassord' => $nødPassord])
            ->hentFørste();
        $this->hoveddata['nødadgang'] = $adgang;
        return $this;
    }
}