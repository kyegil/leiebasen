<?php

namespace Kyegil\Leiebasen\Oppslag\MinSøknad\EndrePassord;


use Exception;
use Kyegil\Leiebasen\Modell\Søknad\Adgang;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use stdClass;

class Mottak extends AbstraktMottak
{
    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Passordet er oppdatert."
        ];
        $get = $_GET;
        $post = $_POST;
        $files = $_FILES;
        $skjema = isset($get['skjema']) ? $get['skjema'] : '';
        try {
            switch ($skjema) {
                case 'endre_passord':
                    $språk = $this->app->hentSpråk();
                    /** @var Adgang $adgang */
                    $adgang = $this->app->hoveddata['søknad_adgang'] ?? null;
                    $recaptcha = $post['g-recaptcha-response'] ?? null;
                    \Kyegil\Leiebasen\Oppslag\Offentlig\Innlogging\Mottak::validerRecaptcha($recaptcha);
                    $adgangsId = $post['adgang_id'] ?? null;
                    $passord = $post['passord1'] ?? null;
                    $passord2 = $post['passord2'] ?? null;

                    if (!$adgang || !$adgangsId || $adgangsId != $adgang->hentId()) {
                        throw new Exception(
                            $språk == 'en'
                                ? 'Du er ikke logget inn, vennligst logg inn på nytt'
                                : 'You are not logged in, please log in again'
                        );
                    }

                    $this->validerPassord($passord, $passord2, $språk);

                    if ($språk == 'en') {
                        $resultat->msg = 'Your password has been updated';
                    }

                    $adgang->pw = password_hash($passord, PASSWORD_DEFAULT);
                    $adgang->nødpassord = null;

                    $resultat->url = strval($this->app->returi->get(1));
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param $epost
     * @return $this
     * @throws Exception
     */
    protected function validerEpost($epost): Mottak
    {
        if($epost && !filter_var($epost, FILTER_VALIDATE_EMAIL)) {
            throw new Exception('Ugyldig epostadresse');
        }
        return $this;
    }

    /**
     * @param string|null $passord1
     * @param string|null $passord2
     * @param string|null $språk
     * @return Mottak
     * @throws Exception
     */
    private function validerPassord(?string $passord1, ?string $passord2, ?string $språk = null): Mottak
    {
        switch($språk) {
            case 'en':
                if (!$passord1) {
                    throw new Exception('The password is missing');
                }
                if($passord1 != $passord2) {
                    throw new Exception('The passwords don\'t match');
                }
                return $this;
            default:
                if (!$passord1) {
                    throw new Exception('Passwordet mangler');
                }
                if($passord1 != $passord2) {
                    throw new Exception('Passordene stemmer ikke overens');
                }
                return $this;
        }
    }
}