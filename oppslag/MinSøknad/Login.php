<?php

namespace Kyegil\Leiebasen\Oppslag\MinSøknad;

use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\Head;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\Html;

class Login extends \Kyegil\Leiebasen\Oppslag\MinSøknadKontroller
{
    /** @var string */
    public $tittel = 'Logg inn';

    /**
     * Oversikt constructor.
     *
     * @param array $di
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        if(isset($_GET['l'])) {
            $this->settSpråk($_GET['l']);
        }
    }

    /**
     * @return bool
     */
    public function preHTML()
    {
        $språk = $this->hentSpråk();
        $this->tittel = $språk == 'en' ? 'Log in' : 'Logg inn';
        /** @var Html $innhold */
        $innhold = $this->hentRespons()->hentInnhold();
        $innhold->hent('header')->sett('språkvelger', $this->lagSpråkvelger());
        /** @var Head $htmlHead */
        $htmlHead = $innhold->getData('head');

        $reCaptcha3SiteKey = \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['google']['recaptcha3']['site_key'] ?? null;
        if ($reCaptcha3SiteKey) {
            $htmlHead
                /**
                 * Google reCaptcha
                 * @link https://developers.google.com/recaptcha/docs/v3
                 */
                ->leggTil('scripts', new HtmlElement('script', [
                    'src' => "https://www.google.com/recaptcha/api.js?render=" . $reCaptcha3SiteKey
                ]));
        }

        return true;
    }

    /**
     * @param string $katalog
     * @param null $søknadId
     * @return bool
     */
    public function adgang($katalog = "min-søknad", $søknadId = null)
    {
        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Respons
     */
    public function hentRespons()
    {
        $this->returi->reset();
        return parent::hentRespons();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function innhold()
    {
        $språk = $this->hentSpråk();

        return $this->vis(\Kyegil\Leiebasen\Visning\min_søknad\html\login\Index::class, [
            'språkKode' => $språk
        ]);
    }
}