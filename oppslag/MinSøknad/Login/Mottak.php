<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 07/09/2020
 * Time: 17:10
 */

namespace Kyegil\Leiebasen\Oppslag\MinSøknad\Login;


use DateInterval;
use DateTime;
use Exception;
use Kyegil\Leiebasen\Autoriserer\MinSøknad;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Søknad\Adgang;
use Kyegil\Leiebasen\Modell\Søknad\Adgangsett;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Visning\felles\epost\shared\Epost;
use Kyegil\Leiebasen\Visning\min_søknad\epost\Passordlenke;
use Kyegil\Leiebasen\Visning\min_søknad\epost\ReferanseOversikt;
use stdClass;

class Mottak extends AbstraktMottak
{
    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
        ];
        $get = $_GET;
        $post = $_POST;
        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'login':
                    $språk = $this->app->hentSpråk();
                    $recaptcha = $post['g-recaptcha-response'] ?? null;
                    \Kyegil\Leiebasen\Oppslag\Offentlig\Innlogging\Mottak::validerRecaptcha($recaptcha);
                    $referanse = $post['referanse'] ?? '';
                    $passord = isset($post['passord']) && $post['passord'] ? trim($post['passord']) : null;

                    /** @var MinSøknad $autoriserer */
                    $autoriserer = $this->app->hentAutoriserer();
                    $resultat->success = $autoriserer->loggInn($referanse, $passord);
                    if (!$resultat->success) {
                        $melding = 'Innloggingen mislyktes.<br>Sjekk detaljene du har oppgitt';
                        if ($språk == 'en') {
                            $melding = 'Login failed.<br>Check you credentials';
                        }
                        throw new Exception($melding);
                    }
                    $resultat->url = strval($this->app->returi->get());
                    break;
                case 'glemt_referanse':
                    $språk = $this->app->hentSpråk();
                    $recaptcha = $post['g-recaptcha-response'] ?? null;
                    \Kyegil\Leiebasen\Oppslag\Offentlig\Innlogging\Mottak::validerRecaptcha($recaptcha);
                    $epost = $post['epost'] ?? null;

                    /** @var Adgangsett $søknadAdganger */
                    $søknadAdganger = $this->app->hentSamling(Adgang::class)
                        ->leggTilFilter(['epost' => trim($epost)]);

                    if (
                        !$søknadAdganger->hentAntall()
                    ) {
                        $melding = 'Det ble ikke funnet noen søknader registrert på denne e-postadressen.';
                        if ($språk == 'en') {
                            $melding = 'No applications were found registered to this email address';
                        }
                        throw new Exception($melding);
                    }

                    $resultat->msg = 'Det ble funnet en eller flere søknader på denne epost-adressen. Sjekk e-post for referanse.';
                    if ($språk == 'en') {
                        $resultat->msg = 'One or more applications were found with this email address. Check email for the reference';
                    }

                    $emne = $språk == 'en' ? 'The reference for logging in to your  application' : 'Referanse for å logge inn til din søknad';

                    $this->app->sendMail((object)[
                        'to' => $epost,
                        'subject' => $emne,
                        'reply' => $this->app->hentValg('epost'),
                        'html' => $this->app->hentVisning(Epost::class, [
                            'språk' => $språk,
                            'tittel' => $emne,
                            'innhold' => $this->app->hentVisning(ReferanseOversikt::class, [
                                'epost' => $epost,
                                'adgangsett' => $søknadAdganger,
                                'språk' => $språk,
                            ]),
                            'bunntekst' => ''
                        ]),
                        'priority' => 1000,
                        'type' => 'min_søknad_glemt_referanse'
                    ]);

                    $resultat->url = strval('/min-søknad/index.php?oppslag=login&l=' . $språk);
                    break;
                case 'glemt_passord':
                    $språk = $this->app->hentSpråk();
                    $recaptcha = $post['g-recaptcha-response'] ?? null;
                    \Kyegil\Leiebasen\Oppslag\Offentlig\Innlogging\Mottak::validerRecaptcha($recaptcha);
                    $referanse = $post['referanse'] ?? null;

                    /** @var Adgang|null $adgang */
                    $adgang = $this->app->hentSamling(Adgang::class)
                        ->leggTilFilter(['referanse' => trim($referanse)])
                        ->hentFørste()
                    ;

                    if (
                        !$adgang
                    ) {
                        $melding = 'Ugyldig referanse.';
                        if ($språk == 'en') {
                            $melding = 'Invalid reference';
                        }
                        throw new Exception($melding);
                    }

                    if (
                        !$adgang->epost
                    ) {
                        $melding = 'Det er ikke knyttet noen epost-adresse til denne søknaden. Passordet kan ikke resettes';
                        if ($språk == 'en') {
                            $melding = 'There is no email address associated with this application. The password cannot be reset';
                        }
                        throw new Exception($melding);
                    }

                    $søknad = $adgang->søknad;

                    $frist = new DateTime();
                    $frist->add(new DateInterval('PT5H'));
                    $adgang->settNødpassord(md5($adgang->referanse . rand(0, 1000000)) . '.' . $frist->getTimestamp());
                    $lenke = $this->app->http_host . '/min-søknad/index.php?oppslag=endre_passord&l=' . $språk . '&ref=' . $adgang->referanse . '&token=' . $adgang->nødpassord;
                    $lenke = new HtmlElement('a', [
                        'title' => $språk == 'en' ? 'Follow the link to reset your password' : 'Følg lenken for å resette passordet',
                        'href' => $lenke
                    ], $lenke);

                    $resultat->msg = 'En epost har blitt sendt til hovedsøker, med lenke for å resette passordet. Lenken må benyttes i løpet av 5 timer.';
                    if ($språk == 'en') {
                        $resultat->msg = 'An email has been sent to the main applicant, with a link to reset the password. The link must be used within 5 hours.';
                    }

                    $emne = $språk == 'en' ? 'Link to reset your password' : 'Lenke for å resette ditt passord';

                    $this->app->sendMail((object)[
                        'to' => $adgang->epost,
                        'subject' => $emne,
                        'reply' => $this->app->hentValg('epost'),
                        'html' => $this->app->hentVisning(Epost::class, [
                            'språk' => $språk,
                            'tittel' => $emne,
                            'innhold' => $this->app->hentVisning(Passordlenke::class, [
                                'lenke' => $lenke,
                                'språk' => $språk,
                            ]),
                            'bunntekst' => ''
                        ]),
                        'priority' => 1000,
                        'type' => 'min_søknad_glemt_passord'
                    ]);

                    $resultat->url = strval('/min-søknad/index.php?oppslag=login&l=' . $språk);
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}