<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/07/2024
 * Time: 23:41
 */

namespace Kyegil\Leiebasen\Oppslag\Eks;

use Kyegil\Leiebasen\Oppslag\AbstraktMottak;

class EksMottak extends AbstraktMottak
{

    /**
     * @inheritDoc
     */
    public function taIMot(?string $skjema)
    {
        $this->settRespons($this->app->taIMot($skjema));
    }
}