<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/07/2024
 * Time: 23:41
 */

namespace Kyegil\Leiebasen\Oppslag\Eks;

use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;

class EksUtlevering extends AbstraktUtlevering
{

    /**
     * @inheritDoc
     */
    public function hentData(?string $data)
    {
        $this->settRespons($this->app->hentData($data));
    }
}