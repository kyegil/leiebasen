<?php
/**
 * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/07/2024
 * Time: 23:42
 */

namespace Kyegil\Leiebasen\Oppslag\Eks;

use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;

class EksOppdrag extends AbstraktOppdrag
{
    /**
     * @param string|null $data
     * @return EksOppdrag
     */
    protected function manipuler(?string $data)
    {
        ob_start();
        $resultat = (string)$this->app->manipuler($data);
        $echo = ob_get_clean();
        $this->settRespons($resultat ?: $echo);
        return $this;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function utskrift()
    {
        ob_start();
        $resultat = (string)$this->app->utskrift();
        $echo = ob_get_clean();
        $this->settRespons($resultat ?: $echo);
        return $this;
    }

    /**
     * @param $oppgave
     * @return mixed
     */
    public function oppgave(string $oppgave)
    {
        $oppgave = $this->metode($oppgave);
        $this->app->pre($this, $oppgave, []);
        ob_start();
        $resultat = (string)$this->app->oppgave($oppgave);
        $echo = ob_get_clean();
        $this->settRespons($resultat ?: $echo);
        return $this->app->post($this, $oppgave, $resultat);
    }

    /**
     * @param $pdf
     * @return mixed
     */
    public function lagPdf(string $pdf)
    {
        list('pdf' => $pdf) = $this->app->pre($this, __FUNCTION__, ['pdf' => $pdf]);
        ob_start();
        $resultat = (string)$this->app->lagPDF($pdf);
        $echo = ob_get_clean();
        $this->settRespons($resultat ?: $echo);
        return $this->app->post($this, __FUNCTION__, $resultat);
    }

    /**
     * @param string $oppdrag
     * @return $this
     */
    public function prosesser(string $oppdrag)
    {
        switch ($oppdrag) {
            case 'lagpdf':
                return $this->lagPdf($this->data['get']['pdf'] ?? '');
            case 'oppgave':
                return $this->oppgave($this->data['get']['oppgave'] ?? '');
            default:
                return parent::prosesser($oppdrag);
        }
    }
}