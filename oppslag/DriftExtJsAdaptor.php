<?php

namespace Kyegil\Leiebasen\Oppslag;

use Kyegil\Leiebasen\Visning\drift\html\shared\Head;
use Kyegil\Leiebasen\Visning\drift\html\shared\Html;

/**
 * Felles adaptor for alle kontroller i det nye formatet (i oppslag-katalogen)
 * som fortsatt av en eller annen grunn må bruke ExtJs brukergrensesnittet.
 */
class DriftExtJsAdaptor extends DriftKontroller
{
    /**
     * @return \Kyegil\Leiebasen\Respons
     */
    public function hentRespons()
    {
        $respons = parent::hentRespons();
        $innhold = $respons->hentInnhold();
        if ($innhold instanceof Html) {
            $htmlHead = $this->vis(Head::class, [
                'extPath' => '/pub/lib/' . $this->ext_bibliotek,
                'title' => $this->tittel,
            ]);
            $innhold->sett('head', $htmlHead);
        }
        return $respons;
    }

    /**
     * Innhold
     *
     * @return string
     */
    public function innhold()
    {
        return '<div id="panel" class="extjs-panel"></div>';
    }
}