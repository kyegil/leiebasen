<?php

namespace Kyegil\Leiebasen\Oppslag;

use Exception;
use Kyegil;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\body\Main;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\body\main\ReturiBreadcrumbs;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\body\main\ReturiElement;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Head;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Html;
use Kyegil\ViewRenderer\ViewArray;

class MineSiderKontroller extends AbstraktKontroller
{
    /** @var string */
    public $tittel = "Mine Sider";
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @inheritdoc */
    public array $område = ['område' => 'mine-sider'];

    /**
     * @param string $oppslag
     * @param string|null $id
     * @param string|null $oppdrag
     * @param array $queryParams
     * @param string $område
     * @return string
     */
    public static function url(
        string $oppslag,
        ?string $id = null,
        ?string $oppdrag = null,
        array $queryParams = [],
        string $område = ''
    ): string {
        return parent::url($oppslag, $id, $oppdrag, $queryParams, 'mine-sider');
    }

    /**
     * MineSiderKontroller constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = [])
    {
        parent::__construct($di, $config);
        $this->returi->setBaseUrl($this->http_host . '/mine-sider/index.php', 'Mine Sider');
        $this->forberedHovedData();
    }

    /**
     * Sjekker om brukeren har adgang til et angitt område.
     *	Samtidig sjekkes og oppdateres økten, og innlogging kreves om nødvendig
     *	$this->bruker fylles også ut som følger:
     *		navn:		Fullt navn
     *		id:			Brukerens id, i samsvar med personadressekort
     *		brukernavn: Brukernavn for innlogging
     *		epost:		Brukerens epostadresse
     *
     * @param string $katalog Området det ønskes adgang til
     * @param int|null $leieforhold Aktuelt leieforhold dersom området er 'mine-sider'
     * @return bool             Sant dersom innlogget bruker har adgang til området
     */
    public function adgang($katalog = "", $leieforhold = null) {
        /** @var \Kyegil\Leiebasen\Autoriserer $autoriserer */
        $autoriserer = $this->hentAutoriserer();
        $leieforholdId = null;
        if(isset($leieforhold)) {
            $leieforholdId = intval(strval($leieforhold));
        }

        if( !$leieforhold && isset($this->område['leieforhold'])) {
            $leieforholdId = intval(strval($this->område['leieforhold']));
        }

        $this->bruker['navn'] = $autoriserer->hentNavn();
        $this->bruker['id'] = $autoriserer->hentId();
        $this->bruker['brukernavn'] = $autoriserer->hentBrukernavn();
        $this->bruker['epost'] = $autoriserer->hentEpost();

        $autoriserer->krevIdentifisering();

        if ($katalog == "") {
            return true;
        }

        $betingelser = [
            'personid' => $this->bruker['id'],
            'adgang' => $katalog
        ];
        if($katalog == Person\Adgang::ADGANG_MINESIDER && isset($leieforholdId)) {
            $betingelser['leieforhold'] = $leieforholdId;
        }
        $resultat = $this->mysqli->arrayData(array(
            'source' => "adganger",
            'where' => $betingelser
        ));
        return (bool)$resultat->totalRows;
    }

    /**
     * @return Respons
     */
    public function hentRespons()
    {
        if(!isset($this->respons)) {
            if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
                $this->returi->reset();
            }
            $this->returi->set(null, $this->tittel);

            /** @var Person $bruker */
            $bruker = $this->hoveddata['bruker'];
            $this->respons = new Respons($this->vis( Html::class, [
                'head' => $this->vis(Head::class, [
                    'title' => $this->tittel
                ]),
                'brukermeny' => $this->vis(Visning\mine_sider\html\shared\body\Brukermeny::class),
                'hovedmeny' => $this->vis(Visning\mine_sider\html\shared\body\Hovedmeny::class, [
                    'bruker' => $bruker
                ]),
                'header' => $this->vis(Visning\mine_sider\html\shared\body\Header::class),
                'footer' => $this->vis(Visning\mine_sider\html\shared\body\Footer::class),
                'main' => $this->vis(Main::class, [
                    'tittel' => $this->tittel,
                    'innhold' => $this->innhold(),
                    'breadcrumbs' => $this->visBreadcrumbs(),
                    'varsler' => $this->visVarsler(),
                    'knapper' => [$this->visTilbakeknapp()]
                ]),
                'scripts' => $this->skript()
            ]));
        }
        return parent::hentRespons();
    }

    /**
     * @return bool
     */
    public function skrivHTML()
    {
        if( !$this->preHTML() ) {
            $this->responder404();
            return false;
        }
        $html = $this->hentRespons();
        echo $html;
        return true;
    }

    /**
     * @return string
     */
    public function innhold()
    {
        return '';
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     */
    public function visBreadcrumbs()
    {
        $elementer = new ViewArray();
        $elementer->setGlue(' > ');
        $sti = $this->returi->getTrace();
        /** @var bool $rot Det første elementet er rot */
        $rot = true;
        foreach($sti as $url) {
            $current = $this->returi->getCurrentLocation() == $url->url;
            $elementer->addItem(
                $this->vis(ReturiElement::class, [
                    'url' => $url->url,
                    'tittel' => $url->title,
                    'rot' => $rot,
                    'current' => $current
                ])
            );
            $rot = false;
        }
        return $this->vis(ReturiBreadcrumbs::class, [
            'crumbs' => $elementer
        ]);
    }

    /**
     * @return string
     */
    public function visTilbakeknapp()
    {
        $sti = $this->returi->getTrace();
        if(count($sti) > 1) {
            return new HtmlElement('a',
                [
                    'href' => $this->returi->get()->url,
                    'title' => "Tilbake til {$this->returi->get()->title}",
                    'class' => "button tilbakeknapp"
                ],
                'Tilbake'
            );
        }
        return '';
    }

    /**
     * @return array
     */
    protected function visVarsler()
    {
        $varsler = [];
        foreach($this->hentAdvarsler(\Kyegil\Leiebasen\Leiebase::VARSELNIVÅ_ADVARSEL) as $advarsel) {
            $varsler[] = new HtmlElement('div', ['class' => 'advarsel'], $advarsel['oppsummering']);
        }
        foreach($this->hentAdvarsler(\Kyegil\Leiebasen\Leiebase::VARSELNIVÅ_ORIENTERING) as $påminnelse) {
            $varsler[] = new HtmlElement('div', ['class' => 'påminnelse'], $påminnelse['oppsummering']);
        }
        return $varsler;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        $leieforhold = isset($this->hoveddata['leieforhold']) ? $this->hoveddata['leieforhold'] : null;
        if(!$this->adgang('mine-sider' , $leieforhold)) {
            $this->responder403();
        }
        $this->hoveddata['bruker'] = $this->hentModell(Person::class, $this->bruker['id']);
        return parent::preHTML();
    }

    /**
     * @param string $oppdrag
     * @throws Exception
     */
    public function oppdrag($oppdrag = "")
    {
        $leieforhold = isset($this->hoveddata['leieforhold']) ? $this->hoveddata['leieforhold'] : null;
        if(!$this->adgang('mine-sider' , $leieforhold)) {
            $this->responder403();
        }
        $this->hoveddata['bruker'] = $this->hentModell(Person::class, $this->bruker['id']);
        parent::oppdrag($oppdrag);
    }

    public function hentMenyStruktur(string $meny = '', ?Person $bruker = null): array
    {
        list('meny' => $meny, 'bruker' => $bruker) = $this->pre($this, __FUNCTION__, ['meny' => $meny, 'bruker' => $bruker]);
        /** @var Person $bruker */
        $bruker = $bruker ?: $this->hentModell(Person::class, $this->bruker['id']);
        if (!$bruker->hentId()) {
            $bruker = null;
        }
        $hovedLeieforhold = $bruker && $bruker->hentHovedLeieforhold() && $bruker->hentHovedLeieforhold()->hentId()
            ? $bruker->hentHovedLeieforhold() : null;
        $hovedLeieobjekt = $hovedLeieforhold ? $hovedLeieforhold->leieobjekt : null;

        $aktiveMenyElementer = $this->hentAktiveMenyElementer();
        $nåværendeElement = end($aktiveMenyElementer);

        switch($meny) {
            case 'brukermeny':
                $gåTilMeny = (object)[
                    'id' => 'gå-til',
                    'text' => 'Gå til',
                    'active' => in_array('gå-til', $aktiveMenyElementer),
                    'items' => [],
                ];
                if($bruker && $bruker->harAdgangTil(Person\Adgang::ADGANG_DRIFT)) {
                    $gåTilMeny->items[] = (object)[
                        'text' => 'Drift',
                        'url' => '/drift/'
                    ];
                }
                $resultat = [
                    (object)[
                        'id' => 'brukerprofil',
                        'text' => 'Min brukerprofil',
                        'url' => "/mine-sider/index.php?oppslag=profil_skjema&returi=default",
                        'active' => in_array('profil_skjema', $aktiveMenyElementer),
                        'nåværende' => 'profil_skjema' === $nåværendeElement,
                        'items' => [
                            (object)[
                                'text' => 'Endre passord',
                                'url' => "/mine-sider/index.php?oppslag=profil_passord_skjema&returi=default",
                            ],
                        ],
                    ],
                    (object)[
                        'id' => 'adgangskontroll',
                        'text' => 'Adgangskontroll',
                        'url' => "/mine-sider/index.php?oppslag=adgang_skjema&returi=default",
                        'active' => in_array('adgangskontroll', $aktiveMenyElementer),
                    ],
                    (object)[
                        'id' => 'varselpreferanser',
                        'text' => 'Varsel-preferanser',
                        'url' => "/mine-sider/index.php?oppslag=profil_kommunikasjon_preferanser&returi=default",
                        'active' => in_array('varselpreferanser', $aktiveMenyElementer),
                        'items' => [
                            (object)[
                                'text' => 'Motta melding om skader',
                                'url' => "/mine-sider/index.php?oppslag=skade_leieobjekt_abonnement_skjema&returi=default",
                            ],
                        ],
                    ],
                ];
                if($gåTilMeny->items) {
                    $resultat[] = $gåTilMeny;
                }
                $resultat[] = (object)[
                    'text' => 'Logg ut',
                    'url' => '/offentlig/index.php?oppslag=index&oppdrag=avslutt',
                    'extraClasses' => ['fas', 'fa-power-off'],
                ];
                return $this->post($this, __FUNCTION__, $resultat, ['meny' => $meny, 'bruker' => $bruker]);
            default:
                /** @var object $mineRegningerMeny */
                $mineRegningerMeny = (object)[
                    'id' => 'mine-regninger-meny',
                    'extraClasses' => [],
                    'text' => 'Mine Regninger og betalinger',
                    'url' => '/mine-sider/index.php?oppslag=mine_regninger&returi=default',
                    'active' => in_array('mine-regninger-meny', $aktiveMenyElementer),
                    'symbol' => null,
                    'items' => []
                ];

                /** @var object $mittLeieforholdMeny */
                $mittLeieforholdMeny = (object)[
                    'id' => 'mitt-leieforhold-meny',
                    'text' => 'Mitt leieforhold',
                    'url' => '/mine-sider/index.php?oppslag=leieforhold&id=' . $hovedLeieforhold . '&returi=default',
                    'extraClasses' => [],
                    'symbol' => null,
                    'active' => in_array('mitt-leieforhold-meny', $aktiveMenyElementer),
                    'items' => [],
                ];

                if($hovedLeieforhold) {
                    $mittLeieforholdMeny->items = [
                        (object)[
                            'text' => 'Min Leieavtale',
                            'url' => '/mine-sider/index.php?oppslag=leieforhold_avtaletekst&id=' . $hovedLeieforhold . '&returi=default',
                            'active' => in_array('leieforhold_avtaletekst', $aktiveMenyElementer),
                            'nåværende' => 'leieforhold_avtaletekst' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Mine varslingspreferanser',
                            'url' => '/mine-sider/index.php?oppslag=leieforhold_preferanser&id=' . $hovedLeieforhold . '&returi=default',
                            'active' => in_array('leieforhold_preferanser', $aktiveMenyElementer),
                            'nåværende' => 'leieforhold_preferanser' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Se transaksjoner',
                            'url' => '/mine-sider/index.php?oppslag=leieforhold_transaksjoner&id=' . $hovedLeieforhold . '&returi=default',
                            'active' => in_array('leieforhold_transaksjoner', $aktiveMenyElementer),
                            'nåværende' => 'leieforhold_transaksjoner' === $nåværendeElement,
                        ],
                    ];
                }

                /** @var object $minBoligMeny */
                $minBoligMeny = (object)[
                    'id' => 'min-bolig-meny',
                    'text' => 'Min bolig',
                    'url' => '/mine-sider/index.php?oppslag=leieobjekt&id=' . $hovedLeieobjekt . '&returi=default',
                    'symbol' => null,
                    'extraClasses' => [],
                    'active' => in_array('min-bolig-meny', $aktiveMenyElementer),
                    'items' => [
                        (object)[
                            'text' => 'Meld skade',
                            'url' => '/mine-sider/index.php?oppslag=skademelding_skjema&leieobjekt=' . $hovedLeieobjekt . '&returi=default',
                            'active' => in_array('skademelding_skjema', $aktiveMenyElementer),
                            'nåværende' => 'skademelding_skjema' === $nåværendeElement,
                        ],
                    ],
                ];

                $leieobjekterMeny = (object)[
                    'id' => 'leieobjekter-meny',
                    'text' => 'Boligmassen',
                    'url' => '/mine-sider/index.php?oppslag=leieobjekter_liste&returi=default',
                    'symbol' => null,
                    'extraClasses' => ['fas', 'fa-building'],
                    'active' => in_array('leieobjekter-meny', $aktiveMenyElementer),
                    'items' => [
                        (object)[
                            'text' => 'Ledige boliger',
                            'url' => '/mine-sider/index.php?oppslag=leieobjekter_ledige&returi=default',
                            'active' => in_array('leieobjekter_ledige', $aktiveMenyElementer),
                            'nåværende' => 'leieobjekter_ledige' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Skader',
                            'url' => '/mine-sider/index.php?oppslag=skadeliste&returi=default',
                            'active' => in_array('skadeliste', $aktiveMenyElementer),
                            'nåværende' => 'skadeliste' === $nåværendeElement,
                        ],
                    ],
                ];

                $statusMeny = (object)[
                    'id' => 'status-meny',
                    'text' => 'Status og statistikk',
                    'url' => '/mine-sider/index.php?oppslag=drift_status&returi=default',
                    'symbol' => null,
                    'extraClasses' => ['fas', 'fa-bolt'],
                    'active' => in_array('status-meny', $aktiveMenyElementer),
                    'items' => [
                        (object)[
                            'text' => 'Driftsstatus',
                            'url' => '/mine-sider/index.php?oppslag=drift_status&returi=default',
                            'active' => in_array('drift_status', $aktiveMenyElementer),
                            'nåværende' => 'drift_status' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Div statistikk',
                            'url' => '/mine-sider/index.php?oppslag=oversikt_inntekter&returi=default',
                            'active' => in_array('delte_kostnader_kostnad_liste', $aktiveMenyElementer),
                            'nåværende' => 'delte_kostnader_kostnad_liste' === $nåværendeElement,
                        ],
                        (object)[
                            'text' => 'Gå til Drift',
                            'url' => '/drift/',
                        ],
                    ],
                ];

                $resultat = [
                    $mineRegningerMeny,
                    $mittLeieforholdMeny,
                    $minBoligMeny,
                    $leieobjekterMeny,
                    $statusMeny,
                ];
                return $this->post($this, __FUNCTION__, $resultat, ['meny' => $meny, 'bruker' => $bruker]);
        }
    }
}