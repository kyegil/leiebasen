<?php

namespace Kyegil\Leiebasen\Oppslag\Offentlig\SøknadSkjema;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Personsett;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Modell\Søknad\Adgang;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Visning\felles\epost\shared\Epost;
use Kyegil\Leiebasen\Visning\felles\epost\søknad\Bekreftelse;
use stdClass;

class Mottak extends AbstraktMottak
{
    public static function genererReferanse(int $søknadId, ?string $epost)
    {
        $md5 = md5($epost . '-' . $søknadId);
        return strtoupper(substr($md5, 0, 5));
    }

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Søknaden er mottatt."
        ];
        $get = $_GET;
        $post = $_POST;
        $files = $_FILES;
        $skjema = isset($get['skjema']) ? $get['skjema'] : '';
        try {
            switch ($skjema) {
                case 'søknad':
                    $språk = $this->app->hentSpråk();
                    $skjemaId = $get['type'] ?? null;
                    $recaptcha = $post['g-recaptcha-response'] ?? null;
                    \Kyegil\Leiebasen\Oppslag\Offentlig\Innlogging\Mottak::validerRecaptcha($recaptcha);
                    $kontaktpersonFornavn = $post['kontaktperson']['fornavn'] ?? null;
                    $kontaktpersonEtternavn = $post['kontaktperson']['etternavn'] ?? null;
                    $kontaktAdresse1 = $post['kontaktperson']['adresse1'] ?? null;
                    $kontaktAdresse2 = $post['kontaktperson']['adresse2'] ?? null;
                    $kontaktPostnr = $post['kontaktperson']['postnr'] ?? null;
                    $kontaktPoststed = $post['kontaktperson']['poststed'] ?? null;
                    $kontaktTelefon = $post['kontaktperson']['telefon'] ?? null;
                    $kontaktEpost = $post['kontaktperson']['epost'] ?? null;
                    $egenkopi = isset($post['kontaktperson']['egenkopi']) && $post['kontaktperson']['egenkopi'];
                    $lagProfil = isset($post['kontaktperson']['lag_profil']) && $post['kontaktperson']['lag_profil'];
                    $passord1 = $post['kontaktperson']['passord1'] ?? null;
                    $passord2 = $post['kontaktperson']['passord2'] ?? null;

                    $this->validerKontaktInfo(
                        $kontaktpersonEtternavn,
                        $kontaktpersonFornavn,
                        $kontaktTelefon,
                        $kontaktEpost,
                        $kontaktAdresse1,
                        $kontaktAdresse2,
                        $kontaktPostnr,
                        $kontaktPoststed,
                        $språk
                    );

                    if ($lagProfil) {
                        $this->validerPassord($passord1, $passord2, $språk);
                    }

                    if ($språk == 'en') {
                        $resultat->msg = 'Your application has been received';
                    }

                    /** @var Søknad\Type $skjemaType */
                    $skjemaType = $this->app->hentModell(Søknad\Type::class, $skjemaId);
                    if (!$skjemaType->hentId()) {
                        throw new Exception(
                            $språk == 'en'
                                ? ('Invalid form ' . $skjemaId)
                                : ('Ugyldig skjema ' . $skjemaId)
                        );
                    }

                    $skjemaData = $post['skjemadata'] ?? [];
                    $søknadsTekst = $skjemaData['søknadstekst'] ?? null;

                    if (!trim(strip_tags($søknadsTekst))) {
                        throw new Exception(
                            $språk == 'en'
                                ? 'Presentation is required'
                                : 'Søknadsteksten mangler'
                        );
                    }

                    $resultat->msg = $språk == 'en' ? 'Your application has been received' :'Søknaden er registrert';
                    /** @var Søknad $søknad */
                    $søknad = $this->app->nyModell(Søknad::class, (object)[
                        'type' => $skjemaType,
                        'meta_data' => (object)[
                            'språk' => $språk,
                            'konfigurering' => $skjemaType->hentKonfigurering()
                        ],
                        'søknadstekst' => $søknadsTekst,
                        Søknad\Type::FELT_KONTAKT_FORNAVN => $kontaktpersonFornavn,
                        Søknad\Type::FELT_KONTAKT_ETTERNAVN => $kontaktpersonEtternavn,
                        'kontakt_adresse1' => $kontaktAdresse1,
                        'kontakt_adresse2' => $kontaktAdresse2,
                        'kontakt_postnr' => $kontaktPostnr,
                        'kontakt_poststed' => $kontaktPoststed,
                        Søknad\Type::FELT_KONTAKT_TELEFON => $kontaktTelefon,
                        Søknad\Type::FELT_KONTAKT_EPOST => $kontaktEpost,
                    ]);

                    $adgang = null;
                    if ($lagProfil) {
                        $adgang = $this->registrerProfil(
                            $kontaktEpost,
                            $søknad,
                            $passord1
                        );
                    }
                    $skjemafeltsett = $skjemaType->hentFeltsett();
                    foreach ($skjemafeltsett as $skjemafelt) {
                        $skjemafeltKode = $skjemafelt->hentKode();
                        if (isset($skjemaData[$skjemafeltKode])) {
                            $søknad->sett($skjemafeltKode, $skjemaData[$skjemafeltKode]);
                        }
                    }

                    $this->sendMottaksbekreftelseEpost($søknad, $språk, $egenkopi, $adgang);

                    try {
                        $this->sendEpostNotifikasjon($søknad);
                    } catch (Exception $e) {
                        $this->app->logger->critical($e->getMessage(), $e->getTrace());
                    }

                    $layout = $skjemaType->hentKonfigurering('layout');
                    if(is_iterable($layout)) {
                        foreach ($layout as $layoutElement) {
                            if($layoutElement->autofilter ?? null) {
                                $søknad->lagAutoAdminFilter($layoutElement);
                            }
                        }
                    }
                    $resultat->url = strval($this->app->returi->get(1));
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param $epost
     * @return $this
     * @throws Exception
     */
    protected function validerEpost($epost): Mottak
    {
        if($epost && !filter_var($epost, FILTER_VALIDATE_EMAIL)) {
            throw new Exception('Ugyldig epostadresse');
        }
        return $this;
    }

    /**
     * @param string|null $passord1
     * @param string|null $passord2
     * @param string|null $språk
     * @return Mottak
     * @throws Exception
     */
    private function validerPassord(?string $passord1, ?string $passord2, ?string $språk = null): Mottak
    {
        switch($språk) {
            case 'en':
                if (!$passord1) {
                    throw new Exception('You need to set a password in order to create a profile');
                }
                if($passord1 != $passord2) {
                    throw new Exception('The passwords don\'t match');
                }
                return $this;
            default:
                if (!$passord1) {
                    throw new Exception('Du må angi passord for a opprette profil');
                }
                if($passord1 != $passord2) {
                    throw new Exception('Passordene stemmer ikke overens');
                }
                return $this;
        }
    }

    /**
     * @param string $epost
     * @param Søknad $søknad
     * @param string $passord
     * @return Adgang
     * @throws Exception
     */
    private function registrerProfil(
        string $epost,
        Søknad $søknad,
        string $passord = null,
        ?string $språk = null
    ): Adgang
    {
        $epost = trim($epost);
        if (!$epost) {
            throw new Exception($språk == 'en' ? 'Email is required' : 'E-post er påkrevet');
        }
        $eksisterende = $this->app->hentSamling(Adgang::class)
            ->leggTilFilter(['epost' => $epost])
            ->leggTilFilter(['søknad_id' => $søknad->hentId()])
            ->hentFørste()
            ;
        if ($eksisterende) {
            throw new Exception(
                $språk == 'en'
                    ? 'Access for this application already exists'
                    : 'Adgang for denne søknaden er allerede registrert'
            );
        }
        $referanse = static::genererReferanse($søknad->hentId(), $epost);
        /** @var Adgang $adgang */
        $adgang = $this->app->nyModell(Adgang::class, (object)[
            'søknad' => $søknad,
            'login' => $epost,
            'epost' => $epost,
            'referanse' => $referanse,
            'pw' => password_hash($passord, PASSWORD_DEFAULT)
        ]);
        return $adgang;
    }

    /**
     * @param Søknad $søknad
     * @param string|null $språk
     * @param bool $egenkopi
     * @param Adgang|null $adgang
     * @return $this
     * @throws Exception
     */
    private function sendMottaksbekreftelseEpost(
        Søknad $søknad,
        ?string $språk,
        bool $egenkopi = false,
        ?Adgang $adgang = null
    )
    {
        $epost = $søknad->hent(Søknad\Type::FELT_KONTAKT_EPOST);
        if (!trim($epost)) {
            return $this;
        }
        $avsenderEpost = $this->app->hentValg('epost');
        $konfigurering = $søknad->hentMetaData('konfigurering');
        if ($konfigurering) {
            $avsenderEpost = $konfigurering->avsenderEpost ?? $avsenderEpost;
        }
        $navn = $søknad->hent(Søknad\Type::FELT_KONTAKT_FORNAVN);
        $navn = ($navn ? $navn . ' ' : '')
            . $søknad->hent(Søknad\Type::FELT_KONTAKT_ETTERNAVN);
        $emne = $språk == 'en' ? 'Confirmation of submitted application' : 'Bekreftelse på innsendt søknad';
        $this->app->sendMail((object)[
            'to' => "{$navn}<{$epost}>",
            'subject' => $emne,
            'reply' => $avsenderEpost,
            'html' => $this->app->hentVisning(Epost::class, [
                'språk' => $språk,
                'tittel' => $emne,
                'innhold' => $this->app->hentVisning(Bekreftelse::class, [
                    'søknad' => $søknad,
                    'adgang' => $adgang,
                    'språk' => $språk,
                    'navn' => $navn,
                    'søknadsOppsummering' => $egenkopi ? null : ''
                ]),
                'bunntekst' => ''
            ]),
            'priority' => 100,
            'type' => 'søknadsskjema_kvittering'
        ]);
        return $this;
    }

    /**
     * @param string|null $kontaktpersonEtternavn
     * @param string|null $kontaktpersonFornavn
     * @param string|null $kontaktTelefon
     * @param string|null $kontaktEpost
     * @param string|null $kontaktAdresse1
     * @param string|null $kontaktAdresse2
     * @param string|null $kontaktPostnr
     * @param string|null $kontaktPoststed
     * @param string|null $språk
     * @return Mottak
     * @throws Exception
     */
    private function validerKontaktInfo(
        ?string $kontaktpersonEtternavn,
        ?string $kontaktpersonFornavn,
        ?string $kontaktTelefon,
        ?string $kontaktEpost,
        ?string $kontaktAdresse1,
        ?string $kontaktAdresse2,
        ?string $kontaktPostnr,
        ?string $kontaktPoststed,
        ?string $språk = null
    ): Mottak
    {
        if (trim($kontaktEpost) && !filter_var($kontaktEpost, FILTER_VALIDATE_EMAIL)) {
            throw new Exception(
                $språk == 'en'
                    ? $kontaktEpost . ' is not a valid email'
                    : $kontaktEpost . ' er ikke en gyldig epost-adresse'
            );
        }
        if (!trim($kontaktEpost) && !trim($kontaktTelefon)) {
            throw new Exception(
                $språk == 'en'
                    ? 'Either email or phone number is required'
                    : 'Enten telefonnummer eller epost må oppgis'
            );

        }
        return $this;
    }

    /**
     * @param Søknad $søknad
     * @return Mottak
     * @throws Exception
     */
    private function sendEpostNotifikasjon(
        Søknad $søknad
    ): Mottak
    {
        $søknadType = $søknad->type;
        $avsenderEpost = $søknadType->hentKonfigurering('avsenderEpost') ?? null;
        $notifikasjonsMottakere = $this->hentNotifikasjonsMottakere();
        foreach ($notifikasjonsMottakere as $mottaker) {
            try {
                $this->app->sendMail((object)[
                    'to' => "{$mottaker->hentNavn()}<{$mottaker->hentEpost()}>",
                    'subject' => 'Ny søknad registrert – ' . $søknad->type->hentNavn(),
                    'reply' => $avsenderEpost,
                    'html' => $this->app->hentVisning(Epost::class, [
                        'tittel' => 'Ny søknad registrert',
                        'innhold' => new HtmlElement('div', [], [
                            $søknad->type->hentNavn() . '<br>',
                            'Dette er en melding om at det har kommet inn en ny søknad.<br>',
                            new HtmlElement('a', [
                                'class' => 'button',
                                'href' => $this->app->http_host . '/flyko/index.php?oppslag=søknad_liste&søknad_type=' . $søknadType . '&returi=default'
                            ],
                                'Klikk her for å se søknaden')
                        ]),
                        'bunntekst' => ''
                    ]),
                    'priority' => 100,
                    'type' => 'søknadsskjema_notifikasjon'
                ]);
            } catch (Exception $e) {
                $this->app->logger->critical($e->getMessage(), $e->getTrace());
            }
        }
        return $this;
    }

    /**
     * @return Personsett
     * @throws Exception
     */
    private function hentNotifikasjonsMottakere(): Personsett
    {
        $this->app->before($this, __FUNCTION__, []);
        /** @var Personsett $mottakere */
        $mottakere = $this->app->hentSamling(Person::class);
        $mottakere->leggTilInnerJoin(Person\Adgang::hentTabell(),
            '`' . Person::hentTabell(). '`.`' . Person::hentPrimærnøkkelfelt(). '` = `' . Person\Adgang::hentTabell(). '`.`personid`'
        );
        $mottakere->leggTilFilter(['`' . Person\Adgang::hentTabell(). '`.`adgang`' => Person\Adgang::ADGANG_FLYKO]);
        $mottakere->leggTilFilter(['`' . Person\Adgang::hentTabell(). '`.`epostvarsling`' => true]);
        $mottakere->leggTilFilter(['`' . Person::hentTabell(). '`.`epost` <>' => '']);
        $mottakere->låsFiltre();
        return $this->app->after($this, __FUNCTION__, $mottakere);
    }
}