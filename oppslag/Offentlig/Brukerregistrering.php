<?php

namespace Kyegil\Leiebasen\Oppslag\Offentlig;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Oppslag\OffentligKontroller;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\Html;
use Kyegil\Leiebasen\Visning\offentlig\js\register_bruker_skjema\Index as Js;

class Brukerregistrering extends OffentligKontroller
{
    /**
     *
     */
    public function forberedHovedData()
    {
        $this->hoveddata['redirect_url'] = $_GET['url'] ?? null;
        parent::forberedHovedData();
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = 'Opprett brukerprofil for beboer';
        /** @var Html $innhold */
        $innhold = $this->hentRespons()->hentInnhold();
        $htmlHead = $innhold->getData('head');

        $reCaptcha3SiteKey = \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['google']['recaptcha3']['site_key'] ?? null;
        if ($reCaptcha3SiteKey) {
            $htmlHead
                /**
                 * Google reCaptcha
                 * @link https://developers.google.com/recaptcha/docs/v3
                 */
                ->leggTil('scripts', new HtmlElement('script', [
                    'src' => "https://www.google.com/recaptcha/api.js?render=" . $reCaptcha3SiteKey
                ]));
        }

        $htmlHead
            /**
             * jQuery validate
             * @link https://jqueryvalidation.org
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"
            ]))

            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]));

        return true;
    }

    /**
     * @return Visning|\Kyegil\ViewRenderer\ViewInterface|string
     */
    public function innhold()
    {
        return $this->vis(Visning\offentlig\html\registrer_bruker_skjema\Index::class, [
            'reCaptchaSiteKey' => \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['google']['recaptcha3']['site_key'] ?? '',
            'redirectUrl' => $this->hoveddata['redirect_url'] ?? ''
        ]);
    }

    /**
     * @return Js|string
     */
    public function skript()
    {
        return $this->vis(Js::class);
    }
}