<?php

namespace Kyegil\Leiebasen\Oppslag\Offentlig;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\CmsSide;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Visning\felles\html\shared\body\main\Kort;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\Head;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\Html;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class Side extends \Kyegil\Leiebasen\Oppslag\OffentligKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
//        'oppgave' => Side\Oppdrag::class
    ];
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $id = !empty($_GET['id']) ? $_GET['id'] : '';
        /** @var CmsSide $cmsSide */
        $cmsSide = CmsSide::hentCmsSide($this, 'offentlig', $id);
        $this->hoveddata['cms_side'] = $cmsSide;
        $this->hoveddata['side_id'] = $cmsSide ? $cmsSide->hentId() : null;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var CmsSide $cmsSide */
        $cmsSide = $this->hoveddata['cms_side'];
        if(!$cmsSide || !$cmsSide->hentId()) {
            return false;
        }
        $this->tittel = $cmsSide->hentTittel();
        return true;
    }

    /**
     * @return Kort
     * @throws Exception
     */
    public function innhold()
    {
        /** @var CmsSide $cmsSide */
        $cmsSide = $this->hoveddata['cms_side'];

        /** @var Kort $kort */
        $kort = $this->vis(Kort::class, [
            'innhold' => $cmsSide->hentHtml(),
        ]);
        return $kort;
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function skript(): ViewInterface
    {
        /** @var CmsSide $cmsSide */
        $cmsSide = $this->hoveddata['cms_side'];

        $skript = new ViewArray([$cmsSide->hentScript()]);
        return $skript;
    }

    public function hentRespons()
    {
        /** @var CmsSide $cmsSide */
        $cmsSide = $this->hoveddata['cms_side'];
        if(!isset($this->respons) && trim($cmsSide->hentCss())) {
            /** @var Respons $respons */
            $respons = parent::hentRespons();
            /** @var Html $html */
            $html = $respons->hentInnhold();
            /** @var Head $htmlHead */
            $htmlHead = $html->hent('head');
            $scripts = $htmlHead->hent('scripts');
            $scripts = $scripts instanceof ViewArray ? $scripts : new ViewArray(trim($scripts) ? [$scripts] : []);
            $scripts->addItem(new HtmlElement('style', [], $cmsSide->hentCss()));
            $htmlHead->sett('scripts', $scripts);
        }
        return parent::hentRespons();
    }
}