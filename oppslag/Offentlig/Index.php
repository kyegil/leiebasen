<?php

namespace Kyegil\Leiebasen\Oppslag\Offentlig;

use Exception;
use Kyegil\Leiebasen\Oppslag\Offentlig\Index\Oppdrag;
use Kyegil\Leiebasen\Oppslag\OffentligKontroller;
use Kyegil\MysqliConnection\MysqliConnection;
use Kyegil\MysqliConnection\MysqliConnectionInterface;

class Index extends OffentligKontroller
{
    protected $oppdrag = [
        'avslutt' => Oppdrag::class
    ];
}