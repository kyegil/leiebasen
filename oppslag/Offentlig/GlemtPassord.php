<?php

namespace Kyegil\Leiebasen\Oppslag\Offentlig;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Oppslag\OffentligKontroller;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\Html;

class GlemtPassord extends OffentligKontroller
{
    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = 'Vet du ikke ditt brukernavn eller passord?';
        /** @var Html $innhold */
        $innhold = $this->hentRespons()->hentInnhold();
        $htmlHead = $innhold->getData('head');

        $reCaptcha3SiteKey = \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['google']['recaptcha3']['site_key'] ?? null;
        if ($reCaptcha3SiteKey) {
        $htmlHead
            /**
             * Google reCaptcha
             * @link https://developers.google.com/recaptcha/docs/v3
             */
            ->leggTil('scripts', new HtmlElement('script', [
                    'src' => "https://www.google.com/recaptcha/api.js?render=" . $reCaptcha3SiteKey
                ]));
        }

        $htmlHead
            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]));

        return true;
    }

    /**
     * @return Visning|\Kyegil\ViewRenderer\ViewInterface|string
     */
    public function innhold()
    {
        return $this->vis(Visning\offentlig\html\glemt_passord_skjema\Index::class, [
            'reCaptchaSiteKey' => \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['google']['recaptcha3']['site_key'] ?? ''
        ]);
    }
}