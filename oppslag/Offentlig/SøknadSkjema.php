<?php

namespace Kyegil\Leiebasen\Oppslag\Offentlig;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Søknad\Type;
use Kyegil\Leiebasen\Oppslag\OffentligKontroller;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\Head;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\Html;
use Kyegil\Leiebasen\Visning\offentlig\js\søknad_skjema\body\main\Index as Js;
use Kyegil\ViewRenderer\ViewArray;

class SøknadSkjema extends OffentligKontroller
{
    protected $tilgjengeligeSpråk = [
        'nb-NO' => 'Norsk (Bokmål)',
        'en' => 'English'
    ];

    /**
     * SøknadSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        if(isset($_GET['l'])) {
            $this->settSpråk($_GET['l']);
        }
        $søknadSkjemaId = 1; // !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Type $søknadSkjema */
        $søknadSkjema = $this->hentModell(Type::class, $søknadSkjemaId);
        $this->hoveddata['søknadsskjema'] = $søknadSkjema;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Type|null $skjema */
        $skjema = $this->hoveddata['søknadsskjema'];
        if (!$skjema || !$skjema->hentId()) {
            return false;
        }
        $språk = $this->hentSpråk();
        $this->tittel = $skjema->hentKonfigurering('tittel', $språk);
        /** @var Html $innhold */
        $innhold = $this->hentRespons()->hentInnhold();
        $innhold->hent('header')->sett('språkvelger', $this->lagSpråkvelger());
        /** @var Head $htmlHead */
        $htmlHead = $innhold->getData('head');

        $reCaptcha3SiteKey = \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['google']['recaptcha3']['site_key'] ?? null;
        if ($reCaptcha3SiteKey) {
        $htmlHead
            /**
             * Google reCaptcha
             * @link https://developers.google.com/recaptcha/docs/v3
             */
            ->leggTil('scripts', new HtmlElement('script', [
                    'src' => "https://www.google.com/recaptcha/api.js?render=" . $reCaptcha3SiteKey
                ]));
        }

        $htmlHead
            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]));

        return true;
    }

    /**
     * @return Visning|\Kyegil\ViewRenderer\ViewInterface|string
     */
    public function innhold()
    {
        /** @var Type $skjema */
        $skjema = $this->hoveddata['søknadsskjema'];
        $språk = $this->hentSpråk();

        return $this->vis(Visning\offentlig\html\søknad_skjema\body\main\Index::class, [
            'søknadSkjema' => $skjema,
            'språkKode' => $språk
        ]);
    }

    /**
     * @return Js|string
     */
    public function skript()
    {
        /** @var Type $skjema */
        $skjema = $this->hoveddata['søknadsskjema'];
        $språk = $this->hentSpråk();
        return $this->vis(Js::class, [
            'søknadSkjema' => $skjema,
            'språkKode' => $språk
        ]);
    }

    /**
     * @return ViewArray
     */
    protected function lagSpråkvelger()
    {
        $alternativeSpråk = $this->tilgjengeligeSpråk;
        unset($alternativeSpråk[$this->hentSpråk()]);
        $språkVelger = new ViewArray();
        foreach ($alternativeSpråk as $kode => $språk) {
            $a = new HtmlElement('a', [
                'href' => "/offentlig/index.php?oppslag=søknad_skjema&l={$kode}&returi=default",
                'hreflang' => $kode,
                'title' => $språk
            ], $språk);
            $språkVelger->addItem($a);
        }
        return $språkVelger;
    }

    /**
     * @return HtmlElement|string
     */
    public function visTilbakeknapp()
    {
        /** @var HtmlElement $tilbakeKnapp */
        $tilbakeKnapp = parent::visTilbakeknapp();
        if(!is_a($tilbakeKnapp, HtmlElement::class)) {
            return $tilbakeKnapp;
        }
        if ($this->hentSpråk() == 'en') {
            $tilbakeKnapp->setContents('Cancel')->setAttribute('title', 'Cancel');
        }
        else {
            $tilbakeKnapp->setContents('Avbryt');
        }
        return $tilbakeKnapp;
    }
}