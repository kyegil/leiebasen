<?php

namespace Kyegil\Leiebasen\Oppslag\Offentlig\Innlogging;


use Exception;
use Kyegil\Leiebasen\Autoriserer\LaminasAuth;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleie;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleie\Framleietaker;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleie\Framleietakersett;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietakersett;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Modell\Leieforhold\Regningsett;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Brukerprofil;
use Kyegil\Leiebasen\Modell\Person\Brukerprofil\Engangsbillett;
use Kyegil\Leiebasen\Modell\Person\Brukerprofilsett;
use Kyegil\Leiebasen\Modell\Personsett;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\epost\brukerprofil\Opprettelsesbekreftelse;
use Kyegil\Leiebasen\Visning\felles\epost\shared\Epost;
use stdClass;

class Mottak extends AbstraktMottak
{
    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
        ];
        $get = $_GET;
        $post = $_POST;
        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'innlogging':
                    $recaptcha = $post['g-recaptcha-response'] ?? null;
                    \Kyegil\Leiebasen\Oppslag\Offentlig\Innlogging\Mottak::validerRecaptcha($recaptcha);
                    $redirectUrl = $post['redirect'] ?? null;
                    $login = $post['login'] ?? '';
                    $passord = $post['passord'] ?? null;
                    /** @var LaminasAuth $autoriserer */
                    $autoriserer = $this->app->hentAutoriserer();
                    $resultat->success = $autoriserer->loggInn($login, $passord);
                    $resultat->url = $redirectUrl;
                    break;
                case 'registrer_bruker_skjema':
                    $resultat->msg = 'Brukerprofilen har blitt opprettet.';
                    $recaptcha = $post['g-recaptcha-response'] ?? null;
                    \Kyegil\Leiebasen\Oppslag\Offentlig\Innlogging\Mottak::validerRecaptcha($recaptcha);
                    $redirectUrl = $post['redirect'] ?? null;
                    $login = $post['login'] ?? '';
                    $passord = array_map('trim', $post['passord'] ?? []);
                    $fornavn = trim($post['fornavn'] ?? '');
                    $etternavn = trim($post['etternavn'] ?? '');
                    $identifiseringsmetode = trim($post['identifiseringsmetode'] ?? '');
                    $epost = trim($post['e-post'] ?? '');
                    $epost = filter_var($epost, FILTER_VALIDATE_EMAIL) ?: null;

                    $telefon = preg_replace("/^00/", '+', preg_replace("/[^0-9+]/", '', trim($post['telefon'] ?? '')));
                    $regningsdato = trim($post['regningsdato']);
                    $kid = trim($post['kid'] ?? '');
                    $regningsbeløp = trim($post['regningsbeløp'] ?? '0');
                    $erFramleier = !empty($post['er_framleier']);
                    $leietakerNavn = trim($post['leietaker_navn'] ?? '');

                    switch($identifiseringsmetode) {
                        case 'e-post':
                            if (!$epost) {
                                throw new Exception('Ugyldig e-postadresse.');
                            }
                            /** @var Personsett $adressesett */
                            $adressesett = $this->app->hentSamling(Person::class)
                                ->leggTilFilter(['epost' => $epost]);
                            if ($adressesett->hentAntall() < 1) {
                                throw new Exception('Denne epostadressen er ikke registrert. Vennligst velg en annen identifiseringsmetode, eller ta kontakt med en administrator for identifisering.');
                            }
                            if ($adressesett->hentAntall() > 1) {
                                throw new Exception('Denne epostadressen er registrert på flere personer. Vennligst velg en annen identifiseringsmetode, eller ta kontakt med en administrator for identifisering.');
                            }
                            /** @var Person $adressekort */
                            $adressekort = $adressesett->hentFørste();
                            if ($adressekort->hentBrukerProfil()) {
                                throw new Exception('Det finnes allerede en brukerprofil knyttet til denne epostadressen. Prøv å logge inn, eller klikk på knappen for glemt passord dersom du har glemt dette eller brukernavnet ditt.');
                            }
                            $login = $this->validerLogin($login);

                            $resultat->msg = 'Brukerprofilen har blitt opprettet. En e-post har blitt sent til ' . $epost . ' med instruksjoner for hvordan du kan logge inn første gang og velge passord.';

                            /** @var Brukerprofil $brukerprofil */
                            $brukerprofil = $this->app->nyModell(Brukerprofil::class, (object)[
                                'login' => $login,
                                'passord' => password_hash(md5(rand(0, 100000)), PASSWORD_DEFAULT),
                                'person' => $adressekort
                            ]);
                            $adressekort->tildelAdgangTilEgneLeieforhold();

                            $engangsbillett = $brukerprofil->opprettEngangsbillett($this->app->http_host . '/mine-sider/index.php?oppslag=profil_skjema&returi=default');
                            $this->sendBekreftelsesEpost($brukerprofil, $engangsbillett);
                            break;
                        case 'sms':
                            throw new Exception('Identifiserings via SMS er for øyeblikket ikke mulig');
                        case 'regning':
                            if (!$kid) {
                                throw new Exception('KID er påkrevd ved identifisering med regning');
                            }
                            /** @var Regningsett $regningsett */
                            $regningsett = $this->app->hentSamling(Regning::class)
                                ->leggTilFilter(['kid' => $kid])
                                ->leggTilFilter(['utskriftsdato >= DATE_SUB(NOW(), INTERVAL 6 MONTH)' ]);
                            /** @var Regning $regning */
                            $regning = $regningsett->hentFørste();
                            if (!$regning
                                || $regning->utskriftsdato->format('Y-m-d') != $regningsdato
                                || $regning->hentBeløp() != $regningsbeløp
                            ) {
                                throw new Exception('De oppgitte detaljene kunne ikke verifiseres. Husk at regningen ikke kan være mer enn 6 mnd gammel, og at du ikke kan bruke leieforholdets faste KID.');
                            }
                            $leieforhold = $regning->leieforhold;

                            /** @var Leietakersett $leietakere */
                            $leietakere = $leieforhold->hentLeietakere()
                                ->leggTilLeftJoin(Person::hentTabell(),
                                    '`' . Leietaker::hentTabell() . '`.`person` = `' . Person::hentTabell() . '`.`' . Person::hentPrimærnøkkelfelt() . '`'
                                )
                                ->leggTilFilter(
                                    ['CONCAT(TRIM(`' . Person::hentTabell() . '`.`fornavn`), " ", TRIM(`' . Person::hentTabell() . '`.`etternavn`)) LIKE' => trim($erFramleier? $leietakerNavn : ($fornavn . ' ' . $etternavn))]
                                );
                            /** @var Leietaker|null $leietaker */
                            $leietaker = $leietakere->hentFørste();

                            if($erFramleier) {
                                /** @var Framleietakersett $framleietakersett */
                                $framleietakersett = $this->app->hentSamling(Framleietaker::class);
                                $framleietakersett
                                    ->leggTilInnerJoinForFramleieforhold()
                                    ->leggTilInnerJoinForPerson()
                                    ->leggTilFilter(['`' . Framleie::hentTabell() . '`.`leieforhold`' => $leieforhold])
                                    ->leggTilFilter(
                                        ['CONCAT(TRIM(`' . Person::hentTabell() . '`.`fornavn`), " ", TRIM(`' . Person::hentTabell() . '`.`etternavn`)) LIKE' => trim($fornavn . ' ' . $etternavn)]
                                    )
                                ;
                                /** @var Framleietaker|null $framleietaker */
                                $framleietaker = $framleietakersett->hentFørste();
                                $bruker = $leietaker && $framleietaker ? $framleietaker->person : null;
                            }
                            else {
                                $bruker = $leietaker ? $leietaker->person : null;
                            }
                            if (!$bruker) {
                                throw new Exception('De oppgitte detaljene kunne ikke verifiseres. Husk at regningen ikke kan være mer enn 6 mnd gammel, og at du ikke kan bruke leieforholdets faste KID.');
                            }
                            if ($bruker->hentBrukerProfil()) {
                                throw new Exception('Du har allerede en brukerprofil. Prøv å logge inn, eller klikk på knappen for glemt passord dersom du har glemt dette eller brukernavnet ditt.');
                            }

                            $login = $this->validerLogin($login);
                            $passordHash = Brukerprofil::validerPassord($passord);
                            /** @var Brukerprofil $brukerprofil */
                            $this->app->nyModell(Brukerprofil::class, (object)[
                                'login' => $login,
                                'passord' => $passordHash,
                                'person' => $bruker
                            ]);
                            $bruker->tildelAdgangTilEgneLeieforhold();
                            if ($epost) {
                                $bruker->epost = $epost;
                            }
                            if (strlen($telefon) >= 8) {
                                $bruker->mobil = $telefon;
                            }
                            $resultat->url = $redirectUrl;
                            $this->app->hentAutoriserer()->loggInn($login, reset($passord));
                            break;
                        default:
                            throw new Exception('Identifiseringsmetode mangler');
                    }
                    break;
                case 'engangsbillett':
                    $epost = trim($post['epost'] ?? '');
                    $epost = filter_var($epost, FILTER_VALIDATE_EMAIL) ?: null;
                    if(!$epost) {
                        throw new Exception('Ugyldig e-postadresse.');
                    }
                    $resultat->msg = "Engangsbillett har blitt sendt til {$epost}.";
                    /** @var Personsett $personsett */
                    $personsett = $this->app->hentSamling(Person::class)
                        ->leggTilFilter(['epost' => $epost]);
                    if ($personsett->hentAntall() < 1) {
                        throw new Exception('Denne epostadressen er ikke registrert.');
                    }
                    /** @var Brukerprofilsett $brukerprofilsett */
                    $brukerprofilsett = $this->app->hentSamling(Brukerprofil::class)
                        ->leggTilLeftJoinForPerson()
                        ->leggTilFilter(['`' . Person::hentTabell() .'`.`epost`' => $epost])
                    ;
                    if ($brukerprofilsett->hentAntall() < 1) {
                        throw new Exception('Det er ikke registrert noen brukerprofil på denne e-postadressen.');
                    }
                    foreach($brukerprofilsett as $brukerprofil) {
                        $this->sendEngangsbillett($brukerprofil->opprettEngangsbillett());
                    }

                    break;

                case 'engangsbillett_innlogging':
                    $engangsbillett = $post['engangsbillett'] ?? '';
                    $autoriserer = $this->app->hentAutoriserer();
                    $resultat->url = $autoriserer->loggInnMedEngangsbillett($engangsbillett);
                    break;

                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param string|null $recaptcha
     * @throws Exception
     */
    public static function validerRecaptcha(?string $recaptcha = null)
    {
        $reCaptcha3SiteKey = \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['google']['recaptcha3']['site_key'] ?? null;
        $reCaptcha3SecretKey = \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['google']['recaptcha3']['secret_key'] ?? null;
        if ($reCaptcha3SiteKey && $reCaptcha3SecretKey) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => $reCaptcha3SecretKey, 'response' => $recaptcha)));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            $responseObject = json_decode($response);
            if(!$responseObject->success) {
                throw new Exception('verifisering av reCaptcha mislyktes. Feilkoder: ' . implode(',', $responseObject->{'error-codes'}));
            }
        }
    }

    /**
     * @param string $login
     * @return string
     * @throws Exception
     */
    private function validerLogin(string $login): string
    {
        $login = trim($login);
        if (mb_strlen($login) < 3) {
            throw new Exception('Brukernavnet er for kort. Bruk minst 3 tegn.');
        }
        /** @var Brukerprofilsett $brukersett */
        $brukersett = $this->app->hentSamling(Brukerprofil::class)->leggTilFilter(['login' => $login]);
        if ($brukersett->hentAntall()) {
            throw new Exception('Brukernavnet er allerede i bruk. Prøv et annet brukernavn.');
        }
        return $login;
    }

    /**
     * @param Brukerprofil $brukerprofil
     * @param Engangsbillett|null $engangsbillett
     * @return $this
     * @throws Exception
     */
    private function sendBekreftelsesEpost(Brukerprofil $brukerprofil, ?Engangsbillett $engangsbillett)
    {
        $person = $brukerprofil->person;
        /** @var Opprettelsesbekreftelse $opprettelsesbekreftelse */
        $opprettelsesbekreftelse = $this->app->vis(Opprettelsesbekreftelse::class, [
            'engangsbillett' => $engangsbillett,
            'brukerprofil' => $brukerprofil
        ]);
        $subject = 'Bekreftelse på opprettet brukerprofil';
        $this->app->sendMail((object)[
            'to' => $person->hentNavn() . "<$person->epost>",
            'subject' => $subject,
            'html' => $this->app->hentVisning(Epost::class, [
                'tittel' => $subject,
                'innhold' => $opprettelsesbekreftelse
            ]),
            'text' => (clone $opprettelsesbekreftelse)->settMal('felles/epost/brukerprofil/Opprettelsesbekreftelse.txt'),
            'type' => 'brukerprofil_bekreftelse',
        ]);
        return $this;
    }

    /**
     * @param Engangsbillett|null $engangsbillett
     * @return Mottak
     */
    private function sendEngangsbillett(?Engangsbillett $engangsbillett): Mottak
    {
        if ($engangsbillett) {
            $brukerprofil = $engangsbillett->brukerprofil;
            $person = $brukerprofil->person;
            /** @var Opprettelsesbekreftelse $epostinnhold */
            $epostinnhold = $this->app->vis(Visning\felles\epost\brukerprofil\Engangsbillett::class, [
                'engangsbillett' => $engangsbillett,
                'brukerprofil' => $brukerprofil
            ]);
            $subject = 'Engangsbillett';
            $this->app->sendMail((object)[
                'to' => $person->hentNavn() . "<$person->epost>",
                'subject' => $subject,
                'html' => $this->app->hentVisning(Epost::class, [
                    'tittel' => $subject,
                    'innhold' => $epostinnhold
                ]),
                'text' => (clone $epostinnhold)->settMal('felles/epost/brukerprofil/Engangsbillett.txt'),
                'type' => 'brukerprofil_engangsbillett',
            ]);
        }
        return $this;
    }
}