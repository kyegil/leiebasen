<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\ProfilSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var \stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Profilen er lagret."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = isset($get['skjema']) ? $get['skjema'] : '';
        try {
            switch ($skjema) {
                case 'passord':
                    $resultat->msg = "Passordet er endret.";
                    $brukerProfilId = isset($post['id']) ? $post['id'] : null;
                    $pw1 = isset($post['pw1']) ? $post['pw1'] : null;
                    $pw2 = isset($post['pw2']) ? $post['pw2'] : null;

                    if($pw1 != $pw2) {
                        throw new Exception('Passordene stemmer ikke overens.');
                    }
                    if(
                        (isset(Person\Brukerprofil::$passordLengde) && strlen($pw1) < Person\Brukerprofil::$passordLengde)
                        || 1 !== preg_match('/' . Person\Brukerprofil::$passordRegex . '/', $pw1)) {
                        throw new Exception(Person\Brukerprofil::$passordHint);
                    }

                    /** @var Person\Brukerprofil $profil */
                    $profil = $this->app->hentModell(Person\Brukerprofil::class, $brukerProfilId);
                    $profil->settPassord(\Kyegil\Leiebasen\Modell\Person\Brukerprofil::validerPassord([$pw1, $pw2]));
                    $resultat->url = '/drift/index.php?oppslag=profil_skjema&id=' . $profil->person->hentId();

                    break;
                case 'brukerprofil':
                    $brukerId = $get['id'] ?? null;
                    $brukerProfilId = $post['id'] ?? null;
                    /** @var Person\Brukerprofil|null $brukerProfil */
                    $brukerProfil = $this->app->hentModell(Person\Brukerprofil::class, $brukerProfilId);
                    /** @var Person|null $bruker */
                    $bruker = $brukerProfil->hentPerson() ?? $this->app->hentModell(Person::class, $brukerId);
                    $login = $post['login'] ?? null;
                    $epost = $post['epost'] ?? null;
                    $adgangIder = $post['adganger'] ?? [];

                    $this->validerEpost($epost);

                    if(!$bruker->hentId()) {
                        throw new Exception('Ukjent person');
                    }

                    $eksisterendeAdganger = $bruker->hentAdganger();

                    if(!$brukerProfil->hentId() && $brukerProfilId != '*') {
                        throw new Exception('Ukjent brukerprofil');
                    }
                    if($brukerProfil->hentId()) {
                        $this->validerLogin($brukerProfil, $login);
                    }
                    else if ($this->app->hentAutoriserer()->erLoginTilgjengelig($login, $bruker->hentId())) {
                        /** @var Person\Brukerprofil $brukerProfil */
                        $brukerProfil = $this->app->nyModell(Person\Brukerprofil::class, (object)[
                            'login' => $login,
                            'person' => $bruker
                        ]);
                    }

                    $brukerProfil->settBrukernavn($login);
                    $bruker->settEpost($epost);

                    foreach ($eksisterendeAdganger as $adgang) {
                        if(!in_array($adgang->hentId(), $adgangIder)) {
                            $adgang->slett();
                        }
                    }

                    break;
                default:
                    $resultat->success = true;
                    break;
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param Person\Brukerprofil $brukerprofil
     * @param string $login
     * @return $this
     * @throws Exception
     */
    protected function validerLogin(Person\Brukerprofil $brukerprofil, string $login): Mottak
    {
        if(!$brukerprofil->kanBrukernavnBenyttes($login)) {
            throw new Exception('Dette brukernavnet kan ikke benyttes. Det er allerede i bruk.');
        }
        return $this;
    }

    /**
     * @param string $epost
     * @return $this
     * @throws Exception
     */
    protected function validerEpost(string $epost): Mottak
    {
        $this->app->validerEpost($epost);
        return $this;
    }
}