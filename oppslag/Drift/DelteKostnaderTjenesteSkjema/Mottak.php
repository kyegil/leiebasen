<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderTjenesteSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;

class Mottak extends AbstraktMottak
{
    /**
     * @param string|null $skjema
     * @return $this
     */
    public function taIMot($skjema = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($skjema) {
                default:
                    $tjenesteId = $this->data['get']['id'] ?? null;

                    $type = isset($this->data['post']['type']) ? trim($this->data['post']['type']) : Krav::TYPE_ANNET;
                    $navn = $this->data['post']['navn'] ?? null;
                    $leverandør = $this->data['post']['leverandør'] ?? null;
                    $avtalereferanse = $this->data['post']['avtalereferanse'] ?? null;
                    $beskrivelse = $this->data['post']['beskrivelse'] ?? null;

                    if (!$tjenesteId) {
                        throw new Exception('Tjeneste-id er ikke angitt');
                    }

                    switch ($type) {
                        case Krav::TYPE_STRØM:
                            $målernummer = $this->data['post']['målernummer'] ?? null;
                            $plassering = $this->data['post']['plassering'] ?? null;
                            if($tjenesteId == '*') {
                                /** @var Strømanlegg $tjeneste */
                                $tjeneste = $this->app->nyModell(Strømanlegg::class, (object)[
                                    'type' => $type,
                                    'navn' => $navn,
                                    'målernummer' => $målernummer,
                                    'plassering' => $plassering,
                                ]);
                            }
                            else {
                                /** @var Tjeneste $tjeneste */
                                $tjeneste = $this->app->hentModell(Strømanlegg::class, $tjenesteId);
                            }
                            if (!$tjeneste->hentId()) {
                                throw new Exception('Ugyldig id');
                            }
                            if($tjenesteId != '*') {
                                $tjeneste
                                    ->settMålernummer($målernummer)
                                    ->settPlassering($plassering)
                                ;
                            }
                            break;
                        default:
                            if($tjenesteId == '*') {
                                /** @var Tjeneste $tjeneste */
                                $tjeneste = $this->app->nyModell(Tjeneste::class, (object)[
                                    'type' => $type,
                                    'navn' => $navn,
                                    'avtalereferanse' => $avtalereferanse,
                                    'beskrivelse' => $beskrivelse,
                                ]);
                            }
                            else {
                                /** @var Tjeneste $tjeneste */
                                $tjeneste = $this->app->hentModell(Tjeneste::class, $tjenesteId);
                            }
                            if (!$tjeneste->hentId()) {
                                throw new Exception('Ugyldig id');
                            }
                    }

                    $tjeneste
                        ->settNavn($navn)
                        ->settAvtalereferanse($avtalereferanse)
                        ->settLeverandør($leverandør)
                        ->settBeskrivelse($beskrivelse)
                    ;

                    $resultat->id = $tjeneste->hentId();
                    $resultat->msg = 'Lagret';
                    $resultat->url = strval($this->app->returi->get(1));
                    break;
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

}