<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\StrømavstemmingSkjema;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav\Delkrav;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav\LeieforholdDelkravtype;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav\LeieforholdDelkravtypesett;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\StrømavstemmingSkjema
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param null $data
     * @return $this|Utlevering
     */
    public function hentData($data = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case "enkeltforhold":
                    /** @var DateTime|null $tildato */
                    $tildato = isset($_GET['tildato']) ? new DateTime($_GET['tildato']) : null;
                    $leieforholdId = isset($_GET['leieforhold']) ? $_GET['leieforhold'] : null;

                    /** @var LeieforholdDelkravtype|null $leieforholdFellesstrømForhold */
                    $leieforholdFellesstrømForhold = $this->app->hentSamling(LeieforholdDelkravtype::class)
                        ->leggTilFilter([
                            'delkravtype' => $this->app->hentValg('delkravtype_fellesstrøm'),
                            'leieforhold' => $leieforholdId,
                            'periodisk_avstemming' => true
                        ])
                        ->hentFørste()
                    ;
                    if(!is_a($leieforholdFellesstrømForhold, LeieforholdDelkravtype::class)) {
                        throw new Exception('Fant ikke Fellesstrøm-delkravtype for dette leieforholdet');
                    }

                    $resultat->data = $this->hentLeieforholdData($leieforholdFellesstrømForhold, $tildato);
                    break;
                default:
                    {
                        /** @var string $sort */
                        $sort = $_GET['sort'] ?? '';
                        /** @var bool $synkende */
                        $synkende = isset($_GET['dir']) && $_GET['dir'] == "DESC";
                        $start = $_GET['start'] ?? 0;
                        $limit = $_GET['limit'] ?? null;

                        /** @var LeieforholdDelkravtypesett $leieforholdFellesstrømForhold */
                        $leieforholdFellesstrømForhold = $this->app->hentSamling(LeieforholdDelkravtype::class)
                            ->leggTilLeftJoin('oppsigelser', LeieforholdDelkravtype::getDbTable() . '.leieforhold = oppsigelser.leieforhold')
                            ->leggTilFilter([
                                'delkravtype' => $this->app->hentValg('delkravtype_fellesstrøm'),
                                'periodisk_avstemming' => true,
                                'oppsigelser.leieforhold' => null
                            ]);

                        /** @var LeieforholdDelkravtype $forhold */
                        foreach ($leieforholdFellesstrømForhold as $forhold) {
                            /** @var \stdClass $data */
                            $data = $this->hentLeieforholdData($forhold);
                            if($data->har_beregningsgrunnlag) {
                                $resultat->data[] = $data;
                            }
                        }
                        $resultat->data = $this->app->sorterObjekter($resultat->data, $sort, $synkende);
                        $resultat->totalRows = count($resultat->data);
                        $resultat->data = array_slice($resultat->data, $start, $limit);
                    }
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * Hent avstemmingsdata for et enkelt leieforhold
     *
     * @param LeieforholdDelkravtype $forhold
     * @return \stdClass
     * @throws Exception
     */
    public function hentLeieforholdData(LeieforholdDelkravtype $forhold, DateTime $avregningsdato = null): object
    {
        $leieforhold = $forhold->leieforhold;
        $sisteAvregningsdato = $leieforhold->hentSisteFellesstrømKravdato();
        $sumBeregnet = 0;
        $sumKrav = 0;
        $kredittliste = [];
        $debetliste = [];
        $tildato = clone $sisteAvregningsdato;

        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andelsett $strømandelerSidenSisteAvregning */
        $strømandelerSidenSisteAvregning = $leieforhold->hentStrømAndeler()
            ->leggTilFilter([
                'fom >=' => $sisteAvregningsdato->format('Y-m-d')
            ]);

        $harBeregningsgrunnlag = $strømandelerSidenSisteAvregning->hentAntall() ? true : false;

        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad\Andel $andel */
        foreach ($strømandelerSidenSisteAvregning as $andel) {
            if (!isset($avregningsdato) || $andel->fom < $avregningsdato) {
                $sumBeregnet += $andel->beløp;
                $debetliste[] = "{$andel->fom->format('d.m.Y')}&nbsp;–&nbsp;{$andel->tom->format('d.m.Y')}: {$this->app->kr($andel->beløp)}";
                if($andel->tom > $tildato) {
                    $tildato = clone $andel->tom;
                }
            }
        }

        if(isset($avregningsdato)) {
            $tildato = $avregningsdato;
        }

        if ($forhold->selvstendigTillegg) {
            $kravSidenSisteAvregning = $this->app->hentSamling(Krav::class)
                ->leggTilFilter(['leieforhold' => $leieforhold->hentId()])
                ->leggTilFilter([
                    'type' => $forhold->delkravtype->kode,
                    'fom >=' => $sisteAvregningsdato->format('Y-m-d'),
                    'fom <' => $tildato->format('Y-m-d')
                ])
                ->leggTilSortering('fom');
        } else {
            $kravSidenSisteAvregning = $this->app->hentSamling(Delkrav::class)
                ->leggTilInnerJoin('krav', Delkrav::getDbTable() . '.kravid = krav.id')
                ->leggTilFilter([
                    'delkravtype' => $forhold->delkravtype->id,
                    'krav.fom >=' => $sisteAvregningsdato->format('Y-m-d'),
                    'krav.fom <' => $tildato->format('Y-m-d')
                ])
                ->leggTilSortering('krav.fom');
        }

        /** @var Krav|\Kyegil\Leiebasen\Modell\Leieforhold\Krav\Delkrav $krav */
        foreach ($kravSidenSisteAvregning as $krav) {
            $sumKrav += $krav->beløp;
            if ($krav instanceof Delkrav) {
                $kredittliste[] = "{$krav->krav->termin}: {$this->app->kr($krav->beløp)}";
            } else {
                $kredittliste[] = "{$krav->termin}: {$this->app->kr($krav->beløp)}";
            }
        }
        $balanse = $sumKrav - $sumBeregnet;

        $satsForslag = $this->foreslåÅrligSats($sisteAvregningsdato, $tildato, $sumBeregnet, $forhold->relativ, $leieforhold->årligBasisleie);

        return (object)[
            'leieforhold' => $leieforhold->hentId(),
            'beskrivelse' => $leieforhold->beskrivelse,
            'siste_avregningsdato' => $sisteAvregningsdato->format('Y-m-d'),
            'nylig_avregnet' => $sisteAvregningsdato > date_create(date('Y') - 1 . date('-m-d')),
            'til' => $tildato->format('Y-m-d'),
            'balanse' => $balanse,
            'relativ' => $forhold->relativ,
            'sats' => $forhold->sats,
            'har_beregningsgrunnlag' => $harBeregningsgrunnlag,
            'ny_sats' => $harBeregningsgrunnlag ? $satsForslag : $forhold->sats,
            'avregning' => -$balanse,
            'transaksjoner' => (string) new HtmlElement('table', ['class' => 'dataload', 'width' => '100%'],
                [
                    new HtmlElement('tr', [],
                        [new HtmlElement('th', [],
                            ['Beregnet']
                        ),
                        new HtmlElement('th', [],
                            ['Krevd']
                        )]
                    ),
                    new HtmlElement('tr', [],
                        [new HtmlElement('td', [],
                            [addslashes(implode('<br>', $debetliste))]
                        ),
                        new HtmlElement('td', [],
                            [addslashes(implode('<br>', $kredittliste))]
                        )]
                    ),
                    new HtmlElement('tr', [],
                        [new HtmlElement('td', ['class' => 'summary'],
                            [$this->app->kr($sumBeregnet)]
                        ),
                        new HtmlElement('td', ['class' => 'summary'],
                            [$this->app->kr($sumKrav)]
                        )]
                    )
                ]
            )
        ];
    }

    /**
     * @param DateTime $fra
     * @param DateTime $til
     * @param $forbruk
     * @param bool $relativ
     * @param null $basisleie
     * @return float
     */
    private function foreslåÅrligSats(DateTime $fra, DateTime $til, $forbruk, bool $relativ, $basisleie = 0)
    {
        $antallDager = $til->diff($fra, true)->days + 1;
        $årsforbruk = $forbruk * 365 / $antallDager;

        if($relativ) {
            $sats = round($årsforbruk / $basisleie, 3);
        }
        else {
            $sats = round($årsforbruk);
        }
        return $sats;
    }
}