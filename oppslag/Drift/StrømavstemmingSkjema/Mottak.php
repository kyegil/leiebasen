<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 17/06/2020
 * Time: 13:33
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\StrømavstemmingSkjema;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav\LeieforholdDelkravtype;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Mottak extends AbstraktMottak
{
    /**
     * @param string|null $skjema
     * @return $this|Mottak
     */
    public function taIMot($skjema = null) {
        /** @var Utlevering $utlevering */
        $utlevering = $this->app->hentUtlevering();
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($skjema) {
                default:
                    if(!isset($_POST['leieforhold'])) {
                        throw new Exception('Leieforhold er ikke angitt');
                    }
                    if(!isset($_POST['fra'])) {
                        throw new Exception('Siste Avregningsdato er ikke angitt');
                    }
                    if(!isset($_POST['avregningsdato'])) {
                        throw new Exception('Avregningsdato er ikke angitt');
                    }
                    if(!isset($_POST['sats'])) {
                        throw new Exception('Ny sats er ikke angitt');
                    }
                    if(!isset($_POST['avregningsbeløp'])) {
                        throw new Exception('Avregningsbeløp er ikke angitt');
                    }
                    $leieforholdId = $_POST['leieforhold'];
                    $fra = new DateTime($_POST['fra']);
                    $avregningsdato = new DateTime($_POST['avregningsdato']);
                    $sats = $_POST['sats'];
                    $avregningsbeløp = $_POST['avregningsbeløp'];

                    /** @var LeieforholdDelkravtype|null $leieforholdFellesstrømForhold */
                    $leieforholdFellesstrømForhold = $this->app->hentSamling(LeieforholdDelkravtype::class)
                        ->leggTilFilter([
                            'delkravtype' => $this->app->hentValg('delkravtype_fellesstrøm'),
                            'leieforhold' => $leieforholdId,
                            'periodisk_avstemming' => true
                        ])
                        ->hentFørste()
                    ;
                    /** @var Leieforhold $leieforhold */
                    $leieforhold = $leieforholdFellesstrømForhold->leieforhold;
                    if(!is_a($leieforholdFellesstrømForhold, LeieforholdDelkravtype::class)) {
                        throw new Exception('Fant ikke Fellesstrøm-delkravtype for dette leieforholdet');
                    }

                    /**
                     * Sett ny sats på delkravet
                     */
                    $leieforholdFellesstrømForhold->settSats($sats);
                    /** Opprett Krav eller kreditt for avstemming */
                    $this->app->nyModell(Krav::class, (object)[
                        'kontrakt' => $leieforhold->kontrakt,
                        'leieforhold' => $leieforhold,
                        'type' => Krav::TYPE_STRØM,
                        'kravdato' => new DateTime(),
                        'fom' => $fra,
                        'tom' => $avregningsdato,
                        'tekst' => 'Avstemming av innkrevd for fellesstrøm',
                        'termin' => "{$fra->format('d.m.Y')}–{$avregningsdato->format('d.m.Y')}",
                        'beløp' => $avregningsbeløp
                    ]);

                    $leieforholdFellesstrømForhold->leieforhold->opprettLeiekrav($avregningsdato);
                    $resultat->data = $utlevering->hentLeieforholdData($leieforholdFellesstrømForhold);
                    break;
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}