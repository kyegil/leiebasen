<?php
/**
 * Part of boligstiftelsen.svartlamon.org
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\Eksport;


use DateTimeImmutable;
use Exception;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Eksportprofil;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløpsett;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Modell\Leieforhold\Regningsett;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Respons\CsvRespons;
use ZipArchive;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\Eksport
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return Utlevering
     */
    public function hentData($data = null):Utlevering {
        try {
            switch ($data) {
                case 'backup':
                    $fil = Leiebase::$config['leiebasen']['server']['db_backup'] ?? null;
                    if(!$fil) {
                        throw new Exception('Lokalisering av backup er ikke angitt i config.php (leiebasen.server.db_backup)');
                    }
                    if(!file_exists($fil)) {
                        throw new Exception('Backup ble ikke funnet.');
                    }
                    if(!is_readable($fil)) {
                        throw new Exception('Backup kan ikke leses på grunn av for restriktive filtillatelser');
                    }
                    $filending = pathinfo($fil, PATHINFO_EXTENSION);
                    $filnavn = 'Leiebasen Backup ' . date('Y-m-d His', filectime($fil)) . '.' . $filending;
                    $mime = mime_content_type($fil) ?: 'application/octet-stream';
                    $respons = new Respons(file_get_contents($fil));
                    $respons->header("Content-Type: {$mime}; charset=utf-8");
                    $respons->header('Content-disposition: attachment; filename="' . $filnavn. '"');
                    $respons->header('Content-Length: ' . filesize($fil));
                    $this->settRespons($respons);
                    $this->app->settValg('backup_siste_nedlastet', time());
                    break;
                case 'eksport':
                    $get = $this->data['get'];
                    $get['fradato'] = isset($get['fradato']) && $get['fradato']
                        ? new DateTimeImmutable($get['fradato'])
                        : null;
                    $get['tildato'] = isset($get['tildato']) && $get['tildato']
                        ? new DateTimeImmutable($get['tildato'])
                        : null;
                    if(isset($get['skilletegn']) && $get['skilletegn'] == '\t') {
                        $get['skilletegn'] = "\t";
                    }
                    if (($get['type'] ?? null) == 'tilpasset') {
                        return $this->tilpassetEksport($get);
                    }
                    else {
                        return $this->standardEksport($get);
                    }

                default:
                    throw new Exception('Ønsket data ikke angitt');
            }
        } catch (Exception $e) {
            $respons = new Respons($e->getMessage());
            $respons->header(($_SERVER["SERVER_PROTOCOL"] ?? 'HTTP/1.1') . ' 500 Internal Server Error');
            $this->settRespons($respons);
        }
        return $this;
    }

    /**
     * Kjør en tilpasset eksport med utgangspunkt i en eksportprofil
     *
     * @param array $valg Associativt array, med nøklene
     *
     *  *
     * @return Utlevering
     * @throws Exception
     */
    private function tilpassetEksport(array $valg): Utlevering
    {
        list('valg' => $valg)
            = $this->app->before($this, __FUNCTION__, [
            'valg' => $valg
        ]);

        $eksportProfilId = $valg['profil']['tilpasset'] ?? null;
        /** @var Eksportprofil $eksportProfil */
        $eksportProfil = $this->app->hentModell(Eksportprofil::class, $eksportProfilId);
        if (!$eksportProfil->hentId()) {
            throw new Exception('Ugyldig eksportprofil');
        }
        $eksportOppsett = $eksportProfil->hentOppsett();

        if($eksportOppsett && isset($eksportOppsett->klasse)) {
            if(!is_a($eksportOppsett->klasse, Eksportprofil::class, true)) {
                throw new Exception('Finner ikke ekportprosessor ' . $eksportOppsett->klasse);
            }
            $eksportProfil = $this->app->hentModell($eksportOppsett->klasse, $eksportProfilId);
        }

        $resultat = $eksportProfil->eksporter($valg);

        $respons = new CsvRespons($resultat);
        if(isset($eksportOppsett->filnavn)) {
            $respons->filnavn = $eksportOppsett->filnavn . '_';
            if(isset($valg['fradato']) && isset($valg['tildato'])) {
                $respons->filnavn .= $valg['fradato']->format('Y-m-d') . '_' . $valg['tildato']->format('Y-m-d');
            }
            else if(isset($valg['fradato'])) {
                $respons->filnavn .= $valg['fradato']->format('Y-m-d');
            }
            else if(isset($valg['tildato'])) {
                $respons->filnavn .= $valg['tildato']->format('Y-m-d');
            }
            $respons->filnavn .= 'v(' . date('YmdHis') . ').csv';
        }
        $respons->separator = $valg['skilletegn'] ?? $respons->separator;
        $respons->enclosure = $valg['innpakning'] ?? $respons->enclosure;
        $this->settRespons($respons);
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @param array $valg
     * @return Utlevering
     * @throws Exception
     */
    private function standardEksport(array $valg): Utlevering
    {
        list('valg' => $valg)
            = $this->app->before($this, __FUNCTION__, [
            'valg' => $valg
        ]);
        $eksport = $valg['profil']['standard'] ?? null;
        $filnavn = $eksport;

        switch($eksport) {
            case 'leieforhold':
                $resultat = $this->hentLeieforholdSett($valg);
                break;
            case 'krav':
                $resultat = $this->hentKravSett($valg);
                break;
            case 'utestående':
                $resultat = $this->hentUteståendeKravSett($valg);
                $filnavn = 'utestående per ' . (isset($valg['tildato']) ? $valg['tildato']->format('Y-m-d') : '');
                break;
            case 'betalinger':
                $resultat = $this->hentDelbeløpSett($valg);
                break;
            case 'bygningsregnskap':
                $resultat = $this->hentBygningsregskapData($valg);
                break;
            case 'regninger_arkiv':
                $this->hentRegningsarkiv($valg);
                return $this->app->after($this, __FUNCTION__, $this);
            default:
                throw new Exception('Ugyldig eksportvalg');
        }

        $respons = new CsvRespons($resultat);
        $respons->filnavn = $filnavn . '.csv';
        $respons->separator = $valg['skilletegn'] ?? $respons->separator;
        $respons->enclosure = $valg['innpakning'] ?? $respons->enclosure;
        $this->settRespons($respons);
        return $this->app->after($this, __FUNCTION__, $this);
    }


    /**
     * @param array $valg
     * @return array
     * @throws Exception
     */
    private function hentLeieforholdSett(array $valg): array
    {
        list('valg' => $valg)
            = $this->app->before($this, __FUNCTION__, [
            'valg' => $valg
        ]);

        $resultat = [];
        /** @var Leieforholdsett $leieforholdsett */
        $leieforholdsett = $this->app->hentSamling(Leieforhold::class);
        $leieforholdsett->leggTilFilter(Eksportprofil::settDatofilter($valg, [
            'and' => [
                '`' . Leieforhold::hentTabell() . '`.`fradato` >=' => '{fra}',
                '`' . Leieforhold::hentTabell() . '`.`fradato` <=' => '{til}',
            ],
        ]));
        $leieforholdsett->leggTilSortering('fradato', false, Leieforhold::hentTabell());
        if($leieforholdsett->hentAntall()) {
            $overskrifter = $leieforholdsett->hentFørste()->getPropertyNames();
            $overskrifter[] = 'leietakere';
            $resultat[] = $overskrifter;
            /** @var Leieforhold $leieforhold */
            foreach ($leieforholdsett as $leieforhold) {
                $linje = [];
                foreach ($leieforhold->getFlatData() as $egenskap => $verdi) {
                    $linje[$egenskap] = Eksportprofil::formaterDato($valg['datoformat'] ?? 'Y-m-d', $verdi);
                }
                $linje['leietakere'] = $leieforhold->hentNavn();
                $resultat[] = $linje;
            }
        }
        return $this->app->after($this, __FUNCTION__, $resultat);
    }

    /**
     * @param array $valg
     * @return array
     * @throws Exception
     */
    private function hentBygningsregskapData(array $valg): array
    {
        list('valg' => $valg)
            = $this->app->before($this, __FUNCTION__, [
            'valg' => $valg
        ]);

        $resultat = [];
        $datasett = $this->app->mysqli->select([
            'source' => '(`'
                . Bygning::hentTabell() . '` INNER JOIN `' . Leieobjekt::hentTabell() . '` ON `'
                . Bygning::hentTabell() . '`.`' . Bygning::hentPrimærnøkkelfelt() . '` = `' . Leieobjekt::hentTabell() . '`.`bygning`
                INNER JOIN `' . Leieforhold::hentTabell() . '` ON `'
                . Leieforhold::hentTabell() . '`.`leieobjekt` = `' . Leieobjekt::hentTabell() . '`.`' . Leieobjekt::hentPrimærnøkkelfelt()
            . '`)'
            . 'LEFT JOIN `' . Krav::hentTabell() . '` ON `'
                . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '` = `' . Krav::hentTabell() . '`.`leieforhold`',
            'where' => Eksportprofil::settDatofilter($valg, [
                'and' => [
                    '`' . Krav::hentTabell() . '`.`kravdato` >=' => '{fra}',
                    '`' . Krav::hentTabell() . '`.`kravdato` <=' => '{til}',
                ],
            ]),
            'fields' => [
                'id' => '`' . Bygning::hentTabell() . '`.`' . Bygning::hentPrimærnøkkelfelt() . '`',
                'kode' => '`' . Bygning::hentTabell() . '`.`kode`',
                'bygning' => '`' . Bygning::hentTabell() . '`.`navn`',
                'leieinntekter' => 'SUM(if(`' . Krav::hentTabell() . '`.`type` = "' . Krav::TYPE_HUSLEIE . '", `' . Krav::hentTabell() . '`.`beløp`, null))',
                'annet' => 'SUM(if(`' . Krav::hentTabell() . '`.`type` != "' . Krav::TYPE_HUSLEIE . '", `' . Krav::hentTabell() . '`.`beløp`, null))',
                'sum' => 'SUM(`' . Krav::hentTabell() . '`.`beløp`)',
            ],
            'orderfields' => [
                '`' . Bygning::hentTabell() . '`.`kode`',
                '`' . Bygning::hentTabell() . '`.`' . Bygning::hentPrimærnøkkelfelt() . '`',
            ],
            'groupfields' => ['`' . Bygning::hentTabell() . '`.`' . Bygning::hentPrimærnøkkelfelt() . '`'],
            'having' => null,
            'sql' => null,
        ]);
        if($datasett->totalRows) {
            $resultat[] = array_keys((array)$datasett->data[0]);
            /** @var Modell $objekt */
            foreach ($datasett->data as $objekt) {
                foreach ($objekt as &$verdi) {
                    $verdi = Eksportprofil::formaterDato($valg['datoformat'] ?? 'Y-m-d', $verdi);
                }
                $resultat[] = $objekt;
            }
        }
        return $this->app->after($this, __FUNCTION__, $resultat);
    }

    /**
     * @param array $valg
     * @return array
     * @throws Exception
     */
    private function hentKravSett(array $valg): array
    {
        list('valg' => $valg)
            = $this->app->before($this, __FUNCTION__, [
            'valg' => $valg
        ]);

        $resultat = [];
        /** @var Kravsett $kravsett */
        $kravsett = $this->app->hentSamling(Krav::class);
        $kravsett->leggTilFilter(Eksportprofil::settDatofilter($valg, [
            'and' => [
                'kravdato >=' => '{fra}',
                'kravdato <=' => '{til}',
            ],
        ]));
        $kravsett->leggTilSortering('kravdato');
        if($kravsett->hentAntall()) {
            $resultat[] = $kravsett->hentFørste()->getPropertyNames();
            /** @var Krav $krav */
            foreach ($kravsett as $krav) {
                $linje = [];
                foreach ($krav->getFlatData() as $egenskap => $verdi) {
                    $linje[$egenskap] = Eksportprofil::formaterDato($valg['datoformat'] ?? 'Y-m-d', $verdi);
                }
                $resultat[] = $linje;
            }
        }
        return $this->app->after($this, __FUNCTION__, $resultat);
    }

    /**
     * @param array $valg
     * @return array
     * @throws Exception
     */
    private function hentUteståendeKravSett(array $valg): array
    {
        list('valg' => $valg)
            = $this->app->before($this, __FUNCTION__, [
            'valg' => $valg
        ]);

        $resultat = [];

        /* Tøm mellomlager for å få utestående beregnet på nytt */
        $this->app->modellOppbevaring->tøm();

        /** @var Kravsett $kravsett */
        $kravsett = $this->app->hentSamling(Krav::class);
        $kravsett->clearAdditionalFields();
        $delbeløpJoin = Eksportprofil::settDatofilter($valg, [
            '`' . Delbeløp::hentTabell() . '`.`krav` = `'
            . Krav::hentTabell() . '`.`' . Krav::hentPrimærnøkkelfelt()
            . '` AND `' . Delbeløp::hentTabell() . '`.`dato` <= "{til}"'
        ])[0];
        $kravsett->leggTilLeftJoin(Delbeløp::hentTabell(), $delbeløpJoin);
        $kravsett->leggTilUttrykk('krav_ekstra', 'innbetalt',
            'IFNULL(SUM(`innbetalinger`.`beløp`),0)'
        );
        $kravsett->leggTilUttrykk('krav_ekstra', 'utestående',
            '`krav`.`beløp` - IFNULL(SUM(`innbetalinger`.`beløp`),0)'
        );
        $kravsett->leggTilFilter(Eksportprofil::settDatofilter($valg, ['kravdato <=' => '{til}']));
        $kravsett->leggTilHaving(['`krav_ekstra.utestående` >' => 0], true);
        $kravsett->leggTilSortering('kravdato');

        $overskrifter = [
            'id',
            'reell_krav_id',
            'leieforhold',
            'regning',
            'dato',
            'type',
            'termin',
            'tekst',
            'forfall',
            'beløp',
            'innbetalt',
            'utestående',
        ];

        $resultat = [$overskrifter];

        /** @var Krav $krav */
        foreach ($kravsett as $krav) {
            $kravData = $krav->getFlatData();
            $linje = [
                'id' => $kravData->id,
                'reell_krav_id' => $kravData->reell_krav_id,
                'leieforhold' => $kravData->leieforhold,
                'regning' => $kravData->regning,
                'dato' => Eksportprofil::formaterDato($valg['datoformat'] ?? 'Y-m-d', $kravData->kravdato),
                'type' => $kravData->type,
                'termin' => $kravData->termin,
                'tekst' => $kravData->tekst,
                'forfall' => Eksportprofil::formaterDato($valg['datoformat'] ?? 'Y-m-d', $kravData->forfall),
                'beløp' => $kravData->beløp,
                'innbetalt' => $krav->getRawData()->krav_ekstra->innbetalt,
                'utestående' => $krav->getRawData()->krav_ekstra->utestående,
            ];
            $resultat[] = $linje;
            /* Fjern beregnet data */
            $krav->nullstill();
        }
        return $this->app->after($this, __FUNCTION__, $resultat);
    }

    /**
     * @param array $valg
     * @return array
     * @throws Exception
     */
    private function hentDelbeløpSett(array $valg): array
    {
        list('valg' => $valg)
            = $this->app->before($this, __FUNCTION__, [
            'valg' => $valg
        ]);

        $resultat = [];
        /** @var Delbeløpsett $delbeløpsett */
        $delbeløpsett = $this->app->hentSamling(Delbeløp::class);
        $delbeløpsett->leggTilFilter(Eksportprofil::settDatofilter($valg, ['`' . Delbeløp::hentTabell() . '`.`konto` <>' => '0']));
        $delbeløpsett->leggTilFilter(Eksportprofil::settDatofilter($valg, [
            'and' => [
                'dato >=' => '{fra}',
                'dato <=' => '{til}',
            ],
        ]));
        $delbeløpsett->leggTilSortering('dato');
        if($delbeløpsett->hentAntall()) {
            $resultat[] = $delbeløpsett->hentFørste()->getPropertyNames();
            /** @var Delbeløp $delbeløp */
            foreach ($delbeløpsett as $delbeløp) {
                $linje = [];
                foreach ($delbeløp->getFlatData() as $egenskap => $verdi) {
                    $linje[$egenskap] = Eksportprofil::formaterDato($valg['datoformat'] ?? 'Y-m-d', $verdi);
                }
                $resultat[] = $linje;
            }
        }
        return $this->app->after($this, __FUNCTION__, $resultat);
    }

    /**
     * @param array $valg
     * @return Utlevering
     * @throws Exception
     */
    private function hentRegningsarkiv(array $valg):Utlevering
    {
        list('valg' => $valg)
            = $this->app->before($this, __FUNCTION__, [
            'valg' => $valg
        ]);

        /** @var Regningsett $regningsett */
        $regningsett = $this->app->hentSamling(Regning::class)
            ->leggTilInnerJoin(Krav::hentTabell(),
                '`' . Krav::hentTabell() . '`.`gironr`'
                . ' = `' . Regning::hentTabell() . '`.`' . Regning::hentPrimærnøkkelfelt() . '`'
            )
            ->leggTilFilter(['`' . Regning::hentTabell() . '`.`utskriftsdato` IS NOT NULL'])
            ->leggTilFilter(Eksportprofil::settDatofilter($valg, ['`' . Krav::hentTabell() . '`.`kravdato` >=' => '{fra}']))
        ;
        if ($valg['tildato'] ?? null) {
            $regningsett->leggTilFilter(Eksportprofil::settDatofilter($valg, ['`' . Krav::hentTabell() . '`.`kravdato` <=' => '{til}']));
        }

        $arkivfil = $this->app->hentFilarkivBane() . '/giroer/_giroer.zip';
        $arkiv = new ZipArchive();
        $arkiv->open($arkivfil, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        /** @var Regning $regning */
        foreach($regningsett as $regning) {
            $regning->lagrePdf();

            $fil = "{$this->app->hentFilarkivBane()}/giroer/{$regning->hentId()}.pdf";
            $arkiv->addFile($fil, "giroer/{$regning->hentId()}.pdf");
        }
        $arkiv->close();

        $respons = new Respons(file_get_contents($arkivfil));
        $respons->header('Content-Type: application/zip');
        $respons->header('Content-disposition: attachment; filename="giroer.zip"');
        $respons->header('Content-Length: ' . filesize( $arkivfil ));
        $this->settRespons($respons);
        return $this->app->after($this, __FUNCTION__, $this);
    }

}