<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\LeieforholdListe;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Modell\Leieforhold\Oppsigelse;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\LeieforholdListe
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null): Utlevering
    {
        switch ($data) {
            default: {
                $resultat = (object)[
                    'success' => true,
                    'data'		=> [],
                    'totalRows' => 0
                ];

                $sorteringsfelt		= $_GET['sort'] ?? null;
                $synkende	= isset($_GET['dir']) && $_GET['dir'] == "DESC";
                $start		= intval($_GET['start'] ?? 0);
                $limit		= $_GET['limit'] ?? null;

                $søkefelt = $_GET['søkefelt'] ?? '';
                $nåværendeFilter = isset($_GET['nåværendeFilter']) && $_GET['nåværendeFilter'];

                try {
                    $leieforholdSett = $this->hentLeieforholdSett(
                        $søkefelt,
                        $nåværendeFilter,
                        $sorteringsfelt,
                        $synkende,
                        $start,
                        $limit
                    );

                    $leieforholdSett->inkluderLeietakere();

                    $kontrakter = $leieforholdSett->inkluderKontrakter();
                    $kontrakter->forhåndslastNavn();
                    $kontrakter->inkluderLeietakere();
                    $kontrakter->inkluderLeietakere();


                    $resultat->totalRows = $leieforholdSett->hentAntall();

                    foreach ($leieforholdSett as $leieforhold) {
                        $resultat->data[] = (object)array(
                            'leieforhold_id' => $leieforhold->hentId(),
                            'kontrakt_id' => $leieforhold->kontrakt->hentId(),
                            'leieforhold_beskrivelse' => $leieforhold->hentBeskrivelse(),
                            'kid' => $leieforhold->hentKid(),
                            'leiebeløp' => $leieforhold->hentLeiebeløp(),
                            'avsluttet' => $leieforhold->hentOppsigelse() ? $leieforhold->hentOppsigelse()->fristillelsesdato->format('Y-m-d') : null,
                            'til_dato' => $leieforhold->hentTildato() ? $leieforhold->hentTildato()->format('Y-m-d') : '',
                            'leieobjekt_id' => $leieforhold->leieobjekt->id,
                            'frosset' => $leieforhold->frosset,
                            'signert_fil' => (bool)$leieforhold->kontrakt->hent('signert_fil'),
                            'leieobjekt_beskrivelse' => $leieforhold->leieobjekt->hentBeskrivelse(),
                            'html' => $this->hentDetaljer($leieforhold)
                        );
                    }
                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }
        }
    }

    /**
     * @param string $søkestreng
     * @param string|null $sorteringsfelt
     * @param bool $synkende
     * @param int $start
     * @param int|null $limit
     * @return Leieforholdsett
     * @throws Exception
     */
    protected function hentLeieforholdSett(
        string  $søkestreng,
        bool    $nåværendeFilter,
        ?string $sorteringsfelt = null,
        bool    $synkende = false,
        int     $start = 0,
        ?int    $limit = null
    ): Leieforholdsett
    {
        $filter = [
            'or' => [
                '`' . Leieobjekt::hentTabell() . '`.`navn` LIKE' => '%' . $søkestreng . '%',
                '`' . Leieobjekt::hentTabell() . '`.`gateadresse` LIKE' => '%' . $søkestreng . '%',
                '`' . Leietaker::hentTabell() . '`.`leietaker` LIKE' => '%' . $søkestreng . '%',
                'CONCAT(`' . Person::hentTabell() . '`.`fornavn`, " ", `' . Person::hentTabell() . '`.`etternavn`) LIKE' => '%' . $søkestreng . '%',
            ]
        ];

        if((int)$søkestreng) {
            $filter['or']['`' . Leieforhold::hentTabell() . '`.`leieforholdnr` IN'] = [
                $søkestreng,
                (int)(intval($søkestreng)/100000000),
                (int)((intval($søkestreng)-1000000)/10)
            ];
            $filter['or']['`' . Leieforhold::hentTabell() . '`.`leieobjekt`'] = $søkestreng;
            $filter['or']['`' . Leieforhold::hentTabell() . '`.`leiebeløp`'] = $søkestreng;
            $filter['or']['`' . Leieforhold\Kontrakt::hentTabell() . '`.`kontraktnr`'] = $søkestreng;
        }

        /**
         * Mapping av sorteringsfelter
         * * kolonnenavn = > [tabellkilde el null for hovedtabell => tabellfelt]
         */
        $sorteringsfelter = [
            'leieforhold_id' => [null, 'leieforholdnr'],
            'kontrakt_id' => [null, 'siste_kontrakt'],
            'leieobjekt_id' => [null, 'leieobjekt'],
            'leiebeløp' => [null, 'leiebeløp'],
            'kid' => [null, 'leieforholdnr'],
            'til_dato' => [null, 'tildato'],
            'signert_fil' => ['kontrakt.eav_egenskaper', 'signert_fil'],
            'avsluttet' => [Oppsigelse::hentTabell(), 'fristillelsesdato'],
            'frosset' => [null, 'frosset'],
        ];
        $sorteringsfelt = ($sorteringsfelt && isset($sorteringsfelter[$sorteringsfelt])) ? $sorteringsfelter[$sorteringsfelt] : null;

        /** @var Leieforholdsett $leieforholdSett */
        $leieforholdSett = $this->app->hentSamling(Leieforhold::class);
        $leieforholdSett
            ->leggTilLeieobjektModell()
            ->leggTilOppsigelseModell()
            ->leggTilKontraktModell()
            ->leggTilLeftJoinForOppsigelse()
            ->leggTilLeftJoin(
                Leietaker::hentTabell(),
                '`' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`'
                . ' = `' . Leietaker::hentTabell() . '`.`leieforhold`'
            )
            ->leggTilLeftJoin(
                Person::hentTabell(),
                '`' . Leietaker::hentTabell() . '`.`person`'
                . ' = `' . Person::hentTabell() . '`.`' . Person::hentPrimærnøkkelfelt() . '`'
            )
            ->leggTilEavVerdiJoin('signert_fil', false, Leieforhold\Kontrakt::class, Leieforhold::hentTabell(), 'siste_kontrakt')
            ->leggTilFelt('signert_fil', 'verdi', null, 'signert_fil', 'kontrakt.' . Egenskap::hentTabell())
        ;

        $leieforholdSett->leggTilFilter($filter);
        if ($nåværendeFilter) {
            $leieforholdSett->leggTilFilter([
                'or' => [
                    '`' . Oppsigelse::hentTabell() . '`.`' . Oppsigelse::hentPrimærnøkkelfelt() . '`' => null,
                    '`' . Oppsigelse::hentTabell() . '`.`oppsigelsestid_slutt` > NOW()'
                ]
            ]);
        }


        $leieforholdSett->låsFiltre();

        if ($sorteringsfelt) {
            $leieforholdSett->leggTilSortering($sorteringsfelt[1], $synkende, $sorteringsfelt[0]);
        }

        if ($limit) {
            $leieforholdSett
                ->settStart($start)
                ->begrens($limit);
        }
        return $leieforholdSett;
    }

    /**
     * @param Leieforhold $leieforhold
     * @return string
     * @throws Exception
     */
    private function hentDetaljer(Leieforhold $leieforhold): string
    {
        $resultat = '';
        /** @var Leieforhold\Kontrakt $kontrakt */
        foreach ($leieforhold->hentKontrakter() as $kontrakt) {
            $resultat .= new HtmlElement(
                'div',
                ['style' => 'margin-left: 50px;'],
                'Avtale nr. ' . $kontrakt->id
                . ' med ' . $kontrakt->hentNavn() . ': '
                . $kontrakt->dato->format('d.m.Y') . ' – ' . ($kontrakt->tildato ? $kontrakt->tildato->format('d.m.Y') : '')
            );
        }

        /** @var Leietaker $leietaker */
        foreach($leieforhold->hentLeietakere() as $leietaker) {
            if($leietaker->person){
                $resultat .= new HtmlElement(
                    'div',
                    ['style' => 'margin-left: 50px;'],
                    $leietaker->hentNavn() . (substr($leietaker->hentNavn(), -1) != 's' ? 's ' : '\' ')
                    . new HtmlElement(
                        'a',
                        [
                            'title' => 'Klikk her for å adressekortet',
                            'href' => '/drift/index.php?oppslag=personadresser_kort&id=' . $leietaker->person->id,
                        ],
                        'adressekort'
                    )
                );
            }
            else {
                $resultat .= new HtmlElement(
                    'div',
                    ['style' => 'margin-left: 50px;'],
                    $leietaker->hentNavn()
                );
            }
        }

        return $resultat;
    }
}