<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 25/11/2022
 * Time: 17:10
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\Innstillinger;


use Exception;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var \stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Innstillingene er lagret."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = isset($get['skjema']) ? $get['skjema'] : '';
        try {
            switch ($skjema) {
                case 'innstillinger':
                    if(isset($post['leiejustering_pos_forankring'])) {
                        $post['leiejustering_pos_forankring'] = str_replace(',', '.', $post['leiejustering_pos_forankring']);
                    }
                    if(isset($post['leiejustering_neg_forankring'])) {
                        $post['leiejustering_neg_forankring'] = str_replace(',', '.', $post['leiejustering_neg_forankring']);
                    }
                    if(isset($post['leiejustering_gjeldende_indeks'])) {
                        $post['leiejustering_gjeldende_indeks'] = str_replace(',', '.', $post['leiejustering_gjeldende_indeks']);
                    }

                    if(isset($post['automatisk_faktura_datoer'])) {
                        if(strpos($post['automatisk_faktura_datoer'], '*') !== false) {
                            $post['automatisk_faktura_datoer'] = '*';
                        }
                        $automatisk_faktura_datoer = $post['automatisk_faktura_datoer'] ? explode(',', $post['automatisk_faktura_datoer']) : [];
                        foreach ($automatisk_faktura_datoer as $dato) {
                            $dato = trim($dato);
                            if($dato != '*' && ($dato < -28 || $dato > 28)) {
                                throw new Exception('Ugyldig verdi `' . $dato . '` i automatisk faktura-datoer');
                            }
                        }
                    }
                    if(isset($post['automatisk_faktura_ukedager'])) {
                        $automatisk_faktura_ukedager =
                            array_filter($post['automatisk_faktura_ukedager'], function ($dag) {
                            return $dag > 0 && $dag < 8;
                        });
                        $automatisk_faktura_ukedager = array_unique($automatisk_faktura_ukedager, SORT_NUMERIC);
                        $post['automatisk_faktura_ukedager'] = implode(',', $automatisk_faktura_ukedager);
                    }
                    if(isset($post['automatisk_faktura_tidsrom'])) {
                        try {
                            new \DateInterval($post['automatisk_faktura_tidsrom']);
                        } catch (Exception $e) {
                            throw new Exception('Ugyldig verdi i `Inkluder krav med forfall fram til`');
                        }
                    }
                    if(isset($post['automatisk_faktura_ekskluder_kravtyper'])) {
                        $post['automatisk_faktura_ekskluder_kravtyper'] =
                            implode(',', $post['automatisk_faktura_ekskluder_kravtyper']);
                    }
                    $this->app->hentValg();
                    $innstillinger = array_keys($this->app->hentValg());
                    foreach($post as $innstilling => $verdi) {
                        if(in_array($innstilling, $innstillinger)) {
                            $verdi = is_array($verdi) ? implode(',', $verdi) : $verdi;
                            $this->app->settValg($innstilling, $verdi);
                        }
                    }
                    $resultat->url = (string)$this->app->returi->get();
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Feilmelding $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        catch (\Throwable $e) {
            $this->app->loggException($e);
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}