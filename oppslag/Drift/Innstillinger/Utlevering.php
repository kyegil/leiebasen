<?php
/**
 * Part of boligstiftelsen.svartlamon.org
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\Innstillinger;


use Exception;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\Innstillinger
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return Utlevering
     */
    public function hentData($data = null):Utlevering {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => null
        ];

        try {
            switch ($data) {
                case 'kpi':
                    $resultat->data = $this->hentKpi();
                    break;

                default:
                    throw new Exception('Ønsket data er ikke angitt');
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    private function hentKpi(): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://data.ssb.no/api/v0/dataset/1088.json");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $responseObject = json_decode($response, true);
        if (isset($responseObject['dataset']['value'][5])) {
            return $responseObject['dataset']['value'][5];
        } else {
            throw new Exception('Klarte ikke hente KPI ifra SSB');
        }
    }
}