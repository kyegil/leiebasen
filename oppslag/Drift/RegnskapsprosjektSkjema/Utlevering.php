<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\RegnskapsprosjektSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Regnskapsprosjekt;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Samling;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\RegnskapsprosjektSkjema
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData($data = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case "områder":
                    $områdeFilter = [];
                    if (isset($_GET['query']) && trim($_GET['query'])) {
                        $query = explode(' ', trim($_GET['query']));
                        foreach ($query as $ord) {
                            $områdeFilter[] = [
                                'or' => [
                                    'id' => $ord,
                                    'navn LIKE' => '%' . $ord . '%',
                                    'beskrivelse LIKE' => '%' . $ord . '%',
                                ]
                            ];
                        }
                    }
                    /** @var Samling $områdesamling */
                    $områdesamling = $this->app->hentSamling(Område::class)
                        ->leggTilSortering(Område::hentPrimærnøkkelfelt())

                        ->leggTilFilter($områdeFilter);

                    /** @var Område $område */
                    foreach ($områdesamling as $område) {
                        $resultat->data[] = [
                            'value' => $område->hentId(),
                            'text' => $område->hentNavn(),
                        ];
                    }
                    break;

                case "leieforhold":
                    $leieforholdFilter = [];
                    if (isset($_GET['query']) && trim($_GET['query'])) {
                        $query = explode(' ', trim($_GET['query']));
                        foreach ($query as $ord) {
                            $leieforholdFilter[] = [
                                'or' => [
                                    '`leieforhold`.`leieforholdnr`' => $ord,
                                    '`kontraktpersoner`.`leietaker` LIKE' => '%' . $ord . '%',
                                    'CONCAT(`personer`.`fornavn`, \' \', `personer`.`etternavn`) LIKE' => '%' . $ord . '%',
                                ]
                            ];
                        }
                    }
                    // Dersom du ikke søker, men trigger alle leieforhold,
                    // begrenses disse til leieforhold som ikke er avsluttet
                    else {
                        $leieforholdFilter[] = [
                            '`oppsigelser`.`leieforhold`' => null
                        ];
                    }
                    /** @var Leieforholdsett $leieforholdsamling */
                    $leieforholdsamling = $this->app->hentSamling(Leieforhold::class)
                        ->leggTilLeftJoin(Leietaker::hentTabell(),
                            Leieforhold::hentTabell() . '.' . Leieforhold::hentPrimærnøkkelfelt()
                            . ' = ' . Leietaker::hentTabell() . '.leieforhold')
                        ->leggTilLeftJoin(Person::hentTabell(),
                            Leietaker::hentTabell() . '.person'
                            . ' = ' . Person::hentTabell() . '.' . Person::hentPrimærnøkkelfelt()
                        )

                        ->leggTilSortering(Leieforhold::hentPrimærnøkkelfelt())

                        ->leggTilFilter($leieforholdFilter);

                    /** @var Leieforhold $leieforhold */
                    foreach ($leieforholdsamling as $leieforhold) {
                        $resultat->data[] = [
                            'value' => $leieforhold->hentId(),
                            'text' => $leieforhold->hentId() . ' | ' . $leieforhold->hentBeskrivelse(),
                        ];
                    }
                    break;

                default:
                    $prosjektId = !empty($_GET['id']) ? $_GET['id'] : null;
                    if (!$prosjektId) {
                        throw new Exception('id-parameter mangler');
                    }
                    /** @var Regnskapsprosjekt $prosjekt */
                    $prosjekt = $this->app->hentModell(Regnskapsprosjekt::class, $prosjektId);
                    if ($prosjekt->hentId()) {
                        $resultat->data = (object) [
                            'id' => $prosjekt->hentId(),
                            'kode' => $prosjekt->kode,
                            'navn' => $prosjekt->navn,
                            'beskrivelse' => $prosjekt->beskrivelse,
                            'konfigurering' => $prosjekt->konfigurering
                        ];
                    }

                    else {
                        throw new Exception('dette prosjektregnskapet finnes ikke');
                    }
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}