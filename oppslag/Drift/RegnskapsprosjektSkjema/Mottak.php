<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 17/06/2020
 * Time: 13:33
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\RegnskapsprosjektSkjema;


use Exception;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Regnskapsprosjekt;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;

class Mottak extends AbstraktMottak
{
    /**
     * @param string|null $skjema
     * @return $this|Mottak
     */
    public function taIMot($skjema = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($skjema) {
                default:
                    $prosjektId = $this->data['get']['id'] ?? null;
                    $kode = isset($this->data['post']['kode']) ? trim($this->data['post']['kode']) : '';
                    $navn = isset($this->data['post']['navn']) ? trim($this->data['post']['navn']) : '';
                    $beskrivelse = $this->data['post']['beskrivelse'] ?? '';
                    $summeringsEnhet = $this->data['post']['summerings_enhet'] ?? '';
                    $segmentering = $this->data['post']['segmentering'] ?? '';

                    $inkluderteOmråder = $this->data['post']['inkluderte_områder'] ?? [];
                    $ekskluderteOmråder = $this->data['post']['ekskluderte_områder'] ?? [];
                    $inkluderteLeieobjekter = $this->data['post']['inkluderte_leieobjekter'] ?? [];
                    $ekskluderteLeieobjekter = $this->data['post']['ekskluderte_leieobjekter'] ?? [];
                    $inkluderteLeieforhold = $this->data['post']['inkluderte_leieforhold'] ?? [];
                    $ekskluderteLeieforhold = $this->data['post']['ekskluderte_leieforhold'] ?? [];

                    if (!$prosjektId) {
                        throw new Exception('Prosjekt-id er ikke angitt');
                    }
                    $this->validerKode($kode, $prosjektId);
                    $konfigurering = $this->lagKonfigurering($summeringsEnhet, $segmentering, $inkluderteLeieobjekter, $ekskluderteLeieobjekter, $inkluderteLeieforhold, $ekskluderteLeieforhold, $inkluderteOmråder, $ekskluderteOmråder);

                    if ($prosjektId == '*') {
                        /** @var Regnskapsprosjekt $prosjekt */
                        $prosjekt = $this->app->nyModell(Regnskapsprosjekt::class, (object)[
                            'kode' => $kode,
                            'navn' => $navn,
                            'beskrivelse' => $beskrivelse,
                            'konfigurering' => $konfigurering
                        ]);
                    }
                    else {
                        /** @var Regnskapsprosjekt $prosjekt */
                        $prosjekt = $this->app->hentModell(Regnskapsprosjekt::class, $prosjektId);
                        $prosjekt->kode = $kode;
                        $prosjekt->navn = $navn;
                        $prosjekt->beskrivelse = $beskrivelse;
                        $prosjekt->konfigurering = $konfigurering;
                    }
                    $resultat->id = $prosjekt->hentId();
                    $resultat->msg = 'Lagret';
                    $resultat->url = strval($this->app->returi->get(1));
                    break;
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param $kode
     * @param $prosjektId
     * @return $this
     * @throws Exception
     */
    public function validerKode($kode, $prosjektId)
    {
        if (!$kode) {
            throw new Exception("Kode er påkrevd");
        }
        if ($this->app->hentSamling(Regnskapsprosjekt::class)
            ->leggTilFilter([
                'kode' => $kode,
                'id !=' => $prosjektId
            ])->hentAntall()) {
            throw new Exception("Kode {$kode} brukes av et annet prosjekt");
        }
        return $this;
    }

    /**
     * @param string $summeringsEnhet
     * @param string $segmentering
     * @param array $inkluderteLeieobjekter
     * @param array $ekskluderteLeieobjekter
     * @param array $inkluderteLeieforhold
     * @param array $ekskluderteLeieforhold
     * @return object
     */
    private function lagKonfigurering(
        string $summeringsEnhet,
        string $segmentering,
        array $inkluderteLeieobjekter,
        array $ekskluderteLeieobjekter,
        array $inkluderteLeieforhold,
        array $ekskluderteLeieforhold,
        array $inkluderteOmråder,
        array $ekskluderteOmråder
    )
    {
        $konfigurering = (object)[
            'summeringsEnhet' => $summeringsEnhet ?: null,
            'segmentering' => $segmentering ?: null,
            'inkludert' => (object)[
                'bygninger' => null, // Null inkluderer alle, [] inkluderer ingen
                'leieobjekter' => null,
                'leieforhold' => null,
                'områder' => null,
            ],
            'ekskludert' => (object)[
                'bygninger' => [], // Null ekskluderer alle, [] ekskluderer ingen
                'leieobjekter' => [],
                'leieforhold' => [],
                'områder' => [],
            ]
        ];

        foreach ($inkluderteLeieobjekter as $inklusjon => $inkludert) {
            if (!$inkludert) {
                continue;
            }
            if ($inklusjon === '*') {
                // Alle bygninger og leieobjekter er allerede inkludert
                break;
            }
            $inklusjon = explode('-', $inklusjon);
            $bygningsId = $inklusjon[0] ?? 0;
            $leieobjektId = $inklusjon[1] ?? 0;
            if ($bygningsId && !$leieobjektId) {
                // Inkluder denne bygningen i sin helhet
                settype($konfigurering->inkludert->bygninger, 'array');
                $konfigurering->inkludert->bygninger[] = $bygningsId;
            }
            $leieobjektId = $inklusjon[1] ?? 0;
            if ($leieobjektId) {
                // Inkluder dette leieobjektet
                settype($konfigurering->inkludert->leieobjekter, 'array');
                $konfigurering->inkludert->leieobjekter[] = $leieobjektId;
            }
        }

        foreach ($ekskluderteLeieobjekter as $eksklusjon => $ekskludert) {
            if (!$ekskludert) {
                continue;
            }
            if ($eksklusjon === '*') {
                // Ekskluder samtlige bygninger og leieobjekter
                $konfigurering->ekskludert->bygninger = null;
                $konfigurering->ekskludert->leieobjekter = null;
                break;
            }
            $eksklusjon = explode('-', $eksklusjon);
            $bygningsId = $eksklusjon[0] ?? 0;
            $leieobjektId = $eksklusjon[1] ?? 0;
            if ($bygningsId && !$leieobjektId) {
                // Ekskluder denne bygningen i sin helhet
                $konfigurering->ekskludert->bygninger[] = $bygningsId;
            }
            if ($leieobjektId) {
                // Ekskluder dette leieobjektet
                $konfigurering->ekskludert->leieobjekter[] = $leieobjektId;
            }
        }

        foreach ($inkluderteLeieforhold as $inklusjon => $inkludert) {
            if (!$inkludert) {
                continue;
            }
            settype($konfigurering->inkludert->leieforhold, 'array');
            $konfigurering->inkludert->leieforhold[] = $inklusjon;
        }

        foreach ($ekskluderteLeieforhold as $eksklusjon => $ekskludert) {
            if (!$ekskludert) {
                continue;
            }
            $konfigurering->ekskludert->leieforhold[] = $eksklusjon;
        }

        foreach ($inkluderteOmråder as $inklusjon => $inkludert) {
            if (!$inkludert) {
                continue;
            }
            settype($konfigurering->inkludert->områder, 'array');
            $konfigurering->inkludert->områder[] = $inklusjon;
        }

        foreach ($ekskluderteOmråder as $eksklusjon => $ekskludert) {
            if (!$ekskludert) {
                continue;
            }
            $konfigurering->ekskludert->områder[] = $eksklusjon;
        }

        return $konfigurering;
    }
}