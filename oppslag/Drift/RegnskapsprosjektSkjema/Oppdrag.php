<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 04/12/2020
 * Time: 23:21
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\RegnskapsprosjektSkjema;


use Exception;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Regnskapsprosjekt;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function slettRegnskap() {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            $prosjektId = $this->data['post']['regnskapsprosjekt_id'];
            /** @var Regnskapsprosjekt $prosjekt */
            $prosjekt = $this->app->hentModell(Regnskapsprosjekt::class, $prosjektId);
            if (!$prosjekt->hentId()) {
                throw new Exception('Finner ikke prosjektregnskapet');
            }
            $resultat->msg = $prosjekt->kode . ' har blitt slettet';
            $prosjekt->slett();
            $resultat->url = $this->app->returi->get();
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}