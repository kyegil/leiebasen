<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\LeieforholdRegningsadresse;


use Exception;
use Kyegil\Leiebasen\EventManager;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema): Mottak
    {
        /** @var stdClass $resultat */
        $resultat = (object)[
            'success' => true,
            'msg' => "Lagret."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'regningsadresse':
                    $leieforholdId = $get['id'] ?? null;
                    $regningTilObjekt = !empty($post['regning_til_objekt']);
                    /** @var string|null $regningsobjektId */
                    $regningsobjektId = !empty($post['regningsobjekt']) ? $post['regningsobjekt'] : null;
                    /** @var string|null $regningspersonId */
                    $regningspersonId = !empty($post['regningsperson']) ? $post['regningsperson'] : null;
                    $giroformat = $post['giroformat'] ?? '';

                    /** @var Leieforhold $leieforhold */
                    $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);

                    if(!$leieforhold->hentId()) {
                        throw new Feilmelding('Ugyldig leieforhold ' . $leieforholdId . '.');
                    }

                    /** @var Leieobjekt $regningsobjekt */
                    $regningsobjekt = $this->app->hentModell(Leieobjekt::class, $regningsobjektId);
                    $regningsobjekt = $regningsobjekt->hentId() ? $regningsobjekt : null;
                    /** @var Person $regningsperson */
                    $regningsperson = $this->app->hentModell(Person::class, $regningspersonId);
                    $regningsperson = $regningsperson->hentId() ? $regningsperson : null;

                    $leieforhold->settRegningTilObjekt($regningTilObjekt);
                    $leieforhold->settRegningsobjekt($regningsobjekt);
                    $leieforhold->settRegningsperson($regningsperson);
                    $leieforhold->settGiroformat($giroformat);

                    $resultat->id = $leieforhold->hentId();
                    $resultat->msg = 'Regningspreferansene er lagret';
                    $resultat->url = (string)$this->app->returi->get(1);

                    EventManager::getInstance()->triggerEvent(
                        Leieforhold::class, 'endret',
                        $leieforhold, ['ny' => false], $this->app
                    );

                    /*
                     * For hver av leietakerne i leieforholdet som også er regningsperson i et annet leieforhold
                     * så oppdateres leveringsadresse på de andre leieforholdene
                     */
                    try {
                        $leietakerPersonIder = $leieforhold->hentLeietakere()->hentIdNumre();

                        /** @var Leieforholdsett $tidligereLeieforholdSett */
                        $tidligereLeieforholdSett = $this->app->hentSamling(Leieforhold::class)
                            ->leggTilFilter([
                                '`' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '` !=' => $leieforhold->hentId()
                            ])
                            ->leggTilFilter(['`' . Leieforhold::hentTabell() . '`.`regningsperson` IN' => $leietakerPersonIder])
                            ->låsFiltre();
                        /** @var Leieforhold $tidligereLeieforhold */
                        foreach ($tidligereLeieforholdSett as $tidligereLeieforhold) {
                            $tidligereLeieforhold->settRegningsobjekt($regningsobjekt);
                            $tidligereLeieforhold->settRegningTilObjekt($regningTilObjekt);
                            $tidligereLeieforhold->settFrosset(false);

                            EventManager::getInstance()->triggerEvent(
                                Leieforhold::class, 'endret',
                                $tidligereLeieforhold, ['ny' => false], $this->app
                            );
                        }
                    } catch (\Throwable $e) {
                        $this->app->loggException($e);
                    }

                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Feilmelding $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        catch (\Throwable $e) {
            $this->app->loggException($e);
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
            return $this;
    }
}