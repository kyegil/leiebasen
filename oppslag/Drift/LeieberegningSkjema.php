<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class LeieberegningSkjema extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => LeieberegningSkjema\Oppdrag::class
    ];
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $leieberegningId = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Leieberegning $leieberegning */
        $leieberegning = $this->hentModell(Leieberegning::class, (int)$leieberegningId);
        if (!$leieberegning->hentId() || $leieberegningId == '*') {
            $leieberegning = null;
        }
        $this->hoveddata['leieberegning'] = $leieberegning;
        $this->hoveddata['leieberegningId'] = $leieberegningId;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Leieberegning $leieberegning */
        $leieberegning = $this->hoveddata['leieberegning'];
        if(!$leieberegning && $this->hoveddata['leieberegningId'] != '*') {
            return false;
        }
        $this->tittel = 'Leieberegning';
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Leieberegning $leieberegning */
        $leieberegning = $this->hoveddata['leieberegning'];

        return $this->vis(\Kyegil\Leiebasen\Visning\drift\html\leieberegning_skjema\Index::class, [
            'leieberegning' => $leieberegning,
            'leieberegningId' => $this->hoveddata['leieberegningId'],
        ]);
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function skript(): ViewInterface
    {
        /** @var Leieberegning $leieberegning */
        $leieberegning = $this->hoveddata['leieberegning'];

        $skript = new ViewArray([
            $this->vis(\Kyegil\Leiebasen\Visning\drift\js\leieberegning_skjema\Index::class, [
                'leieberegning' => $leieberegning,
                'leieberegningId' => $this->hoveddata['leieberegningId'],
            ]),
        ]);
        return $skript;
    }
}