<?php
/**
 * Part of boligstiftelsen.svartlamon.org
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderTjenesteKort;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Visning;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\FsAnleggKort
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param null $data
     * @return $this|Utlevering
     */
    public function hentData($data = null) {
        $tjenesteId = $this->data['get']['id'] ?? null;
        /** @var Tjeneste $tjeneste */
        $tjeneste = $this->app->hentModell(Tjeneste::class, $tjenesteId)->hentTypeModell();

        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => [],
            'totalRows' => 0
        ];

        switch ($data) {
            case 'detaljer':
                try {
                    if ($tjeneste instanceof Strømanlegg) {
                        $resultat = $this->app->vis(Visning\drift\html\fs_anlegg_kort\Strømanlegg::class, ['strømanlegg' => $tjeneste]);
                    }
                    else {
                        $resultat = $this->app->vis(Visning\felles\html\kostnadsdeling\Tjeneste::class, ['tjeneste' => $tjeneste]);
                    }
                } catch (Exception $e) {
                    $resultat = $e;
                }
                $this->settRespons(new Respons($resultat));
                break;

            case 'fordelingsnøkkel':
                $resultat = '';
                try {
                    $fordelingsnøkkel = $tjeneste->hentFordelingsnøkkel();
                    if ($fordelingsnøkkel) {
                        $resultat = $this->app->vis(Visning\felles\html\kostnadsdeling\Fordelingsnøkkel::class, ['fordelingsnøkkel' => $fordelingsnøkkel]);
                    }
                } catch (Exception $e) {
                    $resultat = $e;
                }
                $this->settRespons(new Respons($resultat));
                break;

            case 'regninger':
                try {
                    $sorteringsfelt	= $this->data['get']['sort'] ?? null;
                    $synkende = isset($this->data['get']['dir']) && $this->data['get']['dir'] == "DESC";
                    $start = intval($this->data['get']['start'] ?? 0);
                    $limit = $this->data['get']['limit'] ?? null;

                    $regningsett = $this->hentRegningsett(
                        $tjeneste,
                        $sorteringsfelt,
                        $synkende,
                        $start,
                        $limit
                    );
                    $resultat->totalRows = $regningsett->hentAntall();

                    foreach ($regningsett as $regning) {
                        $html = $this->app->vis(Visning\drift\html\delte_kostnader_tjeneste_kort\extjs4\regninger\Regning::class, ['regning' => $regning]);
                        $linje = (object)[
                            'id' => $regning->id,
                            'fakturanummer' => $regning->fakturanummer,
                            'avtalereferanse' => $tjeneste->avtalereferanse,
                            'tjeneste_id' => $tjeneste->id,
                            'tjenestebeskrivelse' => $tjeneste->beskrivelse,
                            'fradato' => $regning->fradato ? $regning->fradato->format('Y-m-d') : '',
                            'tildato' => $regning->tildato ? $regning->tildato->format('Y-m-d') : '',
                            'termin' => $regning->termin,
                            'beløp' => $regning->fakturabeløp,
                            'beløp_formatert' => $this->app->kr($regning->fakturabeløp),
                            'forbruk' => $regning->forbruk,
                            'forbruk_formatert' => $regning->forbruk ? ($regning->forbruk . '&nbsp' . $regning->forbruksenhet) :null,
                            'forbruksenhet' => $regning->forbruksenhet,
                            'fordelt' => $regning->fordelt,
                            'daglig_forbruk' => $regning->hentDagligForbruk(),
                            'daglig_forbruk_formatert' => $regning->hentDagligForbruk() ? ($regning->hentDagligForbruk() . '&nbsp' . $regning->forbruksenhet) : null,
                            'lagt_inn_av' => $regning->lagtInnAv,
                            'html' => strval($html)
                        ];
                        $resultat->data[] = $linje;
                    }
                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                break;

            case 'forbruksstatistikk':
                $resultat = (object)array(
                    'success'	=> true,
                    'data'		=> array()
                );
                $fra = isset($this->data['get']['fra']) && $this->data['get']['fra']
                    ? new DateTime($this->data['get']['fra'])
                    : null;
                $til = isset($this->data['get']['til']) && $this->data['get']['fra']
                    ? new DateTime($this->data['get']['til'])
                    : null;

                $datoer = $this->hentFakturaendringsdatoer($tjeneste, $fra, $til);
                foreach($datoer as $datoStreng) {
                    $regninger = $tjeneste->hentRegningerForTidsrom(new DateTime($datoStreng));
                    $resultat->data[$datoStreng]['dato'] = $datoStreng;
                    /** @var Tjeneste\Kostnad $regning */
                    foreach ($regninger as $regning) {
                        if ($regning->hentFakturanummer()) {
                            $resultat->data[$datoStreng][$regning->hentFakturanummer()] = $regning->hentDagligForbruk();
                        }
                    }
                }
                $resultat->data = array_values($resultat->data);
                $resultat->totalRows = count($resultat->data);

                $this->settRespons(new JsonRespons($resultat));
                break;

            default:
                $resultat = (object)[
                    'success' => false,
                    'msg' => 'Ønsket data er ikke oppgitt',
                    'data' => []
                ];
                $this->settRespons(new JsonRespons($resultat));
        }
        return $this;
    }

    /**
     * @param Tjeneste $tjeneste
     * @param $sorteringsfelt
     * @param $synkende
     * @param $start
     * @param $limit
     * @return Tjeneste\Kostnadsett
     * @throws Exception
     */
    private function hentRegningsett(
        Tjeneste $tjeneste,
        $sorteringsfelt,
        $synkende,
        $start,
        $limit
    )
    {
        /**
         * Mapping av sorteringsfelter
         * * kolonnenavn = > [tabellkilde el null for hovedtabell => tabellfelt]
         */
        $sorteringsfelter = [
            'id' => [null, 'id'],
            'fradato' => [null, 'fradato'],
            'tildato' => [null, 'tildato'],
            'termin' => [null, 'termin'],
            'forbruk' => [null, 'forbruk'],
            'beløp' => [null, 'beløp'],
            'låst' => [null, 'låst'],
        ];
        $sorteringsfelt = ($sorteringsfelt && isset($sorteringsfelter[$sorteringsfelt])) ? $sorteringsfelter[$sorteringsfelt] : null;
        $regningsett = $tjeneste->hentRegninger();
        if ($sorteringsfelt) {
            $regningsett->leggTilSortering($sorteringsfelt[1], $synkende, $sorteringsfelt[0]);
        }

        if ($limit) {
            $regningsett
                ->settStart($start)
                ->begrens($limit);
        }
        return $regningsett;
    }

    /**
     * @param Tjeneste $tjeneste
     * @param DateTime|null $fra
     * @param DateTime|null $til
     * @return string[]
     * @throws Exception
     */
    private function hentFakturaendringsdatoer(Tjeneste $tjeneste, ?\DateTimeInterface $fra, ?\DateTimeInterface $til): array
    {
        $datoer = [];
        $regningsett = $tjeneste->hentRegninger();
        foreach ($regningsett as $regning) {
            if($regning->fradato) {
                $dato = clone $regning->fradato;
                if(
                    ($dato >= $fra)
                    && ($dato <= $til || !$til)
                ) {
                    $datoer[$dato->format('Y-m-d')] = $dato->format('Y-m-d');
                    $dato->sub(new \DateInterval('P1D'));
                    if ($dato >= $fra) {
                        $datoer[$dato->format('Y-m-d')] = $dato->format('Y-m-d');
                    }
                }
            }
            if($regning->tildato) {
                $dato = clone $regning->tildato;
                if(
                    ($dato >= $fra)
                    && ($dato <= $til || !$til)
                ) {
                    $datoer[$dato->format('Y-m-d')] = $dato->format('Y-m-d');
                    $dato->add(new \DateInterval('P1D'));
                    if ($dato <= $til) {
                        $datoer[$dato->format('Y-m-d')] = $dato->format('Y-m-d');
                    }
                }
            }
        }
        sort($datoer);
        return $datoer;
    }
}