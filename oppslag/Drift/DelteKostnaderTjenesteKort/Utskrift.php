<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 04/12/2020
 * Time: 23:21
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderTjenesteKort;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Visning;
use Kyegil\ViewRenderer\ViewArray;

class Utskrift extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function utskrift() {
        try {
            $tjenesteId = $this->data['get']['id'] ?? null;
            /** @var Tjeneste $tjeneste */
            $tjeneste = $this->app->hentModell(Tjeneste::class, $tjenesteId)->hentTypeModell();

            $inkRegninger = isset($this->data['get']['regninger']) ?? $this->data['get']['regninger'];

            $innhold = new ViewArray();
            if ($tjeneste instanceof Strømanlegg) {
                $innhold->addItem($this->app->vis(Visning\drift\html\fs_anlegg_kort\Strømanlegg::class, ['strømanlegg' => $tjeneste]));
            }
            else {
                $innhold->addItem($detaljer = $this->app->vis(Visning\felles\html\kostnadsdeling\Tjeneste::class, ['tjeneste' => $tjeneste]));
            }

            if ($tjeneste->hentFordelingsnøkkel()) {
                $innhold->addItem(new HtmlElement('h2', [], 'Fordelingsnøkkel'));
                $innhold->addItem($this->app->vis(Visning\felles\html\kostnadsdeling\Fordelingsnøkkel::class, ['fordelingsnøkkel' => $tjeneste->hentFordelingsnøkkel()]));
            }
            if ($inkRegninger) {
                $innhold->addItem($this->app->vis(Visning\felles\html\kostnadsdeling\Fakturaliste::class, [
                    'tjeneste' => $tjeneste
                ]));
            }


            $resultat = $this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\shared\Utskrift::class, [
                'tittel' => $this->app->tittel,
                'innhold' => $innhold
            ]);
        }
        catch (Exception $e) {
            $resultat = $e;
        }
        $this->settRespons(new Respons($resultat));
        return $this;
    }
}