<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use DateTime;
use Exception;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;


/**
 * Utskriftsveiviser Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class Utskriftsveiviser extends \Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Skriv ut regninger og purringer';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => Utskriftsveiviser\Oppdrag::class
    ];


    /**
     * Utskriftsveiviser constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = [])
    {
        parent::__construct($di, $config);
        /** @var int[] $inkLeieforhold */
        $inkLeieforhold = isset($_GET['ink_leieforhold']) && $_GET['ink_leieforhold'] ? explode(',', $_GET['ink_leieforhold']) : [];
        /** @var int[] $eksLeieforhold */
        $eksLeieforhold = isset($_GET['eks_leieforhold']) && $_GET['eks_leieforhold'] ? explode(',', $_GET['eks_leieforhold']) : [];
        /** @var DateTime|null $forfallFramTil */
        $forfallFramTil = isset($_GET['til_dato']) ? new DateTime($_GET['til_dato']) : null;
        /** @var string[] $kravtyper */
        $kravtyper = (array)($_GET['kravtyper'] ?? []);
        /** @var bool $adskilt */
        $adskilt = boolval(isset($_GET['adskilt']) ? $_GET['adskilt'] : $this->hentValg('automatisk_faktura_adskilt'));

        $this->hoveddata['inkLeieforhold']  = $inkLeieforhold;
        $this->hoveddata['eksLeieforhold']  = $eksLeieforhold;
        $this->hoveddata['forfallFramTil']  = $forfallFramTil;
        $this->hoveddata['kravtyper']       = $kravtyper;
        $this->hoveddata['adskilt']         = $adskilt;
    }

    /**
     * @return Index
     */
    public function skript() {
        if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }

        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(\Kyegil\Leiebasen\Visning\drift\js\utskriftsveiviser\ExtOnReady::class, [
                'inkLeieforhold'    => $this->hoveddata['inkLeieforhold'],
                'eksLeieforhold'    => $this->hoveddata['eksLeieforhold'],
                'forfallFramTil'    => $this->hoveddata['forfallFramTil'],
                'kravtyper'         => $this->hoveddata['kravtyper'],
                'adskilt'           => $this->hoveddata['adskilt'],
            ])
        ]);
        return $skript;
    }
}