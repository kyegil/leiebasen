<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_kostnad_arkiv\Index;

/**
 * DelteKostnaderFakturaListe Controller
 *
 * @method \Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderKostnadArkiv\Utlevering hentUtlevering()
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class DelteKostnaderKostnadArkiv extends DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => DelteKostnaderKostnadListe\Oppdrag::class
    ];
    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = "Kostnader fordelt blant beboere";
        return true;
    }

    /**
     * @return Visning
     */
    public function innhold()
    {
        return $this->vis(Index::class);
    }
}