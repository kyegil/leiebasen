<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\TilpassetEavFeltListe;


use Exception;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Egenskapsett;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\TilpassetEavFeltListe
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param Kostnad $regning
     * @return object[]
     * @throws Exception
     */
    public static function hentManuelleFordelingselementer(Kostnad $regning): array
    {
        $resultat = [];
        $fordelingsnøkkel = $regning->hentTjeneste()? $regning->hentTjeneste()->hentFordelingsnøkkel() : null;
        if ($fordelingsnøkkel) {
            foreach($fordelingsnøkkel->hentBeløpelementer() as $element) {
                $leieobjekt = $element->følgerLeieobjekt && $element->leieobjekt && $element->leieobjekt->hentId() ? $element->leieobjekt : null;
                $leieforhold = !$element->følgerLeieobjekt && $element->leieforhold && $element->leieforhold->hentId() ? $element->leieforhold : null;
                if($leieobjekt) {
                    $beskrivelse = \Kyegil\Leiebasen\Leiebase::ucfirst($leieobjekt->hentType()) . ' ' . $leieobjekt->hentId() . ': ' . $leieobjekt->hentBeskrivelse();
                }
                else if($leieforhold) {
                    $beskrivelse = \Kyegil\Leiebasen\Leiebase::ucfirst($leieforhold->hentBeskrivelse());
                }
                $resultat[] = (object)[
                    'element_id' => $element->hentId(),
                    'beskrivelse' => $beskrivelse,
                    'beløp' => $element->hentFastbeløp(),
                ];
            }
        }
        return $resultat;
    }

    /**
     * @param string $data
     * @return $this
     * @throws Exception
     * @link https://datatables.net/manual/server-side
     */
    public function hentData($data = null): Utlevering
    {
        $get = $this->data['get'];
        $post = $this->data['post'];

        switch ($data) {
            case 'egenskaper': {
                $resultat = (object)[
                    'success' => true,
                    'draw' => intval($get['draw'] ?? 0),
                    'data'		=> [],
                    'totalRows' => 0,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                ];

                $sorteringsfelt	= $get['sort'] ?? null;
                $synkende = isset($get['dir']) && $get['dir'] == "DESC";
                if ($sorteringsfelt) {
                    $sortering = [[
                        'columnName' => $sorteringsfelt,
                        'dir' => $synkende ? 'desc' : 'asc'
                    ]];
                }
                else {
                    $sortering  = $get['order'] ?? [];
                }
                $start		= intval($get['start'] ?? 0);
                $limit		= $get['limit'] ?? 25;

                $søkefelt = $get['søkefelt'] ?? ($get['search']['value'] ?? '');

                try {
                    $eavEgenskapsett = $this->hentEavEgenskapSett(
                        $søkefelt,
                        $sortering,
                        $start,
                        $limit
                    );

                    $resultat->recordsFiltered = $resultat->recordsTotal = $resultat->totalRows
                        = $eavEgenskapsett->hentAntall();

                    foreach ($eavEgenskapsett as $eavEgenskap) {
                        $resultat->data[] = $this->hentEgenskapData($eavEgenskap);
                    }
                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->error = $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }

            default:
                $resultat = (object)[
                    'success' => false,
                    'msg' => 'Ønsket data er ikke oppgitt',
                    'data' => []
                ];
                $this->settRespons(new JsonRespons($resultat));
                return $this;
        }
    }

    /**
     * @param string $søkestreng
     * @param string[][] $sortering
     * @param int $start
     * @param int|null $limit
     * @return Egenskapsett
     */
    protected function hentEavEgenskapSett(
        string  $søkestreng,
        array   $sortering = [],
        int     $start = 0,
        ?int    $limit = null
    ): Egenskapsett
    {
        $filter = array(
            'or' => array(
                '`eav_egenskaper`.`modell` LIKE' => 'Kyegil\\\\Leiebasen\\\\Modell\\\\%' . $søkestreng . '%',
                '`eav_egenskaper`.`kode` LIKE' => '%' . $søkestreng . '%',
                '`eav_egenskaper`.`beskrivelse` LIKE' => '%' . $søkestreng . '%',
            )
        );

        /**
         * Mapping av sorteringsfelter
         * * kolonnenavn = > [tabellkilde el null for hovedtabell => tabellfelt]
         */
        $sorteringsfelter = [
            'id' => [null, 'id'],
            'modell' => [null, 'modell'],
            'kode' => [null, 'kode'],
            'er_egendefinert' => [null, 'er_egendefinert'],
            'type' => [null, 'type'],
            'rekkefølge' => [null, 'rekkefølge'],
        ];

        /** @var Egenskapsett $eavEgenskapsett */
        $eavEgenskapsett = $this->app->hentSamling(Egenskap::class);

        $eavEgenskapsett->leggTilFilter($filter);
        $eavEgenskapsett->låsFiltre();

        $eavEgenskapsett->leggTilSortering('rekkefølge');
        foreach ($sortering as $sorteringskonfig) {
            if(isset($sorteringsfelter[$sorteringskonfig['columnName']])) {
                $eavEgenskapsett->leggTilSortering(
                    $sorteringsfelter[$sorteringskonfig['columnName']][1],
                    strtolower($sorteringskonfig['dir']) == 'desc',
                    $sorteringsfelter[$sorteringskonfig['columnName']][0]
                );
            }
        }

        if ($limit) {
            $eavEgenskapsett
                ->settStart($start)
                ->begrens($limit);
        }
        return $eavEgenskapsett;
    }

    /**
     * @param Egenskap $eavEgenskap
     * @return object
     * @throws Exception
     */
    public function hentEgenskapData(Egenskap $eavEgenskap): object
    {
        return (object)array(
            'id' => $eavEgenskap->hentId(),
            'modell' => str_replace('Kyegil\\Leiebasen\\Modell\\', '', $eavEgenskap->modell),
            'kode' => $eavEgenskap->kode,
            'er_egendefinert' => $eavEgenskap->erEgendefinert,
            'type' => $eavEgenskap->type,
            'beskrivelse' => $eavEgenskap->beskrivelse,
            'rekkefølge' => $eavEgenskap->rekkefølge,
        );
    }
}