<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/02/2023
 * Time: 15:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\DelkravSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;

/**
 *
 */
class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Lagret."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'delkravtypeskjema':
                    $id = $get['id'] ?? null;
                    $aktiv = isset($post['aktiv']) && $post['aktiv'];
                    $selvstendigTillegg = isset($post['selvstendig_tillegg']) && $post['selvstendig_tillegg'];
                    $relativ = isset($post['relativ']) && $post['relativ'];
                    $valgfritt = isset($post['valgfritt']) && $post['valgfritt'];
                    $indeksreguleres = isset($post['indeksreguleres']) && $post['indeksreguleres'];
                    $kravtype = $post['kravtype'] ?? null;
                    $kode = isset($post['kode']) ? strip_tags($post['kode']) : null;
                    $navn = isset($post['navn']) ? strip_tags($post['navn']) : null;
                    $beskrivelse = isset($post['beskrivelse']) ? strip_tags($post['beskrivelse']) : null;
                    $sats = !empty($post['sats']) ? str_replace(',', '.', $post['sats']) : 0;

                    $this->validerKode($kode);

                    if($id == '*') {
                        $this->validerKravtype($kravtype);
                        /** @var Delkravtype $delkravtype */
                        $delkravtype = $this->app->nyModell(Delkravtype::class, (object)[
                            'kravtype' => $kravtype,
                            'kode' => $kode,
                            'aktiv' => $aktiv,
                            'selvstendig_tillegg' => $selvstendigTillegg,
                            'valgfritt' => $valgfritt,
                            'sats' => $sats,
                            'relativ' => $relativ,
                            'navn' => $navn,
                            'beskrivelse' => $beskrivelse,
                            'orden' => 0
                        ]);
                        $delkravtype->orden = $delkravtype->id;
                        $delkravtype->oppdaterSatsHistorikk();
                    }
                    else {
                        /** @var Delkravtype $delkravtype */
                        $delkravtype = $this->app->hentModell(Delkravtype::class, $id);
                        if (!$delkravtype->hentId()) {
                            throw new Exception('Delkrav ' . $id . ' finnes ikke');
                        }
                        $satsenErEndret = $this->satsenErEndret($delkravtype, $sats);

                        $delkravtype
                            ->settKode($kode)
                            ->settNavn($navn)
                            ->settBeskrivelse($beskrivelse)
                            ->settSelvstendigTillegg($selvstendigTillegg)
                            ->settValgfritt($valgfritt)
                            ->settRelativ($relativ)
                            ->settSats($sats)
                            ->settAktiv($aktiv)
                        ;
                        if($satsenErEndret) {
                            $delkravtype->oppdaterSatsHistorikk();
                        }
                    }
                    $delkravtype->indeksreguleres = !$relativ && $indeksreguleres ?: null;
                    $resultat->id = $delkravtype->hentId();
                    $resultat->msg = 'Delkravet er lagret';
                    $resultat->url = (string)$this->app->returi->get(1);
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param $kravtype
     * @return Mottak
     * @throws Exception
     */
    private function validerKravtype($kravtype): Mottak
    {
        if (!in_array($kravtype, Krav::$typer)) {
            throw new Exception('Ugyldig kravtype');
        }
        return $this;
    }

    /**
     * @param string|null $kode
     * @return Mottak
     * @throws Exception
     */
    private function validerKode(?string $kode): Mottak
    {
        if (!preg_match('/^[A-Za-zÀ-ÖØ-öø-ÿ]+$/', $kode)) {
            throw new Exception('Ugyldige tegn i koden');
        }
        if (in_array($kode, Krav::$typer)) {
            throw new Exception('Denne koden er resevert');
        }
        return $this;
    }

    /**
     * @param Delkravtype $delkravtype
     * @param string $sats
     * @return bool
     */
    private function satsenErEndret(Delkravtype $delkravtype, string $sats): bool
    {
        return $sats != $delkravtype->hentSats();
    }
}