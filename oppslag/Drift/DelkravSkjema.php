<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class DelkravSkjema extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $delkravtypeId = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Delkravtype $delkravtype */
        $delkravtype = $this->hentModell(Delkravtype::class, (int)$delkravtypeId);
        if (!$delkravtype->hentId() || $delkravtypeId == '*') {
            $delkravtype = null;
        }
        $this->hoveddata['delkravtype'] = $delkravtype;
        $this->hoveddata['delkravtypeId'] = $delkravtypeId;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Delkravtype $delkravtype */
        $delkravtype = $this->hoveddata['delkravtype'];
        if(!$delkravtype && $this->hoveddata['delkravtypeId'] != '*') {
            return false;
        }
        $this->tittel = $delkravtype ? $delkravtype->hentNavn() : 'Konfigurering av delkrav';
        return true;
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Delkravtype $delkravtype */
        $delkravtype = $this->hoveddata['delkravtype'];

        return $this->vis(\Kyegil\Leiebasen\Visning\drift\html\delkrav_skjema\Index::class, [
            'delkravtype' => $delkravtype,
            'delkravtypeId' => $this->hoveddata['delkravtypeId'],
        ]);
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function skript(): ViewInterface
    {
        /** @var Delkravtype|null $delkravtype */
        $delkravtype = $this->hoveddata['delkravtype'];

        $skript = new ViewArray([
            $this->vis(\Kyegil\Leiebasen\Visning\drift\js\delkrav_skjema\Index::class, [
                'delkravtype' => $delkravtype,
                'delkravtypeId' => $this->hoveddata['delkravtypeId'],
            ]),
        ]);
        return $skript;
    }
}