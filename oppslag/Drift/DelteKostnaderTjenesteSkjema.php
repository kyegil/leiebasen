<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_tjeneste_skjema\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * DelteKostnaderTjenesteSkjema Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class DelteKostnaderTjenesteSkjema extends DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Delte Kostnader – Tjeneste';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => DelteKostnaderTjenesteSkjema\Oppdrag::class
    ];


    /**
     * DelteKostnaderTjenesteSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $this->tittel = 'Delte Kostnader – Tjeneste';

        $tjenesteId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['tjeneste'] = null;

        $filter = [
            'or' => [
                ['id' => $tjenesteId],
            ]
        ];
        /** @var Tjeneste|null $tjeneste */
        $tjeneste = $this->hentSamling(Tjeneste::class)
            ->leggTilFilter($filter)->hentFørste();

        if ($tjeneste) {
            if ($tjeneste->hentId()) {
                $this->hoveddata['tjeneste'] = $tjeneste;
            }
        }
        else if ($tjenesteId == '*') {
            $this->hoveddata['tjeneste'] = '*';
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML(): bool
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Tjeneste|string|null $tjeneste */
        $tjeneste = $this->hoveddata['tjeneste'];
        if ($tjeneste instanceof Tjeneste && !$tjeneste->hentId()) {
            return false;
        }
        if (is_string($tjeneste)) {
            if ($tjeneste !== '*') {
                return false;
            }
            $this->hoveddata['tjeneste'] = null;
        }
        else if ($tjeneste && $tjeneste->hentTypeModell() instanceof Strømanlegg) {
            $respons = new Respons('');
            $respons->header("Location: index.php?oppslag=fs_anlegg_skjema&id=$tjeneste");
            $this->settRespons($respons);
        }

        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     * @throws Exception
     */
    public function skript(): Index
    {
        /** @var Tjeneste|string|null $tjeneste */
        $tjeneste = $this->hoveddata['tjeneste'];

        /** @var Index $skript */
        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'tjeneste' => ($tjeneste instanceof Tjeneste) && $tjeneste->hentId() ? $tjeneste : null,
                'tjenesteId' => $tjeneste && $tjeneste->hentId() ? $tjeneste->hentId() : '*',
            ])
        ]);
        return $skript;
    }
}