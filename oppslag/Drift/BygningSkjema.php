<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\bygning_skjema\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * BygningSkjema Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class BygningSkjema extends DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Bygning';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => BygningSkjema\Oppdrag::class
    ];


    /**
     * BygningSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $bygningsId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['område'] = null;

        if ((int)$bygningsId) {
            $this->hoveddata['bygning'] = $this->hentModell(Bygning::class, (int)$bygningsId);
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Bygning $bygning */
        $bygning = $this->hoveddata['bygning'] ?? null;
        if (
            $bygning
            && !$bygning->hentId()
        ) {
            return false;
        }
        else if ($bygning) {
            $this->tittel = $bygning->navn;
        }

        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     * @throws Exception
     */
    public function skript() {
        /** @var Bygning|null $bygning */
        $bygning = $this->hoveddata['bygning'] ?? null;

        if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }

        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'bygning' => $bygning
            ])
        ]);
        return $skript;
    }
}