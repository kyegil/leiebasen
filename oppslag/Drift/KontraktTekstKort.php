<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Jshtml\JsCustom;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\Leiebasen\Visning\felles\html\shared\body\main\Kort;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class KontraktTekstKort extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
//        'oppgave' => KontraktTekstKort\Oppdrag::class
    ];
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $kontraktId = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Kontrakt $kontrakt */
        $kontrakt = $this->hentModell(Kontrakt::class, (int)$kontraktId);
        if (!$kontrakt->hentId()) {
            $kontrakt = null;
        }
        $this->hoveddata['kontrakt'] = $kontrakt;
        $this->hoveddata['kontraktId'] = $kontraktId;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML(): bool
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Kontrakt $kontrakt */
        $kontrakt = $this->hoveddata['kontrakt'];
        if(!$kontrakt || !$kontrakt->hentId()) {
            return false;
        }
        $this->tittel = 'Leieavtale nr. ' . $kontrakt->hentId() . ', leieforhold ' . $kontrakt->leieforhold;
        return true;
    }

    /**
     * @return Kort
     * @throws Exception
     */
    public function innhold(): Kort
    {
        /** @var Kontrakt $kontrakt */
        $kontrakt = $this->hoveddata['kontrakt'];
        $utskriftUrl = DriftKontroller::url(
            'kontrakt_tekst_kort', $kontrakt->hentId(),
            'utlevering', ['data' => 'utskrift']
        );

        $knapper = new ViewArray([
            new HtmlElement('a', [
                'role' => 'button',
                'href' => $this->returi->get()->url,
                'class' => 'btn btn-secondary fortsett',
            ], 'Fortsett'
            ),
            new HtmlElement('div', [
                'role' => 'button',
                'onclick' => new JsCustom("window.open('{$utskriftUrl}');"),
                'class' => 'btn btn-secondary'
            ], 'Skriv ut'),
            new HtmlElement('a', [
                'role' => 'button',
                'href' => $this::url('kontrakt_tekst_skjema', $kontrakt->hentId()),
                'class' => 'button'
            ], 'Endre'),
        ]);

        /** @var Kort $kort */
        $kort = $this->vis(Kort::class, [
            'innhold' => $kontrakt->hentTekst(),
            'knapper' => $knapper
        ]);
        return $kort;
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function skript(): ViewInterface
    {
        /** @var Kontrakt $kontrakt */
        $kontrakt = $this->hoveddata['kontrakt'];

        $skript = new ViewArray([]);
        return $skript;
    }
}