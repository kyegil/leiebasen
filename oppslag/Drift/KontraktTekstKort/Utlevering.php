<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\KontraktTekstKort;


use Exception;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Visning\felles\html\shared\Utskrift;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\KontraktTekstKort
 */
class Utlevering extends AbstraktUtlevering
{

    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null): Utlevering
    {
        $get = $this->data['get'];


        switch ($data) {
            case 'utskrift': {
                try {
                    $kontraktId = $get['id'] ?? null;
                    $kontrakt = $this->app->hentModell(Kontrakt::class, $kontraktId);
                    $avtaleBeholder = new \Kyegil\Leiebasen\Jshtml\Html\HtmlElement(
                        'div', ['class' => 'leieavtale beholder'],
                        $kontrakt->hentTekst()
                    );
                    $utskrift = $this->app->hentVisning(Utskrift::class, [
                        'tittel' => 'Leieavtale ' . $kontrakt->hentId(),
                        'innhold' => $avtaleBeholder,
                    ]);
                }
                catch (Feilmelding $e) {
                    $utskrift = $e->getMessage();
                }
                catch (\Throwable $e) {
                    $this->app->loggException($e);
                    $utskrift = $e->getMessage();
                }
                $this->settRespons(new Respons($utskrift));
                return $this;
            }

            default:
                $resultat = (object)[
                    'success' => false,
                    'msg' => 'Ønsket data er ikke oppgitt',
                    'data' => []
                ];
                $this->settRespons(new JsonRespons($resultat));
                return $this;
        }
    }
}