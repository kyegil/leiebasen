<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use DateTimeImmutable;
use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjektsett;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\ViewRenderer\ViewArray;

class OversiktUtleietabell extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
//        'oppgave' => OversiktUtleietabell\Oppdrag::class
    ];
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $fra = !empty($_GET['fra']) ? new DateTimeImmutable($_GET['fra']) : null;
        $til = !empty($_GET['til']) ? new DateTimeImmutable($_GET['til']) : null;
        $leieobjektIder = !empty($_GET['leieobjekter']) ? explode(',', $_GET['leieobjekter']) : null;
        $bygningIder = !empty($_GET['bygninger']) ? explode(',', $_GET['bygninger']) : null;
        $iDag = new DateTimeImmutable();
        if(!$fra) {
            if($iDag->format('m') < 4) {
                $fra = new DateTimeImmutable(((int)$iDag->format('Y') - 1) . '-07-01');
            }
            else if($iDag->format('m') > 9) {
                $fra = new DateTimeImmutable($iDag->format('Y' . '-07-01'));
            }
            else {
                $fra = new DateTimeImmutable($iDag->format('Y' . '-01-01'));
            }
        }
        if(!$til) {
            if($iDag->format('m') < 4) {
                $til = new DateTimeImmutable($iDag->format('Y' . '-06-30'));
            }
            else if($iDag->format('m') > 9) {
                $til = new DateTimeImmutable(((int)$iDag->format('Y') + 1) . '-06-30');
            }
            else {
                $til = new DateTimeImmutable($iDag->format('Y' . '-12-31'));
            }
        }
        $leieobjekter = null;
        if($leieobjektIder || $bygningIder) {
            /** @var Leieobjektsett $leieobjekter */
            $leieobjekter = $this->hentSamling(Leieobjekt::class);
            if($leieobjektIder) {
                $leieobjekter->filtrerEtterIdNumre($leieobjektIder);
            }
            if($bygningIder) {
                $leieobjekter->leggTilFilter(['`' . Leieobjekt::hentTabell() . '`.`bygning`' => $bygningIder]);
            }
            $leieobjekter->låsFiltre();
        }
        $this->hoveddata['fra'] = $fra;
        $this->hoveddata['til'] = $til;
        $this->hoveddata['leieobjekter'] = $leieobjekter;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML(): bool
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = 'Utleietabell';
        return true;
    }

    /**
     * @return ViewArray
     */
    public function innhold(): ViewArray
    {
        /** @var DateTimeImmutable $fra */
        $fra = $this->hoveddata['fra'];
        /** @var DateTimeImmutable $til */
        $til = $this->hoveddata['til'];
        /** @var Leieobjektsett|null $leieobjekter */
        $leieobjekter = $this->hoveddata['leieobjekter'];

        $visning = new ViewArray();
        $visning->addItem(
            $this->vis(\Kyegil\Leiebasen\Visning\drift\html\OversiktUtleietabell::class, [
                'fra' => $fra,
                'til' => $til,
                'leieobjekter' => $leieobjekter,
            ])
        );
        return $visning;
    }

    /**
     * @return ViewArray
     * @noinspection PhpUnnecessaryLocalVariableInspection
     */
    public function skript(): ViewArray
    {
        $skript = new ViewArray();
        return $skript;
    }
}