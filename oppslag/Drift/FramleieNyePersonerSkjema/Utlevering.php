<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\FramleieNyePersonerSkjema;


use Exception;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\FramleieSkjema
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData($data = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case "adressekort":
                    /** @var Person|null $adressekort */
                    $adressekort = $this->app->hentModell(Person::class, $this->data['get']['adressekort'] ?? null);
                    if (!$adressekort->hentId()) {
                        throw new Exception('Fant ikke adressekortet');
                    }
                    $resultat->data = (object)[
                        'adressekort' => $adressekort->hentId(),
                        'er_org' => $adressekort->erOrg,
                        'fornavn' => $adressekort->fornavn,
                        'etternavn' => $adressekort->etternavn,
                        'fødselsdato' => $adressekort->fødselsdato ? $adressekort->fødselsdato->format('Y-m-d') : null,
                        'personnr' => $adressekort->personnr,
                        'adresse1' => $adressekort->adresse1,
                        'adresse2' => $adressekort->adresse2,
                        'postnr' => $adressekort->postnr,
                        'poststed' => $adressekort->poststed,
                        'land' => $adressekort->land,
                        'telefon' => $adressekort->telefon,
                        'mobil' => $adressekort->mobil,
                        'epost' => $adressekort->epost
                    ];
                    break;

                default:
                    break;
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}