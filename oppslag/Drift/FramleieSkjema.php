<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleie;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\framleie_skjema\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * FramleieSkjema Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class FramleieSkjema extends DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Framleie';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => FramleieSkjema\Oppdrag::class
    ];


    /**
     * FramleieSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $framleieId = !empty($_GET['id']) ? $_GET['id'] : null;
        $leieforholdId = $_GET['leieforhold'] ?? null;
        $this->hoveddata['framleieforhold'] = $this->hoveddata['leieforhold'] = null;

        if ((int)$framleieId) {
            $this->hoveddata['framleieforhold'] = $this->hentModell(Framleie::class, $framleieId);
        }
        else if ($leieforholdId) {
            $this->hoveddata['leieforhold'] = $this->hentModell(Leieforhold::class, $leieforholdId);
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML(): bool
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Framleie $framleieforhold */
        $framleieforhold = $this->hoveddata['framleieforhold'];
        if (
            $framleieforhold
            && !$framleieforhold->hentId()
        ) {
            return false;
        }
        else if ($framleieforhold) {
            $this->tittel = 'Framleie av ' . $framleieforhold->leieforhold->leieobjekt->hentBeskrivelse();
        }
        else {
            $leieforhold = $this->hoveddata['leieforhold'];
            if($leieforhold) {
                $this->tittel = 'Framleie av ' . $leieforhold->leieobjekt->hentBeskrivelse();
            }
        }

        return true;
    }

    /**
     * @return Index
     * @throws Exception
     */
    public function skript(): Index {
        /** @var Framleie|null $framleieforhold */
        $framleieforhold = $this->hoveddata['framleieforhold'];
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $framleieforhold ? $framleieforhold->leieforhold : null;
        if(!$leieforhold || !$leieforhold->hentId()) {
            $leieforhold = $this->hoveddata['leieforhold'];
        }

        if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }

        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'framleieforhold' => $framleieforhold,
                'leieforhold' => $leieforhold,
                'tilbakeKnapp' => $this->returi->get()
            ])
        ]);
        return $skript;
    }
}