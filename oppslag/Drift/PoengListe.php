<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Poengprogram;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\Drift\PoengListe\Utlevering;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\Leiebasen\Visning;
use Kyegil\ViewRenderer\ViewArray;

/**
 * PoengListe Controller
 *
 * @method Utlevering hentUtlevering()
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class PoengListe extends DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [];

    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $programKode = $_GET['kode'] ?? '';
        $leieforholdId = !empty($_GET['id']) ? $_GET['id'] : null;
        $program = $this->hentPoengbestyrer()->hentProgram($programKode);
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $leieforholdId
          ? $this->hentModell(Leieforhold::class, $leieforholdId)
            : null;
        $this->hoveddata['leieforhold'] = $leieforhold;
        $this->hoveddata['leieforholdId'] = $leieforholdId;
        $this->hoveddata['program'] = $program;
        $this->hoveddata['programKode'] = $programKode;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Poengprogram $program */
        $program = $this->hoveddata['program'];
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        if(($leieforhold && !$leieforhold->hentId())
            || !$program || !$program->hentId()
        ) {
            return false;
        }
        $this->tittel = $program->hentNavn();
        if ($leieforhold) {
            $this->tittel .= ' – Leieforhold ' . $leieforhold->hentId();
        }
        return true;
    }

    /**
     * @return Visning
     */
    public function innhold()
    {
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        /** @var Poengprogram $program */
        $program = $this->hoveddata['program'];

        return $this->vis(\Kyegil\Leiebasen\Visning\drift\html\poeng_liste\Index::class, [
            'leieforhold' => $leieforhold,
            'leieforholdId' => $this->hoveddata['leieforholdId'],
            'program' => $program,
            'programKode' => $this->hoveddata['programKode'],
        ]);
    }

    /**
     * @return Visning
     */
    public function skript(): ViewArray
    {
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        /** @var Poengprogram $program */
        $program = $this->hoveddata['program'];

        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];

        $skript = new ViewArray([]);
        return $skript;
    }
}