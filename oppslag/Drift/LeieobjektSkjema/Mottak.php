<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\LeieobjektSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Egenskapsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Mottak extends AbstraktMottak
{
    /**
     * @param string|null $skjema
     * @return $this
     */
    public function taIMot($skjema = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($skjema) {
                default:
                    $leieobjektId = $this->data['get']['id'] ?? null;
                    $aktivert = !isset($this->data['post']['aktivert']) || boolval($this->data['post']['aktivert']);
                    $navn = isset($this->data['post']['navn']) ? trim($this->data['post']['navn']) : '';
                    $etasje = isset($this->data['post']['etasje']) ? strtolower(trim($this->data['post']['etasje'])) : '';
                    $gateadresse = $this->data['post']['gateadresse'] ?? '';
                    /** @var Bygning|null $bygning */
                    $bygning = !empty($this->data['post']['bygning']) ? $this->app->hentModell(Bygning::class, $this->data['post']['bygning']) : null;
                    $beskrivelse = $this->data['post']['beskrivelse'] ?? '';
                    $postnr = $this->data['post']['postnr'] ?? '';
                    $poststed = $this->data['post']['poststed'] ?? '';
                    $boenhet = !isset($this->data['post']['boenhet']) || $this->data['post']['boenhet'];
                    $areal = !empty($this->data['post']['areal']) ? floatval($this->data['post']['areal']) : null;
                    $antallRom = !empty($this->data['post']['ant_rom']) ? $this->data['post']['ant_rom'] : null;
                    $bad = isset($this->data['post']['bad']) && $this->data['post']['bad'];
                    $toalettKategori = $this->data['post']['toalett_kategori'] ?? 0;
                    $toalett = $this->data['post']['toalett'] ?? '';
                    /** @var Leieberegning|null $leieberegning */
                    $leieberegning = $this->app->hentModell(Leieberegning::class, $this->data['post']['leieberegning'] ?? null);
                    $merknader = $this->data['post']['merknader'] ?? '';
                    $bilde = isset($this->data['files']['bildefelt'][0]['name']) && $this->data['files']['bildefelt'][0]['name'] ? $this->data['files']['bildefelt'][0] : null;
                    $områdeValg = $this->data['post']['områder'] ?? [];

                    if (!$leieobjektId) {
                        throw new Exception('Leieobjekt-id er ikke angitt');
                    }

                    if ($leieobjektId == '*') {
                        /** @var Leieobjekt $leieobjekt */
                        $leieobjekt = $this->app->nyModell(Leieobjekt::class, (object)[
                            'bilde' => '',
                            'merknader' => $merknader
                        ]);
                    }
                    else {
                        /** @var Leieobjekt $leieobjekt */
                        $leieobjekt = $this->app->hentModell(Leieobjekt::class, $leieobjektId);
                        if (!$leieobjekt->hentId()) {
                            throw new Exception('Finner ikke leieobjektet. (Angitt id: ' . $leieobjektId);
                        }
                    }

                    if(!$leieberegning->hentId()) {
                        throw new Exception('Ugyldig leieberegningsmodell');
                    }
                    $leieobjekt
                        ->settNavn($navn)
                        ->settIkkeForUtleie(!$aktivert)
                        ->settEtg($etasje)
                        ->settGateadresse($gateadresse)
                        ->settBygning($bygning)
                        ->settBeskrivelse($beskrivelse)
                        ->settPostnr($postnr)
                        ->settPoststed($poststed)
                        ->settBoenhet($boenhet)
                        ->settAreal($areal)
                        ->settAntRom($antallRom)
                        ->settBad($bad)
                        ->settToalettKategori($toalettKategori)
                        ->settToalett($toalett)
                        ->settLeieberegning($leieberegning)
                        ->settMerknader($merknader);

                    $leieobjekt->fjernAllOmrådeDeltakelse();
                    $this->settOmråder($leieobjekt, $områdeValg);

                    if ($bilde) {
                        $this->lastOppOgLagreBilde($bilde, $leieobjekt);
                    }

                    $this->settEavVerdier($leieobjekt);

                    $resultat->id = $leieobjekt->hentId();
                    $resultat->msg = 'Lagret';
                    $resultat->url = strval($this->app->returi->get(1));
                    break;
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param array $bilde
     * @param Leieobjekt $leieobjekt
     * @return $this
     */
    protected function lastOppOgLagreBilde(array $bilde, Leieobjekt $leieobjekt): Mottak
    {
        $sti = \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['root'] . "pub/media/opplastet/leieobjekter/{$leieobjekt->id}/";
        $filnavn = $sti . basename($bilde["name"]);
        $filendelse = pathinfo($filnavn, PATHINFO_EXTENSION);
        $sjekk = getimagesize($bilde["tmp_name"]);

        if($bilde['error']) {
            throw new Exception('Det skjedde en feil under opplastingen av bildet, og det kunne ikke lagres');
        }

        if($sjekk == false) {
            throw new Exception('Den opplastede fila er ikke et bilde.');
        }

        if(!in_array(strtolower($filendelse), ['jpg','jpeg', 'png', 'gif'])) {
            throw new Exception('Det opplastede bildet må være av typen JPG/JPEG, PNG eller GIF.');
        }

        if ($bilde["size"] > 1000000) {
            throw new Exception('Bildet er for stort. Maks. filstørrelse er 1Mb.');
        }

        if (!file_exists($sti)) {
            if(!mkdir($sti, 0777, true)) {
                throw new Exception("Klarte ikke finne eller opprette filplasseringen '{$sti}'.");
            }
        }

        if(file_exists($leieobjekt->bilde)) {
            unlink($leieobjekt->bilde);
        }

        if (!move_uploaded_file($bilde["tmp_name"], $filnavn)) {
            throw new Exception("Bildet kunne ikke lagres som '{$filnavn}' pga ukjent feil.");
        }

        $leieobjekt->bilde = "/pub/media/opplastet/leieobjekter/{$leieobjekt->id}/" . basename($bilde["name"]);
        return $this;
    }

    /**
     * @param Leieobjekt $leieobjekt
     * @param array $områdeValg
     * @return $this
     * @throws Exception
     */
    private function settOmråder(Leieobjekt $leieobjekt, array $områdeValg): Mottak
    {
        foreach ($områdeValg as $områdeId => $valgt) {
            if ($valgt) {
                /** @var Område $område */
                $område = $this->app->hentModell(Område::class, $områdeId);
                if ($område->hentId()) {
                    $leieobjekt->leggTilOmråde($område);
                }
            }
        }
        return $this;
    }

    /**
     * @param Leieobjekt $leieobjekt
     * @return Mottak
     * @throws \Kyegil\CoreModel\CoreModelException
     */
    private function settEavVerdier(Leieobjekt $leieobjekt): Mottak
    {
        /** @var Egenskapsett $eavEgenskaper */
        $eavEgenskaper = $this->app->hentSamling(Egenskap::class);
        $eavEgenskaper->leggTilEavVerdiJoin('felt_type', true);
        $eavEgenskaper->leggTilSortering('rekkefølge');
        $eavEgenskaper->leggTilFilter(['modell' => Leieobjekt::class])->låsFiltre();

        $eavEgenskaper->forEach(function(Egenskap $egenskap) use($leieobjekt) {
            $kode = $egenskap->kode;
            if(array_key_exists($kode, $this->data['post'])) {
                $leieobjekt->sett($kode, $this->data['post'][$kode]);
            }
        });

        return $this;
    }
}