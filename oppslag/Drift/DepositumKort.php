<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold\Depositum;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Visning\drift\html\depositum_kort\Index;
use Kyegil\Leiebasen\Visning\felles\html\shared\body\main\Kort;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class DepositumKort extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
//        'oppgave' => DepositumKort\Oppdrag::class
    ];
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $depositumId = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Depositum $depositum */
        $depositum = $this->hentModell(Depositum::class, (int)$depositumId);
        if (!$depositum->hentId()) {
            $depositum = null;
        }
        $this->hoveddata['depositum'] = $depositum;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Depositum $depositum */
        $depositum = $this->hoveddata['depositum'];
        if(!$depositum || !$depositum->hentId()) {
            return false;
        }
        $this->tittel = 'Depositum for ' . $depositum->leieforhold->hentNavn();

        return true;
    }

    /**
     * @return Kort
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Depositum $depositum */
        $depositum = $this->hoveddata['depositum'];

        $knapper = new ViewArray([
            $this->visTilbakeknapp()
        ]);

        if($depositum->hentFil()) {
            $knapper->addItem(new HtmlElement('a', [
                'href' => '/drift/index.php?oppslag=depositum_kort&oppdrag=hent_data&data=garanti&id=' . $depositum->hentId(),
                'class' => 'button'
            ], 'Vis garantien'));
        }

        $knapper->addItem(new HtmlElement('a', [
            'href' => '/drift/index.php?oppslag=depositum_skjema&id=' . $depositum->hentId(),
            'class' => 'button'
        ], 'Endre'));

        /** @var Kort $kort */
        $kort = $this->vis(Kort::class, [
            'innhold' => $this->vis(Index::class, [
                'depositum' => $depositum,
            ]),
            'knapper' => $knapper
        ]);
        return $kort;
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function skript(): ViewInterface
    {
        /** @var Depositum $depositum */
        $depositum = $this->hoveddata['depositum'];

        $skript = new ViewArray([]);
        return $skript;
    }
}