<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\BygningSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Mottak extends AbstraktMottak
{
    /**
     * @param string|null $skjema
     * @return $this
     */
    public function taIMot($skjema = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($skjema) {
                default:
                    $bygningsId = $this->data['get']['id'] ?? null;
                    $kode = $this->data['post']['kode'] ?? null;
                    $navn = $this->data['post']['navn'] ?? null;
                    $bilde = isset($this->data['files']['bildefelt']['name']) && $this->data['files']['bildefelt']['name'] ? $this->data['files']['bildefelt'] : null;
                    $områdeValg = $this->data['post']['områder'] ?? [];

                    if (!$bygningsId) {
                        throw new Exception('Bygnings-id er ikke angitt');
                    }

                    if ($bygningsId == '*') {
                        /** @var Bygning $bygning */
                        $bygning = $this->app->nyModell(Bygning::class, (object)[
                            'kode' => $kode,
                            'navn' => $navn
                        ]);
                    }
                    else {
                        /** @var Bygning $bygning */
                        $bygning = $this->app->hentModell(Bygning::class, $bygningsId);
                        if (!$bygning->hentId()) {
                            throw new Exception('Finner ikke bygningen. (Angitt id: ' . $bygningsId);
                        }
                        $bygning->kode = $kode;
                        $bygning->navn = $navn;
                    }
                    $resultat->id = $bygning->hentId();

                    $bygning->fjernAllOmrådeDeltakelse();
                    $this->settOmråder($bygning, $områdeValg);

                    if ($bilde) {
                        $this->lastOppOgLagreBilde($bilde, $bygning);
                    }


                    $resultat->msg = 'Lagret';
                    $resultat->url = strval($this->app->returi->get(1));
                    break;
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param array $bilde
     * @param Bygning $bygning
     * @return $this
     */
    protected function lastOppOgLagreBilde(array $bilde, Bygning $bygning): Mottak
    {
        $sti = \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['root'] . "pub/media/opplastet/bygninger/{$bygning->id}/";
        $filnavn = $sti . basename($bilde["name"]);
        $filendelse = pathinfo($filnavn, PATHINFO_EXTENSION);
        $sjekk = getimagesize($bilde["tmp_name"]);

        if($bilde['error']) {
            throw new Exception('Det skjedde en feil under opplastingen av bildet, og det kunne ikke lagres');
        }

        if($sjekk == false) {
            throw new Exception('Den opplastede fila er ikke et bilde.');
        }

        if(!in_array(strtolower($filendelse), ['jpg','jpeg', 'png', 'gif'])) {
            throw new Exception('Det opplastede bildet må være av typen JPG/JPEG, PNG eller GIF.');
        }

        if ($bilde["size"] > 1000000) {
            throw new Exception('Bildet er for stort. Maks. filstørrelse er 1Mb.');
        }

        if (!file_exists($sti)) {
            if(!mkdir($sti)) {
                throw new Exception("Klarte ikke finne eller opprette filplasseringen '{$sti}'.");
            }
        }

        if(file_exists($bygning->bilde)) {
            unlink($bygning->bilde);
        }

        if (!move_uploaded_file($bilde["tmp_name"], $filnavn)) {
            throw new Exception("Bildet kunne ikke lagres som '{$filnavn}' pga ukjent feil.");
        }

        $bygning->bilde = $filnavn;
        return $this;
    }

    /**
     * @param Bygning $bygning
     * @param array $områdeValg
     * @return $this
     * @throws Exception
     */
    private function settOmråder(Bygning $bygning, array $områdeValg)
    {
        foreach ($områdeValg as $områdeId => $valgt) {
            if ($valgt) {
                /** @var Område $område */
                $område = $this->app->hentModell(Område::class, $områdeId);
                if ($område->hentId()) {
                    $bygning->leggTilOmråde($område);
                }
            }
        }
        return $this;
    }
}