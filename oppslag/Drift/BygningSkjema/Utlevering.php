<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\BygningSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Samling;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\BygningSkjema
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData($data = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case "områder":
                    $områdeFilter = [];
                    if (isset($this->data['get']['query']) && trim($this->data['get']['query'])) {
                        $query = explode(' ', trim($this->data['get']['query']));
                        foreach ($query as $ord) {
                            $områdeFilter[] = [
                                'or' => [
                                    'id' => $ord,
                                    'navn LIKE' => '%' . $ord . '%',
                                ]
                            ];
                        }
                    }
                    /** @var Samling $områdesett */
                    $områdesett = $this->app->hentSamling(Område::class)
                        ->leggTilSortering('navn')

                        ->leggTilFilter($områdeFilter);

                    /** @var Område $område */
                    foreach ($områdesett as $område) {
                        $resultat->data[] = [
                            'value' => $område->hentId(),
                            'text' => $område->hentNavn(),
                        ];
                    }
                    break;

                default:
                    $områdeId = !empty($_GET['id']) ? $_GET['id'] : null;
                    if (!$områdeId) {
                        throw new Exception('id-parameter mangler');
                    }
                    /** @var Område $område */
                    $område = $this->app->hentModell(Område::class, $områdeId);
                    if ($område->hentId()) {
                        $resultat->data = (object) [
                            'id' => $område->hentId(),
                            'navn' => $område->navn,
                            'beskrivelse' => $område->beskrivelse
                        ];
                    }

                    else {
                        throw new Exception('dette området finnes ikke');
                    }
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}