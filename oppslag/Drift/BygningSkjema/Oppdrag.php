<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\BygningSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function slettBygning() {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            $bygningsId = $this->data['post']['bygnings_id'];
            /** @var Bygning $bygning */
            $bygning = $this->app->hentModell(Bygning::class, $bygningsId);
            if (!$bygning->hentId()) {
                throw new Exception('Finner ikke bygningen');
            }
            $resultat->msg = $bygning->navn . ' har blitt slettet';
            $bygning->slett();
            $resultat->url = $this->app->returi->get();
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}