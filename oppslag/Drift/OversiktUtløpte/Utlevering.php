<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\OversiktUtløpte;


use DateTime;
use DateInterval;
use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold as LeieforholdModell;
use Kyegil\Leiebasen\Modell\Leieforhold\Oppsigelse;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\OversiktGjeld
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null) {
        switch ($data) {
            default: {
                try {
                    $utløpte = new ViewArray();
                    $framtildato = date_create()->add(new DateInterval('P6M'));
                    /** @var \Kyegil\Leiebasen\Modell\Leieforholdsett $leieforholdsett */
                    $leieforholdsett = $this->app->hentSamling(LeieforholdModell::class);
                    $leieforholdsett->leggTilLeftJoinForOppsigelse();
                    $leieforholdsett
                        ->leggTilFilter(['`' . LeieforholdModell::hentTabell() . '`.`tildato` <' => $framtildato->format('Y-m-d')])
                        ->leggTilFilter(['`' . LeieforholdModell::hentTabell() . '`.`tildato` IS NOT NULL'])
                        ->leggTilFilter(['`' . Oppsigelse::hentTabell() . '`.`' . Oppsigelse::hentPrimærnøkkelfelt() . '`' => null])
                    ;
                    $leieforholdsett->leggTilSortering('tildato');

                    foreach($leieforholdsett as $leieforhold) {
                        $utløpte->addItem($this->app->vis( \Kyegil\Leiebasen\Visning\drift\html\oversikt_utløpte\extjs4\panel\Leieforhold::class,['leieforhold' => $leieforhold]));
                    }
                    $this->settRespons(new Respons($utløpte));

                } catch (Exception $e) {
                    $this->settRespons(new Respons($e));
                }
                return $this;
            }
        }
    }
}