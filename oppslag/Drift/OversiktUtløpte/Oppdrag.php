<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\OversiktUtløpte;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Regnskapsprosjekt;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function opphevTidsbegrensning() {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            $leieforholdId = $this->data['post']['leieforhold_id'];
            /** @var Leieforhold $leieforhold */
            $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);
            if (!$leieforhold->hentId()) {
                throw new Exception('Finner ikke leieforholdet');
            }
            $leieforhold->tildato = null;
            $resultat->msg = 'Leieforhold ' . $leieforhold->hentId() . ' har blitt tids-ubestemt';
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}