<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\LeiereguleringSkjema;


use Exception;
use Kyegil\Leiebasen\Leieregulerer;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Respons\JsonRespons;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\LeiereguleringSkjema
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null): Utlevering
    {
        $get = $this->data['get'];
        $post = $this->data['post'];

        switch ($data) {
            case 'reguleringsforslag': {
                $resultat = (object)[
                    'success' => true,
                    'error' => null,
                    'msg' => null,
                    'data' => new \stdClass(),
                ];

                try {
                    $leieregulerer = Leieregulerer::getInstance($this->app);
                    $leieforholdId = $get['leieforhold'] ?? null;
                    /** @var Leieforhold $leieforhold */
                    $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);
                    $justeringsdato = date_create_immutable($get['justeringsdato'] ?? 'now');
                    $basisLeie = $leieregulerer->foreslåJustertBasisLeie($leieforhold, $justeringsdato);
                    $resultat->data->basisLeiebeløp = $basisLeie;
                    $resultat->data->leieelementer = (object)[
                        'basisleie' => $basisLeie,
                        'bruttoleie' => null,
                        'antTerminer' => $leieforhold->antTerminer,
                        'terminleie' => null,
                        'delbeløp' => new \stdClass()
                    ];

                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->error = $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }
            case 'varselutskrift':
                try {
                    $innhold = $get['innhold'] ?? '';
                    $resultat = $this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\shared\Utskrift::class, [
                        'tittel' => 'Varsel om leieregulering',
                        'innhold' => $innhold
                    ]);
                }
                catch (Exception $e) {
                    $resultat = $e;
                }
                $this->settRespons(new Respons($resultat));
                return $this;
            default:
                $resultat = (object)[
                    'success' => false,
                    'msg' => 'Ønsket data er ikke oppgitt',
                    'data' => []
                ];
                $this->settRespons(new JsonRespons($resultat));
                return $this;
        }
    }
}