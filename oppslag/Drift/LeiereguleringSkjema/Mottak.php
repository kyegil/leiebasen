<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/02/2023
 * Time: 15:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\LeiereguleringSkjema;


use Exception;
use Kyegil\Leiebasen\Leieregulerer;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Leiereguleringen er gjennomført."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'leieregulering':
                    $leieregulerer = Leieregulerer::getInstance($this->app);
                    $id = $get['id'] ?? null;
                    $leieforholdId = $post['id'] ?? null;
                    $justeringsdato = isset($post['justeringsdato']) ? new \DateTimeImmutable($post['justeringsdato']) : null;
                    /** @var float $basisLeie */
                    $basisLeie = $post['basis_leiebeløp'] ?? 0;
                    /** @var array $delkravSatser */
                    $delkravSatser = $post['delkrav'] ?? [];
                    /** @var string $justeringsvarsel */
                    $justeringsvarsel = $post['justeringsvarsel'] ?? '';

                    /** @var Modell\Leieforhold|null $leieforhold */
                    $leieforhold = $this->app->hentModell(Modell\Leieforhold::class, $leieforholdId);
                    if (!$leieforhold->hentId() || $leieforhold->hentId() != $id) {
                        throw new Exception('Leieforhold ' . $leieforholdId . ' finnes ikke');
                    }
                    if ($leieforhold->hentId() != $id) {
                        throw new Exception('Ugyldig leieforhold ' . $id);
                    }

                    if($leieforhold->tildato && $leieforhold->tildato <= $justeringsdato) {
                        throw new Exception('Leieforholdet utløper ' . $leieforhold->tildato->format('d.m.Y') . ' og må fornyes før leien kan justeres.');
                    }

                    $varselType = $leieregulerer->hentVarseltypeForLeieforhold($leieforhold);

                    $leieforholdDelkravtyper = $leieforhold->hentDelkravtyper();

                    foreach ($leieforholdDelkravtyper as $leieforholdDelkravtype) {
                        if ($leieforholdDelkravtype->delkravtype->valgfritt) {
                            $kode = $leieforholdDelkravtype->hentKode();
                            $nySats = floatval($delkravSatser[$kode] ?? $leieforholdDelkravtype->hentSats());
                        }
                        else {
                            $nySats = $leieforholdDelkravtype->delkravtype->sats;
                        }
                        $leieforholdDelkravtype->settSats($nySats);
                    }

                    $leieregulerer->sendReguleringsVarsel($leieforhold, $justeringsvarsel, $varselType);

                    $leieforhold->settÅrligBasisleie($basisLeie);
                    $leieforhold->oppdaterLeieHistorikk(true, $justeringsdato);
                    $leieforhold->opprettLeiekrav($justeringsdato, true);

                    /** Registrer justeringsvarselet som et notat på leieforholdet */
                    $this->app->nyModell(Notat::class, (object)[
                        'leieforhold' => $leieforhold,
                        'dato' => date_create_immutable(),
                        'henvendelse_fra' => Notat::FRA_UTLEIER,
                        'kategori' => 'brev',
                        'brevtekst' => $justeringsvarsel,
                    ]);

                    $resultat->url = (string)$this->app->returi->get(1);

                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}