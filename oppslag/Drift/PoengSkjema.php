<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Poengprogram;
use Kyegil\Leiebasen\Modell\Poengprogram\Poeng;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class PoengSkjema extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => PoengSkjema\Oppdrag::class
    ];
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $programKode = $_GET['kode'] ?? '';
        $poengId = !empty($_GET['id']) ? $_GET['id'] : null;
        $leieforholdId = $_GET['leieforhold'] ?? null;

        $program = $this->hentPoengbestyrer()->hentProgram($programKode);
        /** @var Poeng|null $poeng */
        $poeng = $this->hentModell(Poeng::class, (int)$poengId);
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hentModell(Leieforhold::class, $leieforholdId);
        if(!$leieforhold->hentId()) {
            $leieforhold = null;
        }

        if (!$poeng->hentId() || $poengId == '*') {
            $poeng = null;
        }
        if($poeng) {
            $program = $poeng->program;
            $programKode = $program->hentKode();
        }
        $this->hoveddata['leieforhold'] = $leieforhold;
        $this->hoveddata['poeng'] = $poeng;
        $this->hoveddata['poengId'] = $poengId;
        $this->hoveddata['program'] = $program;
        $this->hoveddata['programKode'] = $programKode;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Poengprogram $program */
        $program = $this->hoveddata['program'];
        /** @var Poeng|null $poeng */
        $poeng = $this->hoveddata['poeng'];
        if(!$program) {
            return false;
        }
        if(!$poeng && $this->hoveddata['poengId'] != '*') {
            return false;
        }
        $this->tittel = $poeng ? $poeng->hentTekst() : $program->hentNavn();
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Poengprogram $program */
        $program = $this->hoveddata['program'];
        /** @var Poeng|null $poeng */
        $poeng = $this->hoveddata['poeng'];

        return $this->vis(\Kyegil\Leiebasen\Visning\drift\html\poeng_skjema\Index::class, [
            'poeng' => $poeng,
            'poengId' => $this->hoveddata['poengId'],
            'program' => $program,
            'programKode' => $this->hoveddata['programKode'],
            'leieforhold' => $this->hoveddata['leieforhold'],
        ]);
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function skript(): ViewInterface
    {
        /** @var Poengprogram $program */
        $program = $this->hoveddata['program'];
        /** @var Poeng|null $poeng */
        $poeng = $this->hoveddata['poeng'];

        $skript = new ViewArray([
            $this->vis(\Kyegil\Leiebasen\Visning\drift\js\poeng_skjema\Index::class, [
                'poeng' => $poeng,
                'poengId' => $this->hoveddata['poengId'],
                'program' => $program,
                'programKode' => $this->hoveddata['programKode'],
            ]),
        ]);
        return $skript;
    }
}