<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderKostnadListe\Mottak;
use Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderKostnadListe\Utlevering;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_kostnad_liste\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index as JsIndex;

/**
 * DelteKostnaderKostnadListe Controller
 *
 * @method Utlevering hentUtlevering()
 * @method Mottak hentMottak()
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class DelteKostnaderKostnadListe extends DriftExtJsAdaptor
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => DelteKostnaderKostnadListe\Oppdrag::class
    ];
    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = "Kostnader for fordeling blant beboere";
        return true;
    }

    /**
     * @return Visning
     */
    public function skript()
    {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }

        /** @var JsIndex $skript */
        $skript = $this->vis(JsIndex::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'bruker' => $bruker,
                'tilbakeKnapp' => $this->returi->get()
            ])
        ]);
        return $skript;
    }
}