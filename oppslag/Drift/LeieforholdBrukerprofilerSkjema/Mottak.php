<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\LeieforholdBrukerprofilerSkjema;


use Exception;
use Kyegil\CoreModel\CoreModelException;
use Kyegil\Leiebasen\Autoriserer;
use Kyegil\Leiebasen\EventManager;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Egenskapsett;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Modell\Person\Brukerprofil;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;
use Throwable;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema):Mottak
    {
        /** @var stdClass $resultat */
        $resultat = (object)[
            'success' => true,
            'msg' => "Lagret."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'brukerprofilskjema':
                    $leieforholdId = $get['id'] ?? null;
                    $personer = $post['personer'] ?? [];
                    /** @var Autoriserer $autoriserer */
                    $autoriserer = $this->app->hentAutoriserer();
                    $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);

                    foreach($leieforhold->hentLeietakere() as $leietaker) {

                        try {
                            $person = $leietaker->person;
                            $personData = $personer[$person->hentId()] ?? [];
                            $login = trim($personData['brukerprofil']['login'] ?? '') ?: '';
                            if ($login) {
                                if ($personData['id'] != $person->hentId()) {
                                    throw new Exception('Ugyldig person-id ' . $personData['id']);
                                }
                                if (!empty($personData['epost']) && $personData['epost'] != $person->hentEpost()) {
                                    $person->settEpost($personData['epost']);
                                }
                                if (!$autoriserer->erLoginTilgjengelig($login, $person->hentId())) {
                                    throw new Feilmelding('Brukernavnet  ' . $login . ' er i bruk av andre.');
                                }
                                $brukerProfil = $person->hentBrukerProfil();
                                if ($nyBrukerprofil = !$brukerProfil) {
                                    $brukerProfil = $this->app->nyModell(Brukerprofil::class, (object)[
                                        'person' => $person,
                                        'login' => $login
                                    ]);
                                }
                                $brukerProfil->settLogin($login);

                                /* Sett passord */
                                if (!empty($personData['brukerprofil']['sett_passord'])) {
                                    $pw1 = $personData['brukerprofil']['pw1'] ?? null;
                                    $pw2 = $personData['brukerprofil']['pw2'] ?? null;
                                    $brukerProfil->settPassord(Brukerprofil::validerPassord([$pw1, $pw2]));
                                }

                                $this->settBrukerprofilEavVerdier($brukerProfil, $personData['brukerprofil']);

                                $adgang = $person->hentAdgangTil(Adgang::ADGANG_MINESIDER, $leieforhold);
                                if($nyAdgang = !$adgang) {
                                    $adgang = $person->tildelAdgang(Adgang::ADGANG_MINESIDER, $leieforhold);
                                }

                                if(isset($personData['adgang']['mine-sider'][$leieforhold->hentId()])) {
                                    $adgang->epostvarsling = !empty($personData['adgang']['mine-sider'][$leieforhold->hentId()]['epostvarsling']);
                                }

                                EventManager::getInstance()->triggerEvent(
                                    Brukerprofil::class, 'endret',
                                    $brukerProfil, ['ny' => $nyBrukerprofil], $this->app
                                );

                                EventManager::getInstance()->triggerEvent(
                                    Adgang::class, 'endret',
                                    $adgang, ['ny' => $nyAdgang], $this->app
                                );

                                $brukerprofilPreferanser = $personData['brukerprofil_preferanser'] ?? null;
                                $person->settBrukerProfilPreferanse('reservasjon_mot_masseepost', !empty($brukerprofilPreferanser['reservasjon_mot_masseepost']));
                                $person->settBrukerProfilPreferanse('reservasjon_mot_massesms', !empty($brukerprofilPreferanser['reservasjon_mot_massesms']));

                                $leieobjekt = $leieforhold->leieobjekt;
                                $nyttSkadeAbonnement = $brukerprofilPreferanser['skade_auto_abonnement'] ?? null;
                                if($leieobjekt) {
                                    /** @var object $skadeAbonnementer */
                                    $skadeAbonnementer = $person->hentBrukerProfilPreferanse('skade_auto_abonnement');
                                    if(empty($nyttSkadeAbonnement['leieobjekter'])) {
                                        $skadeAbonnementer->leieobjekter = array_diff($skadeAbonnementer->leieobjekter, [$leieobjekt->hentId()]);
                                    }
                                    else {
                                        $skadeAbonnementer->leieobjekter[] = $leieobjekt->hentId();
                                        $skadeAbonnementer->leieobjekter = array_unique($skadeAbonnementer->leieobjekter);
                                    }
                                    if($bygning = $leieobjekt->bygning) {
                                        if(empty($nyttSkadeAbonnement['bygninger'])) {
                                            $skadeAbonnementer->bygninger = array_diff($skadeAbonnementer->bygninger, [$bygning->hentId()]);
                                        }
                                        else {
                                            $skadeAbonnementer->bygninger[] = $bygning->hentId();
                                            $skadeAbonnementer->bygninger = array_unique($skadeAbonnementer->bygninger);
                                        }
                                    }
                                    $person->settBrukerProfilPreferanse('skade_auto_abonnement', $skadeAbonnementer);
                                }
                            }
                        } catch (Feilmelding $e) {
                            $resultat->msg .= $e->getMessage() . "<br>";
                            continue;
                        }
                    }

                    $resultat->msg = 'Opplysningene er lagret';
                    $resultat->url = (string)$this->app->returi->get(1);
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Feilmelding $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        catch (Throwable $e) {
            $this->app->loggException($e);
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param Person $person
     * @return $this
     * @throws CoreModelException
     */
    private function settEavVerdier(Person $person, array $adressekort = []): Mottak
    {
        /** @var Egenskapsett $eavEgenskaper */
        $eavEgenskaper = $this->app->hentSamling(Egenskap::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $eavEgenskaper->leggTilEavVerdiJoin('felt_type', true);
        $eavEgenskaper->leggTilSortering('rekkefølge');
        $eavEgenskaper->leggTilFilter(['modell' => Person::class])->låsFiltre();

        $eavEgenskaper->forEach(function(Egenskap $egenskap) use($person, $adressekort) {
            $kode = $egenskap->kode;
            if(array_key_exists($kode, $adressekort)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                $person->sett($kode, $adressekort[$kode]);
            }
        });

        return $this;
    }

    /**
     * @param Brukerprofil $brukerprofil
     * @param array $data
     * @return $this
     * @throws CoreModelException
     */
    private function settBrukerprofilEavVerdier(Brukerprofil $brukerprofil, array $data = []): Mottak
    {
        /** @var Egenskapsett $eavEgenskaper */
        $eavEgenskaper = $this->app->hentSamling(Egenskap::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $eavEgenskaper->leggTilEavVerdiJoin('felt_type', true);
        $eavEgenskaper->leggTilSortering('rekkefølge');
        $eavEgenskaper->leggTilFilter(['modell' => Brukerprofil::class])->låsFiltre();

        $eavEgenskaper->forEach(function(Egenskap $egenskap) use($brukerprofil, $data) {
            $kode = $egenskap->kode;
            if(array_key_exists($kode, $data)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                $brukerprofil->sett($kode, $data[$kode]);
            }
        });
        return $this;
    }

    /**
     * @param Adgang $adgang
     * @param array $data
     * @return $this
     * @throws CoreModelException
     */
    private function settAdgangEavVerdier(Adgang $adgang, array $data = []): Mottak
    {
        /** @var Egenskapsett $eavEgenskaper */
        $eavEgenskaper = $this->app->hentSamling(Egenskap::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $eavEgenskaper->leggTilEavVerdiJoin('felt_type', true);
        $eavEgenskaper->leggTilSortering('rekkefølge');
        $eavEgenskaper->leggTilFilter(['modell' => Adgang::class])->låsFiltre();

        $eavEgenskaper->forEach(function(Egenskap $egenskap) use($adgang, $data) {
            $kode = $egenskap->kode;
            if(array_key_exists($kode, $data)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                $adgang->sett($kode, $data[$kode]);
            }
        });
        return $this;
    }
}