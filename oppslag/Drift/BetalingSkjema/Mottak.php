<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 30/01/2023
 * Time: 17:10
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\BetalingSkjema;


use Exception;
use Kyegil\Leiebasen\EventManager;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var \stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => ''
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = isset($get['skjema']) ? $get['skjema'] : '';
        try {
            switch ($skjema) {
                case 'betalingsskjema':
                    $id = $get['id'] ?? null;
                    $dato = isset($post['dato']) ? new \DateTimeImmutable($_POST['dato']) : null;
                    $beløp = isset($post['beløp']) ? str_replace(',', '.', $_POST['beløp']): 0;
                    $betaler = $post['betaler'] ?? '';
                    $ref = $post['referanse'] ?? '';
                    $konto = $post['konto'] ?? '';
                    $merknad = $post['merknad'] ?? '';

                    if($id == '*') {
                        /** @var Innbetaling $innbetaling */
                        $innbetaling = $this->app->nyModell(Innbetaling::class, (object)[
                            'dato' => $dato,
                            'beløp' => $beløp,
                            'betaler' => $betaler,
                            'ref' => $ref,
                            'konto' => $konto,
                            'merknad' => $merknad,
                        ]);

                        EventManager::getInstance()->triggerEvent(
                            Innbetaling::class, 'endret',
                            $innbetaling, ['ny' => true], $this->app
                        );

                        $resultat->msg = 'Betalingen er lagret';
                        $resultat->url = "/drift/index.php?oppslag=innbetalingskort&id={$innbetaling->hentId()}";
                        break;
                    }

                    else {
                        /** @var Innbetaling $innbetaling */
                        $innbetaling = $this->app->hentModell(Innbetaling::class, $id);
                        if(!$innbetaling->hentInnbetaling()) {
                            throw new Exception('Finner ikke transaksjonen ' . $id);
                        }
                        $innbetaling
                            ->settDato($dato)
                            ->settBeløp($beløp)
                            ->settBetaler($betaler)
                            ->settRef($ref)
                            ->settKonto($konto)
                            ->settMerknad($merknad)
                            ->samle()
                        ;

                        EventManager::getInstance()->triggerEvent(
                            Innbetaling::class, 'endret',
                            $innbetaling, ['ny' => false], $this->app
                        );

                        $resultat->msg = 'Endringene er lagret';
                        $resultat->url = "/drift/index.php?oppslag=innbetalingskort&id={$innbetaling->hentId()}";
                        $this->app->returi->reset();
                        break;
                    }
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}