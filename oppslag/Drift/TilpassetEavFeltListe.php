<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\drift\html\tilpasset_eav_felt_liste\Index;

/**
 * TilpassetEavFeltListe Controller
 *
 * @method \Kyegil\Leiebasen\Oppslag\Drift\TilpassetEavFeltListe\Utlevering hentUtlevering()
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class TilpassetEavFeltListe extends DriftKontroller
{
    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = 'Spesialtilpassede felter';
        return true;
    }

    /**
     * @return Visning
     */
    public function innhold()
    {
        return $this->vis(Index::class);
    }
}