<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\_MalListe;


use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderKostnadListe;

/**
 * @property DelteKostnaderKostnadListe $app
 */
class Oppdrag extends AbstraktOppdrag
{
}