<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\_MalListe;


use Exception;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Samling;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\_MalListe
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     * @throws Exception
     * @link https://datatables.net/manual/server-side
     */
    public function hentData($data = null): Utlevering
    {
        $get = $this->data['get'];
        $post = $this->data['post'];

        switch ($data) {
            case '': {
                $resultat = (object)[
                    'success' => true,
                    'draw' => intval($get['draw'] ?? 0),
                    'data'		=> [],
                    'totalRows' => 0,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                ];

                $sorteringsmapping = [
                ];

                $sorteringsfelt	= $get['sort'] ?? null;
                $synkende = isset($get['dir']) && $get['dir'] == "DESC";
                if ($sorteringsfelt) {
                    $sortering = [[
                        'columnName' => $sorteringsfelt,
                        'dir' => $synkende ? 'desc' : 'asc'
                    ]];
                }
                else {
                    $sortering  = $get['order'] ?? [];
                }

                $start		= intval($get['start'] ?? 0);
                $limit		= $get['limit'] ?? 25;

                $søkefelt = $get['søkefelt'] ?? ($get['search']['value'] ?? '');

                try {
                    $samling = $this->hentSamling(
                        $søkefelt,
                        $sortering,
                        $start,
                        $limit
                    );
                    AbstraktUtlevering::sorter($samling, $sortering, $sorteringsmapping);

                    $resultat->recordsFiltered = $resultat->recordsTotal = $resultat->totalRows
                        = $samling->hentAntall();

                    foreach ($samling as $modell) {
                        $resultat->data[] = $this->hentLinjeData($modell);
                    }
                }
                catch (Feilmelding $e) {
                    $resultat->success = false;
                    $resultat->error = $resultat->msg = $e->getMessage();
                }
                catch (\Throwable $e) {
                    $this->app->loggException($e);
                    $resultat->success = false;
                    $resultat->error = $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }
            default:
                $resultat = (object)[
                    'success' => false,
                    'msg' => 'Ønsket data er ikke oppgitt',
                    'data' => []
                ];
                $this->settRespons(new JsonRespons($resultat));
                return $this;
        }
    }

    /**
     * @param string $søk
     * @param array $sortering
     * @param int $start
     * @param int $limit
     * @return Samling
     */
    private function hentSamling(string $søk, array $sortering, int $start, int $limit): Samling
    {
        list('søk' => $søk, 'sortering' => $sortering, 'start' => $start, 'limit' => $limit )
            = $this->app->before($this, __FUNCTION__, [
            'søk' => $søk, 'sortering' => $sortering, 'start' => $start, 'limit' => $limit
        ]);
        return $this->app->after($this, __FUNCTION__, null);
    }

    /**
     * @param Modell $leieforhold
     * @return object
     */
    private function hentLinjeData(Modell $modell): object
    {
        list('modell' => $modell)
            = $this->app->before($this, __FUNCTION__, [
            'modell' => $modell
        ]);
        $data = (object)[
        ];
        return $this->app->after($this, __FUNCTION__, $data);
    }
}