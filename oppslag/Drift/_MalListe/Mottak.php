<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\_MalListe;


use Exception;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;

/**
 *
 */
class Mottak extends AbstraktMottak
{
    /**
     * @param string|null $skjema
     * @return $this
     */
    public function taIMot($skjema = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => null,
            'data' => null
        ];

        try {
            switch ($skjema) {
                default:
                    throw new Exception('Skjema-parameter mangler');
            }
        }
        catch (Feilmelding $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        catch (\Throwable $e) {
            $this->app->loggException($e);
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}