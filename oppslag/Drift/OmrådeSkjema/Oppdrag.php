<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 04/12/2020
 * Time: 23:21
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\OmrådeSkjema;


use Exception;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function slettOmråde() {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            $områdeId = $this->data['post']['område_id'];
            /** @var Område $område */
            $område = $this->app->hentModell(Område::class, $områdeId);
            if (!$område->hentId()) {
                throw new Exception('Finner ikke området');
            }
            $resultat->msg = $område->navn . ' har blitt slettet';
            $område->slett();
            $resultat->url = $this->app->returi->get();
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}