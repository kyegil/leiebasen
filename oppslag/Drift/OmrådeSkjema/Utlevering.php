<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\OmrådeSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Samling;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\OmrådeSkjema
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData($data = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case "leieobjekter":
                    $leieobjektFilter = [];
                    if (isset($this->data['get']['query']) && trim($this->data['get']['query'])) {
                        $query = explode(' ', trim($this->data['get']['query']));
                        foreach ($query as $ord) {
                            $leieobjektFilter[] = [
                                'or' => [
                                    Leieobjekt::hentTabell() . '.`leieobjektnr`' => $ord,
                                    Leieobjekt::hentTabell() . '.`navn` LIKE' => '%' . $ord . '%',
                                    Leieobjekt::hentTabell() . '.`gateadresse` LIKE' => '%' . $ord . '%',
                                    Leieobjekt::hentTabell() . '.`beskrivelse` LIKE' => '%' . $ord . '%',
                                ]
                            ];
                        }
                    }
                    /** @var Samling $leieobjektsamling */
                    $leieobjektsamling = $this->app->hentSamling(Leieobjekt::class)
                        ->leggTilSortering(Leieobjekt::hentPrimærnøkkelfelt())

                        ->leggTilFilter($leieobjektFilter);

                    /** @var Leieobjekt $leieobjekt */
                    foreach ($leieobjektsamling as $leieobjekt) {
                        $resultat->data[] = [
                            'value' => $leieobjekt->hentId(),
                            'text' => $leieobjekt->hentId() . ' | ' . $leieobjekt->hentBeskrivelse(),
                        ];
                    }
                    break;

                case "bygninger":
                    $bygningFilter = [];
                    if (isset($this->data['get']['query']) && trim($this->data['get']['query'])) {
                        $query = explode(' ', trim($this->data['get']['query']));
                        foreach ($query as $ord) {
                            $bygningFilter[] = [
                                'or' => [
                                    'id' => $ord,
                                    'navn LIKE' => '%' . $ord . '%',
                                    'kode' => $ord,
                                ]
                            ];
                        }
                    }
                    /** @var Samling $bygningsett */
                    $bygningsett = $this->app->hentSamling(Bygning::class)
                        ->leggTilSortering('navn')

                        ->leggTilFilter($bygningFilter);

                    /** @var Bygning $bygning */
                    foreach ($bygningsett as $bygning) {
                        $resultat->data[] = [
                            'value' => $bygning->hentId(),
                            'text' => "{$bygning->hentNavn()} ({$bygning->hentKode()})",
                        ];
                    }
                    break;

                default:
                    $områdeId = !empty($_GET['id']) ? $_GET['id'] : null;
                    if (!$områdeId) {
                        throw new Exception('id-parameter mangler');
                    }
                    /** @var Område $område */
                    $område = $this->app->hentModell(Område::class, $områdeId);
                    if ($område->hentId()) {
                        $resultat->data = (object) [
                            'id' => $område->hentId(),
                            'navn' => $område->navn,
                            'beskrivelse' => $område->beskrivelse
                        ];
                    }

                    else {
                        throw new Exception('dette området finnes ikke');
                    }
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}