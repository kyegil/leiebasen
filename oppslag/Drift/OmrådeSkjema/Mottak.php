<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\OmrådeSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;

class Mottak extends AbstraktMottak
{
    /**
     * @param string|null $skjema
     * @return $this
     */
    public function taIMot($skjema = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($skjema) {
                default:
                    $områdeId = $this->data['get']['id'] ?? null;
                    $navn = isset($this->data['post']['navn']) ? trim($this->data['post']['navn']) : '';
                    $beskrivelse = $this->data['post']['beskrivelse'] ?? '';
                    $bygningsValg = $this->data['post']['bygninger'] ?? [];
                    $leieobjektValg = $this->data['post']['leieobjekter'] ?? [];

                    if (!$områdeId) {
                        throw new Exception('Område-id er ikke angitt');
                    }

                    if ($områdeId == '*') {
                        /** @var Område $område */
                        $område = $this->app->nyModell(Område::class, (object)[
                            'navn' => $navn,
                            'beskrivelse' => $beskrivelse
                        ]);
                    }
                    else {
                        /** @var Område $område */
                        $område = $this->app->hentModell(Område::class, $områdeId);
                        if (!$område->hentId()) {
                            throw new Exception('Finner ikke området. (Angitt id: ' . $områdeId);
                        }
                        $område->navn = $navn;
                        $område->beskrivelse = $beskrivelse;
                    }
                    $område->fjernAllDeltakelse();
                    $this->settBygninger($område, $bygningsValg);
                    $this->settLeieobjekter($område, $leieobjektValg);
                    $resultat->id = $område->hentId();
                    $resultat->msg = 'Lagret';
                    $resultat->url = strval($this->app->returi->get(1));
                    break;
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param Område $område
     * @param int[] $bygningsValg
     * @return $this
     * @throws Exception
     */
    private function settBygninger(Område $område, array $bygningsValg)
    {
        foreach ($bygningsValg as $bygningsId => $valgt) {
            if ($valgt) {
                /** @var Bygning $bygning */
                $bygning = $this->app->hentModell(Bygning::class, $bygningsId);
                if ($bygning->hentId()) {
                    $område->leggTilBygning($bygning);
                }
            }
        }
        return $this;
    }

    /**
     * @param Område $område
     * @param int[] $leieobjektValg
     * @return $this
     * @throws Exception
     */
    private function settLeieobjekter(Område $område, array $leieobjektValg)
    {
        foreach ($leieobjektValg as $leieobjektId => $valgt) {
            if ($valgt) {
                /** @var Leieobjekt $leieobjekt */
                $leieobjekt = $this->app->hentModell(Leieobjekt::class, $leieobjektId);
                if ($leieobjekt->hentId()) {
                    $område->leggTilLeieobjekt($leieobjekt);
                }
            }
        }
        return $this;
    }
}