<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class Eksport extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    public function preHTML()
    {
        $this->tittel = 'Eksport';
        return parent::preHTML();
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        return $this->vis(\Kyegil\Leiebasen\Visning\drift\html\eksport\Index::class);
    }

    /**
     * @return ViewInterface
     */
    public function skript(): ViewInterface
    {
        $skript = new ViewArray([
            $this->vis(\Kyegil\Leiebasen\Visning\drift\js\eksport\Index::class),
        ]);
        return $skript;
    }
}