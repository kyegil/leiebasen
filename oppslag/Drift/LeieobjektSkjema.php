<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;

/**
 * LeieobjektSkjema Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class LeieobjektSkjema extends DriftKontroller
{
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $leieobjektId = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Leieobjekt $leieobjekt */
        $leieobjekt = $this->hentModell(Leieobjekt::class, (int)$leieobjektId);
        if (!$leieobjekt->hentId() || $leieobjektId == '*') {
            $leieobjekt = null;
        }
        $this->hoveddata['leieobjekt'] = $leieobjekt;
        $this->hoveddata['leieobjektId'] = $leieobjektId;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = 'Nytt leieobjekt';
        /** @var Leieobjekt $leieobjekt */
        $leieobjekt = $this->hoveddata['leieobjekt'];
        if (
            $leieobjekt
            && !$leieobjekt->hentId()
        ) {
            return false;
        }
        else if ($leieobjekt) {
            $this->tittel = $leieobjekt->hentBeskrivelse();
        }

        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Leieobjekt $leieobjekt */
        $leieobjekt = $this->hoveddata['leieobjekt'];

        return $this->vis(\Kyegil\Leiebasen\Visning\drift\html\leieobjekt_skjema\Index::class, [
            'leieobjekt' => $leieobjekt,
            'leieobjektId' => $this->hoveddata['leieobjektId'],
        ]);
    }
}