<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\MassemeldingSkjema;


use Exception;
use Kyegil\Leiebasen\MassemeldingMal;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\MassemeldingSkjema
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData($data = null) {
        switch ($data) {
            case "forhåndsvisning": {
                $resultat = (object)[
                    'success' => true
                ];

                try {
                    /** @var array $mottakerLeieforholdIder */
                    $mottakerLeieforholdIder = $this->data['post']['mottakere'] ?? [];
                    $medium = $this->data['post']['medium'] ?? "";
                    $html = !empty($this->data['post']['html']);
                    $emne = $medium == 'epost' && isset($this->data['post']['emne'])
                        ? $this->data['post']['emne']
                        : "";
                    $innhold = $this->data['post']['innhold'] ?? "";
                    $leieforhold = null;
                    if ($mottakerLeieforholdIder) {
                        $slumptall = rand(0, count($mottakerLeieforholdIder) - 1);
                        /** @var Leieforhold $leieforhold */
                        $leieforhold = $this->app->hentModell(Leieforhold::class, $mottakerLeieforholdIder[$slumptall]);
                        if (!$leieforhold->hentId()) {
                            $leieforhold = null;
                        }
                    }
                    if (!$leieforhold) {
                        /** @var Leieforhold|null $leieforhold */
                        $leieforhold = $this->app->hentSamling(Leieforhold::class)->hentTilfeldig();
                    }
                    if ($leieforhold) {
                        $masseEpostMal = new MassemeldingMal($leieforhold);
                        $emne = $masseEpostMal->fyllMal($emne);
                        $innhold = $masseEpostMal->fyllMal($innhold);
                    }

                    if($html) {
                        $resultat->emne = $emne;
                        $resultat->innhold = $innhold;
                    }
                    else {
                        $resultat->emne = htmlspecialchars($emne);
                        $resultat->innhold = '<textarea readonly>' . mb_substr($innhold, 0, 1071) . '</textarea>';
                    }
                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage() . '<br>' . $e->getFile() . ' : ' . $e->getLine();
                }
                break;
            }
            default: {
                $resultat = (object)[
                    'success' => false,
                    'msg' => 'Data ikke angitt'
                ];
            }
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}