<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\MassemeldingSkjema;


use Exception;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\MassemeldingMal;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Melding;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Opplasting;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\ViewRenderer\ViewArray;

class Mottak extends AbstraktMottak
{
    /**
     * @param string|null $skjema
     * @return $this
     */
    public function taIMot(string $skjema = null) {
        switch ($skjema) {
            default:
                $resultat = (object)[
                    'success' => true,
                    'msg' => '',
                    'id' => null
                ];

                try {
                    $medium = $this->data['post']['medium'] ?? Melding::MEDIUM_EPOST;
                    $mottakerLeieforholdIder = $this->data['post']['mottakere'] ?? [];
                    settype($mottakerLeieforholdIder, 'array');
                    $kopi = $this->data['post']['kopi'] ?? [];
                    settype($kopi['oppsummering'], 'boolean');
                    settype($kopi['eksempel'], 'string');
                    $svaradresse = $this->data['post']['svaradresse'] ?? '';
                    $smsAvsender = $this->data['post']['sms_avsender'] ?? '';
                    $emne = $this->data['post']['emne'] ?? "";
                    $innhold = $this->data['post']['innhold'] ?? "";
                    $send = isset($this->data['post']['send']) && $this->data['post']['send'];

                    /* Nye vedlegg blir sendt som FILES */
                    if ($medium == Melding::MEDIUM_EPOST) {
                        /** @var Opplasting[] $vedlegg */
                        $vedlegg = [];
                        $maksFilstørrelse = 1000000;

                        /* Tidligere opplastede vedlegg blir sendt som POST data */
                        if (isset($this->data['post']['vedlegg'])) {
                            foreach ($this->data['post']['vedlegg'] as $filNavn) {
                                if (file_exists($this->app->hentFilarkivBane() . '/midlertidig/' . $filNavn)) {
                                    $opplasting = new Opplasting();
                                    $opplasting->settOpplastetFilbane($this->app->hentFilarkivBane() . '/midlertidig/' . $filNavn);
                                    $maksFilstørrelse -= filesize($this->app->hentFilarkivBane() . '/midlertidig/' . $filNavn);
                                    $vedlegg[] = $opplasting;
                                }
                            }
                        }

                        if (isset($this->data['files']['vedlegg'])) {
                            foreach ($this->data['files']['vedlegg'] as $filData) {
                                $opplasting = new Opplasting($filData);
                                $opplasting->settLagringsmappe($this->app->hentFilarkivBane() . '/midlertidig');
                                $opplasting->settMaksFilstørrelse($maksFilstørrelse);
                                $opplasting->lagre();
                                $maksFilstørrelse -= $opplasting->hentFilStørrelse();
                                $vedlegg[] = $opplasting;
                            }
                        }
                        // Sjekk at alle vedlegg har blitt med
                        $antNyeVedlegg = $this->data['post']['nye_vedlegg_antall'] ?? null;
                        if (!isset($antNyeVedlegg)) {
                            throw new Exception('Det har oppstått et problem med opplasting av vedlegg.<br>Pass på at vedleggene ikke overskrider 1Mb.');
                        }

                        if ($antNyeVedlegg != count($this->data['files']['vedlegg'] ?? [])) {
                            $this->lagre($medium, $emne, $innhold, $svaradresse, $smsAvsender, $kopi, $mottakerLeieforholdIder, $vedlegg);
                            $resultat->url = "/drift/index.php?oppslag=massemelding_skjema";
                            throw new Exception('Det har blitt noe rot med vedleggene.<br>Sjekk at rett antall vedlegg er lastet opp.');
                        }
                    }

                    if ($send && $medium == Melding::MEDIUM_EPOST) {
                        $this->sendSomEpost($emne, $innhold, $svaradresse, $kopi, $mottakerLeieforholdIder, $vedlegg);
                        foreach ($vedlegg as $opplasting) {
                            $opplasting->slettOpplastetFil();
                        }
                        $resultat->msg = "Eposten er lagt i kø for utsending";
                        $resultat->url = (string)$this->app->returi->get();
                    }
                    else if ($send && $medium == Melding::MEDIUM_SMS) {
                        $this->sendSomSms($innhold, $smsAvsender, $mottakerLeieforholdIder);
                        $resultat->msg = "Meldingen er lagt i kø for utsending";
                        $resultat->url = (string)$this->app->returi->get();
                    } else {
                        $this->lagre($medium, $emne, $innhold, $svaradresse, $smsAvsender, $kopi, [], $vedlegg);
                        $resultat->url = "/drift/index.php?oppslag=leieforholdfilter";
                    }

                }
                catch (Feilmelding $e) {
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                catch (\Throwable $e) {
                    $this->app->loggException($e);
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
        }
    }

    /**
     * @param string $medium
     * @param string $emne
     * @param string $innhold
     * @param string $svaradresse
     * @param string $smsAvsender
     * @param array $kopi
     * @param array $mottakerLeieforholdIder
     * @param Opplasting[] $vedlegg
     * @return $this
     */
    private function lagre(
        string $medium,
        string $emne,
        string $innhold,
        string $svaradresse,
        string $smsAvsender,
        array  $kopi,
        array  $mottakerLeieforholdIder = [],
        array  $vedlegg = []
    ) {
        $_SESSION['massemelding']['medium'] = $medium;
        $_SESSION['massemelding']['emne'] = $emne;
        $_SESSION['massemelding']['innhold'] = $innhold;
        $_SESSION['massemelding']['svaradresse'] = $svaradresse;
        $_SESSION['massemelding']['sms_avsender'] = $smsAvsender;
        $_SESSION['massemelding']['kopi'] = $kopi;
        $_SESSION['massemelding']['mottakerLeieforholdIder'] = $mottakerLeieforholdIder;
        $_SESSION['massemelding']['vedlegg'] = [];
        foreach ($vedlegg as $opplasting) {
            $_SESSION['massemelding']['vedlegg'][] = $opplasting->hentOpplastetFilbane();
        }
        return $this;
    }

    /**
     * @param string $emne
     * @param string $innhold
     * @param string $svaradresse
     * @param array $kopi
     * @param array $mottakerLeieforholdIder
     * @param Opplasting[] $vedlegg
     * @return $this
     * @throws Exception
     */
    private function sendSomEpost(
        string $emne,
        string $innhold,
        string $svaradresse,
        array $kopi,
        array $mottakerLeieforholdIder = [],
        array $vedlegg = []
    ) {
        if(!$emne) {
            throw new Feilmelding("Eposten mangler emne");
        }
        if(!trim(strip_tags($innhold))) {
            throw new Feilmelding("Eposten mangler innhold");
        }
        if(!trim($svaradresse)) {
            throw new Feilmelding("Eposten mangler svaradresse");
        }
        if(!$mottakerLeieforholdIder) {
            throw new Feilmelding("Eposten mangler mottakere");
        }

        /** @var Leieforholdsett $leieforholdsett */
        $leieforholdsett = $this->app->hentSamling(Leieforhold::class)
            ->leggTilInnerJoin(Krav::hentTabell(), Krav::hentTabell() . '.leieforhold = ' . Leieforhold::hentTabell() . '.' . Leieforhold::hentPrimærnøkkelfelt())
            ->leggTilUttrykk('leieforhold_ekstra', 'utestående', 'IF(`krav`.`kravdato`<= NOW(), `krav`.`utestående`, 0)', 'sum')
            ->leggTilUttrykk('leieforhold_ekstra', 'forfalt', 'IF(`krav`.`forfall` IS NOT NULL AND `krav`.`forfall`<= "' . date('Y-m-d') . '", `krav`.`utestående`, 0)', 'sum')

            ->filtrerEtterIdNumre($mottakerLeieforholdIder)
            ->låsFiltre();

        $adgangsett = $leieforholdsett->inkluderAdganger();
        $adgangsett->leggTilPersonModell();
        $adgangsett->leggTilEavVerdiJoin(
            'brukerprofil_preferanser',
            false,
            Person::class,
            Adgang::hentTabell(),
            'personid'
        )->leggTilFelt('brukerprofil_preferanser', 'verdi', null, 'brukerprofil_preferanser', 'personer.eav_egenskaper')
        ;
        $adgangsett->leggTilEavFelt('umiddelbart_betalingsvarsel_epost');
        $bcc = ($kopi['eksempel'] == 'alle') ? $svaradresse : '';
        /** @var string[] $sendtTil */
        $sendtTil = [];
        /** @var string[] $vedleggsFiler */
        $vedleggsFiler = [];
        foreach ($vedlegg as $opplasting) {
            $vedleggsFiler[] = $opplasting->hentOpplastetFilbane();
        }

        foreach($leieforholdsett as $leieforhold) {
            $masseEpostMal = new MassemeldingMal($leieforhold);
            $epostAdresser = $leieforhold->hentEpost('informasjon');
            if($epostAdresser && $this->app->sendMail((object)[
                'auto' => false,
                'to' => implode(',', $epostAdresser),
                'bcc' => $bcc,
                'reply' => $svaradresse,
                'subject' => $masseEpostMal->fyllMal($emne),
                'html' => $masseEpostMal->fyllMal($innhold),
                'attachment' => implode(',', $vedleggsFiler),
                'type' => 'masseepost'
            ])) {
                $sendtTil[] = implode(',', $epostAdresser);
            }
            unset($masseEpostMal);
        }

        if($kopi['eksempel'] == 'tilfeldig') {
            /** @var Leieforhold|null $tilfeldigLeieforhold */
            $tilfeldigLeieforhold = $leieforholdsett->hentTilfeldig();
            $masseEpostMal = new MassemeldingMal($tilfeldigLeieforhold);
            $this->app->sendMail((object)[
                'to' => $svaradresse,
                'reply' => $svaradresse,
                'subject' => $masseEpostMal->fyllMal($emne),
                'html' => $masseEpostMal->fyllMal($innhold),
                'attachment' => implode(',', $vedleggsFiler),
                'type' => 'masseepost_tilfeldig_eksempel'
            ]);
            unset($masseEpostMal);
        }

        if($kopi['oppsummering']) {
            $sendtTil = array_map('htmlspecialchars', $sendtTil);
            $oppsummering = new ViewArray([
                new HtmlElement('p', [] , "Oppsummering av masseepost. Mottakerlisten er gjengitt nederst:"),
                new HtmlElement('p', [] , 'Mottakerliste:<br>' . implode('<br>', $sendtTil))
            ]);

            $this->app->sendMail((object)[
                'to' => $svaradresse,
                'reply' => $svaradresse,
                'subject' => "[Oppsummering]: " . $emne,
                'html' => $oppsummering,
                'type' => 'masseepost_oppsummering'
            ]);
        }

        unset($_SESSION['massemelding']);
        return$this;
    }

    /**
     * @param string $innhold
     * @param array $mottakerLeieforholdIder
     * @return $this
     * @throws Exception
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    private function sendSomSms(
        string $innhold,
        string $smsAvsender,
        array $mottakerLeieforholdIder = []
    ): Mottak
    {
        if(!$innhold) {
            throw new Feilmelding("Meldingen mangler innhold");
        }
        if(!$smsAvsender) {
            throw new Feilmelding("Meldingen mangler avsender");
        }
        if(!$mottakerLeieforholdIder) {
            throw new Feilmelding("Meldingen mangler mottakere");
        }

        /** @var Leieforholdsett $leieforholdsett */
        $leieforholdsett = $this->app->hentSamling(Leieforhold::class)
            ->leggTilInnerJoin(Krav::hentTabell(), Krav::hentTabell() . '.leieforhold = ' . Leieforhold::hentTabell() . '.' . Leieforhold::hentPrimærnøkkelfelt())
            ->leggTilUttrykk('leieforhold_ekstra', 'utestående', 'IF(`krav`.`kravdato`<= NOW(), `krav`.`utestående`, 0)', 'sum')
            ->leggTilUttrykk('leieforhold_ekstra', 'forfalt', 'IF(`krav`.`forfall` IS NOT NULL AND `krav`.`forfall`<= "' . date('Y-m-d') . '", `krav`.`utestående`, 0)', 'sum')

            ->filtrerEtterIdNumre($mottakerLeieforholdIder)
            ->låsFiltre();

        $adgangsett = $leieforholdsett->inkluderAdganger();
        $adgangsett->leggTilPersonModell();
        $adgangsett->leggTilEavVerdiJoin(
            'brukerprofil_preferanser',
            false,
            Person::class,
            Adgang::hentTabell(),
            'personid'
        )->leggTilFelt('brukerprofil_preferanser', 'verdi', null, 'brukerprofil_preferanser', 'personer.eav_egenskaper')
        ;

        foreach($leieforholdsett as $leieforhold) {
            $smsMal = new MassemeldingMal($leieforhold);
            $smsNumre = $leieforhold->hentSmsNumre('informasjon');
            foreach ($smsNumre as $sms) {
                $this->app->hentSmsProsessor()->forberedMelding($sms, $smsAvsender, $smsMal->fyllMal($innhold));
            }
            unset($smsMal);
        }

        unset($_SESSION['massemelding']);
        return$this;
    }
}