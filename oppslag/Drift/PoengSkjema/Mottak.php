<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/02/2023
 * Time: 15:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\PoengSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Poengprogram\Poeng;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Lagret."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'poeng':
                    $id = $get['id'] ?? null;
                    $programId = $post['program'] ?? null;
                    $leieforholdId = $post['leieforhold'] ?? null;
                    $tid = $post['tid'] ? new \DateTimeImmutable($post['tid']) : null;
                    $type = $post['type'] ?? null;
                    $tekst = $post['tekst'] ?? '';
                    $verdi = $post['verdi'] ?? null;

                    /** @var Leieforhold $leieforhold */
                    $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);
                    $program = $this->app->hentPoengbestyrer()->hentProgram($programId);
                    if(!$program) {
                        throw new Exception('Ugyldig poengprogram ' . $programId);
                    }
                    if(!$verdi) {
                        throw new Exception('Verdi er påkrevd');
                    }
                    if(!$leieforhold->hentId()) {
                        throw new Exception('Ugyldig leieforhold ' . $leieforholdId);
                    }

                    $levetid = $program->hentLevetid();
                    $foreldes = $levetid ? $tid->add($levetid) : null;

                    if($id == '*') {
                        /** @var Poeng $poeng */
                        $poeng = $this->app->nyModell(Poeng::class, (object)[
                            'leieforhold' => $leieforhold,
                            'program' => $program,
                            'registrerer' => $this->app->bruker['navn']
                        ]);
                    }
                    else {
                        /** @var Poeng $poeng */
                        $poeng = $this->app->hentModell(Poeng::class, $id);
                        if (!$poeng->hentId()) {
                            throw new Exception('Poeng ' . $id . ' finnes ikke');
                        }
                    }
                    $poeng
                        ->settTid($tid)
                        ->settForeldes($foreldes)
                        ->settType($type)
                        ->settTekst($tekst)
                        ->settVerdi($verdi)
                    ;
                    $resultat->id = $poeng->hentId();
                    $resultat->msg = 'Endringene er lagret';
                    $resultat->url = (string)$this->app->returi->get(1);
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}