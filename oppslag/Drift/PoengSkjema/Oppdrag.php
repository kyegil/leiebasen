<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 04/12/2020
 * Time: 23:21
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\PoengSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Poengprogram\Poeng;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return Oppdrag
     */
    protected function slettPoeng():Oppdrag {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            $poengId = (int)$this->data['post']['poeng_id'];
            /** @var Poeng $poeng */
            $poeng = $this->app->hentModell(Poeng::class, $poengId);
            if (!$poeng->hentId()) {
                throw new Exception('Finner ikke denne oppføringen');
            }
            $resultat->msg = 'Oppføringen har blitt slettet';
            $poeng->slett();
            $resultat->url = (string)$this->app->returi->get(1);
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}