<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\leieforholdfilter\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * Leieforholdfilter Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class Leieforholdfilter extends DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Leieforhold-filter';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $brukerId = $this->bruker['id'];
        $bruker = $this->hentModell(Person::class, $brukerId);
        $this->hoveddata['bruker'] = $bruker;
        return true;
    }

    /**
     * @return Index
     * @throws Exception
     */
    public function skript() {
        $bruker = $this->hoveddata['bruker'];
        if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }

        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'bruker' => $bruker
            ])
        ]);
        return $skript;
    }
}