<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\LeiereguleringListe;


use Kyegil\Leiebasen\Leieregulerer;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\Drift\LeiereguleringListe;
use Kyegil\Leiebasen\Respons\JsonRespons;

/**
 * @property LeiereguleringListe $app
 */
class Oppdrag extends AbstraktOppdrag
{
    public function justerLeie()
    {
        $this->app->before($this, __FUNCTION__, []);
        $leieregulerer = Leieregulerer::getInstance($this->app);
        $get = $this->data['get'];
        $post = $this->data['post'];

        $resultat = (object)[
            'success' => true,
            'url' => '/drift/index.php?oppslag=leieregulering_liste',
            'error' => null,
            'msg' => 'Justeringen(e) er gjennomført',
        ];

        try {
            $leieforholdIder = (array)($post['leieforhold'] ?? []);
            $leieforholdSett = $this->app->hentUtlevering()->hentLeieforholdForslagSett();
            $leieforholdSett->filtrerEtterIdNumre($leieforholdIder)->låsFiltre();
            /** Repositoriet tømmes fordi vi forventer tilleggsdata i denne samlingen */
            $this->app->modellOppbevaring->tøm();
            foreach ($leieforholdSett as $leieforhold) {
                $leieregulerer->justerLeieForLeieforhold($leieforhold);
            }
        }
        catch (\Exception $e) {
            $resultat->success = false;
            $resultat->error = $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}