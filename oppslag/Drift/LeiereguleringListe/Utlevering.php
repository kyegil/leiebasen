<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\LeiereguleringListe;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Leieregulerer;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\LeiereguleringListe
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     * @throws Exception
     * @link https://datatables.net/manual/server-side
     */
    public function hentData($data = null): Utlevering
    {
        $get = $this->data['get'];
        $post = $this->data['post'];

        switch ($data) {
            case 'reguleringsforslag': {
                $resultat = (object)[
                    'success' => true,
                    'draw' => intval($get['draw'] ?? 0),
                    'data'		=> [],
                    'totalRows' => 0,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                ];

                $sorteringsmapping = [
                    'id' => [null, 'leieforholdnr'],
                    'navn' => [null, 'leieforholdnr'],
                    'justeringsdato' => ['leieforhold', 'neste_dato'],
                    'eksisterende_leie' => [null, 'leiebeløp'],
                ];

                $sorteringsfelt	= $get['sort'] ?? null;
                $synkende = isset($get['dir']) && $get['dir'] == "DESC";
                if ($sorteringsfelt) {
                    $sortering = [[
                        'columnName' => $sorteringsfelt,
                        'dir' => $synkende ? 'desc' : 'asc'
                    ]];
                }
                else {
                    $sortering  = $get['order'] ?? [];
                }

                $start		= intval($get['start'] ?? 0);
                $limit		= $get['limit'] ?? 25;

                $søkefelt = $get['søkefelt'] ?? ($get['search']['value'] ?? '');

                try {
                    $leieforholdSett = $this->hentLeieforholdForslagSett(
                        $søkefelt,
                        $sortering,
                        $start,
                        $limit
                    );
                    /** Repositoriet tømmes fordi vi forventer tilleggsdata i denne samlingen */
                    $this->app->modellOppbevaring->tøm();
                    AbstraktUtlevering::sorter($leieforholdSett, $sortering, $sorteringsmapping);

                    $resultat->recordsFiltered = $resultat->recordsTotal = $resultat->totalRows
                        = $leieforholdSett->hentAntall();

                    foreach ($leieforholdSett as $leieforhold) {
                        $resultat->data[] = $this->hentReguleringsdata($leieforhold);
                    }
                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->error = $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }
            case 'varselutskrift':
                try {
                    $leieregulerer = Leieregulerer::getInstance($this->app);
                    $leieforholdIder = $get['id'] ?? [];
                    $leieforholdSett = $this->hentLeieforholdForslagSett();
                    $leieforholdSett->filtrerEtterIdNumre($leieforholdIder)->låsFiltre();
                    /** @var string $mal */
                    $mal = $this->app->hentValg('leiejustering_brevmal') ?? '';
                    $innhold = new ViewArray();
                    /** Repositoriet tømmes fordi vi forventer tilleggsdata i denne samlingen */
                    $this->app->modellOppbevaring->tøm();
                    foreach($leieforholdSett as $leieforhold) {
                        $varsel = new HtmlElement('div', ['style' => 'page-break-after: always;'],
                            $leieregulerer->fyllJusteringsVarselForLeieforhold($leieforhold, $mal)
                        );
                        $innhold->addItem($varsel);
                    }

                    $resultat = $this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\shared\Utskrift::class, [
                        'tittel' => 'Varsel om leieregulering',
                        'innhold' => $innhold
                    ]);
                }
                catch (Exception $e) {
                    $resultat = $e;
                }
                $this->settRespons(new Respons($resultat));
                return $this;
            default:
                $resultat = (object)[
                    'success' => false,
                    'msg' => 'Ønsket data er ikke oppgitt',
                    'data' => []
                ];
                $this->settRespons(new JsonRespons($resultat));
                return $this;
        }
    }

    /**
     * @param string $søk
     * @param array $sortering
     * @param int $start
     * @param int $limit
     * @return Leieforholdsett
     * @throws Exception
     */
    public function hentLeieforholdForslagSett(string $søk = '', array $sortering = [], ?int $start = null, ?int $limit = null): Leieforholdsett
    {
        list('søk' => $søk, 'sortering' => $sortering, 'start' => $start, 'limit' => $limit )
            = $this->app->before($this, __FUNCTION__, [
            'søk' => $søk, 'sortering' => $sortering, 'start' => $start, 'limit' => $limit
        ]);
        $leieregulerer = Leieregulerer::getInstance($this->app);

        $leieforholdsett = $leieregulerer->hentForeståendeLeieforholdEmner();
        $leieforholdsett->inkluderAdganger();

        if (isset($start)) {
            $leieforholdsett->settStart($start);
        }
        if (isset($limit)) {
            $leieforholdsett->begrens($limit);
        }
        return $this->app->after($this, __FUNCTION__, $leieforholdsett);
    }

    /**
     * @param Leieforhold $leieforhold
     * @return object
     * @throws Exception
     */
    private function hentReguleringsdata(Leieforhold $leieforhold): object
    {
        list('leieforhold' => $leieforhold)
            = $this->app->before($this, __FUNCTION__, [
            'leieforhold' => $leieforhold
        ]);
        $leieregulerer = Leieregulerer::getInstance($this->app);
        $varselfrist = $leieregulerer->varselfrist;
        $tidligsteForslagsdato = date_create_immutable()->add($varselfrist);
        $sistEndret = $leieforhold->hent('leie_oppdatert') ?? $leieforhold->hentFradato();
        $justeringsdato  = $leieforhold->getRawData()->leieforhold->neste_dato ?? null;
        $justeringsdato = $justeringsdato ? date_create_immutable($justeringsdato) : $tidligsteForslagsdato;
        $nyLeie = $leieregulerer->foreslåJustertTerminLeie($leieforhold, $justeringsdato);
        $data = (object)[
            'id' => $leieforhold->hentId(),
            'navn' => $leieforhold->hentId() . ' – ' . $leieforhold->hentNavn(),
            'justeringsdato' => $justeringsdato->format('Y-m-d'),
            'justeringsdato_formatert' => $justeringsdato->format('d.m.Y'),
            'sist_endret' => $sistEndret ? $sistEndret->format('Y-m-d') : null,
            'sist_endret_formatert' => $sistEndret ? $sistEndret->format('d.m.Y') : null,
            'eksisterende_leie' => $leieforhold->hentLeiebeløp(),
            'eksisterende_leie_formatert' => $this->app->kr($leieforhold->hentLeiebeløp()),
            'ny_leie' => $nyLeie,
            'ny_leie_formatert' => $this->app->kr($nyLeie),
            'varsling' => $this->hentVarsling($leieforhold),
            'klasser' => $this->hentVarsling($leieforhold),
            'utløper_dato' => $leieforhold->tildato ? $leieforhold->tildato->format('Y-m-d') : null,
            'utløper_dato_formatert' => $leieforhold->tildato ? $leieforhold->tildato->format('d.m.Y') : null,
            'utløpt' => $leieforhold->tildato && $leieforhold->tildato <= $justeringsdato,
        ];
        return $this->app->after($this, __FUNCTION__, $data);
    }

    /**
     * @param Leieforhold $leieforhold
     * @return string
     * @throws Exception
     */
    private function hentVarsling(Leieforhold $leieforhold): string
    {
        list('leieforhold' => $leieforhold)
            = $this->app->before($this, __FUNCTION__, [
            'leieforhold' => $leieforhold
        ]);
        $varsling = $leieforhold->hentEpost() ? Leieregulerer::VARSELTYPE_EPOST : Leieregulerer::VARSELTYPE_BREV;
        return $this->app->after($this, __FUNCTION__, $varsling);
    }
}