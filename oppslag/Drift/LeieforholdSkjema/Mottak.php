<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\LeieforholdSkjema;


use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Kyegil\CoreModel\CoreModelException;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\EventManager;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Egenskapsett;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Personsett;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Vedlikehold;
use stdClass;
use Throwable;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema):Mottak
    {
        /** @var stdClass $resultat */
        $resultat = (object)[
            'success' => true,
            'msg' => 'Opplysningene er lagret.'
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'leieforhold_skjema':
                    $leieforholdId = $get['id'] ?? null;
                    $leieforhold = null;
                    if ($leieforholdId !== '*') {
                        $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);
                        if(! $leieforhold->hentId() || $leieforhold->hentId() != $post['id']) {
                            throw new Feilmelding('Oppgitte opplysninger stemmer ikke over ens med oppgitt leieforhold.');
                        }
                        $resultat->id = $leieforhold->hentId();
                    }
                    $this->app->logger->info('LEIEFORHOLD – Leieforholdskjema behandles', $post);

                    /** @var Leieobjekt $leieobjekt */
                    $leieobjekt = $this->app->hentModell(Leieobjekt::class, $post['leieobjekt'] ?? null);
                    /** @var DateTimeImmutable|null $fradato */
                    $fradato = !empty($post['fradato']) ? new DateTimeImmutable($post['fradato']) : null;
                    $fradato = $leieforhold ? $leieforhold->fradato : $fradato;
                    $fornyelse = !empty($post['fornyelse']) && !empty($post['virkedato']);
                    /** @var DateTimeImmutable|null $virkedato */
                    $virkedato = $fornyelse ? new DateTimeImmutable($post['virkedato']) :  null;
                    /** @var DateTimeImmutable|null $tildato */
                    $tildato = !empty($post['tildato']) ? new DateTimeImmutable($post['tildato']) : null;
                    /** @var int $nevner */
                    $nevner = $post['andel']['nevner'] ?? 1;
                    /** @var int $teller */
                    $teller = $post['andel']['teller'] ?? $nevner;
                    /** @var Fraction $andel */
                    $andel = new Fraction($teller, $nevner);

                    /** @var DateInterval $oppsigelsestid */
                    $oppsigelsestid = new DateInterval($post['oppsigelsestid'] ?? 'P3M');
                    /** @var float $årsleieInklDelbeløp */
                    $årsleieInklDelbeløp = floatval(str_replace(',', '.', $post['leiebeløp'] ?? '0'));
                    /** @var string|null $fastDato */
                    $fastDato = !empty($post['fast_dato']) ? substr($post['fast_dato'], -5) : null;
                    /** @var string|null $dagIMåneden */
                    $dagIMåneden = !empty($post['dag_i_måneden']) ? $post['dag_i_måneden'] : null;
                    /** @var int|null $ukedag */
                    $ukedag = intval($post['ukedag'] ?? 0) ?: null;
                    /** @var int $antallTerminer */
                    $antallTerminer = intval($post['ant_terminer'] ?? 1) ?: 1;
                    $terminBetalingsfrist = !empty($post['termin_betalingsfrist'])
                        ? new DateInterval($post['termin_betalingsfrist'])
                        : null;

                    /** @var array<int, float> $delkravSatser */
                    $delkravSatser = [];
                    /** @var bool $periodiskStrømavstemming */
                    $periodiskStrømavstemming = false;
                    foreach($post['delkrav'] ?? [] as $delkrav) {
                        $delkravSatser[$delkrav['id']] = str_replace(',', '.', $delkrav['sats'] ?? []);
                        $periodiskStrømavstemming = $periodiskStrømavstemming
                            || (!empty($delkrav['periodisk_avstemming']) && $delkrav['id'] === $this->app->hentValg('delkravtype_fellesstrøm'));
                    }

                    /** @var object{id:int, er_org:int, fornavn:string, etternavn:string, husstandmedlem:int}[] $kontraktpersoner */
                    $resultat->leietakere = json_decode(json_encode($post['leietakere']));
                    /** @var object{id:int, er_org:int, fornavn:string, etternavn:string, husstandmedlem:int}[] $kontraktpersoner */
                    $kontraktpersoner = [];
                    /** @var object{id:int, er_org:int, fornavn:string, etternavn:string, husstandmedlem:int}[] $kontraktpersoner */
                    $andreBeboere = [];

                    foreach ($resultat->leietakere as $leietaker) {
                        if(empty($leietaker->husstandmedlem)) {
                            $kontraktpersoner[] = $leietaker;
                        }
                        else {
                            $andreBeboere[] = $leietaker;
                        }
                    }

                    /**
                     * Sjekk at leieforholdet eksisterer dersom det endres
                     */
                    if ($leieforhold && !$leieforhold->hentId()) {
                        throw new Feilmelding('Leieforhold ' . $leieforholdId . ' finnes ikke');
                    }

                    /**
                     * Sjekk at leieobjektet eksisterer
                     */
                    if (!$leieobjekt->hentId()) {
                        throw new Feilmelding('Leieobjektet eksisterer ikke');
                    }

                    /**
                     * Validér datoene
                     */
                    $this->validerDatoer($fradato, $tildato, $leieforhold, $fornyelse, $virkedato);

                    /** Sjekk at det er plass i leieobjektet */
                    $this->validerAndel($leieobjekt, $andel, $fornyelse, $leieforhold, $fradato, $virkedato);

                    /**
                     * Validér antall terminer
                     */
                    if(!in_array($antallTerminer, [1,2,3,4,6,12,13,26,52])) {
                        throw new Feilmelding('Leiebasen er ikke i stand til å prosessere ' . $antallTerminer . ' årlige leieterminer');
                    }

                    /** @var Personsett $leietakerSett */
                    $leietakerSett = $this->hentPersonsett($kontraktpersoner);
                    if(!$leietakerSett->hentAntall()) {
                        throw new Feilmelding('Leieforholdet må ha minst én avtalefestet leietaker');
                    }


                    /** @var Personsett $andreBeboereSett */
                    $andreBeboereSett = $this->hentPersonsett($andreBeboere);

                    if($leieforhold) {
                        $resultat->id = $leieforhold->hentId();
                        if ($leieobjekt->hentId() != $leieforhold->leieobjekt->hentId()) {
                            throw new Feilmelding('Leieobjektet kan ikke endres uten å inngå et nytt leieforhold.');
                        }
                        $kontraktpersonerErEndret = $this->kontraktpersonerErEndret($leieforhold, $leietakerSett);

                        $this->oppdaterLeieforhold(
                            $resultat,
                            $leieforhold,
                            $leietakerSett,
                            $antallTerminer,
                            $virkedato,
                            $andel,
                            $tildato,
                            $oppsigelsestid,
                            $årsleieInklDelbeløp,
                            $fastDato,
                            $dagIMåneden,
                            $ukedag,
                            $terminBetalingsfrist,
                            $andreBeboereSett->hentAntall() ? $andreBeboereSett : null,
                            $delkravSatser,
                            $periodiskStrømavstemming
                        );

                        $this->settEavVerdier($leieforhold);

                        $this->app->logger->info('LEIEFORHOLD – Avtaleoppdatering fullført', ['leieforhold' => $leieforhold->hentId(), 'resultat' => $resultat]);

                        $this->app->returi->reset();
                        $this->app->returi->set($this->app->url('leieforholdkort', $leieforhold->hentId()));
                        if($fornyelse) {
                            $this->app->returi->set($this->app->url('leieforhold_brukerprofiler_skjema', $leieforhold));
                            $this->app->returi->set($this->app->url('kontrakt_opplasting_skjema', $leieforhold->kontrakt->hentId()));
                            $this->app->returi->set($this->app->url('kontrakt_tekst_skjema', $leieforhold->kontrakt->hentId()));
                            $this->app->returi->set($this->app->url('leieforhold_regningsadresse', $leieforhold->hentId()));
                            $this->app->returi->set($this->app->url('leieforhold_personer_skjema', $leieforhold->hentId()));
                        }
                        else if($kontraktpersonerErEndret) {
                            $this->app->returi->set($this->app->url('leieforhold_brukerprofiler_skjema', $leieforhold));
                            $this->app->returi->set($this->app->url('leieforhold_regningsadresse', $leieforhold->hentId()));
                            $this->app->returi->set($this->app->url('leieforhold_personer_skjema', $leieforhold->hentId()));
                        }
                    }
                    else {
                        $leieforhold = $this->app->opprettLeieforhold(
                            $leieobjekt,
                            $leietakerSett,
                            $antallTerminer,
                            $fradato,
                            $andel,
                            $tildato,
                            $oppsigelsestid,
                            $årsleieInklDelbeløp,
                            '',
                            $fastDato,
                            $dagIMåneden,
                            $ukedag,
                            $terminBetalingsfrist,
                            $andreBeboereSett,
                            $delkravSatser,
                            $periodiskStrømavstemming
                        );
                        $resultat->id = $leieforhold->hentId();
                        $this->settEavVerdier($leieforhold);

                        // Så opprettes husleiekrav for denne leieavtalen.
                        $leieforhold->opprettLeiekrav($fradato);

                        // Angi videre adressesti (må oppgis i omvendt rekkefølge)
                        $this->app->returi->reset();
                        $this->app->returi->set($this->app->url('leieforholdkort', $leieforhold));
                        $this->app->returi->set($this->app->url('leieforhold_brukerprofiler_skjema', $leieforhold));
                        $this->app->returi->set($this->app->url('kontrakt_opplasting_skjema', $leieforhold->kontrakt));
                        $this->app->returi->set($this->app->url('kontrakt_tekst_skjema', $leieforhold->kontrakt));
                        $this->app->returi->set($this->app->url('leieforhold_regningsadresse', $leieforhold));
                        $this->app->returi->set($this->app->url('leieforhold_personer_skjema', $leieforhold));

                        EventManager::getInstance()->triggerEvent(
                            Leieforhold::class, 'endret',
                            $leieforhold, ['ny' => true], $this->app
                        );

                    }

                    $resultat->id = $leieforhold->hentId();
                    $resultat->url = (string)$this->app->returi->get();
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Feilmelding $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        catch (Throwable $e) {
            $this->app->loggException($e);
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            $resultat->url = isset($resultat->id)
                ? $this->app->url('leieforhold_skjema', $resultat->id)
                : $this->app->url('leieforhold_liste');
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param object{id:int, er_org:int, fornavn:string, etternavn:string, husstandmedlem:int}[] $leietakere
     * @return Personsett
     * @throws Exception
     */
    private function hentPersonsett(array $leietakere): Personsett
    {
        $personer = [];
        foreach ($leietakere as $leietaker) {
            $personId = $leietaker->id ?? null;
            if(intval($personId)) {
                $personer[] = $this->app->hentModell(Person::class, $personId);
            }
            else {
                /** @var Person $person */
                $person = $this->app->nyModell(Person::class, (object)[
                    'er_org' => !empty($leietaker->er_org),
                    'fornavn' => $leietaker->fornavn ?? null,
                    'etternavn' => $leietaker->etternavn ?? null,
                ]);
                EventManager::getInstance()->triggerEvent(
                    Person::class, 'endret',
                    $person, ['ny' => true], $this->app
                );
                $personer[] = $person;
                $leietaker->id = $person->hentId();
            }
        }
        return $this->app->hentSamling(Person::class)->lastFra($personer);
    }

    /**
     * @param Leieforhold $leieforhold
     * @return $this
     * @throws CoreModelException
     * @throws Exception
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    private function settEavVerdier(Leieforhold $leieforhold): Mottak
    {
        /** @var Egenskapsett $eavEgenskaper */
        $eavEgenskaper = $this->app->hentSamling(Egenskap::class);
        $eavEgenskaper->leggTilEavVerdiJoin('felt_type', true);
        $eavEgenskaper->leggTilSortering('rekkefølge');
        $eavEgenskaper->leggTilFilter(['modell' => Leieforhold::class])->låsFiltre();

        $eavEgenskaper->forEach(function(Egenskap $egenskap) use($leieforhold) {
            $kode = $egenskap->kode;
            if(array_key_exists($kode, $this->data['post'])) {
                $leieforhold->sett($kode, $this->data['post'][$kode]);
            }
        });

        return $this;
    }

    /**
     * @param DateTimeInterface|null $fradato
     * @param DateTimeInterface|null $tildato
     * @param Leieforhold|null $leieforhold
     * @param bool|null $fornyelse
     * @param DateTimeImmutable|null $virkedato
     * @return $this
     * @throws Feilmelding
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    private function validerDatoer(?DateTimeInterface $fradato, ?DateTimeInterface $tildato, ?Leieforhold $leieforhold, ?bool $fornyelse, ?DateTimeImmutable $virkedato): Mottak
    {
        $fornyelse = $leieforhold && $fornyelse;
        $virkedato = $fornyelse ? $virkedato : null;
        if($leieforhold) {
            $fradato = $leieforhold->fradato;
            if($virkedato && $virkedato->format('Ymd') < $leieforhold->hentKontrakt()->hentDato()->format('Ymd')) {
                throw new Feilmelding('Kontrakten kan ikke dateres før forrige kontrakt');
            }
        }
        if($fornyelse && !$virkedato) {
            throw new Feilmelding('Dato for kontraktfornyingen er påkrevd');
        }
        if(!$fradato) {
            throw new Feilmelding('Fra-dato er påkrevd');
        }
        if($tildato && $tildato->format('Ymd') < $fradato->format('Ymd')) {
            throw new Feilmelding('Til-dato kan ikke være før fra-dato');
        }
        if($tildato && $virkedato && $tildato->format('Ymd') < $virkedato->format('Ymd')) {
            throw new Feilmelding('Til-dato kan ikke være før kontraktens ikrafttreden');
        }
        return $this;
    }

    /**
     * @param object $resultat
     * @param Leieforhold $leieforhold
     * @param Personsett $leietakere
     * @param int $antallTerminer
     * @param DateTimeInterface|null $kontraktDato
     * @param Fraction $andel
     * @param DateTimeInterface|null $tildato
     * @param DateInterval $oppsigelsestid
     * @param float $årsleieInklDelbeløp
     * @param string|null $fastDato
     * @param string|null $dagIMåneden
     * @param int|null $ukedag
     * @param DateInterval|null $terminBetalingsfrist
     * @param Personsett|null $andreBeboere
     * @param array|null $delkravSatser
     * @param bool $periodiskStrømavstemming
     * @return Mottak
     * @throws CoreModelException
     * @throws Throwable
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    private function oppdaterLeieforhold(
        object             $resultat,
        Leieforhold        $leieforhold,
        Personsett         $leietakere,
        int                $antallTerminer,
        ?DateTimeInterface $kontraktDato,
        Fraction           $andel,
        ?DateTimeInterface $tildato,
        DateInterval       $oppsigelsestid,
        float              $årsleieInklDelbeløp,
        ?string            $fastDato,
        ?string            $dagIMåneden,
        ?int               $ukedag,
        ?DateInterval      $terminBetalingsfrist,
        ?Personsett        $andreBeboere = null,
        ?array             $delkravSatser = null,
        bool               $periodiskStrømavstemming = false
    ): Mottak
    {
        $this->app->logger->info('LEIEFORHOLD – ' . ($kontraktDato ? 'Fornyer' : 'Oppdaterer') . ' avtale', ['leieforhold' => $leieforhold->hentId(), 'post' => $this->data['post']]);
        if ($leieforhold->hentOppsigelse()) {
            throw new Exception('Dette leieforholdet kan ikke endres fordi det er oppsagt.');
        }
        $basisleie = $this->app->beregnBasisLeie($årsleieInklDelbeløp, $delkravSatser);

        $leieoppdatering = $leieforhold->årligBasisleie != $basisleie;
        $genererNyeKrav = $leieoppdatering
            || $leieforhold->hentAndel($kontraktDato)->compare('!=', $andel)
            || $leieforhold->forfallFastDato != $fastDato
            || $leieforhold->forfallFastDagIMåneden != $dagIMåneden
            || $leieforhold->forfallFastUkedag != $ukedag
            || $leieforhold->antTerminer != $antallTerminer
            || $leieforhold->tildato != $tildato
            || $leieforhold->leiebeløp != round($årsleieInklDelbeløp / $antallTerminer);

        $this->oppdaterDelkravSatser($delkravSatser, $leieforhold, $resultat, $periodiskStrømavstemming);

        if($leieforhold->hentAndel()->compare('!=', $andel)) {
            $this->app->logger->info('LEIEFORHOLD – Andelen endres' , ['leieforhold' => $leieforhold->hentId(), 'eksisterende' => $leieforhold->hentAndel()->asFraction(), 'ny' => $andel->asFraction()]);
            $leieforhold->settAndel($andel, $kontraktDato);
        }

        if($antallTerminer != $leieforhold->hentAntTerminer()) {
            $leieforhold->settAntTerminer($antallTerminer);
        }
        if($tildato != $leieforhold->hentTildato()) {
            $leieforhold->settTildato($tildato);
        }
        if($basisleie != $leieforhold->hentÅrligBasisleie()) {
            $leieforhold->settÅrligBasisleie($basisleie);
        }

        if($fastDato != $leieforhold->forfallFastDato) {
            $leieforhold->settForfallFastDato($fastDato);
        }
        if($dagIMåneden != $leieforhold->hentForfallFastDagIMåneden()) {
            $leieforhold->settForfallFastDagIMåneden($dagIMåneden);
        }
        if($ukedag != $leieforhold->hentForfallFastUkedag()) {
            $leieforhold->settForfallFastUkedag($ukedag);
        }

        $leieforhold->settOppsigelsestid($oppsigelsestid);
        $leieforhold->settTerminBetalingsfrist($terminBetalingsfrist);

        $leieforhold->settAndreBeboere($andreBeboere);
        $this->oppdaterKontraktOgLeietakere($resultat, $leieforhold, $leietakere, $kontraktDato, $tildato);

        if ($leieoppdatering) {
            $leieforhold->oppdaterLeieHistorikk(false, $kontraktDato);
        }

        if($genererNyeKrav) {
            // Forestående husleiekrav må gjenopprettes.
            $nyeKrav = $leieforhold->opprettLeiekrav($kontraktDato ?? new DateTimeImmutable(), true);
            $this->app->logger->info('LEIEFORHOLD – Nye leiekrav opprettet', ['leieforhold' => $leieforhold->hentId(), 'krav' => $nyeKrav->hentIdNumre()]);

            $slettebeskrivelser = $leieforhold->leieobjekt->slettOverflødigOppsigelsestid($nyeKrav);

            $resultat->msg .= '<br>Det er opprettet terminer med forfallsdatoer.';
            foreach ($slettebeskrivelser as $beskrivelse) {
                $resultat->msg .= '<br>' . $beskrivelse . ' har blitt forkasta.';
            }
        }

        if($kontraktDato) {
            Vedlikehold::getInstance($this->app)->sjekkLeieavtalerSomBørFornyes();
        }
        return $this;
    }

    /**
     * Oppdater kontrakt og leietakere
     *
     * Ved fornying av kontrakt bør oppdatering av leietakere foregå samtidig,
     * fordi den nye og eksisterende kontakten kan ha ulike leietakere
     *
     * @param object $resultat
     * @param Leieforhold $leieforhold
     * @param Personsett $leietakere
     * @param DateTimeInterface|null $kontraktDato
     * @param DateTimeInterface|null $tilDato
     * @return $this
     * @throws CoreModelException
     * @throws Exception
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     * @noinspection PhpUnusedParameterInspection
     */
    private function oppdaterKontraktOgLeietakere(
        object              $resultat,
        Leieforhold         $leieforhold,
        Personsett          $leietakere,
        ?DateTimeInterface  $kontraktDato = null,
        ?DateTimeInterface  $tilDato = null
    ): Mottak
    {
        $eksisterendePersonIder = [];
        $leieforhold->hentLeietakere()->leggTilPersonModell()
            ->forEach(function (Leieforhold\Leietaker $leietaker) use (&$eksisterendePersonIder) {
                if($leietaker->person) {
                    $eksisterendePersonIder[] = $leietaker->person->hentId();
                }
            });
        $nyePersonIder = $leietakere->hentIdNumre();
        $personIderUt = [];
        $personIderInn = [];
        if($this->kontraktpersonerErEndret($leieforhold, $leietakere)) {
            $this->app->logger->info('LEIEFORHOLD – Endring i leietakere', [
                'leieforhold' => $leieforhold->hentId(),
                'nyePersonIder' => $nyePersonIder,
                'eksisterendePersonIder' => $eksisterendePersonIder
            ]);

            $personIderUt = array_diff($eksisterendePersonIder, $nyePersonIder);
            $personIderInn = array_diff($nyePersonIder, $eksisterendePersonIder);
        }
        if($personIderUt) {
            foreach($leieforhold->hentLeietakere() as $leietaker) {
                $person = $leietaker->hentPerson();
                if($person && in_array($person->hentId(), $personIderUt)) {
                    $leieforhold->slettLeietaker($person, $kontraktDato);
                    $resultat->msg .= '<br>' . $person->hentNavn() . ' er strøket som leietaker.';
                }
            }
        }
        if ($kontraktDato) {
            $leieforhold->leggTilKontrakt($kontraktDato, $tilDato);
        }
        if($personIderInn) {
            foreach($leietakere as $person) {
                if($person && in_array($person->hentId(), $personIderInn)) {
                    $leieforhold->kontrakt->leggTilLeietaker($person);
                    $resultat->msg .= '<br>' . $person->hentNavn() . ' er lagt til som leietaker.';
                }
            }
        }
        if($personIderUt || $personIderInn) {
            $leieforhold->settNavn()->settBeskrivelse();
        }
        return $this;
    }

    /**
     * Validér andel
     *
     * @param Leieobjekt $leieobjekt
     * @param Fraction $andel
     * @param Leieforhold|null $leieforhold
     * @param DateTimeImmutable|null $fradato
     * @param DateTimeImmutable|null $virkeDato
     * @return $this
     * @throws Feilmelding
     * @throws Exception
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     */
    private function validerAndel(
        Leieobjekt $leieobjekt,
        Fraction $andel,
        bool $fornyelse,
        ?Leieforhold $leieforhold,
        ?DateTimeInterface $fradato,
        ?DateTimeInterface $virkeDato
    ): Mottak
    {
        if(!$leieforhold) {
            $virkeDato = $fradato;
        }
        elseif(!$fornyelse) {
            $virkeDato = new DateTimeImmutable();
        }

        $oppsigelse = $leieforhold ? $leieforhold->hentOppsigelse() : null;
        $tildato = $oppsigelse ? $oppsigelse->hentFristillelsesdato() : null;
        $tilgjengelig = $leieobjekt->hentLedighetForTidsrom($virkeDato, $tildato, $leieforhold);
        if($tilgjengelig->compare('<', $andel)) {
            throw new Feilmelding('Det er ikke tilstrekkelig plass i leieobjektet i angitt tidsrom');
        }
        return $this;
    }

    /**
     * @param array|null $delkravSatser
     * @param Leieforhold $leieforhold
     * @param object $resultat
     * @param bool $periodiskStrømavstemming
     * @return $this
     * @throws Exception
     * @noinspection PhpReturnValueOfMethodIsNeverUsedInspection
     * @noinspection PhpUnusedParameterInspection
     */
    private function oppdaterDelkravSatser(
        ?array      $delkravSatser,
        Leieforhold $leieforhold,
        object      $resultat,
        bool $periodiskStrømavstemming = false
    ): Mottak
    {
        if (isset($delkravSatser)) {
            $leieforhold->slettAlleDelkravForekomster();
            $this->app->logger->debug('LEIEFORHOLD – Delkravtyper i leieforholdet slettes for å oppdateres', ['leieforhold' => $leieforhold->hentId()]);
            /**
             * @var int $delkravTypeId
             * @var float $sats
             */
            foreach ($delkravSatser as $delkravTypeId => $sats) {
                /** @var Delkravtype $delkravtype */
                $delkravtype = $this->app->hentModell(Delkravtype::class, $delkravTypeId);
                if ($sats != 0) {
                    $leieforhold->leggTilDelkravtype(
                        $delkravtype,
                        $delkravtype->selvstendigTillegg,
                        $delkravtype->relativ,
                        $sats,
                        $delkravtype->id == $this->app->hentValg('delkravtype_fellesstrøm') && $periodiskStrømavstemming
                    );
                }
            }
            $this->app->logger->debug('LEIEFORHOLD – Delkravtyper i leieforholdet oppdatert', ['leieforhold' => $leieforhold->hentId()]);
        }
        return $this;
    }

    /**
     * @param Leieforhold $leieforhold
     * @param Personsett $kontraktpersoner
     * @return bool
     * @throws CoreModelException
     * @throws Exception
     */
    private function kontraktpersonerErEndret(Leieforhold $leieforhold, Personsett $kontraktpersoner): bool
    {
        $eksisterendePersonIder = [];
        $leieforhold->hentLeietakere()->leggTilPersonModell()
            ->forEach(function (Leieforhold\Leietaker $leietaker) use (&$eksisterendePersonIder) {
                if($leietaker->person) {
                    $eksisterendePersonIder[] = $leietaker->person->hentId();
                }
            });
        $nyePersonIder = $kontraktpersoner->hentIdNumre();
        return $eksisterendePersonIder != $nyePersonIder;
        }
}