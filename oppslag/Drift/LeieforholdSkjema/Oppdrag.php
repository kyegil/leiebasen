<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\LeieforholdSkjema;


use Kyegil\Leiebasen\EventManager;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Respons\JsonRespons;

/**
 * Class Oppdrag
 * @package Kyegil\Leiebasen\Oppslag\Drift\LeieforholdSkjema
 */
class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return Oppdrag
     * @noinspection PhpUnused
     */
    protected function strykLeietaker():Oppdrag {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            /** @var Leieforhold $leieforhold */
            $leieforhold = $this->app->hentModell(Leieforhold::class, $_GET['id'] ?? null);
            /** @var Person $person */
            $person        = $this->app->hentModell(Person::class, $_POST['person'] ?? 0);
            if ($leieforhold->hentLeietakere()->hentAntall() < 2
                && $leieforhold->hentLeietakere()->hentFørste()->hentId() == $person->hentId()
            ) {
                throw new Feilmelding("Kan ikke stryke den eneste personen i leieavtalen.");
            }

            $leieforhold
                ->slettLeietaker($person)
                ->slettAdgangFor($person)
            ;

            $andreBeboerIder = $leieforhold->hentAndreBeboere()
                ? $leieforhold->hentAndreBeboere()->hentIdNumre()
                : [];
            if(in_array($person->hentId(), $andreBeboerIder)) {
                $andreBeboere = null;
                $andreBeboerIder = array_diff($andreBeboerIder, [$person->hentId()]);
                if($andreBeboerIder) {
                    $andreBeboere = $this->app->hentSamling(Person::class)
                        ->filtrerEtterIdNumre($andreBeboerIder);
                }
                $leieforhold->settAndreBeboere($andreBeboere);
            }
            $leieforhold->settNavn()->settBeskrivelse();
            $leieforhold->kontrakt->settTekst($leieforhold->gjengiAvtaletekst());

            EventManager::getInstance()->triggerEvent(
                Leieforhold::class, 'endret',
                $leieforhold, ['ny' => false], $this->app->hentCoreModelImplementering()
            );
        }
        catch (Feilmelding $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        catch (\Throwable $e) {
            $this->app->loggException($e);
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @return Oppdrag
     * @noinspection PhpUnused
     */
    protected function frys():Oppdrag {
        $resultat = (object)[
            'success' => true,
            'msg' => 'Leieforholdet er frosset',
            'data' => []
        ];

        try {
            /** @var Leieforhold $leieforhold */
            $leieforhold = $this->app->hentModell(Leieforhold::class, $_GET['id'] ?? null);

            $leieforhold->settFrosset(true);
        }
        catch (Feilmelding $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        catch (\Throwable $e) {
            $this->app->loggException($e);
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @return Oppdrag
     * @noinspection PhpUnused
     */
    protected function tin():Oppdrag {
        $resultat = (object)[
            'success' => true,
            'msg' => 'Leieforholdet har blitt tint opp',
            'data' => []
        ];

        try {
            /** @var Leieforhold $leieforhold */
            $leieforhold = $this->app->hentModell(Leieforhold::class, $_GET['id'] ?? null);
            $leieforhold->settFrosset(false);
        }
        catch (Feilmelding $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        catch (\Throwable $e) {
            $this->app->loggException($e);
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

}