<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\LeieforholdSkjema;


use DateTimeImmutable;
use Exception;
use Kyegil\CoreModel\CoreModel;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Oppsigelse;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Leieobjektsett;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\LeieforholdSkjema
 */
class Utlevering extends AbstraktUtlevering
{
    /** @noinspection SpellCheckingInspection */
    private static array $forenklinger = [
        'hannes' => 'hans',
        'ceci' => 'sesi',
        'aug' => 'ao',
        'aa' => 'å',
        'ch' => 'k',
        'c' => 'k',
        'dth' => 't',
        'dt' => 't',
        'ph' => 'f',
        'ff' => 'f',
        'gj' => 'i',
        'gt' => 'kt',
        'j' => 'i',
        'kh' => 'k',
        'qu' => 'k',
        'q' => 'k',
        'kk' => 'k',
        'll' => 'l',
        'mm' => 'm',
        'hn' => 'n',
        'nn' => 'n',
        'u' => 'o',
        'oo' => 'o',
        'pp' => 'p',
        'rr' => 'r',
        'z' => 's',
        'x' => 's',
        'ss' => 's',
        'th' => 't',
        'tt' => 't',
        'w' => 'v',
        'vv' => 'v',
        'y' => 'i',
        'æ' => 'e',
    ];

    /**
     * @param string $uttrykk
     * @return string
     */
    public static function forenklet(string $uttrykk): string {
        $uttrykk = mb_strtolower($uttrykk);
        foreach (self::$forenklinger as $sekvens => $forenkling) {
            $uttrykk = str_replace($sekvens, $forenkling, $uttrykk);
        }
        return $uttrykk;
    }

    /**
     * @param string $uttrykk
     * @return string
     */
    public static function queryForenklet(string $uttrykk): string {
        $uttrykk = "LOWER($uttrykk)";
        foreach (self::$forenklinger as $sekvens => $forenkling) {
            $uttrykk = "REPLACE($uttrykk, '$sekvens', '$forenkling')";
        }
        return $uttrykk;
    }

    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null): Utlevering
    {
        $get = $this->data['get'];


        switch ($data) {
            case 'adressekort': {
                $resultat = (object)[
                    'success' => true,
                    'data'		=> []
                ];

                /** @var string|null $term */
                $term = isset($get['term']) ? trim($get['term']) : null;
                $term = str_replace('-', ' ', $term);
                $term = preg_replace('/[^\p{L}\p{N}\s]/u', '', $term);
                $søkeElementer = array_map('trim', explode(' ', $term));
                $søkeElementer = array_filter($søkeElementer);

                if($søkeElementer) {
                    $filtre = [];
                    $filtre[1] = ["CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`) LIKE" => trim($get['term'])];
                    $filtre[2] =['`personer`.`personnr`' =>
                        preg_replace("/[^0-9]/", "", $get['term'] )
                        ? [preg_replace("/[^0-9]/", "", $get['term'] ), substr(preg_replace("/[^0-9]/", "", $get['term'] ), -5)]
                        : []
                    ];

                    $filtre[3] = ['and' => []];
                    foreach($søkeElementer as $element) {
                        if(self::forenklet($element)) {
                            $filtre[3]['and'][] = [self::queryForenklet("CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`)") . " LIKE '%" . self::forenklet($element) . "%'"];
                        }
                    }

                    $filtre[4] = ['and' => ['or' => []]];
                    $filtre[4]['and'][] = [self::queryForenklet('`personer`.`etternavn`') . " LIKE '%" . self::forenklet(end($søkeElementer)) . "%'"];
                    foreach($søkeElementer as $element) {
                        if(self::forenklet($element)) {
                            $filtre[4]['and']['or'][] = [self::queryForenklet('`personer`.`fornavn`') . " LIKE '%" . self::forenklet($element) . "%'"];
                        }
                    }

                    /** @var Modell\Personsett $personer */
                    $personer = $this->app->hentSamling(Modell\Person::class);
                    foreach(array_reverse($filtre, true) as $prioritet => $filter) {
                        $personer->nullstill()->leggTilFilter($filter, true);
                        foreach ($personer as $person) {
                            $tekst = $person->hentId() . ' ' . $person->hentNavn();
                            if($person->hentFødselsdato()) {
                                $tekst .= ' (' . $person->hentFødselsdato()->format('d.m.Y') . ')';
                            }
                            else if($person->hentOrgNr()) {
                                $tekst .= ' (' . $person->hentOrgNr() . ')';
                            }
                            $resultat->data[$person->hentId()] = (object)[
                                'id' => strval($person),
                                'fornavn' => $person->hentFornavn(),
                                'etternavn' => $person->hentEtternavn(),
                                'text' => $tekst,
                                'prioritet' => $prioritet,
                            ];
                        }
                    }
                }

                usort($resultat->data, function ($a, $b) {
                    return ($a->prioritet - $b->prioritet) ?: ($a->id - $b->id);
                });

                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }

            case 'leieobjektliste': {
                /** @var string|null $term */
                $term = isset($get['term']) ? trim($get['term']) : null;

                $fra = new DateTimeImmutable(!empty($get['fradato']) ? $get['fradato'] : 'now');
                $til = !empty($get['tildato']) ? new DateTimeImmutable($get['tildato']) : $fra;
                $innhold = $get['innhold'] ?? [];
                $leieforholdId = $get['id'] ?? null;
                $leieobjektId = !empty($get['leieobjekt']) && intval($get['leieobjekt'])
                    ? intval($get['leieobjekt']): null;
                /** @var Leieforhold|null $leieforhold */
                $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);
                if(!$leieforhold->hentId()) {
                    $leieforhold = null;
                }

                $resultat = (object)[
                    'success' => true,
                    'data'		=> new stdClass(),
                    'fraDato' => $fra->format('Y-m-d'),
                    'tilDato' => $til->format('Y-m-d'),
                    'minDatoer' => in_array('minDatoer', $innhold) ? new stdClass() : null,
                    'bofellesskap' => in_array('bofellesskap', $innhold) ? new stdClass() : null,
                ];

                /** @var Leieobjektsett $leieobjektsett */
                $leieobjektsett = $this->app->hentAktiveLeieobjekter();
                $leieobjektsett->leggTilBygningModell();
                $leieobjektsett->forhåndslastErOverskueligOpptatt();
                /** @var Leieobjektsett $leieforholdsett */
                $leieobjektsett->inkluder('leieforhold');
                if($term) {
                    $leieobjektsett->leggTilFilter([
                        'or' => [
                            ['leieobjektnr' => (int)$term],
                            ['`' . Leieobjekt::hentTabell() . '`.`navn` LIKE' => '%' . $term . '%'],
                            ['`' . Leieobjekt::hentTabell() . '`.`gateadresse` LIKE' => '%' . $term . '%'],
                            ['`' . Leieobjekt::hentTabell() . '`.`beskrivelse` LIKE' => '%' . $term . '%'],
                            ['`' . Bygning::hentTabell() . '`.`navn` LIKE' => '%' . $term . '%'],
                        ]
                    ]);
                }
                if($leieobjektId) {
                    $leieobjektsett->leggTilFilter(['leieobjektnr' => $leieobjektId]);
                }
                $leieobjektsett->låsFiltre();
                $leieobjektsett->leggTilLeftJoin(Leieforhold::hentTabell(),
                    '`' . Leieobjekt::hentTabell() . '`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '` = `' . Leieforhold::hentTabell() . '`.`leieobjekt`');
                $leieobjektsett->leggTilLeftJoin(Oppsigelse::hentTabell(),
                    '`' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '` = `' . Oppsigelse::hentTabell() . '`.`' . Oppsigelse::hentPrimærnøkkelfelt() . '`');

                /** @var Leieobjekt $leieobjekt */
                foreach($leieobjektsett as $leieobjekt) {
                    /** @var Fraction $ledighetsgrad */
                    $ledighetsgrad = new Fraction();
                    /** @var Fraction $andelSomVilFristilles */
                    $andelSomVilFristilles = new Fraction();
                    $minDato = null;
                    $erOverskueligOpptatt = $leieobjekt->hentErOverskueligOpptatt();
                    $beskrivelse = "{$leieobjekt}: {$leieobjekt->beskrivelse}";

                    if($leieforhold || !$erOverskueligOpptatt) {
                        $ledighetsgrad = $leieobjekt->hentLedighetForTidsrom($fra, $til, $leieforhold);
                        $minDato = $leieforhold ? $leieforhold->hentFradato() : $leieobjekt->hentFørsteLedigeDato();
                        if($minDato > $fra) {
                            $andelSomVilFristilles = $leieobjekt->hentLedighet($minDato, $leieforhold);
                        }
                    }

                    if( $ledighetsgrad->compare('=', 0) ) {
                        // Leieobjektet er fullt utleid
                        $beskrivelse .= ' (Utleid';

                        if($andelSomVilFristilles->compare('>', 0)) {
                            $beskrivelse .= (
                                ($andelSomVilFristilles->compare('<', 1))
                                    ? ". {$andelSomVilFristilles->asFraction()} blir ledig fra "
                                    : '. Blir ledig fra '
                                ) . $minDato->format('d.m.Y');
                        }

                        $beskrivelse .= ')';
                    }

                    else {
                        // Leieobjektet er delvis utleid
                        $beskrivelse .= ($ledighetsgrad->compare('<', 1))
                            ? (' (' . $ledighetsgrad->asFraction() . ' er ledig).')
                            : '';
                    }

                    $resultat->data->{$leieobjekt->hentId()} = (object)[
//                        'selected' => false,
                        'disabled'              => !$leieforhold && $erOverskueligOpptatt,
                        'id'                    => $leieobjekt->hentId(),
                        'leieobjektnr'          => $leieobjekt->hentId(),
                        'text'                  => $beskrivelse,
                        'beskrivelse'           => $beskrivelse,
                        'andelSomVilFristilles' => [(int)$andelSomVilFristilles->getNumerator(), (int)$andelSomVilFristilles->getDenominator()],
                        'tilgjengelighet'       => [(int)$ledighetsgrad->getNumerator(), (int)$ledighetsgrad->getDenominator()],
                        'tilgjengelighetFormatert'  => $ledighetsgrad->asFraction()
                    ];

                    if(in_array('minDatoer', $innhold)) {
                        $resultat->minDatoer->{$leieobjekt->hentId()} = $minDato ? $minDato->format('Y-m-d') : null;
                    }

                    if(in_array('bofellesskap', $innhold)) {
                        $typiskLeieandel = $leieobjekt->hentTypiskLeieandel();
                        $typiskLeieandel->simplify();
                        $resultat->bofellesskap->{$leieobjekt->hentId()} = [(int)$typiskLeieandel->getNumerator(), (int)$typiskLeieandel->getDenominator()];
                    }
                }

                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }

            case 'leieforslag': {
                $resultat = (object)[
                    'success' => true,
                    'data'		=> null,
                    'leieobjekt' => null,
                    'andel' => null,
                ];

                try {
                    $leieobjektId = $get['leieobjekt'] ?? null;
                    $andel = new Fraction($get['andel'][0] ?? 1, $get['andel'][1] ?? 1);
                    /** @var Leieobjekt $leieobjekt */
                    $leieobjekt = $this->app->hentModell(Leieobjekt::class, $leieobjektId);
                    $leieobjekt->beregnBasisleie($andel);
                    $resultat->data = $leieobjekt->beregnBasisleie($andel);
                    $resultat->andel = [$andel->getNumerator(), $andel->getDenominator()];
                    $resultat->leieobjekt = $leieobjekt->hentId();
                }
                catch (Feilmelding $e) {
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                catch (\Throwable $e) {
                    $this->app->loggException($e);
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }

            default:
                $resultat = (object)[
                    'success' => false,
                    'msg' => 'Ønsket data er ikke oppgitt',
                    'data' => []
                ];
                $this->settRespons(new JsonRespons($resultat));
                return $this;
        }
    }

}