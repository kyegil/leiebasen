<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class ProfilPassordSkjema extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /**
     * Forbered hoveddata
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $brukerprofilId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['brukerprofil'] = $this->hentModell(Person\Brukerprofil::class, (int)$brukerprofilId);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Person\Brukerprofil $brukerprofil */
        $brukerprofil = $this->hoveddata['brukerprofil'];
        if(!$brukerprofil->hentId()) {
            return false;
        }
        $this->tittel = "Angi passord for {$brukerprofil->hentBrukernavn()}";
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Person\Brukerprofil $brukerprofil */
        $brukerprofil = $this->hoveddata['brukerprofil'];

        return $this->vis(\Kyegil\Leiebasen\Visning\drift\html\profil_passord_skjema\Index::class, [
            'brukerprofil' => $brukerprofil
        ]);
    }

    /**
     * @return ViewInterface
     */
    public function skript(): ViewInterface
    {
        /** @var Person\Brukerprofil|null $brukerprofil */
        $brukerprofil = $this->hoveddata['brukerprofil'];

        $skript = new ViewArray();
        $skript->addItem($this->vis(\Kyegil\Leiebasen\Visning\drift\js\profil_passord_skjema\Index::class));
        return $skript;
    }
}