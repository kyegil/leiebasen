<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Depositum;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Visning\drift\html\depositum_skjema\Index;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class DepositumSkjema extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => DepositumSkjema\Oppdrag::class
    ];
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $depositumId = !empty($_GET['id']) ? $_GET['id'] : null;
        $leieforholdId = $_GET['leieforhold'] ?? null;
        /** @var Depositum $depositum */
        $depositum = $this->hentModell(Depositum::class, (int)$depositumId);
        $leieforhold = $depositum->hentLeieforhold() ?? null;
        $leieforhold = $leieforhold ?? $this->hentModell(Leieforhold::class, (int)$leieforholdId);
        if (!$depositum->hentId() || $depositumId == '*') {
            $depositum = null;
        }
        if ($leieforhold && !$leieforhold->hentId()) {
            $leieforhold = null;
        }
        /**
         * Dersom leieforholdet allerede har depositum registrert,
         * så oppdateres dette i stedet for å opprette nytt
         */
        if($leieforhold && !$depositum && $leieforhold->hentDepositum()) {
            $depositum = $leieforhold->hentDepositum();
            $depositumId = $depositum->hentId();
        }

        $this->hoveddata['depositum'] = $depositum;
        $this->hoveddata['depositumId'] = $depositumId;
        $this->hoveddata['leieforhold'] = $leieforhold;
        $this->hoveddata['leieforholdId'] = $leieforholdId;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Depositum|null $depositum */
        $depositum = $this->hoveddata['depositum'];
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        if(!$depositum && $this->hoveddata['depositumId'] != '*') {
            return false;
        }
        $this->tittel = 'Depositum';
        if($leieforhold) {
            $this->tittel .= ' for ' . $leieforhold->hentNavn();
        }

        return true;
    }

    /**
     * @return Index
     * @throws Exception
     */
    public function innhold()
    {
        return $this->vis(Index::class, [
            'depositum' => $this->hoveddata['depositum'],
            'depositumId' => $this->hoveddata['depositumId'],
            'leieforhold' => $this->hoveddata['leieforhold'],
            'leieforholdId' => $this->hoveddata['leieforholdId'],
        ]);
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function skript(): ViewInterface
    {
        /** @var Depositum $depositum */
        $depositum = $this->hoveddata['depositum'];

        $skript = new ViewArray([
            $this->vis(\Kyegil\Leiebasen\Visning\drift\js\depositum_skjema\Index::class, [
                'depositum' => $depositum,
                'depositumId' => $this->hoveddata['depositumId'],
                'leieforhold' => $this->hoveddata['leieforhold'],
                'leieforholdId' => $this->hoveddata['leieforholdId'],
            ]),
        ]);
        return $skript;
    }
}