<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/10/2020
 * Time: 16:59
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\OversiktGjeld;


use Exception;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Visning\felles\html\table\HtmlTable;

class Utskrift extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function utskrift() {
        try {
            /**
             * @var int $beboerfilter
             * * 0 All gjeld
             * * 1 Kun nåværende leieforhold
             * * 2 Kun avsluttede leieforhold
             * * 5 Kun avsluttede ikke-frosne leieforhold
             * * 10 Kun frosne leieforhold
             */
            $beboerfilter = intval($_GET['leietakere'] ?? 0);
            $datasett = json_decode($this->app->hentUtlevering()->hentData('')->hentRespons()->innholdSomStreng(), false);
            $kolonner = [
                (object)['data' => 'leieforhold', 'title' => 'Leieforhold', 'classes' => []],
                (object)['data' => 'utestående', 'title' => 'Utestående', 'classes' => ['beløp']],
                (object)['data' => 'forfalt', 'title' => 'Forfalt beløp', 'classes' => ['beløp']],
                (object)['data' => 'siste_innbetaling', 'title' => 'Siste betaling', 'classes' => []],
                (object)['data' => 'tilsv_ant_leier', 'title' => 'Tilsv ant leier',],
                (object)['data' => 'betalingsplan_notat', 'title' => 'Betalingsplan',],
            ];
            if($beboerfilter != 1) {
                array_splice( $kolonner, 5, 0, [
                    (object)['data' => 'avsluttet', 'title' => 'Avsluttet', 'classes' => []],
                    (object)['data' => 'frosset', 'title' => 'Frosset', 'classes' => []],
                ] );
            }
            $data = [];

            foreach ($datasett->data as $datasettData) {
                $data[] = (object)[
                    'leieforhold' => $datasettData->leieforhold . ': ' . $datasettData->leieforholdbeskrivelse,
                    'utestående' => $this->app->kr($datasettData->utestående),
                    'forfalt' => $this->app->kr($datasettData->forfalt),
                    'siste_innbetaling' => $datasettData->siste_innbetaling ? date_create($datasettData->siste_innbetaling)->format('d.m.Y') : '',
                    'tilsv_ant_leier' => $datasettData->tilsv_ant_leier ? number_format($datasettData->tilsv_ant_leier, 1, ',',   '') : '',
                    'betalingsplan_notat' => $datasettData->betalingsplan_notat,
                    'avsluttet' => $datasettData->avsluttet ? date_create($datasettData->avsluttet)->format('d.m.Y') : '',
                    'frosset' => $datasettData->avsluttet ? 'frosset' : '',
                ];
            }
            $tabell = $this->app->vis(HtmlTable::class, [
                'caption' => $this->app->tittel,
                'kolonner' => $kolonner,
                'data' => $data
            ]);
            $resultat = $this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\shared\Utskrift::class, [
                'tittel' => $this->app->tittel,
                'innhold' => [
                    $tabell
                ]
            ]);
        }
        catch (Exception $e) {
            $resultat = $e;
        }
            $this->settRespons(new Respons($resultat));
            return $this;
    }
}