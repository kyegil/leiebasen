<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\OversiktGjeld;


use DateInterval;
use DateTime;
use Exception;
use Kyegil\CoreModel\CoreModel;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Oppsigelse;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Modell\Poengprogram\Poeng;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\Leiebasen\Poengbestyrer;
use Kyegil\Leiebasen\Respons\JsonRespons;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\OversiktGjeld
 */
class Utlevering extends AbstraktUtlevering
{
    const POENG_PROGRAM_PREFIX = 'poenger_';

    /**
     * @param string $data
     * @return $this
     * @throws Exception
     * @link https://datatables.net/manual/server-side
     */
    public function hentData($data = null): Utlevering
    {
        $get = $this->data['get'];
        $post = $this->data['post'];

        switch ($data) {
            default: {
                $resultat = (object)[
                    'success' => true,
                    'draw' => intval($get['draw'] ?? 0),
                    'data'		=> [],
//                    'totalRows' => 0,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                ];

                /** @var array{column:int, dir:string, name:string, columnName:string}[] $order */
                $order  = $get['order'] ?? [];
                $start		= intval($get['start'] ?? 0);
                $limit		= $get['length'] ?? 25;

                $søkefelt = $get['search']['value'] ?? '';

                /**
                 * @var int $leieforholdfilter
                 * * 0 All gjeld
                 * * 1 Kun nåværende leieforhold
                 * * 2 Kun avsluttede leieforhold
                 * * 5 Kun avsluttede ikke-frosne leieforhold
                 * * 10 Kun frosne leieforhold
                 */
                $leieforholdfilter = intval($get['leieforholdfilter'] ?? 0);

                try {
                    $ordensAnvisning = [
                        'leieforhold' => [null, 'leieforholdnr'],
                        'utestående' => ['leieforhold_ekstra', 'utestående'],
                        'forfalt' => ['leieforhold_ekstra', 'forfalt'],
                        'siste_innbetaling' => ['leieforhold_ekstra', 'siste_innbetalingsdato'],
                        'tilsv_ant_leier' => ['leieforhold_ekstra', 'tilsv_ant_leier'],
                        'oppfølging' => ['leieforhold_ekstra', 'oppfølging'],
                        'avsluttet' => ['oppsigelser', 'fristillelsesdato'],
                        'frosset' => [null, 'frosset']
                    ];

                    foreach (Poengbestyrer::getInstance($this->app)->hentAktiveProgram() as $program) {
                        $programAlias = self::POENG_PROGRAM_PREFIX . $program->kode;
                        $ordensAnvisning[$programAlias] = ['leieforhold_ekstra', $programAlias];
                    }

                    /** @var Person $bruker */
                    $bruker = $this->app->hentModell(Person::class, $this->app->bruker['id']);

                    /** @var Leieforholdsett $leieforholdsett */
                    $leieforholdsett = $this->app->hentSamling(Leieforhold::class)
                        ->leggTilInnerJoin(Krav::hentTabell(), Krav::hentTabell() . '.leieforhold = ' . Leieforhold::hentTabell() . '.' . Leieforhold::hentPrimærnøkkelfelt())

                        ->leggTilFelt(Krav::hentTabell(), 'utestående', 'sum', 'utestående', 'leieforhold_ekstra')
                        ->leggTilUttrykk('leieforhold_ekstra', 'forfalt', 'IF(`krav`.`forfall` IS NOT NULL AND `krav`.`forfall`<= "' . date('Y-m-d') . '", `krav`.`utestående`, 0)', 'sum')
                        ->leggTilUttrykk('leieforhold_ekstra', 'siste_innbetalingsdato', 'SELECT MAX(`dato`) FROM `' . $this->app->mysqli->table_prefix .'innbetalinger` WHERE `konto` <> "0" AND `beløp` > 0 AND `leieforhold` = `' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`', '')
                        ->leggTilUttrykk('leieforhold_ekstra', 'tilsv_ant_leier', 'IF(`leieforhold`.`leiebeløp`, 12 * sum(`krav`.`utestående`)/(`leieforhold`.`leiebeløp`*`leieforhold`.`ant_terminer`), null)')
                        ->leggTilUttrykk('leieforhold_ekstra', 'oppfølging', 'IF(`leieforhold`.`stopp_oppfølging`, "9999-99-99", IF(`leieforhold`.`avvent_oppfølging` > NOW(), `leieforhold`.`avvent_oppfølging`, "0000-00-00"))')

                        ->leggTilFilter(['`' . Krav::hentTabell(). '`.`kravdato` <= NOW()'])
                        ->leggTilFilter(['`' . Krav::hentTabell(). '`.`utestående` <>' => 0])
                    ;
                    $leieforholdsett->leggTilOppsigelseModell(['fristillelsesdato']);
                    $leieforholdsett->leggTilBetalingsplanModell(['aktiv'])
                        ->leggTilUttrykk('betalingsplaner.betalingsplan_ekstra', 'slutt_dato', 'SELECT MAX(`forfallsdato`) FROM `' . $this->app->mysqli->table_prefix .'betalingsplan_avdrag` WHERE `betalingsplan_id` = `' . Betalingsplan::hentTabell() . '`.`' . Betalingsplan::hentPrimærnøkkelfelt() . '`', '')
                    ;

                    $this->leggTilPoengprogramFelter($leieforholdsett);

                    if ($søkefelt) {
                        foreach(explode(' ', $søkefelt) as $søkeord) {
                            $leieforholdsett->leggTilFilter(['`' . Leieforhold::hentTabell(). '`.`beskrivelse` LIKE' => "%{$søkeord}%"]);
                        }
                    }

                    if ($leieforholdfilter == 1) {
                        $leieforholdsett->leggTilFilter([
                            'or' => [
                                '`oppsigelser`.`leieforhold`' => null,
                                '`oppsigelser`.`fristillelsesdato` >' => date('Y-m-d')
                            ]
                        ]);
                    }
                    else if (in_array($leieforholdfilter, [2,5,10])) {
                        $leieforholdsett->leggTilFilter([
                            'and' => [
                                '`oppsigelser`.`leieforhold` IS NOT NULL',
                                '`oppsigelser`.`fristillelsesdato` <=' => date('Y-m-d')
                            ]
                        ]);
                    }

                    if (in_array($leieforholdfilter, [5,10])) {
                        $leieforholdsett->leggTilFilter([
                            'frosset' => $leieforholdfilter == 10
                        ]);
                    }

                    $leieforholdsett->låsFiltre();

                    $this->sorter($leieforholdsett, $order, $ordensAnvisning);

                    $leieforholdsett->settStart($start)->begrens($limit);

                    $resultat->recordsFiltered = $resultat->recordsTotal = $resultat->totalRows
                        = $leieforholdsett->hentAntall();

                    /** @var Leieforhold $leieforhold */
                    foreach ($leieforholdsett as $leieforhold) {
                        $linjeData = $this->hentLinjeData($leieforhold, $bruker);
                        $resultat->data[] = $linjeData;
                    }
                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->error = $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }
        }
    }

    /**
     * @param Leieforholdsett $leieforholdsett
     * @return void
     * @throws Exception
     */
    private function leggTilPoengprogramFelter(Leieforholdsett $leieforholdsett): void
    {
        foreach (Poengbestyrer::getInstance($this->app)->hentAktiveProgram() as $program) {
            $tp = CoreModel::$tablePrefix;
            $poengTabell = Poeng::hentTabell();
            $programAlias = self::POENG_PROGRAM_PREFIX . $program->kode;

            $subQuery = <<<SUBQUERY
            (
             SELECT SUM(`{$poengTabell}`.`verdi`)
             FROM `{$tp}{$poengTabell}` AS `{$poengTabell}`
             WHERE `{$poengTabell}`.`program_id` = {$program->hentId()}
             AND `{$poengTabell}`.`foreldes` > NOW()
             AND `{$poengTabell}`.`leieforhold_id` = `leieforhold`.`leieforholdnr`
            ) 
            SUBQUERY;

            $leieforholdsett->leggTilUttrykk('leieforhold_ekstra', $programAlias, $subQuery);
        }
    }

    /**
     * @param Leieforhold $leieforhold
     * @param Person $bruker
     * @return object
     * @throws Exception
     */
    private function hentLinjeData(Leieforhold $leieforhold, Person $bruker): object
    {
        extract($this->app->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        /** @var Oppsigelse|false $oppsigelse */
        $oppsigelse = $leieforhold->hentOppsigelse();
        if ($oppsigelse) {
            $avsluttet = clone $oppsigelse->hentFristillelsesdato();
            $avsluttet->sub(new DateInterval('P1D'));
        } else {
            $avsluttet = false;
        }

        $html = "";

        $sisteBetaling = $leieforhold->hentSisteBetaling();

        /** @var Betalingsplan|null $betalingsplan */
        $betalingsplan = null;
        if ($bruker->harAdgangTil(Adgang::ADGANG_OPPFØLGING)) {
            $betalingsplan = $leieforhold->hentBetalingsplan() && $leieforhold->hentBetalingsplan()->hentAktiv() ? $leieforhold->hentBetalingsplan() : null;
        }
        $betalingsplanNotat = $betalingsplan ? 'betalingsplan' : '';

        $oppfølging = new DateTime;
        $oppfølgingFormatert = new HtmlElement('a', [
            'title' => 'Klikk her for oppfølging',
            'href' => '/oppfølging/index.php?oppslag=oversikt&id=' . $leieforhold->hentId(),
        ], 'Gå til oppfølging');
        if ($leieforhold->hentAvventOppfølging() > date_create()) {
            $oppfølging = $leieforhold->hentAvventOppfølging();
            $oppfølgingFormatert = 'Vent til ' . $oppfølging->format('d.m.Y');
        }

        if ($leieforhold->hentStoppOppfølging()) {
            $oppfølging = null;
            $oppfølgingFormatert = 'Videre oppfølging er stoppet';
        }

        $tilsvAntLeier = (
        $leieforhold->hentLeiebeløp()
            ?
            12 * $leieforhold->hentUtestående()
            / max(1, ($leieforhold->hentLeiebeløp() * $leieforhold->hentAntTerminer()))
            : null
        );

        $tilsvAntLeierFormatert = (floor($tilsvAntLeier) != $tilsvAntLeier
                ? '>&nbsp;' : '') . floor($tilsvAntLeier);

        $linjeData = (object)array(
            'leieforhold' => $leieforhold->hentId(),
            'leieforholdbeskrivelse' => DriftKontroller::lenkeTilLeieforholdKort($leieforhold) . ': ' . $leieforhold->hentBeskrivelse(),
            'utestående' => $leieforhold->hentUtestående(),
            'forfalt' => $leieforhold->hentForfalt(),
            'leiebeløp' => $leieforhold->hentLeiebeløp(),
            'tilsv_ant_leier' => $tilsvAntLeier,
            'tilsv_ant_leier_formatert' => $tilsvAntLeierFormatert,
            'oppfølging' => $oppfølging ? $oppfølging->format('Y-m-d') : null,
            'oppfølging_formatert' => (string)$oppfølgingFormatert,
            'siste_innbetaling' => $sisteBetaling ? $sisteBetaling->dato->format('Y-m-d') : null,
            'siste_betalingsplan' => $betalingsplan && $betalingsplan->hentSluttDato() ? $betalingsplan->hentSluttDato()->format('Y-m-d') : null,
            'betalingsplan_notat' => $betalingsplanNotat,
            'siste_beløp' => $sisteBetaling ? $sisteBetaling->hentBeløpTilLeieforhold($leieforhold) : null,
            'avsluttet' => $oppsigelse ? $avsluttet->format('Y-m-d') : false,
            'frosset' => $leieforhold->hentFrosset(),
            'html' => $html
        );

        foreach (Poengbestyrer::getInstance($this->app)->hentAktiveProgram() as $program) {
            $programAlias = self::POENG_PROGRAM_PREFIX . $program->kode;
            $verdi = $leieforhold->getRawData()->leieforhold_ekstra->$programAlias ?? null;
            $linjeData->$programAlias = $verdi;
        }

        return $this->app->post($this, __FUNCTION__, $linjeData, $args);
    }
}