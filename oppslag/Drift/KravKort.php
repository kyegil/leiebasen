<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\Drift;


use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\Drift\KravKort\Oppdrag;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\krav_kort\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * KravKort Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class KravKort extends DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Krav';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'manipuler' => Oppdrag::class
    ];

    /**
     * StrømavstemmingSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $kravId = isset($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['krav'] = $this->hentModell(Krav::class, $kravId);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $brukerId = $this->bruker['id'];
        $bruker = $this->hentModell(Person::class, $brukerId);
        $this->hoveddata['bruker'] = $bruker;

        /** @var Krav $krav */
        $krav = $this->hoveddata['krav'];
        if(!$krav->hentId()) {
            return false;
        }
        $this->tittel = "{$krav->hentTekst()}";

        return true;
    }

    /**
     * @return Index
     * @throws \Exception
     */
    public function skript() {
        /** @var Krav $krav */
        $krav = $this->hoveddata['krav'];

        if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }
        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'krav' => $krav,
                'tilbakeKnapp' => $this->returi->get()
            ])
        ]);
        return $skript;
    }
}