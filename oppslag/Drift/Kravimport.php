<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;


use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\Drift\KravKort\Oppdrag;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\kravimport\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * Kravimport Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class Kravimport extends DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Masseimport/oppretting av krav';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'manipuler' => Oppdrag::class
    ];

    /**
     * @return bool
     * @throws \Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        return true;
    }

    /**
     * @return Index
     * @throws \Exception
     */
    public function skript() {
        if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }

        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'tilbakeKnapp' => $this->returi->get()
            ])
        ]);
        return $skript;
    }
}