<?php
/**
 * Part of boligstiftelsen.svartlamon.org
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel\Element;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_fordelingselement_skjema\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * DelteKostnaderFordelingselementSkjema Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class DelteKostnaderFordelingselementSkjema extends DriftExtJsAdaptor
{
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @var AbstraktOppdrag[] */
//    protected $oppdrag = [
//        'oppgave' => AdgangSkjema\Oppdrag::class
//    ];
//

    /**
     * AdgangSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $this->tittel = 'Fordelingsnøkkel';

        $elementId = !empty($_GET['id']) ? $_GET['id'] : null;
        $nøkkelId = $_GET['nøkkel'] ?? null;

        /** @var Element|null $tjeneste */
        $this->hoveddata['element'] = $elementId == '*' ? '*' : $this->hentModell(Element::class, $elementId);
        /** @var Fordelingsnøkkel|null $tjeneste */
        $this->hoveddata['fordelingsnøkkel'] = $this->hentModell(Fordelingsnøkkel::class, $nøkkelId);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Element|string|null $element */
        $element = $this->hoveddata['element'];
        /** @var Fordelingsnøkkel|null $fordelingsnøkkel */
        $fordelingsnøkkel = $this->hoveddata['fordelingsnøkkel'];
        if ($element instanceof Element && !$element->hentId()) {
            return false;
        }
        if (is_string($element)) {
            $this->hoveddata['element'] = null;
        }
        if (!$fordelingsnøkkel || !$fordelingsnøkkel->hentId()) {
            return false;
        }

        return true;
    }

    /**
     * @return Index
     * @throws Exception
     */
    public function skript(): Index
    {
        /** @var Element|null $element */
        $element = $this->hoveddata['element'];
        /** @var Fordelingsnøkkel $fordelingsnøkkel */
        $fordelingsnøkkel = $this->hoveddata['fordelingsnøkkel'];

        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'element' => $element,
                'elementId' => $element && $element->hentId() ? $element->hentId() : '*',
                'fordelingsnøkkel' => $fordelingsnøkkel,
                'fordelingsnøkkelId' => $fordelingsnøkkel->hentId(),
            ])
        ]);
        return $skript;
    }
}