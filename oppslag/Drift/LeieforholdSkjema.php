<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class LeieforholdSkjema extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => LeieforholdSkjema\Oppdrag::class
    ];
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $leieforholdId = intval($_GET['id'] ?? null) ?: '*';
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentModell(Leieforhold::class, (int)$leieforholdId);
        if ($leieforholdId == '*') {
            $leieforhold = null;
        }
        $this->hoveddata['leieforhold'] = $leieforhold;
        $this->hoveddata['leieforholdId'] = $leieforholdId;

        $fornyelse = !empty($_GET['fornyelse']);
        $this->hoveddata['fornyelse'] = $fornyelse;

        /**
         * Dersom leieobjekt er angitt i GET-parameter, er det ikke nødvendig med combofelt for dette
         * @var int|null $leieobjektId
         */
        $leieobjektId = !empty($_GET['leieobjekt']) ? (int)$_GET['leieobjekt'] : null;
        $leieobjektId = !empty($_POST['leieobjekt']) ? (int)$_POST['leieobjekt'] : $leieobjektId;

        $this->hoveddata['leieobjektId'] = $leieobjektId;

        $teller = intval($_GET['andel']['teller'] ?? ($_POST['andel']['teller'] ?? 1));
        $nevner = intval($_GET['andel']['nevner'] ?? ($_POST['andel']['nevner'] ?? 1));
        $andel = new Fraction($teller, $nevner);
        $this->hoveddata['andel'] = $andel;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        /** @var bool $fornyelse */
        $fornyelse = $this->hoveddata['fornyelse'];
        if($leieforhold && !$leieforhold->hentId()) {
            return false;
        }
        $this->tittel = 'Registrer nytt leieforhold';
        if($leieforhold) {
            $this->tittel = 'Endring av leieforhold ';
            if($fornyelse) {
                $this->tittel = 'Fornying av leieforhold ';
            }
            $this->tittel .= $leieforhold->hentId();
        }
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        /** @var bool $fornyelse */
        $fornyelse = $this->hoveddata['fornyelse'];
        /** @var int|null $leieobjektId */
        $leieobjektId = $leieforhold && $leieforhold->leieobjekt
            ? ($leieforhold->leieobjekt->id)
            : $this->hoveddata['leieobjektId'];
        /** @var Fraction $andel */
        $andel = $this->hoveddata['andel'];

        $visning = new ViewArray();

        $visning->addItem($this->vis(\Kyegil\Leiebasen\Visning\drift\html\LeieforholdSkjema::class, [
            'leieforhold' => $leieforhold,
            'leieforholdId' => $this->hoveddata['leieforholdId'],
            'leieobjektId' => $leieobjektId,
            'fornyelse' => $fornyelse,
            'andel' => $andel,
        ]));

        return $visning;
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function skript(): ViewInterface
    {
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];

        $skript = new ViewArray();

        $skript->addItem($this->vis(\Kyegil\Leiebasen\Visning\drift\js\LeieforholdSkjema::class, [
            'leieforhold' => $leieforhold,
            'leieforholdId' => $this->hoveddata['leieforholdId'],
        ]));

        return $skript;
    }
}