<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\område_skjema\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * OmrådeSkjema Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class OmrådeSkjema extends DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Område';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => OmrådeSkjema\Oppdrag::class
    ];


    /**
     * OmrådeSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $områdeId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['område'] = null;

        if ((int)$områdeId) {
            $this->hoveddata['område'] = $this->hentModell(Område::class, (int)$områdeId);
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Område $område */
        $område = $this->hoveddata['område'];
        if (
            $område
            && !$område->hentId()
        ) {
            return false;
        }
        else if ($område) {
            $this->tittel = $område->navn;
        }

        return true;
    }

    /**
     * @return Index
     */
    public function skript() {
        /** @var Område|null $område */
        $område = $this->hoveddata['område'];

        if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }

        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'område' => $område
            ])
        ]);
        return $skript;
    }
}