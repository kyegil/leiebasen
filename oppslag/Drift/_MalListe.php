<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\Drift\_MalListe\Mottak;
use Kyegil\Leiebasen\Oppslag\Drift\_MalListe\Utlevering;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\ViewRenderer\ViewArray;

/**
 * _MalListe Controller
 *
 * @method Utlevering hentUtlevering()
 * @method Mottak hentMottak()
 * @package Kyegil\Leiebasen\Oppslag\Drift
 * @noinspection PhpUnused
 */
class _MalListe extends DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => _MalListe\Oppdrag::class
    ];
    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = 'Mal';
        return true;
    }

    /**
     * @return ViewArray
     */
    public function innhold()
    {
        $visning = new ViewArray();
        $visning->addItem(
            $this->vis(\Kyegil\Leiebasen\Visning\drift\html\_MalListe::class)
        );

        return $visning;
    }

    /**
     * @return ViewArray
     */
    public function skript(): ViewArray
    {
        $skript = new ViewArray();
        $skript->addItem(
            $this->vis(\Kyegil\Leiebasen\Visning\drift\js\_MalListe::class)
        );
        return $skript;
    }
}