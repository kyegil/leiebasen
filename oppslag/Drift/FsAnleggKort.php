<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_tjeneste_kort\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * FsAnleggKort Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class FsAnleggKort extends DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Strømanlegg';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * FsAnleggKort constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $tjenesteId = !empty($_GET['id']) ? $_GET['id'] : null;
        $tab = $_GET['tab'] ?? '';
        $this->hoveddata['strømanlegg'] = $this->hoveddata['tjeneste'] = $this->hentModell(Strømanlegg::class, $tjenesteId);
        $this->hoveddata['tab'] = in_array($tab, ['detaljer', 'forbruk', 'fakturaer', 'fordelingsnøkkel']) ? $tab : 'detaljer';
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML(): bool
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Strømanlegg|null $strømanlegg */
        $strømanlegg = $this->hoveddata['strømanlegg'];
        if(!$strømanlegg || !$strømanlegg->hentId()) {
            return false;
        }
        $this->tittel = $strømanlegg->hentNavn();

        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     * @throws Exception
     */
    public function skript(): Index
    {
        /** @var Strømanlegg $strømanlegg */
        $strømanlegg = $this->hoveddata['strømanlegg'];
        /** @var string $tab */
        $tab = $this->hoveddata['tab'];

        /** @var Index $skript */
        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'tjeneste' => $strømanlegg,
                'tjenesteId' => $strømanlegg->hentId(),
                'tilbakeKnapp' => $this->returi->get(),
                'tab' => $tab
            ])
        ]);
        return $skript;
    }
}