<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\ViewRenderer\ViewArray;

/**
 * LeieobjekterLedige Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 * @noinspection PhpUnused
 */
class LeieobjekterLedige extends DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
    ];
    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML(): bool
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = 'Ledige leieobjekter';
        return true;
    }

    /**
     * @return ViewArray
     */
    public function innhold(): ViewArray
    {
        $visning = new ViewArray();
        $visning->addItem(
            $this->vis(\Kyegil\Leiebasen\Visning\drift\html\LeieobjekterLedige::class)
        );

        return $visning;
    }

    /**
     * @return ViewArray
     * @noinspection PhpUnnecessaryLocalVariableInspection
     */
    public function skript(): ViewArray
    {
        $skript = new ViewArray();
        return $skript;
    }
}