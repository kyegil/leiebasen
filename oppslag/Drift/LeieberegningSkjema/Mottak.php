<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/02/2023
 * Time: 15:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\LeieberegningSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Lagret."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'leieberegningsskjema':
                    $id = $get['id'] ?? null;
                    $navn = isset($post['navn']) ? strip_tags($post['navn']) : null;
                    $beskrivelse = isset($post['beskrivelse']) ? strip_tags($post['beskrivelse']) : null;
                    $leiePerObjekt = !empty($post['leie_per_objekt']) ? str_replace(',', '.', $post['leie_per_objekt']) : 0;
                    $leiePerKontrakt = !empty($post['leie_per_kontrakt']) ? str_replace(',', '.', $post['leie_per_kontrakt']) : 0;
                    $leiePerKvadratmeter = !empty($post['leie_per_kvadratmeter']) ? str_replace(',', '.', $post['leie_per_kvadratmeter']) : 0;
                    /** @var array $spesialregler */
                    $spesialregler = isset($post['spesialregel']) ? (array)$post['spesialregel'] : [];
                    $this->saniterSpesialregler($spesialregler);
                    usort($spesialregler, function ($regelA, $regelB) {
                        return $regelA->sorteringsorden - $regelB->sorteringsorden;
                    });

                    if($id == '*') {
                        /** @var Leieberegning $leieberegning */
                        $leieberegning = $this->app->nyModell(Leieberegning::class, (object)[
                            'navn' => $navn,
                            'beskrivelse' => $beskrivelse,
                            'leie_per_objekt' => $leiePerObjekt,
                            'leie_per_kontrakt' => $leiePerKontrakt,
                            'leie_per_kvadratmeter' => $leiePerKvadratmeter,
                            'spesialregler' => $spesialregler,
                        ]);
                        $leieberegning->oppdaterSatsHistorikk();
                    }
                    else {
                        /** @var Leieberegning $leieberegning */
                        $leieberegning = $this->app->hentModell(Leieberegning::class, $id);
                        if (!$leieberegning->hentId()) {
                            throw new Exception('Leieberegningsmodell ' . $id . ' finnes ikke');
                        }
                        $satsenErEndret = $this->satsenErEndret($leieberegning, $leiePerObjekt, $leiePerKontrakt, $leiePerKvadratmeter, $spesialregler);

                        $leieberegning
                            ->settNavn($navn)
                            ->settBeskrivelse($beskrivelse)
                            ->settLeiePerObjekt($leiePerObjekt)
                            ->settLeiePerKontrakt($leiePerKontrakt)
                            ->settLeiePerKvadratmeter($leiePerKvadratmeter)
                            ->settSpesialregler($spesialregler)
                        ;
                        if($satsenErEndret) {
                            $leieberegning->oppdaterSatsHistorikk();
                        }
                    }
                    $resultat->id = $leieberegning->hentId();
                    $resultat->msg = 'Leieberegningen er lagret';
                    $resultat->url = (string)$this->app->returi->get(1);
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param array $spesialregler
     * @return Mottak
     * @throws Exception
     */
    private function saniterSpesialregler(array &$spesialregler): Mottak
    {
        list('spesialregler' => $spesialregler)
            = $this->app->before($this, __FUNCTION__, [
            'spesialregler' => $spesialregler
        ]);
        foreach($spesialregler as &$spesialregel) {
            settype($spesialregel, 'object');
            $spesialregel->sats = $spesialregel->sats ? str_replace(',', '.', $spesialregel->sats) : 0;
            settype($spesialregel->sorteringsorden, 'integer');
            settype($spesialregel->unntatt_fra_leiejustering, 'boolean');

            if(!is_callable($spesialregel->prosessor)) {
                throw new Exception('Ugyldig prosessor');
            }
            $spesialregel->param = json_decode($spesialregel->param, false, 512, JSON_THROW_ON_ERROR);
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * @param Leieberegning $leieberegning
     * @param string $leiePerObjekt
     * @param string $leiePerKontrakt
     * @param string $leiePerKvadratmeter
     * @param array $spesialregler
     * @return bool
     */
    private function satsenErEndret(Leieberegning $leieberegning,
        string $leiePerObjekt,
        string $leiePerKontrakt,
        string $leiePerKvadratmeter,
        array $spesialregler
    ): bool
    {
        if(
            $leiePerObjekt != $leieberegning->hentLeiePerObjekt()
            || $leiePerKontrakt != $leieberegning->hentLeiePerKontrakt()
            || $leiePerKvadratmeter != $leieberegning->hentLeiePerKvadratmeter()
        ) {
            return true;
        }
        foreach ($leieberegning->hentSpesialregler() as $idx => $spesialregel) {
            $nySats = isset($spesialregler[$idx]) && isset($spesialregler[$idx]->sats)
                ? $spesialregler[$idx]->sats : null;
            if(($spesialregel->sats ?? 0) != $nySats) {
                return true;
            }
        }
        return false;
    }
}