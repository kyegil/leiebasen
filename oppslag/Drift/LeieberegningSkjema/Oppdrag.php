<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 04/12/2020
 * Time: 23:21
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\LeieberegningSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieobjekt\Leieberegning;
use Kyegil\Leiebasen\Modell\Leieobjektsett;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return Oppdrag
     */
    protected function slettLeieberegning():Oppdrag {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            $leieberegningId = (int)$this->data['post']['leieberegning_id'];
            /** @var Leieberegning $leieberegning */
            $leieberegning = $this->app->hentModell(Leieberegning::class, $leieberegningId);
            if (!$leieberegning->hentId()) {
                throw new Exception('Finner ikke denne leieberegningsmodellen');
            }
            $this->verifiserSletting($leieberegning);
            $resultat->msg = 'Leieberegningen har blitt slettet';
            $leieberegning->slett();
            $resultat->url = (string)$this->app->returi->get(1);
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param Leieberegning $leieberegning
     * @return Oppdrag
     * @throws Exception
     */
    private function verifiserSletting(Leieberegning $leieberegning): Oppdrag
    {
        list('leieberegning' => $leieberegning)
            = $this->app->before($this, __FUNCTION__, [
            'leieberegning' => $leieberegning
        ]);
        /** @var Leieobjektsett $leieobjekter */
        $leieobjekter = $leieberegning->hentBruktAv()->beskjær(0,10);
        if($leieobjekter->hentAntall()) {
            $msg = 'Beregningsmodellen kan ikke slettes fordi den brukes, f.eks av leieobjekt(ene) '
            . implode(',', $leieobjekter->hentIdNumre()) . ".\n"
            . 'Du må endre beregningsmodell for leieobjektene før du sletter denne.'
            ;
            throw new Exception($msg);
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }
}