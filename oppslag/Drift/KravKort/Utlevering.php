<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\KravKort;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Innbetaling\Delbeløp;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Visning;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\KravKort
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param null $data
     * @return $this|Utlevering
     */
    public function hentData($data = null) {
        $tp = $this->app->mysqli->table_prefix;
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        switch ($data) {
            case "utlikningsmuligheter":
                try {
                    $innbetalingsId = isset($_GET['innbetalingsid']) ? $_GET['innbetalingsid'] : '';
                    $delbeløpId = isset($_POST['delbeløp_id']) ? $_POST['delbeløp_id'] : '';
                    /** @var Innbetaling $innbetaling */
                    $innbetaling = $this->app->hentModell(Innbetaling::class, $innbetalingsId);
                    if (!$innbetaling->hentId()) {
                        throw new Exception("Ugyldig betaling");
                    }
                    /** @var Delbeløp $delbeløp */
                    $delbeløp = $innbetaling->hentDelbeløp($delbeløpId);
                    if (!$delbeløp) {
                        throw new Exception("Ugyldig delbeløp");
                    }
                    $leieforhold = $delbeløp->hentLeieforhold();
                    $utbetaling = $innbetaling->hentBeløp() < 0;
                    $motbetalinger = $this->app->mysqli->select([
                        'source' => "{$tp}innbetalinger AS innbetalinger",
                        'fields' => "innbetalinger.leieforhold, SUM(innbetalinger.beløp) as beløp",
                        'groupfields' => "innbetalinger.leieforhold",
                        'where' => "innbetalinger.krav IS NULL
                                        AND innbetalinger.leieforhold = '{$leieforhold}'
                                        AND innbetalinger.innbetaling != '{$innbetaling}'
                                        AND beløp " . ($utbetaling ? ">" : "<") . " 0"
                    ]);
                    $motkrav = $this->app->hentSamling(Krav::class)
                        ->leggTilInnerJoin('kontrakter', 'krav.kontraktnr = kontrakter.kontraktnr')
                        ->leggTilFilter([
                            'utestående ' . ($utbetaling ? "<" : ">") => 0,
                            'kontrakter.leieforhold' => $leieforhold->hentId()
                        ]);
                    foreach ($motbetalinger->data as $betaling) {
                        $resultat->data[] = array(
                            'id' => 0,
                            'tekst' => $utbetaling ? "Innbetalinger" : "Utbetalinger",
                            'beløp' => -$betaling->beløp
                        );
                    }
                    /** @var Krav $krav */
                    foreach ($motkrav as $krav) {
                        $resultat->data[] = array(
                            'id' => $krav->hentId(),
                            'tekst' => $krav->hentTekst(),
                            'beløp' => $krav->hentUtestående()
                        );
                    }
                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                break;

            case "avtalegiro_forsendelseskvittering":
                try {
                    /** @var Krav $krav */
                    $krav = $this->app->hentModell(Krav::class, (int)$_GET['id']);
                    $regning = $krav->hentRegning();
                    $avtalegiro = $regning ? $regning->hentFboTrekk() : null;
                    $resultat = $avtalegiro ? $avtalegiro->hentForsendelsesKvittering() : null;
                    $this->settRespons(new Respons($resultat));
                }
                catch (Exception $e) {
                    $resultat = new HtmlElement('div', [], addslashes($e->getMessage()));
                    $this->settRespons(new Respons($resultat));
                }

                break;

            default:
                try {
                    /** @var Krav $krav */
                    $krav = $this->app->hentModell(Krav::class, (int)$_GET['id']);

                    if($krav->hentBeløp() >= 0) {
                        $resultat = $this->app->vis(Visning\drift\html\krav_kort\extjs4\panel\Krav::class, [
                            'krav' => $krav
                        ]);
                    }
                    else {
                        $resultat = $this->app->vis(Visning\drift\html\krav_kort\extjs4\panel\Kreditt::class, [
                            'krav' => $krav
                        ]);
                    }

                    $this->settRespons(new Respons($resultat));
                } catch (Exception $e) {
                    $resultat = new HtmlElement('div', [], addslashes($e->getMessage()));
                    $this->settRespons(new Respons($resultat));
                }
        }
        return $this;
    }
}