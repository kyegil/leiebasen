<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 04/12/2020
 * Time: 23:21
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\KravKort;


use Exception;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;

class Oppdrag extends AbstraktOppdrag
{
    protected function manipuler($data = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case "frakople":
                    $innbetalingsId = $_GET['innbetalingsid'] ?? null;
                    $delbeløpId = isset($_POST['delbeløp_id']) ? intval($_POST['delbeløp_id']) : null;

                    /** @var Innbetaling $innbetaling */
                    $innbetaling = $this->app->hentModell(Innbetaling::class, $innbetalingsId);
                    if(!$innbetaling->hentId()) {
                        throw new Exception("Finner ikke transaksjonen '{$innbetalingsId}'.");
                    }

                    /** @var Innbetaling\Delbeløp $delbeløp */
                    $delbeløp = $this->app->hentModell(Innbetaling\Delbeløp::class, $delbeløpId);
                    if(!$delbeløp->hentId()) {
                        throw new Exception("Finner ikke delbeløpet '{$delbeløpId}'.");
                    }

                    /** @var Leieforhold\Krav|true|null $motkrav */
                    $motkrav = $delbeløp->hentKrav();

                    if(!$motkrav) {
                        throw new Exception("Delbeløpet '{$delbeløpId}' er ikke avstemt.");
                    }

                    $delbeløp->fjernAvstemming();
                    $innbetaling->samle();
                    break;

                case "kople":
                    $innbetalingsId = isset($_GET['innbetalingsid']) ? $_GET['innbetalingsid'] : null;
                    $leieforholdId = isset($_POST['leieforhold']) ? $_POST['leieforhold'] : null;
                    $verdier = isset($_POST['verdi']) ? (array)$_POST['verdi'] : [];

                    /** @var Innbetaling $innbetaling */
                    $innbetaling = $this->app->hentModell(Innbetaling::class, $innbetalingsId);
                    if(!$innbetaling->hentId()) {
                        throw new Exception("Finner ikke transaksjonen '{$innbetalingsId}'.");
                    }

                    /** @var Leieforhold $leieforhold|null */
                    $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);

                    foreach ($verdier as $kravId => $beløp) {
                            $beløp = str_replace(',', '.', $beløp);
                            if($kravId > 0) {
                                /** @var Leieforhold\Krav $krav */
                                $krav = $this->app->hentModell(Leieforhold\Krav::class, $kravId);
                            }
                            else {
                                /** @var Innbetaling $krav */
                                $krav = $this->app->hentModell(Innbetaling::class, $kravId);
                            }
                            $innbetaling->avstem($krav, $beløp, $leieforhold);
                    }
                    break;

                default:
                    $resultat->msg = "Ugyldig data-parameter i manipuleringsoppdraget.";
                    $resultat->success = false;
                    break;
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}