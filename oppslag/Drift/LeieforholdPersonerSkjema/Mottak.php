<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\LeieforholdPersonerSkjema;


use DateTimeImmutable;
use Exception;
use Kyegil\CoreModel\CoreModelException;
use Kyegil\Leiebasen\EventManager;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Egenskapsett;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Vedlikehold;
use stdClass;
use Throwable;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema):Mottak
    {
        /** @var stdClass $resultat */
        $resultat = (object)[
            'success' => true,
            'msg' => "Lagret."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'adressekort':
                    $netsProsessor = $this->app->hentValg('efaktura')
                    ? $this->app->hentNetsProsessor(): null;

                    $leieforholdId = $get['id'] ?? null;
                    $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);
                    $personer = $post['personer'] ?? [];
                    $leietakerEgenskaper = $post['leietaker-egenskaper'] ?? [];
                    foreach($personer as $adressekort) {
                        $personId = $adressekort['id'] ?? null;
                        $person = $this->app->hentModell(Person::class, $personId);
                        if(!$person->hentId()) {
                            throw new Feilmelding('Finner ikke adressekort ' . $personId);
                        }
                        $person->settErOrg(!empty($adressekort['er_org']));
                        if(!$person->erOrg && empty($adressekort['fornavn'])) {
                            throw new Feilmelding('Fornavn er påkrevet for ' . $personId);
                        }
                        if(empty($adressekort['etternavn'])) {
                            throw new Feilmelding('Etternavn er påkrevet for ' . $personId);
                        }
                        $person->settFornavn($adressekort['fornavn'] ?? null);
                        $person->settEtternavn($adressekort['etternavn']);

                        $fødselsdato = !empty($adressekort['fødselsdato'])
                            ? new DateTimeImmutable($adressekort['fødselsdato'])
                            :  null;
                        $person->settFødselsdato($fødselsdato);

                        $personnr = trim($adressekort['personnr'] ?? '');
                        if($person->erOrg) {
                            $person->settOrgNr($personnr);
                        }
                        else {
                            if($fødselsdato && strlen($personnr) == 11 && strpos($personnr, $fødselsdato->format('dmy')) === 0) {
                                $personnr = substr($personnr, 6);
                            }
                            $person->settPersonnr($personnr);
                        }

                        $person->settEpost($adressekort['epost'] ?? null);
                        $person->settMobil($adressekort['mobil'] ?? null);
                        $person->settTelefon($adressekort['telefon'] ?? null);

                        $person->settAdresse1($adressekort['adresse1'] ?? null);
                        $person->settAdresse2($adressekort['adresse2'] ?? null);
                        $person->settPostnr($adressekort['postnr'] ?? null);
                        $person->settPoststed($adressekort['poststed'] ?? null);
                        $land = $adressekort['land'] ?? '';
                        $person->settLand($land ?: 'Norge');
                        $this->settEavVerdier($person, $adressekort);

                        $this->settLeietakerEgenskaper($leieforhold, $leietakerEgenskaper);

                        if($netsProsessor) {
                            try {
                                $netsProsessor->hentEfakturaIdentifierFor($person);
                            } catch (Exception $e) {
                                $this->app->logger->debug($e->getMessage());
                                continue;
                            }
                        }

                        EventManager::getInstance()->triggerEvent(
                            Person::class, 'endret',
                            $person, ['ny' => false], $this->app
                        );
                    }
                    $leieforhold->settNavn()->settBeskrivelse();
                    $resultat->msg = 'Opplysningene er lagret';
                    $resultat->url = (string)$this->app->returi->get(1);
                    Vedlikehold::getInstance($this->app)->sjekkAdresseoppdateringer();
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Feilmelding $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        catch (Throwable $e) {
            $this->app->loggException($e);
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param Person $person
     * @return $this
     * @throws CoreModelException
     */
    private function settEavVerdier(Person $person, array $adressekort = []): Mottak
    {
        /** @var Egenskapsett $eavEgenskaper */
        $eavEgenskaper = $this->app->hentSamling(Egenskap::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $eavEgenskaper->leggTilEavVerdiJoin('felt_type', true);
        $eavEgenskaper->leggTilSortering('rekkefølge');
        $eavEgenskaper->leggTilFilter(['modell' => Person::class])->låsFiltre();

        $eavEgenskaper->forEach(function(Egenskap $egenskap) use($person, $adressekort) {
            $kode = $egenskap->kode;
            if(array_key_exists($kode, $adressekort)) {
                /** @noinspection PhpUnhandledExceptionInspection */
                $person->sett($kode, $adressekort[$kode]);
            }
        });

        return $this;
    }

    /**
     * @param Leieforhold $leieforhold
     * @param array $leietakerEgenskaper
     * @return $this
     * @throws CoreModelException
     */
    private function settLeietakerEgenskaper(Leieforhold $leieforhold, array $leietakerEgenskaper = [])
    {
        /** @var Egenskapsett $eavEgenskaper */
        $eavEgenskaper = $this->app->hentSamling(Egenskap::class);
        /** @noinspection PhpUnhandledExceptionInspection */
        $eavEgenskaper->leggTilEavVerdiJoin('felt_type', true);
        $eavEgenskaper->leggTilSortering('rekkefølge');
        $eavEgenskaper->leggTilFilter(['modell' => Leietaker::class])->låsFiltre();

        foreach($leieforhold->hentLeietakere() as $leietaker) {
            $eavEgenskaper->forEach(function(Egenskap $egenskap) use($leietaker, $leietakerEgenskaper) {
                $kode = $egenskap->kode;
                if(isset($leietakerEgenskaper[$leietaker->hentId()]) && array_key_exists($kode, $leietakerEgenskaper[$leietaker->hentId()])) {
                    /** @noinspection PhpUnhandledExceptionInspection */
                    $leietaker->sett($kode, $leietakerEgenskaper[$leietaker->hentId()][$kode]);
                }
            });
        }
        return $this;
    }
}