<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_kostnad_kort\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * DelteKostnaderKostnadKort Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class DelteKostnaderKostnadKort extends DriftExtJsAdaptor
{
    protected $oppdrag = [
        'oppgave' => DelteKostnaderKostnadKort\Oppdrag::class,
        'utskrift' => DelteKostnaderKostnadKort\Utskrift::class
    ];
    /** @var string */
    public $tittel = 'Delte Kostnader – Fordelt kostnad';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * DelteKostnaderTjenesteKort constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $kostnadId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['kostnad'] = $kostnadId ? $this->hentModell(\Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad::class, $kostnadId) : null;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML(): bool
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Tjeneste\Kostnad|null $kostnad */
        $kostnad = $this->hoveddata['kostnad'];
        if (!$kostnad || !$kostnad->hentId()) {
            return false;
        }
        $this->tittel = $kostnad->hentNavn();
        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     * @throws Exception
     */
    public function skript(): Index
    {
        /** @var Tjeneste\Kostnad|null $kostnad */
        $kostnad = $this->hoveddata['kostnad'];

        /** @var Index $skript */
        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'kostnad' => $kostnad,
                'tjenesteId' => $kostnad->hentId(),
                'tilbakeKnapp' => $this->returi->get(),
            ])
        ]);
        return $skript;
    }
}