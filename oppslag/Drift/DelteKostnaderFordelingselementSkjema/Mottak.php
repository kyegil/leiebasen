<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderFordelingselementSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Mottak extends AbstraktMottak
{
    /**
     * @param string|null $skjema
     * @return $this
     */
    public function taIMot($skjema = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($skjema) {
                default:
                    $elementId = $this->data['get']['id'] ?? null;
                    $fordelingsnøkkelId = $this->data['post']['fordelingsnøkkel'] ?? null;
                    $leieforholdId = $this->data['post']['leieforhold'] ?? null;
                    $leieobjektId = $this->data['post']['leieobjekt'] ?? null;

                    /** @var Fordelingsnøkkel $fordelingsnøkkel */
                    $fordelingsnøkkel = $this->app->hentModell(Fordelingsnøkkel::class, $fordelingsnøkkelId);
                    /** @var Leieforhold $leieforhold */
                    $leieforhold = $leieforholdId ? $this->app->hentModell(Leieforhold::class, $leieforholdId) : null;
                    /** @var Leieobjekt $leieobjekt */
                    $leieobjekt = $leieobjektId ? $this->app->hentModell(Leieobjekt::class, $leieobjektId) : null;

                    $fordelingsmåte = isset($this->data['post']['fordelingsmåte'])
                        && in_array($this->data['post']['fordelingsmåte'], ['Andeler', 'Prosentvis', 'Fastbeløp'])
                        ? $this->data['post']['fordelingsmåte']
                        : null
                    ;
                    $andeler = intval($this->data['post']['andeler'] ?? 0);
                    $prosentsats = floatval($this->data['post']['prosentsats'] ?? 0) / 100;
                    $fastbeløp = floatval($this->data['post']['fastbeløp'] ?? 0);

                    $følgerLeieobjekt = !isset($this->data['post']['følger_leieobjekt']) || $this->data['post']['følger_leieobjekt'];

                    $forklaring = $this->data['post']['forklaring'] ?? null;

                    if (!$fordelingsnøkkel->hentId()) {
                        throw new Exception('Ugyldig fordelingsnøkkel');
                    }
                    if (!$fordelingsmåte) {
                        throw new Exception('Ugyldig fordelingsmåte');
                    }
                    if ($fordelingsmåte == 'Andeler' && !$andeler) {
                        throw new Exception('Ugyldig andel');
                    }
                    if ($fordelingsmåte == 'Prosentsats' && !$prosentsats) {
                        throw new Exception('Ugyldig prosentsats');
                    }
                    if ($følgerLeieobjekt && !$leieobjekt->hentId()) {
                        throw new Exception('Ugyldig leieobjekt');
                    }
                    if (!$følgerLeieobjekt && !$leieforhold->hentId()) {
                        throw new Exception('Ugyldig leieforhold');
                    }

                    if ($elementId == '*') {
                        /** @var Fordelingsnøkkel\Element $element */
                        $element = $this->app->nyModell(Fordelingsnøkkel\Element::class, (object)[
                            'fordelingsnøkkel' => $fordelingsnøkkel,
                            'følgerLeieobjekt' => $følgerLeieobjekt,
                            'leieobjekt' => $leieobjekt ?: $leieforhold->leieobjekt,
                            'leieforhold' => $leieforhold,
                            'fordelingsmåte' => $fordelingsmåte,
                            'andeler' => $andeler,
                            'prosentsats' => $prosentsats,
                            'fastbeløp' => $fastbeløp,
                            'forklaring' => $forklaring,
                        ]);
                    }
                    else {
                        /** @var Fordelingsnøkkel\Element $element */
                        $element = $this->app->hentModell(Fordelingsnøkkel\Element::class, $elementId);
                        if (!$element->hentId()) {
                            throw new Exception('Ugyldig element-id');
                        }
                        $element
                            ->settFordelingsnøkkel($fordelingsnøkkel)
                            ->settFølgerLeieobjekt($følgerLeieobjekt)
                            ->settFordelingsmåte($fordelingsmåte)
                            ->settAndeler($andeler)
                            ->settProsentsats($prosentsats)
                            ->settFastbeløp($fastbeløp)
                            ->settForklaring($forklaring)
                        ;
                        if ($leieforhold) {
                            $element->settLeieforhold($leieforhold);
                        }
                        if ($leieobjekt) {
                            $element->settLeieobjekt($leieobjekt);
                        }
                    }

                    $resultat->id = $element->hentId();
                    $resultat->msg = 'Lagret';
                    $resultat->url = strval($this->app->returi->get());
                    break;
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

}