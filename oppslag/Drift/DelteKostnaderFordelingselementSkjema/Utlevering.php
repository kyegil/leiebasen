<?php
/**
 * Part of boligstiftelsen.svartlamon.org
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderFordelingselementSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjektsett;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\OmrådeSkjema
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData($data = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case 'leieobjekter':
                    $query = '';
                    if (isset($this->data['get']['query']) && trim($this->data['get']['query'])) {
                        $query = trim($this->data['get']['query']);
                    }
                    if (isset($this->data['get']['initial']) && $this->data['get']['initial']) {
                        $filter = ['leieobjektnr' => (int)$query,];
                    }
                    else {
                        $filter = ['or' => [
                            '`' . Leieobjekt::hentTabell() . '`.`leieobjektnr` LIKE' => $query . '%',
                            '`' . Leieobjekt::hentTabell() . '`.`navn` LIKE' => '%' . $query . '%',
                            '`' . Leieobjekt::hentTabell() . '`.`gateadresse` LIKE' => '%' . $query . '%',
                            '`' . Leieobjekt::hentTabell() . '`.`beskrivelse` LIKE' => '%' . $query . '%',
                        ]];
                    }
                    /** @var Leieobjektsett $leieobjektSett */
                    $leieobjektSett = $this->app->hentSamling(Leieobjekt::class);
                    $leieobjektSett->leggTilFilter($filter)
                        ->leggTilSortering(Leieobjekt::hentPrimærnøkkelfelt());

                    foreach ($leieobjektSett as $leieobjekt) {
                        $resultat->data[] = [
                            'value' => strval($leieobjekt->hentId()),
                            'text' => $leieobjekt->hentId() . ' | ' . $leieobjekt->hentBeskrivelse(),
                        ];
                    }
                    break;

                case 'leieforhold':
                    $query = '';
                    if (isset($this->data['get']['query']) && trim($this->data['get']['query'])) {
                        $query = trim($this->data['get']['query']);
                    }
                    if (isset($this->data['get']['initial']) && $this->data['get']['initial']) {
                        $filter = ['leieobjektnr' => (int)$query,];
                    }
                    else {
                        $filter = ['or' => [
                            '`' . Leieforhold::hentTabell() . '`.`leieforholdnr` LIKE' => $query . '%',
                        ]];
                    }
                    /** @var Leieforholdsett $leieforholdSett */
                    $leieforholdSett = $this->app->hentSamling(Leieforhold::class);
                    $leieforholdSett->leggTilFilter($filter)
                        ->leggTilSortering(Leieobjekt::hentPrimærnøkkelfelt());

                    foreach ($leieforholdSett as $leieforhold) {
                        $resultat->data[] = [
                            'value' => strval($leieforhold->hentId()),
                            'text' => $leieforhold->hentId() . ' | ' . $leieforhold->hentBeskrivelse(),
                        ];
                    }
                    break;

                default:
                    $adgangId = !empty($_GET['id']) ? $_GET['id'] : null;
                    if (!$adgangId) {
                        throw new Exception('id-parameter mangler');
                    }
                    /** @var Adgang $adgang */
                    $adgang = $this->app->hentModell(Adgang::class, $adgangId);
                    if ($adgang->hentId()) {
                        $resultat->data = (object) [
                            'id' => $adgang->hentId(),
                        ];
                    }

                    else {
                        throw new Exception('denne adgangen finnes ikke');
                    }
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}