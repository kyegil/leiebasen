<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Innbetaling;

class BetalingSkjema extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /**
     * Forbered hoveddata
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $betalingId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['transaksjon'] = null;
        $this->hoveddata['transaksjonId'] = $betalingId;
        $transaksjon = $this->hentModell(Innbetaling::class, $betalingId);
        if ($transaksjon->hentId()) {
            $this->hoveddata['transaksjon'] = $transaksjon;
            $this->hoveddata['transaksjonId'] = $transaksjon->hentId();
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var string $transaksjon */
        $transaksjonId = $this->hoveddata['transaksjonId'];
        /** @var Innbetaling|null $transaksjon */
        $transaksjon = $this->hoveddata['transaksjon'];
        if(!$transaksjon && $transaksjonId != '*') {
            return false;
        }
        $this->tittel = 'Transaksjon';
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Innbetaling|null $transaksjon */
        $transaksjon = $this->hoveddata['transaksjon'];

        return $this->vis(\Kyegil\Leiebasen\Visning\drift\html\betaling_skjema\Index::class, [
            'transaksjon' => $transaksjon,
            'transaksjonId' => $transaksjon ? $transaksjon->hentId() : '*',
        ]);
    }
}