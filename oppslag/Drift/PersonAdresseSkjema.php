<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

/**
 * PersonAdresseSkjema Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class PersonAdresseSkjema extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => PersonAdresseSkjema\Oppdrag::class
    ];

    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $personId = $_GET['id'] ?? null;
        /** @var Person $person */
        $person = $this->hentModell(Person::class, (int)$personId);
        if (!$person->hentId() || $personId == '*') {
            $person = null;
        }
        $this->hoveddata['person'] = $person;
        $this->hoveddata['personId'] = $personId;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = 'Personopplysninger';
        /** @var Leieforhold|null $person */
        $person = $this->hoveddata['person'];
        if(
            $person
            && !$person->hentId()
        ) {
            return false;
        }
        if($person) {
            $this->tittel .= ' for ' . $person->hentNavn();
        }

        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Person|null $person */
        $person = $this->hoveddata['person'];

        $visning = new ViewArray();

        $visning->addItem($this->vis(\Kyegil\Leiebasen\Visning\drift\html\PersonAdresseSkjema::class, [
            'person' => $person,
            'personId' => $person ? $person->hentId() : '*',
        ]));

        return $visning;
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function skript(): ViewInterface
    {
        /** @var Person|null $person */
        $person = $this->hoveddata['person'];

        $skript = new ViewArray();

        $skript->addItem($this->vis(\Kyegil\Leiebasen\Visning\drift\js\PersonAdresseSkjema::class, [
            'person' => $person,
            'personId' => $person ? $person->hentId() : '*',
        ]));

        return $skript;
    }
}