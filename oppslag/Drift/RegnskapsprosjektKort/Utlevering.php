<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\RegnskapsprosjektKort;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav\Delkrav;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav\Delkravsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Regnskapsprosjekt;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Respons\CsvRespons;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Visning\felles\html\shared\Utskrift;
use stdClass;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\RegnskapsprosjektKort
 */
class Utlevering extends AbstraktUtlevering
{
    /** @var Regnskapsprosjekt|null */
    protected $prosjekt;

    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null) {
        $prosjektId = $this->data['get']['id'] ?? null;
        $this->prosjekt = $this->app->hentModell(Regnskapsprosjekt::class, $prosjektId);

            switch ($data) {
                case 'utskrift':
                    return $this->utskrift();

                case "rapport_csv":
                    return $this->hentCsvRapport();

                case "rapport_json":
                    return $this->hentJsonRapport();

                default:
                    $resultat = (object)[
                        'success' => false,
                        'msg' => 'Rapportformat mangler',
                        'data' => []
                    ];
                    $this->settRespons(new JsonRespons($resultat));
            }
        return $this;
    }

    /**
     * @return $this
     */
    protected function utskrift()
    {
        $fom = !empty($this->data['get']['fom']) ? new DateTime($this->data['get']['fom']): null;
        $tom = !empty($this->data['get']['tom']) ? new DateTime($this->data['get']['tom']): null;
        $segmentertData = [];
        try {
            if (!$this->prosjekt->hentId()) {
                throw new Exception('Prosjektregnskapet finnes ikke');
            }
            if (!$fom || !$tom) {
                throw new Exception('Både fra- og til-dato må angis');
            }
            if($tom->diff($fom)->y) {
                throw new Exception('Du kan ikke hente data for mer enn ett år i gangen.');
            }
            /** @var Kravsett|Delkravsett $resultatSamling */
            $resultatSamling = $this->prosjekt->hentRegnskapsdata((object)['fom' => $fom, 'tom' => $tom]);

            /** @var Krav|Delkrav $resultatObjekt */
            foreach($resultatSamling as $resultatObjekt) {
                $segmentVerdi = $this->prosjekt->hentSegmentVerdi($resultatObjekt);
                $krav = $resultatObjekt instanceof Krav ? $resultatObjekt : $resultatObjekt->krav;
                /** @var Delkrav|null $delkrav */
                $delkrav = $resultatObjekt instanceof Delkrav ? $resultatObjekt : null;
                $dato = $krav->kravdato;
                $data = (object)[
                    'dato' => $dato->format('Y-m-d'),
                    'beskrivelse' => null,
                    'beløp' => null,
                    'krav' => $krav->id,
                    'leieforhold' => $krav->leieforhold->hentId()
                ];

                if ($delkrav) {
                    $data->beskrivelse = $delkrav->delkravtype->hentNavn() . ' (del av ' . $krav->tekst . ')';
                    $data->beløp = $delkrav->beløp;
                }
                else {
                    $data->beskrivelse = $krav->tekst;
                    $data->beløp = $krav->beløp;
                }

                if (!isset($segmentertData[$segmentVerdi])) {
                    $segmentertData[$segmentVerdi] = [
                        (object)array_combine(array_keys((array)$data), array_keys((array)$data))
                    ];
                }

                $segmentertData[$segmentVerdi][] = $data;
            }

            /** @var HtmlElement $segmenter */
            $segmenter = [];

            foreach($segmentertData as $segmentVerdi => $segmentData) {
                $segmenter[] = $this->hentUtskriftSegment($segmentData, $segmentVerdi);
            }

            $førsteSegment = reset($segmentertData);
            $førsteLinje = (array)reset($førsteSegment);
            $kolonneOverskrifter = array_map(function($linjeData){
                return new HtmlElement('th', [], ucwords(str_replace('_', ' ', $linjeData)));
            }, $førsteLinje);

            $thead = new HtmlElement('thead', [],
                new HtmlElement('tr', [], $kolonneOverskrifter)
            );

            $tbody = new HtmlElement('tbody', [], $segmenter);

            $table = new HtmlElement('table', [], [
                $thead,
                $tbody
            ]);

            $resultat = $this->app->vis(Utskrift::class, [
                'tittel' => $this->prosjekt->hentNavn(),
                'innhold' => $table
            ]);
        }
        catch (Exception $e) {
            $resultat = $e->getMessage();
        }
        $this->settRespons(new Respons($resultat));
        return $this;
    }

    /**
     * @return $this
     */
    protected function hentCsvRapport(): Utlevering
    {
        $fom = !empty($this->data['get']['fom']) ? new DateTime($this->data['get']['fom']) : null;
        $tom = !empty($this->data['get']['tom']) ? new DateTime($this->data['get']['tom']) : null;
        $resultat = [];
        try {
            if (!$this->prosjekt->hentId()) {
                throw new Exception('Prosjektregnskapet finnes ikke');
            }
            if (!$fom || !$tom) {
                throw new Exception('Både fra- og til-dato må angis');
            }
            if ($tom->diff($fom)->y) {
                throw new Exception('Du kan ikke hente data for mer enn ett år i gangen.');
            }
            $resultatSamling = $this->prosjekt->hentRegnskapsdata((object)['fom' => $fom, 'tom' => $tom]);

            /** @var Krav|Delkrav $resultatObjekt */
            foreach ($resultatSamling as $resultatObjekt) {
                $krav = $resultatObjekt instanceof Krav ? $resultatObjekt : $resultatObjekt->krav;
                /** @var Delkrav|null $delkrav */
                $delkrav = $resultatObjekt instanceof Delkrav ? $resultatObjekt : null;
                $dato = $krav->kravdato;
                $leieobjekt = $krav->leieforhold->leieobjekt;
                $bygning = $krav->leieforhold->leieobjekt->bygning;
                $data = (object)[
                    'segment' => $this->prosjekt->hentSegmentVerdi($resultatObjekt),
                    'dato' => $dato->format('Y-m-d'),
                    'beskrivelse' => null,
                    'beløp' => null,
                    'krav' => $krav->id,
                    'delkrav' => null,
                    'leieforhold' => $krav->leieforhold->hentId(),
                    'leieforhold_beskrivelse' => $krav->leieforhold->hentBeskrivelse(),
                    'leieobjekt' => $leieobjekt ? $leieobjekt->hentId() : null,
                    'leieobjekt_beskrivelse' => $leieobjekt ? $leieobjekt->hentBeskrivelse() : '',
                    'bygning' => $bygning ? $bygning->hentId() : null,
                    'bygning_beskrivelse' => $bygning ? $bygning->hentNavn() : '',
                    'kravtype' => $krav->type,
                    'strømanlegg' => $krav->anlegg ? $krav->anlegg->hentId() : null
                ];

                if ($delkrav) {
                    $data->delkrav = $delkrav->id;
                    $data->beskrivelse = $delkrav->delkravtype->hentNavn() . ' (del av ' . $krav->tekst . ')';
                    $data->beløp = $delkrav->beløp;
                } else {
                    $data->delkrav = null;
                    $data->beskrivelse = $krav->tekst;
                    $data->beløp = $krav->beløp;
                }

                if (!$resultat) {
                    $resultat[] = array_keys((array)$data);
                }
                $resultat[] = $data;
            }

        } catch (Exception $e) {
            $resultat = $e->getMessage();
            $this->settRespons(new Respons($resultat));
        }
        $csvRespons = new CsvRespons($resultat);
        $csvRespons->filnavn = $this->prosjekt->kode . '.csv';
        $this->settRespons($csvRespons);
        return $this;
    }

    /**
     * @return Utlevering
     */
    protected function hentJsonRapport(): Utlevering
    {
        $fom = !empty($this->data['get']['fom']) ? new DateTime($this->data['get']['fom']) : null;
        $tom = !empty($this->data['get']['tom']) ? new DateTime($this->data['get']['tom']) : null;
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            if (!$this->prosjekt->hentId()) {
                throw new Exception('Prosjektregnskapet finnes ikke');
            }
            if (!$fom || !$tom) {
                throw new Exception('Både fra- og til-dato må angis');
            }
            if ($tom->diff($fom)->y) {
                throw new Exception('Du kan ikke hente data for mer enn ett år i gangen.');
            }
            $resultatSamling = $this->prosjekt->hentRegnskapsdata((object)['fom' => $fom, 'tom' => $tom]);

            /** @var Krav|Delkrav $resultatObjekt */
            foreach ($resultatSamling as $resultatObjekt) {
                $krav = $resultatObjekt instanceof Krav ? $resultatObjekt : $resultatObjekt->krav;
                /** @var Delkrav|null $delkrav */
                $delkrav = $resultatObjekt instanceof Delkrav ? $resultatObjekt : null;
                $dato = $krav->kravdato;

                $leieobjekt = $krav->leieforhold->leieobjekt;
                $bygning = $krav->leieforhold->leieobjekt->bygning;
                $data = (object)[
                    'krav' => $krav->id,
                    'dato' => $dato->format('Y-m-d'),
                    'måned' => $dato->format('Y-m'),
                    'måned_formatert' => \IntlDateFormatter::formatObject($dato, 'MMMM y', 'nb_NO'),
                    'uke' => $dato->format('W'),
                    'uke_formatert' => 'Uke ' . \IntlDateFormatter::formatObject($dato, 'w yyyy', 'nb_NO'),
                    'leieforhold' => $krav->leieforhold->hentId(),
                    'leieforhold_beskrivelse' => "{$krav->leieforhold->hentId()}: {$krav->leieforhold->hentBeskrivelse()}",
                    'leieobjekt' => $leieobjekt ? $leieobjekt->hentId() : null,
                    'leieobjekt_beskrivelse' => $leieobjekt ? $leieobjekt->hentBeskrivelse() : '',
                    'bygning' => $bygning ? $bygning->hentId() : null,
                    'bygning_beskrivelse' => $bygning ? $bygning->hentNavn() : '',
                    'kravtype' => $krav->type,
                    'strømanlegg' => $krav->anlegg ? $krav->anlegg->hentId() : null,
                    'segment' => $this->prosjekt->hentSegmentVerdi($resultatObjekt)
                ];

                if ($delkrav) {
                    $data->delkrav = $delkrav->id;
                    $data->beskrivelse = $delkrav->delkravtype->hentNavn() . ' (del av ' . $krav->tekst . ')';
                    $data->beløp = $delkrav->beløp;
                } else {
                    $data->delkrav = null;
                    $data->beskrivelse = $krav->tekst;
                    $data->beløp = $krav->beløp;
                }

                $resultat->data[] = $data;
            }

        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param stdClass[] $segmentData
     * @param string $segment
     * @return HtmlElement|null Html <tbody> tag with data
     */
    protected function hentUtskriftSegment(array $segmentData, string $segment): ?HtmlElement
    {
        /** @var HtmlElement[] $tabellSegmentLinjer */
        $tabellSegmentLinjer = [];
        $segmentBeløp = 0;
        if ($segmentData) {
            $førsteSegmentLinje = (array)array_shift($segmentData);
            $antallKolonner = count($førsteSegmentLinje);

            $segmentOverskrift = new HtmlElement('tr', [], new HtmlElement('th', ['colspan' => $antallKolonner], $segment));

            /** @var HtmlElement[] $segmentKolonneOverskrifter */
            $segmentKolonneOverskrifter = array_map(function($linjeData){
                return new HtmlElement('th', [], ucwords(str_replace('_', ' ', $linjeData)));
            }, $førsteSegmentLinje);

            $tabellSegmentLinjer[] = $segmentOverskrift;
//            $tabellSegmentLinjer[] = new HtmlElement('tr', [], $segmentKolonneOverskrifter);

            foreach($segmentData as $verdiLinje) {
                $tabellLinje = $this->hentUtskriftLinje($verdiLinje);
                $segmentBeløp += $verdiLinje->beløp;
                $tabellSegmentLinjer[] = new HtmlElement('tr', [], $tabellLinje);
            }

            $bunnlinje = new HtmlElement('tr', [], [
                new HtmlElement('th', ['colspan' => $antallKolonner], "Sum {$segment}: {$this->app->kr($segmentBeløp)}")
            ]);
            $tabellSegmentLinjer[] = $bunnlinje;
            return new HtmlElement('tbody', ['id' => $segment], $tabellSegmentLinjer);
        }
        return null;
    }

    /**
     * @param stdClass $verdiLinje
     * @return HtmlElement Html <tr> tag med data
     */
    protected function hentUtskriftLinje(stdClass $verdiLinje): HtmlElement
    {
        /** @var HtmlElement $tabellLinje */
        $tabellLinje = [];
        foreach($verdiLinje as $felt => $verdi) {
            if ($felt == 'beløp') {
                $tabellLinje[] = new HtmlElement('td', ['class' => 'value'], $this->app->kr($verdi));
            }
            else if (in_array($felt, ['krav', 'leieforhold'])) {
                $tabellLinje[] = new HtmlElement('td', ['class' => 'value'], $verdi);
            }
            else {
                $tabellLinje[] = new HtmlElement('td', [], $verdi);
            }
        }
        return new HtmlElement('tr', [], $tabellLinje);
    }
}