<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\drift\js\strømavstemming_skjema\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * StrømavstemmingSkjema Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class StrømavstemmingSkjema extends \Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Avstemming av faste fellesstrøm-krav';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * @return Index
     */
    public function skript() {
        /** @var Person|null $bruker */
        $bruker = $this->hoveddata['bruker'];
        if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }

        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'bruker' => $bruker,
                'tilbakeKnapp' => $this->returi->get()
            ])
        ]);
        return $skript;
    }
}