<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\PoengListe;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Poengprogram;
use Kyegil\Leiebasen\Modell\Poengprogram\Poeng;
use Kyegil\Leiebasen\Modell\Poengprogram\Poengsett;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\Leiebasen\Respons\JsonRespons;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\PoengListe
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     * @throws Exception
     * @link https://datatables.net/manual/server-side
     */
    public function hentData($data = null): Utlevering
    {
        $get = $this->data['get'];
        $post = $this->data['post'];

        switch ($data) {
            case 'poeng': {
                $resultat = (object)[
                    'success' => true,
                    'draw' => intval($get['draw'] ?? 0),
                    'data'		=> [],
                    'totalRows' => 0,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                ];

                $programKode = $_GET['kode'] ?? null;
                $leieforholdId = !empty($_GET['id']) ? $_GET['id'] : null;
                $program = $this->app->hentPoengbestyrer()->hentProgram($programKode);
                /** @var Leieforhold|null $leieforhold */
                $leieforhold = $leieforholdId
                    ? $this->app->hentModell(Leieforhold::class, $leieforholdId)
                    : null;

                $sorteringsfelt	= $get['sort'] ?? null;
                $synkende = isset($get['dir']) && $get['dir'] == "DESC";
                if ($sorteringsfelt) {
                    $sortering = [[
                        'columnName' => $sorteringsfelt,
                        'dir' => $synkende ? 'desc' : 'asc'
                    ]];
                }
                else {
                    $sortering  = $get['order'] ?? [];
                }

                $start		= intval($get['start'] ?? 0);
                $limit		= $get['limit'] ?? 25;

                $søkefelt = $get['søkefelt'] ?? ($get['search']['value'] ?? '');

                try {
                    $poengsett = $this->hentPoengsett(
                        $program,
                        $leieforhold,
                        $søkefelt,
                        $sortering, $start, $limit
                    );

                    $resultat->recordsFiltered = $resultat->recordsTotal = $resultat->totalRows
                        = $poengsett->hentAntall();

                    foreach ($poengsett as $poeng) {
                        $resultat->data[] = $this->hentLinjeData($poeng);
                    }
                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->error = $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }
            default:
                $resultat = (object)[
                    'success' => false,
                    'msg' => 'Ønsket data er ikke oppgitt',
                    'data' => []
                ];
                $this->settRespons(new JsonRespons($resultat));
                return $this;
        }
    }

    /**
     * @param Poengprogram $program
     * @param Leieforhold|null $leieforhold
     * @param string $søk
     * @param array $sortering
     * @param int $start
     * @param int $limit
     * @return Poengsett
     * @throws Exception
     */
    public function hentPoengsett(Poengprogram $program, ?Leieforhold $leieforhold, string $søk, array $sortering, int $start, int $limit): Poengsett
    {
        list('program' => $program, 'leieforhold' => $leieforhold, 'søk' => $søk, 'sortering' => $sortering, 'start' => $start, 'limit' => $limit )
            = $this->app->before($this, __FUNCTION__, [
            'program' => $program, 'leieforhold' => $leieforhold, 'søk' => $søk, 'sortering' => $sortering, 'start' => $start, 'limit' => $limit
        ]);
        $filter = array(
            'or' => array(
                '`' . Poeng::hentTabell() . '`.`type` LIKE' => '%' . $søk . '%',
                '`' . Poeng::hentTabell() . '`.`tekst` LIKE' => '%' . $søk . '%',
                '`' . Poeng::hentTabell() . '`.`leieforhold_id`' => $søk,
            )
        );

        /**
         * Mapping av sorteringsfelter
         * * kolonnenavn = > [tabellkilde el null for hovedtabell => tabellfelt]
         */
        $sorteringsfelter = [
            'id'    => [null, 'id'],
            'tid'   => [null, 'tid'],
            'type'  => [null, 'type'],
            'verdi' =>  [null, 'verdi'],
            'tekst' => [null, 'tekst'],
            'leieforhold' => ['leieforhold', 'leieforholdnr'],
        ];

        $poengsett = $program->hentPoengsett();
        if($leieforhold) {
            $poengsett->leggTilFilter(['leieforhold_id' => $leieforhold->hentId()])->låsFiltre();
        }
        $poengsett->leggTilFilter($filter);
        $poengsett->låsFiltre();

        foreach ($sortering as $sorteringskonfig) {
            if(isset($sorteringsfelter[$sorteringskonfig['columnName']])) {
                $poengsett->leggTilSortering(
                    $sorteringsfelter[$sorteringskonfig['columnName']][1],
                    strtolower($sorteringskonfig['dir']) == 'desc',
                    $sorteringsfelter[$sorteringskonfig['columnName']][0]
                );
            }
        }

        if (isset($start)) {
            $poengsett->settStart($start);
        }
        if (isset($limit)) {
            $poengsett->begrens($limit);
        }
        return $this->app->after($this, __FUNCTION__, $poengsett);
    }

    /**
     * @param Poeng $poeng
     * @return object
     * @throws Exception
     */
    private function hentLinjeData(Poeng $poeng): object
    {
        list('poeng' => $poeng)
            = $this->app->before($this, __FUNCTION__, [
            'poeng' => $poeng
        ]);
        $data = (object)[
            'id' => $poeng->hentId(),
            'tid' => $poeng->hentTid()->format('Y-m-d'),
            'tid_formatert' => $poeng->hentTid()->format('d.m.Y'),
            'type' => $poeng->hentType(),
            'tekst' => $poeng->hentTekst(),
            'verdi' => $poeng->hentVerdi(),
            'leieforhold_id' => $poeng->hentLeieforhold()->hentId(),
            'leieforhold' => DriftKontroller::lenkeTilLeieforholdKort($poeng->hentLeieforhold(),$poeng->hentLeieforhold()->hentId()) . ': ' . $poeng->hentLeieforhold()->hentNavn(),
        ];
        return $this->app->after($this, __FUNCTION__, $data);
    }
}