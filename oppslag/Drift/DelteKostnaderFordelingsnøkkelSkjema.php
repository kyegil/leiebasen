<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_fordelingsnøkkel_skjema\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * DelteKostnaderFordelingsnøkkelSkjema Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class DelteKostnaderFordelingsnøkkelSkjema extends DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Fordelingsnøkkel';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';


    /**
     * AdgangSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);

        $tjenesteId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['tjeneste'] = null;
        $this->hoveddata['fordelingsnøkkel'] = null;

        $filter = [
            'or' => [
                ['id' => $tjenesteId],
            ]
        ];
        /** @var Tjeneste|null $tjeneste */
        $tjeneste = $this->hentSamling(Tjeneste::class)
            ->leggTilFilter($filter)->hentFørste();

        if ($tjeneste) {
            if ($tjeneste->hentId()) {
                $this->hoveddata['tjeneste'] = $tjeneste;
                $this->hoveddata['fordelingsnøkkel'] = $tjeneste->hentFordelingsnøkkel();
            }
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML(): bool
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Tjeneste|string|null $tjeneste */
        $tjeneste = $this->hoveddata['tjeneste'];
        if (!$tjeneste || !$tjeneste->hentId()) {
            return false;
        }

        return true;
    }

    /**
     * @return Index
     * @throws Exception
     */
    public function skript(): Index
    {
        /** @var Tjeneste|string|null $tjeneste */
        $tjeneste = $this->hoveddata['tjeneste'];

        /** @var Index $skript */
        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'tjeneste' => ($tjeneste instanceof Tjeneste) && $tjeneste->hentId() ? $tjeneste : null,
                'tjenesteId' => $tjeneste && $tjeneste->hentId() ? $tjeneste->hentId() : '*',
            ])
        ]);
        return $skript;
    }
}