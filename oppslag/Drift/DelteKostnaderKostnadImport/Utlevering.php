<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderKostnadImport;


use Exception;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\CsvRespons;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\Kravimport
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null): Utlevering
    {
        switch ($data) {
            case 'eksempelfil':
                $resultat = [Mottak::LOVLIGE_FELTER];
                $respons = new CsvRespons($resultat);
                $respons->filnavn = 'kostnadimport.csv';
                $this->settRespons($respons);
                return $this;
            default:
                throw new Exception('Ønsket data er ikke oppgitt');
        }
    }
}