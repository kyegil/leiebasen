<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderKostnadImport;


use DateTimeImmutable;
use Exception;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;

class Mottak extends AbstraktMottak
{
    const LOVLIGE_FELTER = [
        'tjeneste_id', 'fakturanummer', 'fakturabeløp',
        'fradato', 'tildato', 'termin', 'forbruk'
    ];

    const PÅKREVDE_FELTER = ['fakturanummer', 'fakturabeløp', 'fradato', 'tildato'];

    private static $tidligstMuligeKravdato;

    private static $kravtyper;

    /**
     * @param string|null $skjema
     * @return $this
     */
    public function taIMot($skjema = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($skjema) {
                default:
                    $importfil = isset($this->data['files']['importfil'][0]['tmp_name']) && $this->data['files']['importfil'][0]['tmp_name'] ? $this->data['files']['importfil'][0] : null;
                    if (!$importfil) {
                        throw new Exception('Importfila mangler');
                    }
                    if($importfil['error'] != 0) {
                        throw new Exception('Det oppstod en feil ved opplasting. Feilkode ' . $importfil['error']);
                    }
                    $tmpName = $importfil['tmp_name'];

                    if ($importfil['type'] != 'text/csv') {
                        throw new Exception('Ugyldig filtype. Last opp i gyldig csv-format');
                    }
                    ini_set('auto_detect_line_endings',TRUE);
                    $handle = fopen($tmpName,'r');
                    if($handle === false) {
                        throw new Exception('Klarte ikke lese opplastet fil');
                    }
                    $antallFelter = null;
                    $kolonner = [];
                    $importObjekter = [];
                    while (($rad = fgetcsv($handle)) !== FALSE) {
                        if(!isset($antallFelter)) {
                            $kolonner = $rad;
                            $antallFelter = count($kolonner);
                            $this->validerKolonner($kolonner);
                        }
                        else {
                            if(count($rad) != $antallFelter) {
                                throw new Exception('Fila har ikke gjennomgående like mange felter i hver linje');
                            }
                            $importObjekter[] = (object)array_combine($kolonner, $rad);
                        }

                    }
                    fclose($handle);

                    foreach($importObjekter as $linje => $importObjekt) {
                        $this->validerImportObjekt($importObjekt, $linje);
                    }
                    $resultat->msg = 'Fila er gyldig og kan importeres';
                    if ($skjema == 'import') {
                        $resultat->msg = 'Kostnadene er importert';
                        $resultat->url = '/drift/index.php?oppslag=delte_kostnader_kostnad_liste';
                        $this->opprettKostnader($importObjekter);
                    }
                    break;
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param string[] $kolonner
     * @return Mottak
     * @throws Exception
     */
    private function validerKolonner(array $kolonner): Mottak
    {
        $manglendeFelter = array_diff(self::PÅKREVDE_FELTER, $kolonner );
        $ulovligeFelter = array_diff($kolonner, self::LOVLIGE_FELTER);

        if (!count($kolonner)) {
            throw new Exception('Fila inneholder ingen felter');
        }
        if (count($manglendeFelter)) {
            throw new Exception('Følgende påkrevde felter mangler: ' . implode(', ', $manglendeFelter));
        }
        if (count($ulovligeFelter)) {
            throw new Exception('Følgende ulovlige felter ble funnet: ' . implode(', ', $ulovligeFelter));
        }
        return $this;
    }

    /**
     * @param stdClass $importObjekt
     * @param int $linje
     * @return Mottak
     * @throws Exception
     */
    public function validerImportObjekt(stdClass $importObjekt, int $linje): Mottak
    {
        $importObjekt->tjeneste = null;
        $importObjekt->forbruksenhet = null;
        if (isset($importObjekt->tjeneste_id) && $importObjekt->tjeneste_id) {
            $importObjekt->tjeneste = $this->app->hentModell(Tjeneste::class, $importObjekt->tjeneste_id);
            $importObjekt->forbruksenhet = $importObjekt->tjeneste->forbruksenhet;
        }
        unset ($importObjekt->tjeneste_id);

        $importObjekt->fradato = isset($importObjekt->fradato) && !empty($importObjekt->fradato) ? new DateTimeImmutable($importObjekt->fradato) : null;
        $importObjekt->tildato = isset($importObjekt->tildato) && !empty($importObjekt->tildato) ? new DateTimeImmutable($importObjekt->tildato) : null;

        try {
            $this->validerDatoer($importObjekt);
            $this->validerBeløp($importObjekt);
            $this->validerTjeneste($importObjekt);
        } catch (Exception $e) {
            throw new Exception('Problem i linje ' . ($linje + 2) . ': ' . $e->getMessage());
        }
        return $this;
    }

    /**
     * @param stdClass $importObjekt
     * @return Mottak
     * @throws Exception
     */
    public function validerDatoer(stdClass $importObjekt): Mottak
    {
        if ($importObjekt->fradato && $importObjekt->tildato) {
            if ($importObjekt->fradato > $importObjekt->tildato) {
                throw new Exception('Fra-dato kan ikke være større enn til-dato.');
            }
        }
        if ($importObjekt->fradato) {
            if ($importObjekt->fradato->format('Y') < (date('Y') - 5)) {
                throw new Exception('Du kan ikke importere kostnader fra over 5 år tilbake i tid.');
            }
            if ($importObjekt->fradato > date_create_immutable()) {
                throw new Exception('Du kan ikke importere framtidige kostnader.');
            }
        }
        if ($importObjekt->tildato) {
            if ($importObjekt->tildato->format('Y') > (date('Y') + 5)) {
                throw new Exception('Du kan ikke importere kostnader så langt fram i tid.');
            }
        }
        return $this;
    }

    /**
     * @param stdClass $importObjekt
     * @return Mottak
     * @throws Exception
     */
    public function validerBeløp(stdClass $importObjekt): Mottak
    {
        if (isset($importObjekt->fakturabeløp) && !empty($importObjekt->fakturabeløp)) {
            if(
                !is_numeric($importObjekt->fakturabeløp)
                || floor($importObjekt->fakturabeløp) > 100000
            ) {
                throw new Exception('Ugyldig beløp');
            }
        }
        return $this;
    }

    /**
     * @param stdClass $importObjekt
     * @return Mottak
     * @throws Exception
     */
    public function validerTjeneste(stdClass $importObjekt): Mottak
    {
        if ($importObjekt->tjeneste) {
            if(!$importObjekt->tjeneste->hentId()) {
                throw new Exception('Ugyldig tjeneste-id');
            }
        }
        return $this;
    }

    /**
     * @param stdClass[] $importObjekter
     * @throws Exception
     */
    private function opprettKostnader(array $importObjekter)
    {
        foreach ($importObjekter as $importObjekt) {
            /** @var Kostnad $kostnad */
            $kostnad = $this->app->nyModell(Kostnad::class, $importObjekt);
            $tjeneste = $kostnad->tjeneste;
            if ($tjeneste
                && $tjeneste->hentFordelingsnøkkel()
                && !$tjeneste->hentFordelingsnøkkel()->hentBeløpelementer()->hentAntall()
                && $kostnad->hentFakturabeløp()
                && $kostnad->hentFradato()
                && $kostnad->hentTildato()
                && $kostnad->hentFradato() <= $kostnad->hentTildato()
            ) {
                $tjeneste->hentFordelingsnøkkel()->fordel($kostnad);
            }
        }
    }
}