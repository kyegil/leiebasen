<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\Drift\LeiereguleringListe\Utlevering;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\drift\html\leieregulering_liste\Index;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

/**
 * LeiereguleringListe Controller
 *
 * @method Utlevering hentUtlevering()
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class LeiereguleringListe extends DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => LeiereguleringListe\Oppdrag::class,
    ];
    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = "Forslag til leieregulering";
        return true;
    }

    /**
     * @return Visning
     */
    public function innhold()
    {
        return $this->vis(Index::class);
    }

    /**
     * @return Visning
     */
    public function skript(): ViewInterface
    {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];

        $skript = new ViewArray([
            $this->vis(\Kyegil\Leiebasen\Visning\drift\js\leieregulering_liste\Index::class),
        ]);
        return $skript;
    }
}