<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;

/**
 * TilpassetEavFeltSkjema Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class TilpassetEavFeltSkjema extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /**
     * Forbered hoveddata
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $eavEgenskapId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['eavEgenskap'] = null;
        $this->hoveddata['eavEgenskapId'] = $eavEgenskapId;
        $eavEgenskap = $this->hentModell(Egenskap::class, $eavEgenskapId);
        if ($eavEgenskap->hentId()) {
            $this->hoveddata['eavEgenskap'] = $eavEgenskap;
            $this->hoveddata['eavEgenskapId'] = $eavEgenskap->hentId();
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var int|string $eavEgenskapId */
        $eavEgenskapId = $this->hoveddata['eavEgenskapId'];
        /** @var Egenskap|null $eavEgenskap */
        $eavEgenskap = $this->hoveddata['eavEgenskap'];
        if(!$eavEgenskap && $eavEgenskapId != '*') {
            return false;
        }
        $this->tittel = $eavEgenskap ? $eavEgenskap->hentBeskrivelse() : 'Spesialtilpasset felt';
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Egenskap|null $eavEgenskap */
        $eavEgenskap = $this->hoveddata['eavEgenskap'];

        return $this->vis(\Kyegil\Leiebasen\Visning\drift\html\tilpasset_eav_felt_skjema\Index::class, [
            'eavEgenskap' => $eavEgenskap,
            'eavEgenskapId' => $eavEgenskap ? $eavEgenskap->hentId() : '*',
        ]);
    }
}