<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 30/01/2023
 * Time: 17:10
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\TilpassetEavFeltSkjema;


use Exception;
use Kyegil\CoreModel\Interfaces\CoreModelInterface;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var \stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => ''
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = isset($get['skjema']) ? $get['skjema'] : '';
        try {
            switch ($skjema) {
                case 'feltskjema':
                    $id = $get['id'] ?? null;
                    $modell = $post['modell'] ?? null;
                    $type = $post['type'] ?? null;
                    $kode  = $post['kode'] ?? null;
                    $beskrivelse = $post['beskrivelse'] ?? null;
                    $rekkefølge = intval($post['rekkefølge'] ?? 0) ?: null;
                    $konfigurering = isset($post['konfigurering']) && trim($post['konfigurering'])
                        ? trim($post['konfigurering'])
                        : null;
                    $feltType = $post['felt_type']['type']
                        ? [
                            'type' => $post['felt_type']['type'],
                            'oppsett' => isset($post['felt_type']['oppsett']) && trim($post['felt_type']['oppsett'])
                                ? $post['felt_type']['oppsett']
                                : null,
                        ]
                        : null;
                    $visning = $post['visning']['type']
                        ? [
                            'type' => $post['visning']['type'],
                            'oppsett' => isset($post['visning']['oppsett']) && trim($post['visning']['oppsett'])
                                ? $post['visning']['oppsett']
                                : null,
                        ]
                        : null;

                    $eksisterende = $this->app->hentSamling(Egenskap::class)
                        ->leggTilFilter(['modell' => $modell])
                        ->leggTilFilter(['kode' => $kode])
                        ->leggTilFilter(['id !=' => $id])
                        ->låsFiltre();
                    if($eksisterende->hentAntall()) {
                        throw new Exception('Koden er allerede i bruk for denne modellen');
                    }

                    if(!in_array($type,['string', 'int', 'boolean', 'date', 'time', 'datetime', 'interval', 'json'])) {
                        throw new Exception('Ugyldig type');
                    }

                    if($konfigurering) {
                        if(json_decode($konfigurering) === null) {
                            throw new Exception('Ugyldig konfigurering (JSON)');
                        }
                        $konfigurering = json_decode($konfigurering);
                        if(is_object($konfigurering) && !(array)$konfigurering) {
                            $konfigurering = null;
                        }
                    }
                    else {
                        $konfigurering = null;
                    }

                    if($feltType && isset($feltType['oppsett'])) {
                        if(json_decode($feltType['oppsett']) === null) {
                            throw new Exception('Ugyldig felttype-konfigurering (JSON)');
                        }
                        $feltType['oppsett'] = json_decode($feltType['oppsett']);
                        if(is_object($feltType['oppsett']) && !(array)$feltType['oppsett']) {
                            $feltType['oppsett'] = null;
                        }
                    }
                    else {
                        $feltType['oppsett'] = null;
                    }

                    if($visning && isset($visning['oppsett'])) {
                        if(json_decode($visning['oppsett']) === null) {
                            throw new Exception('Ugyldig visning-konfigurering (JSON)');
                        }
                        $visning['oppsett'] = json_decode($visning['oppsett']);
                        if(is_object($visning['oppsett']) && !(array)$visning['oppsett']) {
                            $visning['oppsett'] = null;
                        }
                    }
                    else {
                        $visning['oppsett'] = null;
                    }

                    if($id == '*') {
                        if(!is_a($modell, CoreModelInterface::class, true)) {
                            throw new Exception('Ugyldig modell');
                        }

                        /** @var Egenskap $eavEgenskap */
                        $eavEgenskap = $this->app->nyModell(Egenskap::class, (object)[
                            'modell' => $modell,
                            'kode' => $kode,
                            'type' => $type,
                            'beskrivelse' => $beskrivelse,
                            'rekkefølge' => $rekkefølge,
                            'konfigurering' => $konfigurering,
                            'er_egendefinert' => true,
                        ]);

                        $eavEgenskap->settFeltType((object)$feltType)->settVisning((object)$visning);
                        $resultat->msg = 'Feltet er lagret';
                        $resultat->url = "/drift/index.php?oppslag=tilpasset_eav_felt_skjema&id={$eavEgenskap->hentId()}";
                        break;
                    }

                    else {
                        /** @var Egenskap $eavEgenskap */
                        $eavEgenskap = $this->app->hentModell(Egenskap::class, $id);
                        if(!$eavEgenskap->hentId()) {
                            throw new Exception('Finner ikke egenskapen ' . $id);
                        }
                        if(!$eavEgenskap->erEgendefinert) {
                            throw new Exception('Systemfelter kan ikke endres');
                        }
                        $eavEgenskap
                            ->settKode($kode)
                            ->settType($type)
                            ->settBeskrivelse($beskrivelse)
                            ->settKonfigurering($konfigurering)
                            ->settRekkefølge($rekkefølge)
                        ;

                        $eavEgenskap->settFeltType((object)$feltType)->settVisning((object)$visning);
                        $resultat->msg = 'Feltet er lagret';
                        $resultat->url = "/drift/index.php?oppslag=tilpasset_eav_felt_skjema&id={$eavEgenskap->hentId()}";
                        $this->app->returi->reset();
                        break;
                    }
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}