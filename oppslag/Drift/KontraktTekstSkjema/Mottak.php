<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\KontraktTekstSkjema;


use Exception;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema): Mottak
    {
        /** @var stdClass $resultat */
        $resultat = (object)[
            'success' => true,
            'msg' => "Lagret."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'avtaletekst':
                    $id = $get['id'] ?? null;
                    $avtaletekstmal = $post['avtaletekstmal'] ?? '';
                    /** @var Kontrakt $kontrakt */
                    $kontrakt = $this->app->hentModell(Kontrakt::class, $id);
                    if (!$kontrakt->hentId()) {
                        throw new Feilmelding('Kontrakt ' . $id . ' finnes ikke');
                    }
                    if (!$avtaletekstmal) {
                        throw new Feilmelding('Avtaleteksten mangler');
                    }
                    $kontrakt->leieforhold->settAvtaletekstmal($avtaletekstmal);
                    $kontrakt->settTekst($kontrakt->leieforhold->gjengiAvtaletekst(true, $kontrakt));

                    $resultat->data = $kontrakt->leieforhold->avtaletekstmal;
                    $resultat->id = $kontrakt->hentId();
                    $resultat->msg = 'Avtaleteksten er lagret';
//                    $resultat->url = (string)$this->app->returi->get(1);
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Feilmelding $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        catch (\Throwable $e) {
            $this->app->loggException($e);
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
            return $this;
    }
}