<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\KontraktTekstSkjema;


use Exception;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\LeieforholdSkjema
 */
class Utlevering extends AbstraktUtlevering
{

    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null): Utlevering
    {
        $get = $this->data['get'];
        $post = $this->data['post'];


        switch ($data) {
            case 'forhåndsvisning': {
                $resultat = (object)[
                    'success' => true,
                    'data'		=> []
                ];

                try {
                    $kontraktId = $get['id'] ?? null;
                    $kontrakt = $this->app->hentModell(Kontrakt::class, $kontraktId);
                    $mal = $post['mal'] ?? '';
                    $resultat->data = $kontrakt->leieforhold->gjengiAvtaletekst(true, $kontrakt, $mal);
                }
                catch (Feilmelding $e) {
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                catch (\Throwable $e) {
                    $this->app->loggException($e);
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }

            default:
                $resultat = (object)[
                    'success' => false,
                    'msg' => 'Ønsket data er ikke oppgitt',
                    'data' => []
                ];
                $this->settRespons(new JsonRespons($resultat));
                return $this;
        }
    }
}