<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use DateInterval;
use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;


/**
 * UtskriftsveiviserRegninger Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class UtskriftsveiviserRegninger extends \Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Utskriftsveiviser – Velg krav som skal settes sammen til regninger';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $brukerId = $this->bruker['id'];
        $bruker = $this->hentModell(Person::class, $brukerId);
        $this->hoveddata['bruker'] = $bruker;
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            /** @var int[] $inkLeieforhold */
            $inkLeieforhold = isset($_GET['ink_leieforhold']) && $_GET['ink_leieforhold'] ? array_map('intval', explode(',', $_GET['ink_leieforhold'])) : [];
            /** @var int[] $eksLeieforhold */
            $eksLeieforhold = isset($_GET['eks_leieforhold']) && $_GET['eks_leieforhold'] ? array_map('intval', explode(',', $_GET['eks_leieforhold'])) : [];
            $forfallFramTil = isset($_GET['tildato']) ? new DateTime($_GET['tildato']) : null;
            $kravtyper = [];
            $adskilt = isset($_GET['adskilt']) && $_GET['adskilt'];
        }
        else {
            /** @var int[] $inkLeieforhold */
            $inkLeieforhold = array_map('intval', (array)($_POST['ink_leieforhold'] ?? []));
            /** @var int[] $eksLeieforhold */
            $eksLeieforhold = array_map('intval', (array)($_POST['eks_leieforhold'] ?? []));
            /** @var DateTime|null $forfallFramTil */
            $forfallFramTil = isset($_POST['tildato']) ? new DateTime($_POST['tildato']) : null;
            /** @var string[] $kravtyper */
            $kravtyper = (array)($_POST['kravtyper'] ?? []);
            $adskilt = isset($_POST['adskilt']) && $_POST['adskilt'];
        }
        if (!$forfallFramTil) {
            $forfallFramTil = new DateTime();
            $forfallFramTil->add(new DateInterval('P1M'));
        }
        $kravsett = $this->hentUtskriftsprosessor()
            ->hentKravsettForUtskrift($forfallFramTil, $kravtyper, $inkLeieforhold, $eksLeieforhold);

        $this->hoveddata['kravsett'] = $kravsett;
        $this->hoveddata['adskilt'] = $adskilt;
        $this->hoveddata['ink_leieforhold'] = $inkLeieforhold;
        $this->hoveddata['eks_leieforhold'] = $eksLeieforhold;

        return true;
    }

    /**
     * @return Index
     */
    public function skript() {
        /** @var Person|null $bruker */
        $bruker = $this->hoveddata['bruker'];
        /** @var Kravsett|null $kravsett */
        $kravsett = $this->hoveddata['kravsett'];
        /** @var int[] $inkLeieforhold */
        $inkLeieforhold  = $this->hoveddata['ink_leieforhold'] ?? [];
        /** @var int[] $eksLeieforhold */
        $eksLeieforhold  = $this->hoveddata['eks_leieforhold'] ?? [];
        /** @var bool $adskilt */
        $adskilt  = $this->hoveddata['adskilt'];
        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(\Kyegil\Leiebasen\Visning\drift\js\utskriftsveiviser_regninger\ExtOnReady::class, [
                'bruker' => $bruker,
                'kravsett'    => $kravsett,
                'adskilt'    => $adskilt,
                'inkLeieforhold' => json_encode($inkLeieforhold),
                'eksLeieforhold' => json_encode($eksLeieforhold)
            ])
        ]);
        return $skript;
    }
}