<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class LeieforholdBrukerprofilerSkjema extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
//        'oppgave' => LeieforholdBrukerprofilerSkjema\Oppdrag::class
    ];
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $leieforholdId = $_GET['id'] ?? null;
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentModell(Leieforhold::class, (int)$leieforholdId);
        if (!$leieforhold->hentId()) {
            $leieforhold = null;
        }
        $this->hoveddata['leieforhold'] = $leieforhold;
        $this->hoveddata['leieforholdId'] = $leieforholdId;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        if(!$leieforhold) {
            return false;
        }
        $this->tittel = 'Brukerprofiler for leieforhold ' . $leieforhold;
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];

        $visning = new ViewArray();

        $visning->addItem($this->vis(\Kyegil\Leiebasen\Visning\drift\html\LeieforholdBrukerprofilerSkjema::class, [
            'leieforhold' => $leieforhold,
            'leieforholdId' => $leieforhold->hentId(),
        ]));

        return $visning;
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function skript(): ViewInterface
    {
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];

        $skript = new ViewArray();

        $skript->addItem($this->vis(\Kyegil\Leiebasen\Visning\drift\js\leieforhold_brukerprofiler_skjema\Index::class, [
            'leieforhold' => $leieforhold,
            'leieforholdId' => $this->hoveddata['leieforholdId'],
        ]));

        return $skript;
    }
}