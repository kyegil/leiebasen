<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\AdresseListe;


use DateInterval;
use DateTime;
use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Personsett;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\CsvRespons;
use Kyegil\Leiebasen\Respons\JsonRespons;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\AdresseListe
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null): Utlevering
    {
        $get = $this->data['get'];
        $post = $this->data['post'];

        switch ($data) {
            case 'eksport_csv':
                $resultat = [];

                $sorteringsfelt		= $_GET['sort'] ?? [];
                if(is_string($sorteringsfelt)) {
                    $synkende	= isset($_GET['dir']) && $_GET['dir'] == "DESC";
                    $sorteringsfelt = [[
                        'columnName' => $sorteringsfelt,
                        'dir' => $synkende ? 'desc' : 'asc'
                    ]];
                }

                $søkefelt = $_GET['søkefelt'] ?? '';
                $kunNåværende = isset($_GET['leietakere']) && $_GET['leietakere'];

                try {
                    $kontaktSett = $this->hentKontaktSett(
                        $søkefelt,
                        $kunNåværende,
                        $sorteringsfelt
                    );

                    foreach ($kontaktSett as $kontakt) {
                        $tel1Type = null;
                        $tel1 = null;
                        $tel2Type = null;
                        $tel2 = null;
                        $gateAdresse = [];
                        if(trim($kontakt->adresse1)) {
                            $gateAdresse[] = $kontakt->adresse1;
                        }
                        if(trim($kontakt->adresse2)) {
                            $gateAdresse[] = $kontakt->adresse2;
                        }

                        if (trim($kontakt->mobil)) {
                            $tel1Type = 'Mobile';
                            $tel1 = $this->intlTelFormat($kontakt->mobil);
                            if (trim($kontakt->telefon)) {
                                $tel2Type = 'Home';
                                $tel2 = $this->intlTelFormat($kontakt->telefon);
                            }
                        }
                        else if (trim($kontakt->telefon)) {
                            $tel1Type = 'Home';
                            $tel1 = $this->intlTelFormat($kontakt->telefon);
                        }
                        $resultat[] = array(
                            'Name' => $kontakt->hentNavn(),
                            'Given Name' => $kontakt->fornavn,
                            'Family Name' => $kontakt->etternavn,
                            'Address 1 - Type' => 'Home',
                            'Address 1 - Street' => implode(', ', $gateAdresse),
                            'Address 1 - City' => $kontakt->hentPoststed(),
                            'Address 1 - Postal Code' => $kontakt->hentPostnr(),
                            'Address 1 - Country' => $kontakt->hentLand() ?: 'Norge',
                            'Address 1 - Formatted' => $this->hentAdresseFormatert($kontakt),
                            'Organization 1 - Name' => $kontakt->erOrg ? $kontakt->hentNavn() : null,
                            'Phone 1 - Type' => $tel1Type,
                            'Phone 1 - Value' => $tel1,
                            'Phone 2 - Type' => $tel2Type,
                            'Phone 2 - Value' => $tel2,
                            'E-mail 1 - Type' => $kontakt->epost ? '* Home' : null,
                            'E-mail 1 - Value' => $kontakt->epost,
                            'Birthday' => $kontakt->fødselsdato ? $kontakt->fødselsdato->format('Y-m-d') : null,
                        );
                    }
                    if ($resultat) {
                        $overskrifter = array_keys($resultat[0]);
                        array_unshift($resultat, $overskrifter);
                    }
                } catch (Exception $e) {
                    $this->settRespons(new JsonRespons([
                        'success' => false,
                        'msg' => $e->getMessage()
                    ]));
                    return $this;
                }
                $respons = new CsvRespons($resultat);
                $respons->filnavn = 'adresseliste.csv';
                $this->settRespons($respons);
                return $this;
            default: {
                $resultat = (object)[
                    'success' => true,
                    'draw' => intval($get['draw'] ?? 0),
                    'data'		=> [],
                    'totalRows' => 0,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                ];

                $sorteringsfelt	= $get['sort'] ?? null;
                $synkende = isset($get['dir']) && $get['dir'] == "DESC";
                if ($sorteringsfelt) {
                    $sortering = [[
                        'columnName' => $sorteringsfelt,
                        'dir' => $synkende ? 'desc' : 'asc'
                    ]];
                }
                else {
                    $sortering  = $get['order'] ?? [];
                }
                $start		= intval($get['start'] ?? 0);
                $limit		= $get['limit'] ?? 25;

                $søkefelt = $get['søkefelt'] ?? ($get['search']['value'] ?? '');
                $kunNåværende = isset($_GET['leietakere']) && $_GET['leietakere'];

                try {
                    $kontaktSett = $this->hentKontaktSett(
                        $søkefelt,
                        $kunNåværende,
                        $sortering,
                        $start,
                        $limit
                    );

                    $resultat->recordsFiltered = $resultat->recordsTotal = $resultat->totalRows
                        = $kontaktSett->hentAntall();

                    foreach ($kontaktSett as $kontakt) {
                        $resultat->data[] = (object)array(
                            'id' => $kontakt->hentId(),
                            'fornavn' => $kontakt->fornavn,
                            'etternavn' => $kontakt->etternavn,
                            'adresse' => $kontakt->hentPostadresse(),
                            'telefon' => $kontakt->telefon,
                            'mobil' => $kontakt->mobil,
                            'epost' => $kontakt->epost,
                            'fødselsdato' => $kontakt->fødselsdato ? $kontakt->fødselsdato->format('Y-m-d') : null,
                            'html' => $this->hentDetaljer($kontakt, $kunNåværende)
                        );
                    }
                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->error = $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }
        }
    }

    /**
     * @param Person $person
     * @param bool $kunNåværende
     * @return string
     * @throws Exception
     */
    private function hentDetaljer(Person $person, bool $kunNåværende = false): string
    {
        $leieforholdSett = $person->hentLeieforhold($kunNåværende ? new DateTime() : null);
        $linjer = [];

        foreach( $leieforholdSett as $leieforhold) {
            $oppsigelse = $leieforhold->hentOppsigelse();

            $beskrivelse =
                $leieforhold->leieobjekt->hentBeskrivelse() . ': '
                . $leieforhold->fradato->format('d.m.Y') . '–'
            ;
            if ($oppsigelse) {
                $tildato = clone $oppsigelse->fristillelsesdato;
                $tildato->sub( new DateInterval('P1D') );
                $beskrivelse .= $tildato->format('d.m.Y');
                $beskrivelse = new HtmlElement('del', [], $beskrivelse);
            }
            $lenke = new HtmlElement(
                'a', [
                    'href' => '/drift/index.php?oppslag=leieforholdkort&id=' . $leieforhold->hentId(),
                    'title' => 'Gå til leieforholdet'
                ], $beskrivelse
            );
            $linjer[] = new HtmlElement('li', [], $lenke);
        }
        $html = new HtmlElement('ul', [], $linjer);
        return (string)$html;
    }

    /**
     * @param string $søkefelt
     * @param bool $kunNåværende
     * @param string|null $sorteringsfelt
     * @param bool $synkende
     * @param int $start
     * @param int|null $limit
     * @return Personsett
     * @throws Exception
     */
    protected function hentKontaktSett(
        string $søkefelt,
        bool $kunNåværende,
        array   $sortering = [],
        int $start = 0,
        ?int $limit = null
    ): Personsett
    {
        $filter = array(
            'or' => array(
                "REPLACE(LOWER(CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`)), 'å', 'aa') LIKE" => "%" . str_ireplace(array('Å', 'å'), 'aa', $søkefelt) . "%",
                "CONCAT(`personer`.`fødselsdato`, ' ', `personer`.`personnr`) LIKE" => "%{$søkefelt}%"
            )
        );

        /**
         * Mapping av sorteringsfelter
         * * kolonnenavn = > [tabellkilde el null for hovedtabell => tabellfelt]
         */
        $sorteringsfelter = [
            'fornavn' => [null, 'fornavn'],
            'etternavn' => [null, 'etternavn'],
            'fødselsdato' => [null, 'fødselsdato'],
            'telefon' => [null, 'telefon'],
            'mobil' => [null, 'mobil'],
            'epost' => [null, 'epost'],
        ];

        /** @var Personsett $kontaktSett */
        $kontaktSett = $this->app->hentSamling(Person::class)
            ->leggTilFilter($filter);

        if ($kunNåværende) {
            $kontaktSett
                ->leggTilInnerJoin(
                    Leietaker::hentTabell(),
                    '`' . Leietaker::hentTabell() . '`.`person` = `' . Person::hentTabell() . '`.`' . Person::hentPrimærnøkkelfelt() . '`'
                )
                ->leggTilInnerJoin(
                    Leieforhold::hentTabell(),
                    '`' . Leietaker::hentTabell() . '`.`leieforhold` = `' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`'
                )
                ->leggTilLeftJoin(
                    Leieforhold\Oppsigelse::hentTabell(),
                    '`' . Leieforhold\Oppsigelse::hentTabell() . '`.`' . Leieforhold\Oppsigelse::hentPrimærnøkkelfelt() . '` = `' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '`'
                )
                ->leggTilFilter([
                    'or' => [
                        '`' . Leieforhold\Oppsigelse::hentTabell() . '`.`fristillelsesdato` IS NULL',
                        '`' . Leieforhold\Oppsigelse::hentTabell() . '`.`fristillelsesdato` > NOW()'
                    ]
                ]);
        }

        $kontaktSett->låsFiltre();

        foreach ($sortering as $sorteringskonfig) {
            if(isset($sorteringsfelter[$sorteringskonfig['columnName']])) {
                $kontaktSett->leggTilSortering(
                    $sorteringsfelter[$sorteringskonfig['columnName']][1],
                    strtolower($sorteringskonfig['dir']) == 'desc',
                    $sorteringsfelter[$sorteringskonfig['columnName']][0]
                );
            }
        }

        if ($limit) {
            $kontaktSett
                ->settStart($start)
                ->begrens($limit);
        }
        return $kontaktSett;
    }

    /**
     * @param string $tlf
     * @return string
     */
    private function intlTelFormat(string $tlf): string
    {
        str_replace(' ', '', $tlf);
        if (strlen($tlf) == 8 && substr($tlf, 0, 1) != '0') {
            $tlf = '+47' . $tlf;
        }
        return $tlf;
    }

    /**
     * @param Person $person
     * @return string
     */
    private function hentAdresseFormatert(Person $person): string
    {
        $postnrSted = [];
        $adresse = [];

        if (trim($person->postnr)) {
            $postnrSted[] = trim($person->postnr);
        }
        if (trim($person->poststed)) {
            $postnrSted[] = trim($person->poststed);
        }
        $postnrSted = implode(' ', $postnrSted);

        if (trim($person->adresse1)) {
            $adresse[] = trim($person->adresse1);
        }
        if (trim($person->adresse2)) {
            $adresse[] = trim($person->adresse2);
        }
        if ($postnrSted) {
            $adresse[] = $postnrSted;
        }
        if ($person->land != 'Norge') {
            $adresse[] = $person->land;
        }
        return implode("\n", $adresse);
    }
}