<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\Leieforholdfilter;


use DateInterval;
use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Område\Deltakelse as Områdedeltakelse;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Samling;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Kontraktliste;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\Leieforholdfilter
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData($data = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case "bygninger":
                    $bygningFilter = [];
                    if (isset($this->data['get']['query']) && trim($this->data['get']['query'])) {
                        $query = explode(' ', trim($this->data['get']['query']));
                        foreach ($query as $ord) {
                            $bygningFilter[] = [
                                'or' => [
                                    'id' => $ord,
                                    'navn LIKE' => '%' . $ord . '%',
                                    'kode' => $ord,
                                ]
                            ];
                        }
                    }
                    /** @var Samling $bygningsett */
                    $bygningsett = $this->app->hentSamling(Bygning::class)
                        ->leggTilSortering('navn')

                        ->leggTilFilter($bygningFilter);

                    /** @var Bygning $bygning */
                    foreach ($bygningsett as $bygning) {
                        $resultat->data[] = [
                            'value' => $bygning->hentId(),
                            'text' => "{$bygning->hentNavn()} ({$bygning->hentKode()})",
                        ];
                    }
                    break;

                default:
                    $sort		= $this->data['get']['sort'] ?? null;
                    $synkende	= isset($this->data['get']['dir']) && $this->data['get']['dir'] == "DESC";
                    $start		= intval($this->data['get']['start'] ?? 0);
                    $limit		= $this->data['get']['limit'] ?? null;

                    $søkefilter = $this->data['get']['filter'] ?? [];
                    $søkefelt = isset($søkefilter['søkefelt']) ? trim($søkefilter['søkefelt']) : '';
                    $søkefelt = trim($søkefelt) ? explode(' ', $søkefelt) : [];

                    settype($søkefilter['inkludert'], 'array');
                    settype($søkefilter['ekskludert'], 'array');
                    settype($søkefilter['leieobjekt'], 'array');
                    settype($søkefilter['bygning'], 'array');
                    settype($søkefilter['område'], 'array');
                    settype($søkefilter['beløpMin'], 'string');
                    settype($søkefilter['beløpMaks'], 'string');
                    settype($søkefilter['fradatoMin'], 'string');
                    settype($søkefilter['fradatoMaks'], 'string');
                    settype($søkefilter['fornyetMin'], 'string');
                    settype($søkefilter['fornyetMaks'], 'string');
                    settype($søkefilter['tildatoMin'], 'string');
                    settype($søkefilter['tildatoMaks'], 'string');
                    settype($søkefilter['leiejusteringMin'], 'string');
                    settype($søkefilter['leiejusteringMaks'], 'string');
                    settype($søkefilter['tidsbegrensning'], 'array');
                    settype($søkefilter['tidsbegrensning']['tidsbegrenset'], 'string');
                    settype($søkefilter['tidsbegrensning']['oppsigelig'], 'string');
                    settype($søkefilter['aktive'], 'bool');

                    $sorteringsfelter = [
                        'leieforhold' => [null, 'leieforholdnr'],
                        'leieobjekt' => [null, 'leieobjekt'],
                        'leiebeløp' => [null, 'leiebeløp'],
                        'fast_kid' => [null, 'leieforholdnr'],
                        'fradato' => [null, 'fradato'],
                        'fornyet' => ['kontrakter', 'fradato'],
                        'tildato' => [null, 'tildato'],
                        'oppsagt' => ['oppsigelser', 'fristillelsesdato'],
                        'frosset' => [null, 'frosset'],
                    ];
                    $sort = ($sort && isset($sorteringsfelter[$sort])) ? $sorteringsfelter[$sort] : null;

                    $inkludert = $this->valgte($søkefilter['inkludert']);
                    $ekskludert = $this->valgte($søkefilter['ekskludert']);
                    $leieobjekter = $this->valgte($søkefilter['leieobjekt']);
                    $bygninger = $this->valgte($søkefilter['bygning']);
                    $områder = $this->valgte($søkefilter['område']);
                    /** @var Leieforholdsett $leieforholdsett */
                    $leieforholdsett = $this->app->hentSamling(Leieforhold::class)
                        ->leggTilLeftJoin(
                            Leietaker::hentTabell(),
                            Leietaker::hentTabell() . '.`leieforhold` = `' . Leieforhold::hentTabell() . '`.`' . Leieforhold::hentPrimærnøkkelfelt() . '` AND `' . Leietaker::hentTabell() . '`.`slettet` IS NULL'
                        )
                        ->leggTilLeftJoin(
                            Person::hentTabell(),
                            Leietaker::hentTabell() . '.`person` = `' . Person::hentTabell() . '`.`' . Person::hentPrimærnøkkelfelt() . '`'
                        )
                    ;

                    $leieforholdsett->leggTilLeieobjektModell(['beskrivelse', 'navn', 'etg', 'gateadresse']);
                    $leieforholdsett->leggTilOppsigelseModell(['fristillelsesdato']);
                    $leieforholdsett->leggTilEavFelt('leie_historikk');
                    $leieforholdsett->leggTilEavFelt('leie_oppdatert');

                    foreach ($søkefelt as $søkestreng) {
                        $leieforholdsett->leggTilFilter([
                            'or' => [
                                '`leieforhold`.`leieforholdnr`' => (int)((intval($søkestreng)-1000000)/10),
                                '`leieforhold`.`leieforholdnr` LIKE' => "%{$søkestreng}%",
                                '`kontrakter`.`kontraktnr` LIKE' => "%{$søkestreng}%",
                                '`leieforhold`.`leiebeløp` LIKE' => "%{$søkestreng}%",
                                '`leieforhold`.fradato LIKE' => "%{$søkestreng}%",
                                '`leieforhold`.tildato LIKE' => "%{$søkestreng}%",
                                '`leieforhold`.leieobjekt LIKE' => "%{$søkestreng}%",
                                '`leieobjekter`.navn LIKE' => "%{$søkestreng}%",
                                '`leieobjekter`.gateadresse LIKE' => "%{$søkestreng}%",
                                '`kontraktpersoner`.`leietaker` LIKE' => "%{$søkestreng}%",
                                '`personer`.`fornavn` LIKE' => "%{$søkestreng}%",
                                '`personer`.`etternavn` LIKE' => "%{$søkestreng}%",
                            ]
                        ]);
                    }

                    if ($inkludert) {
                        $leieforholdsett->leggTilFilter([
                            '`leieforhold`.`leieforholdnr` IN(' . implode(',', $inkludert) .')'
                        ]);
                    }

                    if ($ekskludert) {
                        $leieforholdsett->leggTilFilter([
                            '`leieforhold`.`leieforholdnr` NOT IN(' . implode(',', $ekskludert) .')'
                        ]);
                    }

                    if ($leieobjekter) {
                        $leieforholdsett->leggTilFilter([
                            '`leieforhold`.`leieobjekt` IN(' . implode(',', $leieobjekter) .')'
                        ]);
                    }

                    if ($bygninger) {
                        $leieforholdsett->leggTilFilter([
                            '`leieobjekter`.`bygning` IN(' . implode(',', $bygninger) .')'
                        ]);
                    }

                    if ($områder) {
                        $leieforholdsett
                            ->leggTilLeftJoin(
                                Bygning::hentTabell(),
                                Leieobjekt::hentTabell() . '.`bygning` = `' . Bygning::hentTabell() . '`.`' . Bygning::hentPrimærnøkkelfelt() . '`'
                            )
                            ->leggTilLeftJoin(
                                ['leieobjektområde' => Områdedeltakelse::hentTabell()],
                                '`leieobjektområde`.`medlem_type` = "' . addslashes(Leieobjekt::class) . '" AND `leieobjektområde`.`medlem_id` = `' . Leieobjekt::hentTabell() . '`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '`'
                            )
                            ->leggTilLeftJoin(
                                ['bygningsområde' => Områdedeltakelse::hentTabell()],
                                '`bygningsområde`.`medlem_type` = "' . addslashes(Bygning::class) . '" AND `bygningsområde`.`medlem_id` = `' . Bygning::hentTabell() . '`.`' . Bygning::hentPrimærnøkkelfelt() . '`'
                            )
                            ->leggTilFilter(['or' => [
                                '`leieobjektområde`.`område_id` IN(' . implode(',', $områder). ')',
                                '`bygningsområde`.`område_id` IN(' . implode(',', $områder). ')'
                            ]])
                        ;
                    }

                    if($søkefilter['beløpMin']) {
                        $leieforholdsett->leggTilFilter([
                            'leieforhold.leiebeløp >=' => $søkefilter['beløpMin']
                        ]);
                    }
                    if($søkefilter['beløpMaks']) {
                        $leieforholdsett->leggTilFilter([
                            'leieforhold.leiebeløp <=' => $søkefilter['beløpMaks']
                        ]);
                    }
                    if($søkefilter['fradatoMin']) {
                        $leieforholdsett->leggTilFilter([
                            'leieforhold.fradato >=' => $søkefilter['fradatoMin']
                        ]);
                    }
                    if($søkefilter['fradatoMaks']) {
                        $leieforholdsett->leggTilFilter([
                            'leieforhold.fradato <=' => $søkefilter['fradatoMaks']
                        ]);
                    }
                    if($søkefilter['fornyetMin']) {
                        $leieforholdsett->leggTilFilter([
                            'kontrakter.fradato >=' => $søkefilter['fornyetMin']
                        ]);
                    }
                    if($søkefilter['fornyetMaks']) {
                        $leieforholdsett->leggTilFilter([
                            'kontrakter.fradato <=' => $søkefilter['fornyetMaks']
                        ]);
                    }
                    if($søkefilter['tildatoMin']) {
                        $leieforholdsett->leggTilFilter([
                            'leieforhold.tildato >=' => $søkefilter['tildatoMin']
                        ]);
                    }
                    if($søkefilter['tildatoMaks']) {
                        $leieforholdsett->leggTilFilter([
                            'leieforhold.tildato <=' => $søkefilter['tildatoMaks']
                        ]);
                    }

                    if($søkefilter['tidsbegrensning']['tidsbegrenset'] xor $søkefilter['tidsbegrensning']['oppsigelig']) {
                        $leieforholdsett->leggTilFilter($søkefilter['tidsbegrensning']['tidsbegrenset']
                            ? ["kontrakter.tildato IS NOT NULL"]
                            : ["kontrakter.tildato IS NULL"]);
                    }

                    if($søkefilter['aktive']) {
                        $leieforholdsett->leggTilFilter(["oppsigelser.leieforhold IS NULL OR oppsigelser.fristillelsesdato > NOW()"]);
                    }

                    $leieforholdsett->låsFiltre();

                    if($sort) {
                        $leieforholdsett->leggTilSortering($sort[1], $synkende, $sort[0]);
                    }

                    $resultat->totalRows = $leieforholdsett->hentAntall();
                    $leieforholdsett->inkluderLeietakere();
                    $kontrakter = $leieforholdsett->inkluderKontrakter();
                    $kontrakter->inkluderLeietakere();
                    if ($limit && $limit < $resultat->totalRows) {
                        $leieforholdsett
                            ->nullstill()
                            ->settStart($start)
                            ->begrens($limit);
                    }

                    foreach ($leieforholdsett as $leieforhold) {
                        $oppsagt = $leieforhold->hentOppsigelse() ? clone $leieforhold->hentOppsigelse()->fristillelsesdato : null;
                        $oppsagt = $oppsagt ? $oppsagt->sub(new DateInterval('P1D'))->format('Y-m-d') : null;

                        $kontrakter = $this->app->vis(Kontraktliste::class, ['leieforhold' => $leieforhold]);
                        $leieOppdatert = $leieforhold->hent('leie_oppdatert');

                        $resultat->data[] = (object)[
                            'leieforhold' => $leieforhold->hentId(),
                            'beskrivelse' => $leieforhold->hentBeskrivelse(),
                            'leieobjekt' => $leieforhold->hentLeieobjekt()->hentId(),
                            'leieobjektbeskrivelse' => $leieforhold->hentLeieobjekt()->hentBeskrivelse(),
                            'leiebeløp' => $leieforhold->hentLeiebeløp(),
                            'fast_kid' => $leieforhold->hentKid(),
                            'kontrakt' => strval($leieforhold->hentKontrakt()),
                            'fradato' => $leieforhold->hentFradato()->format('Y-m-d'),
                            'tildato' => $leieforhold->tildato ? $leieforhold->tildato->format('Y-m-d') : null,
                            'fornyet' => $leieforhold->hentKontrakt()->hentDato()->format('Y-m-d'),
                            'siste_leiejustering' => $leieOppdatert ? $leieOppdatert->format('Y-m-d') : null,
                            'oppsagt' => $oppsagt,
                            'frosset' => $leieforhold->hentFrosset(),
                            'kontrakter' => strval($kontrakter),
                        ];
                    }
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param string[] $checkboxVerdier
     * @return int[]
     */
    protected function valgte($checkboxVerdier)
    {
        $verdier = [];
        foreach($checkboxVerdier as $verdi => $valgt) {
            if($valgt) {
                $verdier[] = intval($verdi);
            }
        }
        return $verdier;
    }
}