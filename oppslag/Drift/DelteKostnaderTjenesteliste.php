<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_tjenesteliste\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * DelteKostnaderTjenesteliste Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class DelteKostnaderTjenesteliste extends DriftExtJsAdaptor
{
    protected $oppdrag = [
        'utskrift' => DelteKostnaderTjenesteliste\Utskrift::class
    ];
    /** @var string */
    public $tittel = 'Eksterne tjenester som deles mellom beboerne';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML(): bool
    {
        if(!parent::preHTML()) {
            return false;
        }
        $brukerId = $this->bruker['id'];
        /** @var Person $bruker */
        $bruker = $this->hentModell(Person::class, $brukerId);
        $this->hoveddata['bruker'] = $bruker;
        return true;
    }

    /**
     * @return Index
     * @throws Exception
     */
    public function skript() {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];

        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'bruker' => $bruker
            ])
        ]);
        return $skript;
    }
}