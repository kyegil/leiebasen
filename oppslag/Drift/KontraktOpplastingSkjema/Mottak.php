<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\KontraktOpplastingSkjema;


use Exception;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use stdClass;

class Mottak extends AbstraktMottak
{
    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema): Mottak
    {
        /** @var stdClass $resultat */
        $resultat = (object)[
            'success' => true,
            'msg' => "Lagret."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'kontrakt_opplasting_skjema':
                    $id = $get['id'] ?? null;
                    $signertAvtale =
                        isset($this->data['files']['signert_avtale'][0]['name']) && $this->data['files']['signert_avtale'][0]['name']
                            ? $this->data['files']['signert_avtale'][0]
                            : null;
                    /** @var Kontrakt $kontrakt */
                    $kontrakt = $this->app->hentModell(Kontrakt::class, $id);

                    if (!$kontrakt->hentId()) {
                        throw new Exception('Finner ikke kontrakten. (Angitt id: ' . $id);
                    }

                    $resultat->id = $kontrakt->hentId();
                    if ($signertAvtale) {
                        $this->lastOppOgLagreAvtale($signertAvtale, $kontrakt);
                    }

                    $resultat->msg = 'Lagret';
                    $resultat->url = strval($this->app->returi->get(1));
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Feilmelding $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        catch (\Throwable $e) {
            $this->app->loggException($e);
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param array $signertAvtale
     * @param Kontrakt $kontrakt
     * @return $this
     * @throws Exception
     */
    protected function lastOppOgLagreAvtale(array $signertAvtale, Kontrakt $kontrakt): Mottak
    {
        if($signertAvtale['error'] ?? null) {
            switch($signertAvtale['error']) {
                case UPLOAD_ERR_INI_SIZE:
                    throw new Exception('Fila kan ikke overstige ' . ini_get('upload_max_filesize') . ' som er satt i php.ini');
                case UPLOAD_ERR_FORM_SIZE:
                    throw new Exception('Fila kan ikke overstige MAX_FILE_SIZE som er angitt i skjemaet');
                case UPLOAD_ERR_PARTIAL:
                    throw new Exception('Fila ble bare delvis lastet opp');
                case UPLOAD_ERR_NO_FILE:
                    throw new Exception('Fila ble ikke med');
                case UPLOAD_ERR_NO_TMP_DIR:
                    throw new Exception('Fila kunne ikke lagres. Midlertidig lagringsplass ble ikke funnet');
                case UPLOAD_ERR_CANT_WRITE:
                    throw new Exception('Fila kunne lagres pga. manglende skrive-tillatelser');
                case UPLOAD_ERR_EXTENSION:
                    throw new Exception('En PHP-extension forhindret opplastingen');
                default:
                    throw new Exception('Det skjedde en feil under opplastingen av avtalen, og den kunne ikke lagres');
            }
        }

        $sti = $this->app->hentFilarkivBane() . '/leieavtaler/';
        $filendelse = pathinfo($signertAvtale['name'], PATHINFO_EXTENSION);
        $filnavn = 'leieavtale-' . $kontrakt->hentId() . '.' . $filendelse;
        $filinnhold = file_get_contents($signertAvtale["tmp_name"]);
        $sjekk = strtolower($filendelse) === 'pdf' && preg_match("/^%PDF-/", $filinnhold);
        $sjekk = $sjekk || getimagesize($signertAvtale["tmp_name"]);
        if($sjekk == false) {
            throw new Exception('Den opplastede fila er ikke av godkjent format. Last opp som pdf eller et bilde.');
        }

        if(!in_array(strtolower($filendelse), ['jpg','jpeg', 'png', 'gif', 'pdf'])) {
            throw new Exception('Det opplastede bildet må være av typen JPG/JPEG, PNG eller GIF.');
        }

        if ($signertAvtale["size"] > 1000000) {
            throw new Exception('Fila er for stor. Maks. filstørrelse er 1Mb.');
        }

        if (!file_exists($sti)) {
            if(!mkdir($sti)) {
                throw new Exception("Klarte ikke finne eller opprette filplasseringen '{$sti}'.");
            }
        }

        if(file_exists($kontrakt->signertFil)) {
            unlink($kontrakt->signertFil);
        }

        if (!move_uploaded_file($signertAvtale["tmp_name"], $sti . $filnavn)) {
            throw new Exception("Fila kunne ikke lagres som '{$filnavn}' pga ukjent feil.");
        }

        $kontrakt->signertFil = $filnavn;
        return $this;
    }
}