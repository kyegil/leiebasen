<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderTjenesteliste;


use Exception;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjenestesett;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderTjenesteliste
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null): Utlevering
    {
        switch ($data) {
            default: {
                $resultat = (object)[
                    'success' => true,
                    'data'		=> [],
                    'totalRows' => 0
                ];

                $sorteringsfelt		= $_GET['sort'] ?? null;
                $synkende	= isset($_GET['dir']) && $_GET['dir'] == "DESC";
                $start		= intval($_GET['start'] ?? 0);
                $limit		= $_GET['limit'] ?? null;

                $søkefelt = $_GET['søkefelt'] ?? '';

                try {
                    $tjenesteSett = $this->hentTjenesteSett(
                        $søkefelt,
                        $sorteringsfelt,
                        $synkende,
                        $start,
                        $limit
                    );

                    $resultat->totalRows = $tjenesteSett->hentAntall();

                    foreach ($tjenesteSett as $tjeneste) {
                        $resultat->data[] = (object)array(
                            'id' => $tjeneste->hentId(),
                            'kravtype' => $tjeneste->kravtype,
                            'navn' => $tjeneste->navn,
                            'avtalereferanse' => $tjeneste->avtalereferanse,
                            'beskrivelse' => $tjeneste->beskrivelse,
                        );
                    }
                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }
        }
    }

    /**
     * @param string $søkestreng
     * @param string|null $sorteringsfelt
     * @param bool $synkende
     * @param int $start
     * @param int|null $limit
     * @return Tjenestesett
     * @throws Exception
     */
    protected function hentTjenesteSett(
        string  $søkestreng,
        ?string $sorteringsfelt = null,
        bool    $synkende = false,
        int     $start = 0,
        ?int    $limit = null
    ): Tjenestesett
    {
        $filter = array(
            'or' => array(
                '`kostnadsdeling_tjenester`.`navn` LIKE' => '%' . $søkestreng . '%',
                '`kostnadsdeling_tjenester`.`avtalereferanse` LIKE' => '%' . $søkestreng . '%',
                '`kostnadsdeling_tjenester`.`beskrivelse` LIKE' => '%' . $søkestreng . '%',
            )
        );

        /**
         * Mapping av sorteringsfelter
         * * kolonnenavn = > [tabellkilde el null for hovedtabell => tabellfelt]
         */
        $sorteringsfelter = [
            'id' => [null, 'id'],
            'kravtype' => [null, 'kravtype'],
            'navn' => [null, 'navn'],
            'avtalereferanse' => [null, 'avtaleref'],
        ];
        $sorteringsfelt = ($sorteringsfelt && isset($sorteringsfelter[$sorteringsfelt])) ? $sorteringsfelter[$sorteringsfelt] : null;

        /** @var Tjenestesett $tjenestesett */
        $tjenestesett = $this->app->hentSamling(Tjeneste::class);

        $tjenestesett->leggTilFilter($filter);
        $tjenestesett->låsFiltre();

        if ($sorteringsfelt) {
            $tjenestesett->leggTilSortering($sorteringsfelt[1], $synkende, $sorteringsfelt[0]);
        }

        if ($limit) {
            $tjenestesett
                ->settStart($start)
                ->begrens($limit);
        }
        return $tjenestesett;
    }
}