<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\Leiebasen\Visning\felles\html\shared\body\main\Kort;
use Kyegil\ViewRenderer\ViewArray;

/**
 * LeieobjektKort Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class LeieobjektKort extends DriftKontroller
{

    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $leieobjektId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['leieobjekt'] = $leieobjektId ? $this->hentModell(Leieobjekt::class, $leieobjektId) : null;
    }


    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $brukerId = $this->bruker['id'];
        $bruker = $this->hentModell(Person::class, $brukerId);
        $this->hoveddata['bruker'] = $bruker;

        /** @var Leieobjekt $leieobjekt */
        $leieobjekt = $this->hoveddata['leieobjekt'];
        if(!$leieobjekt->hentId()) {
            return false;
        }
        $this->tittel = "{$leieobjekt->hentBeskrivelse()}";

        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws \Exception
     */
    public function innhold()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieobjekt $leieobjekt */
        $leieobjekt = $this->hoveddata['leieobjekt'];

        $knapper = new ViewArray([
            $this->visTilbakeknapp(),
            new HtmlElement('a', [
                'href' => '/drift/index.php?oppslag=skadeliste&id=' . $leieobjekt->hentId(),
                'class' => 'button'
            ], 'Vis skader på leieobjektet'),
            new HtmlElement('a', [
                'href' => '/drift/index.php?oppslag=leieobjekt_skjema&id=' . $leieobjekt->hentId(),
                'class' => 'button'
            ], 'Endre'),
        ]);

        $leieobjektKort = $this->vis(Kort::class, [
            'innhold' => $this->vis(\Kyegil\Leiebasen\Visning\drift\html\leieobjekt_kort\Index::class, [
                'leieobjekt' => $leieobjekt,
            ]),
        ]);

        $html = $this->vis(\Kyegil\Leiebasen\Visning\drift\html\shared\body\main\Content::class, [
            'mainBodyContents' => $leieobjektKort,
            'buttons' => $knapper,
        ]);
        return $html;
    }
}