<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 17/06/2020
 * Time: 13:33
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\FramleieSkjema;


use DateInterval;
use DateTime;
use Exception;
use Kyegil\CoreModel\CoreModel;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleie;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;

class Mottak extends AbstraktMottak
{
    /**
     * @param string|null $skjema
     * @return $this|Mottak
     */
    public function taIMot($skjema = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($skjema) {
                case 'framleiepersoner':
                    $avtaleId = $this->data['get']['id'] ?? null;
                    $adressekortId = $this->data['post']['adressekort'] ?? null;
                    $erOrg = isset($this->data['post']['er_org']) ? boolval($this->data['post']['er_org']) : null;
                    $fornavn = $this->data['post']['fornavn'] ?? null;
                    $etternavn = $this->data['post']['etternavn'] ?? null;
                    $fødselsdato = isset($this->data['post']['fødselsdato']) && $this->data['post']['fødselsdato'] ? new DateTime($this->data['post']['fødselsdato']) : null;
                    $personnr = $this->data['post']['personnr'] ?? null;
                    $adresse1 = $this->data['post']['adresse1'] ?? null;
                    $adresse2 = $this->data['post']['adresse2'] ?? null;
                    $postnr = $this->data['post']['postnr'] ?? null;
                    $poststed = $this->data['post']['poststed'] ?? null;
                    $land = $this->data['post']['land'] ?? null;
                    $telefon = $this->data['post']['telefon'] ?? null;
                    $mobil = $this->data['post']['mobil'] ?? null;
                    $epost = $this->data['post']['epost'] ?? null;

                    if($adressekortId == '*') {
                        /** @var Person $adressekort */
                        $adressekort = $this->app->nyModell(Person::class, (object)[
                            'er_org' => $erOrg,
                            'fornavn' => $fornavn,
                            'etternavn' => $etternavn,
                            'adresse1' => $adresse1,
                            'adresse2' => $adresse2,
                            'postnr' => $postnr,
                            'poststed' => $poststed,
                            'land' => $land,
                            'telefon' => $telefon,
                            'mobil' => $mobil
                        ]);
                    }
                    else {
                        /** @var Person $adressekort */
                        $adressekort = $this->app->hentModell(Person::class, $adressekortId);
                        $adressekort->erOrg = $erOrg;
                        $adressekort->fornavn = $fornavn;
                        $adressekort->etternavn = $etternavn;
                        $adressekort->adresse1 = $adresse1;
                        $adressekort->adresse1 = $adresse2;
                        $adressekort->postnr = $postnr;
                        $adressekort->poststed = $poststed;
                        $adressekort->land = $land;
                        $adressekort->telefon = $telefon;
                        $adressekort->mobil = $mobil;
                    }

                    try {
                        $adressekort->epost = $epost;
                        $adressekort->fødselsdato = $fødselsdato;
                        $adressekort->personnr = $personnr;
                    } catch (Exception $e) {
                        $resultat->msg = $e->getMessage();
                    }

                    // Dersom framleieavtalen ikke er lagret ennå, lagres leietakerne midlertidig
                    if ($avtaleId == '*') {
                        $_SESSION['leiebasen']['temp']['framleieskjema']['leietakere'][] = $adressekort->hentId();
                        $_SESSION['leiebasen']['temp']['framleieskjema']['leietakere'] = array_unique($_SESSION['leiebasen']['temp']['framleieskjema']['leietakere']);
                    }
                    else {
                        /** @var Framleie $framleie */
                        $framleie = $this->app->hentModell(Framleie::class, $avtaleId);
                        $framleie->leggTilFramleietaker($adressekort);
                    }
                    $resultat->url = '/drift/index.php?oppslag=framleie_skjema&id=' . $avtaleId;

                    break;

                default:
                    $avtaleId = $this->data['get']['id'] ?? null;
                    /** @var Leieforhold $leieforhold */
                    $leieforhold = isset($this->data['post']['leieforhold']) ? $this->app->hentModell(Leieforhold::class, $this->data['post']['leieforhold']) : null;
                    $fraDato = !empty($this->data['post']['fradato']) ? new DateTime($this->data['post']['fradato']) : null;
                    $tilDato = !empty($this->data['post']['tildato']) ? new DateTime($this->data['post']['tildato']) : null;
                    $oppsigelsestid = !empty($this->data['post']['oppsigelsestid']) ? new DateInterval($this->data['post']['oppsigelsestid']) : null;
                    $avtaletekst = $this->data['post']['avtale'] ?? '';
                    $avtalemal = $this->data['post']['avtalemal'] ?? '';
                    $leggTilLeietakere = boolval($this->data['get']['leggtil'] ?? false);

                    if (!$leggTilLeietakere) {
                        if (!$leieforhold || !$leieforhold->hentId()) {
                            throw new Exception('Angitt leieforhold finnes ikke.');
                        }
                        if(!$fraDato || $fraDato < $leieforhold->fradato) {
                            throw new Exception('Fra-dato kan ikke være før hoved-leieforholdet ble påbegynt.');
                        }
                        if(!$tilDato) {
                            throw new Exception('Til-dato er påkrevd.');
                        }
                        if($leieforhold->tildato && $tilDato > $leieforhold->tildato) {
                            throw new Exception('Framleien kan ikke løpe lenger enn hoved-leieforholdet.');
                        }
                        if(!$avtalemal) {
                            throw new Exception('Avtaleteksten  mangler.');
                        }
                    }
                    if ($leggTilLeietakere && $avtaleId == '*') {
                        $framleietakere = $_SESSION['leiebasen']['temp']['framleieskjema']['leietakere'] ?? [];
                        $_SESSION['leiebasen']['temp']['framleieskjema'] = [
                            'leieforhold' => $leieforhold->hentId(),
                            'fradato' => $fraDato ? $fraDato->format('Y-m-d') : null,
                            'tildato' => $tilDato ? $tilDato->format('Y-m-d') : null,
                            'oppsigelsestid' => $oppsigelsestid ? CoreModel::prepareIso8601IntervalSpecification($oppsigelsestid) : null,
                            'avtalemal' => $avtalemal,
                            'avtale' => $avtaletekst,
                            'leietakere' => $framleietakere
                        ];
                    }
                    else {
                        if ($avtaleId == '*') {
                            $framleietakere = $_SESSION['leiebasen']['temp']['framleieskjema']['leietakere'] ?? [];

                            if (!$framleietakere) {
                                throw new Exception('Du må oppgi minst én framleietaker');
                            }

                            /** @var Framleie $framleie */
                            $framleie = $this->app->nyModell(Framleie::class, (object)[
                                'leieforhold' => $leieforhold,
                                'fradato' => $fraDato,
                                'tildato' => $tilDato,
                                'avtale' => $avtaletekst,
                                'avtalemal' => $avtalemal
                            ]);
                            foreach ($framleietakere as $framleietakerId) {
                                /** @var Person $framleietaker */
                                $framleietaker = $this->app->hentModell(Person::class, $framleietakerId);
                                $framleie->leggTilFramleietaker($framleietaker);
                            }
                        } else {
                            /** @var Framleie $framleie */
                            $framleie = $this->app->hentModell(Framleie::class, $avtaleId)
                                ->settLeieforhold($leieforhold)
                                ->settFradato($fraDato)
                                ->settTildato($tilDato)
                                ->settAvtale($avtaletekst)
                                ->settAvtalemal($avtalemal);
                        }

                        $framleie->sett('oppsigelsestid', $oppsigelsestid);
                        unset($_SESSION['leiebasen']['temp']['framleieskjema']);
                        $resultat->id = $framleie->hentId();
                    }
                    if($leggTilLeietakere) {
                        $resultat->url = '/drift/index.php?oppslag=framleie_nye_personer_skjema&id=' . $avtaleId;
                    }
                    else {
                        $resultat->url = (string)$this->app->returi->get(1);
                    }
                    $resultat->leggTil = $leggTilLeietakere;

                    break;
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}