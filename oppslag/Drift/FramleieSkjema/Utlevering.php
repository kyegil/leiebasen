<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\FramleieSkjema;


use DateInterval;
use Exception;
use Kyegil\CoreModel\CoreModel;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\AvtaleMal;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleie;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Modell\Leieforhold\Oppsigelse;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Adressefelt as LeieforholdAdressefelt;
use Kyegil\Leiebasen\Visning\felles\html\leieobjekt\Adressefelt as LeieobjektAdressefelt;
use Kyegil\Leiebasen\Visning\felles\html\shared\Utskrift;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\FramleieSkjema
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData($data = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case 'utskrift':
                    return $this->utskrift();

                case "leieforhold":
                    $leieforholdFilter = [];
                    if (isset($_GET['query']) && trim($_GET['query'])) {
                        $query = explode(' ', trim($_GET['query']));
                        foreach ($query as $ord) {
                            $leieforholdFilter[] = [
                                'or' => [
                                    '`leieforhold`.`leieforholdnr`' => $ord,
                                    '`kontraktpersoner`.`leietaker` LIKE' => '%' . $ord . '%',
                                    'CONCAT(`personer`.`fornavn`, \' \', `personer`.`etternavn`) LIKE' => '%' . $ord . '%',
                                ]
                            ];
                        }
                    }
                    // Dersom du ikke søker, men trigger alle leieforhold,
                    // begrenses disse til leieforhold som ikke er avsluttet
                    else {
                        $leieforholdFilter[] = [
                            '`oppsigelser`.`leieforhold`' => null
                        ];
                    }
                    /** @var Leieforholdsett $leieforholdsamling */
                    $leieforholdsamling = $this->app->hentSamling(Leieforhold::class)
                        ->leggTilLeftJoin(Leietaker::hentTabell(),
                            Leieforhold::hentTabell() . '.' . Leieforhold::hentPrimærnøkkelfelt()
                            . ' = ' . Leietaker::hentTabell() . '.leieforhold')
                        ->leggTilLeftJoin(Person::hentTabell(),
                            Leietaker::hentTabell() . '.person'
                            . ' = ' . Person::hentTabell() . '.' . Person::hentPrimærnøkkelfelt()
                        )

                        ->leggTilModell(Leieobjekt::class)
                        ->leggTilModell(Oppsigelse::class)
                        ->leggTilSortering('leieforhold', false, 'oppsigelser')
                        ->leggTilSortering(Leieforhold::hentPrimærnøkkelfelt())

                        ->leggTilFilter($leieforholdFilter, true);

                    /** @var Leieforhold $leieforhold */
                    foreach ($leieforholdsamling as $leieforhold) {
                        $leietakere = [];
                        $leieforholdEpostadresser = [];
                        $leieforholdTelefonnumre = [];
                        $startdato = $leieforhold->fradato;
                        $tildato = $leieforhold->tildato;
                        $oppsigelse = $leieforhold->hentOppsigelse();
                        if($oppsigelse) {
                            $tildato = clone $oppsigelse->hentFristillelsesdato();
                            $tildato->sub(new DateInterval('P1D'));
                        }
                        $tildatoFormatert = $tildato ? $tildato->format('d.m.Y') : '';
                        $leietakersett = $leieforhold->hentLeietakere();
                        $antallLeietakere = $leietakersett->hentAntall();
                        /** @var Leieforhold\Leietaker $leietaker */
                        foreach ($leietakersett as $leietaker) {
                            $fødselsdato = $leietaker->person && $leietaker->person->fødselsdato ? $leietaker->person->fødselsdato->format('d.m.Y') : '';
                            $leietakere[] = $leietaker->hentNavn() . ($fødselsdato ? " f. {$fødselsdato}" : '');
                            if ($leietaker->person) {
                                $telefonnummer = $leietaker->person->telefon ?: $leietaker->person->mobil;
                                if ($telefonnummer) {
                                    $leieforholdTelefonnumre[] = $antallLeietakere > 1
                                        ? "{$leietaker->person->fornavn}: {$telefonnummer}"
                                        : $telefonnummer;
                                }
                                if ($leietaker->person->epost) {
                                    $leieforholdEpostadresser[] = $antallLeietakere > 1
                                        ? "{$leietaker->person->fornavn}: {$leietaker->person->epost}"
                                        : $leietaker->person->epost;
                                }
                            }
                        }
                        $resultat->data[] = [
                            'leieforhold' => $leieforhold->hentId(),
                            'visningsfelt' => $leieforhold->hentId() . ' | ' . $leieforhold->hentBeskrivelse() . ' (' . $startdato->format('d.m.Y') . '–' . $tildatoFormatert . ')',
                            'startdato' => $startdato ? $startdato->format('Y-m-d') : null,
                            'tildato' => $tildato ? $tildato->format('Y-m-d') : null,
                            'leieobjektnr' => $leieforhold->leieobjekt->id,
                            'kid' => $leieforhold->hentKid(),
                            'er_bofellesskap' => $leieforhold->erBofellesskap(),
                            'husleie' => $leieforhold->leiebeløp,
                            'kontaktadresse' => (string)$this->app->vis(LeieforholdAdressefelt::class, ['leieforhold' => $leieforhold]),
                            'boligadresse' => (string)$this->app->vis(LeieobjektAdressefelt::class, ['leieobjekt' => $leieforhold->leieobjekt]),
                            'leietakere' => implode('<br>', $leietakere),
                            'telefon' => implode(', ', $leieforholdTelefonnumre),
                            'epost' => implode(', ', $leieforholdEpostadresser)
                        ];
                    }
                    break;

                case 'maler':
                    $malsamling = $this->app->hentSamling(AvtaleMal::class)
                        ->leggTilFilter(['type' => AvtaleMal::TYPE_LEIEAVTALE]);
                    /** @var AvtaleMal $mal */
                    foreach ($malsamling as $mal) {
                        $resultat->data[] = [
                            'id' => $mal->hentId(),
                            'navn' =>$mal->hentMalnavn(),
                            'tekst' => $mal->hentMal()
                        ];
                    }
                    break;

                case 'personliste':
                    $framleieId = !empty($_GET['id']) ? $_GET['id'] : null;
                    if (!$framleieId) {
                        throw new Exception('id-parameter mangler');
                    }
                    if($framleieId == '*') {
                        $leietakere = $_SESSION['leiebasen']['temp']['framleieskjema']['leietakere'] ?? [];
                        $personer = $this->app->hentSamling(Person::class)->leggTilFilter([
                                'personid IN(' . ($leietakere ? implode(',', $leietakere) : 'null') . ')'
                            ]);
                    }
                    else {
                        /** @var Framleie $framleie */
                        $framleie = $this->app->hentModell(Framleie::class, $framleieId);
                        if (!$framleie->hentId()) {
                            throw new Exception('denne framleie-avtalen finnes ikke');
                        }
                        $personer = $framleie->hentFramleiepersoner();
                    }
                    /** @var Person $person */
                    foreach ($personer as $person) {
                        $resultat->data[] = [
                            'personid' => $person->hentId(),
                            'navn' => $person->hentNavn(),
                            'fornavn' => $person->hentFornavn(),
                            'fødselsdato' => $person->fødselsdato ? $person->fødselsdato->format('Y-m-d') : null,
                            'epost' => $person->epost,
                            'telefon' => $person->telefon ?: $person->mobil
                        ];
                    }
                    break;

                default:
                    $framleieId = !empty($_GET['id']) ? $_GET['id'] : null;
                    if (!$framleieId) {
                        throw new Exception('id-parameter mangler');
                    }
                    if ($framleieId == '*') {
                        $sessionData = $_SESSION['leiebasen']['temp']['framleieskjema'] ?? [];
                        if (isset($_SESSION['leiebasen']['temp']['framleieskjema'])) {
                            $resultat->data = (object) [
                                'nr' => '*',
                                'fradato' => $sessionData['fradato'] ?? null,
                                'tildato' => $sessionData['tildato'] ?? null,
                                'oppsigelsestid' => $sessionData['oppsigelsestid'] ?? null,
                                'leieforhold' => $sessionData['leieforhold'] ?? null,
                                'avtalemal' => $sessionData['avtalemal'] ?? null,
                                'leietakere' => $sessionData['avtalemal'] ?? []
                            ];
                        }
                    }
                    else {
                        /** @var Framleie $framleie */
                        $framleie = $this->app->hentModell(Framleie::class, $framleieId);
                        if (!$framleie->hentId()) {
                            throw new Exception('denne framleie-avtalen finnes ikke');
                        }
                        $resultat->data = (object) [
                            'nr' => $framleie->hentId(),
                            'fradato' => $framleie->fradato->format('Y-m-d'),
                            'tildato' => $framleie->tildato->format('Y-m-d'),
                            'oppsigelsestid' => (string)CoreModel::prepareIso8601IntervalSpecification($framleie->hent('oppsigelsestid')),
                            'leieforhold' => $framleie->leieforhold->hentId(),
                            'avtalemal' => $framleie->hentAvtalemal(),
                        ];
                    }
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @return $this
     */
    protected function utskrift()
    {
        try {
            $framleieId = !empty($_GET['id']) ? $_GET['id'] : null;
            /** @var Framleie $framleie */
            $framleie = $this->app->hentModell(Framleie::class, $framleieId);
            $innhold = [];
            $innhold[] = new HtmlElement('style', [], 'body {font-size: 1rem;}');
            $innhold[] = $framleie->avtale;
            $resultat = $this->app->vis(Utskrift::class, [
                'tittel' => 'Framleie-avtale',
                'innhold' => $innhold
            ]);

        }
        catch (Exception $e) {
            $resultat = $e->getMessage();
        }
        $this->settRespons(new Respons($resultat));
        return $this;
    }
}