<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 04/12/2020
 * Time: 23:21
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\FramleieSkjema;


use Exception;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleie;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleie\Framleietaker;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function avbrytRegistrering(): Oppdrag
    {
        unset($_SESSION['leiebasen']['temp']['framleieskjema']);
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @return $this
     */
    protected function slettPerson() {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            $framleieForholdId = $this->data['post']['framleieforhold'];
            $personId = (int)$this->data['post']['personid'];
            if(!$framleieForholdId) {
                throw new Exception('Framleieforhold-id mangler');
            }
            if(!$personId) {
                throw new Exception('Person-id mangler');
            }
            if($framleieForholdId == '*') {
                $framleietaker = $this->app->hentModell(Person::class, $personId);
                if (isset($_SESSION['leiebasen']['temp']['framleieskjema']['leietakere'])) {
                    $_SESSION['leiebasen']['temp']['framleieskjema']['leietakere']
                        = array_diff($_SESSION['leiebasen']['temp']['framleieskjema']['leietakere'], [$personId]);
                    $resultat->msg .= "<br>{$framleietaker->navn} er slettet.";
                }
            }
            else {
                $gjenværendeFramleiepersoner = $this->app->hentSamling(Framleietaker::class)
                    ->leggTilFilter([
                        'framleieforhold' => $framleieForholdId,
                        'personid <>' => $personId
                    ]);
                if ($gjenværendeFramleiepersoner->hentAntall() < 1) {
                    throw new Exception('Kan ikke slette. Det må være minst én framleietaker');
                }
                /** @var Framleietaker $framleietaker */
                foreach ($this->app->hentSamling(Framleietaker::class)
                             ->leggTilFilter([
                                 'framleieforhold' => $framleieForholdId,
                                 'personid' => $personId
                             ]) as $framleietaker) {
                    $navn = $framleietaker->hentPerson()->hentNavn();
                    $framleietaker->slett();
                    $resultat->msg .= "<br>{$navn} er slettet.";
                }
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @return $this
     */
    protected function slettAvtale() {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            $framleieForholdId = $this->data['post']['framleieforhold'];
            /** @var Framleie $framleieForhold */
            $framleieForhold = $this->app->hentModell(Framleie::class, $framleieForholdId);
            if (!$framleieForhold->hentId()) {
                throw new Exception('Finner ikke framleie-avtalen');
            }
            $resultat->msg = 'Framleie-avtale ' . $framleieForhold->hentId() . ' har blitt slettet';
            $framleieForhold->slett();
            $resultat->url = $this->app->returi->get();
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}