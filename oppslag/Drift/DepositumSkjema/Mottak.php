<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\DepositumSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Depositum;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;

class Mottak extends AbstraktMottak
{
    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Lagret."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'depositum_skjema':
                    $depositumId = $get['id'] ?? null;
                    $leieforholdId = $post['leieforhold'] ?? null;
                    $type = $post['type'] ?? 'depositum';
                    $dato = !empty($post['dato']) ? date_create_immutable($post['dato']) : null;
                    $beløp = floatval($post['beløp'] ?? '');
                    $betaler = $post['betaler'] ?? '';
                    $notat = $post['notat'] ?? '';
                    $konto = !empty($post['konto']) ? $post['konto'] : null;
                    $utløpsdato = !empty($post['utløpsdato']) ? date_create_immutable($post['utløpsdato']) : null;

                    $garanti = isset($this->data['files']['garanti'][0]['name']) && $this->data['files']['garanti'][0]['name']
                        ? $this->data['files']['garanti'][0]
                        : null;

                    if(isset($get['leieforhold']) && $get['leieforhold'] != $leieforholdId) {
                        throw new Exception('Leieforholdet kan ikke endres');
                    }
                    /** @var Leieforhold $leieforhold */
                    $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);
                    if(!$leieforhold->hentId()) {
                        throw new Exception('Ugyldig leieforhold');
                    }

                    if($depositumId == '*') {
                        /** @var Depositum $depositum */
                        $depositum = $this->app->nyModell(Depositum::class, (object)[
                            'leieforhold' => $leieforhold,
                            'type' => $type,
                            'dato' => $dato,
                            'beløp' => $beløp,
                            'betaler' => $betaler,
                            'notat' => $notat,
                            'konto' => $konto,
                            'utløpsdato' => $utløpsdato,
                        ]);
                    }
                    else {
                        /** @var Depositum $depositum */
                        $depositum = $this->app->hentModell(Depositum::class, $depositumId);
                        if (!$depositum->hentId()) {
                            throw new Exception('Depositum ' . $depositumId . ' finnes ikke');
                        }
                        $depositum->settType($type)
                            ->settDato($dato)
                            ->settBeløp($beløp)
                            ->settBetaler($betaler)
                            ->settNotat($notat)
                            ->settKonto($konto)
                            ->settUtløpsdato($utløpsdato);
                    }
                    if ($garanti) {
                        $this->lastOppOgLagreGaranti($garanti, $depositum);
                    }
                    $resultat->id = $depositum->hentId();
                    $resultat->msg = 'Endringene er lagret';
                    $resultat->url = (string)$this->app->returi->get(1);
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    private function lastOppOgLagreGaranti(array $garanti, Depositum $depositum): Mottak
    {
        if($garanti['error'] ?? null) {
            switch($garanti['error']) {
                case UPLOAD_ERR_INI_SIZE:
                    throw new Exception('Fila kan ikke overstige ' . ini_get('upload_max_filesize') . ' som er satt i php.ini');
                case UPLOAD_ERR_FORM_SIZE:
                    throw new Exception('Fila kan ikke overstige MAX_FILE_SIZE som er angitt i skjemaet');
                case UPLOAD_ERR_PARTIAL:
                    throw new Exception('Fila ble bare delvis lastet opp');
                case UPLOAD_ERR_NO_FILE:
                    throw new Exception('Fila ble ikke med');
                case UPLOAD_ERR_NO_TMP_DIR:
                    throw new Exception('Fila kunne ikke lagres. Midlertidig lagringsplass ble ikke funnet');
                case UPLOAD_ERR_CANT_WRITE:
                    throw new Exception('Fila kunne lagres pga. manglende skrive-tillatelser');
                case UPLOAD_ERR_EXTENSION:
                    throw new Exception('En PHP-extension forhindret opplastingen');
                default:
                    throw new Exception('Det skjedde en feil under opplastingen av avtalen, og den kunne ikke lagres');
            }
        }

        $sti = $this->app->hentFilarkivBane() . '/leieavtaler/';
        $filendelse = pathinfo($garanti['name'], PATHINFO_EXTENSION);
        $filnavn = 'depositumsgaranti-' . $depositum->leieforhold->hentId() . '.' . $filendelse;
        $filinnhold = file_get_contents($garanti["tmp_name"]);
        $sjekk = strtolower($filendelse) === 'pdf' && preg_match("/^%PDF-/", $filinnhold);
        $sjekk = $sjekk || getimagesize($garanti["tmp_name"]);
        if($sjekk == false) {
            throw new Exception('Den opplastede fila er ikke av godkjent format. Last opp som pdf eller et bilde.');
        }

        if(!in_array(strtolower($filendelse), ['jpg','jpeg', 'png', 'gif', 'pdf'])) {
            throw new Exception('Det opplastede bildet må være av typen JPG/JPEG, PNG eller GIF.');
        }

        if ($garanti["size"] > 1000000) {
            throw new Exception('Fila er for stor. Maks. filstørrelse er 1Mb.');
        }

        if (!file_exists($sti)) {
            if(!mkdir($sti)) {
                throw new Exception("Klarte ikke finne eller opprette filplasseringen '{$sti}'.");
            }
        }

        if($depositum->fil && file_exists($depositum->fil)) {
            unlink($depositum->fil);
        }

        if (!move_uploaded_file($garanti["tmp_name"], $sti . $filnavn)) {
            throw new Exception("Fila kunne ikke lagres som '{$filnavn}' pga ukjent feil.");
        }

        $depositum->fil = $filnavn;
        return $this;
    }
}