<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\DepositumSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Depositum;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return Oppdrag
     * @noinspection PhpUnused
     */
    protected function slettDepositum():Oppdrag {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            $depositumId = (int)$this->data['post']['depositum_id'];
            /** @var Depositum $depositum */
            $depositum = $this->app->hentModell(Depositum::class, $depositumId);
            if (!$depositum->hentId()) {
                throw new Exception('Finner ikke dette depositumet');
            }
            $resultat->msg = 'Depositumet har blitt slettet';
            $depositum->slett();
            $resultat->url = (string)$this->app->returi->get(1);
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}