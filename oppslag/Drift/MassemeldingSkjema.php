<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Melding;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Visning\drift\html\massemelding_skjema\Index;
use Kyegil\ViewRenderer\ViewArray;

/**
 * MassemeldingSkjema Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class MassemeldingSkjema extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected
        $oppdrag = [
        //        'oppgave' => _MalSkjema\Oppdrag::class
    ];

    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $mottakerLeieforholdIder = $_SESSION['massemelding']['mottakerLeieforholdIder'] ?? [];
        if (isset($_POST['leieforhold']) && json_decode($_POST['leieforhold'])) {
            $mottakerLeieforholdIder = json_decode($_POST['leieforhold']);
        }
        $this->hoveddata['mottakerLeieforholdIder'] = $mottakerLeieforholdIder;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = "Masse-kommunikasjon";
        return true;
    }

    /**
     * @return Index
     * @throws Exception
     */
    public function innhold()
    {
        $this->slettGamleMidlertidigeFiler();

        return $this->vis(Index::class, [
            'mottakerLeieforholdIder' => $this->hoveddata['mottakerLeieforholdIder'],
            'medium' => $_SESSION['massemelding']['medium'] ?? Melding::MEDIUM_EPOST,
            'emne' => $_SESSION['massemelding']['emne'] ?? '',
            'innhold' => $_SESSION['massemelding']['innhold'] ?? '',
            'svaradresse' => $_SESSION['massemelding']['svaradresse'] ?? null,
            'sms_avsender' => $_SESSION['massemelding']['sms_avsender'] ?? null,
            'kopiOppsummering' => $_SESSION['massemelding']['kopi']['oppsummering'] ?? '',
            'kopiEksempel' => $_SESSION['massemelding']['kopi']['eksempel'] ?? '',
            'vedlegg' => $_SESSION['massemelding']['vedlegg'] ?? []
        ]);
    }

    /**
     * @return ViewArray
     */
    public function skript() {
        $skript = new ViewArray([
            $this->vis(\Kyegil\Leiebasen\Visning\drift\js\massemelding_skjema\Index::class, [
            ]),
        ]);
        return $skript;
    }
}