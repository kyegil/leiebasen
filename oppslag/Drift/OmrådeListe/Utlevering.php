<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\OmrådeListe;


use Exception;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Samling;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\FramleieListe
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null) {
        switch ($data) {
            default: {
                $resultat = (object)array(
                    'success' => true,
                    'data'		=> array()
                );

                $sort		= $_GET['sort'] ?? null;
                $synkende	= isset($_GET['dir']) && $_GET['dir'] == "DESC";
                $start		= intval($_GET['start'] ?? 0);
                $limit		= $_GET['limit'] ?? null;

                $søkefelt = $_GET['søkefelt'] ?? '';

                $filter = [
                    'or' => [
                        "`områder`.`navn` LIKE" =>  '%' . $søkefelt . '%',
                    ]
                ];

                try {
                    $sorteringsfelter = [
                        'id' => [null, 'id'],
                        'navn' => [null, 'navn'],
                    ];
                    $sort = ($sort && isset($sorteringsfelter[$sort])) ? $sorteringsfelter[$sort] : null;

                    /** @var Samling $områdeSett */
                    $områdeSett = $this->app->hentSamling(Område::class)
                        ->leggTilFilter($filter)
                    ;

                    $områdeSett->låsFiltre();

                    if ($sort) {
                        $områdeSett->leggTilSortering($sort[1], $synkende, $sort[0]);
                    }

                    $resultat->totalRows = $områdeSett->hentAntall();
                    if (isset($limit) && $limit < $resultat->totalRows) {
                        $områdeSett
                            ->nullstill()
                            ->settStart($start)
                            ->begrens($limit);
                    }

                    /** @var Område $område */
                    foreach ($områdeSett as $område) {
                        $resultat->data[] = (object)array(
                            'id' => $område->hentId(),
                            'navn' => $område->navn,
                            'beskrivelse' => $område->beskrivelse
                        );
                    }
                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }
        }
    }
}