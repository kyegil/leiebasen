<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
use Kyegil\Leiebasen\Visning;
use Kyegil\ViewRenderer\ViewArray;

/**
 * OversiktGjeld Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class OversiktGjeld extends DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'utskrift' => OversiktGjeld\Utskrift::class
    ];
    /**
     * @return bool
     * @throws \Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = "Gjeldsoversikt";
        return true;
    }


    /**
     * @return Visning
     */
    public function innhold()
    {
        return $this->vis(\Kyegil\Leiebasen\Visning\drift\html\OversiktGjeld::class);
    }

    /**
     * @return Visning
     */
    public function skript(): ViewArray
    {
        $skript = new ViewArray([
            $this->vis(\Kyegil\Leiebasen\Visning\drift\js\oversikt_gjeld\Index::class),
        ]);
        return $skript;
    }
}