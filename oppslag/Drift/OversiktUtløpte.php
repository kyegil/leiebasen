<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;


use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\oversikt_utløpte\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * OversiktUtløpte Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class OversiktUtløpte extends DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Leieforhold i ferd med å utløpe';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => OversiktUtløpte\Oppdrag::class
    ];


    /**
     * OversiktGjeld constructor.
     *
     * @param array $di
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        setlocale(LC_ALL, array("no","nb_NO"));
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $brukerId = $this->bruker['id'];
        $bruker = $this->hentModell(Person::class, $brukerId);
        $this->hoveddata['bruker'] = $bruker;
        return true;
    }

    /**
     * @return Index
     * @throws \Exception
     */
    public function skript() {
        $bruker = $this->hoveddata['bruker'];
        if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }

        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'bruker' => $bruker,
                'tilbakeKnapp' => $this->returi->get()
            ])
        ]);
        return $skript;
    }
}