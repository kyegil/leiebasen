<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\Data;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Leieobjektsett;
use Kyegil\Leiebasen\Modell\Område;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Personsett;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Samling;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\Data
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null):Utlevering {
        switch ($data) {
            case 'komboliste_personer':
                return $this->kombolistePersoner();
            case 'komboliste_leieforhold':
                return $this->kombolisteLeieforhold();
            case 'komboliste_leieobjekter':
                return $this->kombolisteLeieobjekter();
            case 'komboliste_bygninger':
                return $this->kombolisteBygninger();
            case 'komboliste_områder':
                return $this->kombolisteOmråder();
            default:
                $resultat = (object)[
                    'success' => false,
                    'msg' => 'Ønsket data er ikke oppgitt',
                    'data' => []
                ];
                $this->settRespons(new JsonRespons($resultat));
        }
        return $this;
    }

    /**
     * @return $this
     */
    protected function kombolistePersoner(): Utlevering
    {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];
        $start = isset($this->data['get']['start']) && $this->data['get']['start'] > 0 ? $this->data['get']['start'] : 0;
        $limit = isset($this->data['get']['limit']) && is_int($this->data['get']['limit']) ? $this->data['get']['limit'] : null;
        $query = $this->data['get']['query'] ?? ($this->data['get']['q'] ?? '');
        $query = trim($query) ? explode(' ', $query) : [];

        try {
            /** @var Personsett $personsett */
            $personsett = $this->app->hentSamling(Person::class);
            $personsett->setStart($start)->setLimit($limit);

            if ($query) {
                foreach ($query as $ord) {
                    $personsett->leggTilFilter([
                        'or' => [
                            '`personer`.`fornavn` LIKE' => '%' . $ord . '%',
                            '`personer`.`etternavn` LIKE' => '%' . $ord . '%',
                        ]
                    ]);
                }
            }

            foreach ($personsett as $person) {
                $resultat->data[] = (object)[
                    'value' => $person->hentId(),
                    'text' => $person->hentId() . ' ' . $person->hentNavn()
                ];
            }
        } catch (\Throwable $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }

        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @return $this
     */
    protected function kombolisteLeieforhold(): Utlevering
    {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];
        $start = isset($this->data['get']['start']) && $this->data['get']['start'] > 0 ? $this->data['get']['start'] : 0;
        $limit = isset($this->data['get']['limit']) && is_int($this->data['get']['limit']) ? $this->data['get']['limit'] : null;
        $query = $this->data['get']['query'] ?? ($this->data['post']['query'] ?? '');
        $query = trim($query) ? explode(' ', $query) : [];

        try {
            /** @var Leieforholdsett $leieforholdsett */
            $leieforholdsett = $this->app->hentSamling(Leieforhold::class);
            $leieforholdsett->settStart($start)->setLimit($limit);

            if ($query) {
                $leieforholdsett
                    ->leggTilLeftJoin(Leietaker::hentTabell(),
                        Leieforhold::hentTabell() . '.' . Leieforhold::hentPrimærnøkkelfelt()
                        . ' = ' . Leietaker::hentTabell() . '.leieforhold'
                    )
                    ->leggTilLeftJoin(Person::hentTabell(),
                        Leietaker::hentTabell() . '.person'
                        . ' = ' . Person::hentTabell() . '.' . Person::hentPrimærnøkkelfelt()
                    )
                ;
                foreach ($query as $ord) {
                    $leieforholdsett->leggTilFilter([
                        'or' => [
                            'leieforholdnr' => $ord,
                            '`kontraktpersoner`.`leietaker` LIKE' => '%' . $ord . '%',
                            '`personer`.`fornavn` LIKE' => '%' . $ord . '%',
                            '`personer`.`etternavn` LIKE' => '%' . $ord . '%',
                        ]
                    ]);
                }
            }

            foreach ($leieforholdsett as $leieforhold) {
                $resultat->data[] = (object)[
                    'value' => $leieforhold->hentId(),
                    'text' => $leieforhold->hentId() . ' ' . $leieforhold->hentBeskrivelse()
                ];
            }
        } catch (\Throwable $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }

        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @return $this
     */
    protected function kombolisteLeieobjekter(): Utlevering
    {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];
        $start = isset($this->data['get']['start']) && $this->data['get']['start'] > 0 ? $this->data['get']['start'] : 0;
        $limit = isset($this->data['get']['limit']) && is_int($this->data['get']['limit']) ? $this->data['get']['limit'] : null;
        $query = $this->data['get']['query'] ?? '';
        $query = trim($query) ? explode(' ', $query) : [];

        try {
            /** @var Leieobjektsett $leieobjektsett */
            $leieobjektsett = $this->app->hentSamling(Leieobjekt::class);
            $leieobjektsett->setStart($start)->setLimit($limit);

            if ($query) {
                foreach ($query as $ord) {
                    $leieobjektsett->leggTilFilter([
                        'or' => [
                            'leieobjektnr' => $ord,
                            '`leieobjekter`.`navn` LIKE' => '%' . $ord . '%',
                            '`leieobjekter`.`gateadresse` LIKE' => '%' . $ord . '%',
                            '`leieobjekter`.`beskrivelse` LIKE' => '%' . $ord . '%',
                        ]
                    ]);
                }
            }

            /** @var Leieobjekt $leieobjekt */
            foreach ($leieobjektsett as $leieobjekt) {
                $resultat->data[] = (object)[
                    'value' => $leieobjekt->hentId(),
                    'text' => $leieobjekt->hentId() . ' ' . $leieobjekt->hentBeskrivelse()
                ];
            }
        } catch (\Throwable $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }

        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @return $this
     */
    protected function kombolisteBygninger(): Utlevering
    {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];
        $start = isset($this->data['get']['start']) && $this->data['get']['start'] > 0 ? $this->data['get']['start'] : 0;
        $limit = isset($this->data['get']['limit']) && is_int($this->data['get']['limit']) ? $this->data['get']['limit'] : null;
        $query = $this->data['get']['query'] ?? '';
        $query = trim($query) ? explode(' ', $query) : [];

        try {
            /** @var Samling $bygningssamling */
            $bygningssamling = $this->app->hentSamling(Bygning::class);
            $bygningssamling->setStart($start)->setLimit($limit);

            if ($query) {
                foreach ($query as $ord) {
                    $bygningssamling->leggTilFilter([
                        'or' => [
                            'kode' => $ord,
                            '`bygninger`.`navn` LIKE' => '%' . $ord . '%',
                        ]
                    ]);
                }
            }

            /** @var Bygning $bygning */
            foreach ($bygningssamling as $bygning) {
                $resultat->data[] = (object)[
                    'value' => $bygning->hentId(),
                    'text' => $bygning->hentKode() . ' ' . $bygning->hentNavn()
                ];
            }
        } catch (\Throwable $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }

        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @return $this
     */
    protected function kombolisteOmråder(): Utlevering
    {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];
        $start = isset($this->data['get']['start']) && $this->data['get']['start'] > 0 ? $this->data['get']['start'] : 0;
        $limit = isset($this->data['get']['limit']) && is_int($this->data['get']['limit']) ? $this->data['get']['limit'] : null;
        $query = $this->data['get']['query'] ?? '';
        $query = trim($query) ? explode(' ', $query) : [];

        try {
            /** @var Samling $områdesamling */
            $områdesamling = $this->app->hentSamling(Område::class);
            $områdesamling->setStart($start)->setLimit($limit);

            if ($query) {
                foreach ($query as $ord) {
                    $områdesamling->leggTilFilter([
                        '`områder`.`navn` LIKE' => '%' . $ord . '%'
                    ]);
                }
            }

            /** @var Område $område */
            foreach ($områdesamling as $område) {
                $resultat->data[] = (object)[
                    'value' => $område->hentId(),
                    'text' => $område->hentNavn()
                ];
            }
        } catch (\Throwable $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }

        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}