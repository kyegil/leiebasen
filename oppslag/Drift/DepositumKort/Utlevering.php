<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\DepositumKort;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Depositum;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Respons\PdfRespons;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\DepositumKort
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     * @throws Exception
     * @link https://datatables.net/manual/server-side
     */
    public function hentData($data = null): Utlevering
    {
        $get = $this->data['get'];
        $post = $this->data['post'];

        switch ($data) {
            case 'garanti': {
                try {
                    $depositumId = $get['id'] ?? null;
                    
                    /** @var Depositum $depositum */
                    $depositum = $this->app->hentModell(Depositum::class, $depositumId);
                    $garantiFil = $depositum->hentFil();
                    if(!trim($garantiFil)) {
                        $this->app->responder404();
                    }

                    $sti = $this->app->hentFilarkivBane() . '/leieavtaler/';

                    $filendelse = pathinfo($garantiFil, PATHINFO_EXTENSION);

                    if (!$garantiFil ||
                        !file_exists($sti . $garantiFil)
                        || !is_readable($sti . $garantiFil)
                    ) {
                        $this->app->responder404();
                    }
                    $filinnhold = file_get_contents($sti . $garantiFil);
                    if (!$filinnhold) {
                        $this->app->responder404();
                    }

                    switch(strtolower($filendelse)) {
                        case 'pdf':
                            $respons = new PdfRespons($filinnhold);
                            $respons->filnavn = $garantiFil;
                            $this->settRespons($respons);
                            break;
                        default:
                            $respons = new Respons($filinnhold);
                            $respons->filnavn = $garantiFil;
                            $respons->header('Content-type: image/' . ($filendelse == 'jpg' ? 'jpeg' : $filendelse));
                            $respons->header('Content-Disposition: attachment; filename="' . $garantiFil . '";');
                            $respons->header('Content-Length: ' . strlen($filinnhold));
                            $this->settRespons($respons);
                            break;
                    }
                } catch (Exception $e) {
                    return $this->settRespons(new Respons($e->getMessage()));
                }
                return $this;
            }
            default:
                $resultat = (object)[
                    'success' => false,
                    'msg' => 'Ønsket data er ikke oppgitt',
                    'data' => []
                ];
                $this->settRespons(new JsonRespons($resultat));
                return $this;
        }
    }
}