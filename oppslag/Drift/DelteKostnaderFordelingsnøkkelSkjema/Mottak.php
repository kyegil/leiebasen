<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderFordelingsnøkkelSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel\Element;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Fordelingsnøkkel\Elementsett;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Mottak extends AbstraktMottak
{
    /**
     * @param string|null $skjema
     * @return $this
     */
    public function taIMot($skjema = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($skjema) {
                default:
                    $tjenesteId = $this->data['get']['id'] ?? null;
                    $elementer = $this->data['post']['element'] ?? [];

                    /** @var Tjeneste $tjeneste */
                    $tjeneste = $this->app->hentModell(Tjeneste::class, $tjenesteId);
                    if (!$tjeneste->hentId()) {
                        throw new Exception('Ugyldig tjeneste' . $tjenesteId);
                    }
                    $fordelingsnøkkel = $tjeneste->hentFordelingsnøkkel();
                    /** @var Elementsett|array $eksisterendeElementer */
                    $eksisterendeElementer = $fordelingsnøkkel ? $fordelingsnøkkel->hentElementer() : [];

                    $elementIder = [];
                    foreach ($elementer as $elementData) {
                        $fordelingsmåte = in_array($elementData['fordelingsmåte'], array_keys(Fordelingsnøkkel::ELEMENTORDEN)) ? $elementData['fordelingsmåte'] : Element::FORDELINGSMÅTE_ANDELER;
                        $leieobjekt = $this->app->hentModell(Leieobjekt::class, $elementData['leieobjekt'] ?? null);
                        $leieforhold = $this->app->hentModell(Leieforhold::class, $elementData['leieforhold'] ?? null);

                        if ($elementData['id'] == '*') {
                            /** @var Element $fordelingselement */
                            $fordelingselement = $this->app->nyModell(Element::class, (object)[
                                'fordelingsnøkkel' => $tjeneste,
                                'fordelingsmåte' => $fordelingsmåte,
                                'følger_leieobjekt' => boolval($elementData['følger_leieobjekt'] ?? true),
                                'leieobjekt' => $leieobjekt->hentId() ? $leieobjekt : null,
                                'leieforhold' => $leieforhold->hentId() ? $leieforhold : null,
                                'andeler' => $elementData['andeler'] ?? 1,
                                'prosentsats' => ($elementData['prosentsats'] ?? 0) / 100,
                                'fastbeløp' => $elementData['fastbeløp'] ?? 0,
                                'forklaring' => $elementData['forklaring'] ?? '',
                            ]);
                            $elementIder[] = $fordelingselement->hentId();
                        }
                        else {
                            /** @var Element $fordelingselement */
                            $fordelingselement = $this->app->hentModell(Element::class, $elementData['id'] ?? null);
                            if ($fordelingselement->hentId() && $fordelingselement->fordelingsnøkkel->id == $fordelingsnøkkel->id) {
                                $fordelingselement->fordelingsmåte = $fordelingsmåte;
                                $fordelingselement->følgerLeieobjekt = boolval($elementData['følger_leieobjekt'] ?? true);
                                $fordelingselement->leieobjekt = $leieobjekt->hentId() ? $leieobjekt : null;
                                $fordelingselement->leieforhold = $leieforhold->hentId() ? $leieforhold : null;
                                $fordelingselement->forklaring = $elementData['forklaring'] ?? '';

                                switch($fordelingsmåte) {
                                    case Element::FORDELINGSMÅTE_FAST:
                                        $fordelingselement->fastbeløp = $elementData['fastbeløp'] ?? 0;
                                        break;
                                    case Element::FORDELINGSMÅTE_PROSENT:
                                        $fordelingselement->prosentsats = ($elementData['prosentsats'] ?? 0) / 100;
                                        break;
                                    default:
                                        $fordelingselement->andeler = $elementData['andeler'] ?? 1;
                                }
                                $elementIder[] = $fordelingselement->hentId();
                            }
                        }
                    }

                    /** @var Element $eksisterendeElement */
                    foreach( $eksisterendeElementer as $eksisterendeElement) {
                        if( !in_array($eksisterendeElement->hentId(), $elementIder)) {
                            $eksisterendeElement->slett();
                        }
                    }

                    if ($fordelingsnøkkel) {
                        $fordelingsnøkkel->nullstill();
                    }
                    $resultat->id = $tjeneste->hentId();
                    $resultat->msg = 'Lagret';
                    $resultat->url = strval($this->app->returi->get());
                    break;
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

}