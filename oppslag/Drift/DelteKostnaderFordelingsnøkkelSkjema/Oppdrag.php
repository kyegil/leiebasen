<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderFordelingsnøkkelSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function slettTjeneste() {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            $tjenesteId = $this->data['post']['tjeneste_id'];
            /** @var Tjeneste $tjeneste */
            $tjeneste = $this->app->hentModell(Tjeneste::class, $tjenesteId);
            if (!$tjeneste->hentId()) {
                throw new Exception('Finner ikke denne tjenesten');
            }
            $resultat->msg = 'Tjenesten har blitt slettet';
            $tjeneste->slett();
            $resultat->url = $this->app->returi->get();
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}