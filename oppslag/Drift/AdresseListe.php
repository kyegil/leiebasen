<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\adresse_liste\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * AdresserListe Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class AdresseListe extends DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Adresseliste';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $brukerId = $this->bruker['id'];
        /** @var Person $bruker */
        $bruker = $this->hentModell(Person::class, $brukerId);
        $this->hoveddata['bruker'] = $bruker;
        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     * @throws Exception
     */
    public function skript() {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];

        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'bruker' => $bruker
            ])
        ]);
        return $skript;
    }
}