<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 17/06/2020
 * Time: 13:33
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\Utskriftsveiviser;


use Exception;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Utskriftsprosessor;
use Kyegil\Leiebasen\Vedlikehold;

class Mottak extends AbstraktMottak
{
    /** @var Utskriftsprosessor */
    protected $utskriftsprosessor;

    /**
     * @param \Kyegil\Leiebasen\Oppslag\AbstraktKontroller $app
     * @param $data
     */
    public function __construct(\Kyegil\Leiebasen\Oppslag\AbstraktKontroller $app, $data = null)
    {
        parent::__construct($app, $data);
        $this->utskriftsprosessor = $app->hentUtskriftsProsessor();
    }

    /**
     * @param string|null $skjema
     * @return $this|Mottak
     */
    public function taIMot(string $skjema): Mottak {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($skjema) {
                case 'girotekst':
                    $resultat = (object)array(
                        'success'	=> true,
                        'msg' => 'Giroteksten er lagret.'
                    );

                    $girotekst = $_POST['girotekst'] ?? $this->app->hentValg('girotekst');
                    $this->app->settValg('girotekst', $girotekst);
                    break;
                case 'utskriftsbekreftelse':
                    $resultat = (object)array(
                        'success'	=> true,
                        'msg' => 'Utskriften er registrert.',
                        'url' => null
                    );
                    $adresser = $this->app->utskriftsadresser();
                    if( is_array( $adresser ) ) {
                        $adresser = array_map( 'strval', $adresser);
                    }

                    try {
                        $this->app->logger->info('GIROUTSKRIFT – Registrerer pågående utskrift.');
                        if (!$utskriftsforsøk = $this->utskriftsprosessor->hentLagretUtskriftsforsøk()) {
                            throw new Exception("Utskriften ble ikke funnet");
                        }
                        $this->utskriftsprosessor->sendOgRegistrerUtskrift($utskriftsforsøk);
                        $this->utskriftsprosessor->lagreUtskriftsforsøk(null);

                        foreach ($this->app->returi->getTrace() as $url) {
                            if (strpos($url->url, 'utskriftsveiviser') !== false) {
                                $this->app->returi->set($url->url, $url->title);
                                break;
                            }
                        }
                        $resultat->url = $this->app->returi->get(1)->url;
                    } catch (Exception $e) {
                        $this->app->logger->error('GIROUTSKRIFT – ' . $e);
                        $resultat->success = false;
                        $resultat->msg = $e->getMessage();
                    }

                    $resultat->adresser = (array)$adresser;
                    break;
                default:
                    throw new Exception('Ugyldig skjema');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        Vedlikehold::getInstance($this->app)->sjekkRegningsUtskrifter();
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}