<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 04/12/2020
 * Time: 23:21
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\Utskriftsveiviser;


use Error;
use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Modell\Leieforhold\Regningsett;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Oppslag\AbstraktKontroller;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Utskriftsforsøk;
use Kyegil\Leiebasen\Utskriftsprosessor;
use Kyegil\Leiebasen\UtskriftStatus;
use Kyegil\Leiebasen\Vedlikehold;
use Throwable;

class Oppdrag extends AbstraktOppdrag
{
    /** @var Utskriftsprosessor */
    protected Utskriftsprosessor $utskriftsprosessor;

    /**
     * @param AbstraktKontroller $app
     * @param $data
     */
    public function __construct(AbstraktKontroller $app, $data = null)
    {
        parent::__construct($app, $data);
        $this->utskriftsprosessor = $app->hentUtskriftsProsessor();
    }

    /**
     * @return $this
     */
    protected function opprettUtskrift(): Oppdrag
    {
        $utskriftstatus = new UtskriftStatus();
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => [],
            'url' => null
        ];

        try {
            if ($this->app->hentUtskriftsprosessor()->hentLagretUtskriftsforsøk()) {
                throw new Exception('En annen utskrift har blitt igangsatt parallelt med denne, og må godkjennes før ny utskrift kan påbegynnes.');
            }
            set_time_limit(300);
            $utskriftstatus->oppdaterStatus(UtskriftStatus::STATUS_IGANGSATT, 0, '');
            /** @var Utskriftsforsøk $konfigurasjon */
            $konfigurasjon = $this->hentUtskriftsKonfigurasjon();
            $utskriftstatus->oppdaterStatus(UtskriftStatus::STATUS_PÅGÅR, 0.2, 'Utskriften er forberedt');

            $konfigurasjon->giroer = $this->utskriftsprosessor->filtrerUtBetalteRegninger($konfigurasjon->giroer);

            $this->utskriftsprosessor->lagUtskriftsfil($konfigurasjon);
            $utskriftstatus->oppdaterStatus(UtskriftStatus::STATUS_KOMPLETT, 1, 'Komplett');
            $this->app->logger->notice('GIROUTSKRIFT – komplett', ['memory_usage' => memory_get_usage()]);
        }
        catch (Throwable $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            $utskriftstatus->oppdaterStatus(UtskriftStatus::STATUS_MISLYKKET, 1, $e->getMessage());

            if ($e instanceof Error) {
                $this->app->logger->critical('GIROUTSKRIFT – ' . $e);
                throw $e;
            }
            else {
                $this->app->logger->error('GIROUTSKRIFT – ' . $e);
            }
        }
        $this->settRespons(new JsonRespons($resultat));
        Vedlikehold::getInstance($this->app)->sjekkRegningsUtskrifter();
        return $this;
    }

    /**
     * @return $this
     */
    protected function forkastUtskrift(): Oppdrag {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => [],
            'url' => null
        ];

        try {
            $this->utskriftsprosessor->forkastUtskrift();
            foreach ($this->app->returi->getTrace() as $url) {
                if (strpos($url->url, 'utskriftsveiviser') !== false) {
                    $this->app->returi->set($url->url, $url->title);
                    break;
                }
            }
            $resultat->url = $this->app->returi->get(1)->url;
        }
        catch (Throwable $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();

            if ($e instanceof Error) {
                $this->app->logger->critical('GIROUTSKRIFT – ' . $e);
                throw $e;
            }
            else {
                $this->app->logger->error('GIROUTSKRIFT – ' . $e);
            }
        }
        $this->settRespons(new JsonRespons($resultat));
        Vedlikehold::getInstance($this->app)->sjekkRegningsUtskrifter();
        return $this;
    }

    /**
     * @return Utskriftsforsøk
     * @throws Exception
     */
    private function hentUtskriftsKonfigurasjon(): Utskriftsforsøk
    {
        if ($utskriftsKonfigurasjon = $this->app->hentUtskriftsprosessor()->hentLagretUtskriftsforsøk()) {
            $this->app->logger->info('GIROUTSKRIFT – Data lastes fra eksisterende serialisert utskriftsforsøk');
            /** @var Utskriftsforsøk $utskriftsKonfigurasjon */
            return $utskriftsKonfigurasjon;
        }
        $this->app->logger->info('GIROUTSKRIFT – Data lastes fra POST-data', ['post' => $_POST]);
        $kravArray = array_map('intval', (array)($this->data['post']['krav'] ?? []));
        $purreregningIder = array_map('intval', (array)($this->data['post']['purreregninger'] ?? []));
        $statusoversikter = array_map('intval', (array)($this->data['post']['statusoversikter'] ?? []));
        $gebyrkontrakter = array_map('intval', (array)($this->data['post']['purregebyr'] ?? []));

        /** @var Kravsett $kravsett */
        $kravsett = $this->app->hentSamling(Krav::class)->filtrerEtterIdNumre($kravArray);
        /** @var Regningsett $regningsettForPurringer */
        $regningsettForPurringer = $this->app->hentSamling(Regning::class)->filtrerEtterIdNumre($purreregningIder);
        /** @var Leieforholdsett $leieforholsettForstatusoversikter */
        $leieforholsettForstatusoversikter = $this->app->hentSamling(Leieforhold::class)->filtrerEtterIdNumre($statusoversikter);
        /** @var Leieforholdsett $leieforholdsettForGebyr */
        $leieforholdsettForGebyr = $this->app->hentSamling(Leieforhold::class)->filtrerEtterIdNumre($gebyrkontrakter);
        /** @var array $purreformater */
        $purreformater = $this->data['post']['purreformat'] ?? [];

        $adskilt = isset($this->data['post']['adskilt']) && $this->data['post']['adskilt'];
        $kombipurring = isset($this->data['post']['kombipurring']) && $this->data['post']['kombipurring'];

        /** @var Utskriftsforsøk $utskriftsKonfigurasjon */
        $utskriftsKonfigurasjon = $this->app->hentUtskriftsprosessor()->forberedUtskrift(
            $kravsett,
            $adskilt,
            $regningsettForPurringer,
            $kombipurring,
            $purreformater,
            $leieforholdsettForGebyr,
            $leieforholsettForstatusoversikter
        );
        $this->app->hentUtskriftsprosessor()->lagreUtskriftsforsøk($utskriftsKonfigurasjon);
        $this->app->logger->info('GIROUTSKRIFT – Utskriftskonfigurasjonen opprettet', ['memory_usage' => memory_get_usage()]);
        return $utskriftsKonfigurasjon;
    }
}