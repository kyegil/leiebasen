<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\Utskriftsveiviser;


use Exception;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\UtskriftStatus;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\Utskriftsveiviser
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData($data = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case "utskrift_status":
                    $utskriftStatus = new UtskriftStatus();
                    $resultat = $utskriftStatus->hent();
                    if ($utskriftStatus->hentStatus() != UtskriftStatus::STATUS_PÅGÅR) {
                        $utskriftStatus->exitier();
                    }
                    break;

                default:
                    throw new Exception('Ønsket data er ikke oppgitt');
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}