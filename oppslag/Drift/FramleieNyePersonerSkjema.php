<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleie;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\framleie_nye_personer_skjema\Trinn1ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * FramleieNyePersonerSkjema Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class FramleieNyePersonerSkjema extends DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Framleie';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
//        'oppgave' => FramleieSkjema\Oppdrag::class
    ];


    /**
     * FramleieNyePersonerSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $framleieId = !empty($_GET['id']) ? $_GET['id'] : null;
        $leieforholdId = $_GET['leieforhold'] ?? null;
        $trinn = intval($_GET['trinn'] ?? 1);
        $this->hoveddata['er_org'] = isset($_POST['er_org']) ? (bool)$_POST['er_org'] : null;
        $this->hoveddata['fornavn'] = isset($_POST['fornavn']) ? trim($_POST['fornavn']) : null;
        $this->hoveddata['etternavn'] = isset($_POST['etternavn']) ? trim($_POST['etternavn']) : null;
        $this->hoveddata['framleieforhold'] = $this->hoveddata['leieforhold'] = null;
        $this->hoveddata['engangspolett'] = $_POST['engangspolett'] ?? $this->opprettPolett();
        $adressekortId = $_POST['adressekort'] ?? null;
        $this->hoveddata['adressekort'] = $adressekortId ? $this->hentModell(Person::class, $adressekortId) : null;

        if(!$this->hoveddata['etternavn']) {
            $trinn = 1;
        }

        $this->hoveddata['trinn'] = $trinn;
        if ((int)$framleieId) {
            $this->hoveddata['framleieforhold'] = $this->hentModell(Framleie::class, $framleieId);
        }
        else if ($leieforholdId) {
            $this->hoveddata['leieforhold'] = $this->hentModell(Leieforhold::class, $leieforholdId);
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Framleie $framleieforhold */
        $framleieforhold = $this->hoveddata['framleieforhold'];
        if (
            $framleieforhold
            && !$framleieforhold->hentId()
        ) {
            return false;
        }
        $this->tittel = 'Legg til leietakere til framleieavtale.';

        return true;
    }

    /**
     * @return Index
     * @throws Exception
     */
    public function skript() {
        $trinn = $this->hoveddata['trinn'] ?? 1;
        /** @var Framleie|null $framleieforhold */
        $framleieforhold = $this->hoveddata['framleieforhold'];
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $framleieforhold ? $framleieforhold->leieforhold : null;
        if(!$leieforhold || !$leieforhold->hentId()) {
            $leieforhold = $this->hoveddata['leieforhold'];
        }

        if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }

        switch ($trinn) {
            case 2:
                $skript = $this->vis(Index::class, [
                    'extOnReady' => $this->vis(Trinn1ExtOnReady::class, [
                        'framleieforhold' => $framleieforhold,
                        'leieforhold' => $leieforhold,
                        'tilbakeKnapp' => $this->returi->get(),
                        'engangspolett' => $this->hoveddata['engangspolett'],
                        'erOrg' => $this->hoveddata['er_org'],
                        'fornavn' => $this->hoveddata['fornavn'],
                        'etternavn' => $this->hoveddata['etternavn']
                    ])
                        ->setTemplate('drift/js/framleie_nye_personer_skjema/Trinn2ExtOnReady.js')
                ])
                ;
                break;
            case 3:
                $skript = $this->vis(Index::class, [
                    'extOnReady' => $this->vis(Trinn1ExtOnReady::class, [
                        'framleieforhold' => $framleieforhold,
                        'leieforhold' => $leieforhold,
                        'tilbakeKnapp' => $this->returi->get(),
                        'engangspolett' => $this->hoveddata['engangspolett'],
                        'erOrg' => $this->hoveddata['er_org'],
                        'fornavn' => $this->hoveddata['fornavn'],
                        'etternavn' => $this->hoveddata['etternavn'],
                        'adressekortId' => $this->hoveddata['adressekort'] ? $this->hoveddata['adressekort']->hentId() : null
                    ])
                        ->setTemplate('drift/js/framleie_nye_personer_skjema/Trinn3ExtOnReady.js')
                ])
                ;
                break;
            default:
                        $skript = $this->vis(Index::class, [
                    'extOnReady' => $this->vis(Trinn1ExtOnReady::class, [
                        'framleieforhold' => $framleieforhold,
                        'leieforhold' => $leieforhold,
                        'tilbakeKnapp' => $this->returi->get(),
                        'engangspolett' => $this->hoveddata['engangspolett'],
                        'erOrg' => $this->hoveddata['er_org'],
                        'fornavn' => $this->hoveddata['fornavn'],
                        'etternavn' => $this->hoveddata['etternavn']
                    ])
                ]);
                break;
        }
        return $skript;
    }
}