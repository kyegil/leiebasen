<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\AdgangSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Mottak extends AbstraktMottak
{
    /**
     * @param string|null $skjema
     * @return $this
     */
    public function taIMot($skjema = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($skjema) {
                default:
                    $adgangsId = $this->data['get']['id'] ?? null;
                    $personId = $this->data['post']['person'] ?? null;
                    $adgangsOmråde = isset($this->data['post']['område']) ? trim($this->data['post']['område']) : null;
                    $leieforholdId = $this->data['post']['leieforhold'] ?? null;

                    $epostvarsling = isset($this->data['post']['epostvarsling']) && $this->data['post']['epostvarsling'];
                    $innbetalingsbekreftelse = isset($this->data['post']['innbetalingsbekreftelse']) && $this->data['post']['innbetalingsbekreftelse'];
                    $forfallsvarsel = isset($this->data['post']['forfallsvarsel']) && $this->data['post']['forfallsvarsel'];
                    $umiddelbartBetalingsvarselEpost = isset($this->data['post']['umiddelbart_betalingsvarsel_epost']) && $this->data['post']['umiddelbart_betalingsvarsel_epost'];

                    $brukernavn = isset($this->data['post']['brukernavn']) ? trim($this->data['post']['brukernavn']) : null;
                    $epost = isset($this->data['post']['epost']) ? trim($this->data['post']['epost']) : null;
                    $passord1 = $this->data['post']['passord1'] ?? null;
                    $passord2 = $this->data['post']['passord2'] ?? null;
                    $endrePassord = isset($this->data['post']['endre_passord']) && $this->data['post']['endre_passord'] && $passord1;
                    if (!$brukernavn) {
                        throw new Exception('Brukernavn ikke angitt');
                    }
                    if (!$endrePassord) {
                        $passord1 = $passord2 = null;
                    }
                    if ($endrePassord && !$passord1) {
                        throw new Exception('Passord ikke angitt');
                    }

                    if ($endrePassord && $passord1 !== $passord2) {
                        throw new Exception('Passord-bekreftelsen matchet ikke angitt passord');
                    }

                    if (!$adgangsId) {
                        throw new Exception('Adgang-id er ikke angitt');
                    }
                    if ($adgangsOmråde == Adgang::ADGANG_OPPFØLGING) {
                        throw new Exception('Adgang til oppfølging kan bare administreres fra oppfølgings-området');
                    }

                    $filter = [
                        'or' => [
                            ['adgangsid' => $adgangsId],
                            'and' => [
                                ['personid' => $personId],
                                ['adgang' => $adgangsOmråde]
                            ]
                        ]
                    ];
                    if ($adgangsOmråde == Adgang::ADGANG_MINESIDER) {
                        $filter['or']['and'][] = ['leieforhold' => $leieforholdId];
                    }
                    /** @var Adgang|null $adgang */
                    $adgang = $this->app->hentSamling(Adgang::class)
                        ->leggTilFilter($filter)->hentFørste();
                    /** @var Person $person */
                    $person = $adgang ? $adgang->person : $this->app->hentModell(Person::class, $personId);
                    $adgangsOmråde = $adgang ? $adgang->adgang : $adgangsOmråde;

                    if (!$person->hentId()) {
                        throw new Exception('Ugyldig bruker');
                    }
                    if (!in_array($adgangsOmråde, Adgang::hentAdgangsområder())) {
                        throw new Exception('Ugyldig adgangsområde');
                    }

                    if (!$this->app->hentAutoriserer()->erLoginTilgjengelig($brukernavn, $person->hentId())) {
                        throw new Exception('Dette brukernavnet kan ikke benyttes. Det er allerede i bruk.');
                    }
                    $brukerProfil = $person->hentBrukerProfil();
                    if ($brukerProfil) {
                        $brukerProfil->settBrukernavn($brukernavn);
                    }
                    else {
                        $brukerProfil = $this->app->nyModell(Person\Brukerprofil::class, (object)[
                            'login' => $brukernavn,
                            'person' => $person
                        ]);
                    }

                    $epost = $epost ?? $person->epost;
                    if (!$epost) {
                        throw new Exception('E-post mangler');
                    }
                    $person->settEpost($epost);

                    if ($endrePassord) {
                        $brukerProfil->settPassord(password_hash($passord1, PASSWORD_DEFAULT));
                    }

                    if (!$adgang && $adgangsId == '*') {
                        $leieforhold = null;
                        if ($adgangsOmråde == Adgang::ADGANG_MINESIDER) {
                            /** @var Leieforhold $leieforhold */
                            $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);
                            if (!$leieforhold->hentId()) {
                                throw new Exception('Ugyldig leieforhold');
                            }
                        }
                        /** @var Adgang $adgang */
                        $adgang = $this->app->nyModell(Adgang::class, (object)[
                            'person' => $person,
                            'adgang' => $adgangsOmråde,
                            'leieforhold' => $leieforhold
                        ]);
                    }

                    $adgang->epostvarsling = $epostvarsling;
                    $adgang->innbetalingsbekreftelse = $innbetalingsbekreftelse;
                    $adgang->forfallsvarsel = $forfallsvarsel;
                    $adgang->umiddelbartBetalingsvarselEpost = $umiddelbartBetalingsvarselEpost ?: null;

                    $resultat->id = $adgang->hentId();
                    $resultat->msg = 'Lagret';
                    $resultat->url = strval($this->app->returi->get(1));
                    break;
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

}