<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 04/12/2020
 * Time: 23:21
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\AdgangSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function slettAdgang() {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            $adgangId = $this->data['post']['adgang_id'];
            /** @var Adgang $adgang */
            $adgang = $this->app->hentModell(Adgang::class, $adgangId);
            if (!$adgang->hentId()) {
                throw new Exception('Finner ikke denne adgangen');
            }
            $resultat->msg = 'Adgangen har blitt slettet';
            $adgang->slett();
            $resultat->url = $this->app->returi->get();
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}