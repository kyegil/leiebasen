<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\AdgangSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforholdsett;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Modell\Personsett;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\OmrådeSkjema
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData($data = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case 'brukerprofil':
                    $resultat->data = (object)[
                        'brukernavn' => '',
                        'epost' => ''
                    ];
                    $personId = $this->data['get']['personid'] ?? null;
                    /** @var Person $person */
                    $person = $this->app->hentModell(Person::class, $personId);
                    if ($person->hentId()) {
                        $brukerProfil = $person->hentBrukerProfil();
                        $resultat->data->brukernavn = $brukerProfil ? $brukerProfil->hentBrukernavn() : '';
                        $resultat->data->epost = $person->hentEpost();
                    }
                    break;

                case 'personer':
                    $filter = [];
                    if (isset($this->data['get']['query']) && trim($this->data['get']['query'])) {
                        $query = explode(' ', trim($this->data['get']['query']));
                        foreach ($query as $ord) {
                            $filter[] = [
                                "REPLACE(LOWER(CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`)), 'å', 'aa') LIKE" => '%' . str_ireplace(array('Å', 'å'), 'aa', $ord) . '%',
                            ];
                        }
                    }
                    /** @var Personsett $leieforholdSett */
                    $leieforholdSett = $this->app->hentSamling(Person::class)
                        ->leggTilSortering('fornavn')
                        ->leggTilSortering('etternavn')

                        ->leggTilFilter($filter);

                    foreach ($leieforholdSett as $person) {
                        $resultat->data[] = [
                            'value' => $person->hentId(),
                            'text' => $person->hentId() . ' | ' . $person->hentNavn(),
                        ];
                    }
                    break;

                case 'leieforhold':
                    $query = '';
                    if (isset($this->data['get']['query']) && trim($this->data['get']['query'])) {
                        $query = trim($this->data['get']['query']);
                    }
                    /** @var Leieforholdsett $leieforholdSett */
                    $leieforholdSett = $this->app->hentSamling(Leieforhold::class);
                    $leieforholdSett->leggTilLeietakerNavnFilter($query)
                        ->leggTilSortering(Leieforhold::hentPrimærnøkkelfelt(), true);

                    foreach ($leieforholdSett as $leieforhold) {
                        $resultat->data[] = [
                            'value' => $leieforhold->hentId(),
                            'text' => $leieforhold->hentId() . ' | ' . $leieforhold->hentBeskrivelse(),
                        ];
                    }
                    break;

                default:
                    $adgangId = !empty($_GET['id']) ? $_GET['id'] : null;
                    if (!$adgangId) {
                        throw new Exception('id-parameter mangler');
                    }
                    /** @var Adgang $adgang */
                    $adgang = $this->app->hentModell(Adgang::class, $adgangId);
                    if ($adgang->hentId()) {
                        $resultat->data = (object) [
                            'id' => $adgang->hentId(),
                        ];
                    }

                    else {
                        throw new Exception('denne adgangen finnes ikke');
                    }
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}