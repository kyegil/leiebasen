<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;


/**
 * UtskriftsveiviserPurringer Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class UtskriftsveiviserPurringer extends \Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Utskriftsveiviser – Velg krav/regninger som skal purres';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $brukerId = $this->bruker['id'];
        $bruker = $this->hentModell(Person::class, $brukerId);
        $this->hoveddata['bruker'] = $bruker;
        /** @var int[] $inkLeieforhold */
        $inkLeieforhold = isset($_GET['ink_leieforhold']) && $_GET['ink_leieforhold'] ? array_map('intval', explode(',', $_GET['ink_leieforhold'])) : [];
        /** @var int[] $eksLeieforhold */
        $eksLeieforhold = isset($_GET['eks_leieforhold']) && $_GET['eks_leieforhold'] ? array_map('intval', explode(',', $_GET['eks_leieforhold'])) : [];
        $adskilt = isset($_GET['adskilt']) && $_GET['adskilt'];
        /** @var int[] $kravIder */
        $kravIder = $_POST['krav'] ?? [];

        /** @var Kravsett $kravsett */
        $kravsett = $this->hentSamling(Krav::class)->filtrerEtterIdNumre($kravIder);

        $ubetalteKrav = $this->hentUtskriftsprosessor()->hentKravsettForPurring(
            null,
            $inkLeieforhold,
            $eksLeieforhold
        );

        $this->hoveddata['kravsett'] = $kravsett;
        $this->hoveddata['adskilt'] = $adskilt;
        $this->hoveddata['ubetalteKrav'] = $ubetalteKrav;

        return true;
    }

    /**
     * @return Index
     */
    public function skript() {
        /** @var Person|null $bruker */
        $bruker = $this->hoveddata['bruker'];
        /** @var Kravsett|null $kravsett */
        $kravsett = $this->hoveddata['kravsett'];
        /** @var Kravsett|null $kravsett */
        $ubetalteKrav = $this->hoveddata['ubetalteKrav'];
        /** @var bool $adskilt */
        $adskilt  = $this->hoveddata['adskilt'];
        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(\Kyegil\Leiebasen\Visning\drift\js\utskriftsveiviser_purringer\ExtOnReady::class, [
                'bruker' => $bruker,
                'kravsett' => $kravsett,
                'ubetalteKrav' => $ubetalteKrav,
                'adskilt' => $adskilt,
            ])
        ]);
        return $skript;
    }
}