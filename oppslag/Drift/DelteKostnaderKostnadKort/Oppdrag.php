<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderKostnadKort;


use Exception;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderKostnadListe;
use Kyegil\Leiebasen\Respons\JsonRespons;

/**
 * @property DelteKostnaderKostnadListe $app
 */
class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function avfordel(): Oppdrag {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => null
        ];

        try {
            $regningId = $this->data['post']['faktura_id'] ?? null;
            /** @var Kostnad $regning */
            $regning =  $this->app->hentModell(Kostnad::class, $regningId);
            if (!$regning->hentId()) {
                throw new Exception('Finner ikke kostnaden registrert');
            }
            $regning->hentAndeler()->slettHver();
            $regning->settFordelt(false);
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @return $this
     */
    protected function slett(): Oppdrag {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => null
        ];

        try {
            $regningId = $this->data['post']['faktura_id'] ?? null;
            /** @var Kostnad $regning */
            $regning =  $this->app->hentModell(Kostnad::class, $regningId);
            if (!$regning->hentId()) {
                throw new Exception('Finner ikke kostnaden registrert');
            }
            $regning->slett();
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}