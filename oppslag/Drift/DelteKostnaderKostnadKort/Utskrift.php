<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 04/12/2020
 * Time: 23:21
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderKostnadKort;


use Exception;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_kostnad_liste\Faktura;

class Utskrift extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function utskrift() {
        try {
            $kostnadId = $this->data['get']['id'] ?? null;
            /** @var Kostnad $kostnad */
            $kostnad = $this->app->hentModell(Kostnad::class, $kostnadId);

            $resultat = $this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\shared\Utskrift::class, [
                'tittel' => $this->app->tittel,
                'innhold' => $this->app->vis(Faktura::class, ['faktura' => $kostnad])
            ]);
        }
        catch (Exception $e) {
            $resultat = $e;
        }
        $this->settRespons(new Respons($resultat));
        return $this;
    }
}