<?php
/**
 * Part of boligstiftelsen.svartlamon.org
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderKostnadKort;


use Exception;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_kostnad_liste\Faktura;


/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderKostnadKort
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param null $data
     * @return $this|Utlevering
     */
    public function hentData($data = null) {
        $kostnadId = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Tjeneste\Kostnad $kostnad */
        $kostnad = $this->app->hentModell(Tjeneste\Kostnad::class, $kostnadId);

        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => [],
            'totalRows' => 0
        ];

        switch ($data) {
            case 'detaljer':
                try {
                    $resultat = $this->app->vis(Faktura::class, ['faktura' => $kostnad]);
                } catch (Exception $e) {
                    $resultat = $e;
                }
                $this->settRespons(new Respons($resultat));
                break;

            default:
                $resultat = (object)[
                    'success' => false,
                    'msg' => 'Ønsket data er ikke oppgitt',
                    'data' => []
                ];
                $this->settRespons(new JsonRespons($resultat));
        }
        return $this;
    }
}