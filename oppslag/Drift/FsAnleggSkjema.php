<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\fs_anlegg_skjema\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * FsAnleggSkjema Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class FsAnleggSkjema extends DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Strømanlegg';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * FsAnleggSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $tjenesteId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['strømanlegg'] = null;

        if($tjenesteId != '*') {
            $this->hoveddata['strømanlegg'] = $this->hentModell(Strømanlegg::class, $tjenesteId);
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML(): bool
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Strømanlegg|null $strømanlegg */
        $strømanlegg = $this->hoveddata['strømanlegg'];
        if ($strømanlegg && !$strømanlegg->hentId()) {
            return false;
        }

        return true;
    }

    /**
     * @return Index
     * @throws Exception
     */
    public function skript(): Index
    {
        /** @var Strømanlegg|null $strømanlegg */
        $strømanlegg = $this->hoveddata['strømanlegg'];

        /** @var Index $skript */
        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'strømanlegg' => $strømanlegg,
                'tjenesteId' => $strømanlegg ? $strømanlegg->hentId() : '*',
            ])
        ]);
        return $skript;
    }
}