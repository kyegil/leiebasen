<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderKostnadListe;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_kostnad_liste\Faktura;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderKostnadListe
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param Kostnad $regning
     * @return object[]
     * @throws Exception
     */
    public static function hentManuelleFordelingselementer(Kostnad $regning): array
    {
        $resultat = [];
        $fordelingsnøkkel = $regning->hentTjeneste()? $regning->hentTjeneste()->hentFordelingsnøkkel() : null;
        if ($fordelingsnøkkel) {
            foreach($fordelingsnøkkel->hentBeløpelementer() as $element) {
                $leieobjekt = $element->følgerLeieobjekt && $element->leieobjekt && $element->leieobjekt->hentId() ? $element->leieobjekt : null;
                $leieforhold = !$element->følgerLeieobjekt && $element->leieforhold && $element->leieforhold->hentId() ? $element->leieforhold : null;
                if($leieobjekt) {
                    $beskrivelse = \Kyegil\Leiebasen\Leiebase::ucfirst($leieobjekt->hentType()) . ' ' . $leieobjekt->hentId() . ': ' . $leieobjekt->hentBeskrivelse();
                }
                else if($leieforhold) {
                    $beskrivelse = \Kyegil\Leiebasen\Leiebase::ucfirst($leieforhold->hentBeskrivelse());
                }
                $resultat[] = (object)[
                    'element_id' => $element->hentId(),
                    'beskrivelse' => $beskrivelse,
                    'beløp' => $element->hentFastbeløp(),
                ];
            }
        }
        return $resultat;
    }

    /**
     * @param string $data
     * @return $this
     * @throws Exception
     * @link https://datatables.net/manual/server-side
     */
    public function hentData($data = null): Utlevering
    {
        $get = $this->data['get'];
        $post = $this->data['post'];

        switch ($data) {
            case 'nye_fakturaer': {
                $resultat = (object)[
                    'success' => true,
                    'draw' => intval($get['draw'] ?? 0),
                    'data'		=> [],
                    'totalRows' => 0,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                ];

                $sorteringsfelt	= $get['sort'] ?? null;
                $synkende = isset($get['dir']) && $get['dir'] == "DESC";
                if ($sorteringsfelt) {
                    $sortering = [[
                        'columnName' => $sorteringsfelt,
                        'dir' => $synkende ? 'desc' : 'asc'
                    ]];
                }
                else {
                    $sortering  = $get['order'] ?? [];
                }
                $start		= intval($get['start'] ?? 0);
                $limit		= $get['limit'] ?? 25;

                $søkefelt = $get['søkefelt'] ?? ($get['search']['value'] ?? '');

                try {
                    $kostnadsett = $this->hentKostnadSett(
                        $søkefelt,
                        $sortering,
                        $start,
                        $limit
                    );

                    $resultat->recordsFiltered = $resultat->recordsTotal = $resultat->totalRows
                        = $kostnadsett->hentAntall();

                    foreach ($kostnadsett as $kostnad) {
                        $resultat->data[] = $this->hentRegningsdata($kostnad);
                    }
                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->error = $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }

            case 'fordeling_varseltekst': {
                $resultat = (object)[
                    'success' => true,
                    'data'		=> $this->app->hentValg('strømfordelingstekst'),
                ];

                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }

            default:
                $resultat = (object)[
                    'success' => false,
                    'msg' => 'Ønsket data er ikke oppgitt',
                    'data' => []
                ];
                $this->settRespons(new JsonRespons($resultat));
                return $this;
        }
    }

    /**
     * @param string $søkestreng
     * @param string[][] $sortering
     * @param int $start
     * @param int|null $limit
     * @return Tjeneste\Kostnadsett
     * @throws Exception
     */
    protected function hentKostnadSett(
        string  $søkestreng,
        array   $sortering = [],
        int     $start = 0,
        ?int    $limit = null
    ): Tjeneste\Kostnadsett
    {
        $filter = array(
            'or' => array(
                '`kostnadsdeling_kostnader`.`fakturanummer` LIKE' => '%' . $søkestreng . '%',
                '`kostnadsdeling_kostnader`.`termin` LIKE' => '%' . $søkestreng . '%',
                '`kostnadsdeling_kostnader`.`fakturabeløp`' => $søkestreng,
                '`kostnadsdeling_tjenester`.`navn` LIKE' => '%' . $søkestreng . '%',
                '`kostnadsdeling_tjenester`.`beskrivelse` LIKE' => '%' . $søkestreng . '%',
                '`kostnadsdeling_tjenester`.`avtalereferanse` LIKE' => '%' . $søkestreng . '%',
            )
        );

        /**
         * Mapping av sorteringsfelter
         * * kolonnenavn = > [tabellkilde el null for hovedtabell => tabellfelt]
         */
        $sorteringsfelter = [
            'id' => [null, 'id'],
            'fakturanummer' => [null, 'fakturanummer'],
            'tjeneste_id' => [null, 'tjeneste_id'],
            'termin' => [null, 'termin'],
            'fradato' => [null, 'fradato'],
            'tildato' => [null, 'tildato'],
            'forbruk' => [null, 'forbruk'],
            'beløp' => [null, 'fakturabeløp'],
        ];

        /** @var Tjeneste\Kostnadsett $kostnadsett */
        $kostnadsett = $this->app->hentSamling(Tjeneste\Kostnad::class);
        $kostnadsett->leggTilTjenesteModell();
        $kostnadsett->leggTilFilter(['fordelt' => false]);

        $kostnadsett->leggTilFilter($filter);
        $kostnadsett->låsFiltre();

        foreach ($sortering as $sorteringskonfig) {
            if(isset($sorteringsfelter[$sorteringskonfig['columnName']])) {
                $kostnadsett->leggTilSortering(
                    $sorteringsfelter[$sorteringskonfig['columnName']][1],
                    strtolower($sorteringskonfig['dir']) == 'desc',
                    $sorteringsfelter[$sorteringskonfig['columnName']][0]
                );
            }
        }

        if ($limit) {
            $kostnadsett
                ->settStart($start)
                ->begrens($limit);
        }
        return $kostnadsett;
    }

    /**
     * @param Kostnad $kostnad
     * @return object
     * @throws Exception
     */
    public function hentRegningsdata(Kostnad $kostnad): object
    {
        $fordelingsnøkkel = $kostnad->hentTjeneste() ? $kostnad->hentTjeneste()->hentFordelingsnøkkel() : null;
        $kanFordeles = $kostnad->hentTjeneste()
            && $kostnad->hentFakturabeløp()
            && $kostnad->hentFradato()
            && $kostnad->hentTildato();
        $forslagGenerert = (bool)$kostnad->hentAndeler()->hentAntall();
        /** @var object[] $manuelleFordelingselementer */
        $manuelleFordelingselementer = self::hentManuelleFordelingselementer($kostnad);
        $varslet = $kostnad->hentFordelingVarselSendt();
        $kanVarsles = $forslagGenerert && !$varslet;
        $kanLåses = $forslagGenerert || ($kanFordeles && !$fordelingsnøkkel);

        $html = $this->app->vis(Faktura::class, ['faktura' => $kostnad]);
        if ($manuelleFordelingselementer) {
            $html .= '<br>';
            $html .= new HtmlElement('button',
                ['onclick' => "leiebasen.drift.forberedFordeling({$kostnad->hentId()})"],
                'Beregn fordeling' . ($forslagGenerert ? ' på nytt' : '')
            );
        }

        return (object)array(
            'id' => $kostnad->hentId(),
            'fakturanummer' => $kostnad->fakturanummer,
            'tjeneste_id' => $kostnad->tjeneste ? $kostnad->tjeneste->hentId() : null,
            'tjeneste' => $kostnad->tjeneste ? "{$kostnad->tjeneste->id}: {$kostnad->tjeneste->navn}" : null,
            'beløp' => $kostnad->fakturabeløp,
            'beløp_formatert' => $this->app->kr($kostnad->fakturabeløp),
            'fradato' => $kostnad->fradato ? $kostnad->fradato->format('Y-m-d') : null,
            'tildato' => $kostnad->tildato ? $kostnad->tildato->format('Y-m-d') : null,
            'fradato_formatert' => $kostnad->fradato ? $kostnad->fradato->format('d.m.Y') : null,
            'tildato_formatert' => $kostnad->tildato ? $kostnad->tildato->format('d.m.Y') : null,
            'termin' => $kostnad->termin,
            'forbruk' => $kostnad->forbruk,
            'forbruk_formatert' => $kostnad->forbruk != 0 ? str_replace('.', ',', floatval($kostnad->forbruk)) . '&nbsp;' . $kostnad->forbruksenhet : null,
            'forslag_generert' => $forslagGenerert,
            'varslet' => $varslet ? $varslet->format('Y-m-d H:i:s') : null,
            'lag_inn_av' => $kostnad->lagtInnAv,
            'kan_fordeles' => $kanFordeles,
            'kan_varsles' => $kanVarsles,
            'kan_låses' => $kanLåses,
            'manuelle_fordelingselementer' => $manuelleFordelingselementer,
            'html' => (string)$html,
        );
    }
}