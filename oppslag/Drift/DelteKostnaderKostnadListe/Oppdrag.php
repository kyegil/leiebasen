<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 04/12/2020
 * Time: 23:21
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderKostnadListe;


use Exception;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderKostnadListe;
use Kyegil\Leiebasen\Respons\JsonRespons;

/**
 * @property DelteKostnaderKostnadListe $app
 */
class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return Oppdrag
     */
    protected function fordel(): Oppdrag {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => null
        ];

        try {
            $regningId = $this->data['post']['id'] ?? null;
            $fordelingselementBeløp = json_decode($this->data['post']['beløp'] ?? "{}", true);

            /** @var Kostnad $regning */
            $regning =  $this->app->hentModell(Kostnad::class, $regningId);
            if (!$regning->hentId()) {
                throw new Exception('Finner ikke kostnaden registrert');
            }
            if ($regning->fordelt) {
                throw new Exception('Denne er allerede fordelt');
            }

            if (
                !$regning->hentFakturabeløp()
                || !$regning->hentFradato()
                || !$regning->hentTildato()
            ) {
                throw new Exception('Beløp og datoer er påkrevet før kostnaden kan fordeles');
            }

            $fordelingsnøkkel = $regning->hentTjeneste() ? $regning->hentTjeneste()->hentFordelingsnøkkel() : null;
            if ($fordelingsnøkkel) {
                foreach($fordelingsnøkkel->hentBeløpelementer() as $beløpElement) {
                    if (isset($fordelingselementBeløp[$beløpElement->hentId()])) {
                        $beløpElement->settFastbeløp($fordelingselementBeløp[$beløpElement->hentId()]);
                    }
                    else {
                        throw new Exception('Beløp for element ' . $beløpElement->hentId() . ' mangler');
                    }
                }
                $fordelingsnøkkel->fordel($regning);
            }

            $resultat->data = $this->app->hentUtlevering()->hentRegningsdata($regning);
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @return Oppdrag
     */
    protected function varsle(): Oppdrag {
        $resultat = (object)[
            'success' => true,
            'msg' => 'Varsel om den foreslåtte fordelingen har blitt sendt deltakerne',
            'data' => null
        ];

        try {
            $fordelingVarseltekst = $this->data['post']['fordeling_varseltekst'] ?? null;
            if (!isset($fordelingVarseltekst)) {
                $fordelingVarseltekst = $this->app->hentValg('strømfordelingstekst');
            }
            $regningId = $this->data['post']['id'] ?? null;
            /** @var Kostnad $regning */
            $regning =  $this->app->hentModell(Kostnad::class, $regningId);
            if (!$regning->hentId()) {
                throw new Exception('Finner ikke kostnaden registrert');
            }
            if ($regning->fordelt) {
                throw new Exception('Denne er allerede fordelt');
            }
            if ($regning->hentFordelingVarselSendt()) {
                throw new Exception('Varsler har allerede blitt sendt for denne fordelingen');
            }
            $regning->varsleFordeling();
            $this->app->settValg('strømfordelingstekst', $fordelingVarseltekst);
            $resultat->data = $this->app->hentUtlevering()->hentRegningsdata($regning);
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @return Oppdrag
     */
    protected function bekreft(): Oppdrag {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => null
        ];

        try {
            $regningId = $this->data['post']['id'] ?? null;
            /** @var Kostnad $regning */
            $regning =  $this->app->hentModell(Kostnad::class, $regningId);
            if (!$regning->hentId()) {
                throw new Exception('Finner ikke kostnaden registrert');
            }
            if ($regning->fordelt) {
                throw new Exception('Denne er allerede fordelt');
            }
            $regning->bekreftFordeling();
            $resultat->data = $this->app->hentUtlevering()->hentRegningsdata($regning);
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    protected function slettFaktura(): Oppdrag {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => null
        ];

        try {
            $regningId = $this->data['post']['faktura_id'] ?? null;
            /** @var Kostnad $regning */
            $regning =  $this->app->hentModell(Kostnad::class, $regningId);
            if (!$regning->hentId()) {
                throw new Exception('Finner ikke kostnaden registrert');
            }
            if ($regning->fordelt) {
                throw new Exception('Denne er allerede fordelt');
            }
            $regning->slett();
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}