<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\DelteKostnaderKostnadListe;


use Exception;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Mottak extends AbstraktMottak
{
    /**
     * @param string|null $skjema
     * @return $this
     */
    public function taIMot($skjema = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => null,
            'data' => null
        ];

        try {
            switch ($skjema) {
                case 'oppdatering':
                    $regningId = $this->data['post']['id'] ?? null;
                    $felt = $this->data['post']['felt'] ?? null;
                    $verdi = $this->data['post']['verdi'] ?? null;
                    $opprinnelig = $this->data['post']['opprinnelig'] ?? [];

                    /** @var Kostnad $regning */
                    $regning
                        = $regningId
                        ? $this->app->hentModell(Kostnad::class, $regningId)
                        : $this->app->nyModell(Kostnad::class, (object)[
                            'lagt_inn_av' => $this->app->bruker['navn']
                        ]);
                    if (!$regning->hentId()) {
                        throw new Exception('Finner ikke registrering av kostnaden');
                    }

                    if ($felt && ($verdi != $opprinnelig)) {
                        switch ($felt) {
                            case 'tjeneste_id':
                                /** @var Tjeneste|null $tjeneste */
                                $tjeneste = $verdi ? $this->app->hentModell(Tjeneste::class, $verdi) : null;
                                if ($tjeneste && !$tjeneste->hentId()) {
                                    throw new Exception('Ukjent tjeneste');
                                }
                                $regning->sett('tjeneste', $tjeneste);
                                $regning->settForbruksenhet($tjeneste ? $tjeneste->hentForbruksenhet() : null);
                                break;
                            case 'beløp':
                                $regning->sett('fakturabeløp', $verdi);
                                break;
                            case 'fakturanummer':
                            case 'termin':
                            case 'forbruk':
                            $regning->sett($felt, $verdi);
                                break;
                            case 'fradato':
                            case 'tildato':
                                $verdi = new \DateTime($verdi);
                                $regning->sett($felt, $verdi);
                                break;
                            default:
                                throw new Exception('Ukjent felt');
                        }

                        if (
                            !$regning->fordelt
                            && $regning->hentTjeneste()
                            && $regning->hentFakturabeløp()
                            && $regning->hentFradato()
                            && $regning->hentTildato()
                            && $regning->hentFradato() <= $regning->hentTildato()
                        ) {
                            try {
                                $fordelingsnøkkel = $regning->hentTjeneste()->hentFordelingsnøkkel();
                                if ($fordelingsnøkkel && !$fordelingsnøkkel->hentBeløpelementer()->hentAntall()) {
                                    $fordelingsnøkkel->fordel($regning);
                                }
                                else if ($fordelingsnøkkel) {
                                    $regning->forkastFordelingsforslag();
                                }
                            } catch (Exception $e) {
                                $resultat->msg = $e->getMessage();
                            }
                        }
                    }
                    $resultat->data = $this->app->hentUtlevering()->hentRegningsdata($regning);
                    break;
                default:
                    throw new Exception('Skjema-parameter mangler');
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}