<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_tjeneste_kort\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * DelteKostnaderTjenesteKort Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class DelteKostnaderTjenesteKort extends DriftExtJsAdaptor
{
    protected $oppdrag = [
        'utskrift' => DelteKostnaderTjenesteKort\Utskrift::class
    ];
    /** @var string */
    public $tittel = 'Delte Kostnader – Tjeneste';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * DelteKostnaderTjenesteKort constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $tjenesteId = !empty($_GET['id']) ? $_GET['id'] : null;
        $tab = $_GET['tab'] ?? '';
        $this->hoveddata['tjeneste'] = $tjenesteId ? $this->hentModell(\Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste::class, $tjenesteId) : null;
        $this->hoveddata['tab'] = in_array($tab, ['detaljer', 'forbruk', 'regninger', 'fordelingsnøkkel']) ? $tab : 'detaljer';
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML(): bool
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Tjeneste|null $tjeneste */
        $tjeneste = $this->hoveddata['tjeneste'];
        /** @var string $tab */
        $tab = $this->hoveddata['tab'];
        if (!$tjeneste || !$tjeneste->hentId()) {
            return false;
        }
        $this->tittel = $tjeneste->hentNavn();
        if ($tjeneste->hentTypeModell() instanceof Strømanlegg) {
            $respons = new Respons('');
            $respons->header("Location: index.php?oppslag=fs_anlegg_kort&id=$tjeneste&tab=$tab");
            $this->settRespons($respons);
        }

        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     * @throws Exception
     */
    public function skript(): Index
    {
        /** @var Tjeneste $tjeneste */
        $tjeneste = $this->hoveddata['tjeneste'];
        /** @var string $tab */
        $tab = $this->hoveddata['tab'];

        /** @var Index $skript */
        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'tjeneste' => $tjeneste,
                'tjenesteId' => $tjeneste->hentId(),
                'tilbakeKnapp' => $this->returi->get(),
                'tab' => $tab
            ])
        ]);
        return $skript;
    }
}