<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\PersonAdresseSkjema;


use Exception;
use Kyegil\CoreModel\CoreModelException;
use Kyegil\Leiebasen\EventManager;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Modell\Eav\Egenskap;
use Kyegil\Leiebasen\Modell\Eav\Egenskapsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Vedlikehold;
use Throwable;

class Mottak extends AbstraktMottak
{
    /**
     * @param string|null $skjema
     * @return $this
     */
    public function taIMot($skjema = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => "Lagret."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'adressekort':
                    $netsProsessor = $this->app->hentValg('efaktura')
                        ? $this->app->hentNetsProsessor(): null;

                    $personId = $get['id'] ?? null;
                    if($personId == '*') {
                        /** @var Person $person */
                        $person = $this->app->nyModell(Person::class, (object)[
                            'etternavn' => $post['etternavn']
                        ]);
                    }
                    else {
                        /** @var Person $person */
                        $person = $this->app->hentModell(Person::class, !empty($get['id']) ? $get['id'] : null);
                        if (!$person->hentId()) {
                            throw new Feilmelding('Finner ikke adressekort ' . $personId);
                        }
                    }
                    $resultat->id = $person->hentId();
                    $erOrg = !empty($post['er_org']);
                    if(!$erOrg && empty($post['fornavn'])) {
                        throw new Feilmelding('Fornavn er påkrevet.');
                    }
                    $fødselsdato = !empty($post['fødselsdato']) ? new \DateTimeImmutable($post['fødselsdato']) : null;
                    $personnr = $post['personnr'];
                    $epost = $post['epost'];

                    $person->settErOrg($erOrg);
                    $person->etternavn = $post['etternavn'];
                    $person->fornavn = !$erOrg ? ($post['fornavn'] ?? '') : "";
                    $person->settFødselsdato($fødselsdato);
                    $person->settPersonnr($personnr);
                    $person->settAdresse1($post['adresse1']);
                    $person->settAdresse2($post['adresse2']);
                    $person->settPostnr($post['postnr']);
                    $person->settPoststed($post['poststed']);
                    $person->settLand($post['land']);
                    $person->settMobil($post['mobil']);
                    $person->settTelefon($post['telefon']);
                    $person->settEpost($epost);

                    $this->settEavVerdier($person);
                    $this->settLeietakerEavVerdier($person);

                    if($netsProsessor) {
                        try {
                            $netsProsessor->hentEfakturaIdentifierFor($person);
                        } catch (Exception $e) {
                            $this->app->logger->debug($e->getMessage());
                        }
                    }

                    EventManager::getInstance()->triggerEvent(
                        Person::class, 'endret',
                        $person, ['ny' => false], $this->app
                    );

                    $resultat->msg = 'Lagret';
                    $resultat->url = strval($this->app->returi->get(1));
                    Vedlikehold::getInstance($this->app)->sjekkAdresseoppdateringer();
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Feilmelding $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        catch (Throwable $e) {
            $this->app->loggException($e);
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param Person $person
     * @return Mottak
     * @throws CoreModelException
     */
    private function settEavVerdier(Person $person): Mottak
    {
        /** @var Egenskapsett $eavEgenskaper */
        $eavEgenskaper = $this->app->hentSamling(Egenskap::class);
        $eavEgenskaper->leggTilEavVerdiJoin('felt_type', true);
        $eavEgenskaper->leggTilSortering('rekkefølge');
        $eavEgenskaper->leggTilFilter(['modell' => Person::class])->låsFiltre();

        $eavEgenskaper->forEach(function(Egenskap $egenskap) use($person) {
            $kode = $egenskap->kode;
            if(array_key_exists($kode, $this->data['post'])) {
                $person->sett($kode, $this->data['post'][$kode]);
            }
        });

        return $this;
    }

    /**
     * @param Person $person
     * @return Mottak
     * @throws CoreModelException
     */
    private function settLeietakerEavVerdier(Person $person): Mottak
    {
        /** @var Egenskapsett $eavEgenskaper */
        $eavEgenskaper = $this->app->hentSamling(Egenskap::class);
        $eavEgenskaper->leggTilEavVerdiJoin('felt_type', true);
        $eavEgenskaper->leggTilSortering('rekkefølge');
        $eavEgenskaper->leggTilFilter(['modell' => Leietaker::class])->låsFiltre();

        if($eavEgenskaper->hentAntall()) {
            foreach($person->hentKontraktLinker() as $kontraktlink) {
                $eavEgenskaper->forEach(function(Egenskap $egenskap) use($kontraktlink) {
                    $kode = $egenskap->kode;
                    if(isset($this->data['post']['leietaker-egenskaper'][$kontraktlink->hentId()][$kode])) {
                        $kontraktlink->sett($kode, $this->data['post']['leietaker-egenskaper'][$kontraktlink->hentId()][$kode]);
                    }
                });
            }
        }

        return $this;
    }
}