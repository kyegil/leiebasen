<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\PersonAdresseSkjema;


use Exception;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return Oppdrag
     */
    protected function slettAdressekort():Oppdrag {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            $personId = (int)$this->data['post']['id'];
            /** @var Modell\Person $person */
            $person = $this->app->hentModell(Modell\Person::class, $personId);
            if (!$person->hentId()) {
                throw new Exception('Finner ikke dette adressekortet');
            }
            $resultat->msg = 'Adressekortet har blitt slettet';
            $person->slett();
            $resultat->url = (string)$this->app->returi->get(1);
        }
        catch (Feilmelding $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        catch (\Throwable $e) {
            $this->app->loggException($e);
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}