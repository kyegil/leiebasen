<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class ProfilSkjema extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /**
     * Forbered hoveddata
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $brukerprofilPersonId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['brukerprofil_person'] = $this->hentModell(Person::class, (int)$brukerprofilPersonId);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Person $brukerprofilPerson */
        $brukerprofilPerson = $this->hoveddata['brukerprofil_person'];
        if(!$brukerprofilPerson->hentId()) {
            return false;
        }
        $this->tittel = "Brukerprofil for {$brukerprofilPerson->hentNavn()}";
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Person $brukerprofilPerson */
        $brukerprofilPerson = $this->hoveddata['brukerprofil_person'];

        return $this->vis(\Kyegil\Leiebasen\Visning\drift\html\profil_skjema\Index::class, [
            'brukerprofil_person' => $brukerprofilPerson
        ]);
    }

    /**
     * @return ViewInterface
     */
    public function skript(): ViewInterface
    {
        $skript = new ViewArray();
        return $skript;
    }
}