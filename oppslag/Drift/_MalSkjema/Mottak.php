<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\_MalSkjema;


use Exception;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $resultat */
        $resultat = (object)[
            'success' => true,
            'msg' => "Lagret."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case '':
                    $id = $get['id'] ?? null;
                    if($id == '*') {
                        /** @var Modell $modell */
                        $modell = $this->app->nyModell(Modell::class, (object)[
                        ]);
                    }
                    else {
                        /** @var Modell $modell */
                        $modell = $this->app->hentModell(Modell::class, $id);
                        if (!$modell->hentId()) {
                            throw new Exception('Modell ' . $id . ' finnes ikke');
                        }
                    }
                    $resultat->id = $modell->hentId();
                    $resultat->msg = 'Endringene er lagret';
                    $resultat->url = (string)$this->app->returi->get(1);
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Feilmelding $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        catch (\Throwable $e) {
            $this->app->loggException($e);
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}