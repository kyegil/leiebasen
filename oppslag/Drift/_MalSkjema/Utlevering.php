<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\_MalSkjema;


use Exception;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\_MalSkjema
 */
class Utlevering extends AbstraktUtlevering
{

    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null): Utlevering
    {
        $get = $this->data['get'];


        switch ($data) {
            case '': {
                $resultat = (object)[
                    'success' => true,
                    'data'		=> []
                ];

                try {
                    $id = $get['id'] ?? null;
                }
                catch (Feilmelding $e) {
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                catch (\Throwable $e) {
                    $this->app->loggException($e);
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }

            default:
                $resultat = (object)[
                    'success' => false,
                    'msg' => 'Ønsket data er ikke oppgitt',
                    'data' => []
                ];
                $this->settRespons(new JsonRespons($resultat));
                return $this;
        }
    }
}