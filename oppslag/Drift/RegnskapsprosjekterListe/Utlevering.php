<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\RegnskapsprosjekterListe;


use Exception;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Regnskapsprosjekt;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Samling;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\FramleieListe
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null) {
        switch ($data) {
            default: {
                $resultat = (object)array(
                    'success' => true,
                    'data'		=> array()
                );

                $sort		= $_GET['sort'] ?? null;
                $synkende	= isset($_GET['dir']) && $_GET['dir'] == "DESC";
                $start		= intval($_GET['start'] ?? 0);
                $limit		= $_GET['limit'] ?? null;

                $søkefelt = $_GET['søkefelt'] ?? '';

                $filter = [
                    'or' => [
                        "`regnskapsprosjekter`.`kode` LIKE" =>  '%' . $søkefelt . '%',
                        "`regnskapsprosjekter`.`navn` LIKE" =>  '%' . $søkefelt . '%'
                    ]
                ];

                try {
                    $sorteringsfelter = [
                        'id' => [null, 'id'],
                        'kode' => [null, 'kode'],
                        'navn' => [null, 'navn'],
                    ];
                    $sort = ($sort && isset($sorteringsfelter[$sort])) ? $sorteringsfelter[$sort] : null;

                    /** @var Samling $regnskapsprosjektSett */
                    $regnskapsprosjektSett = $this->app->hentSamling(Regnskapsprosjekt::class)
                        ->leggTilFilter($filter)
                    ;

                    $regnskapsprosjektSett->låsFiltre();

                    if ($sort) {
                        $regnskapsprosjektSett->leggTilSortering($sort[1], $synkende, $sort[0]);
                    }

                    $resultat->totalRows = $regnskapsprosjektSett->hentAntall();
                    if (isset($limit) && $limit < $resultat->totalRows) {
                        $regnskapsprosjektSett
                            ->nullstill()
                            ->settStart($start)
                            ->begrens($limit);
                    }

                    /** @var Regnskapsprosjekt $regnskapsprosjekt */
                    foreach ($regnskapsprosjektSett as $regnskapsprosjekt) {
                        $resultat->data[] = (object)array(
                            'id' => $regnskapsprosjekt->hentId(),
                            'kode' => $regnskapsprosjekt->kode,
                            'navn' => $regnskapsprosjekt->navn,
                            'beskrivelse' => $regnskapsprosjekt->beskrivelse
                        );
                    }
                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }
        }
    }
}