<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\adgang_skjema\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * AdgangSkjema Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class AdgangSkjema extends DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Område';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => AdgangSkjema\Oppdrag::class
    ];


    /**
     * AdgangSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $this->tittel = 'Til adgang til nett-område';

        $adgangsId = !empty($_GET['id']) ? $_GET['id'] : null;
        $personId = $_GET['personid'] ?? null;
        $leieforholdId = $_GET['leieforhold'] ?? null;
        $adgangsområde = $_GET['adgangsområde'] ?? null;
        $this->hoveddata['adgang'] = null;
        $this->hoveddata['person'] = null;
        $this->hoveddata['adgangsområde'] = null;
        $this->hoveddata['leieforhold'] = null;

        $filter = [
            'or' => [
                ['adgangsid' => $adgangsId],
                'and' => [
                    ['personid' => $personId],
                    ['adgang' => $adgangsområde]
                ]
            ]
        ];
        if ($adgangsområde == Adgang::ADGANG_MINESIDER) {
            $filter['or']['and'][] = ['leieforhold' => $leieforholdId];
        }
        /** @var Adgang|null $adgang */
        $adgang = $this->hentSamling(Adgang::class)
            ->leggTilFilter($filter)->hentFørste();

        if ($adgang) {
            if ($adgang->hentId()) {
                $this->hoveddata['adgang'] = $adgang;
                $this->hoveddata['adgangsområde'] = $adgang->adgang;
                $this->hoveddata['person'] = $adgang->person;
                $this->hoveddata['leieforhold'] = $adgang->leieforhold;
            }
        }
        else if ($adgangsId == '*') {
            $this->hoveddata['adgang'] = '*';
            $this->hoveddata['person'] = $personId ? $this->hentModell(Person::class, $personId) : null;
            $this->hoveddata['adgangsområde'] = $adgangsområde;
            $this->hoveddata['leieforhold'] = $leieforholdId ? $this->hentModell(Leieforhold::class, $leieforholdId) : null;
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Adgang|string|null $adgang */
        $adgang = $this->hoveddata['adgang'];
        /** @var Person|null $person */
        $person = $this->hoveddata['person'];
        if ($adgang instanceof Adgang && !$adgang->hentId()) {
            return false;
        }
        if (is_string($adgang)) {
            if ($adgang !== '*') {
                return false;
            }
            $this->hoveddata['adgang'] = null;
        }
        if ($person) {
            $this->tittel = 'Nettadgang for ' . $person->hentNavn();
        }

        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     * @throws Exception
     */
    public function skript() {
        /** @var Adgang|string|null $adgang */
        $adgang = $this->hoveddata['adgang'];
        /** @var Person|null $person */
        $person = $this->hoveddata['person'];
        /** @var string|null $område */
        $område = $this->hoveddata['adgangsområde'];
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];

        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'adgang' => ($adgang instanceof Adgang) && $adgang->hentId() ? $adgang : null,
                'adgangId' => $adgang && $adgang->hentId() ? $adgang->hentId() : '*',
                'person' => $person && $person->hentId() ? $person : null,
                'område' => $område ?: null,
                'leieforhold' => $leieforhold && $leieforhold->hentId() ? $leieforhold : null,
            ])
        ]);
        return $skript;
    }
}