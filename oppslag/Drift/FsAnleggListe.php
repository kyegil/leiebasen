<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\Drift\FsAnleggListe\Utlevering;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\fs_anlegg_liste\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * FsAnleggListe Controller
 *
 * @method Utlevering hentUtlevering()
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class FsAnleggListe extends DriftExtJsAdaptor
{
    protected $oppdrag = [
        'utskrift' => FsAnleggListe\Utskrift::class
    ];
    /** @var string */
    public $tittel = 'Strømanlegg';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * Innhold
     *
     * @return HtmlElement
     */
    public function innhold()
    {
        $innhold = new HtmlElement(
            'table',
            [],
            new HtmlElement('tbody', ['style' => 'height: calc(100vh - 80px)'],
                new HtmlElement('tr', [],
                    [
                        new HtmlElement('td', [
                            'id' => 'panel'
                        ]),
                        new HtmlElement('td', [
                            'style' => 'width: 100%;',
                            'id' => 'detaljpanel'
                        ])
                    ]
                )
            )
        );
        return $innhold;
    }

    /**
     * @return Index
     * @throws Exception
     */
    public function skript() {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];

        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'bruker' => $bruker
            ])
        ]);
        return $skript;
    }
}