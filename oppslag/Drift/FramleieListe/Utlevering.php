<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 15/06/2020
 * Time: 12:24
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\FramleieListe;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleiesett;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Framleie;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Modell\Leieforhold\Leietaker;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\FramleieListe
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null) {
        switch ($data) {
            default: {
                $resultat = (object)array(
                    'success' => true,
                    'data'		=> array()
                );

                $sort		= $_GET['sort'] ?? null;
                $synkende	= isset($_GET['dir']) && $_GET['dir'] == "DESC";
                $start		= intval($_GET['start'] ?? 0);
                $limit		= $_GET['limit'] ?? null;

                $søkefelt = $_GET['søkefelt'] ?? '';

                $filter = [
                    'or' => [
                        "REPLACE(LOWER(CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`)), 'å', 'aa') LIKE" =>  '%' . str_ireplace('å', 'aa', strtolower($søkefelt)) . '%',
                        "REPLACE(LOWER(CONCAT(`framleieperson`.`fornavn`, ' ', `framleieperson`.`etternavn`)), 'å', 'aa') LIKE" =>  '%' . str_ireplace('å', 'aa', strtolower($søkefelt)) . '%'
                    ]
                ];

                try {
                    $sorteringsfelter = [
                        'til_dato' => [null, 'tildato'],
                        'fra_dato' => [null, 'fradato'],
                        'leieforhold' => [null, 'leieforhold'],
                    ];
                    $sort = ($sort && isset($sorteringsfelter[$sort])) ? $sorteringsfelter[$sort] : null;

                    /** @var Framleiesett $framleiesett */
                    $framleiesett = $this->app->hentSamling(Framleie::class)
                        ->leggTilLeieforholdModell()
                        ->leggTilLeftJoin(Leieobjekt::hentTabell(), '`' . Leieobjekt::hentTabell() . '`.`' . Leieobjekt::hentPrimærnøkkelfelt() . '` = `' . Leieforhold::hentTabell() . '`.`leieobjekt`')
                        ->leggTilInnerJoin(Kontrakt::hentTabell(), Kontrakt::hentTabell() . '.leieforhold = ' . Framleie::hentTabell() . '.leieforhold')
                        ->leggTilLeftJoin(['siste_kontrakt' => 'kontrakter'], '`siste_kontrakt`.`kontraktnr` = (SELECT MAX(`kontraktnr`) FROM `' . $this->app->mysqli->table_prefix . 'kontrakter` WHERE `leieforhold` = `' . Framleie::hentTabell() . '`.`leieforhold`)')
                        ->leggTilInnerJoin(Leietaker::hentTabell(), Leietaker::hentTabell() . '.kontrakt = ' . Kontrakt::hentTabell() . '.' . Kontrakt::hentPrimærnøkkelfelt())
                        ->leggTilInnerJoin(Person::hentTabell(), Leietaker::hentTabell() . '.person = ' . Person::hentTabell() . '.' . Person::hentPrimærnøkkelfelt())

                        ->leggTilLeftJoin(Framleie\Framleietaker::hentTabell(), Framleie\Framleietaker::hentTabell() . '.framleieforhold = ' . Framleie::hentTabell() . '.' . Framleie::hentPrimærnøkkelfelt())
                        ->leggTilLeftJoin(['framleieperson' => Person::hentTabell()], Framleie\Framleietaker::hentTabell() . '.personid = ' . 'framleieperson.' . Person::hentPrimærnøkkelfelt())

                        ->leggTilModell(Leieobjekt::class, 'leieforhold.leieobjekt')
                        ->leggTilFelt('siste_kontrakt', 'kontraktnr', null, null, 'leieforhold.siste_kontrakt')

                        ->leggTilFilter($filter, true)
                    ;

                    $framleiesett->låsFiltre();

                    if ($sort) {
                        $framleiesett->leggTilSortering($sort[1], $synkende, $sort[0]);
                    }

                    $resultat->totalRows = $framleiesett->hentAntall();
                    if (isset($limit) && $limit < $resultat->totalRows) {
                        $framleiesett
                            ->nullstill()
                            ->settStart($start)
                            ->begrens($limit);
                    }

                    /** @var Framleie $framleieavtale */
                    foreach ($framleiesett as $framleieavtale) {
                        $framleidTil = [];
                        /** @var Person $framleieperson */
                        foreach ($framleieavtale->hentFramleiepersoner() as $framleieperson) {
                            $framleidTil[] = $framleieperson->hentNavn();
                        }
                        $resultat->data[] = (object)array(
                            'avtale_id' => $framleieavtale->hentId(),
                            'fradato' => $framleieavtale->fradato ? $framleieavtale->fradato->format('Y-m-d') : null,
                            'tildato' => $framleieavtale->tildato ? $framleieavtale->tildato->format('Y-m-d') : null,
                            'leieforhold' => $framleieavtale->leieforhold->hentId(),
                            'leieforhold_beskrivelse' => $framleieavtale->leieforhold->hentBeskrivelse(),
                            'framleid_til' => $this->app->liste($framleidTil)
                        );
                    }
                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }
        }
    }
}