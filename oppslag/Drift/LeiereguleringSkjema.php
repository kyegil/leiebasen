<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class LeiereguleringSkjema extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $leieforholdId = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentModell(Leieforhold::class, (int)$leieforholdId);
        if (!$leieforhold->hentId()) {
            $leieforhold = null;
        }
        $this->hoveddata['leieforhold'] = $leieforhold;
        $this->hoveddata['leieforholdId'] = $leieforholdId;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        if(!$leieforhold) {
            return false;
        }
        $this->tittel = 'Forslag til leieregulering';
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];

        return $this->vis(\Kyegil\Leiebasen\Visning\drift\html\leieregulering_skjema\Index::class, [
            'leieforhold' => $leieforhold,
            'leieforholdId' => $this->hoveddata['leieforholdId'],
        ]);
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function skript(): ViewInterface
    {
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];

        $skript = new ViewArray([
            $this->vis(\Kyegil\Leiebasen\Visning\drift\js\leieregulering_skjema\Index::class, [
                'leieforhold' => $leieforhold,
                'leieforholdId' => $this->hoveddata['leieforholdId'],
            ]),
        ]);
        return $skript;
    }
}