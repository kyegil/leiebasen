<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

/**
 * KontraktOpplastingSkjema Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class KontraktOpplastingSkjema extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
//        'oppgave' => KontraktOpplastingSkjema\Oppdrag::class
    ];

    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $kontraktId = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Kontrakt $kontrakt */
        $kontrakt = $this->hentModell(Kontrakt::class, (int)$kontraktId);
        if (!$kontrakt->hentId()) {
            $kontrakt = null;
        }
        $this->hoveddata['kontrakt'] = $kontrakt;
        $this->hoveddata['kontraktId'] = $kontraktId;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML(): bool
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Kontrakt $kontrakt */
        $kontrakt = $this->hoveddata['kontrakt'];
        if (
            !$kontrakt || !$kontrakt->hentId()
        ) {
            return false;
        }
        $this->tittel = 'Last opp signert avtale ' . $kontrakt->hentId() . ' med ' . $kontrakt->leieforhold->hentNavn();
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Kontrakt $kontrakt */
        $kontrakt = $this->hoveddata['kontrakt'];

        $visning = new ViewArray();
        $visning->addItem($this->vis(\Kyegil\Leiebasen\Visning\drift\html\KontraktOpplastingSkjema::class, [
            'kontrakt' => $kontrakt,
            'kontraktId' => $this->hoveddata['kontraktId'],
            'alleredeOpplastet' => (bool)$kontrakt->hent('signert_fil')
        ]));

        return $visning;
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function skript(): ViewInterface
    {
        /** @var Kontrakt $kontrakt */
        $kontrakt = $this->hoveddata['kontrakt'];

        $skript = new ViewArray();
        return $skript;
    }
}