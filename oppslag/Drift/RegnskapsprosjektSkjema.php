<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Regnskapsprosjekt;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\regnskapsprosjekt_skjema\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * RegnskapsprosjektSkjema Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class RegnskapsprosjektSkjema extends DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Prosjektregnskap';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => RegnskapsprosjektSkjema\Oppdrag::class
    ];


    /**
     * RegnskapsprosjektSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $prosjektId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['prosjekt'] = null;

        if ((int)$prosjektId) {
            $this->hoveddata['prosjekt'] = $this->hentModell(Regnskapsprosjekt::class, (int)$prosjektId);
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Regnskapsprosjekt $prosjekt */
        $prosjekt = $this->hoveddata['prosjekt'];
        if (
            $prosjekt
            && !$prosjekt->hentId()
        ) {
            return false;
        }
        else if ($prosjekt) {
            $this->tittel = $prosjekt->navn . ' (' . $prosjekt->kode . ')';
        }

        return true;
    }

    /**
     * @return Index
     */
    public function skript() {
        /** @var Regnskapsprosjekt|null $prosjekt */
        $prosjekt = $this->hoveddata['prosjekt'];

        if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }

        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'prosjekt' => $prosjekt
            ])
        ]);
        return $skript;
    }
}