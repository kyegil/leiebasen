<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\Kravimport;


use DateTime;
use Exception;
use Kyegil\Fraction\Fraction;
use Kyegil\Leiebasen\Modell\Delkravtype;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;

class Mottak extends AbstraktMottak
{
    const LOVLIGE_FELTER = [
        'leieforhold', 'kravdato', 'tekst', 'beløp',
        'leieobjekt', 'fom', 'tom', 'termin', 'andel',
        'anleggsnr'
    ];

    const PÅKREVDE_FELTER = ['leieforhold', 'tekst', 'beløp'];

    private static $tidligstMuligeKravdato;

    private static $kravtyper;

    /**
     * @param string|null $skjema
     * @return $this
     */
    public function taIMot($skjema = null) {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($skjema) {
                default:
                    $importfil = isset($this->data['files']['importfil'][0]['tmp_name']) && $this->data['files']['importfil'][0]['tmp_name'] ? $this->data['files']['importfil'][0] : null;
                    if (!$importfil) {
                        throw new Exception('Importfila mangler');
                    }
                    if($importfil['error'] != 0) {
                        throw new Exception('Det oppstod en feil ved opplasting. Feilkode ' . $importfil['error']);
                    }
                    $tmpName = $importfil['tmp_name'];

                    if ($importfil['type'] != 'text/csv') {
                        throw new Exception('Ugyldig filtype. Last opp i gyldig csv-format');
                    }
                    ini_set('auto_detect_line_endings',TRUE);
                    $handle = fopen($tmpName,'r');
                    if($handle === false) {
                        throw new Exception('Klarte ikke lese opplastet fil');
                    }
                    $antallFelter = null;
                    $kolonner = [];
                    $importObjekter = [];
                    while (($rad = fgetcsv($handle)) !== FALSE) {
                        if(!isset($antallFelter)) {
                            $kolonner = $rad;
                            $antallFelter = count($kolonner);
                            $this->validerKolonner($kolonner);
                        }
                        else {
                            if(count($rad) != $antallFelter) {
                                throw new Exception('Fila har ikke gjennomgående like mange felter i hver linje');
                            }
                            $importObjekter[] = (object)array_combine($kolonner, $rad);
                        }

                    }
                    fclose($handle);

                    foreach($importObjekter as $linje => $importObjekt) {
                        $this->validerImportObjekt($importObjekt, $linje);
                    }
                    $resultat->msg = 'Fila er gyldig og kan importeres';
                    if ($skjema == 'import') {
                        $resultat->msg = 'Kravene er opprettet';
                        $resultat->url = strval($this->app->returi->get(1));
                        $this->opprettKrav($importObjekter);
                    }
                    break;
            }
        } catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param string[] $kolonner
     * @return Mottak
     * @throws Exception
     */
    private function validerKolonner(array $kolonner): Mottak
    {
        $manglendeFelter = array_diff(self::PÅKREVDE_FELTER, $kolonner );
        $ulovligeFelter = array_diff($kolonner, self::LOVLIGE_FELTER);

        if (!count($kolonner)) {
            throw new Exception('Fila inneholder ingen felter');
        }
        if (count($manglendeFelter)) {
            throw new Exception('Følgende påkrevde felter mangler: ' . implode(', ', $manglendeFelter));
        }
        if (count($ulovligeFelter)) {
            throw new Exception('Følgende ulovlige felter ble funnet: ' . implode(', ', $ulovligeFelter));
        }
        return $this;
    }

    /**
     * @param stdClass $importObjekt
     * @param int $linje
     * @return Mottak
     * @throws Exception
     */
    public function validerImportObjekt(stdClass $importObjekt, int $linje): Mottak
    {
        $importObjekt->type = isset($importObjekt->type) && $importObjekt->type
            ? $importObjekt->type
            : Krav::TYPE_ANNET
        ;
        $importObjekt->leieforhold = $this->app->hentModell(Leieforhold::class, $importObjekt->leieforhold);
        $importObjekt->kravdato = new DateTime($importObjekt->kravdato ?? 'now');
        $importObjekt->fom = isset($importObjekt->fom) && $importObjekt->fom ? date_create($importObjekt->fom) : null;
        $importObjekt->tom = isset($importObjekt->tom) && $importObjekt->tom ? date_create($importObjekt->tom) : null;
        $importObjekt->leieobjekt = isset($importObjekt->leieobjekt) && $importObjekt->leieobjekt
            ? $this->app->hentModell(Leieobjekt::class, $importObjekt->leieobjekt)
            : null;
        $importObjekt->andel = isset($importObjekt->andel) && $importObjekt->andel
            ? new Fraction($importObjekt->andel)
            : null;

        try {
            $this->validerTekst($importObjekt);
            $this->validerLeieforhold($importObjekt);
            $this->validerDato($importObjekt);
            $this->validerBeløp($importObjekt);
            $this->validerType($importObjekt);
            $this->validerLeieobjekt($importObjekt);
        } catch (Exception $e) {
            throw new Exception('Problem i linje ' . ($linje + 2) . ': ' . $e->getMessage());
        }
        return $this;
    }

    /**
     * @param stdClass $importObjekt
     * @return Mottak
     * @throws Exception
     */
    public function validerTekst(stdClass $importObjekt): Mottak
    {
        if(empty($importObjekt->tekst)) {
            throw new Exception('Tekst mangler');
        }
        return $this;
    }

    /**
     * @param stdClass $importObjekt
     * @return Mottak
     * @throws Exception
     */
    public function validerDato(stdClass $importObjekt): Mottak
    {
        if (!isset(self::$tidligstMuligeKravdato)) {
            self::$tidligstMuligeKravdato = $this->app->tidligstMuligeKravdato();
        }
        if (self::$tidligstMuligeKravdato &&  $importObjekt->kravdato < self::$tidligstMuligeKravdato) {
            throw new Exception('Kravdato kan ikke settes tidligere enn ' . self::$tidligstMuligeKravdato->format('d.m.Y'));
        }
        if ($importObjekt->kravdato < $importObjekt->leieforhold->fradato) {
            throw new Exception('Kravdato kan ikke settes til før leieforholdet ble påbegynt');
        }
        return $this;
    }

    /**
     * @param stdClass $importObjekt
     * @return Mottak
     * @throws Exception
     */
    public function validerLeieforhold(stdClass $importObjekt): Mottak
    {
        if(
            empty($importObjekt->leieforhold)
            || !$importObjekt->leieforhold->hentId()
            || $importObjekt->leieforhold->frosset
        ) {
            throw new Exception('Ugyldig leieforhold');
        }
        return $this;
    }

    /**
     * @param stdClass $importObjekt
     * @return Mottak
     * @throws Exception
     */
    public function validerBeløp(stdClass $importObjekt): Mottak
    {
        if(
            !is_numeric($importObjekt->beløp)
            || floor($importObjekt->beløp) > 100000
        ) {
            throw new Exception('Ugyldig beløp');
        }
        return $this;
    }

    /**
     * @param stdClass $importObjekt
     * @return Mottak
     * @throws Exception
     */
    public function validerLeieobjekt(stdClass $importObjekt): Mottak
    {
        if (empty($importObjekt->leieobjekt)) {
            return $this;
        }
        if(!$importObjekt->leieobjekt->id) {
            throw new Exception('Ugyldig leieobjekt');
        }
        return $this;
    }

    /**
     * @param stdClass $importObjekt
     * @return Mottak
     * @throws Exception
     */
    public function validerType(stdClass $importObjekt): Mottak
    {
        if (!isset(self::$kravtyper)) {
            self::$kravtyper = [
                Krav::TYPE_HUSLEIE,
                Krav::TYPE_STRØM,
                Krav::TYPE_PURREGEBYR,
                Krav::TYPE_ANNET,
            ];
            /** @var Delkravtype $tillegg */
            foreach($this->app->hentDelkravtyper()->leggTilFilter(['selvstendig_tillegg' => true]) as $tillegg) {
                self::$kravtyper[] = $tillegg->kode;
            }
        }
        if(
            !in_array( $importObjekt->type, self::$kravtyper)
        ) {
            throw new Exception('Ugyldig kravtype');
        }
        switch ($importObjekt->type) {
            case Krav::TYPE_HUSLEIE:
                $this->validerHusleie($importObjekt);
                break;
            case Krav::TYPE_STRØM:
                $this->validerFellesstrøm($importObjekt);
                break;
            default:
                break;
        }
        return $this;
    }

    /**
     * @param stdClass $importObjekt
     * @return Mottak
     * @throws Exception
     * @todo Implementer validering av husleie
     */
    private function validerHusleie(stdClass $importObjekt): Mottak
    {
        throw new Exception('Husleie kan ikke valideres');
        return $this;
    }

    /**
     * @param stdClass $importObjekt
     * @return Mottak
     * @throws Exception
     * @todo Implementer validering av husleie
     */
    private function validerFellesstrøm(stdClass $importObjekt): Mottak
    {
        throw new Exception('Fellesstrøm kan ikke valideres');
        return $this;
    }

    /**
     * @param stdClass[] $importObjekter
     * @throws Exception
     */
    private function opprettKrav(array $importObjekter)
    {
        foreach ($importObjekter as $importObjekt) {
            $this->app->nyModell(Krav::class, $importObjekt);
        }
    }
}