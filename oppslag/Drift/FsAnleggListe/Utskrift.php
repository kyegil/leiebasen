<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 04/12/2020
 * Time: 23:21
 */

namespace Kyegil\Leiebasen\Oppslag\Drift\FsAnleggListe;


use Exception;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Visning\felles\html\table\HtmlTable;

class Utskrift extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function utskrift() {
        try {
            $datasett = json_decode($this->app->hentUtlevering()->hentData('')->hentRespons()->innholdSomStreng(), false);
            $kolonner = [
                (object)['data' => 'id', 'title' => 'Tjeneste-Id', 'classes' => ['right']],
                (object)['data' => 'anleggsnummer', 'title' => 'Anleggsnr',],
                (object)['data' => 'formål', 'title' => 'Brukes til',],
                (object)['data' => 'målernummer', 'title' => 'Måler',],
                (object)['data' => 'plassering', 'title' => 'Lokalisering av måler',],
            ];
            $data = [];

            foreach ($datasett->data as $datasettData) {
                $data[] = (object)[
                    'id' => $datasettData->id,
                    'anleggsnummer' => $datasettData->anleggsnummer,
                    'målernummer' => $datasettData->målernummer,
                    'plassering' => $datasettData->plassering,
                    'formål' => $datasettData->formål,
                ];
            }
            $tabell = $this->app->vis(HtmlTable::class, [
                'caption' => $this->app->tittel,
                'kolonner' => $kolonner,
                'data' => $data
            ]);
            $resultat = $this->app->vis(\Kyegil\Leiebasen\Visning\felles\html\shared\Utskrift::class, [
                'tittel' => $this->app->tittel,
                'innhold' => [
                    $tabell
                ]
            ]);
        }
        catch (Exception $e) {
            $resultat = $e;
        }
        $this->settRespons(new Respons($resultat));
        return $this;
    }
}