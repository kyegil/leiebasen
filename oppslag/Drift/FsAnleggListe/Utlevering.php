<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\FsAnleggListe;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanleggsett;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling\Fordelingsnøkkel;
use Kyegil\ViewRenderer\ViewArray;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\FsAnleggListe
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null): Utlevering
    {
        switch ($data) {
            default: {
                $resultat = (object)[
                    'success' => true,
                    'data'		=> [],
                    'totalRows' => 0
                ];

                $sorteringsfelt		= $_GET['sort'] ?? null;
                $synkende	= isset($_GET['dir']) && $_GET['dir'] == "DESC";
                $start		= intval($_GET['start'] ?? 0);
                $limit		= $_GET['limit'] ?? null;

                $søkefelt = $_GET['søkefelt'] ?? '';

                try {
                    $anleggSett = $this->hentAnleggSett(
                        $søkefelt,
                        $sorteringsfelt,
                        $synkende,
                        $start,
                        $limit
                    );

                    $resultat->totalRows = $anleggSett->hentAntall();

                    foreach ($anleggSett as $anlegg) {
                        $resultat->data[] = (object)array(
                            'id' => $anlegg->hentId(),
                            'anleggsnummer' => $anlegg->hentAnleggsnummer(),
                            'målernummer' => $anlegg->hentMålernummer(),
                            'plassering' => $anlegg->hentPlassering(),
                            'formål' => $anlegg->hentFormål(),
                            'html' => $this->hentHtml($anlegg),
                        );
                    }
                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }
        }
    }

    /**
     * @param string $søkestreng
     * @param string|null $sorteringsfelt
     * @param bool $synkende
     * @param int $start
     * @param int|null $limit
     * @return Strømanleggsett
     * @throws Exception
     */
    protected function hentAnleggSett(
        string  $søkestreng,
        ?string $sorteringsfelt = null,
        bool    $synkende = false,
        int     $start = 0,
        ?int    $limit = null
    ): Strømanleggsett
    {
        $filter = array(
            'or' => array(
                '`' . Strømanlegg::hentTabell() . '`.`navn` LIKE' => '%' . $søkestreng . '%',
                '`' . Strømanlegg::hentTabell() . '`.`beskrivelse` LIKE' => '%' . $søkestreng . '%',
                '`' . Strømanlegg::hentTabell() . '`.`avtalereferanse` LIKE' => '%' . $søkestreng . '%',
                '`' . Strømanlegg::hentTabell() . '`.`målernummer` LIKE' => '%' . $søkestreng . '%',
            )
        );

        /**
         * Mapping av sorteringsfelter
         * * kolonnenavn = > [tabellkilde el null for hovedtabell => tabellfelt]
         */
        $sorteringsfelter = [
            'id' => [null, 'id'],
            'navn' => [null, 'navn'],
            'anleggsnummer' => [null, 'avtalereferanse'],
            'målernummer' => [null, 'beskrivelse'],
            'formål' => [null, 'beskrivelse'],
        ];
        $sorteringsfelt = ($sorteringsfelt && isset($sorteringsfelter[$sorteringsfelt])) ? $sorteringsfelter[$sorteringsfelt] : null;

        /** @var Strømanleggsett $anleggSett */
        $anleggSett = $this->app->hentSamling(Strømanlegg::class);

        $anleggSett->leggTilFilter($filter);
        $anleggSett->låsFiltre();

        if ($sorteringsfelt) {
            $anleggSett->leggTilSortering($sorteringsfelt[1], $synkende, $sorteringsfelt[0]);
        }

        if ($limit) {
            $anleggSett
                ->settStart($start)
                ->begrens($limit);
        }
        return $anleggSett;
    }

    private function hentHtml(Strømanlegg $anlegg)
    {
        $html = new ViewArray();
        $fordelingsnøkkel = $anlegg->hentFordelingsnøkkel();
        $html->addItem(new HtmlElement('div', [], $anlegg->hentFormål()));
        if ($fordelingsnøkkel) {
            $html->addItem($this->app->vis(Fordelingsnøkkel::class, ['fordelingsnøkkel' => $fordelingsnøkkel]));
        }
        $html->addItem(new HtmlElement('a', [
            'class' => 'button',
            'title' => 'Legg til nytt element til fordelingsnøkkelen',
            'href' => '/drift/index.php?oppslag=delte_kostnader_fordelingselement_skjema&id=*&nøkkel=' . $anlegg->hentId()
        ], 'Legg til nytt element'));

        return (string)$html;
    }
}