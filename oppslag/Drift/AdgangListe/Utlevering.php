<?php

namespace Kyegil\Leiebasen\Oppslag\Drift\AdgangListe;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;

/**
 * Class Utlevering
 * @package Kyegil\Leiebasen\Oppslag\Drift\AdresseListe
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     * @throws Exception
     */
    public function hentData($data = null): Utlevering
    {
        $get = $this->data['get'];
        $post = $this->data['post'];

        switch ($data) {
            default: {
                $resultat = (object)[
                    'success' => true,
                    'draw' => intval($get['draw'] ?? 0),
                    'data'		=> [],
                    'totalRows' => 0,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                ];

                $sorteringsfelt	= $get['sort'] ?? null;
                $synkende = isset($get['dir']) && $get['dir'] == "DESC";
                if ($sorteringsfelt) {
                    $sortering = [[
                        'columnName' => $sorteringsfelt,
                        'dir' => $synkende ? 'desc' : 'asc'
                    ]];
                }
                else {
                    $sortering  = $get['order'] ?? [];
                }
                $start		= intval($get['start'] ?? 0);
                $limit		= $get['limit'] ?? 25;

                $søkefelt = $get['søkefelt'] ?? ($get['search']['value'] ?? '');

                try {
                    $adgangSett = $this->hentAdgangSett(
                        $søkefelt,
                        $sortering,
                        $start,
                        $limit
                    );

                    $resultat->recordsFiltered = $resultat->recordsTotal = $resultat->totalRows
                        = $adgangSett->hentAntall();

                    foreach ($adgangSett as $adgang) {
                        /** @var Leieforhold|null $leieforhold */
                        $leieforhold = $adgang->hentLeieforhold();
                        $resultat->data[] = (object)array(
                            'id' => $adgang->hentId(),
                            'person_id' => $adgang->person->hentId(),
                            'bruker' => $adgang->person->hentNavn(),
                            'adgangsområde' => $adgang->adgang,
                            'leieforhold_id' => $leieforhold ? $leieforhold->hentId() : null,
                            'leieforhold' => $leieforhold ? $leieforhold->hentBeskrivelse() : null,
                            'epostvarsling' => $adgang->epostvarsling
                        );
                    }
                } catch (Exception $e) {
                    $resultat->success = false;
                    $resultat->error = $resultat->msg = $e->getMessage();
                }
                $this->settRespons(new JsonRespons($resultat));
                return $this;
            }
        }
    }

    /**
     * @param string $søkestreng
     * @param string[][] $sortering
     * @param int $start
     * @param int|null $limit
     * @return Person\Adgangsett
     * @throws Exception
     */
    protected function hentAdgangSett(
        string  $søkestreng,
        array   $sortering = [],
        int     $start = 0,
        ?int    $limit = null
    ): Person\Adgangsett
    {
        $filter = array(
            'or' => array(
                "REPLACE(LOWER(CONCAT(`personer`.`fornavn`, ' ', `personer`.`etternavn`)), 'å', 'aa') LIKE" => '%' . str_ireplace(array('Å', 'å'), 'aa', $søkestreng) . '%',
                '`leieforhold`.`leieforholdnr` LIKE' => '%' . $søkestreng . '%',
                '`leieforhold_leietaker_navn_filter`.`leietaker`' => '%' . $søkestreng . '%',
                '`leieforhold_leietaker_person_filter`.`fornavn`' => '%' . $søkestreng . '%',
                '`leieforhold_leietaker_person_filter`.`etternavn`' => '%' . $søkestreng . '%',
            )
        );

        /**
         * Mapping av sorteringsfelter
         * * kolonnenavn = > [tabellkilde el null for hovedtabell => tabellfelt]
         */
        $sorteringsfelter = [
            'id' => [null, 'adgangsid'],
            'adgangsområde' => [null, 'adgang'],
            'leieforhold_id' => [null, 'leieforhold'],
            'bruker' => ['personer', 'etternavn'],
        ];

        /** @var Person\Adgangsett $adgangSett */
        $adgangSett = $this->app->hentSamling(Person\Adgang::class);
        $adgangSett
            ->leggTilLeieforholdModell()
            ->leggTilPersonModell()
            ->leggTilLeftJoin(
                ['leieforhold_leietaker_navn_filter' => Leieforhold\Leietaker::hentTabell()],
                '`leieforhold_leietaker_navn_filter`.`leieforhold` = `' . Person\Adgang::hentTabell() . '`.`leieforhold`'
            )
            ->leggTilLeftJoin(
                ['leieforhold_leietaker_person_filter' => Person::hentTabell()],
                '`leieforhold_leietaker_person_filter`.`' . Person::hentPrimærnøkkelfelt() . '` = `leieforhold_leietaker_navn_filter`.`person`'
            )
        ;

        $adgangSett->leggTilFilter($filter);
        $adgangSett->låsFiltre();

        foreach ($sortering as $sorteringskonfig) {
            if(isset($sorteringsfelter[$sorteringskonfig['columnName']])) {
                $adgangSett->leggTilSortering(
                    $sorteringsfelter[$sorteringskonfig['columnName']][1],
                    strtolower($sorteringskonfig['dir']) == 'desc',
                    $sorteringsfelter[$sorteringskonfig['columnName']][0]
                );
            }
        }

        if ($limit) {
            $adgangSett
                ->settStart($start)
                ->begrens($limit);
        }
        return $adgangSett;
    }
}