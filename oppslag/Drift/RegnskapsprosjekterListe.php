<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\DriftExtJsAdaptor;
use Kyegil\Leiebasen\Visning\drift\js\regnskapsprosjekter_liste\ExtOnReady;
use Kyegil\Leiebasen\Visning\felles\extjs4\Index;

/**
 * RegnskapsprosjekterListe Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Drift
 */
class RegnskapsprosjekterListe extends DriftExtJsAdaptor
{
    /** @var string */
    public $tittel = 'Regnskapsprosjekter';
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => RegnskapsprosjektSkjema\Oppdrag::class
    ];


    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $brukerId = $this->bruker['id'];
        $bruker = $this->hentModell(Person::class, $brukerId);
        $this->hoveddata['bruker'] = $bruker;
        return true;
    }

    /**
     * @return Index
     */
    public function skript() {
        $bruker = $this->hoveddata['bruker'];
        if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
            $this->returi->reset();
        }

        $skript = $this->vis(Index::class, [
            'extOnReady' => $this->vis(ExtOnReady::class, [
                'bruker' => $bruker,
                'tilbakeKnapp' => $this->returi->get()
            ])
        ]);
        return $skript;
    }
}