<?php

namespace Kyegil\Leiebasen\Oppslag\Drift;

use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class _MalSkjema extends \Kyegil\Leiebasen\Oppslag\DriftKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
//        'oppgave' => _MalSkjema\Oppdrag::class
    ];
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $modellId = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Modell $modell */
        $modell = $this->hentModell(Modell::class, (int)$modellId);
        if (!$modell->hentId() || $modellId == '*') {
            $modell = null;
        }
        $this->hoveddata['modell'] = $modell;
        $this->hoveddata['modellId'] = $modellId;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Modell $modell */
        $modell = $this->hoveddata['modell'];
        if(!$modell && $this->hoveddata['modellId'] != '*') {
            return false;
        }
        $this->tittel = 'Mal';
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Modell $modell */
        $modell = $this->hoveddata['modell'];

        $visning = new ViewArray();
        $visning->addItem($this->vis(\Kyegil\Leiebasen\Visning\drift\html\_mal_skjema\Index::class, [
            'modell' => $modell,
            'modellId' => $this->hoveddata['modellId'],
        ]));

        return $visning;
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function skript(): ViewInterface
    {
        /** @var Modell $modell */
        $modell = $this->hoveddata['modell'];

        $skript = new ViewArray();
        $skript->addItem(
            $this->vis(\Kyegil\Leiebasen\Visning\drift\js\_mal_skjema\Index::class, [
                'modell' => $modell,
                'modellId' => $this->hoveddata['modellId'],
            ])
        );
        return $skript;
    }
}