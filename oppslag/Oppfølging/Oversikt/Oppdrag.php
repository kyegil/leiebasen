<?php

namespace Kyegil\Leiebasen\Oppslag\Oppfølging\Oversikt;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Vedlikehold;
use Kyegil\Leiebasen\Visning\felles\epost\shared\Epost;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function nullstillAvventing() {
        $resultat = (object)[
            'success' => true,
            'msg' => null,
            'data' => []
        ];

        try {
            $leieforholdId = $this->data['post']['leieforhold_id'];
            /** @var Leieforhold $leieforhold */
            $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);
            $leieforhold->avventOppfølging = null;
            Vedlikehold::getInstance($this->app)->sjekkOppfølgingspåminnelser();
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    protected function behandleBetalingsutsettelse() {
        $resultat = (object)[
            'success' => true,
            'msg' => 'Lagret',
            'data' => []
        ];

        try {
            $regningId = $this->data['post']['regning'] ?? null;
            $melding = $this->data['post']['melding'] ?? '';
            $beslutning = $this->data['post']['beslutning'] ?? '';
            /** @var Regning $regning */
            $regning = $this->app->hentModell(Regning::class, $regningId);
            if(!$regning->hentId()) {
                throw new \Exception('Finner ikke regning ' . $regningId);
            }
            if(!in_array($beslutning, ['avvist', 'godkjent'])) {
                throw new \Exception('Oppfattet ikke beslutningen');
            }
            $leieforhold = $regning->leieforhold;
            $forespørsel = $regning->hentUtsettelseForespørsel();
            $ønsketForfall = new \DateTimeImmutable($forespørsel->ønsket_forfall);
            $notat = $this->app->hentModell(Notat::class, $forespørsel->notat_id);
            if($forespørsel) {
                $epostMottakere = $leieforhold->hentEpost();
                $epostMottakere[] = "{$this->app->bruker['navn']} <{$this->app->bruker['epost']}>";
                $emne = "Forespørsel om betalingsutsettelse er {$beslutning}";
                $epostHtml = ["Forespørsel om betalingsutsettelse av regning {$regning} er {$beslutning}<br>"];
                if($melding) {
                    $epostHtml[] = "<br>{$melding}<br>";
                }

                if($beslutning == 'godkjent') {
                    if(!$regning->originalForfallsdato) {
                        $regning->originalForfallsdato = $regning->hentForfall();
                    }
                    $regning->settForfall($ønsketForfall);
                    $epostHtml[] = "Nytt forfall er {$regning->hentForfall()->format('d.m.Y')}<br>";
                }

                $regning->settUtsettelseForespørsel(null);

                if ($notat->hentId()) {
                    $notat->notat .= "<p>" . \Kyegil\Leiebasen\Leiebase::ucfirst($beslutning) . "</p><p>{$melding}</p>" ;
                }
                foreach(array_unique($epostMottakere) as $epostMottaker) {
                    $this->app->sendMail((object)[
                        'to' => $epostMottaker,
                        'type' => 'betalingsutsettelse_vedtak',
                        'subject' => $emne,
                        'html' => $this->app->vis(Epost::class, [
                            'tittel' => $emne,
                            'innhold' => new HtmlElement('div', [], $epostHtml),
                        ])
                    ]);
                }
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            $resultat->url = $this->app->returi->get();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}