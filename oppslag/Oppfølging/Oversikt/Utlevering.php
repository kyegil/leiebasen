<?php

namespace Kyegil\Leiebasen\Oppslag\Oppfølging\Oversikt;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData(string $data)
    {
        $resultat = (object)[
            'success' => true,
            'recordsTotal' => null,
            'recordsFiltered' => null,
            'data' => []
        ];

        try {
            switch ($data) {
                case "notater":
                    $resultat->draw = isset($this->data['get']['draw'])
                        ? intval($this->data['get']['draw'])
                        : null;
                    $start = intval($this->data['get']['start'] ?? 0);
                    $lengde = intval($this->data['get']['length'] ?? 10);
                    /** @var array[] $kolonner */
                    $kolonner = $this->data['get']['columns'] ?? [];
                    /**
                     * @var string[][] $sortering Sorteringsprioritet
                     * * column (int) Referanse til kolonnens plassering i kolonner
                     * * dir (string) asc|desc
                     */
                    $sortering = $this->data['get']['order'] ?? [];
                    $søk = $this->data['get']['search']['value'] ?? '';
                    $leieforholdId = !empty($_GET['id']) ? $_GET['id'] : null;

                    /**
                     * Mapping av sorteringsfelter
                     * * kolonnenavn = > [tabellkilde el null for hovedtabell => tabellfelt]
                     */
                    $sorteringsfelter = [
                        'id' => [null, 'id'],
                        'dato' => [null, 'dato'],
                        'kategori' => [null, 'kategori'],
                        'notat' => [null, 'notat'],
                        'brev' => [null, 'brev'],
                        'vedlegg' => [null, 'vedlegg'],
                        'frosset' => [Leieforhold::hentTabell(), 'frosset'],
                    ];

                    $filter = [];
                    if (trim($søk)) {
                        $søk = array_filter(array_unique(explode(' ', $søk)));
                        foreach ($søk as $ord) {
                            $filter[] = ['or' => [
                                '`' . Notat::hentTabell() . '`.`kategori` LIKE' => "%{$ord}%",
                                '`' . Notat::hentTabell() . '`.`leieforhold`' => $ord,
                            ]];
                        }
                    }

                    /** @var Leieforhold|null $leieforhold */
                    $leieforhold = $leieforholdId ? $this->app->hentModell(Leieforhold::class, $leieforholdId) : null;
                    if($leieforhold && !$leieforhold->hentId()) {
                        throw new Exception("Ugyldig leieforhold");
                    }

                    /** @var Leieforhold\Notatsett $notatsett */
                    $notatsett = $this->app->hentSamling(Notat::class);
                    $notatsett->leggTilSortering(Notat::hentPrimærnøkkelfelt(), true);
                    if ($leieforhold) {
                        $notatsett->leggTilFilter(['leieforhold' => $leieforhold->hentId()]);
                    }
                    $resultat->recordsTotal = $notatsett->getTotalRows();

                    foreach($sortering as $sorteringselement) {
                        $kolonneId = $sorteringselement['column'] ?? 0;
                        $synkende = $sorteringselement['dir'] == 'desc';
                        $kolonneData = $kolonner[$kolonneId]['data'] ?? null;
                        if (isset($sorteringsfelter[$kolonneData])) {
                            $notatsett->leggTilSortering($sorteringsfelter[$kolonneData][1], $synkende, $sorteringsfelter[$kolonneData][0]);
                        }
                    }
                    $notatsett->leggTilFilter($filter);
                    $notatsett->settStart($start)->begrens($lengde);
                    $resultat->recordsFiltered = $notatsett->getTotalRows();

                    foreach ($notatsett as $notat) {
                        $status = null;
                        if ($notat->leieforhold->stoppOppfølging) {
                            $status = 'stoppet';
                        }
                        if ($notat->leieforhold->avventOppfølging) {
                            $status = 'avventes';
                        }
                        $leieforholdLenke = new HtmlElement('a', [
                            'href' => '/oppfølging/index.php?oppslag=oversikt&id=' . $notat->leieforhold->hentId(),
                            'title' => 'Se notater for dette leieforholdet'
                        ], $notat->leieforhold->hentId());
                        $beskrivelse = \Kyegil\Leiebasen\Leiebase::ucfirst($notat->kategori) . ($notat->henvendelseFra ? ' ' . $notat->henvendelseFra : '');
                        if (trim($notat->notat)) {
                            $beskrivelse = $notat->notat;
                        }
                        $resultat->data[] = (object)[
                            'id' => $notat->hentId(),
                            'dato' => $notat->hentDato() ? $notat->hentDato()->format('Y-m-d') : '',
                            'kategori' => \Kyegil\Leiebasen\Leiebase::ucfirst($notat->hentKategori() ?: 'notat'),
                            'leieforhold' => $notat->leieforhold->hentId(),
                            'leieforhold_beskrivelse' => $notat->leieforhold->hentBeskrivelse(),
                            'brev' => boolval(trim($notat->hentBrevtekst())),
                            'notat' => $beskrivelse,
                            'vedlegg' => $notat->hentVedlegg(),
                            'status' => $status,
                            'frosset' => $notat->leieforhold->frosset,
                        ];
                    }
                    break;
                default:
                    throw new Exception("feil eller manglende 'data'-parameter mangler i data-forespørsel");
            }

        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->error = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}