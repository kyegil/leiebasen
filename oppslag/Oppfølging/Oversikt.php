<?php

namespace Kyegil\Leiebasen\Oppslag\Oppfølging;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Oppslag\OppfølgingKontroller;
use Kyegil\ViewRenderer\ViewArray;


/**
 * Oversikt Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\Oppfølging
 */
class Oversikt extends OppfølgingKontroller
{
    /** @var string */
    public $tittel = 'Oppfølgingstiltak, notater og henvendelser';

    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
        'oppgave' => Oversikt\Oppdrag::class
    ];

    /**
     * @inheritdoc
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $leieforholdId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['leieforhold'] = null;
        if ($leieforholdId) {
            $this->hoveddata['leieforhold'] = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold::class, $leieforholdId);
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold|null $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        if($leieforhold) {
            if(!$leieforhold->hentId()) {
                return false;
            }
            $this->tittel .= ', leieforhold ' . $leieforhold->hentId();
        }

        return true;
    }

    /**
     * @return ViewArray
     */
    public function innhold()
    {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'] ?? null;
        $visning = new ViewArray();
        $visning->addItem(
            $this->vis(\Kyegil\Leiebasen\Visning\oppfølging\html\NotatListe::class, [
                'leieforhold' => $leieforhold,
            ])
        );

        return $visning;
    }

    /**
     * @return ViewArray
     */
    public function skript(): ViewArray
    {
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'] ?? null;
        $skript = new ViewArray();

        $skript->addItem($this->vis(\Kyegil\Leiebasen\Visning\oppfølging\js\notat_liste\Index::class, [
            'leieforhold' => $leieforhold
        ]));

        return $skript;

    }
}