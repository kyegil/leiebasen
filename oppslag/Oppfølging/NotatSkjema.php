<?php

namespace Kyegil\Leiebasen\Oppslag\Oppfølging;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Visning\oppfølging\html\shared\Head;

class NotatSkjema extends \Kyegil\Leiebasen\Oppslag\OppfølgingKontroller
{
    protected $oppdrag = [
        'oppgave' => NotatSkjema\Oppdrag::class
    ];

    /**
     * @throws Exception
     */
    public function forberedHovedData()
    {
        $notatId = !empty($_GET['id']) ? $_GET['id'] : null;
        $leieforholdId = $_GET['leieforhold'] ?? null;
        $this->hoveddata['notat'] = null;
        $this->hoveddata['leieforhold'] = null;

        if ((int)$notatId) {
            $this->hoveddata['notat'] = $this->hentModell(Notat::class, (int)$notatId);
            $this->hoveddata['leieforhold'] = $this->hoveddata['notat']->hentLeieforhold();
        }
        else if ((int)$leieforholdId) {
            $this->hoveddata['leieforhold'] = $this->hentModell(Leieforhold::class, (int)$leieforholdId);
        }
        parent::forberedHovedData();
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }

        /** @var Notat|null $notat */
        $notat = $this->hoveddata['notat'];
        if ($notat) {
            if (!$notat->hentId()) {
                return false;
            }
            if ($notat->hentKategori() == Notat::KATEGORI_BETALINGSPLAN) {
                /** @var Leieforhold $leieforhold */
                $leieforhold = $this->hoveddata['leieforhold'];
                if(
                    $leieforhold->hentBetalingsplan()
                    && $leieforhold->hentBetalingsplan()->hentNotat()
                    && $leieforhold->hentBetalingsplan()->hentNotat()->hentId() == $notat->hentId()
                ) {
                    header('Location: index.php?oppslag=betalingsplan_skjema&id=' . $notat->hentId());
                    die();
                }
            }
            $this->tittel = static::ucfirst($notat->hentKategori());
        }

        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        $htmlHead
            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]));

        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Notat|null $notat */
        $notat = $this->hoveddata['notat'];
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];

        return $this->vis(\Kyegil\Leiebasen\Visning\oppfølging\html\NotatSkjema::class, [
            'notat' => $notat,
            'notatId' => $notat ? $notat->id : '*',
            'leieforhold' => $leieforhold
        ]);
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     */
    public function skript()
    {
        /** @var Notat|null $notat */
        $notat = $this->hoveddata['notat'];
        return $this->vis(\Kyegil\Leiebasen\Visning\oppfølging\js\notat_skjema\Index::class, [
            'notat' => $notat,
        ]);
    }
}