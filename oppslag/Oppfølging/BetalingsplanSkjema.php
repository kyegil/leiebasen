<?php

namespace Kyegil\Leiebasen\Oppslag\Oppfølging;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\Leiebasen\Visning\oppfølging\html\shared\Head;

class BetalingsplanSkjema extends \Kyegil\Leiebasen\Oppslag\OppfølgingKontroller
{
    protected $oppdrag = [
        'oppgave' => BetalingsplanSkjema\Oppdrag::class
    ];

    /**
     * BetalingsplanSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $notatId = !empty($_GET['id']) ? $_GET['id'] : null;
        $leieforholdId = $_GET['leieforhold'] ?? null;
        $this->hoveddata['notat'] = null;
        $this->hoveddata['leieforhold'] = null;
        $this->hoveddata['betalingsplan'] = null;

        if ((int)$notatId) {
            $this->hoveddata['notat'] = $this->hentModell(Notat::class, (int)$notatId);
            $this->hoveddata['leieforhold'] = $this->hoveddata['notat']->hentLeieforhold();
        }
        else if ((int)$leieforholdId) {
            $this->hoveddata['leieforhold'] = $this->hentModell(Leieforhold::class, (int)$leieforholdId);
        }
        if ($this->hoveddata['leieforhold']) {
            $this->hoveddata['betalingsplan'] = $this->hoveddata['leieforhold']->hentBetalingsplan();
        }
        $this->tittel = 'Betalingsplan';
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }

        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        if(!$leieforhold || !$leieforhold->hentId()) {
            return false;
        }

        /** @var Notat|null $notat */
        $notat = $this->hoveddata['notat'];
        if ($notat && !$notat->hentId()) {
            if (!$notat->hentId()) {
                return false;
            }
            if(!$notat->hentKategori() != Notat::KATEGORI_BETALINGSPLAN) {
                return false;
            }
            if(
                !$leieforhold->hentBetalingsplan()
                || !$leieforhold->hentBetalingsplan()->hentNotat()
                || $leieforhold->hentBetalingsplan()->hentNotat()->hentId() != $notat->hentId()
            ){
                return false;
            }

            $this->tittel = 'Betalingsplan';
        }

        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        $htmlHead
            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]));

        $htmlHead
            /**
             * Importer DataTables
             * @link https://datatables.net/
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => DataTable::DATATABLES_SOURCE_JS
            ]))
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => DataTable::DATATABLES_SOURCE_CSS
            ]))
            /**
             * Importer DataTables Responsive Extension
             * @link https://datatables.net/
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => DataTable::DATATABLES_SOURCE_RESPONSIVE_JS
            ]))
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => DataTable::DATATABLES_SOURCE_RESPONSIVE_CSS
            ]))
            /**
             * Importer DataTables Processing Plugin
             * @link https://datatables.net/
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.datatables.net/plug-ins/1.11.5/api/processing().js"
            ]))
        ;

        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Notat|null $notat */
        $notat = $this->hoveddata['notat'];
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        /** @var Betalingsplan|null $betalingsplan */
        $betalingsplan = $this->hoveddata['betalingsplan'];

        return $this->vis(\Kyegil\Leiebasen\Visning\oppfølging\html\betalingsplan_skjema\Index::class, [
            'notat' => $notat,
            'notatId' => $notat ? $notat->id : '*',
            'leieforhold' => $leieforhold,
            'betalingsplan' => $betalingsplan
        ]);
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     */
    public function skript()
    {
        /** @var Notat|null $notat */
        $notat = $this->hoveddata['notat'];
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        /** @var Betalingsplan|null $betalingsplan */
        $betalingsplan = $this->hoveddata['betalingsplan'];
        return $this->vis(\Kyegil\Leiebasen\Visning\oppfølging\js\betalingsplan_skjema\Index::class, [
            'notat' => $notat,
            'leieforholdId' => $leieforhold->hentId(),
            'leieforhold' => $leieforhold,
            'betalingsplan' => $betalingsplan
        ]);
    }
}