<?php

namespace Kyegil\Leiebasen\Oppslag\Oppfølging\NotatSkjema;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var \stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Notatet er lagret."
        ];
        $get = $_GET;
        $post = $_POST;
        $files = $_FILES;
        $skjema = isset($get['skjema']) ? $get['skjema'] : '';
        try {
            switch ($skjema) {
                case 'notat':
                    $notatId = $get['id'] ?? null;
                    $leieforholdId = $post['leieforhold'] ?? null;
                    $skjulForLeietaker = isset($post['skjul_for_leietaker']) && $post['skjul_for_leietaker'];
                    $kategori = isset($post['kategori']) && in_array($post['kategori'], array_keys((array)Notat::hentKategorier())) ? $post['kategori'] : 'notat';
                    $dato = isset($post['dato']) ? new DateTime($post['dato']) : null;
                    $henvendelseFra = isset($post['henvendelse_fra'])
                        && in_array(strtolower($post['henvendelse_fra']), [Notat::FRA_ANDRE, Notat::FRA_UTLEIER, Notat::FRA_LEIETAKER, Notat::FRA_FRAMLEIER])
                        ? strtolower($post['henvendelse_fra'])
                        : '';
                    $notatTekst = $post['notat'] ?? '';
                    $dokumentreferanse = $post['dokumentreferanse'] ?? '';
                    $dokumenttype = isset($post['dokumenttype']) && trim($post['dokumenttype']) ? trim($post['dokumenttype']) :  '';
                    $brevtekst = $post['brevtekst'] ?? '';
                    $stoppOppfølging = isset($post['stopp_oppfølging']) && $post['stopp_oppfølging'];
                    $avventOppfølging = isset($post['avvent_oppfølging']) && $post['avvent_oppfølging'] ? new DateTime($post['avvent_oppfølging']) : null;
                    $frysLeieforholdet = isset($post['frys_leieforholdet']) && $post['frys_leieforholdet'];
                    $vedlegg = isset($files['vedlegg']) && !empty($files['vedlegg']['name']) ? $files['vedlegg'] : null;

                    if ($vedlegg) {
                        $finfo = finfo_open(FILEINFO_MIME_TYPE);
                        $mime = finfo_file($finfo, $vedlegg['tmp_name']);
                        finfo_close($finfo);

                        switch ($mime) {
                            case 'application/msword':
                            case 'application/pdf':
                            case 'application/vnd.oasis.opendocument.spreadsheet':
                            case 'application/vnd.oasis.opendocument.text':
                            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                            case 'application/x-tar':
                            case 'application/zip':
                            case 'image/jpeg':
                            case 'image/png':
                            case 'text/plain':
                            case 'text/rtf':
                                break;
                            default:
                                throw new Exception("Filer av typen {$mime} tillates desverre ikke. Vedlegget ble ikke lagret.");
                        }

                        if ($vedlegg["size"] > 1000000) {
                            throw new Exception("Fila er for stor. Maks. filstørrelse er 1Mb.");
                        }
                    }

                    if($avventOppfølging && $avventOppfølging <= date_create()) {
                        $avventOppfølging = null;
                    }

                    if ($notatId == '*') {
                        $resultat->msg = 'Notatet er registrert';
                        /** @var Leieforhold $leieforhold */
                        $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);
                        if (!$leieforhold->hentId()) {
                            throw new Exception('Ugyldig leieforhold');
                        }
                        /** @var Notat $notatModell */
                        $notatModell = $this->app->nyModell(Notat::class, (object)[
                            'leieforhold' => $leieforhold,
                            'kategori' => $kategori,
                            'skjul_for_leietaker' => $skjulForLeietaker,
                            'dato' => $dato,
                            'notat' => $notatTekst,
                            'henvendelse_fra' => $henvendelseFra,
                            'brevtekst' => $brevtekst,
                            'dokumentreferanse' => $dokumentreferanse,
                            'dokumenttype' => strtolower($dokumenttype)
                        ]);
                    }
                    else {
                        /** @var Notat $notatModell */
                        $notatModell = $this->app->hentModell(Notat::class, $notatId);
                        if (!$notatModell->hentId()) {
                            throw new Exception('Ugyldig id');
                        }
                        $leieforhold = $notatModell->leieforhold;

                        $notatModell->sett('dato', $dato);
                        $notatModell->sett('skjul_for_leietaker', $skjulForLeietaker);
                        $notatModell->sett('notat', $notatTekst);
                        $notatModell->sett('henvendelse_fra', $henvendelseFra);
                        $notatModell->sett('brevtekst', $brevtekst);
                        $notatModell->sett('kategori', $kategori);
                        $notatModell->sett('dokumentreferanse', $dokumentreferanse);
                        $notatModell->sett('dokumenttype', strtolower($dokumenttype));
                    }

                    $leieforhold->stoppOppfølging = $stoppOppfølging;
                    if($stoppOppfølging) {
                        $avventOppfølging = null;
                    }
                    $leieforhold->avventOppfølging = $avventOppfølging;
                    $leieforhold->frosset = $frysLeieforholdet;

                    if ($vedlegg) {
                        $sti = "{$this->app->hentFilarkivBane()}/dokumenter/";
                        $filnavn = basename($vedlegg["name"]);
                        $lagerref = $sti . md5(time() . $notatId);
                        if (!is_dir($sti)){
                            mkdir($sti, 0777, true);
                        }
                        if (move_uploaded_file($vedlegg["tmp_name"], $lagerref)) {
                            $tidligereFil = $notatModell->vedlegg;
                            if(strpos($tidligereFil, $sti) !== false && file_exists($tidligereFil)) {
                                try {
                                    unlink($tidligereFil);
                                } catch (Exception $e) {
                                    $this->app->logger->error($e);
                                }
                            }
                            $notatModell->sett('vedlegg', $lagerref);
                            $notatModell->sett('vedleggsnavn', $filnavn);
                        }
                        else {
                            $resultat->msg = "Vedlegget kunne ikke lastes opp pga ukjent feil.";
                        }
                    }

                    $resultat->notat = $notatModell->hentId();
                    $resultat->url = "/oppfølging/index.php?oppslag=notat&id=" . $notatModell->hentId();

                    break;
                default:
                    $resultat->success = true;
                    break;
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}