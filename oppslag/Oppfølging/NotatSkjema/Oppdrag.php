<?php

namespace Kyegil\Leiebasen\Oppslag\Oppfølging\NotatSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function slettVedlegg() {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            $notatId = $this->data['post']['notat_id'];
            if ($notatId != $this->data['get']['id']) {
                throw new Exception('Kunne ikke slette pga feil i forespørsel');
            }
            /** @var Notat $notat */
            $notat = $this->app->hentModell(Notat::class, $notatId);
            $vedlegg = $notat->vedlegg;
            if (!$vedlegg) {
                throw new Exception('Fant ikke noe vedlegg å slette');
            }
            if(strpos($vedlegg, $this->app->hentFilarkivBane()) === false) {
                throw new Exception('Kunne ikke slette vedlegget. Det er lagret utenfor tillatt område');
            }
            if(!unlink($vedlegg)) {
                throw new Exception('Kunne ikke slette vedlegget');
            }
            $notat->vedlegg = '';
            $notat->vedleggsnavn = '';
            $resultat->msg = 'Vedlegget har blitt slettet';
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}