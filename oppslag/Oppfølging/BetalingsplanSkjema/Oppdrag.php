<?php

namespace Kyegil\Leiebasen\Oppslag\Oppfølging\BetalingsplanSkjema;


use DateInterval;
use DateTimeImmutable;
use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Visning\felles\epost\shared\Epost;
use Kyegil\ViewRenderer\ViewArray;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function avvisBetalingsplan() {
        $resultat = (object)[
            'success' => true,
            'msg' => 'Betalingsplanen er avvist',
            'data' => []
        ];

        try {
            /** @var integer $notatId */
            $notatId = $this->data['post']['notat_id'] ?? null;
            /** @var Notat $notat */
            $notat= $this->app->hentModell(Notat::class, $notatId);
            $leieforhold = $notat->hentLeieforhold();
            if(
                !$notat->hentId()
                || $notat->hentId() != $this->data['get']['id']
                || $notat->leieforhold != $this->data['get']['leieforhold']
            ) {
                throw new Exception('Ugyldig notat');
            }

            $leieforhold->settBetalingsplanForslag(null);
            $resultat->url = '/oppfølging/index.php?oppslag=leieforhold_merknader&id=' . $leieforhold->hentId();

            $this->sendVedtaksmelding($leieforhold, false);
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @return $this
     */
    protected function godkjennBetalingsplan() {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            /** @var integer $notatId */
            $notatId = $this->data['post']['notat_id'] ?? null;
            /** @var Notat $notat */
            $notat = $this->app->hentModell(Notat::class, $notatId);
            $leieforhold = $notat->hentLeieforhold();
            if(
                !$notat->hentId()
                || $notat->hentId() != $this->data['get']['id']
                || $notat->leieforhold != $this->data['get']['leieforhold']
            ) {
                throw new Exception('Ugyldig notat');
            }
            $forslag =
                $leieforhold->hentBetalingsplanForslag()
                && $leieforhold->hentBetalingsplanForslag()->notat_id == $notat->hentId()
                ? $leieforhold->hentBetalingsplanForslag()
                : null;
            if (!$forslag) {
                throw new Exception('Det foreligger ikke noe forslag til betalingsplan for dette leieforholdet.');
            }

            /** @var Kravsett $kravsett */
            $kravsett = $this->app->hentSamling(Krav::class)->filtrerEtterIdNumre($forslag->originalkrav);

            /** @var Betalingsplan $betalingsplan|null */
            $betalingsplan = $leieforhold->hentBetalingsplan();
            if(!$betalingsplan) {
                /** @var Betalingsplan $betalingsplan */
                $betalingsplan = $this->app->nyModell(Betalingsplan::class, (object)[
                    'leieforhold' => $leieforhold,
                ]);
            }
            $betalingsplan
                ->settDato(date_create_immutable($forslag->dato))
                ->settAvtale($forslag->betalingsavtale)
                ->settAvdragsbeløp($forslag->avdragsbeløp)
                ->settFølgerLeiebetalinger($forslag->følger_leiebetalinger ?? false)
                ->settTerminlengde(new DateInterval($forslag->terminlengde))
                ->settStartDato(date_create_immutable($forslag->start_dato))
                ->settForfallspåminnelseAntDager($forslag->forfallspåminnelse_ant_dager)
                ->settUtenomAvtalegiro($forslag->utenom_avtalegiro ? true : null)
                ->settNotat($notat)
            ;
            $betalingsplan->settKravsett($kravsett);
            $betalingsplan->hentAvdragsett()->slettHver();
            foreach($forslag->avdrag as $dato => $avdragBeløp) {
                $betalingsplan->leggTilAvdrag(new DateTimeImmutable($dato), $avdragBeløp);
            }
            $betalingsplan->settAktiv(true);
            $leieforhold->settBetalingsplanForslag(null);
            $leieforhold->nullstill();

            $this->sendVedtaksmelding($leieforhold, true);
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    private function sendVedtaksmelding(\Kyegil\Leiebasen\Modell\Leieforhold $leieforhold, bool $godkjent)
    {
        $vedtak = $godkjent ? 'godkjent' : 'avvist';
        $epostMottakere = $leieforhold->hentEpost();
        $emne = 'Melding om ' . $vedtak . ' betalingsplan';
        $epostHtml = new ViewArray();
        $betalingsplan = $leieforhold->hentBetalingsplan();
        $epostHtml->addItem(new HtmlElement('div', [], 'Din betalingsplan med ' . $this->app->hentValg('utleier') . ' har blitt ' . $vedtak . '.'));

        if ($godkjent) {
            $epostHtml->addItem(new HtmlElement('div', [], $betalingsplan->visUtfyltAvtaletekst($betalingsplan->hentAvtale())));
        }

        foreach(array_unique($epostMottakere) as $epostMottaker) {
            $this->app->sendMail((object)[
                'to' => $epostMottaker,
                'type' => 'betalingsplan_vedtak',
                'subject' => $emne,
                'html' => $this->app->vis(Epost::class, [
                    'tittel' => $emne,
                    'innhold' => $epostHtml
                ])
            ]);
        }
    }
}