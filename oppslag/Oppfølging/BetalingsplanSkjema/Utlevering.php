<?php

namespace Kyegil\Leiebasen\Oppslag\Oppfølging\BetalingsplanSkjema;


use DateInterval;
use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;

class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData(string $data)
    {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case "utestående":
                    $leieforholdId = $_GET['leieforhold'] ?? null;
                    $kravIder = $_GET['krav'] ?? [];
                    $kravsett = $this->app->hentSamling(Krav::class)
                        ->filtrerEtterIdNumre($kravIder)
                        ->leggTilFilter(['leieforhold' => $leieforholdId]);
                    $resultat->data = 0;
                    foreach ($kravsett as $krav) {
                        $resultat->data += $krav->utestående;
                    }
                    $resultat->msg = 'Sum: ' . $this->app->kr($resultat->data, false);
                    break;

                default:
                    throw new Exception("feil eller manglende 'data'-parameter mangler i data-forespørsel");
            }

        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param DateTime $forfallsdato
     * @return DateTime
     * @throws Exception
     */
    private function korrigerForfall(DateTime $forfallsdato): DateTime
    {
        $dag = new DateInterval('P1D');
        $dato = clone $forfallsdato;
        while(
            $dato->format('N') > 6
            || in_array($dato->format('m-d'), $this->app->bankfridager($dato->format('Y')))
        ) {
            $dato->add($dag);
        }
        return $dato;
    }
}