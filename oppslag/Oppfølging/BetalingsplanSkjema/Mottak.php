<?php

namespace Kyegil\Leiebasen\Oppslag\Oppfølging\BetalingsplanSkjema;


use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\AvtaleMal;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Kravsett;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var \stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Betalingsplanen er lagret."
        ];
        $get = $_GET;
        $post = $_POST;
        $files = $_FILES;
        $skjema = isset($get['skjema']) ? $get['skjema'] : '';
        try {
            switch ($skjema) {
                case 'notat':
                    $notatId = $get['id'] ?? null;
                    $leieforholdId = $post['leieforhold'] ?? null;
                    $dato = isset($post['dato']) ? new DateTime($post['dato']) : null;
                    $henvendelseFra = isset($post['henvendelse_fra'])
                        && in_array(strtolower($post['henvendelse_fra']), [Notat::FRA_ANDRE, Notat::FRA_UTLEIER, Notat::FRA_LEIETAKER, Notat::FRA_FRAMLEIER])
                        ? strtolower($post['henvendelse_fra'])
                        : '';
                    $notatTekst = $post['notat'] ?? '';
                    $aktiv = isset($post['aktiv']) && $post['aktiv'];
                    /** @var Kravsett|null $kravsett */
                    $kravsett = isset($post['originalkrav']) && is_array($post['originalkrav']) ? $this->app->hentSamling(Krav::class)->filtrerEtterIdNumre($post['originalkrav']) : null;
                    $terminbeløp = isset($post['terminbeløp']) && is_numeric($post['terminbeløp']) ? $post['terminbeløp'] : null;
                    $følgerLeiebetalinger = isset($post['følger_leiebetalinger']) && $post['følger_leiebetalinger'];
                    $terminlengde = isset($post['terminlengde']) && $post['terminlengde'] ? new DateInterval($post['terminlengde']) : null;
                    $startdato = isset($post['startdato']) && $post['startdato'] ? new DateTime($post['startdato']) : null;
                    $avdragFelter = $post['avdrag'] ?? [];
                    $dokumentreferanse = $post['dokumentreferanse'] ?? '';
                    $dokumenttype = isset($post['dokumenttype']) && trim($post['dokumenttype']) ? trim($post['dokumenttype']) :  '';
                    $avtaletekstGenerer = isset($post['avtaletekst-generer']) && $post['avtaletekst-generer'];
                    $avtaletekst = str_replace('<div class="avdragsoversikt"></div>', '{avdragsoversikt}', $post['avtaletekst'] ?? '');
                    $vedlegg = isset($files['vedlegg']) && !empty($files['vedlegg']['name']) ? $files['vedlegg'] : null;
                    $avtalegiro = isset($post['avtalegiro']) && $post['avtalegiro'];
                    $forfallspåminnelseAntDager = $post['forfallspåminnelse_ant_dager'] ?? null;

                    if ($vedlegg) {
                        $finfo = finfo_open(FILEINFO_MIME_TYPE);
                        $mime = finfo_file($finfo, $vedlegg['tmp_name']);
                        finfo_close($finfo);

                        switch ($mime) {
                            case 'application/msword':
                            case 'application/pdf':
                            case 'application/vnd.oasis.opendocument.spreadsheet':
                            case 'application/vnd.oasis.opendocument.text':
                            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                            case 'application/x-tar':
                            case 'application/zip':
                            case 'image/jpeg':
                            case 'image/png':
                            case 'text/plain':
                            case 'text/rtf':
                                break;
                            default:
                                throw new \Kyegil\Leiebasen\Exceptions\Feilmelding("Filer av typen {$mime} tillates desverre ikke. Vedlegget ble ikke lagret.");
                        }

                        if ($vedlegg["size"] > 1000000) {
                            throw new \Kyegil\Leiebasen\Exceptions\Feilmelding("Fila er for stor. Maks. filstørrelse er 1Mb.");
                        }
                    }

                    if ($notatId == '*') {
                        $resultat->msg = 'Betalingsplanen er registrert';
                        /** @var Leieforhold $leieforhold */
                        $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);

                        if (!$leieforhold->hentId()) {
                            throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Ugyldig leieforhold');
                        }

                        $avtaletekst = $avtaletekstGenerer
                            ? $this->forberedAvtaletekst($leieforhold)
                            : Betalingsplan::gjengiAvtalemal($avtaletekst, $this->app, $leieforhold);

                        /** @var Notat $notatModell */
                        $notatModell = $this->app->nyModell(Notat::class, (object)[
                            'leieforhold' => $leieforhold,
                            'kategori' => Notat::KATEGORI_BETALINGSPLAN,
                            'skjul_for_leietaker' => false,
                            'dato' => $dato,
                            'notat' => $notatTekst,
                            'henvendelse_fra' => $henvendelseFra,
                            'dokumentreferanse' => $dokumentreferanse,
                            'dokumenttype' => strtolower($dokumenttype)
                        ]);
                    }
                    else {
                        /** @var Notat $notatModell */
                        $notatModell = $this->app->hentModell(Notat::class, $notatId);
                        if (!$notatModell->hentId()) {
                            throw new \Kyegil\Leiebasen\Exceptions\Feilmelding('Ugyldig id');
                        }
                        $leieforhold = $notatModell->leieforhold;

                        $notatModell->sett('dato', $dato);
                        $notatModell->sett('notat', $notatTekst);
                        $notatModell->sett('henvendelse_fra', $henvendelseFra);
                        $notatModell->sett('dokumentreferanse', $dokumentreferanse);
                        $notatModell->sett('dokumenttype', strtolower($dokumenttype));
                    }

                    if ($vedlegg) {
                        $sti = "{$this->app->hentFilarkivBane()}/dokumenter/";
                        $filnavn = basename($vedlegg["name"]);
                        $lagerref = $sti . md5(time() . $notatId);
                        if (!is_dir($sti)){
                            mkdir($sti, 0777, true);
                        }
                        if (move_uploaded_file($vedlegg["tmp_name"], $lagerref)) {
                            $tidligereFil = $notatModell->vedlegg;
                            if(strpos($tidligereFil, $sti) !== false && file_exists($tidligereFil)) {
                                try {
                                    unlink($tidligereFil);
                                } catch (Exception $e) {
                                    $this->app->logger->error($e);
                                }
                            }
                            $notatModell->sett('vedlegg', $lagerref);
                            $notatModell->sett('vedleggsnavn', $filnavn);
                        }
                        else {
                            $resultat->msg = "Vedlegget kunne ikke lastes opp pga ukjent feil.";
                        }
                    }

                    /** @var Betalingsplan $betalingsplan|null */
                    $betalingsplan = $leieforhold->hentBetalingsplan();
                    if(!$betalingsplan) {
                        /** @var Betalingsplan $betalingsplan */
                        $betalingsplan = $this->app->nyModell(Betalingsplan::class, (object)[
                            'leieforhold' => $leieforhold,
                            'avtale' => $avtaletekst,
                        ]);
                    }
                    $betalingsplan->settAktiv($aktiv)
                        ->settAvtale($avtaletekst)
                        ->settDato($dato)
                        ->settAvdragsbeløp($terminbeløp)
                        ->settFølgerLeiebetalinger($følgerLeiebetalinger)
                        ->settTerminlengde($følgerLeiebetalinger ? $leieforhold->hentTerminlengde() : $terminlengde)
                        ->settStartDato($startdato)
                        ->settForfallspåminnelseAntDager($forfallspåminnelseAntDager)
                    ;
                    $betalingsplan->settKravsett($kravsett);
                    if(isset($avdragFelter['dato']) && is_array($avdragFelter['dato'])) {
                        foreach ($betalingsplan->hentAvdragsett() as $avdragIndeks => $eksisterendeAvdrag) {
                            $nyttForfall = $avdragFelter['dato'][$avdragIndeks] ?? null;
                            $nyttBeløp = $avdragFelter['beløp'][$avdragIndeks] ?? 0;
                            if (
                                $nyttForfall == $eksisterendeAvdrag->hentForfallsdato()->format('Y-m-d')
                                && $nyttBeløp == $eksisterendeAvdrag->hentBeløp()
                            ) {
                                unset($avdragFelter['dato'][$avdragIndeks], $avdragFelter['beløp'][$avdragIndeks]);
                            }
                            else {
                                $eksisterendeAvdrag->slett();
                            }
                        }
                        foreach($avdragFelter['dato'] as $avdragIndeks => $avdragForfall) {
                            $avdragBeløp = $avdragFelter['beløp'][$avdragIndeks] ?? 0;
                            /** @var DateTimeImmutable $avdragForfall */
                            $avdragForfall = new DateTimeImmutable($avdragForfall ?? null);
                            $avdrag = $betalingsplan->leggTilAvdrag($avdragForfall, $avdragBeløp);
                            if($følgerLeiebetalinger) {
                                $avdrag->settRegning($this->hentHusleieRegning($leieforhold, $avdragForfall));
                            }
                        }
                    }
                    else {
                        $betalingsplan->slettAlleAvdrag();
                    }
                    $betalingsplan->sett('notat', $notatModell);
                    if($this->app->hentValg('avtalegiro') && $betalingsplan->leieforhold->hentFbo()) {
                        $betalingsplan->settUtenomAvtalegiro($avtalegiro ? null : true);
                    }
                    $notatModell->sett('brevtekst', $betalingsplan->visUtfyltAvtaletekst($avtaletekst));
                    $leieforhold->nullstill();

                    $resultat->notat = $notatModell->hentId();
                        $resultat->url = "/oppfølging/index.php?oppslag=notat&id=" . $notatModell->hentId();

                    break;
                default:
                    $resultat->success = true;
                    break;
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @return string
     */
    private function forberedAvtaletekst(Leieforhold $leieforhold): string
    {
        /** @var AvtaleMal|null $avtaleMal */
        $avtaleMal = $this->app->hentSamling(AvtaleMal::class)
            ->leggTilFilter(['type' => AvtaleMal::TYPE_BETALINGSPLAN])
            ->hentFørste();
        if ($avtaleMal) {
            return Betalingsplan::gjengiAvtalemal($avtaleMal->mal, $this->app, $leieforhold);
        }
        return '';
    }

    /**
     * @param Leieforhold $leieforhold
     * @param DateTimeInterface $avdragForfall
     * @return Regning|null
     * @throws Exception
     */
    private function hentHusleieRegning(Leieforhold $leieforhold, DateTimeInterface $avdragForfall): ?Regning
    {
        $regningSett = $this->app->hentSamling(Regning::class);
        $regningSett->leggTilInnerJoinForKrav();
        $regningSett->leggTilFilter(['leieforhold' => $leieforhold->hentId()]);
        $regningSett->leggTilFilter(['`' . Krav::hentTabell() . '`.`forfall`' => $avdragForfall->format('Y-m-d')]);
        $regningSett->leggTilFilter(['utskriftsdato' => null]);
        return $regningSett->hentFørste();
    }
}