<?php

namespace Kyegil\Leiebasen\Oppslag\Oppfølging;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat as NotatModell;
use Kyegil\MysqliConnection\MysqliConnection;
use Kyegil\MysqliConnection\MysqliConnectionInterface;

class Notat extends \Kyegil\Leiebasen\Oppslag\OppfølgingKontroller
{
    /**
     * @throws Exception
     */
    public function forberedHovedData()
    {
        $notatId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['notat'] = $this->hentModell(NotatModell::class, (int)$notatId);
        parent::forberedHovedData();
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }

        /** @var NotatModell|null $notat */
        $notat = $this->hoveddata['notat'];
        if (!$notat || !$notat->hentId()) {
            return false;
        }
        switch($notat->hentKategori()) {
            case NotatModell::KATEGORI_BETALINGSPLAN:
                $this->tittel = 'Betalingsplan for leieforhold ' . $notat->leieforhold->id;
                break;
            default:
                $this->tittel = 'Registrert aktivitet for leieforhold ' . $notat->leieforhold->id;
        }

        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var NotatModell $notat */
        $notat = $this->hoveddata['notat'];

        switch($notat->hentKategori()) {
            case NotatModell::KATEGORI_BETALINGSPLAN:
                return $this->vis(\Kyegil\Leiebasen\Visning\oppfølging\html\betalingsplan_kort\Index::class, [
                    'notat' => $notat,
                ]);
            case '§4.18-varsel':
            case 'avtale':
            case 'brev':
            case 'forliksklage':
            case 'forliksvarsel':
            case 'inkassovarsel':
            case 'notat':
            case 'purring':
            case 'rettslig kjennelse':
            case 'tvangsfravikelsesbegjæring':
            case 'utleggsbegjæring':
            case 'utløpsvarsel':
            default:
            return $this->vis(\Kyegil\Leiebasen\Visning\oppfølging\html\notat\Index::class, [
                'notat' => $notat,
            ]);
        }
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     */
    public function skript()
    {
        /** @var NotatModell $notat */
        $notat = $this->hoveddata['notat'];

        switch($notat->hentKategori()) {
            case NotatModell::KATEGORI_BETALINGSPLAN:
                return $this->vis(\Kyegil\Leiebasen\Visning\oppfølging\js\betalingsplan_kort\Index::class, [
                    'notat' => $notat,
                ]);
            default:
                return $this->vis(\Kyegil\Leiebasen\Visning\oppfølging\js\notat\Index::class, [
                    'notat' => $notat,
                ]);
        }
    }
}