<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 07/09/2020
 * Time: 17:10
 */

namespace Kyegil\Leiebasen\Oppslag\Oppfølging\AdgangOpprett;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this|Mottak
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Adgangen er registrert."
        ];
        try {
            switch ($skjema) {
                case 'navn':
                    $oppslag = $this->data['post']['navn'] ?? null;

                    /** @var Person $person */
                    $person = $this->app->hentModell(Person::class, $oppslag);
                    if (!$person->hentId()) {
                        throw new Exception('Oppføringen ble ikke funnet');
                    }
                    $person->tildelAdgang(Adgang::ADGANG_OPPFØLGING);
                    $resultat->url = (string)$this->app->returi->get(1);

                    break;
                default:
                    $resultat->success = true;
                    break;
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}