<?php

namespace Kyegil\Leiebasen\Oppslag\Oppfølging;

use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\MysqliConnection\MysqliConnection;
use Kyegil\MysqliConnection\MysqliConnectionInterface;
use Kyegil\ViewRenderer\ViewInterface;

class ProfilSkjema extends \Kyegil\Leiebasen\Oppslag\OppfølgingKontroller
{
    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        $this->tittel = "Brukerprofil for {$bruker->hentNavn()}";
        return true;
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];

        return $this->vis(\Kyegil\Leiebasen\Visning\oppfølging\html\profil_skjema\Index::class, [
            'bruker' => $bruker
        ]);
    }
}