<?php

namespace Kyegil\Leiebasen\Oppslag\Oppfølging;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Oppslag\OppfølgingKontroller;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\oppfølging\html\shared\Head;

class AdgangListe extends OppfølgingKontroller
{
    protected $oppdrag = [
        'oppgave' => AdgangListe\Oppdrag::class
    ];
    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = "Adgang til Oppfølging";

        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        $htmlHead
            /**
             * Importer DataTables
             * @link https://datatables.net/
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_JS
            ]))
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_CSS
            ]))
            /**
             * Importer DataTables Responsive Extension
             * @link https://datatables.net/
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_RESPONSIVE_JS
            ]))
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_RESPONSIVE_CSS
            ]))
        ;

        return true;
    }

    /**
     * @return Visning
     */
    public function innhold()
    {
        return $this->vis(Visning\oppfølging\html\AdgangListe::class);
    }

    /**
     * @return Visning
     */
    public function skript()
    {
        return $this->vis(\Kyegil\Leiebasen\Visning\oppfølging\js\adgang_liste\Index::class);
    }
}