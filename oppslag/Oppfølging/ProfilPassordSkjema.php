<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\Oppfølging;

use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\OppfølgingKontroller;
use Kyegil\Leiebasen\Visning;
use Kyegil\MysqliConnection\MysqliConnection;
use Kyegil\MysqliConnection\MysqliConnectionInterface;
use Kyegil\ViewRenderer\ViewInterface;
use mysqli;

class ProfilPassordSkjema extends OppfølgingKontroller
{
    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = 'Endre passord';
        return true;
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Person $skade */
        $bruker = $this->hoveddata['bruker'];

        return $this->vis(\Kyegil\Leiebasen\Visning\oppfølging\html\profil_passord_skjema\Index::class, [
            'bruker' => $bruker
        ]);
    }

    /**
     * @return Visning|string
     */
    public function skript()
    {
        return $this->vis(\Kyegil\Leiebasen\Visning\oppfølging\js\profil_passord_skjema\Index::class);
    }
}