<?php

namespace Kyegil\Leiebasen\Oppslag;

use Exception;
use Kyegil\Leiebasen\Autoriserer;
use Kyegil\Leiebasen\Autoriserer\MinSøknad;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Modell\Søknad\Adgang;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\min_søknad\html\shared\body\Header;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\body\Footer;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\body\Main;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\body\main\ReturiBreadcrumbs;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\body\main\ReturiElement;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\Head;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\Html;
use Kyegil\ViewRenderer\ViewArray;

class MinSøknadKontroller extends AbstraktKontroller
{
    /** @var string */
    public static $standardoppslag = 'Kyegil\\Leiebasen\\Oppslag\\MinSøknad\\Index';
    /** @var string */
    public $tittel = "Min Søknad";
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @inheritdoc */
    public array $område = ['område' => 'min-søknad'];
    /** @var string[] */
    protected $tilgjengeligeSpråk = [
        'nb-NO' => 'Norsk (Bokmål)',
        'en' => 'English'
    ];

    /**
     * MinSøknadKontroller constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = [])
    {
        parent::__construct($di, $config);
        $this->returi->setBaseUrl($this->http_host . '/min-søknad/index.php', $this->hentSpråk() == 'en' ? 'My application' : 'Min søknad');
        $this->hoveddata['søknad_adgang'] = null;
        \Kyegil\Leiebasen\Leiebase::$config['class_preferences'][Autoriserer::class] = MinSøknad::class;
        $this->forberedHovedData();
    }

    /**
     * @return Autoriserer\MinSøknad
     * @throws Exception
     */
    public function hentAutoriserer(): \Kyegil\Leiebasen\Interfaces\AutorisererInterface
    {
        if(!isset($this->autoriserer)) {
            $autorisererKlasse = static::getClassPreference(Autoriserer\MinSøknad::class);
            $this->autoriserer = new $autorisererKlasse($this->hentCoreModelImplementering());
        }
        return parent::hentAutoriserer();
    }

    /**
     * Sjekker om brukeren har adgang til et angitt område.
     *    Samtidig sjekkes og oppdateres økten, og innlogging kreves om nødvendig
     *    $this->bruker fylles også ut som følger:
     *        navn:        Fullt navn
     *        id:            Brukerens id, i samsvar med personadressekort
     *        brukernavn: Brukernavn for innlogging
     *        epost:        Brukerens epostadresse
     *
     * @param string $katalog Området det ønskes adgang til
     * @param int|null $leieforhold Aktuelt leieforhold dersom området er 'mine-sider'
     * @return bool             Sant dersom innlogget bruker har adgang til området
     * @throws Exception
     */
    public function adgang($katalog = 'min-søknad', $søknadId = null) {
        /** @var \Kyegil\Leiebasen\Autoriserer\MinSøknad $autoriserer */
        $autoriserer = $this->hentAutoriserer($this->mysqli);

        $this->bruker['navn'] = $autoriserer->hentNavn();
        $this->bruker['id'] = $autoriserer->hentId();
        $this->bruker['brukernavn'] = $autoriserer->hentBrukernavn();
        $this->bruker['epost'] = $autoriserer->hentEpost();

        if (!$autoriserer->erInnlogget()) {
            return false;
        }
        /** @var Adgang $adgang */
        $adgang = $autoriserer->hentSøknadAdgang();
        /** @var Søknad $søknad */
        $søknad = $autoriserer->hentSøknad();
        if (!$søknad->hentId()) {
            return false;
        }
        $this->hoveddata['søknad_adgang'] = $adgang;
        $this->hoveddata['søknad'] = $søknad;

        return in_array($katalog, ['min-søknad']);
    }

    /**
     * @return Respons
     */
    public function hentRespons()
    {
        if(!isset($this->respons)) {
            if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
                $this->returi->reset();
            }
            $this->returi->set(null, $this->tittel);

            $this->respons = new Respons($this->vis( Html::class, [
                'head' => $this->vis(Head::class, [
                    'title' => $this->tittel
                ]),
                'brukermeny' => '',
                'hovedmeny' => $this->hoveddata['søknad_adgang'] ? $this->vis(Visning\min_søknad\html\shared\body\Hovedmeny::class, [
                    'språkKode' => $this->hentSpråk()
                ]) : null,
                'header' => $this->vis(Header::class),
                'footer' => $this->vis(Footer::class),
                'main' => $this->vis(Main::class, [
                    'tittel' => $this->tittel,
                    'innhold' => $this->innhold(),
                    'breadcrumbs' => $this->visBreadcrumbs(),
                    'varsler' => $this->visVarsler(),
                    'tilbakeknapp' => $this->visTilbakeknapp()
                ]),
                'scripts' => $this->skript()
            ]));
        }
        return parent::hentRespons();
    }

    /**
     * @return bool
     */
    public function skrivHTML()
    {
        if( !$this->preHTML() ) {
            return false;
        }
        $html = $this->hentRespons();
        echo $html;
        return true;
    }

    /**
     * @return string
     */
    public function innhold()
    {
        return '';
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     */
    public function visBreadcrumbs()
    {
        $elementer = new ViewArray();
        $elementer->setGlue(' > ');
        $sti = $this->returi->getTrace();
        /** @var bool $rot Det første elementet er rot */
        $rot = true;
        foreach($sti as $url) {
            $current = $this->returi->getCurrentLocation() == $url->url;
            $elementer->addItem(
                $this->vis(ReturiElement::class, [
                    'url' => $url->url,
                    'tittel' => $url->title,
                    'rot' => $rot,
                    'current' => $current
                ])
            );
            $rot = false;
        }
        return $this->vis(ReturiBreadcrumbs::class, [
            'crumbs' => $elementer
        ]);
    }

    /**
     * @return string
     */
    public function visTilbakeknapp()
    {
        $sti = $this->returi->getTrace();
        if(count($sti) > 1) {
            return new HtmlElement('a',
                [
                    'href' => $this->returi->get()->url,
                    'title' => $this->hentSpråk() == 'en' ? "Back to {$this->returi->get()->title}" : "Tilbake til {$this->returi->get()->title}",
                    'class' => "button tilbakeknapp"
                ],
                $this->hentSpråk() == 'en' ? 'Back' : 'Tilbake'
            );
        }
        return '';
    }

    /**
     * @return array
     */
    protected function visVarsler()
    {
        $varsler = [];
        foreach($this->hentAdvarsler(\Kyegil\Leiebasen\Leiebase::VARSELNIVÅ_ADVARSEL) as $advarsel) {
            $varsler[] = new HtmlElement('div', ['class' => 'advarsel'], $advarsel['oppsummering']);
        }
        foreach($this->hentAdvarsler(\Kyegil\Leiebasen\Leiebase::VARSELNIVÅ_ORIENTERING) as $påminnelse) {
            $varsler[] = new HtmlElement('div', ['class' => 'påminnelse'], $påminnelse['oppsummering']);
        }
        return $varsler;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!$this->adgang('min-søknad')) {
            header("Location: /min-søknad/index.php?oppslag=login");
            die();
        }
        return parent::preHTML();
    }

    /**
     * @param string $oppdrag
     * @throws Exception
     */
    public function oppdrag($oppdrag = "")
    {
        if(!$this->adgang('min-søknad')) {
            $this->responder403();
        }
        parent::oppdrag($oppdrag);
    }

    /**
     * @return ViewArray
     */
    protected function lagSpråkvelger()
    {
        $alternativeSpråk = $this->tilgjengeligeSpråk;
        unset($alternativeSpråk[$this->hentSpråk()]);
        $get = $_GET;
        $språkVelger = new ViewArray();
        foreach ($alternativeSpråk as $kode => $språk) {
            $get['l'] = $kode;
            $lenke = '/min-søknad/index.php?' . http_build_query($get);
            $a = new HtmlElement('a', [
                'href' => $lenke,
                'hreflang' => $kode,
                'title' => $språk
            ], $språk);
            $språkVelger->addItem($a);
        }
        return $språkVelger;
    }
}