<?php

namespace Kyegil\Leiebasen\Oppslag;

use Exception;
use Kyegil\Leiebasen\Autoriserer;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\Offentlig\Index;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\body\Footer;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\body\Header;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\body\Main;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\body\main\ReturiBreadcrumbs;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\body\main\ReturiElement;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\Head;
use Kyegil\Leiebasen\Visning\offentlig\html\shared\Html;
use Kyegil\ViewRenderer\ViewArray;

class OffentligKontroller extends AbstraktKontroller
{
    public static $standardoppslag = Index::class;
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @inheritdoc */
    public array $område = ['område' => 'offentlig'];

    /**
     * OffentligKontroller constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = [])
    {
        parent::__construct($di, $config);
        $this->tittel = $this->hentValg('utleier');
        $this->returi->setBaseUrl((\Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['installation_url'] ?? ''), (\Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['installation_url'] ?? ''));
        $this->forberedHovedData();
    }

    /**
     * Sjekker om brukeren har adgang til et angitt område.
     *	Samtidig sjekkes og oppdateres økten, og innlogging kreves om nødvendig
     *	$this->bruker fylles også ut som følger:
     *		navn:		Fullt navn
     *		id:			Brukerens id, i samsvar med personadressekort
     *		brukernavn: Brukernavn for innlogging
     *		epost:		Brukerens epostadresse
     *
     * @param string $katalog   Området det ønskes adgang til
     * @param int $leieforhold  Aktuelt leieforhold dersom området er 'mine-sider'
     * @return bool             Sant dersom innlogget bruker har adgang til området
     * @throws Exception
     */
    public function adgang($katalog = "offentlig", $leieforhold = null) {
        /** @var Autoriserer $autoriserer */
        $autoriserer = $this->hentAutoriserer();

        $this->bruker['navn'] = $autoriserer->hentNavn();
        $this->bruker['id'] = $autoriserer->hentId();
        $this->bruker['brukernavn'] = $autoriserer->hentBrukernavn();
        $this->bruker['epost'] = $autoriserer->hentEpost();

        return in_array($katalog, ['', 'offentlig']);
    }

    /**
     * @return Respons
     */
    public function hentRespons()
    {
        if(!isset($this->respons)) {
            if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
                $this->returi->reset();
            }
            $this->returi->set(null, $this->tittel);

            /** @var Person $bruker */
            $bruker = $this->hoveddata['bruker'];
            $this->respons = new Respons($this->vis( Html::class, [
                'head' => $this->vis(Head::class, [
                    'title' => $this->tittel . ($this->hentValg('utleier') ? ' – ' . $this->hentValg('utleier') : '')
                ]),
                'brukermeny' => '',
                'hovedmeny' => '',
                'header' => $this->vis(Header::class),
                'footer' => $this->vis(Footer::class),
                'main' => $this->vis(Main::class, [
                    'tittel' => $this->tittel,
                    'innhold' => $this->innhold(),
                    'breadcrumbs' => $this->visBreadcrumbs(),
                    'varsler' => $this->visVarsler(),
                    'tilbakeknapp' => $this->visTilbakeknapp()
                ]),
                'scripts' => $this->skript()
            ]));
        }
        return parent::hentRespons();
    }

    /**
     * @return bool
     */
    public function skrivHTML()
    {
        if( !$this->preHTML() ) {
            return false;
        }
        $html = $this->hentRespons();
        echo $html;
        return true;
    }

    /**
     * @return string
     */
    public function innhold()
    {
        return '';
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     */
    public function visBreadcrumbs()
    {
        $elementer = new ViewArray();
        $elementer->setGlue(' > ');
        $sti = $this->returi->getTrace();
        /** @var bool $rot Det første elementet er rot */
        $rot = true;
        foreach($sti as $url) {
            $current = $this->returi->getCurrentLocation() == $url->url;
            $elementer->addItem(
                $this->vis(ReturiElement::class, [
                    'url' => $url->url,
                    'tittel' => $url->title,
                    'rot' => $rot,
                    'current' => $current
                ])
            );
            $rot = false;
        }
        return $this->vis(ReturiBreadcrumbs::class, [
            'crumbs' => $elementer
        ]);
    }

    /**
     * @return string
     */
    public function visTilbakeknapp()
    {
        $sti = $this->returi->getTrace();
        if(count($sti) > 1) {
            return new HtmlElement('a',
                [
                    'href' => $this->returi->get()->url,
                    'title' => "Tilbake til {$this->returi->get()->title}",
                    'class' => "button tilbakeknapp"
                ],
                'Tilbake'
            );
        }
        return '';
    }

    /**
     * @return array
     */
    protected function visVarsler()
    {
        $varsler = [];
        foreach($this->hentAdvarsler(\Kyegil\Leiebasen\Leiebase::VARSELNIVÅ_ADVARSEL) as $advarsel) {
            $varsler[] = new HtmlElement('div', ['class' => 'advarsel'], $advarsel['oppsummering']);
        }
        foreach($this->hentAdvarsler(\Kyegil\Leiebasen\Leiebase::VARSELNIVÅ_ORIENTERING) as $påminnelse) {
            $varsler[] = new HtmlElement('div', ['class' => 'påminnelse'], $påminnelse['oppsummering']);
        }
        return $varsler;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!$this->adgang('offentlig')) {
            $this->responder403();
        }
        $bruker = $this->bruker['id'] ? $this->hentModell(Person::class, $this->bruker['id']) : null;
        $this->hoveddata['bruker'] = $bruker && $bruker->hentId() ? $bruker : null;
        return parent::preHTML();
    }

    /**
     * @param string $oppdrag
     * @throws Exception
     */
    public function oppdrag($oppdrag = "")
    {
        if(!$this->adgang('offentlig')) {
            $this->responder403();
        }
        $bruker = $this->bruker['id'] ? $this->hentModell(Person::class, $this->bruker['id']) : null;
        $this->hoveddata['bruker'] = $bruker && $bruker->hentId() ? $bruker : null;
        parent::oppdrag($oppdrag);
    }
}