<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag;

use Exception;
use Kyegil\Leiebasen\CoreModelImplementering;
use Kyegil\Leiebasen\EventManager;
use Kyegil\Leiebasen\Respons;
use Throwable;

class AbstraktKontroller extends CoreModelImplementering
{
    /** @var class-string<AbstraktKontroller> */
    public static $standardoppslag = '';
    /** @var AbstraktUtlevering */
    protected $utlevering;
    /** @var AbstraktMottak */
    protected $mottak;
    /** @var Respons */
    protected $respons;

    /**
     * Oppdrag til prosessor-mapping
     *
     * Hvert oppdrag kan ha hver sin oppdragsprosessor
     *
     * @var array<string, \class-string<AbstraktOppdrag>>
     */
    protected $oppdrag = [];

    /**
     * Forbered hoveddata
     */
    public function forberedHovedData()
    {}

    /**
     * @param string $oppdrag
     * @throws Throwable
     */
    public function oppdrag($oppdrag = "") {
        list('oppdrag' => $oppdrag) = $this->pre($this, __FUNCTION__, ['oppdrag' => $oppdrag]);
        switch ($oppdrag) {
            case 'utlevering':
            case 'hentdata':
            case 'hent_data':
                /** @var AbstraktUtlevering $utlevering */
                $utlevering = $this->hentUtlevering();
                if(!$utlevering) {
                    throw new Exception('Finner ikke ' . get_class($this) . '\\Utlevering.');
                }
                $data = $_GET['data'] ?? null;
                list('data' => $data) = $this->before($utlevering, 'hentData', ['data' => $data]);
                $utlevering->hentData($data);
                $this->after($utlevering, 'hentData', []);
                $this->respons =  $utlevering->hentRespons();
                echo $this->hentRespons();
                break;
            case 'mottak':
            case 'taimotskjema':
            case 'ta_imot_skjema':
            case 'ta_i_mot_skjema':
                /** @var AbstraktMottak $mottak */
                $mottak = $this->hentMottak();
                if(!$mottak) {
                    throw new Exception('Finner ikke ' . get_class($this) . '\\Mottak.');
                }
                $skjema = $_GET['skjema'] ?? null;

                list('skjema' => $skjema) = $this->pre($mottak, 'taIMot', ['skjema' => $skjema]);
                if(!$mottak->harRespons()) {
                    $mottak->taIMot($skjema);
                }
                $this->post($mottak, 'taIMot', []);
                $this->respons = $mottak->hentRespons();
                echo $this->hentRespons();
                break;
            default:
                /** @var AbstraktOppdrag $oppdragsProsessor */
                $oppdragsProsessor = $this->hentOppdrag($oppdrag);
                if(!$oppdragsProsessor) {
                    parent::oppdrag($oppdrag);
                    return;
                }
                $oppdragsProsessor->prosesser($oppdrag);
                $this->respons = $oppdragsProsessor->hentRespons();
                echo $this->hentRespons();
        }
        $this->post($this, __FUNCTION__, $this);
    }

    /**
     * @param AbstraktKontroller $oppslag
     * @return AbstraktKontroller
     */
    public function settOppslag(AbstraktKontroller $oppslag): AbstraktKontroller
    {
        $this->oppslag = $oppslag;
        return $this;
    }

    /**
     * @param AbstraktKontroller $utlevering
     * @return AbstraktKontroller
     */
    public function settUtlevering(AbstraktKontroller $utlevering): AbstraktKontroller
    {
        $this->utlevering = $utlevering;
        return $this;
    }

    /**
     * @param AbstraktKontroller $mottak
     * @return AbstraktKontroller
     */
    public function settMottak(AbstraktKontroller $mottak): AbstraktKontroller
    {
        $this->mottak = $mottak;
        return $this;
    }

    /**
     * @return AbstraktKontroller
     */
    public function hentOppslag(): AbstraktKontroller
    {
        return $this->oppslag;
    }

    /**
     * @param string $oppdrag
     * @return AbstraktOppdrag|null
     */
    public function hentOppdrag(string $oppdrag): ?AbstraktOppdrag
    {
        /** @var \class-string<AbstraktOppdrag>|null $oppdragsklasse */
        $oppdragsklasse = $this->oppdrag[$oppdrag] ?? null;
        if($oppdragsklasse && class_exists($oppdragsklasse) && is_a($oppdragsklasse, AbstraktOppdrag::class, true)) {
            $oppdrag = new $oppdragsklasse($this, [
                    'get' => $_GET,
                    'post' => $_POST
                ]
            );
            return $oppdrag;
        }
        return null;
    }

    /**
     * @param string $oppdrag
     * @param class-string<AbstraktOppdrag> $klasse
     */
    public function settOppdrag($oppdrag, string $klasse)
    {
        $this->oppdrag[$oppdrag] = $klasse;
    }

    /**
     * @return AbstraktKontroller|null
     */
    public function hentUtlevering(): ?AbstraktUtlevering
    {
        if(!isset($this->utlevering)) {
            $this->utlevering = false;
            /** @var string $utleveringsKlasse */
            $utleveringsKlasse = get_class($this) . '\\Utlevering';

            if(class_exists($utleveringsKlasse) && is_a($utleveringsKlasse, AbstraktUtlevering::class, true)) {
                /** @var AbstraktUtlevering utlevering */
                $this->utlevering = new $utleveringsKlasse($this, [
                    'get' => $_GET,
                    'post' => $_POST
                ]);
            }
        }
        return $this->utlevering ?: null;
    }

    /**
     * @return AbstraktMottak|null
     */
    public function hentMottak(): ?AbstraktMottak
    {
        if(!isset($this->mottak)) {
            $this->mottak = false;
            /** @var string $mottaksKlasse */
            $mottaksKlasse = get_class($this) . '\\Mottak';

            if(class_exists($mottaksKlasse) && is_a($mottaksKlasse, AbstraktMottak::class, true)) {
                $files = [];
                if (isset($_FILES)) {
                    foreach ($_FILES as $fileName => $fileData) {
                        $files[$fileName] = $this->reArrayFiles($fileData);
                    }
                }
                /** @var AbstraktMottak mottak */
                $this->mottak = new $mottaksKlasse($this, [
                    'get' => $_GET ?? null,
                    'post' => $_POST ?? null,
                    'files' => $files
                ]);
            }
        }
        return $this->mottak ?: null;
    }

    /**
     * @return Respons
     */
    public function hentRespons()
    {
        $result = EventManager::getInstance()->triggerEvent(
            get_class($this),
            __FUNCTION__,
            $this,
            [$this->respons],
            $this
        );
        return isset($result) ? reset($result) : new Respons('');
    }

    /**
     * @param Respons|string $respons
     * @return $this
     */
    public function settRespons($respons) {
        $result = EventManager::getInstance()->triggerEvent(
            get_class($this),
            __FUNCTION__,
            $this,
            [$respons],
            $this
        );
        $respons = isset($result) ? reset($result) : '';
        $this->respons = is_string($respons) ? new Respons($respons) : $respons;
        return $this;
    }

    /**
     * Placeholder for Utlevering av data
     *
     * @param $data
     * @return null
     */
    public function hentData($data)
    {
        return null;
    }

    /**
     * Placeholder for mottak av data
     *
     * @param $data
     * @return null
     */
    public function taIMot($data)
    {
        return null;
    }

    /**
     * Returner skript
     *
     * @return string
     */
    public function skript() {
        return '';
    }

    /**
     * Re-structuring the uploaded files variable
     *
     * Code taken from user contribution to PHP.net
     * @link https://www.php.net/manual/en/features.file-upload.multiple.php#125035
     *
     * @param string[][] $file_post
     * @return string[][]
     */
    protected function reArrayFiles(array &$file_post): array
    {
        $isMulti    = is_array($file_post['name']);
        $file_count    = $isMulti ? count($file_post['name']) : 1;
        $file_keys    = array_keys($file_post);

        $file_ary    = [];    //Итоговый массив
        for($i=0; $i<$file_count; $i++)
            foreach($file_keys as $key)
                if($isMulti)
                    $file_ary[$i][$key] = $file_post[$key][$i];
                else
                    $file_ary[$i][$key]    = $file_post[$key];

        return $file_ary;
    }
}
