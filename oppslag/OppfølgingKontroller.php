<?php

namespace Kyegil\Leiebasen\Oppslag;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\html\shared\body\main\ReturiBreadcrumbs;
use Kyegil\Leiebasen\Visning\felles\html\shared\body\main\ReturiElement;
use Kyegil\Leiebasen\Visning\oppfølging\html\shared\Head;
use Kyegil\Leiebasen\Visning\oppfølging\html\shared\Html;
use Kyegil\Leiebasen\Visning\oppfølging\html\shared\body\Main;
use Kyegil\ViewRenderer\ViewArray;

class OppfølgingKontroller extends AbstraktKontroller
{
    /** @var string */
    public $tittel = "Oppfølging";
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @inheritdoc */
    public array $område = ['område' => 'oppfølging'];

    /**
     * @param string $oppslag
     * @param string|null $id
     * @param string|null $oppdrag
     * @param array $queryParams
     * @param string $område
     * @return string
     */
    public static function url(
        string $oppslag,
        ?string $id = null,
        ?string $oppdrag = null,
        array $queryParams = [],
        string $område = ''
    ): string {
        return parent::url($oppslag, $id, $oppdrag, $queryParams, 'oppfølging');
    }

    /**
     * OppfølgingKontroller constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = [])
    {
        parent::__construct($di, $config);
        $this->returi->setBaseUrl($this->http_host . '/oppfølging/', 'Oppfølging');
        $this->forberedHovedData();
    }

    /**
     * @return Respons
     */
    public function hentRespons()
    {
        if(!isset($this->respons)) {
            if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
                $this->returi->reset();
            }
            $this->returi->set(null, $this->tittel);

            /** @var Person $bruker */
            $bruker = $this->hoveddata['bruker'];
            $brukermeny = $this->vis(Visning\felles\html\shared\body\Brukermeny::class, [
                'bruker' => $bruker
            ]);
            $varsler = ''; // $this->vis(Visning\felles\html\shared\body\Varsler::class);
            $hovedmeny = $this->vis(Visning\felles\html\shared\body\Hovedmeny::class, [
                'bruker' => $bruker,
                'logo' => '<h3><a href="' . static::url('', null, null,['returi' => 'default']) . '">Oppfølging</a></h3>',
            ]);
            $this->respons = new Respons($this->vis( Html::class, [
                'head' => $this->vis(Head::class, [
                    'title' => $this->tittel,
                ]),
                'brukernavn' => $bruker->hentNavn(),
                'brukermeny' => $brukermeny,
                'hovedmeny' => $hovedmeny,
                'varsler' => $varsler,
                'header' => $this->vis(Visning\oppfølging\html\shared\body\Header::class),
                'main' => $this->vis(Main::class, [
                    'tittel' => $this->tittel,
                    'innhold' => $this->innhold(),
                    'breadcrumbs' => $this->visBreadcrumbs(),
                    'varsler' => $this->visVarsler(),
                    'knapper' => ''
                ]),
                'scripts' => $this->skript()
            ]));
        }
        return parent::hentRespons();
    }

    /**
     * @return bool
     */
    public function skrivHTML()
    {
        if( !$this->preHTML() ) {
            $this->responder404();
            return false;
        }
        $html = $this->hentRespons();
        echo $html;
        return true;
    }

    /**
     * @return string
     */
    public function innhold()
    {
        return '';
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     */
    public function visBreadcrumbs()
    {
        $elementer = new ViewArray();
        $sti = $this->returi->getTrace();
        /** @var bool $rot Det første elementet er rot */
        $rot = true;
        foreach($sti as $url) {
            $current = $this->returi->getCurrentLocation() == $url->url;
            if($url->title) {
            $elementer->addItem(
                $this->vis(ReturiElement::class, [
                    'url' => $url->url,
                    'tittel' => $url->title,
                    'rot' => $rot,
                    'current' => $current
                ])
            );
            }
            $rot = false;
        }
        return $this->vis(ReturiBreadcrumbs::class, [
            'crumbs' => $elementer
        ]);
    }

    /**
     * @return string
     */
    public function visTilbakeknapp()
    {
        $sti = $this->returi->getTrace();
        if(count($sti) > 1) {
            return new HtmlElement('a',
                [
                    'role' => 'button',
                    'href' => $this->returi->get()->url,
                    'title' => "Tilbake til {$this->returi->get()->title}",
                    'class' => "btn btn-secondary tilbakeknapp"
                ],
                'Tilbake'
            );
        }
        return '';
    }

    /**
     * @return string|ViewArray
     */
    public function visKnappRad() {
        return new ViewArray([$this->visTilbakeknapp()]);
    }

    /**
     * @return array
     */
    protected function visVarsler()
    {
        $varsler = [];
        foreach($this->hentAdvarsler(\Kyegil\Leiebasen\Leiebase::VARSELNIVÅ_ADVARSEL) as $advarsel) {
            $varsler[] = new HtmlElement('div', ['class' => 'advarsel'], $advarsel['oppsummering']);
        }
        foreach($this->hentAdvarsler(\Kyegil\Leiebasen\Leiebase::VARSELNIVÅ_ORIENTERING) as $påminnelse) {
            $varsler[] = new HtmlElement('div', ['class' => 'påminnelse'], $påminnelse['oppsummering']);
        }
        return $varsler;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        $this->hoveddata['bruker'] = $this->hentModell(Person::class, $this->bruker['id']);
        return parent::preHTML();
    }

    /**
     *
     */
    public function responder404()
    {
        $this->respons = new Respons($this->vis( Html::class, [
            'head' => $this->vis(Head::class, [
                'title' => 'Ugyldig oppslag',
            ]),
            'main' => [new HtmlElement('h1', [], 'Http Feilkode 404'), new HtmlElement('p', [], 'Adressen du har oppgitt er ugyldig. Oppslaget finnes ikke.')]
        ]));
        $protocol = isset($_SERVER["SERVER_PROTOCOL"]) ? $_SERVER["SERVER_PROTOCOL"] : 'HTTP/1.1';
        $this->respons->header($protocol . ' 404 Not Found');
        die($this->hentRespons());
    }

    /**
     * @param string|null $oppslag
     * @return array
     */
    public function hentAktiveMenyElementer(?string $oppslag = null): array
    {
        $oppslag = $oppslag ?? ($_GET['oppslag'] ?? null);

        $menyElementMapping = [
        ];
        return $menyElementMapping[$oppslag] ?? [$oppslag];
    }

    /**
     * Hent menystruktur for oppfølging
     *
     * @param string $meny
     * @param Person|null $bruker
     * @return array|object[]|string[]
     * @throws Exception
     */
    public function hentMenyStruktur(string $meny = '', ?Person $bruker = null): array
    {
        extract($this->pre($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        /** @var \Kyegil\Leiebasen\Modell\Person $bruker */
        $bruker = $bruker ?: $this->hentModell(\Kyegil\Leiebasen\Modell\Person::class, $this->bruker['id']);
        if (!$bruker->hentId()) {
            $bruker = null;
        }

        $aktiveMenyElementer = $this->hentAktiveMenyElementer();
        $nåværendeElement = end($aktiveMenyElementer);

        switch($meny) {
            case 'brukermeny':
                $gåTil = (object)[
                    'id' => 'gotil',
                    'text' => 'Gå til',
                    'items' => [
                        (object)[
                            'text' => 'Drift',
                            'url' => DriftKontroller::url('')
                        ]
                    ],
                    'active' => in_array('gotil', $aktiveMenyElementer),
                ];

                if($bruker->harAdgangTil(Person\Adgang::ADGANG_MINESIDER)) {
                    $gåTil->items[] = (object)[
                        'text' => 'Mine sider',
                        'url' => MineSiderKontroller::url('')
                    ];
                }

                $resultat = [
                    (object)[
                        'text' => 'Min Brukerprofil',
                        'url' => static::url('profil_skjema', $this->bruker['id'], null, ['returi' => 'default']),
                        'active' => in_array('profil_skjema', $aktiveMenyElementer),
                        'nåværende' => 'profil_skjema' === $nåværendeElement,
                    ],
                    $gåTil,
                    (object)[
                        'text' => 'Logg av',
                        'url' => '/offentlig/index.php?oppslag=index&oppdrag=avslutt',
                        'extraClasses' => ['fas', 'fa-power-off'],
                    ],
                ];
                return $this->post($this, __FUNCTION__, $resultat, $args);
            default:

                /** @var object $oppfølgingMeny */
                $oppfølgingMeny = (object)[
                    'id' => 'oppfølging',
                    'extraClasses' => ['fa-solid', 'fa-people-arrows'],
                    'text' => 'Oppfølging',
                    'active' => in_array('oppfølging-meny', $aktiveMenyElementer),
                    'symbol' => null,
                    'items' => [
                        (object)[
                            'text' => 'Oversikt',
                            'url' => OppfølgingKontroller::url('oversikt', null, null, ['returi' => 'default']),
                            'active' => in_array('oversikt', $aktiveMenyElementer),
                            'nåværende' => 'oversikt' === $nåværendeElement,
                        ],
                        '-',
                        (object)[
                            'text' => 'Adgang til oppfølging',
                            'url' => OppfølgingKontroller::url('adgang_liste', null, null, ['returi' => 'default']),
                            'active' => in_array('adgang_liste', $aktiveMenyElementer),
                            'nåværende' => 'adgang_liste' === $nåværendeElement,
                        ],
                        '-',
                        (object)[
                            'text' => 'Tilbake til Drift',
                            'url' => DriftKontroller::url(''),
                            'active' => false,
                        ],
                    ]
                ];

                $resultat = [
                    $oppfølgingMeny,
                ];
                return $this->post($this, __FUNCTION__, $resultat, $args);
        }
    }
}