<?php

namespace Kyegil\Leiebasen\Oppslag\Sentral;

use Kyegil\Leiebasen\Modell\Person;

class Oversikt extends \Kyegil\Leiebasen\Oppslag\SentralKontroller
{
    /** @var string */
    public $tittel = 'Velkommen til Leiebasen';

    /**
     * @return \Kyegil\Leiebasen\Respons
     */
    public function hentRespons()
    {
        $this->returi->reset();
        return parent::hentRespons();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function innhold()
    {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        return $this->vis(\Kyegil\Leiebasen\Visning\sentral\html\oversikt\body\main\Index::class, [
            'bruker' => $bruker,
        ]);
    }
}