<?php

namespace Kyegil\Leiebasen\Oppslag\Sentral\ProfilSkjema;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var \stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Profilen er lagret."
        ];
        $get = $_GET;
        $post = $_POST;
        $skjema = isset($get['skjema']) ? $get['skjema'] : '';
        try {
            switch ($skjema) {
                case 'passord':
                    $resultat->msg = "Passordet er endret.";
                    $brukerId = isset($post['id']) ? $post['id'] : null;
                    $pw1 = isset($post['pw1']) ? $post['pw1'] : null;
                    $pw2 = isset($post['pw2']) ? $post['pw2'] : null;

                    if($pw1 != $pw2) {
                        throw new Exception('Passordene stemmer ikke overens.');
                    }
                    if(
                        (isset(Person\Brukerprofil::$passordLengde) && strlen($pw1) < Person\Brukerprofil::$passordLengde)
                        || 1 !== preg_match('/' . Person\Brukerprofil::$passordRegex . '/', $pw1)) {
                        throw new Exception(Person\Brukerprofil::$passordHint);
                    }

                    /** @var Person $bruker */
                    $bruker = $this->app->hentModell(Person::class, $brukerId);
                    $profil = $bruker->hentBrukerProfil();
                    $profil->settPassord(\Kyegil\Leiebasen\Modell\Person\Brukerprofil::validerPassord([$pw1, $pw2]));
                    break;
                case 'brukerprofil':
                    $brukerId = isset($post['id']) ? $post['id'] : null;
                    /** @var Person $bruker */
                    $bruker = $this->app->hentModell(Person::class, $brukerId);
                    $login = isset($post['login']) ? $post['login'] : null;
                    $epost = isset($post['epost']) ? $post['epost'] : null;
                    $fødselsdato = isset($post['fødselsdato']) ? new DateTime($post['fødselsdato']) : null;
                    $personnr = isset($post['personnr']) ? $post['personnr'] : null;
                    $adresse1 = isset($post['adresse1']) ? $post['adresse1'] : null;
                    $adresse2 = isset($post['adresse2']) ? $post['adresse2'] : null;
                    $postnr = isset($post['postnr']) ? $post['postnr'] : null;
                    $poststed = isset($post['poststed']) ? $post['poststed'] : null;
                    $land = isset($post['land']) ? $post['land'] : null;
                    $telefon = isset($post['telefon']) ? $post['telefon'] : null;
                    $mobil = isset($post['mobil']) ? $post['mobil'] : null;

                    $this
                        ->validerLogin($bruker, $login)
                        ->validerEpost($epost)
                        ->validerFødselsdato($fødselsdato)
                        ->validerPersonnr($bruker, $personnr, $fødselsdato)
                    ;

                    $bruker->hentBrukerProfil()->settBrukernavn($login);
                    if(!$bruker->hentFødselsdato() && $fødselsdato) {
                        $bruker->settFødselsdato($fødselsdato);
                    }
                    if(!$bruker->hentPersonnr() && $personnr) {
                        $bruker->settPersonnr($personnr);
                    }
                    $bruker
                        ->settEpost($epost)
                        ->settAdresse1($adresse1)->settAdresse2($adresse2)
                        ->settPostnr($postnr)->settPoststed($poststed)
                        ->settLand($land)
                        ->settTelefon($telefon)
                        ->settMobil($mobil);

                    break;
                default:
                    $resultat->success = true;
                    break;
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param Person $bruker
     * @param string $login
     * @return $this
     * @throws Exception
     */
    protected function validerLogin(Person $bruker, string $login): Mottak
    {
        $brukerprofil = $bruker->hentBrukerProfil();
        if(!$brukerprofil->kanBrukernavnBenyttes($login)) {
            throw new Exception('Dette brukernavnet kan ikke benyttes. Det er allerede i bruk.');
        }
        return $this;
    }

    /**
     * @param DateTime|null $fødselsdato
     * @return $this
     * @throws Exception
     */
    protected function validerFødselsdato(DateTime $fødselsdato = null): Mottak
    {
        if($fødselsdato) {
            $iDag = new DateTime();
            $alder = $iDag->diff($fødselsdato);
            if($alder->days > 40000 || $alder->days < 2000) {
                throw new Exception('Ugyldig fødselsdato ' . $fødselsdato->format('Y-m-d'));
            }
        }
        return $this;
    }

    /**
     * @param Person $bruker
     * @param $personnr
     * @param DateTime|null $fødselsdato
     * @return $this
     * @throws Exception
     */
    protected function validerPersonnr(Person $bruker, $personnr, DateTime $fødselsdato = null): Mottak
    {
        $bruker->validerPersonnr($personnr, $fødselsdato, $bruker->erOrg);
        return $this;
    }

    /**
     * @param string $epost
     * @return $this
     * @throws Exception
     */
    protected function validerEpost(string $epost): Mottak
    {
        $this->app->validerEpost($epost);
        return $this;
    }
}