<?php

namespace Kyegil\Leiebasen\Oppslag\Sentral;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Visning\sentral\html\_mal_kort\Index;
use Kyegil\Leiebasen\Visning\felles\html\shared\body\main\Kort;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class _MalKort extends \Kyegil\Leiebasen\Oppslag\SentralKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
//        'oppgave' => _MalKort\Oppdrag::class
    ];
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $modellId = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Modell $modell */
        $modell = $this->hentModell(Modell::class, (int)$modellId);
        if (!$modell->hentId()) {
            $modell = null;
        }
        $this->hoveddata['modell'] = $modell;
        $this->hoveddata['modellId'] = $modellId;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Modell $modell */
        $modell = $this->hoveddata['modell'];
        if(!$modell || !$modell->hentId()) {
            return false;
        }
        $this->tittel = 'Mal';
        return true;
    }

    /**
     * @return Kort
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Modell $modell */
        $modell = $this->hoveddata['modell'];

        $knapper = new ViewArray([
            $this->visTilbakeknapp(),
            new HtmlElement('a', [
                'href' => '/sentral/index.php?oppslag=_mal_skjema&id=' . $modell->hentId(),
                'class' => 'button'
            ], 'Endre'),
        ]);

        /** @var Kort $kort */
        $kort = $this->vis(Kort::class, [
            'innhold' => $this->vis(Index::class, [
                'modell' => $modell,
            ]),
            'knapper' => $knapper
        ]);
        return $kort;
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function skript(): ViewInterface
    {
        /** @var Modell $modell */
        $modell = $this->hoveddata['modell'];

        $skript = new ViewArray([]);
        return $skript;
    }
}