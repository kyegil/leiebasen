<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko\SøknadArkiv;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Modell\Søknadsett;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;

/**
 *
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData(string $data)
    {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => [],
        ];
        $get = $_GET;

        try {
            switch ($data) {
                case "søknader":
                    $resultat->data = [];
                    $typeKolonner = [];

                    /** @var Søknadsett $søknadsett */
                    $søknadsett = $this->app->hentSamling(Søknad::class);
                    $søknadsett->leggTilFilter(['aktiv' => false]);

                    foreach($søknadsett as $søknad) {
                        $oppdatert = $søknad->hentOppdatert() ?? $søknad->hentRegistrert();
                        $linje = (object)[
                            'id' => $søknad->hentId(),
                            'registrert' => $søknad->hentRegistrert()->format('Y-m-d'),
                            'oppdatert' => $oppdatert->format('Y-m-d'),
                            'beskrivelse' => $this->hentBeskrivelse($søknad),
                            'kategorier' => implode(', ', $søknad->hentKategorier()),
                            'stikkord' => implode(' ', $søknad->hentStikkord()),
                        ];
                        foreach($typeKolonner as $kolonne) {
                            if (!empty($kolonne->name)) {
                                $linje->{$kolonne->name} = $søknad->getStringValue($kolonne->name);
                            }
                        }
                        $resultat->data[] = $linje;
                    }
                    break;
                default:
                    throw new Exception("feil eller manglende 'data'-parameter mangler i data-forespørsel");
            }

        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param Søknad $søknad
     * @return string
     * @throws Exception
     */
    private function hentBeskrivelse(Søknad $søknad): string
    {
        $beskrivelse = $søknad->hent('kontaktperson_fornavn') . ' ' . $søknad->hent('kontaktperson_etternavn');
        return new HtmlElement('a', [
            'href' => '/flyko/index.php?oppslag=søknad_kort&id=' . $søknad->hentId(),
            'title' => 'Vis søknaden',
        ], $beskrivelse);
    }
}