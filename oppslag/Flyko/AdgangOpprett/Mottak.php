<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 07/09/2020
 * Time: 17:10
 */

namespace Kyegil\Leiebasen\Oppslag\Flyko\AdgangOpprett;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this|Mottak
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Adgangen er registrert."
        ];
        try {
            switch ($skjema) {
                case 'navn':
                    $oppslag = $this->data['post']['navn'] ?? null;
                    $erOrg = boolval($this->data['post']['er_organisasjon'] ?? false);
                    $fornavn = isset($this->data['post']['fornavn']) ? trim($this->data['post']['fornavn']) : '';
                    $etternavn = isset($this->data['post']['fornavn']) ? trim($this->data['post']['etternavn']) : '';
                    $organisasjonsnavn = isset($this->data['post']['organisasjonsnavn']) ? trim($this->data['post']['organisasjonsnavn']) : '';
                    $epost = $this->data['post']['epost'] ?? '';
                    $mobil = $this->data['post']['mobil'] ?? '';
                    $fødselsdato = (isset($this->data['post']['fødselsdato']) && $this->data['post']['fødselsdato']) ? new DateTime($this->data['post']['fødselsdato']) : null;
                    $orgnr = $this->data['post']['orgnr'] ?? '';
                    $adresse1 = $this->data['post']['adresse1'] ?? '';
                    $adresse2 = $this->data['post']['adresse2'] ?? '';
                    $postnr = $this->data['post']['postnr'] ?? '';
                    $poststed = $this->data['post']['poststed'] ?? '';

                    if (is_numeric($oppslag)) {
                        /** @var Person $person */
                        $person = $this->app->hentModell(Person::class, $oppslag);
                        if (!$person->hentId()) {
                            throw new Exception('Oppføringen ble ikke funnet');
                        }
                    }
                    else {
                        $this->validerEpost($epost)
                            ->validerNavn($erOrg, $fornavn, $etternavn, $organisasjonsnavn);
                        $person = $this->app->nyModell(Person::class, (object)[
                            'erOrg' => $erOrg,
                            'fornavn' => $erOrg ? '' : $fornavn,
                            'etternavn' => $erOrg ? $organisasjonsnavn : $etternavn,
                            'epost' => $epost,
                            'mobil' => $mobil,
                            'fødselsdato' => $erOrg ? null : $fødselsdato,
                            'personnr' => $erOrg ? $orgnr : null,
                            'adresse1' => $adresse1,
                            'adresse2' => $adresse2,
                            'postnr' => $postnr,
                            'poststed' => $poststed
                        ]);
                        if (!$person->hentId()) {
                            throw new Exception('Klarte ikke opprette oppføringen');
                        }
                    }
                    $person->tildelAdgang(Adgang::ADGANG_FLYKO);
                    $resultat->url = (string)$this->app->returi->get(1);

                    break;
                default:
                    $resultat->success = true;
                    break;
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param string $epost
     * @return $this
     * @throws Exception
     */
    private function validerEpost(string $epost = ''): Mottak
    {
        if (!$epost) {
            throw new Exception('Du må oppgi epost-adresse');
        }

        if ($this->app->hentSamling(Person::class)
            ->leggTilFilter(['epost' => $epost])->hentAntall()) {
            throw new Exception('Oppgitt e-post-adresse er allerede registrert.');
        }
        return $this;
    }

    /**
     * @param bool $erOrg
     * @param string $fornavn
     * @param string $etternavn
     * @param string $organisasjonsnavn
     * @return $this
     * @throws Exception
     */
    private function validerNavn(bool $erOrg, string $fornavn = '', string $etternavn = '', string $organisasjonsnavn = ''): Mottak
    {
        if ($erOrg) {
            if (!$organisasjonsnavn) {
                throw new Exception('Navnet mangler');
            }
        }
        else {
            if (!$fornavn || !$etternavn) {
                throw new Exception('Både fornavn og etternavn forventet');
            }
        }
        return $this;
    }
}