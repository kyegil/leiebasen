<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko\SøknadSkjema;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;

class Mottak extends AbstraktMottak
{
    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Søknaden er lagret."
        ];
        $get = $_GET;
        $post = $_POST;
        $files = $_FILES;
        $skjema = isset($get['skjema']) ? $get['skjema'] : '';
        try {
            switch ($skjema) {
                case 'søknad':
                    $søknadId = $get['id'] ?? null;
                    $skjemaId = $get['type'] ?? null;

                    if ($søknadId == '*'){
                        /** @var Søknad\Type $skjema */
                        $søknadType = $this->app->hentModell(Søknad\Type::class, $skjemaId);
                    }
                    else {
                        /** @var Søknad $søknad */
                        $søknad = $this->app->hentModell(Søknad::class, $søknadId);
                        if (!$søknad->hentId()) {
                            throw new Exception('Ugyldig søknad');
                        }
                        $søknadType = $søknad->type;
                    }
                    if (!$søknadType->hentId()) {
                        throw new Exception('Ugyldig søknadsskjema');
                    }

                    $aktiv = isset($post['aktiv']) && $post['aktiv'];
                    $registrert = new DateTime($post['registrert'] ?? 'now');
                    $oppdatert = new DateTime($post['oppdatert'] ?? 'now');

                    $kontaktpersonFornavn = $post['kontaktperson']['fornavn'] ?? null;
                    $kontaktpersonEtternavn = $post['kontaktperson']['etternavn'] ?? null;
                    $kontaktAdresse1 = $post['kontaktperson']['adresse1'] ?? null;
                    $kontaktAdresse2 = $post['kontaktperson']['adresse2'] ?? null;
                    $kontaktPostnr = $post['kontaktperson']['postnr'] ?? null;
                    $kontaktPoststed = $post['kontaktperson']['poststed'] ?? null;
                    $kontaktTelefon = $post['kontaktperson']['telefon'] ?? null;
                    $kontaktEpost = $post['kontaktperson']['epost'] ?? null;

                    $this->validerKontaktInfo(
                        $kontaktpersonEtternavn,
                        $kontaktpersonFornavn,
                        $kontaktTelefon,
                        $kontaktEpost,
                        $kontaktAdresse1,
                        $kontaktAdresse2,
                        $kontaktPostnr,
                        $kontaktPoststed
                    );

                    $skjemaData = $post['skjemadata'] ?? [];
                    $søknadsTekst = $skjemaData['søknadstekst'] ?? '';

                    if (!trim(strip_tags($søknadsTekst))) {
                        throw new Exception('Søknadsteksten mangler');
                    }

                    if (!isset($søknad)) {
                        $søknad = $this->app->nyModell(Søknad::class, (object)[
                            'type' => $søknadType
                        ]);
                        $notatTekst = 'Søknaden registrert';
                    }
                    else {
                        $notatTekst = 'Søknaden ble endret';
                        if($aktiv != $søknad->aktiv) {
                            $notatTekst .= ' (og ' . ($aktiv ? 'aktivert' : 'deaktivert') . ')';
                        }
                    }
                    $metaData = $søknad->hentMetaData();
                    settype($metaData, 'object');
                    $metaData->konfigurering = $søknad->type->hentKonfigurering();
                    $søknad
                        ->settAktiv($aktiv)
                        ->settRegistrert($registrert)
                        ->settOppdatert($oppdatert)
                        ->settSøknadstekst($søknadsTekst)
                        ->sett(Søknad\Type::FELT_KONTAKT_FORNAVN, $kontaktpersonFornavn)
                        ->sett(Søknad\Type::FELT_KONTAKT_ETTERNAVN, $kontaktpersonEtternavn)
                        ->sett('kontakt_adresse1', $kontaktAdresse1)
                        ->sett('kontakt_adresse2', $kontaktAdresse2)
                        ->sett('kontakt_postnr', $kontaktPostnr)
                        ->sett('kontakt_postnr', $kontaktPostnr)
                        ->sett(Søknad\Type::FELT_KONTAKT_TELEFON, $kontaktTelefon)
                        ->sett(Søknad\Type::FELT_KONTAKT_EPOST, $kontaktEpost)
                        ->settMetaData($metaData)
                        ;

                    $skjemafeltsett = $søknad->type->hentFeltsett();
                    foreach ($skjemafeltsett as $skjemafelt) {
                        $skjemafeltKode = $skjemafelt->hentKode();
                        if (isset($skjemaData[$skjemafeltKode])) {
                            $søknad->sett($skjemafeltKode, $skjemaData[$skjemafeltKode]);
                        }
                    }

                    $layout = $søknadType->hentKonfigurering('layout');
                    if(is_iterable($layout)) {
                        foreach ($layout as $layoutElement) {
                            if($layoutElement->autofilter ?? null) {
                                $søknad->lagAutoAdminFilter($layoutElement);
                            }
                        }
                    }

                    $resultat->url = strval($this->app->returi->get(1));
                    $this->app->nyModell(Søknad\Notat::class, (object)[
                        'søknad' => $søknad,
                        'tidspunkt' => new DateTime(),
                        'forfatter' => $this->app->bruker['id'],
                        'systemgenerert' => true,
                        'tekst' => $notatTekst
                    ]);
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param $epost
     * @return $this
     * @throws Exception
     */
    protected function validerEpost($epost): Mottak
    {
        if($epost && !filter_var($epost, FILTER_VALIDATE_EMAIL)) {
            throw new Exception('Ugyldig epostadresse');
        }
        return $this;
    }

    /**
     * @param string|null $kontaktpersonEtternavn
     * @param string|null $kontaktpersonFornavn
     * @param string|null $kontaktTelefon
     * @param string|null $kontaktEpost
     * @param string|null $kontaktAdresse1
     * @param string|null $kontaktAdresse2
     * @param string|null $kontaktPostnr
     * @param string|null $kontaktPoststed
     * @param string|null $språk
     * @return Mottak
     * @throws Exception
     */
    private function validerKontaktInfo(
        ?string $kontaktpersonEtternavn,
        ?string $kontaktpersonFornavn,
        ?string $kontaktTelefon,
        ?string $kontaktEpost,
        ?string $kontaktAdresse1,
        ?string $kontaktAdresse2,
        ?string $kontaktPostnr,
        ?string $kontaktPoststed,
        ?string $språk = null
    ): Mottak
    {
        if (!trim($kontaktEpost) && !trim($kontaktTelefon)) {
            throw new Exception(
                $språk == 'en'
                    ? 'Either email or phone number is required'
                    : 'Enten telefonnummer eller epost må oppgis'
            );

        }
        return $this;
    }
}