<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko\SøknadKort;


use Exception;
use Kyegil\Leiebasen\Modell\Søknad\Notat;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function slettNotat() {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            $notatId = $this->data['post']['notat_id'];
            /** @var Notat $notat */
            $notat = $this->app->hentModell(Notat::class, $notatId);
            if(!$notat->hentId()) {
                throw new Exception('Finner ikke notatet. Det er muligens allerede slettet?');
            }
            if($notat->hentSystemgenerert()) {
                throw new Exception('Notatet er system-generert og kan ikke slettes');
            }
            $resultat->msg = $notat->forfatter->hentNavn() . ' sitt notat fra ' . $notat->tidspunkt->format('d.m.Y H:i') . ' har blitt slettet';
            $notat->slett();
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}