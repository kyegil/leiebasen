<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko\SøknadKort;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use stdClass;

class Mottak extends AbstraktMottak
{
    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true
        ];
        $get = $_GET;
        $post = $_POST;
        $files = $_FILES;
        $skjema = isset($get['skjema']) ? $get['skjema'] : '';
        try {
            switch ($skjema) {
                case 'kategorier':
                    $resultat->msg = 'Kategoriseringen er lagret';
                    $søknadId = $get['id'] ?? null;
                    /** @var Søknad $søknad */
                    $søknad = $this->app->hentModell(Søknad::class, $søknadId);

                    if (!$søknad->hentId()) {
                        throw new Exception('Søknaden eksisterer ikke');
                    }

                    $søknad->settKategorier($post['kategorier'] ?? []);
                    $resultat->url = strval($this->app->returi->get());
                    break;
                case 'notat':
                    $resultat->msg = 'Notatet er lagret';
                    $søknadId = $get['søknad'] ?? null;
                    $tekst = $post['tekst'];
                    $brukerId = $this->app->bruker['id'];
                    /** @var Søknad $søknad */
                    $søknad = $this->app->hentModell(Søknad::class, $søknadId);

                    if (!$søknad->hentId()) {
                        throw new Exception('Søknaden eksisterer ikke');
                    }
                    if (!trim(strip_tags($tekst))) {
                        throw new Exception('Notatet er tomt');
                    }

                    $notat = $this->app->nyModell(Søknad\Notat::class, (object)[
                        'søknad' => $søknad,
                        'forfatter' => $brukerId,
                        'tidspunkt' => new DateTime(),
                        'tekst' => trim($tekst)
                    ]);

                    $resultat->id = $notat->hentId();
                    $resultat->url = strval($this->app->returi->get());
                    break;
                default:
                    throw new Exception('Ukjent skjema');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param $epost
     * @return $this
     * @throws Exception
     */
    protected function validerEpost($epost): Mottak
    {
        if($epost && !filter_var($epost, FILTER_VALIDATE_EMAIL)) {
            throw new Exception('Ugyldig epostadresse');
        }
        return $this;
    }

    /**
     * @param string|null $kontaktpersonEtternavn
     * @param string|null $kontaktpersonFornavn
     * @param string|null $kontaktTelefon
     * @param string|null $kontaktEpost
     * @param string|null $kontaktAdresse1
     * @param string|null $kontaktAdresse2
     * @param string|null $kontaktPostnr
     * @param string|null $kontaktPoststed
     * @param string|null $språk
     * @return Mottak
     * @throws Exception
     */
    private function validerKontaktInfo(
        ?string $kontaktpersonEtternavn,
        ?string $kontaktpersonFornavn,
        ?string $kontaktTelefon,
        ?string $kontaktEpost,
        ?string $kontaktAdresse1,
        ?string $kontaktAdresse2,
        ?string $kontaktPostnr,
        ?string $kontaktPoststed,
        ?string $språk = null
    ): Mottak
    {
        if (!trim($kontaktEpost) && !trim($kontaktTelefon)) {
            throw new Exception(
                $språk == 'en'
                    ? 'Either email or phone number is required'
                    : 'Enten telefonnummer eller epost må oppgis'
            );

        }
        return $this;
    }
}