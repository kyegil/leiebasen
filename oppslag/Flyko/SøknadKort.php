<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Oppslag\FlykoKontroller;

class SøknadKort extends FlykoKontroller
{
    protected $oppdrag = [
        'oppgave' => SøknadKort\Oppdrag::class
    ];

    /**
     * SøknadListe constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $søknadId = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Søknad $søknad */
        $søknad = $this->hentModell(Søknad::class, $søknadId);
        $this->hoveddata['søknad'] = $søknad && $søknad->hentId() ? $søknad : null;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Søknad|null $søknad */
        $søknad = $this->hoveddata['søknad'];
        $this->tittel = 'Søknad';
        if($søknad) {
            $navn = $søknad->hent('kontaktperson_fornavn') . ' ' . $søknad->hent('kontaktperson_etternavn');
            $this->tittel = 'Søknad – ' . $navn;
        }
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        $htmlHead
            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]))
        ;

        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     */
    public function innhold()
    {
        return $this->vis(\Kyegil\Leiebasen\Visning\flyko\html\søknad_kort\body\main\Index::class, [
            'søknad' => $this->hoveddata['søknad']
        ]);
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     */
    public function skript()
    {
        return $this->vis(\Kyegil\Leiebasen\Visning\flyko\js\søknad_kort\body\main\Index::class, [
            'søknad' => $this->hoveddata['søknad']
        ]);
    }
}