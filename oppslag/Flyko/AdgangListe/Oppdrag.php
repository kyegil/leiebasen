<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko\AdgangListe;


use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Modell\Person\Adgangsett;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return $this
     */
    protected function slettAdgang() {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            $personId = $this->data['post']['person_id'];
            /** @var Person $person */
            $person = $this->app->hentModell(Person::class, $personId);
            $resultat->msg = $person->hentNavn() . ' sin adgang til ' . Adgang::ADGANG_FLYKO . ' har blitt slettet';
            /** @var Adgangsett $adganger */
            $adganger = $this->app->hentSamling(Adgang::class)
                ->leggTilFilter(['adgang' => Adgang::ADGANG_FLYKO])
                ->leggTilFilter(['personid' => $personId]);
            if (!$adganger->slettAlle()) {
                throw new Exception('Klarte ikke slette');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}