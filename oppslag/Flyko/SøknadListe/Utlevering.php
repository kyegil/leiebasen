<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko\SøknadListe;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Modell\Søknad\Type;
use Kyegil\Leiebasen\Modell\Søknadsett;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Søknadprosessor;

/**
 *
 */
class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData(string $data)
    {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];
        $get = $_GET;

        try {
            switch ($data) {
                case "søknader":
                    $typeId = $get['id'] ?? null;
                    $kategorier = $get['kategorier'] ?? [];
                    $resultat->data = [];
                    $typeKolonner = [];
                    $iDag = new \DateTimeImmutable();

//                    $a = 'http://leiebasen.kyegil.net/flyko/index.php?oppslag=søknad_liste&returi=default&kategorier[]=enslige&kategorier[]=kollektiv';
//                    $a = $this->app->returi->get();
//                    $a = $this->app->returi->set();

                    /** @var Type|null $søknadType */
                    $søknadType = $typeId ? $this->app->hentModell(Type::class, $typeId) : null;

                    /** @var Søknadsett $søknadsett */
                    $søknadsett = $this->app->hentSamling(Søknad::class);
                    $søknadsett->leggTilFilter(['aktiv' => true]);
                    if ($søknadType) {
                        $typeKolonner = $søknadType->hentKonfigurering('kolonner') ?? [];
                        $søknadsett->leggTilFilter(['søknadstype' => $søknadType->hentId()]);
                    }
                    foreach ($kategorier as $kategori) {
                        $kategori = $this->app->mysqli->real_escape_string($kategori);
                        $søknadsett
                            ->leggTilLeftJoin([$kategori => Søknad\Kategori::hentTabell()],
                                '`' . $kategori . '`.`søknad_id` = `' . Søknad::hentTabell() . '`.`' . Søknad::hentPrimærnøkkelfelt() . '`
                                AND `' . $kategori . "`.`kategori` = '{$kategori}'"
                            )
                            ->leggTilFilter(['`' . $kategori . '`.`søknad_id` IS NOT NULL']);
                        ;
                    }

                    foreach($søknadsett as $søknad) {
                        $gyldighetstid = Søknadprosessor::getInstance($this->app)->hentGyldighet($søknad->type);
                        $oppdatert = $søknad->hentOppdatert() ?? $søknad->hentRegistrert();
                        $utløpt = $gyldighetstid && ($oppdatert < $iDag->sub($gyldighetstid));
                        $linje = (object)[
                            'id' => $søknad->hentId(),
                            'registrert' => $søknad->hentRegistrert()->format('Y-m-d'),
                            'oppdatert' => $oppdatert->format('Y-m-d'),
                            'beskrivelse' => $this->hentBeskrivelse($søknad),
                            'kategorier' => implode(', ', $søknad->hentKategorier()),
                            'stikkord' => implode(' ', $søknad->hentStikkord()),
                            'utløpt' => $utløpt,
                        ];
                        foreach($typeKolonner as $kolonne) {
                            if (!empty($kolonne->name)) {
                                $linje->{$kolonne->name} = $søknad->getStringValue($kolonne->name);
                            }
                        }
                        $resultat->data[] = $linje;
                    }
                    break;
                default:
                    throw new Exception("feil eller manglende 'data'-parameter mangler i data-forespørsel");
            }

        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param Søknad $søknad
     * @return string
     * @throws Exception
     */
    private function hentBeskrivelse(Søknad $søknad): string
    {
        $beskrivelse = $søknad->hent('kontaktperson_fornavn') . ' ' . $søknad->hent('kontaktperson_etternavn');
        return new HtmlElement('a', [
            'href' => '/flyko/index.php?oppslag=søknad_kort&id=' . $søknad->hentId(),
            'title' => 'Vis søknaden',
        ], $beskrivelse);
    }
}