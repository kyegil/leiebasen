<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko;

use Exception;
use Kyegil\MysqliConnection\MysqliConnectionInterface;
use mysqli;

class LeieobjekterLedige extends \Kyegil\Leiebasen\Oppslag\FlykoKontroller
{
    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = "Ledige leieobjekter";
        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     */
    public function innhold()
    {
        $html =  $this->vis(\Kyegil\Leiebasen\Visning\flyko\html\leieobjekter_ledige\body\main\Index::class, []);
        return $html;
    }
}