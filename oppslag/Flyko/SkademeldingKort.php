<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\FlykoKontroller;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\flyko\html\shared\Head;
use Kyegil\Leiebasen\Visning\flyko\html\skademelding_kort\body\main\Index;

class SkademeldingKort extends FlykoKontroller
{
    /**
     * SkademeldingKort constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $skadeId = isset($_GET['id']) ? $_GET['id'] : null;

        /** @var Leieobjekt\Skade $skade */
        $skade = $this->hentModell(Leieobjekt\Skade::class, $skadeId);
        $this->hoveddata['skade'] = $skade->hentId() ? $skade : null;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Leieobjekt\Skade $skade */
        $skade = $this->hoveddata['skade'];
        if(!$skade || !$skade->hentId()) {
            return false;
        }
        $this->tittel = $this->hoveddata['skade']->hentSkade();

        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        $htmlHead
            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]));

        return true;
    }

    /**
     * @return Visning
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        /** @var Leieobjekt\Skade $skade */
        $skade = $this->hoveddata['skade'];

        return $this->vis(Index::class, [
            'bruker' => $bruker,
            'skade' => $skade
        ]);

    }
}