<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Oppslag\FlykoKontroller;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\flyko\html\shared\Head;

class AdgangListe extends FlykoKontroller
{
    protected $oppdrag = [
        'oppgave' => AdgangListe\Oppdrag::class
    ];

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = "Adganger til FlyKo' ressursområde";

        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        $htmlHead
            /**
             * Importer DataTables
             * @link https://datatables.net/
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_JS
            ]))
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_CSS
            ]))
            /**
             * Importer DataTables Responsive Extension
             * @link https://datatables.net/
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_RESPONSIVE_JS
            ]))
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_RESPONSIVE_CSS
            ]))
        ;

        return true;
    }

    /**
     * @return Visning
     */
    public function innhold()
    {
        return $this->vis(\Kyegil\Leiebasen\Visning\flyko\html\adgang_liste\body\main\Index::class);
    }

    /**
     * @return Visning
     */
    public function skript()
    {
        return $this->vis(\Kyegil\Leiebasen\Visning\flyko\js\adgang_liste\body\main\Index::class);
    }
}