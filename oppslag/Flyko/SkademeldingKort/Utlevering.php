<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko\SkademeldingKort;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd\Innlegg;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Leieobjekt\Skade;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons;
use Kyegil\ViewRenderer\ViewArray;

class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData(string $data)
    {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case "vedlegg":
                    $skadeId = isset($_GET['id']) ? $_GET['id'] : null;
                    /** @var Skade $skade */
                    $skade = $this->app->hentModell(Skade::class, $skadeId);
                    $innleggId = isset($_GET['innlegg']) ? $_GET['innlegg'] : null;
                    /** @var Innlegg $innlegg */
                    $innlegg = $this->app->hentModell(Innlegg::class, $innleggId);
                    $vedlegg = $innlegg->hentVedlegg();
                    $fil = "{$this->app->hentFilarkivBane()}/beboerstøtte/drift/{$innlegg}-{$vedlegg}";

                    if(!$skade->hentId()) {
                        throw new Exception('Skaden finnes ikke.');
                    }
                    if(
                        $innlegg->hentSak()->hentId() != $skade->hentBeboerstøtte()->hentId()
                        || !$vedlegg
                        || !file_exists($fil)
                    ) {
                        throw new Exception('Vedlegget finnes ikke.');
                    }
                    $respons = new Respons(file_get_contents($fil));
                    $respons->header('Content-type: ' . mime_content_type($fil));
                    $respons->header('Content-Disposition: attachment; filename="' . $vedlegg . '"');
                    $respons->header('Content-Transfer-Encoding: binary');
                    $respons->header('Content-Length: ' . strlen(file_get_contents($fil)));
                    $respons->header('Accept-Ranges: bytes');

                    $this->settRespons($respons);
                    return $this;

                default:
                    throw new Exception("feil eller manglende 'data'-parameter mangler i data-forespørsel");
            }

        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            $this->settRespons(new JsonRespons($resultat));
        }
        return $this;
    }
}