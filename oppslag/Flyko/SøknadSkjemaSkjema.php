<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Søknad\Type;
use Kyegil\Leiebasen\Visning\flyko\html\shared\Head;
use Kyegil\Leiebasen\Visning\flyko\html\søknad_skjema_skjema\body\main\Index;

class SøknadSkjemaSkjema extends \Kyegil\Leiebasen\Oppslag\FlykoKontroller
{
    /**
     * SøknadSkjema skjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $søknadSkjemaId = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Type $søknadSkjema */
        $søknadSkjema = $søknadSkjemaId == '*' ? null : $this->hentModell(Type::class, $søknadSkjemaId);
        $this->hoveddata['søknadsskjema'] = $søknadSkjema;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Type|null $søknadSkjema */
        $søknadSkjema = $this->hoveddata['søknadsskjema'];
        if ($søknadSkjema && !$søknadSkjema->hentId()) {
            return false;
        }
        $this->tittel = $søknadSkjema ? $søknadSkjema->hentNavn() : "Nytt søknadsskjema";

        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        $htmlHead
            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]));

        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     * @throws \Exception
     */
    public function innhold()
    {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        /** @var Type|null $søknadSkjema */
        $søknadSkjema = $this->hoveddata['søknadsskjema'];

        return $this->vis(Index::class, [
            'bruker' => $bruker,
            'søknadsskjema' => $søknadSkjema
        ]);
    }
}