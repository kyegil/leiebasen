<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko;

use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\FlykoKontroller;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\flyko\html\adresse_kort\body\main\Index;
use Kyegil\MysqliConnection\MysqliConnection;
use Kyegil\MysqliConnection\MysqliConnectionInterface;

class AdresseKort extends FlykoKontroller
{
    /**
     * AdresseKort constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $personId = isset($_GET['id']) ? $_GET['id'] : null;

        /** @var Person $person */
        $person = $this->hentModell(Person::class, $personId);
        $this->hoveddata['person'] = $person->hentId() ? $person : null;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Person $person */
        $person = $this->hoveddata['person'];
        if(!$person || !$person->hentId()) {
            return false;
        }
        $this->tittel = $person->hentNavn();
        return true;
    }

    /**
     * @return Visning
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        /** @var Person $person */
        $person = $this->hoveddata['person'];

        return $this->vis(Index::class, [
            'bruker' => $bruker,
            'adressekort' => $person
        ]);
    }
}