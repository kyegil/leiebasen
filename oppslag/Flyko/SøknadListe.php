<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Søknad\Type;
use Kyegil\Leiebasen\Oppslag\FlykoKontroller;
use Kyegil\Leiebasen\Visning\flyko\html\shared\Head;

class SøknadListe extends FlykoKontroller
{

    /**
     * @return void
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $typeId = !empty($_GET['id']) ? $_GET['id'] : 1;
        /** @var Type $søknadType */
        $søknadType = $this->hentModell(Type::class, $typeId);
        $this->hoveddata['søknad_type'] = $søknadType && $søknadType->hentId() ? $søknadType : null;
        $this->hoveddata['kategorier'] = $_GET['kategorier'] ?? [];
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = "Innkomne søknader";

        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        if($this->hoveddata['søknad_type']) {
            $htmlHead->sett('title', $this->hoveddata['søknad_type']->hentNavn() . ' – Innkomne søknader');
            $this->tittel = 'Innkomne søknader (' . $this->hoveddata['søknad_type']->hentKode() . ')';
        }
        $htmlHead
            /**
             * Importer DataTables
             * @link https://datatables.net/
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_JS
            ]))
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_CSS
            ]))
            /**
             * Importer DataTables Responsive Extension
             * @link https://datatables.net/
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_RESPONSIVE_JS
            ]))
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_RESPONSIVE_CSS
            ]))

            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]))
        ;

        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     */
    public function innhold()
    {
        return $this->vis(\Kyegil\Leiebasen\Visning\flyko\html\søknad_liste\body\main\Index::class, [
            'søknad_type' => $this->hoveddata['søknad_type'],
            'kategorier' => $this->hoveddata['kategorier']
        ]);
    }

    /**
     * @return \Kyegil\Leiebasen\Visning|\Kyegil\ViewRenderer\ViewInterface|string
     */
    public function skript()
    {
        return $this->vis(\Kyegil\Leiebasen\Visning\flyko\js\søknad_liste\body\main\Index::class, [
            'søknad_type' => $this->hoveddata['søknad_type']
        ]);
    }
}