<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko;

use Kyegil\Leiebasen\Modell\Person;
use Kyegil\MysqliConnection\MysqliConnectionInterface;

class Oversikt extends \Kyegil\Leiebasen\Oppslag\FlykoKontroller
{
    /** @var string */
    public $tittel = 'Flytte-koordineringsgruppa';

    /**
     * @return \Kyegil\Leiebasen\Respons
     */
    public function hentRespons()
    {
        $this->returi->reset();
        return parent::hentRespons();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function innhold()
    {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        return $this->vis(\Kyegil\Leiebasen\Visning\flyko\html\oversikt\body\main\Index::class, [
            'bruker' => $bruker,
        ]);
    }
}