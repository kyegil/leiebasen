<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko\SøknadSkjemaSkjema;


use Exception;
use JsonException;
use Kyegil\Leiebasen\Modell\Søknad\Type;
use Kyegil\Leiebasen\Modell\Søknad\Typesett;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use stdClass;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => 'Søknadsskjemaet er registrert.'
        ];
        $get = $_GET;
        $post = $_POST;
        $skjema = isset($get['skjema']) ? $get['skjema'] : '';
        try {
            switch ($skjema) {
                case 'søknadsskjema':
                    $skjemaId = $get['id'] ?? null;
                    $kode = $post['kode'] ?? null;
                    $navn = $post['navn'] ?? null;
                    $beskrivelse = $post['beskrivelse'] ?? null;
                    $konfigurering = $post['konfigurering'] ?? 'null';
                    $js = $post['js'] ?? 'null';

                    if (!$kode) {
                        throw new Exception('Skjema-kode mangler');
                    }

                    $this
                        ->validerKode($kode, $skjemaId)
                        ->validerKonfigurering($konfigurering);

                    $konfigurering = json_decode($konfigurering) ?? 'null';
                    if ($js) {
                        $konfigurering->javaScript = $js;
                    }

                    if ($skjemaId == '*') {
                        $resultat->msg = 'Skjemaet er registrert';
                        /** @var Type $skjema */
                        $skjema = $this->app->nyModell(Type::class, (object)[
                            'kode' => $kode,
                            'navn' => $navn,
                            'beskrivelse' => $beskrivelse,
                            'konfigurering' => $konfigurering
                        ]);
                    }
                    else {
                        /** @var Type $skjema */
                        $skjema = $this->app->hentModell(Type::class, $skjemaId);
                        $skjema->sett('kode', $kode);
                        $skjema->sett( 'navn', $navn);
                        $skjema->sett('beskrivelse', $beskrivelse);
                        $skjema->sett('konfigurering', $konfigurering);
                    }

                    $resultat->skjema = $skjema->hentId();
                    $resultat->url = (string)$this->app->returi->get(1);
                    break;

                default:
                    $resultat->success = true;
                    break;
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param string $kode
     * @param string $skjemaId
     * @return Mottak
     * @throws Exception
     */
    private function validerKode(string $kode, string $skjemaId): Mottak
    {
        /** @var Typesett $eksisterendeSkjema */
        $eksisterendeSkjema = $this->app->hentSamling(Type::class)
            ->leggTilFilter(['kode' => $kode])
            ->leggTilFilter(['`' . Type::hentTabell() . '`.`' . Type::hentPrimærnøkkelfelt() . '` <>' => $skjemaId])
        ;
        if ($eksisterendeSkjema->hentFørste()) {
            throw new Exception('Kode ' . $kode . ' er allered i bruk av skjema ' . $eksisterendeSkjema->hentFørste()->id);
        }
        return $this;
    }

    /**
     * @param string $konfigurering
     * @return Mottak
     * @throws JsonException
     */
    private function validerKonfigurering(string $konfigurering): Mottak
    {
        try {
            $objekt = json_decode($konfigurering, false, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            throw new Exception('Feil i konfigureringsfeltet: ' . $e->getMessage());
        }
        $this->validerUnikeIndekser($objekt->layout ?? []);
        return $this;
    }

    /**
     * @param array|object $konfigurering
     * @param array $registrerteIndexer
     * @return $this
     * @throws Exception
     */
    private function validerUnikeIndekser($konfigurering, array &$registrerteIndexer = [])
    {
        if(is_array($konfigurering)) {
            foreach ($konfigurering as $element) {
                $this->validerUnikeIndekser($element, $registrerteIndexer);
            }
        }
        if (is_object($konfigurering)) {
            foreach($konfigurering as $egenskap => $verdi) {
                $this->validerUnikeIndekser($verdi, $registrerteIndexer);
                if ($egenskap == 'id') {
                    if (in_array($verdi, $registrerteIndexer)) {
                        throw new Exception('Id "' . $konfigurering->id . '" kan ikke brukes mer enn én gang.');
                    }
                    $registrerteIndexer[] = $verdi;
                }
            }
        }
        return $this;
    }
}