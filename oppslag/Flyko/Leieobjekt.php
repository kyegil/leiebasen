<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko;

use Exception;
use Kyegil\Leiebasen\Visning\flyko\html\shared\Html;
use Kyegil\MysqliConnection\MysqliConnection;
use Kyegil\MysqliConnection\MysqliConnectionInterface;
use mysqli;

class Leieobjekt extends \Kyegil\Leiebasen\Oppslag\FlykoKontroller
{
    /**
     * Leieforhold constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $leieobjektId = isset($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['leieforhold'] = null;
        $this->hoveddata['leieobjekt'] = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieobjekt::class, $leieobjektId);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var \Kyegil\Leiebasen\Modell\Leieobjekt $leieobjekt */
        $leieobjekt = $this->hoveddata['leieobjekt'];
        if(!$leieobjekt->hentId()) {
            return false;
        }
        if($leieobjekt->boenhet) {
            $this->tittel = "Leilighet nr. {$leieobjekt->hentId()}";
        }
        else {
            $this->tittel = "Lokale nr. {$leieobjekt->hentId()}";
        }

        /** @var Html $html */
        $html = $this->hentRespons()->hentInnhold();
        $html->getData('head')->setData('title', $leieobjekt->hentBeskrivelse());
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieobjekt $leieobjekt */
        $leieobjekt = $this->hoveddata['leieobjekt'];

        $html = $this->vis(\Kyegil\Leiebasen\Visning\flyko\html\leieobjekt\body\main\Index::class, [
            'leieobjekt' => $leieobjekt,
        ]);
        return $html;
    }
}