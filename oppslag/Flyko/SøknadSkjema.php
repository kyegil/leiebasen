<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Søknad;
use Kyegil\Leiebasen\Modell\Søknad\Type;
use Kyegil\Leiebasen\Visning\flyko\html\shared\Head;
use Kyegil\Leiebasen\Visning\flyko\html\søknad_skjema\body\main\Index;
use Kyegil\Leiebasen\Visning\flyko\js\søknad_skjema\body\main\Index as flykoJs;
use Kyegil\Leiebasen\Visning\offentlig\js\søknad_skjema\body\main\Index as offentligJs;

class SøknadSkjema extends \Kyegil\Leiebasen\Oppslag\FlykoKontroller
{
    /**
     * SøknadSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $søknadSkjemaId = $_GET['skjema'] ?? null;
        $søknadId = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Søknad $søknad */
        $søknad = $søknadId == '*' ? null : $this->hentModell(Søknad::class, $søknadId);
        /** @var Type $søknadSkjema */
        $søknadSkjema = $søknadId == '*' ? $this->hentModell(Type::class, $søknadSkjemaId) : $søknad->hentType();
        $this->hoveddata['søknad'] = $søknad;
        $this->hoveddata['søknadsskjema'] = $søknadSkjema;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Type $søknadSkjema */
        $søknadSkjema = $this->hoveddata['søknadsskjema'];
        /** @var Søknad|null $søknad */
        $søknad = $this->hoveddata['søknad'];
        if ($søknad && (!$søknad->hentId()) || !$søknadSkjema->hentId()) {
            return false;
        }
        $this->tittel = $søknad ? $søknad->type->hentNavn() : "Registrér innkommen søknad";

        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        $htmlHead
            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]))
        ;

        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     * @throws \Exception
     */
    public function innhold()
    {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        /** @var Type $søknadSkjema */
        $søknadSkjema = $this->hoveddata['søknadsskjema'];
        /** @var Søknad|null $søknad */
        $søknad = $this->hoveddata['søknad'];

        return $this->vis(Index::class, [
            'bruker' => $bruker,
            'søknadsskjema' => $søknadSkjema,
            'søknad' => $søknad
        ]);
    }

    /**
     * @return flykoJs|string
     */
    public function skript()
    {
        /** @var Type $skjema */
        $skjema = $this->hoveddata['søknadsskjema'];
        $språk = $this->hentSpråk();
        return $this->vis(flykoJs::class, [
            'søknadSkjema' => $skjema,
            'språkKode' => $språk,
            'script' => $this->vis(offentligJs::class, [
                'søknadSkjema' => $skjema,
                'språkKode' => $språk
            ])
        ]);
    }
}