<?php

namespace Kyegil\Leiebasen\Oppslag\Flyko\SkademeldingSkjema;


use DateTime;
use Exception;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Leieobjekt\Skade;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var \stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => 'Skaden er registrert.'
        ];
        $get = $_GET;
        $post = $_POST;
        $files = $_FILES;
        $skjema = isset($get['skjema']) ? $get['skjema'] : '';
        try {
            switch ($skjema) {
                case 'skademelding':
                    $skadeId = $get['id'] ?? null;
                    $skadenavn = $post['skade'] ?? null;
                    $beskrivelse = $post['beskrivelse'] ?? null;
                    $kategorier = $post['kategorier'] ?? [];
                    $bygningsId = $post['bygning'] ?? null;
                    $leieobjektId = $post['leieobjekt'] ?? null;
                    $gjelderBygning = isset($post['gjelder_bygning']) && (bool)$post['gjelder_bygning'];

                    if (!$skadenavn || !$beskrivelse) {
                        throw new Exception('Skadenavn eller beskrivelse mangler');
                    }

                    /** @var Bygning $bygning */
                    $bygning = $this->app->hentModell(Bygning::class, $bygningsId);

                    /** @var Leieobjekt $leieobjekt */
                    $leieobjekt = $this->app->hentModell(Leieobjekt::class, $leieobjektId);

                    if ($gjelderBygning) {
                        if (!$bygning->hentId()) {
                            throw new Exception('Bygningen er ikke angitt');
                        }
                        $leieobjekt = null;
                    }
                    else {
                        if (!$leieobjekt->hentId()) {
                            throw new Exception('Leieobjektet er ikke angitt');
                        }
                        $bygning = $leieobjekt->hentBygning();
                    }


                    if ($skadeId == '*') {
                        $resultat->msg = 'Skaden er registrert';
                        /** @var Person $registrerer */
                        $registrerer = $this->app->hentModell(Person::class, $this->app->bruker['id']);
                        /** @var Skade $skade */
                        $skade = $this->app->nyModell(Skade::class, (object)[
                            'leieobjekt' => $leieobjekt,
                            'bygning' => $bygning,
                            'registrerer' => $registrerer,
                            'skade' => $skadenavn,
                            'beskrivelse' => $beskrivelse,
                            'kategorier' => $kategorier
                        ]);
                        $skade->opprettBeboerStøtte($registrerer);
                    }
                    else {
                        $skade = $this->app->hentModell(Skade::class, $skadeId);
                        if ($skade->utført) {
                            throw new Exception('Denne skaden er allerede registrert som utbedret');
                        }
                        $skade->sett('leieobjekt', $leieobjekt);
                        $skade->sett( 'bygning', $bygning);
                        $skade->sett('skade', $skadenavn);
                        $skade->sett('beskrivelse', $beskrivelse);
                        $skade->sett('kategorier', $kategorier);
                    }

                    $resultat->skade = $skade->hentId();
                    $resultat->url = $this->app->returi->get(1);
                    if(strpos($resultat->url->url, 'oppslag=skadeliste') === false) {
                        $this->app->returi->set($resultat->url->url, $resultat->url->title);
                        $resultat->url = "/flyko/index.php?oppslag=skadeliste";
                    }
                    settype($resultat->url, 'string');

                    break;

                case 'oppdatering':
                case 'saksopplysning':
                    $resultat->msg = \Kyegil\Leiebasen\Leiebase::ucfirst($skjema) . 'en er registrert';
                    $skadeId = isset($get['id']) ? $get['id'] : null;
                    $innhold = isset($post['tekst']) ? $post['tekst'] : null;
                    $vedlegg = isset($files['vedlegg']) && !empty($files['vedlegg']['name']) ? $files['vedlegg'] : null;
                    $privat = isset($post['privat']) ? (bool)$post['privat'] : false;
                    $saksopplysning = $skjema == 'saksopplysning';

                    /** @var Person $bruker */
                    $bruker = $this->app->hoveddata['bruker'];
                    /** @var Skade $skade */
                    $skade = $this->app->hentModell(Skade::class, $skadeId);
                    if (!$skade->hentId()) {
                        throw new Exception('Denne skademeldingen finnes ikke');
                    }
                    if ($skade->utført) {
                        throw new Exception('Denne skaden er allerede registrert som utbedret');
                    }
                    if (!$innhold) {
                        throw new Exception('Oppdateringen mangler');
                    }
                    if ($vedlegg) {
                        $filnavn = basename($vedlegg["name"]);
                        $finfo = finfo_open(FILEINFO_MIME_TYPE);
                        $mime = finfo_file($finfo, $vedlegg['tmp_name']);
                        finfo_close($finfo);

                        switch ($mime) {
                            case 'application/msword':
                            case 'application/pdf':
                            case 'application/vnd.oasis.opendocument.spreadsheet':
                            case 'application/vnd.oasis.opendocument.text':
                            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                            case 'application/x-tar':
                            case 'application/zip':
                            case 'image/jpeg':
                            case 'image/png':
                            case 'text/plain':
                                break;
                            default:
                                throw new Exception("Filer av typen {$mime} tillates desverre ikke. Vedlegget ble ikke lagret.");
                        }

                        if ($vedlegg["size"] > 1000000) {
                            throw new Exception("Fila er for stor. Maks. filstørrelse er 1Mb.");
                        }
                    }

                    $beboerstøtte = $this->hentBeboerstøtte($skade);
                    $innlegg = $beboerstøtte->leggTilInnlegg((object)[
                        'saksopplysning' => $saksopplysning,
                        'privat' => $privat,
                        'innhold' => $innhold,
                        'avsender' => $bruker
                    ]);

                    if (!$innlegg->erAvsenderAdmin()) {
                        $beboerstøtte->abonner($bruker);
                    }

                    if ($vedlegg) {
                        $sti = "{$this->app->hentFilarkivBane()}/beboerstøtte/drift/";
                        if (!is_dir($sti)){
                            mkdir($sti, 0777, true);
                        }
                        if (move_uploaded_file($vedlegg["tmp_name"], "{$sti}{$innlegg->hentId()}-{$filnavn}")) {
                            $innlegg->sett('vedlegg', $filnavn);
                        }
                        else {
                            $resultat->msg = "Fila kunne ikke lastes opp pga ukjent feil.";
                        }
                    }

                    $innlegg->sendEpost();

                    $resultat->skade = $skade->hentId();
                    $resultat->url = strval($this->app->returi->get(1));

                    break;

                case 'utbedring':
                    $resultat->msg = 'Utbedringen er registrert';
                    $skadeId = isset($get['id']) ? $get['id'] : null;
                    $utført = isset($post['utført']) ? new DateTime($post['utført']) : null;
                    $sluttrapport = isset($post['sluttrapport']) ? $post['sluttrapport'] : null;

                    /** @var Person $bruker */
                    $bruker = $this->app->hoveddata['bruker'];
                    /** @var Skade $skade */
                    $skade = $this->app->hentModell(Skade::class, $skadeId);
                    if ($skade->utført) {
                        throw new Exception('Denne skaden er allerede registrert som utbedret');
                    }
                    $skade
                        ->settUtført($utført)
                        ->settSluttrapport($sluttrapport)
                        ->settSluttRegistrerer($bruker)
                    ;

                    if ($skade->beboerstøtte) {
                        $skade->beboerstøtte->leggTilInnlegg((object)[
                            'saksopplysning' => false,
                            'privat' => false,
                            'innhold' => $sluttrapport,
                            'avsender' => $bruker
                        ])->sendEpost();
                        $skade->beboerstøtte->settStatus(Sakstråd::STATUS_AVSLUTTET);
                    }

                    $resultat->skade = $skade->hentId();
                    $resultat->url = strval($this->app->returi->get(1));

                    break;

                case 'status_abonnement':
                    $resultat->msg = 'Ditt valg er registrert';
                    $status = isset($post['status']) ? (bool)$post['status'] : null;
                    if (!isset($status)) {
                        throw new Exception('Oppfattet ikke ønsket abonnement-status');
                    }
                    $skadeId = isset($get['id']) ? $get['id'] : null;
                    /** @var Person $bruker */
                    $bruker = $this->app->hoveddata['bruker'];
                    /** @var Skade $skade */
                    $skade = $this->app->hentModell(Skade::class, $skadeId);

                    if ($status) {
                        if ($skade->utført) {
                            throw new Exception('Denne skaden er allerede utbedret.');
                        }
                        if (!$skade->beboerstøtte) {
                            $privatObjekt = $skade->registrerer;
                            if ($privatObjekt->harAdgangTil('drift')) {
                                $privatObjekt = null;
                            }
                            $skade->opprettBeboerStøtte($privatObjekt);
                        }
                        $skade->beboerstøtte->abonner($bruker);
                    }
                    else {
                        if ($skade->beboerstøtte && $skade->beboerstøtte->abonnerer($bruker)) {
                            $skade->beboerstøtte->stoppAbonnement($bruker);
                        }
                    }

                    break;
                default:
                    $resultat->success = true;
                    break;
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param Skade $skade
     * @return Sakstråd
     * @throws Exception
     */
    protected function hentBeboerstøtte(Skade $skade)
    {
        if (!$skade->hentBeboerstøtte()) {
            $privatPerson = $skade->hentRegistrerer();
            if ($privatPerson->harAdgangTil('drift')) {
                $privatPerson = null;
            }
            $skade->opprettBeboerStøtte($privatPerson);
        }
        return $skade->hentBeboerstøtte();
    }
}