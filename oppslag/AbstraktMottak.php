<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag;

use Kyegil\Leiebasen\Respons;

abstract class AbstraktMottak extends AbstraktOppdrag
{
    /**
     * @param string $skjema
     * @return $this
     */
    abstract public function taIMot(string $skjema);
}