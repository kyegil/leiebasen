<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag;

use Kyegil\Leiebasen\Samling;

abstract class AbstraktUtlevering extends AbstraktOppdrag
{
    /**
     * @param Samling $samling
     * @param array $sorteringer
     * @param array $mapping
     * @return void
     */
    public static function sorter(Samling $samling, array $sorteringer, array $mapping)
    {
        foreach (array_reverse($sorteringer) as $sortering) {
            $felt = $sortering['columnName'] ?? $sortering['name'] ?? null;
            $synkende = strtolower($sortering['dir'] ?? '') === 'desc';
            if(!empty($mapping[$felt][1])) {
                $samling->leggTilSortering($mapping[$felt][1], $synkende, $mapping[$felt][0] ?? null);
            }
        }
    }

    /**
     * @param string $data
     * @return $this
     */
    abstract public function hentData(string $data);
}