<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag;

use Kyegil\Leiebasen\Respons;

abstract class AbstraktOppdrag
{
    /** @var AbstraktKontroller Kontrolleren */
    protected $app;

    /** @var mixed|null */
    protected $data;

    /** @var Respons|string */
    protected $respons;

    /**
     * AbstraktMottak constructor.
     * @param AbstraktKontroller $app
     * @param mixed $data
     */
    public function __construct(
        AbstraktKontroller $app,
        $data = null
    )
    {
        $this->app = $app;
        $this->data = $data;
    }

    /**
     * @return Respons|string
     */
    public function harRespons() {
        return isset($this->respons);
    }

    /**
     * @param Respons|string $respons
     * @return $this
     */
    public function settRespons($respons) {
        $this->respons = is_string($respons) ? new Respons($respons) : $respons;
        return $this;
    }

    /**
     * @return Respons|string
     */
    public function hentRespons() {
        return $this->respons;
    }

    /**
     * @param string $oppdrag
     * @return $this
     */
    public function prosesser(string $oppdrag)
    {
        list('oppdrag' => $oppdrag) = $this->app->before($this, __FUNCTION__, ['oppdrag' => $oppdrag]);
        switch ($oppdrag) {
            case 'manipuler':
                $data = $this->data['get']['data'] ?? '';
                list('data' => $data) = $this->app->pre($this, 'manipuler', ['data' => $data]);
                return $this->app->post($this, 'manipuler', $this->manipuler($data));
            case 'oppgave':
                $oppgave = $this->metode($this->data['get']['oppgave'] ?? '');
                $this->app->pre($this, $oppgave, []);
                return $this->app->post($this, $oppgave, $this->$oppgave());
            case 'utskrift':
                $this->app->pre($this, 'utskrift', []);
                return $this->app->post($this, 'utskrift', $this->utskrift());
            default:
                return $this->app->post($this, __FUNCTION__, $this);
        }
    }

    /**
     * @return $this
     */
    protected function manipuler(string $data)
    {
        return $this;
    }

    /**
     * Gir kamelnotasjon av en streng
     *
     * @param string $understreket
     * @return string
     */
    public static function metode(string $understreket)
    {
        return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $understreket))));
    }

    /**
     * @param mixed|null $data
     * @return $this
     */
    public function settParametere($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function hentParametere()
    {
        return $this->data;
    }
}