<?php

namespace Kyegil\Leiebasen\Oppslag;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\flyko\html\shared\body\Main;
use Kyegil\Leiebasen\Visning\flyko\html\shared\body\main\ReturiBreadcrumbs;
use Kyegil\Leiebasen\Visning\flyko\html\shared\body\main\ReturiElement;
use Kyegil\Leiebasen\Visning\flyko\html\shared\Head;
use Kyegil\Leiebasen\Visning\flyko\html\shared\Html;
use Kyegil\ViewRenderer\ViewArray;

class FlykoKontroller extends AbstraktKontroller
{
    /** @var string */
    public $tittel = "Flytte-koordineringsgruppa";
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';
    /** @inheritdoc */
    public array $område = ['område' => 'flyko'];

    /**
     * FlykoKontroller constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = [])
    {
        parent::__construct($di, $config);
        $this->returi->setBaseUrl($this->http_host . '/flyko/index.php', 'Flyko');
        $this->forberedHovedData();
    }

    /**
     * @return Respons
     */
    public function hentRespons()
    {
        if(!isset($this->respons)) {
            if( isset( $_GET['returi'] ) && $_GET['returi'] == "default") {
                $this->returi->reset();
            }
            $this->returi->set(null, $this->tittel);

            /** @var Person $bruker */
            $bruker = $this->hoveddata['bruker'];
            $this->respons = new Respons($this->vis( Html::class, [
                'head' => $this->vis(Head::class, [
                    'title' => $this->tittel
                ]),
                'brukermeny' => $this->vis(Visning\flyko\html\shared\body\Brukermeny::class),
                'hovedmeny' => $this->vis(Visning\flyko\html\shared\body\Hovedmeny::class, [
                    'bruker' => $bruker
                ]),
                'header' => $this->vis(Visning\flyko\html\shared\body\Header::class),
                'footer' => $this->vis(Visning\flyko\html\shared\body\Footer::class),
                'main' => $this->vis(Main::class, [
                    'tittel' => $this->tittel,
                    'innhold' => $this->innhold(),
                    'breadcrumbs' => $this->visBreadcrumbs(),
                    'varsler' => $this->visVarsler(),
                    'tilbakeknapp' => $this->visTilbakeknapp()
                ]),
                'scripts' => $this->skript()
            ]));
        }
        return parent::hentRespons();
    }

    /**
     * @return bool
     */
    public function skrivHTML()
    {
        if( !$this->preHTML() ) {
            $this->responder404();
            return false;
        }
        $html = $this->hentRespons();
        echo $html;
        return true;
    }

    /**
     * @return string
     */
    public function innhold()
    {
        return '';
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     */
    public function visBreadcrumbs()
    {
        $elementer = new ViewArray();
        $elementer->setGlue(' > ');
        $sti = $this->returi->getTrace();
        /** @var bool $rot Det første elementet er rot */
        $rot = true;
        foreach($sti as $url) {
            $current = $this->returi->getCurrentLocation() == $url->url;
            $elementer->addItem(
                $this->vis(ReturiElement::class, [
                    'url' => $url->url,
                    'tittel' => $url->title,
                    'rot' => $rot,
                    'current' => $current
                ])
            );
            $rot = false;
        }
        return $this->vis(ReturiBreadcrumbs::class, [
            'crumbs' => $elementer
        ]);
    }

    /**
     * @return string
     */
    public function visTilbakeknapp()
    {
        $sti = $this->returi->getTrace();
        if(count($sti) > 1) {
            return new HtmlElement('a',
                [
                    'href' => $this->returi->get()->url,
                    'title' => "Tilbake til {$this->returi->get()->title}",
                    'class' => "button tilbakeknapp"
                ],
                'Tilbake'
            );
        }
        return '';
    }

    /**
     * @return array
     */
    protected function visVarsler()
    {
        $varsler = [];
        foreach($this->hentAdvarsler(\Kyegil\Leiebasen\Leiebase::VARSELNIVÅ_ADVARSEL) as $advarsel) {
            $varsler[] = new HtmlElement('div', ['class' => 'advarsel'], $advarsel['oppsummering']);
        }
        foreach($this->hentAdvarsler(\Kyegil\Leiebasen\Leiebase::VARSELNIVÅ_ORIENTERING) as $påminnelse) {
            $varsler[] = new HtmlElement('div', ['class' => 'påminnelse'], $påminnelse['oppsummering']);
        }
        return $varsler;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        $this->hoveddata['bruker'] = $this->hentModell(Person::class, $this->bruker['id']);
        return parent::preHTML();
    }

    /**
     *
     */
    public function responder404()
    {
        $this->respons = new Respons($this->vis( Html::class, [
            'head' => $this->vis(Head::class, [
                'title' => 'Ugyldig oppslag',
            ]),
            'main' => [new HtmlElement('h1', [], 'Http Feilkode 404'), new HtmlElement('p', [], 'Adressen du har oppgitt er ugyldig. Oppslaget finnes ikke.')]
        ]));
        $protocol = $_SERVER["SERVER_PROTOCOL"] ?? 'HTTP/1.1';
        $this->respons->header($protocol . ' 404 Not Found');
        die($this->hentRespons());
    }
}