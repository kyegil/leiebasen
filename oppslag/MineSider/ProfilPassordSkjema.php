<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Head;
use Kyegil\MysqliConnection\MysqliConnection;
use Kyegil\MysqliConnection\MysqliConnectionInterface;

class ProfilPassordSkjema extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * @return bool
     * @throws \Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = 'Endre passord';
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws \Exception
     */
    public function innhold()
    {
        /** @var Person $skade */
        $bruker = $this->hoveddata['bruker'];

        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\profil_passord_skjema\body\main\Index::class, [
            'bruker' => $bruker
        ]);
    }

    /**
     * @return Visning|string
     */
    public function skript()
    {
        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\js\profil_passord_skjema\body\main\Index::class);
    }
}