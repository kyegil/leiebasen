<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 09/11/2020
 * Time: 08:10
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider\FsAnlegg;


use DateInterval;
use DateTime;
use Exception;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;

class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData(string $data)
    {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            if(!isset($_GET['id'])) {
                throw new Exception("id er ikke oppgitt");
            }
            /** @var Strømanlegg $strømanlegg */
            $strømanlegg = $this->app->hentModell(Strømanlegg::class, $_GET['id']);
            if(!$strømanlegg->hentId()) {
                throw new Exception("Strømanlegget finnes ikke");
            }
            switch ($data) {
                case "forbruksstatistikk":
                    $fra = isset($_GET['fra']) ? date_create($_GET['fra']) : null;
                    $til = isset($_GET['til']) ? date_create($_GET['til']) : null;

                    $resultat->data = $this->hentForbruksStatistikk($strømanlegg, $fra, $til);

                    break;
                default:
                    throw new Exception("feil eller manglende 'data'-parameter mangler i data-forespørsel");
            }

        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }


    /**
     * @param Strømanlegg $strømanlegg
     * @param DateTime|null $fra
     * @param DateTime|null $til
     * @return stdClass[]
     * @throws Exception
     */
    protected function hentForbruksStatistikk(Strømanlegg $strømanlegg, ?DateTime $fra, ?DateTime $til)
    {
        $resultat = [];
        $datoer = $this->hentFakturaEndringsdatoer($strømanlegg, $fra, $til);

        foreach ($datoer as $dato) {
            $datoData = new stdClass();
            $datoData->dato = $dato;

            /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad[] $regninger */
            $regninger = $strømanlegg->hentRegningerForTidsrom(date_create($dato));
            foreach ($regninger as $regning) {
                $datoData->{strval($regning->hentFakturanummer())} = $regning->hentDagligForbruk();
            }
            $resultat[] = $datoData;
        }
        return $resultat;
    }

    /**
     * @param Strømanlegg $anlegg
     * @param DateTime|null $fra
     * @param DateTime|null $til
     * @return string[]
     * @throws Exception
     */
    protected function hentFakturaEndringsdatoer(Strømanlegg $anlegg, ?DateTime $fra, ?DateTime $til): array
    {
        $datoer = [
            $fra->format('Y-m-d'),
            $til->format('Y-m-d')
        ];
        /** @var \Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad $regning */
        foreach($anlegg->hentRegninger() as $regning) {
            if($regning->fradato) {
                $fradato = clone $regning->fradato;
                if(
                    ($fradato >= $fra || !$fra)
                    && ($fradato <= $til || !$til)
                ) {
                    $datoer[] = $fradato->format('Y-m-d');
                    $fradato->sub(new DateInterval('P1D'));
                    $datoer[] = $fradato->format('Y-m-d');
                }
            }
            if($regning->tildato) {
                $tildato = clone $regning->tildato;
                if(
                    ($tildato >= $fra || !$fra)
                    && ($tildato <= $til || !$til)
                ) {
                    $datoer[] = $tildato->format('Y-m-d');
                    $tildato->add(new DateInterval('P1D'));
                    $datoer[] = $tildato->format('Y-m-d');
                }
            }
        }
        $datoer = array_unique($datoer);
        sort($datoer);

        // $datoer inneholder nå alle fakturaendringer som strenger i formate 'Y-m-d'
        return $datoer;
    }
}