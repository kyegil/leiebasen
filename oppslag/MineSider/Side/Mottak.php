<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 14/02/2023
 * Time: 15:24
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider\Side;


use Exception;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\CmsSide;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Visning\felles\epost\shared\Epost;
use Kyegil\ViewRenderer\ViewArray;
use stdClass;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Lagret."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];
        $cmsSideKode = $get['id'] ?? '';
        $skjema = $get['skjema'] ?? '';
        try {
            $cmsSide = CmsSide::hentCmsSide($this->app, 'mine-sider', $cmsSideKode);
            if(!$cmsSide) {
                throw new Exception('Ukjent side');
            }

            /** @var object $konfigurering */
            $konfigurering = $cmsSide->hentKonfigurering() ?? new stdClass();
            /** @var object $skjemaProsessor */
            $skjemaProsessor = $konfigurering->skjemaProsessor  ?? new stdClass();
            switch ($skjemaProsessor->type ?? '') {
                case 'epost':
                    /** @var string $epostAdresse */
                    $epostAdresse = $skjemaProsessor->epost ?? '';
                    $emne = $skjemaProsessor->emne ?? '';
                    if (!filter_var($epostAdresse, FILTER_VALIDATE_EMAIL)) {
                        throw new Exception('Ugyldig e-post i CMS skjema-prosessor ' . $cmsSide->hentId());
                    }

                    $innhold = $this->formDataTilHtml($post);

                    /** @var Epost $html */
                    $html = $this->app->vis(Epost::class, [
                        'tittel' => $emne,
                        'innhold' => $innhold,
                        'bunntekst' => '',
                    ]);

                    $this->app->sendMail((object)[
                        'type' => 'cms_side_mottak',
                        'to' => $epostAdresse,
                        'subject' => $emne,
                        'html' => $html ?? ''
                    ]);
                    $id = $get['id'] ?? null;
                    $resultat->msg = 'Skjemaet er mottatt';
                    $resultat->url = (string)$this->app->returi->get(1);
                    break;
                default:
                    throw new Exception('Ukjent skjema-prosessor');
            }
        }
        catch (Feilmelding $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        catch (\Throwable $e) {
            $this->app->loggException($e);
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param $post
     * @return HtmlElement
     */
    private function formDataTilHtml($post)
    {
        $html = new ViewArray();
        foreach ($post as $key => $value) {
            $html->addItem(new HtmlElement('dt', [], (is_numeric($key) ? '&nbsp;' : ucfirst(str_replace('_', ' ', $key)))));
            $value = is_array($value) ? $this->formDataTilHtml($value) : $value;
            $html->addItem(new HtmlElement('dd', [], $value));
        }
        return new HtmlElement('dl', [], $html);
    }
}