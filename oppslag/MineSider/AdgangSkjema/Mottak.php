<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 07/09/2020
 * Time: 17:10
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider\AdgangSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Leieforhold;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this|Mottak
     */
    public function taIMot(string $skjema)
    {
        /** @var \stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Dine preferanser er lagret."
        ];
        $skjema = $this->data['get']['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'adgangskjema':
                    /** @var Person|null $bruker */
                    $bruker = $this->app->hoveddata['bruker'] ?? null;
                    if (!$bruker) {
                        throw new Exception('Du er ikke innlogget');
                    }

                    $adganger = $this->data['post']['adgang'] ?? [];
                    settype($adganger['mine-sider'], 'array');
                    $andresAdganger = $this->data['post']['andres_adganger'] ?? [];

                    /** @var Adgang $adgang */
                    foreach ($bruker->hentAdganger() as $adgang) {
                        $adgangsOmråde = $adgang->adgang;
                        if(!isset($adganger[$adgangsOmråde]) || !$adganger[$adgangsOmråde]) {
                            $adgang->slett();
                        }
                        else if ($adgangsOmråde == 'mine-sider' && !in_array($adgang->hentLeieforhold()->hentId(), $adganger['mine-sider'])) {
                            $adgang->slett();
                        }
                    }

                    /** @var Leieforhold $leieforhold */
                    foreach ($bruker->hentLeieforhold() as $leieforhold) {
                        settype($andresAdganger[$leieforhold->hentId()], 'array');
                        settype($adganger['mine-sider'], 'array');

                        if(in_array($leieforhold->hentId(), $adganger['mine-sider'])
                            && !$bruker->harAdgangTil('mine-sider', $leieforhold)) {
                            $bruker->tildelAdgang(Adgang::ADGANG_MINESIDER, $leieforhold);
                        }

                        /** @var Adgang $adgang */
                        foreach ($leieforhold->hentAdganger() as $adgang) {
                            $personId = $adgang->person->hentId();
                            if ($personId == $bruker->hentId()) {
                                // Ikke slett din egen adgang
                                continue;
                            }
                            else if ($adgang->person->erLeietakerI($leieforhold)) {
                                // Ikke slett adgangen for andre leietakere
                                continue;
                            }
                            else if (!in_array($personId, $andresAdganger[$leieforhold->hentId()])) {
                                $adgang->slett();
                            }
                        }
                    }
                    $resultat->url = '/mine-sider/index.php?oppslag=adgang_skjema';

                    break;
                default:
                    $resultat->success = true;
                    break;
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}