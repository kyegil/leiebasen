<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider\BetalingsplanSkjema;


use DateTimeImmutable;
use Exception;
use Kyegil\CoreModel\CoreModel;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Visning\felles\epost\betalingsplan\forslag\Bekreftelse;
use Kyegil\Leiebasen\Visning\felles\epost\shared\Epost;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;
use stdClass;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Lagret."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];

        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'betalingsplan':
                    $dato = date_create_immutable();
                    /** @var int|null $leieforholdId */
                    $leieforholdId = $post['leieforhold'] ?? null;
                    /** @var string $notatTekst */
                    $notatTekst = $post['notat'] ?? '';
                    /** @var integer[] $kravsett */
                    $kravsett = $post['originalkrav'] ?? [];
                    /** @var string $terminbeløp */
                    $terminbeløp = isset($post['terminbeløp']) && is_numeric($post['terminbeløp']) ? $post['terminbeløp'] : null;
                    /** @var bool $følgerLeiebetalinger */
                    $følgerLeiebetalinger = isset($post['følger_leiebetalinger']) && $post['følger_leiebetalinger'];
                    /** @var string $terminlengde */
                    $terminlengde = $post['terminlengde'] ?? null;
                    /** @var DateTimeImmutable|null $startdato */
                    $startdato = isset($post['startdato']) && $post['startdato'] ? new DateTimeImmutable($post['startdato']) : null;
                    $avdrag = $post['avdrag'] ?? [];
                    $avtaletekst = str_replace('<div class="avdragsoversikt"></div>', '{avdragsoversikt}', $post['avtaletekst'] ?? '');
                    $this->validerAvtaletekst($avtaletekst);
                    if ($avdrag) {
                        $avdrag = array_filter(array_combine($avdrag['dato'], $avdrag['beløp']));
                    }
                    ksort($avdrag);
                    $utenomAvtalegiro = isset($post['avtalegiro']) && !$post['avtalegiro'];
                    $forfallspåminnelseAntDager = $post['forfallspåminnelse_ant_dager'] ?? null;
                    $henvendelseFra = Notat::FRA_LEIETAKER;

                    $resultat->msg = 'Betalingsplan-forslaget er levert';
                    /** @var Leieforhold $leieforhold */
                    $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);
                    /** @var Leieforhold\Framleie\Framleietakersett $framleie */
                    $framleie = $this->app->hentSamling(Leieforhold\Framleie\Framleietaker::class);
                    $framleie->leggTilLeftJoinForFramleieforhold()
                        ->leggTilFilter(['`' . Leieforhold\Framleie::hentTabell() . '`.`leieforhold`' => $leieforhold->hentId()])
                        ->leggTilFilter(['`' . Leieforhold\Framleie::hentTabell() . '`.`fradato` <= CURDATE()'])
                        ->leggTilFilter(['`' . Leieforhold\Framleie::hentTabell() . '`.`tildato` >= CURDATE()'])
                        ->leggTilFilter(['personid' => $this->app->bruker['id']])
                    ;
                    if ($framleie->hentAntall()) {
                        $henvendelseFra = Notat::FRA_FRAMLEIER;
                    }

                    if (!$leieforhold->hentId()) {
                        throw new Exception('Ugyldig leieforhold');
                    }

                    $betalingsplanForslag = (object)[
                        'dato' => $dato->format('Y-m-d'),
                        'originalkrav' => $kravsett,
                        'avdrag' => (object)$avdrag,
                        'avdragsbeløp' => $terminbeløp,
                        'følger_leiebetalinger' => $følgerLeiebetalinger,
                        'terminlengde' => $følgerLeiebetalinger
                            ? CoreModel::prepareIso8601IntervalSpecification($leieforhold->hentTerminlengde())
                            : $terminlengde,
                        'utenom_avtalegiro' => $utenomAvtalegiro,
                        'forfallspåminnelse_ant_dager' => $forfallspåminnelseAntDager,
                        'start_dato' => $startdato ? $startdato->format('Y-m-d') : null,
                        'betalingsavtale' => $avtaletekst,
                    ];

                    /** @var Notat $notatModell */
                    $notatModell = $this->app->nyModell(Notat::class, (object)[
                        'leieforhold' => $leieforhold,
                        'kategori' => Notat::KATEGORI_BETALINGSPLAN,
                        'skjul_for_leietaker' => false,
                        'dato' => date_create_immutable(),
                        'notat' => $notatTekst,
                        'henvendelse_fra' => $henvendelseFra,
                        'brevtekst' => $this->visUtfyltAvtaletekst($betalingsplanForslag),
                    ]);

                    $betalingsplanForslag->notat_id = $notatModell->hentId();

                    $leieforhold->settBetalingsplanForslag($betalingsplanForslag);

                    $epostMottakere = $leieforhold->hentEpost();
                    $epostMottakere[] = "{$this->app->bruker['navn']} <{$this->app->bruker['epost']}>";
                    $emne = 'Bekreftelse på mottatt betalingsplan';
                    $avdragHtml = $this->hentHtmlAvdragsoversikt($betalingsplanForslag->avdrag);
                    $epostHtml = $this->app->vis(Bekreftelse::class, [
                       'avdrag' => $avdragHtml,
                        'leieforholdId' => $leieforhold->hentId(),
                        'leieforholdBeskrivelse' => $leieforhold->hentBeskrivelse()
                    ]);
                    foreach(array_unique($epostMottakere) as $epostMottaker) {
                        $this->app->sendMail((object)[
                            'to' => $epostMottaker,
                            'type' => 'betalingsplan_forslag',
                            'subject' => $emne,
                            'html' => $this->app->vis(Epost::class, [
                                'tittel' => $emne,
                                'innhold' => $epostHtml
                            ])
                        ]);
                    }

                    $emne = 'Mottatt betalingsplan';
                    $epostHtml = [$leieforhold->hentNavn() . ' har foreslått betalingsplan for utestående beløp i leieforhold ' . $leieforhold->hentId() . '<br>'];
                    $epostHtml[] = 'Planen må godkjennes ifra oppfølging<br>';
                    $epostHtml[] = new HtmlElement('a', [
                        'title' => 'Klikk her for å se forslaget',
                        'class' => 'button',
                        'href' => $this->app->http_host . '/oppfølging/index.php?oppslag=notat&returi=default&id=' . $betalingsplanForslag->notat_id
                    ], 'Klikk her for å se forslaget');

                    $this->app->sendMail((object)[
                        'to' => "{$this->app->hentValg('utleier')} <{$this->app->hentValg('epost')}>",
                        'type' => 'betalingsplan_forslag',
                        'subject' => $emne,
                        'html' => $this->app->vis(Epost::class, [
                            'tittel' => $emne,
                            'innhold' => new HtmlElement('div', [], $epostHtml),
                            'bunntekst' => ''
                        ])
                    ]);

                    $resultat->notat = $notatModell->hentId();
                        $resultat->url = "/mine-sider/index.php?oppslag=notat&id=" . $notatModell->hentId();

                    break;
                default:
                    $resultat->success = true;
                    break;
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param integer[] $kravIder
     * @return HtmlElement
     * @throws Exception
     */
    private function hentHtmlKravoversikt(array $kravIder) {
        $linjer = new ViewArray();
        $kravsett = $this->app->hentSamling(Leieforhold\Krav::class)->filtrerEtterIdNumre($kravIder);
        foreach ($kravsett as $krav) {
            $linjer->addItem(new HtmlElement('tr', [], [
                new HtmlElement('td', [], $krav->tekst),
                new HtmlElement('td', ['class' => 'beløp'], $this->app->kr($krav->utestående)),
            ]));
        }
        return new HtmlElement('table', ['id' => 'betalingsavtale-kravoversikt'], new HtmlElement('tbody', [], $linjer));
    }

    /**
     * @param stdClass $avdrag
     * @return HtmlElement
     */
    public function hentHtmlAvdragsoversikt(stdClass $avdrag): HtmlElement
    {
        $linjer = new ViewArray();
        foreach ($avdrag as $dato => $beløp) {
            $linjer->addItem(new HtmlElement('tr', [], [
                new HtmlElement('td', [], date_create($dato)->format('d.m.Y')),
                new HtmlElement('td', ['class' => 'beløp'], $this->app->kr($beløp)),
            ]));
        }
        $thead = new HtmlElement('thead', [], new HtmlElement('tr', [], [
            new HtmlElement('th', [], 'Dato'),
            new HtmlElement('th', ['class' => 'beløp'], 'Avdrag')
        ]));
        $tbody = new HtmlElement('tbody', [], $linjer);
        return new HtmlElement('table', ['id' => 'betalingsavtale-avdragsoversikt'], [$thead, $tbody]);
    }

    /**
     * @param string $avtaletekst
     * @return Mottak
     * @throws Exception
     */
    private function validerAvtaletekst(string $avtaletekst): Mottak
    {
        if(strpos($avtaletekst, '{avdragsoversikt}') === false) {
            throw new Exception("Avtaleteksten må inneholde avdragsoversikt, som kan settes inn ifra verktøylinjen.");
        }
        return $this;
    }

    /**
     * @param string $avtaletekst
     * @return array|string|string[]
     * @throws Exception
     */
    private function visUtfyltAvtaletekst(object $betalingsplanForslag)
    {
        /** @var string $avtaletekst */
        $avtaletekst = $betalingsplanForslag->betalingsavtale ?? '';
        /** @var integer[] $kravIder */
        $kravIder = $betalingsplanForslag->originalkrav ?? [];
        /** @var stdClass $avdrag */
        $avdrag = $betalingsplanForslag->avdrag ?? new stdClass();
        /** @var ViewInterface|string $kravoversikt */
        $kravoversikt = $this->hentHtmlKravoversikt($kravIder);
        /** @var string $startdato */
        $startdato = $betalingsplanForslag->start_dato ? date_create_immutable($betalingsplanForslag->start_dato)->format('d.m.Y') : '';
        /** @var string $sluttdato */
        $sluttdato = date_create_immutable(max(array_keys((array)$avdrag)))->format('d.m.Y');
        $avdragsoversikt = $this->hentHtmlAvdragsoversikt($avdrag);

        $avtaletekst = str_replace('{kravoversikt}', $kravoversikt, $avtaletekst);
        $avtaletekst = str_replace('{avdragsoversikt}', $avdragsoversikt, $avtaletekst);
        $avtaletekst = str_replace('{startdato}', $startdato, $avtaletekst);
        $avtaletekst = str_replace('{sluttdato}', $sluttdato, $avtaletekst);
        return $avtaletekst;
    }


}