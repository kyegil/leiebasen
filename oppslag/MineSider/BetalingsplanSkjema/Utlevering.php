<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider\BetalingsplanSkjema;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData(string $data)
    {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case "utestående":
                    $leieforholdId = $this->data['get']['leieforhold'] ?? null;
                    $kravIder = $this->data['get']['krav'] ?? [];
                    $kravsett = $this->app->hentSamling(Krav::class)
                        ->filtrerEtterIdNumre($kravIder)
                        ->leggTilFilter(['leieforhold' => $leieforholdId]);
                    $resultat->data = 0;
                    foreach ($kravsett as $krav) {
                        $resultat->data += $krav->utestående;
                    }
                    $resultat->msg = 'Sum: ' . $this->app->kr($resultat->data, false);
                    break;

                default:
                    throw new Exception("feil eller manglende 'data'-parameter mangler i data-forespørsel");
            }

        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}