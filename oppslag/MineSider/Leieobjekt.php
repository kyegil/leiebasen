<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Html;

class Leieobjekt extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * Leieobjekt constructor.
     *
     * @param array $di
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $leieobjektId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['leieforhold'] = null;
        $this->hoveddata['leieobjekt'] = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieobjekt::class, $leieobjektId);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var \Kyegil\Leiebasen\Modell\Leieobjekt $leieobjekt */
        $leieobjekt = $this->hoveddata['leieobjekt'];
        if(!$leieobjekt->hentId()) {
            return false;
        }
        if($leieobjekt->boenhet) {
            $this->tittel = "Leilighet nr. {$leieobjekt->hentId()}";
        }
        else {
            $this->tittel = "Lokale nr. {$leieobjekt->hentId()}";
        }

        /** @var Html $html */
        $html = $this->hentRespons()->hentInnhold();
        $html->getData('head')->setData('title', $leieobjekt->hentBeskrivelse());
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws \Exception
     */
    public function innhold()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieobjekt $leieobjekt */
        $leieobjekt = $this->hoveddata['leieobjekt'];

        $html = $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\leieobjekt\body\main\Index::class, [
            'leieobjekt' => $leieobjekt,
        ]);
        return $html;
    }
}