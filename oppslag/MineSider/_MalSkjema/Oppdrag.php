<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 04/12/2020
 * Time: 23:21
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider\_MalSkjema;


use Exception;
use Kyegil\Leiebasen\Exceptions\Feilmelding;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Oppdrag extends AbstraktOppdrag
{
    /**
     * @return Oppdrag
     */
    protected function slettModell():Oppdrag {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            $modellId = (int)$this->data['post']['id'];
            /** @var Modell $modell */
            $modell = $this->app->hentModell(Modell::class, $modellId);
            if (!$modell->hentId()) {
                throw new Exception('Finner ikke denne modellen');
            }
            $resultat->msg = 'Modellen har blitt slettet';
//            $modell->slett();
            $resultat->url = (string)$this->app->returi->get(1);
        }
        catch (Feilmelding $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        catch (\Throwable $e) {
            $this->app->loggException($e);
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}