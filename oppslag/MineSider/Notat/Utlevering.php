<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider\Notat;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Visning\felles\html\shared\Utskrift;

class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData(string $data)
    {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case "brev":
                    try {
                        $notatId = $this->data['get']['id'] ?? null;
                        /** @var Notat $notat */
                        $notat = $this->app->hentModell(Notat::class, $notatId);
                        if (!$notat->hentId() || $notat->hentSkjulForLeietaker()) {
                            throw new Exception('Finner ikke brevet');
                        }
                        $tittel = implode(' ', array_filter([
                            $notat->kategori,
                            $notat->henvendelseFra,
                            $notat->dato->format('d.m.Y')
                        ]));

                        $brevtekst = $notat->hentBrevtekst();
                        if(
                            $notat->kategori == $notat::KATEGORI_BETALINGSPLAN
                            && $notat->leieforhold->hentBetalingsplan()
                        ) {
                            $brevtekst = $notat->leieforhold->hentBetalingsplan()->visUtfyltAvtaletekst($brevtekst);
                        }

                        $utskrift = $this->app->vis(Utskrift::class, [
                            'tittel' => $tittel,
                            'innhold' => $brevtekst
                        ]);
                        $respons = new Respons($utskrift);
                        $this->settRespons($respons);
                        return $this;
                    } catch (Exception $e) {
                        $this->app->responder404();
                    }

                case "vedlegg":
                    try {
                        $notatId = $this->data['get']['id'] ?? null;
                        /** @var Notat $notat */
                        $notat = $this->app->hentModell(Notat::class, $notatId);
                        if (!$notat->hentId() || $notat->hentSkjulForLeietaker()) {
                            throw new Exception('Finner ikke notatet');
                        }
                        $fil = $notat->hentVedlegg();
                        if (
                            !file_exists($fil)
                            || !$filInnhold = file_get_contents($fil)
                        ) {
                            throw new Exception('Vedlegget finnes ikke.');
                        }
                        $respons = new Respons($filInnhold);
                        $respons->header('Content-type: ' . mime_content_type($fil));
                        $respons->header('Content-Disposition: attachment; filename="' . $notat->hentVedleggsnavn() . '"');
                        $respons->header('Content-Transfer-Encoding: binary');
                        $respons->header('Content-Length: ' . strlen($filInnhold));
                        $respons->header('Accept-Ranges: bytes');

                        $this->settRespons($respons);
                        return $this;
                    } catch (Exception $e) {
                        $this->app->responder404();
                    }
                default:
                    throw new Exception("feil eller manglende 'data'-parameter mangler i data-forespørsel");
            }

        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            $this->settRespons(new JsonRespons($resultat));
        }
        return $this;
    }
}