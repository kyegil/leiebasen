<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;


use Exception;
use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Innbetaling;
use Kyegil\Leiebasen\Modell\Leieforhold as LeieforholdModell;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Oppslag\MineSiderKontroller;
use Kyegil\Leiebasen\Respons\CsvRespons;

class LeieforholdTransaksjonerCsv extends MineSiderKontroller
{
    /**
     * LeieforholdTransaksjonerCsv constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $leieforholdId = isset($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['leieforhold'] = $this->hentModell(LeieforholdModell::class, $leieforholdId);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        try {
            /** @var LeieforholdModell $leieforhold */
            $leieforhold = $this->hoveddata['leieforhold'];
            if(!$leieforhold->hentId()) {
                return false;
            }
            /** @var \stdClass[] $resultat */
            $resultat = [(object)[
                'dato' => 'dato',
                'tekst' => 'beskrivelse',
                'beløp' => 'beløp'
            ]];
            $transaksjoner = $leieforhold->hentTransaksjoner();
            /** @var Innbetaling|Krav $transaksjon */
            foreach($transaksjoner as $transaksjon) {
                if($transaksjon instanceof Innbetaling) {
                    $beløp = $transaksjon->hentBeløpTilLeieforhold($leieforhold);
                    $resultat[] = (object)[
                        'dato' => $transaksjon->hentDato() ? $transaksjon->hentDato()->format('Y-m-d') : '',
                        'tekst' => $transaksjon->hentBeløp() > 0 ? "Betaling fra {$transaksjon->hentBetaler()}" : 'Tilbakebetaling',
                        'beløp' => -$beløp,
                    ];
                }
                elseif($transaksjon instanceof Krav) {
                    $beløp = $transaksjon->hentBeløp();
                    $resultat[] = (object)[
                        'dato' => $transaksjon->hentKravdato() ? $transaksjon->hentKravdato()->format('Y-m-d') : '',
                        'tekst' => $transaksjon->hentTekst(),
                        'beløp' => $beløp,
                    ];
                }
            }

            $respons = new CsvRespons($resultat);
            $respons->filnavn = 'transaksjoner.csv';
            $this->respons = $respons;
        } catch (Exception $e) {
            $this->settAdvarsel(Leiebase::VARSELNIVÅ_ADVARSEL, ['tekst' => $e->getMessage(), 'oppsummering' => $e->getMessage()]);
        }
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface|string
     * @throws Exception
     */
    public function innhold()
    {
        return '';
    }
}