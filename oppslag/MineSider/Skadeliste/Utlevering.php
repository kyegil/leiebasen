<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 09/11/2020
 * Time: 08:10
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider\Skadeliste;


use Exception;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Skade;

class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData(string $data)
    {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case "skadeliste":
                    $leieobjektId = isset($_GET['id']) ? $_GET['id'] : null;
                    /** @var Leieobjekt|null $leieobjekt */
                    $leieobjekt = $leieobjektId ? $this->app->hentModell(Leieobjekt::class, $leieobjektId) : null;
                    if($leieobjekt && !$leieobjekt->hentId()) {
                        throw new Exception("Ugyldig leieobjekt");
                    }
                    $bygning = $leieobjekt ? $leieobjekt->hentBygning() : null;

                    $skadeSamling = $this->app->hentSamling(Skade::class)
                        ->leggTilSortering('registrert', true);
                    $leieobjektfilter = [];
                    if($leieobjekt) {
                        $leieobjektfilter['or']['leieobjektnr'] = $leieobjekt->hentId();
                    }
                    if($bygning) {
                        $leieobjektfilter['or']['and'] = [
                            ['!leieobjektnr'],
                            'bygning' => $bygning->hentId()
                        ];
                    }
                    if($leieobjekt || $bygning) {
                        $skadeSamling->leggTilFilter($leieobjektfilter);
                    }
                    /** @var Skade $skade */
                    foreach($skadeSamling as $skade) {
                        $leieobjekt = $skade->hentLeieobjekt();
                        $bygning = $skade->bygning;
                        $kategorier = [];
                        $kategoriliste = '<ul class="tags">';
                        /** @var Skade\Kategori $kategori */
                        foreach($skade->hentKategorier() as $kategori) {
                            $kategorier[] = $kategori->hentKategori();
                            $kategoriliste .= '<li>' . strtolower($kategori->hentKategori()) . ' </li>';
                        }
                        $kategoriliste .= '</ul>';
                        $resultat->data[] = (object)[
                            'id' => $skade->hentId(),
                            'skade' => $skade->skade,
                            'registrert' => $skade->registrert->format('Y-m-d H:i:s'),
                            'registrert_formatert' => $skade->registrert->format('d.m.Y'),
                            'registrerer' => $skade->registrerer ? $skade->registrerer->hentNavn() : null,
                            'leieobjekt' => (int)$bygning->hentId() . '-' . $leieobjekt ? $leieobjekt->hentId() : null,
                            'leieobjektbeskrivelse' => $leieobjekt ? $leieobjekt->hentBeskrivelse() : null,
                            'beskrivelse' => $skade->beskrivelse,
                            'utført' => (bool)$skade->utført,
                            'utført_dato_formatert' => $skade->utført ? $skade->utført->format('d.m.Y') : '',
                            'sluttregistrerer' => $skade->sluttregistrerer ? $skade->sluttregistrerer->hentNavn() : null,
                            'sluttrapport' => $skade->sluttrapport,
                            'navn' => $bygning ? $bygning->navn : null,
                            'kategorier' => $kategorier,
                            'kategoriliste' => $kategoriliste
                        ];
                    }
                    break;
                default:
                    throw new Exception("feil eller manglende 'data'-parameter mangler i data-forespørsel");
            }

        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}