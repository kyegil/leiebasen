<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold as LeieforholdModell;
use Kyegil\Leiebasen\Oppslag\MineSiderKontroller;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_avtaletekst\body\main\Index;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Html;

class LeieforholdAvtaletekst extends MineSiderKontroller
{
    /**
     * LeieforholdAvtaletekst constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $leieforholdId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['leieforhold'] = $this->hentModell(LeieforholdModell::class, $leieforholdId);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $leieforholdId = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var LeieforholdModell $leieforhold */
        $leieforhold = $this->hentModell(LeieforholdModell::class, $leieforholdId);
        if(!$leieforhold->hentId()) {
            return false;
        }
        $this->tittel = "Leieavtale leieforhold nr. {$leieforhold->hentId()}";
        /** @var Html $html */
        $html = $this->hentRespons()->hentInnhold();
        $html->getData('main')->setTemplate('mine_sider/html/shared/body/main_noheader');
        return true;
    }

    /**
     * @return Visning
     * @throws Exception
     */
    public function innhold()
    {
        /** @var LeieforholdModell $leieforhold */
        $leieforhold = $this->hentModell(LeieforholdModell::class, $_GET['id']);

        return $this->vis(Index::class, [
            'tekst' => $leieforhold->hentKontrakt()->hentTekst()
        ]);
    }
}