<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning;
use Kyegil\MysqliConnection\MysqliConnection;
use Kyegil\MysqliConnection\MysqliConnectionInterface;

class ProfilSkjema extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        $this->tittel = "Brukerprofil for {$bruker->hentNavn()}";
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];

        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\profil_skjema\body\main\Index::class, [
            'bruker' => $bruker
        ]);
    }

    /**
     * @return Visning|string
     */
    public function skript()
    {
        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\js\profil_skjema\body\main\Index::class);
    }
}