<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Betalingsplan;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Head;

class BetalingsplanKort extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * Forbered hoveddata
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $betalingsplanId = !empty($_GET['id']) ? $_GET['id'] : null;
        $betalingsplan = $this->hentModell(Betalingsplan::class, (int)$betalingsplanId);
        /** @var Leieforhold\Notat|null $notat */
        $notat = $betalingsplan->hentNotat();
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $betalingsplan->hentLeieforhold();

        $this->hoveddata['betalingsplan_id'] = $betalingsplanId;
        $this->hoveddata['betalingsplan'] = $betalingsplan;
        $this->hoveddata['notat'] = $notat;
        $this->hoveddata['leieforhold'] = $leieforhold;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }

        /** @var Betalingsplan|null $betalingsplan */
        $betalingsplan = $this->hoveddata['betalingsplan'];
        /** @var Leieforhold|null $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        if(!$leieforhold) {
            return false;
        }
        if(!$betalingsplan && !$leieforhold->hent('betalingsplan_forslag')) {
            return false;
        }
        if($betalingsplan) {
            $this->tittel = 'Betalingsplan';
        }
        else {
            $this->tittel = 'Forslag til betalingsplan';
        }

        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Betalingsplan|null $betalingsplan */
        $betalingsplan = $this->hoveddata['betalingsplan'];
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];

        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\betalingsplan_kort\Index::class, [
            'leieforhold' => $leieforhold,
            'betalingsplan' => $betalingsplan
        ]);
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     */
    public function skript()
    {
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        $betalingsplan = $leieforhold->hentBetalingsplan();
        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\js\betalingsplan_skjema\Index::class, [
            'leieforholdId' => $leieforhold->hentId(),
            'leieforhold' => $leieforhold,
            'betalingsplan' => $betalingsplan
        ]);
    }
}