<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;


use Kyegil\MysqliConnection\MysqliConnectionInterface;

class Regning extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * Regning constructor.
     *
     * @param array $di
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $regningsId = isset($_GET['id']) ? $_GET['id'] : null;
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold\Regning $regning */
        $regning = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold\Regning::class, $regningsId);
        $leieforhold = $regning->hentLeieforhold();
        $this->hoveddata['regning'] = $regning;
        $this->hoveddata['leieforhold'] = $leieforhold;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $regningsId = isset($_GET['id']) ? $_GET['id'] : null;
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold\Regning $regning */
        $regning = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold\Regning::class, $regningsId);
        $leieforhold = $regning->hentLeieforhold();
        if(!$leieforhold->hentId()) {
            return false;
        }
        $this->tittel = "Regning nr. {$regning->hentId()}";

        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws \Exception
     */
    public function innhold()
    {
        $regningsId = isset($_GET['id']) ? $_GET['id'] : null;
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold\Regning $regning */
        $regning = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold\Regning::class, $regningsId);

        $html = $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\regning\body\main\Index::class, [
            'regning' => $regning,
        ]);
        return $html;
    }
}