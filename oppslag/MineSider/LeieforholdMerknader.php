<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Oppslag\MineSiderKontroller;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Head;

class LeieforholdMerknader extends MineSiderKontroller
{
    /**
     * Forbered hoveddata
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $leieforholdId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['leieforhold'] = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold::class, $leieforholdId);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        if(!$leieforhold->hentId()) {
            return false;
        }
        $this->tittel = 'Oppfølgingsnotater leieforhold ' . $leieforhold->hentId();

        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        $htmlHead
            /**
             * Importer DataTables
             * @link https://datatables.net/
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => DataTable::DATATABLES_SOURCE_JS
            ]))
//            ->leggTil('scripts', new HtmlElement('script', [
//                'src' => '//cdn.datatables.net/plug-ins/1.11.3/sorting/datetime-moment.js'
//            ]))
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => DataTable::DATATABLES_SOURCE_CSS
            ]))
            /**
             * Importer DataTables Responsive Extension
             * @link https://datatables.net/
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_RESPONSIVE_JS
            ]))
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_RESPONSIVE_CSS
            ]))
        ;

        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     */
    public function innhold()
    {
        $leieforhold = $this->hoveddata['leieforhold'] ?? null;
        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_merknader\body\main\Index::class, [
            'leieforhold' => $leieforhold
        ]);
    }

    /**
     * @return \Kyegil\Leiebasen\Visning|\Kyegil\ViewRenderer\ViewInterface|string
     */
    public function skript()
    {
        $leieforhold = $this->hoveddata['leieforhold'] ?? null;
        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\js\leieforhold_merknader\body\main\Index::class, [
            'leieforhold' => $leieforhold
        ]);
    }
}