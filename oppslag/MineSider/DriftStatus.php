<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Html;
use Kyegil\MysqliConnection\MysqliConnectionInterface;

class DriftStatus extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /** @var string */
    public $tittel = 'Drift-status';

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws \Exception
     */
    public function innhold()
    {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];

        $html = $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\drift_status\body\main\Index::class, [
        ]);
        return $html;
    }
}