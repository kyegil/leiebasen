<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\ViewRenderer\ViewInterface;

class LeieforholdPreferanser extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * LeieforholdPreferanser constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $leieforholdId = isset($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['leieforhold'] = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold::class, $leieforholdId);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        if(!$leieforhold->hentId()) {
            return false;
        }
        $this->tittel = "Varslingspreferanser leieforhold nr. {$leieforhold->hentId()}";
        return true;
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];

        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_preferanser\body\main\Index::class, [
            'leieforhold' => $leieforhold
        ]);
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     */
    public function skript()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];

        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\js\leieforhold_preferanser\body\main\Index::class, [
            'leieforhold' => $leieforhold
        ]);
    }
}