<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold as LeieforholdModell;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class BetalingsutsettelseSkjema extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $leieforholdId = $_GET['leieforhold'] ?? null;
        $kravId = $_GET['krav'] ?? null;
        $regningId = $_GET['regning'] ?? null;
        $this->hoveddata['leieforhold'] = $this->hentModell(LeieforholdModell::class, $leieforholdId);
        /** @var Krav $krav */
        $krav = $this->hentModell(Krav::class, (int)$kravId);
        if (!$krav->hentId()) {
            $krav = null;
        }
        if (!$krav) {
            /** @var Regning $regning */
            $regning = $this->hentModell(Regning::class, (int)$regningId);
            $krav = $regning->hentKrav()->hentFørste();
        }
        $this->hoveddata['krav'] = $krav;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = 'Betalingsutsettelse';
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var LeieforholdModell $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        /** @var Krav $krav */
        $krav = $this->hoveddata['krav'];

        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\betalingsutsettelse_skjema\Index::class, [
            'leieforhold' => $leieforhold,
            'krav' => $krav,
        ]);
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function skript(): ViewInterface
    {
        /** @var LeieforholdModell $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        /** @var Krav $krav */
        $krav = $this->hoveddata['krav'];

        $skript = new ViewArray([
            $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\js\betalingsutsettelse_skjema\Index::class, [
                'leieforhold' => $leieforhold,
                'kravId' => $krav ? $krav->hentId() : '',
            ]),
        ]);
        return $skript;
    }
}