<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\MineSiderKontroller;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Head;
use Kyegil\Leiebasen\Visning\mine_sider\html\skademelding_utbedring_skjema\body\main\Index;

class SkademeldingUtbedringSkjema extends MineSiderKontroller
{
    /**
     * SkademeldingSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $skadeId = isset($_GET['id']) ? $_GET['id'] : null;

        /** @var Leieobjekt\Skade $skade */
        $skade = $this->hentModell(Leieobjekt\Skade::class, $skadeId);

        $this->hoveddata['skade'] = $skade->hentId() ? $skade : null;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Leieobjekt\Skade|null $skade */
        $skade = $this->hoveddata['skade'];
        if(!$skade) {
            return false;
        }
        $this->tittel = "Meld skade {$skade->hentId()} som utbedret";
        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        $htmlHead
            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]));

        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     * @throws \Exception
     */
    public function innhold()
    {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        /** @var Leieobjekt\Skade $skade */
        $skade = $this->hoveddata['skade'];

        return $this->vis(Index::class, [
            'bruker' => $bruker,
            'skade' => $skade
        ]);
    }
}