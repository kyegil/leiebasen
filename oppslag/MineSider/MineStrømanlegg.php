<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;

class MineStrømanlegg extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * @return bool
     * @throws \Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = "Mine strømanlegg";

        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws \Exception
     */
    public function innhold()
    {
        /** @var \Kyegil\Leiebasen\Modell\Person $bruker */
        $bruker = $this->hoveddata['bruker'];

        $html = $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\mine_strømanlegg\body\main\Index::class, [
            'bruker' => $bruker,
        ]);
        return $html;
    }
}