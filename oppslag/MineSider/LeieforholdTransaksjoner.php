<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;


class LeieforholdTransaksjoner extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * LeieforholdTransaksjoner constructor.
     *
     * @param array $di
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $leieforholdId = isset($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['leieforhold'] = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold::class, $leieforholdId);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $leieforholdId = isset($_GET['id']) ? $_GET['id'] : null;
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $leieforhold = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold::class, $leieforholdId);
        if(!$leieforhold->hentId()) {
            return false;
        }
        $this->tittel = "Transaksjoner leieforhold nr. {$leieforhold->hentId()}";
//        $html = $this->hentRespons();
//        $html->getData('main')->setTemplate('mine_sider/html/shared/body/main_noheader');
        return true;
    }

    public function innhold()
    {
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $leieforhold = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold::class, $_GET['id']);

        $html =  $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_transaksjoner\body\main\Index::class, [
            'leieforhold' => $leieforhold,
        ]);
        return $html;
    }
}