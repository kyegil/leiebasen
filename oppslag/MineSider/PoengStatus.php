<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Poengprogram;

/**
 *
 */
class PoengStatus extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        $programId = $_GET['program'] ?? null;
        $leieforholdId = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hentModell(Leieforhold::class, (int)$leieforholdId);
        $program = $this->hentPoengbestyrer()->hentProgram($programId);
        $this->hoveddata['poeng_program'] = $program;
        $this->hoveddata['leieforhold'] = $leieforhold;
        parent::forberedHovedData();
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        /** @var Poengprogram $program */
        $program = $this->hoveddata['poeng_program'];
        if(!$leieforhold->hentId()) {
            return false;
        }

        if(!$program) {
            return false;
        }
        $this->tittel = $program->hentNavn();

        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Poengprogram $program */
        $program = $this->hoveddata['poeng_program'];
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];

        return $this->vis(\Kyegil\Leiebasen\Visning\drift\html\poeng_status\Index::class, [
            'program' => $program,
            'leieforhold' => $leieforhold,
        ]);
    }
}