<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Fellesstrøm\Strømanlegg;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Head;

class FsAnlegg extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * FsAnlegg constructor.
     *
     * @param array $di
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $anleggsId = isset($_GET['id']) ? $_GET['id'] : null;
        /** @var Strømanlegg $strømanlegg */
        $strømanlegg = $this->hentModell(Strømanlegg::class, $anleggsId);
        $this->hoveddata['strømanlegg'] = $strømanlegg;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Strømanlegg $strømanlegg */
        $strømanlegg = $this->hoveddata['strømanlegg'];
        if(!$strømanlegg->hentId()) {
            return false;
        }
        $this->tittel = $strømanlegg->hentFormål();

        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        if($htmlHead instanceof Visning) {
            $tittel = "Strømanlegg nr. {$strømanlegg->hentAnleggsnummer()}: {$strømanlegg->hentFormål()}";
            $htmlHead->sett('title', $tittel);
        }

        /**
         * Importer Ext JS
         * @link https://docs.sencha.com/extjs/4.2.1/
         */
        $htmlHead
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "/pub/lib/{$this->ext_bibliotek}/ext-all.js"
            ]));
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws \Exception
     */
    public function innhold()
    {
        /** @var Strømanlegg $strømanlegg */
        $strømanlegg = $this->hoveddata['strømanlegg'];

        $html = $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\fs_anlegg\body\main\Index::class, [
            'strømanlegg' => $strømanlegg,
        ]);
        return $html;
    }

    /**
     * @return Visning|string
     */
    public function skript()
    {
        /** @var Strømanlegg $strømanlegg */
        $strømanlegg = $this->hoveddata['strømanlegg'];

        $skript = $this->vis(\Kyegil\Leiebasen\Visning\felles\extjs4\Index::class, [
            'extOnReady' => $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\js\fs_anlegg\body\main\Forbruksstatistikk::class, [
                'strømanlegg' => $strømanlegg
            ])
        ]);
        return $skript;
    }
}