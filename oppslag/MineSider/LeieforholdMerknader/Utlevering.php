<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 09/11/2020
 * Time: 08:10
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider\LeieforholdMerknader;


use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Beboerstøtte\Sakstråd\Innlegg;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Leieobjekt\Skade;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktUtlevering;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Visning\felles\html\shared\Utskrift;
use Kyegil\ViewRenderer\ViewArray;

class Utlevering extends AbstraktUtlevering
{
    /**
     * @param string $data
     * @return $this
     */
    public function hentData(string $data)
    {
        $resultat = (object)[
            'success' => true,
            'msg' => '',
            'data' => []
        ];

        try {
            switch ($data) {
                case "brev":
                    try {
                        $leieforhold = $this->app->hoveddata['leieforhold'];
                        $notatId = $_GET['brev'] ?? null;
                        /** @var Notat $notat */
                        $notat = $this->app->hentModell(Notat::class, $notatId);
                        if ($notat->hentLeieforhold()->hentId() != $leieforhold->hentId()) {
                            throw new Exception('Ugyldig leieforhold');
                        }
                        if ($notat->hentSkjulForLeietaker()) {
                            throw new Exception('Ikke tillatt');
                        }
                        $tittel = implode(' ', array_filter([
                            $notat->kategori,
                            $notat->henvendelseFra,
                            $notat->dato->format('d.m.Y')
                        ]));
                        $brevtekst = $notat->hentBrevtekst();
                        $utskrift = $this->app->vis(Utskrift::class, [
                            'tittel' => $tittel,
                            'innhold' => $brevtekst
                        ]);
                        $respons = new Respons($utskrift);
                        $this->settRespons($respons);
                        return $this;
                    } catch (Exception $e) {
                        $this->app->responder404();
                    }

                case "vedlegg":
                    try {
                        $leieforhold = $this->app->hoveddata['leieforhold'];
                        $vedleggId = $_GET['vedlegg'] ?? null;
                        /** @var Notat $notat */
                        $notat = $this->app->hentModell(Notat::class, $vedleggId);
                        if ($notat->hentLeieforhold()->hentId() != $leieforhold->hentId()) {
                            throw new Exception('Ugyldig leieforhold');
                        }
                        if ($notat->hentSkjulForLeietaker()) {
                            throw new Exception('Ikke tillatt');
                        }
                        $fil = $notat->hentVedlegg();
                        if (
                            !file_exists($fil)
                            || !$filInnhold = file_get_contents($fil)
                        ) {
                            throw new Exception('Vedlegget finnes ikke.');
                        }
                        $respons = new Respons($filInnhold);
                        $respons->header('Content-type: ' . mime_content_type($fil));
                        $respons->header('Content-Disposition: attachment; filename="' . $notat->hentVedleggsnavn() . '"');
                        $respons->header('Content-Transfer-Encoding: binary');
                        $respons->header('Content-Length: ' . strlen($filInnhold));
                        $respons->header('Accept-Ranges: bytes');

                        $this->settRespons($respons);
                        return $this;
                    } catch (Exception $e) {
                        $this->app->responder404();
                    }
                default:
                    throw new Exception("feil eller manglende 'data'-parameter mangler i data-forespørsel");
            }

        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            $this->settRespons(new JsonRespons($resultat));
        }
        return $this;
    }
}