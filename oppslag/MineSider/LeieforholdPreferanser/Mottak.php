<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 07/09/2020
 * Time: 17:10
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider\LeieforholdPreferanser;


use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this|Mottak
     */
    public function taIMot(string $skjema)
    {
        /** @var \stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Dine preferanser er lagret."
        ];
        $get = $_GET;
        $post = $_POST;
        $skjema = isset($get['skjema']) ? $get['skjema'] : '';
        try {
            switch ($skjema) {
                case 'preferanseskjema':
                    $leieforholdId = isset($get['id']) ? $get['id'] : null;
                    $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);
                    if(!$leieforhold->hentId()) {
                        throw new Exception("Finner ikke leieforholdet");
                    }
                    if(empty($post)) {
                        throw new Exception("Ingen verdier ble sendt");
                    }
                    $direkteAttributter = [
                        'giroformat', 'regning_til_objekt', 'regningsobjekt',
                        'regningsadresse1', 'regningsadresse2', 'postnr', 'poststed', 'land'
                    ];
                    foreach($direkteAttributter as $attributt) {
                        $leieforhold->sett($attributt, isset($post[$attributt]) ? $post[$attributt] : null);
                    }
                    /** @var Person $regningsperson */
                    $regningsperson = $this->app->hentModell(Person::class, $post['regningsperson'] ?? null);
                    $leieforhold->settRegningsperson($regningsperson->hentId() ? $regningsperson : null);
                    break;
                default:
                    $resultat->success = true;
                    break;
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}