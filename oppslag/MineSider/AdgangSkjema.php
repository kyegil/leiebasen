<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Head;
use Kyegil\MysqliConnection\MysqliConnection;
use Kyegil\MysqliConnection\MysqliConnectionInterface;

class AdgangSkjema extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        $this->tittel = "Områder du har adgang til";
        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        $htmlHead
            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]));

        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Person $skade */
        $bruker = $this->hoveddata['bruker'];

        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\adgang_skjema\body\main\Index::class, [
            'bruker' => $bruker
        ]);
    }
}