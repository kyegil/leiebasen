<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Modell;
use Kyegil\Leiebasen\Modell\Leieforhold as LeieforholdModell;
use Kyegil\Leiebasen\Oppslag\AbstraktOppdrag;
use Kyegil\ViewRenderer\ViewArray;
use Kyegil\ViewRenderer\ViewInterface;

class _MalSkjema extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /** @var AbstraktOppdrag[] */
    protected $oppdrag = [
//        'oppgave' => _MalSkjema\Oppdrag::class
    ];
    /**
     * Forbered hoveddata
     *
     * @throws Exception
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $leieforholdId = $_GET['leieforhold'] ?? null;
        $this->hoveddata['leieforhold'] = $this->hentModell(LeieforholdModell::class, $leieforholdId);

        $modellId = !empty($_GET['id']) ? $_GET['id'] : null;
        /** @var Modell $modell */
        $modell = $this->hentModell(Modell::class, (int)$modellId);
        if (!$modell->hentId() || $modellId == '*') {
            $modell = null;
        }
        $this->hoveddata['modell'] = $modell;
        $this->hoveddata['modellId'] = $modellId;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Modell $modell */
        $modell = $this->hoveddata['modell'];
        if(!$modell && $this->hoveddata['modellId'] != '*') {
            return false;
        }
        $this->tittel = 'Mal';
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Modell $modell */
        $modell = $this->hoveddata['modell'];

        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\_mal_skjema\Index::class, [
            'modell' => $modell,
            'modellId' => $this->hoveddata['modellId'],
        ]);
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function skript(): ViewInterface
    {
        /** @var Modell $modell */
        $modell = $this->hoveddata['modell'];

        $skript = new ViewArray([
            $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\js\_mal_skjema\Index::class, [
                'modell' => $modell,
                'modellId' => $this->hoveddata['modellId'],
            ]),
        ]);
        return $skript;
    }
}