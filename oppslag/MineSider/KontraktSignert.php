<?php


namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Modell\Leieforhold\Kontrakt;
use Kyegil\Leiebasen\Oppslag\MineSiderKontroller;
use Kyegil\Leiebasen\Respons;
use Kyegil\Leiebasen\Respons\PdfRespons;

/**
 * KontraktSignert Controller
 *
 * @package Kyegil\Leiebasen\Oppslag\MineSider
 */
class KontraktSignert extends MineSiderKontroller
{
    /**
     * KontraktSignert constructor.
     *
     * @param array $di
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $kontraktId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['kontrakt'] = null;
        /** @var Kontrakt $kontrakt */
        $kontrakt = $this->hentModell(Kontrakt::class, (int)$kontraktId);
        if ($kontrakt->hentId()) {
            $this->hoveddata['kontrakt'] = $kontrakt;
            $this->hoveddata['leieforhold'] = $kontrakt->leieforhold;
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Kontrakt $kontrakt */
        $kontrakt = $this->hoveddata['kontrakt'];
        if (
            !$kontrakt || !$kontrakt->hentId()
        ) {
            return false;
        }
        $sti = $this->hentFilarkivBane() . '/leieavtaler/';

        /** @var string|null $signertFil */
        $signertFil = $kontrakt->hent('signert_fil');
        $filendelse = pathinfo($signertFil, PATHINFO_EXTENSION);

        if (!$signertFil ||
            !file_exists($sti . $signertFil)
            || !is_readable($sti . $signertFil)
        ) {
            return false;
        }
        $filinnhold = file_get_contents($sti . $signertFil);
        if (!$filinnhold) {
            return false;
        }

        switch(strtolower($filendelse)) {
            case 'pdf':
                $respons = new PdfRespons($filinnhold);
                $respons->filnavn = $signertFil;
                $this->settRespons($respons);
                break;
            default:
                $respons = new Respons($filinnhold);
                $respons->filnavn = $signertFil;
                $respons->header('Content-type: image/' . ($filendelse == 'jpg' ? 'jpeg' : $filendelse));
                $respons->header('Content-Disposition: attachment; filename="' . $signertFil . '";');
                $respons->header('Content-Length: ' . strlen($filinnhold));
                $this->settRespons($respons);
                break;
        }
        return true;
    }
}