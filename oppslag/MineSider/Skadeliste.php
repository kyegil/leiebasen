<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieobjekt as LeieobjektModell;
use Kyegil\Leiebasen\Oppslag\MineSiderKontroller;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Head;
use Kyegil\MysqliConnection\MysqliConnection;
use Kyegil\MysqliConnection\MysqliConnectionInterface;

class Skadeliste extends MineSiderKontroller
{
    /**
     * Skadeliste constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $leieobjektId = isset($_GET['id']) ? $_GET['id'] : null;
        /** @var LeieobjektModell $leieobjekt */
        $leieobjekt = $this->hentModell(LeieobjektModell::class, $leieobjektId);
        $this->hoveddata['leieobjekt'] = $leieobjekt && $leieobjekt->hentId() ? $leieobjekt : null;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = "Registrerte skader";
        if($this->hoveddata['leieobjekt']) {
            $this->tittel .= " på leieobjekt {$this->hoveddata['leieobjekt']->hentId()}";
        }

        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        $htmlHead
            /**
             * Importer DataTables
             * @link https://datatables.net/
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_JS
            ]))
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_CSS
            ]))
            /**
             * Importer DataTables Responsive Extension
             * @link https://datatables.net/
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_RESPONSIVE_JS
            ]))
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => \Kyegil\Leiebasen\Visning\felles\html\table\DataTable::DATATABLES_SOURCE_RESPONSIVE_CSS
            ]))
        ;

        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     */
    public function innhold()
    {
        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\skadeliste\body\main\Index::class, [
            'leieobjekt' => $this->hoveddata['leieobjekt']
        ]);
    }
}