<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;

class LeieobjekterLedige extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * @return bool
     * @throws \Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = "Ledige leieobjekter";
        return true;
    }

    public function innhold()
    {
        $html =  $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\leieobjekter_ledige\body\main\Index::class, []);
        return $html;
    }
}