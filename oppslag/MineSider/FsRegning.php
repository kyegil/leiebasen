<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Kyegil\Leiebasen\Modell\Kostnadsdeling\Tjeneste\Kostnad as Strømregning;

class FsRegning extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /** @var string */
    public $ext_bibliotek = 'ext-4.2.1.883';

    /**
     * FsAnlegg constructor.
     *
     * @param array $di
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $regningsId = isset($_GET['id']) ? $_GET['id'] : null;
        /** @var Strømregning $strømregning */
        $strømregning = $this->hentModell(Strømregning::class, $regningsId);
        $this->hoveddata['strømregning'] = $strømregning;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Strømregning $strømregning */
        $strømregning = $this->hoveddata['strømregning'];
        if(!$strømregning->hentId()) {
            return false;
        }
        $this->tittel = "Strømregning faktura nr. {$strømregning->hentFakturanummer()}";
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws \Exception
     */
    public function innhold()
    {
        /** @var Strømregning $strømregning */
        $strømregning = $this->hoveddata['strømregning'];

        $html = $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\fs_regning\body\main\Index::class, [
            'faktura' => $strømregning,
        ]);
        return $html;
    }
}