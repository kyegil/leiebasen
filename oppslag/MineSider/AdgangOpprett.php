<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\mine_sider\html\adgang_opprett\body\main\Index;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Head;
use Kyegil\Leiebasen\Visning\mine_sider\js\adgang_opprett\body\main\Index as JsIndex;
use Kyegil\MysqliConnection\MysqliConnection;
use Kyegil\MysqliConnection\MysqliConnectionInterface;

class AdgangOpprett extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * AdgangOpprett constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $leieforholdId = !empty($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['leieforhold'] = $this->hentModell(Leieforhold::class, $leieforholdId);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        $this->tittel = 'Tildel andre adgang til ditt leieforhold';
        if ($leieforhold) {
            $this->tittel = 'Tildel andre adgang til leieforhold ' . $leieforhold->hentId();
        }
        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        $htmlHead
            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]));

        return true;
    }

    /**
     * @return Index
     * @throws Exception
     */
    public function innhold(): Index
    {
        /** @var Person $skade */
        $bruker = $this->hoveddata['bruker'];
        /** @var Leieforhold $leieforhold|null */
        $leieforhold = $this->hoveddata['leieforhold'];

        return $this->vis(Index::class, [
            'bruker' => $bruker,
            'leieforhold' => $leieforhold
        ]);
    }

    /**
     * @return jsIndex
     */
    public function skript()
    {
        return $this->vis(jsIndex::class);
    }
}