<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\MineSiderKontroller;
use Kyegil\MysqliConnection\MysqliConnection;
use Kyegil\MysqliConnection\MysqliConnectionInterface;
use Kyegil\ViewRenderer\ViewInterface;

class LeieforholdListe extends MineSiderKontroller
{
    /**
     * LeieforholdListe constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = "Velg leieforhold";
        return true;
    }

    /**
     * @return ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        $html = $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_liste\body\main\Index::class, [
            'bruker' => $bruker
        ]);
        return $html;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     */
    public function skript()
    {
        return '';
//        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\js\leieforhold_liste\body\main\Index::class, []);
    }
}