<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider\ProfilKommunikasjonPreferanser;


use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Modell\Person\Adgang;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use stdClass;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this|Mottak
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Dine preferanser er lagret."
        ];
        $skjema = $this->data['get']['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'preferanser':
                    /** @var Person|null $bruker */
                    $bruker = $this->app->hoveddata['bruker'] ?? null;
                    if (!$bruker) {
                        throw new Exception('Du er ikke innlogget');
                    }

                    $reservasjonMotMasseepost = !empty($this->data['post']['reservasjon_mot_masseepost']);
                    $reservasjonMotMasseSms = !empty($this->data['post']['reservasjon_mot_masse_sms']);
                    $bruker->settBrukerProfilPreferanse('reservasjon_mot_masseepost', $reservasjonMotMasseepost);
                    $bruker->settBrukerProfilPreferanse('reservasjon_mot_masse_sms', $reservasjonMotMasseSms);

                    /** @var Adgang $adgang */
                    foreach ($bruker->hentAdganger() as $adgang) {
                        if ($adgang->adgang == Adgang::ADGANG_MINESIDER) {
                            $epostvarsling = boolval($this->data['post']['adgang'][$adgang->hentId()]['epostvarsling'] ?? false);
                            $innbetalingsbekreftelse = $epostvarsling && ($this->data['post']['adgang'][$adgang->hentId()]['innbetalingsbekreftelse'] ?? false);
                            $forfallsvarsel = $epostvarsling && ($this->data['post']['adgang'][$adgang->hentId()]['forfallsvarsel'] ?? false);
                            $umiddelbartBetalingsvarselEpost = $epostvarsling && ($this->data['post']['adgang'][$adgang->hentId()]['umiddelbart_betalingsvarsel_epost'] ?? false);

                            $adgang
                                ->sett('epostvarsling', $epostvarsling)
                                ->sett('innbetalingsbekreftelse', $innbetalingsbekreftelse)
                                ->sett('forfallsvarsel', $forfallsvarsel)
                            ;
                            $adgang->umiddelbartBetalingsvarselEpost = $umiddelbartBetalingsvarselEpost ?: null;
                        }
                    }

                    $resultat->url = '/mine-sider/index.php?oppslag=profil_kommunikasjon_preferanser';

                    break;
                default:
                    throw new Exception('Ugyldig skjema');
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}