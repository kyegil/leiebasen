<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;

class OversiktInntekter extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * @return bool
     * @throws \Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $this->tittel = "Leieinntekter " . $this->hentValg('utleier');
        return true;
    }

    public function innhold()
    {
        $html =  $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\oversikt_inntekter\body\main\Index::class, []);
        return $html;
    }
}