<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;


use Kyegil\Leiebasen\Modell\Leieforhold as LeieforholdModell;
use Kyegil\Leiebasen\Oppslag\MineSiderKontroller;
use Kyegil\MysqliConnection\MysqliConnection;
use Kyegil\MysqliConnection\MysqliConnectionInterface;

class LeieforholdUtestående extends MineSiderKontroller
{
    /**
     * LeieforholdTransaksjoner constructor.
     *
     * @param array $di
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $leieforholdId = isset($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['leieforhold'] = $this->hentModell(LeieforholdModell::class, $leieforholdId);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        $leieforholdId = isset($_GET['id']) ? $_GET['id'] : null;
        /** @var LeieforholdModell $leieforhold */
        $leieforhold = $this->hentModell(LeieforholdModell::class, $leieforholdId);
        if(!$leieforhold->hentId()) {
            return false;
        }
        $this->tittel = "Utestående leieforhold nr. {$leieforhold->hentId()}";
        return true;
    }

    public function innhold()
    {
        /** @var LeieforholdModell $leieforhold */
        $leieforhold = $this->hentModell(LeieforholdModell::class, $_GET['id']);

        $html =  $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\mine_regninger\body\main\index\utestående\Leieforhold::class, [
            'leieforhold' => $leieforhold,
        ]);
        return $html;
    }
}