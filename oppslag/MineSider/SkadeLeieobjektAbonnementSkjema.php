<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\MineSiderKontroller;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Head;
use Kyegil\Leiebasen\Visning\mine_sider\html\skademelding_skjema\body\main\Index;
use Kyegil\Leiebasen\Visning\mine_sider\js\skademelding_skjema\body\main\Index as jsIndex;
use Kyegil\MysqliConnection\MysqliConnection;
use Kyegil\MysqliConnection\MysqliConnectionInterface;

class SkadeLeieobjektAbonnementSkjema extends MineSiderKontroller
{
    /**
     * SkadeLeieobjektAbonnementSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        $this->tittel = "Motta melding om skader";
        parent::__construct($di, $config);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        $htmlHead

            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]));

        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];

        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\skade_leieobjekt_abonnement_skjema\body\main\Index::class, [
            'bruker' => $bruker
        ]);
    }
}