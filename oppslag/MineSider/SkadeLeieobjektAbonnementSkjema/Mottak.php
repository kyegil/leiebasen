<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider\SkadeLeieobjektAbonnementSkjema;


use Exception;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use stdClass;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Dine preferanser er lagret.",
            'url' => '/mine-sider/index.php?oppslag=skade_leieobjekt_abonnement_skjema'
        ];
        $skjema = $this->data['get']['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'abonnement':
                    /** @var Person|null $bruker */
                    $bruker = $this->app->hoveddata['bruker'] ?? null;
                    if (!$bruker) {
                        throw new Exception('Du er ikke innlogget');
                    }

                    $områder = $this->data['post']['områder'] ?? [];
                    $bygninger = $this->data['post']['bygninger'] ?? [];
                    $leieobjekter = $this->data['post']['leieobjekter'] ?? [];
                    $skadeAutoAbonnement = (object)[
                        'leieobjekter' => array_map('intval', $leieobjekter),
                        'bygninger' => array_map('intval', $bygninger),
                        'områder' => array_map('intval', $områder),
                    ];

                    $bruker->settBrukerProfilPreferanse('skade_auto_abonnement', $skadeAutoAbonnement);

                    break;
                default:
                    $resultat->success = true;
                    break;
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}