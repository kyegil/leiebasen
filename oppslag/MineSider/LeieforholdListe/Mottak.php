<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 07/09/2020
 * Time: 17:10
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider\LeieforholdListe;


use Exception;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Person;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var \stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Dine preferanser er lagret."
        ];
        $get = $_GET;
        $post = $_POST;
        $skjema = isset($get['skjema']) ? $get['skjema'] : '';
        try {
            switch ($skjema) {
                case 'hovedleieforhold':
                    /** @var Person|null $bruker */
                    $bruker = $this->app->hoveddata['bruker'];
                    if(!is_a($bruker, Person::class)) {
                        throw new Exception('Finner ikke bruker');
                    }
                    $leieforholdId = isset($post['hovedleieforhold-velger']) ? $post['hovedleieforhold-velger'] : null;
                    $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);
                    if(!$leieforhold->hentId() || !$bruker->harAdgangTil('mine-sider', $leieforhold)) {
                        throw new Exception("Dette leieforholdet kan ikke velges");
                    }
                    $bruker->settHovedLeieforhold($leieforhold);
                    break;
                default:
                    $resultat->success = true;
                    break;
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $resultat->msg = $e->getMessage();
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }
}