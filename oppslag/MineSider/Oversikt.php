<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Kyegil\Leiebasen\Modell\Person;

class Oversikt extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /** @var string */
    public $tittel = 'Mine sider';

    /**
     * Oversikt constructor.
     *
     * @param array $di
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $this->hoveddata['leieforhold'] = null;
    }

    /**
     * @return \Kyegil\Leiebasen\Respons
     */
    public function hentRespons()
    {
        $this->returi->reset();
        return parent::hentRespons();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function innhold()
    {
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold|null */
        $leieforhold = $bruker->hentHovedLeieforhold();
        /** @var \Kyegil\Leiebasen\Modell\Leieobjekt|null $leieobjekt */
        $leieobjekt = $leieforhold ? $leieforhold->hentLeieobjekt() : null;
        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\oversikt\body\main\Index::class, [
            'bruker' => $bruker,
            'leieforhold' => $leieforhold,
            'leieobjekt' => $leieobjekt
        ]);
    }
}