<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Html;

class Leieforhold extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * Leieforhold constructor.
     *
     * @param array $di
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $leieforholdId = isset($_GET['id']) ? $_GET['id'] : null;
        $this->hoveddata['leieforhold'] = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold::class, $leieforholdId);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        if(!$leieforhold->hentId()) {
            return false;
        }
        $this->tittel = "Leieforhold nr. {$leieforhold->hentId()}";

        /** @var Html $html */
        $html = $this->hentRespons()->hentInnhold();
        $html->getData('head')->setData('title', $leieforhold->beskrivelse);
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws \Exception
     */
    public function innhold()
    {
        /** @var \Kyegil\Leiebasen\Modell\Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];

        $html = $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold\body\main\Index::class, [
            'bruker' => $bruker,
            'leieforhold' => $leieforhold,
        ]);
        return $html;
    }
}