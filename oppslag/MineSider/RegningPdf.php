<?php
/**
 * * Part of kyegil/leiebasen
 * Created by Kyegil
 * Date: 10/06/2020
 * Time: 09:54
 */

namespace Kyegil\Leiebasen\Oppslag\MineSider;


use Kyegil\Leiebasen\Leiebase;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Respons;

class RegningPdf extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * RegningPdf constructor.
     *
     * @param array $di
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $regningsId = isset($_GET['id']) ? $_GET['id'] : null;
        /** @var \Kyegil\Leiebasen\Modell\Leieforhold\Regning $regning */
        $regning = $this->hentModell(\Kyegil\Leiebasen\Modell\Leieforhold\Regning::class, $regningsId);
        $leieforhold = $regning->hentLeieforhold();
        $this->hoveddata['regning'] = $regning;
        $this->hoveddata['leieforhold'] = $leieforhold;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        try {
            $regningsId = $_GET['id'];
            /** @var \Kyegil\Leiebasen\Modell\Leieforhold\Regning $regning */
            $regning = $this->hentModell(Regning::class, $regningsId);
            if (!$regning->hentUtskriftsdato()) {
                throw new \Exception('Denne regningen er ikke skrevet ut');
            }
            $pdf = $regning->hentPdf();
            $respons = new Respons($pdf);
            $respons->header('Content-type: application/pdf');
            $respons->header('Content-Disposition: inline; filename="' . $regningsId . '.pdf"');
            $respons->header('Content-Transfer-Encoding: binary');
            $respons->header('Content-Length: ' . strlen($pdf));
            $respons->header('Accept-Ranges: bytes');
            $this->respons = $respons;
        } catch (\Exception $e) {
            $this->settAdvarsel(Leiebase::VARSELNIVÅ_ADVARSEL, ['tekst' => $e->getMessage(), 'oppsummering' => $e->getMessage()]);
        }
        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface|string
     * @throws \Exception
     */
    public function innhold()
    {
        return '';
    }
}