<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Visning\felles\html\table\DataTable;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Head;

class BetalingsplanSkjema extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * Forbered hoveddata
     */
    public function forberedHovedData()
    {
        parent::forberedHovedData();
        $leieforholdId = $_GET['leieforhold'] ?? null;
        $this->hoveddata['leieforhold'] = $this->hentModell(Leieforhold::class, (int)$leieforholdId);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }

        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        if(!$leieforhold || !$leieforhold->hentId()) {
            return false;
        }
        $this->tittel = 'Forslag til betalingsplan ';

        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        $htmlHead
            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]));

        $htmlHead
            /**
             * Importer DataTables
             * @link https://datatables.net/
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => DataTable::DATATABLES_SOURCE_JS
            ]))
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => DataTable::DATATABLES_SOURCE_CSS
            ]))
            /**
             * Importer DataTables Responsive Extension
             * @link https://datatables.net/
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => DataTable::DATATABLES_SOURCE_RESPONSIVE_JS
            ]))
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => DataTable::DATATABLES_SOURCE_RESPONSIVE_CSS
            ]))
            /**
             * Importer DataTables Processing Plugin
             * @link https://datatables.net/
             */
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.datatables.net/plug-ins/1.11.5/api/processing().js"
            ]))
        ;

        return true;
    }

    /**
     * @return \Kyegil\ViewRenderer\ViewInterface
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        $betalingsplan = $leieforhold->hentBetalingsplan();

        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\html\betalingsplan_skjema\Index::class, [
            'leieforhold' => $leieforhold,
            'betalingsplan' => $betalingsplan
        ]);
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     */
    public function skript()
    {
        /** @var Leieforhold $leieforhold */
        $leieforhold = $this->hoveddata['leieforhold'];
        $betalingsplan = $leieforhold->hentBetalingsplan();
        return $this->vis(\Kyegil\Leiebasen\Visning\mine_sider\js\betalingsplan_skjema\Index::class, [
            'leieforholdId' => $leieforhold->hentId(),
            'leieforhold' => $leieforhold,
            'betalingsplan' => $betalingsplan
        ]);
    }
}