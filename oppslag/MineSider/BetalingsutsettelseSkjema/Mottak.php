<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider\BetalingsutsettelseSkjema;


use DateTime;
use DateTimeImmutable;
use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieforhold;
use Kyegil\Leiebasen\Modell\Leieforhold\Krav;
use Kyegil\Leiebasen\Modell\Leieforhold\Notat;
use Kyegil\Leiebasen\Modell\Leieforhold\Regning;
use Kyegil\Leiebasen\Oppslag\AbstraktMottak;
use Kyegil\Leiebasen\Respons\JsonRespons;
use Kyegil\Leiebasen\Visning\felles\epost\shared\Epost;
use stdClass;

class Mottak extends AbstraktMottak
{

    /**
     * @param string $skjema
     * @return $this
     */
    public function taIMot(string $skjema)
    {
        /** @var stdClass $result */
        $resultat = (object)[
            'success' => true,
            'msg' => "Lagret."
        ];
        $get = $this->data['get'];
        $post = $this->data['post'];

        $skjema = $get['skjema'] ?? '';
        try {
            switch ($skjema) {
                case 'betalingsutsettelse':
                    $leieforholdId = $get['leieforhold'] ?? null;
                    $kravId = $post['krav'] ?? '';
                    $notat = $post['notat'] ?? '';
                    $nyForfallsdato = $post['forfallsdato'] ? date_create_immutable($post['forfallsdato']) : null;
                    $iDag = new DateTimeImmutable();

                    /** @var Leieforhold $leieforhold */
                    $leieforhold = $this->app->hentModell(Leieforhold::class, $leieforholdId);
                    /** @var Krav $krav */
                    $krav = $this->app->hentModell(Krav::class, $kravId);
                    $eksisterendeForfall = $krav->forfall;
                    if($eksisterendeForfall instanceof DateTime) {
                        $eksisterendeForfall = DateTimeImmutable::createFromMutable($eksisterendeForfall);
                    }

                    if (!$leieforhold->hentId()) {
                        throw new Exception('Ugyldig leieforhold');
                    }
                    if ($krav->leieforhold->hentId() != $leieforhold->hentId()) {
                        throw new Exception('Ugyldig krav');
                    }
                    if (!$nyForfallsdato) {
                        throw new Exception('Ugyldig forfallsdato');
                    }
                    if($eksisterendeForfall < $iDag) {
                        throw new Exception('Dette kravet har allerede forfalt, og kan ikke utsettes. Du bør istedet be om en betalingsplan.');
                    }

                    // Valideringen er fullført og dato kan endres

                    $spillerom = $leieforhold->hentBetalingUtsettelseSpillerom();
                    if($nyForfallsdato->format('Ymd') == $eksisterendeForfall->format('Ymd')) {
                        $resultat->msg = 'Forfallsdato er uendret';
                    }
                    else {
                        $regning = $krav->regning;
                        if(!$regning) {
                            $regning = $this->app->hentUtskriftsprosessor()->buntRegninger([$krav->hentId()])->hentFørste();
                        }
                        if($spillerom && $nyForfallsdato > $eksisterendeForfall->add($spillerom)) {
                            $this->opprettForespørselOmBetalingsutsettelse($regning, $nyForfallsdato, $notat);
                            $resultat->msg = 'Forespørsel om utsettelse er levert';
                        }

                        else {
                            $originalForfallsdato = $regning->hent('original_forfallsdato');
                            if(!$originalForfallsdato) {
                                $regning->sett('original_forfallsdato', $eksisterendeForfall);
                            }
                            else if ($originalForfallsdato->format('Ymd') == $nyForfallsdato->format('Ymd')) {
                                $regning->sett('original_forfallsdato', null);
                            }
                            $regning->settForfall($nyForfallsdato);
                            $resultat->msg = 'Forfallsdato er endret til ' . $krav->forfall->format('d.m.Y');
                        }
                    }
                    $resultat->url = (string)$this->app->returi->get(1);


                    break;
                default:
                    $resultat->success = false;
                    $resultat->msg = 'Ukjent skjema';
                    break;
            }
        }
        catch (Exception $e) {
            $resultat->success = false;
            $utleier = $this->app->hentValg('utleier');
            $epost = $this->app->hentValg('epost');
            $resultat->msg = $e->getMessage();
            $resultat->msg .= " Kontakt <a href='mailto:{$epost}'>{$utleier}</a> for å be om betalingsutsettelse.";
            error_log($e);
        }
        $this->settRespons(new JsonRespons($resultat));
        return $this;
    }

    /**
     * @param Regning $regning
     * @param DateTimeImmutable $ønsketForfallsdato
     * @param $forespørselTekst
     * @return void
     * @throws Exception
     */
    private function opprettForespørselOmBetalingsutsettelse(Regning $regning, DateTimeImmutable $ønsketForfallsdato, $forespørselTekst): void
    {
        $opprinneligForfall = $regning->hentOriginalForfallsdato();
        $nåværendeForfall = $regning->hentForfall();
        $leieforhold = $regning->leieforhold;

        /** @var Notat $notat */
        $notat = $this->app->nyModell(Notat::class, (object)[
            'leieforhold' => $leieforhold,
            'notat' => 'Forespørsel om å utsette regning ' . $regning->hentId() . ' til ' . $ønsketForfallsdato->format('d.m.Y'),
            'brevtekst' => $forespørselTekst,
            'dato' => new DateTimeImmutable(),
            'henvendelse_fra' => Notat::FRA_LEIETAKER,
            'skjul_for_leietaker' => false,
            'kategori' => Notat::KATEGORI_BETALINGSUTSETTELSE_FORESPØRSEL
        ]);

        $regning->utsettelseForespørsel = (object)[
            'opprinnelig_forfall' => $opprinneligForfall ? $opprinneligForfall->format('Y-m-d') : null,
            'nåværende_forfall' => $nåværendeForfall ? $nåværendeForfall->format('Y-m-d') : null,
            'ønsket_forfall' => $ønsketForfallsdato->format('Y-m-d'),
            'tekst' => $forespørselTekst,
            'notat_id' => $notat->hentId()
        ];

        $emne = 'Mottatt forespørsel om betalingsutsettelse';
        $epostHtml = [$leieforhold->hentNavn() . ' har bedt om betalingsutsettelse for regning ' . $regning->hentId() . '<br>'];
        $epostHtml[] = 'Nåværende forfall: ' . ($nåværendeForfall ? $nåværendeForfall->format('d.m.Y') : null) . '<br>';
        if ($opprinneligForfall) {
            $epostHtml[] = '(Opprinnelig ' . $opprinneligForfall->format('d.m.Y') . ')<br>';
        }
        $epostHtml[] = 'Ønsket utsettelse til: ' . $ønsketForfallsdato->format('d.m.Y') . '<br>';
        $epostHtml[] = '<div>Henvendelse:<br>' . $notat->brevtekst . '<br></div>';
        $epostHtml[] = '<div>Forespørselen må godkjennes eller avvises i fra oppfølging</div>';
        $epostHtml[] = new HtmlElement('a', [
            'title' => 'Klikk her for å godkjenne eller avvise forespørselen',
            'class' => 'button',
            'href' => $this->app->http_host . '/oppfølging/index.php?oppslag=oversikt&id=' . $leieforhold->hentId()
        ], 'Klikk her for å godkjenne eller avvise forespørselen');

        $this->app->sendMail((object)[
            'to' => "{$this->app->hentValg('utleier')} <{$this->app->hentValg('epost')}>",
            'type' => 'betalingsutsettelse_forespørsel',
            'subject' => $emne,
            'html' => $this->app->vis(Epost::class, [
                'tittel' => $emne,
                'innhold' => new HtmlElement('div', [], $epostHtml),
                'bunntekst' => ''
            ])
        ]);
    }
}