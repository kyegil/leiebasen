<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Modell\Person;
use Kyegil\Leiebasen\Visning\mine_sider\html\profil_kommunikasjon_preferanser\body\main\Index;
use Kyegil\Leiebasen\Visning\mine_sider\js\profil_kommunikasjon_preferanser\Script;
use Kyegil\MysqliConnection\MysqliConnection;
use Kyegil\MysqliConnection\MysqliConnectionInterface;

class ProfilKommunikasjonPreferanser extends \Kyegil\Leiebasen\Oppslag\MineSiderKontroller
{
    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Person $bruker */
        $bruker = $this->hoveddata['bruker'];
        $this->tittel = "Preferanser for epost-varsler og kommunikasjon";
        return true;
    }

    /**
     * @return Index
     * @throws Exception
     */
    public function innhold()
    {
        /** @var Person $skade */
        $bruker = $this->hoveddata['bruker'];

        return $this->vis(Index::class, [
            'bruker' => $bruker
        ]);
    }

    /**
     * @return Script
     */
    public function skript()
    {
        return $this->vis(Script::class);
    }
}