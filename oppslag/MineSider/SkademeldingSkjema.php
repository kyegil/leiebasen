<?php

namespace Kyegil\Leiebasen\Oppslag\MineSider;

use Exception;
use Kyegil\Leiebasen\Jshtml\Html\HtmlElement;
use Kyegil\Leiebasen\Modell\Leieobjekt;
use Kyegil\Leiebasen\Modell\Leieobjekt\Bygning;
use Kyegil\Leiebasen\Oppslag\MineSiderKontroller;
use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\mine_sider\html\shared\Head;
use Kyegil\Leiebasen\Visning\mine_sider\html\skademelding_skjema\body\main\Index;
use Kyegil\Leiebasen\Visning\mine_sider\js\skademelding_skjema\body\main\Index as jsIndex;

class SkademeldingSkjema extends MineSiderKontroller
{
    /**
     * SkademeldingSkjema constructor.
     *
     * @param array $di
     * @param array $config
     * @throws Exception
     */
    public function __construct(array $di = [], array $config = []) {
        parent::__construct($di, $config);
        $skadeId = isset($_GET['id']) ? $_GET['id'] : null;
        $leieobjektId = isset($_GET['leieobjekt']) ? $_GET['leieobjekt'] : null;
        $bygningsId = isset($_GET['bygning']) ? $_GET['bygning'] : null;

        /** @var Leieobjekt\Skade $skade */
        $skade = $this->hentModell(Leieobjekt\Skade::class, $skadeId);
        if($skade->hentId()) {
            $leieobjekt = $skade->leieobjekt;
            $bygning = $skade->bygning;
        }
        else {
            /** @var Leieobjekt $leieobjekt */
            $leieobjekt = $this->hentModell(Leieobjekt::class, $leieobjektId);
            /** @var Bygning $bygning */
            $bygning = $this->hentModell(Bygning::class, $bygningsId);
            if(!$bygning->hentId() and $leieobjekt->hentId()) {
                $bygning = $leieobjekt->hentBygning();
            }
        }

        $this->hoveddata['leieforhold'] = null;
        $this->hoveddata['skade'] = $skade->hentId() ? $skade : null;
        $this->hoveddata['leieobjekt'] = $leieobjekt && $leieobjekt->hentId() ? $leieobjekt : null;
        $this->hoveddata['bygning'] = $bygning && $bygning->hentId() ? $bygning : null;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function preHTML()
    {
        if(!parent::preHTML()) {
            return false;
        }
        /** @var Leieobjekt\Skade|null $skade */
        $skade = $this->hoveddata['skade'];
        $this->tittel = $skade ? "Redigér skademelding {$skade->hentId()}" : "Registrer skade på leieobjekt eller bygning";
        /** @var Head $htmlHead */
        $htmlHead = $this->hentRespons()->hentInnhold()->getData('head');
        if($skade) {
            $this->hentRespons()->hentInnhold()->getData('main')->setData('tittel', $skade->hentSkade());
        }
        $htmlHead
            /**
             * Importer Select2 for autocomplete
             * @link https://select2.org/
             */
            ->leggTil('links', new HtmlElement('link', [
                'rel' => "stylesheet",
                'href' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css"
            ]))
            ->leggTil('scripts', new HtmlElement('script', [
                'src' => "//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"
            ]));

        return true;
    }

    /**
     * @return \Kyegil\Leiebasen\Visning
     * @throws \Exception
     */
    public function innhold()
    {
        /** @var Leieobjekt\Skade $skade */
        $skade = $this->hoveddata['skade'];
        /** @var Leieobjekt $leieobjekt */
        $leieobjekt = $this->hoveddata['leieobjekt'];
        /** @var Bygning $bygning */
        $bygning = $this->hoveddata['bygning'];

        return $this->vis(Index::class, [
            'skade' => $skade,
            'leieobjekt' => $leieobjekt,
            'bygning' => $bygning
        ]);
    }

    /**
     * @return Visning|string
     */
    public function skript()
    {
        return $this->vis(jsIndex::class);
    }
}