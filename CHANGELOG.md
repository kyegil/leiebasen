# Changelog

Alle vesentlige endringer i leiebasen dokumenteres i denne fila.

Formatet er basert på [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

### Ikke publiserte endringer

### [0.28.0] 2025-03-10

- Kontra-transaksjoner for refunderte betalinger
- Bedret strukturering av urls

### [0.27.3] 2025-02-24

- Korrigert omdirigering av kredittvisning

### [0.27.2] 2025-02-24

- Fjernet annullering ved sletting av kreditt

### [0.27.1] 2025-02-22

- Fikset bug ved behandling av avviste efakturaer

### [0.27.0] 2025-02-21

- Fjernet feil ved manglende betalingsplan-sluttdato i leieforholdkortet
- Korrigert data-typen for EAV-egenskapen `forfall_fast_dag_i_måneden`
- Fjernet indeks-duplikater
- Mulighet for å endre andeler fortløpende

### [0.26.7] 2025-02-13

- Problemer med andel i nytt skjema

### [0.26.6] 2025-02-13

- Cron errors fixed live

### [0.26.5] 2025-02-13

- Problem med å opprette leietaker i nytt skjema

### [0.26.4] 2025-02-11

- Leieforhold-endringer og fornying flyttet til LeieforholdSkjema
- Fixed footer i oppfølging
- Mulighet for å sette giroformat i leveringsadresse-skjemaet
- Bugfiks: Object of class DateTimeImmutable could not be converted to string

### [0.26.3] 2025-02-05

- Fikset bug i leiereguleringsskjema

### [0.26.2] 2025-02-04

- Tilrettelegging for spesialtilpassede adgangsområder

### [0.26.1] 2025-01-29

- Fikset problemer i massemelding-skjema

### [0.26.0] 2025-01-27

- Fikset problem med duplikat Subject-header i error-log eposter
- Oppdatert 'Opprett leieforhold'-skjema
- Mulighet for fixed footer i nye oppslag
- Fikset feil i foreign key-link

### [0.25.2] 2024-12-19

- Fikset problemer med tom streng som `$_GET['id']`

### [0.25.1] 2024-12-12

- Tilpasset for single entry-point

### [0.25.0] 2024-12-12

- Ordnet så SMS blir sendt til telefonnummer dersom mobilnummer mangler
- CMS-sider
- Single entry point

### [0.24.2] 2024-12-11

– Fikset bug i gjeldsoversikt

### [0.24.1] 2024-11-16

– Lagt til SMS-prosessering

### [0.23.1] 2024-11-16

– Fikset bug i betalingsplan-mottak

### [0.23.0] 2024-11-14

– Lagt til kode, navn og beskrivelse i leieforhold-tabellen

### [0.22.0] 2024-11-11

– Lagt til mulighet for å la betalingsplan følge husleie
– Betalingsplan-avdrag sendes nå ut med avtalt beløp, til tross for evt tidligere ekstra innbetalinger.
- Minsket labels i ExtJs-skjema

### [0.21.0] 2024-11-07

- Forbedret UX i betalingsplaner. Det er nå lettere å holde øye med status påbetalingsplaner.
- Bugfiks: Argument $login must be of type string i innlogging
- Bugfiks: Tatt i bruk SOAP_SINGLE_ELEMENT_ARRAYS i efakturaApiClient

### [0.20.2] 2024-10-15

- Bugfikser i betalingskort
- Bugfikser i betalingsavtaler
- Bugfiks i framleieoversikt

### [0.20.1] 2024-09-30

- Bugfiks: Problemer med overføring av eFaktura via API
- Bugfiks: Hindret purring av regninger med betalingsplan

### [0.20.0] 2024-09-27

- Bugfiks: Problemer med gjennomføring av manuell leieendring
- Låsing av betalte krav
- Lagt til registrering av fremmedbeløp
- Omdøping av ukonvensjonelle tabellnavn
- Fjernet exception ved ugyldig listener
- Lagt til argumenter til postMethod() listeners
- Forbedret definering av menystruktur i mine sider

### [0.19.0] 2024-09-12

- Lagt til depositumsregistrering
- Lagt til innstillinger for betalingsplaner
- Fikset manglende visning av ExtJs radio buttons
- Fikset problemer med sletting av leie i oppsigelsestid

### [0.18.0] 2024-09-04

- Forbedringer i EAV
- Bugfix: Manglende detaljer på visning av manuelle betalinger
- Bugfix: Sikring mot tvetydig kid-fortolkning

### [0.17.3] 2024-07-12

- Bugfix: DateTimeImmutable::diff() expects parameter 1 to be DateTimeInterface, null given
- Bugfix: Undefined index: efaktura_kommunikasjon

### [0.17.2] 2024-07-12

- Fikset enkelte bugs i innbetalingsregistrering

### [0.17.1] 2024-07-08

- Forbedret registrering av FBO

### [0.17.0] 2024-07-02

- Efaktura API
- Refaktorert KID-generering
- Fikset problem med validering av personnummer
- Fikset problem med utlikning mot betaling

### [0.16.8] 2024-05-22

- Korrigert regningsobjekt-join

### [0.16.7] 2024-05-15

- Lagt til void mail-prosessor

### [0.16.6] 2024-05-14

- Fanger opp adresseendringer i avtaleteksten

### [0.16.5] 2024-05-10

- Forbedringer i utskriftsprosessor så krav _uten_ forfall binder seg til neste krav _med_ forfall

### [0.16.4] 2024-05-09

- Fikset bug som nullstiller regningsmottaker

### [0.16.3] 2024-05-07

- Fikset bug i registrering av KID-betalinger

### [0.16.2] 2024-05-01

- Lagt til terminbeløp for delkrav i leieavtaler

### [0.16.1] 2024-04-29

- Bugfix: Nye leieforhold får leiebeløp 0 i avtaleteksten

### [0.16.0] 2024-04-29

- Lagt til felt for konto-id for innbetalinger
- Div tilpasninger for modul

### [0.15.4] 2024-03-18

- Fjernet ExtJs collapsible Fieldset
- Justeringer i automatisk utskrift

### [0.15.3] 2024-03-06

- Skrivefeil i getAllowMultiple() i EAV-egenskaper
- Rettet feil i avstemmingsskjemaet
- Rettet feil ved avstemming av betalingsplan-avdrag

### [0.15.1] 2024-02-29

- Feil i validering av fødselsdato
- Fjernet skille mellom P0D og P0M i betalingsfrist_faste_krav

### [0.15.0] 2024-02-19

- Felt for øvrige beboere
- Betalingsfrist for husleie
- Mindre bugs
- Korrigering av eksport utestående per
- Korrigert feil i poeng-tabell i mine sider
- Forbedret execution time i DatabaseInstallerer

### [0.14.0] 2024-02-15

- Tabell-opppryddinger

### [0.13.2] 2024-02-14

- Fikset feil i rapporter

### [0.13.1] 2024-02-13

- Fornyet leieforhold-oppretting
- Generell opprensking for php8

### [0.13.0] 2024-02-08

- Automatisk utskrift
- Oppdatert passord-validering

### [0.12.2] 2024-02-07

- Fikset Undefined array key
- Fikset problem med AvtaleGiro-trekk for langt fram i tid

### [0.12.1] 2024-02-01

- PHP 8.2 Compatible

### [0.12.0] 2024-01-27

- Lagt til reell Krav-id

### [0.11.2] 2024-01-19

- Forbedret delbeløp-samling av betalinger
- Fikset bug i sletting av betalingsplan-avdrag

### [0.11.1] 2024-01-17

- Reparert problemer med å endre krav manuelt

### [0.11.0] 2024-01-16

- Nye brukere tildeles adgang til egne leieforhold
- Purregebyr-kravet skal ikke føres på purret regning

### [0.10.2] 2023-12-28

- Eksportforbedringer

### [0.10.1] 2023-12-23

- Lagt til mulighet for å forvalte og slette program-poeng

### [0.10.0] 2023-12-13

- Fjernet avtalegirotekst ifra betalingsvarsler
- Faste forfallsdatoern kan nå endres for hvert leieforhold

### [0.9.1] 2023-12-01

- Fikset bug i Leieforhold::hentGiroformat()

### [0.9.0] 2023-12-01

- Umiddelbart varsel ved forfalte krav
- Lagt til event despatchers for oppdrag
- Utvidet innstilling-lengden-feltet i valg-tabellen

### [0.8.1] 2023-11-22
 
- Fikset problem med å slette AvtaleGiro-trekk der hvor FBO er avsluttet
– Korrigert så betalinger i transaksjonshistorikk vises samlet, og ikke brutt opp som delbeløp

### [0.8.0] 2023-11-15

- Mulighet for å be om betalingsutsettelser

### [0.7.0] 2023-11-12

- Automatiske betalingsutsettelser

### [0.6.20] 2023-11-08

- Mulighet for å legge nedbetalingsavtaler på framtidige krav
- Mulighet for å slette hele strømkrav
- Mulighet for å begrense antall felter i tilleggsmodeller ved lasting av samlinger
- Mulighet til å sortere gjeldoversikt på kun avslutta avtaler
- Lagt til mulighet for å plugge inn i AbstraktKontroller::settRespons() og AbstraktKontroller::hentRespons()
- Lagt til bekreftelses-prompt i Ajax-forms
- Fikset problem med å opprette leieobjekter

### [0.6.19] 2023-11-01

- Fikset problem med å laste kreditkrav

### [0.6.18] 2023-10-25

- Forbedret lasting av leieforholdkort

### [0.6.17] 2023-10-17

- Fikset utskrift av gjeldsoverskrift

### [0.6.16] 2023-09-25

- Fikset problem med å legge til ny leietaker

### [0.6.15] 2023-09-06

- Oppdaterer leiehistorikk ved kontraktfornying

### [0.6.14] 2023-08-28

- Korrigert filtrering av leieforhold for automatisk regulering

### [0.6.13] 2023-08-21

- Reparert utskrift av leieavtale
- Reparert feil i leieobjekter tilgjengeligfor utleie

### [0.6.12] 2023-08-16

- Fikset bug som gjorde at ugyldige epostvedlegg blokkerte epostforsendelser

### [0.6.11] 2023-08-10

- Fanget problemer med avviste efakturaer til mottakere som har blokkert avsender

### [0.6.10] 2023-08-10

- Signaturfeil i samlingstrekk

### [0.6.9] 2023-08-08

- Fiks for bug som stoppet purreutskrifter

### [0.6.8] 2023-08-01

- Oppdatert tidsstempel for sist nedlastet backup
- Korrigert filtrering av leiereguleringsforslag

### [0.6.7] 2023-07-31

- Reparert ufullstendig backup-nedlasting

### [0.6.6] 2023-07-25

- Gjeninnsatt forsvunnet send-epost-knapp
- Reparert knapp for å sende leieregulerings-varsel

### [0.6.7] 2023-07-31

- Reparert ufullstendig backup-nedlasting

### [0.6.5] 2023-07-17

- Korrigert fellesstrøm-lenker i leieforholdkort

### [0.6.4] 2023-06-30

- Fjernet leie_var_...-refereanser fra \Leieobjekt

### [0.6.3] 2023-06-27

- Fjernet udefinert funksjon i SkadeKategorisamling
- Oppdatert bruk av utgått metode gjengiAvtaletekst

### [0.6.2] 2023-06-27

- Fjernet unchecked value for checkboxgroups

### [0.6.1] 2023-06-26

- Epost-melding om nye søknader

### [0.6.0] 2023-06-25

- Ne leiejusterings-funksjoner

### [0.5.3] 2023-06-14

- Utbedring av mindre bugs ifra logg

### [0.5.2] 2023-06-14

- Søknadsarkiv for inaktive søknader
- Utbedring av søknadskort

### [0.5.1] 2023-06-05

- Fikset bug ved oppretting av profil ifra Drift

### [0.5.0] 2023-05-30

- Prosessering av søknader

### [0.4.8] 2023-05-26

- Lagt til instruksjoner for søknadshåndtering

### [0.4.7] 2023-05-26

- Utbedret ufullstendig lagring av nye leietakere i leieforhold

### [0.4.6] 2023-05-12

- Fjernet konstanten NETS_ID
- Reparert søkefunksjonen i krav-oversikten
- Reparert bla-funksjonen i efaktura-oversikten

### [0.4.5] 2023-05-10

- Fjernet overflødig utskriftsdato fra krav-tabellen

### [0.4.4] 2023-04-26

- Fikset sortering-kompabilitetsissue mellom ExtJS og DataTables

### [0.4.3] 2023-04-08

- Stikkord lagt til søknader

### [0.4.1] 2023-03-21

- eFaktura 2.0 (Ja Takk Til Alle)

### [0.4.0] 2023-03-17

- eFaktura 2.0 (Ja Takk Til Alle)

### [0.3.24] 2023-02-21

- Fikset problem med å registrere nye betalinger

### [0.3.23] 2023-02-18

- Korrigert _oppdatert dato_ på søknadsregistrering

### [0.3.22] 2023-01-17

- Oppdatert eksportskjema

### [0.3.21] 2023-01-17

- Korrigert redirect ved betalingsregistrering

### [0.3.20] 2022-12-29

- Fellesstrøm kreves nå ikke inn ved fordeling dersom den skal betales med faste tillegg
- Lagt til anleggsnummer igjen på fellesstrøm-krav

### [0.3.19] 2022-12-22

- Behandlet problem med AvtaleGiroer med bank-varsling som sendes for sent
- Slettet utskriftsdato på avviste efakturaer
- Fikset problem med sletting av oppsagte eFaktura-avtaler
- Korrigert AvtaleGiro-informasjon på regninger i hht frister

### [0.3.18] 2022-12-21

- Lazy loading av advarsler
- Lazy-loading av valg
- Flyttet developer_email config namespace
- Lagt til email_dev_copy config

### [0.3.17] 2022-12-16

- Lagt til epost-modell
- Åpnet for overstyring av visning- og modell-klasser
- Flyttet \Leiebase til \Kyegil\Leiebasen\Leiebase
- Lagt til cron-events dispatches
- Lazy-loading av valg

### [0.3.16] 2022-12-13

- Rettet manglende funsjon hentLinkKnapp() i Drift-visning
- Forbedret \Leiebase::array_merge_recursive_distinct() så den fletter ikke associative arrays

### [0.3.15] 2022-12-05

- Oppdatert kode i innstillinger

### [0.3.14] 2022-12-05

- Tatt i bruk drift-visning med ulike templates, for å muligjøre parallell utvikling i ExtJs og ren HTML

### [0.3.13] 2022-11-28

- Behandlet problem med at kreditt ikke slettes fullstendig
- Lagt til varsel om innkomne forslag til betalingsplaner i oppfølging
- Transaksjonstype korrigert for AvtaleGiro-trekk
- Korrigert frister for AvtaleGiro-trekk

### [0.3.12] 2022-11-24

- Korrigert datoer brukt ved beregning av endrefrist hos NETS
- Oppdatert README.md

### [0.3.11] 2022-11-23

- Forbedringer av AvtaleGiro frister

### [0.3.10] 2022-11-22

- Flyttet rester av opplastede bilder ut av repo

### [0.3.9] 2022-11-17

- Fikset kritisk bug som fjernet filtre når da lagret ifra ei samling
- Lagt til et siste forsøk på å sende gjenglemte filer til NETS
- Tatt i bruk nye konfigurasjoner for NETS-tilkopling

### [0.3.8] 2022-11-15

 - Kommentert ut custom error handler

### [0.3.7] 2022-11-15

 - Behandlet problem ved splitting av transaksjoner

### [0.3.6] 2022-11-14

 - Inkludert lisens i composer.json

### [0.3.5] 2022-11-14

 - Første offentlige versjon