<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\txt\kostnadsdeling\fordelingsnøkkel\Element $this
 * @var string $andeler
 * @var string $prosentsats
 * @var boolean $følgerLeieobjekt
 * @var boolean $erBofellesskap
 * @var int $leieobjektId
 * @var string $leieobjektbeskrivelse
 * @var int $leieforholdId
 * @var string $leieforholdbeskrivelse
 * @var string $beboerliste
 */
?>
– Et manuelt beregnet beløp <?php
if($følgerLeieobjekt):?><?php
    if($erBofellesskap):?>fordeles mellom leieforholdene i <?php
        echo $leieobjektbeskrivelse;?><?php
    else:?>betales av <?php echo $leieobjektbeskrivelse;?><?php
    endif;?> (<?php echo trim($beboerliste) ?: 'Kreves i dag ikke inn';?>)<?php
else:?>betales av <?php echo $leieforholdbeskrivelse;?><?php endif;?>

