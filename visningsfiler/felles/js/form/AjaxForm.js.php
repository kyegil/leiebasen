<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\js\form\AjaxForm $this
 * @var string $formId
 * @var string $reCaptchaSiteKey
 * @var string $method
 * @var string $enctype
 */
?>
leiebasen.kjerne = leiebasen.kjerne || {};
leiebasen.kjerne.ajaxForms = leiebasen.kjerne.ajaxForms || [];
leiebasen.kjerne.ajaxFormConfirm = leiebasen.kjerne.ajaxFormConfirm || [];

/**
 * Bekreftelse for at skjemaet skal sendes
 * Returner true for å sende skjemaet, false for å avbryte
 *
 * @param {object} form
 * @param {FormData} formData
 * @returns {boolean}
 */

/**
 * Array av funksjoner som alle må returnere true for at skjemaet skal sendes
 *
 * @type {(function(FormData): boolean)[]}
 */
leiebasen.kjerne.ajaxFormConfirm['<?php echo $formId;?>'] = [];
/**
 * Behandle server-responsen
 * Returner true for å fortsette å vise resultatet, false for å avbryte videre behandling
 *
 * @param {jQuery} _form
 * @param {object} _resultat
 * @returns {boolean}
 */
leiebasen.kjerne.ajaxFormCallBack = function(_form, _resultat) {
    return true;
}
$(function() {
    /** @type {jQuery} */
    let form = $('#<?php echo $formId;?>')[0];
    /** @type {*|jQuery.fn.init|HTMLElement} */
    let formMessages = $('#skjemarespons-<?php echo $formId;?>');
    let buttonClickedName = '';
    let buttonClickedValue = '';

    $("#<?php echo $formId;?> button[type = 'submit']").click(function(){
        buttonClickedName = $(this).attr("name");
        buttonClickedValue = $(this).attr("value");
    })

    $(form).submit(function(event) {
        $(formMessages).removeClass('success').removeClass('alert-success');
        $(formMessages).removeClass('error').removeClass('alert-danger');
        // Stop the browser from submitting the form.
        event.preventDefault();

        let formData = new FormData(form);
        if (buttonClickedName) {
            formData.append(buttonClickedName, buttonClickedValue);
        }
        let getParams = {};

        for (var key in leiebasen.kjerne.ajaxFormConfirm['<?php echo $formId;?>']) {
            if(!leiebasen.kjerne.ajaxFormConfirm['<?php echo $formId;?>'][key](form)) {
                return;
            }
        }

        let showError = function(text) {
            $(formMessages).removeClass('success').removeClass('alert-success');
            $(formMessages).addClass('error').addClass('alert-danger');
            $(formMessages).html(text);
            $(formMessages)[0].scrollIntoView({
                behavior: "smooth", // or "auto" or "instant"
                block: "start" // or "end"
            });
        }

<?php if ($reCaptchaSiteKey): ?>
        grecaptcha.ready(function() {
            grecaptcha.execute('<?php echo $reCaptchaSiteKey; ?>', {action: 'submit'}).then(function(token) {
                formData.append('g-recaptcha-response', token);
<?php endif; ?>
                <?php if($method == 'GET'):?>

                for (let [key, value] of formData) {
                    getParams[key] = value;
                }
                $.get($(form).attr('action'), getParams)
                <?php else:?>

                $.ajax({
                    type: '<?php echo $method;?>',
                    enctype: '<?php echo $enctype;?>',
                    processData: false,
                    contentType: false,
                    url: $(form).attr('action'),
                    data: formData
                })
                <?php endif;?>
                .done(function(response) {
                    try {
                        let resultat = response;
                        if(typeof response === 'string') resultat = JSON.parse(response);
                        if (leiebasen.kjerne.ajaxFormCallBack(form, resultat)) {
                            if(resultat.success) {
                                $(formMessages).removeClass('error').removeClass('alert-danger');
                                if(resultat.msg) {
                                    $(formMessages).addClass('success').addClass('alert-success');
                                }
                                $(formMessages).html(resultat.msg);
                            }
                            else {
                                showError(resultat.msg);
                            }
                            if(typeof (resultat.url) != 'undefined') {
                                setTimeout(function () {window.location = resultat.url;}, 5000);
                            }
                        }
                    }
                    catch(error) {
                        showError(error.message);
                    }
                })
                .fail(function(data) {
                    // Make sure that the formMessages div has the 'error' class.
                    $(formMessages).removeClass('success').removeClass('alert-success');
                    $(formMessages).addClass('error').addClass('alert-danger');

                    // Set the message text.
                    if (data.responseText !== '') {
                        $(formMessages).text(data.responseText);
                    } else {
                        $(formMessages).text('Oops! An error occured and your message could not be sent.');
                    }
                });
<?php if ($reCaptchaSiteKey): ?>
            });
        });
<?php endif; ?>
    });
    // TODO: The rest of the code will go here...
});
