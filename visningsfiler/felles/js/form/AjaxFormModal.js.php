<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\js\form\AjaxForm $this
 * @var string $formId
 * @var string $reCaptchaSiteKey
 * @var string $method
 * @var string $enctype
 */
?>
leiebasen.kjerne = leiebasen.kjerne || {};
leiebasen.kjerne.ajaxForms = leiebasen.kjerne.ajaxForms || [];
leiebasen.kjerne.ajaxFormConfirm = leiebasen.kjerne.ajaxFormConfirm || [];

/**
 * Bekreftelse for at skjemaet skal sendes
 * Returner true for å sende skjemaet, false for å avbryte
 *
 * @param {object} form
 * @param {FormData} formData
 * @returns {boolean}
 */

/**
 * Array av funksjoner som alle må returnere true for at skjemaet skal sendes
 *
 * @type {(function(FormData): boolean)[]}
 */
leiebasen.kjerne.ajaxFormConfirm['<?php echo $formId;?>'] = [];
/**
 * Behandle server-responsen
 * Returner true for å fortsette å vise resultatet, false for å avbryte videre behandling
 *
 * @param {jQuery} _form
 * @param {object} _resultat
 * @returns {boolean}
 */
leiebasen.kjerne.ajaxFormCallBack = function(_form, _resultat) {
    return true;
}
$(function() {
    /** @type {jQuery} */
    let form = $('#<?php echo $formId;?>')[0];
    /** @type {*|jQuery.fn.init|HTMLElement} */
    let formMessages = $('#skjemarespons-<?php echo $formId;?>');
    let $modal = $('#main-modal');
    let buttonClickedName = '';
    let buttonClickedValue = '';

    $("#<?php echo $formId;?> button[type = 'submit']").click(function(){
        buttonClickedName = $(this).attr("name");
        buttonClickedValue = $(this).attr("value");
    })

    $(form).submit(function(event) {
        // Stop the browser from submitting the form.
        event.preventDefault();

        $modal.find(".modal-title").remove();
        $modal.find(".modal-body").remove();
        $modal.find(".modal-footer").remove();
        $modal.find(".modal-header").removeClass().addClass('modal-header');
        let modalFooter = '<div class="modal-footer">' +
            '<button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>'
        '</div>';
        $modal.find(".modal-content").append(modalFooter);
        $modal.off("hidden.bs.modal");

        let formData = new FormData(form);
        if (buttonClickedName) {
            formData.append(buttonClickedName, buttonClickedValue);
        }
        let getParams = {};

        for (var key in leiebasen.kjerne.ajaxFormConfirm['<?php echo $formId;?>']) {
            if(!leiebasen.kjerne.ajaxFormConfirm['<?php echo $formId;?>'][key](form)) {
                return;
            }
        }

        let showError = function(text, title) {
            let modalBody = '<div class="modal-body">' +
                text +
                '</div>';
            let modalTitle = '';
            if(title) {
                modalTitle = '<h5 class="modal-title" id="main-modalLabel">' +
                    title +
                    '</h5>';
            }
            $modal.find(".modal-header button").before(modalTitle);
            $modal.find(".modal-header").addClass(' bg-danger').after(modalBody);
            $modal.modal('show');
        }

<?php if ($reCaptchaSiteKey): ?>
        grecaptcha.ready(function() {
            grecaptcha.execute('<?php echo $reCaptchaSiteKey; ?>', {action: 'submit'}).then(function(token) {
                formData.append('g-recaptcha-response', token);
<?php endif; ?>
                <?php if($method == 'GET'):?>

                for (let [key, value] of formData) {
                    getParams[key] = value;
                }
                $.get($(form).attr('action'), getParams)
                <?php else:?>

                $.ajax({
                    type: '<?php echo $method;?>',
                    enctype: '<?php echo $enctype;?>',
                    processData: false,
                    contentType: false,
                    url: $(form).attr('action'),
                    data: formData
                })
                <?php endif;?>
                .done(function(response) {
                    try {
                        let resultat = response;
                        if(typeof response === 'string') resultat = JSON.parse(response);
                        if (leiebasen.kjerne.ajaxFormCallBack(form, resultat)) {
                            if(typeof (resultat.url) != 'undefined') {
                                $modal.on('hidden.bs.modal', function () {
                                    window.location = resultat.url;
                                });
                            }
                            if(resultat.success) {
                                resultat.msg = resultat.msg || '';
                                let modalTitle = '';
                                let modalBody = '<div class="modal-body">' +
                                    resultat.msg +
                                    '</div>';
                                if(resultat.title) {
                                    modalTitle = '<h5 class="modal-title" id="main-modalLabel">' +
                                        resultat.title +
                                        '</h5>';
                                }
                                $modal.find(".modal-header button").before(modalTitle);
                                $modal.find(".modal-header").addClass(' bg-secondary').after(modalBody);
                                $modal.modal('show');
                            }
                            else {
                                showError(resultat.msg);
                            }
                        }
                    }
                    catch(error) {
                        showError(error.message);
                    }
                })
                .fail(function(jqXHR) {
                    // Make sure that the formMessages div has the 'error' class.
                    $(formMessages).removeClass('success').removeClass('alert-success');
                    $(formMessages).addClass('error').addClass('alert-danger');

                    let melding = jqXHR.responseText !== '' ? jqXHR.responseText : 'Det har oppstått en ukjent feil';
                    showError(melding);
                });
<?php if ($reCaptchaSiteKey): ?>
            });
        });
<?php endif; ?>
    });
});
