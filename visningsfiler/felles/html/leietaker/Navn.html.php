<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\leietaker\Navn $this
 * @var string $leietakerId
 * @var string $personId
 * @var string|false $slettet
 * @var string $navn
 * @var boolean $visLenke
 */
?><?php if($visLenke):?><a href="index.php?oppslag=personadresser_kort&id=<?php echo $personId;?>" title="Klikk for adresse"><?php endif;?><?php if($slettet):?><del><?php endif;?><?php echo $navn;?><?php if($slettet):?></del><?php endif;?><?php if($visLenke):?></a><?php endif;?>