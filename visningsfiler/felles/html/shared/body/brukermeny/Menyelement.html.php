<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\shared\body\brukermeny\Menyelement $this
 * @var string $brukernavn
 * @var int $menyNivå
 * @var string $id
 * @var string $text
 * @var string $url
 * @var bool $active
 * @var string $symbol
 * @var string $extraClasses
 * @var string $items
 * @var bool $nyGruppe
 */
    $class = implode(' ', [$active ? 'active' : '',$nyGruppe ? 'group' : '']);
?><?php switch ($menyNivå):
    default: // Alle nivåer behandles likt ?>
    <li class="level-<?php echo $menyNivå . ' ' . $class;?>">
        <?php if(trim($items)):?>
            <a
                href="#leiebasen-brukermeny-<?php echo $id;?>"
                data-toggle="collapse"
                aria-expanded="false"
                class="dropdown-toggle"
                role="button"
                aria-controls="leiebasen-brukermeny-<?php echo $id;?>
    "><i class="<?php echo $extraClasses;?>"></i><?php echo $text;?></a><ul
            id="leiebasen-brukermeny-<?php echo $id;?>"
            class="collapse list-unstyled"><?php echo $items?></ul>
        <?php else:?>
            <a
                <?php echo trim($url) ? ('href="' . $url . '"') : '';?>><i class="<?php echo $extraClasses;?>"></i>
            <?php echo $text;?></a>
        <?php endif;?>
        </li>
    <?php break;
endswitch;