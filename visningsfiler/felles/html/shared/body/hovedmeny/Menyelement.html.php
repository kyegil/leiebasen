<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\shared\body\hovedmeny\Menyelement $this
 * @var string $brukernavn
 * @var int $menyNivå
 * @var string $id
 * @var string $text
 * @var string $url
 * @var bool $active
 * @var bool $nåværende
 * @var string $symbol
 * @var string $extraClasses
 * @var string $items
 * @var bool $nyGruppe
 */
    $class = implode(' ', [$nåværende ? 'nåværende' : '', $active ? 'active' : '', $nyGruppe ? 'group' : '']);
?><?php switch ($menyNivå):
    default: // Alle nivåer behandles likt ?>
    <li class="level-<?php echo $menyNivå . ' ' . $class;?>">
        <?php if(trim($items)):?>
            <a
                href="#leiebasen-hovedmeny-<?php echo $id;?>"
                data-toggle="collapse"
                aria-expanded="false"
                class="dropdown-toggle"
                role="button"
                aria-controls="leiebasen-hovedmeny-<?php echo $id;?>
    "><i class="menu-item-symbol <?php echo $extraClasses;?>"></i><span
                        class="menu-item-text"><?php echo $text;?></span></a><ul
            id="leiebasen-hovedmeny-<?php echo $id;?>"
            class="collapse list-unstyled"><?php echo $items?></ul>
        <?php else:?>
            <a <?php echo trim($url) ? ('href="' . $url . '"') : '';?>><i
                        class="menu-item-symbol <?php echo $extraClasses;?>"></i><span
                        class="menu-item-text"><?php echo $text;?></span></a>
        <?php endif;?>
        </li>
    <?php break;
endswitch;