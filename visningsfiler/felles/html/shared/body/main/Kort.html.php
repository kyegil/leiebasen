<?php
    /**
     * Leiebasen Visningsblokk
     *
     * @var \Kyegil\Leiebasen\Visning\felles\html\shared\body\main\Kort $this
     * @var string $id
     * @var string $header
     * @var string $innhold
     * @var string $knapper
     */
?><div class="kort"
    <?php echo trim($id) ? (' id="' . $id . '"') : '';?>
>
    <?php if(trim($header)):?>
        <div class="kort-header"><?php echo $header;?></div>
    <?php endif;?>
    <div class="kort-innhold"><?php echo $innhold;?></div>
    <div class="knapprad"><?php echo $knapper;?></div>
</div>