<?php
    /**
     * Leiebasen Visningsblokk
     *
     * @var \Kyegil\Leiebasen\Visning\felles\html\shared\body\main\ReturiElement $this
     * @var string $url
     * @var string $tittel
     * @var boolean $rot
     * @var boolean $current
     */
?><a <?php echo $current ? ' class="current"' : ''; ?><?php echo $rot ? ' class="root"' : ''; ?> title="<?php echo $tittel; ?>" href="<?php echo $url; ?>"><span><?php echo $tittel; ?></span></a>