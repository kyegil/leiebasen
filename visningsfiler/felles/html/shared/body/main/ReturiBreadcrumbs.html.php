<?php
    /**
     * Leiebasen Visningsblokk
     *
     * @var \Kyegil\Leiebasen\Visning\felles\html\shared\body\main\ReturiBreadcrumbs $this
     * @var \Kyegil\ViewRenderer\ViewArray $crumbs
     */
?><div class="breadcrumbs">
    <?php echo $crumbs;?>
</div>
