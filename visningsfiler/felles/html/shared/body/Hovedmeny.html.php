<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\shared\body\Hovedmeny $this
 * @var string $brukernavn
 * @var string $items
 * @var string $logo
 * @var string $versjon
 */
?><nav class="sidebar">

    <!-- close sidebar menu -->
    <div class="dismiss">
        <i class="fas fa-arrow-left"></i>
    </div>

    <div class="logo">
        <?php echo $logo;?>
    </div>

    <ul class="list-unstyled menu-elements">
        <?php echo $items;?>
    </ul>

    <div class="to-top">
        <a class="btn btn-primary btn-customized-3" href="#" role="button">
            <i class="fas fa-arrow-up"></i> Til topps
        </a>
    </div>

<!--    <div class="dark-light-buttons">-->
<!--        <a class="btn btn-primary btn-customized-4 btn-customized-dark" href="#" role="button">Mørk modus</a>-->
<!--        <a class="btn btn-primary btn-customized-4 btn-customized-light" href="#" role="button">Lys modus</a>-->
<!--    </div>-->
    <footer>
        <?php echo $versjon;?>
    </footer>

</nav>