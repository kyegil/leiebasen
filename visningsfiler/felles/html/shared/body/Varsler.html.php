<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\shared\body\Varsler $this
 * @var string $items
 */
?><div class="varsler border-x <?php echo trim($items) ? 'utløst' : '';?>">
    <div class="flex varselmenyknapp" type="button" aria-label="Varselmeny">
        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" viewBox="0 0 16 16" class="">
            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 5.333a4 4 0 1 0-8 0c0 4.667-2 6-2 6h12s-2-1.333-2-6zM9.153 14a1.333 1.333 0 0 1-2.306 0"></path>
        </svg>
    </div>

    <?php if (trim($items)):?>
    <nav class="meny varselmeny" id="varselmeny" aria-hidden="false">

        <div class="menyhode">
<!--            <h3>Varsler</h3>-->
        </div>

        <ul class="list-unstyled menu-elements" tabindex="0">
            <?php echo $items;?>
        </ul>
        <div class="menyfot"></div>
    </nav>
    <?php endif;?>
</div>

