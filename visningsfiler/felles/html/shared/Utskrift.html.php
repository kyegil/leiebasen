<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\shared\Utskrift $this
 * @var string $tittel
 * @var string $innhold
 * @var string $scriptTag
 */
?><!DOCTYPE html>
<html lang="no">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title><?php echo htmlspecialchars($tittel);?></title>
    <link rel="stylesheet" type="text/css" href="/pub/css/print.css">
</head>

<body class="dataload">
<?php echo $innhold;?>
</body>
</html>
<?php echo $scriptTag;?>