<?php
/**
 * Leiebasen Html Data Cell
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\table\Td $this
 * @var string $class
 * @var string $style
 * @var string $dataId
 * @var string $dataName
 * @var string $dataValue
 * @var string $dataSort
 * @var string $dataFilter
 * @var string|\Kyegil\ViewRenderer\ViewInterface $content
 */
?><td
    <?php if(trim($class)):?>class="<?php echo $class;?>"<?php endif;?>
    <?php if(trim($style)):?>style="<?php echo $style;?>"<?php endif;?>
    <?php if(trim($dataId)):?>data-id="<?php echo $dataId;?>"<?php endif;?>
    <?php if(trim($dataName)):?>data-name="<?php echo $dataName;?>"<?php endif;?>
    <?php if(trim($dataValue)):?>data-value="<?php echo $dataValue;?>"<?php endif;?>
    <?php if(trim($dataSort)):?>data-sort="<?php echo $dataSort;?>"<?php endif;?>
    <?php if(trim($dataFilter)):?>data-filter="<?php echo $dataFilter;?>"<?php endif;?>
><?php echo $content;?></td>