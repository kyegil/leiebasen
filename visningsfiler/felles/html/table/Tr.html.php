<?php
/**
 * Leiebasen Html Table line
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\table\Tr $this
 * @var string $class
 * @var string $style
 * @var string $dataId
 * @var string|\Kyegil\ViewRenderer\ViewInterface $cells
 */
?><tr
    <?php if(trim($class)):?>class="<?php echo $class;?>"<?php endif;?>
    <?php if(trim($style)):?>style="<?php echo $style;?>"<?php endif;?>
    <?php if(trim($dataId)):?>data-id="<?php echo $dataId;?>"<?php endif;?>
>
    <?php echo $cells;?>
</tr>
