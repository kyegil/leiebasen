<?php
/**
 * Leiebasen Html Table Head
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\table\TableHead $this
 * @var string $class
 * @var string $style
 * @var string|\Kyegil\ViewRenderer\ViewInterface $rows
 */
?><thead
    <?php if(trim($class)):?>class="<?php echo $class;?>"<?php endif;?>
    <?php if(trim($style)):?>style="<?php echo $style;?>"<?php endif;?>
><?php echo $rows;?>
</thead>
