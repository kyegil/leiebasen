<?php
/**
 * Leiebasen Html Table Footer
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\table\TableFoot $this
 * @var string $class
 * @var string $style
 * @var string|\Kyegil\ViewRenderer\ViewInterface $rows
 */
?><tfoot
    <?php if(trim($class)):?>class="<?php echo $class;?>"<?php endif;?>
    <?php if(trim($style)):?>style="<?php echo $style;?>"<?php endif;?>
><?php echo $rows;?>
</tfoot>
