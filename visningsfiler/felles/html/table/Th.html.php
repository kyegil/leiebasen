<?php
/**
 * Leiebasen Html Data Header Cell
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\table\Th $this
 * @var string $class
 * @var string $style
 * @var string $scope
 * @var string $dataName
 * @var string|\Kyegil\ViewRenderer\ViewInterface $content
 */
?><th
    <?php if(trim($class)):?>class="<?php echo $class;?>"<?php endif;?>
    <?php if(trim($style)):?>style="<?php echo $style;?>"<?php endif;?>
    <?php if(trim($scope)):?>style="<?php echo $scope;?>"<?php endif;?>
    <?php if(trim($dataName)):?>data-name="<?php echo $dataName;?>"<?php endif;?>
><?php echo $content;?></th>