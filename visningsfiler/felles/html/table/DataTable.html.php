<?php
/**
 * Leiebasen DataTables Table
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\table\DataTable $this
 * @var string $tableId
 * @var string $caption
 * @var string|\Kyegil\ViewRenderer\ViewInterface $tableHead
 * @var string|\Kyegil\ViewRenderer\ViewInterface $tableBody
 * @var string|\Kyegil\ViewRenderer\ViewInterface $tableFoot
 * @var string $class
 * @var string $style
 * @var string|\Kyegil\ViewRenderer\ViewInterface $dataTablesConfigJs
 */
?><table
    id="<?php echo $tableId;?>"
    <?php if(trim($class)):?>class="<?php echo $class;?>"<?php endif;?>
    <?php if(trim($style)):?>style="<?php echo $style;?>"<?php endif;?>
>
    <?php if(trim($caption)):?>
        <caption><?php echo $caption;?></caption>
    <?php endif;?>
    <?php echo $tableHead;?>
    <?php echo $tableBody;?>
    <?php echo $tableFoot;?>
</table>
<script>
    leiebasen.datatables = leiebasen.datatables || {};
    $(document).ready( function () {
        leiebasen.datatables.<?php echo $tableId;?> = $('#<?php echo $tableId;?>').DataTable(<?php echo $dataTablesConfigJs;?>);
    } );
</script>