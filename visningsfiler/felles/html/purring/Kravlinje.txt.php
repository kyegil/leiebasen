<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\purring\Kravlinje $this
 * @var string $tekst
 * @var string $forfallsdato
 * @var string $beløp
 * @var int $linjeNummer
 * @var int $antallLinjer
 */
?>

*  <?php echo $forfallsdato;?> <?php echo $tekst;?>: <?php echo strip_tags(html_entity_decode($beløp));?>
