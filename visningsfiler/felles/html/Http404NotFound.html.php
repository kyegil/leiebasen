<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\http404NotFound $this
 * @var string $h1
 * @var string $tekst
 */
?><h1><?php echo $h1;?></h1>
<?php echo $tekst;?>