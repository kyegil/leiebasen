<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\person\Adressefelt $this
 * @var string $navn
 * @var string $adresse1
 * @var string $adresse2
 * @var string $postnr
 * @var string $poststed
 * @var string $land
 */
?><div class="adressefelt">
    <?php if($navn):?>
        <?php echo $navn;?><br>
    <?php endif;?>
    <?php if($adresse1):?>
        <?php echo $adresse1;?><br>
    <?php endif;?>
    <?php if($adresse2):?>
        <?php echo $adresse2;?><br>
    <?php endif;?>
    <?php echo $postnr;?> <?php echo $poststed;?><br>
    <?php echo $land;?>
</div>
