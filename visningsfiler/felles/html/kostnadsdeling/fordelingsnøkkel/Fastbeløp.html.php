<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling\fordelingsnøkkel\Element $this
 * @var string $andeler
 * @var string $prosentsats
 * @var boolean $følgerLeieobjekt
 * @var boolean $erBofellesskap
 * @var int $leieobjektId
 * @var string $leieobjektbeskrivelse
 * @var int $leieforholdId
 * @var string $leieforholdbeskrivelse
 * @var string $beboerliste
 */
?><li class="element fastbeløp">
    Et direkte oppgitt beløp
    <?php if($følgerLeieobjekt):?>
        <?php if($erBofellesskap):?>
            fordeles mellom leieforholdene i <?php echo $leieobjektbeskrivelse;?>
        <?php else:?>
            betales av <?php echo $leieobjektbeskrivelse;?>
        <?php endif;?>
        <div class="beboerliste">(<?php echo trim($beboerliste) ?: "Kreves i dag ikke inn";?>)</div>
    <?php else:?>
        betales av <?php echo $leieforholdbeskrivelse;?>
    <?php endif;?>
</li>