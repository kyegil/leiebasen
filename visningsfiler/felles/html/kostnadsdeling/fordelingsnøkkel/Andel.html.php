<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling\fordelingsnøkkel\Element $this
 * @var string $andeler
 * @var string $prosentsats
 * @var boolean $følgerLeieobjekt
 * @var boolean $erBofellesskap
 * @var int $leieobjektId
 * @var string $leieobjektbeskrivelse
 * @var int $leieforholdId
 * @var string $leieforholdbeskrivelse
 * @var string $beboerliste
 */
?><li class="element andel">
    <?php echo $andeler;?>&nbsp;<?php echo $andeler == 1 ? "del" : "deler";?>
    <?php if($følgerLeieobjekt):?>
        <?php if($erBofellesskap):?>
        betales av hvert leieforhold i <?php echo $leieobjektbeskrivelse;?>
        <?php else:?>
        betales av <?php echo $leieobjektbeskrivelse;?>
        <?php endif;?>
        <div class="beboerliste">(<?php echo trim($beboerliste) ?: "Kreves i dag ikke inn";?>)</div>
    <?php else:?>
        betales av <?php echo $leieforholdbeskrivelse;?>
    <?php endif;?>
</li>