<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling\Fordelingsnøkkel $this
 * @var \Kyegil\Leiebasen\Visning|string $elementer
 */
?><ul class="fordelingsnøkkel"><?php echo $elementer;?></ul>