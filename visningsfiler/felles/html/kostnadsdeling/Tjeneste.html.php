<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling\Tjeneste $this
 * @var string $tjenesteId
 * @var string $navn
 * @var string $beskrivelse
 * @var string $leverandør
 * @var string $avtalereferanse
 */
?><h1><?php echo $navn;?></h1>
<div><?php echo $leverandør;?>
    <?php if ($avtalereferanse):?><?php echo $avtalereferanse;?><?php endif;?>
</div>
<div><?php echo $beskrivelse;?></div>