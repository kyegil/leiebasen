<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling\Fakturaliste $this
 * @var string|\Kyegil\Leiebasen\Visning $regningsliste
 */
?><table class="rutenett">
    <caption>Regninger på strømanlegg</caption>
    <thead>
        <tr>
            <th>Faktura nr.</th>
            <th class="min-width450">Fra</th>
            <th class="min-width600">Til</th>
            <th class="min-width700">Forbruk</th>
            <th>Beløp</th>
            <th class="min-width700">Fordelt</th>
        </tr>
    </thead>
    <tbody>
        <?php echo $regningsliste;?>
    </tbody>
</table>