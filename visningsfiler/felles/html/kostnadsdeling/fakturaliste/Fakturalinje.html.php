<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\kostnadsdeling\fakturaliste\Fakturalinje $this
 * @var string $id
 * @var string $fakturanummer
 * @var string $beløp
 * @var string $anleggsnummer
 * @var string $fraDato
 * @var string $tilDato
 * @var string $termin
 * @var string $forbruk
 * @var boolean $beregnet
 * @var boolean $fordelt
 */
?><tr>
    <td><span class="kolonnelabel">Fakturanummer</span><a title= "Åpne faktura <?php echo $fakturanummer;?>" href="index.php?oppslag=fs_regning&id=<?php echo $id;?>"><?php echo $fakturanummer;?></a></td>
    <td class="min-width450"><?php echo $fraDato;?></td>
    <td class="min-width600"><?php echo $tilDato;?></td>
    <td class="min-width700 høyrejustert"><?php echo $forbruk;?></td>
    <td class="beløp"><span class="kolonnelabel">Beløp</span><?php echo $beløp;?></td>
    <td class="min-width700 sentrert"><?php echo $fordelt ? '✔︎' : '';?></td>
</tr>