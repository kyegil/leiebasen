<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\leieforhold\Leietakerfelt $this
 * @var \Kyegil\ViewRenderer\ViewInterface|string $leietakere
 */
?><div class="leietakerfelt">
    <?php echo $leietakere;?>
</div>
