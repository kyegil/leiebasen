<?php
/**
 * Leiebasen Visningsmal
 *
 * @var Kontraktliste $this
 * @var string|Visning $kontrakter
 */

use Kyegil\Leiebasen\Visning;
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\Kontraktliste;

?><ul class="kontraktliste">
    <?php echo $kontrakter;?>
</ul>