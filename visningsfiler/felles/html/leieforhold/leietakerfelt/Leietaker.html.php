<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\leieforhold\Leietakerfelt\Leietaker $this
 * @var string $navn
 * @var string $url
 * @var string $fødselsnummer
 * @var string $fødselsdato
 */
?><?php if($url):?>
<a class="adressekortlenke" title="Klikk her for å åpne adressekortet" href="<?php echo addslashes($url);?>">
    <?php echo $navn;?>
</a>
<?php else:?>
    <?php echo $navn;?>
<?php endif;?>
<?php if($fødselsnummer):?>
    <span class="fødselsnummer">f.nr. <?php echo $fødselsnummer;?></span>
<?php elseif($fødselsdato):?>
    <span class="fødselsdato">f.nr. <?php echo $fødselsdato;?></span>
<?php endif;?>
<br>
