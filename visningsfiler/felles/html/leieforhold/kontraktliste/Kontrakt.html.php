<?php
/**
 * Leiebasen Visningsmal
 *
 * @var Kontrakt $this
 * @var int $kontraktNr
 * @var string $kontraktPersoner
 * @var string $fraDato
 * @var string $tilDato
 */
use Kyegil\Leiebasen\Visning\felles\html\leieforhold\kontraktliste\Kontrakt;

?><li>Avtale nr. <?php echo $kontraktNr;?><?php if(trim($kontraktPersoner)):?> med <?php echo $kontraktPersoner;?><?php endif;?> fra <?php echo $fraDato;?><?php if(trim($tilDato)):?> til <?php echo $tilDato;?><?php endif;?></li>