<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\leieforhold\Navn $this
 * @var string $navn
 * @var string $id
 * @var boolean $visLenke
 */
?><?php if($visLenke):?><a href="index.php?oppslag=leieforhold&id=<?php echo $id;?>" title="Klikk for å gå til leieforholdet"><?php endif;?><?php echo $navn;?><?php if($visLenke):?></a><?php endif;?>