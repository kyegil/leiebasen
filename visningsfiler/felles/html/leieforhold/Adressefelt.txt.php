<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\leieforhold\Adressefelt $this
 * @var string $adresse1
 * @var string $adresse2
 * @var string $postnr
 * @var string $poststed
 * @var string $land
 */
?><?php if(trim($adresse1)):?>
<?php echo trim($adresse1);?>

<?php endif; if($adresse2):?>
<?php echo trim($adresse2);?>

<?php endif;?><?php echo trim($postnr) . ' ' . trim($poststed);?>

<?php echo $land;?>