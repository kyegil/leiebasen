<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\Select $this
 * @var string $id
 * @var string $name
 * @var string $label
 * @var string $class
 * @var bool $required
 * @var bool $disabled
 * @var bool $multiple
 * @var bool $hidden
 * @var \Kyegil\ViewRenderer\ViewInterface|string $optionFields
 */
?><div <?php echo ($class ? "class=\"{$class}\" " : '') . ($hidden ? 'style="display: none;" ' : '');?>>
    <?php if (trim($label)):?>
        <label <?php echo trim($id) ? ('for="' . $id . '"') : '';?>><?php echo $label;?>:<?php if (trim($id)):?></label><?php endif;?>
    <?php endif;?>
    <select
        <?php if (trim($id)):?>
            id="<?php echo $id;?>"
        <?php endif;?>
            name="<?php echo $name;?>"<?php echo $required ? ' required' : ''; echo $disabled ? ' disabled' : ''; echo $multiple ? ' multiple' : ''; ?>>
        <?php echo $optionFields;?>
    </select>
    <?php if (trim($label) && !trim($id)):?>
        </label>
    <?php endif;?>
</div>