<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\AjaxForm $this
 * @var string $formId
 * @var string $class
 * @var string $action
 * @var string $method
 * @var string $enctype
 * @var string $fields
 * @var string $buttons
 * @var string $reCaptchaSiteKey
 * @var bool $useModal
 * @var string|\Kyegil\ViewRenderer\ViewInterface $script
 */
?><?php if(!$useModal):?><div id="skjemarespons-<?php echo $formId;?>" class="skjemarespons alert"></div><?php endif;?>
<form id="<?php echo $formId;?>" class="<?php echo $class;?>" action="<?php echo $action;?>" method="<?php echo $method;?>" enctype="<?php echo $enctype;?>">
    <?php echo $fields;?>
    <div class="knappelinje">
        <?php echo $buttons;?>
    </div>
</form>
<script>
    <?php echo $script;?>
</script>