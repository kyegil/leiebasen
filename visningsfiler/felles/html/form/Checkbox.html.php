<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\Checkbox $this
 * @var string $id
 * @var string $class
 * @var string $name
 * @var string $value
 * @var string|null $uncheckedValue
 * @var string $label
 * @var string $onclick
 * @var bool $required
 * @var bool $disabled
 * @var bool $checked
 */
$attributes = $this->data;
unset($attributes['label']);
?><?php if($uncheckedValue !== null):?><input
        type="hidden"
        name="<?php echo $name;?>"
        value="<?php echo $uncheckedValue;?>"
><?php endif;?><label><input
        type="checkbox"
        <?php foreach ($attributes as $attribute => $value):?>
            <?php if ($value === true): echo $attribute . "\n"; elseif (trim($value)): echo "{$attribute}=\"{$value}\"\n";endif;?>
            <?php endforeach;?>
    >
    <span><?php echo $label;?></span></label>