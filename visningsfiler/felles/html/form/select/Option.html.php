<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\select\Option $this
 * @var bool $disabled
 * @var string $label
 * @var bool $selected
 * @var string $value
 */
?><option value="<?php echo $value;?>"<?php echo $disabled ? ' disabled' : ''; echo $selected ? ' selected' : '';?>><?php echo htmlspecialchars($label);?></option>