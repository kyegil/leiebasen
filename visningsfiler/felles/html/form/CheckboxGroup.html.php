<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\CheckboxGroup $this
 * @var string $id
 * @var string $class
 * @var string $label
 * @var string $description
 * @var bool $required
 * @var \Kyegil\ViewRenderer\ViewInterface|string $optionFields
 */
?><fieldset<?php echo $id ? " id=\"{$id}\"" : "";?> class="checkboxes <?php echo $class;?>" <?php echo $required ? ' required' : '';?>>
    <legend><?php echo htmlspecialchars($label);?></legend>
    <p class="fieldset-description"><?php echo htmlspecialchars($description);?></p>
    <p class="options">
        <?php echo $optionFields;?>
    </p>
</fieldset>
<?php if($required):?>
<script>
    $( document ).ready(function() {
        <?php echo $id;?>Validation = function () {
            var required = $("fieldset#<?php echo $id;?> :checkbox:checked").length === 0;
            $("fieldset#<?php echo $id;?> :input").each(function(index,element){
                $(element).prop('required', required && $(element).is(':visible'));
            });
        }
        $("fieldset#<?php echo $id;?> :input").each(function(index,element){
            $(element).click(<?php echo $id;?>Validation);
        });
        <?php echo $id;?>Validation();
    });
</script>
<?php endif;?>
