<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\FieldSet $this
 * @var string $id
 * @var string $class
 * @var string $style
 * @var string $label
 * @var string $formId
 * @var string $name
 * @var string $disabled
 * @var \Kyegil\ViewRenderer\ViewInterface|string $contents
 */
?><fieldset
    <?php echo $id ? " id='{$id}'" : "";?>
    <?php echo $class ? " class='{$class}'" : "";?>
    <?php echo $style ? " style='{$style}'" : "";?>
    <?php echo $formId ? " form='{$formId}'" : "";?>
    <?php echo $name ? " name='{$name}'" : "";?>
    <?php echo $disabled ? " disabled" : "";?>
>
    <?php if(trim($label)):?><legend><?php echo $label;?></legend><?php endif;?>
    <?php echo $contents;?>
</fieldset>
