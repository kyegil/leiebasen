<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\RadioButton $this
 * @var string $id
 * @var string $class
 * @var string $name
 * @var string $value
 * @var string $label
 * @var string $onclick
 * @var bool $required
 * @var bool $disabled
 * @var bool $checked
 */
?><label>
    <input type="radio"
        name="<?php echo $name;?>"
        value="<?php echo $value;?>"
        <?php echo $id ? "id='{$id}'" : "";?>
        <?php echo trim($onclick) ? "onclick=\"{$onclick}\" " : "";?>
        <?php echo $class ? "class='{$class}'" : "";?>
        <?php echo $required ? "required " : "";?>
        <?php echo $disabled ? "disabled " : "";?>
        <?php echo $checked ? "checked " : "";?>
    >
    <span><?php echo $label;?></span>
</label>
