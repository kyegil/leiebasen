<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\MultiFileFields $this
 * @var string $id
 * @var string $class
 * @var string $style
 * @var string $label
 * @var string $name
 * @var string $disabled
 * @var \Kyegil\ViewRenderer\ViewInterface|string $contents
 * @var \Kyegil\ViewRenderer\ViewInterface|string $script
 */
?><fieldset
    class="file_uploads {$class}"
    <?php echo $id ? " id='{$id}'" : "";?>
    <?php echo $style ? " style='{$style}'" : "";?>
    <?php echo $disabled ? " disabled" : "";?>
>
    <legend><?php echo $label;?></legend>
    <?php echo $contents;?>
</fieldset>
<script>
    <?php echo $script;?>
    <?php echo $script;?>
</script>