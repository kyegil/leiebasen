<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\TextArea $this
 * @var string $id
 * @var string $name
 * @var string $label
 * @var string $class
 * @var string $style
 * @var bool $required
 * @var bool $disabled
 * @var string $value
 * @var int|string $maxlength
 */
?><div
    <?php echo $class ? "class=\"{$class}\"" : '';?><?php echo $style ? "style=\"{$style}\"" : '';?>>
    <?php if($label):?>
        <label for="<?php echo $id;?>"><?php echo $label;?>:</label>
    <?php endif;?>
    <textarea
        id="<?php echo $id;?>"
        name="<?php echo $name;?>"
        <?php if(trim($maxlength)):?>
            maxlength="<?php echo $maxlength;?>"
        <?php endif;?>
        <?php echo $required ? 'required' : ''; echo $disabled ? 'disabled' : '';?>
    ><?php echo $value;?></textarea>
</div>