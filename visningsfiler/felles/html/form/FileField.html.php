<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\FileField $this
 * @var string $id
 * @var string $name
 * @var string $label
 * @var string $class
 * @var bool $required
 * @var bool $disabled
 * @var bool $autocomplete
 * @var bool $readonly
 * @var bool $multiple
 * @var string $accept
 */
?><div <?php echo $class ? "class=\"{$class}\"" : '';?>>
    <?php if($label):?>
        <label for="<?php echo $id;?>"><?php echo $label;?>:</label>
    <?php endif;?>
    <input type="file"
           id="<?php echo $id;?>"
           name="<?php echo $name;?>"
           accept="<?php echo $accept;?>"
        <?php echo $multiple ? ' multiple' : ''; echo $required ? ' required' : ''; echo $disabled ? ' disabled' : ''; echo $readonly ? ' readonly' : '';?>
    >
</div>