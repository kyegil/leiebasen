<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\RadioButtonGroup $this
 * @var string $id
 * @var string $class
 * @var string $label
 * @var string $description
 * @var \Kyegil\ViewRenderer\ViewInterface|string $optionFields
 */
?><fieldset<?php echo $id ? " id='{$id}'" : "";?>
    class="radio-group <?php echo $class;?>"
>
    <legend><?php echo htmlspecialchars($label);?></legend>
    <?php if($description):?>
        <p class="radio-group-description"><?php echo htmlspecialchars($description);?></p>
    <?php endif;?>
    <p class="options">
        <?php echo $optionFields;?>
    </p>
</fieldset>
