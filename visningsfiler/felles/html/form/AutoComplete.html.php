<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\AutoComplete $this
 * @var string $id
 * @var string $name
 * @var string $label
 * @var string $class
 * @var string $size
 * @var bool $required
 * @var bool $disabled
 * @var bool $multiple
 * @var bool $hidden
 * @var \Kyegil\ViewRenderer\ViewInterface|string $optionFields
 * @var \Kyegil\ViewRenderer\ViewInterface|string $select2Config
 */
?><div <?php echo ($class ? "class=\"{$class}\" " : '') . ($hidden ? 'style="display: none;" ' : '');?>>
        <?php if(trim($label)):?>
            <label for="<?php echo $id;?>"><?php echo $label;?>:</label>
        <?php endif;?>
    <select id="<?php echo $id;?>" name="<?php echo $name . ($multiple ? "[]" : "");?>"<?php echo $required ? ' required' : ''; echo $disabled ? ' disabled' : ''; echo $multiple ? ' multiple' : ''; ?>
        <?php if (trim($size) != ''):?>
            size="<?php echo $size;?>"
        <?php endif;?>
    >
        <?php echo $optionFields;?>
    </select>
</div>
<script>
    $(document).ready(function() {
        $('#<?php echo $id;?>').select2(<?php echo $select2Config;?>);
    });
</script>