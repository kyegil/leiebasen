<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\DateTimeField $this
 * @var string $id
 * @var string $name
 * @var string $label
 * @var string $class
 * @var string $style
 * @var string $type
 * @var bool $required
 * @var bool $disabled
 * @var string $autocomplete
 * @var string $onblur
 * @var bool $readonly
 * @var string $min
 * @var string $minlength
 * @var string $max
 * @var string $maxlength
 * @var string $pattern
 * @var string $step
 * @var string $value
 */
?><div <?php echo $class ? "class=\"{$class}\"" : '';?> <?php echo $style ? "style=\"{$style}\"" : '';?>>
    <?php if ($label):?>
        <label for="<?php echo $id;?>"><?php echo $label;?>:</label>
    <?php endif;?>
    <input type="<?php echo $type;?>"
        id="<?php echo $id;?>"
        name="<?php echo $name;?>"
        value="<?php echo $value;?>"
        autocomplete="<?php echo $autocomplete;?>"
        <?php if (trim($onblur)):?>
            onblur="<?php echo $onblur;?>"
        <?php endif;?>
        <?php if (trim($minlength) != ''):?>
            minlength="<?php echo $minlength;?>"
        <?php endif;?>
        <?php if (trim($min) != ''):?>
            min="<?php echo $min;?>"
        <?php endif;?>
        <?php if (trim($maxlength) != ''):?>
            maxlength="<?php echo $maxlength;?>"
        <?php endif;?>
        <?php if (trim($max) != ''):?>
            max="<?php echo $max;?>"
        <?php endif;?>
        <?php if (trim($pattern)):?>
            pattern="<?php echo $pattern;?>"
        <?php endif;?>
        <?php if (trim($step)):?>
            step="<?php echo $step;?>"
        <?php endif;?>
        <?php echo $required ? ' required' : ''; echo $disabled ? ' disabled' : ''; echo $readonly ? ' readonly' : '';?>
    >
</div>