<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\DisplayField $this
 * @var string $id
 * @var string $label
 * @var string $class
 * @var string $type
 * @var bool $disabled
 * @var string $value
 */
?><div <?php echo $class ? "class=\"{$class}\"" : '';?>>
    <?php if($label):?>
        <label><?php echo $label;?>:</label>
    <?php endif;?>
    <input type="<?php echo $type;?>" id="<?php echo $id;?>" value="<?php echo $value;?>" <?php echo $disabled ? ' disabled' : '';?> readonly>
</div>