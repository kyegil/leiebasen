<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\HtmlTemplateEditor $this
 * @var string $id
 * @var string $name
 * @var string $label
 * @var string $description
 * @var string $class
 * @var bool $disabled
 * @var string $value
 * @var int|string $cols
 * @var int|string $rows
 * @var string $variabelOptionsJs
 * @var string|\Kyegil\Leiebasen\Visning $ckEditorConfigJs
 */
?><div <?php echo $class ? "class=\"{$class}\"" : '';?>>
    <?php if($label):?>
        <label for="<?php echo $id;?>"><?php echo $label;?>:</label>
    <?php endif;?>
    <?php if($description):?>
        <div class="description"><?php echo $description;?>:</div>
    <?php endif;?>
    <textarea id="<?php echo $id;?>" cols="<?php echo $cols;?>" rows="<?php echo $rows;?>" name="<?php echo $name;?>"<?php echo $disabled ? ' disabled' : '';?>><?php echo $value;?></textarea>
</div>
<!--suppress ES6UnusedImports, NpmUsedModulesInstalled, JSFileReferences -->
<script type="module">
    import {
        addListToDropdown,
        ClassicEditor,
        AccessibilityHelp,
        Alignment,
        Autoformat,
        Autosave,
        BlockQuote,
        Bold,
        ButtonView,
        Code,
        Collection,
        createDropdown,
        Font,
        Essentials,
        GeneralHtmlSupport,
        Heading,
        Highlight,
        HorizontalLine,
        Indent,
        IndentBlock,
        Italic,
        Link,
        List,
        ListProperties,
        Paragraph,
        PasteFromOffice,
        Plugin,
        RemoveFormat,
        SelectAll,
        SourceEditing,
        SpecialCharacters,
        Strikethrough,
        Style,
        Subscript,
        Superscript,
        Table,
        TableCaption,
        TableCellProperties,
        TableColumnResize,
        TableProperties,
        TableToolbar,
        TextTransformation,
        TodoList,
        Underline,
        Undo,
        ViewModel
    } from 'ckeditor5';
    import coreTranslations from 'ckeditor5/translations/no.js';

    class TemplateVariables extends Plugin {
        init() {
            const editor = this.editor;
            editor.ui.componentFactory.add( 'variables', () => {
                // The button will be an instance of ButtonView.
                const dropdown = createDropdown( editor.model );
                dropdown.buttonView.set( {
                    class: 'malvariabler',
                    label: 'Variabler',
                    withText: true
                } );

                const variabler = new Collection(<?php echo $variabelOptionsJs;?>);

                addListToDropdown( dropdown, variabler );

                // Execute a callback function when the button is clicked
                dropdown.on( 'execute', (eventInfo) => {
                    editor.model.change( writer => {
                        // Insert the text at the user's current position
                        editor.model.insertContent( writer.createText( eventInfo.source.id ) );
                    } );
                } );

                return dropdown;
            } );
        }
    }

    leiebasen.ckEditor = leiebasen.ckEditor || {};
    ClassicEditor
        .create( document.querySelector( '#<?php echo $id;?>' ), <?php echo $ckEditorConfigJs;?> )
        .then( editor => {
            /**
             * The editor is available globally as leiebasen.ckEditor['<?php echo $id;?>']
             */
                leiebasen.ckEditor['<?php echo $id;?>'] = editor;
        } )
        .catch( error => {
            console.error( error );
        } );
</script>