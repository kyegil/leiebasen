<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\CollapsibleSection $this
 * @var string $id
 * @var string $class
 * @var string $style
 * @var string $label
 * @var string $name
 * @var bool $collapsed
 * @var bool $draggable
 * @var string $ondragstart
 * @var string $ondrag
 * @var string $ondragend
 * @var string $ondragenter
 * @var string $ondragover
 * @var string $ondragleave
 * @var string $ondrop
 * @var \Kyegil\ViewRenderer\ViewInterface|string $contents
 */
?><div
    id="<?php echo $id;?>"
    draggable="<?php echo $draggable ? 'true' : 'false';?>"
    class='collapsible-section<?php echo $class ? (" " . $class) : "";?>'
    <?php echo $style ? " style='{$style}'" : "";?>
    <?php echo $ondragstart ? " ondragstart='{$ondragstart}'" : "";?>
    <?php echo $ondrag ? " ondrag='{$ondrag}'" : "";?>
    <?php echo $ondragend ? " ondragend='{$ondragend}'" : "";?>
    <?php echo $ondragenter ? " ondragenter='{$ondragenter}'" : "";?>
    <?php echo $ondragover ? " ondragover='{$ondragover}'" : "";?>
    <?php echo $ondragleave ? " ondragleave='{$ondragleave}'" : "";?>
    <?php echo $ondrop ? " ondrop='{$ondrop}'" : "";?>
>
    <div
        class="button collapsible-section-button<?php echo $collapsed ? '' : ' active'?>"
        id="<?php echo $id;?>-button"
        type="button"
        data-toggle="collapse"
        data-target="#<?php echo $id;?>-content"
        aria-expanded="<?php echo $collapsed ? 'false' : 'true'?>"
        aria-controls="<?php echo $id;?>-content"
    >
        <?php if($name):?>
            <input type="hidden"
                   name="<?php echo htmlspecialchars($name);?>"
                   value="0"
            >
            <input type="checkbox"
                id="<?php echo $id;?>-input"
                name="<?php echo htmlspecialchars($name);?>"
                value="1"<?php echo !$collapsed ? ' checked' : ''?>
            >
            <label for="<?php echo $id;?>-input"><?php echo htmlspecialchars($label);?></label>
        <?php else:?>
            <?php echo htmlspecialchars($label);?>
        <?php endif;?>
    </div>

    <div
            id="<?php echo $id;?>-content"
            class="collapse<?php echo $collapsed ? '' : ' show'?>"
    >
        <div class="card card-body"><?php echo $contents;?></div>
    </div>
</div><script>
    /**
     * @type {jQuery}
     */
    $("#<?php echo $id;?>-content").on('shown.bs.collapse', function() {
        let $nameInput = $("#<?php echo $id;?>-input");
        $(this).find(".required").prop('required', true);
        $nameInput.prop('checked', true).trigger("change");
    });
    $("#<?php echo $id;?>-content").on('hidden.bs.collapse', function() {
        let $nameInput = $("#<?php echo $id;?>-input");
        $(this).find("[required]").prop('required', false).addClass("required");
        $nameInput.prop('checked', false).trigger("change");
    });
    <?php if($collapsed):?>
    $("#<?php echo $id;?>-content").find("[required]").prop('required', false).addClass("required");
    <?php endif;?>
</script>