<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\form\HtmlEditor $this
 * @var string $id
 * @var string $name
 * @var string $label
 * @var string $description
 * @var string $class
 * @var bool $disabled
 * @var string $value
 * @var int|string $cols
 * @var int|string $rows
 * @var string|\Kyegil\Leiebasen\Visning $ckEditorConfigJs
 */
?><div <?php echo $class ? "class=\"{$class}\"" : '';?>>
    <?php if($label):?>
        <label for="<?php echo $id;?>"><?php echo $label;?>:</label>
    <?php endif;?>
    <?php if($description):?>
        <div class="description"><?php echo $description;?>:</div>
    <?php endif;?>
    <textarea id="<?php echo $id;?>" cols="<?php echo $cols;?>" rows="<?php echo $rows;?>" name="<?php echo $name;?>"<?php echo $disabled ? ' disabled' : '';?>><?php echo $value;?></textarea>
</div>
<!--suppress ES6UnusedImports, NpmUsedModulesInstalled, JSFileReferences -->
<script type="module">
    import {
        ClassicEditor,
        AccessibilityHelp,
        Alignment,
        Autoformat,
        Autosave,
        BlockQuote,
        Bold,
        Code,
        Font,
        Essentials,
        GeneralHtmlSupport,
        Heading,
        Highlight,
        HorizontalLine,
        Indent,
        IndentBlock,
        Italic,
        Link,
        List,
        ListProperties,
        Paragraph,
        PasteFromOffice,
        RemoveFormat,
        SelectAll,
        SourceEditing,
        SpecialCharacters,
        Strikethrough,
        Style,
        Subscript,
        Superscript,
        Table,
        TableCaption,
        TableCellProperties,
        TableColumnResize,
        TableProperties,
        TableToolbar,
        TextTransformation,
        TodoList,
        Underline,
        Undo
    } from 'ckeditor5';
    import coreTranslations from 'ckeditor5/translations/no.js';

    leiebasen.ckEditor = leiebasen.ckEditor || {};
    ClassicEditor
        .create( document.querySelector( '#<?php echo $id;?>' ), <?php echo $ckEditorConfigJs;?> )
        .then( editor => {
            /**
             * The editor is available globally as leiebasen.ckEditor['<?php echo $id;?>']
             */
                leiebasen.ckEditor['<?php echo $id;?>'] = editor;
        } )
        .catch( error => {
            console.error( error );
        } );
</script>