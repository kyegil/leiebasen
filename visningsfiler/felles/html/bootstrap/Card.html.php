<?php
    /**
     * Leiebasen Visningsblokk
     *
     * @var \Kyegil\Leiebasen\Visning\felles\html\bootstrap\Card $this
     * @var string $id
     * @var string $class
     * @var string $header
     * @var string $tittel
     * @var string $innhold
     * @var string $footer
     */
?><div class="card <?php echo $class;?>"
    <?php echo trim($id) ? (' id="' . $id . '"') : '';?>
>
    <?php if(trim($header)):?>
        <div class="card-header"><?php echo $header;?></div>
    <?php endif;?>
    <div class="card-body">
        <?php if(trim($tittel)):?>
            <h5 class="card-title"><?php echo $tittel;?></h5>
        <?php endif;?>
        <?php echo $innhold;?>
    </div>
    <?php if(trim($footer)):?>
        <div class="card-footer text-muted"><?php echo $footer;?></div>
    <?php endif;?>
</div>