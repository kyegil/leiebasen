<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\bootstrap\Modal $this
 * @var string $id
 * @var string $class
 * @var string $title
 * @var string $body
 * @var string $footer
 * @var string $size 'sm'|'lg'|'xl'
 * @var bool $verticallyCentered
 */
?><div
    id="<?php echo $id;?>"
    class="modal fade <?php echo $class;?>"
    tabindex="-1"
    role="dialog"
    aria-labelledby="<?php echo $id;?>Label"
    aria-hidden="true"
>
    <div  role="document"
          class="modal-dialog<?php echo $verticallyCentered ? ' modal-dialog-centered' : '';?><?php echo $size ? " modal-$size" : '';?>"
    >
        <div class="modal-content">

            <div class="modal-header">
                <?php if(trim($title)):?>
                    <h5 class="modal-title" id="<?php echo $id;?>Label"><?php echo $title;?></h5>
                <?php endif;?>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <?php if(trim($body)):?>
                <div class="modal-body">
                    <?php echo $body;?>
                </div>
            <?php endif;?>

            <?php if(trim($footer)):?>
                <div class="modal-footer">
                    <?php echo $footer;?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>