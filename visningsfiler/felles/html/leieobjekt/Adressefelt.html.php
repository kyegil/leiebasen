<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\leieobjekt\Adressefelt $this
 * @var string $navn
 * @var string $gateadresse
 * @var string $postnr
 * @var string $poststed
 */
?><div class="adressefelt">
    <?php if($navn):?>
        <?php echo $navn;?><br>
    <?php endif;?>
    <?php if($gateadresse):?>
        <?php echo $gateadresse;?><br>
    <?php endif;?>
    <?php echo $postnr;?> <?php echo $poststed;?>
</div>
