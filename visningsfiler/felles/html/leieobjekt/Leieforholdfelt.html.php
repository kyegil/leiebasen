<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\leieobjekt\Leieforholdfelt $this
 * @var \Kyegil\ViewRenderer\ViewInterface|string $leieforholdsett
 */
?><ul class="leieforholdsett">
    <?php echo $leieforholdsett;?>
</ul>