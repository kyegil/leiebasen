<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\leieobjekt\leieberegning\Leieberegningsformel $this
 * @var string $navn
 * @var string $beskrivelse
 * @var string $leiePerObjekt
 * @var string $leiePerKontrakt
 * @var string $leiePerKvadratmeter
 * @var string $kvadratmeterLeie
 * @var string $leieForBad
 * @var string $leieForEgenDo
 * @var string $leieForFellesDo
 */
?><span class="leieberegningsformel">
    <?php if($leiePerObjekt):?>
        Grunnbeløp: <?php echo $leiePerObjekt;?> +
    <?php endif;?>
    <?php if($leiePerKontrakt):?>
        <?php echo $leiePerKontrakt;?> per leieavtale +
    <?php endif;?>
    <?php echo $leiePerKvadratmeter;?> per ㎡
    <?php if($leieForBad):?> + <?php echo $leieForBad;?> for tilgang på bad<?php endif;?>
    <?php if($leieForEgenDo):?> + <?php echo $leieForEgenDo;?> for egen do<?php endif;?>
    <?php if($leieForFellesDo):?> + <?php echo $leieForFellesDo;?> for felles do<?php endif;?>
</span>

