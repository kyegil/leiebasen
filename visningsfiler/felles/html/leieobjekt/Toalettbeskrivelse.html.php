<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\leieobjekt\Toalettbeskrivelse $this
 * @var string $kategori
 * @var string $beskrivelse
 */
?><?php switch($kategori):?>
<?php case 2:?>Leieobjektet har eget toalett.<?php break;?>
<?php case 1:?>Det er tilgang til felles toalett i samme bygning/oppgang.<?php break;?>
<?php default:?>Leieobjektet har ikke tilgang til eget toalett<?php echo $beskrivelse ? " ({$beskrivelse})." : "."; break;?>
<?php endswitch;?>