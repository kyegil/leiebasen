<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\Utleierfelt $this
 * @var string $navn
 * @var string $adresse
 * @var string $postnr
 * @var string $poststed
 * @var string $orgNr
 */
?><div class="utleierfelt">
    <?php echo $navn;?><br>
    <?php echo $adresse;?><br>
    <?php echo $postnr;?> <?php echo $poststed;?><br>
    org. nr. <?php echo $orgNr;?><br>
</div>