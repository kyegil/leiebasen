<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\regning\Kravlinje $this
 * @var string $tekst
 * @var string $beløp
 * @var int $linjeNummer
 * @var int $antallLinjer
 */
?>

*  <?php echo $tekst;?>: <?php echo strip_tags(html_entity_decode($beløp));?>
