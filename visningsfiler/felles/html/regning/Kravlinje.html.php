<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\regning\Kravlinje $this
 * @var string $tekst
 * @var string $beløp
 */
?><tr>
    <td><?php echo $tekst;?></td>
    <td class="beløp"><?php echo $beløp;?></td>
</tr>
