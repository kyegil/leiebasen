<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\regning\Kravlinje $this
 * @var string $tekst
 * @var string $beløp
 * @var int $linjeNummer
 * @var int $antallLinjer
 */
?><tr class="item<?php echo ($linjeNummer == $antallLinjer ? " last" : "");?>">
    <td><?php echo $tekst;?></td>
    <td><?php echo $beløp;?></td>
</tr>