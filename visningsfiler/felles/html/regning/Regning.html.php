<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\html\regning\Regning $this
 * @var \Kyegil\ViewRenderer\ViewInterface $detaljer
 * @var string $leieforholdbeskrivelse
 * @var string $dato
 * @var string $forfall
 * @var string $kid
 * @var string $regningsbeløp
 * @var string $innbetalt
 * @var string $utestående
 */
?><dl class="regning egenskaper">
    <dt>Leieforhold:</dt>
    <dd><?php echo $leieforholdbeskrivelse;?></dd>
    <dt>Dato</dt>
    <dd><?php echo $dato;?></dd>
    <dt>Forfall</dt>
    <dd><?php echo $forfall;?></dd>
    <dt>KID</dt>
    <dd><?php echo $kid;?></dd>
</dl>

<table class="regning rutenett">
    <tbody>
    <?php echo $detaljer;?>
    </tbody>
    <tfoot>
    <tr>
        <td>Sum</td>
        <td class="beløp"><?php echo $regningsbeløp;?></td>
    </tr>
    <?php if($innbetalt):?>
        <tr>
            <td>Betalt</td>
            <td class="beløp"><?php echo $innbetalt;?></td>
        </tr>
    <?php endif;?>
    <tr>
        <td>Utestående</td>
        <td class="beløp"><?php echo $utestående;?></td>
    </tr>
    </tfoot>
</table>
