<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\shared\Epost $this */
/** @var int $tittel */
/** @var int $språk */
/** @var int $css */
/** @var int $innhold */
/** @var string $bunntekst */
?><!doctype html>
<html lang="<?php echo $språk;?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $tittel;?></title>

    <?php if($css):?>
    <style>
        <?php echo $css;?>
    </style>
    <?php endif?>
</head>

<body>
    <?php echo $innhold;?>
    <div class="bunntekst"><?php echo $bunntekst;?></div>
</body>
</html>