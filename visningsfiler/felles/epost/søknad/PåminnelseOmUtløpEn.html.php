<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\epost\søknad\PåminnelseOmUtløp $this
 * @var string $navn
 * @var string $adgangEpost
 * @var string $adgangUrl
 * @var string $adgangReferanse
 * @var string $epostTekst
 * @var string $utløpsdato
 * @var string $utleier
 * @var string $kontaktadresse
 */
?><?php if(trim($epostTekst)):?>
    <div><?php echo $epostTekst;?></div>
<?php else:?>
    <div>Hello <?php echo $navn;?>. This is a reminder that you have registered an application with <?php if(trim($utleier)):?><?php echo $utleier;?><?php else:?>us<?php endif;?>.</div>
    <div>If you want this application to remain active, it must be updated by <?php echo $utløpsdato;?>.</div>
<?php endif;?>
<?php if($adgangReferanse):?>
    <div>
        You have previously created a password for your application, and can update it at <a title="My Application" href="<?php echo $adgangUrl;?>"><?php echo $adgangUrl;?></a><br>
        using the reference code <?php echo $adgangReferanse;?> and your password.
    </div>
<?php endif;?>