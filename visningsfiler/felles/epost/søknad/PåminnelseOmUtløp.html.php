<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\epost\søknad\PåminnelseOmUtløp $this
 * @var string $navn
 * @var string $adgangEpost
 * @var string $adgangUrl
 * @var string $adgangReferanse
 * @var string $epostTekst
 * @var string $utløpsdato
 * @var string $utleier
 * @var string $kontaktadresse
 */
?><?php if(trim($epostTekst)):?>
    <div><?php echo $epostTekst;?></div>
<?php else:?>
    <div>Hei <?php echo $navn;?>. Dette er en påminnelse om at du har registrert en søknad hos <?php if(trim($utleier)):?><?php echo $utleier;?><?php else:?>oss<?php endif;?>.</div>
    <div>Søknaden vil bli inaktiv etter <?php echo $utløpsdato;?>.</div>
<?php endif;?>
<?php if($adgangReferanse):?>
    <div>Du har tidligere opprettet et passord for søknaden din, og kan oppdatere den ved å logge inn på <a title="Min søknad" href="<?php echo $adgangUrl;?>"><?php echo $adgangUrl;?></a><br>
        med referanse <?php echo $adgangReferanse;?> sammen med passordet ditt.
    </div>
<?php endif;?>