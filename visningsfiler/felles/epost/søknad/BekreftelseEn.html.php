<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\epost\søknad\Bekreftelse $this
 * @var string $navn
 * @var string $adgangEpost
 * @var string $adgangUrl
 * @var string $epostTekst
 * @var string $adgangReferanse
 * @var \Kyegil\Leiebasen\Visning|string $søknadOppsummering
 */
?><div>Hello <?php echo $navn;?>. Your application has been received.</div>
<?php if($adgangReferanse):?>
    <div>
        You can access your application at <a title="My Application" href="<?php echo $adgangUrl;?>"><?php echo $adgangUrl;?></a>
        <div>You need to log in with the reference code <?php echo $adgangReferanse;?> and your password.</div>
    </div>
<?php endif;?>
<?php if($søknadOppsummering):?>
    <div>Below is a summary of your application:<br></div>
    <?php echo $søknadOppsummering;?>
<?php endif;?>