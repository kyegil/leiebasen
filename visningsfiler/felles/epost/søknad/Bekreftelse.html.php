<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\epost\søknad\Bekreftelse $this
 * @var string $navn
 * @var string $adgangEpost
 * @var string $adgangUrl
 * @var string $epostTekst
 * @var string $adgangReferanse
 * @var \Kyegil\Leiebasen\Visning|string $søknadOppsummering
 */
?><div>Hei <?php echo $navn;?>. Din søknad er registrert.</div>
<?php if($adgangReferanse):?>
<div>
    For å se søknaden kan du logge inn på <a title="Min søknad" href="<?php echo $adgangUrl;?>"><?php echo $adgangUrl;?></a>
    <div>Du må logge inn med referanse <?php echo $adgangReferanse;?> for søknaden, samt passord.</div>
</div>
<?php endif;?>
<?php if($søknadOppsummering):?>
<div>Under følger en oppsummering av søknaden din:<br></div>
    <?php echo $søknadOppsummering;?>
<?php endif;?>