<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\innbetaling\Avstemmingslinje $this */
/** @var string $delbeløpId */
/** @var string $beløp */
/** @var string $avstemmingsbeskrivelse */
/** @var string $opprinneligKravBeløp */
/** @var string $utestående */
?><tr>
    <td style="padding: 0 10px; text-align: right;"><?php echo $beløp;?></td>
    <td style="padding: 0 10px; text-align: left;"><?php echo $avstemmingsbeskrivelse;?></td>
    <td style="padding: 0 10px; text-align: right;"><?php echo $opprinneligKravBeløp;?></td>
    <td style="padding: 0 10px; text-align: right;"><?php echo $utestående;?></td>
</tr>
