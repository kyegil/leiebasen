<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\innbetaling\Kvittering $this */
/** @var string $leieforholdnr */
/** @var string $leieforholdbeskrivelse */
/** @var string $betalingsdato */
/** @var string $beløp */
/** @var string $transaksjonsmåte */
/** @var string $betaler */
/** @var string $kid */
/** @var string $referanse */
/** @var string $bunntekst */
/** @var string $avstemming */
/** @var string $urlBase */

?><!doctype html>
<html lang="nb">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Kvittering for registrert innbetaling til leieforhold <?php echo $leieforholdnr;?></title>

    <style>
        .bunntekst {
            background: #eeeeee;
            border: 1px solid #ddd;
            font-size: 0.8rem;
            padding: 10px;
            line-height: normal;
            text-align: center;
        }
        .kvittering {
            padding: 10px;
            border: 1px solid grey;
            margin-left:auto;
            margin-right:auto;
        }
    </style>
</head>

<body>
    <table class= "kvittering">
        <caption>Følgende betaling er registrert på ditt leieforhold</caption>
        <tr>
            <td>
                <table>
                    <tr>
                        <td style="padding: 0 10px;">Leieforhold:</td>
                        <td style="padding: 0 10px;"><?php echo $leieforholdnr;?> - <?php echo $leieforholdbeskrivelse;?></td>
                    </tr>
                    <tr>
                        <td style="padding: 0 10px;">Dato:</td>
                        <td style="padding: 0 10px; font-weight: bold;"><?php echo $betalingsdato;?></td>
                    </tr>
                    <tr>
                        <td style="padding: 0 10px;">Beløp:</td>
                        <td style="padding: 0 10px; font-weight: bold;"><?php echo $beløp;?></td>
                    </tr>
                    <tr>
                        <td style="padding: 0 10px;">Transaksjonsmåte:</td>
                        <td style="padding: 0 10px;"><?php echo $transaksjonsmåte;?></td>
                    </tr>
                    <tr>
                        <td style="padding: 0 10px;">Innbetalt av:</td>
                        <td style="padding: 0 10px;"><?php echo $betaler;?></td>
                    </tr>
                    <tr>
                        <td style="padding: 0 10px;">Referanse:</td>
                        <td style="padding: 0 10px;"><?php echo $referanse;?></td>
                    </tr>
                    <?php if($kid):?>
                        <tr>
                            <td style="padding: 0 10px;">Anvendt KID:</td>
                            <td style="padding: 0 10px;"><?php echo $kid;?></td>
                        </tr>
                    <?php endif;?>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <caption>Avstemming:</caption>
                    <tr>
                        <th style="padding: 0 10px; text-align: right;">Beløp</th>
                        <th style="padding: 0 10px; text-align: left;">Betaling for</th>
                        <th style="padding: 0 10px; text-align: right;">Oppr. beløp</th>
                        <th style="padding: 0 10px; text-align: right;">Utestående</th>
                    </tr>

                    <?php echo $avstemming;?>
                </table>
            </td>
        </tr>
    </table>
    <div>&nbsp;</div>
    <div class="bunntekst"><?php echo $bunntekst;?></div>
</body>
</html>