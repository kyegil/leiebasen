<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\innbetaling\Kvittering $this */
/** @var string $leieforholdnr */
/** @var string $leieforholdbeskrivelse */
/** @var string $betalingsdato */
/** @var string $beløp */
/** @var string $transaksjonsmåte */
/** @var string $betaler */
/** @var string $kid */
/** @var string $referanse */
/** @var string $bunntekst */
/** @var string $avstemming */
/** @var string $urlBase */

?>
    Følgende betaling er registrert på ditt leieforhold
    Leieforhold: <?php echo $leieforholdnr;?> - <?php echo $leieforholdbeskrivelse;?>

    Dato:				<?php echo $betalingsdato;?>

    Beløp:				<?php echo $beløp;?>

    Transaksjonsmåte:	<?php echo $transaksjonsmåte;?>

    Innbetalt av:		<?php echo $betaler;?>

    Referanse:			<?php echo $referanse;?>

<?php if(trim($kid)):?>Anvendt KID:		<?php echo $kid;?><?php endif;?>


<?php if(trim($avstemming)):?>
    Avstemming:

    <?php echo $avstemming;?>

<?php endif;?>

<?php echo $bunntekst;?>