<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\innbetaling\Avstemmingslinje $this */
/** @var string $delbeløpId */
/** @var string $beløp */
/** @var string $avstemmingsbeskrivelse */
/** @var string $opprinneligKravBeløp */
/** @var string $utestående */
?>
* <?php echo $beløp;?>: <?php echo $avstemmingsbeskrivelse;?> (Utestående <?php echo $utestående;?>)
