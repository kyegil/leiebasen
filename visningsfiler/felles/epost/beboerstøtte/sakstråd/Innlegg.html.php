<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\epost\beboerstøtte\sakstråd\Innlegg $this
 * @var string $saksref
 * @var string $avsendernavn
 * @var string $sakslenke
 * @var string $innleggslenke
 * @var string $innhold
 */
?><div>Det har kommet en oppdatering i sak [<a href="<?php echo $sakslenke;?>" title="Vis saken i din nettleser"><?php echo $saksref;?></a>]. Du mottar denne eposten fordi du abonnerer på oppdateringer i saken.</div>
<div>Husk å beholde emnefeltet med saksnummeret dersom du svarer på denne eposten.</div>
<div><br></div>
<div><strong>Melding fra <?php echo $avsendernavn;?>:</strong></div>
<div><?php echo $innhold;?></div>