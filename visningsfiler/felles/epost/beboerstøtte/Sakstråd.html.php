<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\epost\beboerstøtte\Sakstråd $this
 * @var string $saksref
 * @var string $avsendernavn
 * @var string $sakslenke
 * @var string $innhold
 */
?><div>Ny beboerstøttesak [<a href="<?php echo $sakslenke;?>" title="Vis saken i din nettleser"><?php echo $saksref;?></a>] er opprettet. Du vil motta oppdateringer i saken.</div>
<div>Husk å beholde emnefeltet med saksnummeret dersom du svarer på denne eposten.</div>
<div><br></div>
<div><strong>Melding fra <?php echo $avsendernavn;?>:</strong></div>
<div><?php echo $innhold;?></div>