<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\leieforhold\umiddelbart_betalingsvarsel\Html $this */
/** @var string $leieforholdnr */
/** @var string $leieforholdbeskrivelse */
/** @var bool $fbo */
/** @var bool $ocr */
/** @var bool $fastKid */
/** @var string $urlBase */
/** @var string $linjer */
/** @var string $bankkonto */
/** @var string $epostTekst */

?><p>Leieforhold <?php echo $leieforholdnr;?><br>
    <?php echo $leieforholdbeskrivelse;?>
</p><p><?php echo $epostTekst;?></p><table>
    <caption>Følgende har ikke blitt betalt:</caption>
    <thead>
    <tr>
        <th style="padding: 0 10px;">Forfallsdato</th>
        <th style="padding: 0 10px;">&nbsp;</th>
        <th style="padding: 0 10px; text-align: right;">Opprinnelig beløp</th>
        <th style="padding: 0 10px; text-align: right;">Utestående</th>
        <th style="padding: 0 10px;">KID</th>
    </tr>
    </thead>
    <tbody>
    <?php echo $linjer;?>
    </tbody>
</table>

<p>Betaling kan skje til konto <?php echo $bankkonto;?></p>
<p>
    <?php if($ocr):?>Bruk KID som oppgitt for hvert krav over.<br>
        Ved samlebetalinger kan du også bruke dette leieforholdets generelle KID-nummer: <?php echo $fastKid;?>
    <?php else:?>
        Husk å merke innbetalinga med leieforhold <?php echo $leieforholdnr;?>
    <?php endif;?>
</p>