<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\leieforhold\umiddelbart_betalingsvarsel\Txt $this */
/** @var string $leieforholdnr */
/** @var string $leieforholdbeskrivelse */
/** @var bool $fbo */
/** @var bool $ocr */
/** @var bool $fastKid */
/** @var string $urlBase */
/** @var string $linjer */
/** @var string $bankkonto */
/** @var string $epostTekst */

?>Leieforhold <?php echo $leieforholdnr;?>

<?php echo $leieforholdbeskrivelse;?>

<?php echo $epostTekst;?>


Følgende har ikke blitt betalt:

<?php echo $linjer;?>


Betaling kan skje til konto <?php echo $bankkonto;?>

<?php if($ocr):?>
    Bruk KID som oppgitt for hvert krav over.
    Ved samlebetalinger kan du også bruke dette leieforholdets generelle KID-nummer: <?php echo $fastKid;?>

<?php else:?>
    Husk å merke innbetalinga med leieforhold <?php echo $leieforholdnr;?>

<?php endif;?>