<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\leieforhold\forfallsvarsel\html\Krav $this */
/** @var string $forfall */
/** @var string $tekst */
/** @var string $beløp */
/** @var string $utestående */
/** @var string $kid */
/** @var string $avtaleGiro */
?><tr>
    <td style="padding: 0 10px;"><?php echo $forfall;?></td>
    <td style="padding: 0 10px;"><?php echo $tekst;?></td>
    <td style="padding: 0 10px; text-align: right;"><?php echo $beløp;?></td>
    <td style="padding: 0 10px; text-align: right;"><?php echo $utestående;?></td>
    <td style="padding: 0 10px;"><?php echo $kid;?> <?php echo $avtaleGiro;?></td>
</tr>