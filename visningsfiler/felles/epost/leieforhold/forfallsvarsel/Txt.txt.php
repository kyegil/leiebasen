<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\leieforhold\forfallsvarsel\Txt $this */
/** @var string $leieforholdnr */
/** @var string $leieforholdbeskrivelse */
/** @var bool $fbo */
/** @var bool $ocr */
/** @var bool $fastKid */
/** @var string $urlBase */
/** @var string $linjer */
/** @var string $bankkonto */

?>Leieforhold <?php echo $leieforholdnr;?>

<?php echo $leieforholdbeskrivelse;?>


Dette er en valgfri påminnelse om beløp som er i ferd med å forfalle til betaling:

<?php echo $linjer;?>



<?php if($fbo):?>
    Beløpene vil automatisk trekkes fra din konto på forfallsdato med AvtaleGiro, forutsatt at du ikke har overstyrt dette i nettbanken din, og at det er dekning på konto.
    Beløp som ikke trekkes med AvtaleGiro kan betales inn til konto <?php echo $bankkonto;?>

<?php else:?>
    Betaling kan skje til konto <?php echo $bankkonto;?>

<?php endif;?>

<?php if($ocr):?>
    Bruk KID som oppgitt for hvert krav over.
    Ved samlebetalinger kan du også bruke dette leieforholdets generelle KID-nummer: <?php echo $fastKid;?>

<?php else:?>
    Husk å merke innbetalinga med leieforhold <?php echo $leieforholdnr;?>

<?php endif;?>

Hvis du ikke ønsker å motta forfall-påminnelser for framtida, så kan du endre dette i brukerprofilen di om du har en, eller gi beskjed til utleier.
