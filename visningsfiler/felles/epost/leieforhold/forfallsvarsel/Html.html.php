<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\leieforhold\forfallsvarsel\Html $this */
/** @var string $leieforholdnr */
/** @var string $leieforholdbeskrivelse */
/** @var bool $fbo */
/** @var bool $ocr */
/** @var bool $fastKid */
/** @var string $urlBase */
/** @var string $linjer */
/** @var string $bankkonto */

?><p>Leieforhold <?php echo $leieforholdnr;?><br>
    <?php echo $leieforholdbeskrivelse;?>
</p><table>
    <caption>Dette er en valgfri påminnelse om beløp som er i ferd med å forfalle til betaling:</caption>
    <thead>
    <tr>
        <th style="padding: 0 10px;">Forfallsdato</th>
        <th style="padding: 0 10px;">&nbsp;</th>
        <th style="padding: 0 10px; text-align: right;">Opprinnelig beløp</th>
        <th style="padding: 0 10px; text-align: right;">Utestående</th>
        <th style="padding: 0 10px;">KID</th>
    </tr>
    </thead>
    <tbody>
    <?php echo $linjer;?>
    </tbody>
</table>

<p>
    <?php if($fbo):?>Beløpene vil automatisk trekkes fra din konto på forfallsdato med AvtaleGiro, forutsatt at du ikke har overstyrt dette i nettbanken din, og at det er dekning på konto.<br>
    Beløp som ikke trekkes med AvtaleGiro kan betales inn til konto <?php echo $bankkonto;?>
    <?php else:?>Betaling kan skje til konto <?php echo $bankkonto;?>
    <?php endif;?>
</p>
<p>
    <?php if($ocr):?>Bruk KID som oppgitt for hvert krav over.<br>
        Ved samlebetalinger kan du også bruke dette leieforholdets generelle KID-nummer: <?php echo $fastKid;?>
    <?php else:?>
        Husk å merke innbetalinga med leieforhold <?php echo $leieforholdnr;?>
    <?php endif;?>
</p>
<p>Hvis du ikke ønsker å motta forfall-påminnelser for framtida, så kan du endre dette i brukerprofilen di om du har en, eller gi beskjed til utleier.</p>