<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\leieforhold\forfallsvarsel\txt\Krav $this */
/** @var string $forfall */
/** @var string $tekst */
/** @var string $beløp */
/** @var string $utestående */
/** @var string $kid */
/** @var string $avtaleGiro */
?><?php echo $tekst;?>

Forfallsdato: <?php echo $forfall;?>

Opprinnelig beløp: <?php echo $beløp;?>

Utestående: <?php echo $utestående;?>

<?php echo $kid ? "KID: {$kid}" : "";?>

<?php echo $avtaleGiro;?>

