<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\purring\Epostpurring $this */
/** @var string $logotekst */
/** @var string $avsender */
/** @var string $avsenderadresse */
/** @var string $avsenderEpost */
/** @var string $avsenderOrgNr */
/** @var string $avsenderTelefon */
/** @var string $avsenderBankkonto */
/** @var string $avsenderHjemmeside */
/** @var string $mottaker */
/** @var string $mottakerAdresse */
/** @var int $leieforholdnr */
/** @var string $leieforholdbeskrivelse */
/** @var string $kid */
/** @var string $dato */
/** @var string $forfall */
/** @var string $utestående */
/** @var boolean $fbo */
/** @var boolean $avtalegiroTilgjengelig */
/** @var boolean $efakturaTilgjengelig */
/** @var boolean $efaktura */
/** @var \Kyegil\Leiebasen\Visning|string $detaljer */
/** @var string $girotekst */
/** @var string $bunntekst */
/** @var string $urlBase */
?>******
Betalingspåminnelse

Leieforhold nr. <?php echo $leieforholdnr;?>, <?php echo $leieforholdbeskrivelse;?>

*******

Fra
<?php echo $avsender;?>

<?php echo strip_tags(html_entity_decode(preg_replace('#<br\s*/?>\s*#i', "\r\n", $avsenderadresse)));?>
<?php if(trim($avsenderTelefon)):?>
Telefon: <?php echo $avsenderTelefon;?>
<?php endif;?>

<?php if(trim($avsenderEpost)):?>
<?php echo $avsenderEpost;?>
<?php endif;?>

<?php if(trim($avsenderHjemmeside)):?>
<?php echo $avsenderHjemmeside;?>
<?php endif;?>

<?php if(trim($avsenderOrgNr)):?>
Org. nr. <?php echo $avsenderOrgNr;?>
<?php endif;?>

<?php if(trim($avsenderBankkonto)):?>
Bankkonto: <?php echo $avsenderBankkonto;?>
<?php endif;?>


Til
<?php echo $mottaker;?>

<?php echo trim(strip_tags(html_entity_decode(preg_replace('#<br\s*/?>\s*#i', "\r\n", $mottakerAdresse))));?>


Dato: <?php echo $dato;?>

Betalingsfrist: <?php echo $forfall;?>

KID: <?php echo $kid;?>

<?php echo $detaljer;?>


Totalt Utestående: <?php echo strip_tags(html_entity_decode($utestående));?>


    Du oppfordres til å betale forfalt beløp så snart som mulig.
    Betaling kan skje til konto <?php echo $avsenderBankkonto;?><br>
    KID <?php echo $kid;?>kan brukes fast for alle betalinger til dette leieforholdet.

<?php if( $avtalegiroTilgjengelig && !$fbo):?>
    Avtalegiro kan opprettes i nettbank.

<?php endif;?>

<?php echo strip_tags(html_entity_decode($girotekst));?>


<?php echo strip_tags(html_entity_decode($bunntekst));?>
