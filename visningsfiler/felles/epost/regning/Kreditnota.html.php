<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\regning\Epostfaktura $this */
/** @var string $logotekst */
/** @var string $avsender */
/** @var string $avsenderadresse */
/** @var string $avsenderEpost */
/** @var string $avsenderOrgNr */
/** @var string $avsenderTelefon */
/** @var string $avsenderBankkonto */
/** @var string $avsenderHjemmeside */
/** @var string $mottaker */
/** @var string $mottakerAdresse */
/** @var int $leieforholdnr */
/** @var string $leieforholdbeskrivelse */
/** @var int $gironr */
/** @var string $kid */
/** @var string $dato */
/** @var string $forfall */
/** @var string $girobeløp */
/** @var string $fradrag */
/** @var string $utestående */
/** @var boolean $fbo */
/** @var boolean $avtalegiroTilgjengelig */
/** @var boolean $efakturaTilgjengelig */
/** @var boolean $efaktura */
/** @var \Kyegil\Leiebasen\Visning|string $detaljer */
/** @var string $girotekst */
/** @var string $bunntekst */
/** @var string $urlBase */
?><!doctype html>
<!-- Fakturamal fra https://github.com/sparksuite/simple-html-invoice-template -->
<html lang="nb">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Kreditnota <?php echo $gironr;?> for leieforhold <?php echo $leieforholdnr;?></title>

    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td{
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        .bunntekst {
            background: #eeeeee;
            border: 1px solid #ddd;
            font-size: 0.8rem;
            padding: 10px;
            line-height: normal;
            text-align: center;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }
    </style>
</head>

<body>
<div class="invoice-box">
    <table>
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title">
                            <img src="<?php echo $urlBase;?>/pub/custom/logo/logo-web.png" alt="<?php echo $logotekst;?>" style="height:auto; max-height:200px; width:auto; max-width:300px;">
                        </td>

                        <td>
                            <?php echo $avsender;?><br>
                            <?php echo $avsenderadresse;?><br>
                            <?php if (trim($avsenderTelefon)):?>
                            Telefon: <a href="tel:+47<?php echo str_replace(' ', '', $avsenderTelefon);?>"><?php echo str_replace(' ', '&nbsp;', $avsenderTelefon);?></a><br>
                            <?php endif;?>
                            <?php if (trim($avsenderEpost)):?>
                            <a href="mailto:<?php echo $avsenderEpost;?>"><?php echo $avsenderEpost;?></a><br>
                            <?php endif;?>
                            <?php if (trim($avsenderHjemmeside)):?>
                            <a href="<?php echo $avsenderHjemmeside;?>"><?php echo $avsenderHjemmeside;?></a><br>
                            <?php endif;?>
                            <?php if (trim($avsenderOrgNr)):?>
                            Org. nr. <?php echo str_replace(' ', '&nbsp;', $avsenderOrgNr);?><br>
                            <?php endif;?>
                            <?php if (trim($avsenderBankkonto)):?>
                            Bankkonto: <?php echo str_replace(' ', '&nbsp;', $avsenderBankkonto);?>
                            <?php endif;?>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            <?php echo $mottaker;?><br>
                            <?php echo $mottakerAdresse;?>

                        </td>

                        <td>
                            Kreditnota nr. <?php echo $gironr;?><br>
                            Dato: <?php echo $dato;?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="heading">
            <td colspan="2">Leieforhold nr. <?php echo $leieforholdnr;?></td>
        </tr>

        <tr class="details">
            <td colspan="2"><?php echo $leieforholdbeskrivelse;?></td>
        </tr>

        <tr class="heading">
            <td>Å betale</td>
            <td>Beløp</td>
        </tr>

        <?php echo $detaljer;?>

        <tr class="total">
            <td>Kreditert</td>
            <td><?php echo $girobeløp;?></td>
        </tr>

        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
    </table>

    <div><?php echo $girotekst;?></div>
    <div>&nbsp;</div>
    <div class="bunntekst"><?php echo $bunntekst;?></div>
</div>
</body>
</html>