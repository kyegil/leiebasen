<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\regning\Epostfaktura $this */
/** @var string $logotekst */
/** @var string $avsender */
/** @var string $avsenderadresse */
/** @var string $avsenderEpost */
/** @var string $avsenderOrgNr */
/** @var string $avsenderTelefon */
/** @var string $avsenderBankkonto */
/** @var string $avsenderHjemmeside */
/** @var string $mottaker */
/** @var string $mottakerAdresse */
/** @var int $leieforholdnr */
/** @var string $leieforholdbeskrivelse */
/** @var int $gironr */
/** @var string $kid */
/** @var string $dato */
/** @var string $forfall */
/** @var string $girobeløp */
/** @var string $fradrag */
/** @var string $utestående */
/** @var boolean $fbo */
/** @var boolean $avtalegiroTilgjengelig */
/** @var boolean $efakturaTilgjengelig */
/** @var boolean $efaktura */
/** @var \Kyegil\Leiebasen\Visning|string $detaljer */
/** @var string $girotekst */
/** @var string $bunntekst */
/** @var string $urlBase */
?>******
Kreditnota nr. <?php echo $gironr;?>

Leieforhold nr. <?php echo $leieforholdnr;?>, <?php echo $leieforholdbeskrivelse;?>

*******

Fra
<?php echo $avsender;?>

<?php echo strip_tags(html_entity_decode(preg_replace('#<br\s*/?>\s*#i', "\r\n", $avsenderadresse)));?>
<?php if(trim($avsenderTelefon)):?>
Telefon: <?php echo $avsenderTelefon;?>
<?php endif;?>

<?php if(trim($avsenderEpost)):?>
<?php echo $avsenderEpost;?>
<?php endif;?>

<?php if(trim($avsenderHjemmeside)):?>
<?php echo $avsenderHjemmeside;?>
<?php endif;?>

<?php if(trim($avsenderOrgNr)):?>
Org. nr. <?php echo $avsenderOrgNr;?>
<?php endif;?>

<?php if(trim($avsenderBankkonto)):?>
Bankkonto: <?php echo $avsenderBankkonto;?>
<?php endif;?>


Til
<?php echo $mottaker;?>

<?php echo trim(strip_tags(html_entity_decode(preg_replace('#<br\s*/?>\s*#i', "\r\n", $mottakerAdresse))));?>


Dato: <?php echo $dato;?>



<?php echo $detaljer;?>



Total kreditt: <?php echo strip_tags($girobeløp);?>

<?php echo $girotekst;?>



<?php echo strip_tags(html_entity_decode($bunntekst));?>
