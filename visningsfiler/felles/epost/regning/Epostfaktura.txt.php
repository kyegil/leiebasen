<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\regning\Epostfaktura $this */
/** @var string $logotekst */
/** @var string $avsender */
/** @var string $avsenderadresse */
/** @var string $avsenderEpost */
/** @var string $avsenderOrgNr */
/** @var string $avsenderTelefon */
/** @var string $avsenderBankkonto */
/** @var string $avsenderHjemmeside */
/** @var string $mottaker */
/** @var string $mottakerAdresse */
/** @var int $leieforholdnr */
/** @var string $leieforholdbeskrivelse */
/** @var int $gironr */
/** @var string $kid */
/** @var string $dato */
/** @var string $forfall */
/** @var string $girobeløp */
/** @var string $fradrag */
/** @var string $utestående */
/** @var boolean $fbo */
/** @var boolean $avtalegiroTilgjengelig */
/** @var boolean $efakturaTilgjengelig */
/** @var boolean $efaktura */
/** @var \Kyegil\Leiebasen\Visning|string $detaljer */
/** @var string $girotekst */
/** @var string $bunntekst */
/** @var string $urlBase */
?>******
Regning nr. <?php echo $gironr;?>

Leieforhold nr. <?php echo $leieforholdnr;?>, <?php echo $leieforholdbeskrivelse;?>

*******

Fra
<?php echo $avsender;?>

<?php echo strip_tags(html_entity_decode(preg_replace('#<br\s*/?>\s*#i', "\r\n", $avsenderadresse)));?>
<?php if(trim($avsenderTelefon)):?>
Telefon: <?php echo $avsenderTelefon;?>
<?php endif;?>

<?php if(trim($avsenderEpost)):?>
<?php echo $avsenderEpost;?>
<?php endif;?>

<?php if(trim($avsenderHjemmeside)):?>
<?php echo $avsenderHjemmeside;?>
<?php endif;?>

<?php if(trim($avsenderOrgNr)):?>
Org. nr. <?php echo $avsenderOrgNr;?>
<?php endif;?>

<?php if(trim($avsenderBankkonto)):?>
Bankkonto: <?php echo $avsenderBankkonto;?>
<?php endif;?>


Til
<?php echo $mottaker;?>

<?php echo trim(strip_tags(html_entity_decode(preg_replace('#<br\s*/?>\s*#i', "\r\n", $mottakerAdresse))));?>


Dato: <?php echo $dato;?>

Forfall: <?php echo $forfall;?>

KID: <?php echo $kid;?>

<?php echo $detaljer;?>


Regningsbeløp: <?php echo strip_tags(html_entity_decode($girobeløp));?>

<?php if(trim($fradrag)):?>
Allerede innbetalt: <?php echo strip_tags(html_entity_decode($fradrag));?>

Utestående: <?php echo strip_tags(html_entity_decode($utestående));?>
<?php endif;?>


<?php if($fbo):?>
    Du trenger ikke foreta deg noe for å betale denne regningen.
    Beløpet trekkes fra kontoen din med avtalegiro på forfallsdato.

<?php endif;?>
<?php if( $avtalegiroTilgjengelig && !$fbo):?>
    Avtalegiro kan opprettes i nettbank.

<?php endif;?>

<?php echo strip_tags(html_entity_decode($girotekst));?>


<?php echo strip_tags(html_entity_decode($bunntekst));?>
