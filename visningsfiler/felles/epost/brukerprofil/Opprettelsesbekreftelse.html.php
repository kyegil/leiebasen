<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\epost\brukerprofil\Opprettelsesbekreftelse $this
 * @var string $navn
 * @var string $login
 * @var string $nettstedBeskrivelse
 * @var string $nettstedUrl
 * @var string $engangsbillettUrl
 */
?><div>Hei <?php echo $navn;?>.<br>
    Dette er en bekreftelse på opprettet brukerprofil med brukernavn «<strong><?php echo $login;?></strong>» på <a title="<?php echo $nettstedBeskrivelse;?>" href="<?php echo $nettstedUrl;?>"><?php echo $nettstedBeskrivelse;?></a>.<br></div>
<div></div>
<?php if(trim($engangsbillettUrl)):?>
    <div>
        Du bør logge inn umiddelbart for å angi et passord for brukerprofilen. Det kan du gjøre ved å klikke på knappen under, eller ved å besøke følgende adresse:<br>
        <?php echo $engangsbillettUrl;?><br><br>
        Dette er en midlertidig adgang som kun er tilgjengelig i 6 timer fra profilen ble opprettet, og som bare kan brukes én gang.<br>
    </div>
    <div>
        Dersom du ikke får angitt et eget passord i løpet av denne tiden, må du bruke «glemt passord»-tjenesten for å få tilsendt en ny engangsbillett.<br>
    </div>
    <div>
        <a
                class="button"
                title="Klikk her for å logge inn og oppgi et passord"
                href="<?php echo $engangsbillettUrl;?>">Logg inn</a>
    </div>
    <?php else:?><div>
        Logg inn med «<?php echo $login;?>» og tilhørende passord. Dersom du ikke kjenner passordet, må du bruke «glemt passord»-tjenesten for å oppgi et nytt.<br>
    </div>
<?php endif;?>
