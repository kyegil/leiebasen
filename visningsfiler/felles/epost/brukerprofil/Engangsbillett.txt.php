<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\epost\brukerprofil\Opprettelsesbekreftelse $this
 * @var string $navn
 * @var string $login
 * @var string $nettstedBeskrivelse
 * @var string $nettstedUrl
 * @var string $engangsbillettUrl
 */
?>
Dette er en engangsbillett for å logge inn som bruker «<?php echo $login;?>» på <?php echo $nettstedBeskrivelse;?>.

Du bør logge inn umiddelbart for å angi et varig passord for brukerprofilen. Det kan du gjøre ved å besøke følgende adresse:
<?php echo $engangsbillettUrl;?>

Engangsbilletten er gyldig i 6 timer.
