<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\epost\brukerprofil\Opprettelsesbekreftelse $this
 * @var string $navn
 * @var string $login
 * @var string $nettstedBeskrivelse
 * @var string $nettstedUrl
 * @var string $engangsbillettUrl
 */
?><div>Dette er en engangsbillett for å logge inn som bruker «<strong><?php echo $login;?></strong>» på <a title="<?php echo $nettstedBeskrivelse;?>" href="<?php echo $nettstedUrl;?>"><?php echo $nettstedBeskrivelse;?></a>.</div>
<div></div>
<div>
    Du bør logge inn umiddelbart for å angi et varig passord for brukerprofilen. Det kan du gjøre ved å klikke på knappen under, eller ved å besøke følgende adresse:<br>
    <?php echo $engangsbillettUrl;?><br>
    Engangsbilletten er gyldig i 6 timer.
</div>
<div>
    <a
            class="button"
            title="Klikk her for å logge inn"
            href="<?php echo $engangsbillettUrl;?>">Logg inn</a>
</div>