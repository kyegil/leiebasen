<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\epost\brukerprofil\Opprettelsesbekreftelse $this
 * @var string $navn
 * @var string $login
 * @var string $nettstedBeskrivelse
 * @var string $nettstedUrl
 * @var string $engangsbillettUrl
 */
?>Hei <?php echo $navn;?>.

Dette er en bekreftelse på opprettet brukerprofil med brukernavn «<?php echo $login;?>» på <?php echo $nettstedBeskrivelse;?>.
<div></div>
<?php if(trim($engangsbillettUrl)):?>

Du bør logge inn umiddelbart for å angi et passord for brukerprofilen. Det kan du gjøre ved å klikke på knappen under, eller ved å besøke følgende adresse:
<?php echo $engangsbillettUrl;?>
Dette er en midlertidig adgang som kun er tilgjengelig i 6 timer fra profilen ble opprettet, og som bare kan brukes én gang.

Dersom du ikke får angitt et eget passord i løpet av denne tiden, må du bruke «glemt passord»-tjenesten for å få tilsendt en ny engangsbillett.

Besøk denne adressen for å logge inn og oppgi et passord:
<?php echo $engangsbillettUrl;?>

<?php else:?>
Logg inn med «<?php echo $login;?>» og tilhørende passord. Dersom du ikke kjenner passordet, må du bruke «glemt passord»-tjenesten for å oppgi et nytt.
<?php endif;?>
