<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\epost\betalingsplan\forslag\Bekreftelse $this
 * @var string $utleier
 * @var string $leieforholdId
 * @var string $leieforholdBeskrivelse
 * @var string $avdrag
 * @var string $originalkrav
 * @var string $avtaleGiro
 */
?><div>Dette er en bekreftelse på innlevert forslag til betalingsplan med <?php echo $utleier;?> for leieforhold <?php echo $leieforholdId;?> <?php echo $leieforholdBeskrivelse;?></div>
<div>Du vil svar når forslaget er behandlet.</div>
<?php if(trim($avdrag)):?>
<h3></h3>
    <?php echo $avdrag;?>
<?php endif;?>