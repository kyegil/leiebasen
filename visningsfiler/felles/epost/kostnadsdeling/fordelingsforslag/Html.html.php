<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\kostnadsdeling\fordelingsforslag\Html $this */
/** @var string|\Kyegil\Leiebasen\Visning $tjenesteNavn */
/** @var string|\Kyegil\Leiebasen\Visning $tjenesteBeskrivelse */
/** @var string|\Kyegil\Leiebasen\Visning $fordelingsnøkkelVisning */
/** @var string|\Kyegil\Leiebasen\Visning $periode */
/** @var string|\Kyegil\Leiebasen\Visning $fakturaBeløp */
/** @var string|\Kyegil\Leiebasen\Visning $kostnadBeskrivelse */
/** @var string|\Kyegil\Leiebasen\Visning $fordelingstekst */
/** @var string|\Kyegil\Leiebasen\Visning $fordeling */

?><div>Fordeling av <?php echo $tjenesteNavn;?>
    <?php if(trim($tjenesteBeskrivelse)):?>
        <br><?php echo $tjenesteBeskrivelse;?>
    <?php endif;?>
</div>

<div class="fordelingstekst"><?php echo $fordelingstekst;?></div>

<div><?php echo $periode;?>: <?php echo $fakturaBeløp;?>
    <?php if(trim($kostnadBeskrivelse)):?>
            (<?php echo $kostnadBeskrivelse;?>)
    <?php endif;?>
</div>

<?php if(trim($fordelingsnøkkelVisning)):?>
    <div class="fordelingsnøkkel">Beløpet er fordelt etter følgende fordelingsnøkkel:<br>
        <?php echo $fordelingsnøkkelVisning;?>
    </div>
<?php endif;?>

<?php echo $fordeling;?>