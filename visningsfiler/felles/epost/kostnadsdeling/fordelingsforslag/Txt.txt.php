<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\kostnadsdeling\fordelingsforslag\Txt $this */
/** @var string|\Kyegil\Leiebasen\Visning $tjenesteNavn */
/** @var string|\Kyegil\Leiebasen\Visning $tjenesteBeskrivelse */
/** @var string|\Kyegil\Leiebasen\Visning $fordelingsnøkkelVisning */
/** @var string|\Kyegil\Leiebasen\Visning $periode */
/** @var string|\Kyegil\Leiebasen\Visning $fakturaBeløp */
/** @var string|\Kyegil\Leiebasen\Visning $kostnadBeskrivelse */
/** @var string|\Kyegil\Leiebasen\Visning $fordelingstekst */
/** @var string|\Kyegil\Leiebasen\Visning $fordeling */

?>Fordeling av <?php echo $tjenesteNavn;?>

<?php if(trim($tjenesteBeskrivelse)):?><?php echo $tjenesteBeskrivelse;?><?php endif;?>


<?php echo $fordelingstekst;?>

<?php echo $periode;?>: <?php echo $fakturaBeløp;?>

<?php if(trim($kostnadBeskrivelse)):?>(<?php echo $kostnadBeskrivelse;?>)<?php endif;?>

<?php if(trim($fordelingsnøkkelVisning)):?>Beløpet er fordelt etter følgende fordelingsnøkkel:

<?php echo $fordelingsnøkkelVisning;?>
<?php endif;?>

<?php echo $fordeling;?>