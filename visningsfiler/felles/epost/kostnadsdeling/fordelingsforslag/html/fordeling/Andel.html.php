<?php
/** @var \Kyegil\Leiebasen\Visning\felles\epost\kostnadsdeling\fordelingsforslag\html\fordeling\Andel $this */
/** @var int $leieforholdId */
/** @var string|\Kyegil\Leiebasen\Visning $leieforholdNavn */
/** @var string|\Kyegil\Leiebasen\Visning $andelTekst */
/** @var string|\Kyegil\Leiebasen\Visning $andelBeløp */

?><tr>
    <td><?php echo $leieforholdNavn;?></td>
    <td><?php echo $andelTekst;?></td>
    <td class="beløp"><?php echo $andelBeløp;?></td>
</tr>