<?php
/**
 * Leiebasen Visningsmal for Fpdf-layout
 *
 * @link http://www.fpdf.org/en/doc/index.php
 * @var Visning\felles\fpdf\Brev $this
 * @var bool $rammer
 * @var string|Visning $avsender
 * @var string|Visning $avsenderAdresse
 * @var string|Visning $avsenderBankkonto
 * @var string|Visning $avsenderEpost
 * @var string|Visning $avsenderGateadresse
 * @var string|Visning $avsenderHjemmeside
 * @var string|Visning $avsenderMobil
 * @var string|Visning $avsenderOrgNr
 * @var string|Visning $avsenderPostnr
 * @var string|Visning $avsenderPoststed
 * @var string|Visning $avsenderTelefax
 * @var string|Visning $avsenderTelefon
 * @var string|Visning $leieforholdId
 * @var string|Visning $mottakerAdresse
 */

use Kyegil\Leiebasen\Visning;

/**
 * Avstand i mm fra toppen av arket til OCR-linjen på kvitteringsslippen
 */
$yOcrLinjeKvitteringslipp = 190;
/**
 * Avstand i mm fra toppen av arket til den maskinlesbare OCR-linjen på giroen
 */
$yOcrLinje = 274;
/**
 * Tekstfarge
 */
$tekstFarge = 16;
/**
 * Highlighter-farge
 */
$highlighterFarge = 214;

$pdf = $this->hentPdf();

$pdf->AddPage();
$pdf->SetFillColor(255, 255, 255);


/**
 * FELTER SOM ER FASTSATT I FORHOLD TIL GIROBLANKETT
 */

/**
 * Lag ei rute som viser hvor det gule feltet på kvitteringsslippen begynner
 */
if ($rammer) {
    $pdf->setXY(0, 174);
    $pdf->Cell(
        0,            // bredde i mm (normalt 0)
        21,            // høyde i mm (normalt 0)
        '',
        $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
        0,            // Neste markørposisjon: 0 = til høyre, 1 = til begynnelsen av neste linje, 2 = ned (normalt 0)
        'L',        // Justering: 'L', 'C' eller 'R' (normalt 'L')
        false        // bakgrunnsfarge eller ikke (normalt false)
    );
}

/**
 * Adressefelt mottaker for konvoluttvindu
 */
$pdf->SetFont('Helvetica', '', 9);
$pdf->setY(30);
$pdf->setX(15);
$pdf->MultiCell(
    75,
    3.5,
    utf8_decode(mb_strtoupper($mottakerAdresse, 'UTF-8')),
    $rammer,
    'L'
);


/**
 * FELTER SOM KAN FLYTTES ETTER ØNSKE
 */

/**
 * Logo
 */
if (file_exists(\Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['root'] . 'pub/custom/logo/logo-print.png')) {
    $pdf->Image(
        \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['root'] . 'pub/custom/logo/logo-print.png',
        150,        // x-posisjon i mm for øverste venstre hjørne
        10,        // y-posisjon i mm for øverste venstre hjørne
        50,        // bredde i mm eller 0 for automatisk
        0,        // høyde i mm eller 0 for automatisk
        'png'    //
    );
}

/**
 * Brevhode adressefelt
 */
$pdf->setXY(75, 12);
$pdf->SetFont('Helvetica', '', 10);
$pdf->MultiCell(
    71,            // bredde i mm (normalt 0)
    4.6,            // linjehøyde i mm (normalt 0)
utf8_decode($avsenderAdresse) . "\n"
    . "\n"
    . (trim($avsenderOrgNr) ? utf8_decode('Org. nr. ' . $avsenderOrgNr) . "\n\n" : '')
    . (trim($avsenderEpost) ? utf8_decode($avsenderEpost) . "\n" : '')
    . (trim($avsenderHjemmeside) ? utf8_decode($avsenderHjemmeside) . "\n" : '')
    . (trim($avsenderTelefon) ? utf8_decode('Telefon: ' . $avsenderTelefon) . "\n" : '')
    . (trim($avsenderMobil) ? utf8_decode('Mobil: ' . $avsenderMobil) . "\n" : '')
    . (trim($avsenderBankkonto) ? utf8_decode('Bankkonto: ' . $avsenderBankkonto) . "\n" : ''),
    $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
    'R',        // Justering: 'L', 'C' eller 'R' (normalt 'L')
    false        // bakgrunnsfarge eller ikke (normalt false)
);
