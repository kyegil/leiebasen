<?php
/**
 * Leiebasen Visningsmal for Fpdf-layout
 *
 * @var \Kyegil\Leiebasen\Visning\felles\fpdf\Pdf $this
 * @var \Fpdf\Fpdf $fpdf
 * @var \Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray $innhold
 */
$pdf = $this->hentPdf();

$pdf->SetAutoPageBreak(false);

$innhold = strval($innhold);