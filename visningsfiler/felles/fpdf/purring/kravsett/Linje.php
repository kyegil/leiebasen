<?php
/**
 * Leiebasen Visningsmal for Fpdf-layout
 *
 * @see http://www.fpdf.org/en/doc/index.php
 * @var \Kyegil\Leiebasen\Visning\felles\fpdf\purring\kravsett\Linje $this
 * @var string $kravId
 * @var string $tekst
 * @var string $beløp
 * @var string $forfallsdato
 * @var string $utestående
 */
$pdf = $this->hentPdf();

/**
 * Rammer kan brukes for layoututvikling
 */
$rammer = false;

/**
 * Hent original posisjon
 * $x forventes å være 10mm fra venstre kant
 */
$x = $pdf->getX();
$y = $pdf->getY();

$pdf->Cell(73, 3.5, utf8_decode($tekst), $rammer, 0, 'L');
$pdf->setX($x + 73);
$pdf->Cell(15, 3.5, $beløp, $rammer, 0, 'R', true);
$pdf->setX($x + 88);
$pdf->Cell(17, 3.5, $forfallsdato, $rammer, 0, 'R', true);
$pdf->setX($x + 105);
$pdf->Cell(15, 3.5, $utestående, $rammer, 0, 'R', true);

/**
 * Gjenopprett original posisjon, men flytt én linje ned
 */
$pdf->setX($x);
$pdf->setY($y + 3.5);
