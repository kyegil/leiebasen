<?php
/**
 * Leiebasen Visningsmal for Fpdf-layout
 *
 * @see http://www.fpdf.org/en/doc/index.php
 * @var \Kyegil\Leiebasen\Visning\felles\fpdf\purring\Kravsett $this
 * @var string|PdfArray $linjer
 * @var string $utestående
 * @var bool $rammer
 */

use Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray;

$pdf = $this->hentPdf();

/**
 * Hent original posisjon
 * $x forventes å være 10mm fra venstre kant
 */
$x = $pdf->getX();
$y = $pdf->getY();

/**
 * Overskrifter i fet font
 */
$pdf->SetFont('Arial', 'B', 8);
$pdf->setX($x + 73);
$pdf->Cell(15, 3.5, utf8_decode('Beløp'), false, 0, 'R', true);
$pdf->setX($x + 88);
$pdf->Cell(17, 3.5, utf8_decode('Forfall'), false, 0, 'R', true);
$pdf->setX($x + 105);
$pdf->Cell(15, 3.5, utf8_decode('Ubetalt'), false, 0, 'R', true);
$pdf->setXY($x,$y + 3.5);

/**
 * List kravene i normal font
 */
$pdf->SetFont('Arial', '', 8);

$linjer = strval($linjer);

$x = $pdf->getX();
$y = $pdf->getY();

/**
 * Summeringslinje i fet font
 */
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(105, 3.5, utf8_decode('Sum:'), 'TB', 0, 'L');
$pdf->setX($x + 105);
$pdf->Cell(15, 3.5, $utestående, 'TB', 0, 'R');

$pdf->setY($pdf->getY() + 7);
$pdf->setX($x);