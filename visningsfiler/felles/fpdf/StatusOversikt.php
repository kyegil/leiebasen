<?php
/**
 * Leiebasen Visningsmal for Fpdf-layout
 *
 * @link http://www.fpdf.org/en/doc/index.php
 * @var Visning\felles\fpdf\StatusOversikt $this
 * @var bool $avtalegiroTilgjengelig
 * @var bool $efakturaTilgjengelig
 * @var bool $rammer
 * @var string|Visning $avsender
 * @var string|Visning $avsenderAdresse
 * @var string|Visning $avsenderBankkonto
 * @var string|Visning $avsenderEpost
 * @var string|Visning $avsenderGateadresse
 * @var string|Visning $avsenderHjemmeside
 * @var string|Visning $avsenderMobil
 * @var string|Visning $avsenderOrgNr
 * @var string|Visning $avsenderPostnr
 * @var string|Visning $avsenderPoststed
 * @var string|Visning $avsenderTelefax
 * @var string|Visning $avsenderTelefon
 * @var string|Visning $blankettbeløp
 * @var string|Visning $blankettbeløpKroner
 * @var string|Visning $blankettbeløpØre
 * @var string|Visning $blankettBetalingsinfo
 * @var string|Visning $fastKid
 * @var string|Visning $girotekst
 * @var string|Visning\felles\fpdf\purring\Kravsett $kravsett
 * @var string|Visning $leieforholdBeskrivelse
 * @var string|Visning $leieforholdId
 * @var string|Visning $mottakerAdresse
 * @var string|Visning $ocrKid
 * @var string|Visning $ocrKontonummer
 * @var string|Visning $ocrKontrollsiffer
 * @var string|Visning $statusDato
 * @var string|Visning $utestående
 */

use Kyegil\Leiebasen\Visning;

/**
 * Avstand i mm fra toppen av arket til OCR-linjen på kvitteringsslippen
 */
$yOcrLinjeKvitteringslipp = 190;
/**
 * Avstand i mm fra toppen av arket til den maskinlesbare OCR-linjen på giroen
 */
$yOcrLinje = 274;
/**
 * Tekstfarge
 */
$tekstFarge = 16;
/**
 * Highlighter-farge
 */
$highlighterFarge = 214;

$pdf = $this->hentPdf();

$pdf->AddPage();
$pdf->SetFillColor(255, 255, 255);

$pdf->AddFont('OCR-B-10', '', 'ocrb10.php');

/**
 * FELTER SOM ER FASTSATT I FORHOLD TIL GIROBLANKETT
 */

/**
 * Lag ei rute som viser hvor det gule feltet på kvitteringsslippen begynner
 */
if ($rammer) {
    $pdf->setXY(0, 174);
    $pdf->Cell(
        0,            // bredde i mm (normalt 0)
        21,            // høyde i mm (normalt 0)
        '',
        $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
        0,            // Neste markørposisjon: 0 = til høyre, 1 = til begynnelsen av neste linje, 2 = ned (normalt 0)
        'L',        // Justering: 'L', 'C' eller 'R' (normalt 'L')
        false        // bakgrunnsfarge eller ikke (normalt false)
    );
}

/**
 * Adressefelt mottaker for konvoluttvindu
 */
$pdf->SetFont('Helvetica', '', 9);
$pdf->setY(30);
$pdf->setX(15);
$pdf->MultiCell(
    75,
    3.5,
    utf8_decode(mb_strtoupper($mottakerAdresse, 'UTF-8')),
    $rammer,
    'L'
);

/**
 * Kontonummer på kvitteringsslipp
 */
$pdf->SetFont('OCR-B-10', '', 10);
$pdf->setXY(15, $yOcrLinjeKvitteringslipp);
$pdf->Cell(35, 5, $ocrKontonummer, $rammer, 0, 'L');

/**
 * Beløp på kvitteringsslipp
 */
$pdf->SetFont('OCR-B-10', '', 10);
$pdf->setXY(85, $yOcrLinjeKvitteringslipp);
$pdf->Cell(35, 5, number_format($blankettbeløp, 2, ",", " "), $rammer, 0, 'R');

/**
 * Forfallsdato på kvitteringsslipp
 */
$pdf->SetFont('Helvetica', '', 10);
$pdf->setXY(170, 200);
$pdf->Cell(28, 5, '', $rammer, 0, 'L');

/**
 * Betalingsinformasjon på girodel
 */
$pdf->SetFont('Helvetica', '', 10);
$pdf->setXY(15, 205);
$pdf->MultiCell(75, 4, utf8_decode($blankettBetalingsinfo), $rammer, 'L');

/**
 * Betaler på girodel
 */
$pdf->SetFont('Helvetica', '', 10);
$pdf->setXY(15, 235);
$pdf->MultiCell(75, 4, utf8_decode(mb_strtoupper($mottakerAdresse, 'UTF-8')), $rammer, 'L');

/**
 * Avsender på girodel
 */
$pdf->SetFont('Helvetica', '', 10);
$pdf->setXY(115, 235);
$pdf->multiCell(75, 4, utf8_decode(mb_strtoupper($avsenderAdresse, 'UTF-8')), $rammer, 'L');

/**
 * Maskinlesbart OCR-felt nederst på girodel
 */
$pdf->SetFont('OCR-B-10', '', 10);
$pdf->setXY(1, $yOcrLinje);
if ($rammer) {
    $pdf->Cell(8, 5, 'H', $rammer, 0, 'L');    // Bokstaven H for blankettjustering
}

/**
 * Maskinlesbart KID-nr
 */
$pdf->SetFont('OCR-B-10', '', 10);
$pdf->setXY(10, $yOcrLinje);
$pdf->Cell(65, 5, $ocrKid, $rammer, 0, 'R');

/**
 * Maskinlesbart Beløp
 */
$pdf->SetFont('OCR-B-10', '', 10);
$pdf->setXY(82.5, $yOcrLinje);
$pdf->Cell(30, 5, number_format($blankettbeløpKroner, 0, ", ", " ") . "  $blankettbeløpØre", $rammer, 0, 'R');

/**
 * Maskinlesbart Kontrollsiffer
 */
$pdf->SetFont('OCR-B-10', '', 10);
$pdf->setXY(113.5, $yOcrLinje);
$pdf->Cell(8, 5, '  ' . $ocrKontrollsiffer, $rammer, 0, 'L');

/**
 * Maskinlesbart Kontonummer
 */
$pdf->SetFont('OCR-B-10', '', 10);
$pdf->setXY(130, $yOcrLinje);
$pdf->Cell(35, 5, $ocrKontonummer, $rammer, 0, 'L');




/**
 * FELTER SOM KAN FLYTTES ETTER ØNSKE
 */

/**
 * Logo
 */
if (file_exists(\Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['root'] . 'pub/custom/logo/logo-print.png')) {
    $pdf->Image(
        \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['root'] . 'pub/custom/logo/logo-print.png',
        150,        // x-posisjon i mm for øverste venstre hjørne
        10,        // y-posisjon i mm for øverste venstre hjørne
        50,        // bredde i mm eller 0 for automatisk
        0,        // høyde i mm eller 0 for automatisk
        'png'    //
    );
}

/**
 * Brevhode adressefelt
 */
$pdf->setXY(75, 12);
$pdf->SetFont('Helvetica', '', 10);
$pdf->MultiCell(
    71,            // bredde i mm (normalt 0)
    4.6,            // linjehøyde i mm (normalt 0)
utf8_decode($avsenderAdresse) . "\n"
    . "\n"
    . (trim($avsenderOrgNr) ? utf8_decode('Org. nr. ' . $avsenderOrgNr) . "\n\n" : '')
    . (trim($avsenderEpost) ? utf8_decode($avsenderEpost) . "\n" : '')
    . (trim($avsenderHjemmeside) ? utf8_decode($avsenderHjemmeside) . "\n" : '')
    . (trim($avsenderTelefon) ? utf8_decode('Telefon: ' . $avsenderTelefon) . "\n" : '')
    . (trim($avsenderMobil) ? utf8_decode('Mobil: ' . $avsenderMobil) . "\n" : '')
    . (trim($avsenderBankkonto) ? utf8_decode('Bankkonto: ' . $avsenderBankkonto) . "\n" : ''),
    $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
    'R',        // Justering: 'L', 'C' eller 'R' (normalt 'L')
    false        // bakgrunnsfarge eller ikke (normalt false)
);

/**
 * Konfigurerbar meldingstekst
 */
$pdf->SetFont('Helvetica', '', 7);
$pdf->setXY(10, 150);
$pdf->MultiCell(
    131.4,            // bredde i mm (normalt 0)
    3.5,        // høyde i mm (normalt 0)
    utf8_decode($girotekst),
    $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
    'L',        // Justering: 'J', 'L', 'C' eller 'R' (normalt 'J')
    false        // bakgrunnsfarge eller ikke (normalt false)
);

$x = 10;
$y = 65;

/**
 * Leieforhold
 */
$pdf->SetFont('Helvetica', 'B', 11);
$pdf->setXY(10, 75);
$pdf->setFillColor($highlighterFarge);
$pdf->setTextColor(0);
$pdf->Cell(
    190,            // bredde i mm (normalt 0)
    6,            // høyde i mm (normalt 0)
    utf8_decode('OVERSIKT OVER UTESTÅENDE PER ' . $statusDato),
    $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
    1,            // Neste markørposisjon: 0 = til høyre, 1 = til begynnelsen av neste linje, 2 = ned (normalt 0)
    'L',        // Justering: 'L', 'C' eller 'R' (normalt 'L')
    true        // bakgrunnsfarge eller ikke (normalt false)
);
$pdf->setTextColor($tekstFarge);
$pdf->setFillColor(255);

/**
 * Leieforholdbeskrivelse
 */
$pdf->SetFont('Helvetica', '', 10);
$pdf->setXY(10, 84);
$pdf->Cell(
    190,        // bredde i mm (normalt 0)
    4,            // høyde i mm (normalt 0)
    utf8_decode($leieforholdBeskrivelse),
    $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
    0,            // Neste markørposisjon: 0 = til høyre, 1 = til begynnelsen av neste linje, 2 = ned (normalt 0)
    'L',        // Justering: 'L', 'C' eller 'R' (normalt 'L')
    false        // bakgrunnsfarge eller ikke (normalt false)
);

/**
 * Dato, kid-nummer, leieforhold og forfall
 */
$pdf->SetFont('Helvetica', '', 9);
$pdf->setXY(145, 84);
$pdf->MultiCell(
    55,            // bredde i mm (normalt 0)
    4,        // høyde i mm (normalt 0)
        'Dato: ' . $statusDato . "\n"
        . 'KID: ' . $fastKid . "\n"
        . 'Leieforhold: ' . $leieforholdId . "\n"
        . utf8_decode($leieforholdBeskrivelse) . "\n",
    $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
    'R',        // Justering: 'J', 'L', 'C' eller 'R' (normalt 'J')
    false        // bakgrunnsfarge eller ikke (normalt false)
);

/**
 * Skriv detalj-feltet
 */
$pdf->setXY(10, 90);
$kravsett = strval($kravsett);

/**
 * Høyre kolonne
 */
$x = 145;
$y = 150;

/**
 * Efaktura-logo og tekst
 */
if ($efakturaTilgjengelig) {

    /**
     * Posisjon 145mm fra venstre 135mm ned
     */
    $pdf->Image(
        \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['root'] . "pub/media/bilder/felles/eFaktura_print_4000_943.png",
        $x,        // x-posisjon i mm for øverste venstre hjørne
        $y,        // y-posisjon i mm for øverste venstre hjørne
        23,        // bredde i mm eller 0 for automatisk
        0,        // høyde i mm eller 0 for automatisk
        'png'    //
    );

    /**
     * Tekst om efaktura
     */
    $pdf->setXY($x, $y + 5.5);
    $pdf->SetFont('Helvetica', '', 7);
    $pdf->multiCell(
        23,            // bredde i mm (normalt 0)
        3,            // høyde i mm (normalt 0)
        utf8_decode("Motta regningene rett i nettbanken."),
        $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
        'C',        // Justering: 'L', 'C' eller 'R' (normalt 'L')
        false        // bakgrunnsfarge eller ikke (normalt false)
    );

    $pdf->SetFont('Helvetica', '', 7);
}

/**
 * AvtaleGiro-logo og tekst
 */
if ($avtalegiroTilgjengelig) {

    /**
     * Logo
     */
    $pdf->Image(
        \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['root'] . "pub/media/bilder/felles/AvtaleGiro_print_4000_764.png",
        $x + 55 - 28.4,// x-posisjon i mm for øverste venstre hjørne
        $y,        // y-posisjon i mm for øverste venstre hjørne
        28.4,    // bredde i mm eller 0 for automatisk
        0,        // høyde i mm eller 0 for automatisk
        'png'    //
    );

    /**
     * Tekst om avtaleGiro
     */
    $pdf->setXY($x + 55 - 28.4, $y + 5.5);
    $pdf->SetFont('Helvetica', '', 7);
    $pdf->multiCell(
        28.4,        // bredde i mm (normalt 0)
        3,            // høyde i mm (normalt 0)
        utf8_decode("Få regningene betalt automatisk på forfall."),
        $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
        'C',        // Justering: 'L', 'C' eller 'R' (normalt 'L')
        false        // bakgrunnsfarge eller ikke (normalt false)
    );
}
