<?php
/**
 * Leiebasen Visningsmal for Fpdf-layout
 *
 * @see http://www.fpdf.org/en/doc/index.php
 * @var Kravsett $this
 * @var string|PdfArray $linjer
 * @var string $girobeløp
 * @var bool $rammer
 */

use Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray;
use Kyegil\Leiebasen\Visning\felles\fpdf\kreditnota\Kravsett;

$pdf = $this->hentPdf();

/**
 * Hent original posisjon
 * $x forventes å være 10mm fra venstre kant
 */
$x = $pdf->getX();
$y = $pdf->getY();

/**
 * List kravene i normal font
 */
$pdf->SetFont('Arial', '', 8);

$linjer = strval($linjer);

$x = $pdf->getX();
$y = $pdf->getY();

/**
 * Summeringslinje i fet font
 */
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(90, 3.5, utf8_decode('Sum kreditert beløp:'), 'TB', 0, 'L');
$pdf->setX($x + 105);
$pdf->Cell(15, 3.5, $girobeløp, 'TB', 0, 'R');

$pdf->setY($pdf->getY() + 7);
$pdf->setX($x);