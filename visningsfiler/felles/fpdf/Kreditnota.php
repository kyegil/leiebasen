<?php
/**
 * Leiebasen Visningsmal for Fpdf-layout
 *
 * @link http://www.fpdf.org/en/doc/index.php
 * @var Visning\felles\fpdf\Kreditnota $this
 * @var string|Visning $avsender
 * @var string|Visning $avsenderAdresse
 * @var string|Visning $avsenderGateadresse
 * @var string|Visning $avsenderPostnr
 * @var string|Visning $avsenderPoststed
 * @var string|Visning $avsenderOrgNr
 * @var string|Visning $avsenderTelefon
 * @var string|Visning $avsenderTelefax
 * @var string|Visning $avsenderMobil
 * @var string|Visning $avsenderEpost
 * @var string|Visning $avsenderHjemmeside
 * @var string|Visning $mottakerAdresse
 * @var string|Visning $leieforholdId
 * @var string|Visning $leieforholdBeskrivelse
 * @var string|Visning $fastKid
 * @var string|Visning $gironr
 * @var string|Visning $utskriftsdato
 * @var string|Visning $girobeløp
 * @var string|Visning $girotekst
 * @var string|Visning $bankkonto
 * @var string|Visning\felles\fpdf\regning\Kravsett $kravsett
 * @var string|Visning\felles\fpdf\regning\TidligereUbetalt $tidligereUbetalt
 * @var string|Visning\felles\fpdf\regning\SisteInnbetalinger $sisteInnbetalinger
 * @var bool $rammer
 */

use Kyegil\Leiebasen\Visning;

/**
 * Avstand i mm fra toppen av arket til OCR-linjen på kvitteringsslippen
 */
$yOcrLinjeKvitteringslipp = 190;
/**
 * Avstand i mm fra toppen av arket til den maskinlesbare OCR-linjen på giroen
 */
$yOcrLinje = 274;
/**
 * Tekstfarge
 */
$tekstFarge = 16;
/**
 * Highlighter-farge
 */
$highlighterFarge = 214;

$pdf = $this->hentPdf();

$pdf->AddPage();
$pdf->SetFillColor(255, 255, 255);

$pdf->AddFont('OCR-B-10', '', 'ocrb10.php');

/**
 * FELTER SOM ER FASTSATT I FORHOLD TIL GIROBLANKETT
 */

/**
 * Lag ei rute som viser hvor det gule feltet på kvitteringsslippen begynner
 */
if ($rammer) {
    $pdf->setXY(0, 174);
    $pdf->Cell(
        0,            // bredde i mm (normalt 0)
        21,            // høyde i mm (normalt 0)
        '',
        $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
        0,            // Neste markørposisjon: 0 = til høyre, 1 = til begynnelsen av neste linje, 2 = ned (normalt 0)
        'L',        // Justering: 'L', 'C' eller 'R' (normalt 'L')
        false        // bakgrunnsfarge eller ikke (normalt false)
    );
}

/**
 * Adressefelt mottaker for konvoluttvindu
 */
$pdf->SetFont('Helvetica', '', 9);
$pdf->setY(30);
$pdf->setX(15);
$pdf->MultiCell(
    75,
    3.5,
    utf8_decode(mb_strtoupper($mottakerAdresse, 'UTF-8')),
    $rammer,
    'L'
);

/**
 * Kontonummer på kvitteringsslipp
 */
$pdf->SetFont('OCR-B-10', '', 10);
$pdf->setXY(15, $yOcrLinjeKvitteringslipp);
$pdf->Cell(35, 5, 'Kreditnota', $rammer, 0, 'L');

/**
 * Maskinlesbart OCR-felt nederst på girodel
 */
$pdf->SetFont('OCR-B-10', '', 10);
$pdf->setXY(1, $yOcrLinje);
if ($rammer) {
    $pdf->Cell(8, 5, 'H', $rammer, 0, 'L');    // Bokstaven H for blankettjustering
}

/**
 * Maskinlesbart KID-nr
 */
$pdf->SetFont('OCR-B-10', '', 10);
$pdf->setXY(10, $yOcrLinje);
$pdf->Cell(65, 5, 'Skal ikke betales', $rammer, 0, 'R');

/**
 * FELTER SOM KAN FLYTTES ETTER ØNSKE
 */

/**
 * Logo
 */
if (file_exists(\Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['root'] . 'pub/custom/logo/logo-print.png')) {
    $pdf->Image(
        \Kyegil\Leiebasen\Leiebase::$config['leiebasen']['server']['root'] . 'pub/custom/logo/logo-print.png',
        150,        // x-posisjon i mm for øverste venstre hjørne
        10,        // y-posisjon i mm for øverste venstre hjørne
        50,        // bredde i mm eller 0 for automatisk
        0,        // høyde i mm eller 0 for automatisk
        'png'    //
    );
}

/**
 * Brevhode adressefelt
 */
$pdf->setXY(75, 12);
$pdf->SetFont('Helvetica', '', 10);
$pdf->MultiCell(
    71,            // bredde i mm (normalt 0)
    4.6,            // linjehøyde i mm (normalt 0)
utf8_decode($avsenderAdresse) . "\n"
    . "\n"
    . (trim($avsenderOrgNr) ? utf8_decode('Org. nr. ' . $avsenderOrgNr) . "\n\n" : '')
    . (trim($avsenderEpost) ? utf8_decode($avsenderEpost) . "\n" : '')
    . (trim($avsenderHjemmeside) ? utf8_decode($avsenderHjemmeside) . "\n" : '')
    . (trim($avsenderTelefon) ? utf8_decode('Telefon: ' . $avsenderTelefon) . "\n" : '')
    . (trim($avsenderMobil) ? utf8_decode('Mobil: ' . $avsenderMobil) . "\n" : '')
    . (trim($bankkonto) ? utf8_decode('Bankkonto: ' . $bankkonto) . "\n" : ''),
    $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
    'R',        // Justering: 'L', 'C' eller 'R' (normalt 'L')
    false        // bakgrunnsfarge eller ikke (normalt false)
);

/**
 * Konfigurerbar meldingstekst
 */
$pdf->SetFont('Helvetica', '', 7);
$pdf->setXY(10, 150);
$pdf->MultiCell(
    131.4,            // bredde i mm (normalt 0)
    3.5,        // høyde i mm (normalt 0)
    utf8_decode($girotekst),
    $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
    'L',        // Justering: 'J', 'L', 'C' eller 'R' (normalt 'J')
    false        // bakgrunnsfarge eller ikke (normalt false)
);

$x = 10;
$y = 65;

/**
 * Leieforhold
 */
$pdf->SetFont('Helvetica', 'B', 11);
$pdf->setXY(10, 75);
$pdf->setFillColor($highlighterFarge);
$pdf->setTextColor(0);
$pdf->Cell(
    190,            // bredde i mm (normalt 0)
    6,            // høyde i mm (normalt 0)
    utf8_decode('Kreditnota nr. ' . $gironr),
    $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
    1,            // Neste markørposisjon: 0 = til høyre, 1 = til begynnelsen av neste linje, 2 = ned (normalt 0)
    'L',        // Justering: 'L', 'C' eller 'R' (normalt 'L')
    true        // bakgrunnsfarge eller ikke (normalt false)
);

$pdf->setTextColor($tekstFarge);
$pdf->setFillColor(255);

/**
 * Leieforholdbeskrivelse
 */
$pdf->SetFont('Helvetica', '', 10);
$pdf->setXY(10, 84);
$pdf->Cell(
    190,        // bredde i mm (normalt 0)
    4,            // høyde i mm (normalt 0)
    utf8_decode($leieforholdBeskrivelse),
    $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
    0,            // Neste markørposisjon: 0 = til høyre, 1 = til begynnelsen av neste linje, 2 = ned (normalt 0)
    'L',        // Justering: 'L', 'C' eller 'R' (normalt 'L')
    false        // bakgrunnsfarge eller ikke (normalt false)
);

/**
 * Dato, kid-nummer, leieforhold og forfall
 */
$pdf->SetFont('Helvetica', '', 9);
$pdf->setXY(145, 84);
$pdf->MultiCell(
    55,            // bredde i mm (normalt 0)
    4,        // høyde i mm (normalt 0)
        'Dato: ' . $utskriftsdato . "\n"
        . 'Leieforhold: ' . $leieforholdId . "\n"
        . $leieforholdBeskrivelse . "\n"
        . 'Fast KID: ' . $fastKid . "\n",
    $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
    'R',        // Justering: 'J', 'L', 'C' eller 'R' (normalt 'J')
    false        // bakgrunnsfarge eller ikke (normalt false)
);

/**
 * Skriv detalj-feltet
 */
$pdf->setXY(10, 90);
$kravsett = strval($kravsett);


/**
 * Høyre kolonne
 */
$x = 145;
$y = 150;
