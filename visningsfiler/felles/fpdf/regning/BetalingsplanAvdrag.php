<?php
/**
 * Leiebasen Visningsmal for Fpdf-layout
 *
 * @see http://www.fpdf.org/en/doc/index.php
 * @var \Kyegil\Leiebasen\Visning\felles\fpdf\regning\BetalingsplanAvdrag $this
 * @var bool $vis
 * @var string $avdragDato
 * @var string $beløp
 * @var string $samletGjeld
 * @var string $restbeløp
 * @var bool $rammer
 */

use Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray;

$pdf = $this->hentPdf();

/**
 * Hent original posisjon
 * $x forventes å være 10mm fra venstre kant
 */
$x = $pdf->getX();
$y = $pdf->getY();

if ($vis) {

    /**
     * Vis overskrifter
     */
    $pdf->SetFont('Arial', '', 8);

    $pdf->Cell(105, 3.5, utf8_decode('Avdrag for ' . $avdragDato . '  i følge betalingsplan for tidligere gjeld'), false, 0, 'L');
    $pdf->setX($x + 105);
    $pdf->Cell(15, 3.5, $beløp, false, 0, 'R');
    $pdf->setX($x);
    $pdf->setY($pdf->getY() + 3.5);

    $pdf->SetFont('Arial', '', 7.5);
    $pdf->Cell(105, 3.5, utf8_decode('Resterende i betalingsplan er deretter kr. ' . $restbeløp), false, 0, 'L');
    $pdf->setX($x);
    $pdf->setY($pdf->getY() + 6);
}