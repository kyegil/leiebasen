<?php
/**
 * Leiebasen Visningsmal for Fpdf-layout
 *
 * @link http://www.fpdf.org/en/doc/index.php
 * @var TidligereUbetalt $this
 * @var string|PdfArray $linjer
 * @var bool $vis
 * @var string $utestående
 * @var bool $rammer
 */

use Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray;
use Kyegil\Leiebasen\Visning\felles\fpdf\regning\TidligereUbetalt;

$pdf = $this->hentPdf();

/**
 * Hent original posisjon
 * $x forventes å være 10mm fra venstre kant
 */
$x = $pdf->getX();
$y = $pdf->getY();

/**
 * Denne blokka skal kun vises dersom det er eldre ubetalte regninger
 */
if ($vis) {

    /**
     * Vis overskrifter
     */
    $pdf->SetFont('Arial', 'B', 9);
    $pdf->Cell(
        100,        // bredde i mm (normalt 0)
        4,            // høyde i mm (normalt 0)
        utf8_decode('Tidligere utestående:'),
        $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
        0,            // Neste markørposisjon: 0 = til høyre, 1 = til begynnelsen av neste linje, 2 = ned (normalt 0)
        'L',        // Justering: 'L', 'C' eller 'R' (normalt 'L')
        false        // bakgrunnsfarge eller ikke (normalt false)
    );
    $pdf->setX($x + 105);
    $pdf->Cell(
        15,            // bredde i mm (normalt 0)
        4,        // høyde i mm (normalt 0)
        'Ubetalt',
        $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
        0,            // Neste markørposisjon: 0 = til høyre, 1 = til begynnelsen av neste linje, 2 = ned (normalt 0)
        'R',        // Justering: 'L', 'C' eller 'R' (normalt 'L')
        false        // bakgrunnsfarge eller ikke (normalt false)
    );
    $pdf->setX($x);
    $pdf->setY($pdf->getY() + 4);

    $pdf->SetFont('Arial', '', 8);

    /**
     * Vis detalj-linjer
     */
    $linjer = strval($linjer);

    /**
     * Summeringslinje
     */
    $pdf->setX($x + 105);
    $pdf->SetFont('Arial', 'B', 8);
    $pdf->Cell(
        15,            // bredde i mm (normalt 0)
        3.5,        // høyde i mm (normalt 0)
        $utestående,
        false,        // innramming? Boolsk eller 'LRTB' (normalt false)
        0,            // Neste markørposisjon: 0 = til høyre, 1 = til begynnelsen av neste linje, 2 = ned (normalt 0)
        'R',        // Justering: 'L', 'C' eller 'R' (normalt 'L')
        true        // bakgrunnsfarge eller ikke (normalt false)
    );
    $pdf->setX($x);
    $pdf->setY($pdf->getY() + 3.5);
}