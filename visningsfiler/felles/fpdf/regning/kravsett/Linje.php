<?php
/**
 * Leiebasen Visningsmal for Fpdf-layout
 *
 * @see http://www.fpdf.org/en/doc/index.php
 * @var \Kyegil\Leiebasen\Visning\felles\fpdf\regning\kravsett\Linje $this
 * @var string $kravId
 * @var string $tekst
 * @var string $beløp
 * @var string $utestående
 */
$pdf = $this->hentPdf();

/**
 * Rammer kan brukes for layoututvikling
 */
$rammer = false;

/**
 * Hent original posisjon
 * $x forventes å være 10mm fra venstre kant
 */
$x = $pdf->getX();
$y = $pdf->getY();

$pdf->Cell(90, 3.5, iconv("UTF-8", "ISO-8859-1//TRANSLIT", $tekst), $rammer, 0, 'L');
$pdf->setX($x + 105);
$pdf->Cell(15, 3.5, $beløp, $rammer, 0, 'R', true);

/**
 * Gjenopprett original posisjon, men flytt én linje ned
 */
$pdf->setX($x);
$pdf->setY($y + 3.5);
