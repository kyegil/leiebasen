<?php
/**
 * Leiebasen Visningsmal for Fpdf-layout
 *
 * @see http://www.fpdf.org/en/doc/index.php
 * @var \Kyegil\Leiebasen\Visning\felles\fpdf\regning\siste_innbetalinger\Linje $this
 * @var string $dato
 * @var string $betaler
 * @var string $beløp
 * @var string $referanse
 */
$pdf = $this->hentPdf();

/**
 * Rammer kan brukes for layoututvikling
 */
$rammer = false;

/**
 * Hent original posisjon
 * $x forventes å være 10mm fra venstre kant
 */
$x = $pdf->getX();
$y = $pdf->getY();

$pdf->Cell(15, 3, $dato, $rammer, 0, 'L');
$pdf->Cell(30, 3, utf8_decode($betaler), $rammer, 0, 'L');
$pdf->Cell(20, 3, $beløp, $rammer, 0, 'R');
$pdf->Cell(20, 3, utf8_decode($referanse), $rammer, 0, 'L');


/**
 * Gjenopprett original posisjon, men flytt én linje ned
 */
$pdf->setX($x);
$pdf->setY($y + 3);
