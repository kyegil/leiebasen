<?php
/**
 * Leiebasen Visningsmal for Fpdf-layout
 *
 * @link http://www.fpdf.org/en/doc/index.php
 * @var \Kyegil\Leiebasen\Visning\felles\fpdf\regning\SisteInnbetalinger $this
 * @var string|PdfArray $linjer
 * @var bool $vis
 * @var bool $rammer
 */

use Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray;

$pdf = $this->hentPdf();

/**
 * Hent original posisjon
 * $x forventes å være 10mm fra venstre kant
 */
$x = $pdf->getX();
$y = $pdf->getY();

/**
 * Denne blokka skal kun vises dersom det har blitt foretatt betalinger
 * og dersom det er plass på giroen
 */
if ($vis && $y < 155) {
    $pdf->SetFont('Arial', 'B', 8);
    $pdf->Cell(
        40,            // bredde i mm (normalt 0)
        3.5,        // høyde i mm (normalt 0)
        utf8_decode('Siste registrerte innbetalinger:'),
        $rammer,    // innramming? Boolsk eller 'LRTB' (normalt false)
        0,            // Neste markørposisjon: 0 = til høyre, 1 = til begynnelsen av neste linje, 2 = ned (normalt 0)
        'L',        // Justering: 'L', 'C' eller 'R' (normalt 'L')
        false        // bakgrunnsfarge eller ikke (normalt false)
    );
    $pdf->SetFont('Arial', '', 7);
    $pdf->setY($pdf->getY() + 3.5);
    $pdf->setX($x);
    $pdf->Cell(15, 3, utf8_decode('dato'), $rammer, 0, 'L');
    $pdf->Cell(30, 3, utf8_decode('betalt av'), $rammer, 0, 'L');
    $pdf->Cell(20, 3, utf8_decode('beløp'), $rammer, 0, 'R');
    $pdf->Cell(20, 3, 'ref', $rammer, 0, 'L');
    $pdf->setY($pdf->getY() + 3);
    $linjer = strval($linjer);
}