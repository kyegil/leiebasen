<?php
/**
 * Leiebasen Visningsmal for Fpdf-layout
 *
 * @see http://www.fpdf.org/en/doc/index.php
 * @var \Kyegil\Leiebasen\Visning\felles\fpdf\regning\Kravsett $this
 * @var string|PdfArray $linjer
 * @var string $girobeløp
 * @var string $innbetalt
 * @var string $utestående
 * @var bool $rammer
 */

use Kyegil\Leiebasen\Visning\felles\fpdf\PdfArray;

$pdf = $this->hentPdf();

/**
 * Hent original posisjon
 * $x forventes å være 10mm fra venstre kant
 */
$x = $pdf->getX();
$y = $pdf->getY();

/**
 * List kravene i normal font
 */
$pdf->SetFont('Arial', '', 8);

$linjer = strval($linjer);

$x = $pdf->getX();
$y = $pdf->getY();

/**
 * Summeringslinje i fet font
 */
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(90, 3.5, utf8_decode('Sum denne regning:'), 'TB', 0, 'L');
$pdf->setX($x + 105);
$pdf->Cell(15, 3.5, $girobeløp, 'TB', 0, 'R');

$pdf->setY($pdf->getY() + 7);
$pdf->setX($x);

/**
 * Dersom regningen er delvis betalt, lages ei egen linje for fradrag
 */
if ($innbetalt) {
    $pdf->SetFont('Arial', '', 8);
    $pdf->Cell(105, 3.5, utf8_decode('Tidligere innbetalinger til fradrag'), 'B', 0, 'L');
    $pdf->setX($x + 105);
    $pdf->Cell(15, 3.5, $innbetalt, 'B', 0, 'R');
    $pdf->setY($pdf->getY() + 3.5);
    $pdf->setX($x);
}

/**
 * Til slutt ei fet linje for uteståene beløp
 */
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(105, 3.5, utf8_decode('Utestående:'), false, 0, 'L');
$pdf->setX($x + 105);
$pdf->Cell(15, 3.5, $utestående, false, 0, 'R');
$pdf->setY($pdf->getY() + 9);
$pdf->setX($x);