<?php
/**
 * ExtJs common for classes
 *
 * @var \Kyegil\Leiebasen\Visning\felles\extjs4\Common $this
 * @var string $variableName If specified, the object will be assigned to this JS variable
 * @var string $className If specified, the object will be created using the Ext.create command, rather than by the xtype config parameter
 * @var string $configString
 */
?>
<?php if($variableName):?>var <?php echo $variableName;?> = <?php endif;?><?php if($className):?>Ext.create('<?php echo $className;?>', <?php echo trim($configString);?>)<?php if($variableName):?>;<?php endif;?><?php else:?><?php echo $configString;?><?php endif;?>