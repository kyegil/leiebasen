<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\proxy\Proxy $this
 * @var string $variableName
 * @var string $konfigurering
 */
?><?php if($variableName):?>var <?php echo $variableName;?> = Ext.create('Ext.data.proxy.Proxy', <?php endif;?>{
    <?php echo $konfigurering;?>
}<?php if($variableName):?>);
<?php endif;?>

