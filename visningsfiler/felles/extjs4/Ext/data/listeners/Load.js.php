<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\listeners\Load $this
 */
?>
var jsonData = store.getProxy().getReader().jsonData;
if (typeof jsonData == 'undefined') {
    Ext.MessageBox.alert('Problem:', 'Klarte ikke laste data av ukjent grunn.');
    return;
}
var success = jsonData.success;
var msg = jsonData.msg;
var responseText = jsonData.responseText;
var httpStatus = jsonData.status;

if (typeof success !== 'undefined' && success) {
    return;
}
if (typeof msg !== 'undefined') {
    Ext.MessageBox.alert('Klarte ikke laste data:', msg);
    return;
}
if (typeof httpStatus !== 'undefined' && httpStatus !== 200) {
    if (typeof msg !== 'undefined') {
        msg = 'Dataforespørselen mottok http status ' + httpStatus + ' fra tjeneren';
    }
    Ext.MessageBox.alert('Klarte ikke laste data:', msg);
    return;
}

if (typeof responseText !== 'undefined') {
    msg = responseText.replace(/\r\n|\r|\n/g, '<br>');
    if (msg) {
        msg = '<strong>Respons fra tjeneren</strong><br>' + msg;
    }
    else {
        msg = 'Dataforespørselen mottok blank respons.';
    }
    Ext.MessageBox.alert('Klarte ikke laste data:', msg);
}