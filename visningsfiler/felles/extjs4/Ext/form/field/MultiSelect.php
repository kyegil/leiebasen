<?php
/** @var string $allowBlank */
/** @var string $columnWidth */
/** @var string[]|object $dataValues */
/** @var string $disabled */
/** @var string $editable */
/** @var string $fieldLabel */
/** @var string $forceSelection */
/** @var string $hideLabel */
/** @var string $itemId */
/** @var string $minChars */
/** @var string $name */
/** @var string $store */
/** @var string $typeAhead */
/** @var string $value */
/** @var string $width */
/** @deprecated */
$data = [];
settype($forceSelection, 'boolean');
settype($remote, 'boolean');
if(isset($dataValues)){
    foreach( $dataValues as $valueField => $displayField) {
        if(is_object($dataValues)) {
            $data[] = ['value' => $valueField, 'text' => $displayField];
        }
        else {
            $data[] = ['value' => $displayField];
        }
    }
}
?>

{
    fieldLabel: '<?=isset($fieldLabel) ? addslashes($fieldLabel) : ''?>',
    name: '<?=isset($name) ? addslashes($name) : ''?>',
    itemId: '<?=isset($itemId) ? addslashes($itemId) : ''?>',
    <?=isset($allowBlank) ? ('allowBlank: ' . ($allowBlank ? 'true' : 'false') . ',') : '' ?>

    <?=isset($width) ? ('width: ' . (is_numeric($width) ? $width : ("'" . addslashes($width). "'")) . ',') : '' ?>

    value: '<?=isset($value) ? addslashes($value) : ''?>',

        <?=isset($hideLabel) ? ('hideLabel: ' . ($hideLabel ? 'true' : 'false') . ",\n") : "" ?>
        <?=isset($disabled) ? ('disabled: ' . ($disabled ? 'true' : 'false') . ",\n") : '' ?>
        <?=isset($columnWidth) ? ('columnWidth: ' . (is_numeric($columnWidth) ? $columnWidth : ("'" . addslashes($columnWidth) . "'")) . ",\n") : '' ?>

        <?=isset($allowBlank) ? ('allowBlank: ' . ($allowBlank ? 'true' : 'false') . ",\n") : '' ?>
        <?=isset($forceSelection) ? ('forceSelection: ' . ($forceSelection ? 'true' : 'false') . ",\n") : '' ?>
        <?=isset($editable) ? ('editable: ' . ($editable ? 'true' : 'false') . ",\n") : '' ?>
        <?=isset($typeAhead) ? ('typeAhead: ' . ($typeAhead ? 'true' : 'false') . ",\n") : '' ?>
        <?=isset($minChars) ? ('minChars: ' . intval($minChars) . ",\n") : '' ?>

<?php if($remote):?>
        queryMode: 'remote',
        store: Ext.create('Ext.data.JsonStore', {
            storeId: '<?=isset($itemId) ? addslashes($itemId . '_store') : 'data_store'?>',
            autoLoad: true,
            proxy: {
                type: 'ajax',
                url: "/drift/index.php?docu=<?=$_GET['docu'];?>&trader=<?=$_GET['trader']?>&id=<?=$_GET['id']?>&mission=request&data=attributevalues&attribute=<?=$itemId?>",
                reader: {
                    type: 'json',
                    root: 'data',
                    idProperty: 'value'
                }
            },
            fields: ['value', 'text'],
        }),
<?php elseif(isset($dataValues)):?>
    queryMode: 'local',
    store: Ext.create('Ext.data.Store', {
    storeId: '<?=isset($itemId) ? addslashes($itemId . '_store') : 'data_store'?>',
    <?php if(is_object($dataValues)):?>
            fields: ['value', 'text'],

    <?php else:?>
            fields: ['value'],

    <?php endif;?>
    data: <?= json_encode($data);?>
    }),
<?php else:?>
        queryMode: '<?php echo isset($queryMode) ? $queryMode : 'local'?>',
        <?=isset($store) ? "store: {$store}," : "";?>
<?php endif;?>

    delimiter: ', ',
    valueField: 'value',
    displayField: '<?=is_object($dataValues) ? 'text' : 'value' ?>',
    triggerAction: 'all',
    anyMatch: true,
    <?=isset($multiSelect) ? ('multiSelect: ' . ($multiSelect ? 'true' : 'false') . ',') : '' ?>

    xtype: 'multiselectfield'
}