<?php
/** @var string $fieldLabel */
/** @var string $name */
/** @var string $itemId */
/** @var string $allowBlank */
/** @var string $value */
/** @var string $validator */
?>
{
    <?=isset($fieldLabel) ? ("fieldLabel: " . json_encode($fieldLabel) . ",") : "" ?>
    <?=isset($name) ? ("name: " . json_encode($name) . ",") : "" ?>
    <?=isset($itemId) ? ("itemId: " . json_encode($itemId) . ",") : "" ?>
    <?=isset($allowBlank) ? ('allowBlank: ' . ($allowBlank ? 'true' : 'false') . ',') : '' ?>
    <?=isset($value) ? ("value: " . json_encode($value) . ",") : "" ?>
    <?=isset($validator) ? ("validator: {$validator},") : "" ?>
    inputType: 'password',
    xtype: 'textfield'
}