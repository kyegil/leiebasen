<?php
/**
 * FlexiCheckBoxGroup ComboBox Select Function body
 *
 * @var \Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FlexiCheckboxGroupEvents\ComboKeyup $this
 * @var string $checkBoxGroupItemId
 * @var string $name
 * @var bool $submitAsArray
 */
?>
/**
 * @name combo
 * @type {Ext.form.field.ComboBox}
 */
/**
 * @name e
 * @type {Ext.EventObject}
 */
if(e.keyCode === 13) {
    /** @type {Ext.form.CheckboxGroup} */
    var checkboxGroup = combo.ownerCt.getComponent(<?php echo json_encode($checkBoxGroupItemId);?>);
    var submitAsArray = <?php echo $submitAsArray ? 'true' : 'false';?>;
    var value = combo.getValue();
    var checkboxExists = false;

    if(value) {
        checkboxGroup.items.items.forEach(function (element) {
            if (element.inputId === ('<?php echo $checkBoxGroupItemId;?>-' + value)) {
                checkboxExists = true;
            }
        });
        if (!checkboxExists) {
            if (submitAsArray) {
                checkboxGroup.add({
                    boxLabel: value,
                    inputId: '<?php echo $checkBoxGroupItemId;?>-' + value,
                    name: '<?php echo $name;?>[]',
            value: value,
            inputValue: value,
            uncheckedValue: undefined
                });
            }
            else {
                checkboxGroup.add({
                    boxLabel: value,
                    inputId: '<?php echo $checkBoxGroupItemId;?>-' + value,
                    name: '<?php echo $name;?>[' + value + ']'
                });
            }
        }
    }
    combo.clearValue();
    checkboxGroup.focus();
}
