<?php
/**
 * FlexiCheckBoxGroup ComboBox Select Function body
 *
 * @var \Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\FlexiCheckboxGroupEvents\ComboSelect $this
 * @var string $checkBoxGroupItemId
 * @var string $name
 * @var bool $submitAsArray
 */
?>
/**
 * @name combo
 * @type {Ext.form.field.ComboBox}
 */
/**
 * @name records
 * @type {Ext.data.Model[]}
 */
/** @type {Ext.form.CheckboxGroup} */
var checkboxGroup = combo.ownerCt.getComponent(<?php echo json_encode($checkBoxGroupItemId)?>);
var submitAsArray = <?php echo $submitAsArray ? 'true' : 'false';?>;
var value = records[0].get('value');
var displayValue = value;
if (typeof records[0].get('text') != 'undefined') {
    displayValue = records[0].get('text');
}

var checkboxExists = false;
checkboxGroup.items.items.forEach(function (element) {
    if (element.inputId === ('<?php echo $checkBoxGroupItemId;?>-' + value)) {
        checkboxExists = true;
    }
});
if (!checkboxExists) {
    if (submitAsArray) {
        checkboxGroup.add({
            boxLabel: displayValue,
            inputId: '<?php echo $checkBoxGroupItemId;?>-' + value,
            name: '<?php echo $name;?>[]',
            value: value,
            inputValue: value,
            uncheckedValue: undefined
        });
    }
    else {
        checkboxGroup.add({
            boxLabel: displayValue,
            inputId: '<?php echo $checkBoxGroupItemId;?>-' + value,
            name: '<?php echo $name;?>[' + value + ']'
        });
    }
}
combo.clearValue();
checkboxGroup.focus();
