<?php
/**
 * ExtJs FormPanel Action failed Function body
 *
 * @var \Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\PanelEvents\Actionfailed $this
 */
?>
/**
 * @name form
 * @type {Ext.form.Basic}
 */
/**
 * @name action
 * @type {Ext.form.action.Action}
 */
/**
 * @name eOpts
 * @type {Object}
 */
if (action.type === 'load') {
    leiebasen.formLoadAction.failed(form, action, eOpts);
}

else if (action.type === 'submit'){
    leiebasen.formSubmitAction.failed(form, action, eOpts);
}
