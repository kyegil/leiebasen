<?php
/**
 * ExtJs FormPanel Action Complete Function body
 *
 * @var \Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\PanelEvents\Actioncomplete $this
 */
?>
/**
 * @name form
 * @type {Ext.form.Basic}
 */
/**
 * @name action
 * @type {Ext.form.action.Action}
 */
/**
 * @name eOpts
 * @type {Object}
 */
if (action.type === 'load') {
    leiebasen.formLoadAction.complete(form, action, eOpts);
}

else if (action.type === 'submit'){
    leiebasen.formSubmitAction.complete(form, action, eOpts);
}
