<?php
/**
 * Multi File Upload Add Button Handler
 *
 * @var \Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\MultiFileUploadEvents\AddHandler $this
 * @var string $fileUploadContainerId
 * @var string $name
 */
?>
/**
 * @name addButton
 * @type {Ext.form.button.Button}
 */
/**
 * @name e
 * @type {Ext.EventObject}
 */
/** @type {Ext.form.FieldSet} */
var fileFieldSet = addButton.ownerCt;

/** @type {Ext.form.field.Hidden} */
var newUploadsCountField = fileFieldSet.getComponent('new_' + <?php echo json_encode($name);?> + '_count');
var newUploadsCount = newUploadsCountField.getValue();

var newFileField = Ext.create('Ext.form.field.File', {
    name: <?php echo json_encode($name . '[]');?>,
    allowOnlyWhitespace: false,
    buttonText: 'Søk'
});

var newClearButton = Ext.create('Ext.button.Button', {
    text: 'Fjern',
    margin: '0 0 0 70',
    handler: function(button) {
        var newUploadsCountField = fileFieldSet.getComponent('new_' + <?php echo json_encode($name);?> + '_count');
        newUploadsCountField.setValue(newUploadsCountField.getValue() - 1);
        button.ownerCt.ownerCt.remove(button.ownerCt);
    }
});

var newFileFieldContainer = Ext.create('Ext.form.FieldContainer', {
    layout: 'hbox',
    items: [newFileField, newClearButton]
});

fileFieldSet.remove(addButton, false);
fileFieldSet.add(newFileFieldContainer);
newUploadsCount++;
newUploadsCountField.setValue(newUploadsCount);
fileFieldSet.add(addButton);
newFileField.focus();
// newFileField.button.el.dom.click();
