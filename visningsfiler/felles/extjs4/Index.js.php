<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\felles\extjs4\Index $this
 * @var string $extPath
 * @var string $extLoaderConfig
 * @var string $extRequire
 * @var \Kyegil\Leiebasen\Visning|string $extBeforeOnReady
 * @var \Kyegil\Leiebasen\Visning|string $extOnReady
 */
?>
Ext.tip.QuickTipManager.init();
Ext.form.Field.prototype.msgTarget = 'side';
Ext.Loader.setConfig(<?php echo $extLoaderConfig;?>);
<?php echo $extBeforeOnReady;?>

Ext.Loader.setPath('Ext.ux', '<?php echo $extPath;?>/examples/ux');

Ext.require(<?php echo $extRequire;?>);

var leiebasen = leiebasen || {};
leiebasen.drift = leiebasen.drift || {};
leiebasen.kjerne = leiebasen.kjerne || {};

leiebasen.formLoadAction = {
    /**
     * @param {Ext.form.Basic} form
     * @param {Ext.form.action.Action} action
     */
    complete: function(form, action) {
        if (action.response.responseText === '') {
            Ext.MessageBox.alert('Problem', 'Mottok ikke data fra tjeneren som forventet');
        }
        else {
            Ext.getCmp('save-button').enable();
        }
    },
    /**
     * @param {Ext.form.Basic} form
     * @param {Ext.form.action.Action} action
     */
    failed: function(form, action) {
        if (action.failureType === "connect") {
            Ext.MessageBox.alert('Problem:', 'Klarte ikke laste data. Fikk ikke kontakt med tjeneren.');
        }
        else {
            if (!action.response.responseText) {
                Ext.MessageBox.alert('Problem:', 'Skjemaet mottok ikke data i JSON-format som forventet');
            }
            else {
                var result = Ext.JSON.decode(action.response.responseText, true);
                if (result && result.msg) {
                    Ext.MessageBox.alert('Mottatt tilbakemelding om feil:', action.result.msg);
                }
                else {
                    Ext.MessageBox.alert('Problem:', 'Innhenting av data mislyktes av ukjent grunn. (trolig manglende success-parameter i den returnerte datapakken). Action type='+action.type+', failure type='+action.failureType);
                }
            }
        }
    }
};

leiebasen.formSubmitAction = {
    /**
     * @param {Ext.form.Basic} form
     * @param {Ext.form.action.Action} action
     */
    complete: function(form, action) {
        var success;
        var msg;
        var url;
        if (action.response.responseText === '') {
            Ext.MessageBox.alert('Problem', 'Mottok ikke bekreftelsesmelding fra tjeneren  i JSON-format som forventet');
        } else {
            try {
                var responsObjekt = Ext.JSON.decode(action.response.responseText);
                success = (typeof responsObjekt.success != 'undefined') ? action.result.success : false;
                msg = (typeof responsObjekt.msg === 'string') ? responsObjekt.msg : 'Opplysningene er lagret';
                url = (typeof responsObjekt.url === 'string') ? action.result.url : false;
            } catch (e) {
                success = false;
                msg = action.response.responseText;
            }
            Ext.MessageBox.alert(success ? 'Suksess' : 'Problem', msg, function() {
                if (url) {
                    window.location = url;
                }
            });
        }
    },

    /**
     * @param {Ext.form.Basic} form
     * @param {Ext.form.action.Action} action
     */
    failed: function(form, action) {
        var msg;
        var url;
        if (action.failureType === "client") {
            Ext.MessageBox.alert('Problem:', 'Sjekk skjemaet for feil.');
        }
        else if (action.failureType === "connect") {
            Ext.MessageBox.alert('Problem:', 'Det er problemer med forbindelsen. Klarte ikke lagre.');
        }
        else if (!action.response.responseText) {
            Ext.MessageBox.alert('Problem:', 'Mottok blank respons fra tjeneren. Action type=' + action.type + ', failure type=' + action.failureType);
        }
        else {
            try {
                var responsObjekt = Ext.JSON.decode(action.response.responseText);
                msg = (typeof responsObjekt.msg === 'string') ? responsObjekt.msg : 'Klarte ikke lagre';
                url = (typeof responsObjekt.url === 'string') ? action.result.url : false;
            } catch (e) {
                msg = action.response.responseText;
            }
            if (msg) {
                Ext.MessageBox.alert('Problem', msg, function() {
                    if (url) {
                        window.location = url;
                    }
                });
            }
            else if (url){
                window.location = url;
            }
        }
    }
}

leiebasen.kjerne.kr = function(beløp, html = true, prefiks = true) {
    let formatter = new Intl.NumberFormat('nb-NO', { style: 'currency', currency: 'NOK' });
    let resultat = formatter.format(beløp).replace(',00', ',–');
    if (html) {
        return '<span>' + resultat.replace(" ", '&nbsp;') + '</span>';
    }
    return prefiks ? resultat : resultat.replace('kr ', '');
}

leiebasen.kjerne.prosent = function(verdi, antallDesimaler = 1, html = true) {
    let formatter = new Intl.NumberFormat('nb-NO', { style: 'percent', maximumFractionDigits: antallDesimaler });
    let resultat = formatter.format(verdi);
    if (html) {
        return '<span>' + resultat.replace(" ", '&nbsp;') + '</span>';
    }
    return resultat;
}

Ext.onReady(function() {
    leiebasen.extJs = this;

    /* 5 min timeouts */
    Ext.override(Ext.data.Connection, {timeout: 300000});
    Ext.override(Ext.data.proxy.Ajax, { timeout: 300000 });
    Ext.override(Ext.form.action.Action, { timeout: 300 });
    Ext.Ajax.timeout = 300000;

    <?php echo $extOnReady;?>
});