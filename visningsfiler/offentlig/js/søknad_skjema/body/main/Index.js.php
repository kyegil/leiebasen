<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\offentlig\js\søknad_skjema\body\main\Index $this
 * @var string $script
 */
?>
leiebasen.søknadsskjema = {
    oppdaterPåkrevdeFelter: function() {
        console.log("Oppdaterer påkrevde felter");
        var epostAdressefelt = $("input#kontakt_epost_felt");
        var egenkopiKnapp = $("input#egenkopi_knapp");
        var telefonFelt = $("input#kontakt_telefon_felt");
        var opprettProfilKnapp = $("input#profil_seksjon-input");
        var epost = epostAdressefelt.length ? epostAdressefelt.val() : '';
        var egenkopi = egenkopiKnapp.length && egenkopiKnapp.prop('checked');
        var telefon = telefonFelt.length ? telefonFelt.val() : '';
        var opprettProfil = opprettProfilKnapp.prop('checked');

        epostAdressefelt.prop('required', true);
        telefonFelt.prop('required', true);

        if(epost.length) {
            telefonFelt.prop('required', false);
        }
        if(telefon.length) {
            epostAdressefelt.prop('required', egenkopi);
        }
    }
};
<?php echo $script;?>