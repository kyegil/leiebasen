<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\offentlig\js\register_bruker_skjema\Index $this
 */
?>
leiebasen.offentlig.registerBrukerSkjema = {
    oppdaterPåkrevdeFelter: function() {
        console.log("Oppdaterer påkrevde felter");
        let identifiseringsmetodeFelt = $('input[name="identifiseringsmetode"]:checked');
        let fornavnFelt = $('input[name="fornavn"]');
        let etternavnFelt = $('input[name="etternavn"]');
        let epostAdresseFelt = $('input[name="e-post"]');
        let telefonFelt = $('input[name="telefon"]');
        let pw1Felt = $('input[name="passord[0]"]');
        let pw2Felt = $('input[name="passord[1]"]');
        let navnFeltsett = $('#navnfeltsett');
        let passordFeltsett = $('#passordFeltsett');

        let regningsdatoFelt = $('input[name="regningsdato"]');
        let kidFelt = $('input[name="kid"]');
        let regningsbeløpFelt = $('input[name="regningsbeløp"]');
        let framleierFelt = $('input[name="er_framleier"]');
        let leietakerFelt = $('input[name="leietaker_navn"]');
        let regningFeltsett = $('#regningsfeltsett');


        let identifiseringsmetode = identifiseringsmetodeFelt.length ? identifiseringsmetodeFelt[0].value : '';

        fornavnFelt.prop('required', false);
        etternavnFelt.prop('required', false);
        epostAdresseFelt.prop('required', false);
        telefonFelt.prop('required', false);
        regningsdatoFelt.prop('required', false);
        kidFelt.prop('required', false);
        regningsbeløpFelt.prop('required', false);
        regningFeltsett.hide();

        pw1Felt.prop('required', false);
        pw2Felt.prop('required', false);
        navnFeltsett.hide();
        passordFeltsett.hide();
        $('input[name="identifiseringsmetode"]').prop('required', identifiseringsmetode === '');
        switch (identifiseringsmetode) {
            case 'e-post':
                epostAdresseFelt.prop('required', true);
                break;
            case 'sms':
                telefonFelt.prop('required', true);
                break;
            case 'regning':
                fornavnFelt.prop('required', true);
                etternavnFelt.prop('required', true);
                pw1Felt.prop('required', true);
                pw2Felt.prop('required', true);
                regningsdatoFelt.prop('required', true);
                kidFelt.prop('required', true);
                regningsbeløpFelt.prop('required', true);

                navnFeltsett.show();
                regningFeltsett.show();
                passordFeltsett.show();
                break;
            default:
                break;
        }
        let erFramleier = framleierFelt.is(':visible') && framleierFelt.is(':checked');
        leietakerFelt.prop('required', erFramleier);
        leietakerFelt.parent().toggle(erFramleier);
    }
};

$('input[name="identifiseringsmetode"]').on('change', function(){
    leiebasen.offentlig.registerBrukerSkjema.oppdaterPåkrevdeFelter();
});
$('input[name="er_framleier"]').on('change', function(){
    leiebasen.offentlig.registerBrukerSkjema.oppdaterPåkrevdeFelter();
});
//$('#registrer_bruker_skjema').validate({
//    rules: {
//        'passord[0]': {
//            required: true,
//            minlength: 8
//        },
//        'passord[1]': {
//            required: true,
//            minlength: 8,
//            equalTo: "passord[0]"
//        }
//    }
//});
$('input[name=passord\\[1\\]]').keyup(function () {
    'use strict';

    if ($('input[name="passord[0]"]').val() === $(this).val()) {
        this.setCustomValidity('');
    } else {
        this.setCustomValidity('Passwords must match');
    }
});
leiebasen.offentlig.registerBrukerSkjema.oppdaterPåkrevdeFelter();
