<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\offentlig\html\innlogging_skjema\Index $this
 * @var string $skjema
 * @var string $reCaptchaSiteKey
 * @var string $redirectUrl
 * @var \Kyegil\Leiebasen\Visning|string $knapper
 */
?><?php echo $skjema;?>
<?php echo $knapper;?>