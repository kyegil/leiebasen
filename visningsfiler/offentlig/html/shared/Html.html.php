<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\offentlig\html\shared\Html $this
 * @var string $lang
 * @var \Kyegil\ViewRenderer\ViewInterface $head
 * @var \Kyegil\ViewRenderer\ViewInterface $header
 * @var \Kyegil\ViewRenderer\ViewInterface $hovedmeny
 * @var \Kyegil\ViewRenderer\ViewInterface $brukermeny
 * @var \Kyegil\ViewRenderer\ViewInterface $main
 * @var \Kyegil\ViewRenderer\ViewInterface $footer
 * @var \Kyegil\ViewRenderer\ViewInterface $scripts
 */
?><!DOCTYPE html>
<html class="js no-touchevents" style="" lang="<?php echo $lang;?>">
    <head>
        <?php echo $head;?>
    </head>

    <body class="oppslag leiebasen-custom-logo">

        <?php echo $header;?>

        <?php echo $hovedmeny;?>

        <?php echo $brukermeny;?>

        <?php echo $main;?>

        <?php echo $footer;?>

        <?php if($scripts):?>
            <script>
            <?php echo $scripts;?>
            </script>
        <?php endif;?>
    </body>
</html>