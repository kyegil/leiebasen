<?php
    /**
     * Leiebasen Visningsblokk
     *
     * @var \Kyegil\Leiebasen\Visning\offentlig\html\shared\body\Main $this
     * @var \Kyegil\ViewRenderer\ViewInterface $tittel
     * @var \Kyegil\ViewRenderer\ViewInterface $innhold
     * @var \Kyegil\ViewRenderer\ViewInterface $breadcrumbs
     * @var \Kyegil\ViewRenderer\ViewInterface $varsler
     * @var \Kyegil\ViewRenderer\ViewInterface $tilbakeknapp
     */
?><main id="site-content">
    <article class="section-inner page">
        <?php echo $breadcrumbs;?>

        <?php echo $varsler;?>

        <header class="article-header">
            <h1 class="entry-title"><?php echo $tittel;?></h1>
        </header><!-- .article-header -->

        <div class="article-inner">
            <div class="entry-content">
                <?php echo $innhold;?>
            </div><!-- .entry-content -->
        </div><!-- .article-inner -->
        <?php echo $tilbakeknapp;?>
    </article><!-- .post -->
</main><!-- #site-content -->
