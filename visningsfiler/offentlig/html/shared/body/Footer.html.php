<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\offentlig\html\shared\body\Footer $this
 * @var \Kyegil\ViewRenderer\ViewInterface $head
 * @var \Kyegil\ViewRenderer\ViewInterface $menu
 */
?><footer id="site-footer" role="contentinfo">
    <div class="footer-inner section-inner">
    </div><!-- .footer-bottom -->
</footer><!-- #site-footer -->
