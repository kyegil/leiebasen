<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\offentlig\html\shared\body\Header $this
 * @var \Kyegil\Leiebasen\Visning|string $språkvelger
 * @var string $logo
 * @var string $logoWidth
 * @var string $logoHeight
 * @var string $navn
 */
?><header id="offentlig-header">
    <div class="header-inner">
        <div class="section-inner">
            <div class="header-titles">
                <div class="site-logo faux-heading">
                    <a href="/offentlig/index.php" rel="home" class="custom-logo">
                        <img
                            alt="<?php echo $navn;?>"
                            src="<?php echo $logo;?>"
                            width="<?php echo $logoWidth;?>"
                            height="<?php echo $logoHeight;?>"
                        >
                    </a>
                </div>
                <div class="site-description"><?php echo $navn;?></div>
            </div><!-- .header-titles -->

            <div class="header-navigation-wrapper">
                <div class="header-toggles hide-no-js">
                    <span class="språkvelger"><?php echo $språkvelger;?></span>
                </div><!-- .header-toggles -->
            </div><!-- .header-navigation-wrapper -->
        </div><!-- .section-inner -->
    </div><!-- .header-inner -->
</header><!-- #min-side-header -->
