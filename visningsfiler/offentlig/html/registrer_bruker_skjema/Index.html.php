<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\offentlig\html\registrer_bruker_skjema\Index $this
 * @var string $skjema
 * @var string $reCaptchaSiteKey
 * @var string $redirectUrl
 * @var \Kyegil\Leiebasen\Visning|string $knapper
 */
?><?php echo $skjema;?>
<?php echo $knapper;?>