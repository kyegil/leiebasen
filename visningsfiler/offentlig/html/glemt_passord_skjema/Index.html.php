<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\offentlig\html\glemt_passord_skjema\Index $this
 * @var string $skjema
 * @var \Kyegil\Leiebasen\Visning|string $knapper
 */
?><div>Ved innlogging for første gang, eller om du har glemt passordet ditt, kan du få en engangsbillett tilsendt til e-postadressen du er registrert med.<br>
    Billetten er kun gyldig i 6 timer, og kan brukes for å finne eller endre brukernavnet ditt, eller for å gi deg selv et nytt passord.</div>

    <div>Denne tjenesten hjelper kun dersom du allerede har en brukerprofil.</div><?php echo $skjema;?>
<?php echo $knapper;?>