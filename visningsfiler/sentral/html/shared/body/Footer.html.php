<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\sentral\html\shared\body\Footer $this
 * @var \Kyegil\ViewRenderer\ViewInterface $head
 * @var \Kyegil\ViewRenderer\ViewInterface $menu
 */
?><footer id="site-footer" role="contentinfo">
    <div class="footer-inner section-inner">
        <div class="footer-credits">
            <p class="om-leiebasen color-secondary"><a href="https://docs.google.com/document/d/1R5SYhkNkjdplY4INo9yDAWyz3IiayhwAjcdufe8gr8E/" target="_blank">Om Leiebasen</a></p><!-- .om-leiebasen -->
        </div><!-- .footer-credits -->
    </div><!-- .footer-bottom -->
</footer><!-- #site-footer -->
