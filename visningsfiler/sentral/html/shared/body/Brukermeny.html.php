<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\sentral\html\shared\body\Brukermeny $this
 */
?><div class="menu-modal brukermeny cover-modal" data-modal-target-string=".brukermeny" aria-expanded="false">
    <div class="menu-modal-inner modal-inner bg-body-background">
        <div class="menu-wrapper section-inner">
            <div class="menu-top">
                <div class="menu-modal-toggles header-toggles">
                    <a href="#"
                       class="toggle nav-toggle nav-untoggle"
                       data-toggle-target=".brukermeny"
                       data-toggle-screen-lock="true"
                       data-toggle-body-class="showing-menu-modal"
                       aria-pressed="false"
                       data-set-focus="#sentral-header .nav-toggle"
                    >
                        <div class="bars">
                            <img src="/pub/media/bilder/sentral/profil.png" alt="profil">
                        </div><!-- .bars -->
                    </a><!-- .nav-toggle -->
                </div><!-- .menu-modal-toggles -->

                <ul class="main-menu reset-list-style">
                    <li class="menylenke menylenke_has_children menu-item-has-children"><a href="/sentral/index.php?oppslag=profil_skjema&returi=default">Min brukerprofil</a>
                        <ul class="children">
                            <li class="menylenke"><a href="/sentral/index.php?oppslag=profil_passord_skjema&returi=default">Endre passord</a></li>
                        </ul>
                    </li>
                    <li class="menylenke current_page_ancestor current_page_parent"><a href="/offentlig/index.php?oppslag=index&oppdrag=avslutt">Logg ut</a></li>
                </ul><!-- .main-menu -->
            </div><!-- .menu-top -->
        </div><!-- .menu-wrapper -->
    </div><!-- .menu-modal-inner -->
</div><!-- .menu-modal -->
