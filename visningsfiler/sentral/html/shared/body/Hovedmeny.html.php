<?php
/**
 * Leiebasen Visningsblokk
 *
 */
?><div class="menu-modal hovedmeny cover-modal" data-modal-target-string=".hovedmeny" aria-expanded="false">
    <div class="menu-modal-inner modal-inner bg-body-background">
        <div class="menu-wrapper section-inner">
            <div class="menu-top">
                <div class="menu-modal-toggles header-toggles">
                    <a href="#"
                       class="toggle nav-toggle nav-untoggle"
                       data-toggle-target=".hovedmeny"
                       data-toggle-screen-lock="true"
                       data-toggle-body-class="showing-menu-modal"
                       aria-pressed="false"
                       data-set-focus="#sentral-header .nav-toggle"
                    >
                        <div class="bars">
                            <div class="bar"></div>
                            <div class="bar"></div>
                            <div class="bar"></div>
                        </div><!-- .bars -->
                    </a><!-- .nav-toggle -->
                </div><!-- .menu-modal-toggles -->

                <ul class="main-menu reset-list-style">
                </ul><!-- .main-menu -->
            </div><!-- .menu-top -->

            <div class="menu-bottom">
<!--                <p class="menu-copyright"><a href="https://docs.google.com/document/d/1R5SYhkNkjdplY4INo9yDAWyz3IiayhwAjcdufe8gr8E/" target="_blank">Om Leiebasen</a></p>-->
            </div><!-- .menu-bottom -->
        </div><!-- .menu-wrapper -->
    </div><!-- .menu-modal-inner -->
</div><!-- .menu-modal -->
