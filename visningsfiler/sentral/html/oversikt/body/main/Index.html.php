<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\sentral\html\oversikt\body\main\Index $this
 */
?><h3>Velg område</h3>
<div class="dashboard rutenett">
    <div class="flis">
        <a href="/mine-sider/index.php">
            <div class="rutenett-knapp">
<!--                <div class="ikonbeholder">-->
<!--                    <img src="/pub/media/bilder/sentral/oversiktsfliser/mine-sider-trn.svg" height="414" width="421" alt="mine sider">-->
<!--                </div>-->
                <h3 class="flistekst">mine sider</h3>
            </div>
        </a>
    </div>

    <div class="flis">
        <a href="/egeninnsats/rekordoj/aldonu">
            <div class="rutenett-knapp">
<!--                <div class="ikonbeholder">-->
<!--                    <img src="/pub/media/bilder/sentral/oversiktsfliser/egeninnsats-trn.svg" height="414" width="421" alt="egeninnsats">-->
<!--                </div>-->
                <h3 class="flistekst">egeninnsats</h3>
            </div>
        </a>
    </div>

    <div class="flis">
        <a href="/drift/index.php">
            <div class="rutenett-knapp">
<!--                <div class="ikonbeholder">-->
<!--                    <img src="/pub/media/bilder/sentral/oversiktsfliser/drift-trn.svg" height="414" width="421" alt="drift">-->
<!--                </div>-->
                <h3 class="flistekst">drift</h3>
            </div>
        </a>
    </div>

    <div class="flis">
        <a href="/flyko/index.php">
            <div class="rutenett-knapp">
<!--                <div class="ikonbeholder">-->
<!--                    <img src="/pub/media/bilder/sentral/oversiktsfliser/flyko-trn.svg" height="414" width="421" alt="flyko">-->
<!--                </div>-->
                <h3 class="flistekst">flyko</h3>
            </div>
        </a>
    </div>
</div>
