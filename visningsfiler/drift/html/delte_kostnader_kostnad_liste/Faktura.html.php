<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_kostnad_liste\Faktura $this
 * @var string $fakturaId
 * @var string $fakturanummer
 * @var string $beløp
 * @var int $tjenesteId
 * @var string $tjenesteNavn
 * @var string $tjenesteBeskrivelse
 * @var string $fraDato
 * @var string $tilDato
 * @var string $termin
 * @var string $forbruk
 * @var boolean $beregnet
 * @var boolean $fordelt
 * @var \Kyegil\Leiebasen\Visning|string $fordeling
 * @var string $fordeltBeløp
 */
?><dl class="kostnadsdeling egenskaper <?php echo $fordelt ? 'fordelt' : '';?>">
    <dt>Tjeneste</dt>
    <dd><a href="/drift/index.php?oppslag=delte_kostnader_tjeneste_kort&id=<?php echo $tjenesteId;?>"><?php echo $tjenesteNavn;?></a></dd>
    <dt>Faktura</dt>
    <dd><?php echo $fakturanummer;?></dd>
    <dt>Termin</dt>
    <dd><?php echo $termin;?></dd>
    <dt>Fra og med</dt>
    <dd><?php echo $fraDato;?></dd>
    <dt>Til og med</dt>
    <dd><?php echo $tilDato;?></dd>
    <dt>Forbruk</dt>
    <dd><?php echo $forbruk;?></dd>
    <dt>Regningsbeløp</dt>
    <dd><?php echo $beløp;?></dd>
</dl>

<table class="kostnadsdeling rutenett">
    <caption><?php echo $fordelt ? "Fordeling" : "Forslag til fordeling";?></caption>
    <thead>
        <tr>
            <td>Leieforhold</td>
            <td>Andel</td>
            <td>Beløp</td>
        </tr>
    </thead>
    <tbody>
    <?php echo $fordeling;?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2">Fordelt Beløp</td>
            <td class="beløp"><strong><?php echo $fordeltBeløp;?></strong></td>
        </tr>
    </tfoot>
</table>
