<?php
/**
 * Leiebasen Oppfølgingsnotater
 *
 * @var \Kyegil\Leiebasen\Visning\drift\html\delte_kostnader_kostnad_liste\Index $this
 * @var string|\Kyegil\Leiebasen\Visning $varsler
 * @var string|\Kyegil\Leiebasen\Visning $tabell
 * @var string|\Kyegil\Leiebasen\Visning $knapper
 */
?>
<?php echo $varsler;?>
<?php echo $tabell;?>
<?php echo $knapper;?>