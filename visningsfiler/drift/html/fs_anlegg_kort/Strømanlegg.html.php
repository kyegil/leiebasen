<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\html\fs_anlegg_kort\Strømanlegg $this
 * @var string $tjenesteId
 * @var string $navn
 * @var string $formål
 * @var string $leverandør
 * @var string $anleggsnummer
 * @var string $målernummer
 * @var string $plassering
 */
?><h1><?php echo $navn;?></h1>
<dl>
    <dt>Leverandør</dt>
    <dd><?php echo $leverandør;?></dd>
    <dt>Anleggsnummer</dt>
    <dd><?php echo $anleggsnummer;?></dd>
    <dt>Målernummer</dt>
    <dd><?php echo $målernummer;?></dd>
    <dt>Målerplassering</dt>
    <dd><?php echo $plassering;?></dd>
    <dt>Brukes til</dt>
    <dd><?php echo $formål;?></dd>
</dl>