<?php
/**
 * Leiebasen Visningsmal
 *
 * @var Visning\drift\html\delte_kostnader_tjeneste_kort\extjs4\regninger\regning\Andel $this
 * @var string $andelId
 * @var string $kravId
 * @var string $beløp
 * @var string $andelbeskrivelse
 * @var string $leieforholdId
 * @var string $leieforholdBeskrivelse
 */
use Kyegil\Leiebasen\Visning;
?><tr>
    <td><a
        title="Gå til leieforholdet"
        href="/drift/index.php?oppslag=leieforholdkort&id=<?php echo $leieforholdId;?>"><?php echo $leieforholdBeskrivelse;?></a></td>
    <td>
        <?php if($kravId):?>
            <a
                title="Vis kravet for denne andelen"
                href="/drift/index.php?oppslag=krav_kort&id=<?php echo $kravId;?>"><?php echo $andelbeskrivelse;?></a>
        <?php else:?>
            <?php echo $andelbeskrivelse;?>
        <?php endif;?>
    </td>
    <td>
        <?php echo $beløp;?>
    </td>
</tr>