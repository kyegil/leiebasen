<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\html\leieobjekt_kort\Index $this
 * @var string $leieobjektId
 * @var boolean $aktivert
 * @var string $navn
 * @var string $gateadresse
 * @var string $etasje
 * @var string $beskrivelse
 * @var string $postnummer
 * @var string $poststed
 * @var string $bilde
 * @var string $erBolig
 * @var string $areal
 * @var string $antallRom
 * @var boolean $bad
 * @var string $toalett
 * @var string $toalettKategori
 * @var string $merknader
 * @var string $leieberegningsId
 * @var string $leieberegningsNavn
 * @var string $leieberegningsFormel
 * @var string $bygningsId
 * @var string $bygningsKode
 * @var string $bygningsNavn
 * @var string $leieforholdfelt
 * @var string $leietakere
 * @var string $områder
 * @var string $egendefinerteFelter
 */
?>
<div>
    <?php if(trim($navn)):?>
        <h1><?php echo $navn;?></h1>
        <p><?php echo $gateadresse;?></p>
    <?php else:?>
        <h1><?php echo $gateadresse;?></h1>
    <?php endif;?>
</div>
<div>
    <?php if(trim($bilde)):?>
        <img src="<?php echo $bilde;?>" alt="Leieobjekt <?php echo $leieobjektId;?>" style="float: right; width: 200px; margin: 10px;">
    <?php endif;?>
    <strong><?php if(trim($etasje)):?><?php echo $etasje;?><?php endif;?><?php echo $beskrivelse;?></strong><br>
    <?php echo $postnummer;?> <?php echo $poststed;?><br>
    <dl>
        <dt>Aktivert</dt>
        <dd>Leieobjektet <?php if($aktivert):?>er <?php else:?>er <i>ikke</i> <?php endif;?>aktivert for utleie</dd>

        <dt>Leies ut som</dt>
        <dd><?php if($erBolig):?>Bolig<?php else:?>Nærings- eller annet lokale<?php endif;?></dd>

        <dt>Areal</dt>
        <dd><?php echo $areal ? "{$areal}&nbsp;㎡" : 'Ikke angitt';?></dd>

        <dt>Antall rom</dt>
        <dd><?php echo $antallRom ?: 'Ikke angitt';?></dd>

        <dt>Tilgang til bad</dt>
        <dd><?php echo $bad ? 'Ja' : 'Nei';?></dd>

        <dt>Toalett</dt>
        <dd><?php echo $toalett;?></dd>

        <dt>Leieberegning</dt>
        <dd><?php echo $leieberegningsNavn;?><br>
            <?php echo $leieberegningsFormel;?></dd>

        <dt>Leietakere</dt>
        <dd><?php echo $leietakere;?></dd>

        <?php if(trim($bygningsId)):?>
            <dt>Bygning</dt>
            <dd><?php echo $bygningsKode;?> <?php echo $bygningsNavn;?></dd>
        <?php endif;?>

        <?php if(trim($områder)):?>
            <dt>Områder</dt>
            <dd><?php echo $områder;?></dd>
        <?php endif;?>

        <?php echo $egendefinerteFelter;?>

        <?php if(trim($merknader)):?>
        <dt>Andre merknader</dt>
        <dd><?php echo $merknader;?></dd>
        <?php endif;?>
    </dl>
</div>