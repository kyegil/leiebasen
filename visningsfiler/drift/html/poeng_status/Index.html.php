<?php
/**
 * Leiebasen visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\html\poeng_status\Index $this
 * @var string|\Kyegil\Leiebasen\Visning $varsler
 * @var string|\Kyegil\Leiebasen\Visning $innhold
 * @var string|\Kyegil\Leiebasen\Visning $knapper
 */
?>
<?php echo $varsler;?>
<?php echo $innhold;?>
<?php echo $knapper;?>