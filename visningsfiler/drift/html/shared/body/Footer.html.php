<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\drift\html\shared\body\Footer $this
 */
?><footer id="site-footer" role="contentinfo">
    <div class="footer-inner section-inner">
        <div class="footer-credits">
            <p class="om-leiebasen color-secondary"><a href="https://docs.google.com/document/d/1R5SYhkNkjdplY4INo9yDAWyz3IiayhwAjcdufe8gr8E/" target="_blank">Om Leiebasen</a></p><!-- .om-leiebasen -->
        </div><!-- .footer-credits -->
    </div><!-- .footer-bottom -->
</footer><!-- #site-footer -->
