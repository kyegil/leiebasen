<?php
    /**
     * Leiebasen Visningsblokk
     *
     * @var \Kyegil\Leiebasen\Visning\drift\html\shared\body\main\Content $this
     * @var string $heading
     * @var \Kyegil\ViewRenderer\ViewInterface $mainBodyContents
     * @var \Kyegil\ViewRenderer\ViewInterface $footerContents
     * @var \Kyegil\ViewRenderer\ViewInterface $buttons
     */
?><div id="scrollable-area">
    <div class="main-header">
        <?php if(trim($heading)):?>
        <h1><?php echo $heading;?></h1>
        <?php endif;?>
        <div class="dynamic-notice"></div>
    </div>
    <div class="main-body"><?php echo $mainBodyContents;?></div>
</div>
<?php if(trim($footerContents)):?>
    <div id="fixed-footer"><?php echo $footerContents;?></div>
<?php endif;?>
