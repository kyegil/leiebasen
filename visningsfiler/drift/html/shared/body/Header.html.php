<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\drift\html\shared\body\Header $this
 * @var \Kyegil\Leiebasen\Visning|string $språkvelger
 * @var string $logo
 * @var string $logoWidth
 * @var string $logoHeight
 * @var string $brukermeny
 * @var string $hovedmeny
 * @var string $varsler
 * @var string $navn
 */
?><header id="drift-header">
    <div class="left-header">
        <a href="#" class="toggle hovedmeny-toggle">
            <div class="bars">
                <div class="bar"></div>
                <div class="bar"></div>
                <div class="bar"></div>
            </div>
        </a>
    </div>
    <div class="right-header">
        <?php echo $varsler;?>
        <?php echo $brukermeny;?>
    </div>
</header><!-- #drift-header -->
