<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\drift\html\shared\Head $this
 * @var \Kyegil\Leiebasen\Visning | string $title
 * @var \Kyegil\Leiebasen\Visning | string $extPath
 * @var \Kyegil\Leiebasen\Visning | string $extraMetaTags
 * @var \Kyegil\Leiebasen\Visning | string $links
 * @var \Kyegil\Leiebasen\Visning | string $scripts
 */
?><meta http-equiv="content-type" content="text/html; charset=UTF-8" charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
<meta name="msapplication-TileImage" content="/pub/media/bilder/cropped-favicon-270x270.png">
<meta name="robots" content="noindex, nofollow">
<?php echo $extraMetaTags?>

<title><?php echo $title?></title>

<?php echo $links?>

<link rel="stylesheet" href="/pub/lib/jquery-ui-1.12.1/jquery-ui.min.css" type="text/css" media="screen">

<!--ExtJs styling -->
<?php if (trim($extPath)):?>
<link rel="stylesheet" type="text/css" href="<?php echo $extPath;?>/resources/css/ext-all.css" media="screen">

    <?php if (strpos($extPath, 'ext-3.4.0') !== false ):?>
    <!--Ext 3 theme-->
    <link rel="stylesheet" type="text/css" href="<?php echo $extPath;?>/resources/css/xtheme-gray.css" media="screen">
    <?php else:?>
    <!--Ext 4 theme-->
    <link rel="stylesheet" type="text/css" href="<?php echo $extPath;?>/resources/css/ext-all-gray.css" media="screen">
    <?php endif;?>

<?php endif;?>

<!--Select2 styling-->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css">

<!--Left bar menu styling-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500&display=swap">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link rel="stylesheet" href="/pub/css/jquery.mCustomScrollbar.min.css">
<!--<link rel="stylesheet" href="/pub/css/animate.css">-->

<!--DataTables styling including the following features:
* DataTables 2.1.8
* Fixed Header 4.0.1
* Buttons 3.2.0
* * Column visibility
* * HTML5 export
* * Print view
* *
-->
<link rel="stylesheet" href="https://cdn.datatables.net/v/dt/dt-2.1.8/b-3.2.0/b-colvis-3.2.0/b-html5-3.2.0/b-print-3.2.0/fh-4.0.1/r-3.0.3/datatables.min.css">

<link rel="stylesheet" href="/pub/css/drift.css" type="text/css" media="all">
<link rel="stylesheet" id="print-styles-css" href="/pub/css/print.css" type="text/css" media="print">

<!--Favicons-->
<link rel="icon" href="/pub/custom/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" href="/pub/custom/favicon/favicon-192x192.png" sizes="192x192">
<link rel="apple-touch-icon" href="/pub/custom/favicon/favicon-180x180.png">

<script src="/pub/js/jquery-3.5.1.js"></script>
<script src="/pub/lib/jquery-ui-1.12.1/jquery-ui.js"></script>

<!--ExtJs library -->
<?php if (trim($extPath)):?>
    <?php if (strpos($extPath, 'ext-3.4.0') !== false ):?>
    <script src="<?php echo $extPath;?>/adapter/ext/ext-base.js"></script>
    <?php endif;?>

    <script src="<?php echo $extPath;?>/ext-all.js"></script>
    <?php if(strpos($extPath, 'ext-3.4.0') !== false ):?>
        <script src="/pub/lib/ext-ux/GroupSummary.js"></script>
        <script src="/pub/lib/ext-ux/RowExpander.js"></script>
        <script src="<?php echo $extPath;?>/src/locale/ext-lang-no_NB.js"></script>
    <?php else:?>
        <script src="<?php echo $extPath;?>/locale/ext-lang-no_NB.js"></script>
    <?php endif;?>

    <script src="/pub/js/fellesfunksjoner.js"></script>
<?php endif;?>

<!--DataTables library including the following features:
* DataTables 2.1.8
* Fixed Header 4.0.1
* Buttons 3.2.0
* * Column visibility
* * HTML5 export
* * Print view
* *
-->
<script src="https://cdn.datatables.net/v/dt/dt-2.1.8/b-3.2.0/b-colvis-3.2.0/b-html5-3.2.0/b-print-3.2.0/fh-4.0.1/r-3.0.3/datatables.min.js"></script>

<!--Select2-->
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<!--Left bar menu-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<!--<script src="/pub/lib/jquery.backstretch.min.js"></script>-->
<script src="/pub/lib/wow.min.js"></script>
<script src="/pub/lib/jquery.waypoints.min.js"></script>
<script src="/pub/lib/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/pub/lib/scripts.js"></script>

<script src="/pub/js/construct.js"></script>
<script>
    var leiebasen = leiebasen || {kjerne: {},};
    leiebasen.kjerne = leiebasen.kjerne || {};
    leiebasen.drift = leiebasen.drift || {};
</script>
<?php echo $scripts?>
