<?php
/**
 * Leiebasen Visningsmal
 *
 * @var Visning\drift\html\krav_kort\extjs4\panel\krav\Innbetaling $this
 * @var int $delbeløpId
 * @var string $transaksjonsId
 * @var string $betaler
 * @var string $dato
 * @var string $konto
 * @var string $beløp
 * @var string $referanse
 */
use Kyegil\Leiebasen\Visning;
?><?php echo $beløp;?>
<?php if($konto != '0'):?>
    betalt
    <?php echo($betaler ? "av {$betaler} " : "");?>
    <?php echo "den {$dato}";?>
<?php else:?>
    kreditt
<?php endif;?>
<a title="Gå til betalingskortet" href="/drift/index.php?oppslag=innbetalingskort&id=<?php echo $transaksjonsId?>"> (ref. <?php echo $referanse?>)</a><br>
