<?php
/**
 * Leiebasen Visningsmal
 *
 * @var Visning\drift\html\krav_kort\extjs4\panel\krav\Purring $this
 * @var string $dato
 * @var string $purremåte
 */
use Kyegil\Leiebasen\Visning;
?>
Purret per <?php echo $purremåte;?> den <?php echo $dato;?><br>
