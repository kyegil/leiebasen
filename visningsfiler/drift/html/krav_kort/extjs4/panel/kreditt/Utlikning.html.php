<?php
/**
 * Leiebasen Visningsmal
 *
 * @var Visning\drift\html\krav_kort\extjs4\panel\kreditt\Utlikning $this
 * @var int $utlikningsId
 * @var Visning|string $utlikningsbeskrivelse
 * @var int $leieforholdId
 * @var float $beløp
 * @var string $beløpFormatert
 * @var bool $avstemt
 */
use Kyegil\Leiebasen\Visning;
?>
<tr>
    <td class="value"><?php echo $beløpFormatert;?></td>
    <td><?php echo $utlikningsbeskrivelse;?></td>
    <td>
        <?php if($avstemt):?>
            <a style="cursor: pointer;" onClick="leiebasen.frakople(<?php echo $utlikningsId;?>)">[Løsne]</a>
        <?php else:?>
            <a style="cursor: pointer;"
               onClick="leiebasen.kople(<?php echo $utlikningsId;?>, <?php echo $beløp;?>, <?php echo($leieforholdId ?: 'null');?>)">
                [Kople]
            </a>
        <?php endif;?>
    </td>
</tr>
