<?php
/**
 * Leiebasen Visningsmal
 *
 * @var Visning\drift\html\krav_kort\extjs4\panel\Kreditt $this
 * @var string $kravId
 * @var string $reellKravId
 * @var string $kravTekst
 * @var string $kravDato
 * @var string $kravBeløp
 * @var string $kravtype
 * @var string $utestående
 * @var string $leieforholdId
 * @var string $leieforholdBeskrivelse
 * @var string $leieobjektId
 * @var string $leieobjektBeskrivelse
 * @var string $strømanleggId,
 * @var string $andel
 * @var string $termin
 * @var string $fom
 * @var string $tom
 * @var string $regningsnummer
 * @var string $utskriftsdato
 * @var string $forfall
 * @var string $kid
 * @var string $fastKid
 * @var Visning|string $delkrav
 * @var Visning|string $utlikninger
 */

use Kyegil\Leiebasen\Visning;

?><h1><?php echo $kravTekst;?></h1>
<p>
    <a title="Gå til leieforholdet." href="/drift/index.php?oppslag=leieforholdkort&id=<?php echo $leieforholdId;?>">
        <?php echo $leieforholdBeskrivelse;?>
    </a>
</p>
<table>
    <tr>
        <td style="vertical-align:top; padding:0 20px;">
            <table>
                <tr>
                    <td style="font-weight:bold;">Reell krav-id:</td>
                    <td style="text-align:right;"><?php echo $reellKravId;?></td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Dato:</td>
                    <td style="text-align:right;"><?php echo $kravDato;?></td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Beløp:</td>
                    <td style="text-align:right;"><?php echo $kravBeløp;?></td>
                </tr>

                <?php echo $delkrav;?>

                <tr>
                    <td style="font-weight:bold;">Utestående:</td>
                    <td style="text-align:right;"><?php echo $utestående;?></td>
                </tr>
            </table>
        </td>
        <td style="vertical-align:top; padding:0 20px;">
            <table>
                <tr>
                    <td style="font-weight:bold;">Kravtype:</td>
                    <td style="text-align:right;"><?php echo $kravtype;?></td>
                </tr>

                <?php if($kravtype == \Kyegil\Leiebasen\Modell\Leieforhold\Krav::TYPE_HUSLEIE):?>
                    <tr>
                        <td style="font-weight:bold;">Leieobjekt:</td>
                        <td style="text-align:right;">
                            <a title="Gå til leieobjektkortet."
                               href="/drift/index.php?oppslag=leieobjekt_kort&id=<?php echo $leieobjektId;?>"><?php echo $leieobjektBeskrivelse;?></a>
                        </td>
                    </tr>
                <?php endif;?>

                <?php if($kravtype == \Kyegil\Leiebasen\Modell\Leieforhold\Krav::TYPE_STRØM):?>
                    <tr>
                        <td style="font-weight:bold;">Anleggsnummer:</td>
                        <td style="text-align:right;"><?php echo $strømanleggId;?></td>
                    </tr>
                <?php endif;?>

                <?php if($kravtype == "Husleie"):?>
                    <tr>
                        <td style="font-weight:bold;">Andel:</td>
                        <td style="text-align:right;"><?php echo $andel;?></td>
                    </tr>
                <?php endif;?>

                <?php if($kravtype == "Husleie" or $kravtype == "Fellesstrøm"):?>
                    <tr>
                        <td style="font-weight:bold;">Termin:</td>
                        <td style="text-align:right;"><?php echo $termin;?></td>
                    </tr>
                <?php endif;?>

                <?php if($fom || $tom):?>
                    <tr>
                        <td style="font-weight:bold;">Tidsrom:</td>
                        <td style="text-align:right;"><?php echo ($fom ? ("fra og med {$fom} ") : "") . ($tom ? ("til og med $tom") : "");?></td>
                    </tr>
                <?php endif;?>

            </table>
        </td>
        <td style="vertical-align:top; padding:0 20px;">
            <table>
                <tr>
                    <td style="font-weight:bold;">Gironr:</td>
                    <td style="text-align:right;">
                        <?php if($regningsnummer && $utskriftsdato):?>
                            <?php echo $regningsnummer;?> <a href="/drift/index.php?oppslag=giro&oppdrag=lagpdf&gironr=<?php echo $regningsnummer;?>" target="_blank">[Last&nbsp;ned]</a>
                        <?php endif;?>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Utskriftsdato:</td>
                    <td style="text-align:right;"><?php echo $utskriftsdato ?: "Ikke skrevet ut";?></td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Forfallsdato:</td>
                    <td style="text-align:right;"><?php echo $forfall;?></td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">KID:</td>
                    <td style="text-align:right;"><?php echo $kid;?></td>
                </tr>
                <tr>
                    <td style="font-weight:bold;">Fast KID:</td>
                    <td style="text-align:right;"><?php echo $fastKid;?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table>
    <tr>
        <th class="value">Beløp</th>
        <th>Avstemt mot</th>
        <th>&nbsp;</th>
    </tr>
    <?php echo $utlikninger;?>
</table>
