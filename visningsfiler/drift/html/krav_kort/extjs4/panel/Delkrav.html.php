<?php
/**
 * Leiebasen Visningsmal
 *
 * @var Visning\drift\html\krav_kort\extjs4\panel\Delkrav $this
 * @var string $kode
 * @var string $navn
 * @var string $beløp
 */
use Kyegil\Leiebasen\Visning;
?><tr>
    <td>Inklusive <?php echo $navn;?>:</td>
    <td style="text-align:right;"><?php echo $beløp;?></td>
</tr>