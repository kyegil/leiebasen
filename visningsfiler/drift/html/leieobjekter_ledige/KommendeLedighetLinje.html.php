<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\html\leieobjekter_ledige\KommendeLedighetLinje $this
 * @var string $dato
 * @var string $leieobjektnr
 * @var string $leieobjektbeskrivelse
 * @var string $endring
 */
?>
<tr>
    <td class="dato"><?php echo $dato;?></td>
    <td><strong><?php echo \Kyegil\Leiebasen\Leiebase::ucfirst($leieobjektnr) . ' ' . $leieobjektbeskrivelse;?></strong><br>
        <?php echo $endring;?>
    </td>
</tr>
