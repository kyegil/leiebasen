<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\html\leieobjekter_ledige\LeieobjektLinje $this
 * @var string $leieobjektnr
 * @var string $leieobjektbeskrivelse
 * @var string $sisteLeietaker
 * @var string $ledighetsTekst
 * @var string $leietakerliste
 */
?>
<tr>
    <td class="d-none d-sm-block"<?php echo (trim($leietakerliste) || trim($ledighetsTekst)) ? ' style="border-bottom:none;"' : '';?>><?php echo $leieobjektnr;?></td>
    <td<?php echo (trim($leietakerliste) || trim($ledighetsTekst)) ? ' style="border-bottom:none;"' : '';?>><?php echo $leieobjektbeskrivelse;?></td>
    <td<?php echo (trim($leietakerliste) || trim($ledighetsTekst)) ? ' style="border-bottom:none;"' : '';?>><?php echo $sisteLeietaker;?></td>
</tr>
<?php if(trim($leietakerliste) || trim($ledighetsTekst)):?>
    <tr>
        <td class="d-none d-sm-block" style="border-top:none;"></td>
        <td colspan="2" style="border-top:none;">
            <?php echo $ledighetsTekst;?><br>
            <?php echo $leietakerliste;?>
        </td>
    </tr>
<?php endif;?>