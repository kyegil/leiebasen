<?php
/**
 * Leiebasen visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\html\depositum_kort\Index $this
 * @var string|\Kyegil\Leiebasen\Visning $depositumId
 * @var string|\Kyegil\Leiebasen\Visning $leieforholdId
 * @var string|\Kyegil\Leiebasen\Visning $leieforholdBeskrivelse
 * @var string|\Kyegil\Leiebasen\Visning $type
 * @var string|\Kyegil\Leiebasen\Visning $dato
 * @var string|\Kyegil\Leiebasen\Visning $beløp
 * @var string|\Kyegil\Leiebasen\Visning $betaler
 * @var string|\Kyegil\Leiebasen\Visning $notat
 * @var string|\Kyegil\Leiebasen\Visning $konto
 * @var string|\Kyegil\Leiebasen\Visning $fil
 * @var string|\Kyegil\Leiebasen\Visning $filtype
 * @var string|\Kyegil\Leiebasen\Visning $utløpsdato
 * @var string|\Kyegil\Leiebasen\Visning $påminnelser_sendt
 */

use Kyegil\Leiebasen\Modell\Leieforhold\Depositum;

?>
<div>
    <?php if($type == Depositum::TYPE_GARANTI):?>
        <h1>Depositumsgaranti for leieforhold <?php echo $leieforholdId;?></h1>
        <p><?php echo $leieforholdBeskrivelse;?></p>
    <?php else:?>
        <h1>Depositum for leieforhold <?php echo $leieforholdId;?></h1>
        <p><?php echo $leieforholdBeskrivelse;?></p>
    <?php endif;?>
</div>
<div>
    <dl>
        <?php if(trim($dato)):?>
            <dt>Dato</dt>
            <dd><?php echo $dato;?></dd>
        <?php endif;?>

        <?php if($type == Depositum::TYPE_DEPOSITUM):?>
            <dt>Deponert beløp</dt>
            <dd><?php echo $beløp;?></dd>

            <dt>Betalt av</dt>
            <dd><?php echo $betaler;?></dd>

            <dt>Depositumskonto</dt>
            <dd><?php echo $konto;?></dd>
        <?php else:?>
            <dt>Garantibeløp</dt>
            <dd><?php echo $beløp;?></dd>

            <dt>Garantist</dt>
            <dd><?php echo $betaler;?></dd>

            <?php if(trim($utløpsdato)):?>
                <dt>Garantiens utløpsdato</dt>
                <dd><?php echo $utløpsdato;?></dd>

                <?php if(trim($påminnelser_sendt)):?>
                    <dt>Påminnelse om garantien utløper sendt leietaker</dt>
                    <dd><?php echo $påminnelser_sendt;?></dd>
                <?php endif;?>
            <?php endif;?>

        <?php endif;?>
    </dl>

    <?php if(trim($notat)):?>
        <dt>Notat</dt>
        <dd><div><?php echo $notat;?></div></dd>
    <?php endif;?>


    <?php if(trim($fil)):?>
<!--        <object class="--><?php //echo $filtype;?><!--"-->
<!--            data=-->
<!--            "--><?php //echo $fil;?><!--"-->
<!--            width="800"-->
<!--            height="500">-->
<!--        </object>-->
<!--        <iframe class="--><?php //echo $filtype;?><!--"-->
<!--                src=-->
<!--                "--><?php //echo $fil;?><!--"-->
<!--                width="80%" height="500">-->
<!--        </iframe>-->
    <?php endif;?>

</div>