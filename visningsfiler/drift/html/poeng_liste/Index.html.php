<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\html\poeng_liste\Index $this
 * @var string $leieforholdId
 * @var string $leieforholdBeskrivelse
 * @var string $programId
 * @var string $programKode
 * @var string $programNavn
 * @var string $programBeskrivelse
 * @var string|\Kyegil\Leiebasen\Visning $varsler
 * @var string|\Kyegil\Leiebasen\Visning $tabell
 * @var string|\Kyegil\Leiebasen\Visning $knapper
 */
?>
<?php echo $varsler;?>
<div class="beskrivelse"><?php echo $programBeskrivelse;?></div>
<?php echo $tabell;?>
<?php echo $knapper;?>