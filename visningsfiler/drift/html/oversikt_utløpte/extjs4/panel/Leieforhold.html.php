<?php
/**
 * Leiebasen Visningsmal
 *
 * @var Visning\drift\html\oversikt_utløpte\extjs4\panel\Leieforhold $this
 * @var string $leieforholdId
 * @var string $leieforholdBeskrivelse
 * @var string $fraDato
 * @var string $tilDato
 * @var string $symbol
 */
use Kyegil\Leiebasen\Visning;
?><div><?php echo $symbol;?><a
        title="Gå til leieforhold <?php echo $leieforholdId;?>"
        href="/drift/index.php?oppslag=leieforholdkort&id=<?php echo $leieforholdId;?>"
    >Leieforhold nr. <?php echo $leieforholdId;?>
</a>: <?php echo $fraDato;?>
    – <strong><?php echo $tilDato;?></strong><br>
    <?php echo $leieforholdBeskrivelse;?><br>
    &nbsp;<br>
    <a
            title="Gå til leieforhold <?php echo $leieforholdId;?>"
            href="/drift/index.php?oppslag=leieforholdkort&id=<?php echo $leieforholdId;?>"
    >Vis leieavtalen</a>
    | <a
            title="Forny leieavtalen"
            href="/drift/leieforhold_skjema/<?php echo $leieforholdId;?>?fornyelse=1"
    >Forny denne leieavtalen</a>
    | <a
            title="Gjør avtalen om til en løpende leieavtale"
            onclick="leiebasen.drift.oversiktUtløpte.opphevTidsbegrensning(<?php echo $leieforholdId;?>);"
            href="#"
    >Opphev tidsbegrensningen</a>
    | <a
            title="Registrer oppsigelse for leieforhold <?php echo $leieforholdId;?>"
            href="/drift/index.php?oppslag=oppsigelsesskjema&id=<?php echo $leieforholdId;?>"
    >Avslutt dette leieforholdet</a>
    <br><hr>
</div>