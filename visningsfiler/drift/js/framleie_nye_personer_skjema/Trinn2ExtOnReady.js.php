<?php
/**
 * Leiebasen Visningsmal
 *
 * @var Trinn1 $this
 * @var Meny|string $meny
 * @var string $tilbakeUrl
 * @var string $framleieforholdId
 * @var string $fornavnListe
 * @var string $etternavnListe
 * @var string $personListe
 * @var string $engangspolett
 * @var bool $erOrg
 * @var string $fornavn
 * @var string $etternavn
 * @var string $treffJsonArray
 */
use Kyegil\Leiebasen\Visning\drift\js\framleie_nye_personer_skjema\Trinn1ExtOnReady as Trinn1;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>
/* 5 min timeouts */
Ext.override(Ext.data.Connection, {timeout: 300000});
Ext.override(Ext.data.proxy.Ajax, { timeout: 300000 });
Ext.override(Ext.form.action.Action, { timeout: 300 });
Ext.Ajax.timeout = 300000;

Ext.define('Person', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'navn', type: 'string'}
    ]
});

/**
 * @type {Ext.data.Store}
 */
var personliste = Ext.create('Ext.data.Store', {
    model: 'Person',
    data: <?php echo $personListe;?>
});

/**
 * @type {Ext.button.Button}
 */
var avbryt = Ext.create('Ext.button.Button', {
    text: 'Avbryt',
    id: 'avbryt',
    handler: function() {
        window.location = '<?php echo $tilbakeUrl;?>';
    }
});

/**
 * @type {Ext.button.Button}
 */
var lagreknapp = Ext.create('Ext.button.Button', {
    text: 'Fortsett',
    handler: function() {
        skjema.getForm().submit({
            url: '/drift/index.php?oppslag=framleie_nye_personer_skjema&trinn=3&id=<?php echo $framleieforholdId;?>'
        });
    }
});

/**
 * @type {Ext.form.Panel}
 */
var skjema = Ext.create('Ext.form.Panel', {
    renderTo: 'panel',
    autoScroll: true,
    frame: true,
    title: 'Legg til person',
    bodyPadding: 5,
    standardSubmit: true,

    items: [
        {
            xtype: 'hidden',
            name: 'fornavn',
            value: '<?php echo $fornavn;?>'
        },
        {
            xtype: 'hidden',
            name: 'etternavn',
            value: '<?php echo $etternavn;?>'
        },
        {
            xtype: 'hidden',
            name: 'er_org',
            value: '<?php echo (int)$erOrg;?>'
        },
        {
            xtype: 'hidden',
            name: 'engangspolett',
            value: '<?php echo $engangspolett;?>'
        }
    ],

    buttons: [
        {
            text: 'Avbryt',
            id: 'avbryt',
            handler: function() {
                window.location = '<?php echo $tilbakeUrl;?>';
            }
        },
        lagreknapp
    ]
});

/**
 * @type {Array.<Object>}
 */
var treffSett = <?php echo $treffJsonArray;?>

if (treffSett.length) {
    skjema.add({
        xtype: 'displayfield',
        value: ['Det ble funnet et mulig adressekort for <strong><?php echo (!$erOrg ? ($fornavn . ' ') : '') . $etternavn;?></strong> i leiebasen.<br>'
            + 'Er dette samme <?php echo ($erOrg ? 'virksomhet' : 'person');?>, eller en ny?<br>'
            + 'Dersom du vet at leietakeren allerede er registrert bør du søke fram riktig oppføring selv, for å unngå dobbeltregistreringer.<br><br>'
            + 'OBS: Opplysninger om adresse, telefonnummer, e-postadresse etc. kan korrigeres i neste trinn.<br><br>']
    });
}
else {
    skjema.add({
        xtype: 'displayfield',
        value: 'Det ble ikke funnet noe eksisterende adressekort for <strong><?php echo (!$erOrg ? ($fornavn . ' ') : '') . $etternavn;?></strong>.<br>'
            + 'Dersom du tror <?php echo ($erOrg ? 'virksomheten' : 'personen');?> har vært registrert i leiebasen tidligere bør du prøve å slå opp adressekortet for å unngå dobbeltregistreringer.<br><br>'
            + 'OBS: Opplysninger om adresse, telefonnummer, e-postadresse etc. kan korrigeres i neste trinn.<br><br>'

    });
}

treffSett.forEach(function(treff){
    skjema.add([
        {
            xtype: 'fieldset',
            border: true,
            layout:'column',
            items: [
                {
                    xtype: 'fieldcontainer',
                    columnWidth: 0.4,
                    layout: 'form',
                    items: [{
                        xtype: 'displayfield',
                        value: '<strong>Oppføring:</strong><br>'
                            + treff.navn + '<br>'
                            + (treff.erOrg && treff.personnr ? ('org. nr: ' + treff.personnr + '<br>') : (!treff.erOrg && treff.fødselsdato ? ('f. ' + treff.fødselsdato + '<br>') : ''))
                            + treff.adresse + '<br><br>'
                    }]
                },
                {
                    xtype: 'fieldcontainer',
                    columnWidth: 0.6,
                    layout: 'form',
                    items: [{
                        xtype: 'radio',
                        boxLabel: 'Ja, ' + (treff.erOrg ? 'virksomheten' : 'personen') + ' oppført til venstre er den som skal inn i avtalen',
                        checked: treff.prioritert,
                        hideLabel: true,
                        inputValue: treff.personId,
                        name: 'adressekort'
                    }
                    ]
                }

            ]
        }
    ]);
});

skjema.add([
    {
        xtype: 'fieldset',
        border: true,
        layout:'column',
        items:[{
            xtype: 'fieldcontainer',
            columnWidth: 0.4,
            layout: 'form',
            items: [{
                xtype: 'radio',
                boxLabel: "Bruk følgende adressekort:",
                hideLabel: true,
                inputValue: true,
                name: 'adressekort'
            }]
        },
            {
                xtype: 'fieldcontainer',
                columnWidth: 0.6,
                layout: 'form',
                items: [{
                    xtype: 'combobox',
                    forceSelection: true,
                    anyMatch: true,
                    hideLabel: true,
                    queryMode: 'local',
                    displayField: 'navn',
                    editable: true,
                    store: personliste,
                    triggerAction: 'all',
                    typeAhead: false,
                    valueField: 'id',
                    submitValue: false,
                    listeners: {
                        select: function(combo, records, eOpts) {
                            var radioButton = combo.up().up().down('radio');
                            radioButton.inputValue = combo.getValue();
                            radioButton.setValue(combo.getValue());
                        }
                    }
                }]
            }]
    },
    {
        xtype: 'radio',
        boxLabel: 'Nei, ' + <?php echo json_encode((!$erOrg ? $fornavn . ' ' : '') . $etternavn);?> + ' er <i>ikke</i> registrert tidligere.<br>'
            + 'Opprett nytt adressekort',
        checked: treffSett.length === 0,
        hideLabel: true,
        inputValue: 0,
        name: 'adressekort'
    }
]);