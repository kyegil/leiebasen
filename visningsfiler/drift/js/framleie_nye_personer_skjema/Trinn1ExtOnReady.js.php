<?php
/**
 * Leiebasen Visningsmal
 *
 * @var Trinn1 $this
 * @var Meny|string $meny
 * @var string $tilbakeUrl
 * @var string $framleieforholdId
 * @var string $fornavnListe
 * @var string $etternavnListe
 * @var string $engangspolett
 */
use Kyegil\Leiebasen\Visning\drift\js\framleie_nye_personer_skjema\Trinn1ExtOnReady as Trinn1;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>
/* 5 min timeouts */
Ext.override(Ext.data.Connection, {timeout: 300000});
Ext.override(Ext.data.proxy.Ajax, { timeout: 300000 });
Ext.override(Ext.form.action.Action, { timeout: 300 });
Ext.Ajax.timeout = 300000;

Ext.define('Fornavn', {
    extend: 'Ext.data.Model',
    idProperty: 'navn',
    fields: [
        {name: 'navn', type: 'string'},
        {name: 'er_org', type: 'boolean'}
    ]
});

Ext.define('Etternavn', {
    extend: 'Ext.data.Model',
    idProperty: 'navn',
    fields: [
        {name: 'navn', type: 'string'},
        {name: 'er_org', type: 'boolean'}
    ]
});

/**
 * @type {Ext.data.Store}
 */
var fornavnliste = Ext.create('Ext.data.Store', {
    model: 'Fornavn',
    data: <?php echo $fornavnListe;?>
});

/**
 * @type {Ext.data.Store}
 */
var etternavnliste = Ext.create('Ext.data.Store', {
    model: 'Etternavn',
    data: <?php echo $etternavnListe;?>
});

/**
 * @type {Ext.form.field.Hidden}
 */
var polettFelt = Ext.create('Ext.form.field.Hidden', {
    name: 'engangspolett',
    value: <?php echo json_encode($engangspolett);?>
});

/**
 * @type {Ext.form.field.Checkbox}
 */
var erOrgFelt = Ext.create('Ext.form.field.Checkbox', {
    boxLabel: 'Kryss her for firma-/organisasjons-/gruppenavn',
    checked: false,
    fieldLabel: 'Organisasjon',
    name: 'er_org',
    inputValue: 1,
    uncheckedValue: 0,

    listeners: {
        change: function( field, newValue, oldValue, eOpts ){
            fornavnFelt.setDisabled(newValue);
            if(newValue) {
                fornavnFelt.hide();
                etternavnFelt.setFieldLabel('Navn');
                etternavnFelt.emptyText = 'Navn';
            }
            else {
                fornavnFelt.show();
                etternavnFelt.setFieldLabel('Etternavn');
                etternavnFelt.emptyText = 'Etternavn';
            }
            etternavnFelt.applyEmptyText();
        }
    }
});

/**
 * @type {Ext.form.field.ComboBox}
 */
var fornavnFelt = Ext.create('Ext.form.field.ComboBox', {
    allowBlank: false,
    anyMatch: true,
    displayField: 'navn',
    editable: true,
    readOnly: false,
    emptyText: 'For- og evt. mellomnavn',
    fieldLabel: 'Fornavn',
    forceSelection: false,
    hideLabel: false,
    name: 'fornavn',
    queryMode: 'local',
    selectOnFocus: false,
    store: fornavnliste,
    typeAhead: true,
    hideTrigger: true,
    valueField: 'navn',
    queryDelay: 500,
    width: 700
});

/**
 * @type {Ext.form.field.ComboBox}
 */
var etternavnFelt = Ext.create('Ext.form.field.ComboBox', {
    allowBlank: false,
    anyMatch: true,
    displayField: 'navn',
    editable: true,
    readOnly: false,
    emptyText: 'Etternavn',
    fieldLabel: 'Etternavn',
    forceSelection: false,
    hideLabel: false,
    name: 'etternavn',
    queryMode: 'local',
    selectOnFocus: false,
    store: etternavnliste,
    typeAhead: true,
    hideTrigger: true,
    valueField: 'navn',
    queryDelay: 500,
    width: 700
});

/**
 * @type {Ext.button.Button}
 */
var avbryt = Ext.create('Ext.button.Button', {
    text: 'Avbryt',
    id: 'avbryt',
    handler: function() {
        window.location = '<?php echo $tilbakeUrl;?>';
    }
});

/**
 * @type {Ext.button.Button}
 */
var lagreknapp = Ext.create('Ext.button.Button', {
    text: 'Fortsett',
    handler: function() {
        skjema.getForm().submit({
            url: '/drift/index.php?oppslag=framleie_nye_personer_skjema&trinn=2&id=<?php echo $framleieforholdId;?>'
        });
    }
});

/**
 * @type {Ext.form.Panel}
 */
var skjema = Ext.create('Ext.form.Panel', {
    renderTo: 'panel',
    autoScroll: true,
    frame: true,
    title: 'Legg til person',
    bodyPadding: 5,
    standardSubmit: true,

    items: [
        {
            xtype: 'displayfield',
            value: 'Skriv inn fornavn og etternavn på personen som skal føres som framleier, evt navn på firma eller annen sammenslutning.<br><br>'
        },
        erOrgFelt,
        fornavnFelt,
        etternavnFelt,
        polettFelt
    ],

    buttons: [
        {
            text: 'Avbryt',
            id: 'avbryt',
            handler: function() {
                window.location = '<?php echo $tilbakeUrl;?>';
            }
        },
        lagreknapp
    ]
});