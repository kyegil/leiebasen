<?php
/**
 * Leiebasen Visningsmal
 *
 * @var Trinn1 $this
 * @var Meny|string $meny
 * @var string $tilbakeUrl
 * @var string $framleieforholdId
 * @var string $fornavnListe
 * @var string $etternavnListe
 * @var string $personListe
 * @var string $engangspolett
 * @var bool $erOrg
 * @var string $fornavn
 * @var string $etternavn
 * @var string $treffJsonArray
 * @var string $adressekortId
 */
use Kyegil\Leiebasen\Visning\drift\js\framleie_nye_personer_skjema\Trinn1ExtOnReady as Trinn1;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>
/**
 * @type {Ext.button.Button}
 */
var avbryt = Ext.create('Ext.button.Button', {
    text: 'Avbryt',
    id: 'avbryt',
    handler: function() {
        window.location = '<?php echo $tilbakeUrl;?>';
    }
});

/**
 * @type {Ext.button.Button}
 */
var lagreknapp = Ext.create('Ext.button.Button', {
    text: 'Registrer opplysningene',
    handler: function() {
        skjema.getForm().submit({
            url: '/drift/index.php?oppslag=framleie_skjema&id=<?php echo $framleieforholdId;?>&oppdrag=taimotskjema&skjema=framleiepersoner',
            waitMsg: 'Lagrer adressekortet'
        });
    }
});

/**
 * @type {Ext.form.field.Checkbox}
 */
var erOrg = Ext.create('Ext.form.field.Checkbox', {
    name: 'er_org',
    fieldLabel: 'Organisasjon/Enhet',
    labelWidth: 200,
    inputValue: 1,
    uncheckedValue: 0,
    readOnly: true
});

/**
 * @type {Ext.form.field.Text}
 */
var fornavn = Ext.create('Ext.form.field.Text', {
    name: 'fornavn',
    fieldLabel: 'For- og mellomnavn',
    labelWidth: 200,
    submitValue: true,
    value: <?php echo json_encode($fornavn)?>,
    hidden: <?php echo $erOrg ? 'true' : 'false';?>
});

/**
 * @type {Ext.form.field.Text}
 */
var etternavn = Ext.create('Ext.form.field.Text', {
    name: 'etternavn',
    fieldLabel: '<?php echo $erOrg ? 'Navn' : 'Etternavn';?>',
    labelWidth: 200,
    submitValue: true,
    value: <?php echo json_encode($etternavn)?>
});

/**
 * @type {Ext.form.field.Date}
 */
var fødselsdato = Ext.create('Ext.form.field.Date', {
    name: 'fødselsdato',
    fieldLabel: 'Fødselsdato',
    labelWidth: 200,
    format: 'd.m.Y',
    altFormats: "j.n.y|j.n.Y|j/n/y|j/n/Y|j-n-y|j-n-Y|j. M y|j. M -y|j. M. Y|j. F -y|j. F y|j. F Y|j/n|j-n|dm|dmy|dmY|d|Y-m-d",
    hidden: <?php echo $erOrg ? 'true' : 'false';?>
});

/**
 * @type {Ext.form.field.Text}
 */
var personnr = Ext.create('Ext.form.field.Text', {
    name: 'personnr',
    fieldLabel: '<?php echo $erOrg ? 'Org nr' : 'Personnummer';?>',
    labelWidth: 200
});

/**
 * @type {Ext.form.Panel}
 */
var skjema = Ext.create('Ext.form.Panel', {
    renderTo: 'panel',
    autoScroll: true,
    frame: true,
    title: 'Kontaktopplysninger',
    bodyPadding: 5,
    standardSubmit: false,
    height: '100%',
    defaults: {
        labelWidth: 200
    },
    items: [
        {
            xtype: 'displayfield',
            value: 'Oppdater kontaktopplysningene under før registrering.'
        },
        {
            xtype: 'hidden',
            name: 'engangspolett',
            value: '<?php echo $engangspolett;?>'
        },
        {
            xtype: 'displayfield',
            fieldLabel: 'Adressekort',
            value: '<?php echo $adressekortId ?? 'Ny registrering';?>'
        },
        {
            xtype: 'hidden',
            name: 'adressekort',
            value: '<?php echo $adressekortId ?? '*';?>'
        },
        erOrg,
        fornavn,
        etternavn,
        fødselsdato,
        personnr,
        {
            xtype: 'textfield',
            fieldLabel: 'Adresse',
            name: 'adresse1'
        },
        {
            xtype: 'textfield',
            fieldLabel: ' ',
            labelSeparator: '',
            name: 'adresse2'
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Postnr',
            name: 'postnr'
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Poststed',
            name: 'poststed'
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Land',
            name: 'land'
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Telefonnummer',
            name: 'telefon'
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Mobil-telefonnummer',
            name: 'mobil'
        },
        {
            xtype: 'textfield',
            fieldLabel: 'E-postadresse',
            name: 'epost'
        }
    ],
    buttons: [
        avbryt,
        lagreknapp
    ],

    listeners: {
        actioncomplete: function(form, action){
            if(action.type === 'load') {
                if(erOrg.getValue()) {
                    fornavn.hide();
                    fødselsdato.hide();
                    etternavn.setFieldLabel('Navn');
                    personnr.setFieldLabel('Org nr');
                }
                else {
                    fornavn.show();
                    fødselsdato.show();
                    etternavn.setFieldLabel('Etternavn');
                    personnr.setFieldLabel('Personnr');
                }
                lagreknapp.enable();
            }

            if(action.type === 'submit'){
                if(action.response.responseText === '') {
                    Ext.MessageBox.alert('Problem', 'Mottok ikke bekreftelsesmelding fra tjeneren  i JSON-format som forventet');
                } else {
                    var url = (typeof action.result.url != 'undefined') ? action.result.url : false;
                    var msg = (typeof action.result.msg != 'undefined') ? action.result.msg : false;
                    if (msg) {
                        Ext.MessageBox.alert('Suksess', msg, function() {
                            if (url) {
                                window.location = url;
                            }
                        });
                    }
                    else {
                        if (url) {
                            window.location = url;
                        }
                    }
                }
            }
        },

        actionfailed: function(form,action){
            if(action.type === 'load') {
                if (action.failureType === "connect") {
                    Ext.MessageBox.alert('Problem:', 'Klarte ikke laste data. Fikk ikke kontakt med tjeneren.');
                }
                else {
                    if (!action.response.responseText) {
                        Ext.MessageBox.alert('Problem:', 'Skjemaet mottok ikke data i JSON-format som forventet');
                    }
                    else {
                        var result = Ext.decode(action.response.responseText);
                        if(result && result.msg) {
                            Ext.MessageBox.alert('Mottatt tilbakemelding om feil:', action.result.msg);
                        }
                        else {
                            Ext.MessageBox.alert('Problem:', 'Innhenting av data mislyktes av ukjent grunn. (trolig manglende success-parameter i den returnerte datapakken). Action type='+action.type+', failure type='+action.failureType);
                        }
                    }
                }
            }
            if(action.type === 'submit') {
                if (action.failureType === "connect") {
                    Ext.MessageBox.alert('Problem:', 'Klarte ikke lagre data. Fikk ikke kontakt med tjeneren.');
                }
                else {
                    var result = Ext.decode(action.response.responseText);
                    if(result && result.msg) {
                        Ext.MessageBox.alert('Mottatt tilbakemelding om feil:', result.msg);
                    }
                    else {
                        Ext.MessageBox.alert('Problem:', 'Lagring av data mislyktes av ukjent grunn. Action type='+action.type+', failure type='+action.failureType);
                    }
                }
            }

        }
    }
});


<?php if($adressekortId):?>
lagreknapp.disable();
skjema.getForm().load({
    waitMsg: 'Henter adressekortet',
    url: '/drift/index.php?oppslag=framleie_nye_personer_skjema&id=<?php echo $framleieforholdId;?>&oppdrag=hent_data&data=adressekort&adressekort=<?php echo $adressekortId?>'
});

<?php endif;?>
