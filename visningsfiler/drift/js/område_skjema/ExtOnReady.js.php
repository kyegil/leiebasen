<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var int $områdeId
 * @var Meny|string $meny
 * @var Visning|string $skjema
 * @var string $formActionUrl
 */
use Kyegil\Leiebasen\Visning;use Kyegil\Leiebasen\Visning\drift\js\område_skjema\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>

/**
 * @name leiebasen
 * @type {Object}
 */

/**
 * @type {Ext.form.Panel}
 */
var skjema = <?php echo $skjema;?>;

/**
 * @type {Ext.button.Button}
 */
var saveButton = Ext.getCmp('save-button');

saveButton.setHandler(function() {
    skjema.getForm().submit({
        waitMsg: "Lagrer...",
        url: <?php echo json_encode($formActionUrl);?>
    });
});