<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var int $områdeId
 * @var Meny|string $meny
 * @var Meny|string $initLeiebasenJsObjektEgenskaper
 * @var Visning|string $skjema
 * @var string $formActionUrl
 */
use Kyegil\Leiebasen\Visning;use Kyegil\Leiebasen\Visning\drift\js\adgang_skjema\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>

/**
 * @name leiebasen
 * @type {Object}
 */
<?php echo $initLeiebasenJsObjektEgenskaper;?>
leiebasen.skjema.oppdaterLeieforholdFelt = function() {
    if (leiebasen.skjema.felter.område.getValue() === 'mine-sider' || leiebasen.skjema.felter.område.getValue() === 'Mine Sider') {
        leiebasen.skjema.felter.leieforhold.enable();
    }
    else {
        leiebasen.skjema.felter.leieforhold.disable();
    }
}

leiebasen.skjema.oppdaterBruker = function() {
    leiebasen.skjema.felter.brukernavn.setValue('');
    leiebasen.skjema.felter.epost.setValue('');
    Ext.Ajax.request({
        url: "/drift/index.php?oppslag=adgang_skjema&oppdrag=hent_data&data=brukerprofil&personid=" + leiebasen.skjema.felter.person.getValue(),
        /**
         * @param {XMLHttpRequest} response
         */
        success: function(response) {
            var tilbakemelding = response.responseText;
            if (Ext.JSON.decode(tilbakemelding, true)) {
                tilbakemelding = Ext.JSON.decode(tilbakemelding);
                if (tilbakemelding.success) {
                    leiebasen.skjema.felter.brukernavn.setValue(tilbakemelding.data.brukernavn);
                    leiebasen.skjema.felter.epost.setValue(tilbakemelding.data.epost);
                }
            }
        }
    });
}

/**
 * @type {Ext.form.Panel}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.Panel
 */
var skjema = <?php echo $skjema;?>;

/**
 * @type {Ext.button.Button}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.button.Button
 */
var saveButton = Ext.getCmp('save-button');

saveButton.setHandler(function() {
    skjema.getForm().submit({
        waitMsg: "Lagrer...",
        url: <?php echo json_encode($formActionUrl);?>
    });
});
leiebasen.skjema.oppdaterLeieforholdFelt();