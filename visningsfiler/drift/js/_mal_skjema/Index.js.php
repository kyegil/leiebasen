<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\_mal_skjema\Index $this
 * @var string $modellId
 * @var string $skript
 */
?>

/***
 * @type {jQuery} $
 */
leiebasen.drift._malSkjema = leiebasen.drift._malSkjema || {};
leiebasen.drift._malSkjema.initier = function() {

};

<?php echo $skript;?>

leiebasen.drift._malSkjema.initier();
