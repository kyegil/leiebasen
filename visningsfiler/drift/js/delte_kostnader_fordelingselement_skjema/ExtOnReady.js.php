<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var int $elementId
 * @var int $fordelingsnøkkelId
 * @var Meny|string $meny
 * @var Meny|string $initLeiebasenJsObjektEgenskaper
 * @var Visning|string $skjema
 * @var string $formActionUrl
 */
use Kyegil\Leiebasen\Visning;use Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_fordelingselement_skjema\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>

/**
 * @name leiebasen
 * @type {Object}
 */
<?php echo $initLeiebasenJsObjektEgenskaper;?>
/**
 * @name leiebasen.skjema
 * @type {Object}
 */
leiebasen.skjema.oppdaterSkjema = function() {
    switch (this.felter.fordelingsmåte.getValue().fordelingsmåte) {
        case 'Andeler':
            this.felter.andeler.enable();
            this.felter.prosentsats.disable();
            this.felter.fastbeløp.disable();
            break;
        case 'Prosentvis':
            this.felter.andeler.disable();
            this.felter.prosentsats.enable();
            this.felter.fastbeløp.disable();
            break;
        case 'Fastbeløp':
            this.felter.andeler.disable();
            this.felter.prosentsats.disable();
            this.felter.fastbeløp.enable();
            break;
    }
    switch (this.felter.følgerLeieobjekt.getValue().følger_leieobjekt) {
        case '1':
            this.felter.leieobjekt.enable();
            this.felter.leieforhold.disable();
            break;
        case '0':
            this.felter.leieobjekt.disable();
            this.felter.leieforhold.enable();
            break;
    }
}
/**
 * @type {Ext.form.Panel}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.Panel
 */
var skjema = <?php echo $skjema;?>;

/**
 * @type {Ext.button.Button}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.button.Button
 */
var saveButton = Ext.getCmp('save-button');

saveButton.setHandler(function() {
    skjema.getForm().submit({
        waitMsg: "Lagrer...",
        url: <?php echo json_encode($formActionUrl);?>
    });
});
leiebasen.skjema.felter.fordelingsmåte.on('change', function() {
    leiebasen.skjema.oppdaterSkjema();
});
leiebasen.skjema.felter.følgerLeieobjekt.on('change', function() {
    leiebasen.skjema.oppdaterSkjema();
});
saveButton.disable();
var leieobjektLastet = false;
var leieforholdLastet = false;
leiebasen.skjema.felter.leieobjekt.getStore().load({
    scope: this,
    params: {query: leiebasen.skjema.felter.leieobjekt.lastValue, initial: 1},
    callback: function() {
        leiebasen.skjema.felter.leieobjekt.select(leiebasen.skjema.felter.leieobjekt.lastValue);
        leieobjektLastet = true;
        if (leieforholdLastet) {
            saveButton.enable();
        }
    }
});
leiebasen.skjema.felter.leieforhold.getStore().load({
    scope: this,
    params: {query: leiebasen.skjema.felter.leieforhold.lastValue, initial: 1},
    callback: function() {
        leiebasen.skjema.felter.leieforhold.select(leiebasen.skjema.felter.leieforhold.lastValue);
        leieforholdLastet = true;
        if (leieobjektLastet) {
            saveButton.enable();
        }
    }
});
leiebasen.skjema.oppdaterSkjema();
