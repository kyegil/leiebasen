<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var int $bygningsId
 * @var Meny|string $meny
 * @var Panel|string $skjema
 * @var string $formActionUrl
 */
use Kyegil\Leiebasen\Visning\drift\js\bygning_skjema\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel;
?>

/**
 * @name leiebasen
 * @type {Object}
 */

/**
 * @type {Ext.form.field.Hidden}
 */
var bildeverdi = Ext.create('Ext.form.field.Hidden', {
    name: 'bilde'
});

/**
 * @type {Ext.Img}
 */
var bildevisning = Ext.create('Ext.Img', {
    height: 350,
    maxWidth: 350
});

/**
 * @type {Ext.form.Panel}
 */
var skjema = <?php echo $skjema;?>;

/**
 * @type {Ext.button.Button}
 */
var saveButton = Ext.getCmp('save-button');

saveButton.setHandler(function() {
    skjema.getForm().submit({
        waitMsg: "Lagrer...",
        url: <?php echo json_encode($formActionUrl);?>
    });
});