<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\leieregulering_liste\Index $this
 * @var string $skript
 */
?>
<?php echo $skript;?>

leiebasen.drift.leiereguleringListe = leiebasen.drift.leiereguleringListe || {};

/**
 *
 * @param {DataTable.Api} dataTable
 */
leiebasen.drift.leiereguleringListe.godkjennMarkerte = function(dataTable) {
    let utløpte = dataTable.rows( function(idx, data ) {
        return data.utløpt;
    }, { selected: true } ).ids();
    if (utløpte.length) {
        window.alert('Utløpte leieforhold kan ikke reguleres!');
        return;
    }
    let valgt = dataTable.rows( { selected: true } ).ids();
    let brev = dataTable.rows( '.brev', { selected: true } ).ids();
    var dialogTekst;
    dialogTekst = valgt.length === 1
        ? 'Er du sikker på at den markerte justeringen skal gjennomføres?'
        : 'Er du sikker på at disse ' + valgt.length + ' justeringene skal gjennomføres?';
    if (!window.confirm(dialogTekst)) {
        return;
    }
    if (brev.length) {
        window.open('/drift/index.php?oppslag=leieregulering_liste&oppdrag=hent_data&data=varselutskrift&id[]=' + brev.join('&id[]='), '_blank');
        dialogTekst = brev.length + ' av de valgte forslagene må sendes som brev til mottaker.\nUtskrift er åpnet i en egen vindu eller fane.\nSkriv ut, og klikk deretter `Ja` for å bekrefte at du har skrevet ut varsler og kan sende disse i tide.';
        if (!window.confirm(dialogTekst)) {
            return;
        }
    }
    this.gjennomførJustering(valgt.toArray());
}
/**
 * @param {Number[]} ids
 */
leiebasen.drift.leiereguleringListe.gjennomførJustering = function(ids) {
    jQuery.ajax({
        url: '/drift/index.php?oppslag=leieregulering_liste&oppdrag=oppgave&oppgave=juster_leie',
        method: "POST",
        dataType: 'json',
        data: {
            leieforhold: ids
        },
        success: function(data) {
            if (data.msg) {
                alert(data.msg);
            }
            if(data.url) {
                window.location.href = data.url;
            }
        },
        /**
         * @param {jqXHR} jqXHR
         * @param {String} textStatus
         * @param {String} errorThrown
         */
        error: function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}
