<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\leieforhold_regningsadresse_skjema\Index $this
 * @var int $leieforholdId
 * @var bool $efakturaAktiv
 * @var string $skript
 */
?>

leiebasen.drift.leieforholdRegningsadresseSkjema = leiebasen.drift.leieforholdRegningsadresseSkjema || {
    /**
     * @type {boolean}
     */
    efakturaAktiv: <?php echo $efakturaAktiv ? 'true' : 'false';?>,
    felter: {},
    initier: function() {
        this.felter.regningTilObjektFelt = $("input[type='checkbox'][name='regning_til_objekt']");
        this.felter.regningsObjektFelt = $("[name='regningsobjekt']");
        this.felter.regningspersonFelter = $("[name='regningsperson']");
        this.felter.uavhengigAdresseRadio = $("[name='regningsperson'][value='']");
        this.felter.regningspersonFeltsett = $(".regningsperson-feltsett");
        this.felter.uavhengigAdresseFeltsett = $(".uavhengigAdresseFeltsett");

        this.felter.adresse1Felt = $("[name='regningsadresse1']");
        this.felter.adresse2Felt = $("[name='regningsadresse2']");
        this.felter.postnrFelt = $("[name='postnr']");
        this.felter.poststedFelt = $("[name='poststed']");
        this.felter.landFelt = $("[name='land']");

        if(typeof this._initiert !== 'undefined') {
            return this;
        }

        this.felter.regningTilObjektFelt.on('change', function (){
            leiebasen.drift.leieforholdRegningsadresseSkjema.oppdaterFelter();
        });
        this.felter.regningspersonFelter.on('change', function (){
            leiebasen.drift.leieforholdRegningsadresseSkjema.oppdaterFelter();
        });

        this._initiert = true;

        this.oppdaterFelter();
    },

    oppdaterFelter: function() {
        const regningTilObjekt = this.felter.regningTilObjektFelt.is(':checked');
        const uavhengigAdresse = this.felter.uavhengigAdresseRadio.is(':checked');
        let regningspersonLabel = '';
        if(this.efakturaAktiv && !regningTilObjekt) {
            regningspersonLabel = 'Efaktura-mottaker og adresse for regninger'
        }
        else if(this.efakturaAktiv) {
            regningspersonLabel = 'Efaktura-mottaker';
        }
        else if(!regningTilObjekt) {
            regningspersonLabel = 'Adresse for regninger';
        }

        this.felter.regningspersonFeltsett.find("legend").html(regningspersonLabel);

        $("[name='regningsperson'][value!='']").closest("label").siblings("div.adressefelt").toggle(!regningTilObjekt);

        $("[name='regningsperson'][value!='']").closest("label").each(function (idx, label) {
            const efakturaLabel = $(label).siblings(".efaktura-label").html();
            const adresseLabel = $(label).siblings(".adresse-label").html();
            $(label).find("span").html(regningTilObjekt ? efakturaLabel : adresseLabel);
        });

        this.felter.regningsObjektFelt.prop('required', regningTilObjekt);
        this.felter.regningsObjektFelt.parent().toggle(regningTilObjekt);

        this.felter.regningspersonFeltsett.toggleClass('show', this.efakturaAktiv || !regningTilObjekt);
        this.felter.uavhengigAdresseRadio.parent().toggle(!regningTilObjekt);

        this.felter.adresse1Felt.prop('required', uavhengigAdresse);
        this.felter.adresse1Felt.prop('required', uavhengigAdresse);
        this.felter.postnrFelt.prop('required', uavhengigAdresse);
        this.felter.poststedFelt.prop('required', uavhengigAdresse);
        this.felter.uavhengigAdresseFeltsett.toggleClass('show', uavhengigAdresse);
    }
};

<?php echo $skript;?>

leiebasen.drift.leieforholdRegningsadresseSkjema.initier();
