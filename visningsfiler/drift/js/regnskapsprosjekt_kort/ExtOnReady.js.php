<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var int $prosjektId
 * @var Meny|string $meny
 * @var ComboBox|string $tidsromVelger
 * @var Date|string $fomFelt
 * @var Date|string $tomFelt
 * @var GridPanel|string $resultatPanel
 * @var Panel|string $kort
 */
use Kyegil\Leiebasen\Visning\drift\js\regnskapsprosjekt_kort\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\ComboBox;use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\field\Date;use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\grid\Panel as GridPanel;use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\panel\Panel;
?>

/**
 * @name leiebasen
 * @type {Object}
 */

/**
 * @name leiebasen.extJs
 * @type {Ext}
 */

leiebasen.visRapport = function(eksportType = '') {
    var resultatStore = resultatPanel.getStore();

    if (fomFelt.validate() && tomFelt.validate()) {
        var fom = fomFelt.getValue();
        var tom = tomFelt.getValue();

        switch (eksportType) {
            case 'csv':
                window.open('/drift/index.php?oppslag=regnskapsprosjekt_kort&id=<?php echo $prosjektId;?>&oppdrag=hent_data&data=rapport_csv&fom=' + Ext.Date.format(fom, 'Y-m-d') + '&tom=' + Ext.Date.format(tom, 'Y-m-d') + '&_dc=' + Date.now());
                break;
            case 'utskrift':
                window.open('/drift/index.php?oppslag=regnskapsprosjekt_kort&id=<?php echo $prosjektId;?>&oppdrag=hent_data&data=utskrift&fom=' + Ext.Date.format(fom, 'Y-m-d') + '&tom=' + Ext.Date.format(tom, 'Y-m-d') + '&_dc=' + Date.now());
                break;
            default:
                resultatStore.load({
                    params: {
                        fom: Ext.Date.format(fom, 'Y-m-d'),
                        tom: Ext.Date.format(tom, 'Y-m-d')
                    }
                });
                break;
        }
    }
}

/**
 * @type {Ext.form.field.ComboBox}
 */
var tidsromVelger = <?php echo $tidsromVelger;?>;

/**
 * @type {Ext.form.field.Date}
 */
var fomFelt = <?php echo $fomFelt;?>;

/**
 * @type {Ext.form.field.Date}
 */
var tomFelt = <?php echo $tomFelt;?>;

/**
 * @type {Ext.button.Button}
 */
var utførKnapp = Ext.create('Ext.button.Button', {
    text: 'Vis',
    handler: function() {
        leiebasen.visRapport();
    }
});

/**
 * @type {Ext.button.Button}
 */
var eksportKnapp = Ext.create('Ext.button.Button', {
    text: 'Eksportér ...',
    menu: Ext.create('Ext.menu.Menu', {
        width: 100,
        margin: '0 0 10 0',
        items: [{
            text: 'som utskrift',
            handler: function() {
                leiebasen.visRapport('utskrift');
            }
        }, {
            text: 'som CSV',
            handler: function() {
                leiebasen.visRapport('csv');
            }
        }]
    })
});

/**
 * @type {Ext.grid.Panel}
 */
var resultatPanel = <?php echo $resultatPanel;?>;

/**
 * @type {Ext.panel.Panel}
 */
var panel = <?php echo $kort;?>;

/**
 * @type {Ext.container.DockingContainer}
 */
var bottomBar = panel.getDockedItems('toolbar[dock="bottom"]')[0];
bottomBar.add(utførKnapp);
bottomBar.add(eksportKnapp);

tidsromVelger.on({
    /**
     * @param {Ext.form.field.ComboBox} combo
     * @param {Ext.data.Model[]} records
     */
    select: function(combo, records) {

        var valgt = records[0];
        fomFelt.setValue(valgt.get('fom'));
        tomFelt.setValue(valgt.get('tom'));
    }
});
