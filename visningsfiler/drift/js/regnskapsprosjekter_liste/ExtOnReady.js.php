<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var Meny|string $meny
 * @var string $tilbakeUrl
 */
use Kyegil\Leiebasen\Visning\drift\js\regnskapsprosjekter_liste\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>

/**
 * @param {Ext.data.Model} record
 */
leiebasen.slettOppslag = function(record){
    var oppslagId = record.get('id');
    Ext.Msg.confirm({
        title: 'Slett Prosjektregnskapet',
        msg: 'Er du sikker på du vil slette regnskap ' + oppslagId + '?',
        buttons: Ext.Msg.OKCANCEL,
        fn: function(buttonId) {
            if (buttonId === 'ok') {
                Ext.Ajax.request({
                    params: {
                        'regnskapsprosjekt_id': oppslagId
                    },
                    waitMsg: 'Vent...',
                    url: "/drift/index.php?oppslag=regnskapsprosjekt_skjema&oppdrag=oppgave&oppgave=slett_regnskap",

                    failure: function(response, opts) {
                        Ext.MessageBox.alert('Mislyktes', 'Respons-status ' + response.status + ' fra tjeneren... ', function() {
                            datasett.load();
                        });
                    },
                    success : function(response, opts) {
                        if(!response.responseText) {
                            Ext.MessageBox.alert('Problem', 'Null respons fra tjeneren...', function() {
                                datasett.load();
                            });
                        }
                        var jsonRespons = Ext.JSON.decode(response.responseText, true);
                        if(!jsonRespons) {
                            Ext.MessageBox.alert('Problem', response.responseText, function() {
                                datasett.load();
                            });
                        }
                        else if (jsonRespons.success) {
                            Ext.MessageBox.alert('Slettet', jsonRespons.msg, function() {
                                datasett.load();
                            });
                        }
                        else {
                            Ext.MessageBox.alert('Hmm..', jsonRespons.msg, function() {
                                datasett.load();
                            });
                        }
                    }
                });
            }
        },
        icon: Ext.window.MessageBox.QUESTION
    });
}

Ext.define('leiebasen.Regnskapsprosjekt', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [ // {@link http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.Field}
        {name: 'id', type: 'int'},
        {name: 'kode', type: 'string'},
        {name: 'navn', type: 'string'},
        {name: 'beskrivelse', type: 'string'}
    ]
});

/**
 * @type {Ext.grid.plugin.CellEditing}
 */
var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
    clicksToEdit: 1
});

/**
 * @type {Ext.grid.plugin.RowEditing}
 */
var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
    autoCancel: false,
    listeners: {
        beforeedit: function (grid, e, eOpts) {
            return e.column.xtype !== 'actioncolumn';
        }
    }
});

/**
 * @type {Ext.data.Store}
 */
var datasett = Ext.create('Ext.data.Store', {
    model: 'leiebasen.Regnskapsprosjekt',
    pageSize: 300,
    remoteSort: true,
    proxy: {
        type: 'ajax',
        simpleSortMode: true,
        url: "/drift/index.php?oppslag=regnskapsprosjekter_liste&oppdrag=hent_data",
        timeout: 300000, // 5 mins
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRows'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                let responsStreng = response.responseText;
                let tilbakemelding = Ext.JSON.decode(responsStreng, true);
                if (response.status !== 200 || !tilbakemelding) {
                    Ext.MessageBox.alert('Ugyldig respons', responsStreng);
                }
                else if (!tilbakemelding.success && tilbakemelding.msg) {
                    Ext.MessageBox.alert('Hm', tilbakemelding.msg);
                }
            }
        }
    },
    sorters: [{
        property: 'kode',
        direction: 'DESC'
    }]
});

/**
 * @type {Ext.form.field.Text}
 */
var søkefelt = Ext.create('Ext.form.field.Text', {
    emptyText: 'Søk og klikk ↵',
    name: 'søkefelt',
    width: 200,
    listeners: {
        specialkey: function( felt, e, eOpts ) {
            datasett.getProxy().extraParams = {
                søkefelt: søkefelt.getValue()
            };
            datasett.load({
                params: {
                    start: 0,
                    limit: 300
                }
            });
        }
    }
});

/**
 * @type {Ext.grid.column.Column}
 */
var id = Ext.create('Ext.grid.column.Column', {
    align: 'right',
    dataIndex:	'id',
    header:		'Id',
    sortable:	true,
    width:		80
});

/**
 * @type {Ext.grid.column.Column}
 */
var kode = Ext.create('Ext.grid.column.Column', {
    align: 'right',
    dataIndex:	'kode',
    header:		'Kode',
    sortable:	true,
    width:		80
});

/**
 * @type {Ext.grid.column.Column}
 */
var navn = Ext.create('Ext.grid.column.Column', {
    align: 'left',
    dataIndex:	'navn',
    header:		'',
    sortable:	true,
    width:		80,
    flex:       1,
    renderer:   function(value, metaData, record, rowIndex, colIndex, store, view) {
        return '<span title = "' + record.get('beskrivelse').replace(/"/g, "&#8220;") + '">' + value + '</span>';
    }
});

/**
 * @type {Ext.grid.column.Action}
 */
var vis  = Ext.create('Ext.grid.column.Action', {
    header:		'Åpne',
    altText:    'Åpne',
    dataIndex: 'id',
    icon: '/pub/media/bilder/drift/vis.png',
    tooltip: 'Åpne',
    handler: function(grid, rowIndex, colIndex, item, e, record, row) {
        window.location = '/drift/index.php?oppslag=regnskapsprosjekt_kort&id=' + record.get('id');
    },
    sortable: false,
    width: 50
});

/**
 * @type {Ext.grid.column.Action}
 */
var endre  = Ext.create('Ext.grid.column.Action', {
    header:		'Endre',
    dataIndex: 'id',
    icon: '/pub/media/bilder/drift/rediger.png',
    tooltip: 'Endre',
    handler: function(grid, rowIndex, colIndex, item, e, record, row) {
        window.location = '/drift/index.php?oppslag=regnskapsprosjekt_skjema&id=' + record.get('id');
    },
    sortable: false,
    width: 50
});

/**
 * @type {Ext.grid.column.Action}
 */
var slett  = Ext.create('Ext.grid.column.Action', {
    header:		'Slett',
    dataIndex: 'id',
    icon: '/pub/media/bilder/drift/slett.png',
    tooltip: 'Slett',
    handler: function(grid, rowIndex, colIndex, item, e, record, row) {
        leiebasen.slettOppslag(record);
    },
    sortable: false,
    width: 50
});

/**
 * @type {Ext.toolbar.Paging}
 */
var sidevelger = Ext.create('Ext.toolbar.Paging', {
    xtype: 'pagingtoolbar',
    store: datasett,
    dock: 'bottom',
    displayInfo: true
});

/**
 * @type {Ext.grid.Panel}
 */
var rutenett = Ext.create('Ext.grid.Panel', {
    autoScroll: true,
    layout: 'border',
    frame: false,
    store: datasett,
    title: 'Prosjektregnskaper',
    listeners: {
        celldblclick: function( panel, td, cellIndex, record, tr, rowIndex, e, eOpts ) {
            window.location = "/drift/index.php?oppslag=regnskapsprosjekt_kort&id=" + record.get('id');
        }
    },
    tbar: [
        søkefelt
    ],
    columns: [
        id,
        kode,
        navn,
        vis,
        endre,
        slett
    ],
    renderTo: 'panel',
    height: '100%',

    dockedItems: [sidevelger],

    buttons: [{
        text: 'Tilbake',
        handler: function() {
            window.location = '<?php echo $tilbakeUrl;?>';
        }
    }, {
        text: 'Opprett nytt prosjektregnskap',
        handler: function() {
            window.location = "/drift/index.php?oppslag=regnskapsprosjekt_skjema&id=*";
        }
    }]
});

rutenett.getStore().on({
    load: function(store, records, successful, eOpts) {
        var jsonData = store.getProxy().getReader().jsonData;
        if (typeof jsonData == 'undefined') {
            Ext.MessageBox.alert('Problem:', 'Klarte ikke laste denne tabellen av ukjent grunn.');
            return false;
        }
        if (!successful) {
            var msg = store.getProxy().getReader().jsonData.msg;
            msg = msg.replace(/\r\n|\r|\n/g, '<br>');
            Ext.MessageBox.alert('Klarte ikke laste data:', msg);
        }
    }
});

datasett.getProxy().extraParams = {
    søkefelt: søkefelt.getValue()
};

datasett.load({
    params: {
        start: 0,
        limit: 300
    }
});