<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var Meny|string $meny
 * @var string $tilbakeUrl
 */
use Kyegil\Leiebasen\Visning\drift\js\framleie_liste\ExtOnReady;
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>

/**
 * @param {Ext.data.Model} record
 */
leiebasen.slettAvtale = function(record){
    var framleieforholdId = record.get('avtale_id');
    Ext.Msg.confirm({
        title: 'Slett framleieavtale',
        msg: 'Er du sikker på du vil slette framleieavtale ' + framleieforholdId + '?',
        buttons: Ext.Msg.OKCANCEL,
        fn: function(buttonId) {
            if (buttonId === 'ok') {
                Ext.Ajax.request({
                    params: {
                        'framleieforhold': framleieforholdId
                    },
                    waitMsg: 'Vent...',
                    url: "/drift/index.php?oppslag=framleie_skjema&oppdrag=oppgave&oppgave=slett_avtale",

                    failure: function(response, opts) {
                        Ext.MessageBox.alert('Mislyktes', 'Response status ' + response.status + ' fra tjeneren... ', function() {
                            datasett.load();
                        });
                    },
                    success : function(response, opts) {
                        var jsonRespons = Ext.JSON.decode(response.responseText, true);
                        if(!jsonRespons) {
                            Ext.MessageBox.alert('Problem', 'Null respons fra tjeneren...', function() {
                                datasett.load();
                            });
                        }
                        else if (jsonRespons.success) {
                            Ext.MessageBox.alert('Slettet', jsonRespons.msg, function() {
                                datasett.load();
                            });
                        }
                        else {
                            Ext.MessageBox.alert('Hmm..', jsonRespons.msg, function() {
                                datasett.load();
                            });
                        }
                    }
                });
            }
        },
        icon: Ext.window.MessageBox.QUESTION
    });
}

Ext.define('leiebasen.Framleieavtale', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [ // {@link http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.Field}
        {name: 'avtale_id', type: 'int'},
        {name: 'fradato', type: 'date', dateFormat: 'Y-m-d', useNull: true},
        {name: 'tildato', type: 'date', dateFormat: 'Y-m-d', useNull: true},
        {name: 'leieforhold', type: 'int'},
        {name: 'leieforhold_beskrivelse', type: 'string'},
        {name: 'framleid_til', type: 'string'}
    ]
});

/**
 * @type {Ext.grid.plugin.CellEditing}
 */
var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
    clicksToEdit: 1
});

/**
 * @type {Ext.grid.plugin.RowEditing}
 */
var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
    autoCancel: false,
    listeners: {
        beforeedit: function (grid, e, eOpts) {
            return e.column.xtype !== 'actioncolumn';
        }
    }
});

/**
 * @type {Ext.data.Store}
 */
var datasett = Ext.create('Ext.data.Store', {
    model: 'leiebasen.Framleieavtale',
    pageSize: 300,
    remoteSort: true,
    proxy: {
        type: 'ajax',
        simpleSortMode: true,
        url: "/drift/index.php?oppslag=framleie_liste&oppdrag=hent_data",
        timeout: 300000, // 5 mins
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRows'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                let responsStreng = response.responseText;
                let tilbakemelding = Ext.JSON.decode(responsStreng, true);
                if (response.status !== 200 || !tilbakemelding) {
                    Ext.MessageBox.alert('Ugyldig respons', responsStreng);
                }
                else if (!tilbakemelding.success && tilbakemelding.msg) {
                    Ext.MessageBox.alert('Hm', tilbakemelding.msg);
                }
            }
        }
    },
    sorters: [{
        property: 'til_dato',
        direction: 'DESC'
    }]
});

/**
 * @type {Ext.form.field.Text}
 */
var søkefelt = Ext.create('Ext.form.field.Text', {
    emptyText: 'Søk og klikk ↵',
    name: 'søkefelt',
    width: 200,
    listeners: {
        specialkey: function( felt, e, eOpts ) {
            datasett.getProxy().extraParams = {
                søkefelt: søkefelt.getValue()
            };
            datasett.load({
                params: {
                    start: 0,
                    limit: 300
                }
            });
        }
    }
});

/**
 * @type {Ext.grid.column.Column}
 */
var fraDato = Ext.create('Ext.grid.column.Column', {
    align: 'right',
    dataIndex:	'fradato',
    header:		'Fra dato',
    renderer:	function(value, metaData, record, rowIndex, colIndex, store, view) {
        if (value) {
            return Ext.util.Format.date( value, 'd.m.Y' );
        }
        else {
            return null;
        }
    },
    sortable:	true,
    width:		80
});

/**
 * @type {Ext.grid.column.Column}
 */
var tilDato = Ext.create('Ext.grid.column.Column', {
    align:      'right',
    dataIndex:	'tildato',
    header:		'Til dato',
    renderer:	function(value, metaData, record, rowIndex, colIndex, store, view) {
        if (value) {
            return Ext.util.Format.date( value, 'd.m.Y' );
        }
        else {
            return null;
        }
    },
    sortable:	true,
    width:		80
});

/**
 * @type {Ext.grid.column.Column}
 */
var leieforhold = Ext.create('Ext.grid.column.Column', {
    dataIndex: 'leieforhold',
    header: 'Utleier',
    renderer: function(value, metaData, record, rowIndex, colIndex, store){
        return value + ': ' + record.get('leieforhold_beskrivelse');
    },
    sortable: true,
    width: 100,
    flex: 5
});

/**
 * @type {Ext.grid.column.Column}
 */
var framleidTil = Ext.create('Ext.grid.column.Column', {
    dataIndex: 'framleid_til',
    header: 'Framleid til',
    sortable: false,
    width: 100,
    flex: 3
});

/**
 * @type {Ext.grid.column.Action}
 */
var endre  = Ext.create('Ext.grid.column.Action', {
    header:		'Endre',
    dataIndex: 'avtale_id',
    icon: '/pub/media/bilder/drift/rediger.png',
    tooltip: 'Endre',
    handler: function(grid, rowIndex, colIndex, item, e, record, row) {
        window.location = '/drift/index.php?oppslag=framleie_skjema&id=' + record.get('avtale_id');
    },
    sortable: false,
    width: 50
});

/**
 * @type {Ext.grid.column.Action}
 */
var slett  = Ext.create('Ext.grid.column.Action', {
    header:		'Slett',
    dataIndex: 'avtale_id',
    icon: '/pub/media/bilder/drift/slett.png',
    tooltip: 'Slett',
    handler: function(grid, rowIndex, colIndex, item, e, record, row) {
        leiebasen.slettAvtale(record);
    },
    sortable: false,
    width: 50
});

/**
 * @type {Ext.toolbar.Paging}
 */
var sidevelger = Ext.create('Ext.toolbar.Paging', {
    xtype: 'pagingtoolbar',
    store: datasett,
    dock: 'bottom',
    displayInfo: true
});

/**
 * @type {Ext.grid.Panel}
 */
var rutenett = Ext.create('Ext.grid.Panel', {
    autoScroll: true,
    layout: 'border',
    frame: false,
    store: datasett,
    title: 'Framleieavtaler',
    listeners: {
        celldblclick: function( panel, td, cellIndex, record, tr, rowIndex, e, eOpts ) {
            window.location = "/drift/index.php?oppslag=framleie_skjema&id=" + record.get('avtale_id');
        }
    },
    tbar: [
        søkefelt
    ],
    columns: [
        fraDato,
        tilDato,
        leieforhold,
        framleidTil,
        endre,
        slett
    ],
    renderTo: 'panel',
    height: '100%',

    dockedItems: [sidevelger],

    buttons: [{
        text: 'Tilbake',
        handler: function() {
            window.location = '<?php echo $tilbakeUrl;?>';
        }
    }, {
        text: 'Legg inn ny framleieavtale',
        handler: function() {
            window.location = "/drift/index.php?oppslag=framleie_skjema&id=*";
        }
    }]
});

datasett.getProxy().extraParams = {
    søkefelt: søkefelt.getValue()
};

datasett.load({
    params: {
        start: 0,
        limit: 300
    }
});