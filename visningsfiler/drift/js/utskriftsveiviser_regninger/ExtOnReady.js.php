<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\utskriftsveiviser_regninger\ExtOnReady $this
 * @var \Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny|string $meny
 * @var \Kyegil\Leiebasen\Visning\felles\extjs4\Ext\data\Store $datasett
 * @var bool $adskilt
 * @var string $tilbakeUrl
 * @var string $inkLeieforhold
 * @var string $eksLeieforhold
 * @var bool $utskriftAlleredeIgangsatt
 */
?>

leiebasen.skrivUt = function() {
    var kravIder = [];
    rutenett.getSelectionModel().getSelection().forEach(function (element) {
        kravIder.push(element.getId());
    });

    framdriftsindikator.show({
        msg: 'Vent litt...',
        width: 300,
        wait: true,
        waitConfig: {
            interval: 1000, // interval * increment = tid i ms
            increment: 30,
            text : 'Setter sammen utskriftsfil'
        }
    });

    Ext.Ajax.request({
        url: '/drift/index.php?oppslag=utskriftsveiviser&oppdrag=oppgave&oppgave=opprett_utskrift',
        params: {
            adskilt: <?php echo $adskilt ? '1' : '0';?>,
            'krav[]': kravIder
        },
        success: function(response) {
            framdriftsindikator.hide();
            var tilbakemelding = JSON.parse(response.responseText);
            if(tilbakemelding.success === true) {
                leiebasen.visBekreftelsesvindu();
                window.open('/drift/index.php?oppslag=giro&oppdrag=lagpdf', '_blank');
            }
            else {
                Ext.MessageBox.alert('Hmm..', tilbakemelding.msg);
                datasett.load();
            }
        },
        failure: function(response) {
            framdriftsindikator.hide();
            Ext.MessageBox.alert('Mislyktes', 'Respons-status ' + response.status + ' fra tjeneren... ', function() {
                datasett.load();
            });
        }
    });
}

leiebasen.visBekreftelsesvindu = function() {
    window.onbeforeunload = function() {
        return 'Du bør ikke forlate utskriftsveiviseren uten å bekrefte om utskriften skal registreres i leiebasen.';
    };

    Ext.Msg.buttonText.yes = "Utskriften er OK. Registrer utskrifts- og forfallsdatoer";
    Ext.Msg.buttonText.no = "Utskriften skal forkastes uten å registreres";
    Ext.Msg.show({
        title: 'Bekreft at utskriften er OK',
        msg: '<p>Vent til utskriften er fullført, og bekreft at utskriften er vellykket og vil brukes.<br><br>Først når den er bekreftet vil utskrift og purringer bli registrert.</p>',
        buttons: Ext.Msg.YESNO,
        closable: false,
        fn: function(buttonId) {
            if(buttonId === 'yes') {
                leiebasen.registrerUtskrift();
            }
            else if(buttonId === 'no') {
                leiebasen.forkastUtskrift();
            }
        }
    });


}

leiebasen.registrerUtskrift = function() {
    Ext.MessageBox.wait('Utskriften registreres', 'Vent litt...');
    Ext.Ajax.request({

        url: '/drift/index.php?oppslag=utskriftsveiviser&oppdrag=ta_imot_skjema&skjema=utskriftsbekreftelse',
        params: {},
        failure:function(){
            Ext.MessageBox.alert('Whoops! Problemer...','Oppnår ikke kontakt med databasen! Prøv igjen senere.');
        },

        success: function(response) {
            var tilbakemelding = Ext.JSON.decode(response.responseText);
            if(tilbakemelding['success'] === true) {
                Ext.MessageBox.alert('Utskriften er registrert', tilbakemelding.msg, function() {
                    window.onbeforeunload = null;
                    Ext.Msg.buttonText.yes = "Skriv ut konvolutter";
                    Ext.Msg.buttonText.no = "Nei, takk";
                    Ext.MessageBox.confirm("Vil du skrive ut konvolutter?",
                        'Før du skriver ut må du putte konvolutter i skriveren.<br>Justering av adressefeltet kan gjøres i innstillingene for leiebasen.',
                        function(buttonId) {
                            if(buttonId === 'yes' && tilbakemelding['adresser']) {
                                window.open( "/drift/index.php?oppslag=personadresser_utskrift&oppdrag=lagpdf&pdf=konvolutter&leieforhold=" + tilbakemelding['adresser'].join(), '_blank');
                            }
                            window.location = tilbakemelding.url;
                        }
                    );
                });
            }
            else {
                Ext.MessageBox.alert('Hmm..',tilbakemelding['msg']);
            }
        }
    });
};

leiebasen.forkastUtskrift = function() {
    Ext.MessageBox.wait('Utskriften forkastes', 'Vent litt...');
    Ext.Ajax.request({
        url: '/drift/index.php?oppslag=utskriftsveiviser&oppdrag=oppgave&oppgave=forkast_utskrift',

        failure: function() {
            Ext.MessageBox.alert('Whoops! Problemer...', 'Oppnår ikke kontakt med databasen! Prøv igjen senere.');
        },

        success: function(response) {
            var tilbakemelding = Ext.JSON.decode(response.responseText);
            if(tilbakemelding['success'] === true) {
                window.onbeforeunload = null;
                Ext.MessageBox.alert('Utskriften er forkastet', tilbakemelding.msg, function() {
                    window.location = tilbakemelding.url;
                });
            }
            else {
                Ext.MessageBox.alert('Hmm..', tilbakemelding['msg']);

            }
        }

    });
};

leiebasen.fortsettTilPurringer = function() {
    /**
     * @type {Number[]}
     */
    var inkLeieforhold = <?php echo $inkLeieforhold;?>;
    /**
     * @type {Number[]}
     */
    var eksLeieforhold = <?php echo $eksLeieforhold;?>;
    /**
     * @type string
     */
    var adskilt = <?php echo $adskilt ? '1' : '0';?>;
    var url = '/drift/index.php?oppslag=utskriftsveiviser_purringer';
    if (inkLeieforhold.length) {
        url += '&ink_leieforhold=' + inkLeieforhold.join();
    }
    if (eksLeieforhold.length) {
        url += '&eks_leieforhold=' + eksLeieforhold.join();
    }
    url += '&adskilt=' + adskilt;
    /**
     * @type {Object[]}
     */
    var skjulteSkjemaFelter = [];
    rutenett.getSelectionModel().getSelection().forEach(function (element) {
        skjulteSkjemaFelter.push({
            tag: 'input',
            name: 'krav[]',
            value: element.getId(),
            type: 'hidden'
        });
    });
    var skjultSkjema = Ext.getBody().createChild({
        tag: 'form',
        cls: 'x-hidden',
        id: 'form',
        action: url,
        method: 'POST',
        children: skjulteSkjemaFelter
    });
    skjultSkjema.dom.submit();
};

leiebasen.bekreftEksisterendeUtskrift = function() {
    window.onbeforeunload = function() {
        return 'Du bør ikke forlate utskriftsveiviseren uten å bekrefte om utskriften skal registreres i leiebasen.';
    };
    /**
     * @type {Ext.window.MessageBox}
     */
    var utskriftsbekreftelse = Ext.create('Ext.window.MessageBox', {
        buttons: [
            {
                text: 'Vis utskriften på nytt.',
                handler: function () {
                    window.open('/drift/index.php?oppslag=giro&oppdrag=lagpdf', '_blank');
                }
            },
            {
                text: 'Utskriften var OK. Registrer den.',
                handler: function () {
                    leiebasen.registrerUtskrift();
                }
            },
            {
                text: 'Utskriften skal forkastes',
                handler: function () {
                    leiebasen.forkastUtskrift();
                }
            }
        ]
    });

    utskriftsbekreftelse.show({
        title: 'Bekreft den eksisterende utskriften',
        msg: 'Det har allerede blitt påbegynt en utskrift som fortsatt står ubekreftet.<br>Du må bekrefte eller forkaste den påbegynte utskriften før du kan skrive ut på nytt.',
        closable: false,
        fn: function(buttonId) {
            if(buttonId === 'yes') {
                leiebasen.registrerUtskrift();
            }
            else if(buttonId === 'no') {
                leiebasen.forkastUtskrift();
            }
        }
    });
}

/**
 * @param {Ext.data.Model} record
 */
Ext.define('leiebasen.Krav', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [ // {@link http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.Field}
        {name: 'valgt', type: 'boolean'},
        {name: 'id', type: 'int'},
        {name: 'tekst', type: 'string'},
        {name: 'type', type: 'string'},
        {name: 'dato', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'leieforhold_id', type: 'int'},
        {name: 'leieforhold_beskrivelse', type: 'string'},
        {name: 'regningsformat', type: 'string'},
        {name: 'forsinket_efaktura', type: 'boolean'}
    ]
});

/**
 * @type {Ext.window.MessageBox}
 */
var framdriftsindikator = Ext.create('Ext.window.MessageBox', {
    renderTo: Ext.getBody(),
    width: 300
});

/**
 * @type {Ext.data.Store}
 */
var datasett = <?php echo $datasett;?>;

/**
 * @type {Ext.grid.column.Column}
 */
var id = Ext.create('Ext.grid.column.Column', {
    align: 'right',
    dataIndex:	'id',
    header:		'Id',
    sortable:	true,
    width:		80
});

/**
 * @type {Ext.grid.column.Column}
 */
var tekst = Ext.create('Ext.grid.column.Column', {
    align: 'left',
    dataIndex:	'tekst',
    header:		'Tekst',
    sortable:	true,
    flex:       5
});

/**
 * @type {Ext.grid.column.Column}
 */
var type = Ext.create('Ext.grid.column.Column', {
    align: 'left',
    dataIndex:	'type',
    header:		'Kravtype',
    sortable:	true,
    flex:       1
});

/**
 * @type {Ext.grid.column.Date}
 */
var dato = Ext.create('Ext.grid.column.Date', {
    align: 'right',
    dataIndex:	'dato',
    header:		'Dato',
    sortable:	true,
    width:      70,
    format:     'd.m.Y'
});

/**
 * @type {Ext.grid.column.Column}
 */
var leieforhold = Ext.create('Ext.grid.column.Column', {
    align: 'left',
    dataIndex:	'leieforhold_id',
    header:		'Leieforhold',
    sortable:	true,
    flex:       3,
    renderer: function(value, metaData, record) {
        return value + ': ' + record.get('leieforhold_beskrivelse');
    }
});

/**
 * @type {Ext.grid.column.Column}
 */
var regningsformat = Ext.create('Ext.grid.column.Column', {
    align: 'left',
    dataIndex:	'regningsformat',
    header:		'Regningsformat',
    sortable:	true,
    flex:       1
});

/**
 * @type {Ext.grid.column.Boolean}
 */
var forsinketEfaktura = Ext.create('Ext.grid.column.Boolean', {
    align: 'left',
    dataIndex:	'forsinket_efaktura',
    trueText:   '<img src="/bilder/timer.png" alt="Sendes automatisk innen 48 timer" height="12px">',
    falseText:   '',
    header:		'Efaktura sendes forsinket',
    sortable:	true,
    flex:       1
});

/**
 * @type {Ext.grid.Panel}
 */
var rutenett = Ext.create('Ext.grid.Panel', {
    autoScroll: true,
    layout: 'border',
    frame: false,
    store: datasett,
    selModel: Ext.create('Ext.selection.CheckboxModel', {
        checkOnly: true
    }),
    columns: [
        id,
        dato,
        leieforhold,
        tekst,
        type,
        regningsformat,
        forsinketEfaktura
    ],
    renderTo: 'panel',
    height: '100%',

    buttons: [{
        text: 'Avbryt',
        handler: function() {
            window.location = '<?php echo $tilbakeUrl;?>';
        }
    }, {
        text: 'Skriv ut regninger uten purringer',
        handler: function() {
            leiebasen.skrivUt();
        }
    }, {
        text: 'Fortsett for å legge til purringer',
        handler: function() {
            leiebasen.fortsettTilPurringer();
        }
    }]
});
rutenett.getSelectionModel().selectAll();

<?php if ($utskriftAlleredeIgangsatt):?>
leiebasen.bekreftEksisterendeUtskrift();
<?php endif;?>
