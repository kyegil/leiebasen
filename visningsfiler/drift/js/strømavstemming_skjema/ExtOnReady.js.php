<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var int $prosjektId
 * @var Meny|string $meny
 * @var string $tilbakeUrl
 * @var string $tittel
 * @var \Kyegil\Leiebasen\Visning|string $tilEditor
 * @var \Kyegil\Leiebasen\Visning|string $nySatsEditor
 * @var \Kyegil\Leiebasen\Visning|string $avregningEditor
 */
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;use Kyegil\Leiebasen\Visning\drift\js\strømavstemming_skjema\ExtOnReady;
?>

Ext.define('leiebasen.Avstemmingsdata', {
    extend: 'Ext.data.Model',
    idProperty: 'leieforhold',
    fields: [ // http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.Field
        {name: 'leieforhold', type: 'int'},
        {name: 'beskrivelse', type: 'string'},
        {name: 'siste_avregningsdato', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'til', type: 'date', dateFormat: 'Y-m-d', allowNull: true},
        {name: 'nylig_avregnet', type: 'boolean'},
        {name: 'balanse', type: 'float'},
        {name: 'relativ', type: 'boolean'},
        {name: 'sats', type: 'float'},
        {name: 'har_beregningsgrunnlag', type: 'boolean'},
        {name: 'ny_sats', type: 'float'},
        {name: 'avregning', type: 'float'},
        {name: 'transaksjoner'}
    ]
});

var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
    pluginId: 'cellEditing',
    clicksToEdit: 1
});

var oppdaterLinje = function(grid, e) {
    if(e.field === 'til'){
        var ventemelding = Ext.MessageBox.wait('Vennligst vent..', 'Beregner nye data for leieforhold ' + e.record.getId());
        var til = e.value;
        if(til instanceof Date) {
            til = Ext.Date.format(e.value, 'Y-m-d');
        }
        Ext.Ajax.request({
            method: 'GET',
            waitMsg: 'Oppdaterer...',
            url: "/drift/index.php?oppslag=strømavstemming_skjema&oppdrag=hent_data&data=enkeltforhold",
            params: {
                leieforhold: e.record.getId(),
                tildato: til
            },

            failure:function() {
                ventemelding.hide();
                Ext.MessageBox.alert('Whoops! Problemer...', 'Klarte ikke å oppdatere beregningen.<br>Kan du ha mistet nettforbindelsen?');
            },

            success: function(response) {
                ventemelding.hide();
                var tilbakemelding = Ext.JSON.decode(response.responseText);
                if(tilbakemelding['success'] == true) {
                    var data = tilbakemelding.data;
                    e.record.beginEdit();
                    e.record.set('til', new Date(data.til));
                    e.record.set('balanse', data.balanse);
                    e.record.set('sats', data.sats);
                    e.record.set('ny_sats', data.ny_sats);
                    e.record.set('avregning', data.avregning);
                    e.record.set('transaksjoner', data.transaksjoner);
                    e.record.endEdit();
                    datasett.commitChanges();
                    if(tilbakemelding.msg) {
                        Ext.MessageBox.alert('Obs!', tilbakemelding.msg);
                    }
                }
                else {
                    Ext.MessageBox.alert('Advarsel!', tilbakemelding['msg']);

                }
            }
        });
    }
}

var lagre = function(record) {
    var ventemelding = Ext.MessageBox.wait('Vennligst vent..', 'Lagrer avstemming for leieforhold ' + record.getId());
    Ext.Ajax.request({
        waitMsg: 'Lagrer...',
        url: "/drift/index.php?oppslag=strømavstemming_skjema&oppdrag=ta_i_mot_skjema",
        params: {
            leieforhold: record.getId(),
            fra: Ext.Date.format(record.get('siste_avregningsdato'), 'Y-m-d'),
            avregningsdato: Ext.Date.format(record.get('til'), 'Y-m-d'),
            sats: record.get('ny_sats'),
            avregningsbeløp: record.get('avregning')
        },

        failure:function() {
            ventemelding.hide();
            Ext.MessageBox.alert('Whoops! Problemer...', 'Klarte ikke å lagre avregningen.<br>Kan du ha mistet nettforbindelsen?');
        },

        success: function(response) {
            ventemelding.hide();
            var tilbakemelding = Ext.JSON.decode(response.responseText);
            if(tilbakemelding['success'] == true) {
                var data = tilbakemelding.data;
                record.beginEdit();
                record.set('siste_avregningsdato', new Date(data.siste_avregningsdato));
                record.set('til', new Date(data.til));
                record.set('nylig_avregnet', data.nylig_avregnet);
                record.set('balanse', data.balanse);
                record.set('sats', data.sats);
                record.set('ny_sats', data.ny_sats);
                record.set('avregning', data.avregning);
                record.set('transaksjoner', data.transaksjoner);
                record.endEdit();
                datasett.commitChanges();
                if(tilbakemelding.msg) {
                    Ext.MessageBox.alert('Obs!', tilbakemelding.msg);
                }
            }
            else {
                Ext.MessageBox.alert('Advarsel!', tilbakemelding['msg']);

            }
        }
    });
}

var filtrerOgLast = function() {
    datasett.load({
        params: {
            start: 0,
            limit: 300
        }
    });
}

var datasett = Ext.create('Ext.data.Store', {
    model: 'leiebasen.Avstemmingsdata',
    pageSize: 300,
    remoteSort: true,
    proxy: {
        type: 'ajax',
        simpleSortMode: true,
        actionMethods: {
            read: 'GET'
        },
        url: "/drift/index.php?oppslag=strømavstemming_skjema&oppdrag=hent_data",
        timeout: 900000, // 15 min
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRows'
        },
        listeners: {
            exception: function(proxy, response) {
                let responsStreng = response.responseText;
                let tilbakemelding = Ext.JSON.decode(responsStreng, true);
                if (response.status !== 200 || !tilbakemelding) {
                    Ext.MessageBox.alert('Ugyldig respons', responsStreng);
                }
                else if (!tilbakemelding.success && tilbakemelding.msg) {
                    Ext.MessageBox.alert('Hm', tilbakemelding.msg);
                }
            }
        }
    },
    sorters: [{
        property: 'siste_avregningsdato',
        direction: 'ASC'
    }]
});



var leieforholdId = {
    id: 'leieforholdId',
    dataIndex: 'leieforhold',
    header: 'Leieforhold',
    align: 'right',
    hidden: false,
    renderer: function(value) {
        return '<a title="Klikk for å gå til leieforhold-kortet" href="/drift/index.php?oppslag=leieforholdkort&id=' + value + '">' + value + '</a>';
    },
    sortable: true,
    width: 70
};

var beskrivelse = {
    id: 'beskrivelse',
    dataIndex: 'beskrivelse',
    header: 'Leietaker(e)',
    hidden: false,
    sortable: true,
    width: 50,
    flex: 1
};

var siste_avregningsdato = {
    id: 'siste_avregningsdato',
    dataIndex: 'siste_avregningsdato',
    header: 'Sist avregnet',
    align: 'right',
    hidden: false,
    renderer: Ext.util.Format.dateRenderer('d.m.Y'),
    sortable: true,
    width: 90
};

var til = {
    id: 'til',
    dataIndex: 'til',
    editor: <?php echo $tilEditor;?>,
    header: 'Avregnes',
    align: 'right',
    hidden: false,
    renderer: Ext.util.Format.dateRenderer('d.m.Y'),
    sortable: true,
    width: 70
};

var balanse = {
    id: 'balanse',
    dataIndex: 'balanse',
    header: 'Balanse',
    align: 'right',
    hidden: false,
    renderer: Ext.util.Format.noMoney,
    sortable: true,
    width: 70
};

var sats = {
    id: 'sats',
    dataIndex: 'sats',
    header: 'Årlig Sats',
    align: 'right',
    hidden: false,
    renderer: function(value, metadata, record) {
        if(record.get('relativ')) {
            return Ext.util.Format.round(value * 100, 1) + '&nbsp;%';
        }
        else {
            return Ext.util.Format.noMoney(value);
        }
    },
    sortable: true,
    width: 70
};

var nySats = {
    id: 'ny_sats',
    dataIndex: 'ny_sats',
    editor: <?php echo $nySatsEditor;?>,
    header: 'Ny sats',
    align: 'right',
    hidden: false,
    renderer: function(value, metadata, record) {
        if(record.get('relativ')) {
            return Ext.util.Format.round(value * 100, 1) + '&nbsp;%';
        }
        else {
            return Ext.util.Format.noMoney(value);
        }
    },
    sortable: true,
    width: 70
};

var avregning = {
    id: 'avregning',
    dataIndex: 'avregning',
    editor: <?php echo $avregningEditor;?>,
    header: 'Avregning',
    align: 'right',
    hidden: false,
    renderer: Ext.util.Format.noMoney,
    sortable: true,
    width: 70
};

var utfør = {
    dataIndex: 'utfør',
    align: 'center',
    xtype: 'actioncolumn',
    icon: '/pub/media/bilder/drift/utfør.png',
    tooltip: 'Utfør avstemming',
    handler: function(grid, rowIndex) {
        var record = grid.getStore().getAt(rowIndex);
        lagre(record);
    },
    text: 'Avstem',
    sortable: false,
    width: 50
};

var rutenett = Ext.create('Ext.grid.Panel', {
    title: '<?php echo $tittel;?>',
    autoScroll: true,
    layout: 'border',
    renderTo: 'panel',
    height: '100%',
    store: datasett,
    selType: 'cellmodel',

    columns: [
        leieforholdId,
        beskrivelse,
        siste_avregningsdato,
        til,
        sats,
        balanse,
        nySats,
        avregning,
        utfør
    ],

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: datasett,
        dock: 'bottom',
        displayInfo: true
    }],

    plugins: [
        cellEditing,
        {
            ptype: 'rowexpander',
            rowBodyTpl : new Ext.XTemplate('{transaksjoner}')
        }
    ],
    viewConfig: {
        stripeRows: true,
        getRowClass: function(record){
            return record.get("nylig_avregnet") ? "rad-grønn" : "";
        }
    }
});
filtrerOgLast();
cellEditing.on('edit', oppdaterLinje);