<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\oversikt_gjeld\Index $this
 * @var string $skript
 */
?>
<?php echo $skript;?>

leiebasen.drift.oversiktGjeld = leiebasen.drift.oversiktGjeld || {};
leiebasen.drift.oversiktGjeld.initier = function() {
    $('#gjeldsoversikt').on('column-visibility.dt', function (e, settings, column, state) {
        let columnName = $('#gjeldsoversikt').DataTable().settings().init().columns[column].name;
        console.log( 'Column ' + columnName + ' was made ' + (state ? '' : 'in') + 'visible');
    });
};

leiebasen.drift.oversiktGjeld.oppdaterTabell = function() {
    let leieforholdfilter = Number($("#leieforholdfilter").find(":selected").val());

    leiebasen.datatables.gjeldsoversikt.column('frosset:name').visible([5,10].indexOf(leieforholdfilter) ===-1);

    leiebasen.datatables.gjeldsoversikt.ajax.reload();
}

/**
 * @param {number} poeng
 * @param {string} programKode
 * @param {number} leieforholdId
 */
leiebasen.drift.oversiktGjeld.formaterPoeng = function(poeng, programKode, leieforholdId) {
    return poeng
        ? ('<a title="Vis detaljer" href="/drift/index.php?oppslag=poeng_liste&kode=' + programKode + '&id=' + leieforholdId + '">' + poeng + '</a>')
        : '';
}

leiebasen.drift.oversiktGjeld.initier();
