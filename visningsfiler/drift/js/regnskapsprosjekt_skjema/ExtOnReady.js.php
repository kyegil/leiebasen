<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var int $prosjektId
 * @var Meny|string $meny
 * @var Visning|string $skjema
 * @var string $formActionUrl
 */
use Kyegil\Leiebasen\Visning;use Kyegil\Leiebasen\Visning\drift\js\regnskapsprosjekt_skjema\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>

/**
 * @name leiebasen
 * @type {Object}
 */

leiebasen.oppdaterEksklusiveTre = function() {
    var checkedIds = [];
    inkluderteLeieobjekter.getChecked().forEach(function(node) {
        checkedIds.push(node.getId());
    });

    var ekskludertRot = ekskluderteLeieobjekter.getStore().getRootNode();

    if(checkedIds.indexOf(ekskludertRot.getId()) !== -1) {
        // Rot skal aldri aktiveres for ekskludering
        // Dersom rot har blitt valgt for inkludering,
        // så må alle deaktiverte bygninger under denne reaktiveres
        ekskludertRot.cascadeBy(function(bygningsNode) {
            if (bygningsNode === ekskludertRot) {
                bygningsNode.set('checked', null);
            }
            else if (bygningsNode.get('checked') === null) {
                bygningsNode.set('checked', false);
            }
        });
    }
    else {
        // Dersom rot ikke er valgt for inkludering,
        // så må vi gå gjennom bygningene for å sjekke disse
        ekskludertRot.set('checked', null);
        ekskludertRot.eachChild(function(bygningsNode) {
            if(checkedIds.indexOf(bygningsNode.getId()) !== -1) {
                // Dersom bygningen har blitt valgt for inkludering,
                // så må den deaktiveres for ekskludering
                // Alle deaktiverte leieobjekter under bygningen må reaktiveres
                bygningsNode.cascadeBy(function(leieobjektNode) {
                    if (leieobjektNode === bygningsNode) {
                        leieobjektNode.set('checked', null);
                    }
                    else if (leieobjektNode.get('checked') === null) {
                        leieobjektNode.set('checked', false);
                    }
                });
                bygningsNode.expand();
            }
            else {
                // Dersom bygningen ikke er valgt for inkludering,
                // så må alle leieobjekter under denne deaktiveres
                bygningsNode.cascadeBy(function(leieobjektNode) {
                    leieobjektNode.set('checked', null);
                });
            }
        });
    }
}

/**
 * @type {Ext.form.Panel}
 */
var skjema = <?php echo $skjema;?>;

/**
 * @type {Ext.button.Button}
 */
var saveButton = Ext.getCmp('save-button');

/**
 * @type {Ext.tree.Panel}
 */
var inkluderteLeieobjekter = Ext.getCmp('inkluderte_leieobjekter');

/**
 * @type {Ext.tree.Panel}
 */
var ekskluderteLeieobjekter = Ext.getCmp('ekskluderte_leieobjekter');

saveButton.setHandler(function() {
    var params = {};
    inkluderteLeieobjekter.getChecked().forEach(function(node) {
        params["inkluderte_leieobjekter[" + node.getId() + "]"] = 1;
    });
    ekskluderteLeieobjekter.getChecked().forEach(function(node) {
        params["ekskluderte_leieobjekter[" + node.getId() + "]"] = 1;
    });

    skjema.getForm().submit({
        waitMsg: "Lagrer...",
        params: params,
        url: <?php echo json_encode($formActionUrl);?>
    });
});

inkluderteLeieobjekter.on({
    /**
     * @param {Ext.data.NodeInterface} node The node who's checked property was changed
     * @param {boolean} checked The node's new checked state
     */
    checkchange: function(node, checked) {
        node.bubble(function(parentNode) {
            if (node.getId() !== parentNode.getId()) {
                parentNode.set('checked', false);
            }
        });
        node.cascadeBy(function(childNode) {
            if (node.getId() !== childNode.getId()) {
                childNode.set('checked', false);
            }
        });
        if (checked) {
            node.collapse();
        }

        leiebasen.oppdaterEksklusiveTre();
    }
});

leiebasen.oppdaterEksklusiveTre();