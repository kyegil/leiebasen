<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var Meny|string $meny
 * @var string $tilbakeUrl
 * @var string $kredittkoplingId
 * @var string $kravId
 */
use Kyegil\Leiebasen\Visning\drift\js\krav_kort\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>

Ext.tip.QuickTipManager.init();
Ext.form.Field.prototype.msgTarget = 'side';
Ext.Loader.setConfig({
    enabled:true
});
/* 5 min timeouts */
Ext.override(Ext.data.Connection, {timeout: 300000});
Ext.override(Ext.data.proxy.Ajax, { timeout: 300000 });
Ext.override(Ext.form.action.Action, { timeout: 300 });
Ext.Ajax.timeout = 300000;

leiebasen.kople = function(delbeløpId, beløp, leieforhold) {
    var utlikning = 0;

    var oppdater = function() {
        utlikning = 0;
        for (var index = 0; index < kravsett.length; ++index) {
            var krav = kravsett[index];
            utlikning += (komb[krav.id].getValue()) * (verdi[krav.id].value);
        }

        utlikningsvindu.setTitle('Valgt krav for kr. ' + Ext.util.Format.noMoney(utlikning) + ' --- Rest kr. ' + Ext.util.Format.noMoney(beløp - utlikning));
        return utlikning;
    }

    var utlikningsskjema = Ext.create('Ext.form.Panel', {
        layout: 'column',
        autoScroll: true,
        standardSubmit: false,
        buttons: [{
            text: 'Lagre',
            handler: function() {
                utlikningsskjema.getForm().submit({
                    url: '/drift/index.php?oppslag=krav_kort&oppdrag=manipuler&data=kople&id=<?php echo $kravId;?>&innbetalingsid=<?php echo $kredittkoplingId;?>',
                    params: {
                        leieforhold: leieforhold
                    },
                    success: function() {
                        panel.getLoader().load('/drift/index.php?oppslag=<?php echo $_GET['oppslag'];?>&oppdrag=hent_data&id=<?php echo $kravId;?>&innbetalingsid=<?php echo $kredittkoplingId;?>');
                        utlikningsvindu.close();
                    }
                });
            }
        }]
    });

    var utlikningsvindu = Ext.create('Ext.window.Window', {
        layout: 'fit',
        modal: true,
        title: 'Avstemming av innbetaling',
        width: 600,
        height: 400,
        items: [utlikningsskjema]
    });

    var kravsett = [];
    var komb = [];
    var verdi = [];

    Ext.Ajax.request({
        url: "/drift/index.php?oppslag=krav_kort&oppdrag=hent_data&data=utlikningsmuligheter&id=<?php echo $kravId;?>&innbetalingsid=<?php echo $kredittkoplingId;?>",
        params: {
            delbeløp_id: delbeløpId,
            leieforhold: leieforhold
        },
        success : function(response) {
            var result = Ext.JSON.decode( response.responseText );
            if( result.success ) {
                kravsett = result.data;
                for (index = 0; index < kravsett.length; ++index) {
                    var krav = kravsett[index];

                    komb[krav.id] = Ext.create('Ext.form.field.Checkbox', {
                        boxLabel: krav.tekst + ( krav.id ? " (" + krav.id + ")" : ''),
                        hideLabel: true,
                        inputValue: 1,
                        listeners: {
                            change( checkbox, checked ) {
                                var checkBoxId = checkbox.getName().substring(checkbox.getName().indexOf("[") + 1, checkbox.getName().lastIndexOf("]"));
                                var startverdi = verdi[checkBoxId].startverdi;
                                if(checked) {
                                    var rest = beløp - utlikning;
                                    var maksverdi = (Math.abs(startverdi) < Math.abs(rest) ? startverdi : rest );
                                    verdi[checkBoxId].enable();
                                    verdi[checkBoxId].setValue(maksverdi);
                                }
                                else {
                                    verdi[checkBoxId].disable();
                                    verdi[checkBoxId].setValue(startverdi);
                                }
                                oppdater();
                                verdi[checkBoxId].validate();
                            }
                        },
                        name: 'komb[' + krav.id + ']',
                        width: 500
                    });
                    utlikningsskjema.add(komb[krav.id]);

                    verdi[krav.id] = Ext.create('Ext.form.field.Number', {
                        decimalSeparator: ',',
                        hideTrigger: true,
                        disabled: true,
                        listeners: {change: oppdater},
                        minValue: Math.min(krav.beløp, 0),
                        maxValue: Math.max(krav.beløp, 0),
                        name: 'verdi[' + krav.id + ']',
                        value: krav.beløp,
                        validator: function(v){
                            resultat = oppdater();
                            if(Math.abs(resultat) > Math.abs(beløp)) {
                                verdi[krav.id].setValue(
                                    Math.abs(krav.beløp) < Math.abs(v - (resultat - beløp))
                                        ? krav.beløp
                                        : (v - (resultat - beløp))
                                );
                            }
                            return true;
                        },
                        width: 50
                    });
                    verdi[krav.id].startverdi = krav.beløp

                    utlikningsskjema.add(verdi[krav.id]);

                }
                utlikningsvindu.show();
            }
            else {
            }
        }
    });

}

leiebasen.visAvtaleGiroKvittering = function() {
    var kvitteringsvindu = Ext.create('Ext.window.Window', {
        layout: 'fit',
        modal: true,
        autoScroll: true,
        title: 'AvtaleGiro Forsendelseskvittering',
        width: 600,
        height: 400,
        loader: {
            url: '/drift/index.php?oppslag=<?php echo $_GET['oppslag'];?>&oppdrag=hent_data&id=<?php echo $kravId;?>&data=avtalegiro_forsendelseskvittering',
            autoLoad: true
        }
    });
    kvitteringsvindu.show();
    return false; // Return false to prevent redirection if clicked in an anchor tag
}

leiebasen.frakople = function(delbeløpId) {
    Ext.Ajax.request({
        waitMsg: 'Løsner...',
        url: "/drift/index.php?oppslag=krav_kort&oppdrag=manipuler&data=frakople&id=<?php echo $kravId;?>&innbetalingsid=<?php echo $kredittkoplingId;?>",
        params: {
            delbeløp_id: delbeløpId
        },
        success : function() {
            panel.getLoader()
                .load('/drift/index.php?oppslag=krav_kort&oppdrag=hent_data&id=<?php echo $kravId;?>');
        }
    });
}

var panel = Ext.create('Ext.panel.Panel', {
    autoLoad: '/drift/index.php?oppslag=krav_kort&oppdrag=hent_data&id=<?php echo $kravId;?>',
    autoScroll: true,
    renderTo: 'panel',
    bodyStyle: 'padding:5px',
    title: '',
    frame: true,
    height: 500,
    buttons: [{
        handler: function() {
            window.location = '<?php echo $tilbakeUrl;?>';
        },
        text: 'Tilbake'
    }, {
        handler: function() {
            window.location = '/drift/index.php?oppslag=kravskjema&id=<?php echo $kravId;?>';
        },
        text: 'Redigér'
    }]
});

