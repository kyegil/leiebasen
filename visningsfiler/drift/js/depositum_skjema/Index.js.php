<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\depositum_skjema\Index $this
 * @var string $leieforholdId
 * @var string $depositumId
 * @var string $skript
 */
?>
<?php echo $skript;?>

leiebasen.drift.depositumSkjema = leiebasen.drift.depositumSkjema || {
    slett: function () {
        /**
         * @type {Number} depositumId
         */
        let depositumId = <?php echo (int)$depositumId;?>;
        if (confirm('Er du sikker på at du vil slette denne depositum-oppføringen?')) {
            $.ajax({
                url: '/drift/index.php?oppslag=depositum_skjema&id=' + depositumId + '&oppdrag=oppgave&oppgave=slett_depositum',
                data: {depositum_id: depositumId},
                method: "POST",
                dataType: 'json',
                success: function (data) {
                    if (data.msg) {
                        alert(data.msg);
                    }
                    if (data.url) {
                        location.href = data.url;
                    }
                }
            });
        }
    }
};

leiebasen.drift.depositumSkjema.initier = function() {
    this.felter = {
        typeFelt: $("[type='radio'][name='type']"),
        beløpFelt: $("[name='beløp']"),
        betalerFelt: $("[name='betaler']"),
        kontoFelt: $("[name='konto']"),
        garantiFelt: $("[name='garanti']"),
        utløpsdatoFelt: $("[name='utløpsdato']")
    }
    this.felter.typeFelt.on('change', function() {
        leiebasen.drift.depositumSkjema.oppdaterSkjema();
    });
    return this;
};

/**
 * Oppdater skjemaet avhengig av type depositum eller garanti
 */
leiebasen.drift.depositumSkjema.oppdaterSkjema = function () {
    let type = $("[type='radio'][name='type']:checked").val();
    let beløpLabel = $("label[for='" + $(this.felter.beløpFelt).attr('id') + "']")
    let betalerLabel = $("label[for='" + $(this.felter.betalerFelt).attr('id') + "']")
    if (type === 'garanti') {
        beløpLabel.html('Garantibeløp: ');
        betalerLabel.html('Navn på garantist: ');
        this.felter.kontoFelt.parent().hide();
        this.felter.garantiFelt.parent().show();
        this.felter.utløpsdatoFelt.parent().show();
    }
    else {
        beløpLabel.html('Deponert beløp: ');
        betalerLabel.html('Navn på betaler: ');
        this.felter.kontoFelt.parent().show();
        this.felter.garantiFelt.parent().hide();
        this.felter.utløpsdatoFelt.parent().hide();
    }
    return this;
};

leiebasen.drift.depositumSkjema.initier().oppdaterSkjema();