<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var int $tjenesteId
 * @var Meny|string $meny
 * @var string $tilbakeUrl
 * @var string $tjenesteId
 * @var string $tjenesteBeskrivelse
 * @var string $kostnadId
 * @var string $kostnadBeskrivelse
 * @var bool $fordelt
 */
use Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_kostnad_kort\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>

/**
 * @name leiebasen
 * @type {Object}
 * @name leiebasen.extJs
 * @type {Object}
 */
leiebasen.drift.delteKostnaderKostnadKort = leiebasen.drift.delteKostnaderKostnadKort || {};

leiebasen.drift.delteKostnaderKostnadKort.fordelt = <?php echo $fordelt ? 'true' : 'false';?>;

leiebasen.drift.delteKostnaderKostnadKort.bekreftAvfordeling = function() {
    let msg = 'Denne fakturaen har allerede blitt fordelt. Er du sikker på du vil omgjøre fordelingen? Dette vil kunne føre til at innkrevde andeler blir kreditert.';
    if (this.fordelt) {
        Ext.Msg.confirm({
            title: 'Gjør om fordelingen',
            msg: msg,
            buttons: Ext.Msg.OKCANCEL,
            fn: function(buttonId) {
                if (buttonId === 'ok') {
                    leiebasen.drift.delteKostnaderKostnadKort.avfordel();
                }
            },
            icon: Ext.window.MessageBox.QUESTION
        });
    }
    else {
        leiebasen.drift.delteKostnaderKostnadKort.avfordel();
    }
}

leiebasen.drift.delteKostnaderKostnadKort.avfordel = function() {
    Ext.Ajax.request({
        params: {
            'faktura_id': <?php echo $kostnadId;?>
        },
        waitMsg: 'Vent...',
        url: "/drift/index.php?oppslag=delte_kostnader_kostnad_kort&id=<?php echo $kostnadId;?>&oppdrag=oppgave&oppgave=avfordel",

        failure: function(response) {
            Ext.MessageBox.alert('Mislyktes', 'Respons-status ' + response.status + ' fra tjeneren... ');
        },
        success : function(response) {
            if(!response.responseText) {
                Ext.MessageBox.alert('Problem', 'Null respons fra tjeneren...');
            }
            var jsonRespons = Ext.JSON.decode(response.responseText, true);
            if(!jsonRespons) {
                Ext.MessageBox.alert('Problem', response.responseText);
            }
            else if (jsonRespons.success) {
                Ext.MessageBox.alert('Avfordelt', jsonRespons.msg, function () {
                    window.location.reload();
                });
            }
            else {
                Ext.MessageBox.alert('Hmm..', jsonRespons.msg, function() {
                    datasett.load();
                });
            }
        }
    });
}

leiebasen.drift.delteKostnaderKostnadKort.slett = function() {
    let msg = 'Er du sikker på du vil slette denne fakturaen/kostnaden?';
    if (this.fordelt) {
        msg = 'Er du sikker på du vil slette denne kostnaden som allerede har blitt fordelt? Dette vil kunne føre til at innkrevde andeler blir kreditert.';
    }
    Ext.Msg.confirm({
        title: 'Slett kostnaden',
        msg: msg,
        buttons: Ext.Msg.OKCANCEL,
        fn: function(buttonId) {
            if (buttonId === 'ok') {
                Ext.Ajax.request({
                    params: {
                        'faktura_id': <?php echo $kostnadId;?>
                    },
                    waitMsg: 'Vent...',
                    url: "/drift/index.php?oppslag=delte_kostnader_kostnad_kort&id=<?php echo $kostnadId;?>&oppdrag=oppgave&oppgave=slett",

                    failure: function(response) {
                        Ext.MessageBox.alert('Mislyktes', 'Respons-status ' + response.status + ' fra tjeneren... ');
                    },
                    success : function(response) {
                        if(!response.responseText) {
                            Ext.MessageBox.alert('Problem', 'Null respons fra tjeneren...');
                        }
                        var jsonRespons = Ext.JSON.decode(response.responseText, true);
                        if(!jsonRespons) {
                            Ext.MessageBox.alert('Problem', response.responseText);
                        }
                        else if (jsonRespons.success) {
                            Ext.MessageBox.alert('Slettet', jsonRespons.msg, function () {
                                window.location = '<?php echo $tilbakeUrl;?>';
                            });
                        }
                        else {
                            Ext.MessageBox.alert('Hmm..', jsonRespons.msg, function() {
                                datasett.load();
                            });
                        }
                    }
                });
            }
        },
        icon: Ext.window.MessageBox.QUESTION
    });
}


/**
 * @type {Ext.tab.Panel}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.tab.Panel
 */
var panel = Ext.create('Ext.panel.Panel', {
    title: <?php echo json_encode($kostnadBeskrivelse);?>,
    autoScroll: true,
    bodyPadding: 5,
    autoLoad: '/drift/index.php?oppslag=delte_kostnader_kostnad_kort&oppdrag=hent_data&data=detaljer&id=<?php echo $kostnadId;?>',
    frame: true,

    renderTo: 'panel',
    height: '100%',

    buttons: [{
        scale: 'medium',
        text: 'Opphev fordelingen av fakturaen/kostnaden',
        handler: function() {
            leiebasen.drift.delteKostnaderKostnadKort.bekreftAvfordeling();
        }
    }, {
        scale: 'medium',
        text: 'Opphev fordelingen og slett fakturaen/kostnaden',
        handler: function() {
            leiebasen.drift.delteKostnaderKostnadKort.slett();
        }
    }, {
        scale: 'medium',
        text: 'Skriv ut',
        handler: function() {
            window.open('/drift/index.php?oppslag=delte_kostnader_kostnad_kort&oppdrag=utskrift&id=<?php echo $kostnadId;?>');
        }
    }, {
        scale: 'medium',
        text: 'Tilbake',
        handler: function() {
            window.location = '<?php echo $tilbakeUrl;?>';
        }
    }]
});