<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\utskriftsveiviser\ExtOnReady $this
 * @var Meny|string $meny
 * @var Panel|string $skjema
 * @var string $formActionUrl
 * @var bool $utskriftAlleredeIgangsatt
 */
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel;
?>

/**
 * @name leiebasen
 * @type {Object}
 */

leiebasen.registrerUtskrift = function() {
    Ext.MessageBox.wait('Utskriften registreres', 'Vent litt...');
    Ext.Ajax.request({

        url: '/drift/index.php?oppslag=utskriftsveiviser&oppdrag=ta_imot_skjema&skjema=utskriftsbekreftelse',
        params: {},
        failure:function(){
            Ext.MessageBox.alert('Whoops! Problemer...','Oppnår ikke kontakt med databasen! Prøv igjen senere.');
        },

        success: function(response) {
            var tilbakemelding = Ext.JSON.decode(response.responseText);
            if(tilbakemelding['success'] === true) {
                Ext.MessageBox.alert('Utskriften er registrert', tilbakemelding.msg, function() {
                    window.onbeforeunload = null;
                    Ext.Msg.buttonText.yes = "Skriv ut konvolutter";
                    Ext.Msg.buttonText.no = "Nei, takk";
                    Ext.MessageBox.confirm("Vil du skrive ut konvolutter?",
                        'Før du skriver ut må du putte konvolutter i skriveren.<br>Justering av adressefeltet kan gjøres i innstillingene for leiebasen.',
                        function(buttonId) {
                            if(buttonId === 'yes' && tilbakemelding['adresser']) {
                                window.open( "/drift/index.php?oppslag=personadresser_utskrift&oppdrag=lagpdf&pdf=konvolutter&leieforhold=" + tilbakemelding['adresser'].join());
                            }
                            window.location = tilbakemelding.url;
                        }
                    );
                });
            }
            else {
                Ext.MessageBox.alert('Hmm..',tilbakemelding['msg']);
            }
        }
    });
};

leiebasen.forkastUtskrift = function() {
    Ext.MessageBox.wait('Utskriften forkastes', 'Vent litt...');
    Ext.Ajax.request({
        url: '/drift/index.php?oppslag=utskriftsveiviser&oppdrag=oppgave&oppgave=forkast_utskrift',

        failure: function() {
            Ext.MessageBox.alert('Whoops! Problemer...', 'Oppnår ikke kontakt med databasen! Prøv igjen senere.');
        },

        success: function(response) {
            var tilbakemelding = Ext.JSON.decode(response.responseText);
            if(tilbakemelding['success'] === true) {
                window.onbeforeunload = null;
                Ext.MessageBox.alert('Utskriften er forkastet', tilbakemelding.msg, function() {
                    window.location = tilbakemelding.url;
                });
            }
            else {
                Ext.MessageBox.alert('Hmm..', tilbakemelding['msg']);

            }
        }

    });
};

leiebasen.fortsett = function() {
    var girotekst = skjema.getComponent('girotekst').getValue();
    Ext.Ajax.request({
        url: '/drift/index.php?oppslag=utskriftsveiviser&oppdrag=ta_imot_skjema&skjema=girotekst',
        params: {
            girotekst: girotekst
        },
        failure: function(){
            Ext.MessageBox.alert('Whoops! Problemer...','Oppnår ikke kontakt med databasen! Prøv igjen senere.');
        },
        success: function(){
            skjema.remove('girotekst');
            var inkLeieforhold = '';
            var eksLeieforhold = '';
            var kravtyper = '';

            var formValues = skjema.getForm().getValues();
            if (formValues['ink_leieforhold[]'] !== undefined ) {
                inkLeieforhold = formValues['ink_leieforhold[]'];
            }
            if (Array.isArray(inkLeieforhold)) {
                inkLeieforhold = inkLeieforhold.join();
            }
            if (formValues['eks_leieforhold[]'] !== undefined ) {
                eksLeieforhold = formValues['eks_leieforhold[]'];
            }
            if (Array.isArray(eksLeieforhold)) {
                eksLeieforhold = eksLeieforhold.join();
            }
            if (formValues['kravtyper[]'] !== undefined ) {
                kravtyper = formValues['kravtyper[]'];
            }
            if (Array.isArray(kravtyper)) {
                kravtyper = kravtyper.join();
            }
            var tildato = formValues.tildato;
            var adskilt = formValues.adskilt;

            window.location = '<?php echo $formActionUrl;?>'
                + '&tildato=' + tildato
                + '&ink_leieforhold=' + inkLeieforhold
                + '&eks_leieforhold=' + eksLeieforhold
                + '&kravtyper=' + kravtyper
                + '&adskilt=' + adskilt;
        }
    });

}

leiebasen.bekreftEksisterendeUtskrift = function() {
    window.onbeforeunload = function() {
        return 'Du bør ikke forlate utskriftsveiviseren uten å bekrefte om utskriften skal registreres i leiebasen.';
    };
    /**
     * @type {Ext.window.MessageBox}
     */
    let utskriftsbekreftelse = Ext.create('Ext.window.MessageBox', {
        buttons: [
            {
                text: 'Vis utskriften på nytt.',
                handler: function () {
                    window.open('/drift/index.php?oppslag=giro&oppdrag=lagpdf');
                }
            },
            {
                text: 'Utskriften var OK. Registrer den.',
                handler: function () {
                    leiebasen.registrerUtskrift();
                }
            },
            {
                text: 'Utskriften skal forkastes',
                handler: function () {
                    leiebasen.forkastUtskrift();
                }
            }
        ]
    });

    let title = 'Bekreft den eksisterende utskriften';
    let msg = 'Det har allerede blitt påbegynt en utskrift som fortsatt står ubekreftet.<br>Du må bekrefte eller forkaste den påbegynte utskriften før du kan skrive ut på nytt.';
    let queryString = window.location.search;
    let urlParams = new URLSearchParams(queryString);
    if (urlParams.has('last_ned')) {
        title = 'Bekreft utskriften';
        msg = 'Om ikke utskriften lastes ned umiddelbart, kan du klikke under for å vise den på nytt.<br>Utskriften må bekreftes skrevet ut.<br>Automatisk forberedte utskrifter blir forkastet etter 24 timer.';
    }

    utskriftsbekreftelse.show({
        title: title,
        msg: msg,
        closable: false,
        fn: function(buttonId) {
            if(buttonId === 'yes') {
                leiebasen.registrerUtskrift();
            }
            else if(buttonId === 'no') {
                leiebasen.forkastUtskrift();
            }
        }
    });

    if (urlParams.has('last_ned')) {
        setTimeout(window.open('/drift/index.php?oppslag=giro&oppdrag=lagpdf'), 3000);
    }

}

/**
 * @type {Ext.form.Panel}
 */
var skjema = <?php echo $skjema;?>;

<?php if ($utskriftAlleredeIgangsatt):?>
leiebasen.bekreftEksisterendeUtskrift();
<?php endif;?>
