<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var Meny|string $meny
 * @var string $tilbakeUrl
 * @var string $framleieforholdId
 * @var string $leieforholdId
 * @var string $leieforholdBeskrivelse
 * @var string $leieforholdStartdato
 * @var string $leieforholdTildato
 * @var string $leieforholdJsonData
 */
use Kyegil\Leiebasen\Visning\drift\js\framleie_skjema\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>

/**
 * @param {int} personid
 * @param {int} framleieforhold
 */
leiebasen.stryk = function(personid, framleieforhold){
    var navn = framleietakere.getById(personid).get('navn');
    Ext.Msg.confirm({
        title: 'Slett framleietaker',
        msg: 'Er du sikker på du vil slette ' + navn + '?',
        buttons: Ext.Msg.OKCANCEL,
        /**
         * @param {string} buttonId
         */
        fn: function(buttonId) {
            if (buttonId === 'ok') {
                Ext.Ajax.request({
                    params: {
                        'personid': personid,
                        'framleieforhold': framleieforhold
                    },
                    waitMsg: 'Vent...',
                    url: "/drift/index.php?oppslag=framleie_skjema&oppdrag=oppgave&oppgave=slett_person&id=<?php echo $framleieforholdId;?>",

                    /**
                     * @param {XMLHttpRequest} response
                     */
                    failure: function(response) {
                        Ext.MessageBox.alert('Mislyktes', 'Response status ' + response.status + ' fra tjeneren... ', function() {
                            framleietakere.load();
                        });
                    },
                    /**
                     * @param {XMLHttpRequest} response
                     */
                    success : function(response) {
                        var jsonRespons = Ext.JSON.decode(response.responseText, true);
                        if (!jsonRespons) {
                            Ext.MessageBox.alert('Problem', 'Null respons fra tjeneren...', function() {
                                framleietakere.load();
                            });
                        }
                        else if (jsonRespons.success) {
                            Ext.MessageBox.alert('Slettet', jsonRespons.msg, function() {
                                framleietakere.load();
                            });
                        }
                        else {
                            Ext.MessageBox.alert('Hmm..', jsonRespons.msg, function() {
                                framleietakere.load();
                            });
                        }
                    }
                });
            }
        },
        icon: Ext.window.MessageBox.QUESTION
    });
}

/**
 * @returns {*}
 */
leiebasen.kontraktFormatert = function() {
    var dato = new Date();
    var tekst = avtalemal.getValue();
    var fradatoFormatert = '';
    var leieforholdRecord = leieforhold.findRecordByValue(leieforhold.getValue());
    var framleietakerFormatert = [];
    var framleietakereEpost = [];
    var framleietakereTelefon = [];
    var antallFramleietakere = framleietakere.getCount();
    framleietakere.each(function(framleietaker) {
        var leietakerLinje = framleietaker.get('navn');
        var fødselsdato = framleietaker.get('fødselsdato');
        if (fødselsdato) {
            leietakerLinje = leietakerLinje + ' f. ' + Ext.Date.format(fødselsdato, 'd.m.Y');
        }
        framleietakereEpost.push(antallFramleietakere > 1 ? (framleietaker.get('fornavn') + ': ' + framleietaker.get('epost')): framleietaker.get('epost'));
        framleietakereTelefon.push(antallFramleietakere > 1 ? (framleietaker.get('fornavn') + ': ' + framleietaker.get('telefon')): framleietaker.get('telefon'));
        framleietakerFormatert.push(leietakerLinje);
    });
    if (fradato.getValue()) {
        fradatoFormatert = Ext.Date.format(fradato.getValue(), 'd.m.Y');
    }
    var tildatoFormatert = '';
    if (tildato.getValue()) {
        tildatoFormatert = Ext.Date.format(tildato.getValue(), 'd.m.Y');
    }
    var oppsigelsestidFormatert = '';
    if (oppsigelsestid.getValue()) {
        oppsigelsestidFormatert = oppsigelsestid.findRecordByValue(oppsigelsestid.getValue()).get('tekst');
        tekst = tekst.replace(/{dersom oppsigelsestid}(.*){\/dersom oppsigelsestid}/gs, '$1');
        tekst = tekst.replace(/{dersom ikke oppsigelsestid}(.*){\/dersom ikke oppsigelsestid}/gs, '');
    }
    else {
        tekst = tekst.replace(/{dersom ikke oppsigelsestid}(.*){\/dersom ikke oppsigelsestid}/gs, '$1');
        tekst = tekst.replace(/{dersom oppsigelsestid}(.*){\/dersom oppsigelsestid}/gs, '');
    }

    if (leieforholdRecord && leieforholdRecord.get('er_bofellesskap')) {
        tekst = tekst.replace(/{dersom bofellesskap}(.*){\/dersom bofellesskap}/gs, '$1');
        tekst = tekst.replace(/{dersom ikke bofellesskap}(.*){\/dersom ikke bofellesskap}/gs, '');
    }
    else {
        tekst = tekst.replace(/{dersom ikke bofellesskap}(.*){\/dersom ikke bofellesskap}/gs, '$1');
        tekst = tekst.replace(/{dersom bofellesskap}(.*){\/dersom bofellesskap}/gs, '');
    }

    tekst = tekst.replace('{fradato}', fradatoFormatert);
    tekst = tekst.replace('{tildato}', tildatoFormatert);
    tekst = tekst.replace('{oppsigelsestid}', oppsigelsestidFormatert);

    tekst = tekst.replace('{framleietaker}', framleietakerFormatert.join('<br>'));
    tekst = tekst.replace('{framleietaker telefon}', framleietakereTelefon.join(', '));
    tekst = tekst.replace('{framleietaker epost}', framleietakereEpost.join(', '));

    if (leieforholdRecord) {
        tekst = tekst.replace('{framleietaker kontaktadresse}', leieforholdRecord.get('boligadresse'));
        tekst = tekst.replace('{leieholder}', leieforholdRecord.get('leietakere'));
        tekst = tekst.replace('{leieholder kontaktadresse}', leieforholdRecord.get('kontaktadresse'));
        tekst = tekst.replace('{leieholder epost}', leieforholdRecord.get('epost'));
        tekst = tekst.replace('{leieholder telefon}', leieforholdRecord.get('telefon'));
        tekst = tekst.replace('{leieobjektnr}', leieforholdRecord.get('leieobjektnr'));
        tekst = tekst.replace('{kid}', leieforholdRecord.get('kid'));
        tekst = tekst.replace('{leiebeløp}', Ext.util.Format.noMoney(leieforholdRecord.get('husleie')));
        tekst = tekst.replace('{dato}', Ext.Date.format(dato, 'd.m.Y'));
    }
    return tekst;
}

/**
 * @param {int} malId
 */
var lastMaltekst = function(malId) {
    var maltekst = malsett.getById(malId).get('tekst');
    avtalemal.setValue(maltekst);
}

Ext.define('Leieforhold', {
    extend: 'Ext.data.Model',
    idProperty: 'leieforhold',
    fields: [
        {name: 'leieforhold', type: 'int'}, // combo value is type sensitive
        {name: 'visningsfelt', type: 'string'},
        {name: 'startdato', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'tildato', type: 'date', dateFormat: 'Y-m-d', useNull: true},
        {name: 'leieobjektnr', type: 'int'},
        {name: 'husleie', type: 'float'},
        {name: 'er_bofellesskap', type: 'boolean'},
        {name: 'kid', type: 'string'},
        {name: 'kontaktadresse', type: 'string'},
        {name: 'boligadresse', type: 'string'},
        {name: 'leietakere'},
        {name: 'telefon', type: 'string'},
        {name: 'epost', type: 'string'}
    ]
});

Ext.define('Person', {
    extend: 'Ext.data.Model',
    idProperty: 'personid',
    fields: [
        {name: 'personid', type: 'int'},
        {name: 'navn', type: 'string'},
        {name: 'fornavn', type: 'string'},
        {name: 'fødselsdato', type: 'date', dateFormat: 'Y-m-d', useNull: true},
        {name: 'epost', type: 'string'},
        {name: 'telefon', type: 'string'}
    ]
});

Ext.define('Avtalemal', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'navn', type: 'string'},
        {name: 'tekst', type: 'string'}
    ]
});

/**
 * @type {Ext.data.Store}
 */
var framleietakere = Ext.create('Ext.data.Store', {
    model: 'Person',
    remoteSort: false,
    proxy: {
        type: 'ajax',
        simpleSortMode: true,
        url: '/drift/index.php?oppdrag=hent_data&oppslag=framleie_skjema&data=personliste&id=<?php echo $framleieforholdId;?>',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRows'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                let responsStreng = response.responseText;
                let tilbakemelding = Ext.JSON.decode(responsStreng, true);
                if (response.status !== 200 || !tilbakemelding) {
                    Ext.MessageBox.alert('Ugyldig respons', responsStreng);
                }
                else if (!tilbakemelding.success && tilbakemelding.msg) {
                    Ext.MessageBox.alert('Hm', tilbakemelding.msg);
                }
            }
        }
    },
    autoLoad: true
});

/**
 * @type {Ext.data.Store}
 */
var malsett = Ext.create('Ext.data.Store', {
    model: 'Avtalemal',
    remoteSort: false,
    <?php if ($framleieforholdId == '*'):?>
    autoLoad: {
        callback: function (records) {
            if (records.length === 1) {
                lastMaltekst(records[0].get('id'));
            }
            else {
                records.forEach(function(mal) {
                    malknapper.add({
                        xtype: 'button',
                        itemId: mal.get('id'),
                        text: 'Last ' + mal.get('navn'),
                        /**
                         * @param {Ext.button.Button} button
                         */
                        handler: function(button) {
                            lastMaltekst(button.getItemId());
                        }
                    });
                });
            }
        },
        scope: this
    },
    <?php endif;?>
    proxy: {
        type: 'ajax',
        simpleSortMode: true,
        url: '/drift/index.php?oppdrag=hent_data&oppslag=framleie_skjema&data=maler',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRows'
        },
        listeners: {
            exception: function(proxy, response) {
                let responsStreng = response.responseText;
                let tilbakemelding = Ext.JSON.decode(responsStreng, true);
                if (response.status !== 200 || !tilbakemelding) {
                    Ext.MessageBox.alert('Ugyldig respons', responsStreng);
                }
                else if (!tilbakemelding.success && tilbakemelding.msg) {
                    Ext.MessageBox.alert('Hm', tilbakemelding.msg);
                }
            }
        }
    }
});

/**
 * @type {Ext.data.Store}
 */
var oppsigelsestidData = Ext.create('Ext.data.Store', {
    fields: ['verdi', 'tekst'],
    data : [
        {"verdi": "", "tekst": "Ingen oppsigelsestid"},
        {"verdi": "P14D", "tekst": "14 dager"},
        {"verdi": "P1M", "tekst": "1 måned"},
        {"verdi": "P3M", "tekst": "3 måneder"}
    ]
});

/**
 * @type {Ext.container.Container}
 */
var malknapper = Ext.create('Ext.container.Container', {
    layout: 'hbox',
    defaults: {
        margin: '0 10 5 0'
    }
});

/**
 * @type {Ext.data.Store}
 */
var leieforholddata = Ext.create('Ext.data.Store', {
    model: 'Leieforhold',
    remoteSort: false,
    <?php if ($leieforholdId):?>
    data: <?php echo $leieforholdJsonData; ?>,
    <?php else:?>
    proxy: {
        type: 'ajax',
        simpleSortMode: true,
        url: '/drift/index.php?oppslag=framleie_skjema&oppdrag=hent_data&data=leieforhold',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRows'
        },
        listeners: {
            exception: function(proxy, response) {
                let responsStreng = response.responseText;
                let tilbakemelding = Ext.JSON.decode(responsStreng, true);
                if (response.status !== 200 || !tilbakemelding) {
                    Ext.MessageBox.alert('Ugyldig respons', responsStreng);
                }
                else if (!tilbakemelding.success && tilbakemelding.msg) {
                    Ext.MessageBox.alert('Hm', tilbakemelding.msg);
                }
            }
        }
    },
    autoLoad: true,
    <?php endif;?>
    pageSize: 50
});

/**
 * @type {Ext.form.field.ComboBox}
 */
var leieforhold = Ext.create('Ext.form.field.ComboBox', {
    allowBlank: false,
    displayField: 'visningsfelt',
    editable: true,
    readOnly: <?php echo $leieforholdId ? 'true' : 'false';?>,
    emptyText: '<?php echo $leieforholdId ? '' : 'Velg leieforhold som er framleid';?>',
    fieldLabel: 'Leieforhold',
    forceSelection: true,
    hideLabel: false,
    listeners: {
        /**
         * @param {Ext.form.field.ComboBox} combo
         * @param {Ext.data.Model[]} records
         */
        select: function( combo, records ) {
            var mindato = records[0].get('startdato');
            var maxdato = records[0].get('tildato');
            fradato.setMinValue( mindato );
            fradato.setMaxValue( maxdato );
            tildato.setMinValue( mindato );
            tildato.setMaxValue( maxdato );
        }
    },
    listWidth: 700,
    maxHeight: 600,
    matchFieldWidth: false,
    minChars: 3,
    name: 'leieforhold',
    queryMode: 'remote',
    selectOnFocus: true,
    store: leieforholddata,
    typeAhead: false,
    valueField: 'leieforhold',
    listConfig: {
        loadingText: 'Søker ...',
        emptyText: 'Ingen treff...',
        maxHeight: 600,
        width: 600
    },
    width: 700
});

/**
 * @type {Ext.form.field.ComboBox}
 */
var oppsigelsestid = Ext.create('Ext.form.field.ComboBox', {
    allowBlank: false,
    displayField: 'tekst',
    editable: false,
    fieldLabel: 'Oppsigelsestid',
    forceSelection: true,
    hideLabel: false,
    name: 'oppsigelsestid',
    queryMode: 'local',
    selectOnFocus: true,
    store: oppsigelsestidData,
    typeAhead: false,
    valueField: 'verdi',
    value: 'P1M'
});

/**
 * @type {Ext.form.field.Date}
 */
var fradato = Ext.create('Ext.form.field.Date', {
    allowBlank: false,
    fieldLabel: 'Framleie fra dato',
    format: 'd.m.Y',
    name: 'fradato',
    submitFormat: 'Y-m-d',
    width: 200
});

/**
 * @type {Ext.form.field.Date}
 */
var tildato = Ext.create('Ext.form.field.Date', {
    allowBlank: false,
    fieldLabel: 'Framleie til dato',
    format: 'd.m.Y',
    name: 'tildato',
    submitFormat: 'Y-m-d',
    width: 200
});

/**
 * @type {Ext.form.field.HtmlEditor}
 */
var avtalemal = Ext.create('Ext.form.field.HtmlEditor', {
    allowBlank: false,
    fieldLabel: 'Framleie-avtaletekst',
    name: 'avtalemal',
    height: 200
});

/**
 * @type {Ext.grid.Panel}
 */
var framleietakerListe = Ext.create('Ext.grid.Panel', {
    autoScroll: true,
    layout: 'border',
    store: framleietakere,
    title: 'Framleid til',
    columns: [
        {
            dataIndex: 'navn',
            header: 'Navn',
            sortable: true,
            flex: 1,
            width: 200
        },
        {
            dataIndex: 'personid',
            header: 'Slett',
            renderer: function(v){
                return "<a style=\"cursor: pointer\" onClick=\"leiebasen.stryk(" + v + ", '<?php echo $framleieforholdId;?>')\"><img src=\"/pub/media/bilder/drift/slett.png\"></a>";
            },
            sortable: false,
            width: 50
        }
    ],
    height: 130,
    buttons: [{
        text: 'Legg til ny person',
        handler: function(){
            fradato.allowBlank = true;
            tildato.allowBlank = true;
            leieforhold.allowBlank = true;
            skjema.form.submit({
                url:'/drift/index.php?oppslag=framleie_skjema&id=<?php echo $framleieforholdId;?>&oppdrag=taimotskjema&leggtil=1'
            });
        }
    }]
});

/**
 * @type {Ext.button.Button}
 */
var avbryt = Ext.create('Ext.button.Button', {
    text: 'Avbryt',
    id: 'avbryt',
    handler: function() {
        Ext.Ajax.request({
            url: "/drift/index.php?oppslag=framleie_skjema&oppdrag=oppgave&oppgave=avbryt_registrering",

            callback: function() {
                window.location = '<?php echo $tilbakeUrl;?>';
            }
        });
    }
});

/**
 * @type {Ext.button.Button}
 */
var lagreknapp = Ext.create('Ext.button.Button', {
    text: 'Lagre',
    disabled: true,
    handler: function() {
        skjema.getForm().submit({
            url: '/drift/index.php?oppslag=framleie_skjema&id=<?php echo $framleieforholdId;?>&oppdrag=taimotskjema',
            waitMsg: 'Lagrer...',
            params: {
                avtale: leiebasen.kontraktFormatert()
            }
        });
    }
});

/**
 * @type {Ext.button.Button}
 */
var forhåndsvisningsKnapp = Ext.create('Ext.button.Button', {
    text: 'Forhåndsvis',
    handler: function() {
        Ext.create('Ext.window.Window', {
            title: 'Hello',
            height: '90%',
            width: '90%',
            layout: 'fit',
            items: [{
                xtype: 'panel',
                autoScroll: true,
                html: leiebasen.kontraktFormatert()
            }]
        }).show();
    }
});

/**
 * @type {Ext.form.Panel}
 */
var skjema = Ext.create('Ext.form.Panel', {
    renderTo: 'panel',
    autoScroll: true,
    frame: true,
    title: 'Framleie',
    bodyPadding: 5,
    standardSubmit: false,
    height: '100%',
    items: [
        leieforhold,
        framleietakerListe,
        {
            xtype: 'container',
            layout: 'hbox',
            defaults: {
                margin: '0 10 5 0'
            },
            items: [
                fradato,
                tildato,
                oppsigelsestid
            ]
        },
        avtalemal,
        malknapper
    ],
    buttons: [
        forhåndsvisningsKnapp,
        avbryt,
        lagreknapp
    ]
});

skjema.getForm().load({
    url: '/drift/index.php?oppslag=framleie_skjema&id=<?php echo $framleieforholdId;?>&oppdrag=hent_data',
    waitMsg:'Laster ...'
});

skjema.on({

    /**
     *
     * @param {Ext.form.Basic} form
     * @param {Ext.form.action.Action} action
     * @returns {boolean}
     */
    beforeAction: function(form, action) {
        if (action.type === 'submit' && framleietakere.getCount() < 1 && !action.getUrl().includes('leggtil=1')) {
            Ext.MessageBox.alert('Vent litt..', 'Du må oppgi minst én framleietaker');
            return false;
        }
    },

    /**
     *
     * @param {Ext.form.Basic} form
     * @param {Ext.form.action.Action} action
     */
    actioncomplete: function(form, action){
        if (action.type === 'load') {
            <?php if($leieforholdId):?>
            lagreknapp.enable();
            <?php else:?>
            var responsObjekt = Ext.decode(action.response.responseText);
            if (responsObjekt && typeof responsObjekt.data.leieforhold !== 'undefined') {
                var leieforholdId = responsObjekt.data.leieforhold;
                leieforholddata.load({
                    scope: this,
                    params: {query: leieforholdId},
                    callback: function() {
                        var leieforholdRecord = leieforholddata.findRecord('leieforhold', leieforholdId);
                        if (leieforholdRecord) {
                            leieforhold.select(leieforholdRecord);
                            leieforhold.fireEvent('select', leieforhold, [leieforholdRecord]);
                        }
                        lagreknapp.enable();
                    }
                });
            }
            else {
                lagreknapp.enable();
            }
            <?php endif;?>
        }

        if (action.type === 'submit'){
            if (action.response.responseText === '') {
                Ext.MessageBox.alert('Problem', 'Mottok ikke bekreftelsesmelding fra tjeneren  i JSON-format som forventet');
            } else {
                var avtaleId = (typeof action.result.id != 'undefined') ? action.result.id : false;
                var leggTil = (typeof action.result.leggTil != 'undefined') ? action.result.leggTil : false;
                var url = (typeof action.result.url != 'undefined') ? action.result.url : false;
                if (leggTil) {
                    if (url) {
                        window.location = url;
                    }
                }
                else {
                    Ext.MessageBox.alert('Suksess', 'Opplysningene er lagret', function() {
                        if (avtaleId) {
                            window.open('/drift/index.php?oppdrag=hent_data&oppslag=framleie_skjema&data=utskrift&id=' + avtaleId);
                        }
                        if (url) {
                            window.location = url;
                        }
                    });
                }
            }
        }
    },

    /**
     * @param {Ext.form.Basic} form
     * @param {Ext.form.action.Action} action
     */
    actionfailed: function(form, action){
        var result;
        if (action.type === 'load') {
            if (action.failureType === "connect") {
                Ext.MessageBox.alert('Problem:', 'Klarte ikke laste data. Fikk ikke kontakt med tjeneren.');
            }
            else {
                if (!action.response.responseText) {
                    Ext.MessageBox.alert('Problem:', 'Skjemaet mottok ikke data i JSON-format som forventet');
                }
                else {
                    result = Ext.decode(action.response.responseText);
                    if (result && result.msg) {
                        Ext.MessageBox.alert('Mottatt tilbakemelding om feil:', action.result.msg);
                    }
                    else {
                        Ext.MessageBox.alert('Problem:', 'Innhenting av data mislyktes av ukjent grunn. (trolig manglende success-parameter i den returnerte datapakken). Action type='+action.type+', failure type='+action.failureType);
                    }
                }
            }
        }
        else if (action.type === 'submit') {
            if (action.failureType === "client") {
                Ext.MessageBox.alert('Problem:', 'Sjekk skjemaet for feil.');
            }
            if (action.failureType === "connect") {
                Ext.MessageBox.alert('Problem:', 'Klarte ikke lagre data. Fikk ikke kontakt med tjeneren.');
            }
            else {
                result = Ext.decode(action.response.responseText);
                if (result && result.msg) {
                    Ext.MessageBox.alert('Mottatt tilbakemelding om feil:', result.msg);
                }
                else {
                    Ext.MessageBox.alert('Problem:', 'Lagring av data mislyktes av ukjent grunn. Action type='+action.type+', failure type='+action.failureType);
                }
            }
        }

    }
});

<?php if ($leieforholdId):?>
leieforhold.select(<?php echo $leieforholdId;?>);
leieforhold.fireEvent('select', leieforhold, [leieforholddata.getAt(0)]);
<?php endif;?>
<?php if (intval($framleieforholdId)):?>
lagreknapp.enable();
<?php endif;?>
