<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\leieforhold_personer_skjema\Index $this
 * @var int $leieforholdId
 * @var string $skript
 */
?>

leiebasen.drift.leieforholdPersonerSkjema = leiebasen.drift.leieforholdPersonerSkjema || {
    initier: function() {
        if(typeof this._initiert !== 'undefined') {
            return this;
        }

        $("[name^='personer'][name$='[er_org]']").on('change', function() {
            let $adresseKort = $(this).closest('.col');
            leiebasen.drift.leieforholdPersonerSkjema.oppdaterLeietakerNavnFelter($adresseKort);
        });

        $(".autofyll-adresse").on('click', function() {
            let $adresseKort = $(this).closest('.col');
            leiebasen.drift.leieforholdPersonerSkjema.autoFyllAdresse($adresseKort);
        });

        $(".autofyll-adresser").on('click', function() {
            $(".col").each(function () {
                leiebasen.drift.leieforholdPersonerSkjema.autoFyllAdresse($(this));
            });
        });

        $(".col").each(function () {
            leiebasen.drift.leieforholdPersonerSkjema.oppdaterLeietakerNavnFelter($(this));
        });

        this._initiert = true;
    },

    /**
     * @param {jQuery} $adresseKort
     */
    oppdaterLeietakerNavnFelter: function($adresseKort) {
        let $erOrgFelt = $adresseKort.find("[type='checkbox'][name$='[er_org]']");
        let $fornavnFelt = $adresseKort.find("[name$='[fornavn]']");
        let $etternavnFelt = $adresseKort.find("[name$='[etternavn]']");
        let $fødselsdatoFelt = $adresseKort.find("[name$='[fødselsdato]']");
        let $personnrFelt = $adresseKort.find("[name$='[personnr]']");

        let erOrg = $erOrgFelt.is(':checked');
        $fornavnFelt.attr('required', !erOrg);
        $fornavnFelt.parent().toggle(!erOrg);
        $fødselsdatoFelt.parent().toggle(!erOrg);

        $etternavnLabel = $adresseKort.find("label[for='" + $etternavnFelt.attr('id') + "']");
        $etternavnLabel.html(erOrg ? 'Navn' : 'Etternavn');

        $personnrFelt.attr('pattern', erOrg ? '[8-9][0-9]{8}' : '[0-9]{5}|[0-9]{11}');
        $personnrLabel = $adresseKort.find("label[for='" + $personnrFelt.attr('id') + "']");
        $personnrLabel.html(erOrg ? 'Organisasjonsnr (9 sifre)' : 'Personnummer (5 eller 11 sifre)');
    },
    /**
     * @param {jQuery} $adresseKort
     */
    autoFyllAdresse: function($adresseKort) {
        let $adresse1Felt = $adresseKort.find("[name$='[adresse1]']");
        let $adresse2Felt = $adresseKort.find("[name$='[adresse2]']");
        let $postnrFelt = $adresseKort.find("[name$='[postnr]']");
        let $poststedFelt = $adresseKort.find("[name$='[poststed]']");
        let $landFelt = $adresseKort.find("[name$='[land]']");

        $adresse1Felt.val($(".leieobjekt-adresse.adresse1 input").val())
        $adresse2Felt.val($(".leieobjekt-adresse.adresse2 input").val())
        $postnrFelt.val($(".leieobjekt-adresse.postnr input").val())
        $poststedFelt.val($(".leieobjekt-adresse.poststed input").val())
        $landFelt.val($(".leieobjekt-adresse.land input").val())
    }
};

<?php echo $skript;?>

leiebasen.drift.leieforholdPersonerSkjema.initier();
