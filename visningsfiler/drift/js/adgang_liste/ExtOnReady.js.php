<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var Meny|string $meny
 * @var string $tilbakeUrl
 */
use Kyegil\Leiebasen\Visning\drift\js\adgang_liste\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>

leiebasen.lastRutenett = function() {
    datasett.getProxy().extraParams = {
        søkefelt: søkefelt.getValue()
    };
    sidevelger.moveFirst();
    datasett.load({
        params: {
            start: 0,
            limit: 300
        }
    });
}

/**
 * @param {Ext.data.Model} record
 */
leiebasen.slettOppslag = function(record){
    var oppslagId = record.get('id');
    Ext.Msg.confirm({
        title: 'Slett adgangen',
        msg: 'Er du sikker på du vil slette denne adgangen for ' + record.get('bruker') + '?',
        buttons: Ext.Msg.OKCANCEL,
        fn: function(buttonId) {
            if (buttonId === 'ok') {
                Ext.Ajax.request({
                    params: {
                        'adgang_id': oppslagId
                    },
                    waitMsg: 'Vent...',
                    url: "/drift/index.php?oppslag=adgang_skjema&oppdrag=oppgave&oppgave=slett_adgang",

                    failure: function(response, opts) {
                        Ext.MessageBox.alert('Mislyktes', 'Respons-status ' + response.status + ' fra tjeneren... ', function() {
                            datasett.load();
                        });
                    },
                    success : function(response, opts) {
                        if(!response.responseText) {
                            Ext.MessageBox.alert('Problem', 'Null respons fra tjeneren...', function() {
                                datasett.load();
                            });
                        }
                        var jsonRespons = Ext.JSON.decode(response.responseText, true);
                        if(!jsonRespons) {
                            Ext.MessageBox.alert('Problem', response.responseText, function() {
                                datasett.load();
                            });
                        }
                        else if (jsonRespons.success) {
                            Ext.MessageBox.alert('Slettet', jsonRespons.msg, function() {
                                datasett.load();
                            });
                        }
                        else {
                            Ext.MessageBox.alert('Hmm..', jsonRespons.msg, function() {
                                datasett.load();
                            });
                        }
                    }
                });
            }
        },
        icon: Ext.window.MessageBox.QUESTION
    });
}

/**
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext-method-define
 */
Ext.define('leiebasen.Adgang', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [ // {@link http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.Field}
        {name: 'id', type: 'int'},
        {name: 'person_id', type: 'int'},
        {name: 'bruker', type: 'string'},
        {name: 'adgangsområde', type: 'string'},
        {name: 'leieforhold_id', type: 'int'},
        {name: 'leieforhold', type: 'string'}
    ]
});

/**
 * @type {Ext.grid.plugin.CellEditing}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.plugin.CellEditing
 */
var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
    clicksToEdit: 1
});

/**
 * @type {Ext.grid.plugin.RowEditing}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.plugin.RowEditing
 */
var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
    autoCancel: false,
    listeners: {
        beforeedit: function (grid, e, eOpts) {
            return e.column.xtype !== 'actioncolumn';
        }
    }
});

/**
 * @type {Ext.data.Store}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.Store
 */
var datasett = Ext.create('Ext.data.Store', {
    model: 'leiebasen.Adgang',
    pageSize: 300,
    remoteSort: true,
    proxy: {
        type: 'ajax',
        simpleSortMode: true,
        url: "/drift/index.php?oppslag=adgang_liste&oppdrag=hent_data",
        timeout: 300000, // 5 mins
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRows'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                let responsStreng = response.responseText;
                let tilbakemelding = Ext.JSON.decode(responsStreng, true);
                if (response.status !== 200 || !tilbakemelding) {
                    Ext.MessageBox.alert('Ugyldig respons', responsStreng);
                }
                else if (!tilbakemelding.success && tilbakemelding.msg) {
                    Ext.MessageBox.alert('Hm', tilbakemelding.msg);
                }
            }
        }
    },
    sorters: [{
        property: 'id',
        direction: 'DESC'
    }]
});

/**
 * @type {Ext.form.field.Text}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Text
 */
var søkefelt = Ext.create('Ext.form.field.Text', {
    emptyText: 'Søk og klikk ↵',
    name: 'søkefelt',
    width: 200,
    listeners: {
        specialkey: function( felt, e, eOpts ) {
            datasett.getProxy().extraParams = {
                søkefelt: søkefelt.getValue()
            };
            leiebasen.lastRutenett();
        }
    }
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var id = Ext.create('Ext.grid.column.Column', {
    align:		'right',
    dataIndex:	'id',
    text:		'Nr.',
    sortable:	true,
    hidden:		true,
    width:		40
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var bruker = Ext.create('Ext.grid.column.Column', {
    dataIndex:	'bruker',
    text:		'Bruker',
    sortable:	true,
    width:		100,
    flex:		1
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var adgangsområde = Ext.create('Ext.grid.column.Column', {
    dataIndex:	'adgangsområde',
    text:		'Område',
    sortable:	true,
    width:		100
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var leieforhold_id = Ext.create('Ext.grid.column.Column', {
    dataIndex:	'leieforhold_id',
    text:		'Leieforhold',
    sortable:	true,
    width:		100,
    flex:		1,
    /**
     * Renderer
     *
     * @param value
     * @param metaData
     * @param record
     * @returns {string}
     */
    renderer:	function(value, metaData, record) {
        if (value) {
            return value + ': ' + record.get('leieforhold');
        }
    }
});

/**
 * @type {Ext.grid.column.Action}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Action
 */
var vis  = Ext.create('Ext.grid.column.Action', {
    header:		'Detaljer',
    dataIndex: 'id',
    icon: '/pub/media/bilder/drift/rediger.png',
    tooltip: 'Inspiser eller endre',
    handler: function(grid, rowIndex, colIndex, item, e, record) {
        window.location = '/drift/index.php?oppslag=adgang_skjema&id=' + record.get('id');
    },
    sortable: false,
    width: 50
});

/**
 * @type {Ext.grid.column.Action}
 */
var slett  = Ext.create('Ext.grid.column.Action', {
    header:		'Slett',
    dataIndex: 'id',
    icon: '/pub/media/bilder/drift/slett.png',
    tooltip: 'Slett',
    handler: function(grid, rowIndex, colIndex, item, e, record, row) {
        leiebasen.slettOppslag(record);
    },
    sortable: false,
    width: 50
});

/**
 * @type {Ext.toolbar.Paging}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.toolbar.Paging
 */
var sidevelger = Ext.create('Ext.toolbar.Paging', {
    xtype: 'pagingtoolbar',
    store: datasett,
    dock: 'bottom',
    displayInfo: true
});

/**
 * @type {Ext.grid.Panel}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.Panel
 */
var rutenett = Ext.create('Ext.grid.Panel', {
    autoScroll: true,
    layout: 'border',
    frame: false,
    store: datasett,
    title: 'Brukeres adganger til områder og leieforhold',
    tbar: [
        søkefelt
    ],
    columns: [
        id,
        bruker,
        adgangsområde,
        leieforhold_id,
        vis,
        slett
    ],
    renderTo: 'panel',
    height: '100%',

    dockedItems: [sidevelger],

    buttons: [{
        text: 'Tilbake',
        handler: function() {
            window.location = '<?php echo $tilbakeUrl;?>';
        }
    }, {
        text: 'Registrer ny adgang',
        handler: function() {
            window.location = "/drift/index.php?oppslag=adgang_skjema&id=*";
        }
    }]
});

rutenett.getStore().on({
    load: function(store, records, successful) {
        var jsonData = store.getProxy().getReader().jsonData;
        if (typeof jsonData == 'undefined') {
            Ext.MessageBox.alert('Problem:', 'Klarte ikke laste denne tabellen av ukjent grunn.');
            return false;
        }
        if (!successful) {
            var msg = store.getProxy().getReader().jsonData.msg;
            msg = msg.replace(/\r\n|\r|\n/g, '<br>');
            Ext.MessageBox.alert('Klarte ikke laste data:', msg);
        }
    }
});

datasett.getProxy().extraParams = {
    søkefelt: søkefelt.getValue()
};

leiebasen.lastRutenett();