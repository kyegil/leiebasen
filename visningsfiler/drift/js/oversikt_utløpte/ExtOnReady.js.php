<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var Meny|string $meny
 * @var string $tilbakeUrl
 */
use Kyegil\Leiebasen\Visning\drift\js\oversikt_utløpte\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>
leiebasen.drift.oversiktUtløpte = {
    opphevTidsbegrensning: function(id) {
        Ext.Msg.show({
            title: 'Opphev tidsbegrensingen',
            id: id,
            msg: 'Er du sikker på at du vil oppheve tidsbegrensningen i leieforhold nr. ' + id + '?<br>Leieforholdet vil da løpe til det blir sagt opp.',
            buttons: Ext.Msg.OKCANCEL,
            fn: function(buttonId){
                if(buttonId === 'ok') {
                    Ext.Ajax.request({
                        waitMsg: 'Fjerner tidsbegrensningen...',
                        params: {
                            'leieforhold_id': id
                        },
                        url: "/drift/index.php?oppslag=oversikt_utløpte&oppdrag=oppgave&oppgave=opphev_tidsbegrensning",
                        success: function(response){
                            var tilbakemelding = Ext.JSON.decode(response.responseText);
                            if(tilbakemelding.success === true) {
                                Ext.MessageBox.alert('Utført', tilbakemelding.msg, function(){
                                    panel.getLoader().load({
                                        url: "/drift/index.php?oppslag=oversikt_utløpte&oppdrag=hentdata"
                                    });
                                });
                            }
                            else {
                                Ext.MessageBox.alert('Hmm..', tilbakemelding.msg);
                            }
                        }
                    });
                }
            },
            animEl: 'elId',
            icon: Ext.MessageBox.QUESTION
        });
    }
}

var panel = Ext.create('Ext.panel.Panel', {
    autoLoad: '/drift/index.php?oppslag=oversikt_utløpte&oppdrag=hentdata',
    autoScroll: true,
    bodyStyle: 'padding: 5px',
    title: 'Leieavtaler som bør fornyes',
    frame: true,
    plain: false,
    height: '100%'
});

panel.render('panel');

