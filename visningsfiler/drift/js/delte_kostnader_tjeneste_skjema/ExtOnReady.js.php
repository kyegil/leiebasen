<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var int $tjenesteId
 * @var Meny|string $meny
 * @var string $initLeiebasenJsObjektEgenskaper
 * @var Visning|string $skjema
 * @var string $formActionUrl
 */
use Kyegil\Leiebasen\Visning;use Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_tjeneste_skjema\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>

/**
 * @name leiebasen
 * @type {Object}
 */
<?php echo $initLeiebasenJsObjektEgenskaper;?>

/**
 * @type {Ext.form.Panel}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.Panel
 */
var skjema = <?php echo $skjema;?>;

/**
 * @type {Ext.button.Button}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.button.Button
 */
var saveButton = Ext.getCmp('save-button');

saveButton.setHandler(function() {
    skjema.getForm().submit({
        waitMsg: "Lagrer...",
        url: <?php echo json_encode($formActionUrl);?>
    });
});
