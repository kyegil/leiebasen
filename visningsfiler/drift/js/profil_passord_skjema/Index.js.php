<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\profil_passord_skjema\Index $this
 */
?>
leiebasen.drift.validerPassord = function() {
    let pw1 = $("#pw1")[0].value;
    let pw1Esc = pw1.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
    let re = new RegExp(pw1Esc, "i");
    $("#pw2")[0].setAttribute('pattern', re.source);
}
$("#pw1").change(function()
{
    leiebasen.drift.validerPassord();
});