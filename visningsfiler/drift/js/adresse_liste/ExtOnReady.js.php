<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var Meny|string $meny
 * @var string $tilbakeUrl
 */
use Kyegil\Leiebasen\Visning\drift\js\adresse_liste\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>

leiebasen.lastRutenett = function() {
    datasett.getProxy().extraParams = {
        søkefelt: søkefelt.getValue(),
        leietakere: (leietakerfilter.getValue() ? 1 : 0)
    };
    sidevelger.moveFirst();
    datasett.load({
        params: {
            start: 0,
            limit: 300
        }
    });
}

/**
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext-method-define
 */
Ext.define('leiebasen.Person', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [ // {@link http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.Field}
        {name: 'id', type: 'int'},
        {name: 'fornavn', type: 'string'},
        {name: 'etternavn', type: 'string'},
        {name: 'er_org', type: 'bool'},
        {name: 'fødselsdato', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'personnr'},
        {name: 'adresse'},
        {name: 'postnr'},
        {name: 'poststed'},
        {name: 'land'},
        {name: 'telefon'},
        {name: 'mobil'},
        {name: 'epost'},
        {name: 'html'}
    ]
});

/**
 * @type {Ext.grid.plugin.CellEditing}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.plugin.CellEditing
 */
var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
    clicksToEdit: 1
});

/**
 * @type {Ext.grid.plugin.RowEditing}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.plugin.RowEditing
 */
var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
    autoCancel: false,
    listeners: {
        beforeedit: function (grid, e) {
            return e.column.xtype !== 'actioncolumn';
        }
    }
});

/**
 * @type {Ext.data.Store}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.Store
 */
var datasett = Ext.create('Ext.data.Store', {
    model: 'leiebasen.Person',
    pageSize: 300,
    remoteSort: true,
    proxy: {
        type: 'ajax',
        simpleSortMode: true,
        url: "/drift/index.php?oppslag=adresse_liste&oppdrag=hent_data",
        timeout: 300000, // 5 mins
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRows'
        },
        listeners: {
            exception: function(proxy, response) {
                let responsStreng = response.responseText;
                let tilbakemelding = Ext.JSON.decode(responsStreng, true);
                if (response.status !== 200 || !tilbakemelding) {
                    Ext.MessageBox.alert('Ugyldig respons', responsStreng);
                }
                else if (!tilbakemelding.success && tilbakemelding.msg) {
                    Ext.MessageBox.alert('Hm', tilbakemelding.msg);
                }
            }
        }
    },
    sorters: [{
        property: 'etternavn',
        direction: 'ASC'
    }]
});

/**
 * @type {Ext.form.field.Text}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Text
 */
var søkefelt = Ext.create('Ext.form.field.Text', {
    emptyText: 'Søk og klikk ↵',
    name: 'søkefelt',
    width: 200,
    listeners: {
        specialkey: function() {
            datasett.getProxy().extraParams = {
                søkefelt: søkefelt.getValue()
            };
            leiebasen.lastRutenett();
        }
    }
});

/**
 * @type {Ext.form.field.Checkbox}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Checkbox
 */
var leietakerfilter = Ext.create('Ext.form.field.Checkbox', {
    boxLabel: 'Kun nåværende leietakere',
    name: 'leietakerfilter',
    inputValue: '1',
    uncheckedValue: '0',
    checked: true,
    width: 200,
    listeners: {
        change: function() {
            leiebasen.lastRutenett();
        }
    }
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var personid = Ext.create('Ext.grid.column.Column', {
    align:		'right',
    dataIndex:	'id',
    text:		'Nr.',
    sortable:	true,
    hidden:		true,
    width:		40
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var fornavn = Ext.create('Ext.grid.column.Column', {
    dataIndex:	'fornavn',
    text:		'Fornavn',
    sortable:	true,
    width:		100
});

/**
 * @type {Ext.grid.column.Column}
 */
var etternavn = Ext.create('Ext.grid.column.Column', {
    dataIndex:	'etternavn',
    text:		'Etternavn',
    sortable:	true,
    width:		100
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var fødselsdato = Ext.create('Ext.grid.column.Column', {
    dataIndex:	'fødselsdato',
    text:		'Fødselsdato',
    renderer:	Ext.util.Format.dateRenderer('d.m.Y'),
    sortable:	true,
    width:		100
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var adresse = Ext.create('Ext.grid.column.Column', {
    dataIndex:	'adresse',
    text:		'Adresse',
    sortable:	false,
    width:		200,
    flex:		1
});

/**
 * @type {Ext.grid.column.Column}
 */
var telefon = Ext.create('Ext.grid.column.Column', {
    dataIndex:	'telefon',
    text:		'Tlf.',
    sortable:	true,
    width:		80,
    renderer:	function(value) {
        if(value) return '<a href="tel:' + value + '">'+ value + '</a>';
    }
});

/**
 * @type {Ext.grid.column.Column}
 */
var mobil = Ext.create('Ext.grid.column.Column', {
    dataIndex:	'mobil',
    text:		'Mobil',
    sortable:	true,
    width:		100,
    renderer:	function(value) {
        if(value) return '<a href="tel:' + value + '">'+ value + '</a>';
    }
});

/**
 * @type {Ext.grid.column.Column}
 */
var epost = Ext.create('Ext.grid.column.Column', {
    dataIndex:	'epost',
    text:		'E-post',
    sortable:	true,
    width:		120,
    renderer:	function(value) {
        if(value) return '<a href="mailto:' + value + '">'+ value + '</a>';
    }
});

/**
 * @type {Ext.grid.column.Action}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Action
 */
var vis  = Ext.create('Ext.grid.column.Action', {
    header:		'Vis',
    dataIndex: 'id',
    icon: '/pub/media/bilder/drift/vis.svg',
    tooltip: 'Vis',
    handler: function(grid, rowIndex, colIndex, item, e, record) {
        window.location = '/drift/index.php?oppslag=personadresser_kort&id=' + record.get('id');
    },
    sortable: false,
    width: 50
});

/**
 * @type {Ext.toolbar.Paging}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.toolbar.Paging
 */
var sidevelger = Ext.create('Ext.toolbar.Paging', {
    xtype: 'pagingtoolbar',
    store: datasett,
    dock: 'bottom',
    displayInfo: true
});

/**
 * @type {Ext.grid.Panel}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.Panel
 */
var rutenett = Ext.create('Ext.grid.Panel', {
    autoScroll: true,
    layout: 'border',
    frame: false,
    store: datasett,
    title: 'Komplett adresseliste',
    tbar: [
        søkefelt,
        leietakerfilter
    ],
    columns: [
        personid,
        fornavn,
        etternavn,
        fødselsdato,
        adresse,
        telefon,
        mobil,
        epost,
        vis
    ],
    renderTo: 'panel',
    height: '100%',

    dockedItems: [sidevelger],
    plugins: [{
        ptype: 'rowexpander',
        rowBodyTpl : ['{html}']
    }],

    buttons: [{
        text: 'Eksportér',
        handler: function() {
            var sorters = datasett.sorters.items[0];
            var søk = søkefelt.getValue();
            var leietakere = leietakerfilter.getValue() ? 1 : 0;

            var url = '/drift/index.php?oppslag=adresse_liste&oppdrag=hent_data&data=eksport_csv';
            url += '&sort=' + sorters.property + '&dir=' + sorters.direction;
            url += '&leietakere=' + leietakere;
            if( søk ) {
                url += '&søkefelt=' + søk;
            }

            window.open(url);
        }
    }, {
        text: 'Tilbake',
        handler: function() {
            window.location = '<?php echo $tilbakeUrl;?>';
        }
    }, {
        text: 'Registrer ny kontakt',
        handler: function() {
            window.location = "/drift/person_adresse_skjema/*";
        }
    }]
});

rutenett.getStore().on({
    load: function(store, records, successful) {
        var jsonData = store.getProxy().getReader().jsonData;
        if (typeof jsonData == 'undefined') {
            Ext.MessageBox.alert('Problem:', 'Klarte ikke laste denne tabellen av ukjent grunn.');
            return false;
        }
        if (!successful) {
            var msg = store.getProxy().getReader().jsonData.msg;
            msg = msg.replace(/\r\n|\r|\n/g, '<br>');
            Ext.MessageBox.alert('Klarte ikke laste data:', msg);
        }
    }
});

datasett.getProxy().extraParams = {
    søkefelt: søkefelt.getValue()
};

leiebasen.lastRutenett();