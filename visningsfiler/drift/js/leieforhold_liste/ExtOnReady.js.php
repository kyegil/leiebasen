<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var Meny|string $meny
 * @var string $tilbakeUrl
 */
use Kyegil\Leiebasen\Visning\drift\js\leieforhold_liste\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>

leiebasen.drift.leieforholdListe = {
    lastRutenett: function() {
        datasett.getProxy().extraParams = {
            søkefelt: søkefelt.getValue(),
            nåværendeFilter: nåværendeFilter.getValue() ? 1 : 0
        };
        sidevelger.moveFirst();
        datasett.load({
            params: {
                start: 0,
                limit: 300
            }
        });
    }
}
/**
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext-method-define
 */
Ext.define('leiebasen.Leieforhold', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [ // {@link http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.Field}
        {name: 'leieforhold_id', type: 'int'},
        {name: 'kontrakt_id', type: 'int'},
        {name: 'kid', type: 'string'},
        {name: 'leiebeløp', type: 'float'},
        {name: 'avsluttet', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'til_dato', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'leieobjekt_id', type: 'float'},
        {name: 'frosset', type: 'bool'},
        {name: 'signert_fil', type: 'bool'},
        {name: 'leieforhold_beskrivelse'},
        {name: 'leieobjekt_beskrivelse'},
        {name: 'html'}
    ]
});

/**
 * @type {Ext.grid.plugin.CellEditing}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.plugin.CellEditing
 */
var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
    clicksToEdit: 1
});

/**
 * @type {Ext.grid.plugin.RowEditing}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.plugin.RowEditing
 */
var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
    autoCancel: false,
    listeners: {
        beforeedit: function (grid, e) {
            return e.column.xtype !== 'actioncolumn';
        }
    }
});

/**
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.plugin.RowExpander
 */
var rowExpander = new Ext.grid.plugin.RowExpander({
    rowBodyTpl : new Ext.XTemplate('{html}')
});


/**
 * @type {Ext.data.Store}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.Store
 */
var datasett = Ext.create('Ext.data.Store', {
    model: 'leiebasen.Leieforhold',
    pageSize: 300,
    remoteSort: true,
    proxy: {
        type: 'ajax',
        simpleSortMode: true,
        url: "/drift/index.php?oppslag=leieforhold_liste&oppdrag=hent_data",
        timeout: 300000, // 5 mins
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRows'
        },
        listeners: {
            exception: function(proxy, response) {
                let responsStreng = response.responseText;
                let tilbakemelding = Ext.JSON.decode(responsStreng, true);
                if (response.status !== 200 || !tilbakemelding) {
                    Ext.MessageBox.alert('Ugyldig respons', responsStreng);
                }
                else if (!tilbakemelding.success && tilbakemelding.msg) {
                    Ext.MessageBox.alert('Hm', tilbakemelding.msg);
                }
            }
        }
    },
    sorters: [{
        property: 'kontrakt_id',
        direction: 'DESC'
    }]
});

/**
 * @type {Ext.form.field.Text}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Text
 */
var søkefelt = Ext.create('Ext.form.field.Text', {
    emptyText: 'Søk og klikk ↵',
    name: 'søkefelt',
    width: 200,
    listeners: {
        specialkey: function() {
            leiebasen.drift.leieforholdListe.lastRutenett();
        }
    }
});

/**
 * @type {Ext.form.field.Text}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Checkbox
 */
var nåværendeFilter = Ext.create('Ext.form.field.Checkbox', {
    name: 'nåværendeFilter',
    boxLabel: 'Utelat avsluttede leieforhold',
    checked: true,
    listeners: {
        change: function() {
            leiebasen.drift.leieforholdListe.lastRutenett();
        }
    }
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var leieforholdKolonne = Ext.create('Ext.grid.column.Column', {
    dataIndex: 'leieforhold_id',
    header: 'Leieforhold',
    align: 'right',
    renderer: function(value){
        return '<a title="Klikk for å se siste leieavtale i dette leieforholdet." href="/drift/index.php?oppslag=leieforholdkort&id=' + value + '">' + value + '</a>';
    },
    sortable: true,
    width: 40
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var kontraktKolonne = Ext.create('Ext.grid.column.Column', {
    dataIndex: 'kontrakt_id',
    header: 'Kontrakt',
    hidden: true,
    align: 'right',
    sortable: true,
    width: 40
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var kidKolonne = Ext.create('Ext.grid.column.Column', {
    dataIndex: 'kid',
    header: 'kid',
    renderer: function(value){
        return '<div title="Fast KIDnummer for innbetalinger til dette leieforholdet.">' + value + '</div>';
    },
    sortable: false,
    width: 90
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var leiebeløpKolonne = Ext.create('Ext.grid.column.Column', {
    align: 'right',
    dataIndex: 'leiebeløp',
    header: 'Leiebeløp',
    renderer: Ext.util.Format.noMoney,
    sortable: true,
    width: 70
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var avsluttetKolonne = Ext.create('Ext.grid.column.Column', {
    dataIndex: 'avsluttet',
    header: 'Avsluttet',
    renderer: function(value){
        if(value){
            return '<div title="Siste bo-dag.">' + Ext.util.Format.date(value, 'd.m.Y') + '</div>';
        }
    },
    sortable: true,
    width: 70
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var frossetKolonne = Ext.create('Ext.grid.column.Column', {
    dataIndex: 'frosset',
    header: 'Frosset',
    renderer: function(value){
        if(value){
            return "<div title=\"Leieavtalen er frosset, og ignoreres av leiebasen\">" + Ext.util.Format.hake(value) + "</div> ";
        }
    },
    sortable: true,
    width: 30
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var signertFilKolonne = Ext.create('Ext.grid.column.Column', {
    dataIndex: 'signert_fil',
    header: 'Signert fil',
    renderer: function(value){
        if(value){
            return "<div title=\"Signert versjon av leieavtalen er lagret.\">" + Ext.util.Format.hake(value) + "</div> ";
        }
    },
    sortable: true,
    width: 30
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var tilDatoKolonne = Ext.create('Ext.grid.column.Column', {
    dataIndex: 'til_dato',
    header: 'Utløper',
    renderer: function(value, metaData, record){
        return '<span ' + (record.data.avsluttet ? 'style="text-decoration: line-through;" title="Utløpsdatoen er overstyrt av at leieforholdet er avsluttet"' : 'title="Datoen da leieforholdet utløper og må fornyes eller avsluttes"') + '>' + Ext.util.Format.date(value, 'd.m.Y') + '</span>';
    },
    sortable: true,
    width: 70
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var leieobjektKolonne = Ext.create('Ext.grid.column.Column', {
    dataIndex: 'leieobjekt_id',
    header: 'Leil',
    align: 'right',
    renderer: function(value){
        return '<a title="Klikk for å gå til leieobjektkortet." href="/drift/index.php?oppslag=leieobjekt_kort&id=' + value + '">' + value + '</a>';
    },
    sortable: true,
    width: 40
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var beskrivelseKolonne = Ext.create('Ext.grid.column.Column', {
    dataIndex: 'leieforhold_beskrivelse',
    header: 'Beskrivelse',
    renderer: function(value, metaData, record){
        return '<a ' + (record.data.avsluttet ? 'style="text-decoration: line-through;" ' : '') + 'title="Klikk for å se siste leieavtale i dette leieforholdet." href="/drift/index.php?oppslag=leieforholdkort&id=' + record.data.leieforhold_id + '">' + value + '</a>';
    },
    sortable: false,
    flex: 1,
    width: 50
});

/**
 * @type {Ext.toolbar.Paging}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.toolbar.Paging
 */
var sidevelger = Ext.create('Ext.toolbar.Paging', {
    xtype: 'pagingtoolbar',
    store: datasett,
    dock: 'bottom',
    displayInfo: true
});

/**
 * @type {Ext.grid.Panel}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.Panel
 */
var rutenett = Ext.create('Ext.grid.Panel', {
    autoScroll: true,
    layout: 'border',
    frame: false,
    store: datasett,
    title: '',
    tbar: [
        søkefelt, nåværendeFilter
    ],
    columns: [
        leieforholdKolonne,
        beskrivelseKolonne,
        leieobjektKolonne,
        leiebeløpKolonne,
        kidKolonne,
        tilDatoKolonne,
        avsluttetKolonne,
        frossetKolonne,
        signertFilKolonne
    ],
    autoExpandColumn: 2,
    renderTo: 'panel',
    height: '100%',
    stripeRows: true,
    dockedItems: [sidevelger],
    plugins: [rowExpander]
});

datasett.on({
    load: function(store, records, successful) {
        var jsonData = store.getProxy().getReader().jsonData;
        if (typeof jsonData == 'undefined') {
            Ext.MessageBox.alert('Problem:', 'Klarte ikke laste denne tabellen av ukjent grunn.');
            return false;
        }
        if (!successful) {
            var msg = store.getProxy().getReader().jsonData.msg;
            msg = msg.replace(/\r\n|\r|\n/g, '<br>');
            Ext.MessageBox.alert('Klarte ikke laste data:', msg);
        }
    }
});

leiebasen.drift.leieforholdListe.lastRutenett();