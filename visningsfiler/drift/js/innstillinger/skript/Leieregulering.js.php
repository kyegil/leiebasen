<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\innstillinger\skript\Leieregulering $this
 * @var string $skript
 */
?>

leiebasen.drift.innstillinger.leieregulering = leiebasen.drift.innstillinger.leieregulering || {
    init: function () {
        this.felter = {
            /**
             * @type {jQuery}
             */
            leiejusteringGjeldendeIndeks: $('*[name="leiejustering_gjeldende_indeks"]'),
            /**
             * @type {jQuery}
             */
            leiejusteringAutoKpi: $('*[type="checkbox"][name="leiejustering_auto_kpi"]')
        };
        this.felter.leiejusteringAutoKpi.on('click', function () {
            if(!$(this).is(':checked')) {
                leiebasen.drift.innstillinger.leieregulering.hentKpi();
            }
            leiebasen.drift.innstillinger.leieregulering.oppdaterFelter();
        });
        this.oppdaterFelter();
    },
    oppdaterFelter: function() {
        let leiejusteringAutoKpi = this.felter.leiejusteringAutoKpi.is(':checked');
        this.felter.leiejusteringGjeldendeIndeks.parent().toggle( !leiejusteringAutoKpi );
    }
};


leiebasen.drift.innstillinger.leieregulering.hentKpi = function() {
    $.ajax({
        url: '/drift/index.php?oppslag=innstillinger&oppdrag=hent_data&data=kpi',
        method: "GET",
        dataType: 'json',
        success: function(data) {
            if (data.msg) {
                alert(data.msg);
            }
            if(data.success) {
                alert('Gjeldende KPI er ' + data.data);
            }
        }
    });
}

leiebasen.drift.innstillinger.leieregulering.init();
