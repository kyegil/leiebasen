<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\eksport\Index $this
 * @var string $eksportFormat
 */
?>
leiebasen.drift.eksport = leiebasen.drift.eksport || {
    init: function() {
        this.oppdaterEksportTyper();
        this.oppdaterTidsromFelter();
    },
    oppdaterEksportTyper: function() {
        let valgtType = $('*[name="type"]:checked').val();
        $('select[name*="profil"]').parent().hide();
        if(valgtType === 'tilpasset') {
            $('select[name="profil[tilpasset]"]').parent().show();
        }
        else {
            $('select[name="profil[standard]"]').parent().show();
        }
        this.oppdaterEksportformat();
    },
    oppdaterEksportformat: function() {
        let valgtProfil = $('select[name*="profil"]:visible').find(':selected').val();
        if (typeof valgtProfil == 'undefined') {
            return;
        }
        if (typeof this.innstillinger[valgtProfil] == 'undefined') {
            return;
        }
        if (this.innstillinger[valgtProfil].csvSkilletegn) {
            $('select[name="skilletegn"]').val(this.innstillinger[valgtProfil].csvSkilletegn);
        }
        if (this.innstillinger[valgtProfil].datoformat) {
            $('select[name="datoformat"]').val(this.innstillinger[valgtProfil].datoformat);
        }
        if (this.innstillinger[valgtProfil].innpakning) {
            $('select[name="innpakning"]').val(this.innstillinger[valgtProfil].innpakning);
        }
    },
    oppdaterTidsromFelter: function() {
        let valgtProfil = $('select[name*="profil"]:visible').find(':selected').val();
        let valgtPeriode = $('select[name="periode"]').find(':selected').val();
        let fraDatoFelt = $('input[name="fradato"]');
        let tilDatoFelt = $('input[name="tildato"]');
        if(valgtPeriode) {
            let sisteDag = new Date(valgtPeriode.split('-')[0], valgtPeriode.split('-')[1], 0).getDate();
            fraDatoFelt.val(valgtPeriode + '-01');
            tilDatoFelt.val(valgtPeriode + '-' + sisteDag);
            fraDatoFelt.parent().hide();
            tilDatoFelt.parent().hide();
        }
        else {
            fraDatoFelt.parent().show();
            tilDatoFelt.parent().show();
        }

        if(valgtProfil === 'utestående') {
            fraDatoFelt.parent().hide();
            tilDatoFelt.parent().find('label').html('Utestående per');
            tilDatoFelt.parent().show();
            $('select[name="periode"]').parent().hide();
        }
        else {
            $('select[name="periode"]').parent().show();
            tilDatoFelt.parent().find('label').html('Til dato');
        }
    }
};

leiebasen.drift.eksport.innstillinger = <?php echo $eksportFormat;?>;

$('*[name="type"]').on('change', function () {
    leiebasen.drift.eksport.oppdaterEksportTyper();
    leiebasen.drift.eksport.oppdaterTidsromFelter();
});
$('select[name*="profil"]:visible').on('change', function () {
    leiebasen.drift.eksport.oppdaterEksportformat();
    leiebasen.drift.eksport.oppdaterTidsromFelter();
});
$('*[name="periode"]').on('change', function () {
    leiebasen.drift.eksport.oppdaterTidsromFelter();
});
$('input[name="fradato"]').on('change', function () {
    leiebasen.drift.eksport.oppdaterTidsromFelter();
});
$('input[name="tildato"]').on('change', function () {
    leiebasen.drift.eksport.oppdaterTidsromFelter();
});
leiebasen.drift.eksport.init();
