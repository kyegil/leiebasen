<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\kontrakt_tekst_skjema\Index $this
 * @var int $kontraktId
 * @var int $leieforholdId
 * @var string $avtalemaler
 * @var string $skript
 */
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
?>

leiebasen.drift.kontraktTekstSkjema = leiebasen.drift.kontraktTekstSkjema || {
    felter: {},
    /**
     * @type {Object}
     */
    avtalemaler: <?php echo $avtalemaler;?>,
    initier: function() {
        if(typeof this._initiert !== 'undefined') {
            return this;
        }

        this.felter = {
            malVelger: $("#mal-velger"),
            avtaleRedigererFelt: $("#avtale-redigerer")
        };

        this.felter.malVelger.on('change', function() {
            leiebasen.drift.kontraktTekstSkjema.oppdaterMaltekst();
        });

        $(".forhåndsvis").on('click', function() {
            leiebasen.drift.kontraktTekstSkjema.forhåndsvis();
        });

        $(".utskrift").on('click', function() {
            window.open('<?php echo DriftKontroller::url('kontrakt_tekst_kort', $kontraktId, 'utlevering', ['data' => 'utskrift']);?>');
        });

        this.oppdaterMaltekst();

        leiebasen.ckEditor['avtale-redigerer'].model.document.on('change:data', function() {
            leiebasen.drift.kontraktTekstSkjema.oppdaterSkjema();
        });

        this._initiert = true;
    },
    oppdaterMaltekst: function() {
        let malId = this.felter.malVelger.val();
        let mal = leiebasen.drift.kontraktTekstSkjema.avtalemaler[malId];
        leiebasen.ckEditor['avtale-redigerer'].setData(mal);
        leiebasen.drift.kontraktTekstSkjema.avtalemaler[malId] = leiebasen.ckEditor['avtale-redigerer'].getData();
        this.oppdaterSkjema();
    },
    oppdaterSkjema: function() {
        let dirty = leiebasen.ckEditor['avtale-redigerer'].getData() !==  this.avtalemaler['0'];
        $("button[type='submit']").prop('disabled', !dirty);
        $(".btn.utskrift").toggleClass('disabled', dirty);
        $(".btn.fortsett").toggleClass('disabled', dirty);
    },
    forhåndsvis: function() {
        const mal = leiebasen.ckEditor['avtale-redigerer'].getData();
        $(".overlay").addClass('laster');

        $.ajax({
            url: "<?php echo DriftKontroller::url('kontrakt_tekst_skjema', $kontraktId, 'utlevering', ['data' => 'forhåndsvisning']);?>",
            method: 'POST',
            data: {
                mal: mal
            },
            dataType: 'json',
            success: function (data) {
                $(".overlay").removeClass('laster');
                $("#forhåndsvisning .modal-body").html(data.data)
                $("#forhåndsvisning").modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus ? textStatus : errorThrown);
            }
        });
    }
};

leiebasen.kjerne.ajaxFormCallBack = function(form, resultat) {
    if(resultat.success) {
        leiebasen.ckEditor['avtale-redigerer'].setData(resultat.data);
        leiebasen.drift.kontraktTekstSkjema.avtalemaler['0'] = leiebasen.ckEditor['avtale-redigerer'].getData();
        leiebasen.drift.kontraktTekstSkjema.felter.malVelger.val('0');
        leiebasen.drift.kontraktTekstSkjema.oppdaterSkjema();
    }
    return true;
}

<?php echo $skript;?>

$('document').ready(function(){
    leiebasen.drift.kontraktTekstSkjema.initier();
});