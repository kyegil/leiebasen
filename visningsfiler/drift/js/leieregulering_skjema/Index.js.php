<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\leieregulering_skjema\Index $this
 * @var string $leieforholdId
 * @var string $skript
 * @var string $eksisterendeBasisLeie
 * @var string $antallTerminer
 * @var string $varselType
 */
?>
<?php echo $skript;?>
/**
 * @type {Object}
 */
leiebasen.drift.leiereguleringSkjema = leiebasen.drift.leiereguleringSkjema || {};
leiebasen.drift.leiereguleringSkjema.leieforholdId = Number(<?php echo json_encode($leieforholdId);?>);
leiebasen.drift.leiereguleringSkjema.varselType = <?php echo json_encode($varselType);?>;
leiebasen.drift.leiereguleringSkjema.antallTerminer = Number(<?php echo json_encode($antallTerminer);?>);
leiebasen.drift.leiereguleringSkjema.initier = function() {
    this.verdier = {
        eksisterendeBasisLeie: Number(<?php echo json_encode($eksisterendeBasisLeie);?>),
        basisleie: null,
        bruttoleie: null,
        terminleie: null
    };
    this.felter = {
        justeringsDatoFelt: $("[name='justeringsdato']"),
        basisLeiebeløpFelt: $("[name='basis_leiebeløp']"),
        delkravFelter: $("[name^='delkrav[']"),
        justeringsvarselFelt: $("[name='justeringsvarsel']")
    };
    this.leieOppbyggingTabell = $("#leieOppbyggingTabell");
    this.justeringsvarselRedigerer = leiebasen.ckEditor['justeringsvarsel'];
    this.utskriftsKnapp = $("#utskriftsknapp");
    this.submitKnapp = $("button[type='submit']");

    this.oppdaterLeieOppbyggingVerdier();

    this.felter.justeringsDatoFelt.change(function(){
        leiebasen.drift.leiereguleringSkjema.hentNyttJusteringsforslag();
    });

    this.felter.basisLeiebeløpFelt.keyup(function(){
        leiebasen.drift.leiereguleringSkjema.oppdaterLeieOppbyggingVerdier();
    });
    this.felter.basisLeiebeløpFelt.change(function(){
        leiebasen.drift.leiereguleringSkjema.oppdaterLeieOppbyggingVerdier();
    });

    this.felter.delkravFelter.keyup(function(){
        leiebasen.drift.leiereguleringSkjema.oppdaterLeieOppbyggingVerdier();
    });
    this.felter.delkravFelter.change(function(){
        leiebasen.drift.leiereguleringSkjema.oppdaterLeieOppbyggingVerdier();
    });

    if (this.varselType === 'brev') {
        this.submitKnapp.prop('disabled', true);
    }
    this.oppdaterLeieOppbyggingTabell();
};

leiebasen.drift.leiereguleringSkjema.hentNyttJusteringsforslag = function() {
    this.utskriftsKnapp.addClass('disabled');
    this.submitKnapp.prop('disabled', true);
    let params = {
        oppslag: 'leieregulering_skjema',
        oppdrag: 'hent_data',
        data: 'reguleringsforslag',
        leieforhold: this.leieforholdId,
        justeringsdato: this.felter.justeringsDatoFelt.val(),
        delkrav: {}
    }
    this.felter.delkravFelter.each(function(){
        let feltnavn = this.getAttribute('name');
        let kode = this.getAttribute('name').substring(8, feltnavn.length - 1)
        params.delkrav[kode] = $(this).val();
    });
    jQuery.ajax({
        url: 'index.php',
        method: "GET",
        contentType: 'application/json',
        dataType: 'json',
        data: params,
        success: function(data) {
            leiebasen.drift.leiereguleringSkjema.felter.basisLeiebeløpFelt.val(data.data.leieelementer.basisleie);
            leiebasen.drift.leiereguleringSkjema.oppdaterLeieOppbyggingVerdier();
            leiebasen.drift.leiereguleringSkjema.utskriftsKnapp.removeClass('disabled');
            if (leiebasen.drift.leiereguleringSkjema.varselType !== 'brev') {
                leiebasen.drift.leiereguleringSkjema.submitKnapp.prop('disabled', false);
            }

            if (data.msg) {
                alert(data.msg);
            }
            if(data.url) {
                window.location.href = data.url;
            }
        },
        /**
         * @param {jqXHR} jqXHR
         * @param {String} textStatus
         * @param {String} errorThrown
         */
        error: function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}

leiebasen.drift.leiereguleringSkjema.oppdaterLeieOppbyggingVerdier = function() {
    this.leieOppbyggingTabell.find('tr').each(function() {
        let verdi;
        let kode = $(this).attr('data-leie-element');
        let verdiCelle = $(this).find('td.beløp');

        if($(verdiCelle).find('input').length) {
            verdi = $(verdiCelle).find('input').val();
        }
        else {
            verdi = verdiCelle.html();
        }
        if (kode) {
            leiebasen.drift.leiereguleringSkjema.verdier[kode] = Number(verdi);
        }
    });
    this.verdier.basisleie = Number(this.felter.basisLeiebeløpFelt.val());
    this.oppdaterLeieOppbyggingTabell();
}

leiebasen.drift.leiereguleringSkjema.oppdaterLeieOppbyggingTabell = function() {
    let leie = this.verdier.basisleie;
    this.leieOppbyggingTabell.find('tr').each(function() {
        let sats;
        let beløp;
        let kode = $(this).attr('data-leie-element');
        let verdiCelle = $(this).find('td.beløp');
        let relativ = $(this).hasClass('relativ');

        if(kode && ['basisleie', 'bruttoleie', 'terminleie'].indexOf(kode) === -1) {
            sats = leiebasen.drift.leiereguleringSkjema.verdier[kode];
            if (!isNaN(sats)) {
                beløp = relativ ? (sats / 100 * leie) : sats;
                if($(verdiCelle).find('input').length) {
                    $(verdiCelle).find('input').val(sats);
                }
                else {
                    verdiCelle.html(leiebasen.kjerne.kr(beløp));
                }
                leie += beløp;
            }
        }
    });
    this.verdier.bruttoleie = Math.round(leie);
    this.verdier.terminleie = Math.round(leie / this.antallTerminer);

    $("tr[data-leie-element='basisleie'] td.beløp" ).html(leiebasen.kjerne.kr(this.verdier.basisleie));
    $("tr[data-leie-element='bruttoleie'] td.beløp" ).html(leiebasen.kjerne.kr(this.verdier.bruttoleie));
    $("tr[data-leie-element='terminleie'] td.beløp" ).html(leiebasen.kjerne.kr(this.verdier.terminleie));
    this.oppdaterVarselVariabler();
}

leiebasen.drift.leiereguleringSkjema.oppdaterVarselVariabler = function() {
    /**
     * @type {string}
     */
    let varsel = this.justeringsvarselRedigerer.getData();
    let justeringsdatoFormatert = this.felter.justeringsDatoFelt.val().split('-').reverse().join('.');
    let justering = this.verdier.basisleie / this.verdier.eksisterendeBasisLeie - 1;
    let jQueryVarsel = jQuery('<div>' + varsel + '</div>');

    jQueryVarsel.find("span[data-mal-variabel='{virkning fra dato}']").html(justeringsdatoFormatert);
    jQueryVarsel.find("span[data-mal-variabel='{ny nettoleie}']").html(leiebasen.kjerne.kr(this.verdier.basisleie));
    jQueryVarsel.find("span[data-mal-variabel='{ny bruttoleie}']").html(leiebasen.kjerne.kr(this.verdier.terminleie));
    jQueryVarsel.find("span[data-mal-variabel='{justering}']").html(leiebasen.kjerne.prosent(justering));

    this.justeringsvarselRedigerer.setData(jQueryVarsel.html());
}

leiebasen.drift.leiereguleringSkjema.skrivUt = function() {
    this.submitKnapp.prop('disabled', true);
    let varsel = this.justeringsvarselRedigerer.getData();
    window.open('/drift/index.php?oppslag=leieregulering_skjema&oppdrag=hent_data&data=varselutskrift&id=<?php echo $leieforholdId;?>&innhold=' + encodeURIComponent(varsel), '_blank');
    this.submitKnapp.prop('disabled', false);
    if (this.varselType === 'brev') {
        window.alert('Varselet åpnes i eget vindu, og må leveres til leietaker.\nDu kan nå gjennomføre denne leiereguleringen.');
    }
}

$('document').ready(function(){
    leiebasen.drift.leiereguleringSkjema.initier();
});
