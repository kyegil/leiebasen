<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\leieforhold_brukerprofiler_skjema\Index $this
 * @var int $leieforholdId
 * @var string $skript
 */
?>

leiebasen.drift.leieforholdBrukerprofilerSkjema = leiebasen.drift.leieforholdBrukerprofilerSkjema || {
    initier: function() {
        if(typeof this._initiert !== 'undefined') {
            return this;
        }

        $("[type='checkbox'][name^='personer'][name$='[sett_passord]']").on('change', function() {
            if($(this).is(':checked')) {
                let $profil = $(this).closest('.collapsible-section');
                leiebasen.drift.leieforholdBrukerprofilerSkjema.oppdaterProfilFelter($profil);
            }
        });
        $("[name^='personer'][name$='[pw1]']").on('change', function() {
            let $profil = $(this).closest('.collapsible-section');
            leiebasen.drift.leieforholdBrukerprofilerSkjema.oppdaterProfilFelter($profil);
        });

        this._initiert = true;
    },

    /**
     * @param {jQuery} $profil
     */
    oppdaterProfilFelter: function($profil) {
        const $pw1 = $profil.find("[name^='personer'][name$='[pw1]']");
        const $pw2 = $profil.find("[name^='personer'][name$='[pw2]']");
        const pw1 = $pw1.val();
        const pw1Escaped = pw1.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
        $pw2.attr('pattern', pw1Escaped);
    }
};

<?php echo $skript;?>

leiebasen.drift.leieforholdBrukerprofilerSkjema.initier();
