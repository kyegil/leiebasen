<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var string $initLeiebasenJsObjektEgenskaper
 * @var Visning|string $meny
 * @var string $tilbakeUrl
 */
use Kyegil\Leiebasen\Visning;use Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_tjenesteliste\ExtOnReady;
?>

/**
 * @name leiebasen
 * @type {Object}
 */
<?php echo $initLeiebasenJsObjektEgenskaper;?>

leiebasen.lastRutenett = function() {
    datasett.getProxy().extraParams = {
        søkefelt: søkefelt.getValue()
    };
    sidevelger.moveFirst();
    datasett.load({
        params: {
            start: 0,
            limit: 300
        }
    });
}

/**
 * @param {Ext.data.Model} record
 */
leiebasen.slettOppslag = function(record){
    var tjenesteId = record.get('id');
    Ext.Msg.confirm({
        title: 'Slett tjenesten',
        msg: 'Er du sikker på du vil slette denne tjenesten?',
        buttons: Ext.Msg.OKCANCEL,
        fn: function(buttonId) {
            if (buttonId === 'ok') {
                Ext.Ajax.request({
                    params: {
                        'tjeneste_id': tjenesteId
                    },
                    waitMsg: 'Vent...',
                    url: "/drift/index.php?oppslag=delte_kostnader_tjeneste_skjema&oppdrag=oppgave&oppgave=slett_tjeneste",

                    /**
                     * @param {XMLHttpRequest} response
                     */
                    failure: function(response) {
                        Ext.MessageBox.alert('Mislyktes', 'Respons-status ' + response.status + ' fra tjeneren... ', function() {
                            datasett.load();
                        });
                    },

                    /**
                     * @param {XMLHttpRequest} response
                     */
                    success : function(response) {
                        if(!response.responseText) {
                            Ext.MessageBox.alert('Problem', 'Null respons fra tjeneren...', function() {
                                datasett.load();
                            });
                        }
                        var jsonRespons = Ext.JSON.decode(response.responseText, true);
                        if(!jsonRespons) {
                            Ext.MessageBox.alert('Problem', response.responseText, function() {
                                datasett.load();
                            });
                        }
                        else if (jsonRespons.success) {
                            Ext.MessageBox.alert('Slettet', jsonRespons.msg, function() {
                                datasett.load();
                            });
                        }
                        else {
                            Ext.MessageBox.alert('Hmm..', jsonRespons.msg, function() {
                                datasett.load();
                            });
                        }
                    }
                });
            }
        },
        icon: Ext.window.MessageBox.QUESTION
    });
}

/**
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext-method-define
 */
Ext.define('leiebasen.Tjeneste', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [ // {@link http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.Field}
        {name: 'id', type: 'int'},
        {name: 'navn', type: 'string'},
        {name: 'avtalereferanse', type: 'string'},
        {name: 'beskrivelse', type: 'string'}
    ]
});

/**
 * @type {Ext.grid.plugin.CellEditing}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.plugin.CellEditing
 */
var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
    clicksToEdit: 1
});

/**
 * @type {Ext.grid.plugin.RowEditing}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.plugin.RowEditing
 */
var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
    autoCancel: false,
    listeners: {
        beforeedit: function (grid, e) {
            return e.column.xtype !== 'actioncolumn';
        }
    }
});

/**
 * @type {Ext.data.Store}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.Store
 */
var datasett = Ext.create('Ext.data.Store', {
    model: 'leiebasen.Tjeneste',
    pageSize: 300,
    remoteSort: true,
    proxy: {
        type: 'ajax',
        simpleSortMode: true,
        url: "/drift/index.php?oppslag=delte_kostnader_tjenesteliste&oppdrag=hent_data",
        timeout: 300000, // 5 mins
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRows'
        },
        listeners: {
            exception: function(proxy, response) {
                let responsStreng = response.responseText;
                let tilbakemelding = Ext.JSON.decode(responsStreng, true);
                if (response.status !== 200 || !tilbakemelding) {
                    Ext.MessageBox.alert('Ugyldig respons', responsStreng);
                }
                else if (!tilbakemelding.success && tilbakemelding.msg) {
                    Ext.MessageBox.alert('Hm', tilbakemelding.msg);
                }
            }
        }
    },
    sorters: [{
        property: 'id',
        direction: 'DESC'
    }]
});

/**
 * @type {Ext.form.field.Text}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Text
 */
var søkefelt = Ext.create('Ext.form.field.Text', {
    emptyText: 'Søk og klikk ↵',
    name: 'søkefelt',
    width: 200,
    listeners: {
        specialkey: function() {
            datasett.getProxy().extraParams = {
                søkefelt: søkefelt.getValue()
            };
            leiebasen.lastRutenett();
        }
    }
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var id = Ext.create('Ext.grid.column.Column', {
    align:		'right',
    dataIndex:	'id',
    text:		'Id',
    sortable:	true,
    hidden:		true,
    width:		40
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var navn = Ext.create('Ext.grid.column.Column', {
    dataIndex:	'navn',
    text:		'Navn',
    sortable:	true,
    width:		100,
    flex:		1
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var avtalereferanse = Ext.create('Ext.grid.column.Column', {
    dataIndex:	'avtalereferanse',
    text:		'Avtalereferanse',
    sortable:	true,
    width:		100
});

/**
 * @type {Ext.grid.column.Action}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Action
 */
var vis  = Ext.create('Ext.grid.column.Action', {
    header:		'Detaljer',
    dataIndex: 'id',
    align: 'center',
    icon: '/pub/media/bilder/drift/detaljer.png',
    tooltip: 'Inspiser eller endre',
    handler: function(grid, rowIndex, colIndex, item, e, record) {
        window.location = '/drift/index.php?oppslag=delte_kostnader_tjeneste_kort&id=' + record.get('id');
    },
    sortable: false,
    width: 50
});

/**
 * @type {Ext.grid.column.Action}
 */
var slett  = Ext.create('Ext.grid.column.Action', {
    header:		'Slett',
    dataIndex: 'id',
    align: 'center',
    icon: '/pub/media/bilder/drift/slett.png',
    tooltip: 'Slett',
    handler: function(grid, rowIndex, colIndex, item, e, record) {
        leiebasen.slettOppslag(record);
    },
    sortable: false,
    width: 50
});

/**
 * @type {Ext.toolbar.Paging}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.toolbar.Paging
 */
var sidevelger = Ext.create('Ext.toolbar.Paging', {
    xtype: 'pagingtoolbar',
    store: datasett,
    dock: 'bottom',
    displayInfo: true
});

/**
 * @type {Ext.grid.Panel}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.Panel
 */
var rutenett = Ext.create('Ext.grid.Panel', {
    autoScroll: true,
    layout: 'border',
    frame: false,
    store: datasett,
    title: 'Oversikt over tjenester',
    tbar: [
        søkefelt
    ],
    columns: [
        id,
        navn,
        avtalereferanse,
        vis
    ],
    renderTo: 'panel',
    height: '100%',

    dockedItems: [sidevelger],

    buttons: [{
        text: 'Tilbake',
        handler: function() {
            window.location = '<?php echo $tilbakeUrl;?>';
        }
    }, {
        text: 'Skriv ut',
        handler: function() {
            window.open ("/drift/index.php?oppslag=delte_kostnader_tjenesteliste&oppdrag=utskrift");
        }
    }, {
        text: 'Registrer ny tjeneste',
        handler: function() {
            window.location = "/drift/index.php?oppslag=delte_kostnader_tjeneste_skjema&id=*";
        }
    }]
});

rutenett.getStore().on({
    load: function(store, records, successful) {
        var jsonData = store.getProxy().getReader().jsonData;
        if (typeof jsonData == 'undefined') {
            Ext.MessageBox.alert('Problem:', 'Klarte ikke laste denne tabellen av ukjent grunn.');
            return false;
        }
        if (!successful) {
            var msg = store.getProxy().getReader().jsonData.msg;
            msg = msg.replace(/\r\n|\r|\n/g, '<br>');
            Ext.MessageBox.alert('Klarte ikke laste data:', msg);
        }
    }
});

datasett.getProxy().extraParams = {
    søkefelt: søkefelt.getValue()
};

leiebasen.lastRutenett();