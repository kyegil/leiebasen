<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\personadresse_skjema\ExtOnReady $this
 * @var \Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny|string $meny
 * @var string $initLeiebasenJsObjektEgenskaper
 * @var \Kyegil\Leiebasen\Visning|string $skjema
 * @var string $formActionUrl
 */
?>

/**
 * @name leiebasen
 * @type {Object}
 */
<?php echo $initLeiebasenJsObjektEgenskaper;?>

/**
 * @type {Ext.form.Panel}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.Panel
 */
var skjema = <?php echo $skjema;?>;