<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var int $tjenesteId
 * @var Meny|string $meny
 * @var string $tilbakeUrl
 * @var string $tjenesteId
 * @var string $tjenesteBeskrivelse
 * @var string $tab
 * @var string $forbruksdiagramAkseFelter
 * @var string $forbruksdiagramModellFelter
 * @var string|\Kyegil\Leiebasen\Visning $forbruksdiagramStore
 */
use Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_tjeneste_kort\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>

/**
 * @name leiebasen
 * @type {Object}
 * @name leiebasen.extJs
 * @type {Object}
 */
leiebasen.drift.delteKostnaderTjenesteKort = {
    oppdaterDiagram: function() {
        let fraDato = fradatoFelt.getValue();
        let tilDato = tildatoFelt.getValue();
        if (fraDato instanceof Date && tilDato instanceof Date) {
            forbruksdiagram.axes.getAt(1).fromDate = fraDato;
            forbruksdiagram.axes.getAt(1).toDate = tilDato;
            forbruksdiagramdata.load({
                params: {
                    fra: Ext.Date.format(fraDato, 'Y-m-d'),
                    til: Ext.Date.format(tilDato, 'Y-m-d')
                }
            });
        }
    }
};

Ext.define('leiebasen.Faktura', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [ // http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.Field
        {name: 'id'},
        {name: 'fakturanummer'},
        {name: 'avtalereferanse'},
        {name: 'tjeneste_id'},
        {name: 'anlegg'},
        {name: 'fradato',			type: 'date', dateFormat: 'Y-m-d', useNull: true},
        {name: 'tildato',			type: 'date', dateFormat: 'Y-m-d', useNull: true},
        {name: 'termin'},
        {name: 'beløp',			type: 'float', useNull: true},
        {name: 'beløp_formatert'},
        {name: 'fordelt',	type: 'boolean'},
        {name: 'forbruk',	type: 'float', useNull: true},
        {name: 'forbruk_formatert'},
        {name: 'daglig_forbruk',	type: 'float', useNull: true},
        {name: 'daglig_forbruk_formatert'},
        {name: 'lagt_inn_av'},
        {name: 'html'}
    ]
});

Ext.define('leiebasen.ForbruksdiagramModell', {
    extend: 'Ext.data.Model',
    fields: <?php echo $forbruksdiagramModellFelter;?>
});

/**
 * @type {Ext.data.Store}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.Store
 */
var fakturasett = Ext.create('Ext.data.Store', {
    model: 'leiebasen.Faktura',

    pageSize: 100,
    buffered: true,
    leadingBufferZone: 100,

    proxy: {
        type: 'ajax',
        simpleSortMode: true,
        url: "/drift/index.php?oppslag=delte_kostnader_tjeneste_kort&id=<?php echo $tjenesteId;?>&oppdrag=hent_data&data=regninger",
        reader: {
            type: 'json',
            root: 'data',
            actionMethods: { // Feil plassering? actionMethods tilhører proxy..
                read: 'POST'
            },
            totalProperty: 'totalRows'
        },
        listeners: {
            exception: function(proxy, response) {
                let responsStreng = response.responseText;
                let tilbakemelding = Ext.JSON.decode(responsStreng, true);
                if (response.status !== 200 || !tilbakemelding) {
                    Ext.MessageBox.alert('Ugyldig respons', responsStreng);
                }
                else if (!tilbakemelding.success && tilbakemelding.msg) {
                    Ext.MessageBox.alert('Hm', tilbakemelding.msg);
                }
            }
        }
    },

    remoteSort: true,
    sorters: [{
        property: 'fradato',
        direction: 'DESC'
    }]
});

/**
 * @type {Ext.data.JsonStore}
 */
var forbruksdiagramdata = <?php echo $forbruksdiagramStore;?>

/**
 * @type {Ext.grid.Panel}
 */
var fakturaPanel = Ext.create('Ext.grid.Panel', {
    itemId: 'fakturaer',
    store: fakturasett,
    title: 'Regninger',
    autoScroll: true,
    frame: false,
    features: [],

    plugins: [{
        ptype: 'rowexpander',
        rowBodyTpl : ['{html}']
    }, {
        ptype: 'bufferedrenderer',
        trailingBufferZone: 100,
        leadingBufferZone: 100
    }],

    tbar: [
        //			søkefelt
    ],
    columns: [{
            align: 'right',
            dataIndex: 'id',
            header: 'Id',
            hidden: true,
            sortable: true,
            width: 50
        },
        {
            dataIndex: 'fakturanummer',
            header: 'Faktura nr',
            flex: 1,
            sortable: true
        },
        {
            dataIndex: 'fradato',
            header: 'Fra dato',
            renderer: Ext.util.Format.dateRenderer('d.m.Y'),
            sortable: true,
            width: 70
        },
        {
            dataIndex: 'tildato',
            header: 'Til dato',
            renderer: Ext.util.Format.dateRenderer('d.m.Y'),
            sortable: true,
            width: 70
        },
        {
            dataIndex: 'termin',
            header: 'Termin',
            flex: 1,
            sortable: true
        },
        {
            dataIndex: 'forbruk',
            header: 'Forbruk',
            renderer: function(value, metadata, record) {
                return record.get('forbruk_formatert');
            },
            sortable: true,
            align: 'right',
            width: 90
        },
        {
            dataIndex: 'daglig_forbruk',
            header: 'per dag',
            renderer: function(value) {
                return value ? ((Math.round(value * 100) / 100) + '&nbsp;kWh') : '';
            },
            sortable: true,
            align: 'right',
            width: 90
        },
        {
            dataIndex: 'beløp',
            header: 'Beløp',
            renderer: function(value, metadata, record) {
                return record.get('beløp_formatert');
            },
            sortable: true,
            align: 'right',
            width: 90
        },
        {
            dataIndex: 'fordelt',
            header: 'Fordelt',
            renderer: function(val) {
                return val ? '✔︎' : '';
            },
            sortable: true,
            width: 50
        },
        {
            dataIndex: 'id',
            header: 'Vis',
            hidden: false,
            sortable: false,
            renderer: function(value, metaData, record) {
                if(!record.data.id) return "";
                return "<a title=\"Vis denne fakturaen\" href=\"/drift/index.php?oppslag=delte_kostnader_kostnad_kort&id="  + record.get('id') + "\"><img alt=\"detaljer\" src=\"/pub/media/bilder/drift/detaljer_lite.png\"></a>";
            },
            width: 30
        }
    ],

    listeners: {
        celldblclick: function(view, td, cellIndex, record) {
            window.location = "/drift/index.php?oppslag=delte_kostnader_kostnad_kort&id=" + record.get('id');
        }
    },

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: fakturasett,
        dock: 'bottom',
        displayInfo: true
    }],

    buttons: []
});
fakturaPanel.on({
    viewready: function() {
        fakturasett.loadPage(1);
    }
});

/**
 * @type {Ext.form.field.Date}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Date
 */
var fradatoFelt = Ext.create('Ext.form.field.Date', {
    allowBlank: true,
    fieldLabel: 'Fra',
    labelAlign: 'right',
    labelWidth: 50,
    format: 'd.m.Y',
    value: '<?=(date('Y')-1) . "-01-01";?>',
    width: 150,
    listeners: {
        change: leiebasen.drift.delteKostnaderTjenesteKort.oppdaterDiagram,
        select: leiebasen.drift.delteKostnaderTjenesteKort.oppdaterDiagram
    }
});

/**
 * @type {Ext.form.field.Date}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Date
 */
var tildatoFelt = Ext.create('Ext.form.field.Date', {
    allowBlank: true,
    fieldLabel: 'Til',
    labelAlign: 'right',
    labelWidth: 50,
    format: 'd.m.Y',
    name: 'fradato',
    value: '<?=date("Y-m-01");?>',
    width: 150,
    listeners: {
        change: leiebasen.drift.delteKostnaderTjenesteKort.oppdaterDiagram,
        select: leiebasen.drift.delteKostnaderTjenesteKort.oppdaterDiagram
    }
});

/**
 * @type {Ext.chart.Chart}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.chart.Chart
 */
var forbruksdiagram = Ext.create('Ext.chart.Chart', {
    id: 'forbruksdiagram',
    style: 'background:#fff',
    animate: true,
    store: forbruksdiagramdata,
    axes: [
        {
            title: 'Daglig forbruk',
            type: 'Numeric',
            grid: {
                odd: {
                    opacity: 1,
                    fill: '#ddd',
                    stroke: '#bbb',
                    'stroke-width': 1
                }
            },
            position: 'left',
            fields: <?php echo $forbruksdiagramAkseFelter;?>,
            minimum: 0,
            maximum: 200
        },
        {
            title: 'Dato',
            type: 'Time',
            grid: false,
            label: {
                rotate: {
                    degrees: 315
                }
            },
            position: 'bottom',
            fields: ['dato'],
            dateFormat: 'd.m.Y',
            constrain: true,
            fromDate: Date.parse('<?=(date('Y')-1) . "-01-01";?>'),
            toDate: Date.parse('<?=date("Y-m-01");?>'),
            step: ['mo', 1]
        }
    ],
    series: [
        {
            type: 'area',
            highlight: true,
            tips: {
                trackMouse: true,
                width: 250,
                height: 35,
                renderer: function(storeItem, item) {
                    var d = Ext.Date.format(new Date(storeItem.get('dato')), 'd. M Y');
                    this.setTitle(d + '<br>Faktura ' + item.storeField + ' - ' + (Math.round(storeItem.get(item.storeField)*10)/10).toLocaleString('nb-NO') + ' per dag.');
                }
            },
            xField: 'dato',
            yField: <?php echo $forbruksdiagramAkseFelter;?>,
            style: {
                opacity: 0.93
            }
        }
    ]
});

/**
 * @type {Ext.panel.Panel}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.panel.Panel
 */
var diagramPanel = Ext.create('Ext.panel.Panel', {
    itemId: 'forbruk',
    title: 'Forbruk',
    layout: 'border',
    items: [
        forbruksdiagram
    ],
    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        items: [fradatoFelt, tildatoFelt]
    }]
});

leiebasen.drift.delteKostnaderTjenesteKort.oppdaterDiagram();

/**
 * @type {Ext.form.Panel}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.Panel
 */
var fordelingsnøkkelPanel = Ext.create('Ext.form.Panel', {
    itemId: 'fordelingsnøkkel',
    title: 'Fordelingsnøkkel',
    autoScroll: true,
    bodyPadding: 5,
    autoLoad: '/drift/index.php?oppslag=delte_kostnader_tjeneste_kort&id=<?php echo $tjenesteId;?>&oppdrag=hent_data&data=fordelingsnøkkel',
    buttons: [{
        text: 'Endre fordeling',
        handler: function () {
            window.location = '/drift/index.php?oppslag=delte_kostnader_fordelingsnøkkel_skjema&id=<?php echo $tjenesteId?>';
        }
    }]
});

/**
 * @type {Ext.panel.Panel}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.panel.Panel
 */
var detaljPanel = Ext.create('Ext.panel.Panel', {
    itemId: 'detaljer',
    title: 'Detaljer',
    autoScroll: true,
    bodyPadding: 5,
    autoLoad: '/drift/index.php?oppslag=delte_kostnader_tjeneste_kort&oppdrag=hent_data&data=detaljer&id=<?php echo $tjenesteId;?>'
});

/**
 * @type {Ext.tab.Panel}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.tab.Panel
 */
var panel = Ext.create('Ext.tab.Panel', {
    title: <?php echo json_encode($tjenesteBeskrivelse);?>,
    autoScroll: true,
    bodyPadding: 5,
    frame: true,
    items: [
        detaljPanel,
        fakturaPanel,
        diagramPanel,
        fordelingsnøkkelPanel
    ],

    activeTab: <?php echo json_encode($tab);?>,

    renderTo: 'panel',
    height: '100%',

    buttons: [{
        scale: 'medium',
        text: 'Skriv ut',
        menu: {
            items: [
                {
                    text: 'Detaljer og fordelingsnøkkel',
                    handler: function(){
                        window.open('/drift/index.php?oppslag=delte_kostnader_tjeneste_kort&oppdrag=utskrift&id=<?php echo $tjenesteId?>');
                    }
                },
                {
                    text: 'Liste over regningene',
                    handler: function(){
                        window.open('/drift/index.php?oppslag=delte_kostnader_tjeneste_kort&oppdrag=utskrift&regninger=1&id=<?php echo $tjenesteId?>');
                    }
                }
            ]
        }
    }, {
        scale: 'medium',
        text: 'Endre',
        handler: function() {
            window.location = '/drift/index.php?oppslag=delte_kostnader_tjeneste_skjema&id=<?php echo $tjenesteId?>';
        }
    }, {
        scale: 'medium',
        text: 'Tilbake',
        handler: function() {
            window.location = '<?php echo $tilbakeUrl;?>';
        }
    }]
});