<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var int $leieobjektId
 * @var Meny|string $meny
 * @var Panel|string $skjema
 * @var string $formActionUrl
 */
use Kyegil\Leiebasen\Visning\drift\js\leieobjekt_skjema\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;use Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel;
?>

/**
 * @name leiebasen
 * @type {Object}
 */

/**
 * @type {Ext.form.Panel}
 */
var skjema = <?php echo $skjema;?>;

/**
 * @type {Ext.button.Button}
 */
var saveButton = Ext.getCmp('save-button');

saveButton.setHandler(function() {
    skjema.getForm().submit({
        waitMsg: "Lagrer...",
        url: <?php echo json_encode($formActionUrl);?>
    });
});