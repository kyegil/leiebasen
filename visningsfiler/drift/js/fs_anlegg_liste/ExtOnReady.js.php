<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var Meny|string $meny
 * @var string $tilbakeUrl
 */
use Kyegil\Leiebasen\Visning\drift\js\fs_anlegg_liste\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>

leiebasen.lastRutenett = function() {
    datasett.getProxy().extraParams = {
        søkefelt: søkefelt.getValue()
    };
    sidevelger.moveFirst();
    datasett.load({
        params: {
            start: 0,
            limit: 300
        }
    });
}

/**
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext-method-define
 */
Ext.define('leiebasen.Fellesstrømanlegg', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [ // {@link http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.Field}
        {name: 'id', type: 'int'},
        {name: 'anleggsnummer'},
        {name: 'målernummer'},
        {name: 'plassering'},
        {name: 'formål'},
        {name: 'html'}
    ]
});

/**
 * @type {Ext.grid.plugin.CellEditing}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.plugin.CellEditing
 */
var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
    clicksToEdit: 1
});

/**
 * @type {Ext.grid.plugin.RowEditing}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.plugin.RowEditing
 */
var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
    autoCancel: false,
    listeners: {
        beforeedit: function (grid, e) {
            return e.column.xtype !== 'actioncolumn';
        }
    }
});

/**
 * @type {Ext.data.Store}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.Store
 */
var datasett = Ext.create('Ext.data.Store', {
    model: 'leiebasen.Fellesstrømanlegg',
    pageSize: 300,
    remoteSort: true,
    proxy: {
        type: 'ajax',
        simpleSortMode: true,
        url: "/drift/index.php?oppslag=fs_anlegg_liste&oppdrag=hent_data",
        timeout: 300000, // 5 mins
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRows'
        }
    },
    sorters: [{
        property: 'id',
        direction: 'ASC'
    }]
});

/**
 * @type {Ext.form.field.Text}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Text
 */
var søkefelt = Ext.create('Ext.form.field.Text', {
    emptyText: 'Søk og klikk ↵',
    name: 'søkefelt',
    width: 200,
    listeners: {
        specialkey: function() {
            datasett.getProxy().extraParams = {
                søkefelt: søkefelt.getValue()
            };
            leiebasen.lastRutenett();
        }
    }
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var id = Ext.create('Ext.grid.column.Column', {
    align:		'right',
    dataIndex:	'id',
    text:		'Id',
    sortable:	true,
    hidden:		true,
    width:		40
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var anleggsnummer = Ext.create('Ext.grid.column.Column', {
    dataIndex: 'anleggsnummer',
    text: 'Anlegg',
    align: 'right',
    sortable:	true,
    width:		90
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var målernummer = Ext.create('Ext.grid.column.Column', {
    dataIndex: 'målernummer',
    text: 'Måler',
    align: 'right',
    sortable:	true,
    width:		90
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var plassering = Ext.create('Ext.grid.column.Column', {
    dataIndex: 'plassering',
    text: 'Plassering',
    sortable:	true,
    width:		200,
    flex:		1
});

/**
 * @type {Ext.grid.column.Column}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Column
 */
var formål = Ext.create('Ext.grid.column.Column', {
    dataIndex: 'formål',
    text: 'Bruksformål',
    sortable:	true,
    width:		420,
    flex:		1
});

/**
 * @type {Ext.grid.column.Action}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.column.Action
 */
var vis  = Ext.create('Ext.grid.column.Action', {
    header:		'Detaljer',
    dataIndex: 'id',
    icon: '/pub/media/bilder/drift/detaljer_lite.png',
    tooltip: 'Vis detaljer',
    handler: function(grid, rowIndex, colIndex, item, e, record) {
        window.location = '/drift/index.php?oppslag=fs_anlegg_kort&id=' + record.get('id');
    },
    sortable: false,
    width: 50
});

/**
 * @type {Ext.toolbar.Paging}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.toolbar.Paging
 */
var sidevelger = Ext.create('Ext.toolbar.Paging', {
    xtype: 'pagingtoolbar',
    store: datasett,
    dock: 'bottom',
    displayInfo: true
});

/**
 * @type {Ext.grid.Panel}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.Panel
 */
var rutenett = Ext.create('Ext.grid.Panel', {
    autoScroll: true,
    layout: 'border',
    frame: false,
    store: datasett,
    title: 'Strømanlegg og anvendelse',
    tbar: [
        søkefelt
    ],
    columns: [
        id,
        anleggsnummer,
        målernummer,
        plassering,
        formål,
        vis
    ],
    renderTo: 'panel',
    height: 600,
    width: 500,

    dockedItems: [sidevelger],

    buttons: [{
        text: 'Tilbake',
        handler: function() {
            window.location = '<?php echo $tilbakeUrl;?>';
        }
    }, {
        text: 'Skriv ut liste over strømanlegg',
        handler: function() {
            window.open('/drift/index.php?oppslag=fs_anlegg_liste&oppdrag=utskrift');
        }
    }, {
        text: 'Registrer nytt fellesstrømanlegg',
        handler: function() {
            window.location = "/drift/index.php?oppslag=fs_anlegg_skjema&id=*";
        }
    }]
});

/**
 * Oppretter detaljpanelet
 *
 * @type {Ext.panel.Panel}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.panel.Panel
 */
var detaljpanel = Ext.create('Ext.panel.Panel', {
    frame: true,
    height: 600,
    items: [
        {
            id: 'detaljfelt',
            region: 'center',
            bodyStyle: {
                background: '#ffffff',
                padding: '7px'
            },
            autoScroll: true,
            html: 'Velg et strømanlegg i listen til venstre for å se fordelingsnøkkelen.'
        }
    ],
    layout: 'border',
    renderTo: 'detaljpanel',
    title: 'Fordelingsnøkkel'
});

rutenett.getStore().on({
    load: function(store, records, successful) {
        var jsonData = store.getProxy().getReader().jsonData;
        if (typeof jsonData == 'undefined') {
            Ext.MessageBox.alert('Problem:', 'Klarte ikke laste denne tabellen av ukjent grunn.');
            return false;
        }
        if (!successful) {
            var msg = store.getProxy().getReader().jsonData.msg;
            msg = msg.replace(/\r\n|\r|\n/g, '<br>');
            Ext.MessageBox.alert('Klarte ikke laste data:', msg);
        }
    }
});

/**
 * Hva skjer når du klikker på ei linje i rutenettet?
 */
rutenett.on('select', function( rowModel, record ) {
    var detaljfelt = Ext.getCmp('detaljfelt');

    // Format for detaljvisningen
    var mal = new Ext.Template([
        '{html}'
    ]);
    mal.overwrite( detaljfelt.body, record.data );
});

datasett.getProxy().extraParams = {
    søkefelt: søkefelt.getValue()
};

leiebasen.lastRutenett();