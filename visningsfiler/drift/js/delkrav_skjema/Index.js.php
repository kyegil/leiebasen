<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\delkrav_skjema\Index $this
 * @var integer $delkravtypeId
 */
?>

/***
 * @type {jQuery} $
 */
leiebasen.drift.delkravSkjema = leiebasen.drift.delkravSkjema || {}

leiebasen.drift.delkravSkjema.initier = function() {
    let satsFelt = $("[name='sats']");
    this.felter = {
        kravtypeFelt: $("[name='kravtype']"),
        aktivFelt: $("[type='checkbox'][name='aktiv']"),
        relativFelt: $("[type='checkbox'][name='relativ']"),
        satsFelt: satsFelt,
        satsLabel: $("label[for='" + $(satsFelt).attr('id') + "']"),
        indeksreguleresFelt: $("[type='checkbox'][name='indeksreguleres']")
    }
    this.felter.aktivFelt.on('change', function() {
        leiebasen.drift.delkravSkjema.oppdaterSkjema();
    });
    this.felter.relativFelt.on('change', function() {
        leiebasen.drift.delkravSkjema.oppdaterSkjema();
    });
    return this;
}

/**
 * Oppdater skjemaet og planen hver gang det er gjort endringer i feltene som danner grunnlaget for den
 */
leiebasen.drift.delkravSkjema.oppdaterSkjema = function () {
    let relativ = this.felter.relativFelt.prop('checked');
    let satsLabel = $("label[for='" + $(this.felter.satsFelt).attr('id') + "']")
    if (relativ) {
        satsLabel.html('Prosentsats som legges på kravbeløpet: ');
        this.felter.indeksreguleresFelt.parent().hide();
    }
    else {
        satsLabel.html('Beløp i kroner (per år): ');
        this.felter.indeksreguleresFelt.parent().show();
    }
    return this;
}

leiebasen.drift.delkravSkjema.initier().oppdaterSkjema();