<?php
/**
 * Leiebasen Visningsmal
 *
 * @var Meny $this
 * @var int $brukerId
 * @var string|\Kyegil\ViewRenderer\ViewInterface $menyElementer
 */
use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;
?>
leiebasen.visMeny = function() {
    Ext.create('Ext.toolbar.Toolbar', {

        renderTo: 'menylinje',
        layout: {
            type: 'hbox'
        },
        border: false,
        padding: '0',
        items: <?php echo $menyElementer;?>
    });
}
leiebasen.visMeny();

