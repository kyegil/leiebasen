<?php
/**
 * Leiebasen Visningsmal
 *
 * @var Index $this
 * @var string $loaderPath
 * @var string $extRequire
 * @var Visning|string $extOnReady
 */
use Kyegil\Leiebasen\Visning;use Kyegil\Leiebasen\Visning\drift\js\shared\body\main\Index;
?>
Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', '<?php echo $loaderPath;?>');

Ext.require(<?php echo $extRequire;?>);

Ext.onReady(function() {
    <?php echo $extOnReady;?>
});
