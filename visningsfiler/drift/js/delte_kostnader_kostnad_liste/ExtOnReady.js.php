<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var string $initLeiebasenJsObjektEgenskaper
 * @var Visning|string $meny
 * @var string $tilbakeUrl
 */
use Kyegil\Leiebasen\Visning;use Kyegil\Leiebasen\Visning\drift\js\delte_kostnader_kostnad_liste\ExtOnReady;
?>

/**
 * @name leiebasen
 * @type {Object}
 */
<?php echo $initLeiebasenJsObjektEgenskaper;?>

leiebasen.lastRutenett = function() {
    datasett.getProxy().extraParams = {
        søkefelt: søkefelt.getValue()
    };
    sidevelger.moveFirst();
}

/**
 * @param {leiebasen.Kostnad} record
 */
leiebasen.drift.slettFaktura = function(record){
    let fakturaId = record.get('id');
    Ext.Msg.confirm({
        title: 'Slett kostnaden',
        msg: 'Er du sikker på du vil slette denne kostnaden?',
        buttons: Ext.Msg.OKCANCEL,
        fn: function(buttonId) {
            if (buttonId === 'ok') {
                Ext.Ajax.request({
                    params: {
                        'faktura_id': fakturaId
                    },
                    waitMsg: 'Vent...',
                    url: "/drift/index.php?oppslag=delte_kostnader_kostnad_liste&oppdrag=oppgave&oppgave=slett_faktura",

                    failure: function(response) {
                        Ext.MessageBox.alert('Mislyktes', 'Respons-status ' + response.status + ' fra tjeneren... ', function() {
                            datasett.load();
                        });
                    },
                    success : function(response) {
                        if(!response.responseText) {
                            Ext.MessageBox.alert('Problem', 'Null respons fra tjeneren...', function() {
                                datasett.load();
                            });
                        }
                        var jsonRespons = Ext.JSON.decode(response.responseText, true);
                        if(!jsonRespons) {
                            Ext.MessageBox.alert('Problem', response.responseText, function() {
                                datasett.load();
                            });
                        }
                        else if (jsonRespons.success) {
                            datasett.remove(record);
                            Ext.MessageBox.alert('Slettet', jsonRespons.msg);
                        }
                        else {
                            Ext.MessageBox.alert('Hmm..', jsonRespons.msg, function() {
                                datasett.load();
                            });
                        }
                    }
                });
            }
        },
        icon: Ext.window.MessageBox.QUESTION
    });
}

/**
 * @param {Number} recordId
 */
leiebasen.drift.forberedFordeling = function(recordId) {
    let record = datasett.getById(recordId);
    if (!record || !record.get('kan_fordeles')) {
        alert('Denne regningen kan ikke fordeles');
        return;
    }
    /**
     * @type {[{}]}
     */
    let manuelleFordelingselementer = record.get('manuelle_fordelingselementer');
    if (manuelleFordelingselementer.length) {
        /**
         * @type {Ext.form.Panel}
         */
        let fordelingsskjema = Ext.create('Ext.form.Panel', {
            border: false,
            layout: 'form',
            padding: 10,
            autoScroll: true,
            buttons: [{
                text: 'Fordél',
                formBind: true,
                handler: function() {
                    leiebasen.drift.fordel(record, fordelingsskjema.getValues());
                    popup.close();
                }
            }]
        });
        manuelleFordelingselementer.forEach(function(fordelingelement){
            fordelingsskjema.add(Ext.create('Ext.form.field.Number', {
                minValue: 0,
                hideTrigger: true,
                allowDecimals: false,
                allowBlank: false,
                value: fordelingelement.beløp,
                name: fordelingelement.element_id,
                labelWidth: 500,
                fieldLabel: fordelingelement.beskrivelse
            }));
        });
        let popup = Ext.create('Ext.window.Window', {
            title: 'Oppgi fast beløp / manuell beregning',
            height: 300,
            width: 700,
            layout: 'fit',
            items: fordelingsskjema
        });
        popup.show();
    }
    else {
        this.fordel(record, {});
    }
}

/**
 * @param {leiebasen.Kostnad} record
 * @param {Object} fastbeløp
 */
leiebasen.drift.fordel = function(record, fastbeløp) {
    /**
     * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.Ajax-method-request
     */
    Ext.Ajax.request({
        params: {
            id: record.get('id'),
            beløp: Ext.JSON.encode(fastbeløp)
        },

        waitMsg: 'Fordeler...',
        url: '/drift/index.php?oppslag=delte_kostnader_kostnad_liste&oppdrag=oppgave&oppgave=fordel',

        /**
         * @param {XMLHttpRequest} response
         */
        failure:function(response) {
            Ext.MessageBox.alert('Whoops! Problemer...', 'Klarte ikke å fordele.<br>Kan du ha mistet nettforbindelsen?');
        },

        /**
         * @param {XMLHttpRequest} response
         */
        success: function(response) {
            let responsObjekt = Ext.JSON.decode(response.responseText, true);
            if(!responsObjekt) {
                Ext.MessageBox.alert('Klarte ikke fordele', response.responseText);
            }
            else if (responsObjekt.success && responsObjekt.data) {
                let oppdatertLinje = Ext.create('leiebasen.Kostnad', responsObjekt.data);
                record.set(oppdatertLinje.getData());
                datasett.commitChanges();
                if(responsObjekt.msg) {
                    Ext.MessageBox.alert('Obs!', responsObjekt.msg);
                }
            }
            else {
                Ext.MessageBox.alert('Hmm..', responsObjekt.msg);
            }
        }
    });
}

/**
 * @param {leiebasen.Kostnad} record
 */
leiebasen.drift.varsleFordeling = function(record) {
    /**
     * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.Ajax-method-request
     */
    Ext.Ajax.request({
        params: {
            id: record.get('id'),
            tjeneste_id: record.get('tjeneste_id')
        },

        url: '/drift/index.php?oppslag=delte_kostnader_kostnad_liste&oppdrag=hent_data&data=fordeling_varseltekst',

        /**
         * @param {XMLHttpRequest} response
         */
        failure:function(response) {
            Ext.MessageBox.alert('Whoops! Problemer...', 'Klarte ikke å laste eksisterende varseltekst.<br>Kan du ha mistet nettforbindelsen?');
        },

        /**
         * @param {XMLHttpRequest} response
         */
        success: function(response) {
            let responsObjekt = Ext.JSON.decode(response.responseText, true);
            if(!responsObjekt) {
                Ext.MessageBox.alert('Klarte ikke laste eksisterende varseltekst', response.responseText);
            }
            else if (!responsObjekt.success) {
                Ext.MessageBox.alert('Hmm..', responsObjekt.msg);
            }
            else {
                /**
                 * @type {Ext.form.field.TextArea}
                 */
                let fordelingVarseltekstFelt = Ext.create('Ext.form.field.TextArea', {
                    hideLabel: true,
                    height: '100%',
                    name: 'strømfordelingstekst',
                    value: responsObjekt.data,
                    width: '100%'
                });

                /**
                 * @type {Ext.window.Window}
                 */
                let fordelingVarseltekstFeltVindu = Ext.create('Ext.window.Window', {
                    title: 'Bekreft varsel-tekst',
                    closeAction: 'hide',
                    layout: 'fit',
                    height: 200,
                    width: 800,
                    items: [fordelingVarseltekstFelt],
                    buttons: [
                        {
                            text: 'Send',
                            handler: function(button, event) {
                                /**
                                 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.Ajax-method-request
                                 */
                                Ext.Ajax.request({
                                    params: {
                                        id: record.get('id'),
                                        fordeling_varseltekst: fordelingVarseltekstFelt.getValue()
                                    },

                                    waitMsg: 'Sender varsler om foreslått fordeling...',
                                    url: '/drift/index.php?oppslag=delte_kostnader_kostnad_liste&oppdrag=oppgave&oppgave=varsle',

                                    /**
                                     * @param {XMLHttpRequest} response
                                     */
                                    failure:function(response) {
                                        Ext.MessageBox.alert('Whoops! Problemer...', 'Klarte ikke å varsle.<br>Kan du ha mistet nettforbindelsen?');
                                    },

                                    /**
                                     * @param {XMLHttpRequest} response
                                     */
                                    success: function(response) {
                                        let responsObjekt = Ext.JSON.decode(response.responseText, true);
                                        if(!responsObjekt) {
                                            Ext.MessageBox.alert('Klarte ikke varsle', response.responseText);
                                        }
                                        else if (responsObjekt.success && responsObjekt.data) {
                                            fordelingVarseltekstFeltVindu.close();
                                            let oppdatertLinje = Ext.create('leiebasen.Kostnad', responsObjekt.data);
                                            record.set(oppdatertLinje.getData());
                                            datasett.commitChanges();
                                            if(responsObjekt.msg) {
                                                Ext.MessageBox.alert('Utført', responsObjekt.msg);
                                            }
                                        }
                                        else {
                                            Ext.MessageBox.alert('Hmm..', responsObjekt.msg);
                                        }
                                    }
                                });
                            }
                        }
                    ]
                });
                fordelingVarseltekstFeltVindu.show();
            }
        }
    });
}

/**
 * @param {leiebasen.Kostnad} record
 */
leiebasen.drift.bekreftFordeling = function(record) {
    /**
     * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.Ajax-method-request
     */
    Ext.Ajax.request({
        params: {
            id: record.get('id')
        },

        waitMsg: 'Bekrefter og krever inn fordelingen ...',
        url: '/drift/index.php?oppslag=delte_kostnader_kostnad_liste&oppdrag=oppgave&oppgave=bekreft',

        /**
         * @param {XMLHttpRequest} response
         */
        failure:function(response) {
            Ext.MessageBox.alert('Whoops! Problemer...', 'Klarte ikke å varsle.<br>Kan du ha mistet nettforbindelsen?');
        },

        /**
         * @param {XMLHttpRequest} response
         */
        success: function(response) {
            let responsObjekt = Ext.JSON.decode(response.responseText, true);
            if(!responsObjekt) {
                Ext.MessageBox.alert('Klarte ikke bekrefte fordelingen', response.responseText);
            }
            else if (responsObjekt.success) {
                datasett.remove([record]);
                if(responsObjekt.msg) {
                    Ext.MessageBox.alert('Utført', responsObjekt.msg);
                }
            }
            else {
                Ext.MessageBox.alert('Hmm..', responsObjekt.msg);
            }
        }
    });
}

/**
 * @param {Ext.grid.plugin.CellEditing} editor
 * @param {Object} e
 * @param {Object} eOpts
 */
leiebasen.drift.lagreEndringer = function(editor, e, eOpts) {
    if(
        e.originalValue != null
        && typeof e.value === 'object'
        && typeof e.originalValue === 'object'
        && e.value.toString() === e.originalValue.toString()
    ) {
        return true;
    }

    // Dersom dette er ei tom linje, og det ikke er noen verdier å lagre, skipper vi
    if(!e.record.getId() && !e.value) {
        return true;
    }
    // Dersom ei tom linje taes i bruk må det opprettes en ny
    if(!e.record.getId()) {
        e.store.add({});
    }

    /**
     * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.Ajax-method-request
     */
    Ext.Ajax.request({
        params: {
            id: e.record.get('id'),
            felt: e.field,
            verdi: e.value,
            opprinnelig: e.originalValue
        },

        waitMsg: 'Lagrer...',
        url: '/drift/index.php?oppslag=delte_kostnader_kostnad_liste&oppdrag=ta_i_mot_skjema&skjema=oppdatering',

        /**
         * @param {XMLHttpRequest} response
         */
        failure:function(response) {
            Ext.MessageBox.alert('Whoops! Problemer...', 'Klarte ikke å lagre endringen.<br>Kan du ha mistet nettforbindelsen?');
        },

        /**
         * @param {XMLHttpRequest} response
         */
        success: function(response) {
            let responsObjekt = Ext.JSON.decode(response.responseText, true);
            if(!responsObjekt) {
                Ext.MessageBox.alert('Klarte ikke lagre', response.responseText);
            }
            else if (responsObjekt.success && responsObjekt.data) {
                let oppdatertLinje = Ext.create('leiebasen.Kostnad', responsObjekt.data);
                e.record.set(oppdatertLinje.getData());
                datasett.commitChanges();
                if(responsObjekt.msg) {
                    Ext.MessageBox.alert('Obs!', responsObjekt.msg);
                }
            }
            else {
                Ext.MessageBox.alert('Hmm..', responsObjekt.msg);
            }
        }
    });
}

/**
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext-method-define
 */
Ext.define('leiebasen.Kostnad', {
    extend: 'Ext.data.Model',
    idProperty: 'id',
    fields: [ // http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.Field
        {name: 'id', type: 'int', useNull: true},
        {name: 'tjeneste_id', type: 'int', useNull: true},
        {name: 'tjeneste'},
        {name: 'fakturanummer'},
        {name: 'beløp', type: 'float', useNull: true},
        {name: 'beløp_formatert'},
        {name: 'fradato', type: 'date', dateFormat: 'Y-m-d', useNull: true},
        {name: 'tildato', type: 'date', dateFormat: 'Y-m-d', useNull: true},
        {name: 'termin'},
        {name: 'forbruk', type: 'float', useNull: true},
        {name: 'forbruk_formatert'},
        {name: 'varslet', type: 'date', dateFormat: 'Y-m-d H:i:s', useNull: true},
        {name: 'lagt_inn_av'},
        {name: 'forslag_generert', type: 'boolean'},
        {name: 'kan_fordeles', type: 'boolean'},
        {name: 'kan_varsles', type: 'boolean'},
        {name: 'kan_låses', type: 'boolean'},
        {name: 'manuelle_fordelingselementer'},
        {name: 'html'}
    ]
});


/**
 * @type {Ext.grid.plugin.CellEditing}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.plugin.CellEditing
 */
var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
    clicksToEdit: 1
});
cellEditing.on('edit', leiebasen.drift.lagreEndringer);

/**
 * @type {Ext.grid.plugin.RowEditing}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.plugin.RowEditing
 */
var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
    autoCancel: false,
    listeners: {
        beforeedit: function (grid, e) {
            return e.column.xtype !== 'actioncolumn';
        }
    }
});

/**
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.plugin.RowExpander
 */
var rowExpander = new Ext.grid.plugin.RowExpander({
    rowBodyTpl : new Ext.XTemplate('{html}')
});

/**
 * @type {Ext.data.Store}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.data.Store
 */
var datasett = Ext.create('Ext.data.Store', {
    model: 'leiebasen.Kostnad',
    storeId: 'kostnadssett',
    pageSize: 300,
    remoteSort: true,
    proxy: {
        type: 'ajax',
        simpleSortMode: true,
        url: "/drift/index.php?oppslag=delte_kostnader_kostnad_liste&oppdrag=hent_data&data=nye_fakturaer",
        timeout: 300000, // 5 mins
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRows'
        },
        listeners: {
            exception: function(proxy, response) {
                let responsStreng = response.responseText;
                let tilbakemelding = Ext.JSON.decode(responsStreng, true);
                if (response.status !== 200 || !tilbakemelding) {
                    Ext.MessageBox.alert('Ugyldig respons', responsStreng);
                }
                else if (!tilbakemelding.success && tilbakemelding.msg) {
                    Ext.MessageBox.alert('Hm', tilbakemelding.msg);
                }
            }
        }
    },
    sorters: [{
        property: 'id',
        direction: 'DESC'
    }]
});

/**
 * @type {Ext.form.field.Text}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Text
 */
var søkefelt = Ext.create('Ext.form.field.Text', {
    emptyText: 'Søk og klikk ↵',
    name: 'søkefelt',
    width: 200,
    listeners: {
        specialkey: function() {
            datasett.getProxy().extraParams = {
                søkefelt: søkefelt.getValue()
            };
            leiebasen.lastRutenett();
        }
    }
});

/**
 * @type {Ext.toolbar.Paging}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.toolbar.Paging
 */
var sidevelger = Ext.create('Ext.toolbar.Paging', {
    xtype: 'pagingtoolbar',
    store: datasett,
    dock: 'bottom',
    displayInfo: true
});

/**
 * @type {Ext.grid.Panel}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.grid.Panel
 */
var rutenett = Ext.create('Ext.grid.Panel', {
    autoScroll: true,
    layout: 'border',
    frame: false,
    store: datasett,
    title: 'Kostnader for fordeling blant beboere',
    tbar: [
        søkefelt
    ],

//    selType: 'checkboxmodel',

    columns: [
        leiebasen.drift.kolonner.id,
        leiebasen.drift.kolonner.fakturanummer,
        leiebasen.drift.kolonner.tjeneste_id,
        leiebasen.drift.kolonner.termin,
        leiebasen.drift.kolonner.fradato,
        leiebasen.drift.kolonner.tildato,
        leiebasen.drift.kolonner.forbruk,
        leiebasen.drift.kolonner.beløp,
        leiebasen.drift.kolonner.varslet,
        leiebasen.drift.kolonner.varsle,
        leiebasen.drift.kolonner.bekreft,
        leiebasen.drift.kolonner.slett
    ],

    plugins: [cellEditing, rowExpander],

    renderTo: 'panel',
    height: '100%',
    width: 900,

    dockedItems: [sidevelger],

    buttons: [{
        text: 'Tilbake',
        handler: function() {
            window.location = '<?php echo $tilbakeUrl;?>';
        }
    }, {
        text: 'Importer kostnader fra fil',
        handler: function() {
            window.location = "/drift/index.php?oppslag=delte_kostnader_kostnad_import";
        }
    }, {
        text: 'Registrer ny tjeneste',
        handler: function() {
            window.location = "/drift/index.php?oppslag=delte_kostnader_tjeneste_skjema&id=*";
        }
    }]
});


rutenett.getStore().on({
    /**
     * @param {Ext.data.Store} store
     * @param {leiebasen.Kostnad[]} records
     * @param {boolean} successful
     * @returns {boolean}
     */
    load: function(store, records, successful) {
        var jsonData = store.getProxy().getReader().jsonData;
        if (typeof jsonData == 'undefined') {
            Ext.MessageBox.alert('Problem:', 'Klarte ikke laste denne tabellen av ukjent grunn.');
            return false;
        }
        if (successful) {
            store.add({});
        }
        else {
            var msg = store.getProxy().getReader().jsonData.msg;
            msg = msg.replace(/\r\n|\r|\n/g, '<br>');
            Ext.MessageBox.alert('Klarte ikke laste data:', msg);
        }
    }
});

datasett.getProxy().extraParams = {
    søkefelt: søkefelt.getValue()
};

leiebasen.lastRutenett();