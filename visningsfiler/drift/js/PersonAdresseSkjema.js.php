<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\PersonAdresseSkjema $this
 * @var string $personId
 * @var string $skript
 */
?>

leiebasen.drift.personAdresseSkjema = leiebasen.drift.personAdresseSkjema || {
    felter: {},
    initier: function() {
        if(typeof this._initiert !== 'undefined') {
            return this;
        }
        this.personId = <?php echo json_encode($personId); ?>;
        this.felter = {
            erOrgFelt: $("[name='er_org']"),
            fornavnFelt: $("[name='fornavn']"),
            etternavnFelt: $("[name='etternavn']"),
            fødselsdatoFelt: $("[name='fødselsdato']"),
            personnrFelt: $("[name='personnr']")
        };

        this.felter.erOrgFelt.on('change', function() {
            leiebasen.drift.personAdresseSkjema.oppdaterFelter();
        });

        this.oppdaterFelter();

        this._initiert = true;
    },

    oppdaterFelter: function() {
        let erOrg = this.felter.erOrgFelt.is(':checked');
        this.felter.fornavnFelt.attr('required', !erOrg);
        this.felter.fornavnFelt.parent().toggle(!erOrg);
        this.felter.fødselsdatoFelt.parent().toggle(!erOrg);

        $etternavnLabel = $("label[for='" + this.felter.etternavnFelt.attr('id') + "']");
        $etternavnLabel.html(erOrg ? 'Navn' : 'Etternavn');

        this.felter.personnrFelt.attr('pattern', erOrg ? '[8-9][0-9]{8}' : '[0-9]{5}|[0-9]{11}');
        $personnrLabel = $("label[for='" + this.felter.personnrFelt.attr('id') + "']");
        $personnrLabel.html(erOrg ? 'Organisasjonsnr (9 sifre)' : 'Personnummer (5 eller 11 sifre)');
    },

    bekreftOgSlettAdressekort: function() {
        if(confirm("Er du sikker på at du vil slette dette adressekortet?\nDu bør ikke slette adressekort for nåværende leietakere...")) {

            $.ajax({
                url: '/drift/person_adresse_skjema/' + this.personId + '/oppgave&oppgave=slett_adressekort',
                data: {id: this.personId},
                method: "POST",
                dataType: 'json',
                success: function (data) {
                    if (data.msg) {
                        alert(data.msg);
                    }
                    if (data.url) {
                        location.href = data.url;
                    }
                }
            });
        }
    }
};

<?php echo $skript;?>

leiebasen.drift.personAdresseSkjema.initier();
