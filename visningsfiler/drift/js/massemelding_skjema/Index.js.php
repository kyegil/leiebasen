<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\massemelding_skjema\Index $this
 * @var string $skript
 */
?>
leiebasen.drift.massemeldingSkjema = leiebasen.drift.massemeldingSkjema || {};
leiebasen.drift.massemeldingSkjema.initier = function() {
    this.medium = 'epost';
    this.felter = {
        medieVelger: $("[name='medium']"),
        mottakerLeieforholdIderFelt: $("[name='mottakere[]']"),
        svaradresseFelt: $("[name='svaradresse']"),
        smsAvsenderFelt: $("[name='sms_avsender']"),
        vedleggFelter: $("[name='vedlegg*']"),
        vedleggBeholder: $("*[data-name='vedlegg']").parent(),
        emneFelt: $("[name='emne']"),
        renTekstInnholdFelt: $("#tekst-innhold"),
        htmlInnholdFelt: $("#html-innhold")
    };
    this.felter.medieVelger.on('change', function() {
        leiebasen.drift.massemeldingSkjema.oppdaterSkjema();
    });

    $("#massemelding_skjema button[type = 'submit']").click(function() {
        const send = ($(this).attr("value")) === '1';
        const medium = leiebasen.drift.massemeldingSkjema.medium;
        const felter = leiebasen.drift.massemeldingSkjema.felter;
        felter.mottakerLeieforholdIderFelt.prop('required',send);
        felter.svaradresseFelt.prop('required', send && (medium === 'epost'));
        felter.emneFelt.prop('required', send && (medium === 'epost'));
        felter.smsAvsenderFelt.prop('required', send && (medium === 'sms'));
    })

    this.oppdaterSkjema();
};

leiebasen.drift.massemeldingSkjema.oppdaterSkjema = function() {
    let html = '';
    let renTekst = '';
    if(this.felter.medieVelger.filter(":checked").length !== 0) {
        this.medium = this.felter.medieVelger.filter(":checked").val();
    }
    if(leiebasen.ckEditor && leiebasen.ckEditor['html-innhold']) {
        if(this.medium === 'epost' && this.felter.renTekstInnholdFelt.parent().is(":visible")) {
            html = this.felter.renTekstInnholdFelt.val().replace(/(?:\r\n|\r|\n)/g, '<br>');
            leiebasen.ckEditor['html-innhold'].setData(html);
        }
        else if(this.medium === 'sms' && this.felter.htmlInnholdFelt.parent().is(":visible")) {
            renTekst = leiebasen.ckEditor['html-innhold'].getData();
            renTekst = renTekst.replace(/<(br|\/p|\/div|\/li)\s*[\/]?>/gi, '\n');
            renTekst = renTekst.replace(/<[^>]*>/g, '');
            renTekst = renTekst.replace(/&nbsp;/g, '');
            renTekst = renTekst.replace(/\n$/g, '');
            this.felter.renTekstInnholdFelt.val(renTekst);
        }
    }
    this.felter.htmlInnholdFelt.prop('disabled', this.medium !== 'epost');
    this.felter.htmlInnholdFelt.parent().toggle(this.medium === 'epost');

    this.felter.renTekstInnholdFelt.prop('disabled', this.medium === 'epost');
    this.felter.renTekstInnholdFelt.parent().toggle(this.medium !== 'epost');

    this.felter.vedleggFelter.prop('disabled', this.medium !== 'epost');
    this.felter.vedleggBeholder.toggle(this.medium === 'epost');

    $("input[type='checkbox'][name='kopi[oppsummering]']").parent().toggle(this.medium === 'epost');
    $("[name='kopi[eksempel]']").parent().parent().parent().toggle(this.medium === 'epost');

    this.felter.svaradresseFelt.parent().toggle(this.medium === 'epost');
    this.felter.emneFelt.parent().toggle(this.medium === 'epost');
    this.felter.smsAvsenderFelt.parent().toggle(this.medium !== 'epost');
}

leiebasen.drift.massemeldingSkjema.forhåndsvis = function() {
    const html = !this.felter.htmlInnholdFelt.prop('disabled');
    const innhold = html ? leiebasen.ckEditor['html-innhold'].getData() : $("*:enabled[name='innhold']").val();

    $.ajax({
        url: "/drift/index.php?oppslag=massemelding_skjema&oppdrag=hent_data&data=forhåndsvisning",
        method: 'POST',
        data: {
            medium: this.medium,
            html: html ? 1 : 0,
            mottakere: this.felter.mottakerLeieforholdIderFelt.val(),
            emne: this.felter.emneFelt.val(),
            innhold: innhold
        },
        dataType: 'json',
        success: function (data) {
            $("#forhåndsvisning .modal-title").html(data.emne)
            $("#forhåndsvisning .modal-body").html(data.innhold)
            $("#forhåndsvisning").modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown ) {
            alert(textStatus ? textStatus : errorThrown);
        }
    });
}

<?php echo $skript;?>

leiebasen.drift.massemeldingSkjema.initier();
