<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\poeng_skjema\Index $this
 * @var string $poengId
 * @var string $programKode
 * @var string $skript
 */
?>
<?php echo $skript;?>

leiebasen.drift.poengSkjema = leiebasen.drift.poengSkjema || {
    slett: function () {
        /**
         * @type {Number} poengId
         */
        let poengId = <?php echo (int)$poengId;?>;
        if (confirm('Er du sikker på at du vil slette denne oppføringen?')) {
            $.ajax({
                url: '/drift/index.php?oppslag=poeng_skjema&id=' + poengId + '&oppdrag=oppgave&oppgave=slett_poeng',
                data: {poeng_id: poengId},
                method: "POST",
                dataType: 'json',
                success: function (data) {
                    if (data.msg) {
                        alert(data.msg);
                    }
                    if (data.url) {
                        location.href = data.url;
                    }
                }
            });
        }
    }
};
leiebasen.drift.poengSkjema.initier = function() {

};

leiebasen.drift.poengSkjema.initier();
