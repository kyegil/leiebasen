<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\utskriftsveiviser_purringer\ExtOnReady $this
 * @var \Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny|string $meny
 * @var \Kyegil\Leiebasen\Visning\felles\extjs4\Ext\form\Panel|string $skjema
 * @var bool $adskilt
 * @var string $tilbakeUrl
 * @var bool $utskriftAlleredeIgangsatt
 */
?>

leiebasen.skrivUt = function() {
    framdriftsindikator.show({
        msg: 'Vent litt...',
        width: 300,
        wait: true,
        waitConfig: {
            interval: 1000, // interval * increment = tid i ms
            increment: 30,
            text : 'Setter sammen utskriftsfil'
        }
    });

    skjema.getForm().submit({
        url: '/drift/index.php?oppslag=utskriftsveiviser&oppdrag=oppgave&oppgave=opprett_utskrift'
    });
}

leiebasen.visBekreftelsesvindu = function() {
    window.onbeforeunload = function() {
        return 'Du bør ikke forlate utskriftsveiviseren uten å bekrefte om utskriften skal registreres i leiebasen.';
    };

    Ext.Msg.buttonText.yes = "Utskriften er OK. Registrer utskrifts- og forfallsdatoer";
    Ext.Msg.buttonText.no = "Utskriften skal forkastes uten å registreres";
    Ext.Msg.show({
        title: 'Bekreft at utskriften er OK',
        msg: '<p>Vent til utskriften er fullført, og bekreft at utskriften er vellykket og vil brukes.<br><br>Først når den er bekreftet vil utskrift og purringer bli registrert.</p>',
        buttons: Ext.Msg.YESNO,
        closable: false,
        fn: function(buttonId) {
            if(buttonId === 'yes') {
                leiebasen.registrerUtskrift();
            }
            else if(buttonId === 'no') {
                leiebasen.forkastUtskrift();
            }
        }
    });


}

leiebasen.registrerUtskrift = function() {
    Ext.MessageBox.wait('Utskriften registreres', 'Vent litt...');
    Ext.Ajax.request({

        url: '/drift/index.php?oppslag=utskriftsveiviser&oppdrag=ta_imot_skjema&skjema=utskriftsbekreftelse',
        params: {},
        failure:function(){
            Ext.MessageBox.alert('Whoops! Problemer...','Oppnår ikke kontakt med databasen! Prøv igjen senere.');
        },

        success: function(response) {
            var tilbakemelding = Ext.JSON.decode(response.responseText);
            if(tilbakemelding['success'] === true) {
                Ext.MessageBox.alert('Utskriften er registrert', tilbakemelding.msg, function() {
                    window.onbeforeunload = null;
                    Ext.Msg.buttonText.yes = "Skriv ut konvolutter";
                    Ext.Msg.buttonText.no = "Nei, takk";
                    Ext.MessageBox.confirm("Vil du skrive ut konvolutter?",
                        'Før du skriver ut må du putte konvolutter i skriveren.<br>Justering av adressefeltet kan gjøres i innstillingene for leiebasen.',
                        function(buttonId) {
                            if(buttonId === 'yes' && tilbakemelding['adresser']) {
                                window.open( "/drift/index.php?oppslag=personadresser_utskrift&oppdrag=lagpdf&pdf=konvolutter&leieforhold=" + tilbakemelding['adresser'].join());
                            }
                            window.location = tilbakemelding.url;
                        }
                    );
                });
            }
            else {
                Ext.MessageBox.alert('Hmm..',tilbakemelding['msg']);
            }
        }
    });
};

leiebasen.forkastUtskrift = function() {
    Ext.MessageBox.wait('Utskriften forkastes', 'Vent litt...');
    Ext.Ajax.request({
        url: '/drift/index.php?oppslag=utskriftsveiviser&oppdrag=oppgave&oppgave=forkast_utskrift',

        failure: function() {
            Ext.MessageBox.alert('Whoops! Problemer...', 'Oppnår ikke kontakt med databasen! Prøv igjen senere.');
        },

        success: function(response) {
            var tilbakemelding = Ext.JSON.decode(response.responseText);
            if(tilbakemelding['success'] === true) {
                window.onbeforeunload = null;
                Ext.MessageBox.alert('Utskriften er forkastet', tilbakemelding.msg, function() {
                    window.location = tilbakemelding.url;
                });
            }
            else {
                Ext.MessageBox.alert('Hmm..', tilbakemelding['msg']);

            }
        }

    });
};

leiebasen.bekreftEksisterendeUtskrift = function() {
    window.onbeforeunload = function() {
        return 'Du bør ikke forlate utskriftsveiviseren uten å bekrefte om utskriften skal registreres i leiebasen.';
    };
    /**
     * @type {Ext.window.MessageBox}
     */
    var utskriftsbekreftelse = Ext.create('Ext.window.MessageBox', {
        buttons: [
            {
                text: 'Vis utskriften på nytt.',
                handler: function () {
                    window.open('/drift/index.php?oppslag=giro&oppdrag=lagpdf');
                }
            },
            {
                text: 'Utskriften var OK. Registrer den.',
                handler: function () {
                    leiebasen.registrerUtskrift();
                }
            },
            {
                text: 'Utskriften skal forkastes',
                handler: function () {
                    leiebasen.forkastUtskrift();
                }
            }
        ]
    });

    utskriftsbekreftelse.show({
        title: 'Bekreft den eksisterende utskriften',
        msg: 'Det har allerede blitt påbegynt en utskrift som fortsatt står ubekreftet.<br>Du må bekrefte eller forkaste den påbegynte utskriften før du kan skrive ut på nytt.',
        closable: false,
        fn: function(buttonId) {
            if(buttonId === 'yes') {
                leiebasen.registrerUtskrift();
            }
            else if(buttonId === 'no') {
                leiebasen.forkastUtskrift();
            }
        }
    });
}

leiebasen.formSubmitAction.complete = function(form, action) {
    var success;
    var msg;
    if (action.response.responseText === '') {
        framdriftsindikator.hide();
        Ext.MessageBox.alert('Problem', 'Mottok ikke bekreftelse fra tjeneren  i forventet format');
    } else {
        try {
            var responsObjekt = Ext.JSON.decode(action.response.responseText);
            success = (typeof responsObjekt.success != 'undefined') ? action.result.success : false;
            msg = (typeof responsObjekt.msg === 'string') ? responsObjekt.msg : 'Opplysningene er lagret';
            if (success) {
                framdriftsindikator.hide();
                leiebasen.visBekreftelsesvindu();
                window.open('/drift/index.php?oppslag=giro&oppdrag=lagpdf');
            }
            else {
                framdriftsindikator.hide();
                Ext.MessageBox.alert('Mislyktes', msg);
            }

        } catch (e) {
            msg = action.response.responseText;
            Ext.MessageBox.alert('Mislyktes', msg);
        }
    }
}

leiebasen.formSubmitAction.failed = function(form, action) {
    var msg;
    if (action.failureType === "client") {
        framdriftsindikator.hide();
        Ext.MessageBox.alert('Problem:', 'Sjekk skjemaet for feil.');
    }
    else if (action.failureType === "connect") {
        framdriftsindikator.hide();
        Ext.MessageBox.alert('Problem:', 'Det er problemer med forbindelsen. Klarte ikke lagre.');
    }
    else if (!action.response.responseText) {
        framdriftsindikator.hide();
        Ext.MessageBox.alert('Problem:', 'Mottok blank respons fra tjeneren. Action type=' + action.type + ', failure type=' + action.failureType);
    }
    else {
        try {
            var responsObjekt = Ext.JSON.decode(action.response.responseText);
            msg = (typeof responsObjekt.msg === 'string') ? responsObjekt.msg : 'Klarte ikke lagre';
        } catch (e) {
            msg = action.response.responseText;
        }
        if (msg) {
            framdriftsindikator.hide();
            Ext.MessageBox.alert('Mislyktes', msg);
        }
    }
}


/**
 * @type {Ext.window.MessageBox}
 */
var framdriftsindikator = Ext.create('Ext.window.MessageBox', {
    renderTo: Ext.getBody(),
    width: 300
});

/**
 * @type {Ext.form.Panel}
 */
var skjema = <?php echo $skjema;?>;

<?php if ($utskriftAlleredeIgangsatt):?>
leiebasen.bekreftEksisterendeUtskrift();
<?php endif;?>
