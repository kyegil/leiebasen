<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\leieberegning_skjema\Index $this
 * @var integer $leieberegningId
 * @var string $spesialregelMal
 * @var string $spesialregler
 */
?>

/***
 * @type {jQuery} $
 */
leiebasen.drift.leieberegningSkjema = leiebasen.drift.leieberegningSkjema || {
    _initialised: false,
    regelTeller: 0,
    felter: {},
    init: function() {
        if(this._initialised) {
            return this;
        }
        /**
         * @type {*|jQuery}
         */
        this.felter.spesialregelBeholder = $(".leieberegning.spesialregler.beholder");
        this.spesialregler.forEach(function(regel) {
            let prosessor = regel.prosessor;
            let regelFeltsett = this.nySpesialRegel();
            regelFeltsett.find('input[name$="[forklaring]"]').val(regel.forklaring);
            regelFeltsett.find('input[name$="[sats]"]').val(regel.sats);
            regelFeltsett.find('input[name$="[unntatt_fra_leiejustering]"]').prop('checked', regel.unntattFraLeiejustering);
            regelFeltsett.find('select[name$="[prosessor]"]').val(prosessor);
            regelFeltsett.find('textarea[name$="[param]"]').val(JSON.stringify(regel.param, null, 2));
            this.leggTilSpesialRegel(regelFeltsett);
            regelFeltsett.find('.collapsible-section-button').trigger('click');
        }.bind(this));
        this._initialised = true;
        return this;
    },
    /**
     * @returns {jQuery}
     */
    nySpesialRegel: function() {
        let mal = this.spesialregelMal.replaceAll(
            'leieberegning-spesialregler-mal',
            'leieberegning-spesialregler-' + this.regelTeller
        ).replaceAll(
            '{#}',
            this.regelTeller
        );
        this.regelTeller++;
        let regelFeltsett = $(mal);
        regelFeltsett.find('.collapsible-section-button').on('click', regelFeltsett, function(clickEvent) {
            leiebasen.drift.leieberegningSkjema.oppdaterSeksjon(clickEvent.data);
        });
        return regelFeltsett;
    },
    /**
     * @param {jQuery} mal
     */
    leggTilSpesialRegel: function(mal) {
        mal = mal ? mal : this.nySpesialRegel();
        this.felter.spesialregelBeholder.find('.ny.button').before(mal);
        this.oppdaterSorteringsorden();
        return this;
    },
    /**
     * @param {jQuery} regelFeltsett
     */
    oppdaterSeksjon: function(regelFeltsett) {
        let forklaring = regelFeltsett.find('input[name$="[forklaring]"]').val();
        let sats = regelFeltsett.find('input[name$="[sats]"]').val();
        forklaring += ' (kr ' + sats + ' per år)';
        regelFeltsett.find('.collapsible-section-button').html(forklaring);
        return this;
    },
    /**
     * @returns {leiebasen.drift.leieberegningSkjema}
     */
    oppdaterSorteringsorden: function () {
        let sorteringsorden = 0;
        this.felter.spesialregelBeholder.find('input[name$="[sorteringsorden]"]').each( function() {
            $(this).val(++sorteringsorden);
        });
        return this;
    },
    /**
     *
     * @param {number} index
     * @returns {leiebasen.drift.leieberegningSkjema}
     */
    forkastRegel: function (index) {
        if(typeof index != 'undefined') {
            /**
             * @type {jQuery}
             */
            let regelFeltsett = this.felter.spesialregelBeholder.find('.spesialregler input[name$="[' + index + '][sats]"]').parents().eq(2);
            regelFeltsett.remove();
        }
        return this;
    },
    spesialregelDragElement: null,
    /**
     * @param {DragEvent} dragEvent
     */
    spesialregelOnDragStart: function(dragEvent) {
        dragEvent.dataTransfer.effectAllowed = "move";
        dragEvent.dataTransfer.setData("text/plain", null);
        this.spesialregelDragElement = $(dragEvent.target);
    },
    /**
     * @param {DragEvent} dragEvent
     */
    spesialregelOnDragOver: function(dragEvent) {
        let flyttetElement = this.spesialregelDragElement;
        let målElement = $(dragEvent.target.parentNode);
        let originalPosisjon = flyttetElement.find('input[name$="[sorteringsorden]"]').val();
        let målPosisjon = målElement.find('input[name$="[sorteringsorden]"]').val();
        if(flyttetElement.parent()[0] === målElement.parent()[0] && originalPosisjon !== målPosisjon) {
            if(originalPosisjon > målPosisjon) {
                målElement.before(flyttetElement);
            }
            else {
                målElement.after(flyttetElement);
            }
        }
    },
    spesialregelOnDragEnd: function() {
        this.oppdaterSorteringsorden();
    },
    slettLeieberegning: function() {
        /**
         * @type {Number} leieberegningId
         */
        let leieberegningId = <?php echo (int)$leieberegningId;?>;
        if (confirm('Er du sikker på at du vil slette denne leieberegningsmodellen?')) {
            $.ajax({
                url: '/drift/index.php?oppslag=leieberegning_skjema&id=' + leieberegningId + '&oppdrag=oppgave&oppgave=slett_leieberegning',
                data: {leieberegning_id: leieberegningId},
                method: "POST",
                dataType: 'json',
                success: function(data) {
                    if (data.msg) {
                        alert(data.msg);
                    }
                    if(data.url) {
                        location.href = data.url;
                    }
                }
            });
        }
    }
};

/**
 * @type {string}
 */
leiebasen.drift.leieberegningSkjema.spesialregelMal = <?php echo $spesialregelMal;?>;

/**
 * @type {Object[]}
 */
leiebasen.drift.leieberegningSkjema.spesialregler = <?php echo $spesialregler;?>;

leiebasen.drift.leieberegningSkjema.init();
