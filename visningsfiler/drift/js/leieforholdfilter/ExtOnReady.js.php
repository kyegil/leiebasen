<?php
/**
 * Leiebasen Visningsmal
 *
 * @var ExtOnReady $this
 * @var Meny|string $meny
 * @var string $tilbakeUrl
 * @var Common $inkludertFilter
 * @var Common $ekskludertFilter
 * @var Common $leieobjektFilter
 * @var Common $bygningsFilter
 * @var Common $områdeFilter
 * @var Common $leiebeløpMinFilter
 * @var Common $leiebeløpMaksFilter
 * @var Common $fradatoMinFilter
 * @var Common $fradatoMaksFilter
 * @var Common $sistFornyetMinFilter
 * @var Common $sistFornyetMaksFilter
 * @var Common $tildatoMinFilter
 * @var Common $tildatoMaksFilter
 * @var Common $sisteLeiejusteringMinFilter
 * @var Common $sisteLeiejusteringMaksFilter
 * @var Common $oppsigelighetsFilter
 * @var Common $kunAktiveFilter
 * @var Common $resultatSkjema
 */
use Kyegil\Leiebasen\Visning\drift\js\leieforholdfilter\ExtOnReady;use Kyegil\Leiebasen\Visning\drift\js\shared\extjs4\Meny;use Kyegil\Leiebasen\Visning\felles\extjs4\Common;
?>

leiebasen.selection = [];

leiebasen.filtrerOgLast = function(){
    this.selection = rutenett.getSelectionModel().getSelection();
    datasett.getProxy().extraParams = filterForm.getValues();
    datasett.getProxy().extraParams['filter[søkefelt]'] = søkefelt.getValue();
    datasett.getProxy().extraParams['filter[aktive]'] = kunAktiveFilter.getSubmitValue();
    filterKnapp.disable();
    datasett.load({
        params: {
            start: 0,
            limit: 300
        },
        callback: function(records, operation, success) {
            if (success) {
                filterKnapp.enable();
                filtervindu.hide();
            }
            else {
                var jsonData = datasett.getProxy().getReader().jsonData;
                if (typeof jsonData == 'undefined') {
                    Ext.MessageBox.alert('Problem:', 'Klarte ikke laste denne tabellen av ukjent grunn.');
                }
                else {
                    var msg = jsonData.msg;
                    msg = msg.replace(/\r\n|\r|\n/g, '<br>');
                    Ext.MessageBox.alert('Klarte ikke laste data:', msg);
                }
            }
        }
    });
}

Ext.define('leiebasen.Leieforhold', {
    extend: 'Ext.data.Model',
    idProperty: 'leieforhold',
    fields: [ // http://docs.sencha.com/extjs/4.2.2/#!/api/Ext.data.Field
        {name: 'leieforhold', type: 'int'},
        {name: 'beskrivelse', type: 'string'},
        {name: 'leieobjektbeskrivelse', type: 'string'},
        {name: 'leieobjekt', type: 'int'},
        {name: 'fast_kid', type: 'string'},
        {name: 'kontrakt', type: 'int'},
        {name: 'leiebeløp', type: 'float'},
        {name: 'fradato', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'fornyet', type: 'date', dateFormat: 'Y-m-d', allowNull: true},
        {name: 'siste_leiejustering', type: 'date', dateFormat: 'Y-m-d', allowNull: true},
        {name: 'oppsagt', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'tildato', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'frosset', type: 'bool'},
        {name: 'kontrakter', type: 'string'}
    ]
});

/**
 * @type {Ext.grid.plugin.CellEditing}
 */
var cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
    clicksToEdit: 1
});

/**
 * @type {Ext.grid.plugin.RowEditing}
 */
var rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
    autoCancel: false,
    listeners: {
        beforeedit: function (grid, e) {
            return e.column.xtype !== 'actioncolumn';
        }
    }
});

/**
 * @type {Ext.data.Store}
 */
var datasett = Ext.create('Ext.data.Store', {
    model: 'leiebasen.Leieforhold',
    pageSize: 300,
    remoteSort: true,
    proxy: {
        type: 'ajax',
        simpleSortMode: true,
        url: "/drift/index.php?oppslag=leieforholdfilter&oppdrag=hent_data",
        timeout: 300000, // 5 mins
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalRows'
        },
        listeners: {
            exception: function(proxy, response) {
                let responsStreng = response.responseText;
                let tilbakemelding = Ext.JSON.decode(responsStreng, true);
                if (response.status !== 200 || !tilbakemelding) {
                    Ext.MessageBox.alert('Ugyldig respons', responsStreng);
                }
                else if (!tilbakemelding.success && tilbakemelding.msg) {
                    Ext.MessageBox.alert('Hm', tilbakemelding.msg);
                }
            }
        }
    },
    sorters: [{
        property: 'leieforhold',
        direction: 'DESC'
    }]
});

/**
 * @type {Ext.form.field.Text}
 */
var søkefelt = Ext.create('Ext.form.field.Text', {
    emptyText: 'Søk og klikk ↵',
    name: 'filter[søkefelt]',
    width: 200,
    listeners: {
        specialkey: leiebasen.filtrerOgLast
    }
});

/**
 * @type {Ext.form.FieldSet}
 */
var inkludertFilter = <?php echo $inkludertFilter;?>;

/**
 * @type {Ext.form.FieldSet}
 */
var ekskludertFilter = <?php echo $ekskludertFilter;?>;

/**
 * @type {Ext.form.FieldSet}
 */
var bygningsFilter = <?php echo $leieobjektFilter;?>;

/**
 * @type {Ext.form.FieldSet}
 */
var områdeFilter = <?php echo $områdeFilter;?>;

/**
 * @type {Ext.form.FieldSet}
 */
var leieobjektFilter = <?php echo $bygningsFilter;?>;

/**
 * @type {Ext.form.field.Number}
 */
var leiebeløpMinFilter = <?php echo $leiebeløpMinFilter;?>;

/**
 * @type {Ext.form.field.Number}
 */
var leiebeløpMaksFilter = <?php echo $leiebeløpMaksFilter;?>;

/**
 * @type {Ext.form.field.Date}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Date
 */
var fradatoMinFilter = <?php echo $fradatoMinFilter;?>;

/**
 * @type {Ext.form.field.Date}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Date
 */
var fradatoMaksFilter = <?php echo $fradatoMaksFilter;?>;

/**
 * @type {Ext.form.field.Date}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Date
 */
var sistFornyetMinFilter = <?php echo $sistFornyetMinFilter;?>;

/**
 * @type {Ext.form.field.Date}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Date
 */
var sistFornyetMaksFilter = <?php echo $sistFornyetMaksFilter;?>;

/**
 * @type {Ext.form.field.Date}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Date
 */
var tildatoMinFilter = <?php echo $tildatoMinFilter;?>;

/**
 * @type {Ext.form.field.Date}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Date
 */
var tildatoMaksFilter = <?php echo $tildatoMaksFilter;?>;

/**
 * @type {Ext.form.field.Date}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Date
 */
var sisteLeiejusteringMinFilter = <?php echo $sisteLeiejusteringMinFilter;?>;

/**
 * @type {Ext.form.field.Date}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.form.field.Date
 */
var sisteLeiejusteringMaksFilter = <?php echo $sisteLeiejusteringMaksFilter;?>;

/**
 * @type {Ext.form.CheckboxGroup}
 */
var tidsbegrensingsFilter = <?php echo $oppsigelighetsFilter;?>;

/**
 * @type {Ext.form.field.Checkbox}
 */
var kunAktiveFilter = <?php echo $kunAktiveFilter;?>;

/**
 * @type {Ext.button.Button}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.button.Button
 */
var filterKnapp = Ext.create('Ext.button.Button', {
    text: 'Filtrer',
    handler: function() {
        leiebasen.filtrerOgLast();
    }
});

/**
 * @type {Ext.form.Panel}
 */
var filterForm = Ext.create('Ext.form.Panel', {
    bodyStyle: 'padding: 15px',
    layout: 'form',
    autoScroll: true,
    items: [
        inkludertFilter,
        ekskludertFilter,
        leieobjektFilter,
        bygningsFilter,
        områdeFilter,
        {
            xtype: 'fieldcontainer',
            layout: 'hbox',
            width: '100%',
            defaults: {
                labelAlign: 'top',
                flex: 1
            },
            items: [leiebeløpMinFilter, leiebeløpMaksFilter]
        },
        {
            xtype: 'fieldcontainer',
            layout: 'hbox',
            defaults: {
                labelAlign: 'right'
            },
            items: [fradatoMinFilter, fradatoMaksFilter]
        },
        {
            xtype: 'fieldcontainer',
            layout: 'hbox',
            defaults: {
                labelAlign: 'right'
            },
            items: [sistFornyetMinFilter, sistFornyetMaksFilter]
        },
        {
            xtype: 'fieldcontainer',
            layout: 'hbox',
            defaults: {
                labelAlign: 'right'
            },
            items: [tildatoMinFilter, tildatoMaksFilter]
        },
        //{
        //    xtype: 'fieldcontainer',
        //    layout: 'hbox',
        //    defaults: {
        //        labelAlign: 'right'
        //    },
        //    items: [sisteLeiejusteringMinFilter, sisteLeiejusteringMaksFilter]
        //},
        tidsbegrensingsFilter
    ],

    buttons: [filterKnapp]
});

/**
 * @type {Ext.window.Window}
 * @link https://docs.sencha.com/extjs/4.2.1/#!/api/Ext.window.Window
 */
var filtervindu = Ext.create('Ext.window.Window', {
    title: 'Filter',
    layout: 'fit',
    height: '90%',
    width: '80%',
    autoScroll: true,
    closeAction: 'hide',
    items: [filterForm]
});

var leieforhold = {
    id: 'leieforhold',
    dataIndex: 'leieforhold',
    header: 'Leieforhold',
    align: 'right',
    renderer: function(value, metaData, record, rowIndex, colIndex, store){
        return '<a title="Klikk for å se siste leieavtale i dette leieforholdet." href="/drift/index.php?oppslag=leieforholdkort&id=' + record.data.leieforhold + '">' + value + '</a>';
    },
    hidden: false,
    sortable: true,
    width: 50
};

var beskrivelse = {
    id: 'beskrivelse',
    dataIndex: 'beskrivelse',
    header: 'Beskrivelse',
    renderer: function(value, metaData, record, rowIndex, colIndex, store){
        return '<a ' + (record.data.oppsagt ? 'style="text-decoration: line-through;" ' : '') + 'title="Klikk for å se siste leieavtale i dette leieforholdet." href="/drift/index.php?oppslag=leieforholdkort&id=' + record.data.leieforhold + '">' + value + '</a>';
    },
    hidden: false,
    sortable: false,
    width: 50,
    flex: 1
};

var leieobjekt = {
    id: 'leieobjekt',
    dataIndex: 'leieobjekt',
    header: 'Leil',
    hidden: false,
    align: 'right',
    renderer: function(value, metaData, record, rowIndex, colIndex, store){
        return '<a title="Klikk for å gå til leieobjektkortet." href="/drift/index.php?oppslag=leieobjekt_kort&id=' + value + '">' + value + '</a>';
    },
    sortable: true,
    width: 40
};

var leiebeløp = {
    align: 'right',
    dataIndex: 'leiebeløp',
    header: 'Leiebeløp',
    renderer: Ext.util.Format.noMoney,
    sortable: true,
    width: 70
};

var fastKid = {
    id: 'fast_kid',
    dataIndex: 'fast_kid',
    header: 'KID',
    hidden: false,
    renderer: function(value, metaData, record, rowIndex, colIndex, store){
        return '<div title="Fast KIDnummer for innbetalinger til dette leieforholdet.">' + value + '</div>';
    },
    sortable: true,
    width: 90
};

var fradato = {
    id: 'fradato',
    itemId: 'fradato',
    dataIndex: 'fradato',
    header: 'Påbegynt',
    renderer: Ext.util.Format.dateRenderer('d.m.Y'),
    hidden: false,
    sortable: true,
    width: 90
};

var fornyet = {
    id: 'fornyet',
    dataIndex: 'fornyet',
    header: 'Fornyet',
    renderer: Ext.util.Format.dateRenderer('d.m.Y'),
    sortable: true,
    width: 90
};

var leiejustering = {
    id: 'leiejustering',
    dataIndex: 'siste_leiejustering',
    header: 'Leiejustering',
    renderer: Ext.util.Format.dateRenderer('d.m.Y'),
    sortable: false,
    width: 90
};

var tildato = {
    id: 'tildato',
    dataIndex: 'tildato',
    header: 'Utløper',
    renderer: function(value, metaData, record, rowIndex, colIndex, store){
        return '<span ' + (record.data.oppsagt ? 'style="text-decoration: line-through;" title="Utløpsdatoen er overstyrt av at leieforholdet er avsluttet"' : 'title="Datoen da leieforholdet utløper og må fornyes eller avsluttes"') + '>' + Ext.util.Format.date(value, 'd.m.Y') + '</span>';
    },
    sortable: true,
    width: 90
};

var oppsagt = {
    id: 'oppsagt',
    dataIndex: 'oppsagt',
    header: 'Avsluttet',
    renderer: function(value, metaData, record, rowIndex, colIndex, store){
        if(value){
            return '<div title="Siste bo-dag.">' + Ext.util.Format.date(value, 'd.m.Y') + '</div>';
        }
    },
    sortable: true,
    width: 70
};

var frosset = {
    dataIndex: 'frosset',
    header: 'Frosset',
    renderer: function(value, metaData, record, rowIndex, colIndex, store){
        if(value){
            return "<div title=\"Leieavtalen er frosset, og ignoreres av leiebasen\">" + Ext.util.Format.hake(value) + "</div> ";
        }
    },
    sortable: true,
    width: 30
};

/**
 * @type {Ext.form.Panel}
 */
var resultatSkjema = <?php echo $resultatSkjema;?>;

/**
 * @type {Ext.grid.Panel}
 */
var rutenett = Ext.create('Ext.grid.Panel', {
    title: 'Leieforhold',
    autoScroll: true,
    layout: 'border',
    renderTo: 'panel',
    height: '100%',
    store: datasett,
    tbar: [
        søkefelt,
        {
            text: 'avansert filter',
            border: 1,
            style: {
                borderColor: '#bbb',
                borderStyle: 'solid'
            },
            xtype: 'button',
            handler: function () {
                filtervindu.show();
            }
        },
        kunAktiveFilter
    ],
    selType: 'checkboxmodel',

    columns: [
        leieforhold,
        beskrivelse,
        leieobjekt,
        leiebeløp,
        fastKid,
        fradato,
        fornyet,
        leiejustering,
        tildato,
        oppsagt,
        frosset
    ],

    dockedItems: [{
        xtype: 'pagingtoolbar',
        store: datasett,
        dock: 'bottom',
        listeners: {
            beforechange: function(pagingtoolbar, page, eOpts) {
                leiebasen.selection = rutenett.getSelectionModel().getSelection();
            },
            change: function(pagingtoolbar, pageData, eOpts) {
                rutenett.getSelectionModel().select(leiebasen.selection, true);
            }
        },
        displayInfo: true
    }],

    plugins: [{
        ptype: 'rowexpander',
        rowBodyTpl : new Ext.XTemplate('{kontrakter}')
    }],

    buttons: [{
        text: 'Velg ingen',
        handler: function() {
            leiebasen.selection = [];
            rutenett.getSelectionModel().select([]);
        }
    }, {
        text: 'Fortsett',
        handler: function() {
            var leieforhold = [];
            leiebasen.selection = rutenett.getSelectionModel().getSelection();
            leiebasen.selection.forEach(function(element, index, array){
                leieforhold.push(element.get('leieforhold'));
            })

            resultatSkjema.getForm().setValues({
                leieforhold: JSON.stringify(leieforhold)
            });

            resultatSkjema.submit();
        }
    }]
});

leiebasen.filtrerOgLast();