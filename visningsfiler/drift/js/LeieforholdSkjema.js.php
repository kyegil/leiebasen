<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\drift\js\LeieforholdSkjema $this
 * @var int|string $leieforholdId
 * @var string $ledigeLeieobjekter
 * @var string $skript
 */
use Kyegil\Leiebasen\Oppslag\DriftKontroller;
?>

leiebasen.drift.leieforholdSkjema = leiebasen.drift.leieforholdSkjema || {
    initier: function() {
        if(typeof this._initiert !== 'undefined') {
            return this;
        }
        $('.overlay').addClass('initialiserer');
        this.ledigeLeieobjekter = [];
        this.minDatoer = null;
        this.bofellesskap = null;
        this.adresseSøkSubjekt = null;
        this.basisbeløp = null;
        this.felter = {
            idFelt: $("[name='id']"),
            leieobjektFelt: $("[name='leieobjekt']"),
            fradatoFelt: $("[name='fradato']"),
            fornyFelt: $("[name='fornyelse']"),
            virkeDatoFelt: $("[name='virkedato']"),
            tidsbegrenset: $("#tidsbegrenset-leieforhold"),
            tildatoFelt: $("[name='tildato']"),
            oppsigelsestidFelt: $("[name='oppsigelsestid']"),
            antTerminerFelt: $("[name='ant_terminer']"),
            fastDatoFelt: $("[name='fast_dato']"),
            dagIMånedenFelt: $("[name='dag_i_måneden']"),
            ukedagFelt: $("[name='ukedag']"),

            andelFelter: {
                bofellesskap: $("*[type='checkbox'][name='bofellesskap']"),
                teller: $("[name='andel[teller]']"),
                nevner: $("[name='andel[nevner]']"),
                likeDeler: $("#bofellesskap-like-deler")
            },

            leiebeløpFelt: $("[name='leiebeløp']"),
            foreslåttLeiebeløpFelt: $("[name='foreslått_leiebeløp']"),
            nåværendeLeiebeløpFelt: $("[name='nåværende_leiebeløp']"),

            adressekortVelgerFelt: $("#adressekort-velger")
        };
        this.eksisterendeLeieforhold = this.felter.idFelt.val() > 0;
        this.originalVerdier = {
            leieobjekt: this.felter.leieobjektFelt.val(),
            fradato: this.felter.fradatoFelt.val(),
            tidsbegrenset: this.felter.tidsbegrenset.is(':checked'),
            tildato: this.felter.tildatoFelt.val(),
            oppsigelsestid: this.felter.oppsigelsestidFelt.val(),
            antTerminer: this.felter.antTerminerFelt.val(),
            bofellesskap: this.felter.andelFelter.bofellesskap.is(':checked'),
            likeDeler: this.felter.andelFelter.teller.is(':checked'),
            teller: this.felter.andelFelter.teller.val(),
            nevner: this.felter.andelFelter.nevner.val()
        }

        /**
         * @link https://select2.org
         */
        this.felter.leieobjektFelt.select2({
            data: this.ledigeLeieobjekter,
            placeholder: 'Angi leieobjekt'
        });

        this.felter.tildatoFelt.parent().addClass('collapse');
        this.felter.virkeDatoFelt.parent().addClass('collapse');
        this.felter.andelFelter.teller.parent().addClass('collapse');

        this.felter.leieobjektFelt.on('change', function() {
            leiebasen.drift.leieforholdSkjema.oppdaterMinDato();
            leiebasen.drift.leieforholdSkjema.oppdaterAndeler();
            leiebasen.drift.leieforholdSkjema.hentLeieforslag();
        });

        this.felter.fradatoFelt.on('change', function() {
            leiebasen.drift.leieforholdSkjema.hentLedighet();
        });

        this.felter.virkeDatoFelt.on('change', function() {
            leiebasen.drift.leieforholdSkjema.felter.tildatoFelt.val(null);
            leiebasen.drift.leieforholdSkjema.oppdaterSkjema();
        });

        this.felter.tidsbegrenset.on('change', function() {
            leiebasen.drift.leieforholdSkjema.oppdaterSkjema();
        });

        this.felter.fornyFelt.on('change', function() {
            leiebasen.drift.leieforholdSkjema.oppdaterSkjema();
        });

        this.felter.tildatoFelt.on('change', function() {
            leiebasen.drift.leieforholdSkjema.bekreftTildato();
        });

        this.felter.andelFelter.nevner.on('change', function() {
            leiebasen.drift.leieforholdSkjema.validerAndeler();
            leiebasen.drift.leieforholdSkjema.hentLeieforslag();
        });

        this.felter.andelFelter.bofellesskap.on('change', function() {
            leiebasen.drift.leieforholdSkjema.oppdaterAndelFelter();
            leiebasen.drift.leieforholdSkjema.hentLeieforslag();
        });

        this.felter.andelFelter.likeDeler.on('change', function() {
            leiebasen.drift.leieforholdSkjema.oppdaterAndelFelter();
        });

        this.felter.antTerminerFelt.on('change', function() {
            leiebasen.drift.leieforholdSkjema.oppdaterTerminFelter();
            leiebasen.drift.leieforholdSkjema.oppdaterLeiePerTermin();
        });

        $("[name$='[er_org]']").on('change', function() {
            leiebasen.drift.leieforholdSkjema.oppdaterLeietakerNavnFelter($( this ).closest(".leietaker-feltsett"));
        });

        $("[name^='leietaker'][name$='[etternavn]']").on('change', function(){
            leiebasen.drift.leieforholdSkjema.oppdaterLeietakerNavnFelter($( this ).closest(".leietaker-feltsett"));
        });

        $("[name^='leietaker']").on('invalid', function(){
            $( this ).closest(".collapsible-section").find(".collapse").show();
        });

        $("[name^='leietaker'][name$='[id]']").on('invalid', function(invalidEvent){
            invalidEvent.preventDefault();
            let fornavn = $(this).closest(".leietaker-feltsett").find("[name^='leietaker'][name$='[fornavn]']").val();
            let etternavn = $(this).closest(".leietaker-feltsett").find("[name^='leietaker'][name$='[etternavn]']").val();
            if(etternavn) {
                alert('Angi adressekort for ' + fornavn + ' ' + etternavn);
            }
        });

        $(".fjern-leietaker-knapp").on('click', function(){
            leiebasen.drift.leieforholdSkjema.fjernLeietaker($( this ).closest(".leietaker-feltsett"));
        });

        $(".bruk-foreslått-leiebeløp").on('click', function () {
            leiebasen.drift.leieforholdSkjema.felter.leiebeløpFelt.val(
                leiebasen.drift.leieforholdSkjema.felter.foreslåttLeiebeløpFelt.val()
            );
            leiebasen.drift.leieforholdSkjema.oppdaterLeiePerTermin();
        });

        $(".bruk-nåværende-leiebeløp").on('click', function () {
            leiebasen.drift.leieforholdSkjema.felter.leiebeløpFelt.val(
                leiebasen.drift.leieforholdSkjema.felter.nåværendeLeiebeløpFelt.val()
            );
            leiebasen.drift.leieforholdSkjema.oppdaterLeiePerTermin();
        });

        $(".leietaker-feltsett").each( function() {
            leiebasen.drift.leieforholdSkjema.oppdaterLeietakerNavnFelter($( this ));
        });

        $("input[name='leiebeløp']").on('change', function () {
            leiebasen.drift.leieforholdSkjema.oppdaterLeiePerTermin();
        });

        $("input[name$='[sats]']").on('change', function () {
            leiebasen.drift.leieforholdSkjema.oppdaterLeiePerTermin();
        });

        $(".adresse-søk-knapp").on('click', function() {
            let adressefeltsett = $( this ).closest(".leietaker-feltsett");
            leiebasen.drift.leieforholdSkjema.adresseSøkSubjekt = adressefeltsett;
            let erOrg = adressefeltsett.find("[name$='[er_org]']").is(':checked');
            let fornavn = !erOrg ? adressefeltsett.find("[name$='[fornavn]']").val() : null;
            let etternavn = adressefeltsett.find("[name$='[etternavn]']").val();

            let søk = [etternavn];
            if (!erOrg) {
                søk.unshift(fornavn);
            }
            søk = søk.filter(Boolean).join(' ');
            $('.overlay').addClass('laster');
            $.ajax("<?php echo DriftKontroller::url('leieforhold_skjema', $leieforholdId, 'utlevering');?>", {
                method: "GET",
                dataType: 'json',
                timeout: 300000,
                data: {
                    data: 'adressekort',
                    term: søk
                },
                success: function (data) {
                    data.data = data.data ?? [];
                    $('#adressekort-modal .modal-body #adressekort-velger').empty();

                    let tomtKort = $(
                        '<div class="kontakt">' +
                        "<label><input type='radio' class='adressekort-radio mr-2' name='valgt-adressekort' value='*'><span>Registrer som ny kontakt</span></label>" +
                        "<input type='hidden' class='valgt-er-org' value='" + "'>" +
                        "<input type='hidden' class='valgt-fornavn' value='" + "'>" +
                        "<input type='hidden' class='valgt-etternavn' value='" + "'>" +
                        '</div>'
                    );

                    data.data.forEach((resultat) => {
                        let container = tomtKort.clone();
                        container.find('input.adressekort-radio').parent().find('span').text(resultat.text);
                        container.find('input.adressekort-radio').val(resultat.id);
                        container.find('input.valgt-er-org').val(resultat.erOrg ? '1' : '0');
                        container.find('input.valgt-fornavn').val(resultat.fornavn);
                        container.find('input.valgt-etternavn').val(resultat.etternavn);
                        container.appendTo('#adressekort-velger');
                    });
                    tomtKort.appendTo('#adressekort-velger');

                    $('#adressekort-velger input.adressekort-radio').on('change', (event) => {
                        let $valgtAdressekort = $(event.target);
                        let valgtAdressekortId = $valgtAdressekort.val()
                        let adresseSøkSubjekt = leiebasen.drift.leieforholdSkjema.adresseSøkSubjekt;
                        adresseSøkSubjekt.find("input[name$='[id]']").val(valgtAdressekortId);
                        if(valgtAdressekortId !== '*') {
                            adresseSøkSubjekt.find("input[name$='[er_org]']").prop('checked', +$valgtAdressekort.closest(".kontakt").find('input.valgt-er-org').first().val());
                            adresseSøkSubjekt.find("input[name$='[fornavn]']").val($valgtAdressekort.closest(".kontakt").find('input.valgt-fornavn').first().val());
                            adresseSøkSubjekt.find("input[name$='[etternavn]']").val($valgtAdressekort.closest(".kontakt").find('input.valgt-etternavn').first().val());
                        }
                        $('#adressekort-modal').modal('hide');
                        leiebasen.drift.leieforholdSkjema.oppdaterLeietakerNavnFelter(leiebasen.drift.leieforholdSkjema.adresseSøkSubjekt);
                    });

                    $('.overlay').removeClass('laster');
                    $('#adressekort-modal').modal('show');
                }
            });
        });

        $(".ny-leietaker-knapp").on('click', function (){
            leiebasen.drift.leieforholdSkjema.klonLeietakerfelt();
        });

        this._initiert = true;

        this.oppdaterSkjema();
        this.hentLedighet()
    },

    /**
     * Oppdater skjemaet ved å vise eller skjule betingede felter
     */
    oppdaterSkjema: function() {
        if(typeof this._initiert === 'undefined') {
            this.initier();
        }

        let fornying = this.felter.fornyFelt.is(':checked');

        let tidsbegrenset = this.felter.tidsbegrenset.is(':checked');
        if(!tidsbegrenset) {
            this.felter.tildatoFelt.val(null);
            this.felter.tildatoFelt.val(null);
        }
        this.felter.tildatoFelt.prop('disabled', !tidsbegrenset);
        this.felter.tildatoFelt.parent().toggleClass('show', tidsbegrenset);

        if(this.felter.fradatoFelt.val()) {
            this.felter.tildatoFelt.prop('min', this.felter.fradatoFelt.val());
            if(fornying && this.felter.virkeDatoFelt.val()) {
                this.felter.tildatoFelt.prop('min', this.felter.virkeDatoFelt.val());
            }
        }

        this.felter.virkeDatoFelt.prop('disabled', !fornying);
        this.felter.virkeDatoFelt.parent().toggleClass('show', fornying);

        this.oppdaterAndelFelter();
        this.oppdaterTerminFelter();
    },

    /**
     * Oppdater andel-feltene med hensyn til om leieforholdet inngår i et bofellesskap eller ikke.
     */
    oppdaterAndelFelter: function() {
        if(typeof this._initiert === 'undefined') {
            this.initier();
        }
        let bofellesskap = this.felter.andelFelter.bofellesskap.is(':checked');
        let andreLeieforholdFinnes = this.felter.andelFelter.nevner.prop('min') > 1;
        if(!bofellesskap && andreLeieforholdFinnes) {
            this.felter.andelFelter.bofellesskap.prop('checked', true);
            bofellesskap = true;
            this.felter.andelFelter.bofellesskap.parent().siblings('.collapse').toggleClass('show', bofellesskap);
        }
        if(!bofellesskap) {
            this.felter.andelFelter.teller.val(1);
            this.felter.andelFelter.nevner.val(1);
            this.felter.andelFelter.likeDeler.prop('checked', true);
        }
        let likeDeler = this.felter.andelFelter.likeDeler.is(':checked');
        if(likeDeler) {
            this.felter.andelFelter.teller.val(1);
        }
        $("label[for='" + this.felter.andelFelter.nevner.attr('id') + "']")
            .text(likeDeler ? 'Totalt antall leieforhold:' : 'Totalt antall deler som leieobjektet er delt i:');
        this.felter.andelFelter.teller.parent().toggleClass('show', !likeDeler);
    },

    /**
     * Oppdater skjemaets leietaker-navn-felter,
     * med hensyn til påkrevde felter for personer eller organisasjoner,
     * og sørg for at det blir forsøkt satt et adressekort.
     *
     * @param {jquery} $navnFeltsett
     */
    oppdaterLeietakerNavnFelter: function($navnFeltsett) {
        let erOrg = Boolean($navnFeltsett.find("[name$='[er_org]']:checked").length);
        /** @type {jQuery} */
        let $fornavnFelt = $navnFeltsett.find("[name$='[fornavn]']");
        /** @type {jQuery} */
        let $adressekortFelt = $navnFeltsett.find("[name$='[id]']");
        /** @type {jQuery} */
        let $etternavnFelt = $navnFeltsett.find("[name$='[etternavn]']");
        /** @type {jQuery} */
        let $adresseSøkKnapp = $navnFeltsett.find(".adresse-søk-knapp");
        let låstAdressekort = $adressekortFelt.val().length > 0 && $adressekortFelt.val() !== '*';
        if(erOrg) {
            $navnFeltsett.find("[name$='[fornavn]']").val(null);
        }
        $fornavnFelt.attr('required', !erOrg);
        $etternavnFelt.attr('required', true);
        $adressekortFelt.attr('required', true);
        $fornavnFelt.parent().toggle(!erOrg);
        $etternavnFelt.parent().removeClass(erOrg ? 'col-3' : 'col-6');
        $etternavnFelt.parent().addClass(erOrg ? 'col-6' : 'col-3');
        $etternavnFelt.prop('placeholder', erOrg ? 'Navn' : 'Etternavn');

        $navnFeltsett.find("[type='checkbox'][name$='[er_org]']").prop('disabled', låstAdressekort);
        $fornavnFelt.prop('disabled', låstAdressekort);
        $etternavnFelt.prop('disabled', låstAdressekort);

        $adresseSøkKnapp.toggleClass('disabled', !$etternavnFelt.val().length > 0 || $adressekortFelt.val().length > 0);
        $adressekortFelt.attr('readonly', $adressekortFelt.val().length > 0).toggle($adressekortFelt.val().length > 0);

        $("[type='checkbox'][name^='leietaker'][name$='[husstandmedlem]']").first().prop( "checked", false ).parent().hide();
    },

    /**
     * Oppdater felter for faste forfall basert på antall årlige leieterminer
     */
    oppdaterTerminFelter: function() {
        if(typeof this._initiert === 'undefined') {
            this.initier();
        }
        let antTerminer = this.felter.antTerminerFelt.val();
        let månedBasert = ['1','2','3','4','6','12'].indexOf(antTerminer) !== -1;
        let ukeBasert = ['1','2','4','13','26','52'].indexOf(antTerminer) !== -1;
        this.felter.ukedagFelt.prop('disabled', !ukeBasert);
        this.felter.dagIMånedenFelt.prop('disabled', !månedBasert);
    },

    /**
     * Oppdater foreslått årlig leiebeløp
     */
    oppdaterLeiefelter: function() {
        if(typeof this._initiert === 'undefined') {
            this.initier();
        }
        let basisbeløp = this.basisbeløp;
        let delbeløpSum = 0;
        if(!basisbeløp) {
            this.felter.foreslåttLeiebeløpFelt.val(null);
            this.hentLeieforslag();
        }
        else {
            $(".delbeløp [name^='delkrav'][name$='[sats]']").each(function () {
                let sats = Number($(this).val().replace(',', '.'));
                let relativ = Boolean($(this).parent().closest('fieldset').find("[name$='[relativ]']").val());
                let delbeløp = relativ ? (basisbeløp * sats / 100) : sats;
                delbeløpSum += delbeløp;
            });
            this.felter.foreslåttLeiebeløpFelt.val(basisbeløp + delbeløpSum);
        }
    },

    /**
     * Oppdater leiebeløp per termin
     */
    oppdaterLeiePerTermin: function() {
        let leiePerÅr = this.felter.leiebeløpFelt.val();
        leiePerÅr = leiePerÅr === '' ? null : Number(leiePerÅr);
        let antTerminer = this.felter.antTerminerFelt.val();
        let leiePerTermin = null;
        let totaltPerTermin = null;
        let tilleggSum = 0;
        $(".tillegg [name^='delkrav'][name$='[sats]']").each(function () {
            let sats = Number($(this).val().replace(',', '.'));
            let relativ = Boolean($(this).parent().closest('fieldset').find("[name$='[relativ]']").val());
            let delbeløp = relativ ? (leiePerÅr * sats / 100) : sats;
            tilleggSum += delbeløp;
        });

        if(leiePerÅr && antTerminer > 0) {
            leiePerTermin = leiebasen.kjerne.kr(Math.round(leiePerÅr / antTerminer), false);
            totaltPerTermin = leiebasen.kjerne.kr(Math.round((leiePerÅr + tilleggSum) / antTerminer), false);
        }

        $(".leie-per-termin input").val(leiePerTermin);
        $(".totalt-per-termin input").val(totaltPerTermin);
    },

    /**
     * Oppdater listen over ledige leieobjekter
     */
    oppdaterLeieobjektVelgerOptions: function () {
        let dato = this.hentForespurtDato();
        let selected = this.felter.leieobjektFelt.val();
        let leieobjekter = Object.values(this.ledigeLeieobjekter[dato] ?? {});
        leieobjekter.sort(function(a,b) {
            return a.disabled - b.disabled;
        });
        this.felter.leieobjektFelt.find('option').remove();
        leieobjekter.forEach(function (leieobjekt) {
            let tekst = leieobjekt.text;
            var option = new Option(tekst, leieobjekt.id);
            option.disabled = leieobjekt.disabled;
            leiebasen.drift.leieforholdSkjema.felter.leieobjektFelt.append(option);
        });
        this.felter.leieobjektFelt.val(selected).trigger('change');
    },

    /**
     * Oppdater tidligste mulige leiedato basert på når leieobjektet er tilgjengelig
     */
    oppdaterMinDato: function () {
        let leieobjektId = parseInt(this.felter.leieobjektFelt.val());
        let minDato = this.minDatoer[leieobjektId] ?? null;
        this.felter.fradatoFelt.attr('min', minDato);
        this.felter.tildatoFelt.attr('min', minDato);
    },

    /**
     * Sjekk hvor stor plass som er ledig i leieobjektet ved bofellesskap,
     * og oppdater andel-verdiene i henhold til dette
     */
    oppdaterAndeler: function () {
        let leieobjektId = parseInt(this.felter.leieobjektFelt.val());
        let bofellesskap = this.bofellesskap[leieobjektId] ?? [1,1];
        let nevnerFelt = this.felter.andelFelter.nevner;
        let tellerFelt = this.felter.andelFelter.teller;

        if(!this.eksisterendeLeieforhold) {
            tellerFelt.val(bofellesskap[0]);
            nevnerFelt.val(bofellesskap[1]);
        }

        this.felter.andelFelter.bofellesskap.prop('checked', bofellesskap[1] !== 1);
        this.felter.andelFelter.bofellesskap.parent().siblings('.collapse').toggleClass('show', bofellesskap[1] !== 1);
        this.oppdaterAndelFelter();
        this.validerAndeler();
    },

    /**
     * Oppdater min-og maks-verdi for andelene ved bofellesskap
     */
    validerAndeler: function () {
        let dato = this.hentForespurtDato();

        let leieobjektId = parseInt(this.felter.leieobjektFelt.val());
        let ledigeLeieobjekter = this.ledigeLeieobjekter[dato] ?? {};
        let leieobjektData = ledigeLeieobjekter[leieobjektId] ?? {};
        let tilgjengelighet = leieobjektData.tilgjengelighet ?? [1,1];
        let nevnerFelt = this.felter.andelFelter.nevner;
        let tellerFelt = this.felter.andelFelter.teller;

        let maksTeller = Math.floor(nevnerFelt.val() * tilgjengelighet[0] / tilgjengelighet[1]);
        let minNevner = Math.ceil(tilgjengelighet[1] / Math.max(tilgjengelighet[0], 1));
        maksTeller = Math.min(maksTeller, nevnerFelt.val());
        tellerFelt.attr('max', maksTeller);
        nevnerFelt.attr('min', minNevner);
        if(minNevner > 1) {
            this.felter.andelFelter.bofellesskap.prop('checked', true).prop('disabled', true);
        }
    },

    /**
     * Hent ledighet for et leieobjekt ved angitt dato
     */
    hentLedighet: function() {
        if(typeof this._initiert === 'undefined') {
            this.initier();
        }

        let dato = this.hentForespurtDato();

        let leieforholdId = this.felter.idFelt.val();
        let leieobjektId = this.felter.leieobjektFelt.val();
        let tilDato = this.felter.tildatoFelt.val();
        let innhold = [];
        if(!this.ledigeLeieobjekter || !this.ledigeLeieobjekter.length) {
            innhold.push('ledige_leieobjekter');
        }
        if(!this.minDatoer || !this.minDatoer.length) {
            innhold.push('minDatoer');
            innhold.push('minDatoer');
        }
        if(!this.bofellesskap || !this.bofellesskap.length) {
            innhold.push('bofellesskap');
        }

        if(!this.ledigeLeieobjekter[dato]) {
            $.ajax({
                url: "<?php echo DriftKontroller::url('leieforhold_skjema', $leieforholdId, 'utlevering');?>",
                method: "GET",
                dataType: 'json',
                timeout: 300000,
                data: {
                    data: 'leieobjektliste',
                    fradato: dato,
                    tildato: tilDato,
                    leieobjekt: leieforholdId > 0 ? leieobjektId : null,
                    innhold: innhold
                },
                success: function (data) {
                    if (data.msg) {
                        alert(data.msg);
                    }
                    /**
                     * @type {string}
                     */
                    let dato = data.fraDato;
                    leiebasen.drift.leieforholdSkjema.ledigeLeieobjekter[dato] = data.data;
                    if(data.minDatoer) {
                        leiebasen.drift.leieforholdSkjema.minDatoer = data.minDatoer;
                    }
                    if(data.bofellesskap) {
                        leiebasen.drift.leieforholdSkjema.bofellesskap = data.bofellesskap;
                    }
                    leiebasen.drift.leieforholdSkjema.oppdaterLeieobjektVelgerOptions();
                    $('.overlay').removeClass('initialiserer');
                }
            });
        }
        else {
            leiebasen.drift.leieforholdSkjema.oppdaterLeieobjektVelgerOptions();
        }
    },

    /**
     * Hent foreslått leiebeløp for ønsket andel i et gitt leieobjekt
     */
    hentLeieforslag: function() {
        if(typeof this._initiert === 'undefined') {
            this.initier();
        }

        this.basisbeløp = null;
        $(".bruk-foreslått-leiebeløp").addClass('disabled');

        let leieobjektId = this.felter.leieobjektFelt.val();
        if(!leieobjektId) {
            return;
        }
        let andel = [this.felter.andelFelter.teller.val(), this.felter.andelFelter.nevner.val()];

        $.ajax({
            url: "<?php echo DriftKontroller::url('leieforhold_skjema', $leieforholdId, 'utlevering');?>",
            method: "GET",
            dataType: 'json',
            timeout: 300000,
            data: {
                data: 'leieforslag',
                leieobjekt: leieobjektId,
                andel: andel
            },
            success: function (data) {
                if (data.msg) {
                    alert(data.msg);
                }
                if(data.success) {
                    if(data.data !== null) {
                        leiebasen.drift.leieforholdSkjema.basisbeløp = Number(data.data);
                        leiebasen.drift.leieforholdSkjema.oppdaterLeiefelter();
                        leiebasen.drift.leieforholdSkjema.oppdaterLeiePerTermin();
                        $(".bruk-foreslått-leiebeløp").removeClass('disabled');
                        $(".bruk-nåværende-leiebeløp").removeClass('disabled');
                    }
                }
            }
        });

    },

    /**
     * Dersom til-dato er satt som første dag i måneden,
     * vil du bli bedt om å bekrefte at du ikke mente siste dag i måneden
     */
    bekreftTildato: function() {
        let tildato = this.felter.tildatoFelt.val();
        if(tildato.substring(8, 10) === '01') {
            let korrigering = new Date(tildato);
            korrigering.setDate(korrigering.getDate() - 1);
            if(window.confirm('OBS! Datoene er inklusive! Vil du endre til-dato til ' + korrigering.toLocaleDateString('no-NB', {day: '2-digit', month: '2-digit', year: 'numeric'}) + ' for ikke å gå én dag inn i en ny måned?')) {
                this.felter.tildatoFelt.val(korrigering.toISOString().substring(0,10));
            }
        }
    },

    /**
     * Opprett nye leietakerfelter ved å klone fra malen
     */
    klonLeietakerfelt: function() {
        let feltsett = $('.leietaker-feltsett');
        let nyttFeltsett = $(feltsett[0]).clone(true, true);
        for (let i = 0; i < feltsett.length; i++) {
            $(feltsett[i]).find("[name$='[id]']").attr('name', 'leietakere[' + i + '][id]');
            $(feltsett[i]).find("[name$='[er_org]']").attr('name', 'leietakere[' + i + '][er_org]');
            $(feltsett[i]).find("[name$='[husstandmedlem]']").attr('name', 'leietakere[' + i + '][husstandmedlem]');
            $(feltsett[i]).find("[name$='[fornavn]']").attr('name', 'leietakere[' + i + '][fornavn]');
            $(feltsett[i]).find("[name$='[etternavn]']").attr('name', 'leietakere[' + i + '][etternavn]');
        }
        nyttFeltsett.find("[name$='[id]']").attr('name', 'leietakere[' + feltsett.length + '][id]').val(null);
        nyttFeltsett.find("[name$='[er_org]']").attr('name', 'leietakere[' + feltsett.length + '][er_org]');
        nyttFeltsett.find("[type='checkbox'][name$='[er_org]']").prop( "checked", false );
        nyttFeltsett.find("[name$='[husstandmedlem]']").attr('name', 'leietakere[' + feltsett.length + '][husstandmedlem]');
        nyttFeltsett.find("[type='checkbox'][name$='[husstandmedlem]']").prop( "checked", false ).parent().show();
        nyttFeltsett.find("[name$='[fornavn]']").attr('name', 'leietakere[' + feltsett.length + '][fornavn]').val(null);
        nyttFeltsett.find("[name$='[etternavn]']").attr('name', 'leietakere[' + feltsett.length + '][etternavn]').val(null);
        nyttFeltsett.insertAfter(feltsett.last());
        this.oppdaterLeietakerNavnFelter(nyttFeltsett);
    },

    /**
     * Fjern en leietaker
     *
     * @param {jquery} $navnFeltsett
     */
    fjernLeietaker: function($navnFeltsett) {
        let antallKontraktholdere = $("input[type='checkbox'][name$='[husstandmedlem]']:not(:checked)").length;
        let erKontraktholder = $navnFeltsett.find("input[type='checkbox'][name$='[husstandmedlem]']:is(:checked)").length === 0;
        if(antallKontraktholdere > (erKontraktholder ? 1 : 0)) {
            $navnFeltsett.remove();
        }
        else {
            alert('Leieavtalen må ha minst én avtaleansvarlig leietaker');
        }
    },

    /**
     * Enkelte endringer i eksisterende kontrakt krever at denne fornyes.
     *
     * @returns {boolean}
     */
    krevKontraktfornying: function() {
        return true;
    },

    /**
     * Hent datoen leieobjektet er forespurt ledig.
     * For nye leieforhold er dette fra-dato,
     * for eksisterende leieforhold avhenger dette av om leieforholdet fornyes eller ikke.
     *
     * @returns {string}
     */
    hentForespurtDato: function() {
        if(typeof this._initiert === 'undefined') {
            this.initier();
        }
        let fornying = this.felter.fornyFelt.is(':checked');

        let dato = this.felter.fradatoFelt.val()
            ? this.felter.fradatoFelt.val()
            : new Date().toISOString().substring(0, 10);
        if(this.eksisterendeLeieforhold) {
            if(fornying) {
                dato = this.felter.virkeDatoFelt.val()
                    ? this.felter.virkeDatoFelt.val()
                    : new Date().toISOString().substring(0, 10);
            }
            else {
                dato = new Date().toISOString().substring(0, 10);
            }
        }
        return dato;

    }
};

<?php echo $skript;?>

$('document').ready(function(){
    /* Forhindre at skjemaet sendes uforvarende ved enter-klikk */
    $(window).keydown(function(event){
        if(event.keyCode === 13) {
            event.preventDefault();
            return false;
        }
    });
    leiebasen.drift.leieforholdSkjema.initier();
});
