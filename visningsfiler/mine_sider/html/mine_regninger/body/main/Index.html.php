<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\mine_regninger\body\main\Index $this
 * @var \Kyegil\Leiebasen\Visning|string $utestående
 */
?><div id="ubetalte-regninger">
    <?php echo $utestående;?>
</div>