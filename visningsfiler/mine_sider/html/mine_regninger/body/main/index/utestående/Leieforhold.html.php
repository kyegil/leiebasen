<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\mine_regninger\body\main\index\utestående\Leieforhold $this
 * @var \Kyegil\Leiebasen\Visning|string $uteståendeRegninger
 * @var string $leieforholdId
 * @var string $leieforholdBeskrivelse
 * @var string $sumUtestående
 * @var string $visBetalingsplanKnapp
 * @var string $lagBetalingsplanKnapp
 * @var string $betalingutsettelseKnapp
 */
?>
<table class="rutenett">
    <caption><a title="Gå til leieforholdet" href="/mine-sider/index.php?oppslag=leieforhold&id=<?php echo $leieforholdId;?>">Leieforhold <?php echo $leieforholdId;?></a> – <?php echo $leieforholdBeskrivelse;?></caption>
    <thead>
    <tr>
        <th>Forfall</th>
        <th>Beskrivelse</th>
        <th>Regning</th>
        <th>Utestående saldo</th>
    </tr>
    </thead>
    <tbody>
    <?php echo $uteståendeRegninger;?>
    </tbody>
    <tfoot>
    <tr class="min-width450">
        <td colspan="3">Sum</td>
        <td class="beløp"><?php echo $sumUtestående;?></td>
    </tr>
    </tfoot>
</table>
<?php if(trim($betalingutsettelseKnapp)): echo $betalingutsettelseKnapp; endif;?>
<?php if(trim($visBetalingsplanKnapp)): echo $visBetalingsplanKnapp; endif;?>
<?php if(trim($lagBetalingsplanKnapp)): echo $lagBetalingsplanKnapp; endif;?>

