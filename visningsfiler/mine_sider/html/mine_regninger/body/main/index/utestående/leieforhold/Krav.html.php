<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\mine_regninger\body\main\index\utestående\leieforhold\Krav $this
 * @var string $tekst
 * @var string $regningsnr
 * @var string $forfallsdato
 * @var string $utestående
 */
?><tr>
    <td class="dato"><span class="Forfall"><span class="kolonnelabel">Forfall</span><?php echo $forfallsdato;?></td>
    <td><?php echo $tekst;?></td>
    <td><?php echo $regningsnr ? '<a title="Vis Regning" href="/mine-sider/index.php?oppslag=regning&id=' . $regningsnr . '">Vis Regning</a>' : "";?></td>
    <td class="beløp"><span class="kolonnelabel">Utestående</span><?php echo $utestående;?></td>
</tr>
