<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\mine_regninger\body\main\index\Utestående $this
 * @var \Kyegil\Leiebasen\Visning|string $uteståendeLeieforhold
 */
?><h2>Ubetalte regninger</h2>
<?php echo $uteståendeLeieforhold ? $uteståendeLeieforhold : "Ingen regninger utestående";?>