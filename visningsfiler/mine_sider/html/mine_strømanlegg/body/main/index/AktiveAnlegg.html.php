<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\mine_strømanlegg\body\main\index\AktiveAnlegg $this
 * @var \Kyegil\Leiebasen\Visning|string $anlegg
 */
?><?php if(trim($anlegg)):?><ul class="blocks strømanlegg aktive-anlegg">
    <?php echo $anlegg;?>
</ul><?php else:?>Du er for øyeblikket ikke med på strømdeling.<?php endif;?>