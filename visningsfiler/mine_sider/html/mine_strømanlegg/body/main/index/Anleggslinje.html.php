<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\mine_strømanlegg\body\main\index\Anleggslinje $this
 * @var string $anleggsnummer
 * @var string $beskrivelse
 */
?><li>
    <div><?php echo addslashes($beskrivelse);?></div>
    <div>Anlegg nr. <?php echo addslashes($anleggsnummer);?></div>
    <a class="button" href="/mine-sider/index.php?oppslag=fs_anlegg&id=<?php echo addslashes($anleggsnummer);?>">Se detaljer</a>
</li>