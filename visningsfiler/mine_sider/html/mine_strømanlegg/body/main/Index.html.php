<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\mine_strømanlegg\body\main\Index $this
 * @var \Kyegil\Leiebasen\Visning|string $aktiveAnlegg
 * @var \Kyegil\Leiebasen\Visning|string $passiveAnlegg
 */
?><h2>Nåværende strømdeling</h2>
<?php echo $aktiveAnlegg;?>

<?php if(trim($passiveAnlegg)):?>
    <h2>Tidligere/andre strømanlegg</h2>
    <?php echo $passiveAnlegg;?>
<?php endif;?>