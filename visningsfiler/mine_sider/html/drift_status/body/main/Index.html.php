<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\drift_status\body\main\Index $this
 * @var string $utleier
 * @var string $sisteLeieforholdDato
 * @var string $sisteLeieforholdNavn
 * @var string $antallLedigeLeieobjekter
 * @var int $sisteSkademeldingId
 * @var string $sisteSkademeldingDato
 * @var string $sisteSkademeldingBeskrivelse
 * @var string $sisteGiroutskriftDato
 * @var string $sisteInnbetalingDato
 * @var string $oppgjør
 */
?><h2><?php echo $utleier;?></h2>
<div class="drift-status">
    <div class="siste-leieforhold">
        <h4>Nyeste leieforhold</h4>
        <span class="dato"><?php echo $sisteLeieforholdDato;?></span>: <?php echo $sisteLeieforholdNavn;?>
    </div>
    <div class="antall-ledige-leieobjekter">
        <h4>Antall ledige leieobjekter</h4>
        <a title="Vis" href="/mine-sider/index.php?oppslag=leieobjekter_ledige"><?php echo $antallLedigeLeieobjekter;?></a>
    </div>
    <div class="siste-skademelding">
        <h4>Siste skademelding</h4>
        <span class="dato"><?php echo $sisteSkademeldingDato;?></span>: <a title="Vis skademeldingen" href="/mine-sider/index.php?oppslag=skademelding_kort&id=<?php echo $sisteSkademeldingId;?>"><?php echo $sisteSkademeldingBeskrivelse;?></a>
    </div>
    <div class="siste-giroutskrift">
        <h4>Siste utskrift av regninger</h4>
        <?php echo $sisteGiroutskriftDato;?>
    </div>
    <div class="siste-innbetaling">
        <h4>Siste registrerte innbetaling</h4>
        <?php echo $sisteInnbetalingDato;?>
    </div>
    <div class="oppgjør">
        <h4>Oppgjør</h4>
        <?php echo $oppgjør;?>
    </div>
</div>