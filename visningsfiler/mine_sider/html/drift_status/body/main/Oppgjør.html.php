<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\drift_status\body\main\Oppgjør $this
 * @var string $betalingsgradSisteMnd
 * @var string $betaltInnenForfallSisteMnd
 * @var string $ubetalt1FraDato
 * @var string $ubetalt1TilDato
 * @var string $ubetalt1Beløp
 * @var string $ubetalt2FraDato
 * @var string $ubetalt2TilDato
 * @var string $ubetalt2Beløp
 */
?><div><?php echo $betalingsgradSisteMnd;?> av forfalte leier siste måned er betalt.<br>
    <?php echo $betaltInnenForfallSisteMnd;?> ble betalt innen forfall.
</div>
<div>Ubetalte krav <?php echo $ubetalt1FraDato;?>&nbsp;–&nbsp;<?php echo $ubetalt1TilDato;?>: <?php echo $ubetalt1Beløp;?>.<br>
    Ubetalte krav <?php echo $ubetalt2FraDato;?>&nbsp;–&nbsp;<?php echo $ubetalt2TilDato;?>: <?php echo $ubetalt2Beløp;?>.
</div>