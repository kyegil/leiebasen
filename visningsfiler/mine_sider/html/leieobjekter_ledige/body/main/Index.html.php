<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\leieobjekter_ledige\body\main\Index $this
 * @var \Kyegil\ViewRenderer\ViewInterface $dato
 * @var \Kyegil\ViewRenderer\ViewArray|string $leieobjekter
 * @var string $nedlastingsknapp
 * @var string $kommendeLedigheter
 */
?>
<table class="rutenett">
    <caption>Ledige leieobjekter og lokaler per <?php echo $dato?></caption>
    <!--    <colgroup>-->
    <!--        <col class="dato">-->
    <!--        <col class="beskrivelse">-->
    <!--        <col class="regning">-->
    <!--        <col class="forfall dato">-->
    <!--        <col class="beløp">-->
    <!--        <col class="beløp">-->
    <!--    </colgroup>-->
    <thead>
    <tr>
        <th class="min-width450">Leieobjekt nr.</th>
        <th>Beskrivelse</th>
        <th>Forrige Leietaker</th>
    </tr>
    </thead>
    <tbody>
    <?php echo $leieobjekter;?>
    </tbody>
</table>
<table class="rutenett">
    <caption>Kommende inn- og utflyttinger</caption>
    <!--    <colgroup>-->
    <!--        <col class="dato">-->
    <!--        <col class="beskrivelse">-->
    <!--        <col class="regning">-->
    <!--        <col class="forfall dato">-->
    <!--        <col class="beløp">-->
    <!--        <col class="beløp">-->
    <!--    </colgroup>-->
    <thead>
    <tr>
        <th>Dato</th>
        <th>Leieobjekt</th>
    </tr>
    </thead>
    <tbody>
    <?php echo $kommendeLedigheter;?>
    </tbody>
</table>
