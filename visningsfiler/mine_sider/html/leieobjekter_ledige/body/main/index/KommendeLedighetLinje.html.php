<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\leieobjekter_ledige\body\main\index\KommendeLedighetLinje $this
 * @var string $dato
 * @var string $leieobjektnr
 * @var string $leieobjektbeskrivelse
 * @var string $endring
 */
?>
<tr>
    <td class="dato"><span class="kolonnelabel">Dato</span><?php echo $dato;?></td>
    <td><span class="kolonnelabel">Leieobjekt</span><strong><?php echo \Kyegil\Leiebasen\Leiebase::ucfirst($leieobjektnr) . ' ' . $leieobjektbeskrivelse;?></strong><br>
        <?php echo $endring;?>
    </td>
</tr>
