<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\leieobjekter_ledige\body\main\index\LeieobjektLinje $this
 * @var string $leieobjektnr
 * @var string $leieobjektbeskrivelse
 * @var string $sisteLeietaker
 * @var string $ledighetsTekst
 * @var string $leietakerliste
 */
?>
<tr>
    <td class="min-width450"<?php echo (strval($leietakerliste) || strval($ledighetsTekst)) ? ' style="border-bottom:none;"' : '';?>><?php echo $leieobjektnr;?></td>
    <td<?php echo (strval($leietakerliste) || strval($ledighetsTekst)) ? ' style="border-bottom:none;"' : '';?>><span class="kolonnelabel">Leieobjekt</span><?php echo $leieobjektbeskrivelse;?></td>
    <td<?php echo (strval($leietakerliste) || strval($ledighetsTekst)) ? ' style="border-bottom:none;"' : '';?>><span class="kolonnelabel">Forrige leietaker</span><?php echo $sisteLeietaker;?></td>
</tr>
<?php if(strval($leietakerliste) || strval($ledighetsTekst)):?>
    <tr>
        <td class="min-width450" style="border-top:none;"></td>
        <td colspan="2" style="border-top:none;">
            <?php echo $ledighetsTekst;?><br>
            <?php echo $leietakerliste;?>
        </td>
    </tr>
<?php endif;?>