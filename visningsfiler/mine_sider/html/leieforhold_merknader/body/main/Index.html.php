<?php
/**
 * Leiebasen Oppfølgingsnotater
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_merknader\body\main\Index $this
 * @var string|\Kyegil\Leiebasen\Visning $oppsummeringsfelt
 * @var string|\Kyegil\Leiebasen\Visning $filtre
 * @var string|\Kyegil\Leiebasen\Visning $notatTabell
 * @var string|\Kyegil\Leiebasen\Visning $knapper
 */
?>
<?php echo $oppsummeringsfelt;?>
<?php echo $filtre;?>
<?php echo $notatTabell;?>
<?php echo $knapper;?>