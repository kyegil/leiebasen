<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\fs_regning\body\main\Index $this
 * @var string $id
 * @var string $fakturanummer
 * @var string $beløp
 * @var string $anleggsnummer
 * @var string $fraDato
 * @var string $tilDato
 * @var string $termin
 * @var string $forbruk
 * @var boolean $beregnet
 * @var boolean $fordelt
 * @var string $strømanlegg
 * @var string $anleggsnummer
 * @var string $anleggsbeskrivelse
 * @var \Kyegil\Leiebasen\Visning|string $fordeling
 * @var string $fordeltBeløp
 */
?><dl class="kostnadsdeling egenskaper <?php echo $fordelt ? 'fordelt' : '';?>">
    <dt>Anlegg:</dt>
    <dd><?php echo $anleggsnummer;?></dd>
    <dt>Faktura:</dt>
    <dd><?php echo $fakturanummer;?></dd>
    <dt>Termin:</dt>
    <dd><?php echo $termin;?></dd>
    <dt>Fra og med:</dt>
    <dd><?php echo $fraDato;?></dd>
    <dt>Til og med:</dt>
    <dd><?php echo $tilDato;?></dd>
    <dt>Forbruk:</dt>
    <dd><?php echo $forbruk;?></dd>
    <dt>Regningsbeløp:</dt>
    <dd><?php echo $beløp;?></dd>
</dl>

<table class="kostnadsdeling rutenett">
    <caption><?php echo $fordelt ? "Fordeling" : "Forslag til fordeling";?></caption>
    <thead>
        <tr>
            <td>Leieforhold</td>
            <td>Andel</td>
            <td>Beløp</td>
        </tr>
    </thead>
    <tbody>
    <?php echo $fordeling;?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2">Fordelt Beløp</td>
            <td class="beløp"><strong><?php echo $fordeltBeløp;?></strong></td>
        </tr>
    </tfoot>
</table>
