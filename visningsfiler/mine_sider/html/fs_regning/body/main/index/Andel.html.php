<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\fs_regning\body\main\index\Andel $this
 * @var string $id
 * @var string $leieforhold
 * @var string $leieforholdBeskrivelse
 * @var string $beskrivelse
 * @var string $beløp
 */
?><tr>
    <td><span class="kolonnelabel">Leieforhold</span><?php echo $leieforholdBeskrivelse;?></td>
    <td><span class="kolonnelabel">Andel</span><?php echo $beskrivelse;?></td>
    <td class="beløp"><span class="kolonnelabel">Beløp</span><?php echo $beløp;?></td>
</tr>
