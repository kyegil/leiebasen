<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\skademelding_skjema\body\main\Index $this
 * @var string $skjema
 */
?>
<div>
    Skaderegisteret er tilgjengelig for, og kan oppdateres av alle beboere og ansatte.<br>
    Du vil motta epost ved oppdateringer.
</div>
<?php echo $skjema;?>