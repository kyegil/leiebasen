<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\leieobjekt\body\main\Index $this
 * @var string $leieobjektId
 * @var string $navn
 * @var boolean $ikkeForUtleie
 * @var string $etasje
 * @var string $gateadresse
 * @var string $beskrivelse
 * @var string $postnummer
 * @var string $poststed
 * @var string $bilde
 * @var string $bygningsId
 * @var string $bygningsnavn
 * @var string $areal
 * @var string $antRom
 * @var boolean $bad
 * @var string $toalett
 * @var string $toalettKategori
 * @var string $merknader
 * @var string $leieberegningsmetode
 * @var string $leieberegningsformel
 * @var string $leieforholdfelt
 */
?>
<div class="leieobjektkort adresse">
    <?php if($bilde):?>
        <img class="leieobjektbilde" src="<?php echo $bilde;?>" title="<?php echo $navn ? $navn : $gateadresse;?>" alt="<?php echo $navn ? $navn : $gateadresse;?>">
    <?php endif;?>
    <p><strong><?php echo $navn ? $navn : $gateadresse;?></strong></p>
    <?php if($ikkeForUtleie):?>
        <p class="varsel">Leieobjektet er ikke aktivert for utleie.</p>
    <?php endif;?>
    <p><?php if($navn):?>
        <?php echo $gateadresse;?><br>
    <?php endif;?>
        <?php echo $etasje;?> <?php echo $beskrivelse;?><br>
        <?php echo $postnummer;?> <?php echo $poststed;?>
    </p>
</div>
<?php if($bygningsId):?>
    <div class="leieobjektkort bygning">
        <h3>Bygning</h3>
        <p><?php echo $bygningsnavn;?></p>
    </div>
<?php endif;?>
<div class="leieobjektkort leietakere">
    <h3>Leietakere</h3>
    <?php echo $leieforholdfelt;?>
</div>
<div class="leieobjektkort detaljer">
    <h3>Detaljer:</h3>
    <p>
        <strong>Areal:</strong> <?php echo $areal ? "{$areal}&nbsp;㎡" : "Ikke angitt";?><br>
        <strong>Antall rom:</strong> <?php echo $antRom ?: "Ikke angitt";?><br>
        <strong>Tilgang til bad i leieobjekt el. bygning:</strong> <?php echo $bad ? "Ja" : "Nei";?><br>
        <strong>Tilgang til toalett i leieobjekt el. bygning:</strong> <?php echo $toalett;?> (Kategori <?php echo $toalettKategori;?>)<br>
    </p>
    <?php if($merknader):?>
        <p class="merknader">
            <strong>Merknader:</strong><br>
        </p>
    <?php endif;?>
</div>
<div class="leieobjektkort leieberegning">
    <h3>Beregning av leie</h3>
    <p>
        <strong>Beregningsmetode:</strong> <?php echo $leieberegningsmetode;?><br>
        <?php echo $leieberegningsformel;?>
    </p>
</div>
