<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_transaksjoner\body\main\Krav $this
 * @var string $dato
 * @var string $tekst
 * @var string $regning
 * @var string $forfall
 * @var string $beløp
 * @var string $utestående
 */
?><tr>
    <td class="dato"><span class="kolonnelabel">Dato</span><?php echo $dato;?></td>
    <td><?php echo $tekst;?></td>
    <td class="min-width700"><?php echo $regning ? '<a title="Vis Regning" href="/mine-sider/index.php?oppslag=regning&id=' . $regning . '">Vis Regning</a>' : "";?></td>
    <td class="min-width600 dato"><span class="Forfall"><?php echo $dato;?></td>
    <td class="beløp"><span class="kolonnelabel">Beløp</span><?php echo $beløp;?></td>
    <td class="min-width450 beløp"><span class="kolonnelabel">Utestående</span><?php echo $utestående;?></td>
</tr>
