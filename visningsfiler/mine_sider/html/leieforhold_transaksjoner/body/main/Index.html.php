<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_transaksjoner\body\main\Index $this
 * @var \Kyegil\ViewRenderer\ViewInterface $transaksjoner
 * @var string $tidligereUtestående
 * @var string $nedlastingsknapp
 */
?>Dette er en oversikt over de seneste regningene og betalingene på leieforholdet.
Det er også mulig å laste ned en komplett historisk oversikt om du ønsker det.
<table class="rutenett">
    <caption>Siste transaksjoner</caption>
    <!--    <colgroup>-->
    <!--        <col class="dato">-->
    <!--        <col class="beskrivelse">-->
    <!--        <col class="regning">-->
    <!--        <col class="forfall dato">-->
    <!--        <col class="beløp">-->
    <!--        <col class="beløp">-->
    <!--    </colgroup>-->
    <thead>
    <tr>
        <th>Dato</th>
        <th>Beskrivelse</th>
        <th class="min-width700">Regning</th>
        <th class="min-width600">Forfall</th>
        <th>Beløp</th>
        <th class="min-width450">Utestående saldo</th>
    </tr>
    </thead>
    <tbody>
    <?php echo $transaksjoner;?>
    </tbody>
    <tfoot>
    <tr class="min-width450">
        <td colspan="5">Tidligere utestående</td>
        <td class="beløp"><?php echo $tidligereUtestående;?></td>
    </tr>
    </tfoot>
</table>
<?php echo $nedlastingsknapp;?>