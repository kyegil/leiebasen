<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_transaksjoner\body\main\Betaling $this
 * @var string $dato
 * @var string $tekst
 * @var string $beløp
 * @var string $utestående
 */
?><tr>
    <td class="dato"><span class="kolonnelabel">Dato</span><?php echo $dato;?></td>
    <td colspan="3"><?php echo $tekst;?></td>
    <td class="beløp"><span class="kolonnelabel">Beløp</span><?php echo $beløp;?></td>
    <td class="min-width450 beløp"><span class="kolonnelabel">Utestående</span><?php echo $utestående;?></td>
</tr>
