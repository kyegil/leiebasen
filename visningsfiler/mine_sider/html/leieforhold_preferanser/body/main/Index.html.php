<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_preferanser\body\main\Index $this
 * @var string $leieforholdId
 * @var \Kyegil\ViewRenderer\ViewInterface $form
 */
?>Angi varslingspreferanser for leieforhold <?php echo $leieforholdId;?>.
<?php echo $form;?>