<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\oversikt\body\main\Index $this
 * @var string $leieforholdId
 * @var string $leieobjektId
 */
?><div class="dashboard rutenett">
    <div class="flis">
        <a href="/mine-sider/index.php?oppslag=mine_regninger">
            <div class="rutenett-knapp">
                <div class="ikonbeholder">
                    <img src="/pub/media/bilder/mine-sider/oversiktsfliser/mine-regninger-trn.svg" height="414" width="421" alt="mine regninger">
                </div>
                <h3 class="flistekst">mine regninger</h3>
            </div>
        </a>
    </div>

    <?php if($leieforholdId):?>
    <div class="flis">
        <a href="/mine-sider/index.php?oppslag=leieforhold&id=<?php echo $leieforholdId;?>">
            <div class="rutenett-knapp">
                <div class="ikonbeholder">
                    <img src="/pub/media/bilder/mine-sider/oversiktsfliser/mitt-leieforhold-trn.svg" height="648" width="634" alt="mitt leieforhold">
                </div>
                <h3 class="flistekst">mitt leieforhold</h3>
            </div>
        </a>
    </div>
    <?php endif;?>

    <?php if($leieobjektId):?>
    <div class="flis">
        <a href="/mine-sider/index.php?oppslag=leieobjekt&id=<?php echo $leieobjektId;?>">
            <div class="rutenett-knapp">
                <div class="ikonbeholder">
                    <img src="/pub/media/bilder/mine-sider/oversiktsfliser/min-bolig-trn.svg" height="414" width="421" alt="min bolig">
                </div>
                <h3 class="flistekst">min bolig</h3>
            </div>
        </a>
    </div>
    <?php endif;?>

    <div class="flis">
        <a href="/mine-sider/index.php?oppslag=skademelding_skjema&id=*<?php echo $leieobjektId ? "&leieobjekt={$leieobjektId}" : "";?>">
            <div class="rutenett-knapp">
                <div class="ikonbeholder">
                    <img src="/pub/media/bilder/mine-sider/oversiktsfliser/skademelding-trn.svg" height="414" width="421" alt="meld om skade">
                </div>
                <h3 class="flistekst">meld om skade</h3>
            </div>
        </a>
    </div>

    <div class="flis">
        <a href="/mine-sider/index.php?oppslag=mine_strømanlegg">
            <div class="rutenett-knapp">
                <div class="ikonbeholder">
                    <img src="/pub/media/bilder/mine-sider/oversiktsfliser/strømdeling-trn.svg" height="414" width="421" alt="strømdeling">
                </div>
                <h3 class="flistekst">strømdeling</h3>
            </div>
        </a>
    </div>
</div>
