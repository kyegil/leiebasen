<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\skademelding_kort\body\main\Index $this
 * @var string $registrerer
 * @var string $registrert
 * @var \Kyegil\Leiebasen\Visning|string $kategorier
 * @var string $skadebeskrivelse
 * @var \Kyegil\Leiebasen\Visning|string $bygning
 * @var \Kyegil\Leiebasen\Visning|string $leieobjektbeskrivelse
 * @var string $utbedretDato
 * @var string $sluttregistrerer
 * @var string $sluttrapport
 * @var \Kyegil\Leiebasen\Visning|string $saksopplysninger
 * @var \Kyegil\Leiebasen\Visning|string $kommunikasjonstråd
 * @var \Kyegil\Leiebasen\Visning|string $endreSkademeldingKnapp
 * @var \Kyegil\Leiebasen\Visning|string $leggTilSaksopplysningKnapp
 * @var \Kyegil\Leiebasen\Visning|string $leggTilInnleggKnapp
 * @var \Kyegil\Leiebasen\Visning|string $utbedreKnapp
 * @var \Kyegil\Leiebasen\Visning|string $abonnementStatus
 */
?>
<div>
    <div class="registrert"><strong>Skade registrert av <?php echo $registrerer;?> <?php echo $registrert;?></strong>:</div>
    <div class="kategorier">Kategorier: <?php echo $kategorier;?></div>
    <?php if(trim($bygning)):?>
        <div class="bygning"><?php echo $bygning;?></div>
    <?php endif;?>
    <?php if(trim($leieobjektbeskrivelse)):?>
        <div class="leieobjektnavn"><?php echo $leieobjektbeskrivelse;?></div>
    <?php endif;?>

    <?php if(trim($utbedretDato)):?>
        <div class="skadeutbedring">
            <div class="utbedret"><strong>Skaden er registrert utbedret av <?php echo $sluttregistrerer;?> <?php echo $utbedretDato;?></strong>:</div>
            <div class="sluttrapport"><?php echo $sluttrapport;?></div>
            <div><br></div>
            <div class="beskrivelse">
                <div><strong>Opprinnelig skadebeskrivelse:</strong></div>
                <?php echo $skadebeskrivelse;?>
            </div>
        </div>
    <?php else:?>
        <div class="beskrivelse"><?php echo $skadebeskrivelse;?></div>
    <?php endif;?>
</div>
<?php echo $saksopplysninger;?>
<?php echo $kommunikasjonstråd;?>

<?php echo $endreSkademeldingKnapp;?>
<?php echo $leggTilSaksopplysningKnapp;?>
<?php echo $leggTilInnleggKnapp;?>
<?php echo $utbedreKnapp;?>
<div id="abonnement-status"><?php echo $abonnementStatus;?></div>