<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\fs_anlegg\body\main\Index $this
 * @var string $anleggsnummer
 * @var string $målernummer
 * @var string $beskrivelse
 * @var string $målerplassering
 * @var \Kyegil\Leiebasen\Visning|string $fordeling
 * @var \Kyegil\Leiebasen\Visning|string $forbruksstatistikk
 * @var \Kyegil\Leiebasen\Visning|string $forbruksstatistikkFraDato
 * @var \Kyegil\Leiebasen\Visning|string $forbruksstatistikkTilDato
 * @var \Kyegil\Leiebasen\Visning|string $regningsliste
 */
?>
<p><strong>Strømanlegg nr. <?php echo $anleggsnummer;?></strong></p>
<p><strong>Måler nr:</strong> <?php echo $målernummer;?><br>
    <strong>Plassering av måler:</strong> <?php echo $målerplassering;?></p>
<?php if(trim($forbruksstatistikk)):?>
    <h3>Forbruk</h3>
    <?php echo $forbruksstatistikk;?>
    <div class="verktøylinje">
        <?php echo $forbruksstatistikkFraDato;?><?php echo $forbruksstatistikkTilDato;?>
        <button onclick="leiebasen.extJs.oppdaterDiagram();">Oppdater</button>
    </div>
<?php endif;?>
<div id="fordelingsnøkkel">
    <h3>Fordeling</h3>
    <?php if(trim($fordeling)):?>
        <?php echo $fordeling;?>
    <?php else:?>
        Det er ingen fordeling av strømforbruket på dette anlegget.
    <?php endif;?>
</div>
<?php if(trim($regningsliste)):?>
    <h3>Strømregninger</h3>
    <?php echo $regningsliste;?>
<?php endif;?>
