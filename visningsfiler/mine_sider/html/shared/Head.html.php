<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\shared\Head $this
 * @var string $title
 * @var \Kyegil\Leiebasen\Visning | string $extraMetaTags
 * @var \Kyegil\Leiebasen\Visning | string $links
 * @var \Kyegil\Leiebasen\Visning | string $scripts
 * @var \Kyegil\Leiebasen\Visning | string $title
 */
?><meta http-equiv="content-type" content="text/html; charset=UTF-8" charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="msapplication-TileImage" content="/pub/media/bilder/cropped-favicon-270x270.png">
<?php echo $extraMetaTags?>

<title><?php echo $title?></title>

<?php echo $links?>

<link rel="stylesheet" href="/pub/lib/jquery-ui-1.12.1/jquery-ui.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="/pub/lib/ext-4.2.1.883/resources/css/ext-all-gray.css" type="text/css" media="screen">
<link rel="stylesheet" id="style-css" href="/pub/css/styles.css" type="text/css" media="all">
<link rel="stylesheet" id="mine_sider-css" href="/pub/css/mine_sider.css" type="text/css" media="all">
<link rel="stylesheet" id="print-styles-css" href="/pub/css/print.css" type="text/css" media="print">

<link rel="icon" href="/pub/custom/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" href="/pub/custom/favicon/favicon-192x192.png" sizes="192x192">
<link rel="apple-touch-icon" href="/pub/custom/favicon/favicon-180x180.png">

<script type="text/javascript" src="/pub/js/jquery-3.5.1.js"></script>
<script type="text/javascript" src="/pub/lib/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="/pub/js/construct.js"></script>
<script>
    var leiebasen = leiebasen || {kjerne: {},};
    leiebasen.kjerne = leiebasen.kjerne || {};
    leiebasen.mineSider = leiebasen.mineSider || {};
</script>
<?php echo $scripts?>
