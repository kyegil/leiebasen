<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\shared\body\hovedmeny\Menyelement $this
 * @var string $brukernavn
 * @var int $menyNivå
 * @var string $id
 * @var string $text
 * @var string $url
 * @var bool $active
 * @var bool $nåværende
 * @var string $symbol
 * @var string $extraClasses
 * @var string $items
 * @var bool $nyGruppe
 */
    $class = implode(' ', [$nåværende ? 'nåværende' : '', $active ? 'active' : '', $nyGruppe ? 'group' : '']);
?><?php switch ($menyNivå):
    default: // Alle nivåer behandles likt ?>
    <li class="menylenke <?php echo trim($items) ? 'menylenke_has_children menu-item-has-children ' : '';?><?php echo $class;?>"
        ><a
                href="<?php echo trim($url) ? $url : '#';?>"><?php echo $text;?></a>
        <?php if (trim($items)):?>
            <ul class="children">
                <?php echo $items;?>
            </ul>
        <?php endif;?>
        </li>
    <?php break;
endswitch;