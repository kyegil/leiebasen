<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\shared\body\Brukermeny $this
 * @var string $brukernavn
 * @var string $items
 */
?><div class="menu-modal brukermeny cover-modal" data-modal-target-string=".brukermeny" aria-expanded="false">
    <div class="menu-modal-inner modal-inner bg-body-background">
        <div class="menu-wrapper section-inner">
            <div class="menu-top">
                <div class="menu-modal-toggles header-toggles">
                    <a href="#"
                       class="toggle nav-toggle nav-untoggle"
                       data-toggle-target=".brukermeny"
                       data-toggle-screen-lock="true"
                       data-toggle-body-class="showing-menu-modal"
                       aria-pressed="false"
                       data-set-focus="#min-side-header .nav-toggle"
                    >
                        <div class="bars">
                            <img src="/pub/media/bilder/mine-sider/profil.png" alt="profil">
                        </div><!-- .bars -->
                    </a><!-- .nav-toggle -->
                </div><!-- .menu-modal-toggles -->

                <ul class="main-menu reset-list-style">
                    <?php echo $items;?>
                </ul><!-- .main-menu -->
            </div><!-- .menu-top -->
        </div><!-- .menu-wrapper -->
    </div><!-- .menu-modal-inner -->
</div><!-- .menu-modal -->
