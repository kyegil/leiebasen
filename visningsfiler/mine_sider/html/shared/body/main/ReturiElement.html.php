<?php
    /**
     * Leiebasen Visningsblokk
     *
     * @var \Kyegil\Leiebasen\Visning\mine_sider\html\shared\body\main\ReturiElement $this
     * @var string $url
     * @var string $tittel
     * @var boolean $rot
     * @var boolean $current
     */
?><a title="<?php echo $tittel; ?>" href="<?php echo $url; ?>"><span <?php echo $current ? ' class="current"' : ''; ?><?php echo $rot ? ' class="root"' : ''; ?>><?php echo $tittel; ?></span></a>