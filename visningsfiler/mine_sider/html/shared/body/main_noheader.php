<?php
    /**
     * Leiebasen Visningsblokk
     *
     * @var \Kyegil\ViewRenderer\ViewInterface $tittel
     * @var \Kyegil\ViewRenderer\ViewInterface $innhold
     * @var \Kyegil\ViewRenderer\ViewInterface $breadcrumbs
     * @var \Kyegil\ViewRenderer\ViewInterface $knapper
     */
?><main id="site-content">
    <article class="section-inner page">
        <?php echo $breadcrumbs?>

        <div class="article-inner">
            <div class="entry-content">
                <?php echo $innhold?>
            </div><!-- .entry-content -->
        </div><!-- .article-inner -->
        <?php echo $knapper;?>
    </article><!-- .post -->
</main><!-- #site-content -->
