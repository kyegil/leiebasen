<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\shared\Html $this
 * @var \Kyegil\ViewRenderer\ViewInterface $head
 * @var \Kyegil\ViewRenderer\ViewInterface $header
 * @var \Kyegil\ViewRenderer\ViewInterface $hovedmeny
 * @var \Kyegil\ViewRenderer\ViewInterface $brukermeny
 * @var \Kyegil\ViewRenderer\ViewInterface $main
 * @var \Kyegil\ViewRenderer\ViewInterface $footer
 * @var \Kyegil\ViewRenderer\ViewInterface $scripts
 */
?><!DOCTYPE html>
<html class="js no-touchevents" style="" lang="nb-NO">
    <head>
        <?php echo $head;?>
    </head>

    <body class="oppslag leiebasen-custom-logo">

        <?php echo $header;?>

        <?php echo $hovedmeny;?>

        <?php echo $brukermeny;?>

        <?php echo $main;?>

        <?php echo $footer;?>

        <?php if($scripts):?>
            <script>
            <?php echo $scripts;?>
            </script>
        <?php endif;?>

        <!--
                        <script type="text/javascript" src="ressurser/core.js"></script>
                        <script type="text/javascript" src="ressurser/widget.js"></script>
                        <script type="text/javascript" src="ressurser/mouse.js"></script>
                        <script type="text/javascript" src="ressurser/sortable.js"></script>
                 -->
    </body>
</html>