<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold\body\main\Index $this
 * @var \Kyegil\ViewRenderer\ViewInterface|string $leieforholdnavn
 * @var string $fradato
 * @var string $sistFornyet
 * @var string $oppsigelsesdato
 * @var string $fristillelsesdato
 * @var string $leietakere
 * @var int $leieobjektId
 * @var string $leieobjekt
 * @var string $utestående
 * @var string $forfalt
 * @var string $leiebeløp
 * @var string $terminlengde
 * @var string $fastKid
 * @var bool $efakturaAktivert
 * @var string $efakturaavtale
 * @var bool $avtaleGiroAktivert
 * @var string $fboRegistrert
 * @var string $poengProgram
 * @var bool $harAktivBetalingsplan
 * @var string $aktivBetalingsplanTilDato
 * @var string $visBetalingsplanKnapp
 * @var \Kyegil\Leiebasen\Jshtml\Html\HtmlElement|string $avtaletekstKnapp
 * @var \Kyegil\Leiebasen\Jshtml\Html\HtmlElement|string $transaksjonerKnapp
 * @var \Kyegil\Leiebasen\Jshtml\Html\HtmlElement|string $preferanserKnapp
 * @var \Kyegil\Leiebasen\Jshtml\Html\HtmlElement|string $merknaderKnapp
 * @var \Kyegil\Leiebasen\Jshtml\Html\HtmlElement|string $uteståendeKnapp
 * @var \Kyegil\Leiebasen\Jshtml\Html\HtmlElement|string $lagBetalingsplanKnapp
 * @var bool $visLeieforholdVelger
 */
?>
<p><strong><?php echo $leieforholdnavn;?></strong></p>
<?php if($visLeieforholdVelger):?><p><a href="/mine-sider/index.php?oppslag=leieforhold_liste&returi=default">Vis et annet leieforhold</a></p><?php endif;?>
<p>
    Leieforholdet ble påbegynt <?php echo $fradato;?><br>

<?php if($sistFornyet != $fradato):?>
    Siste inngåtte leieavtale løper fra <?php echo $sistFornyet;?>.<br>
<?php endif;?>

</p>
<?php echo $avtaletekstKnapp;?>
<br>

<?php if($oppsigelsesdato):?>
    <h3>Oppsigelse</h3>
    <p>
        <strong style="color: red">Dette leieforholdet har blitt oppsagt med virkning fra <?php echo $fristillelsesdato;?></strong><br>
        Oppsigelsen ble levert <?php echo $oppsigelsesdato;?>.<br />
    </p>
<?php endif;?>

<h3>Leietakere<br></h3>
<p>
    <?php echo $leietakere;?>
</p>

<h3>Leieobjekt</h3>
<p>
    <a href="/mine-sider/index.php?oppslag=leieobjekt&id=<?php echo $leieobjektId;?>" title="Klikk for å åpne leieobjektkortet">
        <?php echo \Kyegil\Leiebasen\Leiebase::ucfirst($leieobjekt);?>
    </a><br>
</p>

<h3>Utestående</h3>
<p>
    <?php if($forfalt):?>
    <strong><?php echo $forfalt;?> har forfalt</strong><br>
    <?php elseif($utestående):?>
        <?php echo $utestående;?> (har ikke forfalt)<br>
    <?php else:?>
        <strong>Ingenting utestående</strong><br>
    <?php endif;?>
</p>

<?php if(trim($utestående) && $harAktivBetalingsplan):?>
<p>Det er inngått en betalingsavtale som løper fram til <?php echo $aktivBetalingsplanTilDato;?></p>

<?php endif;?>

<?php echo $uteståendeKnapp;?>
<?php if(trim($lagBetalingsplanKnapp)): echo $lagBetalingsplanKnapp; endif;?>
<?php if(trim($visBetalingsplanKnapp)): echo $visBetalingsplanKnapp; endif;?>

<h3>Leie</h3>
<p>
    <?php echo $leiebeløp;?> per <?php echo $terminlengde;?>.<br>
</p>
<p>
    Fast KID: <?php echo $fastKid;?><br>

    <?php if($efakturaAktivert):?>
        eFaktura Identifier: <?php echo $efakturaavtale ?: "Ikke registrert";?><br>
    <?php endif;?>

    <?php if($avtaleGiroAktivert):?>
        Status AvtaleGiro: <?php echo $fboRegistrert ? "AvtaleGiro registrert {$fboRegistrert}" : "Ingen avtale registrert";?><br>
    <?php endif;?>
</p>
<p>
    <?php echo $poengProgram;?>
</p>
<?php echo $transaksjonerKnapp;?>
<?php echo $preferanserKnapp;?>
<?php echo $merknaderKnapp;?>
