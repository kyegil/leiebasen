<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\html\leieforhold_transaksjoner\body\main\Index $this
 * @var \Kyegil\ViewRenderer\ViewInterface $transaksjoner
 * @var string $inntekterKolonneOverskrifter
 * @var string $inntekterTabellLinjer
 */
?>
<table class="rutenett">
    <caption>Inntekter</caption>
    <thead>
    <tr>
        <?php echo $inntekterKolonneOverskrifter;?>
    </tr>
    </thead>
    <tbody>
        <?php echo $inntekterTabellLinjer;?>
    </tbody>
</table>