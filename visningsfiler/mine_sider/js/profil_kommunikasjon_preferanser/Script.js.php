<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\js\profil_kommunikasjon_preferanser\Script $this
 */
?>

/**
 * Returnerer sann dersom et eksisterende oppslag er valgt
 *
 * @returns {boolean}
 */
leiebasen.mineSider.oppdaterForbindelser = function() {
    var epostVarslinger = $("input[id^=epostvarsling-]");
    epostVarslinger.each(function(index, element) {
        var adgangId = $(element).attr('id').substring(14);
        leiebasen.mineSider.oppdaterForbindelse(adgangId, false);
    });
}

leiebasen.mineSider.oppdaterForbindelse = function(adgangId, onclick) {
    var epostVarsling = $("input#epostvarsling-" + adgangId);
    var innbetalingsbekreftelse = $("input#innbetalingsbekreftelse-" + adgangId);
    var forfallsvarsel = $("input#forfallsvarsel-" + adgangId);
    var umiddelbartBetalingsvarselEpost = $("input#umiddelbart_betalingsvarsel_epost-" + adgangId);

    if (epostVarsling.is(":checked")) {
        innbetalingsbekreftelse.prop({disabled: false});
        forfallsvarsel.prop({disabled: false});
        umiddelbartBetalingsvarselEpost.prop({disabled: false});
        if (onclick) {
            innbetalingsbekreftelse.prop({checked: true});
            forfallsvarsel.prop({checked: true});
            umiddelbartBetalingsvarselEpost.prop({checked: true});
        }
    }
    else {
        innbetalingsbekreftelse.prop({disabled: true, checked: false});
        forfallsvarsel.prop({disabled: true, checked: false});
        umiddelbartBetalingsvarselEpost.prop({disabled: true, checked: false});
    }
}


// I utgangspunktet skal nyregistrering-skjemaet skjules
leiebasen.mineSider.oppdaterForbindelser();
