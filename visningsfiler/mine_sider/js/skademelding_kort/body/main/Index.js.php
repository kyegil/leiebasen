<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\js\skademelding_kort\body\main\Index $this
 * @var string $skadeId
 */
?>
leiebasen.mineSider = leiebasen.mineSider || {};
leiebasen.mineSider.oppdaterAbonnementStatus = function() {
    console.log('abonnementstatus oppdateres');
    var statusfelt = $('#abonnement-status');
    if(statusfelt.length) {
        $.ajax({
            url: '/mine-sider/index.php?oppslag=skademelding_kort&id=<?php echo $skadeId;?>&oppdrag=hent_data&data=status_abonnement',
            success: function(data, textStatus, jqXHR) {
                statusfelt.html(data);
                console.log('abonnementstatus oppdatert');
            }
        });
    }
}

leiebasen.mineSider.abonner = function(status) {
    console.log('abonnementstatus endres');
    $.ajax({
        url: '/mine-sider/index.php?oppslag=skademelding_skjema&id=<?php echo $skadeId;?>&oppdrag=ta_i_mot_skjema&skjema=status_abonnement',
        method: 'POST',
        data: {status: status ? 1 : 0},
        success: function(data, textStatus, jqXHR) {
            var respons = JSON.parse(data);
            console.log('abonnementstatus endret');
            leiebasen.mineSider.oppdaterAbonnementStatus();
            if(respons.msg) {
                alert(respons.msg);
            }
        }
    });
}

leiebasen.mineSider.oppdaterAbonnementStatus();
