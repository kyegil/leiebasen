<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\js\betalingsutsettelse_skjema\Index $this
 * @var bool $eksisterendePlan
 * @var int|null $leieforholdId
 * @var int|null $kravId
 * @var string $helligdager
 * @var string $framtidigeKravJson
 */
?>
leiebasen.mineSider.betalingsutsettelseSkjema = leiebasen.mineSider.betalingsutsettelseSkjema || {};

leiebasen.mineSider.betalingsutsettelseSkjema.initier = function() {
    /**
     *
     * @type {{kravFelt: (jQuery), forfallsdatoFelt: (jQuery)}}
     */
    this.felter = {
        kravFelt: $("[name='krav']"),
        forfallsdatoFelt: $("[name='forfallsdato']"),
        notatFelt: $("[name='notat']")
    }
    this.submitKnapp = $("[type='submit']");

    /**
     *
     * @type {Object.<string, {id: number, forfall: string, originalForfallsdato: string, maxForfallsdato: string, beskrivelse: string}>}}
     */
    this.framtidigeKrav = <?php echo $framtidigeKravJson;?>;

    /**
     * @type {string[]}
     */
    this.helligdager = <?php echo $helligdager;?>;

    this.felter.kravFelt.empty();
    Object.values(this.framtidigeKrav).forEach(krav => {
        this.felter.kravFelt.append($('<option>', {
            value: krav.id,
            text: krav.beskrivelse
        }));
        this.felter.kravFelt.val(<?php echo $kravId ?? '';?>);
    });

    this.felter.kravFelt.on('change', function() {
        leiebasen.mineSider.betalingsutsettelseSkjema.oppdaterKalender();
    });
    this.felter.forfallsdatoFelt.on('change', function() {
        leiebasen.mineSider.betalingsutsettelseSkjema.oppdaterForespørsel();
    });
}

/**
 * Oppdater skjemaet og planen hver gang det er gjort endringer i feltene som danner grunnlaget for den
 */
leiebasen.mineSider.betalingsutsettelseSkjema.oppdaterKalender = function() {
    /**
     * @type {string}
     */
    let valgtKravId = this.felter.kravFelt.val();
    let valgtKrav = this.framtidigeKrav[valgtKravId];
    if(valgtKrav) {
        this.felter.forfallsdatoFelt.val(valgtKrav.forfall);
        this.felter.forfallsdatoFelt.attr({min: valgtKrav.originalForfallsdato});
    }
}

/**
 * Vis/skjul notatfeltet avhengig av om leietakerhar spillerom for automatisk godkjenning eller ikke
 */
leiebasen.mineSider.betalingsutsettelseSkjema.oppdaterForespørsel = function() {
    /**
     * @type {string}
     */
    let valgtKravId = this.felter.kravFelt.val();
    let valgtForfallsdato = this.felter.forfallsdatoFelt.val();
    let valgtKrav = this.framtidigeKrav[valgtKravId];
    if(valgtKrav && valgtKrav.maxForfallsdato < valgtForfallsdato) {
        this.felter.notatFelt.parent().show();
        this.submitKnapp.html('Send forespørsel om utsettelse');
    }
    else {
        this.felter.notatFelt.parent().hide();
        this.submitKnapp.html('Lagre ny forfallsdato');
    }
}

leiebasen.mineSider.betalingsutsettelseSkjema.initier();
leiebasen.mineSider.betalingsutsettelseSkjema.oppdaterKalender();
leiebasen.mineSider.betalingsutsettelseSkjema.oppdaterForespørsel();