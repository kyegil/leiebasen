<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\js\notat\Index $this
 * @var int $notatId
 */
?>
leiebasen.mineSider.notat = leiebasen.mineSider.notat || {};
leiebasen.mineSider.notat.lastVedlegg = function() {
    window.open('/mine-sider/index.php?oppslag=notat&oppdrag=hent_data&data=vedlegg&id=<?php echo $notatId;?>');
}
leiebasen.mineSider.notat.visBrev = function() {
    window.open('/mine-sider/index.php?oppslag=notat&oppdrag=hent_data&data=brev&id=<?php echo $notatId;?>');
}
