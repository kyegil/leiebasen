<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\js\profil_skjema\body\main\Index $this
 */
?>
leiebasen.mineSider.oppdaterElementer = function() {
    $('#gjelderBygning-velger').change(function()
    {
        var leieobjektVelger = $('#leieobjekt-velger');
        var bygningVelger = $('#bygning-velger');
        if(this.checked === true)
        {
            leieobjektVelger.parent().hide();
            leieobjektVelger.prop( "disabled", true );
            bygningVelger.parent().show();
            bygningVelger.prop( "disabled", false );
        }
        else{
            leieobjektVelger.parent().show()
            leieobjektVelger.prop( "disabled", false );
            bygningVelger.parent().hide();
            bygningVelger.prop( "disabled", true );
        }
    });
}

leiebasen.mineSider.oppdaterElementer();
