<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\js\leieforhold_preferanser\body\main\Index $this
 */
?>
leiebasen.mineSider.oppdaterElementer = function() {
    let regningTilObjekt = $("#regningTilObjekt-velger").is(":checked");
    let regningsperson = $("#regningsperson-velger").find("input[name='regningsperson']:checked").val();
    if(regningTilObjekt) {
        $("#regningsobjekt-velger").show();
        $("label[for='regningsobjekt-velger']").show();
        $("#regningsperson-velger").hide();
        $("#regningsadresse-felter").hide();
    }
    else {
        $("#regningsperson-velger").show();
        $("#regningsobjekt-velger").hide();
        $("label[for='regningsobjekt-velger']").hide();
        if(regningsperson > 0) {
            $("#regningsadresse-felter").hide();
        }
        else {
            $("#regningsadresse-felter").show();
        }
    }
}

$('#regningTilObjekt-velger').click(function () {
    leiebasen.mineSider.oppdaterElementer();
});
$("#regningsperson-velger").find('input[type=radio][name=regningsperson]').change(function() {
    leiebasen.mineSider.oppdaterElementer();
});
leiebasen.mineSider.oppdaterElementer();
