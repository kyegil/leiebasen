<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\js\fs_anlegg\body\main\Forbruksstatistikk $this
 * @var \Kyegil\Leiebasen\Visning|string $store
 * @var string $forbruksdiagramModellFelter
 * @var string $forbruksdiagramAkseFelter
 */
?>

leiebasen.extJs.oppdaterDiagram = function() {
    var fradato = new Date();
    fradato.setFullYear(fradato.getFullYear() - 1);
    fradato.setDate(fradato.getDate() + 1);
    var tildato = new Date();

    var fradatofelt = Ext.get('forbruksstatistikk-fradato');
    var tildatofelt = Ext.get('forbruksstatistikk-tildato');
    if(fradatofelt && fradatofelt.getValue()) {
        fradato = new Date(fradatofelt.getValue());
    }
    if(tildatofelt && tildatofelt.getValue()) {
        tildato = new Date(tildatofelt.getValue());
    }

    forbruksdiagram.axes.getAt(1).fromDate = fradato.valueOf();
    forbruksdiagram.axes.getAt(1).toDate = tildato.valueOf();
    forbruksdiagramdata.load({
        params: {
            fra: Ext.Date.format(fradato, 'Y-m-d'),
            til: Ext.Date.format(tildato, 'Y-m-d')
        }
    });
}

Ext.define('ForbruksdiagramModell', {
    extend: 'Ext.data.Model',
    fields: <?php echo $forbruksdiagramModellFelter;?>
});


/**
 * @type {Ext.data.JsonStore}
 */
var forbruksdiagramdata = <?php echo $store;?>;

/**
 * @type {Ext.chart.Chart}
 */
var forbruksdiagram = Ext.create('Ext.chart.Chart', {
    id: 'forbruksdiagram',
    style: 'background:#fff',
    animate: true,
    store: forbruksdiagramdata,
    axes: [
        {
            title: 'Daglig forbruk i kWh',
            type: 'Numeric',
            grid: {
                odd: {
                    opacity: 1,
                    fill: '#ddd',
                    stroke: '#bbb',
                    'stroke-width': 1
                }
            },
            position: 'left',
            fields: <?php echo $forbruksdiagramAkseFelter;?>,
            minimum: 0,
            maximum: 200
        },
        {
            title: 'Dato',
            type: 'Time',
            grid: false,
            label: {
                rotate: {
                    degrees: 315
                }
            },
            position: 'bottom',
            fields: ['dato'],
            dateFormat: 'd.m.Y',
            constrain: true,
            fromDate: Date.parse('2019-01-01'),
            toDate: Date.parse('2020-11-01'),
            step: ['mo', 1]
        }
    ],
    series: [
        {
            type: 'area',
            highlight: true,
            tips: {
                trackMouse: true,
                width: 250,
                height: 35,
                renderer: function(storeItem, item) {
                    var d = Ext.Date.format(new Date(storeItem.get('dato')), 'd. M Y');
                    this.setTitle(d + '<br />Faktura ' + item.storeField + ' - ' + (Math.round(storeItem.get(item.storeField)*10)/10).toLocaleString('nb-NO') + '&nbsp;kWh per dag.');
                }
            },
            xField: 'dato',
            yField: <?php echo $forbruksdiagramAkseFelter;?>,
            style: {
                opacity: 0.93
            }
        }
    ],
    renderTo: 'forbruksstatistikk',
    width: '100%',
    height: 400
});

leiebasen.extJs.oppdaterDiagram();

