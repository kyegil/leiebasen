<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\mine_sider\js\leieforhold_merknader\body\main\Index $this
 * @var int $leieforholdId
 */
?>
leiebasen.mineSider = leiebasen.mineSider || {};
leiebasen.mineSider.lastVedlegg = function(notatnr) {
    window.open('/mine-sider/index.php?oppslag=leieforhold_merknader&id=<?php echo $leieforholdId;?>&oppdrag=hent_data&data=vedlegg&vedlegg=' + notatnr);
}
leiebasen.mineSider.visBrev = function(notatnr) {
    window.open('/mine-sider/index.php?oppslag=leieforhold_merknader&id=<?php echo $leieforholdId;?>&oppdrag=hent_data&data=brev&brev=' + notatnr);
}
