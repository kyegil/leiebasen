<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\oppfølging\html\betalingsplan_kort\Index $this
 * @var \Kyegil\Leiebasen\Visning|string $leieforholdBeskrivelse
 * @var bool $frosset
 * @var bool $stoppOppfølging
 * @var string $avventOppfølging
 * @var \Kyegil\Leiebasen\Visning|string $aktivitet
 * @var \Kyegil\Leiebasen\Visning|string $dato
 * @var \Kyegil\Leiebasen\Visning|string $notatTekst
 * @var \Kyegil\Leiebasen\Visning|string $registrert
 * @var \Kyegil\Leiebasen\Visning|string $avtaleTekst
 * @var \Kyegil\Leiebasen\Visning|string $kravoversikt
 * @var \Kyegil\Leiebasen\Visning|string $avdragsoversikt
 * @var \Kyegil\Leiebasen\Visning|string $eksterntDokument
 * @var \Kyegil\Leiebasen\Visning|string $vedlegg
 * @var \Kyegil\Leiebasen\Visning|string $knapper
 */
?>
<div<?php if($frosset):?> class="frosset"<?php endif;?>>
    <div><?php echo $leieforholdBeskrivelse;?></div>
</div>

<div class="notat">
    <h3><?php echo $aktivitet;?></h3>
    <div><strong>inngått:</strong> <?php echo $dato;?></div>

    <?php if(trim($notatTekst)):?>
        <div class="kort"><?php echo $notatTekst;?></div>
    <?php endif;?>

    <?php if(trim($avdragsoversikt)):?>
        <div class="avdragsoversikt"><?php echo $avdragsoversikt;?></div>
    <?php endif;?>

    <?php if(trim($kravoversikt)):?>
        <div class="kravoversikt"><?php echo $kravoversikt;?></div>
    <?php endif;?>

    <?php if(trim($eksterntDokument)):?>
    <div>
        <strong>Eksternt dokument:</strong>
        <?php echo htmlspecialchars($eksterntDokument);?>
    </div>
    <?php endif;?>

    <?php if(trim($vedlegg)):?>
        <div>
            <strong>Vedlegg:</strong>
            <?php echo htmlspecialchars($vedlegg);?>
        </div>
    <?php endif;?>

    <?php if(trim($avtaleTekst)):?>
        <div class="brev visning"><?php echo $avtaleTekst;?></div>
    <?php endif;?>

</div>
<?php echo $knapper;?>