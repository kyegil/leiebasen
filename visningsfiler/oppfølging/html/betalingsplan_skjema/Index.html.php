<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\oppfølging\html\betalingsplan_skjema\Index $this
 * @var int $notatId
 * @var string $skjema
 * @var \Kyegil\Leiebasen\Visning|string $knapper
 */
?><?php echo $skjema;?>
<?php echo $knapper;?>