<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\oppfølging\html\shared\Html $this
 * @var \Kyegil\ViewRenderer\ViewInterface $head
 * @var \Kyegil\ViewRenderer\ViewInterface $header
 * @var \Kyegil\ViewRenderer\ViewInterface $hovedmeny
 * @var \Kyegil\ViewRenderer\ViewInterface $brukermeny
 * @var \Kyegil\ViewRenderer\ViewInterface $main
 * @var \Kyegil\ViewRenderer\ViewInterface $footer
 * @var \Kyegil\ViewRenderer\ViewInterface $scripts
 */
?><!DOCTYPE html>
<html lang="nb-NO">
    <!--suppress HtmlRequiredTitleElement -->
    <head>
        <?php echo $head;?>
    </head>

    <body class="oppfølging">

        <?php echo $header;?>

        <?php echo $hovedmeny;?>

        <?php echo $main;?>

        <?php if($scripts = trim($scripts)):?>
            <script>
                <?php echo $scripts;?>
            </script>
        <?php endif;?>
    </body>
</html>