<?php
    /**
     * Leiebasen Visningsblokk
     *
     * @var \Kyegil\Leiebasen\Visning\oppfølging\html\shared\body\Main $this
     * @var \Kyegil\ViewRenderer\ViewInterface $tittel
     * @var \Kyegil\ViewRenderer\ViewInterface $innhold
     * @var \Kyegil\ViewRenderer\ViewInterface $breadcrumbs
     * @var \Kyegil\ViewRenderer\ViewInterface $varsler
     * @var \Kyegil\ViewRenderer\ViewInterface $knapper
     */
?><main id="site-content">

    <!-- Dark overlay -->
    <div class="overlay"></div>

    <?php echo $breadcrumbs;?>

        <!-- Main content -->
        <div id="main-content">
            <div class="container">
                <?php echo $innhold;?>
            </div>
        </div>

</main>
