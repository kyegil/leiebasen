<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\oppfølging\html\notat\Index $this
 * @var \Kyegil\Leiebasen\Visning|string $leieforholdBeskrivelse
 * @var bool $frosset
 * @var bool $stoppOppfølging
 * @var string $avventOppfølging
 * @var \Kyegil\Leiebasen\Visning|string $aktivitet
 * @var \Kyegil\Leiebasen\Visning|string $dato
 * @var \Kyegil\Leiebasen\Visning|string $notatTekst
 * @var \Kyegil\Leiebasen\Visning|string $registrert
 * @var bool $skjulForLeietaker
 * @var \Kyegil\Leiebasen\Visning|string $brevTekst
 * @var \Kyegil\Leiebasen\Visning|string $eksterntDokument
 * @var \Kyegil\Leiebasen\Visning|string $vedlegg
 * @var \Kyegil\Leiebasen\Visning|string $knapper
 */
?>
<div<?php if($frosset):?> class="frosset"<?php endif;?>>
    <div><?php echo $leieforholdBeskrivelse;?></div>
    <?php if($stoppOppfølging || $avventOppfølging):?>
        <div><strong>
                Videre oppfølging av leieforholdet <?php if($avventOppfølging):?>avventes inntil <?php echo $avventOppfølging;?><?php else:?>har opphørt<?php endif;?>
            </strong></div>
    <?php endif;?>

    <?php if($frosset):?>
        <div class="frosset"><strong>Leieforholdet er frosset</strong></div>
    <?php endif;?>
</div>

<div class="notat">
    <h3><?php echo $aktivitet;?></h3>
    <div><strong>Dato: </strong><?php echo $dato;?></div>

    <?php if(trim($notatTekst)):?>
        <div class="kort"><?php echo $notatTekst;?></div>
    <?php endif;?>

    <div><strong>Gjennomsiktighet:</strong>
    <?php if(trim($skjulForLeietaker)):?>
        Dette er et internt notat, som ikke er synlig for leietaker
    <?php else:?>
        Denne oppføringen er også synlig for leietaker, via 'mine sider'
    <?php endif;?>
    </div>

    <?php if(trim($eksterntDokument)):?>
    <div>
        <strong>Eksternt dokument:</strong>
        <?php echo htmlspecialchars($eksterntDokument);?>
    </div>
    <?php endif;?>

    <?php if(trim($vedlegg)):?>
        <div>
            <strong>Vedlegg:</strong>
            <?php echo htmlspecialchars($vedlegg);?>
        </div>
    <?php endif;?>

    <div>Registrert <?php echo $registrert;?></div>
    <?php if(trim($brevTekst)):?>
        <div class="brev visning"><?php echo $brevTekst;?></div>
    <?php endif;?>

</div>
<?php echo $knapper;?>