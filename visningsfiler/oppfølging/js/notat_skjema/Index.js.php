<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\oppfølging\js\notat_skjema\Index $this
 * @var int $notatId
 */
?>
leiebasen.oppfølging.notatSkjema = leiebasen.oppfølging.notatSkjema || {};

leiebasen.oppfølging.notatSkjema.initier = function() {
    this.felter = {
        kategoriFelt: $('#kategori_felt'),
        avventOppfølgingFelt: $('#avvent_oppfølging_felt'),
        stoppOppfølgingFelt: $('#stopp_oppfølging_felt'),
        frysLeieforholdetFelt: $('#frys_leieforholdet_felt')
    }
    this.felter.kategoriFelt.on('change', function() {
        leiebasen.oppfølging.notatSkjema.oppdaterSkjema();
    })
    this.felter.frysLeieforholdetFelt.on('change', function() {
        if (leiebasen.oppfølging.notatSkjema.felter.frysLeieforholdetFelt.prop('checked')) {
            leiebasen.oppfølging.notatSkjema.felter.stoppOppfølgingFelt.prop('checked', true)
        }
        leiebasen.oppfølging.notatSkjema.oppdaterOppfølgingsfelter();
    })
    this.felter.stoppOppfølgingFelt.on('change', function() {
        leiebasen.oppfølging.notatSkjema.oppdaterOppfølgingsfelter();
    })
    this.oppdaterOppfølgingsfelter();
}

leiebasen.oppfølging.notatSkjema.oppdaterOppfølgingsfelter = function() {
    this.felter.avventOppfølgingFelt.prop('disabled', this.felter.stoppOppfølgingFelt.prop('checked'));
}

leiebasen.oppfølging.notatSkjema.oppdaterSkjema = function() {
    var kategori = this.felter.kategoriFelt.val();
    switch(kategori) {
        case 'betalingsplan':
            <?php if(!intval($notatId)):?>
            var leieforholdFelt = $('[name=leieforhold]');
            if (leieforholdFelt && leieforholdFelt.val()) {
                window.location.replace('/oppfølging/index.php?oppslag=betalingsplan_skjema&leieforhold=' + leieforholdFelt.val() + 'id=*');
            }
           <?php endif;?>
            break;
        default:
            break;
    }
}

leiebasen.oppfølging.notatSkjema.lastNedVedlegg = function() {
    window.open('/oppfølging/index.php?oppslag=notat&oppdrag=hent_data&data=vedlegg&id=<?php echo $notatId;?>');
}

leiebasen.oppfølging.notatSkjema.slettVedlegg = function() {
    if(confirm('Er du sikker på at du vil slette det eksisterende vedlegget?')) {
        $.ajax({
            url: '/oppfølging/index.php?oppslag=notat_skjema&id=<?php echo $notatId;?>&oppdrag=oppgave&oppgave=slett_vedlegg',
            data: {notat_id: <?php echo $notatId ?? '"*"';?>},
            method: "POST",
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
                $("div.eksisterende-vedlegg").hide();
                if (data.msg) {
                    alert(data.msg);
                }
            }
        });
    }
}

leiebasen.oppfølging.notatSkjema.initier();
$('[name=leieforhold]').on('change', function() {
    leiebasen.oppfølging.notatSkjema.oppdaterSkjema();
});