<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\oppfølging\js\notat\Index $this
 * @var int $leieforholdId
 * @var int $notatId
 */
?>
leiebasen.oppfølging.notat = leiebasen.oppfølging.notat || {};
leiebasen.oppfølging.notat.lastVedlegg = function() {
    window.open('/oppfølging/index.php?oppslag=notat&oppdrag=hent_data&data=vedlegg&id=<?php echo $notatId;?>');
}
leiebasen.oppfølging.notat.visBrev = function() {
    window.open('/oppfølging/index.php?oppslag=notat&oppdrag=hent_data&data=brev&id=<?php echo $notatId;?>');
}
leiebasen.oppfølging.notat.godkjennBetalingsplan = function() {
    $.ajax({
        url: '/oppfølging/index.php?oppslag=betalingsplan_skjema&leieforhold=<?php echo $leieforholdId;?>&oppdrag=oppgave&oppgave=godkjenn_betalingsplan&id=<?php echo $notatId;?>',
        data: {notat_id: <?php echo json_encode($notatId);?>},
        method: "POST",
        dataType: 'json',
        success: function(data) {
            if (data.msg) {
                alert(data.msg);
            }
            if(data.success) {
                location.reload();
            }
        }
    });
}
leiebasen.oppfølging.notat.avvisBetalingsplan = function() {
    $.ajax({
        url: '/oppfølging/index.php?oppslag=betalingsplan_skjema&leieforhold=<?php echo $leieforholdId;?>&oppdrag=oppgave&oppgave=avvis_betalingsplan&id=<?php echo $notatId;?>',
        data: {notat_id: <?php echo json_encode($notatId);?>},
        method: "POST",
        dataType: 'json',
        success: function(data) {
            if (data.msg) {
                alert(data.msg);
            }
            if (data.url) {
                location.href = data.url;
            }
            else if(data.success) {
                location.reload();
            }
        }
    });
}