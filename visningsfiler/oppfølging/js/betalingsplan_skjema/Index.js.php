<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\oppfølging\js\betalingsplan_skjema\Index $this
 * @var int $notatId
 * @var bool $eksisterendePlan
 * @var int $leieforholdId
 * @var string $helligdager
 * @var string $eksisterendePlanJson
 * @var string $leiebetalingerJson
 */
?>
leiebasen.oppfølging.betalingsplanSkjema = leiebasen.oppfølging.betalingsplanSkjema || {};

leiebasen.oppfølging.betalingsplanSkjema.initier = function() {
    this.felter = {
        originalkravFelt: $("[name='originalkrav[]']"),
        terminbeløpFelt: $("[name='terminbeløp']"),
        terminlengdeFelt: $("[name='terminlengde']"),
        startdatoFelt: $("[name='startdato']"),
        avtaletekstFelt: $("[name='avtaletekst']"),
        følgerLeiebetalingerFelt: $("[name='følger_leiebetalinger']")
    }
    this.sumUtestående = 0;
    this.erOriginalplan = false;
    this.plan = <?php echo $eksisterendePlanJson;?>;
    this.redigeringsIdx = null;
    this.helligdager = <?php echo $helligdager;?>;

    /**
     * @typedef {object} this.leiebetalinger
     * @property {boolean} tilgjengelig
     * @property {object} terminlengde
     * @property {object} terminBetalingsfrist
     * @property {object} betalingsElementer
     * @property {number} forfallFastDagIMåneden
     * @property {number} forfallFastUkedag
     * @property {string} forfallFastDato
     * @property {string} førsteLeieTermin
     */
    this.leiebetalinger = <?php echo $leiebetalingerJson;?>;
    this.følgerLeiebetalinger = false;

    $("#plantabell").hide();
    if(this.leiebetalinger.tilgjengelig) {
        this.felter.følgerLeiebetalingerFelt.on('change', function() {
            leiebasen.oppfølging.betalingsplanSkjema.følgerLeiebetalinger = $(this).is(':checked');
            leiebasen.oppfølging.betalingsplanSkjema.oppdaterSkjema();
        });
    }
    else {
        this.felter.følgerLeiebetalingerFelt.attr("disabled", true);
    }

    this.felter.originalkravFelt.on('change', function() {
        leiebasen.oppfølging.betalingsplanSkjema.oppsummerUtestående();
    });
    this.felter.terminbeløpFelt.on('change', function() {
        leiebasen.oppfølging.betalingsplanSkjema.oppdaterSkjema();
    });
    this.felter.terminlengdeFelt.on('change', function() {
        leiebasen.oppfølging.betalingsplanSkjema.oppdaterSkjema();
    });
    this.felter.startdatoFelt.on('change', function() {
        leiebasen.oppfølging.betalingsplanSkjema.oppdaterSkjema();
    });

    /**
     * Sørg for at skjemaet ikke sendes når enter trykkes i avdrag-feltene
     */
    $("#frekvens_feltsett").keydown(function(event){
        if(event.keyCode === 13) {
            event.preventDefault();
            return false;
        }
    });

    /*
    Lag en forenklet statisk visning av alle originalkrav for de tilfellene hvor disse ikke trenger endres
     */
    let redigerOriginalKravKnapp = $('<a>Endre grunnlaget for betalingsplanen</a>');
    $(redigerOriginalKravKnapp).on('click', function(){
        leiebasen.oppfølging.betalingsplanSkjema.felter.originalkravFelt.each(function(idx, felt) {
            $(felt).parent().show();
            $(felt).show();
        });
        $(this).replaceWith('<div style="padding: 1rem; background-color: linen">Velg hvilke utestående beløp som skal inngå i betalingsplanen.</div>');
    });
    this.felter.originalkravFelt.each(function(idx, felt) {
        if(!$(felt).is(":checked")) {
            $(felt).parent().hide();
        }
        $(felt).hide();
    });
    $('#utestående_feltsett').append(redigerOriginalKravKnapp);
    this.oppdaterTotalBeløp();
}

/**
 * Summer alle beløpene i planforslaget som det foreligger
 */
leiebasen.oppfølging.betalingsplanSkjema.summerPlan = function() {
    var sum = 0;
    this.plan.forEach(function(linje) {
        sum += Number(linje.avdrag);
    });
    return sum;
}

/**
 * Reduserer (forkorter) planen med et visst beløp
 *
 * @param reduksjon
 */
leiebasen.oppfølging.betalingsplanSkjema.reduserPlanen = function(reduksjon) {
    while(reduksjon > 0) {
        let sisteAvdrag = this.plan[this.plan.length - 1];
        if (sisteAvdrag.avdrag > reduksjon) {
            sisteAvdrag.avdrag -= reduksjon;
            sisteAvdrag.avdrag = sisteAvdrag.avdrag.toFixed(2);
            reduksjon = 0;
        }
        else {
            reduksjon -= sisteAvdrag.avdrag;
            reduksjon = reduksjon.toFixed(2);
            this.plan.pop();
        }
    }
    this.tegnTabell();
}

/**
 * Legg til 1 dag til dato-streng eller dato
 *
 * @param {string|Date} dato
 * @returns {string|Date}
 */
leiebasen.oppfølging.betalingsplanSkjema.leggTilDag = function(dato) {
    let somStreng = !(dato instanceof Date);
    if (somStreng) {
        dato = new Date(dato + 'T00:00:00Z');
    }
    dato.setUTCDate(dato.getUTCDate()+1);

    if (somStreng) {
        return dato.toISOString().slice(0, 10);
    }
    else {
        return dato;
    }
}

/**
 * Legg til 1 dag til dato-streng eller dato
 *
 * @param {string|Date} dato
 * @returns {string|Date}
 */
leiebasen.oppfølging.betalingsplanSkjema.korrigerForfall = function(dato) {
    let somStreng = !(dato instanceof Date);
    if (somStreng) {
        dato = new Date(dato + 'T00:00:00Z');
    }

    if(this.følgerLeiebetalinger) {
        terminBetalingsfrist = this.leiebetalinger.terminBetalingsfrist;
        dato.setUTCFullYear(dato.getUTCFullYear() + terminBetalingsfrist.y);
        dato.setUTCMonth(dato.getUTCMonth() + terminBetalingsfrist.m);
        dato.setUTCDate(dato.getUTCDate() + terminBetalingsfrist.d);
    }
    while(dato.getUTCDay() === 0 || dato.getUTCDay() > 5 || this.helligdager.indexOf(dato.toISOString().slice(0, 10)) !== -1) {
        dato.setUTCDate(dato.getUTCDate() + 1);
    }

    if (somStreng) {
        return dato.toISOString().slice(0,10);
    }
    else {
        return dato;
    }
}

/**
 * Legg til 1 termin til datostreng eller dato
 *
 * @param {string|Date} dato
 * @returns {string|Date}
 */
leiebasen.oppfølging.betalingsplanSkjema.leggTilTermin = function(dato) {
    let somStreng = !(dato instanceof Date);
    let terminlengde = this.felter.terminlengdeFelt.val();
    if (somStreng) {
        dato = new Date(dato + 'T00:00:00Z');
    }

    if(this.følgerLeiebetalinger) {
        let referanseDato = new Date(dato.toISOString());
        terminlengde = this.leiebetalinger.terminlengde;
        dato.setUTCFullYear(dato.getUTCFullYear() + terminlengde.y);
        dato.setUTCMonth(dato.getUTCMonth() + terminlengde.m);
        dato.setUTCDate(dato.getUTCDate() + terminlengde.d);

        if(this.leiebetalinger.forfallFastDato) {
            if(
                referanseDato.toISOString().slice(5.5) < this.leiebetalinger.forfallFastDato
                && dato.toISOString().slice(5.5) > this.leiebetalinger.forfallFastDato
            ) {
                dato.setUTCMonth(this.leiebetalinger.forfallFastDato.slice(5,2) - 1);
                dato.setUTCDate(this.leiebetalinger.forfallFastDato.slice(8,2));
            }
        }

        if(this.leiebetalinger.forfallFastUkedag) {
            if(
                referanseDato.getDay() !== this.leiebetalinger.forfallFastUkedag - 1
                && dato.getDay() !== this.leiebetalinger.forfallFastUkedag -1
                && Math.abs(dato - referanseDato) > (7 * 24 * 60 * 60 * 1000)
            ) {
                dato = new Date(referanseDato.toISOString());
                dato.setUTCDate(dato.getUTCDate() + (6 + this.leiebetalinger.forfallFastUkedag - dato.getDay()) % 7);
            }
    }

        if(this.leiebetalinger.forfallFastDagIMåneden
            && dato.getUTCDate() !== this.leiebetalinger.forfallFastDagIMåneden
        ) {
            let bruddDato = new Date(referanseDato.toISOString());
            if(this.leiebetalinger.forfallFastDagIMåneden === 't') {
                bruddDato.setUTCMonth(referanseDato.getUTCMonth() + 1);
                bruddDato.setUTCDate(0);
            }
            else if(referanseDato.getUTCDate() > this.leiebetalinger.forfallFastDagIMåneden) {
                bruddDato.setUTCMonth(referanseDato.getUTCMonth() + 1)
                bruddDato.setUTCDate(this.leiebetalinger.forfallFastDagIMåneden);
            }
            else {
                bruddDato.setUTCDate(this.leiebetalinger.forfallFastDagIMåneden);
            }
            dato = bruddDato < dato ? bruddDato : dato;
        }
    }
    else {
    switch (terminlengde) {
        case 'P7D':
                dato.setUTCDate(dato.getUTCDate() + 7);
            break;
        case 'P14D':
                dato.setUTCDate(dato.getUTCDate() + 14);
            break;
        case 'P28D':
                dato.setUTCDate(dato.getUTCDate() + 28);
            break;
        case 'P2M':
                dato.setUTCMonth(dato.getUTCMonth() + 2);
            break;
        case 'P3M':
                dato.setUTCMonth(dato.getUTCMonth() + 3);
            break;
        case 'P6M':
                dato.setUTCMonth(dato.getUTCMonth() + 6);
            break;
        default:
                dato.setUTCMonth(dato.getUTCMonth() + 1);
        }
    }

    if (somStreng) {
        return dato.toISOString().slice(0,10);
    }
    else {
        return dato;
    }
}

/**
 * Bygg, eller bygg på, forslag til betalingsplan
 *
 * @param beløp
 */
leiebasen.oppfølging.betalingsplanSkjema.utvidPlanen = function(beløp) {
    /**
     * @type {string}
     */
    let forfallsdato = this.felter.startdatoFelt.val();
    if(this.følgerLeiebetalinger) {
        forfallsdato = this.leiebetalinger.førsteLeieTermin;
    }
    let terminbeløp = Number(this.felter.terminbeløpFelt.val());
    let avdrag = 0;
    if(this.plan.length) {
        while(forfallsdato <= this.plan[this.plan.length - 1].dato) {
            forfallsdato = this.leggTilTermin(forfallsdato);
        }
    }
    while (beløp > 0) {
        avdrag = Math.min(terminbeløp, beløp).toFixed(2);
        this.plan.push({
            dato: forfallsdato,
            avdrag: avdrag
        });
        beløp -= avdrag;
        beløp = beløp.toFixed(2);
        forfallsdato = this.leggTilTermin(forfallsdato);
    }
}

leiebasen.oppfølging.betalingsplanSkjema.foreslåPlan = function() {
    let planSum = this.summerPlan();
    if (planSum > this.sumUtestående) {
        this.reduserPlanen(planSum - this.sumUtestående);
        this.tegnTabell();
    }
    else if (planSum < this.sumUtestående) {
        this.utvidPlanen(this.sumUtestående - planSum);
        this.tegnTabell();
    }
}

/**
 * Oppdater skjemaet og planen hver gang det er gjort endringer i feltene som danner grunnlaget for den
 */
leiebasen.oppfølging.betalingsplanSkjema.oppdaterSkjema = function() {
    let tHeaders = $("#plantabell th .dt-column-title");
    tHeaders[0].innerHTML = 'Dato (kan tilpasses)';
    if(this.følgerLeiebetalinger) {
        let førsteLeieTermin = new Date(this.leiebetalinger.førsteLeieTermin + 'T00:00:00Z');
        let terminBetalingsfrist = this.leiebetalinger.terminBetalingsfrist;
        førsteLeieTermin.setUTCFullYear(førsteLeieTermin.getUTCFullYear() + terminBetalingsfrist.y);
        førsteLeieTermin.setUTCMonth(førsteLeieTermin.getUTCMonth() + terminBetalingsfrist.m);
        førsteLeieTermin.setUTCDate(førsteLeieTermin.getUTCDate() + terminBetalingsfrist.d);
        this.felter.startdatoFelt.val(førsteLeieTermin.toISOString().substring(0,10));
        tHeaders[0].innerHTML = 'Dato';
    }
    this.felter.startdatoFelt.attr('readonly', this.følgerLeiebetalinger);
    this.felter.terminlengdeFelt.parent().toggle(!this.følgerLeiebetalinger);
    this.oppdaterTotalBeløp();

    let terminbeløp = this.felter.terminbeløpFelt.val();
    let terminlengde = this.felter.terminlengdeFelt.val();
    let startdato = this.felter.startdatoFelt.val();
    if (!this.erOriginalplan && this.sumUtestående && terminbeløp && startdato && (terminlengde || this.følgerLeiebetalinger)) {
        this.plan = [];
        this.foreslåPlan();
    }
    else if (this.erOriginalplan) {
        this.erOriginalplan = false;
        this.tegnTabell();
    }
}

/**
 * Oppsummer utestående på grunnlag av valgte krav
 */
leiebasen.oppfølging.betalingsplanSkjema.oppsummerUtestående = function() {
    let kravIder = [];
    this.felter.originalkravFelt.each(function(idx, felt) {
        if($(felt).is(':checked')) {
            kravIder.push($(felt).val());
        }
    });
    $.ajax({
        url: '/oppfølging/index.php?oppslag=betalingsplan_skjema&leieforhold=<?php echo $leieforholdId;?>&id=<?php echo $notatId ?? '*';?>&oppdrag=hent_data&data=utestående',
        data: {krav: kravIder},
        method: "GET",
        dataType: 'json',
        success: function(data) {
            leiebasen.oppfølging.betalingsplanSkjema.sumUtestående = data.data;
            $(".sum.gjeld").html('<strong>' + data.msg + '</strong>');
            leiebasen.oppfølging.betalingsplanSkjema.oppdaterSkjema();
        }
    });
}

/**
 * Tegn betalingsplan-tabellen
 */
leiebasen.oppfølging.betalingsplanSkjema.tegnTabell = function() {
    $("#plantabell").show();
    leiebasen.datatables.plantabell.clear();
    this.plan.forEach(function(avdrag) {
        leiebasen.datatables.plantabell.row.add({
            dato: '<input type="date" name="avdrag[dato][]" value="' + leiebasen.oppfølging.betalingsplanSkjema.korrigerForfall(avdrag.dato) + '">',
            avdrag: '<input type="number" name="avdrag[beløp][]" step="0.01" min="0" value="' + avdrag.avdrag + '">'
        });
    });
    leiebasen.datatables.plantabell.draw();
    $('input[name="avdrag[dato][]"]').attr('readonly', this.følgerLeiebetalinger);
    $("#plantabell tr input").on('change', leiebasen.oppfølging.betalingsplanSkjema.oppdaterRedigertTabell);
}

/**
 * Oppdater planen dersom det har blitt manuelle endringer i avdrags-tabellen
 *
 * @param event
 */
leiebasen.oppfølging.betalingsplanSkjema.oppdaterRedigertTabell = function (event) {
    var rowIdx = $(event.target).parent().parent().index();
    leiebasen.oppfølging.betalingsplanSkjema.redigeringsIdx = rowIdx;
    var avdragsDato = $(event.target).parent().parent().find("input[type='date']").val();
    var avdragsBeløp = $(event.target).parent().parent().find("input[type='number']").val();
    leiebasen.oppfølging.betalingsplanSkjema.plan[rowIdx] = {
        dato: avdragsDato,
        avdrag: Number(avdragsBeløp)
    };
    leiebasen.oppfølging.betalingsplanSkjema.foreslåPlan();
}

leiebasen.oppfølging.betalingsplanSkjema.lastNedVedlegg = function() {
    window.open('/oppfølging/index.php?oppslag=notat&oppdrag=hent_data&data=vedlegg&id=<?php echo $notatId;?>');
}

leiebasen.oppfølging.betalingsplanSkjema.oppdaterTotalBeløp = function () {
    if(this.følgerLeiebetalinger) {
        let elementer = [];
        let totalbeløp = Number(this.felter.terminbeløpFelt.val());
        Object.keys(this.leiebetalinger.betalingsElementer).forEach(function(beskrivelse) {
            elementer.push(beskrivelse + '&nbsp;' + leiebasen.kjerne.kr(leiebasen.oppfølging.betalingsplanSkjema.leiebetalinger.betalingsElementer[beskrivelse]) + '<br>');
            totalbeløp = totalbeløp + Number(leiebasen.oppfølging.betalingsplanSkjema.leiebetalinger.betalingsElementer[beskrivelse]);
        });
        elementer.push('Nedbetalingsavdrag&nbsp;' + leiebasen.kjerne.kr(this.felter.terminbeløpFelt.val()));
        let html = '<ul><li>'
            + elementer.join('</li><li>')
            + '</li></ul>'
            + '<strong>Totalt hver betaling: '
            + leiebasen.kjerne.kr(totalbeløp)
            + '</strong>';
        $('.totalbeløp').html(html).show();
    }
    else {
        $('.totalbeløp').hide();
    }
}

leiebasen.oppfølging.betalingsplanSkjema.slettVedlegg = function() {
    if(confirm('Er du sikker på at du vil slette det eksisterende vedlegget?')) {
        $.ajax({
            url: '/oppfølging/index.php?oppslag=notat_skjema&id=<?php echo $notatId;?>&oppdrag=oppgave&oppgave=slett_vedlegg',
            data: {notat_id: <?php echo $notatId ?? '"*"';?>},
            method: "POST",
            dataType: 'json',
            success: function(data) {
                $("div.eksisterende-vedlegg").hide();
                if (data.msg) {
                    alert(data.msg);
                }
            }
        });
    }
}

leiebasen.oppfølging.betalingsplanSkjema.initier();
leiebasen.oppfølging.betalingsplanSkjema.erOriginalplan = <?php echo json_encode($eksisterendePlan);?>;
leiebasen.oppfølging.betalingsplanSkjema.oppsummerUtestående();