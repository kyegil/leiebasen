<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\oppfølging\js\notat_liste\Index $this
 * @var int $leieforholdId
 */
?>
leiebasen.oppfølging.notatListe = leiebasen.oppfølging.notatListe || {};
leiebasen.oppfølging.notatListe.nullstillAvventing = function(leieforholdId) {
    $.ajax({
        url: '/oppfølging/index.php?oppslag=notat_liste&oppdrag=oppgave&oppgave=nullstill_avventing',
        data: {leieforhold_id: leieforholdId},
        method: "POST",
        dataType: 'json',
        success: function(data) {
            if (data.msg) {
                alert(data.msg);
            }
            $('.gjenopptaking.gjenopptaking-' + leieforholdId).hide();
        }
    });
}
$('#leieforhold-velger').on('select2:select', function(e) {
    var leieforholdId = e.params.data.id;
    if (leieforholdId) {
        window.location = '/oppfølging/index.php?oppslag=oversikt&id=' + leieforholdId;
    }
});