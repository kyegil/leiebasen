<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\min_søknad\epost\Passordlenke $this
 * @var string $språk
 * @var string $utleier
 * @var string $lenke
 */
?><p>Du har bedt om å resette passordet for en søknad hos <?php echo $utleier;?>.</p>
    <div>Følg lenken under for å resette passordet:</div>
<?php echo $lenke;?>