<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\min_søknad\epost\ReferanseOversikt $this
 * @var string $epost
 * @var string $språk
 * @var string $utleier
 * @var string $referanser
 */
?><p>Vi har mottatt en forespørsel etter innloggingsreferanser for søknader registrert hos <?php echo $utleier;?>.</p>
    <div>Vi har funnet følgende søknader med epostadressen <?php echo $epost;?></div>
<?php echo $referanser;?>