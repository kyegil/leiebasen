<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\min_søknad\epost\Passordlenke $this
 * @var string $språk
 * @var string $utleier
 * @var string $lenke
 */
?><p>You have asked to reset the password for an application at <?php echo $utleier;?>.</p>
    <div>Follow the link below to reset your password:</div>
<?php echo $lenke;?>