<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\min_søknad\epost\ReferanseOversikt $this
 * @var string $epost
 * @var string $språk
 * @var string $utleier
 * @var string $referanser
 */
?><p>This is the response for a request for login references for applications registered with <?php echo $utleier;?>.</p>
<div>The following applications were found assigned to the email <?php echo $epost;?></div>
<?php echo $referanser;?>