<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\min_søknad\html\shared\body\Hovedmeny $this
 * @var string $språkKode
 */
?><div class="menu-modal hovedmeny cover-modal" data-modal-target-string=".hovedmeny" aria-expanded="false">
    <div class="menu-modal-inner modal-inner bg-body-background">
        <div class="menu-wrapper section-inner">
            <div class="menu-top">
                <div class="menu-modal-toggles header-toggles">
                    <a href="#"
                       class="toggle nav-toggle nav-untoggle"
                       data-toggle-target=".hovedmeny"
                       data-toggle-screen-lock="true"
                       data-toggle-body-class="showing-menu-modal"
                       aria-pressed="false"
                       data-set-focus="header .nav-toggle"
                    >
                        <div class="bars">
                            <div class="bar"></div>
                            <div class="bar"></div>
                            <div class="bar"></div>
                        </div><!-- .bars -->
                    </a><!-- .nav-toggle -->
                </div><!-- .menu-modal-toggles -->

                <ul class="main-menu reset-list-style">
                    <li class="menylenke"><a href="/min-søknad/index.php?oppslag=endre_passord&returi=default">Endre passord</a></li>
                    <li class="menylenke current_page_ancestor current_page_parent"><a href="/offentlig/index.php?oppslag=index&oppdrag=avslutt">Logg ut</a></li>
                </ul><!-- .main-menu -->
            </div><!-- .menu-top -->
        </div><!-- .menu-wrapper -->
    </div><!-- .menu-modal-inner -->
</div><!-- .menu-modal -->
