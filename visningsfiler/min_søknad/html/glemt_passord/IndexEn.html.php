<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\min_søknad\html\glemt_passord\Index $this
 * @var string $skjema
 * @var string $språkKode
 */
?>
<p>If you have forgotten the password needed to log in to your application, you may be able to reset it. Please enter the login reference that was assigned to the application.</p>
<?php echo $skjema;?>
<p><a title="Click here if you have forgotten your login reference" href="/min-søknad/index.php?oppslag=glemt_referanse&l=<?php echo $språkKode;?>">Forgotten Your Login Reference?</a></p>

