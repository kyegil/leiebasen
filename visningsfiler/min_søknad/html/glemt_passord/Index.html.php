<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\min_søknad\html\glemt_passord\Index $this
 * @var string $skjema
 * @var string $språkKode
 */
?>
<p>Dersom du har glemt passordet som trengs for å logge inn til din søknad, kan det hende det er mulig å resette det. Vennligst oppgi innloggings-referansen som ble tildelt for søknaden.</p>
<?php echo $skjema;?>
<p><a title="Klikk her dersom du har glemt innloggingsreferansen din" href="/min-søknad/index.php?oppslag=glemt_referanse&l=<?php echo $språkKode;?>">Glemt referansen?</a></p>

