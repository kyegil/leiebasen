<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\min_søknad\html\login\Index $this
 * @var string $skjema
 * @var string $språkKode
 */
?>
<p>For å se eller endre en eksisterende søknad må du logge inn med referansen som ble sendt på e-post, samt passordet du oppgav i søknaden.<br>
</p>
<?php echo $skjema;?>
<p><a title="Klikk her dersom du må ha hjelp til å finne innloggingsreferansen din" href="/min-søknad/index.php?oppslag=glemt_referanse&l=<?php echo $språkKode;?>">Glemt referansen?</a></p>
<p><a title="Klikk her dersom du har glemt passordet" href="/min-søknad/index.php?oppslag=glemt_passord&l=<?php echo $språkKode;?>">Glemt passordet?</a></p>
