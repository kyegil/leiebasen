<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\min_søknad\html\login\Index $this
 * @var string $skjema
 * @var string $språkKode
 */
?>
<p>In order to access your application you need to log in using the reference that was issued when submitting your application, and the password that you chose with your application.<br>
</p>
<?php echo $skjema;?>
<p><a title="Click here if you need help finding your login reference" href="/min-søknad/index.php?oppslag=glemt_referanse&l=<?php echo $språkKode;?>">Forgotten Your Login Reference?</a></p>
<p><a title="Click here if you have forgotten your password" href="/min-søknad/index.php?oppslag=glemt_passord&l=<?php echo $språkKode;?>">Forgotten Your Password?</a></p>