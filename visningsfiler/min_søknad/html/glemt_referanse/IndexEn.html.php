<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\min_søknad\html\glemt_referanse\Index $this
 * @var string $skjema
 * @var string $språkKode
 */
?>
<p>If you have forgotten the reference needed to log in to your application, you can enter the email address provided for the main applicant here, and we'll see if we can obtain it.<br>
    If you did not provide an email address in the application, we're afraid this will not be possible</p>
<?php echo $skjema;?>