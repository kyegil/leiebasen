<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\min_søknad\html\glemt_referanse\Index $this
 * @var string $skjema
 * @var string $språkKode
 */
?>
<p>Dersom du har glemt referansen som trengs for å logge inn til din søknad, kan du oppgi e-post-adressen som ble oppgitt for hovedsøker her, for å prøve å finne den.<br>
    Om du ikke oppga noen e-postadresse i søknaden, er dette desverre ikke mulig.
</p>
<?php echo $skjema;?>