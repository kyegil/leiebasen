<?php
/**
 * Leiebasen Skadeliste
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\html\skadeliste\body\main\Index $this
 * @var string|\Kyegil\Leiebasen\Visning $skadetabell
 * @var string|\Kyegil\Leiebasen\Visning $meldSkadeKnapp
 */
?>
<div>
    Skaderegisteret er tilgjengelig for, og kan oppdateres av alle beboere og ansatte.<br>
    Dersom du ønsker det kan du motta epost ved oppdateringer i en skademelding.
</div>
<?php echo $skadetabell;?>
<?php echo $meldSkadeKnapp;?>