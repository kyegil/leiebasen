<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\html\søknad_skjema\body\main\Index $this
 * @var \Kyegil\Leiebasen\Visning|string $skjema
 * @var \Kyegil\Leiebasen\Visning|string $knapper
 */
?>
<?php echo $skjema;?>
<?php echo $knapper;?>
