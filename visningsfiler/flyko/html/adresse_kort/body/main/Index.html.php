<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\html\adresse_kort\body\main\Index $this
 * @var \Kyegil\Leiebasen\Visning|string $kort
 * @var \Kyegil\Leiebasen\Visning|string $knapper
 */
?>
<?php echo $kort;?>
<?php echo $knapper;?>
