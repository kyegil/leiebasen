<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\html\søknad_skjema_skjema\body\main\Index $this
 * @var \Kyegil\Leiebasen\Visning|string $skjema
 * @var \Kyegil\Leiebasen\Visning|string $knapper
 */
?>
<div>
    Rediger søknadsskjemaet.
</div>
<?php echo $skjema;?>
<?php echo $knapper;?>
