<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\html\søknad_skjema_skjema\body\main\index\StandardFelt $this
 * @var \Kyegil\Leiebasen\Visning $innhold
 */
?>
<div class="søknadsfelt standard" draggable="true" ondragstart="drag(event)">
    <div
            class="hempe"
    <div
            class="felt-grense nord"
            ondrop="leiebasen.flyko.SøknadSkjemaSkjema.drop(event)"
            ondragover="allowDrop(event)"> </div>
    <div class="feltlinje">
        <div class="felt-grense vest"
             ondrop="leiebasen.flyko.SøknadSkjemaSkjema.drop(event)"
             ondragover="leiebasen.flyko.SøknadSkjemaSkjema.allowDrop(event)"
        ></div>
        <div class="felt-kjerne"><?php echo $innhold;?></div>
        <div
                class="felt-grense øst"
                ondrop="leiebasen.flyko.SøknadSkjemaSkjema.drop(event)"
                ondragover="leiebasen.flyko.SøknadSkjemaSkjema.allowDrop(event)"
        ></div>
    </div>
    <div
            class="felt-grense sør"
            ondrop="leiebasen.flyko.SøknadSkjemaSkjema.drop(event)"
            ondragover="leiebasen.flyko.SøknadSkjemaSkjema.allowDrop(event)"
    ></div>
</div>