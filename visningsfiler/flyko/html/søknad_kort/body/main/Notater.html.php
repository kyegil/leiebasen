<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\html\søknad_kort\body\main\Notater $this
 * @var \Kyegil\Leiebasen\Visning|string $notatListe
 * @var \Kyegil\Leiebasen\Visning|string $notatSkjema
 */
?>
<div id="søknad-notater" class="søknad-notater">
    <h3>Administrative notater</h3>
    <p class="utdyping">Merknader og interne tilleggsopplysninger som følger søknaden. Notatene kan ikke leses av søker.</p>
    <?php echo $notatListe;?>
    <?php echo $notatSkjema;?>
</div>
