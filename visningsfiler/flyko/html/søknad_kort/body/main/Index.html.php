<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\html\søknad_kort\body\main\Index $this
 * @var \Kyegil\Leiebasen\Visning|string $kort
 * @var \Kyegil\Leiebasen\Visning|string $notater
 * @var \Kyegil\Leiebasen\Visning|string $knapper
 */
?>
<?php echo $kort;?>
<?php echo $notater;?>
<?php echo $knapper;?>
