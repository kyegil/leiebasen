<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\html\søknad_kort\body\main\NotatSkjema $this
 * @var \Kyegil\Leiebasen\Visning|string $skjema
 * @var \Kyegil\Leiebasen\Visning|string $knapper
 */
?>
<?php echo $skjema;?>
<?php echo $knapper;?>
