<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\html\søknad_kort\body\main\notater\Notat $this
 * @var string $notat_id
 * @var string $søknad_id
 * @var string $bruker_id
 * @var \Kyegil\Leiebasen\Visning|string $søknad
 * @var \Kyegil\Leiebasen\Visning|string $forfatter
 * @var \Kyegil\Leiebasen\Visning|string $tidspunkt
 * @var \Kyegil\Leiebasen\Visning|string $tekst
 * @var \Kyegil\Leiebasen\Visning|string $sletteKnapp
 */
?>
<div class="søknad-notat">
    <div>
        <strong>
            <span class="forfatter"><?php echo $forfatter;?></span>
            <span class="tidspunkt"><?php echo $tidspunkt;?></span>:
        </strong>
    </div>
    <?php echo $sletteKnapp;?>
    <div class="søknad-notat-tekst"><?php echo $tekst;?></div>
    <hr>
</div>