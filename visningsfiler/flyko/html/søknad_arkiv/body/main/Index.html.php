<?php
/**
 * Leiebasen Søknadsarkiv (inaktive søknader)
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\html\søknad_arkiv\body\main\Index $this
 * @var string|\Kyegil\Leiebasen\Visning $tabell
 * @var string|\Kyegil\Leiebasen\Visning $knapper
 */
?>
<?php echo $tabell;?>
<?php echo $knapper;?>