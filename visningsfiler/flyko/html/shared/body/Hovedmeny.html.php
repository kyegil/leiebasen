<?php
/**
 * Leiebasen Visningsblokk
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\html\shared\body\Hovedmeny $this
 * @var string $utleier
 */
?><div class="menu-modal hovedmeny cover-modal" data-modal-target-string=".hovedmeny" aria-expanded="false">
    <div class="menu-modal-inner modal-inner bg-body-background">
        <div class="menu-wrapper section-inner">
            <div class="menu-top">
                <div class="menu-modal-toggles header-toggles">
                    <a href="#"
                       class="toggle nav-toggle nav-untoggle"
                       data-toggle-target=".hovedmeny"
                       data-toggle-screen-lock="true"
                       data-toggle-body-class="showing-menu-modal"
                       aria-pressed="false"
                       data-set-focus="#flyko-header .nav-toggle"
                    >
                        <div class="bars">
                            <div class="bar"></div>
                            <div class="bar"></div>
                            <div class="bar"></div>
                        </div><!-- .bars -->
                    </a><!-- .nav-toggle -->
                </div><!-- .menu-modal-toggles -->

                <ul class="main-menu reset-list-style">
                    <li class="menylenke menylenke_has_children menu-item-has-children"><a href="/flyko/index.php?oppslag=adgang_liste&returi=default">Flyko</a>
                        <ul class="children">
                            <li class="menylenke"><a href="/flyko/index.php?oppslag=adgang_liste&returi=default">Flyko's medlemmer</a></li>
                        </ul>
                    </li>
                    <li class="menylenke menylenke_has_children menu-item-has-children"><a href="/flyko/index.php?oppslag=søknad_liste&returi=default">Søknader</a>
                        <ul class="children">
                            <li class="menylenke"><a href="/flyko/index.php?oppslag=søknad_liste&returi=default">Oversikt over innkomne søknader</a></li>
                            <li class="menylenke"><a href="/flyko/index.php?oppslag=søknad_skjema_liste&returi=default">Oversikt over søknadsskjema</a></li>
                            <li class="menylenke"><a href="/flyko/index.php?oppslag=søknad_arkiv&returi=default">Inaktive søknader</a></li>
                        </ul>
                    </li>
                    <li class="menylenke"><a href="/flyko/index.php?oppslag=framleie_liste&returi=default">Framleie</a></li>
                    <li class="menylenke menylenke_has_children menu-item-has-children"><a href="/flyko/index.php?oppslag=leieobjekter_liste&returi=default">Boligmassen</a>
                        <ul class="children">
                            <li class="menylenke"><a href="/flyko/index.php?oppslag=leieobjekter_ledige&returi=default">Ledige boliger</a></li>
                            <li class="menylenke"><a href="/flyko/index.php?oppslag=skadeliste&returi=default">Skader</a></li>
                        </ul>
                    </li>
                    <li class="menylenke menylenke_has_children current_page_ancestor current_page_parent menu-item-has-children"><a href="/flyko/index.php?oppslag=drift_status&returi=default"><?php echo $utleier;?></a>
                        <ul class="children">
                            <li class="menylenke"><a href="/drift/">Gå til Drift</a></li>
                            <li class="menylenke"><a href="/mine-sider/">Gå til Mine sider</a></li>
                        </ul>
                    </li>
                </ul><!-- .main-menu -->
            </div><!-- .menu-top -->

            <div class="menu-bottom">
<!--                <p class="menu-copyright"><a href="https://docs.google.com/document/d/1R5SYhkNkjdplY4INo9yDAWyz3IiayhwAjcdufe8gr8E/" target="_blank">Om Flyko-området</a></p>-->
            </div><!-- .menu-bottom -->
        </div><!-- .menu-wrapper -->
    </div><!-- .menu-modal-inner -->
</div><!-- .menu-modal -->
