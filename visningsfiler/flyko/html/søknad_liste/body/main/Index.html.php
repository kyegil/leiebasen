<?php
/**
 * Leiebasen Søknadsliste
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\html\søknad_liste\body\main\Index $this
 * @var string|\Kyegil\Leiebasen\Visning $filtre
 * @var string|\Kyegil\Leiebasen\Visning $tabell
 * @var string|\Kyegil\Leiebasen\Visning $knapper
 */
?>
<?php echo $filtre;?>
<?php echo $tabell;?>
<?php echo $knapper;?>