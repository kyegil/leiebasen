<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\html\oversikt\body\main\Index $this
 */
?><div class="dashboard rutenett">
    <div class="flis">
        <a href="/flyko/index.php?oppslag=søknad_liste&returi=default">
            <div class="rutenett-knapp">
                <div class="ikonbeholder">
<!--                    <img src="/pub/media/bilder/flyko/oversiktsfliser/mine-regninger-trn.svg" height="414" width="421" alt="mine regninger">-->
                </div>
                <h3 class="flistekst">søknader</h3>
            </div>
        </a>
    </div>

    <div class="flis">
        <a href="/flyko/index.php?oppslag=leieobjekter_ledige&returi=default">
            <div class="rutenett-knapp">
                <div class="ikonbeholder">
                    <!--                    <img src="/pub/media/bilder/flyko/oversiktsfliser/mine-regninger-trn.svg" height="414" width="421" alt="mine regninger">-->
                </div>
                <h3 class="flistekst">ledige boliger</h3>
            </div>
        </a>
    </div>

    <div class="flis">
        <a href="/flyko/index.php?oppslag=framleie_liste&returi=default">
            <div class="rutenett-knapp">
                <div class="ikonbeholder">
                    <!--                    <img src="/pub/media/bilder/flyko/oversiktsfliser/mine-regninger-trn.svg" height="414" width="421" alt="mine regninger">-->
                </div>
                <h3 class="flistekst">framleie</h3>
            </div>
        </a>
    </div>
</div>
