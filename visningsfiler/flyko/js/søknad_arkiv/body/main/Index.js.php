<?php
/**
 * Leiebasen Skript-fil
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\js\søknad_arkiv\body\main\Index $this
 * @var string $ekstraJs
 */
?>
leiebasen.flyko.søknadArkiv = {
    renderJson: function(data, type, row, meta) {
        if(data === null) {
            return null;
        }
        if (type === 'display') {
            let obj = JSON.parse(data);
            if (obj instanceof Array) {
                return obj.join('/');
            }
            return JSON.stringify(obj);
        }
        return data;
    },
    renderBool: function(data, type, row, meta) {
        if (type === 'display' && data) {
            return '✓';
        }
        return data;
    },
    renderDate: function(data, type, row, meta) {
        if ((type === 'display' || type === 'filter') && data) {
            return data.split('-').reverse().join('.');
        }
        return data;
    },
    renderOppdatert: function(data, type, row, meta) {
        if ((type === 'display' || type === 'filter') && data) {
            return data.split('-').reverse().join('.');
        }
        else if (type === 'sort') {
            return data ? data : row.registrert;
        }
        return data;
    }

};
<?php echo $ekstraJs;?>
