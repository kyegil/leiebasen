<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\js\søknad_skjema\body\main\Index $this
 * @var string $script
 */
?>
leiebasen.flyko.søknadSkjema = {
    bekreftDeaktivering: function (checkbox) {
        if (!checkbox.checked) {
            checkbox.checked = !window.confirm('Er du sikker på at du vil deaktivere søknaden?\nInaktive søknader vil ikke lenger være tilgjengelig fra søknadsoversikten.');
        }
    }
};
<?php echo $script;?>