<?php
/**
 * Leiebasen Visningsmal
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\js\adgang_opprett\body\main\Index $this
 */
?>
leiebasen.flyko.adgangOpprett = {
    navnesøkFelt: $("#navnesøk-felt"),
    nyoppføringCheckbox: $("#nyoppføring-checkbox"),
    nyoppføring: {
        felter: $("#nyoppføring-felter"),
        erOrg: $("#nyoppføring-er-org"),
        fornavn: $("#nyoppføring-fornavn-felt"),
        etternavn: $("#nyoppføring-etternavn-felt"),
        navn: $("#nyoppføring-navn-felt"),
        fødselsdato: $("#nyoppføring-fødselsdato-felt"),
        orgNr: $("#nyoppføring-orgnr-felt"),
        epost: $('#nyoppføring-epost-felt')
    }
};

/**
 * Returnerer sann dersom et eksisterende oppslag er valgt
 *
 * @returns {boolean}
 */
leiebasen.flyko.adgangOpprett.eksisterendeOppslagErValgt = function() {
    var valgtPerson = this.navnesøkFelt.val();
    return parseInt(valgtPerson) > 0;
}

/**
 * Returnerer sann dersom det er skrevet inn fritekst uten match
 *
 * @returns {boolean}
 */
leiebasen.flyko.adgangOpprett.erFritekstNavn = function() {
    var verdi = this.navnesøkFelt.val();
    return (verdi.length > 0) && !(parseInt(verdi) > 0);
}

/**
 * Vis skjema for å registrere nytt adressekort
 */
leiebasen.flyko.adgangOpprett.visNyOppføringSkjema = function() {

    /**
     * Hent navnet ifra søkefeltet, og pass på at hvert navn begynner med stor bokstav
     * Navnet vil brukes for auto-utfylling av registreringsskjemaet
     */
    var navn = this.navnesøkFelt.val().trim();
    if (navn.length) {
        var navnElementer = navn.split(" ");
        for (let i = 0; i < navnElementer.length; i++) {
            navnElementer[i] = navnElementer[i][0].toUpperCase() + navnElementer[i].substr(1);
        }
        navn = navnElementer.join(" ");
    }

    /**
     * Se etter eksisterende verdier i de skjulte feltene
     */
    var /** string */ nyoppføringFornavn = this.nyoppføring.fornavn.val();
    var /** string */ nyoppføringEtternavn = this.nyoppføring.etternavn.val();
    var /** string */ nyoppføringNavn = this.nyoppføring.navn.val();

    /**
     * Bruk navnet i søkefeltet til å forhåndsfylle registreringsskjemaet
     */
    if (!nyoppføringFornavn.length && !nyoppføringEtternavn.length && navn.includes(" ")) {
        var sisteMellomrom = navn.lastIndexOf(' ');
        var fornavn = navn.substring(0, sisteMellomrom);
        var etternavn = navn.substring(sisteMellomrom + 1);
        this.nyoppføring.fornavn.val(fornavn);
        this.nyoppføring.etternavn.val(etternavn);
        this.nyoppføring.erOrg.prop('checked', false);
    }
    if (!nyoppføringNavn && this.erFritekstNavn()) {
        this.nyoppføring.navn.val(navn);
    }
    if (navn.length > 0 && !navn.includes(" ")) {
        this.nyoppføring.erOrg.prop('checked', true);
    }

    this.veksleMellomPersonOgOrg();

    /**
     * Sett påkrevde felter, og vis skjemaet
     */
    this.navnesøkFelt.prop({required: false});
    this.nyoppføring.epost.prop({required: true});
    this.nyoppføringCheckbox.prop('checked', true);
    this.navnesøkFelt.val(null).trigger('change');
    this.nyoppføring.felter.show();
}


/**
 * Skjul skjemaet for å registrere nytt adressekort
 */
leiebasen.flyko.adgangOpprett.skjulNyOppføringSkjema = function() {
    this.navnesøkFelt.prop({required: true});
    this.nyoppføring.felter.hide()
        .find('input').prop({required: false});
}

/**
 * Veklse mellom organisasjonsnavn-felt og personnavn-felt
 */
leiebasen.flyko.adgangOpprett.veksleMellomPersonOgOrg = function() {
    var erOrg = this.nyoppføring.erOrg.is(":checked");
    if (erOrg) {
        this.nyoppføring.fornavn.prop({disabled: true, required: false}).parent().hide();
        this.nyoppføring.etternavn.prop({disabled: true, required: false}).parent().hide()
        this.nyoppføring.fødselsdato.prop({disabled: true}).parent().hide()
        this.nyoppføring.navn.prop({disabled: false, required: true}).parent().show();
        this.nyoppføring.orgNr.prop({disabled: false}).parent().show()
    }
    else {
        this.nyoppføring.fornavn.prop({disabled: false, required: true}).parent().show();
        this.nyoppføring.etternavn.prop({disabled: false, required: true}).parent().show();
        this.nyoppføring.fødselsdato.prop({disabled: false}).parent().show()
        this.nyoppføring.navn.prop({disabled: true, required: false}).parent().hide()
        this.nyoppføring.orgNr.prop({disabled: true}).parent().hide()
    }
}

/**
 * Vis eller skjul skjema for å registrere nytt adressekort etter behov
 */
leiebasen.flyko.adgangOpprett.visSkjulNyoppføring = function() {
    var erNyOppføring = this.nyoppføringCheckbox.is(":checked") || this.erFritekstNavn();
    if (erNyOppføring) {
        this.visNyOppføringSkjema();
    }
    else {
        this.nyoppføringCheckbox.prop('checked', false);
        this.skjulNyOppføringSkjema();
    }
}


/**
 * Listener for dropdown-søk
 */
leiebasen.flyko.adgangOpprett.navnesøkFelt.on('select2:select', function() {
    if (leiebasen.flyko.adgangOpprett.eksisterendeOppslagErValgt()) {
        leiebasen.flyko.adgangOpprett.nyoppføringCheckbox.prop('checked', false);
    }
    leiebasen.flyko.adgangOpprett.visSkjulNyoppføring();
});

/**
 * Listener for checkbox
 */
leiebasen.flyko.adgangOpprett.nyoppføringCheckbox.on('click', function() {
    var checked = leiebasen.flyko.adgangOpprett.nyoppføringCheckbox.prop('checked');
    if (checked) {
        // Dersom du velger å legg til ny, fjernes evt oppslag du har valgt
        if (leiebasen.flyko.adgangOpprett.eksisterendeOppslagErValgt()) {
            leiebasen.flyko.adgangOpprett.navnesøkFelt.val(null).trigger('change');
        }
    }
    leiebasen.flyko.adgangOpprett.visSkjulNyoppføring();
});

// I utgangspunktet skal nyregistrering-skjemaet skjules
leiebasen.flyko.adgangOpprett.skjulNyOppføringSkjema();