<?php
/**
 * Leiebasen Skript-fil
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\js\adgang_liste\body\main\Index $this
 */
?>
leiebasen.flyko.adgangListe = {
    slettAdgang: function(personId, navn) {
        if(confirm('Er du sikker på at du vil slette ' + navn + ' sin adgang til FlyKo-sidene?')) {
            $.ajax({
                url: '/flyko/index.php?oppslag=adgang_liste&id=' + personId + '&oppdrag=oppgave&oppgave=slett_adgang',
                data: {person_id: personId},
                method: "POST",
                dataType: 'json',
                success: function(data, textStatus, jqXHR) {
                    if (data.msg) {
                        alert(data.msg);
                    }
                    if(data.success) {
                        location.reload();
                    }
                }
            });
        }
    }
};