<?php
/**
 * Leiebasen Skript-fil
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\js\søknad_liste\body\main\Index $this
 * @var string $ekstraJs
 */
?>
leiebasen.flyko.søknadListe = {
    renderJson: function(data, type, row, meta) {
        if(data === null) {
            return null;
        }
        if (type === 'display') {
            let obj = JSON.parse(data);
            if (obj instanceof Array) {
                return obj.join('/');
            }
            return JSON.stringify(obj);
        }
        return data;
    },
    renderBool: function(data, type, row, meta) {
        if (type === 'display' && data) {
            return '✓';
        }
        return data;
    },
    renderDate: function(data, type, row, meta) {
        if ((type === 'display' || type === 'filter') && data) {
            if (row.utløpt) {
                return '<span class="utløpt">' + data.split('-').reverse().join('.') + '</span>'
            }
            return data.split('-').reverse().join('.');
        }
        return data;
    },
    renderOppdatert: function(data, type, row, meta) {
        if ((type === 'display' || type === 'filter') && data) {
            if (row.utløpt) {
                return '<span class="utløpt">' + data.split('-').reverse().join('.') + '</span>'
            }
            return data.split('-').reverse().join('.');
        }
        else if (type === 'sort') {
            return data ? data : row.registrert;
        }
        return data;
    }

};
<?php echo $ekstraJs;?>

$("#kategori-filter").on('change', function(e) {
    $('#søknader').DataTable().ajax.reload();
});
