<?php
/**
 * Leiebasen Skript-fil
 *
 * @var \Kyegil\Leiebasen\Visning\flyko\js\søknad_kort\body\main\Index $this
 * @var string $ekstraJs
 */
?>
leiebasen.flyko.søknadKort = {
    slettNotat: function(notatId) {
        if (confirm('Er du sikker på at du vil slette notatet?')) {
            $.ajax({
                url: '/flyko/index.php?oppslag=søknad_kort&id=' + notatId + '&oppdrag=oppgave&oppgave=slett_notat',
                data: {notat_id: notatId},
                method: "POST",
                dataType: 'json',
                success: function(data, textStatus, jqXHR) {
                    if (data.msg) {
                        alert(data.msg);
                    }
                    if(data.success) {
                        location.reload();
                    }
                }
            });
        }
    }
};